(function (eventHandler) {
	eventHandler.menu.htmlValidation = {
        /**
        * Function to validate HTML content in editor.
        */
        validateHTML: function(param,targetNode){
            var returnValue = true;
            var userRole = $('#contentContainer').attr('data-role');
            /* Iterate through "element" in json ruleObject */
            if($('*[data-validation-rid]').length > 0){
                $('*[data-validation-rid]').removeAttr('data-validation-rid');
            }
            if($('#navContainer #validationDivNode').find('table tbody').length > 0){
                $('#navContainer #validationDivNode').find('table tbody').empty();
            }
            if(validationRules && validationRules != "" && (typeof validationRules == 'object') && !$.isEmptyObject(validationRules)){
                var checkingElements = validationRules.element;
                $(targetNode).find('.progressIcon').removeClass('hidden');
                checkingElements.forEach(function(key,value){
                    var roleList = key.role;
                    var userRoleRegex = '\,'+userRole+'\,';
                    var userRoleObj = eventHandler.menu.htmlValidation.getRegexObject(userRoleRegex);
                    if(!userRole || (userRole && roleList && roleList!= '' && roleList.match(userRoleObj))){
                        // Get the element mentioned in the "selector" of rules 
                        var nodeToCheck = eventHandler.menu.htmlValidation.getSelectorElement(key);
                        // Check if the element is mandatory and the condition satifies
                        var mandatoryElement = eventHandler.menu.htmlValidation.checkMandatory(nodeToCheck,key);
                        // Get the validation message based on the result 
                        var validationMessage = eventHandler.menu.htmlValidation.getValidationMesg(mandatoryElement,key);
                        eventHandler.menu.htmlValidation.showValidationMessage(validationMessage);
                        // If node exists, then iterate through collected nodes and check for attributes mentioned in rules 
                        nodeToCheck.each(function(index, object){
                            checkObject = this;
                            if(key.attributes){
                                var checkAttr = key.attributes;
                                // Iterate through the attribute list from rules. Check if attribute exists and the attribute value. And throw corresponding validation message
                                checkAttr.forEach(function(key, value){
                                    var mandatoryElement = eventHandler.menu.htmlValidation.checkMandatory(checkObject,key);
                                    var attrValue = key.value;
                                    if(!mandatoryElement || !attrValue){
                                        var validationMessage = eventHandler.menu.htmlValidation.getValidationMesg(mandatoryElement,key);
                                        eventHandler.menu.htmlValidation.showValidationMessage(validationMessage,checkObject);
                                    }
                                    if(mandatoryElement){
                                        var attrValCheck = eventHandler.menu.htmlValidation.checkValue(checkObject,key);
                                        var validationMessage = eventHandler.menu.htmlValidation.getValidationMesg(attrValCheck,key);
                                        eventHandler.menu.htmlValidation.showValidationMessage(validationMessage,checkObject);
                                    }
                                });
                            }
                            eventHandler.menu.htmlValidation.processValFunctions(key.functions,checkObject);
                        });
                    }
                    
                });
                // Show validation message in editor
                if($('#navContainer #validationDivNode').find('table tbody tr').length > 0){
                    $('.navDivContent').find('> .nav-action-icon.active').attr('data-last-active','true');
                    $('.navDivContent').find('> .nav-action-icon').addClass('disabled');
                    eventHandler.menu.edit.toggleNavPane('validationDivNode',$('#navContainer #validationDivNode'));
                    returnValue = false;
                }else{
                    $('.closeValObject').trigger('click');
                }
                $(targetNode).find('.progressIcon').addClass('hidden');
            }
            return returnValue;
        },
        closeValidationMesg: function(param,targetNode){
            if($('#navContainer #validationDivNode').length > 0){
                $('#navContainer #validationDivNode').removeClass('active');
                $('.navDivContent').find('> .nav-action-icon').removeClass('disabled');
                $('.navDivContent').find('> .nav-action-icon[data-last-active]').trigger('click');
                $('.navDivContent').find('> .nav-action-icon[data-last-active]').removeAttr('data-last-active');
            }
        },
        /**
        * Function to check the mandatory element/attribute exist or not.
        * @param - {checkObject} - DOMElementList/DOMElement to check.
        * @param - {ruleObject} - object which has validation rules.
        */
        checkMandatory: function(checkObject,ruleObject){
            var returnValue = true;
            if(checkObject && ruleObject && (checkObject.length == 0 || (checkObject && checkObject.nodeType == 1 && !checkObject.hasAttribute(ruleObject.name))) && ruleObject.mandatory && ruleObject.mandatory == 'true'){
                returnValue = false;
            }
            return returnValue;
        },
        /**
        * Function to check the attribute value or node value.
        * @param - {checkObject} - DOMElementList/DOMElement to check.
        * @param - {ruleObject} - object which has validation rules.
        */
        checkValue : function(checkObject,ruleObject){
            var returnValue = true;
            // Check the attribute value against regex match
            if(checkObject && ruleObject && checkObject.nodeType == 1 && ruleObject.name && ruleObject.value && ruleObject.value.match(/:regex/) && checkObject.hasAttribute(ruleObject.name)){
                regex = eventHandler.menu.htmlValidation.getRegexObject(ruleObject.value);
                if(regex && regex != '' && !checkObject.getAttribute(ruleObject.name).match(regex)){
                    returnValue = false;
                }
            }// Check the node value against regex match
            else if(checkObject && ruleObject && checkObject.nodeType == 1 && ruleObject.name && ruleObject.nodeValue && ruleObject.nodeValue.match(/:regex/) && checkObject.nodeValue){
                regex = eventHandler.menu.htmlValidation.getRegexObject(ruleObject.nodeValue);
                if(regex && regex != '' && !checkObject.getAttribute(ruleObject.name).match(regex)){
                    returnValue = false;
                }
            }// Check the attribute value against equality.
            else if(checkObject && ruleObject && checkObject.nodeType == 1 && ruleObject.name && ruleObject.value && checkObject.hasAttribute(ruleObject.name) && checkObject.getAttribute(ruleObject.name) != ruleObject.value){
                returnValue = false;
            }// Check the node value against equality.
            else if(checkObject && ruleObject && checkObject.nodeType == 1 && ruleObject.name && ruleObject.nodeValue && checkObject.nodeValue && checkObject.nodeValue != ruleObject.nodeValue){
                returnValue = false;
            }
            return returnValue;
        },
        /**
        * Function to select the list of elements mentioned in the selector or the element to look for.
        * @param - {ruleObject} - object which has validation rules or direct string of xpath.
        * @param - {rootElement} - Element - To select the list of element within the given rootElement.
        */
        getSelectorElement: function(ruleObject,rootElement){
            var returnElement = '';var nodeSelector = '';
            if(typeof ruleObject == 'object'){
                nodeSelector = ruleObject.selector;
            }else if(typeof ruleObject == 'string'){
                nodeSelector = ruleObject;
            }
            
            if(rootElement && rootElement != '' && nodeSelector && nodeSelector!=''){
                returnElement = $(rootElement).find(nodeSelector);
            }else if(nodeSelector && nodeSelector!=''){
                returnElement = $(nodeSelector);
            }
            return returnElement;
        },
        /**
        * Function to check the element has specified child element.
        * @param - {checkObject} - Element in which should check the child element.
        * @param - {param} - object - list of parameters which has been given in rules and holds the xpath of the child element to look for.
        */
        checkChildElement: function(checkObject,param){
            var returnValue = true;
            if(checkObject && checkObject.nodeType == 1 && param && param.childSelector && param.childSelector != ""){
                // get the child element
                var nodeToCheck = eventHandler.menu.htmlValidation.getSelectorElement(param.childSelector,checkObject);
                if(nodeToCheck.length == 0){
                    returnValue = false;
                }
            }
            return returnValue;
        },
        /**
        * Function to validate the url.
        * @param - {checkObject} - Element which has the url.
        * @param - {ruleObject} - object - Holds validation rules.
        */
        validateURL: function(checkObject,ruleObject){
            var url = $(checkObject).attr('href');
            var urlResponse = true;
            if(url && url!= ""){
                $.ajax({
                    type: 'HEAD',
                    url: url,
                    async: false,
                    success: function(){
                        urlResponse = true;
                    },
                    error: function() {
                        urlResponse = false;
                    }
                });
            }else{
                urlResponse = false;
            }
            return urlResponse;
        },
        /**
        * Function to call the functions which has been mentioned to call in validation rules.
        * @param - {checkObject} - Element - Which needs to be validated.
        * @param - {functObject} - object - Holds function name and function parameters.
        */
        processValFunctions: function(functObject,checkObject){
            if(functObject){
                var checkFunctions = functObject;
                checkFunctions.forEach(function(key,value){
                    if(key.functionName && key.functionName != ''){
                        var funcToCallName = key.functionName;var functionVal = '';
                        if(key.functionParam && key.functionParam != ''){
                            functionVal = eventHandler.menu.htmlValidation[funcToCallName](checkObject,key.functionParam);
                        }else{
                            functionVal = eventHandler.menu.htmlValidation[funcToCallName](checkObject, key);
                        }
                        // Get the validation message based on the returned value from the function
                        var validationMessage = eventHandler.menu.htmlValidation.getValidationMesg(functionVal,key);
                        // Show the error message
                        eventHandler.menu.htmlValidation.showValidationMessage(validationMessage,checkObject);
                    }
                });
            }
        },
        highlightElements: function(checkObject,param){
            var returnValue = '';
            var nodeSelector = param.selector;
            var validationMesg = param.message;
            if(nodeSelector && nodeSelector!='' && validationMesg && validationMesg!=''){
                var nodeToCheck = eventHandler.menu.htmlValidation.getSelectorElement(nodeSelector);
                nodeToCheck.each(function(index,object){
                    eventHandler.menu.htmlValidation.showValidationMessage(validationMesg,this);
                });
            }
            return returnValue;
        },
        splitParameters: function(param){
            var splittedParam = [];
            if(param.match(/^'/)){
                param = param.replace(/^\s*'|'\s*$/,'');
                param = param.replace(/('\s*\,\s*')/,',');
            }
            param = param.replace(/(\s*\,\s*)/,',');
            splittedParam = param.split(",");
            return splittedParam;
        },
        /*sampleFunction: function(checkObject,param){
            //var paramList = eventHandler.menu.htmlValidation.splitParameters(param);
            console.log('checkObject : ' + checkObject);
            console.log('sampleFunction - 1' + param.param1);
            console.log('sampleFunction - 2' + param.param2);
        },*/
        /**
        * Function to check the email id format and validate the email id.
        * @param - {checkObject} - Element - Which needs to be validated.
        * @param - {param} - object - list of parameters which has been given in rules to validate the condition.
        */
        checkEmailFormat: function(checkObject,param){
            var validateRes = true;
            var emailValue = $(checkObject).text();
            var atCount = eventHandler.menu.htmlValidation.charCount(emailValue,'@'); // get "@" count in the mail id
            var dotCount = eventHandler.menu.htmlValidation.charCount(emailValue,'.'); // get dot(.) count in the mail id
            var spaceCount = eventHandler.menu.htmlValidation.charCount(emailValue,'\\s'); // get space count in the mail id
            var checkDomain = param.mailDomain;
            if(!param.mailDomain || param.mailDomain == ""){
                checkDomain = '^[a-z0-9]+(.*)\.[a-z]{2,3}$';
            }
            checkDomainObj = new RegExp(checkDomain,"gi");
            if(atCount == 0 || atCount == undefined || dotCount == 0 || dotCount == undefined || spaceCount != 0 || (checkDomainObj && !emailValue.match(checkDomainObj))){
                validateRes = false;
            }else if((atCount != 0 && dotCount != 0 && emailValue.match(/[a-z]+/i)) && (atCount > 1 || emailValue.match(/((\.\.)|(@[0-9\-\.]+)+)/) || !emailValue.match(/^[a-z0-9]/i) || emailValue.match(/@[0-9]+/))){
                validateRes = false;
            }
            return validateRes;
        },
        /**
        * Function to return the count of the specific character in a string.
        * @param - {string} - String - In which to check the character count.
        * @param - {character} - regex - regex of the character which needs to be counted.
        */
        charCount: function(string,character){
            var returnCount = 0;
            var re = new RegExp(character,"gi");
            if(re && string.match(re)){
                returnCount = string.match(re).length;
            }
            return returnCount;
        },
        /**
        * Function to check if more than one element has same attribute value.e.g block id "data-id"
        * @param - {checkObject} - Element - To check more than one element has the attribute value specified in this element.
        * @param - {key} - object - Holds the validation rules.
        */
        checkDuplication: function(checkObject,key){
            var returnValue = false;var rootElementToCheck = '';
            if(key.rootElement && key.rootElement != ''){
                rootElementToCheck = key.rootElement;
            }else{
                rootElementToCheck = '#contentContainer';
            }
            if(checkObject && checkObject.nodeType == 1 && key.checkAgainst && key.checkAgainst != ""){
                if(checkObject.hasAttribute(key.checkAgainst) && checkObject.getAttribute(key.checkAgainst)!='' && $(rootElementToCheck).find('*['+key.checkAgainst+'="'+checkObject.getAttribute(key.checkAgainst)+'"]').length > 1){
                    returnValue = true;
                }
            }
            return returnValue;
        },
        /**
        * Function to check if the citation mapped with the existing block. If no element exists with the mapping id of the citation then show error message.
        * @param - {checkObject} - Element - To check the mapping.
        * @param - {key} - object - Holds the validation rules.
        */
        checkCitationMapping: function(checkObject,key){
            var returnValue = true;var rootElementToCheck = '';
            if(key.rootElement && key.rootElement != ''){
                rootElementToCheck = key.rootElement;
            }else{
                rootElementToCheck = '#contentContainer';
            }
            var checkAgainstBlkAttr = key.checkAgainstBlkAttr;
            var checkAgainstCitAttr = key.checkAgainstCitAttr;
            blkAttrPrifix = key.blkAttrPrifix;
            // If no attribute has been given in the rules couldn't validate the element.
            if(!checkAgainstBlkAttr || (checkAgainstBlkAttr && checkAgainstBlkAttr == "") || !checkAgainstCitAttr || (checkAgainstCitAttr && checkAgainstCitAttr == "")){
                returnValue = false;
            }
            else if(checkObject && checkObject.nodeType == 1 && checkAgainstBlkAttr && checkAgainstBlkAttr != "" && checkAgainstCitAttr && checkAgainstCitAttr != "" && checkObject.hasAttribute(checkAgainstCitAttr)){
                var citationValue = checkObject.getAttribute(checkAgainstCitAttr);
                citationValue = citationValue.replace(/(&nbsp;|(\,\s)+)/g,' ');
                citationValue = citationValue.replace(/^\s*|\s*$/g,'');
                if(citationValue.match(/^(\s+|&nbsp;+)$/) || citationValue == ""){
                    returnValue = false;
                }else{
                    var citationValueArray = citationValue.split(' ');
                    $.each(citationValueArray,function(key,value){
                        var checkValue = value;
                        if(blkAttrPrifix!=undefined){
                            checkValue = blkAttrPrifix + checkValue;
                        }
                        if($(rootElementToCheck).find('*['+checkAgainstBlkAttr+'="'+checkValue+'"]').length == 0){
                            returnValue = false;
                        }
                    });
                }
            }
            else if(checkObject && checkObject.nodeType == 1 && checkAgainstBlkAttr && checkAgainstBlkAttr != "" && checkAgainstCitAttr && checkAgainstCitAttr != "" && !checkObject.hasAttribute(checkAgainstCitAttr)){
                returnValue = false;
            }
            return returnValue;
        },
        /**
        * Function to validate the table cells, colspan and rowspan.
        * @param - {checkObject} - Element - To validate.
        * @param - {key} - object - Holds the validation rules.
        */
        validateTable: function(checkObject,key){
            var maxCell = "";
            //var tableNodes = $('#contentDivNode .jrnlTblBlock, #contentDivNode .jrnlInlineTable').find('table');
            if(checkObject && checkObject.nodeType == 1){
                var theadNodes = $(checkObject).find('thead');
                var tbodyNodes = $(checkObject).find('tbody');
                /* Table should not has thead without tbody */
                if(theadNodes.length > 0 && tbodyNodes.length == 0){
                    var valMessage = 'Table has "thead" without "tbody"';
                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,checkObject);
                }
                /* Table should not have empty tbody */
                tbodyNodes.each(function(index,object){
                    if($(this).text() == ""){
                        var valMessage = 'Table should not contain empty "tbody"';
                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,checkObject);
                    }
                });
                
                var tblTrNodes = $(checkObject).find('tr:eq(0)');
                /* Get the cell count from the first row of table. It can be row from thead or tbody */
                if(tblTrNodes.length > 0){
                    tblTrNode = tblTrNodes[0];
                    var tblTdNodes = $(tblTrNode).find('td, th');
                    var colSpanTdNodes = $(tblTrNode).find('td[colspan], th[colspan]');
                    if(tblTdNodes.length > 0){
                        maxCell = tblTdNodes.length;
                        colSpanTdNodes.each(function(index,object){
                            var colspanValue = $(this).attr('colspan');
                            maxCell = parseInt(maxCell + (colspanValue - 1));
                        });
                    }
                }
                var rowSpanNodes = $(checkObject).find('td[rowspan], th[rowspan]');
                if(rowSpanNodes.length > 0){
                    eventHandler.menu.htmlValidation.checkRowSpanExtension(rowSpanNodes);
                }
                var allTblTrNodes = $(checkObject).find('tr:not(:eq(0))');
                allTblTrNodes.each(function(index,object){
                    var trPosition = $(this).index(),valMessage="";
                    var tr = $(this);
                    var tblTdNodes = $(this).find('td, th');
                    if($(this).attr('data-invalid-row')){
                        $(this).removeAttr('data-invalid-row');
                    }
                    /* If the current tr cell count is less than max cell count - check the cells has colspan */
                    if(tblTdNodes.length < maxCell && tblTdNodes.length > 0){
                        var tdExist = tblTdNodes.length;
                        var requiredCell = maxCell - tdExist;
                        var colSpanTdNodes = $(this).find('td[colspan], th[colspan]');
                        /* if there is no colspan - check for rowspan in previous rows of cells */
                        if(colSpanTdNodes.length == 0){
                            var rowSpanNodes = $(this).prevAll('tr').find('td[rowspan], th[rowspan]');
                            if(rowSpanNodes.length > 0){
                                /* IF there is rowspan in previous row cells and its not upto the current cell - then throw error message */
                                var rowCellCount = parseInt(rowSpanNodes.length + tblTdNodes.length);
                                cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                                if(cellToAdjust < requiredCell){
                                    valMessage = ' '+(requiredCell - cellToAdjust)+' cell required for this row ';
                                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                }else if(cellToAdjust > requiredCell){
                                    valMessage = ' This row has '+(cellToAdjust - requiredCell)+' cells excess';
                                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                }
                            }else{/* If no nodes has row span then throw error message */
                                valMessage = ' colspan missing ';
                                eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                            }
                        }else{/* If nodes has colspan - include the colspan count along with the cell count - and then compare with the max cell count */
                            var currTdCount = tblTdNodes.length;
                            colSpanTdNodes.each(function(index,object){
                                var colspanValue = $(this).attr('colspan');
                                currTdCount = parseInt(currTdCount + parseInt(colspanValue - 1));
                            });
                            /* If the cell count after colspan id less than max cell count - check for previous row cells for rowspan */
                            if(currTdCount < maxCell){
                                var remainingCells = maxCell - currTdCount;
                                var rowSpanNodes = $(this).prevAll('tr').find('td[rowspan], th[rowspan]');
                                if(rowSpanNodes.length > 0){
                                    cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                                    /* IF there is rowspan in previous row cells and its not upto the current cell - then throw error message */
                                    if(cellToAdjust < remainingCells){
                                        valMessage = ' '+(remainingCells - cellToAdjust)+' cell required for this row ';
                                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                    }
                                    else if(cellToAdjust > remainingCells){
                                        valMessage = ' This row has '+(cellToAdjust - remainingCells)+' cells excess';
                                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                    }
                                }else{/* If no nodes has row span then throw error message */
                                    valMessage = ' '+remainingCells+' cell required for this row ';
                                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                }
                            }else if(currTdCount > maxCell){
                                var excessCells = currTdCount - maxCell;
                                var rowSpanNodes = $(this).prevAll('tr').find('td[rowspan], th[rowspan]');
                                /* Validate against rowspan */
                                if(rowSpanNodes.length > 0){
                                    cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                                    /* IF there is rowspan in previous row cells and its not upto the current cell - then throw error message */
                                    valMessage = ' This row has '+parseInt(excessCells + cellToAdjust)+' cells excess';
                                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                }else{
                                    valMessage = ' Has '+(currTdCount - maxCell)+' excess cells than first row ';
                                    eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                }
                            }/* if the cells are equal validate against the rowspan */
                            else if(currTdCount == maxCell){
                                var rowSpanNodes = $(this).prevAll('tr').find('td[rowspan], th[rowspan]');
                                if(rowSpanNodes.length > 0){
                                    cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                                    /* IF there is rowspan in previous row cells and its not upto the current cell - then throw error message */
                                    if(cellToAdjust != 0){
                                        valMessage = ' This row has '+cellToAdjust+' cells excess';
                                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                                    }
                                }
                            }
                        }
                    }/* If the cell count is greater than maxcell count - simply throw error message */
                    else if(tblTdNodes.length > maxCell){
                        var noOfExcessCells = tblTdNodes.length - maxCell;
                        valMessage = ' Has '+noOfExcessCells+' excess cells than first row ';
                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                    }else if(tblTdNodes.length == maxCell){
                        var colSpanTdNodes = $(this).find('td[colspan], th[colspan]');
                        var rowSpanNodes = $(this).prevAll('tr').find('td[rowspan], th[rowspan]');
                        if(colSpanTdNodes.length > 0 && rowSpanNodes.length == 0){
                            valMessage = ' No. of cells are equal with cells of first row. But '+colSpanTdNodes.length+' cells of this row has colspan ';
                            eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                        }
                        else if(rowSpanNodes.length > 0 && colSpanTdNodes.length == 0){
                            cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                            if(cellToAdjust != 0){
                                valMessage = ' No. of cells are equal with cells of first row. But '+cellToAdjust+' cells of this row has rowspan extension ';
                                eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                            }
                        }
                        else if(rowSpanNodes.length > 0 && colSpanTdNodes.length > 0){
                            cellToAdjust = eventHandler.menu.htmlValidation.checkRowSpan(rowSpanNodes,trPosition);
                            if(cellToAdjust != 0){
                                valMessage = ' No. of cells are equal with cells of first row. But '+parseInt(cellToAdjust + colSpanTdNodes.length)+' cells of this row has rowspan/colspan ';
                                eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                            }
                        }
                    }else if (tblTdNodes.length == 0){
                        valMessage = ' Empty row exists in this table ';
                        eventHandler.menu.htmlValidation.showValidationMessage(valMessage,this);
                    }
                });
            }
        },
        /**
        * Function to check rowSpan - If the rowspan is greater than the no. of rows from the current row, 	throw validation error
        * @param - {rowSpanNodes} - DOMList - td elements which has rowspan attribute 
        */
        checkRowSpanExtension: function(rowSpanNodes){
            rowSpanNodes.each(function(index,object){
                var rowSpanValue = $(this).attr('rowspan');
                var followingTrNodes = $(this).closest('tr').nextAll('tr');
                var followingTrLength = followingTrNodes.length;
                if(followingTrLength < (rowSpanValue - 1)){
                    var parentNodeName = $(this).closest('tr').parent().prop('nodeName');
                    valMessage = "In '" + parentNodeName.replace(/^t/i,'').toLowerCase() + "' given rowspan is greater than the actual no. of rows available";
                }
            });
        },
        /**
        * Function to check the count of rowSpan - that is the rowspan extended upto the current row 
        * @param - {rowSpanNodes} - DOMElementList - cells with row span
        * @param - {trPosition} - String - Position od the current tr to check the row span extention
        */
        checkRowSpan: function(rowSpanNodes,trPosition){
            var cellToAdjust = 0;
            rowSpanNodes.each(function(index,object){
                var rowSpanValue = $(this).attr('rowspan');
                var parentTrIndex = $(this).closest('tr').index();
                var rowSpanTRIndex = parseInt(parentTrIndex + (rowSpanValue - 1));
                if(trPosition <= rowSpanTRIndex){
                    cellToAdjust++;
                    if($(this).attr('colspan')){
                        var rowColSpanValue = parseInt($(this).attr('colspan') - 1);
                        cellToAdjust = parseInt(cellToAdjust + rowColSpanValue);
                    }
                }
            });
            return cellToAdjust;
        },
        /**
        * Function to read the rules object and return the message to display
        * @param - {mandatoryElement} - boolean - Based on this value pick the validation message.
        * @param - {ruleObject} - object - Holds validation rules.
        */
        getValidationMesg: function(mandatoryElement,ruleObject){
            var returnValMessage = '';
            if(ruleObject.action && typeof ruleObject.action == 'object'){
                ruleObject.action.forEach(function(key, value){
                    if(key[mandatoryElement]){
                        returnValMessage = key[mandatoryElement];
                        if(typeof returnValMessage == 'object'){
                            returnValMessage.forEach(function(key,value){
                                if(key.functions){
                                    returnValMessage = eventHandler.menu.htmlValidation.processValFunctions(key.functions);
                                }
                                if(key.message){
                                    returnValMessage = key.message;
                                }
                            });
                        }
                    }
                });
            }else if(ruleObject.action && ruleObject.action!= '' && (typeof ruleObject.action == 'string')){
                returnValMessage = ruleObject.action;
            }
            return returnValMessage;
        },
        /**
        * Function to return the regex object
        * @param - {matchValue} - regex - regex text to create the regex object.
        */
        getRegexObject: function(matchValue){
            var regex = '';
            if(matchValue && matchValue!=''){
                var valueToCheck = matchValue.replace(/:regex\((.*)\)/,'$1');
                regexFlags 		= 'g',
                regex 			= new RegExp(valueToCheck.replace(/^s+|s+$/g,''), regexFlags);
            }
            return regex;
        },
        /**
        * Function to display the validation message
        * @param - {validationMessage} - String - Message that needs to be displayed in the editor.
        * @param - {checkObject} - Element - Which to be mapped on click the validation message.
        */
        showValidationMessage: function(validationMessage,checkObject){
            if(validationMessage && validationMessage != ''){
                var errorId = uuid.v4();
                var nodeContent = '';
                if(checkObject && (checkObject.nodeValue != "" && checkObject.nodeValue != null) || (checkObject.innerHTML != "" && checkObject.innerHTML != null)){
                    nodeContent = checkObject.nodeValue;
                    if(!nodeContent){
                        nodeContent = $(checkObject).text();
                    }
                    nodeContent = (nodeContent.length >= 15)?nodeContent.substring(0, 15)+'...':nodeContent;
                }else if(checkObject && (checkObject.nodeValue == "" || checkObject.nodeValue == null) && (checkObject.innerHTML != "" && checkObject.innerHTML != null) && checkObject.hasAttribute('class')){
                    var nodeClass = checkObject.getAttribute('class');
                    nodeContent = 'Element "' + nodeClass + '"';
                }else {
                    nodeContent = 'Element name "' + checkObject.nodeName + '"';
                }
                var validationHTML = '<tr class="jrnlValMesg" data-validation-id="'+errorId+'"><td>'+nodeContent+'</td><td>'+validationMessage+'</td></tr>';
                if($('#navContainer #validationDivNode').find('table tbody').length > 0){
                    $('#navContainer #validationDivNode').find('table tbody').append(validationHTML);
                }
                if(checkObject && checkObject.nodeType==1){
                    if(checkObject.hasAttribute('data-validation-rid') && checkObject.getAttribute('data-validation-rid').replace(/\s*|&nbsp;*/g,'') != ''){
                        checkObject.setAttribute('data-validation-rid',checkObject.getAttribute('data-validation-rid') + ' ' +errorId);
                    }else{
                        checkObject.setAttribute('data-validation-rid',errorId);
                    }
                }
                //console.log(validationMessage);
            }
        }
    };
	return eventHandler;
})(eventHandler || {});

/**
* Function to use the regex in jq selector
* site link - http://james.padolsey.com/javascript/regex-selector-for-jquery/
* Modify by jagan on 10-09-2016
* Usage $(".jrnlHead1:regex(css:font-size,^[0-9]+)") or $(".jrnlHead1:regex(text,^[0-9]+)")
*/
jQuery.expr[':'].regex 	= function(elem, index, match) {
    var matchParams 	= match[3].split(', '),
        validLabels 	= /^(text|data:|css:)/,
        attr 			= {
				            method: matchParams[0].match(validLabels) ? 
				                        matchParams[0].split(':')[0] : 'attr',
				            property: matchParams.shift().replace(validLabels,'')
        				},
        regexFlags 		= 'g',
        regex 			= new RegExp(matchParams.join('').replace(/^s+|s+$/g,''), regexFlags);
        if(attr.property && attr.property != ''){
			return  regex.test(jQuery(elem)[attr.method](attr.property))
        }else{
        	return	regex.test(jQuery(elem)[attr.method]());		
        }
    	
}

jQuery.expr[':'].textregex = function(elem, index, match) {
    
        regexFlags 	= 'ig',
        regex 		= new RegExp(match[3], regexFlags);
    	return 		regex.test(jQuery(elem).text());
}