﻿var config = {
    "bookmarks":4,
    "pubIdentifier":"10.1136", 
    "defaultUnits":"pt",
    "baseLeading":11.5,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "landscapeFloatOnFirstPage":false,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY,KEY_BACK",
    "minStackSpace":18,
    "placeStyle":"Sandwich",
    "figCaptionMinLines": 0,
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.15,
    "minNoLinesOnPag":3,
    "applyTableBorderColor":'WHITE',
    "pageSize":{
        "width":595.276,
        "height":793.701
    },
    "placeComBoxLib":{
        "jrnlAbsBoxedText":1818584692
        },
    "exportPDFpreset":{
        "print":{
            "crop_marks":true, 
            "bleed_marks":false, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":8.504
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "watermark":{
        "Pre-editing":true, 
        "Typesetter QA":true, 
        "Author proof":true, 
        "Author revision":true, 
        "Revises":true
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":14.852,
      "right":14.852,
      "gutter":14.852
    },
    "geoBoundsVerso":[72,34,757.416999999999,561.276
    ],
    "geoBoundsRecto":[72,629.275181102362,757.416999999999,1156.55118110236
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1",
    "RESEARCH":"LAYOUT1",
    "RHMSADRG":"LAYOUT1",
    "EDITORIALS":"LAYOUT2",
    "SEOFTEATRW":"LAYOUT3"
    },
    "pageColumnDetails":{
        "LAYOUT1":{
                "openerPageColumnDetails":[
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                "columnDetails":[
                  {"width":107.736, "height":684.701,"floatsCited":false,"gutter":0, "imaginary":true},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                 "openerPageMargin":{
                    "top":72,
                    "bottom":37,
                    "inside":141.732,
                    "outside":34
                },
            "otherPageMargin":{
                "top":72,
                "bottom":37,
                "inside":141.732,
                "outside":34
                }
            },
        "LAYOUT2":{        
                "openerPageColumnDetails":[
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":12},
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                "columnDetails":[
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":12},
                  {"width":167.759, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                 "openerPageMargin":{
                    "top":72,
                    "bottom":37,
                    "inside":34,
                    "outside":34  
                },
            "otherPageMargin":{
                "top":72,
                "bottom":37,
                "inside":34,
                "outside":34  
                }
            },
        "LAYOUT3":{        
                "openerPageColumnDetails":[
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                "columnDetails":[
                  {"width":107.736, "height":684.701,"floatsCited":false,"gutter":0, "imaginary":true},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":0},
                  {"width":203.764, "height":684.701,"floatsCited":false,"gutter":12}
                ],
                 "openerPageMargin":{
                    "top":72,
                    "bottom":37,
                    "inside":141.732,
                    "outside":34
                },
            "otherPageMargin":{
                "top":72,
                "bottom":37,
                "inside":141.732,
                "outside":34  
                }
            }
    },
  "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":342.8, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
  },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":80
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsPara,jrnlAbsHead,jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,jrnlHead5"
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["COMMENTARY"
    ],
    
    "docFontsList":[
       {
          "Meta Serif Pro Book": {
            "Regular": {
                "Symbol": "Medium"
              },
            "Italic": {
                "Symbol": "Italic"
              },
            "Bold": {
                "Symbol": "Bold"
              }
          }
      },
        {
          "MetaHeadlineOT (OTF)": {
            "Regular": {
                "ITC Zapf Dingbats": "Medium"
            }
          }
      },
        {
          "MetaHeadlineOT": {
            "Regular": {
                "ITC Zapf Dingbats": "Medium"
              }
          }
        }
    ],
    
   "replFonts":[
            {"fontFamily":"Meta Serif Pro Book", "fontStyle":"Regular"},
            {"fontFamily":"MetaHeadlineOT", "fontStyle":"Regular"}
    ],    
    
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubDOI']", "frame-name":"STUB_DOI", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCRRH']", "frame-name":"CRRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCLRH']", "frame-name":"CLRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRH']", "frame-name":"RRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRH']", "frame-name":"LRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlMetaInfo']", "frame-name":"METAINFO", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlRelatedInfo']", "frame-name":"RELATED_ARTICLE_INFO", "action":"move", "styleOverride":null}
       ]       
     },
     "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"91,53,0,0"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,5"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,112,173"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"241,242,242"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "RESEARCH":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"91,53,0,0"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,5"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,112,173"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"241,242,242"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "EDITORIALS":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,96,90,2"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,5"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"225,31,33"
                    },
                 "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"241,242,242"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "SEOFTEATRW":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"80,0,30,10"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,5"
                    },
                "COLOR4":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"8,0,3,1"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,167,173"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"241,242,242"
                    },
                "COLOR4":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"228,241,241"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "RHMSADRG":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"91,53,0,0"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,5"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,112,173"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    },
                "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"241,242,242"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            }
        },
    "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "7px",
                    "font-family" : "'MetaProLight', MetaProLight"
                },
                "fontPath"  : "MetaProLight/MetaProLight.otf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'MetaProMedium',MetaProMedium",
                    "font-size" : "7px",
                    "line-height" : "8.5px",
                    "padding" : "4px 0px 4px 4px",
                },
                "fontPath"  : "MetaProLight/MetaProMedium.otf",
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'MetaProLight',MetaProLight",
                    "font-size" : "7.25px",
                    "line-height" : "8.5px",
                    "padding" : "4px 0px 4px 4px",
                },
                "fontPath"  : "MetaProLight/MetaProLight.otf",
                "align" : "left",
                "valign": "top"
            }
        },
     "equation" : {
          "default" : {
              "page":{
                  "size" : "419.528pt"
              },
              "font" : {
                  "size"    : "9pt",
                  "ledding" : "11.5pt",
                  "path"    : "/MetaSerifProBook/",
                     "ext"     : "otf",
                  "bold"    : "MetaSerifProBook-Bold",
                  "italic"  : "MetaSerifProBook-Italic",
                  "bold_italic":"MetaSerifProBook-BoldItalic",
                  "main"    : "MetaSerifProBook"
              }
           },
           "jrnlTblBlock" : {
              "page":{
                  "size" : "419.528pt"
              },
              "font" : {
                "size"    : "7.25pt",
                "ledding" : "8.5pt",
                "path"    : "/MetaProLight/",
                "ext"     : "otf",
                  "bold"    : "MetaProMedium",
                  "italic"  : "MetaProLight-Italic",
                  "main":"MetaProLight",
              }
           }
        },
    "stubColObj":{
        "top": {
            "STUB_DOI": true,
            "STUB_COLUMN": true,
            "CROSSMARK": true,
            "STUB_STMT": false
        },
        "bottom": {
            "METAINFO": false,
            "RELATED_ARTICLE_INFO": false
        }
    }
}
this.config = config;