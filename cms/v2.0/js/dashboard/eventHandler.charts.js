/**
 * chart - This file is to display all charts in dashboard Kriya2.0 based on highcharts.com configuration.
 * Functions:
 *     renderChart() - Create chart based on article data and configuration.
 *     makeCharts() - To get required data from elasticNode-proxy using getarticlelist api and send responce to the particular function.
 *     getChartData() - To build the data from responce according to chart configuration.
 *     durationCalculation() - To calculate the mean, median, mode, range for chart data.
 */
var doiList = {};
var clickEvent;
var chartDataTypeName;
var reportArticles;
(function (eventHandler) {
    eventHandler.highChart = {
        /**
         *  renderChart() - Create chart based on article data and configuration.
         *  @param param - Chart configuration for particular chart with out data.
         */
        renderChart: function (param) {
            Highcharts[param.modalType](param.chartId, param.options);
            $('.la-container').css('display', 'none');
        },
        /**
         *  makeCharts() - To get required data from elasticNode-proxy using getarticlelist api and send responce to the particular function.
         *  @param param - Journal name based on filter.
         */
        makeCharts: function (param, rePopulate) {
            $('.la-container').css('display', '');
            if (param && param.project && param.project != undefined && param.project != '') {
                param.project = param.project.join(' OR ');
            } else {
                if (param.customer && projectLists) {
                    var project = '';
                    param.customer.forEach(function (customerName) {
                        if(projectLists[customerName] && projectLists[customerName].length > 0){
                            project += projectLists[customerName].join(' OR ');
                        }
                    })
                    param.project = project;
                }
            }
            var defaultCharts = JSON.parse(JSON.stringify(eventHandler.highChart.defaultCharts));
            if (rePopulate) {
                defaultCharts = [];
                defaultCharts[0] = param;
            }
            doiList = {};
            /**
             * To get the configuration of chart from defaultCharts array.
             */
            defaultCharts.forEach(function (index, keyIndex) {
                var chartConfig = index;
                var output = [];
                var count = 0;
                if (chartConfig.api) {
                    var result = [];
                    chartConfig.api.forEach(function (apiConfig, apiIndex) {
                        if (apiConfig.data) {
                            for (parameter in apiConfig.data) {
                                if (param[parameter]) {
                                    apiConfig.data[parameter] = param[parameter];
                                } else if (param['project'] && parameter == "regex") {
                                    apiConfig.data['project'] = param['project'].replace(/ OR /g, '|');
                                }
                            }
                        }
                        /**
                         * To retrieve data for journals.
                         */
                        $.ajax({
                            type: "POST",
                            url: '/api/getarticlelist',
                            data: apiConfig.data,
                            dataType: 'json',
                            //async: false,
                            success: function (respData) {
                                if (!respData || respData.length == 0) {
                                    return;
                                }
                                var input = respData;
                                var array = [];
                                if (chartConfig.chartInfo && chartConfig.chartInfo.functionName != "") {
                                    if (typeof (chartConfig.chartInfo.functionName) == "string" && typeof (eval(chartConfig.chartInfo.functionName)) == "function") {
                                        eval(chartConfig.chartInfo.functionName)(chartConfig, respData.body.aggregations.jobStatus, param.project)
                                    } else if (typeof (chartConfig.chartInfo.functionName) == "object") {
                                        chartConfig.chartInfo.functionName.forEach(function (index, key) {
                                            if (index && index.input) {
                                                if (index.sendAsArray) {
                                                    chartDataTypeName = '';
                                                    input = JSONPath({
                                                        json: respData,
                                                        path: index.input
                                                    });
                                                } else {
                                                    if (index.merge) {
                                                        input = [];
                                                        index.merge.forEach(function (jsonpath, mergeIndex) {
                                                            if (jsonpath.append && jsonpath.append == "first") {
                                                                var docCount = JSONPath({
                                                                    json: respData,
                                                                    path: jsonpath.docCount
                                                                })[0];
                                                                var data = JSONPath({
                                                                    json: respData,
                                                                    path: jsonpath.valPath
                                                                })[0];
                                                                var push = {
                                                                    "key": jsonpath.appendAs,
                                                                    "doc_count": docCount,
                                                                    "project": data
                                                                };
                                                                array[jsonpath.appendTo].unshift(push);
                                                            } else {
                                                                array[jsonpath.appendTo] = JSONPath({
                                                                    json: respData,
                                                                    path: jsonpath.valPath
                                                                })[0];
                                                            }
                                                        })
                                                        input.push(array);

                                                    } else {
                                                        input = JSONPath({
                                                            json: respData,
                                                            path: index.input
                                                        })[0];
                                                        if (input && input[0] == undefined) {
                                                            chartDataTypeName = Object.keys(input);
                                                            input = Object.keys(input).map(e => input[e])
                                                            input.forEach(function (name, i) {
                                                                name["key"] = chartDataTypeName[i];
                                                            })
                                                        } else if (index.maindata && index.maindata.defaultReplaceName && index.maindata.defaultReplaceValue) {
                                                            chartDataTypeName = index.maindata.defaultReplaceValue;
                                                        }

                                                    }
                                                }
                                            }
                                            // result.push(input);
                                            result.splice( apiIndex, 0, input );
                                            if (chartConfig.api.length == 1) {
                                                if (typeof (eval(index.funcName)) == "function") {
                                                    count = 0;
                                                    eval(index.funcName)(index, chartConfig, input, param.project, param.customer, param.project)
                                                }
                                            } else if (count == chartConfig.api.length - 1) {
                                                if (typeof (eval(index.funcName)) == "function") {
                                                    count = 0;
                                                    eval(index.funcName)(index, chartConfig, result, param.project, param.customer, param.project)
                                                }
                                            }
                                            count++;
                                        })
                                    }
                                }
                            },
                            error: function (respData) {
                                $('.la-container').css('display', 'none');
                                console.log(respData);
                            }
                        });
                    });

                }
                if (eventHandler.highChart.defaultCharts.length >= keyIndex + 1) {
                    $('.la-container').css('display', 'none');
                }
            })

        },
        /**
         *  getChartData() - To build the data from responce according to chart configuration.
         *  @param chartConfig - Chart configuration for particular chart.
         *  @param input - Required data for to build data for charts.
         *  @param labels - Filtered journal names.
         */
        getChartData: function (chartConfig, particularChartConfig, input, labels, customers, project) {
            doiList[chartConfig.chartData.chartId] = {};
            var seriesData = [];
            var config = {},
                chartData = {},
                category = [],
                date = [];
            var chartMapping = {
                "drilldown": 0,
                "heatmap": 1,
                "stock": 2,
                "heatmapRemovedDate": 3,
            };
            var drillOverData = [],
                calculationArray = [],
                xAxisLabel = [],
                yAxisLabel = [];
            var seriesName;
            var pageArrayCount = [];
            if (chartConfig) config = chartConfig;
            if (chartConfig && chartConfig.chartData) chartData = JSON.stringify(chartConfig.chartData);
            if (input && input != undefined) {
                if (config.chartType && chartMapping[config.chartType] == 0) {
                    if (config.order && config.order.array && config.order.stageVariable) {
                        var respInput = input;
                        var input = [];
                        config.order.array.forEach(function (stageOrder) {
                            if (respInput.find(o => o[config.order.stageVariable] === stageOrder) != undefined)
                                input.push(respInput.find(o => o[config.order.stageVariable] === stageOrder))
                        })
                    }
                    input.forEach(function (inputValue, inputIndex) {
                        if (config.maindata && config.maindata.path) {
                            seriesData = [];
                            if (config.maindata.order && config.maindata.order.array && config.maindata.order.stageVariable) {
                                var oldStage = JSONPath({
                                    json: inputValue,
                                    path: config.maindata.path
                                })[0];
                                var inputValue = [];
                                config.maindata.order.array.forEach(function (stageOrder) {
                                    if (oldStage.find(o => o[config.maindata.order.stageVariable] === stageOrder) != undefined)
                                        inputValue.push(oldStage.find(o => o[config.maindata.order.stageVariable] === stageOrder))
                                })
                                //inputValue.push(newStage);
                            } else {
                                inputValue = JSONPath({
                                    json: inputValue,
                                    path: config.maindata.path
                                })[0];
                            }
                            if (chartDataTypeName) {
                                doiList[chartConfig.chartData.chartId][chartDataTypeName[inputIndex]] = {};
                            }
                            // if (JSONPath({ json: inputValue, path: config.maindata.path }) && JSONPath({ json: inputValue, path: config.maindata.path }).length > 0) {
                            if (inputValue) {
                                pageArrayCount = [];
                                inputValue.forEach(function (keyValue, keyIndex) {
                                    /**
                                     * For main series data
                                     */
                                    if (config.maindata.seriesDataFormat) {
                                        var dataFormat = {};
                                        Object.keys(config.maindata.seriesDataFormat).forEach(function (sData) {
                                            if (config.maindata.seriesDataFormat[sData].match(/^.{0}[#]/)) {
                                                var addName = (/#(.*)(\$.*)/gm.exec(config.maindata.seriesDataFormat[sData]));
                                                dataFormat[sData] = config.maindata[addName[1]][inputIndex] + JSONPath({
                                                    json: keyValue,
                                                    path: addName[2]
                                                })[0];
                                            } else if (config.maindata.seriesDataFormat[sData].match(/\$../)) {
                                                dataFormat[sData] = JSONPath({
                                                    json: keyValue,
                                                    path: config.maindata.seriesDataFormat[sData]
                                                })[0];
                                                if (sData == 'name') {
                                                    seriesName = JSONPath({
                                                        json: keyValue,
                                                        path: config.maindata.seriesDataFormat[sData]
                                                    })[0];
                                                    if (chartDataTypeName) {
                                                        doiList[chartConfig.chartData.chartId][chartDataTypeName[inputIndex]][seriesName] = {};
                                                    } else {
                                                        doiList[chartConfig.chartData.chartId][seriesName] = {};
                                                    }
                                                }
                                            } else {
                                                dataFormat[sData] = config.maindata.seriesDataFormat[sData];
                                            }
                                        })
                                        /**
                                         * For additional charts in existing chart
                                         */
                                        if (config.maindata.multiplegraph && config.maindata.multiplegraph.path && config.maindata.multiplegraph.replaceWith) {
                                            var pageCount = JSONPath({
                                                json: keyValue,
                                                path: config.maindata.multiplegraph.path[inputIndex]
                                            })[0];
                                            var count = 0;
                                            if (pageCount.length != 0) {
                                                Object.keys(pageCount).forEach(function (a) {
                                                    count += (Math.ceil(Number(pageCount[a].key)) * pageCount[a]['doc_count']);
                                                    // count += Number(pageCount[a].key);
                                                })
                                            }
                                            var data = {
                                                'name': seriesName,
                                                'y': count
                                            }
                                            pageArrayCount.push(data);
                                        }
                                        /**
                                         * For drilldown data
                                         */
                                        if (config.drilldowndata && config.drilldowndata.path) {
                                            var drillData = {};
                                            if (config.drilldowndata.order && config.drilldowndata.order.array && config.drilldowndata.order.stageVariable) {
                                                var oldStageDrill = JSONPath({
                                                    json: keyValue,
                                                    path: config.drilldowndata.path
                                                })[0];
                                                var inputValueDrill = [];
                                                config.drilldowndata.order.array.forEach(function (stageOrderDrill) {
                                                    if (oldStageDrill && oldStageDrill.find(o => o[config.drilldowndata.order.stageVariable] === stageOrderDrill) != undefined)
                                                        inputValueDrill.push(oldStageDrill.find(o => o[config.drilldowndata.order.stageVariable] === stageOrderDrill))
                                                })
                                            } else {
                                                inputValueDrill = JSONPath({
                                                    json: keyValue,
                                                    path: config.drilldowndata.path
                                                })[0];
                                            }
                                            var seriesCount = 0;
                                            inputValueDrill.forEach(function (a) {
                                                seriesCount += Number(JSONPath({ json: a, path: '$.doc_count' }))
                                            })
                                            dataFormat['y'] = seriesCount;
                                            //if (JSONPath({ json: keyValue, path: config.drilldowndata.path }) && JSONPath({ json: keyValue, path: config.drilldowndata.path }).length > 0) {
                                            if (inputValueDrill) {
                                                inputValueDrill.forEach(function (dValue, dIndex) {
                                                    Object.keys(config.drilldowndata.drilldownDataFormat).forEach(function (dData) {
                                                        if (typeof (config.drilldowndata.drilldownDataFormat[dData]) == 'string') {
                                                            if (config.drilldowndata.drilldownDataFormat[dData].match(/\$\$../)) {
                                                                config.drilldowndata.drilldownDataFormat[dData] = config.drilldowndata.drilldownDataFormat[dData].replace('\$\$', '$');
                                                                drillData[dData] = JSONPath({
                                                                    json: dValue,
                                                                    path: config.drilldowndata.drilldownDataFormat[dData]
                                                                })[0];
                                                            } else if (config.drilldowndata.drilldownDataFormat[dData].match(/^.{0}[#]/)) {
                                                                var addUniqueName = (/#(.*)(\$.*)/gm.exec(config.drilldowndata.drilldownDataFormat[dData]));
                                                                drillData[dData] = config.maindata[addUniqueName[1]][inputIndex] + JSONPath({
                                                                    json: keyValue,
                                                                    path: addUniqueName[2]
                                                                })[0];
                                                            } else if (config.drilldowndata.drilldownDataFormat[dData].match(/^.{0}[y]/)) {
                                                                var addUniqueName = (/y(.*)(\$.*)/gm.exec(config.drilldowndata.drilldownDataFormat[dData]));
                                                                drillData[dData] = JSONPath({
                                                                    json: input[inputIndex],
                                                                    path: addUniqueName[2]
                                                                })[0];
                                                            } else if (config.drilldowndata.drilldownDataFormat[dData].match(/\$../)) {
                                                                drillData[dData] = JSONPath({
                                                                    json: keyValue,
                                                                    path: config.drilldowndata.drilldownDataFormat[dData]
                                                                })[0];
                                                            } else {
                                                                drillData[dData] = config.drilldowndata.drilldownDataFormat[dData];
                                                            }
                                                        } else if (typeof (config.drilldowndata.drilldownDataFormat[dData]) == 'object' && Array.isArray(config.drilldowndata.drilldownDataFormat[dData])) {
                                                            if (!drillData[dData] || drillData[dData] == undefined) {
                                                                drillData[dData] = [];
                                                            }
                                                            var tempArr = [];
                                                            config.drilldowndata.drilldownDataFormat[dData].forEach(function (subData, subIndex) {
                                                                if (typeof (subData) == 'string') {
                                                                    if (subData.match(/\$\$../)) {
                                                                        subData = subData.replace('\$\$', '$');
                                                                        tempArr.push(JSONPath({
                                                                            json: dValue,
                                                                            path: subData
                                                                        })[0]);
                                                                        if (subData == '$..key') {
                                                                            var drillName = JSONPath({ json: dValue, path: subData })[0];
                                                                            if (chartDataTypeName) {
                                                                                doiList[chartConfig.chartData.chartId][chartDataTypeName[inputIndex]][seriesName][drillName] = {};
                                                                                doiList[chartConfig.chartData.chartId][chartDataTypeName[inputIndex]][seriesName][drillName]['doi'] = [];
                                                                            } else {
                                                                                doiList[chartConfig.chartData.chartId][seriesName][drillName] = {};
                                                                                doiList[chartConfig.chartData.chartId][seriesName][drillName]['doi'] = [];
                                                                            }
                                                                            JSONPath({ json: dValue, path: "$..doi..buckets" })[0].forEach(function (doi) {
                                                                                if (chartDataTypeName) {
                                                                                    doiList[chartConfig.chartData.chartId][chartDataTypeName[inputIndex]][seriesName][drillName]['doi'].push(JSONPath({ json: doi, path: "$.key" })[0])
                                                                                } else {
                                                                                    doiList[chartConfig.chartData.chartId][seriesName][drillName]['doi'].push(JSONPath({ json: doi, path: "$.key" })[0])
                                                                                }

                                                                            })
                                                                        }
                                                                    } else if (subData.match(/^.{0}[#]/)) {
                                                                        var addUniqueName = (/#(.*)(\$.*)/gm.exec(subData));
                                                                        tempArr.push(config.maindata[addUniqueName[1]][inputIndex] + JSONPath({
                                                                            json: keyValue,
                                                                            path: addUniqueName[2]
                                                                        })[0]);
                                                                    } else if (subData.match(/\$../)) {
                                                                        tempArr.push(JSONPath({
                                                                            json: keyValue,
                                                                            path: subData
                                                                        })[0]);
                                                                    } else {
                                                                        tempArr.push(subData);
                                                                    }
                                                                }
                                                            });
                                                            drillData[dData].push(tempArr);
                                                        } else if (typeof (config.drilldowndata.drilldownDataFormat[dData]) == 'object') {
                                                            drillData[dData] = {};
                                                            Object.keys(config.drilldowndata.drilldownDataFormat[dData]).forEach(function (subData) {
                                                                if (typeof (config.drilldowndata.drilldownDataFormat[dData][subData]) == 'string') {
                                                                    if (config.drilldowndata.drilldownDataFormat[dData][subData].match(/\$\$../)) {
                                                                        config.drilldowndata.drilldownDataFormat[dData][subData] = config.drilldowndata.drilldownDataFormat[dData][subData].replace('\$\$', '$');
                                                                        drillData[dData][subData] = JSONPath({
                                                                            json: dValue,
                                                                            path: config.drilldowndata.drilldownDataFormat[dData][subData]
                                                                        })[0];
                                                                    } else if (config.drilldowndata.drilldownDataFormat[dData][subData].match(/^.{0}[#]/)) {
                                                                        var addUniqueName = (/#(.*)(\$.*)/gm.exec(config.drilldowndata.drilldownDataFormat[dData][subData]));
                                                                        drillData[dData][subData] = config.maindata[addUniqueName[1]][inputIndex] + JSONPath({
                                                                            json: keyValue,
                                                                            path: addUniqueName[2]
                                                                        })[0];
                                                                    } else if (config.drilldowndata.drilldownDataFormat[dData][subData].match(/\$../)) {
                                                                        drillData[dData][subData] = JSONPath({
                                                                            json: keyValue,
                                                                            path: config.drilldowndata.drilldownDataFormat[dData][subData]
                                                                        })[0];
                                                                    } else {
                                                                        drillData[dData][subData] = config.drilldowndata.drilldownDataFormat[dData][subData];
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    })
                                                })
                                            }
                                            drillOverData.push(drillData);
                                        }
                                        seriesData.push(dataFormat);
                                    }

                                })
                                if (config.maindata.multiplegraph && config.maindata.multiplegraph.replaceWith && pageArrayCount) {
                                    chartData = chartData.replace('"' + config.maindata.multiplegraph.replaceWith[inputIndex] + '"', JSON.stringify(pageArrayCount))
                                }
                            }
                            /**
                             * Assign Zero value to the labels
                             */
                            if (config.maindata.labelsMust) {
                                dataFormat = {};
                                if (labels) {
                                    labels = labels.split(' OR ');
                                    var names = seriesData.map(function (item) {
                                        return item['name'];
                                    });
                                    labels.forEach(function (labelName) {
                                        var arrayIndex = names.indexOf(labelName);
                                        if (arrayIndex < 0) {
                                            var replaceValue = JSON.stringify(config.maindata.labelsMust);
                                            var replace = replaceValue.replace("{" + config.maindata.conditionToCheck + "}", labelName);
                                            seriesData.push(JSON.parse(replace));
                                        }
                                    });
                                }
                            }
                            seriesData = JSON.stringify(seriesData);
                            chartData = chartData.replace('"' + config.maindata.replaceWith[inputIndex] + '"', seriesData);
                        }
                        /**
                         *  To call the other function based on configuration.
                         */
                        else if (config.callFunction && config.callFunction.funcName && config.callFunction.variableToPass) {
                            var returnData = eval(config.callFunction.funcName)(inputValue, config);
                            if (returnData != null && returnData != undefined && returnData) {
                                Object.keys(returnData).forEach(function (calculatedData, count) {
                                    dataFormat = {};
                                    Object.keys(config.main.seriesDataFormat).forEach(function (sData) {
                                        if (config.main.seriesDataFormat[sData].match(/\$../)) {
                                            dataFormat[sData] = JSONPath({
                                                json: inputValue,
                                                path: config.main.seriesDataFormat[sData]
                                            })[0];
                                        } else {
                                            dataFormat[sData] = returnData[calculatedData];
                                        }
                                    });
                                    if (calculationArray[calculatedData] == undefined) {
                                        calculationArray[calculatedData] = JSON.stringify(dataFormat);
                                    } else {
                                        calculationArray[calculatedData] += ',' + JSON.stringify(dataFormat);
                                    }
                                });
                            }
                        }

                    });
                }
                /**
                 * To make chart based on heatmap configuration.
                 */
                else if (config.chartType && chartMapping[config.chartType] == 1) {
                    var xAxisPoint = 0,
                        yAxisPoint = 0,
                        xAxisLabel = [],
                        yAxisLabel = [],
                        heatMapData = [],
                        drillOverData = [],
                        inputData = [];
                    diffName = '';
                    if (config.maindata.order && config.maindata.order.array && config.maindata.order.stageVariable) {
                        config.maindata.order.array.forEach(function (stageOrder) {
                            if (input.find(o => o[config.maindata.order.stageVariable] === stageOrder) != undefined)
                                inputData.push(input.find(o => o[config.maindata.order.stageVariable] === stageOrder))
                        })
                    } else {
                        inputData = input;
                    }
                    inputData.forEach(function (inputValue, inputIndex) {
                        diffName = JSONPath({ json: inputValue, path: '$..key' })[0];
                        doiList[chartConfig.chartData.chartId][diffName] = {};
                        if (config.maindata && config.maindata.path) {
                            yAxisPoint = 0;
                            var yInnerValue = 0;
                            if (JSONPath({
                                json: inputValue,
                                path: config.maindata.path
                            }) && JSONPath({
                                json: inputValue,
                                path: config.maindata.path
                            }).length > 0) {
                                JSONPath({
                                    json: inputValue,
                                    path: config.maindata.path
                                })[0].forEach(function (keyValue, keyIndex) {

                                    if (config.maindata.seriesDataFormat) {
                                        var dataFormat = {};
                                        Object.keys(config.maindata.seriesDataFormat).forEach(function (sData) {
                                            if (config.maindata.seriesDataFormat[sData].match(/^.{0}[$]/)) {
                                                dataFormat[sData] = JSONPath({
                                                    json: inputValue,
                                                    path: config.maindata.seriesDataFormat[sData]
                                                })[0];
                                                if (sData == 'name') {
                                                    seriesName = JSONPath({
                                                        json: keyValue,
                                                        path: config.maindata.seriesDataFormat[sData]
                                                    })[0].replace('_', '');
                                                    doiList[chartConfig.chartData.chartId][diffName][seriesName] = {};
                                                }
                                            } else if (config.maindata.seriesDataFormat[sData].match(/^.{0}[v]/)) {
                                                var addUniqueName = (/v(.*)(\$.*)/gm.exec(config.maindata.seriesDataFormat[sData]));
                                                dataFormat[sData] = JSONPath({
                                                    json: keyValue,
                                                    path: addUniqueName[2]
                                                })[0];
                                            } else if (config.maindata.seriesDataFormat[sData].match(/^.{0}[#]/)) {
                                                var addUniqueName = (/#(.*)(\$.*)/gm.exec(config.maindata.seriesDataFormat[sData]));
                                                dataFormat[sData] = JSONPath({
                                                    json: inputValue,
                                                    path: config.XaxisLabelPath
                                                })[0] + JSONPath({
                                                    json: keyValue,
                                                    path: addUniqueName[2]
                                                })[0];
                                            } else if (config.maindata.seriesDataFormat[sData].match(/^.{0}[x]/)) {
                                                dataFormat[sData] = xAxisPoint;
                                            } else if (config.maindata.seriesDataFormat[sData].match(/^.{0}[y]/)) {
                                                dataFormat[sData] = yAxisPoint;
                                            }
                                        })
                                        if (config.drilldowndata && config.drilldowndata.path) {
                                            var drillData = {},
                                                innerDrillData = [];
                                            Object.keys(config.drilldowndata.drilldownDataFormat).forEach(function (dData) {
                                                if (config.drilldowndata.drilldownDataFormat[dData].match(/^.{0}[#]/)) {
                                                    var addUniqueName = (/#(.*)(\$.*)/gm.exec(config.drilldowndata.drilldownDataFormat[dData]));
                                                    drillData[dData] = JSONPath({
                                                        json: inputValue,
                                                        path: config.XaxisLabelPath
                                                    })[0] + JSONPath({
                                                        json: keyValue,
                                                        path: addUniqueName[2]
                                                    })[0];
                                                } else if (config.drilldowndata.drilldownDataFormat[dData].match(/^.{0}[$]/)) {
                                                    drillData[dData] = JSONPath({
                                                        json: inputValue,
                                                        path: config.drilldowndata.drilldownDataFormat[dData]
                                                    })[0];
                                                } else {
                                                    var xInnerValue = 0;
                                                    if (JSONPath({
                                                        json: keyValue,
                                                        path: config.drilldowndata.path
                                                    }) && JSONPath({
                                                        json: keyValue,
                                                        path: config.drilldowndata.path
                                                    }).length > 0) {

                                                        JSONPath({
                                                            json: keyValue,
                                                            path: config.drilldowndata.path
                                                        })[0].forEach(function (dValue, dIndex) {
                                                            var drillValue = {}
                                                            Object.keys(config.maindata.seriesDataFormat).forEach(function (drillData) {
                                                                if (config.maindata.seriesDataFormat[drillData].match(/^.{0}[$]/)) {
                                                                    drillValue[drillData] = JSONPath({
                                                                        json: dValue,
                                                                        path: config.maindata.seriesDataFormat[drillData]
                                                                    })[0];
                                                                    if (drillData == 'name') {
                                                                        var drillName = JSONPath({
                                                                            json: dValue,
                                                                            path: config.maindata.seriesDataFormat[drillData]
                                                                        })[0];
                                                                        if (chartDataTypeName) {
                                                                            doiList[chartConfig.chartData.chartId][diffName][seriesName][drillName] = {};
                                                                            doiList[chartConfig.chartData.chartId][diffName][seriesName][drillName]['doi'] = [];
                                                                        }
                                                                        JSONPath({ json: dValue, path: "$..doi..buckets" })[0].forEach(function (doi) {
                                                                            doiList[chartConfig.chartData.chartId][diffName][seriesName][drillName]['doi'].push(JSONPath({ json: doi, path: "$.key" })[0])
                                                                        })
                                                                    }
                                                                } else if (config.maindata.seriesDataFormat[drillData].match(/^.{0}[v]/)) {
                                                                    var addUniqueName = (/v(.*)(\$.*)/gm.exec(config.drilldowndata[drillData]));
                                                                    drillValue[drillData] = JSONPath({
                                                                        json: dValue,
                                                                        path: addUniqueName[2]
                                                                    })[0];
                                                                } else if (config.maindata.seriesDataFormat[drillData].match(/^.{0}[x]/)) {
                                                                    drillValue[drillData] = xInnerValue;
                                                                } else if (config.maindata.seriesDataFormat[drillData].match(/^.{0}[y]/)) {
                                                                    drillValue[drillData] = yInnerValue;
                                                                }
                                                            })
                                                            innerDrillData.push(drillValue);
                                                            xInnerValue += 1;
                                                        })
                                                        yInnerValue += 1;
                                                        drillData[dData] = innerDrillData;
                                                    }
                                                }
                                            })
                                            drillData['dataLabels'] = {
                                                'enabled': true,
                                                'color': 'black',
                                                'style': {
                                                    'textShadow': 'none',
                                                    'HcTextStroke': null
                                                }
                                            }
                                            drillOverData.push(drillData);
                                        }
                                    }
                                    yAxisPoint += 1;
                                    heatMapData.push(dataFormat);
                                });


                            }
                        }
                        xAxisPoint += 1;
                    });
                    heatMapData = JSON.stringify(heatMapData);
                    config.maindata.replaceWith.forEach(function (replaceName) {
                        chartData = chartData.replace('"' + replaceName + '"', heatMapData);
                    });
                }
                else if (config.chartType && chartMapping[config.chartType] == 2) {
                    var key = 0;
                    chartData = JSON.parse(chartData);
                    // var movementDataName = ['Inflow', 'Outflow'];
                    if(!config.maindata && !config.maindata.orderToDisplay){
                        return;
                    }
                    var movementDataName = config.maindata.orderToDisplay;
                    input.forEach(function (inputValue, inputIndex) {
                        var seriesStockData = [];
                        doiList[chartConfig.chartData.chartId][movementDataName[inputIndex]] = {};
                        if (JSONPath({
                            json: inputValue,
                            path: config.maindata.path
                        }) && JSONPath({
                            json: inputValue,
                            path: config.maindata.path
                        }).length > 0) {
                            JSONPath({
                                json: inputValue,
                                path: config.maindata.path
                            })[0].forEach(function (keyValue, keyIndex) {
                                var stockData = [];
                                Object.keys(config.maindata.seriesDataFormat).forEach(function (sData) {
                                    if (config.maindata.seriesDataFormat[sData].match(/^.{0}[$]/)) {
                                        stockData.push(JSONPath({
                                            json: keyValue,
                                            path: config.maindata.seriesDataFormat[sData]
                                        })[0]);
                                        if (sData == 'name') {
                                            var date = JSONPath({ json: keyValue, path: '$.key_as_string' })[0].replace(/ /g, '');
                                            doiList[chartConfig.chartData.chartId][movementDataName[inputIndex]][date] = {};
                                            doiList[chartConfig.chartData.chartId][movementDataName[inputIndex]][date]['doi'] = [];
                                            if (JSONPath({ json: keyValue, path: "$..doi..buckets" })[0]) {
                                                JSONPath({ json: keyValue, path: "$..doi..buckets" })[0].forEach(function (doi) {
                                                    doiList[chartConfig.chartData.chartId][movementDataName[inputIndex]][date]['doi'].push(JSONPath({ json: doi, path: "$.key" })[0])
                                                })
                                            }
                                        }
                                    }
                                })
                                seriesStockData.push(stockData);
                            })
                        }
                        chartData.options.series[inputIndex].data = seriesStockData;
                        if (chartData.options.rangeSelector && seriesStockData.length == 0) {
                            if (inputIndex != input.length - 1) {
                                chartData.options.rangeSelector.y = 10;
                            }
                        } else {
                            chartData.options.rangeSelector.y = 0;
                        }
                    })
                    chartData = JSON.stringify(chartData);
                }
                else if (config.chartType && chartMapping[config.chartType] == 3) {
                    input = input.map((eachArray) => {
                        return JSONPath({ json: eachArray, path: '$..script_score' })[0][0]
                    })
                    var dotData = {};
                    var dotOutput = {};
                    dotData.data = input;
                    dotData.output = dotOutput;
                    var pagefn = doT.template(document.getElementById('performanceReport').innerHTML, undefined, undefined);
                    pagefn(dotData);
                    var inputData = [];
                    var output = [];
                    var keys = config.maindata.order.array;
                    for (var i = 0, n = keys.length; i < n; i++) {
                        var key = keys[i];
                        if (dotOutput[key] != undefined) output.push({ 'key': key, 'date': dotOutput[key] })
                    }
                    var dateRange = ['0d-1d', '1d-2d', '2d-3d', '3d-5d', '5d-10d', '>10d'];
                    var seriesData = [];
                    var allDrillDownData = [];
                    var seriesDataXaxis = 0;
                    var seriesDataYaxis = 0;
                    output.forEach((value, seriesIndex) => {
                        var innerSeriesData = {}, drilldownData = {};
                        doiList[chartConfig.chartData.chartId][value.key] = {};
                        dateRange.forEach((yAxis, index) => {
                            innerSeriesData = {};
                            drilldownData = {};
                            innerSeriesData.name = drilldownData.name = value.key;
                            innerSeriesData.value = [].concat.apply([], JSONPath({ json: value, path: '$..' + yAxis + '..doi' })).length;
                            innerSeriesData.x = seriesDataXaxis;
                            innerSeriesData.y = index;
                            innerSeriesData.drilldown = drilldownData.id = value.key + '_' + yAxis;
                            seriesData.push(innerSeriesData);
                            drilldownData.data = [];
                            doiList[chartConfig.chartData.chartId][value.key][yAxis] = {};
                            if (value.date[yAxis] != undefined) {
                                Object.keys(value.date[yAxis]).forEach((project, innerIndex) => {
                                    var innerData = {};
                                    doiList[chartConfig.chartData.chartId][value.key][yAxis][project] = {};
                                    doiList[chartConfig.chartData.chartId][value.key][yAxis][project]['doi'] = [];
                                    innerData.name = project;
                                    innerData.x = innerIndex;
                                    innerData.y = index;
                                    var data = [].concat.apply([], JSONPath({ json: value.date[yAxis], path: '$..' + project + '..doi' }));
                                    innerData.value = data.length;
                                    doiList[chartConfig.chartData.chartId][value.key][yAxis][project]['doi'] = data;
                                    drilldownData.data.push(innerData);
                                })
                            }
                            drilldownData.dataLabels = {
                                'enabled': true,
                                'color': 'black',
                                'style': {
                                    'textShadow': 'none',
                                    'HcTextStroke': null
                                }
                            }
                            allDrillDownData.push(drilldownData);
                        })
                        seriesDataXaxis++;
                    })
                    drillOverData = allDrillDownData;
                    var heatMapData = JSON.stringify(seriesData);
                    config.maindata.replaceWith.forEach(function (replaceName) {
                        chartData = chartData.replace('"' + replaceName + '"', heatMapData);
                    });
                }
                if (config.callFunction && config.callFunction.funcName && config.callFunction.variableToPass) {
                    config.main.replaceWith.forEach(function (replaceName) {
                        if (calculationArray[replaceName] != undefined) {
                            chartData = chartData.replace('"' + replaceName + '"', '[' + calculationArray[replaceName] + ']');
                        } else {
                            chartData = chartData.replace('"' + replaceName + '"', '[ ]');
                        }
                    });
                }
            }
            if (chartData.match('drilldownData')) {
                chartData = chartData.replace('"' + config.drilldowndata.replaceWith + '"', JSON.stringify(drillOverData));
            }
            /**
             * To display buttons in chart.
             */
            var buttons = {
                exportXLSX: {
                    onclick: function (e) {
                        //To display loading icon when Export-XLSX button is clicked
                        $('.la-container').css('display', '');
                        var currentSelected = this;
                        if(config.movementReportDetails){
                            var exportButton = true;
                        }
                        eventHandler.highChart.tableReport(e, currentSelected, config, customers, 'exportExcel', 'buttonClick', exportButton)
                    },
                    text: 'Export-XLSX'
                },
                exportTable: {
                    onclick: function (e) {
                        var currentSelected = this;
                        //To display loading icon when Table-View button is clicked
                        $('.la-container').css('display', '');
                        if(config.movementReportDetails){
                            var exportButton = true;
                        }
                        eventHandler.highChart.tableReport(e, currentSelected, config, customers, 'exportTable', 'buttonClick', exportButton)
                    },
                    text: 'View Details'
                },
                thisweek: {
                    text: 'Past 7 Days',
                    onclick: function (e) {
                        var currentNode = this;
                        if (chartConfig && chartConfig.chartExtraButton && chartConfig.chartExtraButton.functioName && chartConfig.chartExtraButton.thisweek) {
                            eval(chartConfig.chartExtraButton.functioName)(currentNode, particularChartConfig, chartConfig.chartExtraButton.thisweek);
                        }
                    }
                },
                thismonth: {
                    text: 'This Month',
                    onclick: function (e) {
                        var currentNode = this;
                        if (chartConfig && chartConfig.chartExtraButton && chartConfig.chartExtraButton.functioName && chartConfig.chartExtraButton.thismonth) {
                            eval(chartConfig.chartExtraButton.functioName)(currentNode, particularChartConfig, chartConfig.chartExtraButton.thismonth);
                        }
                    }
                },
                thisyear: {
                    text: 'This Year',
                    onclick: function (e) {
                        var currentNode = this;
                        if (chartConfig && chartConfig.chartExtraButton && chartConfig.chartExtraButton.functioName && chartConfig.chartExtraButton.thisyear) {
                            eval(chartConfig.chartExtraButton.functioName)(currentNode, particularChartConfig, chartConfig.chartExtraButton.thisyear);
                        }
                    }
                }
            }
            /**
             * To display total count on top of charts.
             */
            var event = {
                load: function (event) {
                    var total = 0;
                    for (var j = 0; j < this.series.length; j++) {
                        if (this.series[j].userOptions && !this.series[j].userOptions.type) {
                            if (this.series[j].name == 'Performance Report') {
                                for (var i = 0, len = this.series[j].data.length; i < len; i++) {
                                    total += this.series[j].data[i].value;
                                }
                            } else {
                                for (var i = 0, len = this.series[j].data.length; i < len; i++) {
                                    total += this.series[j].data[i].y;
                                }
                            }
                        }
                    }
                    if(total){
                        var text = this.renderer.text(
                            'Articles Count: ' + total,
                            this.plotLeft,
                            this.plotTop - 5
                        ).attr({ 'OverallCount':total }).add()
                    }
                },
                drilldown: function (e) {
                    if(e && e.target && e.target.renderTo && e.target.renderTo.id && e.point && e.point.options){
                        var value = 0;
                        if(e.target.renderTo.id == 'overdue'){
                            if(e.points) {
                                e.points.forEach( (count) => {
                                    value += count.value;
                                })
                            } else {
                                value = e.point.options.value;
                            }
                        } else {
                            if(e.points) {
                                if(e.points[0].total){
                                    value = e.points[0].total;
                                } else {
                                    e.points.forEach( (count) => {
                                        value += count.options.y;
                                    })
                                }
                            } else {
                                value = e.point.options.y;
                            }
                        }
                        $('#' + e.target.renderTo.id + ' text:contains(Articles Count)').attr({ 'OverAllDrillDownCount': value })
                        $('#' + e.target.renderTo.id + ' text:contains(Articles Count) tspan').text('Selected : ' + e.point.name + ' ( ' + value + ' )')
                    }
                },
                drillup: function (e) {
                    if (e && e.target && e.target.renderTo && e.target.renderTo.id) {
                        $('#' + e.target.renderTo.id + ' text:contains(Selected) tspan').text('Articles Count: ' + $('#' + e.target.renderTo.id + ' text:contains(Selected)').attr('OverallCount'))
                    }
                }
            };

            var movementCount = {
                load: function (event) {
                    var total = [];
                    for (var j = 0; j < this.series.length; j++) {
                        if (this.series[j].name.match(/Inflow|Outflow/gi)) {
                            total[j] = 0;
                            for (i of this.series[j].processedYData) {
                                total[j] += i;
                            }
                        }
                    }
                    if (total[0] != 0 || total[1] != 0 ) {
                        var text = this.renderer.text(
                            'Inflow Count: ( ' + total[0] + ' )  Outflow Count: ( ' + total[1] + ' )',
                            this.plotLeft,
                            this.plotTop - 5
                        ).attr({ 'OverallCount': total }).add()
                    }
                },
            }
            var movementXaxisCount = {
                afterSetExtremes: function (e) {
                    var total = [];
                    for (var j = 0; j < this.series.length; j++) {
                        if (this.series[j].name.match(/Inflow|Outflow/gi)) {
                            total[j] = 0;
                            for (i of this.series[j].processedYData) {
                                total[j] += i;
                            }
                        }
                    }
                    if (total) {
                        $('#' + e.target.chart.renderTo.id + ' text:contains(Inflow) tspan').text('Inflow Count: ( ' + total[0] + ' )  Outflow Count: ( ' + total[1] + ' )');
                    }
                }

            }
            /**
             * To change the count of the Articles Count in chart when the legend is clicked.
             */
            var pieChartCounts = {
                legendItemClick: function (e) {
                    if (e && e.target && e.target.series && e.target.series.chart && e.target.series.chart.renderTo && e.target.series.chart.renderTo.id && e.target.options && e.target.options.y) {
                        var countName;
                        var id = e.target.series.chart.renderTo.id;
                        var data = e.target.options.y;
                        if (e.target.drilldown) {
                            countName = 'OverallCount';
                        } else {
                            countName = 'OverAllDrillDownCount';
                        }
                        var clickedData = '';
                        if ($('#' + id + ' text:contains(Selected) tspan').length) {
                            clickedData = Number($('#' + id + ' text:contains(Selected)').attr(countName));
                        } else {
                            clickedData = Number($('#' + id + ' text:contains(Articles Count)').attr(countName));
                        }
                        if (e.target.visible) {
                            var value = clickedData - data;
                        } else {
                            var value = clickedData + data;
                        }
                        if ($('#' + id + ' text:contains(Selected) tspan').length) {
                            $('#' + id + ' text:contains(Selected) tspan').text('Selected : ' + e.target.series.name + ' ( ' + value + ' )');
                            $('#' + id + ' text:contains(Selected)').attr({ [countName]: value })
                        } else {
                            $('#' + id + ' text:contains(Articles Count) tspan').text('Articles Count: ' + value);
                            $('#' + id + ' text:contains(Articles Count)').attr({ [countName]: value })
                        }
                    }
                }
            }
            var legendReverse = {
                legendItemClick: function (event) {
                    var s = this.chart.series;
                    var id = event.target.chart.renderTo.id;
                    var value = 0;
                    for (i = 0; i < s.length; i++) {
                        if (this.name == 'Show All' || this == s[i]) {
                            s[i].yData.forEach(function (index) {
                                value += index;
                            })
                            $('#' + id + ' text:contains(Articles Count) tspan').text('Articles Count: ' + value);
                            s[i].setVisible(true);
                        } else {
                            s[i].setVisible(false);
                        }
                    }
                    return false;
                }
            }
            /**
             * To make the reports for charts.
             */
            var reportModalBox = {
                click: function (e) {
                    clickEvent = this;
                    if (this.name && (this.y >= 0 && config.chartData.chartId == 'overdue') && this.series && this.series.userOptions && this.series.userOptions.type != 'line') {
                        //To display loading icon when chart is clicked
                        $('.la-container').css('display', '');
                        eventHandler.highChart.tableReport(e, clickEvent, config, customers, 'exportTable');
                    } else if (this.series && this.series.name && this.y && this.series.userOptions && this.series.userOptions.type != 'line') {
                        //To display loading icon when chart is clicked
                        $('.la-container').css('display', '');
                        eventHandler.highChart.tableReport(e, clickEvent, config, customers, 'exportTable');
                    }
                }
            };
            chartData = JSON.parse(chartData);
            chartDataTypeName = '';
            if (chartData.options.exporting && chartData.options.exporting.menuItemDefinitions) chartData.options.exporting.menuItemDefinitions = buttons;
            if (chartData.options.chart && chartData.options.chart.events) chartData.options.chart.events = event;
            if (chartData.options.plotOptions && chartData.options.plotOptions.series && chartData.options.plotOptions.series.point && chartData.options.plotOptions.series.point.events) chartData.options.plotOptions.series.point.events = reportModalBox;
            if (chartData.options.plotOptions && chartData.options.plotOptions.pie && chartData.options.plotOptions.pie.point && chartData.options.plotOptions.pie.point.events) chartData.options.plotOptions.pie.point.events = pieChartCounts;
            if (chartData.options.plotOptions && chartData.options.plotOptions.series && chartData.options.plotOptions.series.events) chartData.options.plotOptions.series.events = legendReverse;
            if (chartData.modalType == 'stockChart' && chartData.options && chartData.options.chart && chartData.options.chart.events) chartData.options.chart.events = movementCount;
            if (chartData.modalType == 'stockChart' && chartData.options && chartData.options.xAxis && chartData.options.xAxis.events) chartData.options.xAxis.events = movementXaxisCount;
            // if (chartData.options.exporting && chartData.options.exporting.buttons) chartData.options.exporting.buttons = durationButtons;

            /**
             * Pass the overall config for chart with built data to renderChart() function.
             */
            if (chartData != undefined && chartData != null && chartData != '') eventHandler.highChart.renderChart(chartData)
        },
        /**
         *  durationCalculation() - To calculate the mean, median, mode, range for chart data.
         *  @param calcData - Data for the calculation.
         *  @param config - Chart configuration.
         */
        durationCalculation: function (calcData, config) {
            if (!calcData) {
                return false;
            }
            var dataForCalc = [];
            if (config.callFunction.pathToGetData && config.callFunction.pathToGetDocCount && config.callFunction.pathToGetArticleDayDifference && JSONPath({
                json: calcData,
                path: config.callFunction.pathToGetData
            }) && JSONPath({
                json: calcData,
                path: config.callFunction.pathToGetData
            }).length > 0) {
                var articlesCountInStage = JSONPath({
                    json: calcData,
                    path: config.callFunction.pathToGetDocCount
                })[0];
                JSONPath({
                    json: calcData,
                    path: config.callFunction.pathToGetData
                })[0].forEach(function (calcValue, calcIndex) {
                    for (var i = 0; i < JSONPath({
                        json: calcValue,
                        path: config.callFunction.pathToGetDocCount
                    })[0]; i++) {
                        dataForCalc.push(parseInt(JSONPath({
                            json: calcValue,
                            path: config.callFunction.pathToGetArticleDayDifference
                        })[0]));
                    }
                });
            }
            if (!dataForCalc) {
                return false;
            }
            //To find Mean.
            var mean = parseFloat((dataForCalc.reduce((a, b) => a + b, 0) / articlesCountInStage).toFixed(2));

            //To find Median.
            if (dataForCalc.length > 1) {
                var median = dataForCalc.sort(function (a, b) {
                    return a - b;
                });
                var medianIndex = Math.ceil((dataForCalc.length - 1) / 2);
                if ((dataForCalc.length - 1) % 2 == 1) {
                    median = dataForCalc[medianIndex];
                } else {
                    median = ((dataForCalc[medianIndex] + dataForCalc[medianIndex + 1]) / 2.0);
                }
            } else {
                median = dataForCalc[0];
            }

            //To find Range.
            var range = dataForCalc[dataForCalc.length - 1] - dataForCalc[0];

            //To find Mode.
            var mf = 1,
                m = 0,
                item = 0;
            for (var i = 0; i < dataForCalc.length; i++) {
                for (var j = i; j < dataForCalc.length; j++) {
                    if (dataForCalc[i] == dataForCalc[j])
                        m++;
                    if (mf < m) {
                        mf = m;
                        item = dataForCalc[i];
                    }
                }
                m = 0;
            }

            if (item != undefined) var mode = item;
            if (mean != undefined && median != undefined) {
                return {
                    'mean': mean,
                    'median': median,
                    'mode': mode
                };
            }
        },
        /**
         * Will change the range of the Articles duration in each stage.
         */
        differentDurationOfArticles: function (currentNode, particularChartConfig, rangeData) {
            if (currentNode && currentNode.renderTo && currentNode.renderTo.id) {
                var id = currentNode.renderTo.id;
                var config = particularChartConfig;
                $('#' + id).css('background-color', 'white');
                $('#' + id).html('<div class="chartLoader"></div>');
                if (config && config.api && config.api[0] && config.api[0].data && config.api[0].data.startdate && rangeData.startdate && rangeData.enddate) {
                    config.api[0].data.startdate = rangeData.startdate;
                    config.api[0].data.enddate = rangeData.enddate;
                    if (config.chartInfo && config.chartInfo.functionName && config.chartInfo.functionName[0] && config.chartInfo.functionName[0].chartData && config.chartInfo.functionName[0].chartData.options && config.chartInfo.functionName[0].chartData.options.title && config.chartInfo.functionName[0].chartData.options.title.text) {
                        config.chartInfo.functionName[0].chartData.options.title.text = rangeData.text;
                    }
                    eventHandler.highChart.makeCharts(config, true);
                }
            }
        },
        tableReport: function (e, clickEvent, config, customers, tableType, buttonClick, exportButton,fromTotal, toTotal) {
            var xPath = '', clickEventSeriesName = '';
            var params = {};
            $('#modalReport #clickedText, #modalReport #titleText').text('');
            if(fromTotal == undefined && toTotal == undefined){
                var fromTotal = 0, toTotal = articleLoopCountInTableReport;
                reportArticles = '';
                if (exportButton) {
                    var startDate = $('#' + config.chartData.chartId).find('.highcharts-label.highcharts-range-input tspan:first').text()
                    var endDate = $('#' + config.chartData.chartId).find('.highcharts-label.highcharts-range-input tspan:last').text();
                    startDate = moment(startDate).format('dddd,MMMD,YYYY');
                    endDate = moment(endDate).format('dddd,MMMD,YYYY');
                    var dateArray = [];
                    for (var day = 1; moment(endDate).isAfter(startDate) || moment(endDate).isSame(startDate) ; day++) {
                        xpath = '$.' + config.chartData.chartId + '..' + startDate + '..doi';
                        dateArray.push([].concat.apply([], JSONPath({ json: doiList, path: xpath })));
                        startDate = moment(startDate, 'dddd,MMMD,YYYY').add('days', 1).format('dddd,MMMD,YYYY');
                    }
                    reportArticles = [].concat.apply([], dateArray);
                } else {
                    if ((clickEvent.ddDupes == undefined || !clickEvent.ddDupes.length) && buttonClick) {
                        xPath = '$..' + config.chartData.chartId + '..doi';
                    } else if ((clickEvent.ddDupes && clickEvent.ddDupes[0] && e.currentTarget.innerText) && buttonClick) {
                        if (clickEvent.ddDupes[0].match('_')) {
                            clickEventSeriesName = clickEvent.ddDupes[0].replace('_', '\'][\'');
                        } else {
                            clickEventSeriesName = clickEvent.series[0].name;
                        }
                        xPath = '$..' + config.chartData.chartId + '[\'' + clickEventSeriesName + '\']..doi';
                    } else {
                        if (clickEvent.series && clickEvent.series.userOptions && clickEvent.series.userOptions.id && clickEvent.series.userOptions.id.match('_')) {
                            clickEventSeriesName = clickEvent.series.userOptions.id.replace('_', '\'][\'');
                        } else {
                            clickEventSeriesName = clickEvent.series.name;
                        }
                        if (clickEvent.name == undefined) {
                            clickEvent.name = moment(clickEvent.options.x).format('dddd,MMMD,YYYY');
                        }
                        xPath = '$..' + config.chartData.chartId + '[\'' + clickEventSeriesName + '\'][\'' + clickEvent.name + '\'].doi'
                    }
                    reportArticles = [].concat.apply([], JSONPath({ json: doiList, path: xPath }));
                }
            }
            var doiString = reportArticles.slice(fromTotal, toTotal).join(' OR ').replace(/,/g, ' OR ');
            if(doiString == ''){
                $('.la-container').css('display', 'none');
                return;
            }
            if (typeof(customers) == "object") {
                customers = customers[0];
            }
            if (params) {
                params.url = '/api/getarticlelist';
                params[tableType] = true;
                params.doistring = doiString;
                params.urlToPost = 'getDoiArticles';
                params.from = "0";
                params.size = "500";
                params.customer = customers;
                if(!fromTotal){
                    eventHandler.components.actionitems.getUserDetails(customers);
                }
                if (config.movementReportDetails) {
                    var movementData = {
                        'stageName': config.movementReportDetails.stageName,
                        'dateType': clickEvent.series.name,
                        'date': moment(clickEvent.name).format('YYYY-MM-DD')
                    }
                    if(movementData.dateType == undefined){
                        movementData.dateType = 'All';
                    }
                }
                eventHandler.bulkedit.export.exportTable(params, customers, fromTotal, toTotal, reportArticles.length, tableType, config.chartData.chartTableType, config.chartData.chartTableTemplateName, movementData, config, clickEvent);
            }
        },
        /**
         * Containing charts config for Dashboard.
         */
        defaultCharts: [
            /**
             *  Journal Wise pie chart with drilldown.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getReport',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,
                        "maindata": {
                            "path": "$..project.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],

                        },
                        "drilldowndata": {
                            "path": "$..stages.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'ipBoard',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Journals'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Ongoing Articles",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /**
           *  Production days pie chart with drilldown.
           */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getProductionDays',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,
                        "maindata": {
                            "path": "$..groupby.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],

                        },
                        "drilldowndata": {
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                            // "order": false
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'prodChart',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Days in Production'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Days in Production",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /***
           * Overdue articles by Days
           */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getOverdueArticles',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,
                        "maindata": {
                            "path": "$..dayDiff.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['1 Day', '2 Days', '3 Days', '> 3 Days']
                            }
                        },
                        "drilldowndata": {
                            "path": "$..stages.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'overduealone',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Overdue articles by Days'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Overdue articles by Days",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /**
            * Overdue articles by stage
            */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getOverdueArticlesByStage',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,
                        "maindata": {
                            "path": "$..stages.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        "drilldowndata": {
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'overdueStage',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Overdue articles by stage'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {

                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Overdue articles by stage",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /**
            * To display fasttrack articles
            */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getFastTrackArticles',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,

                        "maindata": {
                            "path": "$..stages.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        "drilldowndata": {
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData"
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'fasttrack',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Urgent articles by Stage'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {

                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Urgent articles by journal",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /**
             * To display urgent articles in pie chart
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getFastTrackArticlesByStage',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus",
                        "sendAsArray": true,
                        "maindata": {
                            "path": "$..dayDiff.buckets",
                            "conditionToCheck": "name",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["seriesData"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['0 - 1 Day', '1 - 2 Days', '2 - 3 Days', '3 - 4 Days', '> 4 Days']
                            }
                        },
                        "drilldowndata": {
                            "path": "$..stages.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                            "order": {
                                "stageVariable": "key",
                                "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'urgentStage',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'pie',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Urgent articles by Days'
                                },
                                'plotOptions': {
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true,
                                            'format': '{point.y}',
                                            'distance': -50
                                        },
                                        'cursor': 'pointer',
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    'pie': {
                                        'showInLegend': true,
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'align': 'right',
                                    'verticalAlign': 'top',
                                    'layout': 'vertical',
                                    'x': 0,
                                    'y': 100,
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{series.name}</span><br>',
                                    'pointFormat': '<span style=\'color:{point.color}\'>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                "series": [{
                                    "name": "Urgent articles by stage",
                                    "colorByPoint": true,
                                    "data": "seriesData"
                                }],
                                "drilldown": {
                                    "series": "drilldownData"
                                }
                            }
                        }
                    }
                    ]
                },
            },
            /**
             * Journal wise general, on-hold, fasttrack articles in stackedBar chart with drilldown.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getJournalWise',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "maindata": {
                            "path": "$..project.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["Fasttrack_", "Normal_", "On-Hold_", "overdue"]
                        },
                        "drilldowndata": {
                            "path": "$..stages.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData",
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'statusBoard',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'marginTop': 50,
                                    'type': 'column',
                                    'events': true
                                },
                                'title': {
                                    'text': 'Ongoing Articles Across Journal by Status'
                                },
                                'xAxis': {
                                    'type': 'category',
                                    'labels': {
                                        'style': {
                                            'fontSize': '8px'
                                        }
                                    }
                                },
                                'yAxis': {
                                    'min': 0,
                                    'title': {
                                        'text': '#Articles'
                                    },
                                },
                                'legend': {
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'plotOptions': {
                                    'series': {
                                        'stacking': 'normal',
                                        'borderWidth': 0,
                                        'dataLabels': {
                                            'enabled': true
                                        },
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    }
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "name": "Normal",
                                    "data": "Normal_"
                                },
                                {
                                    "name": "Fasttrack",
                                    "data": "Fasttrack_"
                                },
                                {
                                    "name": "On-Hold",
                                    "data": "On-Hold_"
                                },
                                {
                                    "name": "Show All"
                                }
                                ],
                                'drilldown': {
                                    'series': 'drilldownData'
                                }
                            }
                        }
                    }],
                },
            },
             /**
             *  Stage wise journal summary in bar chart with drilldown.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getJournalSummary',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "merge": [{
                            "valPath": "$..general.stages.buckets",
                            "appendTo": "general"
                        },
                        {
                            "valPath": "$..overDue.project",
                            "appendTo": "general",
                            "appendAs": "overdue",
                            "docCount": "$..overDue.doc_count",
                            "append": "first"
                        },
                        {
                            "valPath": "$..urgent.project",
                            "appendTo": "general",
                            "appendAs": "urgent",
                            "docCount": "$..urgent.doc_count",
                            "append": "first"
                        }

                        ],
                        "sendAsArray": false,
                        "maindata": {
                            "path": "$..general",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "replaceWith": ["general"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        "drilldowndata": {
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData"
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'journalsummary',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'column',
                                    'marginTop': 50,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Journal Summary By Stage'
                                },
                                'xAxis': {
                                    'type': 'category',
                                    'labels': {
                                        'style': {
                                            'fontSize': '9px'
                                        }
                                    }
                                },
                                'yAxis': {
                                    'min': 0,
                                    'title': {
                                        'text': '#Articles'
                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'borderWidth': 0,
                                        'dataLabels': {
                                            'enabled': true
                                        },
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                'legend': {
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "name": "Journal Summary",
                                    "data": "general"
                                }],
                                'drilldown': {
                                    'series': 'drilldownData'
                                }
                            }
                        }
                    }],
                },
            },
            /**
            *  Stage Wise inflow, outflow articles for Pre-editing.
            */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Pre-editing',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Pre-editing',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Pre-editing"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"

                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementReport',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Pre-editing)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',
                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
            /**
             *  Stage Wise inflow, outflow articles for Copyediting.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Copyediting',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Copyediting',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Copyediting"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"
                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementcopy',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Copyediting)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',

                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
            /**
             *  Stage Wise inflow, outflow articles for Copyediting Check.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Copyediting Check',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Copyediting Check',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Copyediting Check"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"
                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementcopycheck',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Copyediting Check)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',

                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
            /**
            *  Stage Wise inflow, outflow articles for Typesetter Check.
            */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Typesetter Check',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Typesetter Check',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Typesetter Check"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"

                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementtypesetter',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Typesetter Check)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',

                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
            /**
            *  Stage Wise inflow, outflow articles for Typesetter Review.
            */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Typesetter Review',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Typesetter Review',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Typesetter Review"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"

                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },


                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementtypesetterrev',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Typesetter Review)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',

                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
            /**
             *  Stage Wise inflow, outflow articles for Final Deliverables.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': '*',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Final Deliverables',
                        'dateType': 'stage.sla-start-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                },
                {
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getMovement',
                        'customer': 'customer',
                        'project': '.*',
                        'stagename': 'Final Deliverables',
                        'dateType': 'stage.end-date',
                        'regex': true,
                        'version': 'v2.0'
                    }
                }
                ],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..body.aggregations.jobStatus.buckets",
                        "sendAsArray": false,
                        "movementReportDetails": {
                            "stageName": "Final Deliverables"
                        },
                        "maindata": {
                            "path": "$..date.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$.article.doc_count",
                                "z": "$.doc_count"

                            },
                            "replaceWith": ["inflow", "outflow"],
                            "orderToDisplay": ["Inflow", "Outflow"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },


                        'chartType': 'stock',
                        'chartData': {
                            'modalType': 'stockChart',
                            'chartId': 'movementfinal',
                            'chartTableType': 'movementChart',
                            'chartTableTemplateName': 'movementChartReportTemplate',
                            'options': {
                                'chart': {
                                    'alignTicks': false,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Movement Report (Final Deliverables)'
                                },
                                'rangeSelector': {
                                    'buttonPosition': {
                                        'align': 'left',
                                        'x': 0,
                                        'y': -4
                                    },
                                    'inputPosition': {
                                        'align': 'right',
                                        'x': -5,
                                        'y': -4
                                    },
                                    'buttons': [{
                                        'type': 'week',
                                        'count': 1,
                                        'text': '1W'
                                    }, {
                                        'type': 'month',
                                        'count': 1,
                                        'text': '1M'
                                    },
                                    {
                                        'type': 'year',
                                        'count': 1,
                                        'text': '1Y'
                                    },
                                    {
                                        'type': 'all',
                                        'count': 1,
                                        'text': 'All'
                                    }
                                    ],
                                    'selected': 0,
                                    'inputEnabled': true,
                                    'floating': false,
                                    'y': 0,
                                    'verticalAlign': 'top'
                                },
                                'xAxis': {
                                    'events': true
                                },
                                'yAxis': {
                                    'top': '1%',
                                    'height': '99%',
                                    'opposite': false,
                                    'title': {
                                        'text': '#Articles',

                                    },
                                },
                                'plotOptions': {
                                    'series': {
                                        'gapsize': 1,
                                        'showInNavigator': true,
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        },
                                        'dataLabels': {
                                            'enabled': true
                                        }
                                    }
                                },
                                'tooltip': {
                                    'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                    'valueDecimals': 0,
                                    'split': true
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "type": 'column',
                                    "name": "Inflow",
                                    "data": "inflow"
                                },
                                {
                                    "type": 'column',
                                    "name": "Outflow",
                                    "data": "outflow"
                                }]
                            }

                        }
                    }]
                }
            },
             /***
             *  Stage wise article duartion in line chart.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getDurationOfArticles',
                        'customer': 'customer',
                        'project': 'project',
                        "startdate": "now-7d/d",
                        "enddate": "now/d",
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..stageName.buckets",
                        "order": {
                            "stageVariable": "key",
                            "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                        },
                        "sendAsArray": false,
                        "main": {
                            "path": "$..year.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": ""
                            },
                            "replaceWith": ["mean", "median", "mode"]
                        },
                        'callFunction': {
                            'funcName': 'eventHandler.highChart.durationCalculation',
                            'variableToPass': 'inputValue',
                            'pathToGetData': '$..groupby.buckets',
                            'pathToGetDocCount': '$..doc_count',
                            'pathToGetArticleDayDifference': "$..key"
                        },
                        'chartExtraButton': {
                            'functioName': 'eventHandler.highChart.differentDurationOfArticles',
                            'thisweek': {
                                'startdate': 'now-7d/d',
                                'enddate': 'now/d',
                                'text': 'Duration of Articles (Past 7 Days)'
                            },
                            'thismonth': {
                                'startdate': 'now/M',
                                'enddate': 'now/d',
                                'text': 'Duration of Articles (This Month)'
                            },
                            'thisyear': {
                                'startdate': 'now/y',
                                'enddate': 'now/d',
                                'text': 'Duration of Articles (This Year)'
                            }
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'articlesDuration',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'line',
                                },
                                'title': {
                                    'text': 'Duration of Articles (Past 7 Days)',
                                },
                                'subtitle': {
                                    'text': ''
                                },
                                'xAxis': {
                                    'type': 'category',
                                    'labels': {
                                        'style': {
                                            'fontSize': '7px'
                                        },
                                    }
                                },
                                'yAxis': {
                                    'min': 0,
                                    'title': {
                                        'text': '#Days'
                                    },
                                },
                                'legend': {
                                    'itemStyle': {
                                        'fontSize': '11px'
                                    }
                                },
                                'plotOptions': {
                                    'series': {
                                        'borderWidth': 0,
                                        'dataLabels': {
                                            'enabled': true
                                        },
                                    }
                                },
                                'exporting': {
                                    "menuItemDefinitions": true,
                                    'buttons': {
                                        'contextButton': {
                                            "menuItems": [
                                                'thisweek',
                                                'thismonth',
                                                'thisyear',
                                                'downloadCSV'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "name": "Mean",
                                    "data": "mean"
                                },
                                {
                                    "name": "Median",
                                    "data": "median"
                                },
                                {
                                    "name": "Mode",
                                    "data": "mode"
                                }
                                ]
                            }
                        }
                    }],
                },
            },
             /**
             *  Stage wise performance report in heatmap chart with drilldown.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'in-progress',
                        'urlToPost': 'getOverdue',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..hits.hits",
                        "sendAsArray": false,
                        "XaxisLabelPath": "$..key",
                        "maindata": {
                            "path": "$..groupby.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                'name': "$.key",
                                'x': "xAxis",
                                'y': "yAxis",
                                "value": "value$.renest.doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "defaultReplaceName": true,
                            "defaultReplaceValue": ['<1day', '1d', '2d', '3d-5d', '5d-10d', '>10d'],
                            "replaceWith": ["heatmapData"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['content-loading', 'addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                            },
                        },
                        "drilldowndata": {
                            "value": "value$.doc_count",
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$.key",
                                "id": "#replaceWith$..key",
                                "data": "data"
                            },
                            "replaceWith": "drilldownData"
                        },
                        'chartType': 'heatmapRemovedDate',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'overdue',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                'chart': {
                                    'type': 'heatmap',
                                    'marginTop': 50,
                                    'events': true
                                },
                                'title': {
                                    'text': 'Performance Report'
                                },
                                'xAxis': {
                                    'type': 'category',
                                    'labels': {
                                        'style': {
                                            'fontSize': '7px'
                                        },
                                    }
                                },
                                'yAxis': {
                                    'categories': ['0d-1d', '1d-2d', '2d-3d', '3d-5d', '5d-10d', '>10d'],
                                    'title': {
                                        'text': '#Days'
                                    },
                                },

                                'colorAxis': {
                                    'min': 0,
                                    'minColor': '#FFFFFF',
                                    'maxColor': '#84b9ed'
                                },

                                'legend': {
                                    'align': 'right',
                                    'layout': 'vertical',
                                    'margin': 0,
                                    'verticalAlign': 'top',
                                    'y': 25,
                                    'symbolHeight': 320
                                },
                                'tooltip': {
                                    'headerFormat': '<span style=\'font-size:11px\'>{point.name}</span><br>',
                                    'pointFormat': '<span style=\'color:black\'>{point.name}</span>: <b>{point.value}</b> of total<br/>'
                                },
                                'plotOptions': {
                                    'heatmap': {
                                        'allowPointSelect': true
                                    },
                                    'series': {
                                        'dataLabels': {
                                            'enabled': true
                                        },
                                        'cursor': 'pointer',
                                        'point': {
                                            'events': true
                                        }
                                    }
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    'name': 'Performance Report',
                                    'borderWidth': 1,
                                    'drilldown': true,
                                    'data': 'heatmapData',
                                    'dataLabels': {
                                        'enabled': true,
                                        'color': 'black',
                                        'style': {
                                            'textShadow': 'none',
                                            'HcTextStroke': null
                                        }
                                    }
                                }

                                ],
                                'drilldown': {
                                    'series': 'drilldownData'
                                }

                            }
                        }
                    }],
                },
            },
            /**
            *  Articles published journal wise in stackedBar chart with drilldown based on month.
            */
           {
            'api': [
            {
                'data': {
                    'workflowStatus': 'completed',
                    'urlToPost': 'getMovement',
                    'customer': 'customer',
                    'project': '.*',
                    'stagename': 'Archive|Moved to Archive',
                    'dateType': 'stage.end-date',
                    'regex': true,
                    'version': 'v2.0'
                }
            }
            ],
            'chartInfo': {
                'functionName': [{
                    "funcName": "eventHandler.highChart.getChartData",
                    "input": "$..body.aggregations.jobStatus.buckets",
                    "sendAsArray": false,
                    "movementReportDetails": {
                        "stageName": "Archive|Moved to Archive"
                    },
                    "maindata": {
                        "path": "$..date.buckets",
                        'labelsMust': false,
                        "seriesDataFormat": {
                            "name": "$..key",
                            "y": "$.article.doc_count",
                            "z": "$.doc_count"
                        },
                        "replaceWith": ["inflow"],
                        "orderToDisplay": ["Published"]
                    },
                    'chartType': 'stock',
                    'chartData': {
                        'modalType': 'stockChart',
                        'chartId': 'publishedBoard',
                        'chartTableType': 'default',
                        'chartTableTemplateName': 'chartReportTemplate',
                        'options': {
                            'chart': {
                                'alignTicks': false
                            },
                            'title': {
                                'text': 'Articles Published'
                            },
                            'rangeSelector': {
                                'buttons': [{
                                    'type': 'week',
                                    'count': 1,
                                    'text': '1W'
                                }, {
                                    'type': 'month',
                                    'count': 1,
                                    'text': '1M'
                                },
                                {
                                    'type': 'year',
                                    'count': 1,
                                    'text': '1Y'
                                },
                                {
                                    'type': 'all',
                                    'count': 1,
                                    'text': 'All'
                                }
                                ],
                                'selected': 0,
                                'inputEnabled': true,
                                'floating': false,
                                'y': 0,
                                'verticalAlign': 'top'
                            },
                            'yAxis': {
                                'opposite': false,
                                'title': {
                                    'text': '#Articles',
                                },
                            },
                            'plotOptions': {
                                'series': {
                                    'gapsize': 1,
                                    'showInNavigator': true,
                                    'cursor': 'pointer',
                                    'point': {
                                        'events': true
                                    },
                                    'dataLabels': {
                                        'enabled': true
                                    }
                                }
                            },
                            'tooltip': {
                                'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Articles<br/>',
                                'valueDecimals': 0,
                                'split': true
                            },
                            "exporting": {
                                "menuItemDefinitions": true,
                                "buttons": {
                                    "contextButton": {
                                        "menuItems": [
                                            'exportXLSX',
                                            'exportTable'
                                        ]
                                    }
                                }
                            },
                            'series': [
                            {
                                "type": 'column',
                                "name": "Published",
                                "data": "inflow"
                            }]
                        }

                    }
                }]
            }
        },
            /***
             *  Published articles month wise with year wise comparision in bar chart with drilldown.
             */
            {
                'api': [{
                    'data': {
                        'workflowStatus': 'completed',
                        'urlToPost': 'getPublishedYearWise',
                        'customer': 'customer',
                        'project': 'project',
                        'version': 'v2.0'
                    }
                }],
                'chartInfo': {
                    'functionName': [{
                        "funcName": "eventHandler.highChart.getChartData",
                        "input": "$..spend_per_year.buckets",
                        "sendAsArray": false,
                        "maindata": {
                            "path": "$..year.buckets",
                            'labelsMust': false,
                            "seriesDataFormat": {
                                "name": "$..key",
                                "y": "$..comment_to_issue.doc_count",
                                "drilldown": "#replaceWith$..key"
                            },
                            "defaultReplaceName": true,
                            "defaultReplaceValue": ["Last Year", "This Year"],
                            "replaceWith": ["Last Year_", "This Year_"],
                            "order": {
                                "stageVariable": "key",
                                "array": ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                            },
                            "multiplegraph": {
                                "path": ["$..count.buckets", "$..count.buckets"],
                                "replaceWith": ["lastyearpagecount", "thisyearpagecount"],
                            }
                        },
                        "drilldowndata": {
                            "path": "$..project.buckets",
                            "label": "key",
                            'labelsMust': false,
                            "drilldownDataFormat": {
                                "name": "$..key",
                                "year": "y$.key_as_string",
                                "id": "#replaceWith$..key",
                                "data": ['$$..key', '$$..doc_count']
                            },
                            "replaceWith": "drilldownData"
                        },
                        'chartType': 'drilldown',
                        'chartData': {
                            'modalType': 'chart',
                            'chartId': 'published',
                            'chartTableType': 'default',
                            'chartTableTemplateName': 'chartReportTemplate',
                            'options': {
                                "chart": {
                                    "type": 'column',
                                    'marginTop': 50,
                                    'events': true
                                },
                                "title": {
                                    "text": 'Published (Month Wise)'
                                },
                                "xAxis": [{
                                    'type': 'category',
                                    'labels': {
                                        'style': {
                                            'fontSize': '9px'
                                        },
                                    }
                                }],
                                "yAxis": [{
                                    "title": {
                                        "text": '#Articles'
                                    }
                                }, {
                                    "opposite": true,
                                    "title": {
                                        "text": '#Page Count'
                                    }
                                }],
                                'legend': {
                                    'itemStyle': {
                                        'fontSize': '9px'
                                    }
                                },
                                "plotOptions": {
                                    "series": {
                                        'point': {
                                            'events': true
                                        },
                                        'events': true
                                    },
                                    "column": {
                                        'cursor': 'pointer',
                                        "dataLabels": {
                                            "enabled": true,
                                            "format": '{point.y}'
                                        },
                                    },
                                    "line": {
                                        "dataLabels": {
                                            "enabled": true,
                                            "format": '{point.y}'
                                        },
                                        'point': {
                                            'events': false
                                        },
                                    }
                                },
                                "exporting": {
                                    "menuItemDefinitions": true,
                                    "buttons": {
                                        "contextButton": {
                                            "menuItems": [
                                                'exportXLSX',
                                                'exportTable'
                                            ]
                                        }
                                    }
                                },
                                'series': [{
                                    "name": "Last Year",
                                    "data": "Last Year_",
                                    'tooltip': {
                                        'headerFormat': '<span style="font-size:11px">{series.name}</span><br>',
                                        'pointFormat': '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                    },
                                },
                                {
                                    "name": "This Year",
                                    "data": "This Year_",
                                    'tooltip': {
                                        'headerFormat': '<span style="font-size:11px">{series.name}</span><br>',
                                        'pointFormat': '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                    },
                                }, {
                                    "name": 'Last Year Page Count',
                                    "color": '#FFFF33',
                                    "type": 'line',
                                    "data": "lastyearpagecount",
                                    'yAxis': 1,
                                    'tooltip': {
                                        'headerFormat': '<span style="font-size:11px">{series.name}</span><br>',
                                        'pointFormat': '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                    },
                                }, {
                                    "name": 'This Year Page Count',
                                    "color": '#89A54E',
                                    "type": 'line',
                                    "data": "thisyearpagecount",
                                    'yAxis': 1,
                                    'tooltip': {
                                        'headerFormat': '<span style="font-size:11px">{series.name}</span><br>',
                                        'pointFormat': '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                    },
                                }, {
                                    "name": "Show All"
                                }
                                ],
                                'drilldown': {
                                    'tooltip': {
                                        'headerFormat': '<span style="font-size:11px">{series.name}</span><br>',
                                        'pointFormat': '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                                    },
                                    'series': 'drilldownData'
                                }

                            }
                        }
                    }],
                },
            },
        ]
    }
})(eventHandler || {})

