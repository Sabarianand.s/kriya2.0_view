/*
*  citation renumbering class
*  data-cite-type="insert" data-cite-ids=" F2 "
*
*/
var citeJS = function() {
	/**
	*   cues to identify the float type
	*	F	- Figure
	*	T	- Table
	*	S	- Figure Supplements
	*	SD	- Source Data
	*	SC	- Source Code
	*	SP	- Supplementary File
	*	V	- Video
	*	
	*	settings.Reference.citationType => 0 = Vancouver; 1 = Harvard (Author/Year)
	*	settings.Reference.authorFormat.etalFormat => sup,b,i,sub
	*	settings.Reference.authorFormat: {
				'1': 'SN|, |Year',
				'2': 'SN| and |SN|, |Year',
				'more': 'SN| |et al|., |Year',
				'etalFormat': 'i',
				},
	*/
	var settings = {};
	settings = {
		'containerElm': '#contentDivNode',
		'showMsg' : true,
		'citeClass' : {
			'jrnlFigBlock': 'jrnlFigRef',
			'jrnlTblBlock': 'jrnlTblRef',
			'jrnlSupplBlock': 'jrnlSupplRef',
			'jrnlVidBlock': 'jrnlVidRef',
			'jrnlBoxBlock': 'jrnlBoxRef',
			'jrnlRefText' : 'jrnlBibRef',
			'jrnlFootNote': 'jrnlFootNoteRef',	
		},
		/** First check the relevent citation configuration else get from common**/
		'rangePunc'   : '&ndash;',
		'separator': ', ',
		/** The punctuations only handles when inserting citation **/
		'startPunc' : ' (', 
		'endPunc' : ') ',
		'interPunc' : '; ',
		'multiCitationOrder' : 'F|T|V|E|S|FN|Ex|B|R',
		/* Figures */
		'F': {
			'prefix'			:	'',
			'singularString'	:	'Figure ',
			'pluralString'		:	'Figures ',
			'suffix'			:	'',
			'formatting'		:	'',
			'labelSuffix'		: 	'.',
			/* Figure Supplements */
			'S'					:	{
				'prefix'			:	'&mdash;',
				'singularString'	:	'figure supplement ',
				'pluralString'		:	'figure supplements ',
				'suffix'			:	'',
				'formatting'		:	'',
				'labelSuffix'		: 	'.',
			},
			/* Source Code */
			'SC': {
				'prefix'			:	'&mdash;',
				'singularString'	:	'source code ',
				'pluralString'		:	'source codes ',
				'suffix'			:	'',
				'formatting'		:	'',
				'labelSuffix'		: 	'.',
			},
			/* Source Data */
			'SD': {
				'prefix'			:	'&mdash;',
				'singularString'	:	'source data ',
				'pluralString'		:	'source datas ',
				'suffix'			:	'',
				'formatting'		:	'',
				'labelSuffix'		: 	'.',
			},
		},
		/* Reference */
		'R': {
			'prefix': '',
			'singularString': '',
			'pluralString': '',
			'suffix': '',
			'labelSuffix' : '',
			'formatting'  :	'',
			'citationType': '0',
			'sameasprevauthorpunc': ', ', //Intermediate punctuation if the citation same as previous default is comma
			'sameasprevauthoryearpunc': ',',
			'authorFormat': {
				'direct' : {
					'1': {'pattern': 'Surname|, (|Year|)'},
					'2': {'pattern': 'Surname| and |Surname|, (|Year|)'},
					'sameasprevauthor': {'pattern': 'Year'}, //If the citation author matched the previous not year
					'sameasprevauthoryear': {'pattern': 'alphaYear'}, //If the citation author and year matched the previous
					'replacesurname' : {'pattern': 'Surname| |Givenname'}, //If the citation surname is equal but given name is different
					'more': {'pattern': 'Surname| |et al|. (|Year|)'},
					'etalFormat': '',
				},
				'indirect' : {
					'1': {'pattern': 'Surname|, |Year'},
					'2': {'pattern': 'Surname| and |Surname|, |Year'},
					'sameasprevauthor': {'pattern': 'Year'}, //If the citation author matched the previous not year
					'sameasprevauthoryear': {'pattern': 'alphaYear'}, //If the citation author and year matched the previous
					'replacesurname' : {'pattern': 'Surname| |Givenname'}, //If the citation surname is equal but given name is different
					'more': {'pattern': 'Surname| |et al|., |Year'},
					'etalFormat': '',
				}
			},
		},
		'B': {
			'prefix'			:	'',
			'singularString'	:	'Box ',
			'pluralString'		:	'Boxes ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'		: 	'.',
		},
		/* Figure Supplement */
		'S': {
			'prefix'			:	'',
			'singularString'	:	'Figure supplement ',
			'pluralString'		:	'Figure supplements ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Source Code */
		'SC': {
			'prefix'			:	'',
			'singularString'	:	'Source Code ',
			'pluralString'		:	'Source Codes ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Source Data */
		'SD': {
			'prefix'			:	'',
			'singularString'	:	'Source Data ',
			'pluralString'		:	'Source Datas ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Supplementary Files */
		'SP': {
			'prefix'			:	'',
			'singularString'	:	'Supplementary file ',
			'pluralString'		:	'Supplementary files ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Table */
		'T': {
			'prefix'			:	'',
			'singularString'	:	'Table ',
			'pluralString'		:	'Tables ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Video */
		'V': {
			'prefix'			:	'',
			'singularString'	:	'Video ',
			'pluralString'		:	'Videos ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		/* Equation */
		'E': {
			'prefix'			:	'',
			'singularString'	:	'Equation ',
			'pluralString'		:	'Equations ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '.',
		},
		'FN': {
			'prefix'			:	'',
			'singularString'	:	'',
			'pluralString'		:	'',
			'suffix'			:	'',
			'formatting'        :	'sup',
			'labelSuffix'       :   '. ',
			'startPunc' : '',
			'endPunc' : '',
			'interPunc': ', ',
		},
		'EN': {
			'prefix'			:	'',
			'singularString'	:	'',
			'pluralString'		:	'',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '',
		},
		'EX': {
			'prefix'			:	'',
			'singularString'	:	'Example ',
			'pluralString'		:	'Examples ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'       :   '',
		},
		'AR' : {
			'prefix'			:	'',
			'singularString'	:	'',
			'pluralString'		:	'',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'		: 	'',
			'skipNumber'        :   'true',
			'T': {
				'prefix'			:	'',
				'singularString'	:	'table ',
				'pluralString'		:	'tables ',
				'suffix'			:	'',
				'formatting'        :	'',
				'labelSuffix'       :   '.',
			},
			'F': {
				'prefix'			:	'',
				'singularString'	:	'Author response image ',
				'pluralString'		:	'Author response images ',
				'suffix'			:	'',
				'formatting'        :	'',
				'labelSuffix'		: 	'.',
				/* Figure Supplements */
				'S'					:	{
					'prefix'			:	'&mdash;',
					'singularString'	:	'figure supplement ',
					'pluralString'		:	'figure supplements ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				},
				/* Source Code */
				'SC': {
					'prefix'			:	'&mdash;',
					'singularString'	:	'source code ',
					'pluralString'		:	'source codes ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				},
				/* Source Data */
				'SD': {
					'prefix'			:	'&mdash;',
					'singularString'	:	'source data ',
					'pluralString'		:	'source datas ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				}
			}
		},
		/* Appendix */
		'A' : {
			'prefix'			:	'',
			'singularString'	:	'Appendix ',
			'pluralString'		:	'Appendix ',
			'suffix'			:	'',
			'formatting'        :	'',
			'labelSuffix'		: 	'.',
			'T': {
				'prefix'			:	'&mdash;',
				'singularString'	:	'Table ',
				'pluralString'		:	'Tables ',
				'suffix'			:	'',
				'formatting'        :	'',
				'labelSuffix'       :   '.',
			},
			'F': {
				'prefix'			:	'&mdash;',
				'singularString'	:	'Figure ',
				'pluralString'		:	'Figures ',
				'suffix'			:	'',
				'formatting'        :	'',
				'labelSuffix'		: 	'.',
				/* Figure Supplements */
				'S'					:	{
					'prefix'			:	'&mdash;',
					'singularString'	:	'figure supplement ',
					'pluralString'		:	'figure supplements ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				},
				/* Source Code */
				'SC': {
					'prefix'			:	'&mdash;',
					'singularString'	:	'source code ',
					'pluralString'		:	'source codes ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				},
				/* Source Data */
				'SD': {
					'prefix'			:	'&mdash;',
					'singularString'	:	'source data ',
					'pluralString'		:	'source datas ',
					'suffix'			:	'',
					'formatting'        :	'',
					'labelSuffix'		: 	'.',
				}
			}
		}
	};
	//settings.citSeqHash = {};
	settings.citSeqHash = [];
	settings.floatsVisitedHash = {};
	settings.citNodePrefix	= '';
	
	//add or override the initial setting
	this.initialize = function(params){
		function extend(obj, ext){
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++){
				ext = args[i];
				for (name in ext){
					if (ext.hasOwnProperty(name)){
						value = ext[name];
						if (value !== "undefined"){
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		citeJS.settings.containerElm = (kriya.config && kriya.config.containerElm)?kriya.config.containerElm:'#contentDivNode';
		return this;
	};

	// function to get information from settings variable
	this.getSettings = function(){
		var newSettings = {};
		for (var prop in settings){
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

    return {
        init: initialize,
        getSettings: getSettings,
        settings: settings
    };
	
}();


/**
*    Extend citeJS to add renumbering of float citations
*/
(function (citeJS) {
	// get settings
	var settings = citeJS.settings;
	citeJS.floats = {
		/**
		* collect all citations in the main document
		* return jquery object of the citation nodes in the main document
		*/
		getDocumentCitations: function(citationClassName, rootElementID, citPrefix, includeFloats, includeFloatCitationsAfterBody){
			if (typeof(rootElementID) == 'undefined'){
				rootElementID = 'contentDivNode';
			}

			/**
			 * If the includeFloats is has float class then move the float objects next to the citation node and get the citaton 
			 * else get the citation without moving the floats
			 * Added by jagan
			 */
			if(includeFloats){
				//if includefloats is all then get the float class from config skip the jrnlSupplBlock and jrnlRefText
				//else get the float classes by splting includeFloats string
				if(includeFloats == "all"){
					var objKeys = Object.keys(citeJS.settings.citeClass);
					var floatClasses = [];
					for(o=0;o<objKeys.length;o++){
						if(objKeys[o] != "jrnlRefText" && objKeys[o] != "jrnlSupplBlock"){
							floatClasses.push(objKeys[o]);
						}
					}
				}else{
					var floatClasses = includeFloats.split(',');
				}
				
				for(f=0;f<floatClasses.length;f++){
					var fClass = floatClasses[f];
					
					//get the float blocks for the class and skip the deleted elements
					var floatBlocks = $('#' + rootElementID + ' .' + fClass).not('[data-track="del"],.del');
					floatBlocks.reverse();
					floatBlocks.each(function(){
						var citeID = $(this).attr('data-id');
						if(citeID){
							citeID = citeID.replace(/BLK_/g, '');
							//get the anchor citation node for the float
							var citeNode = $('#contentClone [data-citation-string*=" ' + citeID + ' "][data-anchor="true"]').not('[data-track="del"],.del');
							//if citation anchor node is not found then get the normal citation node
							citeNode = (citeNode.length > 0)?citeNode:$('#contentClone [data-citation-string=" ' + citeID + ' "]').not('[data-track="del"],.del');
							//Get the first citation
							citeNode = citeNode.first();

							//insert the float block next to the citation node
							//insert the flag element to identify the old position
							if(citeNode.length > 0){
								$('<span class="tmpObj" tmp-id="' + $(this).attr('data-id') + '"/>').insertAfter(this);
								$(this).insertAfter(citeNode);
							}
						}
					});

				}
				//get the citation nodes
				var citationNodes = $('#' + rootElementID + ' .' + citationClassName);

				//Move the block nodes to the old position and remove the flag element
				$('#' + rootElementID + ' span.tmpObj[tmp-id]').each(function(){
					var tmpId = $(this).attr('tmp-id');
					var floatBlock = $('#' + rootElementID + ' [data-id="' + tmpId + '"]').not('[data-track="del"],.del');
					if(floatBlock.length > 0){
						floatBlock.insertAfter(this);
						$(this).remove();
					}
				});

			}else if(includeFloatCitationsAfterBody){
				var objKeys = Object.keys(citeJS.settings.citeClass);
				for(o=0;o<objKeys.length;o++){
					if(objKeys[o] != "jrnlRefText" && objKeys[o] != "jrnlSupplBlock"){
						//get the float blocks for the class and skip the deleted elements
						var floatBlocks = $('#' + rootElementID + ' .' + objKeys[o]).not('[data-track="del"],.del');
						floatBlocks.each(function(){
							$('<span class="tmpObj" tmp-id="' + $(this).attr('data-id') + '"/>').insertAfter(this);
							$('#' + rootElementID).append(this);
						});
					}
				}

				//get the citation nodes
				var citationNodes = $('#' + rootElementID + ' .' + citationClassName);

				//Move the block nodes to the old position and remove the flag element
				$('#' + rootElementID + ' span.tmpObj[tmp-id]').each(function(){
					var tmpId = $(this).attr('tmp-id');
					var floatBlock = $('#' + rootElementID + ' [data-id="' + tmpId + '"]').not('[data-track="del"],.del');
					if(floatBlock.length > 0){
						floatBlock.insertAfter(this);
						$(this).remove();
					}
				});

			}else{
				var citationNodes = $('#' + rootElementID + ' .' + citationClassName);
			}
			
			
			//Check if the citation order is correct and check is te first citation is valid 
			var isOrdered = true;
			var citeFlag = 1;
			citationNodes.each(function(i, obj){
				var citeString = $(obj).attr('data-citation-string');
				if(citeString){
					citeString = citeString.replace(/^\s+|\s+$/g, '').split(/\s+/);
					$.each(citeString, function(i, val){
						var citeNum = val.replace(/[a-z]+/gi, '');
						if(citeNum > citeFlag && citeFlag == 1 && $(obj).attr('data-cite-type') != "insert" && citationNodes.filter('[data-first-valid="false"]').length < 1){
							$(obj).attr('data-first-valid', 'false');
						}else if(citeNum > citeFlag && $(obj).attr('data-cite-type') != "insert"){
							isOrdered = false;
							return false;
						}else if(citeNum == citeFlag){
							citeFlag++;
						}
					});
					if(isOrdered == false){
						return false;
					}
				}
			});

			citationNodes.each(function(i, obj){
				var ridString = $(obj).attr('data-citation-string');
				if (ridString == ""){
					return;
				}
				if (ridString == undefined){
					commonjs.general.updateCitationData($(obj), false);
					ridString = $(obj).attr('data-citation-string');
				}
				nodePrefix = citeJS.general.getPrefix(ridString);
				if(citPrefix && nodePrefix != citPrefix){
					citationNodes = citationNodes.not(obj);
				}
				//remove the deleted nodes
				if($(obj).closest('.del, [data-track="del"], .jrnlDeleted').length > 0){
					citationNodes = citationNodes.not(obj);
				}
				
				//remove the citations in author response if citation float is not in author response
				if($(obj).closest('.sub-article[data-article-type="reply"]').length > 0 && nodePrefix && !nodePrefix.match(/^AR/)){
					citationNodes = citationNodes.not(obj);
				}
				
				//If the first citation is not valid and order is true then don't reutnr return the citation node
				if($(obj).attr('data-first-valid') == "false" && isOrdered == true){
					$(obj).removeAttr('data-first-valid');
					citationNodes = citationNodes.not(obj);
				}else if($(obj).attr('data-first-valid') == "false"){
					$(obj).removeAttr('data-first-valid');
				}
			});
			return citationNodes;
		},
		addAnchorTag: function(citeId, rootElementID){
			var returnVal = false;
			var blockCiteNodes = $(rootElementID+' [data-citation-string*=" '+ citeId +' "]');
			var citeClass = blockCiteNodes.attr('class');
			if(citeClass == "jrnlBibRef" || citeClass == "jrnlSupplRef"){
				return false;
			}
			blockCiteNodes.filter('[data-anchor]').removeAttr('data-anchor');
			blockCiteNodes = blockCiteNodes.not('[data-track="del"],.del');

			blockCiteNodes = blockCiteNodes.filter(function(){
				if($(this).closest('[class^="jrnl"][class$="Block"]').length < 1 && $(this).closest('.jrnlDeleted').length == 0){
					return this;
				}
			});
			if(blockCiteNodes.length > 0){
				blockCiteNodes.first().attr('data-anchor', 'true');
				returnVal = blockCiteNodes.first();
			}
			return returnVal;
		},
		/**
		 * collect all citation of the given block Id
		 */
		getBlockCitations: function(citationClassName, blockId, rootElementID){
			if (typeof(rootElementID) == 'undefined'){
				rootElementID = 'contentDivNode';
			}
			if(citationClassName && blockId){
				var citationNodes = citeJS.floats.getDocumentCitations(citationClassName, rootElementID);
				blockId = blockId.replace(/BLK_/i,'');
				citationNodes.each(function(i, obj){
					var citeString = $(obj).attr('data-citation-string');
					if(citeString && $(obj).closest('.del,[data-track="del"]').length < 1){
						citeString = citeString.replace(/^\s+|\s+$/g, '').split(/\s+/);
						if($.inArray(blockId ,citeString) < 0){
							citationNodes = citationNodes.not(obj);
						}
					}else{
						citationNodes = citationNodes.not(obj);
					}
				});
			}
			return citationNodes;
		},
		collectCitationSeq: function(citationNodes, citationClassName, checkFloatBlock, ignoreSuppl){
			citationNodes.each(function(i, obj){

				//skip the deleted citations
				if($(this).closest('.del, [data-track="del"], .jrnlDeleted').length > 0){
					return;
				}

				var ridString  = $(obj).attr('data-citation-string');
				var parentNode = $(obj).closest('[class*="Block"]');
				
				if (ridString == "" || typeof ridString == "undefined"){
					return;
				}
				ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
				rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);
				// get unique elements of array
				rids = $.grep(rids, function(v, k){
					return $.inArray(v ,rids) === k;
				});
				// sort array - alpha-numeric
				//rids = citeJS.general.sortArrayAlphaNumeric(rids);
				$.each(rids, function(i, v){
					// check to see if the citation is not a supplementary node
					//these citations are the not the only citations present in the document, and are inside blocks, so should not be considered
					if ((parentNode.length > 0) && ($('#contentClone [data-citation-string*=" ' + v + ' "]').length > 1)){
						var count = 0;
						$('#contentClone [data-citation-string*=" ' + v + ' "]').each(function(){
							if ($(this).closest('[class*="Block"]').length > 0){
								count++;
							}
						});
						//to check if all the citations are inside flaot block nodes
						if (count != $('#contentClone [data-citation-string*=" ' + v + ' "]').length){
							//return true;
						}
					}
					
						var suppParts = v.split(/-/);

						//ignore the supplement reorder if main float citation present in the content
						//Create the details in hash array for supplement after creating the main float citation hash - jagan
						if(suppParts.length > 1 && $('#contentClone [data-citation-string*=" ' + suppParts[0] + ' "]').length != 0 && ignoreSuppl != false){
							return;
						}
						
						//Ignore float supplement - jai 12-10-2017
						//commented by jagan - ignore float supplement handled in createSeqHash function
						/*if (suppParts.length > 1){
							return true;
						}*/
						
						//If checkFloatBlock is true and citation node is with in block and more than one citation is present in article then break the loop
						var filteredCItations = $('#contentClone [data-citation-string*=" ' + v + ' "]').filter(function(){
							if($(this).closest('[data-id^="BLK_"]').length == 0 && $(this).closest('.del, [data-track="del"], .jrnlDeleted').length == 0){
								return this;
							}
							return false;
						});

						if(checkFloatBlock && $(obj).closest('[data-id^="BLK_"]').length > 0 && filteredCItations.length > 0){
							return;
						}
						
						// if checkFloatBlock is false (means we are looking at citations inside of floatblocks) and 
						// these citations are the only citations present in the document
						// then these has to be considered for renumbering, if not ignore them
						/*if (!checkFloatBlock){
							var blockCitNodes = $('#contentClone .' + citationClassName + ':not([class*="Block"] .' + citationClassName + ')[data-citation-string*=" ' + suppParts[0] + ' "]');
							if (blockCitNodes.length > 0){
								return true;
							}
						}*/

						var citeHash = citeJS.floats.createSeqHash(v);
						
						//Get the supplement citations for 
						var supplCitations = $('#contentClone [data-citation-string*=" ' + v + '-"]');
						if(supplCitations.length > 0 && ignoreSuppl != false){
							citeJS.floats.collectCitationSeq(supplCitations, citationClassName, settings.excludeCitationInFloats, false);
						}
						
						// collect all the current float type citation from the corresponding float caption and check if they are
						// present in sequence if its the only citation in the entire document
						/*blockCitationNodes = $('#contentClone [data-id="' + v +'"]').closest('[class*="Block"]').find('.' + citationClassName);
						if (checkFloatBlock && (blockCitationNodes.length > 0)){
							citeJS.floats.collectCitationSeq(blockCitationNodes, citationClassName, false);
						}*/
				});
			});
		},
		/**
		 * Function to create the citation sequence hash for the given Id
		 * @param id - ID of the float element
		 */
		createSeqHash : function(id){

			var suppParts = id.split(/-/);

			var prevHashSeq  = settings.citSeqHash;
			var prevCitePart = "";
			var citeConfig = citeJS.floats.getConfig(id); //Get the citation config
			for (var p = 0; p < suppParts.length; p++) {
				var citePart = suppParts[p];
				var citePartPrefix = citePart.replace(/[0-9\_]+/, '');
				var citePartNumber = citePart.replace(citePartPrefix, '');
				var chapNumber = "";
				if(citePartNumber.match(/\_/g)){
					chapNumber = citePartNumber.replace(/\_[0-9]+/, '');
					citePartNumber = citePartNumber.replace(/^[0-9]+\_/, '');
				}
				

				var prevFloatNum = (citePartNumber && citePartNumber > 1 && citePartPrefix)?citePartPrefix+(citePartNumber-1):'';
				
				settings.citSeqHash['prefix'][citePart] = citePartPrefix;
				if (!prevHashSeq[citePart]){
					prevHashSeq[citePart] = {};
					
					
					if(p == 0){
						if(citeConfig && citeConfig.renumber == "false"){
							//If config has renumber false and given id has number then change the number return existing number else return ridIndex
							var n = citePart;
							prevHashSeq[citePart].ridIndex = parseInt(citePartNumber);
						}else{
							prevHashSeq[citePart].ridIndex = settings.citSeqHash.ridIndex++;
							var n = citePartPrefix + ((chapNumber)?chapNumber + '_':'') + prevHashSeq[citePart].ridIndex;
						}
					}else if(prevFloatNum && prevHashSeq[prevFloatNum] && prevHashSeq[prevFloatNum].ridIndex){
						//For sub floats like figure, table supplementary file
						//If config has renumber false and given id has number then change the number return existing number else return ridIndex
						if(citeConfig && citeConfig.renumber == "false"){
							var n = citePart;
							prevHashSeq[citePart].ridIndex = parseInt(citePartNumber);
						}else{
							prevHashSeq[citePart].ridIndex = prevHashSeq[prevFloatNum].ridIndex;
							prevHashSeq[citePart].ridIndex++;
							var n = citePartPrefix + ((chapNumber)?chapNumber + '_':'') + prevHashSeq[citePart].ridIndex;
						}
					}else{
						if(citeConfig && citeConfig.renumber == "false"){
							var n = citePart;
							prevHashSeq[citePart].ridIndex = parseInt(citePartNumber);
						}else{
							prevHashSeq[citePart].ridIndex = 1;
							var n = citePartPrefix + ((chapNumber)?chapNumber + '_':'') + prevHashSeq[citePart].ridIndex;
						}
					}
					
					//var n = citePartPrefix + ((citeConfig && citeConfig.renumber == "false" && citePartNumber)?citePartNumber:prevHashSeq[citePart].ridIndex);					
					prevHashSeq[citePart].newVal = n;

					if (citePart != n){
						settings.citSeqHash['floats'][prevCitePart+citePart] = (prevHashSeq && prevHashSeq.newVal)?prevHashSeq.newVal + '-' +n:n;
						citSeqObject.float = false;
						settings.citSeqHash['prefix'][n] = citePartPrefix;
					}
				}
				prevCitePart = prevCitePart+citePart+ "-";

				prevHashSeq = prevHashSeq[citePart];
			}
			return settings.citSeqHash;
		},
		checkSequence: function(classNameToCheckSeq) {
			//initialize some default values
			settings.citSeqHash = {};
			settings.citSeqHash['prefix'] = {}; // to store the prefix string of individual float ids
			settings.citSeqHash['floats'] = {}; // to store ids of those floats which are not in sequence
			settings.citSeqHash.ridIndex = 1;
			citSeqObject = {'float': true, 'suppl': true, 'citationClassName': ''};

			$('#contentClone').remove();
			$('body').append('<div id="contentClone" class="updatedContent" style="display: none !important;">' + $(kriya.config.containerElm).html() + '</div>');

			if (typeof(classNameToCheckSeq) != 'undefined'){
				citationClassName = classNameToCheckSeq;
				citSeqObject.citationClassName = citationClassName;
				settings.citSeqHash['citationClassName'] = citationClassName;
				settings.citSeqHash[citationClassName] = {};

				var citID = $(citeJS.settings.containerElm + ' .' + citationClassName).first().attr('data-citation-string');
				settings.citNodePrefix = citeJS.general.getPrefix(citID);

			}else if ($(citeJS.settings.containerElm + ' [data-cite-type]').length > 0){
				// find the citation type using the attribute 'data-cite-type'
				// the value will be either 'insert', 'update' or 'delete'

				$(citeJS.settings.containerElm + ' .modifiedCitation').removeClass('modifiedCitation');
				var citationClassName = $(citeJS.settings.containerElm + ' [data-cite-type]').first().attr('class').split(' ')[0];
				// get the prefix of the cited node
				var citID = $(citeJS.settings.containerElm + ' [data-cite-type]').first().attr('data-citation-string');
				settings.citNodePrefix = citeJS.general.getPrefix(citID);

				$(citeJS.settings.containerElm + ' [data-citation-string]:empty').remove();
			
				citationClassName = citationClassName.replace(/^.*(jrnl[A-Za-z]{3,4}Ref).*?$/, '$1');	
				// if we are not able to fetch the citationClassName from the attribute and 
				// if the class name is given as input to the function use it
				if (! citationClassName && (typeof(classNameToCheckSeq) != 'undefined')){
					citationClassName = classNameToCheckSeq;
				}
				if (citationClassName){
					citSeqObject.citationClassName = citationClassName;
					settings.citSeqHash['citationClassName'] = citationClassName;
					settings.citSeqHash[citationClassName] = {};
				}else{
					return false;
				}
			}else{
				return false;
			}

			if(citationClassName){
				var objKeys = Object.keys(settings.citeClass);
				var objValue = Object.values(settings.citeClass);
				var classIndex = objValue.indexOf(citationClassName);
				if(classIndex >= 0){
					settings.citSeqHash.floatClassName = objKeys[classIndex];
				}
			}

			var citationNodes = citeJS.floats.getDocumentCitations(citationClassName, 'contentClone', settings.citNodePrefix, settings.includeFloats, settings.includeFloatCitationsAfterBody);
			
			citeJS.floats.collectCitationSeq(citationNodes, citationClassName, settings.excludeCitationInFloats);
			$('#contentClone').remove();
			if(citeJS.settings.citSeqHash.floats && Object.keys(citeJS.settings.citSeqHash.floats).length > 0){
				return $.extend(true, {}, citeJS.settings.citSeqHash.floats);
			}else{
				return false;
			}
		},
		/*
		 * Function to renumber the citation and reorder the floats
		 * return - Array [0] -> renumberd citation ids | [1] -> renmberd float Ids | [2] -> Details renumbering
		 */
		reorder: function(){
			$('#contentClone').remove();
			//Clone the content from the article
			$('body').append('<div id="contentClone" class="updatedContent" style="displa1y: none !important;">' + $(kriya.config.containerElm).html() + '</div>');
			$('#contentClone .modifiedCitation').removeClass('modifiedCitation');
			var citeIDs  = citeJS.floats.renumberCitations();
			var floatIds = citeJS.floats.reorderFloats();

			citeJS.floats.updateRightPanel(); //Update the right navigation panel
			//reset the cloned content in maib article
			$(kriya.config.containerElm).html($('#contentClone').html());
			$('#contentClone').remove();

			/*setTimeout(function(){
				//Recreate the cloned content after the citation reorder
				createCloneContent();
			},2000);*/
			var status = '';
			return [citeIDs, floatIds, status];
		},
		renumberConfirmation: function(action){
			var statusText = '';
			var returnVal = [];
			var citeStatus = this.renumberStatus();
			
			/*if(citeStatus && citeStatus['insert']){
				for(var citeClass in citeStatus['insert']){
					var citeModObj = citeStatus['insert'][citeClass];
					for(var k in citeModObj){
						var citeNode = citeModObj[k];
						if($(citeNode).length > 0){
							var citeText = $(citeNode).clone(true).cleanTrackChanges().html();
							if($(citeNode).hasClass('jrnlBibRef')){
								statusText += '<span class="renumberStatus">A reference citation for ' + citeText + ' was inserted. </span><br />';
							}else{
								statusText += '<span class="renumberStatus">A citation for ' + citeText + ' was inserted. </span><br />';
							}
						}
					}
					if(returnVal.indexOf(citeClass) < 0){
						returnVal.push(citeClass);
					}
				}
			}

			if(citeStatus && citeStatus['delete']){
				for(var citeClass in citeStatus['delete']){
					var citeModObj = citeStatus['delete'][citeClass];
					for(var k in citeModObj){
						var citeNode = citeModObj[k];
						if($(citeNode).length > 0){
							var citeText = $(citeNode).clone(true).cleanTrackChanges().html();
							if($(citeNode).hasClass('jrnlBibRef')){
								statusText += '<span class="renumberStatus">A reference citation for ' + citeText + ' was deleted. </span><br />';
							}else{
								statusText += '<span class="renumberStatus">A Citation for ' + citeText + ' was deleted. </span><br />';
							}
						}
					}
					if(returnVal.indexOf(citeClass) < 0){
						returnVal.push(citeClass);
					}
				}
			}*/

			if(citeStatus && citeStatus['error']){
				for(var citeClass in citeStatus['error']){
					var citeModObj = citeStatus['error'][citeClass];
					for(var k in citeModObj){
						var citeNode = citeModObj[k];
						if($(citeNode).length > 0){
							var citeText = $(citeNode).clone(true).cleanTrackChanges().html();
							var xpath = kriya.getElementTreeXPath($(citeNode)[0]);
							if($(citeNode).hasClass('jrnlBibRef')){
								statusText += '<span class="renumberStatus" style="cursor:pointer;" data-rid="" data-node-xpath=\'' + xpath + '\' data-message="{\'click\':{\'funcToCall\': \'scrollElementOnContent\',\'channel\':\'menu\',\'topic\':\'navigation\'}}">A reference citation <b>' + citeText + '</b> is invalid. </span><br />';
							}else{
								statusText += '<span class="renumberStatus" style="cursor:pointer;" data-rid="" data-node-xpath=\'' + xpath + '\' data-message="{\'click\':{\'funcToCall\': \'scrollElementOnContent\',\'channel\':\'menu\',\'topic\':\'navigation\'}}">A Citation <b>' + citeText + '</b> is invalid. </span><br />';
							}
						}
					}
				}

				if(Object.keys(citeStatus['error']).length > 0){
					statusText += 'Please contact support to fix this issue or delete the invalid citation.';
					var confirmationContent = '<div class="row" style="max-height: 20rem;">' + statusText + '</div>';
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						content : confirmationContent,
						icon : 'icon-warning'
					});
					return false;
				}
			}

			if(citeStatus && citeStatus['status']){
				for(var citeClass in citeStatus['status']){
					var citeSeqStatus = citeStatus['status'][citeClass];
					for(var k in citeSeqStatus){
						if(citeClass == "jrnlBibRef"){
							if (settings['R'].citationType == '0'){
								statusText += '<span class="renumberStatus">The reference ' + k + ' will be changed to ' + citeSeqStatus[k] + ' </span><br />';	
							}
						}else if(citeClass == "jrnlFootNoteRef"){
							statusText += '<span class="renumberStatus">The footnote ' + k + ' will be changed to ' + citeSeqStatus[k] + ' </span><br />';	
						}else{
							statusText += '<span class="renumberStatus">' + citeJS.floats.getCitationHTML([k], [], 'getNewCitation') + ' will be changed to '+ citeJS.floats.getCitationHTML([citeSeqStatus[k]], [], '') +' </span><br />';
						}
					}
					if(returnVal.indexOf(citeClass) < 0){
						returnVal.push(citeClass);
					}
				}
			}


			if(returnVal.length > 0){
				var renumberClass = returnVal.join();
				var confirmationContent = '<div class="row" style="max-height: 20rem;">' + statusText + '</div><div class="row" style="margin-top: 1rem !important;"><input type="checkbox" class="filled-in" data-type="checkBoxComponent" id="showMsg" data-message="{\'change\':{\'funcToCall\': \'changeMsgStatus\',\'channel\':\'components\',\'topic\':\'citation\'}}"><label for="showMsg" style="color: white;">Don\'t show this message again.</label></div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-message="{\'click\':{\'funcToCall\': \'citationConfirmation\',\'param\' : {\'renumberClass\' : \'' + renumberClass + '\',\'action\':\'' + action +'\' },\'channel\':\'components\',\'topic\':\'citation\'}}">Reorder</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
				kriya.notification({
					title : 'Our system bases Figure/Table/Video/Supplementary file numbering on their first citations in text. The edit made has altered this order, so the numbering will be altered as described below. To keep the old numbering, please modify the order of the first citations in the text as needed.',
					type  : 'success',
					content : confirmationContent,
					icon : 'icon-info',
					closeIcon : false
				});
				return false;
			}else{
				/*kriya.notification({
					title : 'Citation Information',
					type  : 'success',
					content : 'Citations are ordered in sequence',
					icon : 'icon-warning2',
					timeout : 5000
				});*/
				return true;
			}
		},
		renumberStatus: function() {
			var returnVal  = {
				'insert' : {},
				'delete' : {},
				'status' : {},
				'error'  : {}
			};
			//Check the citation sequence
			var citeRefArray = Object.values(settings.citeClass);
			for(c=0;c<citeRefArray.length;c++){
				var citeClass = citeRefArray[c];
				if (citeClass == 'jrnlSupplRef') continue;
				//if (citeClass == 'jrnlFootNoteRef') continue;
				var citeSeqStatus = citeJS.floats.checkSequence(citeClass);
				if(citeSeqStatus){
					if(citeClass == "jrnlBibRef"){
						if (settings['R'].citationType == '0'){
							returnVal['status'][citeClass] = citeSeqStatus;
						}
					}else{
						returnVal['status'][citeClass] = citeSeqStatus;
					}
				}

				//Check the inserted or deleted citation
				if($(citeJS.settings.containerElm).find('.'+citeClass+'[data-cite-type]').length > 0){
					$(citeJS.settings.containerElm).find('.'+citeClass+'[data-cite-type]').each(function(){
						var citeType = $(this).attr('data-cite-type');
						var ridString = $(this).attr('data-citation-string');
						if(typeof(returnVal[citeType]) == "object" && ridString){
							if(returnVal[citeType][citeClass]){
								returnVal[citeType][citeClass].push(this);
							}else{
								returnVal[citeType][citeClass] = [this];
							}
						}
					});
				}

				//Get the invalid citation nodes
				$(citeJS.settings.containerElm).find('.'+citeClass).each(function(){
					if($(this).closest('.del, [data-track="del"], .jrnlDeleted').length > 0){
						return;
					}

					var citeType = $(this).attr('data-cite-type');
					var ridString = $(this).attr('data-citation-string');
					ridString = ridString.replace(/^\s+|\s+$/g, '');
					if(!ridString){
						if(returnVal['error'][citeClass]){
							returnVal['error'][citeClass].push(this);
						}else{
							returnVal['error'][citeClass] = [this];
						}
					}
				});
			}

			if(Object.keys(returnVal['status']).length == 0 && Object.keys(returnVal['insert']).length > 0){
				$.each(returnVal['insert'], function(citeClass, obj){
					if(citeClass != "jrnlBibRef" && citeClass != "jrnlFootNoteRef" && citeClass != "jrnlSupplRef"){
						$(obj).each(function(i, item){
							var dataCiteString = $(this).attr('data-citation-string');
							dataCiteString = dataCiteString.replace(/^\s+|\s+$/g, '').split(/\s+/);
							$.each(dataCiteString, function(i, rid){
								var blockNode = $(citeJS.settings.containerElm).find('[data-id="' + rid + '"]').closest('[data-id^="BLK_"]');
								if(blockNode.length > 0){
									var prevNode = blockNode.prev();
									if(prevNode.find(obj).length != 0){
										returnVal['insert'][citeClass].pop(item);
									}
								}
							});
						});
						if(returnVal['insert'][citeClass].length == 0){
							delete returnVal['insert'][citeClass];
						}
					}else if(citeClass == "jrnlBibRef" && settings['R'].citationType == "1"){
						//Remove the inserted object if the bibref citation is author year
						delete returnVal['insert'][citeClass];
					}
				});
			}

			if(Object.keys(returnVal['status']).length > 0 || Object.keys(returnVal['insert']).length > 0 || Object.keys(returnVal['delete']).length > 0 || Object.keys(returnVal['error']).length > 0){
				return returnVal;	
			}else{
				return false;
			}
		},
		renumberStatus_old: function(){
			var returnVal = '';
			var confirmContent = $('<table class="reorderList"><tbody></tbody></table>');
			var citSeqHash     = settings.citSeqHash;
			var citationClassName = settings.citSeqHash.citationClassName;
			if ($('#contentClone').length == 0){
				$('body').append('<div id="contentClone" class="updatedContent" style="display: none !important;">' + $(kriya.config.containerElm).html() + '</div>');
			}
			$('#contentClone .modifiedCitation').removeClass('modifiedCitation');
			var citationNodes = citeJS.floats.getDocumentCitations(citationClassName, 'contentClone', settings.citNodePrefix);
			
			/*   start -  To get the uncited floating Blocks 	*/
			$('#contentClone [data-uncited="true"]').each(function(i,obj) {
				var captionNode = $(this).find('[class^=jrnl][class*=Caption]').first();
				if(captionNode.length > 0){
					var floatId = $(captionNode).attr('data-id');
					var label   = $(captionNode).find('.label').clone(true);
					
					//label.find('.del').remove();
					//label.find('.ins').contents().unwrap();
					label.cleanTrackChanges();

					label = label.text();
					label = label.replace(/([\s\.\,\|]+)$/, '');
				}else{
					var floatId = $(obj).attr('data-id');
					var label   = 'Reference '+floatId.match(/([\d]+)/)[0];
				}
				
				var renumbertext = '<span class="renumberStatus floatUnCited">The ' + label + '  has no citation. </span>';
				returnVal = returnVal+renumbertext+'<br>';
				confirmContent.find('tbody').append('<tr><td>' + renumbertext + '</td></tr>');
			});
			/* end -  To get the uncited floating Blocks */

			var duplicates = [];
			citationNodes.each(function(i, obj){
				var ridString = $(obj).attr('data-citation-string');
				if (ridString == ""){
					return;
				}
				ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
				rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);
				// get unique elements of array
				rids = $.grep(rids, function(v, k){
					return $.inArray(v ,rids) === k;
				}); 
				rids = citeJS.general.sortArrayAlphaNumeric(rids);
				var dirty = false;
				$.each(rids, function(i, v){
					if(duplicates.indexOf(v) < 0){
						duplicates.push(v);
						var suppParts = v.split(/-/);
						var prevHashSeq = settings.citSeqHash;
						var newVAlArray = [];
						for (var p = 0; p < suppParts.length; p++) {
							var citePart = suppParts[p];
							if (prevHashSeq && prevHashSeq[citePart]){
								if (v != prevHashSeq[citePart].newVal){
									dirty = true;
								}
								newVAlArray.push(prevHashSeq[citePart].newVal);
								prevHashSeq = prevHashSeq[citePart];
							}
						}
						var newId = newVAlArray.join("-");
						if(newId != "" && newId != v){
							var refCite = '';
							if(settings.citSeqHash.citationClassName == "jrnlBibRef"){
								if (settings['R'].citationType == '0'){
									var renumbertext = '<span class="renumberStatus">The Reference ' + citeJS.floats.getCitationHTML([v], [], '') + '<b> ( ' + citeJS.floats.getReferenceHTML(newId, obj,true) +' )</b>  was changed as '+ citeJS.floats.getCitationHTML([newId], [], '') +' </span>';
									returnVal = returnVal+renumbertext+'<br>';
									confirmContent.find('tbody').append('<tr><td>' + renumbertext + '</td></tr>');
								}
							}else{
								var renumbertext = '<span class="renumberStatus">' + citeJS.floats.getCitationHTML([v], [], '') + ' was changed as '+ citeJS.floats.getCitationHTML([newId], [], '') +' </span>';
								returnVal = returnVal+renumbertext+'<br>';
								confirmContent.find('tbody').append('<tr><td>' + renumbertext + '</td></tr>');
							}	
							dirty = true;
						}
					}
				});
			});
			//Disaply the renumber details in popup
			var confirmationSettings = {
				'icon'  : '<i class="material-icons">info</i>',
				'title' : 'Renumbered Citation Details',
				'text'  : confirmContent[0].outerHTML,
				'size'  : 'pop-md',
				'buttons' : false
 			}
 			$('#contentClone').remove();
 			if(confirmContent.find('tr').length > 0){
 				kriya.actions.confirmation(confirmationSettings);
 				return returnVal;
 			}else{
 				return false;
 			}
		},
		renumberCitations: function(){
			var returnArray = []; // Array contains the Ids of the renumered citation
			var citSeqHash  = settings.citSeqHash;
			var citationClassName = settings.citSeqHash.citationClassName;
			var citationNodes = citeJS.floats.getDocumentCitations(citationClassName, 'contentClone', settings.citNodePrefix);
			// loop through the citation nodes
			citationNodes.each(function(i, obj){
				var ridString = $(obj).attr('data-citation-string');
				if (ridString == ""){
					//$(obj).remove();
					return;
				}
				ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
				rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);
				// get unique elements of array
				rids = $.grep(rids, function(v, k){
					return $.inArray(v ,rids) === k;
				});

				// sort array - alpha-numeric
				//rids = citeJS.general.sortArrayAlphaNumeric(rids);

				var newCitIDArray = []; var dirty = false;
				$.each(rids, function(i, v){
					var suppParts = v.split(/-/);

					var prevHashSeq = settings.citSeqHash;
					var newVAlArray = [];
					for (var p = 0; p < suppParts.length; p++) {
						var citePart = suppParts[p];
						if(prevHashSeq){
							if (prevHashSeq[citePart]){
								if (v != prevHashSeq[citePart].newVal){
									dirty = true;
								}
								newVAlArray.push(prevHashSeq[citePart].newVal);
							}
							prevHashSeq = prevHashSeq[citePart];
						}
					}
					newCitIDArray.push(newVAlArray.join("-"));
				});
				//end of foreach nodes
				//console.log(dirty + $(obj).attr('data-citation-string') + $(obj)[0].hasAttribute('data-cite-type'));
				if (dirty || $(obj)[0].hasAttribute('data-cite-type')){
					var oldObj = $(obj).clone();
					$(oldObj).find('.ins').each(function(){
						$(this).contents().unwrap();
					});
					$(oldObj).find('.del').each(function(){
						$(this).remove();
					});
					var oldCitString = $(oldObj).text(); 
					oldCitString = oldCitString.replace(/^[\s\t\r\n\;\,\.\,\(\[]+|[\s\t\r\n\;\,\.\,\)\]]+$/gi, '');
					oldCitString = oldCitString.replace(/\b(\s*and\s*|\s*&\s*)\b/gi, ' ');
					oldCitString = oldCitString.replace(/(Fig[ures\.]*\s*[0-9]+\s*[\u2010\u2013\u2014\-]\s*(Fig[ures\.]*\sSuppl[mentarys\.]*|Source Codes?|Source Data)\s)/gi, '');
					oldCitString = oldCitString.replace(/(\s*[0-9]+\s*[\u2010\u2013\u2014\-]\s*(Source Codes?|Source Data)\s)/gi, '');
					oldCitString = oldCitString.replace(/(Fig[ures\.]*|Tab[les\.]*|Tbl\.?|Videos?|Suppl[mentarys\.]*|Sch[mase\.]*|Images?|Img\.?|Box|Files?)\s*/gi, ''); //|[\-\u2010\u2013\u2014]+
					oldCitString = oldCitString.replace(/(Appendix*\s*[0-9]+\s*[\u2010\u2013\u2014\-]\s*)/gi, '');
					oldCitString = oldCitString.replace(/(Eq[uations\.]*|Eqn*)\s*/gi, '');
					//console.log('ridString: ' + ridString + '; text: ' + $(obj).text() + '; partsString: ' + oldCitString);
					newCitIDArray = newCitIDArray.sort(citeJS.general.naturalSort);
					if(citationClassName == "jrnlBibRef") {
						if (settings['R'].citationType == '1'){
							var citeText = $(obj).text();
							var isDirect = false;	
							if(citeText.match(/\(\d{4}[a-z]?\)/)){
								isDirect = true;
							}

							//Get the newcitation string if the prev id has data-cite-string
							var prevCiteNdde = false;
							if (obj && obj.previousSibling){
								var prevNode = obj.previousSibling;
								if (prevNode && (prevNode.nodeName == '#text' || prevNode.className == "del") && (/^[\,\s\.\;\[\]\(\)\u00A0]+$/i.test(((prevNode.nodeValue)?prevNode.nodeValue:prevNode.innerHTML)))) {
									prevNode = prevNode.previousSibling;
								}
								if (prevNode && ((prevNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevNode.getAttribute('class')))){
									prevCiteNdde = prevNode;
								}
							}

							newCitIDArray = $(obj).attr('data-citation-string').replace(/^\s+|\s+$/g, '').split(' ');
							var newCitString = false;
							if ($(this).attr('data-cite-type') != undefined || (prevCiteNdde && $(prevCiteNdde).attr('data-cite-type') != undefined)){
								var newCitString = citeJS.floats.getReferenceHTML(newCitIDArray.join(' '), obj, '', isDirect);
							}
						}else{
							var newCitString = citeJS.floats.getReferenceHTML(newCitIDArray.join(' '));
						}
					}else{
						var citeConfig = citeJS.floats.getConfig(newCitIDArray[0]);
						if(nodeIsFirstInSentence(obj) && citeConfig && (citeConfig.sentenceStartSingular || citeConfig.sentenceStartPlural)){
							var newCitString = citeJS.floats.getCitationHTML(newCitIDArray, oldCitString, 'renumberCitations', true);
						}else{
							var newCitString = citeJS.floats.getCitationHTML(newCitIDArray, oldCitString, 'renumberCitations');
						}
						
					}
					
					if (!newCitString){
						newCitString = $(obj).html();
					}
					if(newCitString != $(obj).html()){
						$(obj).attr('data-citation-string', ' ' + newCitIDArray.join(' ') + ' ');
						
						var clonedQueries = $(obj).find('.jrnlQueryRef').clone();
						$(obj).html(newCitString);
						$(obj).prepend(clonedQueries);

						if (($(obj).find('sup').length > 0) && ($(obj).closest('sup').length > 0)){
							$(obj).closest('sup').contents().unwrap();
						}

						var citeId = $(obj).closest('[id][id!=""]').attr('id');
						returnArray.push(citeId);
					}
				}
			});
			return returnArray;
		},
		/**
		 * move the float blocks close to citations
		 */
		reorderFloats: function(){
			var returnArray = []; // Array contains the Ids of the reordered floats
			var citationsSeqHash   = {};
			var supplDetailsHash   = [];
			var unsupplDetailsHash = {};
			var floatIDs = new Array();
			if ($('#contentClone').length == 0){
				$('body').append('<div id="contentClone" class="updatedContent" style="display: none !important;">' + $(kriya.config.containerElm).html() + '</div>');
			}
			$('#contentClone').find('.activeElement').removeClass('activeElement');
			var citationClassName = settings.citSeqHash.citationClassName;

			if(citationClassName != "jrnlBibRef" && citationClassName != "jrnlFootNoteRef" && citationClassName != "jrnlSupplRef"){
				/* Check citations before the inserted citation inside the Para  - Maduravalli */
				var beforeNodes = $('#contentClone [data-cite-type="insert"]').first().prevUntil("p.jrnlSecPara");
				beforeNodes.each(function(i, obj){
					if($(obj)[0].hasAttribute("data-citation-string"))	{
						var ridString = $(obj).attr('data-citation-string');	
						var parentNode = $(obj).closest('[class*="Block"]');
						var ridString = $(obj).attr('data-citation-string');
						ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
						ridString = ridString.replace(/^\s+|\s+$/g, '');
						rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);
						if (parentNode.length == 0){
							parentNode 	= $(obj).closest('p,h1,h2,h3,h4,h5,h6');
						}else if ($('#contentClone [data-citation-string="' + ridString + '"]').length > 0){
							return;
						}
						if(!citationsSeqHash[ridString]){
							citationsSeqHash[ridString] = parentNode;
							floatIDs.push(ridString);
						}
						// check supplementary citations
						var beforeSuppNodes = $('#contentClone span[data-citation-string*="'+ ridString +'"]');	
						beforeSuppNodes.each(function(k, objsup){
							var parentNode = $(objsup).closest('[class*="Block"]');
							var ridStr = $(objsup).attr('data-citation-string');
							ridStr = ridStr.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
							ridStr = ridStr.replace(/^\s+|\s+$/g, '');					
							rids = ridStr.replace(/^\s+|\s+$/g, '').split(/\s+/);
							if (parentNode.length == 0){
								parentNode 	= $(objsup).closest('p,h1,h2,h3,h4,h5,h6');
							}else if ($('#contentClone [data-citation-string="' + ridString + '"]').length > 0){
								return;
							}
							$.each(rids, function(r, v){
								if (/\-/.test(v)){
									var floatPart = v.replace(/^([A-Z0-9]+)(\-.*)$/, '$1');
									var supplPart = v.replace(/^([A-Z0-9]+)\-(.*)$/, '$2');
									/*if (supplDetailsHash[floatPart]){
										supplDetailsHash[floatPart] = supplDetailsHash[floatPart] + '|' + supplPart;
									}else{
										supplDetailsHash[floatPart] = supplPart;
									}*/
								}
								else if (v && !citationsSeqHash[v]){
									citationsSeqHash[v] = parentNode;
									floatIDs.push(v);
								}	
							});
						});
					}
				});	
				/* End */
	
				//Commented by jagan - figure supplement block class name may be jrnlSupplBlock so get all block nodes and filter based on node prefix 
				/*if(settings.citSeqHash.floatClassName){
					floatBlocks = $('#contentClone .'+settings.citSeqHash.floatClassName).not('[data-track="del"],.del');
				}else{
					floatBlocks = $('#contentClone *[class^="jrnl"][class*="Block"],#contentClone *[class^="jrnlEqnPara"]').not('[data-track="del"],.del');	
				}*/
				floatBlocks = $('#contentClone *[class^="jrnl"][class*="Block"],#contentClone *[class^="jrnlEqnPara"]').not('[data-track="del"],.del');

				$(floatBlocks).removeAttr('data-uncited');
				//remove blocks which do not match 
				var citationClassName = settings.citSeqHash.citationClassName;
				floatBlocks.each(function(i, obj){
					var captionNode = $(this).find('[class^=jrnl][class*=Caption]').first();
					if(captionNode.length > 0){
						var id = $(captionNode).attr('data-id');
					}else{
						var id = $(obj).attr('data-id');
					}

					//If block doesn't have id then remove block from block list and continue the loop
					if (typeof(id) == 'undefined'){
						floatBlocks = floatBlocks.not(obj);
						return;
					} 
					nodePrefix = citeJS.general.getPrefix(id);
					if(settings.citNodePrefix && nodePrefix != settings.citNodePrefix){
						floatBlocks = floatBlocks.not(obj);
					}else if(!settings.citSeqHash[id]){
						//Set attribute uncited true
						$(obj).attr('data-uncited','true');
						var citeHash = citeJS.floats.createSeqHash(id);
						//floatBlocks = floatBlocks.not(obj);
					}
				});

				// change all floats to new ids
				settings.citSeqHash.float_nodes = floatBlocks;
				floatBlocks.each(function(i, obj){
					var blockNode = obj;
					var blockNodeClass = $(blockNode).attr('class');
					//if (settings[blockNodeClass] != citationClassName) return;
					var captionNode = $(this).find('[class^=jrnl][class*=Caption]');
					if(captionNode.length > 0){
						var id = $(captionNode).attr('data-id');
					}else{
						var id = $(obj).attr('data-id');
					}

					if (typeof(id) == 'undefined') return;
					var suppParts = id.split(/-/);
					var prevHashSeq = settings.citSeqHash;
					var newIDArray = [];
					for (var p = 0; p < suppParts.length; p++) {
						var citePart = suppParts[p];
						if (prevHashSeq && prevHashSeq[citePart]){
							newIDArray.push(prevHashSeq[citePart].newVal);
						}
						prevHashSeq = prevHashSeq[citePart];
					}
					var newID = newIDArray.join("-");

					if (settings.citSeqHash.floats[id] || (settings.citSeqHash[suppParts[0]] && settings.citSeqHash[suppParts[0]].newVal)){
						// If the id is not same to the newId
						if (id != newID){
							var newFloatLabelString = citeJS.floats.getCitationHTML([newID], [], 'renumberFloats');
							//If caption node is found then change the new id and label
							//Else update the new id for the block node and update the equation label
							if(captionNode.length > 0){
								$(blockNode).attr('data-id', 'BLK_' + newID);
								$(blockNode).attr('data-stream-name', 'a_' + newID);
								$(captionNode).attr('data-id', newID);
								$(captionNode).attr('data-old-id', id);

								//Update the uncited float query id
								$('.query-div[data-query-action="uncited-reference"][data-float-id="' + id + '"]').attr('data-new-float-id', newID);

								if($(captionNode).find('.label').length == 0){
									$(captionNode).first().prepend('<span class="label"/>');
								}
								$(captionNode).find('.label:first').html(newFloatLabelString);
							}else{
								$(blockNode).attr('data-id', newID);
								$(blockNode).attr('data-old-id', id);
								newFloatLabelString = newFloatLabelString.match(/(\d+)/)[0];
								$(blockNode).find('.jrnlEqnLabel').html('('+newFloatLabelString+')');
							}

							var blockId = $(blockNode).closest('[id]').attr('id');
							returnArray.push(blockId);
						}
					}
				});

				//Update the new float id to float id and remove new attribute
				$('.query-div[data-query-action="uncited-reference"][data-new-float-id]').each(function(){
					var newId = $(this).attr('data-new-float-id');
					var queryID = $(this).attr('id');
					$(this).attr('data-float-id', newId);
					$(this).removeAttr('data-new-float-id');

					if($.inArray(queryID ,returnArray) < 0){
						returnArray.push(queryID);
					}
				});

				// collect all citation nodes
				//citationNodes = $('#contentClone .body,#contentClone .back').find('*:not(.del) > *:not([class="jrnlBibRef"]):not([class="jrnlPossBibRef"]):not([class="jrnlAffRef"])[class^="jrnl"][class$="Ref"]');
				citationNodes = citeJS.floats.getDocumentCitations(citationClassName, 'contentClone', settings.citNodePrefix);
				citationNodes.each(function(i, obj){
					var parentNode = $(obj).closest('[class*="Block"]');
					var ridString = $(obj).attr('data-citation-string');
					if (parentNode.length == 0){
						parentNode = $(obj).closest('p,h1,h2,h3,h4,h5,h6');
					}
					
					ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
					rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);

					$.each(rids, function(i, v){

						//Get the previous citation node of citeNode in parentNode
						var prevCiteId = null;
						var parentCiteNodes = parentNode.find(kriya.config.citationSelector).not('.jrnlBibRef,.jrnlFootNoteRef,.jrnlSupplRef, [data-track="del"], .del');
						if(parentCiteNodes.length > 0){
							parentCiteNodes.each(function(i){
								i = parentCiteNodes.index(this);
								var citeString = $(this).attr('data-citation-string');
								citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
								
								if(this == obj){
									if(i != 0 && parentCiteNodes.eq(i-1).length > 0 && obj != parentCiteNodes.eq(i-1)[0]){
										var prevCiteNode = parentCiteNodes.eq(i-1);
										prevCiteId = $(prevCiteNode).attr('data-citation-string');
										prevCiteId = prevCiteId.replace(/^\s+|\s+$/g, '').split(' ').pop(); //get the last cite id
									}
									if(citeString.length > 1 && citeString.indexOf(v) > 0){
										var index = citeString.indexOf(v);
										prevCiteId = citeString[index-1];
									}
									return false;
								}else{
									for(var c=0;c<citeString.length;c++){
										var firstNode = $('#contentClone [data-citation-string*="' + citeString[c] + '"]:not([data-track="del"], .del):first');
										if(firstNode.length > 0 && firstNode[0] != this){
											parentCiteNodes = parentCiteNodes.not(this);
										}else if(citeString[c].match(/-/g)){
											parentCiteNodes = parentCiteNodes.not(this);
										}
									}
								}
							});
						}

						//Get the float block node for the prevCiteId
						if(prevCiteId && $('#contentClone [data-id="' + prevCiteId + '"]').closest('[class*="Block"]').length > 0){
							parentNode = $('#contentClone [data-id="' + prevCiteId + '"]').closest('[class*="Block"]');
							//if parentNode has supplement then get last supplement has parentNode
							if($('#contentClone [data-id^="' + prevCiteId + '-"]').closest('[class*="Block"]').length > 0){
								parentNode = $('#contentClone [data-id^="' + prevCiteId + '-"]').closest('[class*="Block"]').last();
							}
						}

						if (/\-/.test(v)){
							var floatPart = v.replace(/^([A-Z0-9]+)(\-.*)$/, '$1');
							var supplPart = v.replace(/^([A-Z0-9]+)\-(.*)$/, '$2');
							/*if (supplDetailsHash[floatPart]){
								supplDetailsHash[floatPart] = supplDetailsHash[floatPart] + '|' + supplPart;
							}else{
								supplDetailsHash[floatPart] = supplPart;
							}*/
						}else if (v && !citationsSeqHash[v]){
							citationsSeqHash[v] = parentNode;
							floatIDs.push(v);
						}
					});
				});
				
				//move floats to their relative place, where its first citation are placed
				//floatIDs = floatIDs.reverse();
				var blockIdsToSave = [];
				$.each(floatIDs, function(i, v){
					var blockNode = $('#contentClone [data-id="'+ v +'"]').closest('[class*="Block"],[class^="jrnlEqnPara"]').not('[data-track="del"], .del');
					var citeNodes = $('#contentClone .' + citationClassName + '[data-citation-string=" ' + v + ' "').closest('[class*="Block"]');
					if ((citeNodes.length > 0) && ($('#contentClone .' + citationClassName + '[data-citation-string=" ' + v + ' "').length == 0)) {

					}else{
						if ($(citationsSeqHash[v]).closest('[class="sub-article"]').length > 0){
							$(blockNode).appendTo("#contentClone .body");
						}else{
							$(blockNode).insertAfter($(citationsSeqHash[v]));
						}
					}
					var blockDataId = $(blockNode).attr('data-id');
					//Insert the supplement blocks next to the float block
					var supplBlock = $('#contentClone [data-id^="' + blockDataId + '-"]').closest('[class*="Block"]');
					if (supplBlock.length > 0){
						supplBlock.insertAfter(blockNode);
						supplBlock.attr('data-moved', 'true');
						supplBlock.each(function(){
							var suppDataId = $(this).attr('data-id');
							supplDetailsHash.push(suppDataId);
							var blockId = $(this).closest('[id]').attr('id');
							if($.inArray(blockId ,returnArray) < 0){
								blockIdsToSave.push(blockId);
							}		
						});
					}

					/*if (supplDetailsHash[v]){ //get its supplements and move it after main block
						supplIDs = supplDetailsHash[v].split(/\|/);
						// get unique elements of array
						supplIDs = $.grep(supplIDs, function(v, k){
							return $.inArray(v, supplIDs) === k;
						});
						supplIDs = citeJS.general.sortArrayAlphaNumeric(supplIDs);
						supplIDs = supplIDs.reverse();
						$.each(supplIDs, function(si, sv){
							var supplBlockID = v + '-' + sv;
							var supplBlockNode = $('#contentClone [data-id="' + supplBlockID + '"]').closest('[class*="Block"]');
							$(supplBlockNode).insertAfter($(blockNode)).removeAttr('data-uncited');
						});
					}*/

					$(blockNode).attr('data-moved', 'true');
					var blockId = $(blockNode).closest('[id]').attr('id');
					if($.inArray(blockId ,returnArray) < 0){
						blockIdsToSave.push(blockId);
						//returnArray.push(blockId);
					}
				});
				blockIdsToSave = blockIdsToSave.reverse();
				returnArray = $.merge( returnArray, blockIdsToSave);
				
				//move all uncited to the end of body or at the last float block
				//unCitedFloatBlocks = $('#contentClone *[class^="jrnl"][class*="Block"][data-uncited="true"]').not('[data-track="del"], .del');
				unCitedFloatBlocks = floatBlocks.filter('[data-uncited="true"]').not('[data-track="del"], .del');
				if(unCitedFloatBlocks.length >0) {
					unCitedFloatBlocks.each(function(i, unobj){
						var objId = $(unobj).attr('data-id').replace('BLK_' ,'');
						
						//If uncited sub floats like figure, table suplpemnts found then continue the loop
						if(supplDetailsHash.indexOf($(unobj).attr('data-id')) >= 0){
							return;
						}
						
						if($(unobj).closest('.body').length > 0){
							$(unobj).appendTo("#contentClone .body");
							$('#contentClone *[class^="jrnl"][class*="Block"][data-id*="' + objId + '-"]').each(function(){
								$(this).appendTo("#contentClone .body");
							});
						}else if($(unobj).closest('.jrnlAppBlock').length > 0){
							$(unobj).appendTo($(unobj).closest('.jrnlAppBlock'));
							$('#contentClone *[class^="jrnl"][class*="Block"][data-id*="' + objId + '-"]').each(function(){
								$(this).appendTo($(unobj).closest('.jrnlAppBlock'));
							});
						}else{
							$(unobj).appendTo("#contentClone .back");
							$('#contentClone *[class^="jrnl"][class*="Block"][data-id*="' + objId + '-"]').each(function(){
								$(this).appendTo("#contentClone .back");
							});
						}
						$(this).attr('data-moved', 'true');
						var blockId = $(this).closest('[id]').attr('id');
						if($.inArray(blockId ,returnArray) < 0){
							returnArray.push(blockId);
						}
					});
				}
			}else if(citationClassName == "jrnlSupplRef"){

				var citationNodes = citeJS.floats.getDocumentCitations(citationClassName, 'contentClone', settings.citNodePrefix);
				citationNodes.each(function(i, obj){
					var ridString = $(obj).attr('data-citation-string');
					var ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
					var rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);

					$.each(rids, function(i, v){
						if(v  && floatIDs.indexOf(v) < 0){
							floatIDs.push(v);
						}
					});
				});

				var floatBlocks = $('#contentClone *[class="jrnlSupplBlock"]').not('[data-track="del"], .del');
				floatBlocks.each(function(i, obj){
					var captionNode = $(this).find('[class^=jrnl][class*=Caption]').first();
					if(captionNode.length > 0){
						var id = $(captionNode).attr('data-id');
					}
					
					if (typeof(id) == 'undefined') return;
					nodePrefix = citeJS.general.getPrefix(id);
					if(nodePrefix != settings.citNodePrefix){
						//floatBlocks = floatBlocks.not(obj);
					}else if(!settings.citSeqHash[id]){
						//Set attribute uncited true
						$(obj).attr('data-uncited','true');
						var citeHash = citeJS.floats.createSeqHash(id);
					}
				});

				settings.citSeqHash.float_nodes = floatBlocks;
				floatBlocks.each(function(i, obj){
					var blockNode = obj;
					var captionNode = $(this).find('[class^=jrnl][class*=Caption]');
					if(captionNode.length > 0){
						var id = $(captionNode).attr('data-id');
					}

					if (typeof(id) == 'undefined') return;
					var newID = citeJS.general.constructNewID(id);

					if (settings.citSeqHash.floats[id]){
						// If the id is not same to the newId
						if (id != newID){
							var newFloatLabelString = citeJS.floats.getCitationHTML([newID], [], 'renumberFloats');
							//If caption node is found then change the new id and label
							if(captionNode.length > 0){
								$(blockNode).attr('data-id', 'BLK_' + newID);
								$(blockNode).attr('data-stream-name', 'a_' + newID);
								$(captionNode).attr('data-id', newID);
								$(captionNode).attr('data-old-id', id);
								if($(captionNode).find('.label').length == 0){
									$(captionNode).first().prepend('<span class="label"/>');
								}
								$(captionNode).find('.label:first').html(newFloatLabelString);
							}
							var blockId = $(blockNode).closest('[id]').attr('id');
							returnArray.push(blockId);
						}
					}
				});

				$.each(floatIDs, function(i, v){
					var lastSupplement = $('#contentClone *[class="jrnlSupplBlock"]').last();
					var blockNode = $('#contentClone [data-id="'+ v +'"]').closest('[class*="Block"]').not('[data-track="del"],.del');
					if(lastSupplement){
						$(blockNode).insertAfter(lastSupplement);
					}
					
					$(blockNode).attr('data-moved', 'true');
					var blockId = $(blockNode).closest('[id]').attr('id');
					if($.inArray(blockId ,returnArray) < 0){
						returnArray.push(blockId);
					}
				});

			}else if(citationClassName == "jrnlBibRef" || citationClassName == "jrnlFootNoteRef"){
				$('.citeRefText').remove();
				$('#contentClone .jrnlRefText:first').before('<div class="citeRefText"></div>');
				if (settings['R'].citationType != '1' || citationClassName == "jrnlFootNoteRef"){
					
					var floatSelector = (citationClassName == "jrnlFootNoteRef")?'.jrnlFootNote[data-id*="FN"]':'.jrnlRefText';
					var sNoClass      = (citationClassName == "jrnlFootNoteRef")?'':'RefSlNo';

					var floatSeqHash  = settings.citSeqHash.floats;
					var refNodes = $('#contentClone '+floatSelector).not('[data-track="del"], .del');
					settings.citSeqHash.float_nodes = refNodes;

					$(refNodes).each(function(i, obj){
						var blockId    = $(obj).closest('[id]').attr('id');
						var refdataId  = $(obj).attr('data-id');
						var prefix     = citeJS.general.getPrefix(refdataId);
						var citeConfig = citeJS.floats.getConfig(refdataId);

						//check if citation exists for current reference
						if (settings.citSeqHash[refdataId] && typeof(settings.citSeqHash[refdataId].newVal) == 'undefined'){ //check if citation exists for current reference
							if (Object.keys(floatSeqHash).length > 0){
								var refID = Object.keys(floatSeqHash).length - 1;//else get last key from floatSeqHash
								refID = Object.keys(floatSeqHash)[refID];
								refID = floatSeqHash[refID]; //and add +1 to it andassign it to the current
								refID = parseInt(refID.replace(/[A-Z]/, '')) + 1;
							}else if (Object.keys(settings.citSeqHash[citationClassName]).length > 0){
								var refID = Object.keys(settings.citSeqHash[citationClassName]).length - 1;//else get last key from floatSeqHash
								refID = Object.keys(settings.citSeqHash[citationClassName])[refID];
								refID = settings.citSeqHash[citationClassName][refID];//and add +1 to it andassign it to the current
								refID = parseInt(refID.replace(/[A-Z]+/, '')) + 1;
							}else{
								refID = 1;
							}
							//If the array has the id then move add it as last reference
							if($.inArray(prefix+refID, Object.values(settings.citSeqHash[citationClassName])) >= 0){
								refID = Object.values(settings.citSeqHash[citationClassName]).length+1;
							}
							
							floatSeqHash[refdataId] = prefix + refID;
						}else if(!settings.citSeqHash[refdataId]){
							$(obj).attr('data-uncited', 'true');
							citeJS.floats.createSeqHash(refdataId);
						}

						if (typeof(floatSeqHash[refdataId]) != 'undefined' && floatSeqHash[refdataId] != refdataId){
							$(obj).attr('data-old-id', refdataId);
							$(obj).attr('data-id', floatSeqHash[refdataId]);

							//Update the uncited float query id
							$('.query-div[data-query-action="uncited-reference"][data-float-id="' + refdataId + '"]').attr('data-new-float-id', floatSeqHash[refdataId]);
						}
						if($(obj).attr('data-id') && citationClassName != "jrnlFootNoteRef"){
							if ($(obj).find('.'+sNoClass).length == 0){
								$(obj).prepend('<span class="' + sNoClass + '">' + $(obj).attr('data-id') + '</span>');
							}
							$(obj).find('.'+sNoClass).each(function(){
								newLabelText = citeConfig['prefix'] + 
												$(obj).attr('data-id').replace(/[A-Z]+/gi, '') + 
												citeConfig['labelSuffix'];
								$(this).html(newLabelText);
							});
						}
						
						//Don't add id to save if ref group found - jagan
						//Send whole ref group to save
						if($.inArray(blockId ,returnArray) < 0 && $(this).closest('.jrnlRefGroup[id]').length == 0){
							returnArray.push(blockId);
						}
					});
					
					$('.query-div[data-query-action="uncited-reference"][data-new-float-id]').each(function(){
						var newId = $(this).attr('data-new-float-id');
						var queryID = $(this).attr('id');
						$(this).attr('data-float-id', newId);
						$(this).removeAttr('data-new-float-id');

						if($.inArray(queryID ,returnArray) < 0){
							returnArray.push(queryID);
						}
					});
					if(refNodes.closest('.jrnlRefGroup[id]').length > 0){
						var refGroupId = refNodes.closest('.jrnlRefGroup[id]').attr('id');
						returnArray.push(refGroupId);
					}
					refNodes.attr('data-moved', 'true');
					tinysort(refNodes, {attr:'data-id'});
				}else{
					if ($('#contentClone .jrnlRefText[data-insert-new]').length > 0){
						var charOrder = unescape(JSON.parse('"z[\u017A\u017C\u017E]y[\u0177\u0178\u024F\u1EF3]x[x]w[\u1E81\u1E83\u1E85\u0175]v[v]u[\u00F9\u00FA\u00FB\u00FC\u0169\u016B\u016D\u016F\u0171\u0173]t[\u0163\u0165\u0167]s[\u015B\u015D\u015F\u0161]r[\u0155\u0157\u0159\u024D]q[\u024B]p[p]o[\u014D\u014F\u0151\u01A1\u00F2\u00F3\u00F4\u00F5\u00F6]n[\u0144\u0146\u0148\u0149\u014B]m[m]l[\u013A\u013C\u013E\u0140\u0142]k[\u0137]j[\u0135]i[\u0129\u012B\u012D\u012F]h[\u0125\u0127]g[\u011D\u011F\u0121\u0123]f[f]e[\u00E8\u00E9\u00EA\u00EB\u0119\u04D7]d[\u010F\u0111\u1E11]c[\u00E7\u0107\u0109\u010B\u010D]b[\u03B2\u0432]a[\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5\u00E5\u0105\u0103]"'));
						tinysort('#contentClone .jrnlRefText:not([data-track="del"])','span.RefAuthor,span.RefCollaboration',{charOrder: charOrder});

						$('#contentClone .jrnlRefText').each(function(i, obj){
							$(obj).attr('data-moved', 'true');
							var blockId    = $(obj).closest('[id]').attr('id');
							if($.inArray(blockId ,returnArray) < 0){
								returnArray.push(blockId);
							}
						});
					}
					$('#contentClone .jrnlRefText[data-insert-new]').removeAttr('data-insert-new');
				}
				var refNodes = $('#contentClone .jrnlRefText').not('[data-track="del"],.del');
				settings.citSeqHash.float_nodes = refNodes;
				$(refNodes).each(function(i, obj){
					$('#contentClone .citeRefText').before($(this));
				});
				//move the deleted ref at the end
				$('#contentClone .jrnlRefText[data-track="del"], #contentClone .jrnlRefText.del').each(function(){
					$('#contentClone .citeRefText').before($(this));
				});
				$('#contentClone .citeRefText').remove();
			}
			return returnArray;
		},
		updateRightPanel: function(floatNode, citationClass){
			if (floatNode){
				settings.citSeqHash.float_nodes = floatNode;
			}
			if (citationClass){
				settings.citSeqHash.citationClassName = citationClass;
			}
			if(!settings.citSeqHash.float_nodes){
				return false;
			}
			settings.citSeqHash.float_nodes.each(function(){
				var fId = $(this).attr('data-id');
				kriya.general.updateRightNavPanel(fId, '#contentClone');
			});
			$('#navContainer #dataDivNode .card').each(function(){
				var fId = $(this).attr('id');
				if($('#contentClone [data-id="' + fId + '"]').length < 1){
					$(this).remove();
				}
			});
		},
		getReferenceHTML:function(newRIDs, obj, authorYear, isDirect, prevObj, isFollowing, isPossessive, abbrCollab){
			var citationIDArray = newRIDs.replace(/^\s+|\s+$/g, '');
			citationIDArray     = citationIDArray.split(/\s+/);
			var citeConfig      = citeJS.floats.getConfig(citationIDArray[0]);
			if(citeConfig){
				var formatStart = '';
				var formatEnd   = '';
				if(citeConfig.formatting){
					var arr = citeJS.floats.getFormatTag(citeConfig.formatting);
					if(arr){
						formatStart = arr[0];
						formatEnd   = arr[1]
					}
				}
				if(authorYear == true && !citeConfig.authorFormat){
					citeConfig.authorFormat = {
						'direct' : {
							'1': {'pattern': 'Surname|, (|Year|)'},
							'2': {'pattern': 'Surname| and |Surname|, (|Year|)'},
							'more': {'pattern': 'Surname| |et al|. (|Year|)'},
							'etalFormat': '',
						},
						'indirect' : {
							'1': {'pattern': 'Surname|, |Year'},
							'2': {'pattern': 'Surname| and |Surname|, |Year'},
							'more': {'pattern': 'Surname| |et al|., |Year'},
							'etalFormat': '',
						}
					}
				}
				if (citeConfig.citationType == '1' || authorYear == true){
					var authorString = '';
					
					var authoFormatObj = (isDirect == true)?citeConfig.authorFormat.direct:citeConfig.authorFormat.indirect;

					var citationEtAlFormatString = authoFormatObj['etalFormat'];

					$.each(citationIDArray, function(cIndex, cRid){
						var refNode = $(citeJS.settings.containerElm + ' [data-id="' + cRid +'"]').not('[data-track="del"], .del');
						var citeNode = $(citeJS.settings.containerElm + ' [data-citation-string*=" ' + cRid +' "]').not('[data-track="del"], .del');
						if (refNode.length > 0){
							var refAuthors    = $(refNode).find('.RefAuthor').not('.del,[data-track="del"]');

							var refSurNames   = $(refNode).find('.RefSurName').not('.del,[data-track="del"]');;
							var refGivenNames = $(refNode).find('.RefGivenName').not('.del,[data-track="del"]');;
							var refYear       = $(refNode).find('.RefYear').not('.del,[data-track="del"]');;

							var refAuthorsLen = refAuthors.length;
							// if no authors found, then check for collaboration
							if(refAuthorsLen == 0)	{
								refSurNames = $(refNode).find('.RefCollaboration').not('.del,[data-track="del"]');;
								refAuthorsLen = refSurNames.length;
							}

							//If ref author and collabrator not found then get ref editor
							if(refAuthorsLen == 0){
								refAuthors    = $(refNode).find('.RefEditor').not('.del,[data-track="del"]');;
								refAuthorsLen = refAuthors.length;
							}

							// collect the formatting information from the config settings based on the number of authors
							var citationFormatString = null;
							if(isFollowing == true && authoFormatObj[refAuthorsLen] && authoFormatObj[refAuthorsLen]['following-pattern']){
								citationFormatString = authoFormatObj[refAuthorsLen]['following-pattern'];
							}else if(authoFormatObj[refAuthorsLen] && authoFormatObj[refAuthorsLen].pattern){
								citationFormatString = authoFormatObj[refAuthorsLen].pattern;
							}
							
							if (!citationFormatString){
								//citationFormatString = authoFormatObj['last'];
								citationFormatString = authoFormatObj['more'].pattern;
							}

							var prevNode;
							if(prevObj && $(prevObj).length > 0){
								prevNode    = $(prevObj)[0];
								var punNode = prevNode.nextSibling;
							}else if (obj && obj.previousSibling){
								prevObj = obj.previousSibling;
								if (prevObj && (prevObj.nodeName == '#text') && (/^[\,\s\.\;\[\]\(\)\u00A0]+$/i.test(prevObj.nodeValue))) {
									var punNode = prevObj;
									prevObj = prevObj.previousSibling;
									if (prevObj && ((prevObj.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevObj.getAttribute('class')))){
										prevNode = prevObj;
									}
								}
							}

							if(prevNode){
								//to check previous reference's Author names match current author's 
								//if matches and the citation style is to show only year, which has to be set in the settings as 'sameasprevious'
								var prevCiteString = prevNode.getAttribute('data-citation-string').replace(/^\s+|\s+$/g, '');
								var prevID      = prevCiteString.split(/\s+/)[0];
								var prevRefNode = $(citeJS.settings.containerElm + ' [data-id="' + prevID +'"]');
								if (prevRefNode.length > 0){
									
									var prevYear            = prevRefNode.find('.RefYear').clone(true).cleanTrackChanges().text();
									var prevAuthorSurName   = prevRefNode.find('.RefSurName').clone(true).cleanTrackChanges().text();
									var prevAuthorGivenName = prevRefNode.find('.RefGivenName').clone(true).cleanTrackChanges().text();
									
									var prevCiteText        = $(prevNode).clone(true).cleanTrackChanges().text();
									var curCiteText         = citeJS.floats.getAuthorYearText(cRid, citationEtAlFormatString, citationFormatString, isDirect);

									var prevIsDirect = (prevCiteText.match(/\(\d{4}[a-z]?.*\)/i))?true:false;
									
									var currRefYear         = refNode.find('.RefYear').clone(true).cleanTrackChanges().text();
									var currAuthorSurName   = refSurNames.clone(true).cleanTrackChanges().text();
									var currAuthorGivenName = refGivenNames.clone(true).cleanTrackChanges().text();
									var isSameYear = ((prevYear.match(/(\d{4})/) && currRefYear.match(/(\d{4})/)) && (prevYear.match(/(\d{4})/)[0] == currRefYear.match(/(\d{4})/)[0]));
									var sameAsPrevPunc = ',';

									//If prev citation only has the year
									if(prevCiteText == prevYear){
										prevCiteText  = citeJS.floats.getAuthorYearText(prevID, citationEtAlFormatString, citationFormatString, prevIsDirect);
									}
									var prevAuthorText = prevCiteText.replace(prevYear, "");
									var curAuthorText  = curCiteText.replace(currRefYear, "");

									if(prevAuthorText.replace(/<\/?[i|b]>/g,'') == curAuthorText.replace(/<\/?[i|b]>/g,'')){// "/<\/?[i|b]>/" To remove format tags(italic and bold)
										if(isSameYear && authoFormatObj.sameasprevauthoryear){
											citationFormatString = authoFormatObj.sameasprevauthoryear.pattern;
											sameAsPrevPunc = (citeConfig.sameasprevauthoryearpunc)?citeConfig.sameasprevauthoryearpunc:sameAsPrevPunc;
										}else if (authoFormatObj.sameasprevauthor){
											citationFormatString = authoFormatObj.sameasprevauthor.pattern;
											sameAsPrevPunc = (citeConfig.sameasprevauthorpunc)?citeConfig.sameasprevauthorpunc:sameAsPrevPunc;
										}

										//If the citation is same as previous citation then repalce the punctuation node with comma space
										if(authoFormatObj.sameasprevauthoryear || authoFormatObj.sameasprevauthor){
											if(isDirect == true && prevIsDirect == true){
												$(punNode).remove();
												$(obj).remove();
												var currCiteString   = citeJS.floats.getAuthorYearText(cRid, citationEtAlFormatString, citationFormatString);
												var prevHtml = $(prevNode).clone(true).cleanTrackChanges().html();
												prevHtml = prevHtml.replace(/(\d{4}[^\)]*)/i, '$1'+sameAsPrevPunc+currCiteString);
												$(prevNode).html(prevHtml);
												$(prevNode).attr('data-citation-string', ' ' + prevCiteString + ' ' + cRid + ' ');
											}else{
												$(punNode).replaceWith(sameAsPrevPunc);
											}
										}
									}

									if(prevAuthorSurName != "" && currAuthorSurName != "" && prevAuthorSurName == currAuthorSurName){
										if(prevAuthorGivenName != "" && currAuthorGivenName != "" && prevAuthorGivenName == currAuthorGivenName){
											if(isSameYear && citeConfig.authorFormat.sameasprevauthoryear){
												citationFormatString = citeConfig['authorFormat']['sameasprevauthoryear'].pattern;
												sameAsPrevPunc = (citeConfig.sameasprevauthoryearpunc)?citeConfig.sameasprevauthoryearpunc:sameAsPrevPunc;
											}else if (citeConfig.authorFormat.sameasprevauthor){
												citationFormatString = citeConfig['authorFormat']['sameasprevauthor'].pattern;
												sameAsPrevPunc = (citeConfig.sameasprevauthorpunc)?citeConfig.sameasprevauthorpunc:sameAsPrevPunc;
											}

											//If the citation is same as previous citation then repalce the punctuation node with comma space
											if(citeConfig.authorFormat.sameasprevauthoryear || citeConfig.authorFormat.sameasprevauthor){
												if(isDirect == true && prevIsDirect == true){
													$(punNode).remove();
													$(obj).remove();
													var currCiteString   = citeJS.floats.getAuthorYearText(cRid, citationEtAlFormatString, citationFormatString);
													var prevHtml = $(prevNode).clone(true).cleanTrackChanges().html();
													prevHtml = prevHtml.replace(/(\d{4}[^\)]*)/i, '$1'+sameAsPrevPunc+currCiteString);
													$(prevNode).html(prevHtml);
													$(prevNode).attr('data-citation-string', ' ' + prevCiteString + ' ' + cRid + ' ');
												}else{
													$(punNode).replaceWith(sameAsPrevPunc);
												}
											}
										}else if(isSameYear && authoFormatObj.replacesurname){
											citationFormatString   = citationFormatString.replace(/surname/i, authoFormatObj.replacesurname);
											var prevAuthorString   = citeJS.floats.getAuthorYearText(prevID, citationEtAlFormatString, citationFormatString, prevIsDirect);
											$(prevNode).html(prevAuthorString);
										}
									}
								}
							}
							authorString   += citeJS.floats.getAuthorYearText(cRid, citationEtAlFormatString, citationFormatString, isDirect, isPossessive, abbrCollab);
						}
					});
					newCitationText = formatStart + authorString + formatEnd;
				}
				// check if the reference citation is Vancouver style
				else {
					newCitationTextArray = newRIDs.replace(/^\s+|\s+$|[A-Z]/g, '').split(/\s+/gi);
					//newCitationTextArray.shift();
					if (newCitationTextArray.length == 1){
						newCitationText = newCitationTextArray[0];
						citationString = ''; //settings['citationClass'][type]['singularString'];
					}else{
						
						var rangePunc      = (citeConfig && citeConfig.rangePunc != undefined)?citeConfig.rangePunc:(settings.rangePunc)?settings.rangePunc:'';
						var interPunc      = (citeConfig && citeConfig.interPunc != undefined)?citeConfig.interPunc:(settings.interPunc)?settings.interPunc:'';
						var penultimate    = (citeConfig && citeConfig.penultimatePunc != undefined)?citeConfig.penultimatePunc:(settings.penultimatePunc)?settings.penultimatePunc:'';
						var citeNumPrefix  = (citeConfig && citeConfig.numberPrefix)?citeConfig.numberPrefix:'';

						newCitationText = citeJS.general.getRanges(newCitationTextArray, rangePunc, interPunc, penultimate);
						citationString  = ''; //settings['citationClass'][type]['pluralString'];
					}

					newCitationText = ((citeConfig['prefix'])?citeConfig['prefix']:'') + citationString + 
										formatStart + newCitationText + 
										((citeConfig['suffix'])?citeConfig['suffix']:'') + formatEnd;

					/*newCitationText = citeConfig['prefix'] + citationString + 
						citeConfig['formatStart'] + newCitationText + 
						citeConfig['suffix'] + citeConfig['formatEnd'];*/
				}
			}
			return newCitationText;
		},
		getAuthorYearText: function(refId, etalFormat, format, isDirect, isPossessive, abbrCollab){
			var refNode = $(citeJS.settings.containerElm + ' [data-id="' + refId +'"]').not('[data-track="del"], .del');
			var authorString = '';
			if (refNode.length > 0){
				etalFormat = citeJS.floats.getFormatTag(etalFormat);
				var etalStart = '';
				var etalEnd   = '';
				if(etalFormat){
					etalStart  = etalFormat[0];
					etalEnd    = etalFormat[1];	
				}

				var refSurNames   = "";
				var refGivenNames = "";
				if($(refNode).find('.RefAuthor').length > 0){
					refSurNames   = $(refNode).find('.RefSurName').clone(true);
					refGivenNames = $(refNode).find('.RefGivenName').clone(true);
				}else if($(refNode).find('.RefCollaboration').length > 0){
					refSurNames = $(refNode).find('.RefCollaboration');
				}else if($(refNode).find('.RefEditor').length > 0){
					refSurNames   = $(refNode).find('.RefSurName').clone(true);
					refGivenNames = $(refNode).find('.RefGivenName').clone(true);
				}
				
				var refYear    = $(refNode).find('.RefYear').clone(true);
				citFormatArray = format.split(/\|/);

				if(isPossessive){
					var lastSurName   = citFormatArray.lastIndexOf('Surname');
					var lastGivenName = citFormatArray.lastIndexOf('Givenname');
					var lastAuthorIndex = (lastGivenName > lastSurName)?lastGivenName:lastSurName;
					citFormatArray.splice(lastAuthorIndex+1, 0, "'s");
				}
				
				var si = 0;
				var gi = 0;
				var refYearText = $(refYear).cleanTrackChanges().text();
				$.each(citFormatArray, function(cf, cfv){
					if (cfv.match(/surname/i) && refSurNames){
						var surNameNode = $(refSurNames[si++]);
						var surNameText = surNameNode.cleanTrackChanges().text();
						if(abbrCollab && surNameNode.attr('class') == "RefCollaboration"){
							var collabText = surNameNode.html();
							if(collabText.match(/\[(.*)\]/g)){
								surNameText = collabText.match(/\[(.*)\]/)[1];
							}
						}
						authorString = authorString + surNameText;
					}else if(cfv.match(/givenname/i) && refGivenNames){
						authorString = authorString + $(refGivenNames[gi++]).cleanTrackChanges().text();
					}else if (cfv.match(/et[\s]al/i)){
						authorString = authorString + etalStart + cfv + etalEnd;
					}else if (cfv.match(/alphaYear/i)){
						var alpha = refYearText.replace(/\d{4}/g, '');
						authorString = authorString + alpha;
					}else if (cfv.match(/year/i)){
						if (refYearText){
							authorString = authorString + refYearText;
						}
					}else{
						authorString = authorString + cfv;
					}
				});
			}
			return authorString;
		},
		getFormatTag: function(type){
			var returnArray = [];
			if(type){
				var startTag = '<' + type.replace(/\s/g, '><') + '>';
				var endTag = '</' + type.replace(/\s/g, '></') + '>';
				returnArray.push(startTag);
				returnArray.push(endTag);
				return returnArray;
			}else{
				return false;
			}
			
		},
		getCitationHTML: function(citationArray, oldCitString, callingFunc, isSentenceStart){
			if ((typeof(citationArray) == 'undefined') || (citationArray.length == 0)){
				console.log('citationArray is undefined');
				return '';
			}
			if ((callingFunc == 'renumberCitations') && (typeof(oldCitString) == 'undefined')){
				console.log('oldCitString is undefined');
				return '';
			}
			if (callingFunc == 'renumberCitations'){
				oldCitString = oldCitString.replace(/^[\s\t\r\n]+|[\s\t\r\n]+$/, '');
				//Expand the range EX: 3-6 -> 3,4,5,6
				var re = new RegExp('(((\\d+)([a-z]+)?)([\\-\\u2013\\u2014])((\\d+)([a-z]+)?))', 'gi');
				oldCitString = oldCitString.replace(re, function(match){
					var list = [arguments[2]];
					for (var i = parseInt(arguments[3])+1; i <= parseInt(arguments[7])-1; i++) {
						list.push(i);
					}
					list.push(arguments[6]);
					if(list.length > 0){
						return list.join(', ');
					}
				});
				
				var labelsObj = kriya.general.extractLabelFromText(oldCitString);
				var oldCitArray  = [];
				$.each(labelsObj, function(i, obj){
					if(obj['match-val']){
						oldCitArray.push(obj['match-val']);
					}
				});
				
				if (citationArray.length > oldCitArray.length){
					console.log('parts length mismatch');
					return;
				}
			}
			
			var citationPrefix = '', floatLabelSuffix = '';
			// Assumption Made: one citation node will contain only one type of citation
			// for example, either figure citation or figure supplementary citation or table citation, ...
			
			//Get the citation prefix and suffix
			item = citationArray[0];
			if(item){
				var suppParts = item.split(/-/);
				var citePart = "";
				var citeConfig = {};
				for (var p = 0; p < suppParts.length; p++) {
					citePart = citePart+suppParts[p];
					citeConfig = citeJS.floats.getConfig(citePart);
					if(citeConfig){
						//Get the fig number if loop is not last and suppParts length is more than one
						var citeNum = (suppParts.length > 1 && p != (suppParts.length-1))?suppParts[p].replace(/[A-Z]+/g, ''):''; 
						
						var singular = (isSentenceStart && citeConfig.sentenceStartSingular)?citeConfig.sentenceStartSingular:citeConfig.singularString;
						var plural = (isSentenceStart && citeConfig.sentenceStartPlural)?citeConfig.sentenceStartPlural:citeConfig.pluralString;
						
						//Get the float string singular or plural value
						var floatString = (citationArray.length == 1 || (citationArray.length > 1 && p != (suppParts.length-1)))?singular:plural;
						
						//When undefined combine NaN will return So,
						//If variable is undefied then change to empty string
						floatString = (floatString)?floatString:'';
						var prefix = (citeConfig.prefix)?citeConfig.prefix:'';
						var suffix = (citeConfig.suffix)?citeConfig.suffix:'';

						//Contruct the citation prefix
						citationPrefix += prefix + floatString + suffix + ((citeConfig.skipNumber)?'':citeNum);
						
						if(callingFunc == 'renumberFloats')	{
							floatLabelSuffix 	= citeConfig.labelSuffix; 
						}
					}
					citePart += (p != (suppParts.length-1))?"-":"";
				}
			}
				
			/*return false;	

			if (typeof(item) == 'undefined'){
				return '';
			}else if (!/\-/.test(item)){
				prefix = settings.citSeqHash.prefix[item];
				floatID = item.replace(prefix, '');
				if (citationArray.length == 1){
					citationPrefix = settings[prefix].prefix + settings[prefix].singularString + settings[prefix].suffix
				}else{
					citationPrefix = settings[prefix].prefix + settings[prefix].pluralString + settings[prefix].suffix
				}
				if(callingFunc == 'renumberFloats')	{
					citationSuffix 	= settings[prefix].labelSuffix; 
				}
			}else if (/^[A-Z0-9]+\-[A-Z\-0-9]+$/.test(item)){
				var suppParts = item.split(/-/);
				var figPart = suppParts[0]; suppPart = suppParts[1];
				figPartPrefix = settings.citSeqHash.prefix[figPart];
				if (typeof(settings.citSeqHash.prefix[suppPart]) != 'undefined'){
					suppPartPrefix = settings.citSeqHash.prefix[suppPart];
				}else{
					suppPartPrefix = suppPart.replace(/[0-9]+/g, '');
				}
				if (suppParts.length == 3){
					subSuppPart = suppParts[2];
					if (typeof(settings.citSeqHash.prefix[subSuppPart]) != 'undefined'){ 
						subSuppPartPrefix = settings.citSeqHash.prefix[subSuppPart];
					}else{
						subSuppPartPrefix = subSuppPart.replace(/[0-9]+/g, '');
					}
				}
				//suppPartPrefix = settings.citSeqHash.prefix[suppPart];
				
				var figNum = figPart.replace(/[A-Z]+/, '');
				var subFigNum = suppPart.replace(/[A-Z]+/, '');
				if (citationArray.length == 1){
					citationPrefix = settings[figPartPrefix].prefix + settings[figPartPrefix].singularString + figNum + settings[figPartPrefix][suppPartPrefix].prefix  + settings[figPartPrefix][suppPartPrefix].singularString;
					if (suppParts.length == 3){
						citationPrefix = settings[figPartPrefix].prefix + settings[figPartPrefix].singularString + figNum + settings[figPartPrefix][suppPartPrefix].prefix  + settings[figPartPrefix][suppPartPrefix].singularString + subFigNum + settings[figPartPrefix][subSuppPartPrefix].prefix  + settings[figPartPrefix][subSuppPartPrefix].singularString
					}
				}else{
					citationPrefix = settings[figPartPrefix].prefix + settings[figPartPrefix].singularString + figNum + settings[figPartPrefix][suppPartPrefix].prefix  + settings[figPartPrefix][suppPartPrefix].pluralString;
					if (suppParts.length == 3){
						citationPrefix += citationPrefix = settings[figPartPrefix].prefix + settings[figPartPrefix].singularString + figNum + settings[figPartPrefix][suppPartPrefix].prefix  + settings[figPartPrefix][suppPartPrefix].singularString + subFigNum + settings[figPartPrefix][subSuppPartPrefix].prefix  + settings[figPartPrefix][subSuppPartPrefix].pluralString;
					}
				}
				if(callingFunc == 'renumberFloats')	{
					citationSuffix 	= settings[figPartPrefix][suppPartPrefix].labelSuffix; 
				}
			}
			// citation type not handled
			else{
				return '';
			}*/
			
			var rangePunc      = (citeConfig && citeConfig.rangePunc != undefined)?citeConfig.rangePunc:(settings.rangePunc)?settings.rangePunc:'';
			var interPunc      = (citeConfig && citeConfig.interPunc != undefined)?citeConfig.interPunc:(settings.interPunc)?settings.interPunc:'';
			var penultimate    = (citeConfig && citeConfig.penultimatePunc != undefined)?citeConfig.penultimatePunc:(settings.penultimate)?settings.penultimatePunc:'';
			var citeNumPrefix  = (citeConfig && citeConfig.numberPrefix)?citeConfig.numberPrefix:'';
			var chapNumSuffix = (citeConfig && citeConfig.chapNumSuffix)?citeConfig.chapNumSuffix:'';

			var floatSeqArray = [];
			var index = 0;
			var chapNumber = "";
			citationArray.forEach(function(item) {
				var suppParts = item.split(/-/);
				var lastPart  = suppParts.pop();
				var floatElement = $(citeJS.settings.containerElm + ' [data-id="' + item + '"]').closest('[data-id^="BLK_"]');
				var citationNum = lastPart.replace(/^[A-Z]+/, '');
				if(citationNum.match(/\_/g)){
					chapNumber  = citationNum.replace(/\_[0-9]+[a-z]?/, '');
					citationNum = citationNum.replace(/^[0-9]+\_/, '');
				}
				if(callingFunc == 'getNewCitation' && floatElement.length > 0 && floatElement.find('[class*="Caption"] .label').length > 0 && floatElement[0].hasAttribute('data-uncited')){
					var labelText = floatElement.find('[class*="Caption"] .label').text();
					labelText = labelText.replace(/[\s\.\|]+$/gi,  '');
					citationPrefix = (citationArray.length > 1)?labelText + interPunc + citationPrefix:labelText;
				}else if (callingFunc == 'renumberCitations'){
					floatSeqArray.push(citationNum + oldCitArray[index++].replace(/[0-9\_]+/, ''));
				}else{
					floatSeqArray.push(citationNum);
				}

				/*if (!/\-/.test(item)){
					if (callingFunc == 'renumberCitations'){
						floatSeqArray.push(item.replace(/^[A-Z]+/, '') + oldCitArray[index++].replace(/[0-9]+/, ''));
					}else{
						floatSeqArray.push(item.replace(/^[A-Z]+/, ''));
					}
				}else{
					var suppParts = item.split(/-/);
					var figPart = suppParts[0]; suppPart = suppParts[1];
					if (suppParts.length == 3) suppPart = suppParts[2];
					figPartPrefix = settings.citSeqHash.prefix[figPart];
					if (typeof(settings.citSeqHash.prefix[suppPart]) != 'undefined'){ 
						suppPartPrefix = settings.citSeqHash.prefix[suppPart];
					}else{
						suppPartPrefix = suppPart.replace(/[0-9]+/g, '');
					}
					//suppPartPrefix = settings.citSeqHash.prefix[suppPart];
					if (callingFunc == 'renumberCitations'){
						floatSeqArray.push(suppPart.replace(/^[A-Z]+/, '') + oldCitArray[index++].replace(/[0-9]+/, ''));
					}
					else{
						floatSeqArray.push(suppPart.replace(/^[A-Z]+/, ''));
					}
				}*/
			});
			
			var formatStart = '';
			var formatEnd   = '';
			if(citeConfig && citeConfig.formatting){
				var arr = citeJS.floats.getFormatTag(citeConfig.formatting);
				if(arr){
					formatStart = arr[0];
					formatEnd   = arr[1]
				}
			}

			var rangeVal = citeJS.general.getRanges(floatSeqArray, rangePunc, interPunc, penultimate, citeNumPrefix);
			var newCitationString  = formatStart + citationPrefix;
			if(rangeVal && chapNumber){
				newCitationString = newCitationString + chapNumber + chapNumSuffix;
			}
			
			if(rangeVal && citeConfig && !citeConfig.skipNumber){
				newCitationString += rangeVal;
			}

			newCitationString += formatEnd;
			newCitationString += floatLabelSuffix;

			//newCitationString  = citationPrefix + ((citeConfig)?citeConfig.formatStart:'') + citeJS.general.getRanges(floatSeqArray, rangePunc, interPunc, penultimate, citeNumPrefix) + ((citeConfig)?citeConfig.formatEnd:'') + floatLabelSuffix;

			//console.log('new citation string : ' + newCitationString );
			return newCitationString;
		},
		/**
		 * Get the configuration setting for the gien float id
		 * @param floatID - Float element data-id
		 */
		getConfig: function(floatID){
			var suppParts = floatID.split(/-/);
			var citeSettings = settings;
			var citeAncestor = settings;
			for (var p = 0; p < suppParts.length; p++) {
				var citePart = suppParts[p];
				var citePrefix = citeJS.general.getPrefix(citePart);
				if(citeSettings[citePrefix]){
					citeAncestor = citeSettings;
					citeSettings = citeSettings[citePrefix];
				}else if(citeAncestor[citePrefix]){
					citeSettings = citeAncestor[citePrefix];
					citeAncestor = citeAncestor;
				}
			}
			return citeSettings;
		}
	}
})(citeJS || {});

/**
*    Extend citeJS to add renumbering of reference citations
*/
(function (citeJS) {
	var settings = citeJS.settings;
	citeJS.general = {
		/**
		* sort an array alpha numerically, e.g., A10, A1, A4, A3 => A1, A3, A4, A10
		* http://stackoverflow.com/questions/4340227/sort-mixed-alpha-numeric-array
		* @param array
		*
		* @returns {string}
		*/
		sortArrayAlphaNumeric: function(myArray) {
			var reA = /[^a-zA-Z]/g;
			var reN = /[^0-9]/g;
			function sortAlphaNum(a,b){
				var aA = a.replace(reA, "");
				var bA = b.replace(reA, "");
				if(aA === bA){
					var aN = parseInt(a.replace(reN, ""), 10);
					var bN = parseInt(b.replace(reN, ""), 10);
					return aN === bN ? 0 : aN > bN ? 1 : -1;
				} else {
					return aA > bA ? 1 : -1;
				}
			}
			return myArray.sort(sortAlphaNum);
		},
		
		/**
		* Code from https://github.com/javve/natural-sort
		*
		* Author: Jim Palmer (based on chunking idea from Dave Koelle)
		*/
		naturalSort: function(a, b, options) {
		  var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
			sre = /(^[ ]*|[ ]*$)/g,
			dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
			hre = /^0x[0-9a-f]+$/i,
			ore = /^0/,
			options = options || {},
			i = function(s) { return options.insensitive && (''+s).toLowerCase() || ''+s; },
			// convert all to strings strip whitespace
			x = i(a).replace(sre, '') || '',
			y = i(b).replace(sre, '') || '',
			// chunk/tokenize
			xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
			yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
			// numeric, hex or date detection
			xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
			yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
			oFxNcL, oFyNcL,
			mult = options.desc ? -1 : 1;
		  // first try and sort Hex codes or Dates
		  if (yD)
			if ( xD < yD ) return -1 * mult;
			else if ( xD > yD ) return 1 * mult;
		  // natural sorting through split numeric strings and default strings
		  for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
			// find floats not starting with '0', string or 0 if not defined (Clint Priest)
			oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
			oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
			// handle numeric vs string comparison - number < string - (Kyle Adams)
			if (isNaN(oFxNcL) !== isNaN(oFyNcL)) { return (isNaN(oFxNcL)) ? 1 : -1; }
			// rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
			else if (typeof oFxNcL !== typeof oFyNcL) {
			  oFxNcL += '';
			  oFyNcL += '';
			}
			if (oFxNcL < oFyNcL) return -1 * mult;
			if (oFxNcL > oFyNcL) return 1 * mult;
		  }
		  return 0;
		},

		/**
		* get ranges - convert an array of numbers into ranges
		* http://stackoverflow.com/questions/2270910/how-to-convert-sequence-of-numbers-in-an-array-to-range-of-numbers
		* @param array
		* @modified by jagan
		* @returns {string}
		*/
		getRanges: function (array, rangePunc, interPunc, penultimatePunc, citeNumPrefix){
			var returnVal = '';
			rangePunc = (rangePunc)?rangePunc:"-";
			interPunc = (interPunc)?interPunc:", ";
			
			//array = citeJS.general.sortArrayAlphaNumeric(array);
			array = array.sort(citeJS.general.naturalSort);
			var ranges = [], rstart, rend;
			for (var i = 0; i < array.length; i++){
				rstart = array[i];
				rend = rstart;
				while (array[i + 1] - array[i] == 1){
					rend = array[i + 1]; // increment the index if the numbers sequential
					i++;
				}
				ranges.push(rstart == rend ? rstart+'' : rstart + rangePunc + rend);
			}
			function replacer(match){
				rangeVals = match.split(rangePunc);
				if ((rangeVals[1] - rangeVals[0]) == 1){
					return rangeVals[0] + interPunc + rangeVals[1];
				}else{
					return match;
				}
			};
			var re = new RegExp('((\\d+)'+rangePunc+'(\\d+))', 'g');
			returnVal = ranges.join(interPunc).replace(re, replacer);
			//Add penultimate punctuation
			if(penultimatePunc)	{
				var re = new RegExp(interPunc+'(\\d+)$', 'g');	
				if(re.test(returnVal)){
					returnVal = returnVal.replace(re, penultimatePunc+'$1');
				}
			}
			//Add number prefix
			//Ex Figure S1,S2
			if(citeNumPrefix){
				returnVal = returnVal.replace(/(\d+)/g, citeNumPrefix+'$1');
			}
			return returnVal;
		},
		
		/**
		* get Prefix of the Citation Node
		* for F1 -> F, F1-S1 -> F 
		* @param datacitationstring
		*
		* @returns prefix
		*/
		getPrefix: function(datacitationstring)	{
			if(!datacitationstring){
				return false;
			}

			if (!/\-/.test(datacitationstring)){		// check to see if the citation is not a supplementary node
				var citIDPrefix = datacitationstring.replace(/\s*([A-Z]+)[0-9]+.*?$/, '$1'); 
			}else{									// if the citation is a supplementary citation
				var suppParts 	= datacitationstring.split(/-/);
				var figPart 	= suppParts[0]; 
				var citIDPrefix = figPart.replace(/[0-9\_]+/, '');
			}
			citIDPrefix			= citIDPrefix.replace(/\s*/, '');
			citIDPrefix         = citIDPrefix.replace(/fig([\d]+)?/, 'F');

			return citIDPrefix;
		},
		/**
		 * Function to get the new id
		 * Contruct the id from settings.citSeqHash
		 * @param id - old id of the float
		 * @return newId of the float
		 */
		constructNewID: function(id){
			var suppParts = id.split(/-/);
			var prevHashSeq = settings.citSeqHash;
			var newIDArray = [];
			for (var p = 0; p < suppParts.length; p++) {
				var citePart = suppParts[p];
				if (prevHashSeq && prevHashSeq[citePart]){
					newIDArray.push(prevHashSeq[citePart].newVal);
				}
				prevHashSeq = (prevHashSeq)?prevHashSeq[citePart]:null;
			}
			return newIDArray.join("-");
		},
		captureCitation: function(data){
			var thisObj = this;
			var returnVal   = '';
			
			//#147 - Citations are copy and pasted up for multiple citations.- Mohammed Navaskhan (mohammednavas.a@focusite.com)
            var citeRegText = '(Fig(?:ure)?s?\.?|Tables|Table|Tab\.|Tab|Tbl|Schemas|Schema|Sch\.|Sch|Box(?:es)?|Pictures?|Author response image|Images?|Charts?|Appendix|Movies?|Videos?|rich media files?|audios?|(?:supplement(s|ary|al)? (?:Files?|movies?|videos?|appendix)))\.?[\s\-]{0,2}(?:\([0-9]{1,3}\)|[a-z0-9]{1,3})(–[a-z0-9])?(, [a-z0-9]{1,3})?( and [a-z0-9])?|([a-zA-Z]+, ([0-9]{4}))';
            var re = new RegExp(citeRegText, 'ig');
			//End of #147
			if(data && typeof(data) == "string"){
				var parsedData = $.parseHTML(data);
				parsedData.forEach(function(item) {
					if(item.nodeType == 3 && item.nodeName == "#text"){
						var textValue = item.nodeValue;
						textValue = textValue.replace(re, function(match){
							var citeData = thisObj.getCitationData(match);
							if(citeData){
								//#476 - All Customers | Reference and Content | Copy paste Numeric Values - Rajasekar. T(rajasekar.t@focusite.com)
								//added class checkIfRef if the reference type is jrnlBifRef
								return '<span data-capturd="paste" class="' + citeData[0] +' '+ ((citeData[0]=="jrnlBibRef") ?'checkIfRef':'') + '" data-citation-string="' + citeData[1] + '">' + match + '</span>';
							}else{
								return match;
							}
						});
						item.nodeValue = textValue;
					}else if(item.nodeType == 1 && $(item).closest(kriya.config.citationSelector).length == 0){
						var capturedData = thisObj.captureCitation(item.innerHTML);
						if(capturedData){
							item.innerHTML = capturedData;
						}
					}else if($(item).closest(kriya.config.citationSelector).length > 0){
						$(item).closest(kriya.config.citationSelector).attr('data-capturd', 'paste');
					}
					returnVal += (item.nodeType == 3 && item.nodeName == "#text")?item.nodeValue:item.outerHTML;
				});
			}
			
			return returnVal;
		},
		/**
		 * Functoin to get the class and rid for the citation
		 * @param String citRefNodeText -> citation node text
		 * @return Array returnVal -> citation class and rid OR boolean false
		 */
		getCitationData : function(citRefNodeText){
			var returnVal = [];
            citRefNodeText = citRefNodeText.replace(/\u00A0/g, ' ');
            var citeType = '';
            var ridVal = this.getRID(citRefNodeText);
            if(!ridVal){
            	return false;
            }
            ridVal = ' ' + ridVal + ' ';
            if (/Fig/gi.test(citRefNodeText)){
                citeType = 'jrnlFigRef';
                ridVal = ridVal.replace(/RID/g, 'F');
            }else if (/Tab|Tbl/gi.test(citRefNodeText)){
                citeType = 'jrnlTblRef';
                ridVal = ridVal.replace(/RID/g, 'T');
            }else if (/(Suppl|Source).*(movie|video|file|data|code)/gi.test(citRefNodeText)){
                citeType = 'jrnlSupplRef';
            }else if (/movie|video/gi.test(citRefNodeText)){
                citeType = 'jrnlVidRef';
                ridVal = ridVal.replace(/RID/g, 'V');
            }else if (/Eq|Eqn/gi.test(citRefNodeText)){
                citeType = 'jrnlEqnRef';
                ridVal = ridVal.replace(/RID/g, 'E');
            }else if (/(^\d|\d{4})/gi.test(citRefNodeText)){  //If the citation text start with number or text has year then cite type is jrnlBibRef
            	citeType = 'jrnlBibRef';
            	ridVal = ridVal.replace(/RID/g, 'R');
            }

            return [citeType, ridVal];
        },
        getRID : function(citRefNodeText){
        	var ridVal = '';
        	if(citRefNodeText){
				citRefNodeText = citRefNodeText.replace(/[\(\)\[\]]|^\s+|\s+$/g, '');
				//#147 - Citations are copy and pasted up for multiple citations. - Mohammed Navaskhan (mohammednavas.a@focusite.com)
        		citRefNodeText = citRefNodeText.replace(/[a-z\s+]+/ig, ',');        		
				var citeRefArray   = citRefNodeText.split(/[,;\s]/);
				citeRefArray   = citeRefArray.filter(function(v){return v!==''});
				//End of #147
        		for(r=0;r<citeRefArray.length;r++){
        			var refIdVal = citeRefArray[r];
        			if(refIdVal.match(/([0-9]+)(\s+)?([\u2012\u2013\u2014\-])(\s+)?([0-9]+)/g)){
        				var citeRange = refIdVal.split(/([\u2012\u2013\u2014\-])/);
        				var splitLen = citeRange.length;
        				var lastVal = citeRange[splitLen-1];
        				for(cr=citeRange[0];cr<=lastVal;cr++){
        					ridVal += ' RID'+cr;
        				}
        			}else{
        				ridVal += ' RID' + refIdVal;
        			}
        		}
        	}
        	ridVal = ridVal.replace(/^\s+/g, '');
        	return ridVal;
        },
        /**
         * Function to get the alphabetical posistion of the reftext
         * tiny sort used to get the position
         * @param refText string - Reference text
         * @return - Next reference id and Previous reference id 
        **/
        getRefAlphaPosition: function(refId){
        	if(refId){
				$('body').append('<div id="refContent" style="display: none !important;">' + $(kriya.config.containerElm).html() + '</div>');
				var regexArr = {'z':'\u017A\u017C\u017E', 'y': '\u0177\u0178\u024F\u1EF3', 'w': '\u1E81\u1E83\u1E85\u0175', 'u': '\u00F9\u00FA\u00FB\u00FC\u0169\u016B\u016D\u016F\u0171\u0173', 't': '\u0163\u0165\u0167]', 's' : '[\u015B\u015D\u015F\u0161', 'r': '\u0155\u0157\u0159\u024D', 'q': '\u024B', 'o': '\u014D\u014F\u0151\u01A1\u00F2\u00F3\u00F4\u00F5\u00F6', 'n': '\u0144\u0146\u0148\u0149\u014B', 'l': '\u013A\u013C\u013E\u0140\u0142', 'k': '\u0137', 'j': '\u0135', 'i': '\u0129\u012B\u012D\u012F', 'h': '\u0125\u0127', 'g': '\u011D\u011F\u0121\u0123', 'e': '\u00E8\u00E9\u00EA\u00EB\u0119\u04D7', 'd': '\u010F\u0111\u1E11', 'c': '\u00E7\u0107\u0109\u010B\u010D', 'b': '\u03B2\u0432', 'a': '\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5\u00E5\u0105\u0103'};
				$('#refContent .jrnlRefText[data-track="del"], #refContent .jrnlRefText.del').remove();
				$('#refContent .jrnlRefText').find('span.RefAuthor, span.RefCollaboration, span.RefEditor').each(function(){
					$(this).cleanTrackChanges();  //Clean the track changes in author
					if($(this).attr('data-track') == "del" || $(this).hasClass('del')){
						return;
					}
					var textNodes = textNodesUnder(this);
					for(var t=0;t<textNodes.length;t++){
						var node = textNodes[t];
						var nodeValue = node.nodeValue;
						var regKeys = Object.keys(regexArr);
						for(var k=0;k<regKeys.length;k++){
							var regStr = regexArr[regKeys[k]];
							var reg = new RegExp('[' + regStr + ']', 'gi');
							if(reg.test(nodeValue)){
								//console.log('Before update', nodeValue);
								nodeValue = nodeValue.replace(reg, regKeys[k]);
								node.nodeValue = nodeValue;
								//console.log('After update', nodeValue);
								//console.log(nodeValue);
							}
						}
					}
				});

				tinysort('#refContent .jrnlRefText', 'span.RefAuthor,span.RefCollaboration,span.RefEditor', 'span.RefYear');
				var refNode = $('#refContent .jrnlRefText[data-id="' + refId + '"]');
				var citeConfig = citeJS.floats.getConfig(refId);
				var nextNodeID = null;
				var prevNodeID = null;
				if(citeConfig && citeConfig['same-first-author-check'] == "true"){
					var firstAuthortext = refNode.find('.RefAuthor:first').text();
					var firstAuthorLen  =  refNode.find('.RefAuthor').length;
					var isAllLenSame = true;
					var refArr = {};
					refArr[refId]  = refNode.find('.RefAuthor').length;
					refNode.prevAll('.jrnlRefText').each(function(){
						var dataId = $(this).attr('data-id');
						if(firstAuthortext == $(this).find('.RefAuthor:first').text()){
							refArr[dataId] = $(this).find('.RefAuthor').length;
							if(firstAuthorLen != $(this).find('.RefAuthor').length){
								isAllLenSame = false;
							}
						}
					});
					refNode.nextAll('.jrnlRefText').each(function(){
						var dataId = $(this).attr('data-id');
						if(firstAuthortext == $(this).find('.RefAuthor:first').text()){
							refArr[dataId] = $(this).find('.RefAuthor').length;
							if(firstAuthorLen != $(this).find('.RefAuthor').length){
								isAllLenSame = false;
							}
						}
					});
					if(Object.keys(refArr).length > 0 && !isAllLenSame){
						var sortedList = Object.keys(refArr).sort(function(a,b){return refArr[a]-refArr[b]});
						var refInd     = sortedList.indexOf(refId);
						nextNodeID = sortedList[refInd+1];
						prevNodeID = sortedList[refInd-1];
					}
				}
				if(!nextNodeID && !prevNodeID){
					nextNodeID = refNode.next('.jrnlRefText').attr('data-id');
					prevNodeID = refNode.prev('.jrnlRefText').attr('data-id');
				}
				
				$('body').find('#refContent').remove();
				return {'nextId': nextNodeID, 'prevId': prevNodeID};
        	}
        	return false;
        },
		moveFloat: function(citeID, rootElementID){
			if(citeID && $(rootElementID + ' [data-id="' + citeID + '"]').length > 0 && !/\-/.test(citeID)){
				//get the citation nodes skip the deleted
				var citeNode = kriya.xpath('//*[@id="contentDivNode"]//*[contains(@data-citation-string, " ' + citeID + ' ")][not(@data-track="del")][not(ancestor::*[@data-track="del"])][not(ancestor::*[contains(@class, "del")])][not(ancestor::*[contains(@class, "jrnlDeleted")])]');
				if($(citeNode).length > 0 && !$(citeNode).hasClass('jrnlBibRef') && !$(citeNode).hasClass('jrnlFootNoteRef') && !$(citeNode).hasClass('jrnlSupplRef')){
					
					//If excludeCitationInFloats and first citation is with in float block and more than one citation is present in content then get citation not in block
					var filteredCItations = $(citeNode).filter(function(){
						if($(this).closest('[data-id^="BLK_"]').length == 0){
							return this;
						}
						return false;
					});

					if(citeJS.settings.excludeCitationInFloats && filteredCItations.length > 0){
						citeNode = $(filteredCItations).first();
					}else{
						citeNode = $(citeNode).first();
					}
					
					var parentNode = citeNode.closest('[class*="Block"]');
					if (parentNode.length == 0){
						parentNode = $(citeNode).closest('p,h1,h2,h3,h4,h5,h6');
					}
					
					//Get the previous citation node of citeNode in parentNode
					var prevCiteId = null;
					var citeNodes = parentNode.find(kriya.config.citationSelector).not('.jrnlBibRef,.jrnlFootNoteRef,.jrnlSupplRef, [data-track="del"], .del');
					if(citeNodes.length > 0){
						citeNodes.each(function(i){
							i = citeNodes.index(this);
							var citeString = $(this).attr('data-citation-string');
							citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
							if(this == citeNode[0]){
								if(i != 0 && citeNodes.eq(i-1).length > 0 && citeNode[0] != citeNodes.eq(i-1)[0]){
									var prevCiteNode = citeNodes.eq(i-1);
									prevCiteId = $(prevCiteNode).attr('data-citation-string');
									prevCiteId = prevCiteId.replace(/^\s+|\s+$/g, '').split(' ').pop(); //get the last cite id
								}
								if(citeString.length > 1 && citeString.indexOf(citeID) > 0){
									var index = citeString.indexOf(citeID);
									prevCiteId = citeString[index-1];
								}
								return false;
							}else{
								for(var c=0;c<citeString.length;c++){
									var firstNode = $(rootElementID + ' [data-citation-string*="' + citeString[c] + '"]:not([data-track="del"], .del):first');
									if(firstNode.length > 0 && firstNode[0] != this){
										citeNodes = citeNodes.not(this);
									}else if(citeString[c].match(/-/g)){
										citeNodes = citeNodes.not(this);
									}
								}

							}
						});
					}

					//Get the float block node for the prevCiteId
					if(prevCiteId && $(rootElementID + ' [data-id="' + prevCiteId + '"]').closest('[class*="Block"]').length > 0){
						parentNode = $(rootElementID + ' [data-id="' + prevCiteId + '"]').closest('[class*="Block"]');
						//if parentNode has supplement then get last supplement has parentNode
						if($(rootElementID + ' [data-id^="' + prevCiteId + '-"]').closest('[class*="Block"]').length > 0){
							parentNode = $(rootElementID + ' [data-id^="' + prevCiteId + '-"]').closest('[class*="Block"]').last();
						}
					}

					
					var blockNode = $(rootElementID + ' [data-id="' + citeID + '"]').closest('[class*="Block"]');
					
					//If next element of parentNode is not block node then move the float block next to citation parentNode
					if(parentNode.next('[data-id="' + blockNode.attr('data-id') + '"]').length == 0){
						blockNode.insertAfter(parentNode);
						
						blockNode.attr('data-moved', 'true');
						kriyaEditor.settings.undoStack.push(blockNode);

						//Insert the supplement blocks next to the float block
						var supplBlock = $(rootElementID + ' [data-id^="' + citeID + '-"]').closest('[class*="Block"]');
						if (supplBlock.length > 0){
							supplBlock.insertAfter(blockNode);
							supplBlock.attr('data-moved', 'true');
							supplBlock.each(function(){
								kriyaEditor.settings.undoStack.push(this);
							});
						}
					}
				}
			}
		},
		updateStatus: function(){
			var status = citeJS.floats.renumberStatus();
			if(status){
				citeJS.settings.reorderStatus = true;
				$('.citeReorder').removeClass('disabled');				
			}else if(status == false){
				citeJS.settings.reorderStatus = false;
				$('.citeReorder').addClass('disabled');
			}
			this.updateFloatDetails();
		},
		updateFloatDetails: function(){
			$(citeJS.settings.containerElm).find('[data-id^="BLK_"], .jrnlRefText').each(function(){
				var floatId = $(this).attr('data-id');
				if(floatId){
					var citeId = floatId.replace(/BLK_/g, '');
					var citeNodes = $(citeJS.settings.containerElm).find('[data-citation-string*=" ' + citeId + ' "]');
					citeNodes = citeNodes.filter(function(){
						if($(this).closest('.del,[data-track="del"]').length == 0){
							return this;
						}
						return false;
					});
					var citationLength = citeNodes.length;
					if($(this).find('.floatHeader').length > 0){
						//Update count in float header
						$(this).find('.floatHeader .citeCount').text(citationLength);
						//Update float label in header
						var labeltext = $(this).find('.label').clone(true).cleanTrackChanges().html();
						if(labeltext){
							labeltext = labeltext.replace(/[\s\|\.]+$/g, '');
							$(this).find('.floatHeader .floatLabel').html(labeltext);	
						}
					}
					
					//Update count in navigation panel
					$('#navContainer [data-panel-id="' + floatId + '"] .citeCount').text(citationLength);
				}
			});
			//Updated uncited count length
			var floatClassArr = Object.keys(citeJS.settings.citeClass);
			for(var f=0;f<floatClassArr.length;f++){
				var className = floatClassArr[f];
				var uncitedObj = $('#navContainer [data-class="' + className + '"]').parent().find('.uncited-object-btn');
				var curText = uncitedObj.find('.notify-badge').text();
				if(uncitedObj.length > 0){
					var uncitedLen = $(citeJS.settings.containerElm).find('.' + className + '[data-uncited]:not([data-track="del"],.del)').length;
					if(uncitedObj.find('.notify-badge').length == 0 && uncitedLen != 0){
						uncitedObj.append('<span class="notify-badge"/>');
					}else if(uncitedLen == 0){
						uncitedObj.find('.notify-badge').remove();
					}
					
					if(curText > uncitedLen){
						uncitedObj.trigger('click');
					}
					uncitedObj.find('.notify-badge').text(uncitedLen);	
				}
			}
		}
	}
})(citeJS || {});
