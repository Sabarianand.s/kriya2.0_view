var probeRules = function() {
	//default settings
	var settings = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend probeRules function with event handling
*/
(function (probeRules) {
	settings = probeRules.settings;
	
	probeRules.actions = {
		validate: function(el) {
			var xmlContent = $('<xml-probe><rule id="e002"><expression>//*[@class="jrnlFigCaption"]</expression><message>email tag should not contain whitespace</message></rule><rule id="e002"><function>validateCitations</function><message>email tag should not contain whitespace</message></rule></xml-probe>');
			if ($('#xmlProbe').length > 0){
				$.ajax({
					type: "GET",
					url: "/config/html-validation.xml",
					dataType: "xml",
					success: function(response) {
						var xmlProbe = $('<div id="xmlProbe">' + response + '</div>');
						probeRules.actions.validate();
					}
				});
				return false;
			}
			var rules = xmlContent.find('rule');
			$(rules).each(function(){
				var rule = $(this);
				var nodes = false;
				if (rule.find('expression').length > 0){
					nodes = kriya.xpath(rule.find('expression').text());
				}
				if (rule.find('function').length > 0){
					functionName = rule.find('function').text();
					var params = false;
					if (rule.find('params').length > 0){
						params = rule.find('params').text();
						params = params.split(',');
					}
					if (typeof(window['probeRules']['actions'][functionName]) == "function"){
						window['probeRules']['actions'][functionName](nodes, params);
					}
				}
			});
		},
		validateCitations: function(){
			var citationNodes = $('#contentDivNode [data-citation-string]');
			$(citationNodes).each(function(){
				var dataCites = $(this).attr('data-citation-string');
				dataCites = dataCites.replace(/^\s|\s$/g, '');
				dataCites = dataCites.split(' ');
				$.each(dataCites, function(k, v){
					if ($('#contentDivNode [id="' + v + '"]').length == 0){
						console.log(v);
					}
				});
			});
			
			var floatNodes = $('#contentDivNode').find('.jrnlRefText,[class*="Block"]');
			$(floatNodes).each(function(){
				var floatID = $(this).attr('id');
				floatID = floatID.replace(/^.*_/, '');
				if ($('#contentDivNode [data-citation-string*=" ' + floatID + ' "]').length == 0){
					console.log(floatID);
					var qid = eventHandler.query.action.createQueryDiv('Author', 'info');
					var queryContent = $('#' + qid).find('.query-content');
					$(queryContent).html('No citation found');
				}
			});
		},
	}
	return probeRules;

})(probeRules || {});