function initReportTable() {
	var filtersConfig = {
        base_path: '/js/libs/tablefilter/',
				grid_layout: true,
      	grid_width: '100%',
				responsive: true,
        auto_filter: true,
        auto_filter_delay: 1100, //milliseconds
        filters_row_index: 1,
        alternate_rows: true,
	paging: false,
        state: {
          types: ['local_storage'],
          filters: false,
          page_number: false,
          page_length: false,
          sort: false
        },
        results_per_page: ['Records: ', [20,35, 50, 100]],
	rows_counter: true,
 	loader: true,
        loader_html: '<div id="lblMsg"></div>',
        loader_css_class: 'myLoader',
        status_bar: true,
        status_bar_target_id: 'lblMsg',
        status_bar_css_class: 'myStatus',
        btn_reset: true,
        status_bar: true,
	mark_active_columns: true ,
        /* columns visibility and sort extension */
        extensions: [{
              name: 'colsVisibility',
              text: 'Columns: ',
              enable_tick_all: true
          }, {
              name: 'sort'
        }]
    };
	var basicTatTF = new TableFilter('stageTable',filtersConfig);
  basicTatTF.init();
	$('#dataContainer').height($(window).height() - $('#headerContainer').height() - $('#footerContainer').height());
$('#articleReports').height($(window).height() - $('#headerContainer').height() - $('#footerContainer').height());
	$('.preloader-wrapper').removeClass('active');
}

$(document).ready(function () {
	//$('#articleReports').html(window.opener.document.getElementById('articleReports').innerHTML);
	if($('#articleReports').html()) initReportTable();
	$('.reportLink').on('click', function(){
		$('.reportLink').removeClass('active');
		$(this).addClass('active');
	})
});

function export_report() {
  
  var wb = new Workbook();    
  var theTable = document.getElementById('stageTable');
  theTable = theTable.cloneNode(true);

  var tableHead = document.querySelectorAll('.grd_headTblCont table thead');
  tableHead = tableHead[0].cloneNode(true);
  theTable.insertBefore(tableHead, theTable.firstChild);

  var oo = generateArray(theTable);
  var ranges = oo[1];

  /* original data */
  var data = oo[0]; 
  var ws_name = 'stageTable';
  console.log(data);

  var ws = sheet_from_array_of_arrays(data);

  /* add ranges to worksheet */
  ws['!merges'] = ranges; 

  /* add worksheet to workbook */
  wb.SheetNames.push(ws_name);
  wb.Sheets[ws_name] = ws;
    
  var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:false, type: 'binary'});

  saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'report.xlsx')
  
}