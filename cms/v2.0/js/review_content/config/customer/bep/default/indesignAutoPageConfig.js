﻿var config = {
    "defaultUnits":"pt",
    "baseLeading":15,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"],
    "floatTypeOnFirstPage":"KEY",    
    "minStackSpace":18,
    "placeStyle":"Sandwich",
    "minNoLinesOnPag":3,
    "applyTableLeftRightBorder":false,   
     "pageSize":{
             "width":432,
             "height":648
        },
    "breakingSectionHeads":{
        "jrnlRefHead":{
            "startAt":"frame",
            "styleoverrides":{
                "startParagraph":1313235563
                
                }
            }
        },
    "exportPDFpreset":{
        "print":{
            "crop_marks":true, 
            "bleed_marks":true, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":18.5
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":12,
      "right":12,
      "gutter":14.173
    },
"geoBoundsVerso":[66,54,600,348
    ],
    "geoBoundsRecto":[66,516,600,810
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1"
    },    
    "pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
                  {"width":294, "height":516,"floatsCited":false,"gutter":0}
                ],
                "columnDetails":[
                  {"width":294, "height":534,"floatsCited":false,"gutter":0}
                ],
                "openerPageMargin":{
                 "top":84,
                "bottom":49,
                "inside":84,
                "outside":54
                },
            "otherPageMargin":{
                 "top":66,
                "bottom":49,
                "inside":84,
                "outside":54
                }
            }
        },
    "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":340.85, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,jrnlFundingHead,jrnlEqualContribFNHead,BL-T,NL-T,BL-O,NL-O,EQN-O,EQN-T,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,EQN-O,EQN-B,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,EQN-O,EQN-T,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,EQN-O,EQN-B,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsHead,jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,jrnlHead5",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":40,
            "maximum":80
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],
    
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlSTRH']", "frame-name":"jrnlSTRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlARH']", "frame-name":"jrnlARH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlJVH']", "frame-name":"jrnlJVH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlJT']", "frame-name":"jrnlJT", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlDefBlock']", "frame-name":"ABBREVATION", "action":"move", "styleOverride":null}
         ]       
     },
    "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"35,31,32"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"35,31,32"
                    }
                }            
            }
        },
         "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "10px",
                    "font-family" : "'Goudy', Goudy"
                },
                "fontPath"  : "Goudy/Goudy.ttf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'Goudy-Bold',GoudyBold'",
                    "font-size" : "10px",
                    "line-height" : "12px",
                    "padding" : "4px 4px 4px 4px",
                },
                "fontPath"  : "Goudy/Goudy-Bold.ttf",
                "align" : "left",
                "valign": "top"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'Goudy', Goudy",
                    "font-size" : "10px",
                    "line-height" : "12px",
                    "padding" : "4px 4px 4px 4px",
                },
                "fontPath"  : "Goudy/Goudy.ttf",
                "align" : "left",
                "valign": "top"
            }
        },
        "equation" : {
          "default" : {
              "page":{
                  "size" : "294pt"
              },
              "font" : {
                  "size"    : "10.5pt",
                  "ledding" : "15pt",
                  "path"    : "/AGaramondPro-Regular/",
                  "ext"     : "otf",
                  "bold"    : "AGaramondPro-Bold",
                  "italic"  : "AGaramondPro-Italic",
                  "bold_italic":"AGaramondPro-BoldItalic",
                  "main"    : "AGaramondPro-Regular"
              }
           }
    },
    "relinkFigures":['LOGO_1.eps'],
    "stubColObj":{
        "top": {
            "STUB_COLUMN": false,
            "STUB_STMT": false
        },
        "bottom": {
            "METAINFO": true,
            "CROSSMARK": false,
            "RELATED_ARTICLE_INFO": true
        }
    }
}
this.config = config;