var kriyaEditor = function() {
	//default settings
	var settings = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (kriyaEditor) {
	settings = kriyaEditor.settings;
	var timer;
	var structedData;
	kriyaEditor.user = {};
	kriyaEditor.user.name = $('#headerContainer .userProfile .username').text();
	kriyaEditor.user.id   = $('#headerContainer .userProfile .username').attr('data-user-id');
	
	kriyaEditor.init = {
		loadImage: function (){
			var imageNodes = document.querySelectorAll('img');
			for (var t = 0;t<imageNodes.length;t++) {
				var imageNode = imageNodes[t];
				var orginalImage = imageNode.getAttribute('data-alt-src');
				if(orginalImage){
					imageNode.setAttribute('src', orginalImage);	
				}
			}
		},
		editor: function(dataNode) {
			return new wysihtml.Editor(dataNode, {
				//toolbar: "toolbar",
				parserRules: wysihtmlParserRules,
				useLineBreaks: false,
				autoLink: false
			});
		},
		trackArticle: function(){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return true;
			}
			if ((typeof(timeTicker) != "undefined" && timeTicker.isRunning()) || typeof(timeTicker) == "undefined"){
				var parameters = {
					'customerName': kriya.config.content.customer,
					'projectName': kriya.config.content.project,
					'doi': kriya.config.content.doi,
					'skipElasticUpdate': 'true'
				};
				var d = new Date();
				var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time></log></job-logs></stage>';
				kriya.general.sendAPIRequest('updatedatausingxpath', parameters, function(res){});
				setTimeout(function(){
					kriyaEditor.init.trackArticle();
				}, 120000);
			}
		},
		undoRedo: function(dataNode){
			var x = document.querySelector.bind(document);
			kriyaEditor.settings.undoStack = [];
			kriyaEditor.settings.contentNode = dataNode;
			kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
			kriyaEditor.settings.changeNode = document.getElementById('navContainer');
			kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
			kriyaEditor.settings.forkedNode = false;
			var contentUndoObject = {
				container: kriyaEditor.settings.contentNode,
				oldValue: kriyaEditor.settings.contentNode.innerHTML,
				newValue: ''
			}
			
			kriyaEditor.settings.undo = x('[data-tooltip="Undo"]');
			kriyaEditor.settings.redo = x('[data-tooltip="Redo"]');
			kriyaEditor.settings.save = x('[data-tooltip="Save"]');
	
			stack = new Undo.Stack();
			stack.isDirty = false;
			var newValue = "";
				
			/**
			*	hook key-up events to track the changes made to content
			*
			*/
			var keysPressed = [];
			kriyaEditor.settings.contentNode.addEventListener('keyup', function(event){
				//keys[event.keyCode] = false;
				var index = keysPressed.indexOf(event.key.toLocaleLowerCase());
				keysPressed.splice(index, 1);
				var range = rangy.getSelection();
				var targetElement = range.anchorNode;
				if (targetElement == undefined) return;
				kriyaEditor.lastSelection = range;
				switch (event.keyCode) {
					case 46: // delete
					case 8: // backspace
						kriyaEditor.init.addUndoLevel('delete-key', targetElement);
						break;
					case 32: // space
						//console.log('add undo level called, space keys pressed');
						kriyaEditor.init.addUndoLevel('space-key', targetElement);
						break;
					case 33: // page up
					case 34: // page down
					case 35: // end
					case 36: // home
					case 37: // arrow key: left
						if (range.text() != '') kriya.selection = range.getRangeAt(0);
					case 38: // arrow key: up
						if (range.text() != '') kriya.selection = range.getRangeAt(0);
					case 39: // arrow key: right
						if (range.text() != '') kriya.selection = range.getRangeAt(0);
					case 40: // arrow key: down
						if (range.text() != '') kriya.selection = range.getRangeAt(0);
						if (stack.isDirty){
							//console.log('add undo level called, navigation keys pressed');
							kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
						}
						break;
					default:
						var keycode = event.keyCode;
						if (
							(keycode > 47 && keycode < 58)   || // number keys
							keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
							(keycode > 64 && keycode < 91)   || // letter keys
							(keycode > 95 && keycode < 112)  || // numpad keys
							(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
							(keycode > 218 && keycode < 223)   // [\]' (in order)
							){
								if (! event.ctrlKey){
									//console.log('add undo level called, character keys pressed');
									if (keycode == 13 && kriyaEditor.settings.forkedNode && ( kriyaEditor.settings.forkedNode.hasAttribute('id') || kriyaEditor.settings.forkedNode.hasAttribute('data-id'))){
										if (targetElement.nodeType == 3){
											targetElement = targetElement.parentNode;
										}
										while (kriya.config.blockElements.indexOf(targetElement.nodeName.toLocaleLowerCase()) == -1){
											targetElement = targetElement.parentNode;
										}
										if ($(targetElement).hasClass('jrnlRefText') || $(targetElement).hasClass('jrnlFootNotePara')){
											if($(targetElement).hasClass('jrnlRefText')){
												var className = "jrnlRefText";
												var refclass= "jrnlBibRef"
												var rid= "R";
											}else{
												var className = "jrnlFootNotePara";
												var refclass= "jrnlFootNoteRef"
												var rid= "BFN";
											}
											//set the data-id for the splitted reference - Jai - 07-Dec-2018
											var refArray = [];
											$(kriya.config.containerElm + ' .'+className).not(targetElement).each(function(){
												refArray.push($(this).attr('data-id').replace(/[a-z]+/gi, ''));
											});
											var max = Math.max.apply( Math, refArray );
											//when we enter first ref at that time it giving as a -infinite-kirankumar
											if(max == -Infinity){
												var refId = rid+'1';
											}else{
												var refId = rid + (max + 1);
											}
											$(targetElement).attr('id', refId);
											$(targetElement).attr('data-id', refId);
											if(className == "jrnlFootNotePara"){
												var rcount = 1;  
												$(kriya.config.containerElm + ' .'+className).each(function(){
													var oldId = $(this).attr('data-id');
													var newID = rid + rcount
													if(newID != oldId){
														$(kriya.config.containerElm + ' .'+refclass+'[data-citation-string*=" '+ oldId +' "]').each(function(){
															var cid = $(this).attr('data-citation-string');
															cid = cid.replace(" "+ oldId +" ", " " + rid + rcount+ " ");
															$(this).attr('data-citation-string', cid);
														})
														
													}
													$(this).attr('id', newID);
													$(this).attr('data-id', newID);
													rcount++;
												});
											}
										}else{
											$(targetElement).attr('id', uuid.v4());
											$(targetElement).removeAttr('data-id');
										}
										if ($(kriyaEditor.settings.forkedNode).hasClass('activeElement')){
											$(kriyaEditor.settings.forkedNode).removeClass('activeElement');
										}
										kriyaEditor.init.addUndoLevel('navigation-key', [kriyaEditor.settings.forkedNode, targetElement]);
										kriyaEditor.init.save();
										kriyaEditor.settings.forkedNode = false;
									}else{
										kriyaEditor.init.addUndoLevel('character-key', targetElement);
									}
								}
							}
				}
				
				//add/remove track changes to the side panel
				if (range.anchorNode.nodeType == 3){
					var parentNode = range.anchorNode.parentNode;
				}else if(kriya.selection && kriya.selection.startOffset != kriya.selection.endOffset){
				//Get the tracked elements when select the text and press delete or backspace key
					var parentNode = kriya.selection.startContainer.nextElementSibling;
				}else{
					var parentNode = range.anchorNode;
				}
				if ($(parentNode).hasClass('del') || $(parentNode).hasClass('ins')){
					//eventHandler.menu.observer.addTrackChanges(parentNode, 'added');
					//addUndoLevel('change', 'changesDivNode');
				}
				if (parentNode && ($(parentNode.nextElementSibling).hasClass('del') || $(parentNode.nextElementSibling).hasClass('ins'))){
					//Add track changes for the inserted and deleted element when select the text and start typing
					//eventHandler.menu.observer.addTrackChanges(parentNode.nextElementSibling, 'added');
				}
				if ($(range.anchorNode.nextElementSibling).hasClass('del') || $(range.anchorNode.nextElementSibling).hasClass('ins')){
					//Add track change if the next sibling is tracked Ex: when ctrl+x  pressed the range will be next of the tracked element
					//eventHandler.menu.observer.addTrackChanges(range.anchorNode.nextElementSibling, 'added');
				}

				var underlayNode = $('#clonedContent #' + $(parentNode).closest('*[clone-id]').attr('clone-id'));
				if (underlayNode.length > 0 && $(underlayNode).find('.highlight').length > 0){
					eventHandler.menu.findReplace.reSearch(underlayNode);
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('keydown', function(event){
				//console.log(rangy.getSelection().anchorNode.parentNode);
				var range = rangy.getSelection();
				var targetElement = range.anchorNode;
				if (targetElement == undefined) return;
				if (targetElement.nodeType == 3){
					targetElement = targetElement.parentNode;
				}
				var keycode = event.keyCode;
				if (
				(keycode > 47 && keycode < 58)   || // number keys
				keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
				keycode == 46 || keycode == 8   || // spacebar & return key(s) (if you want to allow carriage returns)
				(keycode > 64 && keycode < 91)   || // letter keys
				(keycode > 95 && keycode < 112)  || // numpad keys
				(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
				(keycode > 218 && keycode < 223)   // [\]' (in order)
				){
					if ((event.ctrlKey && (! /^(X|V)$/i.test(event.key))) || event.altKey || keycode == 13){
					}else{
						event.preventDefault();
						event.stopPropagation();
					}
				}
				if (keycode == 13){
					while (kriya.config.blockElements.indexOf(targetElement.nodeName.toLocaleLowerCase()) == -1){
						targetElement = targetElement.parentNode;
					}
					kriyaEditor.settings.forkedNode = targetElement;
				}
				
				
				// short cut key controls
				var sk = "";
				if (event.ctrlKey) sk += "ctrl+";
				else if (event.altKey) sk += "alt+";
				else if (event.shiftKey) sk += "shift+";
				if (! /(control|alt|shift)/i.test(event.key)){
					sk += event.key;
				}

				if ($('[data-shortcut="'+sk+'"]').length > 0){
					if ($('[data-shortcut="'+sk+'"]').attr('data-action') != undefined){
						var action = $('[data-shortcut="'+sk+'"]').attr('data-action');
						if (action == "focus"){
							$('[data-shortcut="'+sk+'"]').focus();
						}else{
							$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
						}
					}else{
						$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
					}
					event.preventDefault();
					event.stopPropagation();
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('mouseleave', function(event){
				var range = rangy.getSelection();
				if (range.rangeCount > 0 && range.text() != ""){
					var r = range.getRangeAt(0).startContainer.parentElement
					if ($(r).closest('#contentDivNode').length > 0){
						//kriya.selection = range.getRangeAt(0);
						//kriya.selectedNodes = kriya.selection.getNodes();
						//findReplace.general.highlight(range, 'textSelection');
					}
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('click', function(event){
				if(stack.isDirty && kriyaEditor.lastSelection){
					var targetElement = kriyaEditor.lastSelection.anchorNode;
					kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
				}
				$('.highlight.textSelection,.highlight.queryText').remove();
				$('.dropdown-button').dropdown('close');
				if($(event.target).closest('a').length > 0 && $(event.target).closest('a').attr('href')){
					window.open($(event.target).closest('a').attr('href'));
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('focusout', function(event){
				if(stack.isDirty && kriyaEditor.lastSelection){
					var targetElement = kriyaEditor.lastSelection.anchorNode;
					kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
				}
			});

			function stackUI() {
				if(stack.canRedo()){
					kriyaEditor.settings.redo.classList.remove("disabled")
				}else{
					kriyaEditor.settings.redo.classList.add("disabled")
				}
				
				if(stack.canUndo()){
					kriyaEditor.settings.undo.classList.remove("disabled");
					kriyaEditor.settings.save.classList.remove("disabled");
				}else{
					
					kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
					kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;

					kriyaEditor.settings.undo.classList.add("disabled");
					//kriyaEditor.settings.save.classList.add("disabled");
				}
			}
			
			kriyaEditor.settings.undo.addEventListener("click", function () {
				var range = rangy.getSelection();
				var targetElement = range.anchorNode;
				if (stack.isDirty){
					kriyaEditor.init.addUndoLevel('undo-key', targetElement);
				}
				stack.undo();
			});

			kriyaEditor.settings.redo.addEventListener("click", function () {
				stack.redo();
			});

			stack.changed = function () {
				stackUI();
			};	
		},
		addUndoLevel: function(type, element){
			var stackObj = [];
			
			var newElement = null;
			var oldValue   = null;
			var newValue   = null;
			
			if (kriyaEditor.settings.undoStack.length > 0){
				element = kriyaEditor.settings.undoStack;
			}
			kriyaEditor.settings.undoStack = [];
			if(element && $(element).length > 0){
				$(element).each(function(i, obj){
					if($(this).closest('[id]').length > 0){
						if ($(this)[0].nodeType == 1 && $(this)[0].hasAttribute('id')){
							newElement = $(this)[0];
						}else{
							newElement = $(this).closest('[id]')[0];
						}
						var newEleID   = newElement.getAttribute('id');
						var oldElement = $('<div>' + kriyaEditor.settings.startValue + '</div>').find('#'+newEleID);
						oldValue   = (oldElement.length > 0) ? oldElement[0].innerHTML : null;
						newValue   = newElement.innerHTML;
						stackObj.push({container: newElement,oldValue : oldValue,newValue : newValue});
					}else{
						console.log("element not found to add in stack");
					}
				});
				stackObj.push({'saveNodes': element});
			}
			newElement = kriyaEditor.settings.contentNode;
			oldValue   = kriyaEditor.settings.startValue;
			newValue   = newElement.innerHTML;
			stackObj.push({container: newElement,oldValue : oldValue,newValue : newValue});
			
			//Adding right panel in the stack
			stackObj.push({
				container: kriyaEditor.settings.changeNode,
				oldValue : kriyaEditor.settings.contextStartValue,
				newValue : kriyaEditor.settings.changeNode.innerHTML,
				save     : false
			});

			//add clone element in the stack
			stackObj.push({
				container: kriyaEditor.settings.cloneNode,
				oldValue : kriyaEditor.settings.cloneValue,
				newValue : kriyaEditor.settings.cloneNode.innerHTML,
				save     : false
			});

			/*if(element && $(element).closest('[id]').length > 0){
				var newElement = $(element).closest('[id]')[0];
				var newEleID   = newElement.getAttribute('id');
				var oldElement = $(kriyaEditor.settings.startValue).find('#'+newEleID);
				var oldValue   = (oldElement.length > 0) ?oldElement[0].innerHTML:null;
				var newValue   = newElement.innerHTML;
			}else{
				var newElement = kriyaEditor.settings.contentNode;
				var oldValue   = kriyaEditor.settings.startValue;
				var newValue   = newElement.innerHTML;
			}*/

			var EditCommand = Undo.Command.extend({
				constructor: function (undoObj) {
					this.undoObject = undoObj;
				},
				execute: function () {
					this.undoObject.forEach(function(item, index, array){
						if(item.saveNodes){
							//kriyaEditor.init.save(item.saveNodes);
						}
					});
				},
				undo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(item.saveNodes){
							//kriyaEditor.init.save(item.saveNodes);
						}else{
							item.container.innerHTML = item.oldValue;
						}
					});
				},
				redo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(item.saveNodes){
							//kriyaEditor.init.save(item.saveNodes);
						}else{
							item.container.innerHTML = item.newValue;
						}
					});
				},
			});
			if ((type == 'delete-key') || (type == 'character-key')) {
				stack.isDirty = true;
				stack.oldValue = oldValue;
				stack.newValue = newValue;
				(stack.isDirty)?(
					kriyaEditor.settings.undo.classList.remove("disabled"),
					kriyaEditor.settings.save.classList.remove("disabled")
				):(
					kriyaEditor.settings.undo.classList.add("disabled"),
					kriyaEditor.settings.save.classList.add("disabled")
				);
			}else{
				stack.execute(new EditCommand(stackObj));
				
				kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
				kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;
				kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
				stack.isDirty = false;
			}
			return;
		},
		save: function(modal){
			if(kriya.config.content.doi){
				if (modal) $('.la-container').fadeIn();
				var content_element = document.createElement('body');
				content_element.innerHTML = kriyaEditor.settings.contentNode.innerHTML;
				var parameters = {
					'customerName':kriya.config.content.customer, 
					'project' : kriya.config.content.project,
					'doi':kriya.config.content.doi, 
					'content':encodeURIComponent(content_element.outerHTML)
				};
				kriya.general.sendAPIRequest('save_html_content',parameters,function(res){
					$('.la-container').fadeOut();
					kriyaEditor.settings.save.classList.add("disabled");
					if(modal && typeof(modal) == "object" && typeof(modal.funcName)=="function"){
						eval(modal.funcName)(modal.funcParam);
					}
				},function(res){
					$('.la-container').fadeOut();
					console.log('ERROR:saveing content.'+res);
				});
			}
		},
		approveStyling: function(){
			// remove the class used to show hints for applied spans in table
			$('#contentDivNode table.showSpanHint').removeClass('showSpanHint');
			var validated = kriyaEditor.init.validateBeforeApprove();
			if (validated){
				kriya.notification({
					title : 'ERROR',
					type  : 'error',
					content : validated,
					icon : 'icon-warning2'
				});
				return false;
			}
			$('.pre-load-container .pre-loader-head').html('Approving to Review Content');
			$('.pre-load-container').fadeIn();
			kriyaEditor.init.probeValidation(kriyaEditor.init.approveToNextStage);
		},
		restoreStructuredData: function(){
			if(structedData){
				$('div#contentContainer').html(structedData);
				kriyaEditor.settings.contentNode = $('div#contentDivNode')[0];
				kriyaEditor.init.save('true');
				$('*[data-name="ApproveStyling"]').addClass('hidden');
				$('*[data-name="ApplyHouseRules"]').removeClass('hidden');
			}
			$('*[data-name="restoreStructuredData"]').addClass('hidden');
		},
		approveToNextStage: function(update){
			var updatexml = update;
			$('.pre-load-container').fadeIn();
			eventHandler.components.general.closePopUp('', $('[data-component="PROBE_VALIDATION_edit"]'));
			//save it before converting it to xml, so we dont miss any content - jai 14-12-2017
			if(kriya.config.content.doi){
				$('.pre-load-container .pre-loader-message').html('XML conversion is in progress');
				$('.query-div[data-xml-xpath]').appendTo('.front');
				var content_element = document.createElement('body');
				content_element.innerHTML = kriyaEditor.settings.contentNode.innerHTML;
				var parameters = {
					'customerName':kriya.config.content.customer, 
					'project' : kriya.config.content.project,
					'doi':kriya.config.content.doi, 
					'content':encodeURIComponent(content_element.outerHTML)
				};
				kriya.general.sendAPIRequest('save_html_content',parameters,function(res){
					var content_element = document.createElement('content');
					content_element.innerHTML = kriyaEditor.settings.contentNode.innerHTML;
					var parameters = {'customerName':kriya.config.content.customer, 'projectName':kriya.config.content.project, 'doi':kriya.config.content.doi, 'content':encodeURIComponent(content_element.outerHTML)};
					if(updatexml){
						parameters['xmlupdate'] = 'true';
					}
					kriya.general.sendAPIRequest('htmltoxml',parameters,function(res){
						if (res && res.status == "200"){
							if(updatexml){
								var parameters = {
									'customer': kriya.config.content.customer,
									'project' : kriya.config.content.project,
									'doi'     : kriya.config.content.doi,
									'xpath'   : '//workflow/stage[name[.="Pre-editing"]]/job-logs'
								};
								keeplive.sendAPIRequest('getdatausingxpath', parameters, function(res){
									if(res && res != ''){
										var stageTime = 0;
										var logs = $(res).find('log');
										$(logs).each(function(){
										var startDate = new Date($(this).find('start-date').text()+' '+$(this).find('start-time').text());
										var endDate = new Date($(this).find('end-date').text()+' '+$(this).find('end-time').text());
										var diff = endDate.getTime() - startDate.getTime();
										 stageTime += Math.floor(diff / 1000);

										})
										if(stageTime/60 > 60){
											var stageTime = Math.floor(stageTime/60/60)+'H:'+Math.floor(stageTime/60 % 60)+'M:'+Math.floor(stageTime % 60)+'S';
										}else{
											stageTime = Math.floor(stageTime/60)+'M:'+Math.floor(stageTime%60)+'S';
										}
										//it will calculate total time taken for StructureContent-kirankumar
										var parameters = {
											'customer': kriya.config.content.customer,
											'project' : kriya.config.content.project,
											'doi'     : kriya.config.content.doi,
											'type' : "tracker"
										};
										parameters.logMsg = "<div><p>StructureContent taken time :</p><p>"+stageTime+"</p></div>";
										keeplive.sendAPIRequest('logerrors', parameters, function(res){
										});
									}
								});
								kriyaEditor.init.validationlog();
								kriyaEditor.init.saveBackup('approved');
								kriyaEditor.init.validationlog('kriya_raw');
								setTimeout(function(){
									window.location.href = window.location.href.replace('structure_content', 'review_content');
								}, 2000);
							}else{
								var parameters  = {
									'doi': kriya.config.content.doi,
									'customer': kriya.config.content.customer,
									'rules': 'kriya_raw',
									'project':kriya.config.content.project,
									'role':'preeditor',
									'stage':'preeditor',
									'content':res.content
								};
								kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
									if(res.error){
										$('.pre-load-container').fadeOut();
										kriya.notification({
											title: 'Failed',
											type: 'error',
											content: 'Validation Failed. Please contact support.',
											timeout: 8000,
											icon : 'icon-info'
										});
									}else if(res){
										res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); //change self closed column tag to open and close tag
										var popper = $('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]');
										var resNode = $(res);
										var stataus = true;
										if((resNode.find('message').length > 0) && (resNode.find('message signOff-allow:contains("false")').length > 0)){
											$('.pre-load-container').fadeOut();
											var container = $('<div />');
											var ruleTypes = [];
											var styleString = '';
											resNode.find('rule-type').each(function(){
												var ruleType = $(this).text();
												if(ruleTypes.indexOf(ruleType) < 0){
													ruleTypes.push(ruleType);
													var msgLength = $(this).closest('probe-result').find('rule-type:contains(' + ruleType + ')').length;
													var tabeNode = $('<input id="_probe_' + ruleType + '_" type="radio" name="tabs_probe"/><label for="_probe_' + ruleType + '_">' + ruleType + '<span class="badge">' + msgLength + '</span></label>');
													if(ruleTypes.length == 1){
														tabeNode.filter('input[type="radio"]').attr('checked', 'true');
													}
													container.append(tabeNode);
												}
											});
											for(var r=0;r<ruleTypes.length;r++){
												var tabId = 'probe_' + ruleTypes[r];
												var ruleDiv = $('<div id="' + tabId + '" class="carousel-item" />');
												container.append(ruleDiv);

												styleString += '#compDivContent #_' + tabId + '_:checked ~ #' + tabId + ',';
											}
											styleString = styleString.replace(/\,$/g, '');
											styleString += '{display:block;}';
											var styleNode = $('<style>' + styleString + '</style>');
												resNode.find('message').each(function(){													
												resNode.find('rule-type').each(function(){
													var ruleType = $(this).text();
													if(ruleTypes.indexOf(ruleType) < 0){
														ruleTypes.push(ruleType);
														var msgLength = $(this).closest('probe-result').find('rule-type:contains(' + ruleType + ')').length;
														var tabeNode = $('<input id="_probe_' + ruleType + '_" type="radio" name="tabs_probe"/><label for="_probe_' + ruleType + '_">' + ruleType + '<span class="badge">' + msgLength + '</span></label>');
														if(ruleTypes.length == 1){
															tabeNode.filter('input[type="radio"]').attr('checked', 'true');
														}
														container.append(tabeNode);
													}
												});
												var ruleType = $(this).find('rule-type').text();
												var errorId = $(this).find('error-id').text();
												var queryTo = $(this).find('query-role').text();

												var row = $('<div class="row" />');
												var left = $('<div class="col s2" />');
												var right = $('<div class="col s10" />');
												
												if(ruleType == "DTD-Validator"){
													left.append('<p> Rule ID: ' + $(this).find('rule-id').text() + '</p>');
													left.append('<p> Line: ' + $(this).find('line').text() + '</p>');
													left.append('<p> Column: ' + $(this).find('column').text() + '</p>');	
													row.append(left);
												}else if(ruleType == "Schematron-Validator"){
													left.append('<p> Error ID: ' + errorId + '</p>');
													row.append(left);
												}
												right.append('<p>' + $(this).find('probe-message').text() + '</p>');
												right.append('<pre>' + $(this).find('xpath').text() + '</pre>');
												if($(this).find('text').text() != ""){
													right.append('<pre><b>' + $(this).find('text').text() + '</pre></b>');
												}else if($(this).find('xml').text() != ""){
													right.append('<pre><b>' + $(this).find('xml').text() + '</pre><b>');
												}
												row.append(right);
												container.find('#probe_' + ruleType).append(row);
											});

											popper.find('.probeResultContainer').html(container);
											popper.find('.probeResultContainer').append(styleNode);

											$(popper).removeClass('hidden');
											$(popper).attr('style','top: 0px; left: 0px; width: 100%; height: 100%; opacity: 1;')

											if(kriyaEditor.settings.undoStack && kriyaEditor.settings.undoStack.length > 0){
												kriyaEditor.init.addUndoLevel('probe-validation');
											}
											return false;
										}else{
											kriyaEditor.init.approveToNextStage('true')
										}
									}
								}, function(err){
									$('.pre-load-container').fadeOut();
										kriya.notification({
											title: 'Failed',
											type: 'error',
											content: 'Validation Failed. Please contact support.',
											timeout: 8000,
											icon : 'icon-info'
										});
									});
							}
						}else{
							$('.pre-load-container').fadeOut();
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Approve Failed. Please contact support.',
								timeout: 8000,
								icon : 'icon-info'
							});
						}
						},function(res){
							$('.pre-load-container').fadeOut();
							console.error('ERROR:saving content.'+res);
						});
				},function(res){
					$('.pre-load-container').fadeOut();
					console.log('ERROR:saveing content.'+res);
				});
			}
		},
		validateBeforeApprove: function (){
			var validatedMessage = '';
			if ($(kriya.config.containerElm).find('.jrnlRefText').length > 0 && $(kriya.config.containerElm).find('.jrnlRefText:first').closest('.jrnlRefGroup').length == 0){
				validatedMessage += '<p>References are not grouped</p>';
			}if ($(kriya.config.containerElm).find('div:not(".query-head") > span:not(".jrnlDeleted, .del"):not([data-track="del"])').length > 0 || $(kriya.config.containerElm).find('> *:not(".body, .back, .front, #queryDivNode")').length > 0){
				validatedMessage += '<p>Tagging pattern incorrect. Please contact support team</p>';
			}
			$(kriya.config.containerElm).find('.jrnlCorrAff').each(function(){
				var rid = $(this).attr('data-id');
				var clone = $(this).clone(true);
				clone.find('span[class]').remove();
				clone.text(clone.text().replace(/[\,\:\;\s]+|E-?mail/g, ''))
				if (/[a-z]/i.test(clone.text())){
					validatedMessage += '<p data-clone-id="' + rid +'">Corresponding information are untagged</p>';
				}
				if ($('.jrnlAuthorGroup *[rid="' + rid + '"]').length == 0){
					validatedMessage += '<p data-clone-id="' + rid +'">Corresponding information is not linked</p>';
				}
			});
			
			//Commentde by jagan action queries will only add in review_content page
			/*$(kriya.config.containerElm).find('.jrnlRefText').each(function(){
				var rid = $(this).attr('id');
				if ($('*[data-citation-string*=" ' + rid + '"]').length == 0 && $(this).find('.jrnlQueryRef[data-query-action="uncited-reference"]').length == 0){
					validatedMessage += '<p data-clone-id="' + rid +'">Reference ' + $(this).attr('data-id') + ' is uncited</p>';
				}
			});

			$(kriya.config.containerElm).find('*[class$="Caption"][id][data-id]:not(".jrnlBoxCaption")').each(function(){
				var FID = $(this).attr('id');
				if ($('*[data-citation-string*=" ' + FID + ' "]').length == 0 && $(this).find('.jrnlQueryRef[data-query-action="uncited-float"]').length == 0){
					validatedMessage += '<p data-clone-id="' + FID +'">Float ' + $(this).attr('id') + ' is uncited</p>';
				}
			});*/


			$(kriya.config.containerElm).find('*[class$="Block"][id]').each(function(){
				var FID = $(this).attr('id');
				if ($('*[id="' + FID + '"]').length > 1){
					validatedMessage += '<p data-clone-id="' + FID +'">Float ' + FID + ' is duplicated</p>';
				}
			});
			if ($(kriya.config.containerElm).find('.back *[class^="jrnlHead"]').length > 0){
				validatedMessage += '<p>Back matter contains undefined Head level like Head1, Head2...</p>';
			}

			if (validatedMessage != ""){
				return validatedMessage;
			}else{
				return false;
			}
		},
		validateBeforeAHR: function (){
			var validatedMessage = '';
			$(kriya.config.containerElm).find('.jrnlAuthors').each(function(){
				var clone = $(this).clone(true);
				clone.find('.jrnlAuthor,.jrnlDegrees,sup,span[class]').remove();
				clone.text(clone.text().replace(/ and[\,\s]+/, ''))
				if (/[a-z]/i.test(clone.text())){
					validatedMessage += '<p>Authors are untagged</p>';
				}
			});
			if ($(kriya.config.containerElm).find('.jrnlPossAff').length > 0){
				validatedMessage += '<p>Some of the Affiliations are not styled as affiliation instead styled as PossAff</p>';
			}
			if ($(kriya.config.containerElm).find('.jrnlAuthAff').length > 0){
				validatedMessage += '<p>Affiliations should not contain Author names. Please check and tag affiliations properly.</p>';
			}if ($(kriya.config.containerElm).find('.front .jrnlArtTitle').length > 1){
				validatedMessage += '<p>The Article title should not have more than one. Please check and tag properly.</p>';
			}if ($(kriya.config.containerElm).find('div > span').length > 0 || $(kriya.config.containerElm).find('> *:not(".body, .back, .front, #queryDivNode")').length > 0){
				validatedMessage += '<p>Tagging pattern incorrect. Please contact support team</p>';
			}if ($(kriya.config.containerElm).find('.jrnlAuthors').length == 0 && $(kriya.config.containerElm).find('.jrnlAff').length > 0){
				validatedMessage += '<p>Affiliations should not come without author.</p>';
			}if ($(kriya.config.containerElm).find('div:not(.back) .jrnlRefText').length > 0){
				validatedMessage += '<p>Reference should not come outside the back matter.</p>';
			}if ($(kriya.config.containerElm).find('li h1, li h2,li h3,li h4, li h5').length > 0){
				validatedMessage += '<p>Head level should not allow in list.</p>';
			}if($(kriya.config.containerElm).find('.jrnlRefText[data-duplicate="true"]').length > 0){
				var errormsg = "";
				$(kriya.config.containerElm).find('.jrnlRefText[data-duplicate="true"]').each(function(){	
				var duplicateid = $(this).attr('data-duplicate-id')
				if($(kriya.config.containerElm).find('.jrnlRefText[id="'+duplicateid+'"]').length > 0){
					var originaltext = $(kriya.config.containerElm).find('.jrnlRefText[id="'+duplicateid+'"]').text()
					var duplicatetext = $(this).text()
					var refid = $(this).attr('data-id');
					var skipduplicate = "$(kriya.config.containerElm).find('.jrnlRefText[id="+refid+"]').removeAttr(\'data-duplicate\');$(this).closest(\'p\').addClass(\'hidden\')";
					var deletedduplicate = "kriyaEditor.init.removeDupRef('"+refid+"','"+duplicateid+"');$(this).closest(\'p\').addClass(\'hidden\');";
					 errormsg += '<p data-clone-id="'+refid+'"><b>Original Reference</b><br/>&nbsp;&nbsp;&nbsp;'+originaltext+'<br><b>Duplicate Reference</b><br/>&nbsp;&nbsp;&nbsp;'+duplicatetext+'<br><span class="btn btn-small orange" onclick='+skipduplicate+'>Not a Duplicate</span>&nbsp;&nbsp;&nbsp;<span class="btn btn-small red" onclick='+deletedduplicate+'>Delete Duplicate</span><br></p>';
				}
				})
				validatedMessage += '<p><b style="display: block; text-align: center;">Reference Duplicated</b>Please delete the duplicate reference or if not duplicate, Click "Not a Duplicate" button to skip the validation</b><br/></p>'+errormsg;
			}if($(kriya.config.containerElm).find('.jrnlRefText').length > 0){
				$(kriya.config.containerElm).find('.jrnlRefText').each(function(){
					var rid = $(this).attr('id')
					$(this).find('span[class][class^="Ref"]:not(".RefAuthor, .RefCollaboration, .RefEditor, .RefComments, .RefGivenName, RefSurName")').each(function(){
						var newclass = $(this).attr('class')
						if($(kriya.config.containerElm).find('.jrnlRefText[id="'+rid+'"] .'+newclass).length > 1){
							console.log('duplicate id:'+rid+ 'classname' + newclass)
							validatedMessage += '<p data-clone-id="'+rid+'">'+newclass+' is duplicated in reference ['+rid+']</p>';
						}
					})
				})
			}
			$(kriya.config.containerElm).find('h1').each(function(){
				var cla = $(this).attr('class')
				if(!cla.match(/jrnlHead1|jrnlAppHead1|jrnlDeleted|jrnlRefHead/) && $(kriya.config.containerElm).find('.'+cla).length>1){
					var head = cla.replace('jrnl','')
					validatedMessage += '<p>'+head+' duplicated. Please check and tag properly.</p>';
				}
			})
			if (! /integration/.test(window.location.origin)){
				$(kriya.config.containerElm).find('.jrnlRefText').each(function(){
					$(this).removeAttr('data-untag');
					var clone = $(this).clone(true);
					var rid = $(this).attr('id');
					clone.find('span[class^="Ref"]').remove();
					var textNodes = textNodesUnder(clone[0]);
					var untagged = false;
					for (var tl = 0, tnLength = textNodes.length; tl < tnLength; tl++){
						var curNode = textNodes[tl];
						curNode.textContent = curNode.textContent.replace(/([\.\,\;\:\?\"\'\(\[\u0020\u2212\u2013\u2014\u201C\u201D\u2018\u2019\u0026\-\)\]]+)/ig, '');
						curNode.textContent = curNode.textContent.replace(/(and|doi|in|pp?|eds?|available (to|from))/ig, '');
						curNode.textContent = curNode.textContent.replace(/[\s\u00A0\u0009]+/ig, '');
						if (curNode.textContent != "") {
							untagged = true;
						}
					}
					if (untagged){
						validatedMessage += '<p data-clone-id="' + rid +'">Reference ' + $(this).attr('data-id') + ' is untagged</p>';
						$(this).attr('data-untag', 'true');
					}
					var refContainer = $(this)[0];
					$(refContainer).removeAttr('data-reftype');
					var popper = $('div[data-type="popUp"][data-component="jrnlRefText"]')
					$(popper).find('*[data-if-selector]').each(function(){
						var ifSelector = $(this).attr('data-if-selector').split('|');
						var ifIdentified = true;
						for (var x = 0, xl = ifSelector.length; x < xl; x++){
							var noflag = false;
							if (/^!/.test(ifSelector[x])){
								ifSelector[x] = ifSelector[x].replace('!', '');
								noflag = true;
							}
							var condition = kriya.xpath(ifSelector[x], refContainer);
							if (condition.length > 0 && noflag){
								ifIdentified = false;
							}else if (condition.length == 0 && !noflag){
								ifIdentified = false;
							}
						}
						if (ifIdentified){
							$(refContainer).attr('data-reftype', $(this).attr('data-ref-type'));
							if (/journal|book/i.test($(this).attr('data-ref-type')) && !refContainer.hasAttribute('data-incomplete')){
								if ($(refContainer).find('.RefYear,.RefInPress').length == 0){
									validatedMessage += '<p data-clone-id="' + rid +'" data-incomplete="true">Reference ' + rid + ' doesn\'t contain an year in it, please identify or confirm it does not have an year&nbsp;&nbsp;<span class="btn btn-small orange" onclick="kriyaEditor.init.incompleteRef(\'' + rid + '\')">Confirm</span><br/></p>';
								}
							}
						}
					});
					if (refContainer.hasAttribute('data-reftype')&& $(popper).find('*[data-if-selector][data-ref-type="' + refContainer.getAttribute('data-reftype') + '"]').length > 0){
						var refTypeNode = $(popper).find('*[data-if-selector][data-ref-type="' + refContainer.getAttribute('data-reftype') + '"]');
						var invalidElements = [];
						$(refContainer).find('*[class]').each(function(){
							var className = $(this).attr('class');
							if (refTypeNode.find('[data-node-name="' + className + '"]').length == 0){
								invalidElements.push(className)
							}
						});
						if (invalidElements.length > 0){
							validatedMessage += '<p data-clone-id="' + rid +'">Reference ' + $(this).attr('data-id') + ' has invalid elements: ' + invalidElements.join(', ') + '</p>';
						}
					}else if (! refContainer.hasAttribute('data-untag')){
						validatedMessage += '<p data-clone-id="' + rid +'">Reference styling for ' + $(this).attr('data-id') + ' is not valid</p>';
					}
				});
			}
			if ($(kriya.config.containerElm).find('.jrnlunTagged').length > 0){
				validatedMessage += '<p data-clone-id="' + $(this).attr('id') +'">Content is untagged, please style those untagged and proceed</p>';
			}
			if ($(kriya.config.containerElm).find('.back *[class^="jrnlHead"]').length > 0){
				validatedMessage += '<p>Back matter contains undefined Head level like Head1, Head2...</p>';
			}
			$('.jrnlDeleted').removeAttr('id');
			$(kriya.config.containerElm).find('.jrnlFigCaption[id], .jrnlTblCaption[id]').each(function(){
				if($(this).attr('data-skip-validation') == "true"){
					return;
				}
				var FID = $(this).attr('id');
				var DID = $(this).attr('data-id');
				if (FID == "" || DID == ""){
					validatedMessage += '<p>Some captions are not styled properly, ID missing.</p>';
				}
				if (/^[A-Z][0-9]+$/.test(FID)){
					if ($(kriya.config.containerElm).find('[id="' + FID + '"]').length > 1){
						var dataid = $(this).attr('id');
						var skipvalidation = 'kriyaEditor.init.skipvalidation(\''+ dataid +'\');$(this).parent().addClass(\'hidden\')';
						validatedMessage += '<p data-clone-id="' + $(this).attr('id') +'">There is a possible duplicate of float : ' + FID + '<br><span class="btn btn-small blue" onclick='+skipvalidation+'>Skip</span></p>';
					}
					if ($(this).attr('class').match(/jrnlFigCaption|jrnlTblCaption/)){
						if ($(this).attr('class').replace(/^jrnl([A-Z]).*$/, '$1') != FID.replace(/^([A-Z]).*$/, '$1')){
							validatedMessage += '<p data-clone-id="' + $(this).attr('id') +'">Caption ' + FID + ' styled incorrectly</p>';
						}
					}
				}
			});
			if (validatedMessage != ""){
				return validatedMessage;
			}else{
				return false;
			}
		},
		skipvalidation: function(id){
			if(!id)return false;
			$(kriya.config.containerElm).find('*[id="'+id+'"]').each(function(){
				$(this).attr('data-skip-validation', 'true');
			})
		},
		//removeDuplicate reference added by vijaykumar on 21-01-2019
		removeDupRef: function(refid, duplicateid){
			if(!refid && !duplicateid) return false;
			$(kriya.config.containerElm).find('.jrnlRefText[id="'+refid+'"]').removeAttr('class-name').attr('class','jrnlDeleted').find('*').contents().unwrap();
			$(kriya.config.containerElm).find('.jrnlRefText[id="'+duplicateid+'"]').attr('data-Reference-reorder', 'true');
			$(kriya.config.containerElm).find('.jrnlBibRef[data-citation-string*=" '+refid+' "]').each(function(){
				var rid =	$(this).attr('data-citation-string')
				rid = rid.replace(' '+refid+' ', ' '+duplicateid+' ');
				$(this).attr('data-citation-string', rid)
			})
		},
		mergetable: function(){
			var selection = rangy.getSelection();
			var node = kriya.selection.commonAncestorContainer;
			if($(node).closest('table').length >0){
				var table = $(node).closest('table');
				var tablepre = table.prev()
				var mergetable = false;
				while(tablepre.length > 0 && tablepre.attr('class') == 'jrnlDeleted' || tablepre.attr('class')== 'ecsTable'){
					if(tablepre.attr('class') == 'ecsTable'){
						mergetable = true;
						break;
					}else{
						tablepre = tablepre.prev()
					}
				}
				if(mergetable == true && tablepre.attr('class') == 'ecsTable'){
					var tablepretdlength = $(tablepre).find('tr:last td').length
					$(tablepre).find('tr:last td[colspan]').each(function(){
						tablepretdlength	= tablepretdlength + $(this).attr('colspan') - 1;
					})
					var newtabletd = $(table).find('tr:last td').length
					$(tablepre).find('tr:last td[colspan]').each(function(){
						newtabletd	= newtabletd + $(this).attr('colspan') - 1;
					})
					if(newtabletd == tablepretdlength){
						if($(table).find('tbody') == 0 ){
							$(table).append('<tbody>')	
						}
						$(table).find('tr,th').each(function(){
							$(tablepre).find('tbody').append($(this));
						})
						$(table).remove()
						kriyaEditor.init.addUndoLevel('tablemerge');
						kriyaEditor.init.save();
					}else{
						kriya.notification({
							title : 'Table cells count should be equal',
							type  : 'error',
							timeout : 5000,
							content : 'Table Merge Failed',
							icon: 'icon-warning2'
						});
					}
				}else{
					kriya.notification({
						title : 'Table not found',
						type  : 'error',
						timeout : 5000,
						content : 'Table Merge Failed',
						icon: 'icon-warning2'
					});
				}
			}
		},
		markasheader: function(){
			var selection = rangy.getSelection();
			var node = kriya.selection.commonAncestorContainer;
			if($(node).closest('table').length > 0 && $(node).closest('tbody').length > 0){
				var table = $(node).closest('table');
				var tr = $(node).closest('tr');
				if($(table).find('tbody').find('tr').index(tr) == 0){
					if($(table).find('thead').length > 0){
						$(table).find('thead').append(tr)	
					}else{
						$(table).prepend('<thead>');
						$(table).find('thead').append(tr)	
					}
					kriyaEditor.init.addUndoLevel('tablemerge');
					kriyaEditor.init.save();
				}
			}else if($(node).closest('table').length > 0 && $(node).closest('thead').length > 0){
				var table = $(node).closest('table');
				var tr = $(node).closest('tr');
				var trlength  = $(node).closest('table thead').find('tr').length;
				if($(table).find('thead').find('tr').index(tr) == trlength - 1){
					if($(table).find('tbody').length > 0){
						$(table).find('tbody').prepend(tr)
					}else{
						$(table).prepend('<tbody>');
						$(table).find('tbody').append(tr)	
					}
					if($(node).closest('table thead').find('tr').length == 0){
						$(node).closest('table').find('thead').remove();
					}
					kriyaEditor.init.addUndoLevel('tablemerge');
					kriyaEditor.init.save();
				}
			}
		},
		tableToEquation: function(){
			var selection = rangy.getSelection();
			var node = kriya.selection.commonAncestorContainer;
			if($(node).closest('table').length > 0){
				var table = $(node).closest('table');
				var	tablerow = $(node).closest('table tr, table th');
				if(tablerow.length > 0 && (table).find('img.kriyaFormula').length >0){
					$(table).find('tr,th').each(function(){
						var tablenode ="";
						$(this).find('p').each(function(){
							tablenode = tablenode+$(this).html();
						})
						var neweqn = '<p class="jrnlEqnPara" class-name="EqnPara" data-converted="true">'+tablenode+'</p>'
						$(table).before(neweqn)
					})
					$(table).remove();
				}
			}
		},
		applyHouseRules: function(){
			//it will count No. of untagged elements and No. of re-styled elements-kirankumar
			var parameters = {
				'customer': kriya.config.content.customer,
				'project' : kriya.config.content.project,
				'doi'     : kriya.config.content.doi,
				'type'	  : "tracker"
			};
			parameters.logMsg += "<div><p>No. of untagged elements :</p><p>"+$('#contentContainer').find('.jrnluntagged:not([data-untag]), [data-untag]:not(.jrnluntagged), [data-untag].jrnluntagged').length+"</p></div>";
			parameters.logMsg += "<div><p>No. of re-styled elements :</p><p>"+$('#contentContainer').find('[data-old-class]').length+"</p></div>";
			keeplive.sendAPIRequest('logerrors', parameters, function(res){
			});
			if($('div.WordSection1').length > 0){
				structedData = $('div.WordSection1')[0].outerHTML;
			}
			// remove the class used to show hints for applied spans in table
			$('#contentDivNode table.showSpanHint').removeClass('showSpanHint');
			stylePalette.actions.trackLog();
			if ($(kriya.config.containerElm).find('.jrnlPossBibRef, .jrnlPossRef[data-type="jrnlBibRef"]').length > 0){
				kriyaEditor.citations.checkCitations();
				return false;
			}
			//unwrap the givenname and surname in reference author after structure content
			$('#contentDivNode .RefGivenname, #contentDivNode .RefSurname').contents().unwrap()
			kriyaEditor.init.save('true')
			$('#infoDivContent > [id*=validationDivNode]').remove();
			var validated = kriyaEditor.init.validateBeforeAHR();
			if (validated){
				$('#infoDivContent > [id*=DivNode]').removeClass('active');
				$('#infoDivContent').append($('<div id="validationDivNode" class="active"><p>Validation Errors</p><div>' + validated + '</div></div>'));
				kriya.notification({
					title : 'ERROR',
					type  : 'error',
					timeout : 5000,
					content : 'Validation Failed',
					icon: 'icon-warning2'
				});
				return false;
			}
			eventHandler.components.general.getResources();
		},
		postValidation: function(){
			var valid = eventHandler.components.general.validateResources();
			if (!valid || $('#contentDivNode .queryDivNode div').length > 0){
				return false;
			}
			$('[data-type="popUp"]').addClass('hidden');
			$('.la-container').fadeOut();
			$('.pre-load-container .pre-loader-head').html('Applying House Rules');
			$('.pre-load-container').fadeIn();
			$('.pdfStatus').remove();
			//$('body').append('<span class="pdfStatus btn btn-primary">Apply House Rules...</span>');
			kriyaEditor.init.saveBackup('preedited');
			var content = $(kriya.config.containerElm).html();
			var params = {'content':content, 'customerName':kriya.config.content.customer, 'projectName':kriya.config.content.project, 'doi':kriya.config.content.doi, 'resources': valid.html()};
			// to pick only basic default styles from the rulesets - JAI 19-Feb-2019
			if ($('#contentContainer').attr('data-rulesets') != undefined){
				params.rulesetStyle = $('#contentContainer').attr('data-rulesets');
			}
			//to send proof config ID - JAi 20-Feb-2019
			if ($('#contentContainer').attr('data-proof-config-id') != undefined){
				params.proofConfig = $('#contentContainer').attr('data-proof-config-id');
			}
			$.ajax({
				xhr: function(){
					var xhr = new window.XMLHttpRequest();
					//Download progress
					xhr.addEventListener("progress", function(evt){
						var lines = evt.currentTarget.response.split("\n");
						if(lines.length){
							var progress = lines[lines.length-1];
							if (progress) {
								//$('.pdfStatus').html(progress);
								if ($('<r>' + progress + '</r>').find('content').length == 0){
									$('.pre-load-container .pre-loader-message').html(progress);
								}
							}
						}else{
							console.log(evt.currentTarget.response);
						}
					}, false);
					return xhr;
				},
				type: "POST",
				url: "/api/applyHouseRules",
				data: params,
				success: function (data) {
					$('.pre-load-container').fadeOut();
					$('.pre-load-container .pre-loader-message').html('');
					data = data.replace(/&apos;/g, "\'");
					var response = $('<r>'+ data + '</r>');
					if ($(response).find('content').length > 0){
						if(structedData){
							$('*[data-name="restoreStructuredData"]').removeClass('hidden');
							kriya.notification({
									type: 'success',
									timeout: 20000,
									content: 'Click "BackToSC" button to restore the sturcture content data.'
							});
						}
						$('.applyHouseRules,.approveStyle').toggleClass('hidden');
						$('.btn.dropdown-button:contains("Corresp")').parent().attr('data-type', ' front ')
						$('#contentDivNode').html($(response).find('content').html());
						$('#contentDivNode').find('.front,.body,.back').attr('data-edited', 'true');
						$('.pdfStatus').remove();
						$('b:empty').remove();
						$('.label .del').remove();
						//Add list of author names to correspoding author option added by vijayakumar on 1-11-2018
						$('ul#drop-down-Authorslist1').html('');
						$('.front .jrnlAuthors .jrnlAuthor').each(function(){
							var authorname = $(this).text()
							$('ul#drop-down-Authorslist1').append('<li><a href="javascript:;" onclick="stylePalette.actions.correspMapping(\''+authorname+'\');">'+authorname+'</a></li>')
						})
						kriyaEditor.settings.save.classList.remove("disabled");
						kriyaEditor.init.save();
						setTimeout(function(){
							kriyaEditor.init.saveBackup('applyhouserules');
						}, 3000);
					}else{
						var errortext = data.split(/\r?\n/);
						kriya.notification({
							title: 'Apply houlse rules failed',
							type: 'error',
							content: errortext[errortext.length - 1],
							timeout: 8000,
							icon : 'icon-info'
						});
					}
				},
				error: function(err){
					console.log(err);
					$('.pdfStatus').remove();
					$('.pre-load-container').fadeOut();
				}
			});
		},
		probeValidation: function(param, targetNode){
			$('.pre-load-container').fadeIn();
			$('.pre-load-container .pre-loader-message').html('Running probe validations');
			$('#infoDivContent #validationDivNode').html('');
			var parameters  = {
				'doi': kriya.config.content.doi,
				'customer': kriya.config.content.customer,
				'rules' : 'exeter',
				'project':kriya.config.content.project,
				'role':'preeditor',
				'stage':'preeditor',
				'content':$(kriya.config.containerElm).html()
			};
			kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
				if(res.error){
					$('.pre-load-container').fadeOut();
					kriya.notification({
						title: 'Failed',
						type: 'error',
						content: 'Validation Failed. Please contact support.',
						timeout: 8000,
						icon : 'icon-info'
					});
				}else if(res){
					res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); //change self closed column tag to open and close tag
					var popper = $('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]');
					var resNode = $(res);
					var stataus = true;
					if((resNode.find('message').length > 0) && (resNode.find('message signOff-allow:contains("false")').length > 0)){
						$('.pre-load-container').fadeOut();
						var container = $('<div />');
						var ruleTypes = [];
						resNode.find('rule-type').each(function(){
							var ruleType = $(this).text();
							if(ruleTypes.indexOf(ruleType) < 0){
								ruleTypes.push(ruleType);
								var msgLength = $(this).closest('probe-result').find('rule-type:contains(' + ruleType + ')').length;
								var tabeNode = $('<input id="_probe_' + ruleType + '_" type="radio" name="tabs_probe"/><label for="_probe_' + ruleType + '_">' + ruleType + '<span class="badge">' + msgLength + '</span></label>');
								if(ruleTypes.length == 1){
									tabeNode.filter('input[type="radio"]').attr('checked', 'true');
								}
								container.append(tabeNode);
							}
						});

						var styleString = '';
						for(var r=0;r<ruleTypes.length;r++){
							var tabId = 'probe_' + ruleTypes[r];
							var ruleDiv = $('<div id="' + tabId + '" class="carousel-item" />');
							//var displayTable = $('<table />');
							//displayTable.append('<thead><tr><th>Rule ID</th><th width="10%">Position</th><th>Message</th><th>Xpath</th></tr></thead>');
							//displayTable.append('<tbody />');
							//ruleDiv.html(displayTable);
							container.append(ruleDiv);

							styleString += '#compDivContent #_' + tabId + '_:checked ~ #' + tabId + ',';
						}
						styleString = styleString.replace(/\,$/g, '');
						styleString += '{display:block;}';
						var styleNode = $('<style>' + styleString + '</style>');
						
						resNode.find('message').each(function(){
							var row = $('<p data-clone-id="' + $(this).find('node-id').text() + '" data-allow-signoff="' + $(this).find('signOff-allow').text() + '">' + $(this).find('probe-message').text() + '</p>');
							var ruleType = $(this).find('rule-type').text();
							container.find('#probe_' + ruleType).append(row);
						});
						$('#infoDivContent > [id*=validationDivNode]').remove();
						$('#infoDivContent > [id*=DivNode]').removeClass('active');
						$('#infoDivContent').append($('<div id="validationDivNode" class="active" style="height: 668px;"><p>Validation Errors</p></div>'));
						$('#validationDivNode').append(container)
						$('#validationDivNode').append(styleNode);
						$('#validationDivNode label[for]').remove();
						return false;
					}else{
						if(param && typeof(param) == "function"){
							param();
						}
					}
				}
			}, function(err){
				if(onError && typeof(onError) == "function"){
					onError(err);
				}
				$('.pre-load-container').fadeOut();
			});
		},
		validationlog: function(type){
			var parameters  = {
					'doi': kriya.config.content.doi,
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project,
					'role': 'preedting',
					'stage':kriya.config.content.stage,
					'log': 'true'
				};
				if(type){
					parameters['type'] = type;
					parameters['xmltype'] = 'raw';
				}
				
				kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
					if(res && res.error){
						if(onError && typeof(onError) == "function"){
							onError(res);
						}
					}else if(res){
							console.log('validation created successfully')
					}else{
						if(onError && typeof(onError) == "function"){
							onError("Empty response.");
						}	
					}
				}, function(err){
					if(onError && typeof(onError) == "function"){
						onError(err);
					}
				});
		},
		saveBackup: function(fileName){
			var content = $('div.WordSection1')[0].outerHTML
			var parameters = {'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'doi':kriya.config.content.doi, 'content':encodeURIComponent(content), 'fileName': fileName};
			kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
				console.log('Data Saved');
			})
		},
		/*confirm ref with out year*/
		incompleteRef: function(rid){
			$('#contentDivNode .jrnlRefText#' + rid).attr('data-incomplete', 'true');
			$('#infoDivContent > [id*=validationDivNode] [data-clone-id="' + rid +'"][data-incomplete="true"]').remove();
		},
		/**
    	 * Function to execute the command
    	 * @param arguments[0] - function name or command to execute
    	 * @param arguments[1] - parameters to the function
    	 */
    	execCommand: function(){
    		var range = rangy.getSelection();
			var targetElement = range.anchorNode;
			targetElement = $(targetElement).closest('[id]');
    		if(arguments[0]){
    			if(arguments[0].match(/cut|copy|paste/) && arguments[0]){
    				if(document.queryCommandEnabled(arguments[0])){
    					this[arguments[0]](arguments[1]);
						return;	
    				}else{
    					kriya.notification({
							title : 'ERROR',
							type  : 'error',
							timeout : 8000,
							content : "Your browser doesn't support direct access to the clipboard. Please use the keyboard shortcuts (Ctrl+X/C/V) instead.",
							icon: 'icon-warning2'
						});
    				}
				}
    			if(arguments[0].match(/indent|outdent|justify/)){
    				var saveNode = null; status=false;
					if (targetElement[0].nodeName == "TD" || $(targetElement).closest('td,th').length > 0){
						var textIndent = (!$(targetElement).closest('td,th').attr('text-indent') || $(targetElement).closest('td,th').attr('text-indent')<=0) ? 0 : $(targetElement).closest('td,th').attr('text-indent');
						if (arguments[0] == "indent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html('&emsp;'+$(this).html());
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)+1);
							});
						}else if (arguments[0] == "outdent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html($(this).html().replace(/^\u2003/, ''));
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)-1);
							})
						}
						$("td[text-indent]").each(function() {
						    if($(this).attr("text-indent") <= 0) $(this).removeAttr('text-indent');
						});
						saveNode = $('.wysiwyg-tmp-selected-cell').closest('table');
					}else if (targetElement[0].nodeName == "LI" || $(targetElement).closest('li').length > 0 || $(targetElement).siblings('li').prevObject.length > 0){
						if (arguments[0] == "indent"){
							status=editor.composer.commands.exec('indentList');
						}else if (arguments[0] == "outdent"){
							

						
							var paraEl = editor.composer.commands.exec('outdentList');
							
						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//var parentEle = $(targetElement).closest('ul,ol')[0].parentElement;
                        
                        //On Indent or outdent the next or the previous list style should be applied. - Lakshminarayanan. S(lakshminarayanan.s@focusite.com)
							var optionTexts = [];
							if($(targetElement).closest('ul').length == 0){
								$("#order-list-dropdown li").each(function(key,val) { 
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');
									
									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument)
									});
							} else{
					
								$("#unorder-list-dropdown li").each(function(key,val) { 
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');
									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument) 
								});
							}
						if (!status && arguments[0] == "indent"){
							var listStyle = optionTexts[(optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType)+1)%optionTexts.length];
							$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							$(targetElement).closest('ul,ol')[0].type = listStyle;
                            kriyaEditor.settings.undoStack.push($(targetElement).closest('ul,ol').parents('ul,ol')[0]);
						}else {
							//var listStyle = optionTexts[optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.style.listStyleType)-1];
							//$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							//$(targetElement).closest('ul,ol')[0].type = listStyle;
						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType;
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')//[0].parentElement.parentElement.style.listStyleType;
						saveNode = [$(targetElement).closest('ul,ol')[0]];
					}else{
						saveNode = targetElement;
						document.execCommand(arguments[0]);
					}
					if(typeof saveNode !=undefined && saveNode[0]!=undefined){
					//#535-Duplication happens after refresh while indend-Helen.j@focusite.com
					saveNode = $(saveNode).parent().closest('ol,ul[id]').length > 0 ? $(saveNode).parent().closest('ol,ul[id]') : saveNode;
					//End of #535
					kriyaEditor.settings.undoStack.push($(saveNode)[0]);}
					kriyaEditor.settings.undoStack = kriyaEditor.settings.undoStack.concat(paraEl);
					//kriyaEditor.init.addUndoLevel('formatting');
					return;
    			}else{
    				//Handling list
	    			if(arguments[0].match(/list/i)){
						var listNode = null;
					if($(range.anchorNode).parents('ul,ol').length > 0){
	    					listNode = $(range.anchorNode).parents('ul,ol').first();
							listNode.attr('type', arguments[1]);
							listNode.css('list-style-type', arguments[1]);
							//rename the elemet if list type is change
                            //#18 - For create tmpRange for change bullets after none by - Vimala.J ( vimala.j@focusite.com)
                            var tmpRange = rangy.saveSelection();
							if(arguments[0].match(/unOrderedList/i)){
								$(listNode).renameElement('ul');
							}else{	
								$(listNode).renameElement('ol');  
							}
                            //#18 - send tmpRange restoreselection by - Vimala.J ( vimala.j@focusite.com)
							rangy.restoreSelection(tmpRange);
							// End of #18 
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}else{
	    					editor.composer.commands.exec(arguments[0], arguments[1]);
	    					listNode = $(targetElement).closest('ul,ol').first();
							listNode.css('list-style-type', arguments[1]);
							if (! $(listNode)[0].hasAttribute('id')){
								$(listNode).attr('id', uuid.v4());
							}
							$(listNode).find('li,p').each(function(){
								if (! $(this)[0].hasAttribute('id')){
									$(this).attr('id', uuid.v4());
								}
							});
							$(listNode).attr('data-inserted', 'true');
							
							$(listNode).find('li p').each(function(){
								var clonedPara = $(this).clone(true);
								clonedPara.attr('data-removed', 'true');
								kriyaEditor.settings.undoStack.push(clonedPara[0]);
							});
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}
					
						//kriyaEditor.init.addUndoLevel('formatting');
						//createCloneContent();
						return;
					
	    		}
	    			//editor.composer.commands.exec(arguments[0], arguments[1]);
	    			/*if(!tracker || tracker.length < 1){
    					var formatedNode = $(window.getSelection().getRangeAt(0).startContainer.parentElement);
    					formatedNode.kriyaTracker('sty');
	    			}else if(kriya.selection.text() == kriya.selection.startContainer.parentElement.textContent){
	    				var cid = $(kriya.selection.startContainer.parentElement).attr('data-cid');
						$('#updDivContent'+' [data-rid="' + cid + '"]').find('.reject-changes').trigger('click');
	    			}*/
    			}
    			//kriyaEditor.init.addUndoLevel('formatting', targetElement);
			}
    	}
	}
	return kriyaEditor;

})(kriyaEditor || {});

(function (kriyaEditor) {
	settings = kriyaEditor.settings;
	
	kriyaEditor.citations = {
		checkCitations: function(){
			$('.jrnlPossBibRef:empty').remove();
			$('.jrnlBibRef:empty').remove();
			$(kriya.config.containerElm).find('.jrnlPossBibRef').attr('class', 'jrnlPossRef').attr('data-type', 'jrnlBibRef');
			$('#navContainer label[for]').addClass('hidden');
			$('#untaggedDivNode').removeClass('active').addClass('hidden');
			$('#checkCiteDivNode').removeClass('hidden').addClass('active');
			$('#checkCiteDivNode').css('height', $(window).height() - $('#checkCiteDivNode').offset().top - $('#footerContainer').height());
			kriyaEditor.citations.nextCitation();
		},
		nextCitation: function(action){
			var contentNode = $(kriya.config.containerElm);
			var citationClass = {
				'jrnlFigCaption':'jrnlFigRef',
				'jrnlTblCaption':'jrnlTblRef',
				'jrnlRefText':'jrnlBibRef',
				'jrnlSupplCaption':'jrnlSupplRef',
				'jrnlMapCaption':'jrnlMapRef',
			}
			if (action == 'notProvided'){
				$(contentNode).find('.jrnlPossRef.active').attr('class', 'jrnlUncitedRef');
			}
			else if (action == 'markCited'){
				var className = $('.selectedRef').attr('class').split(' ')[0];
				className = citationClass[className];
					if ($(contentNode).find('.jrnlPossRef.active').html() != $('#possCiteValue').html()){
						if(className == 'jrnlBibRef'){
						var e = $('#possCiteValue').text();
						e = e.replace(/([\[\/\.\(\)\'\u2019\"])/g, '\\$1');
						var r = new RegExp('^(.*?)'+ e + '(.*?)$');
						var prefixString = $(contentNode).find('.jrnlPossRef.active').text().replace(r, '$1');
						if (prefixString != ""){
							$(contentNode).find('.jrnlPossRef.active').before(prefixString);
						}
						var prefixString = $(contentNode).find('.jrnlPossRef.active').text().replace(r, '$2');
						if (prefixString != ""){
							$(contentNode).find('.jrnlPossRef.active').after(prefixString);
						}
						
					}
					$('#possCiteValue').prepend($(contentNode).find('.jrnlPossRef.active .jrnlQueryRef').clone())
						$(contentNode).find('.jrnlPossRef.active').html($('#possCiteValue').html())
				}
				var rid = [];
				$('.selectedRef').each(function(){
					rid.push($(this).attr('id'));
				})
				rid = rid.join(' ');
				$(contentNode).find('.jrnlPossRef.active').attr('class',className).attr('data-citation-string', ' ' + rid + ' ').attr('data-mark-cited', rid);
			}
			else if (action == 'notCitation'){
				$(contentNode).find('.jrnlPossRef.active').contents().unwrap();
			}
			
			if ($(contentNode).find('.jrnlPossRef.active').length == 0){
				$(contentNode).find('.jrnlPossRef:first').addClass('active');
			}else{
				var prevCite = $(contentNode).find('.jrnlPossRef.active');
				$(prevCite).removeClass('active');
				if ($(prevCite).nextInDOM('.jrnlPossRef').length > 0){
					$(prevCite).nextInDOM('.jrnlPossRef').addClass('active');
				}
			}
			
			$('#refListDiv').html('');
			$('#markCited').addClass('disabled');
			$('#notCited,#skipCitation').removeClass('disabled');
			$('.toggle-refs').addClass('hidden');
			if ($(contentNode).find('.jrnlPossRef.active').length > 0){
				$(contentNode).find('.jrnlPossRef.active')[0].scrollIntoView();
				$('#possCiteValue').html($(contentNode).find('.jrnlPossRef.active').html());
				if ($('.jrnlPossRef').attr('data-type') == 'jrnlBibRef'){
					if ($('.jrnlPossRef.active').attr('data-tag-type') == 'AUTHORYEARREF'){
						$('.toggle-refs').removeClass('hidden');
					}
					var year = $('.jrnlPossRef.active').text();
					author = year.replace(/^([^\s]+).*$/ig, '$1');
					year = year.replace(/^[a-z\s\,\.\;]+/ig, '');
					year = year.replace(/[a-z\s\,\.\;]+/ig, '');
					$(contentNode).find('.jrnlRefText').each(function(){
						var thisRef = $(this).clone();
						if ($('.jrnlPossRef.active').attr('data-tag-type') == 'AUTHORYEARREF'){
							thisRef.addClass('hidden');
							var ry = new RegExp(year.replace(/(?=[() ])/g, '\\'))
							var ra = new RegExp(author.replace(/(?=[() ])/g, '\\'))
							if (ry.test($(this).find('.RefYear').text()) || ra.test($(this).find('.RefAuthor:first').text())){
								thisRef.attr('related-refs', 'true').removeClass('hidden');
							}
						}
						$('#refListDiv').append(thisRef);
					});
				}
				if ($('.jrnlPossRef').attr('data-type') == 'jrnlTblRef'){
					$(contentNode).find('.jrnlTblCaption').each(function(){
						$('#refListDiv').append($(this).clone());
					});
				}
				if ($('.jrnlPossRef').attr('data-type') == 'jrnlFigRef'){
					$(contentNode).find('.jrnlFigCaption').each(function(){
						$('#refListDiv').append($(this).clone());
					});
				}
			}else{
				$('#checkCiteDivNode').addClass('hidden').removeClass('active');
				kriyaEditor.init.save();
				if($('.WordSection1 div.front[data-edited="true"]').length == 0){
					kriyaEditor.init.applyHouseRules();	
				}
			}
		},
		toggleReferences: function(){
			$('.toggle-refs').toggleClass('btn-default')
			$('.toggle-refs').toggleClass('btn-warning');
			if ($('#refListDiv .jrnlRefText.hidden').length > 0){
				$('#refListDiv .jrnlRefText').removeClass('hidden');
			}else{
				$('#refListDiv .jrnlRefText').addClass('hidden');
				$('#refListDiv .jrnlRefText[related-refs]').removeClass('hidden');
			}
		}
	}
	return kriyaEditor;

})(kriyaEditor || {});

	
function handleChanges(summary){
	//return false;
	/*if (summary[0].added.length > 0){
		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {'funcToCall': 'nodeChange', 'param': {'node': summary[0].added, 'type': 'added'}},
					event: 'click',
					target: ''
				}
			});
		},10)
	}*/
	if (summary[0].added.length > 0 && $(summary[0].added[0]).closest('.jrnlRefText').length > 0){
		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {'funcToCall': 'nodeChange', 'param': {'node': summary[0].added, 'type': 'added'}},
					event: 'click',
					target: ''
				}
			});
		},10)
	}
	if (summary[0].removed.length > 0){
		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {'funcToCall': 'nodeChange', 'param': {'node': summary[0].removed, 'type': 'removed'}},
					event: 'click',
					target: ''
				}
			});
		},10)
	}
}

function setupWatch(){
	var observer = new MutationSummary({
	  callback: handleChanges, // required
	  rootNode: document.getElementById('contentDivNode'), // optional, defaults to window.document
	  observeOwnChanges: false,// optional, defaults to false
	  oldPreviousSibling: false,// optional, defaults to false
	  queries: [
		{ all : true }
	  ]
	});
}

	window.kriya = {
		// allows optional configuration on the object
        classes: function () {
            return {
               // moduleOne: new kriya.ModuleOne(),
               // moduleTwo: new kriya.ModuleOne(config.modTwo)
			   // utilsPack: new kriya.kriyaUtils()
            };
        },
		notification: function(param){
        	var id = uuid.v4();
        	var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
			notice.append('<div class="row kriya-notice-header" />');
			notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');

			if(param.icon){
				notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
			}
			if(param.title){
				notice.find('.kriya-notice-header').append(param.title);
			}
			notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="kriya.removeNotify(this)"></i>');
			
			Materialize.toast(notice[0].outerHTML, param.timeout);
			return id;
        },
        removeNotify: function(target){
        	if(target){
        		$(target).closest('.toast').fadeOut(function(){
			        $(this).remove();
			    });
        	}
        },
	};
	kriya.config = {
		containerElm : "#contentDivNode",
		blockElements : ['p','h1','h2','h3','h4','h5','h6','ul','ol','div'],
	}
	
	
/**
 * Functio to progress on the element
 * This progress function now only handled for the popup component
 */
$.fn.progress = function(label,remove){
	label = (label) ? label : 'Loading';
	var loadingEle = $('<div class="loadingProgress"><span data-label="progress">' + label + '...</span><div class="progress"><div class="indeterminate"/></div></div>');
	this.each(function(i, el){
		var comp = $(this).find('[data-input-editable]');
		comp = (comp.length > 0) ? comp : $(this);
		$(this).find('.loadingProgress').remove();
		$(this).find('.loading-wraper').contents().unwrap();
		if(remove){
			return;
		}
		comp.children().wrap("<div class='loading-wraper'></div>");
		$(this).append(loadingEle);
		loadingEle.css('width',comp.css('width'));
		//loadingEle.css('top','-'+comp.css('top'));
		loadingEle.css('display','block');
		
		var compoHeght = parseFloat(comp.height());
		loadingEle.css('bottom', compoHeght/2);
	});
};

/**
 * Functio to change the element name
 */
$.fn.renameElement = function(newTag, renameUsingData){
	this.each(function(i, el){
		var $el = $(el);
		//rename using the data-tag attribute
		if(renameUsingData){
			newTag = $(el).attr('data-tag');
			$(el).removeAttr('data-tag');
		}
		//continue if the newTag is null
		if(!newTag){
			return;
		}
		$newTag = $("<" + newTag + ">");

		// attributes
		$.each(el.attributes, function(i, attribute){
			$newTag.attr(attribute.nodeName, attribute.nodeValue);
		});
		
		$newTag.html($el.html());
		$el.replaceWith($newTag[0].outerHTML);
	});
};


function textNodesUnder(el){
  var n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
  while(n=walk.nextNode()) a.push(n);
  return a;
}

function callInitializeEvents(){
	eventHandler.publishers.add();
	eventHandler.subscribers.add();
	if ($('.body[data-edited="true"]').length > 0){
		$('.approveStyle').removeClass('hidden');
	}else{
		$('.applyHouseRules').removeClass('hidden');
	}
	if ($('#contentContainer').attr('data-state') != 'read-only' && $('.time-notice').length > 0){
		timeTicker = new easytimer.Timer();
		timeTicker.start();
		timeTicker.addEventListener('secondsUpdated', function (e) {
			$('.time-notice').html(timeTicker.getTimeValues().toString());
		});
	}
	setTimeout(function(){
		kriyaEditor.init.trackArticle();
	}, 120000);
	kriya.config.content = {};
	kriya.config.content.doi = $('#contentContainer').attr('data-doi');
	kriya.config.content.customer = $('#contentContainer').attr('data-customer');
	kriya.config.content.project = $('#contentContainer').attr('data-project');
	kriya.config.content.stageFullName = $('#contentContainer').attr('data-stage-name');
	kriya.config.content.stage = $('#contentContainer').attr('data-stage-name').toLowerCase();
	eventHandler.components.general.getReferenceStylus();
	
	if (($(contentDivNode).length > 0) && $(contentDivNode).is(':visible')) {
		$(contentDivNode).attr('contenteditable', 'true');
		$('#contentContainer').css('height', (window.innerHeight - $('#contentContainer').offset().top - $('#footerContainer').height() )+ 'px');
		$('#contentContainer').removeClass('nano');
	}
	$('#navContainer, #navContainer section').attr('style', 'overflow-y:auto;').css('height', $(window).height() - $('#navContainer').offset().top - $('#footerContainer').height());
	
	window.addEventListener("resize", function(){
		$('#contentContainer').css('height', $(window).height() - $('#contentContainer').offset().top);
		$('#navContainer').css('height', $(window).height() - $('#navContainer').offset().top);
	});
	$('[data-type="popUp"]').find('input,textarea').attr("autocomplete","off").attr("autocorrect", "off").attr("autocapitalize", "off").attr("spellcheck", "false");
	$('[data-tooltip]').tooltip({delay: 50});
	
	setTimeout(function(){
		initialiseDragDrop();
		var resizeTimer;
		$('#contentDivNode > br, #contentDivNode div > br').remove();
		/*$('#contentDivNode').find('.jrnlAff[id]:not([data-id]), *[class*="Caption"][id]:not([data-id])').each(function(){
			$(this).attr('data-id', $(this).attr('id'));
		});*/

		var clonedContent = $('<div id="clonedContent" class="nano-content">')
		$('#contentDivNode').after(clonedContent)
		var cid = 1;
		$('#contentDivNodes').find('div.front,div.body,div.back').each(function(){
			var wrapper = $(this).clone();
			if ($(wrapper)[0].hasAttribute('id')){
				$(wrapper).removeAttr('id');
			}else{
				$(this).attr('id', uuid.v4());
				$(wrapper).attr('clone-id', $(this).attr('id'));
			}
			$(wrapper).html('');
			$('#clonedContent').append(wrapper);
			$(this).children().each(function(){
				var childObj = cloneChild($(this), cid);
				var clone = childObj[0];
				cid = childObj[1];
				$(wrapper).append(clone);
				if ($(clone)[0].nodeName == 'DIV'){
					$(this).children().each(function(){
						var childObj = cloneChild($(this), cid);
						var subChild = childObj[0];
						cid = childObj[1];
						if (subChild[0].nodeName == "TABLE"){
							$(this).find('tr').each(function(i,e){
								var childObj = cloneChild($(this), cid);
								var clonedRow = childObj[0];
								cid = childObj[1];
								$(subChild).append(clonedRow);
								$(this).find('th, td').each(function(i,e){
									var childObj = cloneChild($(this), cid);
									var clonedCol = childObj[0];
									cid = childObj[1];
									$(clonedRow).append(clonedCol);
								})
							});
						}
						$(clone).append(subChild);
					})
				}
			})
		})
		
		function cloneChild(node, cid){
			var subChild = $(node).clone();
			$(subChild).html('');
			if (subChild[0].hasAttribute('id')){
				$(subChild).attr('clone-id', $(subChild).attr('id'));
				$(subChild).removeAttr('id');
			}else{
				$(node).attr('id', uuid.v4());
				$(subChild).attr('clone-id', $(node).attr('id'));
			}
			subChild.css({'position': 'relative'})
			if ($(node).css('display') == "none"){
				subChild.height('0');
				subChild.css({'display': 'none'});
			}else{
				subChild.height($(node).outerHeight());
				subChild.css({'display': 'block'});
			}
			$(subChild).css('margin', $(node).css('margin'));
			$(subChild).css('margin-top', $(node).css('marginTop'));
			$(subChild).css('margin-bottom', $(node).css('marginBottom'));
			$(subChild).css('padding-top', $(node).css('paddingTop'));
			$(subChild).css('padding-left', $(node).css('paddingLeft'));
			$(subChild).css('padding-bottom', $(node).css('paddingBottom'));
			$(subChild).css('padding-right', $(node).css('paddingRight'));
			return [subChild, cid];
		}
		
		//$('#clonedContent').height($('#contentDivNode').height());
		$('#contentContainer').scroll(function(){
			//$('#clonedContent').scrollTop($('#contentDivNode').scrollTop())
			//$('.suggestions,.resolve-query').remove();
			$('.templates [data-component]').addClass('hidden');
		});
		
		kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
		kriyaEditor.settings.cloneNode = document.getElementById('clonedContent');
		kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;

	},2000);
	
	$('body').on({
		paste: function (evt){
			if(evt.originalEvent.clipboardData){
				var targetNode = kriya.evt.target || kriya.evt.srcElement;
				var popper     = $(targetNode).closest('[data-component]');
				var tableHTML = evt.originalEvent.clipboardData.getData('text/html');
				tableHTML     = cleanupOnPaste(tableHTML);
				tableHTML     = '<div>'+tableHTML+'</div>';
				if($(tableHTML).find('table').length == 0){
					evt.stopPropagation();
					evt.preventDefault();
					kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table not found in pasted content.' ,icon: 'icon-warning2'});
				}else{
					var tblNode = $(tableHTML).find('table');
					$(tblNode).css('table-layout','');
					tblNode.find('td,th').each(function(){
						//remove the unwanted space
						var cellHtml = $(this).html();
						cellHtml = cellHtml.replace(/\s\s+/g, ' '); //remove the more than one normal space
						cellHtml = cellHtml.replace(/^\s+$/g, ''); // if cell html is full of normal space then remove every space
						//Wrap the para in table cell
						if($(this).find('p').length > 0){
							$(this).find('p').addClass('jrnlTblBody');
						}else{
							$(this).html('<p class="jrnlTblBody">' + cellHtml + '</p>');
						}
					});
					$('.la-container').fadeIn();
					var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.project+'.captions', 'fileData': '<div class="body">'+tblNode[0].outerHTML+'</div>'};
					kriya.general.sendAPIRequest('structurecontent', parameters, function(res){
						if(res.error){
							$('.la-container').fadeOut();
							kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
						}else if(res){
							data = res.replace(/&apos;/g, "\'");
							var response = $('<r>'+ data + '</r>');
							if ($(response).find('response').length > 0 && $(response).find('response content').length > 0 && $(response).find('table').length > 0){
								if($(kriya.config.containerElm).find('.body[data-edited="true"]').length == 0){
										var table = $(response).find('table')
										if(table.length > 0){
											var tableInnerHtml = table[0].outerHTML;
											$(table).each(function(){
												$.each(this.attributes,function(i,a){
													popper.find('table[data-type="floatComponent"]').attr(a.name,a.value)
												})
											})
											popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
											popper.find('table[data-type="floatComponent"]').removeAttr('style');
											popper.find('table[data-type="floatComponent"]').attr('data-focusout-data', 'paste');
											popper.find('.pasteTableDiv').addClass('hidden').html('');
											popper.find('.resetTableBtn').removeClass('disabled');	
										}else{
											kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
										}
										
										$('.la-container').fadeOut();
								}else{
									var parameters = {'customerName' : kriya.config.content.customer, 'projectName' : kriya.config.content.project, 'articleID' : kriya.config.content.project+'.captions', 'content': $(response).find('response content').html()};
									kriya.general.sendAPIRequest('applyHouseRules', parameters, function(applyres){
										if(applyres.error){
											$('.la-container').fadeOut();
											kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
										}else if(applyres){
											var response = $('<r>'+ applyres + '</r>');
											var table = $(response).find('table')
											var tableInnerHtml = table[0].outerHTML;
											if(table.length > 0){
												var tableInnerHtml = table[0].outerHTML;
												$(table).each(function(){
													$.each(this.attributes,function(i,a){
														popper.find('table[data-type="floatComponent"]').attr(a.name,a.value)
													})
												})
												popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
												popper.find('table[data-type="floatComponent"]').removeAttr('style');
												popper.find('table[data-type="floatComponent"]').attr('data-focusout-data', 'paste');
												popper.find('.pasteTableDiv').addClass('hidden').html('');
												popper.find('.resetTableBtn').removeClass('disabled');	
												$('.la-container').fadeOut();
											}else{
												
											}
										}
									}, function(err){
										$('.la-container').fadeOut();
										kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
									});
								}
								
							}							
						}
					}, function(err){
						$('.la-container').fadeOut();
						kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
					});
				}
			}
		}
	}, '[data-type="popUp"] .pasteTableDiv[contenteditable="true"]');
	
	function commentNodesUnder(el){
		var treeWalker = document.createTreeWalker(el,NodeFilter.SHOW_ALL,null,false);
		var commentList = [];
		while (treeWalker.nextNode()){
			// keep only comments
			if (treeWalker.currentNode.nodeType === 8) 
			commentList.push(treeWalker.currentNode);
		}
		return commentList;
	}
	function removeTralingSpaces(node, leading, trailing){
		var textNodes = textNodesUnder(node);
		var tnLength = textNodes.length;
		if (leading){
			for(var t=0;t<tnLength;t++){
				var tNode = textNodes[t];
				if ($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef').length == 0){
					var firstNodeText = tNode.nodeValue;
					if (firstNodeText.match(/^([\u0020]+)/g)){
						tNode.nodeValue = firstNodeText.replace(/^([\u0020]+)/, '');
						break;
					}else{
						break;
					}
				}
			}
		}
		if (trailing){
			for(var t=tnLength-1;t>=0;t--){
				var tNode = textNodes[t];
				if($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef').length == 0){
					var lastNodeText = tNode.nodeValue;
					if(lastNodeText.match(/([\u0020]+)$/g)){
						tNode.nodeValue = lastNodeText.replace(/([\u0020]+)$/, '');
						break;
					}else{
						break;
					}
				}
			}
		}
		return node
	}
	function getStyle(style,attr){
		if(!style) return false;
		matchStr = new RegExp('\\s?'+attr+'[\\-\\w+]*\\s?\\:\\s?[^\\;|$]*','gmi');
		var styles='';
		if(match = style.match(matchStr)){
			if(match.length > 0){
				match.forEach(function(styleAttr){
					styles+= styleAttr;
				});
			}
		}
		return styles ? styles : false;
	}
	function cleanupOnPaste(text){
		text = text.replace(/<!--[\s\S]+?-->/g, '');
		text = text.replace(/[\n\r\t]+/g, ''); // Remove the line break and tab - jagan
		var $result = $('<div></div>').html(text);
		
		//Remove the start end spaces in the para - jagan
		$result.find('p').each(function(){
			removeTralingSpaces(this, true, true);
		});
	
		//Remove the comment node - jagan
		var cNodes = commentNodesUnder($result[0]);
		$(cNodes).each(function(){
			$(this).remove();
		});
		var dataOrg = ['border','margin','padding','background'];
	
		$result.find('td,tr,th').each(function(i,e){
			if(style = e.getAttribute('style')){
				dataOrg.forEach(function(ele){
					if(attr = getStyle(style,ele)){
						if(ele=='background'){
							e.setAttribute('data-table-background-color',attr.replace('background:',''));
						}else{
							e.setAttribute('data-original-'+ele,attr);
						}
					};
				});
			}
		
			if(bgcolor = e.getAttribute('bgcolor')){
				e.setAttribute('data-table-background-color',bgcolor);
				e.removeAttribute('bgcolor');e.removeAttribute('height');
			}
		});
		
		$result.children('style,title,meta,link,script').remove();
		$result.find(':empty:not("td,tr"), colgroup').remove();
		 // replace all styles except bold and italic
		$.each($result.find("*"), function(idx, val) {
			var $item = $(val);
			//Set the new id for the element 
			$item.attr('id', uuid.v4());
			 cleanRoutine($item);
			 
			 function cleanRoutine(rawHTML){
				if ($item.length > 0){
					if (($item.css('font-weight') > 400) ||($item.css('font-weight') == 'bold')){
						 $item.wrap('<strong>');
					}
					if (($item.css('font-style') != 'normal') && ($item.css('font-style') != '')){
						$item.wrap('<em>');
					}
					if (($item.css('text-decoration') == 'underline') && ($item.css('font-style') != '')){
						$item.wrap('<U>');
					}
					if(!/^(TABLE|TD|TH|TBODY|TR|THEAD)$/i.test($item[0].nodeName)){
						$item.removeAttr('style');
					}
					if (!/^(I|SUP|SUB|B|STRONG|EM|U|A|OL|UL|LI|TABLE|TD|TH|TBODY|TR|THEAD)$/i.test($item[0].nodeName)){
						if (!(($item[0].attributes.class) && ($item[0].attributes.class.textContent.match(/^(del|ins|jrnl|Ref|label)/gi)))){
							$item.contents().unwrap();
						}
					}
					return rawHTML;
				}
			}
		});
		$result.find('strong,b').find('b,strong').contents().unwrap();
		$result.find('em,i').find('i,em').contents().unwrap();
		$result.find('span').each(function(){
			if($(this).attr('class') && $(this).attr('class').match(/^jrnl/)){
				$(this).contents().unwrap();
			}
		});
		return $result.html();
	}
	$('body').on({
		mouseenter: function (evt) {
			if ($('.kriyaMenuControl.active').length > 0){
				if (! $(this).find('.kriyaMenuBtn').hasClass('disabled')){
					$('.kriyaMenuControl.active').removeClass('active');
					$(this).addClass('active');
				}
			}
		},
	}, '.kriyaMenuControl');
	$('body').on('click',function(e){	
		if ($('.kriyaMenuControl.active').length > 0 && $(e.target).closest('.kriyaMenuControl').length == 0){
			$('.kriyaMenuControl.active').removeClass('active');
		}
	});
	$('body').on({
		click: function(e){
			if ($('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]').length > 0){
				$('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]')[0].scrollIntoView();
			}
		},
	}, '#validationDivNode [data-clone-id]');
	$('#refListDiv').on('click', '.jrnlRefText, .jrnlTblCaption, .jrnlFigCaption, .jrnlSupplCaption', function (e) {
		if ($(this).hasClass('selectedRef')){
			$(this).toggleClass('selectedRef');
		}else{
			//$('.selectedRef').removeClass('selectedRef');
			$(this).addClass('selectedRef');
		}
		if ($('.selectedRef').length > 0){
			$('#markCited').removeClass('disabled');
			$('#spellMistake').removeClass('disabled');
			$('#notCited').addClass('disabled');
			$('#skipCitation').addClass('disabled');
		}else{
			$('#markCited').addClass('disabled');
			$('#spellMistake').addClass('disabled');
			$('#notCited').removeClass('disabled');
			$('#skipCitation').removeClass('disabled');
		}
	});
	
	$('#untaggedDivNode').on('click', '[data-untag]', function (e) {
		var rid = $(this).attr('data-untag');
		$('#contentDivNode [data-untag="' + rid + '"]')[0].scrollIntoView();
	});

	$('body').on({
		mousedown: function (evt) {
			//To prevent the selction in editor when click on header - jagan
			if($(evt.target).closest('.findSection').length < 1){
				evt.preventDefault();
			}
		},
	}, '#headerContainer');
}

// Additional functions needed to handle table structure edit - MMH
function assignUUIDs(elements){
	if(elements.length){
		$(elements).each(function(i,elem){
			$(elem).attr('id', uuid.v4());
		});
	}
}

/**
 * Additional functions needed to handle table structure edit - MMH
 * Function to set the cursor position in the element
 * @author jagan
 */
function setCursorPosition($element, position) {
	var textChilds = textNodesUnder($element);
	var tLength = 0;
	$(textChilds).each(function(){
		tLength = tLength + this.nodeValue.length;
		if(tLength >= position){
			var remaingLength = tLength-position;
			var thisPos = Math.abs(this.nodeValue.length-remaingLength);
			moveCursor(this, thisPos);
			return false;
		}
	});
}

/**
 * Additional functions needed to handle table structure edit - MMH
 * Function to get the all text node from element
 * https://stackoverflow.com/questions/10730309/find-all-text-nodes-in-html-page
 */
function textNodesUnder(el){
	var n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
	while(n=walk.nextNode()) a.push(n);
	return a;
}

// Additional functions needed to handle table structure edit - MMH
function moveCursor(ele, pos){
    var range,selection;
    if(document.createRange && ele){
        range = document.createRange();
        range.setStart(ele, pos);
        //range.setEnd(ele, pos);
        //range.selectNodeContents(contentEditableElement);
        range.collapse(true);
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }
}