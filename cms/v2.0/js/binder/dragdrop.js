/*https://codepen.io/sagarpatil/pen/LEZLav*/
// function that handles the dragstart event
function dragStart(e) {
    e.stopPropagation();
    var boxObj = $(this);
    boxObj.addClass('is-dragged');

    // when an element is dragged the preview image is needed to show that the element is being dragged
    //  for some reason the default image is not clear, so did some tweaks to get the working
    var width = boxObj.width()/1.5;
    var height = boxObj.height()/1.5;

    var dragImage = boxObj.clone();
    dragImage.removeAttr('id').removeClass('uaa').addClass('uaaDrag');
    $('#dragImage').removeClass('hide')
    .width(width + 20).height(height + 20)
    .html(dragImage)
    .find('.uaaDrag').width(width-10).height(height + 10);

    e.originalEvent.dataTransfer.setDragImage($('#dragImage .uaaDrag').get(0), 0, 0);
    e.originalEvent.dataTransfer.effectAllowed = 'copy';
}

function dragEnd(e) {
    e.stopPropagation();
    $('#dragImage').html('').addClass('hide');
    $(this).removeClass('is-dragged');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
    $('.dropTop').removeClass('dropTop');
    $('.dropBottom').removeClass('dropBottom');
}

function dragEnter(e) {
    if (e.stopPropagation) e.stopPropagation();
    $('.dropArrow.icon-arrow_forward').removeClass('hide');
}

function dragLeave(e) {
    if (e.stopPropagation) e.stopPropagation();
    $(this).removeClass('dropTop').removeClass('dropBottom');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
}

/**
 * based on were you are dragging over the droppable element, show a clue as to whether
 *  the dropped element is to be placed above or below the droppable element
 * @param {*} e
 */
function dragOver(e) {
    if (e.stopPropagation) e.stopPropagation();
    e.preventDefault();
    e.originalEvent.dataTransfer.dropEffect = 'copy';
    var clientX = e.originalEvent.clientX;
    var clientY = e.originalEvent.clientY;
    var thisPosition = $(this).position();
    var halfHeight = thisPosition.top + ($(this).height()/2)
    var leftValue = thisPosition.left + parseInt($(this).css('margin-left')) - parseInt($('.dropArrow').css('font-size'));
    var topValue = thisPosition.top;
    if (halfHeight > clientY){
        $(this).addClass('dropTop').removeClass('dropBottom');
    }
    else{
        topValue += $(this).height();
        $(this).addClass('dropBottom').removeClass('dropTop');
    }
    $('.dropArrow.icon-arrow_forward').removeClass('hide').css({top: topValue, left: leftValue})
    return false;
}

function drop(e) {
    $('.la-container').fadeIn();
    if (e.stopPropagation) e.stopPropagation();
    var droppedOnNode = $(e.currentTarget);
    var dropTop = false;
    if (droppedOnNode.hasClass('dropTop')){
        dropTop = true;
    }

    var draggedNode = $('.is-dragged');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
    droppedOnNode.removeClass('dropTop').removeClass('dropBottom');
    var entry = '';
    if (draggedNode.attr('data-obj')){
        var entryData = draggedNode.attr('data-obj');
        entryData = entryData.replace(/\'/gi, '"');
        entry = JSON.parse(entryData);
    }
    else{
        entry = draggedNode.data('data');
    }
    var dataType = ' data-type="manuscript"';
    if (entry._attributes && entry._attributes.type) {
        dataType = ' data-type="' + entry._attributes.type + '"';
    }
    var dataGroupType = '';
    if (entry._attributes && entry._attributes.grouptype) {
        dataGroupType = ' data-grouptype="' + entry._attributes.grouptype + '"';
    }
    var runOnFlag = 'false'; // check if run-on article
    if (entry._attributes && entry._attributes['data-run-on']) {
        runOnFlag =  entry._attributes['data-run-on'];
    }
    //check if unpaginated
    var unpaginated = 'false';
    if(entry._attributes && entry._attributes['data-unpaginated']){
        unpaginated = entry._attributes['data-unpaginated']
    }
    // get the last MS container
    var lastNode = $('#tocBodyDiv div[data-c-id]:last');
    var lastNodeID = parseInt(lastNode.attr('data-c-id'));
    var tempID = lastNodeID + 1;
    // for blank/filler/filler-toc/advert add tempID to entry id to generate unique ID
    if (/^(blank|advert|filler|filler-toc)$/gi.test(entry._attributes.type)) {
        entry._attributes['id'] +=  tempID;
    }
    var msConObj = {
        "dataType": dataType,
        "tempID": tempID,
        "sectionIndex": droppedOnNode.attr('data-sec').replace('sec',''),
        "dataGroupType":dataGroupType
    }
    var msContainer = getMSContainer(entry, msConObj);
    if ((msContainer == '') || (typeof(msContainer) === 'undefined')){
        showMessage({ 'text': 'Unable to add the requested file. Please try again or contact support', 'type': 'error' });
        return false;
    }
     if((dataType !=' data-type="filler"')&&(dataType !=' data-type="filler-toc"')) {
        if (droppedOnNode.hasClass('dropTop')){
            $(msContainer).insertAfter(droppedOnNode);
        }
        else{
            $(msContainer).insertBefore(droppedOnNode);
        }
    }
    else{  //if the drop node is filler/filler-toc, then add it only after manuscript
        if(droppedOnNode.attr("data-type") == 'manuscript'){
            $(msContainer).insertAfter(droppedOnNode);
        }
        else{ //if filler/filler-toc is added after blank/advert display error msg
           // $(msContainer).insertBefore(droppedOnNode);
           showMessage({ 'text': 'Filler/Filler TOC can oly be added below Manuscript ', 'type': 'error' });
            $('.la-container').fadeOut();
            return false;
        }
    }
    entry._attributes.section = $(droppedOnNode).data('data')._attributes.section;
    if(($(droppedOnNode).data('data')._attributes.grouptype) &&(/^(roman|front|back)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))){
        entry._attributes.grouptype = $(droppedOnNode).data('data')._attributes.grouptype;
        saveData();
    }
    var romanFlag = false
    if(($(droppedOnNode).data('data')._attributes.grouptype) &&(/^(roman)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))){
        romanFlag = true
    }
    $('[data-c-id="' + tempID + '"]').removeClass('hide');
    var lastNode = $('#tocBodyDiv div[data-c-id]:last');
    var lastNodeID = parseInt(lastNode.attr('data-c-id'));
    //push the 'entry' data into array as if its the last node, then push it through the array to it actual position
    var seqID = tempID;
    while(lastNodeID != tempID){
        $('#tocContainer').data('binder')['container-xml'].contents.content[lastNodeID + 1] = $('#tocContainer').data('binder')['container-xml'].contents.content[lastNodeID];
        lastNode.attr('data-c-id', seqID--);
        lastNode = $(lastNode).prevAll('[data-c-id]:first');
        lastNodeID = parseInt(lastNode.attr('data-c-id'));
    }

    lastNode.attr('data-c-id', seqID).data('data', entry);
    $('#tocContainer').data('binder')['container-xml'].contents.content[seqID] = entry;
    // to determine the start page number of the currently added MS look for page number in the previous node
    // previous node will be something like manuscript or blank or ..., else it would be section, if its a section get the first page, else get the last page
    var prevNode = lastNode.prev();
    var startPage = 1;
    if (prevNode){
        if (prevNode.attr('data-type') == 'section'){
            if ($('[data-type="section"]:first .rangeStart').length > 0){
                if(unpaginated == 'true'){  // if run-on article, set the prev node lpage as fpage 
                    startPage = parseInt($(droppedOnNode).data('data')._attributes.fpage) - 1;
                }
                else{
                // startPage = parseInt($('[data-type="section"]:first .rangeStart').text());
                startPage = parseInt($(droppedOnNode).data('data')._attributes.fpage);
                }
            }
        }
        else{
            if(runOnFlag == 'true'){      
                startPage = parseInt(prevNode.data('data')._attributes.lpage);
            }
            else if(romanFlag == true){  
                startPage = fromRoman(prevNode.data('data')._attributes.lpage);    
                startPage = parseInt(startPage) + 1;
            }
            else{
                startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
            }
           // startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
        }
    }
    if(unpaginated != 'true'){
        updatePageNumber(seqID, startPage,entry._attributes.grouptype);
    }
    if (!draggedNode.attr('data-obj')){
        //dont remove the card for blank/advert/filler
		if ((entry._attributes.type != 'blank') && (entry._attributes.type != 'advert') && (entry._attributes.type != 'advert_unpaginated') && (entry._attributes.type != 'filler') && (entry._attributes.type != 'filler-toc')){
			draggedNode.remove();
		}
	}
    saveData();
    $('.la-container').fadeOut();
    return false;
}