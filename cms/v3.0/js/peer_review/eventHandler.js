/**
 * eventHandler - this javascript holds all the functions required for Reference handling
 *				 so that the functionalities can be turned on and off by just calling the required functions
 *				 Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
 */
var eventHandler = function () {
	//default settings
	var settings = {};
	var proofInterval, proofStartTime;

	settings.subscriptions = {};
	settings.subscribers = ['menu', 'query', 'components', 'welcome'];
	//add or override the initial setting
	var initialize = function (params) {
		function extend(obj, ext) {
			if (obj === undefined) {
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};



	var getSettings = function () {
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;
	var proofInterval = 120000; //120secs
	var proofStage = '', proofStartTime = new Date().getTime();

	eventHandler.publishers = {
		add: function () {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel',
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function () {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		domInsertCBFunc: function (event) {
			if (/ins\b/.test(event.target.className) || /del\b/.test(event.target.className)) {
				if (!$('#changesDivNode').length) {
					$('#navigationContentDiv container-for-navigation').append('<div id="changesDivNode" class="sub-menu-divs"></div>');
				}
				var changeID = event.target.getAttribute('data-cid');
				var userName = event.target.getAttribute('data-username');
				var objDate = new Date(new Date(parseInt(event.target.getAttribute('data-time'))));
				var locale = "en-us",
					month = objDate.toLocaleString(locale, { month: "short" });
				var objDate = objDate.getDate() + ' ' + month + ' ' + (objDate.getYear() + 1900) + ' (' + objDate.getHours() + ':' + objDate.getMinutes() + ')';
				var changeHTML = '<span class="';
				if (/ins\b/.test(event.target.className)) {
					changeHTML = changeHTML + 'insertion" data-rid="' + changeID + '"><b>' + userName + '</b> on ' + objDate + '<br/><b>Inserted: </b><span class="changeData">&nbsp;</span></span>'
				}
				else if (/del\b/.test(event.target.className)) {
					changeHTML = changeHTML + 'deletion" data-rid="' + changeID + '"><b>' + userName + '</b> on ' + objDate + '<br/><b>Deleted: </b><span class="changeData">&nbsp;</span></span>'
				}
				var changeNode = $('#navigationContentDiv [data-rid="' + changeID + '"]');
				if (changeNode.length > 0) {
					changeNode.replaceWith(changeHTML);
				} else {
					$('#changesDivNode').append(changeHTML);
				}
			}
		},
		eventCallbackFunction: function (event) {
			if (typeof kriya == "undefined") return false;
			// event.target is the clicked element!
			kriya.evt = event;
			if (typeof (event.pageX) != "undefined") {
				kriya.pageXaxis = event.pageX;
				kriya.pageYaxis = event.pageY;
			}

			var highlighter = false;
			//check if the cuurent click position has a underlay behind it
			var elements = elementsFromPoint(event.pageX, event.pageY);
			$(elements).each(function () {
				//when we close author query popup if behind there any components(Author group or keyword group) at that time contentEvt() is triggering but no need to trigger contentEvt() because of that code is breaking in contentEvt()
				//if ($(this).hasClass('highlight') && !$(this).hasClass('textSelection')){
				if ($(this).hasClass('highlight') && !$(this).hasClass('textSelection') && !$(this).hasClass('queryText')) {
					highlighter = true;
				}
			});
			if (!highlighter) {
				elements = $(event.target);
			}

			$(elements).each(function () {
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length) {
					targetNode = $(targetNodes[0]);
					var message = eval('(' + targetNode.attr('data-message') + ')');
					var eveDetails = message[event.type];
					if (!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type) {
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic = targetNode.attr('data-topic');
					} else if (typeof (eveDetails) == "string" && typeof (message[eveDetails]) == "object") {
						eveDetails = message[eveDetails];
					} else if (!eveDetails || typeof (eveDetails) == "string") {
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic: eveDetails.topic + '.' + event.type,
						data: {
							message: eveDetails,
							event: event.type,
							target: targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function () {
			var subscribers = eventHandler.settings.subscribers;
			for (var i = 0; i < subscribers.length; i++) {
				initSubscribe(subscribers[i], subscribers[i] + '_subscription');
			}

			function initSubscribe(subscribeChannel, subscribeName) {
				if (!eventHandler.settings.subscriptions[subscribeName]) {
					eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
						channel: subscribeChannel,
						topic: "*.*",
						callback: function (data, envelope) {
							//console.log(data, envelope);
							if (data.message != '') {
								/*http://stackoverflow.com/a/3473699/3545167*/
								if (typeof (data.message) == "object") {
									var array = data.message;
								} else {
									var array = eval('(' + data.message + ')');
								}
								var functionName = array.funcToCall;
								var param = array.param;
								var topic = envelope.topic.split('.');
								//var fn = 'eventHandler.menu.edit.'+functionName;
								if (typeof (window[functionName]) == "function") {
									window[functionName](param, data.target);
								} else if (typeof (window['eventHandler'][envelope.channel][topic[0]]) == "object") {
									if (typeof (window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function') {
										window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
									}
								} else {
									console.log(functionName + ' is not defined in ' + topic[0]);
								}
							} else {
								console.log('Message is empty');
							}

							// `data` is the data published by the publisher.
							// `envelope` is a wrapper around the data & contains
							// metadata about the message like the channel, topic,
							// timestamp and any other data which might have been
							// added by the sender.
						}
					});
				}
			}
			eventHandler.settings.subscriptions['citation_reorder'] = postal.subscribe({
				channel: 'citejs',
				topic: "reorder",
				callback: function (data, envelope) {
					if ((data.element && data.element.length > 0 && data.type) || data.citeClassArray) {
						var renumberStatus = [];
						var renumberStatusString = "";

						if (!data.citeClassArray) {
							$('[data-cite-type]').removeAttr('data-cite-type');
							$(data.element).attr('data-cite-type', data.type);
							var classNameArray = [];
							var trackNode = $('[data-cite-type]').closest('.ins, .del, [data-track="ins"], [data-track="del"]');
							$(data.element).each(function () {
								var ridString = $(this).attr('data-citation-string');
								var className = $(this).attr('class');
								className = className.replace(/^\s+|\s+$/g, '').split(' ')[0];
								if (!ridString) {
									return;
								}
								ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
								ridString = ridString.replace(/^\s+|\s+$/g, '').split('-');
								var citeConfig = citeJS.floats.getConfig(ridString[0]);
								if (citeConfig && citeConfig.reorder != false && ($(this).closest('.' + citeConfig.reorder).length > 0 || !citeConfig.reorder) && classNameArray.indexOf(className) < 0) {
									classNameArray.push(className);
								}
							});
						} else {
							classNameArray = data.citeClassArray;
						}

						$.each(classNameArray, function (i, classVal) {
							if (classVal == 'jrnlSupplRef') {
								var suppIdArray = [];
								$(kriya.config.containerElm + ' .' + classVal).each(function () {
									if ($(this).closest('.del, [data-track="del"]').length > 0) {
										return;
									}
									var cId = $(this).attr('data-citation-string');
									if (!cId.match(/\-/g)) {
										cId = citeJS.general.getPrefix(cId);
										if (suppIdArray.indexOf(cId) < 0) {
											suppIdArray.push(cId);
											citeSeqObject = citeJS.floats.checkSequence(classVal, cId);
											var reorderObj = citeJS.floats.reorder();
											if (renumberStatus.length == 0) {
												renumberStatus = renumberStatus.concat(reorderObj);
											} else {
												for (var r = 0; r < reorderObj.length; r++) {
													renumberStatus[r] = renumberStatus[r].concat(reorderObj[r]);
												}
											}

										}
									}
								});
							} else {
								citeSeqObject = citeJS.floats.checkSequence(classVal);
								renumberStatus = citeJS.floats.reorder();
							}



							// Add the changed citations in the stack to save the content
							if (renumberStatus && renumberStatus[0] && renumberStatus[0].length > 0) {
								for (var c = 0; c < renumberStatus[0].length; c++) {
									var citeElement = $(kriya.config.containerElm + ' #' + renumberStatus[0][c]);
									citeElement = (citeElement.length == 0) ? $('#navContainer #' + renumberStatus[0][c]) : citeElement;
									kriyaEditor.settings.undoStack.push(citeElement);
								}
							}
							//Add the changed floats in stack to save the content
							if (renumberStatus && renumberStatus[1] && renumberStatus[1].length > 0) {
								for (var f = 0; f < renumberStatus[1].length; f++) {
									var citeElement = $(kriya.config.containerElm + ' #' + renumberStatus[1][f]);
									citeElement = (citeElement.length == 0) ? $('#navContainer #' + renumberStatus[1][f]) : citeElement;
									kriyaEditor.settings.undoStack.push(citeElement);
								}
							}
							renumberStatusString += renumberStatus[2];
							//Remove the data-cite-type attribute for the reordered class
							if ($('.' + classVal + '[data-cite-type]').length > 0) {
								$('.' + classVal + '[data-cite-type]').each(function () {
									$(this).removeAttr('data-cite-type');
									kriyaEditor.settings.undoStack.push(this);
								});
							}
						});


						if (trackNode && trackNode.length > 0) {
							if (renumberStatusString && renumberStatusString != "") {
								trackNode.attr('data-track-detail', renumberStatusString);
							}
							//$(trackNode).attr('data-callback','kriya.general.rejectReorder');
							//eventHandler.menu.observer.nodeChange({'node':trackNode});
							kriyaEditor.settings.undoStack.push(trackNode);
						}
						//Update the status after citation reorder
						citeJS.general.updateStatus();

					} else {
						console.log('Data missing reorder failed.');
					}
				}
			});
		},
		remove: function () {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.components = {
		general: {
			contentEvt: function (param, targetNode) {
				var target = kriya.evt.target || kriya.evt.srcElement;
				var range = rangy.getSelection();
				if (range.anchorNode == null && range.rangeCount == 0) {//clicked some where outside the content - JAI 19-02-2018
					return false;
				}
				kriya.selection = range.getRangeAt(0);
				kriya.selectedNodes = kriya.selection.getNodes();

				//Update menu bar visibility
				$('.kriyaMenuBar .kriyaMenuControl .kriyaMenuItems [data-display-selector]').each(function(){
						var selector = $(this).attr('data-display-selector');
						var blockNode = $(kriya.selection.startContainer).closest('div:not(.front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,.nano),p,h1,h2,h3,h4,h5,h6,table');
						if($(blockNode).is(selector) == true){
							$(this).removeClass('hidden');
						}else{
							$(this).addClass('hidden');
						}
				});
				
				//some times target come as null
				if (target && target.className.split(" ").length > 0) {
					var componentName = target.className.split(" ")[0];
				}
				//Show/hide the new and edit elements
				var component = $('[data-component="' + componentName + '"]');
				if (component.length > 0 && $(component).find('[data-pop-type]').length > 0) {
					$(component).find('[data-pop-type]').addClass('hidden');
					$(component).find('[data-pop-type="edit"]').removeClass('hidden');
				}
				// for context menu
				var componentName = 'contextMenu';
				selection = rangy.getSelection();
				if ($('#contentContainer').attr('data-state') != 'read-only' && $('[data-component="' + componentName + '"]').attr('data-selection') != undefined) {
					kriya.selection = selection.getRangeAt(0);
					/*if (kriya.selection.toString() == ""){
						return;
					}*/
					if (kriya.selection.toString() != "") {
						kriya.config.activeElements = $(target);
						// If target is span element then pass the parent element - Rajesh
						//if target have deleted node no need to show contextmenu.for that added in if(&&!$(kriya.selection.getNodes()).hasClass('del'))-kirankumar
						if ($(kriya.selection.getNodes()).last().closest('.del').length == 0 && $(kriya.selection.getNodes()).first().closest('.del').length == 0) {
							/*if(kriya.config.activeElements[0].tagName == "SPAN" || kriya.config.activeElements[0].tagName == "B" || kriya.config.activeElements[0].tagName == "I" || kriya.config.activeElements[0].tagName == "U" || kriya.config.activeElements[0].tagName == "SUP" || kriya.config.activeElements[0].tagName == "SUB"){
								kriya['styles'].init($(target).parent(), componentName);
							}else{
								kriya['styles'].init($(target), componentName);
							}*/
							//When we select Formated text at that time insertquery option not come-kirankumar
							var targetele = $(target);
							var tags = ["SPAN", "B", "I", "U", "SUP", "SUB", "EM"];
							//while(tags.includes(targetele[0].tagName)){ some times hear returning this error Object does not support 'includes' property or method.
							while (tags.indexOf(targetele[0].tagName) >= 0) {
								targetele = $(targetele[0]).parent();
							}
							kriya['styles'].init(targetele, componentName);
						}
					}
				}
				if (kriya.selection.toString() == "") {
					componentName = 'pasteCitation';
				}

				$('.activeElement').removeClass('activeElement');
				$(kriya.selection.startContainer).closest('div:not(.front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,.nano),p,h1,h2,h3,h4,h5,h6,table').addClass('activeElement');

				//to avoid showing up cite now in front matter and in reference - jai
				if ($('[data-component="' + componentName + '"]').length > 0 && kriya.config.content.copiedCitation && $(target).closest('.front,.jrnlRefText').length == 0) {
					if (kriya.config.preventTyping && ($(target).closest(kriya.config.preventTyping).length > 0)) {
						kriya.notification({ title: 'Info', type: 'error', timeout: 5000, content: 'Place the cursor in valid position.', icon: 'icon-warning2' });
					} else {
						kriya['styles'].init($(target), componentName);
					}
				}


				var menuTarget = kriya.evt.target || kriya.evt.srcElement;
				if ($('[data-component="pasteFootNoteLink"]').length > 0 && kriya.config.content.copiedTableLink && $(menuTarget).closest('.del,[data-track="del"]').length < 1) {
					if ($(menuTarget).closest('table').length > 0 || $(menuTarget).closest('.jrnlTblCaption').length > 0) {
						var linkId = $(kriya.config.content.copiedTableLink).attr('data-citation-string');
						if (linkId) {
							linkId = linkId.replace(/^\s+|\s$/g, '');
							linkId = linkId.replace(/_FN(\d+)/g, '');
							if ($(target).closest('[data-id="BLK_' + linkId + '"]').length > 0) {
								kriya['styles'].init($(target), 'pasteFootNoteLink');
							}
						}
					} else {
						kriya.notification({ title: 'Info', type: 'error', timeout: 5000, content: 'Place the cursor in valid position.', icon: 'icon-warning2' });
					}
				}

				//For table floating menu
				/* Sathiyad : for LangTrans table Applicable for jb  */
				/*if($(menuTarget).closest('table').length > 0 && $(menuTarget).closest('.del,[data-track="del"]').length < 1){
					kriya['styles'].init($(menuTarget).closest('table'), 'tableMenu');
				}*/


				$('.class-highlight').text($(kriya.selection.startContainer).closest('*[class]').attr('class').split(' ')[0].replace(/^(jrnl|Ref)/, '').replace(/([a-z])([A-Z])/g, '$1 $2'));

			},
			validateEmailAddress: function(param, targetNode) {
				if ($(targetNode).text() != "" && !$(targetNode).text().match(/^([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4})(([\;\,]\s?)([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4}))*$/)) {
					$(targetNode).attr('data-error', 'Please enter the valid email');
					$(targetNode).closest('[data-type="popUp"]').find('.mailConfirmBtn').addClass('disabled');
					return false;
				}else{
					$(targetNode).removeAttr('data-error');
					$(targetNode).closest('[data-type="popUp"]').find('.mailConfirmBtn').removeClass('disabled');
				}
			},
			replaceManuscript: function(param, targetNode){
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'process' : 'get'};
				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
				 	if (res && res.status && res.status == 500){
						// do nothing
					}else{
						var filesList = $(res).html();
						$('[data-component="fileDesignator_edit"] .files_list').html(filesList);
						$('[data-component="fileDesignator_edit"] .uploadedFile').text('');
					}
					$('[data-component="fileDesignator_edit"] .file-table .file-case:not(:first)').remove();
					//Show the mandatory files when re-submitting manuscript - jagan - 05/04/2019
					var mandatoryLen = $('[data-component="fileDesignator_edit"] .file-table .file-case:first .file-name select [data-mandatory="true"]:not(:first)').length;
					$('[data-component="fileDesignator_edit"] .file-table .file-case:first .file-name select [data-mandatory="true"]:not(:first)').each(function(i){
							var mandatoryVal = $(this).val();
							var acceptFile = $(this).attr('data-accept-file');
							var row = $(this).closest('.file-case');
							var clone = $(row).clone();
							$(clone).find('input[type="file"]').removeAttr('accept');
							if(acceptFile){
								$(clone).find('input[type="file"]').attr('accept', acceptFile);
							}

							$(clone).find('.uploadedFile').text('');
							$(clone).find('.removeFile').removeClass('disabled');
							if(i+1 != mandatoryLen){
								$(clone).find('.addNewFile').addClass('disabled');
							}else{
								$(clone).find('.addNewFile').removeClass('disabled');
							}
							
							$(clone).find('.file-name select').val(mandatoryVal);
							clone.removeAttr('file_name').removeAttr('file_alt_name').removeAttr('file_extension').removeAttr('file_path').removeAttr('file_href');
							$('[data-component="fileDesignator_edit"] .file-table .file-case:last').after(clone);
					});

					$('.la-container').fadeOut();
					kriya.popUp.openPopUps($('[data-component="fileDesignator_edit"]'));
				});
			},
			changeDropDown: function(param, targetNode){
				var row = $(targetNode).closest('.file-case');
				$(row).find('input[type="file"]').removeAttr('accept');
				var acceptFile = $(targetNode).find(':selected').attr('data-accept-file');
				if(acceptFile){
					$(row).find('input[type="file"]').attr('accept', acceptFile);
				}
			},
			addNewFile: function(param, targetNode){
				var row = $(targetNode).closest('.file-case');
				var clone = $(row).clone();

				$(clone).find('input[type="file"]').removeAttr('accept');
				var acceptFile = $(clone).find('select :selected').attr('data-accept-file');
				if(acceptFile){
					$(clone).find('input[type="file"]').attr('accept', acceptFile);
				}

				$(clone).find('.uploadedFile').text('');
				$(clone).find('.removeFile').removeClass('disabled');
				clone.removeAttr('file_name').removeAttr('file_alt_name').removeAttr('file_extension').removeAttr('file_path').removeAttr('file_href');
				$(targetNode).addClass('disabled');
				$(row).after(clone);
			},
			removeFile: function(param, targetNode){
				var fileTable = $(targetNode).closest('.file-table')
				$(fileTable).find('.file-case').find('.addNewFile').addClass('disabled');
				$(targetNode).closest('.file-case').remove();
				if ($(fileTable).find('.file-case').length > 1){
					$(fileTable).find('.file-case:last').find('.addNewFile').removeClass('disabled');
				}else{
					$(fileTable).find('.file-case:first').find('.addNewFile').removeClass('disabled');
				}
			},
			filterReviewer: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var filteredRows = popper.find('table tbody tr');
				popper.find('.filterReviewer').each(function(){
					var target = this;
					var columnIndex = $(target).closest('th').index();
					var filterTex = $(target).val();
					filteredRows.each(function(){
						if($(this).find('td:eq(' + columnIndex + '):contains(' + filterTex + ')').length > 0){
							$(this).removeClass('hidden');
						}else{
							$(this).addClass('hidden');
						}
					});
					filteredRows = popper.find('table tbody tr:visible');
				});
			},
			openEmptyPopup: function(param, targetNode){
				if(param && param.component){
					$('[data-component="' + param.component + '"] [data-type="htmlComponent"]').html('');
					kriya['styles'].init($(targetNode), param.component);
					$('[data-component="' + param.component + '"]').removeAttr('data-node-xpath');
					//kriya.popUp.openPopUps($('[data-component="' + param.component + '"]'));					
					$('[data-component="' + param.component + '"] .popupHead[data-pop-type="new"]').removeClass('hidden');
					$('[data-component="' + param.component + '"] .popupHead[data-pop-type="edit"]').addClass('hidden');
				}
			},
			addNewUserConfirm: function(param, targetNode){
				if(param && !param.accessLevel){
					return false;
				}
				var popper = $(targetNode).closest('[data-component]');
				popper.find('[data-validate]').each(function(){
					if($(this).attr('data-validate').match(/required/)){
						if($(this).text() == ""){
							$(this).attr('data-error', 'Input required');
						}else{
							$(this).removeAttr('data-error');
						}
					}
					if($(this).attr('data-validate').match(/email/) && $(this).text() != ""){
						if (!$(this).text().match(/^([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4})(([\;\,]\s?)([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4}))*$/)) {
							$(this).attr('data-error', 'Please enter the valid email');
						}else{
							$(this).removeAttr('data-error');
						}
					}
				});
				if(popper.find('[data-error]').length > 0){
					return false;
				}

				var expertiseKeys = [];
				popper.find('#expertise-keyword .kriya-tagsinput .tags').each(function(){
					expertiseKeys.push($(this).text());
				});

				var parameters = {
					'customer' : kriya.config.content.customer,
					'title' : popper.find('[data-class="title"]').text(),
					'firstName' : popper.find('[data-class="fname"]').text(),
					'lastName'  : popper.find('[data-class="lname"]').text(),
					'email' : popper.find('[data-class="email"]').text(),
					'institution' : popper.find('[data-class="institution"]').text(),
					'city' : popper.find('[data-class="city"]').text(),
					'country' : popper.find('[data-class="country"]').text(),
					'expertise' : expertiseKeys,
					'roles' : [{
						"access-level": param.accessLevel,
						"role-type": 'production',
						"customer-name" : kriya.config.content.customer
					}]
				};

				var eleNodeXPath = popper.attr('data-node-xpath');
				var userRow  = "";
				if(eleNodeXPath){
					userRow = kriya.xpath(eleNodeXPath);
					parameters['modifiedData'] = "true";
					parameters['fileName'] = $(userRow).attr('data-file-name');
				}

				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
					if(param.component){
						
						var eleNodeXPath = popper.attr('data-node-xpath');
						var userRow = kriya.xpath(eleNodeXPath);
						
						var reviewerAff = ('<span class="reviewer-institution">' + parameters.institution + '</span>') + ((parameters.institution && parameters.city)?', ':'') + ('<span class="reviewer-city">' + parameters.city + '</span>') + ((parameters.country && parameters.city)?', ':'') + ('<span class="reviewer-country">' + parameters.country + '</span>') + ((parameters.country)?'.':'');
						if($(userRow).length > 0){
							$(userRow).find('.reviewer-title').html(parameters.title);
							$(userRow).find('.reviewer-surname').html(parameters.firstName);
							$(userRow).find('.reviewer-given-name').html(parameters.lastName);
							$(userRow).find('.reviewer-email').html(parameters.email);
							$(userRow).find('.reviewer-aff').html(reviewerAff);
						}else{
							var dropDownId = 'assign-dropdown' + ($('.assignReviewerBtn').length + 1);
							var userEmail = $('.userProfile .username').attr('data-user-email');
							var userName = $('.userProfile .username').text();
							if(param.accessLevel == "associateeditor"){
								var userRow = $('<tr><td><span class="reviewer-title">' + parameters.title + '</span> <span class="reviewer-surname">' + parameters.firstName + '</span> <span class="reviewer-given-name">' + parameters.lastName + '</span><br /> <span class="reviewer-email">' + parameters.email + '</span><br /><span class="reviewer-aff">' + reviewerAff + '</span>  <br /> <span class="btn btn-small green lighten-1" data-message="{\'click\':{\'funcToCall\': \'editUser\', \'param\' : {\'component\' : \'add_editor_edit\'}, \'channel\':\'components\',\'topic\':\'general\'}}">Edit</span><span class="btn btn-small red lighten-1" style="margin-left: 0.5rem;" data-message="{\'click\':{\'funcToCall\': \'deleteUser\', \'channel\':\'components\',\'topic\':\'general\'}}">Delete</span> </td><td><span class="btn btn-small green lighten-1" data-currStage="assignassociateeditor" data-work-name="associate-editorcheck" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\',\'param\': {\'userMailID\': \' ' + userEmail + ' \', \'userName\': \' ' + userName + ' \'} ,\'channel\':\'components\',\'topic\':\'general\'}}">Send</span></td></tr>');
							}else{
								var userRow = $('<tr><td><span class="reviewer-title">' + parameters.title + '</span> <span class="reviewer-surname">' + parameters.firstName + '</span> <span class="reviewer-given-name">' + parameters.lastName + '</span><br /> <span class="reviewer-email">' + parameters.email + '</span><br /><span class="reviewer-aff">' + reviewerAff + '</span>  <br /> <span class="btn btn-small green lighten-1" data-message="{\'click\':{\'funcToCall\': \'editUser\', \'param\' : {\'component\' : \'add_reviewer_edit\'}, \'channel\':\'components\',\'topic\':\'general\'}}">Edit</span><span class="btn btn-small red lighten-1" style="margin-left: 0.5rem;" data-message="{\'click\':{\'funcToCall\': \'deleteUser\', \'channel\':\'components\',\'topic\':\'general\'}}">Delete</span> </td><td style="text-align:center;" class="reviewer-expertise"></td><td style="text-align:center;"><span class="reviewer-article-assigned">0</span></td><td style="text-align:center;"><span class="reviewer-article-declined">0</span></td><td style="text-align:center;"><span class="reviewer-article-inprogress">0</span></td><td style="text-align:center;"><span class="reviewer-article-completed">0</span></td><td><span class="btn btn-small green lighten-1 assignReviewerBtn" data-activates="' + dropDownId + '">Assign</span><ul id="' + dropDownId + '" class="dropdown-content dropdown-list"><li class="dropdown-assignReviewer" data-work-name="assignToReviewer" data-currStage="associateediter-assign-reviewer" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\', \'param\': {\'userMailID\': \' ' + userEmail + ' \', \'userName\': \' ' + userName + ' \'}, \'channel\':\'components\',\'topic\':\'general\'}}">Assign Now</li><li class="dropdown-addBackup" data-message="{\'click\':{\'funcToCall\': \'addBackUpReviewer\', \'channel\':\'components\',\'topic\':\'general\'}}">Assign as back-up</li></ul></td></tr>');
							}
							
							$('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(.current-version)').each(function(){
								var verText    = $(this).text();
								var versionCol = $('<td class="reviewer-version-history" data-version="' + verText + '"></td>');
								if($(userRow).find('.assignReviewerBtn').length > 0){
									$(userRow).find('.assignReviewerBtn').closest('td').before(versionCol);
								}else{
									$(userRow).find('td:last').before(versionCol);
								}
							});

							
							$('[data-component="' + param.component + '"] table tbody').append(userRow);
							$('[data-activates="' + dropDownId + '"]').dropdown();
						}

						$(userRow).find('.reviewer-expertise').html('');
						popper.find('#expertise-keyword .kriya-tagsinput .tags').each(function(){
							var expertiseText = '<span class="expertise-key">' + $(this).text() + '</span>';
							$(userRow).find('.reviewer-expertise').append(expertiseText);
						});

					}

					$('.la-container').fadeOut();
					kriya.popUp.closePopUps(popper);
				}, function(err){
					$('.la-container').fadeOut();
				});
			},
			addReviwer: function (param, targetNode) {
				var popper = $(targetNode).closest('[data-component]');
				var tempNode = $(targetNode).closest('p[data-role-for]').clone();
				var roleFor = tempNode.attr('data-role-for');
				tempNode.find('input').remove();
				var removeOption = $('[data-component="signoff_edit"] [data-template] p[data-role-for="' + roleFor +'"]').find('.removeOption').clone();
				$(targetNode).closest('p[data-role-for]').append(removeOption);
				$(targetNode).remove();
				$(targetNode).closest('p[data-role-for]').after($(tempNode));
			},
			removeReviwer: function (param, targetNode) {
				$(targetNode).closest('p[data-role-for]').remove();
			},
			triggerUpload: function(param, targetNode){
				$(targetNode).parent().find('input').val('');
				$(targetNode).parent().find('input').trigger('click');
			},
			uploadManuscript: function(param, targetNode){
				var currFileName = targetNode[0].files[0].name;
				currFileName = currFileName.replace(/\%20/g, ' ');
				$(targetNode).closest('.row.file-case').find('.uploadedFile').text(currFileName);
			},
			uploadManuscriptOld: function(param, targetNode){
				var blockNode = $(targetNode).closest('[data-type="popUp"]');
				$('.replace-in-progress').removeClass('replace-in-progress');
				$(targetNode).closest('.row').addClass('replace-in-progress');
				var param = {
					'file': targetNode[0].files[0], 
					'block': blockNode,
					'convert': 'false',
					'success': function(res){
						/**
						 * Callback function for upload file success
						 * Update the url in supplementary source
						 * Remove the progress bar in block
						 */
						var fileDetailsObj = res.fileDetailsObj;
						var fileName = fileDetailsObj.name+fileDetailsObj.extn;
						var filePath = '/resources/' + fileDetailsObj.dstKey;
						$(targetNode).closest('.row.file-case').find('.uploadedFile').text(fileName);
						$(targetNode).closest('.row.file-case').attr('file_name', fileName);
						$(targetNode).closest('.row.file-case').attr('file_alt_name', fileName);
						$(targetNode).closest('.row.file-case').attr('file_extension', fileDetailsObj.extn);
						$(targetNode).closest('.row.file-case').attr('file_path', '/'+ fileDetailsObj.dstKey);
						$(targetNode).closest('.row.file-case').attr('file_href', '/'+ fileDetailsObj.dstKey);
						$('.row.replace-in-progress').find('.temp-list').remove();
						$(targetNode).closest('.row').find('.uploadedFile').removeAttr('data-error');
						blockNode.progress('Uploaded', true);
					},
					'error': function(err){
						//Error callback function remove the progress in blocknode
						blockNode.progress('', true);
						//Show the error notification
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Uploading file failed.',
							timeout: 8000,
							icon : 'icon-info'
						});
					}
				}
				eventHandler.menu.upload.uploadFile(param, targetNode);
			},
			resubmitManuscriptConfirm: function(param, targetNode){
				/*
				* resubmit a manuscript : check for validation
				* 1. Zip uploaded files
				* 2. create back up of current version and save the path in the xml
				* 3. get current file list and send zip file to resubmit manuscript api
				*/
				var popper = $(targetNode).closest("[data-component]");
				var validInput = true;
				var validManuscript = 0;
				var mustHaveFiles = [];
				popper.find('.file-table .file-case[data-type]:first .file-name select option').each(function(){
					if ($(this)[0].hasAttribute('data-mandatory')){
						mustHaveFiles.push($(this).text());
					}
				});
				var zip = new JSZip();
				var fileList = '<files>';
				popper.find('.file-table .file-case[data-type]').each(function(){
					$(this).find('.uploadedFile').removeAttr('data-error');
					var currFile = $(this).find('.file-designation input')[0];
					if (currFile.files.length == 0){
						$(this).find('.uploadedFile').attr('data-error', 'Input required');
						validInput = false;
					}
					if ($(this).find('select').val() == 'manuscript'){
						validManuscript++;
					}
					var fileType = $(this).find('select option:selected').text()
					var fileValue = $(this).find('select').val()
					var mindex = mustHaveFiles.indexOf(fileType);
					if (mindex > -1) {
						mustHaveFiles.splice(fileType, 1);
					}
					if (currFile.files.length > 0){
						var currFileName = currFile.files[0].name;
						currFileName = currFileName.replace(/\%20/g, ' ');
						fileList += '<file file_name="' + currFileName + '" file_size="' + currFile.files[0].size + '" file_designation="' + fileValue + '"/>'
						zip.file(currFileName, currFile.files[0]);
					}
				});
				fileList += '</files>';
				if (!validInput){
					return false;
				}
				if (validManuscript > 1){
					kriya.notification({
						title: 'Warning',
						type: 'error',
						content: 'Only manuscript can be uploaded.',
						icon: 'icon-warning2'
					});
					return false;
				}
				if (mustHaveFiles.length > 0){
					kriya.notification({
						title: 'Files missing',
						type: 'error',
						content: 'Please upload the following file type : ' + mustHaveFiles.join(', '),
						icon: 'icon-warning2'
					});
					return false;
				}
				zip.file('kriya_job_manifest.xml', fileList);
				$('.la-container').fadeIn();
				targetNode.addClass('disabled')
				var versionNumber = 'R' + ($('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(:contains(Original))').length + 1);
				var param = {
					'project'   : kriya.config.content.project,
					'doi'       : kriya.config.content.doi,
					'customer'  : kriya.config.content.customer,
					'fileName'  : 'replace_manuscript'
				};
				kriya.general.sendAPIRequest('createsnapshots',param , function(res){
					if(res && typeof(res) == "object" && res.status.versionPath){
						var versionName = $('.current-version').attr('data-version');
						var versionPath = res.status.versionPath;
						/*var parameter = {
							'customer': kriya.config.content.customer,
							'projectName' : kriya.config.content.project,
							'doi'         : kriya.config.content.doi,
							'xmlFrag'     : '<article-version vocab-identifier="' + versionPath + '" article-version-type="' + versionName + '">Version</article-version>',
							'xpath'       : '//article//article-meta//article-version[@article-version-type="' + versionName + '"]',
							'insertInto'  : '//article//article-meta'
						};*/
						var parameters = {
							'customer':kriya.config.content.customer,
							'project':kriya.config.content.project,
							'doi':kriya.config.content.doi,
							'data': {
								'process': 'append',
								'append': '//article//article-meta',
								'content': '<article-version vocab-identifier="' + versionPath + '" article-version-type="' + versionName + '">Version</article-version>'
							}
						};
		
						kriya.general.sendAPIRequest('updatedata', parameters, function(res){
						//kriya.general.sendAPIRequest('updatearticle' ,parameter , function(res){
							var parameters = {
								'customer' : kriya.config.content.customer,
								'project'  : kriya.config.content.project,
								'doi'  : kriya.config.content.doi,
								'process' : 'get',
								'raw': 'true'
							};
							kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
								if (res && res.status && res.status == 500){
								}else{
									var filesList = $(res).html();
									var parameters = new FormData();
									parameters.append('customer', kriya.config.content.customer);
									parameters.append('project', kriya.config.content.project);
									parameters.append('doi', kriya.config.content.doi);
									parameters.append('version', versionNumber);
									parameters.append('filesList', filesList);
									zip.generateAsync({type:"blob"})
										.then(function(content) {
											var fileName = new Date().getTime() + '.zip';
											parameters.append('manuscript', content, fileName);
											$.ajax({
												type: 'POST',
												url: '/api/resubmitmanuscript',
												data: parameters,
												contentType: false,
												processData: false,
												success: function (response) {
													var userEmail = $('.userProfile .username').attr('data-user-email');
													var userName = $('.userProfile .username').text();
													eventHandler.components.general.getEmailTemplateToEdit({'currStage' : 'authorreview', 'reviewer': 'resubmit-manuscript', 'userName' : userName, 'userMailID' : userEmail}, '');
												},
												error: function () {
													$('.la-container').css('display', 'none');
													kriya.notification({title: 'ERROR', type: 'error', content: 'Signoff failed. Please contact Publisher', icon: 'icon-warning2'});
												}
											})
										})
										.catch(function(err){
											$('.la-container').css('display', 'none');
											kriya.notification({title: 'ERROR', type: 'error', content: 'Signoff failed, Please contact Publisher', icon: 'icon-warning2'});
										})
								}
							});
						}, function(){
							// failed to create version in xml
							$('.la-container').css('display', 'none');
							kriya.notification({title: 'ERROR', type: 'error', content: 'Signoff failed, Please contact Publisher', icon: 'icon-warning2'});
						});	
					}else{
						//snapshot creation failed
						$('.la-container').css('display', 'none');
						kriya.notification({title: 'ERROR', type: 'error', content: 'Signoff failed, Please contact Publisher', icon: 'icon-warning2'});
					}
				}, function(err){
					//error
					$('.la-container').css('display', 'none');
					kriya.notification({title: 'ERROR', type: 'error', content: 'Signoff failed, Please contact Publisher', icon: 'icon-warning2'});
				});
			},
			continueResubmitManuscript: function(param, targetNode){
					var popper = $(targetNode).closest("[data-component]");
					var versionNumber = 'R' + ($('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(:contains(Original))').length + 1);
					var userEmail = $('.userProfile .username').attr('data-user-email');
					var userName  = $('.userProfile .username').text();
					var signPparameters = {
						'customer'  : kriya.config.content.customer,
						'project' : kriya.config.content.project,
						'doi' : kriya.config.content.doi,
						'currStage' : 'resubmit-manuscript',
						'version' : versionNumber,
						'userMailID': userEmail,
						'userName' : userName,
					};
					signPparameters['emailBody'] = popper.find('.email-body').html();
					if (popper.find('.email-cc').text() != "") {
						signPparameters['cc'] = popper.find('.email-cc').text();
					}
					if (popper.find('.email-bcc').text() != "") {
						signPparameters['bcc'] = popper.find('.email-bcc').text();
					}
					if (popper.find('.email-to').text() != "") {
						signPparameters['to'] = popper.find('.email-to').text();
						signPparameters['editedTo'] = popper.find('.email-to').text();
					}
					if (popper.find('.email-replyTo').text() != "") {
						signPparameters['replyTo'] = popper.find('.email-replyTo').text();
					}
					if (popper.find('.email-subject').text() != "") {
						signPparameters['subject'] = popper.find('.email-subject').text();
					}

					if(popper.find('.attachementContainer .card').length > 0){
						signPparameters['attachments'] = [];
						popper.find('.attachementContainer .card').each(function(){
							signPparameters['attachments'].push({
								filename: $(this).attr('data-file-name'),
								path: $(this).attr('data-file-path'),
								contentType: $(this).attr('data-file-type')
							});
						});
					}

					$('.la-container').fadeIn();
					$.ajax({
						type : "POST",
						url  : "/api/signoff",
						data : signPparameters,
						success: function (res) {
							var parameters = {
								'customer' :kriya.config.content.customer,
								'project':kriya.config.content.project,
								'doi':kriya.config.content.doi,
								'role'     :kriya.config.content.role,
								'currStage': 'resubmit-manuscript-author',
							};
							$.ajax({
								type : "POST",
								url  : "/api/signoff",
								data : parameters,
								success: function (res) {
									$('.la-container').fadeOut();
									if(res && res.status && res.status.message == "failed"){
										kriya.notification({
											title: 'ERROR',
											type: 'error',
											content: 'Sending email was failed.',
											icon: 'icon-warning2'
										});
									}else{
										//$('[data-component="fileDesignator_edit"] [data-wrapper]').html('<p style="text-align: center;">You have <strong>signed off</strong> this manuscript successfully. <em>No further changes</em> can be made to the manuscript.</p>');
										$('body .messageDiv .message').html('You have successfully signed off the article to the next stage.<br> Please close the browser to exit the system.');
										$('body .messageDiv').removeClass('hidden');
										$('body .messageDiv .feedBack').removeClass('hidden');
										kriya.config.pageStatus = "signed-off";
									}
								},
								error: function(err){
									$('.la-container').fadeOut();
									kriya.notification({
										title: 'ERROR',
										type: 'error',
										content: 'Sending email was failed.',
										icon: 'icon-warning2'
									});
								}
							});
						},
						error: function (err) {
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'ERROR',
								type: 'error',
								content: 'Sending email was failed.',
								icon: 'icon-warning2'
							});
						}
					});
			},
			replaceManuscriptConfirm: function(param, targetNode){
				var popper = $(targetNode).closest("[data-component]");
				/*if(!popper.find('.uploadedFile[file_designation="manuscript"]').attr('file_path')){
					popper.find('.uploadedFile[file_designation="manuscript"]').attr('data-error', 'Input required');
					return false;
				}else{
					popper.find('.uploadedFile[file_designation="manuscript"]').removeAttr('data-error');
				}*/
				var validInput = true;
				var validManuscript = 0;
				var mustHaveFiles = [];
				popper.find('.file-table .file-case[data-type]:first .file-name select option').each(function(){
					if ($(this)[0].hasAttribute('data-mandatory')){
						mustHaveFiles.push($(this).text());
					}
				})
				popper.find('.file-table .file-case[data-type]').each(function(){
					$(this).find('.uploadedFile').removeAttr('data-error');
					if (!$(this).attr('file_path')){
						$(this).find('.uploadedFile').attr('data-error', 'Input required');
						validInput = false;
					}
					if ($(this).find('select').val() == 'manuscript'){
						validManuscript++;
					}
					var fileType = $(this).find('select option:selected').text()
					var mindex = mustHaveFiles.indexOf(fileType);
					if (mindex > -1) {
						mustHaveFiles.splice(fileType, 1);
					}
				});
				if (!validInput){
					return false;
				}
				if (validManuscript > 1){
					kriya.notification({
						title: 'Warning',
						type: 'error',
						content: 'Only manuscript can be uploaded.',
						icon: 'icon-warning2'
					});
					return false;
				}
				if (mustHaveFiles.length > 0){
					kriya.notification({
						title: 'Files missing',
						type: 'error',
						content: 'Please upload the following file type : ' + mustHaveFiles.join(', '),
						icon: 'icon-warning2'
					});
					return false;
				}
				var versionNumber = 'R' + ($('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(:contains(Original))').length + 1);
				popper.find('.file-table .file-case[data-type]').each(function(){
					var clone = $(this).clone();
					clone.html('');
					clone.removeAttr('data-type');
					clone.attr('data-version', versionNumber);
					popper.find('.files_list').append(clone);
					$(this).find('.temp-list .file-case').each(function(){
						$(this).attr('data-version', versionNumber);
						popper.find('.files_list').append($(this));
					});
				});
				//return false;
				$('.la-container').fadeIn();
				var data = $('<files>');
				popper.find('.files_list .file-case').each(function(){
					var clone = $(this).clone();
					clone.removeAttr('class');
					var file = $('<file>');
					for (var i = 0; i < clone[0].attributes.length; i++) {
						var attrib = clone[0].attributes[i];
						file.attr(attrib.name, attrib.value);
					}
					data.append(file);
				});

				var param = {
					'project'   : kriya.config.content.project,
					'doi'       : kriya.config.content.doi,
					'customer'  : kriya.config.content.customer,
					'fileName'  : 'replace_manuscript'
				};
				kriya.general.sendAPIRequest('createsnapshots',param , function(res){
						if(res && typeof(res) == "object" && res.status.versionPath){
							var versionName = $('.current-version').attr('data-version');
							var versionPath = res.status.versionPath;
							var parameter = {
								'customer': kriya.config.content.customer,
								'projectName' : kriya.config.content.project,
								'doi'         : kriya.config.content.doi,
								'xmlFrag'     : '<article-version vocab-identifier="' + versionPath + '" article-version-type="' + versionName + '">Version</article-version>',
								'xpath'       : '//article//article-meta//article-version[@article-version-type="' + versionName + '"]',
								'insertInto'  : '//article//article-meta'
							};
							kriya.general.sendAPIRequest('updatearticle' ,parameter , function(res){
									var parameters = {
										'customer' : kriya.config.content.customer,
										'project'  : kriya.config.content.project,
										'doi'  : kriya.config.content.doi,
										'data' : data[0].outerHTML,
										'file' : 'file_list'
									};
									kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
										kriya.popUp.closePopUps(popper);
										$(data).find('file:not([data-version])').remove();
										var params = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'fileData': data[0].outerHTML, 'stage':kriya.config.content.stage};
										/** Sending the file list to structure content **/
										$('.pdfStatus').remove();
										//$('body').append('<span class="pdfStatus btn btn-primary">Structuring content...</span>');
										$.ajax({
											xhr: function(){
												var xhr = new window.XMLHttpRequest();
												//Download progress
												xhr.addEventListener("progress", function(evt){
													var lines = evt.currentTarget.response.split("\n");
													if(lines.length){
														var progress = lines[lines.length-1];
														if (progress) {
															//$('.pdfStatus').html(progress);
															if ($('<r>' + progress + '</r>').find('response').length == 0){
																$('.pre-load-container .pre-loader-message').html(progress);
															}
														}
													}else{
														console.log(evt.currentTarget.response);
													}
												}, false);
												return xhr;
											},
											type: "POST",
											url: "/api/structurecontent",
											data: params,
											success: function (data) {
													$('.pre-load-container').fadeOut();
													$('.pre-load-container .pre-loader-message').html('please wait ...');
													$('[data-type="popUp"]').addClass('hidden');
													$('.la-container').fadeOut();
													data = data.replace(/&apos;/g, "\'");
													var response = $('<r>'+ data + '</r>');
													if ($(response).find('response').length > 0 && $(response).find('response content').length > 0){
														$('#contentDivNode').html($(response).find('response content').html());
														$('.pdfStatus').remove();
														//unwrap the givenname and surname in reference author after structure content
														$('#contentDivNode .RefGivenname, #contentDivNode .RefSurname').contents().unwrap();
														$('#contentDivNode .front, #contentDivNode .body, #contentDivNode .back').attr('data-non-editable', 'true')
														kriyaEditor.init.save();
														//var newVersion = 'R' + (parseInt(versionName.replace(/[a-z]+/gi, '')) + 1);
														//var currentVersion = $('.current-version');
														//currentVersion.find('a').attr('target', '_blank');
														//currentVersion.find('a').attr('href', "/version?pageName=peer_review&customer=" + kriya.config.content.customer + "&project=" + kriya.config.content.project + "&doi=" + kriya.config.content.doi + "&versions=" + versionPath);
														//currentVersion.removeClass('current-version');
														//$('<span class="kriyaMenuBtn current-version" data-version="' + newVersion + '"><a>' + newVersion + '</a></span>').insertAfter(currentVersion);
													}
											},
											error: function(err){
												console.log(err);
												$('.pdfStatus').remove();
												$('.pre-load-container').fadeOut();
											}
										});
									});
							}, function(){
								// failed to create version in xml
							});	
						}else{
							//snapshot creation failed
						}
				}, function(err){
						//error
				});
			},
			replaceDoc: function(customer, jobID, block, doi, status){
				/** Option to replace manuscript, which will send the ms-file to jon-manager for processing **/
				var loglen = status.log.length;    
				var result = status.log[loglen-1];
				var resultFile = $(result);
				var popper = $('.row.replace-in-progress').closest("[data-component]");
				$('.row.replace-in-progress').find('.temp-list').remove();
				$('.row.replace-in-progress').append('<div class="temp-list hidden"/>');
				resultFile.each(function(i,v){
					var file = $('<div class="file-case"/>')
					for (var ci = 0; ci < $(this)[0].attributes.length; ci++) {
						var attrib = $(this)[0].attributes[ci];
						if (attrib.name != "data-href"){
							file.attr(attrib.name, attrib.value);
							if (i == 0){
								$('.row.replace-in-progress').attr(attrib.name, attrib.value);
							}
						}else{
							file.attr(attrib.name, '/resources' + attrib.value);
							if (i == 0){
								$('.row.replace-in-progress').attr(attrib.name, '/resources' + attrib.value);
							}
						}
					}
					//file.attr('file_designation', $('.row.replace-in-progress').find('select').val());
					//popper.find('.files_list').append(file);
					if (i > 0){
						$('.row.replace-in-progress').find('.temp-list').append(file);
					}
					if (i == 0){
						$('.row.replace-in-progress').find('.uploadedFile').text($(this).attr('file_name'));
						//$('.row.replace-in-progress').find('.uploadedFile').attr('file_path', $(this).attr('file_path'));
					}
				});
				$('.row.replace-in-progress').find('.uploadedFile').removeAttr('data-error');
				$('.row.replace-in-progress').closest('[data-type="popUp"]').progress('Uploaded', true);
			},
			rejectReviewer: function(param, targetNode){
				var userEmail = $('.userProfile .username').attr('data-user-email');
				var userName  = $('.userProfile .username').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();

				$('.la-container').fadeIn();
				$.ajax({
					type: "GET",
					url: '/api/getdata?customer=' + kriya.config.content.customer + '&project=' + kriya.config.content.project + '&doi=' + kriya.config.content.doi + '&xpath=' + '//contrib-group/contrib[@data-version="' + versionNumber + '"][@contrib-type="reviewer"][contains(./email, "' + userEmail + '")]',
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (res) {
							res = res.documentElement;
							var resNode = $(res);
							if(resNode.attr('data-reviewer-status') != "terminated"){
								var parameters = {
									'customer'  : kriya.config.content.customer,
									'project' : kriya.config.content.project,
									'doi' : kriya.config.content.doi,
									'role'      : 'reviewer',
									'currStage' : 'reviewer-reject',
									'stagename' : 'Reviewer Check',
									'userMailID': userEmail,
									'userName' : userName,
									'reviewerStatus' : 'rejected',
									'reviewerMessage' : 'Rejected to Review',
									'version' : versionNumber
								};
								$.ajax({
									type : "POST",
									url  : "/api/signoff",
									data : parameters,
									success: function (res) {
										if(res && res.status && res.status.message == "failed"){
											//Failed
										}else{
											var userEmail = $('.userProfile .username').attr('data-user-email');
											var reviewerNode = $('#contentDivNode .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')');
											if(reviewerNode.length > 0){
												reviewerNode.attr('data-reviewer-status', 'rejected');
												reviewerNode.attr('data-reviewer-message', 'Rejected to Review');
											}

											$.ajax({
												type : "GET",
												url  : "/api/getuserdata?format=xml&userEmail=" + userEmail + '&roleType=production&accessLevel=reviewer',
												success: function (res) {
														if(res){
															res = res.replace(/<(\d+)>/g, '<user>');
															res = res.replace(/<\/(\d+)>/g, '</user>');
															var userDoc = $('<users>' + res + '</users>');
															//var userDoc = $(res);
															var parameters = {
																'customer'    : kriya.config.content.customer,
																'email'           : userDoc.find('email').text(),
																'modifiedData'    : 'true',
																'doi'       : kriya.config.content.doi,
																'reviewStatus'    : 'declined',
															};
															kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
																//Success
															}, function(err){
																console.log(err);
															});

														}
												},
												error: function(err){
													console.log(err);
												}
											});

											var backUpNode = $('.jrnlReviewersGroup .jrnlReviewerGroup[data-version="' + versionNumber + '"][data-reviewer-status="back-up"]');
											if(backUpNode.length == 0){
												$('.la-container').fadeOut();
												$('#welcomeContainer').html('<p style="top: 15%;position: absolute;width: 100%;text-align: center;">You have successfully rejected this article.</p>');
											}else{
												var userEmail = backUpNode.first().find('.jrnlEmail').text();
												var surName   = backUpNode.first().find('.jrnlSurName').text();
												var givenName = backUpNode.first().find('.jrnlGivenName').text();
												$.ajax({
													type : "GET",
													url  : "/api/getuserdata?format=xml&userEmail=" + userEmail,
													success: function (res) {
														res = res.replace(/<(\d+)>/g, '<user>');
														res = res.replace(/<\/(\d+)>/g, '</user>');
														var assigned = $(res).find('roles role[customer-name="' + kriya.config.content.customer + '"]').attr('article-assigned');
														var parameters = {
															'userEmail' : userEmail,
															'surName' : surName,
															'givenName' : givenName,
															'assigned' : assigned
														};
														eventHandler.components.general.assignToReviewer(parameters,'', function(){
															$('.la-container').fadeOut();
															$('#welcomeContainer').html('<p style="top: 15%;position: absolute;width: 100%;text-align: center;">You have successfully rejected this article.</p>');
														},function(err){
															console.log(err);
														});
													},
													error: function(err){

													}
												});
											}
										}
									},
									error: function (err) {
										console.log(err);
										//Error
									}
								});
							}else{
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed',
									type: 'error',
									content: 'You can\'t reject this article. Review was terminated.',
									icon: 'icon-warning2'
								});
							}
					},
					error: function(err){
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Rejecting this article was failed.',
								icon: 'icon-warning2'
							});
					}
				});
			},
			acceptReviewer: function(param, targetNode){
				var userEmail = $('.username').attr('data-user-email');
				var userName  = $('.userProfile .username').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();
				var reviewerNode = $('.jrnlReviewersGroup .jrnlReviewerGroup[data-version="' + versionNumber + '"] .jrnlEmail:contains("' + userEmail + '")').closest('.jrnlReviewerGroup');
				if(reviewerNode.length > 0){
					$('.la-container').fadeIn();
					$.ajax({
						type: "GET",
						url: '/api/getdata?customer=' + kriya.config.content.customer + '&project=' + kriya.config.content.project + '&doi=' + kriya.config.content.doi + '&xpath=' + '//contrib-group/contrib[@data-version="' + versionNumber + '"][@contrib-type="reviewer"][contains(./email, "' + userEmail + '")]',
						contentType: "application/xml; charset=utf-8",
						dataType: "xml",
						success: function (res) {
							res = res.documentElement;
							var resNode = $(res);
							if(resNode.attr('data-reviewer-status') != "terminated"){
									var parameters = {
										'customer' :kriya.config.content.customer,
										'project':kriya.config.content.project,
										'doi':kriya.config.content.doi,
										'role'     :'reviewer',
										'currStage': 'reviewer-accept',
										'to' : userEmail,
										'userMailID': userEmail,
										'userName'  : userName,
										'recipientName' : userName,
										'stagename' : 'Reviewer Check',
										'version' : versionNumber,
										'reviewerStatus' : 'accepted',
										'skipAssignTo' : 'true'
									};
									$.ajax({
										type : "POST",
										url  : "/api/signoff",
										data : parameters,
										success: function (res) {
											if(res && res.status && res.status.message == "failed"){
												//Failed
											}else{
														
														$.ajax({
															type : "GET",
															url  : "/api/getuserdata?format=xml&userEmail=" + userEmail + '&roleType=production&accessLevel=reviewer',
															success: function (res) {
																	if(res){
																		res = res.replace(/<(\d+)>/g, '<user>');
																		res = res.replace(/<\/(\d+)>/g, '</user>');
																		var userDoc = $(res);
																		var inprogress = userDoc.find('role[customer-name="' + kriya.config.content.customer + '"]').attr('article-inprogress');
																		inprogress = (inprogress && inprogress > -1)?(parseInt(inprogress) + 1):1;
																		var parameters = {
																			'customer'    : kriya.config.content.customer,
																			'email'           : userDoc.find('email').text(),
																			'modifiedData'    : 'true',
																			'doi'       : kriya.config.content.doi,
																			'reviewStatus'    : 'in-progress',
																		};
																		kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
																			$('.la-container').fadeOut();
																			$('#welcomeContainer').html('<p style="top: 15%;position: absolute;width: 100%;text-align: center;">An email has been sent out with the link to access the article. Thank you for accepting the paper to review!</p>');
																		}, function(err){
																				console.log(err);
																		});

																	}
															},
															error: function(err){
																console.log(err);
															}
														});

											}
										},
										error: function (err) {
											console.log(err);
											//Error
										}
									});
							}else{
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed',
									type: 'error',
									content: 'You can\'t accept this article. Review was terminated.',
									icon: 'icon-warning2'
								});
							}
						},
						error: function(err){
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Accepting this article was failed.',
								icon: 'icon-warning2'
							});
						}
					});
				}
			},
			getSendToEditor: function(param, targetNode){
				var status = eventHandler.components.general.validateElements();
				if(status == false){
					return false;
				}
				$('.la-container').fadeIn();
				$.ajax({
					type : "GET",
					url  : "/api/getuserdata?format=xml&customer=" + kriya.config.content.customer + '&roleType=production&accessLevel=associateeditor',
					success: function (res) {
						$('.la-container').fadeOut();
						$('[data-component="associateeditor_edit"] table tbody').html('');
						if(res){
							var versionNumber      = $('.versionMenu .current-version').text().trim();
							var currentEditorEmail = $('.jrnlEditorsGroup .jrnlEditorGroup .jrnlEmail:first').text();
							res = res.replace(/<(\d+)>/g, '<user>');
							res = res.replace(/<\/(\d+)>/g, '</user>');
							var userObj = $('<users>' + res + '</users>');

							var userEmail = $('.userProfile .username').attr('data-user-email');
							var userName  = $('.userProfile .username').text();

							userObj.find('user:contains(' + currentEditorEmail + ')').each(function(){
								var reviewerRow = $('<tr data-file-name="' + $(this).find('file').text() + '"><td><span class="reviewer-title"></span> <span class="reviewer-surname"></span> <span class="reviewer-given-name"></span><br /> <span class="reviewer-email" /><br /><span class="reviewer-aff" /></span> <br /> <span class="btn btn-small green lighten-1" data-message="{\'click\':{\'funcToCall\': \'editUser\', \'param\' : {\'component\' : \'add_editor_edit\'}, \'channel\':\'components\',\'topic\':\'general\'}}">Edit</span><span class="btn btn-small red lighten-1" style="margin-left: 0.5rem;" data-message="{\'click\':{\'funcToCall\': \'deleteUser\', \'channel\':\'components\',\'topic\':\'general\'}}">Delete</span></td><td><span class="btn btn-small green lighten-1" data-currStage="assignassociateeditor" data-work-name="associate-editorcheck" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\', \'param\': {\'userMailID\': \' ' + userEmail + ' \', \'userName\': \' ' + userName + ' \'}, \'channel\':\'components\',\'topic\':\'general\'}}">Send</span></td></tr>');
								
								$('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(.current-version)').each(function(){
									var verText = $(this).text();
									var versionCol = $('<td class="reviewer-version-history" data-version="' + verText + '"></td>');
									$(reviewerRow).find('td:last').before(versionCol);
								});
								
								reviewerRow.find('.reviewer-title').html($(this).find('name title').text());								
								reviewerRow.find('.reviewer-surname').html($(this).find('name first').text());									
								reviewerRow.find('.reviewer-given-name').html($(this).find('name last').text());
								reviewerRow.find('.reviewer-email').html($(this).find('email').text());

								var institution = $(this).find('affiliation institution').text();
								var city = $(this).find('affiliation city').text();
								var country = $(this).find('affiliation country').text();

								var reviewerAff = ('<span class="reviewer-institution">' + institution + '</span>') + ((institution && city)?', ':'') + ('<span class="reviewer-city">' + city + '</span>') + ((country && city)?', ':'') + ('<span class="reviewer-country">' + country + '</span>') + ((country)?'.':'');
								reviewerRow.find('.reviewer-aff').html(reviewerAff);
								
								$('[data-component="associateeditor_edit"] table tbody').append(reviewerRow);
							});

							$('[data-component="associateeditor_edit"] table thead [data-version]').remove();
							$('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(.current-version)').each(function(){
								var verText = $(this).text();
								var versionCol = $('<th class="reviewer-version-history" data-version="' + verText + '" style="width: 50px; text-align: center;">' + verText + ' <br> decision</th>');
								$('[data-component="associateeditor_edit"] table thead .last-col-assign-head').before(versionCol);
							});

							
							var versionCountObj = [];
							$('#contentDivNode .jrnlEditorGroup').each(function(){
								var editorMail = $(this).find('.jrnlEmail').text();
								var editorRow = $('[data-component="associateeditor_edit"] tr:contains(' + editorMail + ')');
								
								var rowIndex    = editorRow.index();
								if(rowIndex >= 0 && !versionCountObj[rowIndex]){
									versionCountObj[rowIndex] = {'email' : editorMail, 'vercnt' : 0};
								}
								
								var editorMsg = $(this).attr('data-reviewer-message');
								if ($(this).attr('data-version') != versionNumber){
									if(versionCountObj[rowIndex]){
										versionCountObj[rowIndex]['vercnt']++;
									}
									var vn = $(this).attr('data-version');
									if($(this).attr('data-reviewer-status') == "terminated"){
										editorRow.find('td[data-version="' + vn + '"]').append('<i class="material-icons" style="line-height: inherit;color: red;">close</i>');
									}else if(editorMsg){
										editorRow.find('td[data-version="' + vn + '"]').append('<span class="reviewer-message">' + editorMsg + '</span>');
									}else{
										editorRow.find('td[data-version="' + vn + '"]').append('<span class="reviewer-message">Incomplete</span>');
									}
								}

							});
							versionCountObj = versionCountObj.sort(function(a, b){
								return a.vercnt-b.vercnt;
							});
							for(var a=0;a<versionCountObj.length;a++){
								if(versionCountObj[a] && versionCountObj[a].email){
									var reviewerRow = $('[data-component="associateeditor_edit"] tr:contains(' + versionCountObj[a].email + ')');
									$('[data-component="associateeditor_edit"] tbody').prepend(reviewerRow);
								}
							}

						}

						kriya.popUp.openPopUps($('[data-component="associateeditor_edit"]'));
					},
					error: function (res) {
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Failed to get the associate editors.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			getAssignToReviewer: function(param, targetNode){
				var status = eventHandler.components.general.validateElements();
				if(status == false){
					return false;
				}
				$('.la-container').fadeIn();
				$.ajax({
					type : "GET",
					url  : "/api/getuserdata?format=xml&customer=" + kriya.config.content.customer + '&roleType=production&accessLevel=reviewer',
					success: function (res) {
						$('.la-container').fadeOut();
						$('[data-component="reviewer_edit"] table tbody').html('');
						if(res){
							res = res.replace(/<(\d+)>/g, '<user>');
							res = res.replace(/<\/(\d+)>/g, '</user>');
							var userObj = $('<users>' + res + '</users>');
							var versions = $('.kriyaMenuControl.versionMenu .kriyaMenuBtn').length - 1;
							var userEmail = $('.userProfile .username').attr('data-user-email');
							var userName  = $('.userProfile .username').text();
							userObj.children().each(function(i){
								var reviewerRow = $('<tr data-file-name="' + $(this).find('file').text() + '"><td><span class="reviewer-title"></span> <span class="reviewer-surname"></span> <span class="reviewer-given-name"></span><br /> <span class="reviewer-email" /><br /><span class="reviewer-aff" /></span> <br /> <span class="btn btn-small green lighten-1" data-message="{\'click\':{\'funcToCall\': \'editUser\', \'param\' : {\'component\' : \'add_reviewer_edit\'}, \'channel\':\'components\',\'topic\':\'general\'}}">Edit</span><span class="btn btn-small red lighten-1" style="margin-left: 0.5rem;" data-message="{\'click\':{\'funcToCall\': \'deleteUser\', \'channel\':\'components\',\'topic\':\'general\'}}">Delete</span> </td><td class="reviewer-expertise" style="text-align:center;"></td> <td style="text-align:center;"><span class="reviewer-article-assigned" data-message="{\'click\':{\'funcToCall\': \'displayReviewArticle\', \'channel\':\'components\',\'topic\':\'PopUp\'}}"/></td><td style="text-align:center;"><span class="reviewer-article-declined" data-message="{\'click\':{\'funcToCall\': \'displayReviewArticle\', \'channel\':\'components\',\'topic\':\'PopUp\'}}"/></td><td style="text-align:center;"><span class="reviewer-article-inprogress" data-message="{\'click\':{\'funcToCall\': \'displayReviewArticle\', \'channel\':\'components\',\'topic\':\'PopUp\'}}"/></td><td style="text-align:center;"><span class="reviewer-article-completed" data-message="{\'click\':{\'funcToCall\': \'displayReviewArticle\', \'channel\':\'components\',\'topic\':\'PopUp\'}}"/></td><td><span class="btn btn-small green lighten-1 assignReviewerBtn" data-activates="assign-dropdown' + i + '">Assign</span><ul id="assign-dropdown' + i + '" class="dropdown-content dropdown-list"><li class="dropdown-assignReviewer" data-work-name="assignToReviewer" data-currStage="associateediter-assign-reviewer" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\', \'param\': {\'userMailID\': \' ' + userEmail + ' \', \'userName\': \' ' + userName + ' \'}, \'channel\':\'components\',\'topic\':\'general\'}}">Assign Now</li><li class="dropdown-addBackup" data-message="{\'click\':{\'funcToCall\': \'addBackUpReviewer\', \'channel\':\'components\',\'topic\':\'general\'}}">Assign as back-up</li></ul></td></tr>');
								$('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(.current-version)').each(function(){
									var verText = $(this).text();
									var versionCol = $('<td class="reviewer-version-history" data-version="' + verText + '"></td>');
									$(reviewerRow).find('.assignReviewerBtn').closest('td').before(versionCol);
								});
								
								reviewerRow.find('.reviewer-surname').html($(this).find('name first').text());
								reviewerRow.find('.reviewer-title').html($(this).find('name title').text());
								reviewerRow.find('.reviewer-given-name').html($(this).find('name last').text());
								reviewerRow.find('.reviewer-email').html($(this).find('email').text());
								
								$(this).find('expertise key').each(function(){
									  reviewerRow.find('.reviewer-expertise').append('<span class="expertise-key">' + $(this).html() + '</span>');
								});

								var institution = $(this).find('affiliation institution').text();
								var city = $(this).find('affiliation city').text();
								var country = $(this).find('affiliation country').text();

								var reviewerAff = ('<span class="reviewer-institution">' + institution + '</span>') + ((institution && city)?', ':'') + ('<span class="reviewer-city">' + city + '</span>') + ((country && city)?', ':'') + ('<span class="reviewer-country">' + country + '</span>') + ((country)?'.':'');
								reviewerRow.find('.reviewer-aff').html(reviewerAff);
								
								/*var assigned = $(this).find('role[customer-name="' + kriya.config.content.customer + '"]').attr('article-assigned');
								assigned = (assigned)?assigned:0;
								var completed = $(this).find('role[customer-name="' + kriya.config.content.customer + '"]').attr('article-completed');
								completed = (completed)?completed:0;
								var inprogress = $(this).find('role[customer-name="' + kriya.config.content.customer + '"]').attr('article-inprogress');
								inprogress = (inprogress)?inprogress:0;*/
								var assigned   = $(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article').length;
								var completed  = $(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("completed"))').length;
								var inprogress = $(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("in-progress"))').length;
								var declined = $(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("declined"))').length;

								var assignedArticles = [];
								$(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article').each(function(){
										assignedArticles.push($(this).find('article-id').text());
								});
								assignedArticles = assignedArticles.join();

								var completedArticles = [];
								$(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("completed"))').each(function(){
									completedArticles.push($(this).find('article-id').text());
								});
								completedArticles = completedArticles.join();

								var inprogressArticles = [];
								$(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("in-progress"))').each(function(){
									inprogressArticles.push($(this).find('article-id').text());
								});
								inprogressArticles = inprogressArticles.join();

								var declinedArticles = [];
								$(this).find('reviewer-status:has(customer-name:contains("' + kriya.config.content.customer + '")) article:has(review-status:contains("declined"))').each(function(){
									declinedArticles.push($(this).find('article-id').text());
								});
								declinedArticles = declinedArticles.join();

								reviewerRow.find('.reviewer-article-assigned').html(assigned);
								reviewerRow.find('.reviewer-article-assigned').attr('data-article-id', assignedArticles);

								reviewerRow.find('.reviewer-article-completed').html(completed);
								reviewerRow.find('.reviewer-article-completed').attr('data-article-id', completedArticles);

								reviewerRow.find('.reviewer-article-inprogress').html(inprogress);
								reviewerRow.find('.reviewer-article-inprogress').attr('data-article-id', inprogressArticles);

								reviewerRow.find('.reviewer-article-declined').html(declined);
								reviewerRow.find('.reviewer-article-declined').attr('data-article-id', declinedArticles);
								
								$('[data-component="reviewer_edit"] table tbody').append(reviewerRow);
								$('[data-activates="assign-dropdown' + i + '"]').dropdown();
							});
							$('[data-component="reviewer_edit"] table thead [data-version]').remove();
							
							$('.kriyaMenuControl.versionMenu .kriyaMenuBtn:not(.current-version)').each(function(){
								var verText = $(this).text();
								var versionCol = $('<th class="reviewer-version-history" data-version="' + verText + '" style="width: 50px; text-align: center;">' + verText + ' <br> decision</th>');
								$('[data-component="reviewer_edit"] table thead .last-col-assign-head').before(versionCol);
							});

							/*for (var v = 0; v < versions; v++){
								var versionCol = $('<th class="reviewer-version-history" data-version="R' + (v+1) + '" style="width: 50px; text-align: center;">R' + (v+1) + '</th>');
								$('[data-component="reviewer_edit"] table thead .last-col-assign-head').before(versionCol);
							}*/
						}

						$.ajax({
							type: "GET",
							url: "/api/getdata?doi=" + kriya.config.content.doi + "&project=" + kriya.config.content.project + "&customer=" + kriya.config.content.customer + "&xpath=//article//article-meta//contrib-group[@data-group='reviewer']",
							contentType: "application/xml; charset=utf-8",
							success: function (msg) {
								var versionNumber = $('.versionMenu .current-version').text().trim();
								var versionCountObj = [];
								$(msg).find('contrib[contrib-type="reviewer"]').each(function(){
									var reviewerEmail = $(this).find('email').text();
									var reviewerRow = $('[data-component="reviewer_edit"] tr:contains(' + reviewerEmail + ')');
									
									var rowIndex    = reviewerRow.index();
									if(rowIndex >= 0 && !versionCountObj[rowIndex]){
										versionCountObj[rowIndex] = {'email' : reviewerEmail, 'vercnt' : 0};
									}
									
									var reviewerMsg = $(this).attr('data-reviewer-message');
									if ($(this).attr('data-version') == versionNumber){
										if($(this).attr('data-reviewer-status') == "terminated" || $(this).attr('data-reviewer-status') == "rejected"){
											reviewerRow.find('.dropdown-assignReviewer').html('Reassign now');
											reviewerRow.find('.dropdown-assignReviewer').attr('data-message', "{'click':{'funcToCall': 'getEmailTemplateToEdit','param': {'userMailID': '" + userEmail + "', 'userName': '" + userName + "'}, 'channel':'components','topic':'general'}}");
											reviewerRow.find('.dropdown-assignReviewer').attr('data-currStage', 'associateediter-assign-reviewer');
											reviewerRow.find('.dropdown-assignReviewer').attr('data-work-name', 'reAssignToReviewer');
										}else if($(this).attr('data-reviewer-status') == "back-up"){
											reviewerRow.find('.assignReviewerBtn.btn').addClass('disabled');
											reviewerRow.find('.assignReviewerBtn.btn').html('<i class="material-icons" style="line-height: inherit;color: green;">check_circle</i> Assigned as back-up');
											//reviewerRow.find('.addBackUp.btn').after('<span class="btn btn-small red lighten-1 removeBackUp" data-message="{\'click\':{\'funcToCall\': \'removeBackUpReviewer\', \'channel\':\'components\',\'topic\':\'general\'}}">Remove</span>');
										}else{
											//reviewerRow.find('.addBackUp.btn').remove();
											reviewerRow.find('.assignReviewerBtn.btn').addClass('disabled');
											reviewerRow.find('.assignReviewerBtn.btn').html('<i class="material-icons" style="line-height: inherit;color: green;">check_circle</i> Assigned');
											if($(this).attr('data-reviewer-status') != "completed"){
												reviewerRow.find('.assignReviewerBtn.btn').closest('td').prepend('<span class="btn btn-small blue lighten-1 followup" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\',\'param\': {\'confirmMsg\': {\'click\':{\'funcToCall\': \'followupConfirm\',\'channel\':\'components\',\'topic\':\'general\'}}, \'userMailID\': \'' + userEmail + '\', \'userName\': \'' + userName + '\'}, \'channel\':\'components\',\'topic\':\'general\'}}" data-currStage="associateediter-assign-reviewer" data-work-name="followupReviewer">Follow&nbsp;Up</span>');
											}									
										}
									}else{
										if(versionCountObj[rowIndex]){
											versionCountObj[rowIndex]['vercnt']++;
										}
										
										if($(this).attr('data-reviewer-status') == "terminated"){
											var vn = $(this).attr('data-version');
											reviewerRow.find('td[data-version="' + vn + '"]').append('<i class="material-icons" style="line-height: inherit;color: red;">close</i>');
										}else if(reviewerMsg){
											var vn = $(this).attr('data-version');
											reviewerRow.find('td[data-version="' + vn + '"]').append('<span class="reviewer-message">' + reviewerMsg + '</span>');
										}else{
											var vn = $(this).attr('data-version');
											reviewerRow.find('td[data-version="' + vn + '"]').append('<span class="reviewer-message">Incomplete</span>');
										}
									}
								});

								versionCountObj = versionCountObj.sort(function(a, b){
									return a.vercnt-b.vercnt;
								});
								for(var a=0;a<versionCountObj.length;a++){
									if(versionCountObj[a] && versionCountObj[a].email){
										var reviewerRow = $('[data-component="reviewer_edit"] tr:contains(' + versionCountObj[a].email + ')');
										$('[data-component="reviewer_edit"] tbody').prepend(reviewerRow);
									}
								}
								$('[data-component="reviewer_edit"] .filterReviewer').val('');
								kriya.popUp.openPopUps($('[data-component="reviewer_edit"]'));	

							},
							error: function(err){
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed',
									type: 'error',
									content: 'Failed to get the reviewers.',
									icon: 'icon-warning2'
								});		
							}
						});
					},
					error: function (res) {
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Failed to get the reviewers.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			sendToAssociateEditor: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				targetNode = kriya.xpath(nodeXpath);

				var userEmail = $(targetNode).closest('tr').find('.reviewer-email').text();
				var surName  = $(targetNode).closest('tr').find('.reviewer-surname').text();
				var givenName  = $(targetNode).closest('tr').find('.reviewer-given-name').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();
				$(targetNode).text('Sending...');
				$(targetNode).addClass('disabled');
				$('.la-container').fadeIn();
				var parameters = {
					'customer' :kriya.config.content.customer,
					'project':kriya.config.content.project,
					'doi':kriya.config.content.doi,
					'role'     :kriya.config.content.role,
					'currStage': 'associate-editorcheck',
					'to' : userEmail,
					'recipientName' : surName + ' ' + givenName,
					'version' : versionNumber,
					'assignUser' : 'true'
				};
				parameters['emailBody'] = popper.find('.email-body').html();
				if (popper.find('.email-cc').text() != "") {
					parameters['cc'] = popper.find('.email-cc').text();
				}
				if (popper.find('.email-bcc').text() != "") {
					parameters['bcc'] = popper.find('.email-bcc').text();
				}
				if (popper.find('.email-to').text() != "") {
					parameters['to'] = popper.find('.email-to').text();
					parameters['editedTo'] = popper.find('.email-to').text();
				}
				if (popper.find('.email-replyTo').text() != "") {
					parameters['replyTo'] = popper.find('.email-replyTo').text();
				}
				if (popper.find('.email-subject').text() != "") {
					parameters['subject'] = popper.find('.email-subject').text();
				}

				if(popper.find('.attachementContainer .card').length > 0){
					parameters['attachments'] = [];
					popper.find('.attachementContainer .card').each(function(){
						parameters['attachments'].push({
							filename: $(this).attr('data-file-name'),
							path: $(this).attr('data-file-path'),
							contentType: $(this).attr('data-file-type')
						});
					});
				}
				
				$.ajax({
					type : "POST",
					url  : "/api/signoff",
					data : parameters,
					success: function (res) {
						if(res && res.status && res.status.message == "failed"){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Sending to associate editor was failed.',
								icon: 'icon-warning2'
							});
						}else{
								if($('#contentDivNode .jrnlEditorGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')').length == 0){
									var editorData = $('<span class="jrnlEditorGroup" data-contrib-type="editor" id="' + uuid.v4() + '" data-editable="false" data-reviewer-status="in-progress" data-version="' + versionNumber + '"/>');
									editorData.append('<span class="jrnlAuthor" id="' + uuid.v4() + '" />');
									editorData.append('<span class="jrnlEmail" id="' + uuid.v4() + '" >' + userEmail + '</span>');

									editorData.find('.jrnlAuthor').append('<span class="jrnlSurName" id="' + uuid.v4() + '" >' + surName + '</span>');
									editorData.find('.jrnlAuthor').append('<span class="jrnlGivenName" id="' + uuid.v4() + '" >' + givenName + '</span>');

									if($('#contentDivNode .jrnlEditorsGroup').length == 0){
										$('#contentDivNode').prepend('<div class="jrnlEditorsGroup" data-inserted="true" data-group="editor" id="' + uuid.v4() + '" />');
									}
									$('#contentDivNode .jrnlEditorsGroup').append(editorData);

								}else{
									var editorData	 = $('#contentDivNode .jrnlEditorGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')');
								}
								
								editorData.attr('data-reviewer-status', 'in-progress');
								var formData = new FormData();
								formData.append('customer', kriya.config.content.customer);
								formData.append('doi', kriya.config.content.doi);
								formData.append('project', kriya.config.content.project);

								var blob = new Blob(['<content>' +  $('#contentDivNode .jrnlEditorsGroup')[0].outerHTML + '</content>'], {
									type: 'text/html'
								});

								formData.append('file', blob, 'save_data.html');

								$.ajax({
									url: '/api/save_content',
									type: 'POST',
									data: formData,
									processData: false,
									contentType: false,
									success: function(res){
										$('.la-container').fadeOut();
										$('body .messageDiv .message').html('You have successfully signed off the article to the next stage.<br> Please close the browser to exit the system.');
										$('body .messageDiv').removeClass('hidden');
										$('body .messageDiv .feedBack').removeClass('hidden');
										kriya.config.pageStatus = "signed-off";
									},
									error: function(err){
										console.log(err);
									}
								});
						}
					},
					error: function (res) {
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Assign to reviewer was failed.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			reAssignToReviewer: function(param, targetNode){
				var userEmail = $(targetNode).closest('tr').find('.reviewer-email').text();
				var surName  = $(targetNode).closest('tr').find('.reviewer-surname').text();
				var givenName  = $(targetNode).closest('tr').find('.reviewer-given-name').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();
				$(targetNode).closest('td').find('.assignReviewerBtn').text('Reassigning...');
				$(targetNode).closest('td').find('.assignReviewerBtn').addClass('disabled');
				var parameters = {
					'customer'  : kriya.config.content.customer,
					'project' : kriya.config.content.project,
					'doi' : kriya.config.content.doi,
					'role'      : 'reviewer',
					'currStage' : 'reAssignToReviewer',
					'to' : userEmail,
					'recipientName' : surName + ' ' + givenName,
					'stagename' : 'Reviewer Check',
					'version' : versionNumber
				};
				
				$.ajax({
					type : "POST",
					url  : "/api/signoff",
					data : parameters,
					success: function (res) {
						if(res && res.status && res.status.message == "failed"){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Assign to reviewer was failed.',
								icon: 'icon-warning2'
							});
						}else{
							if($('#contentDivNode .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')').length > 0){
								var reviewerNode = $('#contentDivNode .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')');
								reviewerNode.attr('data-reviewer-status', 'waiting');
								reviewerNode.removeAttr('data-reviewer-message');
								if(reviewerNode.find('.jrnlFN').length == 0){
									reviewerNode.append('<span class="jrnlFN" fn-type="review-history"/>');
								}
								reviewerNode.find('.jrnlFN').append('<span class="jrnlFNPara" data-assign-date="' + new Date().getTime() + '">waiting</span>');


								var formData = new FormData();
								formData.append('customer', kriya.config.content.customer);
								formData.append('doi', kriya.config.content.doi);
								formData.append('project', kriya.config.content.project);
								var blob = new Blob(['<content>' +  reviewerNode[0].outerHTML + '</content>'], {
									type: 'text/html'
								});
								formData.append('file', blob, 'save_data.html');

								$.ajax({
									url: '/api/save_content',
									type: 'POST',
									data: formData,
									processData: false,
									contentType: false,
									success: function(res){
										var reviewerList = $('#queryDivNode #openQueryDivNode li[data-reviewer-email="' + userEmail + '"]');
										reviewerList.attr('data-reviewer-status', 'waiting');
										reviewerList.find('.reviewer-message').remove();

										if(reviewerList.find('.terminateBtn').length > 0){
											reviewerList.find('.terminateBtn').removeClass('disabled');
											reviewerList.find('.terminateBtn').html('Terminate');
										}else{
											$('<span class="btn btn-small terminateBtn" data-message="{\'click\':{\'funcToCall\': \'terminateReviewer\',\'channel\':\'components\',\'topic\':\'general\'}}">Terminate</span>').insertBefore(reviewerList.find('.notify-badge'));
										}
										
										
										var assigned = $(targetNode).closest('tr').find('.reviewer-article-assigned').text();
										assigned = (assigned)?(parseInt(assigned) + 1):1;
										var parameters = {
											'customer'    : kriya.config.content.customer,
											'email'           : userEmail,
											'modifiedData'    : 'true',
											'doi'       : kriya.config.content.doi,
											'reviewStatus'    : 'waiting'
										};
										kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
											$(targetNode).closest('td').find('.assignReviewerBtn').html('<i class="material-icons" style="line-height: inherit;color: green;">check_circle</i> Assigned');
											var currentUserEmail = $('.userProfile .username').attr('data-user-email');
											var currentUserName = $('.userProfile .username').text();
											$(targetNode).closest('td').prepend('<span class="btn btn-small blue lighten-1 followup" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\',\'param\': {\'confirmMsg\': {\'click\':{\'funcToCall\': \'followupConfirm\',\'channel\':\'components\',\'topic\':\'general\'}},\'userMailID\': \'' + currentUserEmail + '\', \'userName\': \'' + currentUserName + '\'}, \'channel\':\'components\',\'topic\':\'general\'}}" data-currStage="associateediter-assign-reviewer" data-work-name="followupReviewer">Follow&nbsp;Up</span>');
											$(targetNode).closest('tr').find('.reviewer-article-assigned').text(assigned);
										}, function(err){
											console.log(err);
										});

									},
									error: function(err){
										console.log(err);
									}
								});
							}
						}
					},
					error: function (res) {
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Assign to reviewer was failed.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			terminateReviewer: function(param, targetNode){
				var userEmail = $(targetNode).closest('[data-reviewer-email]').attr('data-reviewer-email');
				var userName  = $(targetNode).closest('[data-reviewer-name]').attr('data-reviewer-name');
				var curentUser = $('.userProfile .username').attr('data-user-email');
				eventHandler.components.general.getEmailTemplateToEdit({
						'currStage'      : 'reviewerTerminate',
						'reviewer'       : 'terminateReviewer',
						'recipientName'  : userName,
						'userEmail'      : userEmail, 
						'userMailID'     : curentUser,
						'confirmMsg'     : "{'click':{'funcToCall': 'terminateReviewerConfirm','channel':'components','topic':'general'}}"
					}, targetNode);
			},
			followupConfirm: function(param, targetNode){
				var popper       = $(targetNode).closest('[data-component]');
				$('.la-container').fadeIn();
				var parameters = {
					'customer' : kriya.config.content.customer,
					'project': kriya.config.content.project,
					'doi': kriya.config.content.doi,
					'role'       : kriya.config.content.role,
					'currStage'  : 'followupReviewer',
					'stagename'  : kriya.config.content.stageFullName,
				};

				parameters['emailBody'] = popper.find('.email-body').html();
				if (popper.find('.email-cc').text() != "") {
					parameters['cc'] = popper.find('.email-cc').text();
				}
				if (popper.find('.email-bcc').text() != "") {
					parameters['bcc'] = popper.find('.email-bcc').text();
				}
				if (popper.find('.email-to').text() != "") {
					parameters['to'] = popper.find('.email-to').text();
					parameters['editedTo'] = popper.find('.email-to').text();
				}
				if (popper.find('.email-replyTo').text() != "") {
					parameters['replyTo'] = popper.find('.email-replyTo').text();
				}
				if (popper.find('.email-subject').text() != "") {
					parameters['subject'] = popper.find('.email-subject').text();
				}

				if(popper.find('.attachementContainer .card').length > 0){
					parameters['attachments'] = [];
					popper.find('.attachementContainer .card').each(function(){
						parameters['attachments'].push({
							filename: $(this).attr('data-file-name'),
							path: $(this).attr('data-file-path'),
							contentType: $(this).attr('data-file-type')
						});
					});
				}
				
				$.ajax({
					type : "POST",
					url  : "/api/signoff",
					data : parameters,
					success: function (res) {
						if(res && res.status && res.status.message == "failed"){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Terminate reviewer was failed.',
								icon: 'icon-warning2'
							});
						}
						kriya.popUp.closePopUps(popper);
						$('.la-container').fadeOut();
					},
					error: function (res) {
						$('.la-container').fadeOut();
						kriya.popUp.closePopUps(popper);
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Terminate reviewer was failed.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			terminateReviewerConfirm: function(param, targetNode){
				var popper       = $(targetNode).closest('[data-component]');
				var nodeXpath    = popper.attr('data-node-xpath');
				var terminateBtn = kriya.xpath(nodeXpath);
				$('.la-container').fadeIn();
				$(terminateBtn).text('Terminating...');
				$(terminateBtn).addClass('disabled');
				var userEmail = $(terminateBtn).closest('[data-reviewer-email]').attr('data-reviewer-email');
				var versionNumber = $('.versionMenu .current-version').text().trim();
				var parameters = {
					'customer' : kriya.config.content.customer,
					'project': kriya.config.content.project,
					'doi': kriya.config.content.doi,
					'role'       : kriya.config.content.role,
					'currStage'  : 'terminateReviewer',
					'stagename'  : kriya.config.content.stageFullName,
					'userMailID' : userEmail,
					'reviewerStatus' : "terminated",
					'version' : versionNumber
				};

				parameters['emailBody'] = popper.find('.email-body').html();
				if (popper.find('.email-cc').text() != "") {
					parameters['cc'] = popper.find('.email-cc').text();
				}
				if (popper.find('.email-bcc').text() != "") {
					parameters['bcc'] = popper.find('.email-bcc').text();
				}
				if (popper.find('.email-to').text() != "") {
					parameters['to'] = popper.find('.email-to').text();
					parameters['editedTo'] = popper.find('.email-to').text();
				}
				if (popper.find('.email-replyTo').text() != "") {
					parameters['replyTo'] = popper.find('.email-replyTo').text();
				}
				if (popper.find('.email-subject').text() != "") {
					parameters['subject'] = popper.find('.email-subject').text();
				}

				if(popper.find('.attachementContainer .card').length > 0){
					parameters['attachments'] = [];
					popper.find('.attachementContainer .card').each(function(){
						parameters['attachments'].push({
							filename: $(this).attr('data-file-name'),
							path: $(this).attr('data-file-path'),
							contentType: $(this).attr('data-file-type')
						});
					});
				}
				
				$.ajax({
					type : "POST",
					url  : "/api/signoff",
					data : parameters,
					success: function (res) {
						if(res && res.status && res.status.message == "failed"){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Terminate reviewer was failed.',
								icon: 'icon-warning2'
							});

							$(terminateBtn).removeClass('disabled');
							$(terminateBtn).text('Terminate');
						}else{
							$.ajax({
								type : "GET",
								url  : "/api/getuserdata?format=xml&userEmail=" + userEmail + '&roleType=production&accessLevel=reviewer',
								success: function (res) {
										if(res){
											res = res.replace(/<(\d+)>/g, '<user>');
											res = res.replace(/<\/(\d+)>/g, '</user>');
											var userDoc = $(res);
											var parameters = {
												'customer'    : kriya.config.content.customer,
												'email'           : userDoc.find('email').text(),
												'modifiedData'    : 'true',
												'doi'       : kriya.config.content.doi,
												'reviewStatus'    : 'terminated'
											};
											kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
												var reviewerNode = $('#contentDivNode .jrnlReviewersGroup .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + userEmail +')');
												reviewerNode.attr('data-reviewer-status', 'terminated');
												reviewerNode.find('.jrnlFN .jrnlFNPara:last').attr('data-decision-date', new Date().getTime());
												reviewerNode.find('.jrnlFN .jrnlFNPara:last').text('terminated');

												$(terminateBtn).closest('[data-reviewer-status]').attr('data-reviewer-status', 'terminated');
												$(terminateBtn).text('Terminated');
												$('.la-container').fadeOut();
												kriya.popUp.closePopUps(popper);
											}, function(err){
												$('.la-container').fadeOut();
													kriya.popUp.closePopUps(popper);
													kriya.notification({
														title: 'Failed',
														type: 'error',
														content: 'Update user data was failed.',
														icon: 'icon-warning2'
													});
											});
										}
								},
								error: function(err){
									kriya.notification({
										title: 'Failed',
										type: 'error',
										content: 'Update user data was failed.',
										icon: 'icon-warning2'
									});
									$('.la-container').fadeOut();
									kriya.popUp.closePopUps(popper);
								}
							});
						}
					},
					error: function (res) {
						$('.la-container').fadeOut();
						kriya.popUp.closePopUps(popper);
						$(terminateBtn).text('Terminate');
						$(terminateBtn).removeClass('disabled');
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Terminate reviewer was failed.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			deleteUser: function(param, targetNode){
				var editRowNode = $(targetNode).closest('tr');
				var nodeXpath = kriya.getElementXPath(editRowNode);
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  "Are you sure you want to delete the user?",
					'size'  : 'pop-sm',
					'subcomp' : 'true',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'     : {
								'data-node-xpath' : nodeXpath
							},
							'message' : "{'click':{'funcToCall': 'deleteUserConfirm','channel':'components','topic':'general'}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
			},
			deleteUserConfirm: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var nodeXpath = $(targetNode).attr('data-node-xpath');
				if(nodeXpath){
					var dataNode = kriya.xpath(nodeXpath);
					if($(dataNode).length > 0){
						var parameters = {
							'customer'    : kriya.config.content.customer,
							'email'           : $(dataNode).find('.reviewer-email').text(),
							'modifiedData'    : 'true',
							'dormantuser'     : 'dormant',
						};
						$('.la-container').fadeIn();
						kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
							kriya.popUp.closePopUps(popper);
							$('.la-container').fadeOut();
							$(dataNode).remove();
						}, function(err){
								kriya.popUp.closePopUps(popper);
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed',
									type: 'error',
									content: 'Deleting user was failed.',
									icon: 'icon-warning2'
								});
						});
					}
				}
			},
			editUser: function(param, targetNode){
				if(param.component){
					var editRowNode = $(targetNode).closest('tr');
					kriya['styles'].init(editRowNode, param.component);
					$('[data-component="' + param.component + '"] .popupHead[data-pop-type="edit"]').removeClass('hidden');
					$('[data-component="' + param.component + '"] .popupHead[data-pop-type="new"]').addClass('hidden');
				}
			},
			removeBackUpReviewer: function(param, targetNode){
				var userEmail = $(targetNode).closest('[data-reviewer-email]').attr('data-reviewer-email');
				var versionNumber = $('.versionMenu .current-version').text().trim();
				var reviewerNode = $('#contentDivNode .jrnlReviewerGroup[data-reviewer-status="back-up"][data-version="' + versionNumber + '"]:contains(' + userEmail + ')');
				reviewerNode.remove();

				var formData = new FormData();
				formData.append('customer', kriya.config.content.customer);
				formData.append('doi', kriya.config.content.doi);
				formData.append('project', kriya.config.content.project);

				var blob = new Blob(['<content>' +  $('#contentDivNode .jrnlReviewersGroup')[0].outerHTML + '</content>'], {
					type: 'text/html'
				});
				formData.append('file', blob, 'save_data.html');

				$(targetNode).text('Removing...');
				$(targetNode).addClass('disabled');
				$.ajax({
					url: '/api/save_content',
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(res){
						$(targetNode).closest('li').remove();
						var queriesCollaps = $('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"] .kriya-collapsible');
						queriesCollaps.find('li[data-reviewer-email="' + userEmail + '"]').remove();
					},
					error: function(err){
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Add reviewer to back up was failed.',
							icon: 'icon-warning2'
						});
					}
				});

			},
			addBackUpReviewer: function(param, targetNode){
				var userEmail = $(targetNode).closest('tr').find('.reviewer-email').text();
				var surName  = $(targetNode).closest('tr').find('.reviewer-surname').text();
				var givenName  = $(targetNode).closest('tr').find('.reviewer-given-name').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();
				$(targetNode).closest('td').find('.assignReviewerBtn').text('Adding...');
				$(targetNode).closest('td').find('.assignReviewerBtn').addClass('disabled');

				var reviewerNode = $('#contentDivNode .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + userEmail + ')');
				if(reviewerNode.length == 0){
					reviewerNode = $('<span class="jrnlReviewerGroup" data-contrib-type="reviewer" id="' + uuid.v4() + '" data-editable="false" data-reviewer-status="back-up" data-version="' + versionNumber + '"/>');
					reviewerNode.append('<span class="jrnlAuthor" id="' + uuid.v4() + '" />');
					reviewerNode.append('<span class="jrnlEmail" id="' + uuid.v4() + '" >' + userEmail + '</span>');

					reviewerNode.find('.jrnlAuthor').append('<span class="jrnlSurName" id="' + uuid.v4() + '" >' + surName + '</span>');
					reviewerNode.find('.jrnlAuthor').append('<span class="jrnlGivenName" id="' + uuid.v4() + '" >' + givenName + '</span>');

						if($('#contentDivNode .jrnlReviewersGroup').length == 0){
							$('#contentDivNode').prepend('<div class="jrnlReviewersGroup" data-inserted="true" data-group="reviewer" id="' + uuid.v4() + '" />');
						}
						$('#contentDivNode .jrnlReviewersGroup').append(reviewerNode);
					}else{
						reviewerNode.removeAttr('data-reviewer-message');
						reviewerNode.attr('data-reviewer-status', 'back-up');
					}

					var formData = new FormData();
					formData.append('customer', kriya.config.content.customer);
					formData.append('doi', kriya.config.content.doi);
					formData.append('project', kriya.config.content.project);

					var blob = new Blob(['<content>' +  $('#contentDivNode .jrnlReviewersGroup')[0].outerHTML + '</content>'], {
						type: 'text/html'
					});

					formData.append('file', blob, 'save_data.html');

					$.ajax({
						url: '/api/save_content',
						type: 'POST',
						data: formData,
						processData: false,
						contentType: false,
						success: function(res){
							$(targetNode).closest('td').find('.assignReviewerBtn').html('<i class="material-icons" style="line-height: inherit;color: green;">check_circle</i> Assigned as back-up');
							//$(targetNode).after('<span class="btn btn-small red lighten-1 removeBackUp" data-message="{\'click\':{\'funcToCall\': \'removeBackUpReviewer\', \'channel\':\'components\',\'topic\':\'general\'}}">Remove</span>');
							if($('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"] .kriya-collapsible').length == 0){
								$('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"]').append('<ul class="kriya-collapsible"/>');
							}
							
							var queriesCollaps = $('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"] .kriya-collapsible');
							var reviewerList = queriesCollaps.find('li[data-reviewer-email="' + userEmail + '"]');
							if(reviewerList.length == 0){
								reviewerList = $('<li />');
								reviewerList.addClass('reviewer');
								reviewerList.attr('data-reviewer-email', userEmail);
								reviewerList.attr('data-reviewer-status', 'back-up');
								reviewerList.append('<div class="collapsible-header" data-message="{\'click\':{\'funcToCall\': \'commentCollapsible\',\'channel\':\'components\',\'topic\':\'general\'}}"><i class="material-icons collap-icon">keyboard_arrow_right</i><i class="material-icons" data-message="{\'click\':{\'funcToCall\': \'viewReviewDetails\',\'channel\':\'components\',\'topic\':\'general\'}}">assignment_ind</i> <span class="reviewer-name">' + surName + ' ' + givenName + '<span class="userLabel">Reviewer</span></span> <span class="notify-badge">0</span> </div>');
								reviewerList.append('<div class="collapsible-body" />');
								queriesCollaps.append(reviewerList);
							}else{
								reviewerList.attr('data-reviewer-status', 'back-up');
								reviewerList.find('.reviewer-message').remove();
								reviewerList.find('.terminateBtn').remove();
							}
							$('<span class="btn btn-small rmBackupBtn" data-message="{\'click\':{\'funcToCall\': \'removeBackUpReviewer\',\'channel\':\'components\',\'topic\':\'general\'}}">Remove back-up</span>').insertBefore(reviewerList.find('.notify-badge'));
						},
						error: function(err){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Add reviewer to back up was failed.',
								icon: 'icon-warning2'
							});
						}
				  });
			},
			getEmailTemplateToEdit: function(param, targetNode){
				var currStage = $(targetNode).attr('data-currStage');
				currStage = (param && param.currStage)?param.currStage:currStage;

				var userEmail = $(targetNode).closest('tr').find('.reviewer-email').text();
				userEmail = (param && param.userEmail)?param.userEmail:userEmail;
				var surName  = $(targetNode).closest('tr').find('.reviewer-surname').text();
				var givenName  = $(targetNode).closest('tr').find('.reviewer-given-name').text();
				var recipientName = surName + ' '+ givenName;
				recipientName = (param && param.recipientName)?param.recipientName:recipientName;
				
				var compoenet = $('[data-component="assign_reviewer_edit"]');
				var parameter = 'customer=' + kriya.config.content.customer + '&doi=' + kriya.config.content.doi + '&project=' + kriya.config.content.project;
				parameter = parameter + '&currStage=' + currStage;

				$('.la-container').fadeIn();
				$.ajax({
					type: "GET",
					url: "/api/getsignoffoptions?" + parameter,
					success: function (data) {
						$('.la-container').fadeOut();
						var res = $('<div>' + data + '</div>');
						var reviewer = $(targetNode).attr('data-work-name');
						reviewer = (param && param.reviewer)?param.reviewer:reviewer;
						if (res.find('trigger[name="' + reviewer + '"]').length > 0){
							var emailComp = $('<div class="email-component card" style="padding: 0px 30px 10px;margin: 5px 10px;"><p style="margin: 0px -30px;padding: 10px 10px;background: #3c3c3c;color: #fff;font-weight: bold;box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);border-top-left-radius: 3px;border-top-right-radius: 3px;">Email Message</p></div>');
							var trigger   = res.find('trigger[name="' + reviewer + '"]:first');
							$(trigger).find('> *').each(function(){
								if ($(this)[0].nodeName != 'DIV'){
									var currEmailComp = $('<p style="padding: 0px 5px;border-bottom: 1px solid #848484;" contenteditable="true" class="email-' + $(this)[0].nodeName.toLocaleLowerCase() + '">');
								}else{
									var currEmailComp = $('<div contenteditable="true" data-paste-clean="false" class="' + $(this).attr('class') + '">');
								}
								var compData = $(this).html();
								compData = compData.replace(/{recipientName}/g, recipientName);
								
								if(param && param.userMailID){
									compData = compData.replace(/{userMailID}/g, param.userMailID);
								}
								if(param && param.userName){
									compData = compData.replace(/{userName}/g, param.userName);
								}
								
								currEmailComp.html(compData);
								if ($(this)[0].nodeName == 'TO' && userEmail){
									currEmailComp.attr('data-message', "{'focusout':{'funcToCall': 'validateEmailAddress', 'channel':'components','topic':'general'}}");
									currEmailComp.html(userEmail);
								}
								if($(this)[0].nodeName != 'TRIGGER'){
									$(emailComp).append(currEmailComp);
								}
							});
							var currEmailComp = $('<p style="padding: 0px 5px;border-bottom: 1px solid #848484;" contenteditable="true" class="email-replyTo">');
							$(emailComp).find('.email-subject').before(currEmailComp);
							
							var currEmailComp = $('<div class="email-attachement row"><span class="btn btn-medium amber"><input type="file" name="files[]" multiple="" style="display:none;" data-channel="components" data-topic="PopUp" data-event="change" data-message="{\'funcToCall\': \'changeAttachFile\'}"/><span class="fileChooser" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'attachFileTrigger\'}"><i class="material-icons">attach_file</i>Please click here to attach</span></span></div><div class="row attachementContainer"/>');
							$(emailComp).append(currEmailComp);

							$(compoenet).find('[data-wrapper]').html(emailComp);
						}
						var nodeXpath = kriya.getElementXPath(targetNode);
						$(compoenet).attr('data-node-xpath', nodeXpath);
						
						if(param && param.confirmMsg){
							if(typeof(param.confirmMsg) == "object"){
								param.confirmMsg = JSON.stringify(param.confirmMsg);
							}
							$(compoenet).find('.mailConfirmBtn').attr('data-message', param.confirmMsg);
						}else if(currStage == "assignassociateeditor"){
							$(compoenet).find('.mailConfirmBtn').attr('data-message', "{'click':{'funcToCall': 'sendToAssociateEditor', 'channel':'components','topic':'general'}}");
						}else if(currStage == "authorreview"){
							$(compoenet).find('.mailConfirmBtn').attr('data-message', "{'click':{'funcToCall': 'continueResubmitManuscript', 'channel':'components','topic':'general'}}");
							$(compoenet).find('.cancelBtn').remove();
						}
						
						kriya.popUp.openPopUps(compoenet);
						$(compoenet).scrollTop(0);
					},
					error: function (err) {
						$('.la-container').fadeOut();
					}
				});
			},
			assignToReviewer: function(param, targetNode, onsuccess, onError){
				if(!param || !param.userEmail){
					return false;
				}
				var popper = $('');
				if(param.popper){
					popper = $(param.popper);
				}
				var versionNumber = $('.versionMenu .current-version').text().trim();
				var parameters = {
					'customer' :kriya.config.content.customer,
					'project':kriya.config.content.project,
					'doi':kriya.config.content.doi,
					'role'     :'reviewer',
					'currStage': 'assignToReviewer',
					'to' : param.userEmail,
					'recipientName' : param.surName + ' ' + param.givenName,
					'stagename' : 'Reviewer Check',
					'version' : versionNumber,
					'skipAssignTo' : 'true'
				};
				
				if (popper.find('.email-body').html() != "") {
					parameters['emailBody'] = popper.find('.email-body').html();
				}
				if (popper.find('.email-cc').text() != "") {
					parameters['cc'] = popper.find('.email-cc').text();
				}
				if (popper.find('.email-bcc').text() != "") {
					parameters['bcc'] = popper.find('.email-bcc').text();
				}
				if (popper.find('.email-to').text() != "") {
					parameters['to'] = popper.find('.email-to').text();
					parameters['editedTo'] = popper.find('.email-to').text();
				}
				if (popper.find('.email-replyTo').text() != "") {
					parameters['replyTo'] = popper.find('.email-replyTo').text();
				}
				if (popper.find('.email-subject').text() != "") {
					parameters['subject'] = popper.find('.email-subject').text();
				}
				
				if(popper.find('.attachementContainer .card').length > 0){
					parameters['attachments'] = [];
					popper.find('.attachementContainer .card').each(function(){
						parameters['attachments'].push({
							filename: $(this).attr('data-file-name'),
							path: $(this).attr('data-file-path'),
							contentType: $(this).attr('data-file-type')
						});
					});
				}

				$.ajax({
					type : "POST",
					url  : "/api/signoff",
					data : parameters,
					success: function (res) {
						if(res && res.status && res.status.message == "failed"){
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Assign to reviewer was failed.',
								icon: 'icon-warning2'
							});
						}else{
							var reviewerNode = $('#contentDivNode .jrnlReviewerGroup[data-version="' + versionNumber + '"]:contains(' + param.userEmail + ')');
							if(reviewerNode.length > 0){
								reviewerNode.attr('data-reviewer-status', 'waiting');
								reviewerNode.removeAttr('data-reviewer-message');
							}else{
								reviewerNode = $('<span class="jrnlReviewerGroup" data-contrib-type="reviewer" id="' + uuid.v4() + '" data-editable="false" data-reviewer-status="waiting" data-version="' + versionNumber + '"/>');
								reviewerNode.append('<span class="jrnlAuthor" id="' + uuid.v4() + '" />');
								reviewerNode.append('<span class="jrnlEmail" id="' + uuid.v4() + '" >' + param.userEmail + '</span>');								
								reviewerNode.find('.jrnlAuthor').append('<span class="jrnlSurName" id="' + uuid.v4() + '" >' + param.surName + '</span>');
								reviewerNode.find('.jrnlAuthor').append('<span class="jrnlGivenName" id="' + uuid.v4() + '" >' + param.givenName + '</span>');

								if($('#contentDivNode .jrnlReviewersGroup').length == 0){
									$('#contentDivNode').prepend('<div class="jrnlReviewersGroup" data-inserted="true" data-group="reviewer" id="' + uuid.v4() + '" />');
								}
								$('#contentDivNode .jrnlReviewersGroup').append(reviewerNode);
							}
							if(reviewerNode.find('.jrnlFN').length == 0){
								reviewerNode.append('<span class="jrnlFN" fn-type="review-history"/>');
							}
							reviewerNode.find('.jrnlFN').append('<span class="jrnlFNPara" data-assign-date="' + new Date().getTime() + '">waiting</span>');



								var formData = new FormData();
								formData.append('customer', kriya.config.content.customer);
								formData.append('doi', kriya.config.content.doi);
								formData.append('project', kriya.config.content.project);

								var blob = new Blob(['<content>' +  $('#contentDivNode .jrnlReviewersGroup')[0].outerHTML + '</content>'], {
									type: 'text/html'
								});

								formData.append('file', blob, 'save_data.html');

								$.ajax({
									url: '/api/save_content',
									type: 'POST',
									data: formData,
									processData: false,
									contentType: false,
									success: function(res){
										if($('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"] .kriya-collapsible').length == 0){
											$('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"]').append('<ul class="kriya-collapsible"/>');
										}
										
										var queriesCollaps = $('#queryDivNode #openQueryDivNode #commentsToAuthor [data-current-version="true"] .kriya-collapsible');
										var reviewerList = queriesCollaps.find('li[data-reviewer-email="' + param.userEmail + '"]');
										if(reviewerList.length > 0){
											reviewerList.attr('data-reviewer-status', 'waiting');
											reviewerList.find('.reviewer-message').remove();
											if(reviewerList.find('.terminateBtn').length > 0){
												reviewerList.find('.terminateBtn').removeClass('disabled');
												reviewerList.find('.terminateBtn').html('Terminate');
											}else{
												$('<span class="btn btn-small terminateBtn" data-message="{\'click\':{\'funcToCall\': \'terminateReviewer\',\'channel\':\'components\',\'topic\':\'general\'}}">Terminate</span>').insertBefore(reviewerList.find('.notify-badge'));
											}
										}else{
											reviewerList = $('<li />');
											reviewerList.addClass('reviewer');
											reviewerList.attr('data-reviewer-email', param.userEmail);
											reviewerList.attr('data-reviewer-name', (param.surName + ' ' + param.givenName));
											reviewerList.attr('data-reviewer-status', 'waiting');
											reviewerList.append('<div class="collapsible-header" data-message="{\'click\':{\'funcToCall\': \'commentCollapsible\',\'channel\':\'components\',\'topic\':\'general\'}}"><i class="material-icons collap-icon">keyboard_arrow_right</i><i class="material-icons" data-message="{\'click\':{\'funcToCall\': \'viewReviewDetails\',\'channel\':\'components\',\'topic\':\'general\'}}">assignment_ind</i> <span class="reviewer-name">' + param.surName + ' ' + param.givenName + '<span class="userLabel">Reviewer</span></span><span class="btn btn-small terminateBtn" data-message="{\'click\':{\'funcToCall\': \'terminateReviewer\',\'channel\':\'components\',\'topic\':\'general\'}}">Terminate</span> <span class="notify-badge">0</span> </div>');
											reviewerList.append('<div class="collapsible-body" />');
											queriesCollaps.append(reviewerList);
										}
										
										//var assigned = param.assigned;
										//assigned = (assigned)?(parseInt(assigned) + 1):1;
										var parameters = {
											'customer'    : kriya.config.content.customer,
											'email'           : param.userEmail,
											'modifiedData'    : 'true',
											'doi'       : kriya.config.content.doi,
											'reviewStatus'    : 'waiting',
										};
										kriya.general.sendAPIRequest('updateuserdata', parameters, function(res){
											if(onsuccess && typeof(onsuccess) == "function"){
												onsuccess(res);
											}
										}, function(err){
											if(onError && typeof(onError) == "function"){
												onError(err);
											}
										});
									},
									error: function(err){
										if(onError && typeof(onError) == "function"){
											onError(err);
										}
									}
								});
						}
					},
					error: function (res) {
						if(onError && typeof(onError) == "function"){
							onError(res);
						}
					}
				});
			},
			assignToReviewerConfirm: function(param, targetNode){
				var popper    = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				targetNode    = kriya.xpath(nodeXpath);
				
				var userEmail = $(targetNode).closest('tr').find('.reviewer-email').text();
				var surName  = $(targetNode).closest('tr').find('.reviewer-surname').text();
				var givenName  = $(targetNode).closest('tr').find('.reviewer-given-name').text();
				var versionNumber = $('.versionMenu .current-version').text().trim();
				
				$(targetNode).closest('td').find('.assignReviewerBtn').text('Assigning...');
				$(targetNode).closest('td').find('.assignReviewerBtn').addClass('disabled');
				popper.progress('Assigning...');
				
				var assigned = $(targetNode).closest('tr').find('.reviewer-article-assigned').text();
				var parameters = {
					'userEmail' : userEmail,
					'surName'   : surName,
					'givenName' : givenName,
					'assigned'  : assigned,
					'popper'    : popper
				};
				eventHandler.components.general.assignToReviewer(parameters,'', function(){
					$(targetNode).closest('td').find('.assignReviewerBtn').html('<i class="material-icons" style="line-height: inherit;color: green;">check_circle</i> Assigned');
					var currentUserEmail = $('.userProfile .username').attr('data-user-email');
					var currentUserName = $('.userProfile .username').text();
					$(targetNode).closest('td').prepend('<span class="btn btn-small blue lighten-1 followup" data-message="{\'click\':{\'funcToCall\': \'getEmailTemplateToEdit\',\'param\': {\'confirmMsg\': {\'click\':{\'funcToCall\': \'followupConfirm\',\'channel\':\'components\',\'topic\':\'general\'}}, \'userMailID\': \'' + currentUserEmail + '\', \'userName\': \'' + currentUserName + '\'}, \'channel\':\'components\',\'topic\':\'general\'}}" data-currStage="associateediter-assign-reviewer" data-work-name="followupReviewer">Follow&nbsp;Up</span>');
					assigned = (assigned)?(parseInt(assigned)+1):1;
					$(targetNode).closest('tr').find('.reviewer-article-assigned').text(assigned);
					popper.progress('Assigning...', 'true');
					kriya.popUp.closePopUps(popper);
				},function(err){
					kriya.notification({
						title: 'Failed',
						type: 'error',
						content: 'Assign to reviewer was failed.',
						icon: 'icon-warning2'
					});
				});
			},
			commentCollapsible: function(param, targetNode){
				var targetBody = $(targetNode).closest('li').find(' > .collapsible-body');
				var targetHead = $(targetNode).closest('li').find(' > .collapsible-header');
				$(targetNode).closest('ul').find(' > li .collapsible-body').not(targetBody).css('display', 'none');
				$(targetNode).closest('ul').find(' > li .collapsible-header').not(targetHead).find('.collap-icon').text('keyboard_arrow_right');

				if(targetBody.is(':visible')){
					targetHead.find('.collap-icon').text('keyboard_arrow_right');
				}else{
					targetHead.find('.collap-icon').text('keyboard_arrow_down');
				}
				
				targetBody.animate({
					height: "toggle"
				  }, 500);
			},
			viewReviewDetails: function(param, targetNode){
				var range = rangy.getSelection();
				kriya.selection = range.getRangeAt(0);
				$('[data-component="reviewer_details"]').addClass('hidden');
				$('.la-container').fadeIn();
				$.ajax({
					type : "GET",
					url  : "/api/getuserdata?format=xml&userEmail=" + $(targetNode).closest('li').attr('data-reviewer-email'),
					success: function (res) {
						$('.la-container').fadeOut();
						if(res){
							res = res.replace(/<(\d+)>/g, '<user>');
							res = res.replace(/<\/(\d+)>/g, '</user>');
							var userObj = $('<users>' + res + '</users>');
							if(userObj.find('user').length > 0){
								$('[data-component="reviewer_details"] .firstName').html(userObj.find('user:first name first').text());
								$('[data-component="reviewer_details"] .lastName').html(userObj.find('user:first name last').text());
								$('[data-component="reviewer_details"] .reviewer-email').html(userObj.find('user:first email').text());
								
								$('[data-component="reviewer_details"] .reviewer-institution').html(userObj.find('user:first aff institution').text());
								$('[data-component="reviewer_details"] .reviewer-city').html(userObj.find('user:first aff city').text());
								$('[data-component="reviewer_details"] .reviewer-country').html(userObj.find('user:first aff country').text());

								var assigned = userObj.find('user:first reviewer-status article').length;
								var completed = userObj.find('user:first reviewer-status  article[review-status="completed"]').length;

								$('[data-component="reviewer_details"] .no-reviewed').html(assigned);
								$('[data-component="reviewer_details"] .no-accpedted').html(completed);

								kriya['styles'].init($(targetNode), "reviewer_details");
								var cp = kriya.pageXaxis - ($('[data-component="reviewer_details"]').outerWidth()/2);
								if (cp < $(kriya.config.containerElm).offset().left) cp = $(kriya.config.containerElm).offset().left;	
								$('[data-component="reviewer_details"]').css('left', cp);
								var acp = kriya.pageXaxis - cp - ($('[data-component="reviewer_details"] .material-icons.arrow-denoter').width()/2);
								$('[data-component="reviewer_details"] .material-icons.arrow-denoter').css('left', acp);
							}else{
								kriya.notification({
									title: 'Information',
									type: 'success',
									content: 'Reviewer details not found.',
									icon: 'icon-info'
								});
							}
						}
					},
					error: function (res) {
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Failed to get the reviewer details.',
							icon: 'icon-warning2'
						});
					}
				});
			},
			validateElements: function(param, targetNode){
				var arrayList = ['jrnlArtTitle', 'jrnlAuthors', 'jrnlAff', 'jrnlAbsPara'];
				var eleName = [];
				for (var a = 0;a<arrayList.length;a++){
					var eleClass = arrayList[a];
					if($(kriya.config.containerElm).find('.' + eleClass).length == 0){
						var b = (eleClass == 'jrnlArtTitle')?'Article Title':(eleClass == 'jrnlAuthors')?'Author':(eleClass == 'jrnlAff')?'Affiliation':(eleClass == 'jrnlAbsPara')?'Abstracts':'';
						eleName.push(b);
					}
				}
				
				if(eleName.length > 0){
					var msg = (eleName.length == 1)?'Please identify ':'Please identify the following Elements : ';
					msg = msg + eleName.join(', ');
					kriya.notification({
						title   : 'Information',
						type    : 'warning',
						content :  msg,
						icon    : 'icon-info'
					});
					return false;
				}
				return true;
			}
		},
		PopUp: {
			addPopUp: function (param, targetNode) {
				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				$('.activeElement').removeClass('activeElement');
				kriya.config.activeElements = false;

				var componentType = param.component + '_edit';
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				$(component).find('*[data-if-selector].hidden').removeClass('hidden');
				$(component).find('*[remove-class]').each(function () {
					var className = $(this).attr('remove-class');
					$(this).removeClass(className);
				})
				$(component).find('input').prop('checked', false);
				var ele = $(targetNode).parent().find('.' + param.component);
				if (ele.length == 0) {
					ele = $(kriya.config.containerElm + ' .' + param.component);
				}
				if (ele.length == 0) {
					ele = $(kriya.config.containerElm);
				}

				//when related add show the first if selector
				if (param.component == "jrnlRelArt") {
					$(component).find('*[data-if-selector]').addClass('hidden');
					var relArtType = $(component).find('select[data-class="RelType"]').val();
					if (relArtType) {
						$(component).find('*[data-if-selector][data-rel-art-type="' + relArtType + '"]').removeClass('hidden');
					} else {
						$(component).find('*[data-if-selector]:first').removeClass('hidden');
					}
				}
				if ($(component).attr('data-component') == "jrnlAuthorGroup_edit") {
					$(component).find('span[data-type="AuthorBioImage"]').children('img').remove();
				}
				kriya.popUp.showEmptyPopUp(component, ele.first());
				$(component).find('[data-wrapper]').scrollTop(0)

				if (targetNode.hasClass('btn')) {
					var nodeXpath = kriya.getElementXPath(targetNode.parent());
					component.attr('data-node-xpath', nodeXpath);
				}
				$(component).find('[data-error]').removeAttr('data-error');
				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="new"]').removeClass('hidden');

				//if related article popup is opened then show the search button and hide reset button
				if (componentType == "jrnlRelArt_edit") {
					$(component).find('.searchRel').removeClass('hidden');
					$(component).find('.resetRel').addClass('hidden');
				}

				/*Start - Issue ID - #113 - Dhatshayani . D - Nov 25, 2017 - To add attribute "data-tmp-selector" in the compoenet to select the appropriate template of footnote */
				if (param.callback && param.callback != '') {
					var execFn = getStringObj(param.callback);
					execFn(param, targetNode);
					//eventHandler.components.PopUp.addDataTempSelector(param, targetNode);
				}/*End Issue ID - #113 */
			},
			showPopUp: function (param, targetNode) {
				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				$('.activeElement').removeClass('activeElement');
				kriya.config.activeElements = false;
				var componentType = param.component;
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				var ele = $(targetNode);
				if (param && param.container) {
					ele = kriya.xpath(param.container, targetNode);
					ele = $(ele);
					kriya.config.popUpContainer = ele;
				}
				if (param && param.sourceSelector && componentType == "modifyOrder_edit") {
					$(component).find('.collection').attr('data-source-selector', param.sourceSelector);
				}

				kriya['styles'].init(ele, componentType);
				// add manualSave class to component to call converttemplate - priya #180
				if ($(targetNode).attr("data-add-saveclass") != undefined) {
					$('[data-type="popUp"]').addClass($(targetNode).attr("data-add-saveclass"));
				}
				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="new"]').removeClass('hidden');

				//If proof control component is open then set the val of selected control
				if (componentType == "proofControls") {
					var selectedType = $(component).find('#proofControlAction').find(':selected').val();
					var controlAttr = $(component).find('#proofControlAction').find(':selected').attr('data-attr');
					if (controlAttr) {
						var val = $(kriya.selection.startContainer).closest('[' + controlAttr + ']').attr(controlAttr);
						val = parseFloat(val);
						if (selectedType == "splCharacterStyle") {
							val = $(kriya.selection.startContainer).closest('span[' + controlAttr + ']').attr(controlAttr);
						} else if (selectedType == "splParagraphStyle") {
							val = $(kriya.selection.startContainer).closest('p[' + controlAttr + ']').attr(controlAttr);
						} else if (selectedType == "removeParaIndent") {
							if ($(kriya.selection.startContainer).closest('[' + controlAttr + ']').length) {
								val = "Indent removed for this paragraph";
							}
						}
						if (val) {
							if (controlAttr == "data-spl-style" || controlAttr == "data-no-indent") {
								$(component).find('.splStyle').val(val);
							}
							else {
								$(component).find('.controlVal').val(val);
							}
						}
					}
				}
			},
			deletePopUp: function (param, targetNode) {
				var paramString = (param) ? "'param':" + JSON.stringify(param) : '';
				var deleteFunction = (param && param.delFunc) ? param.delFunc : 'deleteElement';
				var nodeXpath = $(targetNode).closest('[data-node-xpath]').attr('data-node-xpath');
				if (!kriya.config.activeElements || targetNode.hasClass('removeNode')) {
					kriya.config.activeElements = targetNode.parent();
				}

				//prasent address deletion not saving-kirankumar
				if (targetNode.closest('[data-fn-type][id]').length > 0) {
					kriya.config.activeElements = targetNode.closest('[data-fn-type]');
				}

				//To check replied query; If data-replied is false then not allowed to delete.
				var isCheck = true;
				$(kriya.config.activeElements).find('.jrnlQueryRef').each(function () {
					if ($(this)[0].hasAttribute('data-replied') == false) {
						isCheck = false;
						return;
					}
				});
				if (isCheck == false) {
					var message = '<div class="row">Please ensure that you have replied to the quries raised before you can perform this action.</div>';
					kriya.notification({
						title: 'Information',
						type: 'success',
						content: message,
						icon: 'icon-info'
					});
					return;
				}
				var attrObj = {};
				var message = "Are you sure you want to delete?";
				if (nodeXpath) {
					var selectorNode = kriya.xpath(nodeXpath);
					if ($(selectorNode).closest('[class^="jrnl"][class*="Block"]').length > 0 && $(selectorNode).closest('.jrnlInlineFigure').length == 0) {
						var blockNode = $(selectorNode).closest('[class^="jrnl"][class*="Block"]');
						var label = blockNode.find('.label:first');
						if (label.length > 0) {
							label = label.clone(true).cleanTrackChanges().text();
							message = "Are you sure you want to delete " + label + "? Deleting " + label + " would also remove all citations associated with it.";
						}
					} else if ($(selectorNode).closest('.jrnlRefText').length > 0) {
						var refNode = $(selectorNode).closest('.jrnlRefText');
						var refId = refNode.attr('data-id');
						message = "Are you sure you want to delete the reference" + refId + "? Deleting " + refID + " would also remove all citations associated with it.";
					}
					attrObj['data-node-xpath'] = nodeXpath;
				}
				var settings = {
					'icon': '<i class="material-icons">warning</i>',
					'title': 'Confirmation Needed',
					'text': message,
					'size': 'pop-sm',
					'buttons': {
						'cancel': {
							'text': 'No',
							'class': 'btn-danger pull-right',
							'attr': attrObj,
							'message': "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok': {
							'text': 'Yes',
							'class': 'btn-success pull-right',
							'attr': attrObj,
							'message': "{'click':{'funcToCall': '" + deleteFunction + "','channel':'components','topic':'general'," + paramString + "}}"
						}
					}
				}
				kriya.actions.confirmation(settings);
			},
			deleteConfirmation: function (param, targetNode) {
				var rid = targetNode.parent().attr('data-rid');
				var dataClass = targetNode.closest('ul').attr('data-class');
				var className = targetNode.closest('[data-type="popUp"]').attr('data-class');
				var ridCount = 0;
				$('.' + className).find('.' + dataClass).each(function (i) {
					var ridArray = $(this).attr('data-rid').split(', ');
					if (ridArray.indexOf(rid) != -1) {
						ridCount = ridCount + 1;
					}
				});
				if (ridCount > 1) {
					var message = "Are you sure you want to remove from this author?";
				} else {
					var message = "Are you sure you want to delete?";
				}
				var nodeXpath = kriya.getElementXPath(targetNode.parent());
				var attrObj = {};
				attrObj['data-node-xpath'] = nodeXpath;
				var settings = {
					'icon': '<i class="material-icons">warning</i>',
					'title': 'Confirmation Needed',
					'text': message,
					'size': 'pop-sm',
					'buttons': {
						'cancel': {
							'text': 'No',
							'class': 'btn-danger pull-right',
							'attr': attrObj,
							'message': "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok': {
							'text': 'Yes',
							'class': 'btn-success pull-right',
							'attr': attrObj,
							'message': "{'click':{'funcToCall': 'removeLinks','channel':'components','topic':'general'}}"
						}
					}
				}
				kriya.actions.confirmation(settings, targetNode);
			},
			editPopUp: function (param, targetNode) {
				var popper = $(targetNode).closest('[data-component]');
				var componentName = (param && param.component) ? param.component : $(targetNode).closest("[data-component]").attr('data-component') + '_edit';
				var editElement = false;
				if (popper.length > 0 && popper[0].hasAttribute('data-node-xpath')) {
					var nodeXpath = popper.attr('data-node-xpath');
					editElement = kriya.xpath(nodeXpath);
				} else if ($(targetNode).attr('data-selector') != undefined) {
					var nodeXpath = $(targetNode).attr('data-selector');
					editElement = kriya.xpath(nodeXpath, $(targetNode));
				} else if (param && param.id) {
					editElement = $('#' + param.id);
					// to re-assign image as active element while replacing image - jai
					kriya.config.activeElements = editElement;
					$('.activeElement').removeClass('activeElement');
					kriya.config.activeElements.addClass('activeElement');
				}

				//If it is a block element like figure block table block
				//Don't get block if the edit element is inline image -jagan , 31/01/18
				if ($(editElement).closest('[data-id^="BLK_"]').length > 0 && !$(editElement).hasClass('jrnlInlineFigure')) {
					editElement = $(editElement).closest('[data-id^="BLK_"]');
					kriya.config.popUpContainer = editElement;
				}

				var component = $("*[data-type='popUp'][data-component='" + componentName + "']");
				// Start Issue Id - #448 - Dhatshayani . D Jan 10, 2018 - Change the component type based on the type of figure.
				if (component && component.length > 0 && kriya.config.activeElements && $(kriya.config.activeElements)[0].nodeName == 'IMG' && $(kriya.config.activeElements)[0].hasAttribute('class') && $(kriya.config.activeElements)[0].getAttribute('class').match(/figure/ig)) {
					eventHandler.components.PopUp.changeFigureType(component, kriya.config.activeElements);
				} else if (component && component.length > 0 && targetNode[0].hasAttribute('data-change-type')) {
					var currActiveElements = $(editElement).find('.jrnlFigure:not([data-track="del"])');
					eventHandler.components.PopUp.changeFigureType(component, currActiveElements);
				}// End Issue Id - #448
				if (kriya.isUndefined(kriya.componentList[componentName]) && $(editElement).length > 0) {
					//kriya['styles'].init($(kriya.config.activeElements), componentName);
					kriya['styles'].init($(editElement), componentName);
				}

				//Change the popup heading
				/*#174 -While editing the existing table the column and row value is set as default- Prabakaran.A-Focusteam */
				//commented by jai as this requirement is no longer needed
				//$(component).find('.addColumnTemplate').val((($(component).find('tr>th').length + $(component).find('tr>td').length) / $(component).find('tr').length));
				//$(component).find('.addRowTemplate').val($(component).find('tr').length);
				/*end of the code #174--While editing the existing table the column and row value is set as default- Prabakaran.A-Focusteam*/

				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="edit"]').removeClass('hidden');
			},
			editSubPopUp: function (param, targetNode) {
				var popper = $(targetNode).closest("[data-component]");
				var componentName = (param && param.component) ? param.component : '';
				if (kriya.isUndefined(kriya.componentList[componentName])) {
					//kriya.config.popUpContainer = $(targetNode).closest("li").find('[data-input="true"]');
					$(targetNode).attr('data-for-edit', 'true');
					var component = $('[data-type="popUp"][data-component="' + componentName + '"]');
					component.attr('data-sub-type', 'true');

					kriya.config.popUpContainer = $(targetNode);
					kriya['styles'].init(kriya.config.popUpContainer, componentName);
					kriya.config.subPopUpCards = $('<div />');
					//If popup is a edit popup
					if ($(targetNode).html() != "") {
						$(component).find('[data-pop-type]').addClass('hidden');
						$(component).find('[data-pop-type="edit"]').removeClass('hidden');
					} else {
						$(component).find('[data-pop-type]').addClass('hidden');
						$(component).find('[data-pop-type="new"]').removeClass('hidden');
					}
				}
			},
			closePopUp: function (param, targetNode) {
				var popper = $(targetNode).closest("[data-component]");
				kriya.popUp.closePopUps(popper);
				if (popper.attr('data-component') == 'QUERY') {
					$('[data-component="QUERY"]').removeAttr('data-display').removeAttr('data-sub-type').removeAttr('style');
				}
				if (popper.attr('data-component') == 'validateRef_edit') {
					popper.find('.validateAllBtn').removeClass('disabled');
				}
				popper.find('[data-type="dropDown"]').addClass("hidden"); //hide the dropdown in component
			},
			saveComponent: function (param, targetNode) {
				var comp = $(targetNode).closest('[data-type="popUp"]');
				/*var elm;
				if(comp.find('*[data-for-edit]').length > 0){
					elm = comp.find('*[data-for-edit]').closest('li');
				}*/
				if ($('[data-type="popUp"]:visible').length > 1 && comp.find('*[data-for-edit]').length == 0 && comp.attr('data-class') != 'jrnlCorrAff') {
					if (kriya.saveComponent.validateComponents(comp[0])) {
						if (comp.attr('data-class') == 'jrnlGroupAuthor') {
							var savedNode = kriya.saveComponent.init(comp[0]);
						} else {
							var savedNode = kriya.saveComponent.addNewComponent(comp[0]);
						}
					}
				} else if ($('[data-type="popUp"]:visible').find('.authorReorder').length > 0) {
					var savedNode = kriya.saveComponent.init(comp[0], $(kriyaEditor.settings.contentNode).find('.jrnlAuthors')[0]);
				} else if (kriya.config.popUpContainer) {
					//To save the compoenet Ex: edit the affiliation from the author popup or figure
					var savedNode = kriya.saveComponent.init(comp[0], kriya.config.popUpContainer);
					if (savedNode) {
						kriya.config.popUpContainer = null;
						kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
						//If the save node is content element then return false don't add undo level
						if ($(savedNode).closest(kriya.config.containerElm).length == 0) {
							return false;
						}
					}
				} else {
					var savedNode = kriya.saveComponent.init(comp[0]);
					//Trigger the citation reorder after inserting the figure,table etc.
					/*if($(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]').length > 0){
						var reorderNode = $(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]');
						reorderNode.removeAttr('citation-reorder');
						//kriya.general.triggerCitationReorder($(reorderNode), 'insert');
						citeJS.general.updateStatus();
					}*/
				}
				//callback functions - jai 21-08-2017
				if (savedNode && comp[0].hasAttribute('data-callback')) {
					var execFn = getStringObj(comp.attr('data-callback'));
					execFn(savedNode);
				}

				//Remove the empty related article author - jagan
				if ($(comp).attr('data-component') == "jrnlRelArt_edit") {
					$(savedNode).find('.jrnlRelArtAuthorGroup,.jrnlRelArtAuthor').each(function () {
						if ($(this).text() == "") {
							$(this).remove();
						}
					});
				}
				//Remove the empty name in product info - aravind
				if ($(comp).attr('data-component') == "jrnlProduct_edit") {
					$(savedNode).find('.jrnlProNameGroup,.jrnlProName').each(function () {
						if (!(/^[a-zA-Z]/).test($(this).text())) {
							$(this).remove();
						}
					});
				}

				//Add id to the save for closest save node of newly added node - jagan
				if (savedNode && $(savedNode).closest('[data-save-node]:not([id])').length > 0) {
					$(savedNode).closest('[data-save-node]:not([id])').attr('id', uuid.v4());
				}


				// sorting abbreviations - priya #180
				if (savedNode && comp[0].hasAttribute('data-sort')) {
					var divList = $("." + comp.attr('data-class'));
					/*var sortedDivs = divList.sort(function (a, b) {
						return String.prototype.localeCompare.call($(a).find("."+comp.attr('data-sort')).text().toLowerCase() , $(b).find("."+comp.attr('data-sort')).text().toLowerCase());
					});*/
					// Sorting ascending order from filterArray value - rajesh
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;
					function sortAlphaNum(a, b) {
						a = $(a).find("." + comp.attr('data-sort')).text()
						b = $(b).find("." + comp.attr('data-sort')).text()
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					var sortedDivs = divList.sort(sortAlphaNum);
					var parentNode = $("." + comp.attr('data-class')).parent();
					if (parentNode.length > 0) {
						$(parentNode).find("." + comp.attr('data-class')).remove();
						$(parentNode).append(sortedDivs);
						savedNode = parentNode;
					}
				}
				if (kriya.config.subPopUpCards && $(kriya.config.subPopUpCards).find('.historyCard').length > 0) {
					$(kriya.config.subPopUpCards).find('.historyCard').each(function () {
						$('#historyDivNode').prepend(this);
						kriyaEditor.settings.undoStack.push(this);
						kriya.config.subPopUpCards = undefined;
					});
				}
				if (savedNode && savedNode != true && $(savedNode).length > 0 && $(savedNode)[0].hasAttribute('data-id') && $(savedNode)[0].hasAttribute('data-inserted')) {
					var dataId = $(savedNode).attr('data-id');


					//#274 - Inline table Insertion: Citation popup must not be shown after insertion -Helen.J(helen.j@focusite.com)
					//#274 - Removing and adding citation after a table insert -Helen.J(helen.j@focusite.com)
					if (dataId.match(/^BLK_/) && savedNode.attr('insert-citation') == "true") {
						var newFlatID = $(savedNode).find('.label').attr('data-block-id');
						if (newFlatID) {
							$(savedNode).find('.label').removeAttr('data-block-id');
							savedNode.attr('data-id', 'BLK_' + newFlatID);
							savedNode.find('[class*="Caption"]').attr('data-id', newFlatID);
						}
						//End of #274
						$(savedNode)[0].scrollIntoView();
						kriya.general.updateRightNavPanel(dataId, kriya.config.containerElm);
						var floatLabel = $(savedNode).find('.label').text();
						floatLabel = floatLabel.replace(/([\.\,\:]+)$/, '');
						var savedXpath = kriya.getElementXPath($(savedNode)[0]);
						var message = '<div class="row">' + floatLabel + ' has been inserted. Please inset a citation for ' + floatLabel + ', as all Objects must have at least one citation in the text. Please note that our system bases Figure/Table/Video/Supplementary file numbering on where they are first cited in the text.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
						kriya.notification({
							title: 'Information',
							type: 'success',
							content: message,
							icon: 'icon-info'
						});
					} else if (dataId.match(/^FN/)) {
						//Scroll to inserted foot note
						$(savedNode)[0].scrollIntoView();
					}
					else if ($(savedNode).attr('class') == "jrnlAppBlock") {
						//Scroll to inserted foot note
						$(savedNode)[0].scrollIntoView();
					}
				}

				if (TomloprodModal.isOpen) {
					TomloprodModal.closeModal();
				}

				if (savedNode) {
					kriya.popUp.closePopUps(comp);
					$(comp).find('[data-type="dropDown"]').addClass("hidden"); //hide the dropdown in component
					//Dont save the content when group author is added in author popup
					if (comp.attr('data-class') == 'jrnlGroupAuthor' && $(comp).hasClass('manualSave') && $('[data-component]:visible').length > 1) {
						return false;
					}
					if (savedNode != true) {
						kriyaEditor.settings.undoStack.push(savedNode);
						var saveNodesList = kriyaEditor.settings.undoStack;
						kriyaEditor.init.addUndoLevel('save-component');
						callUpdateArticleCitation($(saveNodesList), '.jrnlCitation');
					}
				}
				//added by kiran:-when insert new image uncited list count not updated
				citeJS.general.updateFloatDetails();
			},
			attachFileTrigger: function(param, targetNode){
				$(targetNode).parent().find('[type="file"]').val('');
				$(targetNode).parent().find('[type="file"]').trigger('click');
			},
			removeAttachment: function(param, targetNode){
				$(targetNode).closest('.card').remove();
			},
			changeAttachFile: function(param, targetNode){
				var files = $(targetNode)[0].files;
				//return false;
				if(files.length > 0){
					var blockNode = $(targetNode).closest('[data-component]');
					blockNode.progress('Uploading');
					var ouputLen = 0;
					for(var f=0;f<files.length;f++){
						var fileObj = files[f];
						var fileSize = fileObj.size/1000/1000;
						if(fileSize > 20){
							kriya.notification({
								title: 'Warning',
								type: 'warning',
								content: fileObj.name + ' file is too large. Please upload file with less than 20MB size.',
								icon : 'icon-info'
							});		
							continue;
						}
						var param = {
							'file': fileObj,
							'convert': 'false',
							'success': function(res){
								ouputLen++;
								var fileDetailsObj = res.fileDetailsObj;
								var fileName = fileDetailsObj.name+fileDetailsObj.extn;
								var filePath = window.location.origin  + '/resources/' + fileDetailsObj.dstKey;
								var attachCard = $('<div class="card col s3" data-file-type="' + fileObj.type + '" data-file-path="' + filePath + '" data-file-name="' + fileName + '"><div class="card-content"><p title="' + fileName + '">' + fileName + ' <i class="close material-icons" style="float: right;line-height: inherit;cursor: pointer;" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'removeAttachment\'}">close</i></p></div></div>');
								blockNode.find('.attachementContainer').append(attachCard);
								if(ouputLen == files.length){
									blockNode.progress('Uploaded', true);
								}
							},
							'error': function(err){
								ouputLen++;
								//Error callback function remove the progress in blocknode
								if(ouputLen == files.length){
									blockNode.progress('', true);
								}
								//Show the error notification
								kriya.notification({
									title: 'Failed',
									type: 'error',
									content: 'Uploading file failed.',
									timeout: 8000,
									icon : 'icon-info'
								});
							}
						}
						eventHandler.menu.upload_new.uploadFile(param, targetNode);
					}
					
				}else{
					kriya.notification({
						title: 'Failed',
						type: 'error',
						content: 'Invalid file.',
						timeout: 8000,
						icon : 'icon-info'
					});
				}
			},
			displayReviewArticle: function(param, targetNode){
				var articleIds = $(targetNode).attr('data-article-id');
				articleIds = articleIds.split(',');
				var popper = $('[data-component="display_reviewe_article_edit"]');
				kriya.popUp.openPopUps(popper);
				popper.find('tbody').html('');
				popper.progress('Fetching');
				if($(targetNode).attr('data-article-id')){
					var reLen = 0;
					for(var a=0; a<articleIds.length;a++){
						var artDOI = articleIds[a];
						$.ajax({
							type: "GET",
							url: '/api/getdata?customer=' + kriya.config.content.customer + '&project=' + kriya.config.content.project + '&doi=' + artDOI + '&xpath=' + '//body/div[@class="front"]/*[@class="jrnlArtTitle"] | //article-meta/article-id[@pub-id-type="doi"]',
							success: function (res) {
								if(res){
									reLen++;
									var artTitle = $('<res>' + res + '</res>').find('.jrnlArtTitle').html();
									var doi = $('<res>' + res + '</res>').find('article-id').html();
									popper.find('tbody').append('<tr><td style="text-align: center;">' + (popper.find('tbody tr').length+1) + '</td><td style="text-align: center;">' + doi + '</td><td> ' + artTitle + ' </td></tr>');
									if(reLen == articleIds.length){
										popper.progress('Fetched', true);
									}
								}
							},
							error: function(er){
								reLen++;
								if(reLen == articleIds.length){
									popper.progress('Fetched', true);
								}
							}
						});
					}
				}else{
					popper.find('tbody').append('<tr><td colspan="4" style="text-align: center;">No Data Found.</td></tr>');
					popper.progress('Fetched', true);
				}
			}
		},
		query: {
			alignQuery: function (queryNode) {
				var queryId = $(queryNode).attr('data-rid');
				var topOffset = $(queryNode).position().top;
				if (queryId != undefined) {
					var currQueryDiv = $('#queryDivNode').find('[id=' + queryId + ']');
					if (currQueryDiv && $(currQueryDiv).prevAll().length > 0) {
						$(currQueryDiv).prevAll().each(function () {
							topOffset = topOffset - $(this).height();
						});
					}
					$(currQueryDiv).css('top', topOffset);
				}
			}
		}
	};
	eventHandler.menu = {
		upload_new: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				//If the param has block node then show the progess bar on the block
				if(block){
					block.progress('Uploading to server');
				}
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {'filename': file.name},
					success: function(response){
						if (response && response.upload_url){
							var formData = new FormData();
							$.each(response.params, function(k, v){
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.menu.upload_new.uploadToServer(param, targetNode);
						}
					},
					error: function(err){
						if(param.error && typeof(param.error) == "function"){
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						return xhr;
					},
					success: function(res){
						if(block){
							block.progress('Converting for web view');
						}
						if (res && res.hasChildNodes()){
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
										srcBucket: uploadResult.find('Bucket,bucket').text(),
										srcKey: uploadResult.find('Key,key').text(),
										dstBucket: "kriya-resources-bucket",
										dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
										convert: 'true'
									}
								if(param.convert){
									params.convert = param.convert;
								}
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function(status){
										//Call the sucess function if req was successfully completed
										if(param.success && typeof(param.success) == "function"){
											param.success(status);
										}
									},
									error: function(err){
										//Call the error function if req failed
										if(param.error && typeof(param.error) == "function"){
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			}
		},
		upload: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				var ext = file.name.replace(/^.*\./,'');
				if (/^(xml|html?)$/.test(ext)){
					kriya.notification({
						title: 'ERROR',
						type: 'error',
						content: 'Not a valid file to upload',
						icon: 'icon-warning2'
					});
					return true;
				}
				var block = param.block;
				//If the param has block node then show the progess bar on the block
				if(block){
					block.progress('Uploading to server');
				}
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {'filename': file.name},
					success: function(response){
						if (response && response.upload_url){
							var formData = new FormData();
							$.each(response.params, function(k, v){
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.menu.upload.uploadToServer(param, targetNode);
						}
					},
					error: function(err){
						if(param.error && typeof(param.error) == "function"){
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						return xhr;
					},
					success: function(res){
						if(block){
							block.progress('Converting for web view');
						}
						if (res && res.hasChildNodes()){
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
										srcBucket: uploadResult.find('Bucket,bucket').text(),
										srcKey: uploadResult.find('Key,key').text(),
										dstBucket: "kriya-resources-bucket",
										dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
										convert: 'true'
									}
								if(param.convert){
									params.convert = param.convert;
								}
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function(status){
										//Call the sucess function if req was successfully completed
										var fileDetailsObj = status.fileDetailsObj;
										if (/docx?/i.test(status.fileDetailsObj.extn)){
											var parameters = {'customer': kriya.config.content.customer, 'project': kriya.config.content.project, 'doi': kriya.config.content.doi, 'key': status.fileDetailsObj.dstKey, 'service': 'doctohtml', 'data': fileDetailsObj}
											$.ajax({
												type: "POST",
												url: "/api/uploaddigest",
												data: parameters,
												success: function (msg) {
													eventHandler.menu.upload.getJobStatus(kriya.config.content.customer, msg.message.jobid, block, 'exeter', 'typesetter', kriya.config.content.doi, {'onsuccess': 'eventHandler.components.general.replaceDoc'});
												},
												error: function(xhr, errorType, exception) {
													$('.la-container').fadeOut()
													return null;
												}
											});
										}else{
											$(targetNode).attr('src', '/resources/' + fileDetailsObj.dstKey);
											$(targetNode).attr('data-href', fileDetailsObj.dstKey);
											$(targetNode).attr('data-extension', status.fileDetailsObj.extn);
											$(targetNode).attr('data-name', status.fileDetailsObj.name);
											if(param.success && typeof(param.success) == "function"){
												param.success(status);
											}
										}
									},
									error: function(err){
										//Call the error function if req failed
										if(param.error && typeof(param.error) == "function"){
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			},
			getJobStatus: function(customer, jobID, notificationID, userName, userRole, doi, callback){
				/***
				**	to get status of a job from job mananger
				**	optional : can send callback as an object {'process': 'somefunction', 'onfailure': 'anotherfunction', 'onsucess': 'successfunction'}, with which the status will be returned to that function according to the status
				***/
				if (callback == undefined) callback = false;
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.status){
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if(data.status.message.stage.current){
							currStep = data.status.message.stage.current;
							}    
							var loglen = data.status.message.log.length;    
							var process = data.status.message.log[loglen-1];
							if (/completed/i.test(status)){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onsuccess){
									var functionArray = callback.onsuccess.split('.');
									if (functionArray.length > 3 && typeof(window[functionArray[0]][functionArray[1]][functionArray[2]][functionArray[3]]) == "function")
									window[functionArray[0]][functionArray[1]][functionArray[2]][functionArray[3]](customer, jobID, notificationID, doi, data.status.message);
								}
							}else if (/failed/i.test(status) || code != '200'){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onfailure && typeof(window[callback.onfailure]) == "function"){
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							}else if (/no job found/i.test(status)){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onfailure && typeof(window[callback.onfailure]) == "function"){
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							}else{
								if (process != '') $('.notify').html(process);
								if (notificationID) notificationID.progress(process);
								if(callback && callback.process && typeof(window[callback.process]) == "function"){
									window[callback.process](customer, jobID, notificationID, doi, data.status.message);
								}else{
									setTimeout(function() {
										eventHandler.menu.upload.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback);
									}, 1000 );
								}
							}
						}
						else{
							$('.notify').html('Failed');
						}
					},
					error: function(error){
						if(error.status == 502){
							setTimeout(function() {
								eventHandler.menu.upload.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback);
							}, 1000 );
						}
					}
				});
			},
		},
		edit: {
			addUndoModal: function(message){
				if (message != undefined){
					$('.undo-modal').remove();
					$('body').append('<div class="undo-modal">' + message + ' <span data-channel="menu" data-topic="edit" data-event="click" data-message="{\'funcToCall\': \'undoLastAction\'}">Undo</span></div>');
					setTimeout(function(){
						$('.undo-modal').remove();
					}, 8000);
				}
			},
			showHideMenu: function(param,targetNode){
				//get active nodes and remove class "active"
				if (! $(targetNode).closest('.kriyaMenuControl').hasClass('active')){
					$('.kriyaMenuControl.active').removeClass('active');
					$('[data-menu-selector]').addClass('hidden');
					$('[data-menu-selector]:first').removeClass('hidden');
					if (kriya.selection && $(targetNode).closest('.kriyaMenuControl').find('[data-menu-selector]').length > 0){
						var node = kriya.selection.commonAncestorContainer;
						if ($(node).closest('div.front,div.body,div.back,div.sub-article').length > 0){
							$('[data-menu-selector]').addClass('hidden');
							var blockClass = $(node).closest('div.front,div.body,div.back,div.sub-article').attr('class');
							$('*[data-menu-selector*=" ' + blockClass + ' "]').removeClass('hidden');
						}
					}
					$(targetNode).closest('.kriyaMenuControl').addClass('active');
				}else{
					$('.kriyaMenuControl.active').removeClass('active');
				}
			},
			toggleVersion: function(param, targetNode){
					var versionName = $(targetNode).attr('data-version');
					var versionPath = $(targetNode).attr('data-version-path');
					$(targetNode).closest('.versionMenu').find('.kriyaMenuBtn').removeClass('active');
					$(targetNode).addClass('active');

					if($(targetNode).hasClass('current-version')){
							$(kriya.config.containerElm).removeClass('hidden');
							$('#versionContainer').addClass('hidden');							
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version]').addClass('hidden');
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version="' + versionName + '"]').removeClass('hidden');
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-message*="\'mainDoc\':\'true\'"]').trigger('click');
							$('.kriyaMenuContainer [data-name="Styles"]').removeClass('disabled');
							$('#filesContainer .row:has(#hide-to-reviewer)').removeClass('hidden');
					}else{
						if($('#versionContainer .versionDivNode[data-version-name="' + versionName + '"]').length > 0 ){
							$('#versionContainer .versionDivNode').removeClass('active');
							$('#versionContainer .versionDivNode[data-version-name="' + versionName + '"]').addClass('active');
							$(kriya.config.containerElm).addClass('hidden');
							$('#versionContainer').removeClass('hidden');
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version]').addClass('hidden');
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version="' + versionName + '"]').removeClass('hidden');
							$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-message*="\'mainDoc\':\'true\'"]').trigger('click');
							$('.kriyaMenuContainer [data-name="Styles"]').addClass('disabled');
							$('#filesContainer .row:has(#hide-to-reviewer)').addClass('hidden');
						}else{
							$('.la-container').fadeIn();
							$.ajax({
								type    : "GET",
								url     : '/peer_review?doi=' + kriya.config.content.doi + '&customer=' + kriya.config.content.customer + '&project=' + kriya.config.content.project + '&versions=' + versionPath + '&versionNumber=' + versionName,
								success : function(data) {
									$('.la-container').fadeOut();
									var data = $(data);
									
									data.find('#contentDivNode .jrnlReviewersGroup, #contentDivNode .jrnlEditorsGroup').remove();
									$('#versionContainer .versionDivNode').removeClass('active');
									var versionNode = $('<div class="versionDivNode active" data-version-name="' + versionName + '"/>');
									versionNode.html(data.find('#contentDivNode').html());
									$('#versionContainer').append(versionNode);

									$(kriya.config.containerElm).addClass('hidden');
									$('#versionContainer').removeClass('hidden');
									
									$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version]').addClass('hidden');
									$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-file-version="' + versionName + '"]').removeClass('hidden');
									$('.kriyaSubMenuContainer .btnGroupContainer.kriyaTab[data-message*="\'mainDoc\':\'true\'"]').trigger('click');
									$('.kriyaMenuContainer [data-name="Styles"]').addClass('disabled');
									$('#filesContainer .row:has(#hide-to-reviewer)').addClass('hidden');
								},
								error: function(err) {
									$('.la-container').fadeOut();
									kriya.notification({
										title: 'ERROR', 
										type: 'error', 
										content: 'Unable to get version data.', 
										icon: 'icon-warning2' 
									});
								}
							});
						}
					}
			},
			toggleNavPane: function(param,targetNode){
				$('.highlight.findOverlay').remove(); // remove search highlights - priya 
				//get active nodes and remove class "active" 
				$('.nav-action-icon').removeClass('active');
				$('.navDivContent > div').removeClass('active');
				//add "active" class to current nodes
				$('#'+param).addClass('active');
				targetNode.addClass('active');
			},
			hideFileToRole: function(param, targetNode){
				var filePath = $('.kriyaMenuBlock .kriyaSubMenuContainer .btnGroupContainer.active').attr('data-file-path');
				var fileNode = $('#filesContainer .fileListData file[file_path="' + filePath + '"]');
				if(fileNode.length > 0){
					if($(targetNode).is(':checked') == true){
						fileNode.attr('data-hide-to-reviewer', 'true');
					}else if($(targetNode).is(':checked') == false){
						fileNode.removeAttr('data-hide-to-reviewer');
					}
				}
				var parameters = {
					'customer' : kriya.config.content.customer, 
					'project'  : kriya.config.content.project, 
					'doi'      : kriya.config.content.doi, 
					'data'     : '<files>' + $('#filesContainer .fileListData').html() + '</files>',
					'file'     : 'file_list'
				};
				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
					$('.la-container').fadeOut();
				}, function(err){
					$('.la-container').fadeOut();
					kriya.notification({
							title: 'ERROR', 
							type: 'error', 
							content: 'Unable to update file list.', 
							icon: 'icon-warning2' 
						});
				});
			},
			toggleFile: function(param, targetNode){
				$('.kriyaMenuBlock .kriyaSubMenuContainer .btnGroupContainer').removeClass('active');
				$(targetNode).addClass('active');
				if(param.mainDoc == "true"){
					$('#contentContainer').css('display', 'block');
					$('#filesContainer').css('display', 'none');
					$('.kriyaMenuContainer .kriyaMenuBar .kriyaMenuBtn[data-name="Styles"]').removeClass('hidden');
				}else if(param.fileHref){
					$('.kriyaMenuContainer .kriyaMenuBar .kriyaMenuBtn[data-name="Styles"]').addClass('hidden');
					$('#contentContainer').css('display', 'none');
					$('#filesContainer').css('display', 'block');
					
					var iframe = $('#filesContainer iframe');
					iframe.attr('src', param.fileHref);
					var iframeHeight = iframe.closest('.nano').height();
					if($('#hide-to-reviewer:visible').length > 0){
						var rowHeight = $('#hide-to-reviewer').closest('.row').height();
						iframeHeight = iframeHeight-rowHeight;
					}
					iframe.css('height', iframeHeight);

					var filePath = $(targetNode).attr('data-file-path');
					var fileNode = $('#filesContainer .fileListData file[file_path="' + filePath + '"]');
					if(fileNode.attr('data-hide-to-reviewer') == "true"){
						$('#hide-to-reviewer').prop('checked', true);
					}else{
						$('#hide-to-reviewer').prop('checked', false);
					}
				}
			},
			quickDemo: function(param, targetNode){
				if($('#contentContainer').attr('data-version-path')){
					 return false;
				}

				var keyVal = "demo-"+kriya.config.content.doi+'-flag';
				 if(store.get(keyVal)){
					return false;
				 }
				store.set(keyVal, 'true');
				var enjoyhint_instance = new EnjoyHint({
					onStart : function(){
						$('.kriya-collapsible:last > li:has(.collapsible-body:not(:empty)):last .collapsible-body').css('display', 'block');
					}
				});
				var enjoyhint_script_steps = [
				  {
					'next .resubmit-manuscript' : 'Click here to resubmit your manuscript.'
				  },{
						'next .query-reply:visible' : 'Click here to reply for comment.',
						'showSkip' : false,
						'nextButton' : {className: "myNext", text: "Got it!"}
				  }
				];
				enjoyhint_instance.set(enjoyhint_script_steps);
				enjoyhint_instance.run();
			}
		},
		query: {
			deleteQuery: function (param, targetNode) {
				//if the cursor placed at the tracked node
				var trackParent = $(kriya.selection.startContainer).parents('[data-rid *="Q"]');
				var clonedSelection = kriya.selection.cloneContents();
				var selectedQuery = $('[data-rid *= "Q"].selected');
				if (trackParent.length > 0) {
					$('#queryDivNode #' + trackParent.attr('data-rid').replace(/\s+/, '') + ' .query-delete').trigger('click');
				} else if ($(kriya.selection.startContainer).prev('.jrnlQueryRef').length > 0) {
					//if cursor is within the query start and end tag
					var rid = $(kriya.selection.startContainer).prev('.jrnlQueryRef').attr('data-rid').replace(/\s+/, '');
					$('#queryDivNode #' + rid + ' .query-delete').trigger('click');
				} else if ($(clonedSelection).find('[data-rid *= "Q"]').length > 0) {
					//if the track elements with in the content selection
					$(clonedSelection).find('[data-rid *= "Q"]').each(function () {
						$('#queryDivNode #' + $(this).attr('data-rid').replace(/\s+/, '') + ' .query-delete').trigger('click');
					});
				} else if (selectedQuery.length > 0) {
					$('#queryDivNode #' + selectedQuery.attr('data-rid').replace(/\s+/, '') + ' .query-delete').trigger('click');
					this.moveQuery({ 'type': 'next' });
				} else {
					this.moveQuery({ 'type': 'next' });
				}
			},
			moveQuery: function (param, targetNode) {
				var queryEle = $("#queryDivNode .query-div");
				var selectedTrack = queryEle.filter(".selected");
				//if not the last track element
				if (param && param.type == "next" && queryEle.index(selectedTrack) + 1 != queryEle.length && selectedTrack.length > 0) {
					selectEle = queryEle.eq(queryEle.index(selectedTrack) + 1);
				} else if (param && param.type == "prev" && queryEle.index(selectedTrack) != 0 && selectedTrack.length > 0) {
					selectEle = queryEle.eq(queryEle.index(selectedTrack) - 1);
				} else if (param && param.type == "prev") {
					selectEle = queryEle.last();
				} else {
					selectEle = queryEle.first();
				}

				if (selectEle.length > 0) {
					$('#' + selectEle.attr('id')).trigger('click');
				}
			}
		},
		insert: {
			newCite: function (param, targetNode) {
				componentName = param.type;
				if (componentName == "QUERY") {
					$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('data-class-selector', 'default');
					$('[data-component="QUERY"]').find('.queryContent').html('');
					$('[data-component="QUERY"]').find('[data-type="file"]').remove();
					$('[data-component="QUERY"]').removeAttr('data-display').removeAttr('data-sub-type').removeAttr('style');
					$('[data-component="QUERY"]').find("#queryPerson option").removeClass("hidden")
					$('[data-component="QUERY"]').find("#queryPerson option").each(function () {
						if ($(this).attr("value").toLowerCase() == kriya.config.content.role.toLowerCase()) {
							$(this).addClass("hidden");
						}
					});
				}
				if (targetNode.closest('.query-div').length > 0) {
					var rid = targetNode.closest('.query-div').attr('id');
					if ($(kriya.config.containerElm + ' [data-rid="' + rid + '"]').length > 0) {
						kriya.config.activeElements = $(kriya.config.containerElm + ' [data-rid="' + rid + '"]').parent();
						$(kriya.config.activeElements)[0].scrollIntoView();
					}
				}
				if ($('[data-component]:visible').length > 0) {
					targetNode = kriya.config.activeElements;
					if (!$('[data-component]:visible').hasClass('autoWidth') && componentName == "QUERY") {//to display query pop-up over another pop-up
						$('[data-component="QUERY"]').attr('data-display', "modal");
						$('[data-component="QUERY"]').attr('data-sub-type', $('[data-component]:visible').attr('data-component'))
					}
				}
				kriya['styles'].init($(targetNode), componentName);
				//Added by jagan
				//Scroll to the selected item and select the selected item tab
				if (componentName == "CITATION_edit" && $('[data-component="CITATION_edit"] .selected').length > 0) {
					var carouselID = $('[data-component="CITATION_edit"] .selected').closest('.carousel-item').attr('id');
					var radioBox = $('[data-component="CITATION_edit"] input[type="radio"]#_' + carouselID + '_');
					if (radioBox.length > 0) {
						radioBox[0].checked = true;
					}
					setTimeout(function () {
						$('[data-component="CITATION_edit"] .selected')[0].scrollIntoView();
					}, 1000);
				}
				//Prevent adding footnote citation inside footnote
				if (componentName == "CITATION_edit" && $(kriya.selection.startContainer).closest('.jrnlFootNote').length > 0) {
					$('[data-component="CITATION_edit"] [for="_cite_fn_tab_"]').addClass('hidden');
				}
				if (componentName == "QUERY") {
					$('[data-component="QUERY"]').removeAttr('data-sub-type');
					$('[data-component="QUERY"] .reply-needed input').prop('checked', true);
					$('[data-component="QUERY"] [data-class-selector].selected').removeClass('selected');
					$('[data-component="QUERY"] .tabs .tab[data-type]').removeClass('active');
					$('[data-component="QUERY"] .tabs .tab[data-type="freeform"]').addClass('active');

					$('[data-component="QUERY"] [data-query-type]').addClass('hidden');
					$('[data-component="QUERY"] [data-query-type="freeform"]').removeClass('hidden');
				}
			}
		},
		comments:{
			commentsTab: function(param, targetNode){
				var commentsTabSel = $(targetNode).attr('data-comment-to');
				$(targetNode).closest('.jrnlCommnetsBox').find('.commentsField').attr('data-comment-to',commentsTabSel);
				$(targetNode).parent().find('.selected').removeClass('selected');
				$(targetNode).addClass('selected');
				$('#queryDivNode #openQueryDivNode > div').addClass('hidden');
				$('#queryDivNode #openQueryDivNode #commentsTo'+commentsTabSel).removeClass('hidden');
				//$('#queryDivNode .commentsField .comment-label').remove();
				var toRole = $('#queryDivNode .commentsField').attr('data-comment-to');
				$('#queryDivNode .commentsField .comment-label').html('Comments will be added to ' + toRole);
			},
			commentsField: function(param, targetNode){
				if($(targetNode).closest('[data-comment-to][data-message]').length > 0){
					$(targetNode).closest('[data-comment-to][data-message]').trigger('click');
				}
				$('#queryDivNode .commentsField').removeClass('hidden');
				//$('#queryDivNode .commentsField .comment-label').remove();
				var toRole = $('#queryDivNode .commentsField').attr('data-comment-to');
				$('#queryDivNode .commentsField .comment-label').html('Comments will be added to ' + toRole);
				$('#queryDivNode .commentsField .reply-content').focus();
				//$('#queryDivNode .commentsField .confidentialComment input[type="checkbox"]').prop('checked', false);
			},
			closeCommentsField: function(param, targetNode){
				$('#queryDivNode .commentsField').addClass('hidden');
			},
			changeRadio: function(param, targetNode){
				var commentTo = $(targetNode).attr('data-to-role');
				$(targetNode).closest('.commentsField').attr('data-comment-to', commentTo);
			}
		},
		style: {
			applyFormat: function(param, targetNode){
				if (kriya.selection){
					range = rangy.getSelection();
					range.removeAllRanges();
					range.addRange(kriya.selection);
				}
				//kriya.evt.preventDefault();
				//http://stackoverflow.com/a/4220880/3545167
				//get block node with rangy
				if (param.type == "inline"){
					var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					console.log(range.endContainer);
					if (range.endContainer == range.commonAncestorContainer){
						//If parentNode or comman ancestor is a span and span node is a track change node then wrap the track node with span
						//else set the class in the soan node 
						//modify by jagan
						if (range.commonAncestorContainer.parentNode.nodeName == 'SPAN' || range.commonAncestorContainer.nodeName == 'SPAN'){
							var rangeParentNode = (range.commonAncestorContainer.parentNode.nodeName == 'SPAN')?range.commonAncestorContainer.parentNode:range.commonAncestorContainer;
							if(rangeParentNode.classList.contains('ins') || rangeParentNode.classList.contains('del') || rangeParentNode.classList.contains('sty') || rangeParentNode.classList.contains('styrm')){
								var newNode = document.createElement('span');
								newNode.setAttribute('class', param.class);
								newNode.innerHTML = rangeParentNode.outerHTML;
								rangeParentNode.parentNode.insertBefore(newNode, rangeParentNode);
								rangeParentNode.parentNode.removeChild(rangeParentNode);
								kriya.selection = newNode;
							}else{
								rangeParentNode.setAttribute('class', param.class);	
							}
						}
						else {
								var newNode = document.createElement('span');
								newNode.setAttribute('class', param.class);
								newNode.innerHTML = range.toHtml();
								kriya.selection = newNode;
								selection.getRangeAt(0).deleteContents();
								selection.getRangeAt(0).insertNode(newNode);
							}
						} 
					else{
						var newNode = document.createElement('span');
						newNode.setAttribute('class', param.class);
						newNode.innerHTML = range.toHtml();
						var formattingEls = newNode.getElementsByTagName("*");
						// Remove the formatting elements
						for (var i = 0, c = 0, l = formattingEls.length; i < l; i++ ) {
							el = formattingEls[c];
							if (/^(I|B|EM|STRONG)/.test(el.nodeName)){
								eventHandler.menu.style.DOMRemove(el);
							}else{
								c++;
							}
						}
						selection.getRangeAt(0).deleteContents()
						selection.getRangeAt(0).insertNode(newNode);
					}
				
					
			}else{
					var blockNodeRegex = /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i;
					function isBlockNode(node) {
						if (!node) {
							return false;
						}
						var nodeType = node.nodeType;
						return nodeType == 1 && blockNodeRegex.test(node.nodeName)
							|| nodeType == 9 || nodeType == 11;
					}
					
					function isBlockElement(el) {
						// You may want to add a more complete list of block level element
						// names on the next line
						return /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i.test(el.tagName);
					}
					var sel = rangy.getSelection();
					var range = sel.getRangeAt(0);
					if (sel.rangeCount) {
						// get All selected elements including text nodes
						var selectedNodes = [];
						for (var i = 0; i < sel.rangeCount; ++i) {
							//http://stackoverflow.com/a/10075894/3545167
							//this gives all nodes including text and childnodes of the selected blocknodes
							//selectedNodes = selectedNodes.concat( sel.getRangeAt(i).getNodes() );
							
							//http://stackoverflow.com/a/4220880/3545167
							//this returns only the block level nodes, which we actually need here
							selectedNodes = range.getNodes([1], isBlockElement);
						}
						//check if the first node is a blockNodeRegex
						//if not get the first possible block node
						if (selectedNodes.length == 0){
							ancestor = range.commonAncestorContainer;
							var blockElements = isBlockNode(ancestor);
							if (! blockElements){
								do {
									ancestor = ancestor.parentNode;
									var blockElements = isBlockNode(ancestor);
								}while (! blockElements);
							}
							selectedNodes = [ancestor];
						}
					}
					selectedNodes = eventHandler.menu.style.renameNode(selectedNodes, param.name, param.class, param.subClass, param.wrapNode);
					//if (selectedNodes == undefined) return false; selectedNodes will come as undefined or array if there is no elements in side that array then also it is not equal to undefined
					if (selectedNodes == undefined || selectedNodes.length < 1) return false;
					//if the abstract convert to any format  data-level is not updating-kirankumar
					var datalevel=$(selectedNodes[0]).prop('tagName').replace(/[A-Z]+/ig,'');
					if(datalevel !='')
					{
						$(selectedNodes[0]).attr('data-level',datalevel);
					}else{
						$(selectedNodes[0]).removeAttr('data-level');
					}
					//to retain the selection
					if (selectedNodes.length > 0){
						sel = rangy.getSelection();
						range = sel.getRangeAt(0);
						range.setStart(selectedNodes[0], 1);
						var len = selectedNodes.length - 1;
						range.setEnd(selectedNodes[len], 1);
						sel.removeAllRanges();
						sel.addRange(range);
						kriya.selection = range;
					}
					//kriyaEditor.settings.undoStack.push(selectedNodes);
				}
				//added by kirankumar for update TOC
				//this.updateTOC(param,selectedNodes);
				//kriyaEditor.init.addUndoLevel('style-change');
				kriyaEditor.init.save();
			},
			addBlind: function(param, targetNode){
				var sel = rangy.getSelection();
				var range = sel.getRangeAt(0);
				if($(kriya.selectedNodes).length == 0){
					$(range.startContainer).closest('div:not(.front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,.nano),p,h1,h2,h3,h4,h5,h6,table').attr('data-blind', 'true');
				} else {
					$(kriya.selectedNodes).each(function(){
						$(this).closest('div:not(.front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,.nano),p,h1,h2,h3,h4,h5,h6,table').attr('data-blind', 'true');
					});
				}
				kriyaEditor.init.save();
			},
			removeBlind: function(param, targetNode){
				var sel = rangy.getSelection();
				var range = sel.getRangeAt(0);
				if($(kriya.selectedNodes).length == 0){
					$(range.startContainer).closest('[data-blind]').removeAttr('data-blind');
				} else {
					$(kriya.selectedNodes).each(function(){
						$(this).closest('[data-blind]').removeAttr('data-blind');
					});
				}
				kriyaEditor.init.save();
			},
			applyPartLabel: function(param, targetNode){
				//to add/remove part label 
				if ($(kriya.selectedNodes).parents('.' + param.class).length > 0){
					//kriyaEditor.settings.undoStack.push($(kriya.selectedNodes).parents('.' + param.class)[0].parentNode);
					$(kriya.selectedNodes).parents('.' + param.class).each(function(index ,value){
						$(this).contents().unwrap();
					});
				}else{
					//to combine the text nodes
					if (kriya.selection && kriya.selection.commonAncestorContainer){
						$(kriya.selection.commonAncestorContainer).html($(kriya.selection.commonAncestorContainer).html());
					}
					this.applyFormat(param, targetNode);
					if(kriya.selection && kriya.selection.nodeType == 1){
						//kriyaEditor.settings.undoStack.push($(kriya.selection));
					}else if(kriya.selection && kriya.selection.commonAncestorContainer){
						//kriyaEditor.settings.undoStack.push($(kriya.selection.commonAncestorContainer));
					}
				}
				//kriyaEditor.init.addUndoLevel('style-change');
				kriyaEditor.init.save();
			},
			renameNode: function(node, name, className, subClass, wrapNode){
				var newNodes = [];
				if(wrapNode){
					var prvnode = $(node).prev('.'+className);
					var nextnode = $(node).next('.'+className);
					var CheckFrontQuoteAttrib = true;
					//this code will check "FrontQuoteAttrib" class in blockQuote and pullQuote
					//if there "FrontQuoteAttrib" class as last element or first element based on that blockQuote or pullQuote will create
					//start
					if(prvnode.length > 0 || nextnode.length > 0){
						if(prvnode.length > 0 && nextnode.length > 0){
							var prvtemp = $(prvnode).find('p').last();
							var nexttemp = $(nextnode).find('p').first();
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib') && nexttemp.length>0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else if(prvnode.length > 0){
							var temp = $(prvnode).find('p').last();
							if(temp.length > 0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else{
							var temp = $(nextnode).find('p').first();
							if(temp.length>0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}
					}
					if(CheckFrontQuoteAttrib && (prvnode.length > 0 || nextnode.length > 0) && $(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length == 0){
						var newParentNode =	$(node).prev('.'+className);
						if($(node).prev('.'+className).length > 0 && $(node).next('.'+className).length > 0){
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib')){
								var parent ="next";
								newParentNode =	$(node).next('.'+className);
							}else if(nexttemp.length > 0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								var parent ="prev";
							}else{//end
								var parent = "both";
							}
						}else if($(node).prev('.'+className).length > 0){
							var parent ="prev";
						}else{
							var parent ="next";
							newParentNode =	$(node).next('.'+className);
						}
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							//kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
							}
							if(parent == "both"){
								var next = $(nextnode[0]).clone();
								$(newParentNode).append($(currNode)).append($(next).contents().unwrap());
								$(nextnode).attr('data-removed',true);
								//kriyaEditor.settings.undoStack.push($(nextnode));
								$(nextnode).remove();
							}else if(parent == "next"){
								$(newParentNode).prepend($(currNode))
							}else{
								$(newParentNode).append($(currNode))
							}

						}
						//need to return newNodes if new nodes is empty save will not trigger
						newNodes.push($(newParentNode)[0]);
						//kriyaEditor.settings.undoStack.push(newParentNode)
					}else if($(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length > 0){
						var parentnode = $(node).closest('.jrnlBlockQuote , .jrnlPullQuote')
						$(parentnode).attr('class',className);
						var prvnode = $(parentnode).prev('.'+className);
						var nextnode = $(parentnode).next('.'+className);
						if(prvnode.length > 0 && nextnode.length > 0){
							var newParentNode = prvnode;
							var next = $(nextnode[0]).clone();
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap()).append($(next).contents().unwrap());
							$(nextnode).attr('data-removed',true);
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							//kriyaEditor.settings.undoStack.push($(nextnode));
							//kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
							$(nextnode).remove();
						}else if(prvnode.length > 0){
							var newParentNode = prvnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							//kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}else if(nextnode.length > 0){
							var newParentNode = nextnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).prepend($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							//kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}
						//kriyaEditor.settings.undoStack.push($(parentnode));
						// if block already block quote are pull then, save is not tiggered
						// so we are intiating undolevel
						/*if(kriyaEditor.settings.undoStack.length != 0){
							//kriyaEditor.init.addUndoLevel('style-change');
							kriyaEditor.init.save();
						}*/
					}else{
						var newParentNode = document.createElement(name);
						$(newParentNode).attr('class',className);
						$(newParentNode).attr('id',uuid.v4());
						$(newParentNode).attr('data-inserted','true');
						$(newParentNode).insertBefore(node[0]);
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							//kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
							}
							$(newParentNode).append($(currNode))
						}
						newNodes.push(newParentNode);
					}

				}else{
						for (var r = 0, rl = node.length; r < rl; r++){
							var currNode = node[r];						
							if (currNode.nodeName == "DIV") return;
							if (typeof(className) != "undefined"){
								currNode.setAttribute('class', className);
							}
							if (currNode.nodeName != name){
								var newNode = document.createElement(name);
								Array.prototype.slice.call(currNode.attributes).forEach(function(a) {
									newNode.setAttribute(a.name, a.value);
								});
								while (currNode.firstChild) {
									newNode.appendChild(currNode.firstChild);
								}
								currNode.parentNode.replaceChild(newNode, currNode);
								newNodes.push(newNode);
							}else{
								newNodes.push(currNode);
							}							
						}						
						var unWarpFromNode = ['jrnlBlockQuote','jrnlPullQuote'];
						var allowedParaNode = ['jrnlQuoteAttrib','jrnlQuotePara'];
						var parentSelNode;
						var savedNodes = [];
						for(var uw = 0, nn = newNodes.length;  uw < nn; uw++){
							if((unWarpFromNode.indexOf($(newNodes[uw]).closest('div').attr('class'))>=0)&&(allowedParaNode.indexOf($(newNodes[uw]).attr('class')) < 0)){
								parentSelNode = $(newNodes[uw]).closest('div');																												
								var nextNode = $(newNodes[uw]).next();
								var prevNode = $(newNodes[uw]).prev();
								if(nextNode.length > 0 && prevNode.length > 0){
									var allNextNodes = $(newNodes[uw]).nextAll();
									$(newNodes[uw]).insertAfter($(parentSelNode));									
									$(newNodes[uw]).attr('data-inserted','true');
									//kriyaEditor.settings.undoStack.push($(parentSelNode));
									//kriyaEditor.settings.undoStack.push($(newNodes[uw]));
									var className = $(parentSelNode).attr('class') ;
									var newParentNode = document.createElement('div');
									$(newParentNode).attr('class',className);
									$(newParentNode).attr('id',uuid.v4());
									$(newParentNode).attr('data-inserted','true');
									$(newParentNode).insertAfter(newNodes[uw]);										
									$(newParentNode).append($(allNextNodes));
									//kriyaEditor.settings.undoStack.push($(newParentNode));								
								}
								else{
									if(nextNode.length == 0 && prevNode.length == 0){
										$(newNodes[uw]).attr('data-moved','true');
										$(newNodes[uw]).insertBefore($(parentSelNode));	
										//kriyaEditor.settings.undoStack.push($(newNodes[uw]));
										//kriyaEditor.init.addUndoLevel('style-change');
										//kriyaEditor.init.save();
									    newNodes.splice(uw);									
									} 
									else{
										if(nextNode.length > 0){
											$(newNodes[uw]).attr('data-moved','true');
											$(newNodes[uw]).insertBefore($(parentSelNode));	
										}
										else if(prevNode.length > 0){
											$(newNodes[uw]).attr('data-moved','true');
											$(newNodes[uw]).insertAfter($(parentSelNode));	
										}
										//kriyaEditor.settings.undoStack.push($(newNodes[uw]));																	
									}									
								}								
							}
							if($(parentSelNode).find('p').length == 0){									
								//kriyaEditor.settings.undoStack.push($(parentSelNode).attr('data-removed','true'));								
								//kriyaEditor.init.addUndoLevel('style-change');							
								//kriyaEditor.init.save();
								$(parentSelNode).remove();
							}
						}																
				}				
				return newNodes;
			}
		}
	};
	eventHandler.welcome = {
		signoff: {
			cancelSignOff: function (param, targetNode) {
				kriya.popUp.closePopUps($('[data-component="signoff_edit"]'));
			},
			signoffPage: function (param, targetNode) {
				if(kriya.config.content.role == "associateeditor" && $('.kriya-version-collapsible li[data-current-version="true"] .kriya-collapsible .reviewer:not([data-reviewer-status="completed"]):not([data-reviewer-status="terminated"]):not([data-reviewer-status="rejected"]):not([data-reviewer-status="back-up"])').length > 0){
					kriya.notification({
						 title: 'WARNING',
						 type: 'warning',
						 content: 'One of their reviewers has not completed their review. Wait for them to complete or terminate their review.',
						 icon: 'icon-warning2'
					});
					return false;
				}

				$('.la-container').fadeIn();
				
				var currentStage = kriya.config.content.stage;
				if (param && param.stage){
					currentStage = param.stage;
				}

				$.ajax({
					type: "GET",
					url: '/api/getdata?customer=' + kriya.config.content.customer + '&project=' + kriya.config.content.project + '&doi=' + kriya.config.content.doi + '&xpath=' + '//workflow/stage[last()]',
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (res) {
						if (res) {
							var stageNode = $(res);
							var currentStatus = stageNode.find(' > status').text();
							if (currentStatus == "completed") {
								$('.la-container').fadeOut();
								kriya.notification({ title: 'ERROR', type: 'error', content: 'Looks like there is an issue with workflow. You will not be able to approve the manuscript at the moment. Please contact support/publisher.', icon: 'icon-warning2' });
							} else {
								var params = 'customer=' + kriya.config.content.customer + '&doi=' + kriya.config.content.doi + '&project=' + kriya.config.content.project;
								if(kriya.config.content.role == "reviewer"){
									params = params + '&currStage=reviewercheck';
								}else{
									params = params + '&currStage=' + currentStage;
								}
								$.ajax({
									type: "GET",
									url: "/api/getsignoffoptions?" + params,
									success: function (data) {
										$('.la-container').fadeOut();
										var res = $('<div>' + data + '</div>');
										$('[data-component="signoff_edit"] [data-wrapper]').html($(res).find('options').html());
										$(res).find('trigger').attr('style', 'display:none');
										$('[data-component="signoff_edit"] [data-wrapper]').append($(res).find('trigger'));
										$(res).children('option[name]').each(function(){
											$('[data-component="signoff_edit"] [data-wrapper]').append('<p>'+$(this).html()+'</p>');
										})
										$('[data-component="signoff_edit"] [data-wrapper] input[type="radio"]').attr('name', 'signoff-radio');
										if ($('[data-component="signoff_edit"] [data-wrapper] p[data-role-for]').length > 0){
											var roleFor = $('[data-component="signoff_edit"] [data-wrapper] p[data-role-for]').attr('data-role-for');
											var addOption = $('[data-component="signoff_edit"] [data-template] p[data-role-for="' + roleFor +'"]').find('.addOption').clone();
											$('[data-component="signoff_edit"] [data-wrapper] p[data-role-for]').append(addOption);
										}
										
										if($('[data-component="signoff_edit"] [data-wrapper] input[type="radio"]').length == 1){
											$('[data-component="signoff_edit"] [data-wrapper] input[type="radio"]').prop('checked', 'true');
										}
	
										//If the configured role made any correction then remove that node wich has data-check-no-correction attribute - jagan
										$('[data-component="signoff_edit"] [data-wrapper] [data-check-no-correction]').each(function(){
											var checkCor = $(this).attr('data-check-no-correction');
											var correctionNode = $('#changesDivNode .change-div.card[data-role="' + checkCor + '"], #historyDivNode .historyCard.card[data-role="' + checkCor + '"]');
											if(correctionNode.length > 0){
												$(this).remove();
											}
										});
										$('[data-component="signoff_edit"] [data-wrapper] [data-check-correction]').each(function(){
											var checkCor = $(this).attr('data-check-correction');
											var correctionNode = $('#changesDivNode .change-div.card[data-role="' + checkCor + '"], #historyDivNode .historyCard.card[data-role="' + checkCor + '"]');
											if(correctionNode.length == 0){
												$(this).remove();
											}
										});
										
										kriya.popUp.openPopUps($('[data-component="signoff_edit"]'));
									},
									error: function (res) {
										$('.la-container').fadeOut()
										console.error('ERROR:saving content.' + res);
									}
								});
							}
						}
					},
					error: function(err){
						$('.la-container').fadeOut();
						kriya.notification({ title: 'ERROR', type: 'error', content: 'Unable to fetch current workflow status. Please try again or contact support/publisher.', icon: 'icon-warning2' });
					}
				});

			},
			signOffToNextStage: function (param, targetNode) {
				var nextStage = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').attr('data-reviewer');
				var parameters = {
					'customer': kriya.config.content.customer,
					'project': kriya.config.content.project,
					'doi': kriya.config.content.doi,
					'role': kriya.config.content.role,
					'currStage': nextStage
				};
				var userEmail = $('.userProfile .username').attr('data-user-email');
				var userName = $('.userProfile .username').text();
				if(kriya.config.content.role == "reviewer"){
					parameters['userMailID'] = userEmail;
					parameters['userName'] = userName;
					parameters['reviewerStatus'] = "completed";
					parameters['updateUserXML']     = "true";
					parameters['reviewerMessage'] = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').closest('p').text();
				}else if(kriya.config.content.role == "editor"){
					parameters['reviewerStatus'] = "in-progress";
					parameters['userMailID'] = userEmail;
					parameters['assignUser'] = 'true';
					parameters['updateEditor'] = 'true';
				}else if(kriya.config.content.role == "associateeditor"){
					parameters['userMailID'] = userEmail;
					parameters['reviewerStatus'] = "completed";
					parameters['updateEditor'] = 'true';
					parameters['reviewerMessage'] = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').closest('p').text();
				}

				var versionNumber = $('.versionMenu .current-version').text().trim();
				parameters['version'] = versionNumber;

				var selectedOption = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').closest('p');
				//send mail has to be handled as many reciver may be there
				$('[data-reviewers-block="true"]').each(function(){
					if ($(this).find('.recipient').length > 0 && $(this).find('.recipient').val() != "---") {
						parameters['to'] = $(this).find('.recipient').val();
						parameters['recipientName'] = $(this).find('.recipient option:checked').text();
					}
				});
				//get email recipients info from the sign-off workflow html
				if (selectedOption.find('.recipient').length > 0 && selectedOption.find('.recipient').val() == "---"){
						return true;
				}
				if (selectedOption.find('.recipient').length > 0 && selectedOption.find('.recipient').val() != "---"){
					parameters['to'] = selectedOption.find('.recipient').val();
					parameters['recipientName'] = selectedOption.find('.recipient option:checked').text();
				}
				if ($('trigger[name="' + nextStage + '"]').length > 0) {
					var trigger = $('trigger[name="' + nextStage + '"][action="sendMail"]').first();
					if (typeof (parameters['to']) == "undefined" && trigger.find('to').text() != "") {
						parameters['to'] = trigger.find('to').text();
					}

					if (typeof (parameters['recipientName']) == "undefined" && trigger.find('recipient').text() != "") {
						if (trigger.find('recipient .given-names').length > 0) {
							parameters['recipientName'] = trigger.find('recipient .given-names').text() + ' ' + trigger.find('recipient .surname').text();
						} else {
							parameters['recipientName'] = trigger.find('recipient').text();
						}
					}

					if (trigger.find('cc').text() != "") {
						parameters['cc'] = trigger.find('cc').text();
					}
					if (trigger.find('bcc').text() != "") {
						parameters['bcc'] = trigger.find('bcc').text();
					}
					if (trigger.find('recipientName').text() != "") {
						parameters['recipientName'] = trigger.find('recipientName').text();
					}
					if ($('trigger[name="' + nextStage + '"][action="addStage"]:first stage').text() != "") {
						parameters['stagename'] = $('trigger[name="' + nextStage + '"][action="addStage"] stage').text();
					}
					var emailComponent = $('[data-component="signoff_edit"] .email-component');
					if (emailComponent.length > 0) {
						if (trigger.find('.email-body').html() != emailComponent.find('.email-body').html()) {
							//Remove the empty list items
							var clonedBody = emailComponent.find('.email-body').clone();
							clonedBody.find('li').each(function(){
								if($(this).text() == ""){
									$(this).remove();
								}
							});
							parameters['emailBody'] = clonedBody.html();
						}

						
						if (emailComponent.find('.email-cc').text() != "") {
							parameters['cc'] = emailComponent.find('.email-cc').text();
						}
						if (emailComponent.find('.email-bcc').text() != "") {
							parameters['bcc'] = emailComponent.find('.email-bcc').text();
						}
						if (emailComponent.find('.email-to').text() != "") {
							parameters['to'] = emailComponent.find('.email-to').text();							
						}
						if (emailComponent.find('.email-replyTo').text() != "") {
							parameters['replyTo'] = emailComponent.find('.email-replyTo').text();
						}
						if (emailComponent.find('.email-subject').text() != "") {
							parameters['subject'] = emailComponent.find('.email-subject').text();
						}

					}
					var popper = $('[data-component="signoff_edit"]')
					if(popper.find('.attachementContainer .card').length > 0){
						parameters['attachments'] = [];
						popper.find('.attachementContainer .card').each(function(){
							parameters['attachments'].push({
								filename: $(this).attr('data-file-name'),
								path: $(this).attr('data-file-path'),
								contentType: $(this).attr('data-file-type')
							});
						});
					}

				}
				//return error if to address is not found
				if (typeof (parameters['to']) == "undefined" || parameters['to'] == "") {
					kriya.notification({
						title: 'ERROR',
						type: 'error',
						timeout: 10000,
						content: 'Email address not found!<br>Contact Administrator',
						icon: 'icon-warning2'
					});
					return false;
				}
				$(targetNode).addClass('disabled');
				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('signoff', parameters, function (res) {
					if (res && res.status && res.status.message == "failed") {
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'ERROR',
							type: 'error',
							content: 'Signoff failed.',
							icon: 'icon-warning2'
						});
					} else {
						$('[data-component="signoff_edit"] .btn').addClass('hidden');
						$('[data-component="signoff_edit"] [data-wrapper]').html('<p style="text-align: center;">You have <strong>signed off</strong> this manuscript successfully. <em>No further changes</em> can be made to the manuscript.</p>');
						setTimeout(function () {
							$('.la-container').fadeOut();
							$('body .messageDiv .message').html('You have successfully signed off the article to the next stage.<br> Please close the browser to exit the system.');
							$('body .messageDiv').removeClass('hidden');
							$('body .messageDiv .feedBack').removeClass('hidden');

							kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
							//window.close();
						}, 3000);

						if($('#contentContainer').attr('article-access-type') == "linkwithkey"){
							$.ajax({
								type: 'GET',
								url : '/logout?key=true',
								success: function (res) {
									$('.la-container').fadeOut();
								},
								error: function(err){
									$('.la-container').fadeOut()
								}
							});
						}

					}
				}, function (res) {
					$('.la-container').fadeOut();
					$(targetNode).removeClass('disabled');
					kriya.notification({
						title: 'ERROR',
						type: 'error',
						content: 'Signoff failed. Please try again.',
						icon: 'icon-warning2'
					});
				});
			},
			logOut: function(){
				// to prevent closing article while content to be saved is in save queue - aravind
				if(kriya.config.saveQueue && kriya.config.saveQueue.length > 0){
					if(!(confirm("your are having unsaved data! Do you still want to logout?"))){
						return false;
					}
				}
				if ($('#contentContainer').attr('data-stage-name') == undefined) return false;
				$('.la-container').fadeIn();
				var d = new Date();
				var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				var parameters = {
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project,
					'doi':kriya.config.content.doi,
					'type':'mergeData',
					'data': {
						'process': 'update',
						'xpath': '//workflow/stage[name[.="' + $('#contentContainer').attr('data-stage-name') + '"] and status[.="in-progress"]]',
						'content': '<stage><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status type="user">logged-off</status></log></job-logs></stage>'
					}
				};

				kriya.general.sendAPIRequest('updatedata', parameters, function(res){
					setTimeout(function(){
						//Reset the welcome page cookie when logout the article
						var d = new Date();
						var expires = "expires=" + d.toGMTString();
						var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
						document.cookie = cname + "=true;" + expires + ";path=/";
						
						$('.la-container').fadeOut();
						$('body .messageDiv .message').html('You have successfully logged off.<br> Please close the browser to exit the system.');
						$('body .messageDiv').removeClass('hidden');
						$('body .messageDiv .feedBack').addClass('hidden');
						kriya.config.pageStatus = "logged-off"; //This flag used in on window close in keeplive.js
						//window.close();	
					},1000);
				},function(res){
					$('.la-container').fadeOut()
				});
			},
		}
	};


	return eventHandler;

})(eventHandler || {});

// returns a list of all elements under the cursor
function elementsFromPoint(x, y) {
	var elements = [],
		previousPointerEvents = [],
		current, i, d;
	if (typeof (x) == 'undefined' || typeof (y) == undefined) {
		return elements;
	}
	if (typeof document.elementsFromPoint === "function")
		return document.elementsFromPoint(x, y);
	if (typeof document.msElementsFromPoint === "function")
		return document.msElementsFromPoint(x, y);

	// get all elements via elementFromPoint, and remove them from hit-testing in order
	while ((current = document.elementFromPoint(x, y)) && elements.indexOf(current) === -1 && current != null) {

		// push the element and its current style
		elements.push(current);
		previousPointerEvents.push({
			value: current.style.getPropertyValue('pointer-events'),
			priority: current.style.getPropertyPriority('pointer-events')
		});

		// add "pointer-events: none", to get to the underlying element
		current.style.setProperty('pointer-events', 'none', 'important');
	}

	// restore the previous pointer-events values
	for (i = previousPointerEvents.length; d = previousPointerEvents[--i];) {
		elements[i].style.setProperty('pointer-events', d.value ? d.value : '', d.priority);
	}

	// return our results
	return elements;
}

function textNodesUnder(el) {
	var n, a = [],
		walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null, false);
	while (n = walk.nextNode()) a.push(n);
	return a;
}

$.urlParam = function (name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return null;
	} else {
		return results[1] || 0;
	}
}
