
/**
* eventHandler - this javascript holds all the functions required for Reference handling
*				 so that the functionalities can be turned on and off by just calling the required functions
*				 Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	
	settings.subscriptions = {};
	settings.subscribers = ['menu','query','components','welcome'];
	settings.tempFileList = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;
	var proofInterval = 120000; //120secs
	var proofStage = '', proofStartTime = new Date().getTime(); 

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel', 
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);
			
			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			
			elements = $(event.target);
			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}
								
								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.menu = {
		search:{
			query: function(param, targetNode){
				if ($('.querySelector').find('#logtype').val() == ''){
					return false;
				}
				var query = "?queryString=logtype.keyword:(" + $('.querySelector').find('#logtype').val() + ")";
				$('.querySelector .input-field input').each(function(){
					var IID = $(this).attr('id');
					if (! /^(logtype|fromdate|todate)$/.test(IID)){
						if ($(this).val() != ''){
							query += " AND " + IID + ".keyword:(" + $('.querySelector').find('#'+IID).val() + ")";
						}
					}
				});
				if ($('.querySelector').find('#todate').val() != ''){
					query += "&date=" + $('.querySelector').find('#todate').val();
				}
				$('#dataContainer').find('#query-response').remove();
				$('.querySelector').find('.log-count').remove();
				jQuery.ajax({
					type: "GET",
					url: "/api/logsmanager" + query,
					success: function (esData) {
						if (esData && esData.hits && esData.hits.total > 0){
							$('.querySelector .row:last').append('<span class="log-count" style="float:right;padding: 0px 10px;background: #23a094;color: #fff;margin-right: 10px;">' + esData.hits.hits.length + ' of ' + esData.hits.total + '</span>');
							var response = $('<table class="striped" id="query-response"><tbody></tbody></table>');
							var logs = esData.hits.hits;
							for (var h = 0, hl = logs.length; h < hl; h++){
								var log = logs[h];
								var logSource = log._source;
								var tempRow = $('.query-template tr').clone();
								for (value in logSource){
									if ($(tempRow).find('.' + value).length > 0){
										if (value == 'requesturl'){
											$(tempRow).find('.' + value).html('<a href="' + logSource[value] + '" target="_blank" style="background: #2196F3;padding: 3px 6px;border-radius: 3px;color: rgb(255,255,255)!important;">Article Link</a>')
										}else if (logSource.logtype == 'articlekey' && value == 'logdata'){
											var logValue = decodeURIComponent(logSource[value]);
											var logDOM = $('<root>' + logValue + '</root>');
											logValue = '<p>Stage : ' + logDOM.find('stage name').text() + '<br/>';
											logValue += '<a href="' + $(logDOM).find('key').text() + '" target="_blank" style="background: rgb(255,87,34);padding: 3px 6px;border-radius: 3px;color: rgb(255,255,255)!important;">Email Link</a></p>';
											$(tempRow).find('.' + value).html(logValue);
										}else if (logSource.logtype == 'save' && value == 'logdata'){
											var logValue = decodeURIComponent(logSource[value]);
											if (logSource[value] != logValue){
												var logContent = $('<root>' + $(logValue).find('> response').find('message').text() + '</root>');
												var linkMsg = '<span class="category">' + logContent.html().replace(/<content>.*?<\/content>/, '') + '</span><br/>';
												linkMsg += '<a href="' + $(logValue).find('> path').text() + '" target="_blank" style="background: rgb(255,87,34);padding: 3px 6px;border-radius: 3px;color: rgb(255,255,255)!important;">Failure Link</a>';
												linkMsg += '<br/>' + '<span class="occurrence">' + $(logValue).find('> occurrence').text() + '</span>';
												$(tempRow).find('.' + value).html(linkMsg)
											}else{
												$(tempRow).find('.' + value).html('<a href="' + logSource[value] + '" target="_blank" style="background: rgb(255,87,34);padding: 3px 6px;border-radius: 3px;color: rgb(255,255,255)!important;">Failure Link</a>')
											}
										}else if (value == 'date'){
											$(tempRow).find('.' + value).html(logSource[value].replace(/^([0-9\-]+)T([0-9\:]+)\..*$/, '$1 $2'))
										}else{
											$(tempRow).find('.' + value).html(logSource[value]);
										}
									}
								}
								response.find('tbody').append(tempRow)
							}
							$('#dataContainer').append(response);
						}
					},
					error: function(xhr, errorType, exception) {
					  return null;
					}
				});
			}
		},
		components: {
			saveComponent: function(param, targetNode){
				$('.workflow-trigger').removeClass('selected');
			},
			closePopUp: function(param, targetNode){
				$('.workflow-trigger').removeClass('selected');
			},
			openWFTrigger: function(param, targetNode){
				var stageName = $(targetNode).attr('data-stage-name');
				var trigger = $('.wf-modal trigger[name="' + stageName + '"]');
				$('#componentDiv .workflow-trigger').addClass('selected');
				$('#componentDiv .workflow-trigger .right-tab div[data-type="email-content"][data-cloned]').remove();
				$(trigger).find('trigger[action="sendMail"]').each(function(){
					var clonedEmail = $(this).clone();
					var cloneTemplate = $('#componentDiv .workflow-trigger .right-tab div[data-type="email-content"][data-template]').clone();
					cloneTemplate.removeAttr('data-template').attr('data-cloned', 'true');
					clonedEmail.find('> *').each(function(){
						var nodeName = $(this)[0].nodeName.toLocaleLowerCase();
						if (cloneTemplate.find('[data-type="' + nodeName + '"]').length > 0){
							cloneTemplate.find('[data-type="' + nodeName + '"]').html($(this).html());
						}
					});
					$('#componentDiv .workflow-trigger .right-tab').append(cloneTemplate);
				})
			},
			editUserInfo: function(param, targetNode){
				$('.la-container').fadeIn();
				$('.userInfo.active').remove();
				var userInfo = JSON.parse(decodeURIComponent($(targetNode).parent().attr('data-user-data')));
				var userInfoDiv = $('.users-panel .userInfo').clone();
				for (var k in userInfo){
					if (Object.keys(userInfo[k]).length == 1){
						$(userInfoDiv).find('input#' + k).val(userInfo[k]._text);
						$(userInfoDiv).find('input#' + k).parent().find('label').addClass('active');
					}else{
						for (var v in userInfo[k]){
							if (Object.keys(userInfo[k][v]).length == 1){
								$(userInfoDiv).find('input#' + v).val(userInfo[k][v]._text).focus();
								$(userInfoDiv).find('input#' + v).parent().find('label').addClass('active');
							}
						}
					}
				}
				$(userInfoDiv).addClass('active').removeClass('hidden');
				$('.users-panel > *').addClass('hidden');
				$('.users-panel').append($(userInfoDiv));
				$('.la-container').fadeOut();
			},
			addNewUser: function(param, targetNode){
				$('.la-container').fadeIn();
				$('.users-panel .userInfo.active').remove();
				var userInfoDiv = $('.users-panel .userInfo').clone();
				$(userInfoDiv).find('h5.first').text('Add User');
				$(userInfoDiv).addClass('active').removeClass('hidden');
				$('.users-panel > *').addClass('hidden');
				$('.users-panel').append($(userInfoDiv));
				$('.la-container').fadeOut();
			},
			saveUserInfo: function(param, targetNode){
				$('.userinfo-btn').trigger('click');
			},
			closeUserPopUp: function(param, targetNode){
				$('.userinfo-btn').trigger('click');
			}
		}
	};
	return eventHandler;

})(eventHandler || {});
