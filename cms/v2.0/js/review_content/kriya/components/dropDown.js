/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    // 1. CONFIGURATION
    var config = {
    };

    // 2. COMPONENT OBJECT
    kriya.dropDown = {

        init: function (elm, component) {
			this.container = elm;
			this.component = component;
            getOptions(component);
			
			function getOptions(component){
				if ($('#contentContainer').attr('data-state') == 'read-only') return false;
				if ($('[data-type="dropDown"][data-component="'+component.name+'"]').length > 0){
					var selector = $('[data-type="dropDown"][data-component="'+component.name+'"]');
					thiscontainer = kriya.dropDown.container;
					thisevt = kriya.dropDown.evt;
					
					//#379 IE| Clicking Article type - Article type should be displayed as a button not as a image. - Prabakaran.A (prabakaran.a@focusite.com)
					//#533 Cannot read property 'parentNode' of undefined(chrome) & TypeError: a is null(firefox) - Mariyammal (mariyammal.m@praniontech.com) (parent->parentElement)
					var topCss = thiscontainer.offset().top + ((thiscontainer.parentElement && $(thiscontainer.parentElement).find('.jrnlSubject, .jrnlArtType').length>0)?thiscontainer.height()+10 : thiscontainer.height());
					//End of #379
				
					var cp = thiscontainer.offset().left;
					var dropDownNode = selector.find('.dropdown-content');
					dropDownNode.css('left', cp);
					dropDownNode.css('top', topCss);
					dropDownNode.css('opacity', '1');
					dropDownNode.css('display', 'block');
					
					selector.removeClass('hidden');
					selector[0].addEventListener('mouseleave', function(){
						selector.addClass('hidden');
					});

					$(selector).off('click');
					$(selector).on('click', function(evt){
						var targetNode = $(evt.target).closest('li');
						if(targetNode.length > 0){
							var dataValue = $(targetNode).html();
							if(targetNode.length > 0 && targetNode[0].hasAttribute('data-value')){
								dataValue = $(targetNode).attr('data-value');
							}

							if($(targetNode).attr('data-remove-data') == "true" && thiscontainer[0].hasAttribute('id')){
								$(thiscontainer).html(dataValue);
								thiscontainer[0].setAttribute('data-removed', 'true');
								thiscontainer[0].setAttribute('data-template-button', 'true');
							}else if(dataValue){
								$(thiscontainer).html(dataValue);
								if (thiscontainer[0].hasAttribute('data-template-button')){
									$(thiscontainer).removeAttr('data-template-button');
									thiscontainer[0].setAttribute('id', uuid.v4());
									thiscontainer[0].setAttribute('data-inserted', 'true');
								}
							}
							selector.addClass('hidden');
							kriyaEditor.settings.undoStack.push(thiscontainer);
							kriyaEditor.init.addUndoLevel('drop-down-update');
						}
					});
				}
			}
        },

		initDropdown: function () {
			
		},

    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));