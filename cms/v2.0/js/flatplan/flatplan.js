var binder = {
    meta: {
        "journal-name._text": {
            "component": "text",
            "label": "Journal Name: "
        },
        "journal-abbrev._text": {
            "component": "text",
            "label": "Journal Abbreviation: "
        },
        "volume._text": {
            "component": "text",
            "label": "Volume: "
        },
        "issue._text": {
            "component": "text",
            "label": "Issue: "
        },
        "day._text": {
            "component": "text",
            "label": "Publication Day: "
        },
        "month._text": {
            "component": "text",
            "label": "Publication Month: "
        },
        "year._text": {
            "component": "text",
            "label": "Publication Year: "
        },
        "page-count._text": {
            "component": "text",
            "label": "Page Count: "
        }
    },
    "cover": {
        "data.p": {
            "component": "contenteditable",
            "label": "Cover text"
        },
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "editorial-board": {
        "file._attributes.path": {
            "component": "file",
            "label": "File"
        }
    },
    "advert": {
        "region": {
            "component": "region",
            "label": "Region"
        }
    },
    "table-of-contents": {
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "filler-toc": {
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "blank": {
        "file._attributes.path": {
            "component": "file",
            "label": "File"
        }
    },
    "manuscript": {
        "_attributes.id": {
            "component": "text",
            "label": "ID"
        },
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        },
        "color._attributes": {
            "component": "color",
            "label": "Color in print"
        }
    }
}
var convert = '';
var configData = {};

function showMessage(obj) {
    var hide = true
    if (obj.hide == false){
        hide = false        
    }
    var pnObj = {
        text: obj.text, //'That thing that you were trying to do worked.',
        type: obj.type,
        hide: hide,
        buttons: { closer: obj.closer, sticker: false },
    }
    if (obj.title){
        pnObj.title = obj.title
    }
    new PNotify(pnObj);
}

function imgError(current){
    current.src = "../images/NoImageFound.jpg"
 }

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

/* function to scrollintoview of the specifed elementID */
function goToView(elementID) {
    var element = $('#' +elementID)
    $('#flatPlanBodyDiv .wrapper').find('.focus').removeClass('focus')
    $('#' +elementID).parent().addClass('focus')
    $('#flatPlanBodyDiv .wrapper').find('img[data-id="'+ elementID +'"]').parent().addClass('focus')
    $('#flatPlanBodyDiv .container').mCustomScrollbar('scrollTo',$('#flatPlanBodyDiv .container').find('#' +elementID));
}
/* input -path-string, original data; returns - data from specified path */
function getDataFromKey(pathString,data){
    var keyArr = pathString.split('.');
    var myData = data;
    keyArr.forEach(function (key) {
        if (myData[key]) {
            myData = myData[key];
        }
        else if (myData.constructor.toString().indexOf("Array") > -1) {
            var mData = [];
            myData.forEach(function (mKey) {
                mData.push(mKey[key]);
            })
            myData = mData;
        }
        else {
            myData = '';
        }
    })
    //console.log(myData)
    return myData
}
/*input - node, data; updates the data in the specified node*/
function updateNodeVal(nodeToUpdate,data){
    nodeToUpdate.each(function(){
        var current=$(this)
        var updateNodeVal = current.attr('data-update-node')
        var keyArr = updateNodeVal.split('.');
        var currObj = data;
        keyArr.forEach(function (key) {
            if (currObj[key]) {
                currObj = currObj[key];
            }
            else if (currObj.constructor.toString().indexOf("Array") > -1) {
                var mData = [];
                currObj.forEach(function (mKey) {
                    mData.push(mKey[key]);
                })
                currObj = mData;
            }
            else {
                currObj = '';
            }
        })
        current.text(currObj)
        current.removeAttr('data-update-node')
    })
}
function updateChangeHistory(histObj) {
    var userName = $('.userinfo:first').attr('data-name');
    var currTime = new Date().toString();
    var changeText = histObj.change;
    var fileID = histObj.id;
    var fileType = histObj.type;
    var changeObj = { 'user': userName, 'time': currTime, 'change': changeText, 'file': fileID, 'fileType': fileType, 'id-type': histObj['id-type'] };
    $('#tocContainer').data('binder')['container-xml']['change-history'].push(changeObj);
    eventHandler.general.components.populateHistory(changeObj);
}

// init change events for data save
function initEvents() {
    $('#detailsContainer')
        // when the flags are changed
        .on('change', 'select.flag, .flag', function(e){
            var flagType = '', flagValue = 0;
            var dataCID = $('#msDataDiv').attr('data-c-rid');
            var currNodeIndex = parseInt(dataCID);
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
                flagType = $(this).attr('data-flag-type');
                flagValue = this.value;
            }
            else if (e.currentTarget.nodeName.toLowerCase() === 'input'){
                flagType = $(this).attr('data-flag-type');
                if ($(this).is(':checked')) {
                    flagValue = 1;
                    //if flagtype openacess , remove hide from open acess select
                    if(flagType == 'open-access'){
                        $(this).parent().find('select').removeClass('hide')
                        flagValue =  $(this).parent().find('select')[0].value
                    }
                    // add cc-by as open access label
                }
                else{
                    if(flagType == 'open-access'){
                        $(this).parent().find('select').addClass('hide')
                        flagValue = 0
                    }
                }
                //if flagtype is openacess
                //if flag is not checked, remove openacess from data
            }
            // flagType is not set then do nothing, this check is used to skip the extra event on select due to materialisecss
            if (flagType == '') return true;
            console.log('flag: ', flagType, flagValue);
            // get data from the corresponding article
            var data = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
            var histObj = JSON.parse(JSON.stringify(data._attributes));
            histObj.change = 'changed flag <u>' + flagType + '</u> to be ' + flagValue.toString();
            //if flag is set, add the flagetype to the  card in toc
                if(flagValue == 1 || (/(CC)/gi.test(flagValue))){
                    var currFlag =''
                    if(/(CC)/gi.test(flagValue)){
                        currFlag = flagValue
                    }
                    else if(flagValue == 1){
                        currFlag = flagType
                    }
                    if(currFlag){
                    if($('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.flagList').length == 1){
                        if($('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.flagList span[data-flag-type="'+ flagType +'"]').length == 1){
                            $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.flagList span[data-flag-type="'+ flagType +'"]').text(currFlag)
                        }
                    else{    
                    $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.flagList').append('<span data-flag-type="'+ flagType +'"> '+ currFlag +'</span>')
                    }//artType
                    }
                    else{
                       // $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.msID').append('<span class="flagList"><span data-flag-type="'+ flagType +'">'+ currFlag +';</span></span>')
                      $('<span class="flagList"><span data-flag-type="'+ flagType +'">'+ currFlag +'</span></span>').insertAfter($('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.msID .artType'))
                    }
                }
                }
            //if flag is unset, remove the flag from the flaglist in toc card
         //   else if(flagValue == 0 || (/(none)/gi.test(flagValue))){
            else if(/(none)$/gi.test(flagValue) || flagValue == 0){
                $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').find('.flagList span[data-flag-type="'+ flagType +'"]').remove();
            }
            // the current article does not have a flag yet. directly set one
            if (typeof(data.flag) === 'undefined' && flagType != 'section-head'){
                data.flag = [{"_attributes":{"type": flagType, "value": flagValue}}];
            }
            else {
                // the current article has one flag set, convert it into array
                if (data.flag && typeof(data.flag[0]) === 'undefined' && flagType != 'section-head'){
                    data.flag = [{"_attributes":{"type": data.flag._attributes.type, "value": data.flag._attributes.value}}];
                }
                // loop through the available flags for current article.
                // if the flag type matches the current flagType then update its value, else add one
                var flagsArray = data.flag;
                var flagSet = false;
                var fpageVal = histObj.fpage;
                if(flagsArray){
					if (flagsArray.constructor.toString().indexOf("Array") < 0){
                        flagsArray = [flagsArray];
                        }
                flagsArray.forEach(function(currFlagObj){
                    if (currFlagObj._attributes.type == flagType){
                        currFlagObj._attributes.value = flagValue;
                        flagSet = true;
                    }
                });
                
                if (!flagSet){
                    flagsArray.push({"_attributes":{"type": flagType, "value": flagValue}});
                    //data.flag.push({"_attributes":{"type": flagType, "value": flagValue}});
                    data.flag = flagsArray
                }
            }
            }
            if(flagType == 'section-head'){
                var currSection = data._attributes.section
                data._attributes.section = flagValue;
                data.type = flagValue;
                var firstNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:first');
                var firstNodeID = parseInt(firstNode.attr('data-c-id'));
                var firstNodeData = $(firstNode).data('data')._attributes;
                var  firstNodefpageVal = parseInt(firstNodeData['fpage']);
                var firstSectionNode = $('#tocBodyDiv div[data-type="manuscript"][data-section="'+ flagValue +'"]:first');
                var firstSectionNodeIndex = parseInt(firstSectionNode.attr('data-c-id'));
                var currNode = $('[data-c-id="' + currNodeIndex + '"]');

                //if the current article is the first article of the section and it has follow-on articles, remove the run-on from next article
                var currSectionFirstNode = $('#tocBodyDiv div[data-type="manuscript"][data-section="'+ currSection +'"]:first').attr('data-c-id')
                if(currNodeIndex == currSectionFirstNode){
                    var nextNodeIndex = parseInt(currNodeIndex)+1
                    var nextNode =  $('[data-c-id="' + nextNodeIndex + '"]')
                    if (nextNode){
                        var nextNodeData = $(nextNode).data('data');
                        var nexRunOn = nextNodeData._attributes['data-run-on']
                        if(nexRunOn == 'true'){
                            $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex+1]._attributes['data-run-on'] = 'false'
                            var nexRunOnflag = nextNodeData.flag
                            if(nexRunOnflag){
                                if (nexRunOnflag.constructor.toString().indexOf("Array") < 0){
                                    nexRunOnflag = [nexRunOnflag];
                                    }
                                    nexRunOnflag.forEach(function(flag, rcIndex){
                                    if (flag._attributes['type'] == 'run-on' ){
                                        flag._attributes['value'] = '0';
                                    }
                                })
                            }
                
                        }
                    }
                }
                //Insert as First Article when moved to next Section
                if(currNodeIndex < firstSectionNodeIndex){
                    var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                    //move article as first node in sectionChild of new section
                    for(var currVal = currNodeIndex; currVal<firstSectionNodeIndex ; currVal++){
                        $('#tocContainer').data('binder')['container-xml'].contents.content[currVal] =  $('#tocContainer').data('binder')['container-xml'].contents.content[currVal + 1]
                    }
                    $('#tocContainer').data('binder')['container-xml'].contents.content[firstSectionNodeIndex-1] = tempData;
                }
                // Insert as Last Article when moved to previous Section
                else if(currNodeIndex > firstSectionNodeIndex){
                    //move article as last node of section
                    var lastSectionNode = $('#tocBodyDiv div[data-type="manuscript"][data-section="'+ flagValue +'"]:last');
                    var lastSectionNodeIndex = parseInt(lastSectionNode.attr('data-c-id'));
                    var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                    var temp = $('#tocContainer').data('binder')['container-xml'].contents.content[lastSectionNodeIndex];
                    for(var currVal = currNodeIndex; currVal > lastSectionNodeIndex ; currVal--){
                        
                        $('#tocContainer').data('binder')['container-xml'].contents.content[currVal] =  $('#tocContainer').data('binder')['container-xml'].contents.content[currVal - 1]
                    }
                    $('#tocContainer').data('binder')['container-xml'].contents.content[lastSectionNodeIndex+1] = tempData;
                }
                eventHandler.general.actions.saveData();
                eventHandler.general.components.populateTOC();
                updatePageNumber(firstNodeID ,parseInt(firstNodefpageVal), data._attributes["grouptype"]);
                eventHandler.general.actions.saveData();
                eventHandler.general.components.populateTOC();
            }

            if(flagType == 'run-on'){
            //if run-on flag is set, hide pagerange, set current fpage as start page for next node
                if(flagValue == 1){
                    data._attributes["data-run-on"]='true';
                    eventHandler.general.actions.saveData();
                    eventHandler.general.components.populateTOC()
                    var currNode = $('[data-c-id="' + currNodeIndex + '"]');
                    var currNodeSection = currNode.attr('data-section');
                    var currNodeData = $(currNode).data('data')._attributes;
					var sectionFirstNode = $(currNode).prevAll('[data-type="manuscript"][data-section="'+ currNodeSection +'"][data-run-on!="true"]:first')
                    var sectionFirstNodeIndex = parseInt(sectionFirstNode.attr('data-c-id'));
                    var sectionFirstData = $(sectionFirstNode).data('data')._attributes;
                    var sectionFirstFpageVal = parseInt(sectionFirstData.fpage)
                    var sectionFirstSubArt = sectionFirstData['data-subarticles'] ? sectionFirstData['data-subarticles']: ''
                    if(sectionFirstSubArt == ""){
                        sectionFirstData['data-subarticles'] = currNodeData.id
                    } 
                    else{
                            sectionFirstData['data-subarticles'] = sectionFirstSubArt.concat(','+currNodeData['id'])
                        
                    }
                    data._attributes["data-runon-with"]=sectionFirstData.id
                    updatePageNumber(sectionFirstNodeIndex,sectionFirstFpageVal,data._attributes["grouptype"]);
                }
                else{
                    data._attributes["data-run-on"]='false';
                    eventHandler.general.actions.saveData();
                    eventHandler.general.components.populateTOC()
                    var currNode = $('[data-c-id="' + currNodeIndex + '"]');
                    var currNodeSection = currNode.attr('data-section');
                    var currNodeData = $(currNode).data('data')._attributes;
                    var sectionFirstNode = $(currNode).prevAll('[data-type="manuscript"][data-section="'+ currNodeSection +'"][data-run-on!="true"]:first')
                    var sectionFirstData = $(sectionFirstNode).data('data')._attributes;
                    var sectionFirstSubArt = sectionFirstData['data-subarticles'] ? sectionFirstData['data-subarticles']: ''
                    if(sectionFirstSubArt == ""){
                        //parentArtNodeData['data-subarticles'] = currNodeData.id
                    } 
                    else{
                            var temp = sectionFirstSubArt.replace(currNodeData['id'],'')
                            temp = temp.replace(/\,\,/,'')
                            temp = temp.replace(/\,$/,'')
                            sectionFirstData['data-subarticles'] = temp
                       
                    }
                    var groupType = data._attributes["grouptype"]    
                    var prevNode = $(currNode).prevAll('[data-type="manuscript"][data-grouptype="'+ groupType +'"][data-run-on!="true"]:first')
                    var prevNodeData;
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                        fpageVal = parseInt(prevNodeData.lpage) + 1;
                    }
                    updatePageNumber(currNodeIndex,fpageVal,groupType);
                }
               
                eventHandler.general.components.populateTOC()
            }
            else if(flagType == 'retract'){
                //if run-on flag is set, hide pagerange, set current fpage as start page for next node
                    if(flagValue == 1){
                        data._attributes["data-retracted"]='true';
                        
                        var currNode = $('[data-c-id="' + currNodeIndex + '"]');
                        var currNodeSection = currNode.attr('data-section');
                        var currNodeData = $(currNode).data('data')._attributes;
    
                        var parentArtNode = $(currNode).prevAll('[data-type="manuscript"][data-section="'+ currNodeSection +'"][data-run-on!="true"]:first');
                        var parentArtNodeData = $(parentArtNode).data('data')._attributes;
                        var parentArtNodeSubArt = parentArtNodeData['data-subarticles'] ? parentArtNodeData['data-subarticles']: ''
                        if(parentArtNodeSubArt == ""){
                            //parentArtNodeData['data-subarticles'] = currNodeData.id
                        } 
                        else{
                                var temp = parentArtNodeSubArt.replace(currNodeData['id'],'')
                                temp = temp.replace(/\,\,/,'')
                                temp = temp.replace(/\,$/,'')
                                parentArtNodeData['data-subarticles'] = temp
                            
                        }
                        eventHandler.general.actions.saveData();
                        
                    }
                    else{
                        data._attributes["data-retracted"]='false';
                        var currNode = $('[data-c-id="' + currNodeIndex + '"]');
                        var currNodeSection = currNode.attr('data-section');
                        var currNodeData = $(currNode).data('data')._attributes;
    
                        var parentArtNode = $(currNode).prevAll('[data-type="manuscript"][data-section="'+ currNodeSection +'"][data-run-on!="true"]:first');
                        var parentArtNodeData = $(parentArtNode).data('data')._attributes;
                        var parentArtNodeSubArt = parentArtNodeData['data-subarticles'] ? parentArtNodeData['data-subarticles']: ''
                        if(parentArtNodeSubArt == ""){
                            parentArtNodeData['data-subarticles'] = currNodeData.id
                        } 
                        else{
                            parentArtNodeData['data-subarticles'] = parentArtNodeSubArt.concat(','+currNodeData['id'])
                        }
                        eventHandler.general.actions.saveData();
                        
                    }
                   
                    
                }
            else if(flagType == 'unpaginated'){
                if(flagValue == 1){
                    data._attributes["data-unpaginated"]='true';
                   // data._attributes["type"]= data._attributes["type"].replace(/\_unpaginated/g,'');
                    data._attributes["type"]= data._attributes["type"].concat('_unpaginated');
                    if(/^(body|epage)$/gi.test(data._attributes["grouptype"])){
                        eventHandler.general.actions.saveData();
                        eventHandler.general.components.populateTOC();
                        var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                        var prevNodeData,nextNodeData;
                        if(prevNode){
                            prevNodeData = $(prevNode).data('data')._attributes;
                            fpageVal = parseInt(prevNodeData.lpage) + 1;
                        }
                        else{
                            fpageVal = parseInt(histObj.fpage)
                        }
                         //fpageVal = parseInt(histObj.fpage) - 1;// 
                        updatePageNumber(currNodeIndex + 1 ,parseInt(fpageVal), data._attributes["grouptype"]);
                        
                    }    
                }
                else{
                    data._attributes["data-unpaginated"]='false';
                    data._attributes["type"]= data._attributes["type"].replace(/\_unpaginated/g,'');
                    //if(data._attributes["grouptype"] == 'body'){
                    if(/^(body|e\-page)$/gi.test(data._attributes["grouptype"])){
                        eventHandler.general.actions.saveData();
                        eventHandler.general.components.populateTOC()
                        var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                        var prevNodeData,nextNodeData;
                        if(prevNode){
                            prevNodeData = $(prevNode).data('data')._attributes;
                            fpageVal = parseInt(prevNodeData.lpage) + 1;
                        }
                        else{
                            var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
                                if(nextNode){
                                    nextNodeData = $(nextNode).data('data')._attributes;
                                    fpageVal = parseInt(nextNodeData.fpage);    
                                }
                        }
                        //fpageVal = parseInt(histObj.fpage) + 1;     // 
                        updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
                        
                    }    
                }
               
            }
			//change grouptype as 'epage' when epage flag is set
			else if(flagType == 'e-page'){
                if(flagValue == 1){
                    //  if previous node as epage, take prev node endPage as startpage and add 'e' as prefix
                // if previous node is not e-page, then take e-start page as fpage, fpage and lpage will be same  
                    var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                    var prevNodeData = '';
                    var endPageVal;
                        fpageVal = histObj.fpage;
                    var pageCount = data._attributes["pageExtent"] ? data._attributes["pageExtent"] : parseInt(0)
                        pageCount = parseInt(pageCount);
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                    }
                  // if previous node is epage, 'add 1 to previous node lpage' as fpage
                    if(prevNodeData && prevNodeData["grouptype"] == 'epage'){
                        fpageVal = prevNodeData['lpage']
                        if (/^(e)/gi.test(fpageVal)) { 
                            fpageVal = fpageVal.replace('e','')
                        } 
                        fpageVal = parseInt(fpageVal) + 1;
                     }
                    else{
                        // get e-start page as fpage
                        // if e-start page is not available get current fpage
                        var meta = $('#tocContainer').data('binder')['container-xml'].meta;
                        fpageVal = meta["e-first-page"] ?  meta["e-first-page"]['_text'] : histObj.fpage;
                       // endPageVal = fpageVal;// fpage as lpage based on config
                    }
                    data._attributes["grouptype"]='epage';
                    if(fpageVal == '' || /(undefined)/gi.test(fpageVal)){
                        fpageVal = 'e1'
                    }
                    eventHandler.general.actions.saveData(); // change grouptype as epage and save
                    if (/^(e)/gi.test(fpageVal)) { 
                        fpageVal = fpageVal.replace('e','')
                    }  
                    // if config data says e-lpage as false, set fpage and lpage as same 
                    if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')){
                        endPageVal = fpageVal;
                    }
                    else{
                         // calculate endpage based on page count
                        endPageVal = parseInt(fpageVal) + pageCount - 1; 
                    }
                    // add e as prefix for fpage and lpage
                    if (!(/^(e)/gi.test(fpageVal))) { 
                        fpageVal = 'e'.concat(fpageVal)
                    }
                    if (!(/^(e)/gi.test(endPageVal))) { 
                        endPageVal = 'e'.concat(endPageVal)
                    } 
                    // update fpage and lpage
                    data._attributes["fpage"]=fpageVal;
                    data._attributes["lpage"]=endPageVal;
                    var startpage = fpageVal.replace('e','')
                    updatePageNumber(currNodeIndex + 1 ,parseInt(startpage) + 1, 'epage');
                   //updatePageNumber(currNodeIndex ,fpageVal, 'epage');
                }
                else{
				//change grouptype as 'body' when epage flag is unset
                    data._attributes["grouptype"]='body';
                    var epage = parseInt(histObj.fpage)
                    fpageVal = histObj.fpage;
                   var endPageVal = histObj.lpage;
                    var pageCount = data._attributes["pageExtent"] ? data._attributes["pageExtent"] : parseInt(0)
                        pageCount = parseInt(pageCount)
                    var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                    var prevNodeData = '';
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                    }
                    if(prevNodeData && prevNodeData["grouptype"] == 'body'){
                        fpageVal = parseInt(prevNodeData['lpage'])+1;
                       // endPageVal = prevNodeData['lpage'];
                       
                    }
					// remove prefix 'e' from fpage and lpage
                    if (/^(e)/gi.test(fpageVal)) { 
                        fpageVal = fpageVal.replace('e','')
                   }
                   fpageVal = parseInt(fpageVal)
                    if(pageCount && pageCount != 0){
                        endPageVal = fpageVal + pageCount - 1; 
                    }
                   
                   if (/^(e)/gi.test(endPageVal)) { 
                       endPageVal = endPageVal.replace('e','')
                   }
                    data._attributes["fpage"]= fpageVal;
                    data._attributes["lpage"]= endPageVal;
                    eventHandler.general.actions.saveData();
                    
					// update the page number following epage articles 
                    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
                       var ePage = meta["e-first-page"] ?  meta["e-first-page"]['_text'] : parseInt(endPageVal)+1;
                    updatePageNumber(currNodeIndex + 1,ePage,'epage');
                }
                eventHandler.general.components.populateTOC();
               // updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
            }
            updateChangeHistory(histObj);
            if(flagType != 'section-head'){
            $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data', data);
            }
            eventHandler.general.actions.saveData();
        })
        // when the color checkbox is clicked update the figure status on the corresponding article data
        .on('click', '.color', function () {
            var figRef = $(this).closest('p').attr('rid');
            var color = 0;
            if ($(this).is(':checked')) {
                color = 1;
            }
            var dataCID = $('#msDataDiv').attr('data-c-rid');
            // get data from the corresponding article
            var data = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
            var histObj = JSON.parse(JSON.stringify(data._attributes));
          
        //for adverts/fillers
        if(data.region){
            var currRegion = $(this).attr('data-region-id');
            var regions = data.region;
            var layoutSec = $(this).attr('data-layout-sec');
            if (regions.constructor.toString().indexOf("Array") < 0){
                regions = [regions];
                }
            var regionsObj = {}, currIndex = 0;   
            regions.forEach(function(regionsObj, currIndex){
                if (regionsObj._attributes['type'] == currRegion){ 
                    //regionsObj['_attributes']['data-color'] = color
                    if(regionsObj['file']){
                        var files = regionsObj['file']
                        if (files.constructor.toString().indexOf("Array") < 0){
                            files = [files];
                        }
                        var file = {}, fIndex = 0;
                    
                        files.forEach(function(file, fIndex){
                            if (file._attributes['type'] == 'file' && layoutSec == file._attributes['data-layout']){ 
                                if (color) {
                                    file['_attributes']['data-color'] = 'cmyk'
                                }
                                else{
                                    file['_attributes']['data-color'] = ''
                                }
                            }
                        })
                    }
                    if (color) {
                        histObj.change = 'changed Advert/Filler <b>' + data._attributes.id +'</b> of region <b>'+ currRegion +'</b> to be color in print';
                        //data.region['_attributes']['data-color'] = 'cmyk'
                    }
                    else {
                        histObj.change = 'changed Advert/Filler ' + data._attributes.id + ' of region '+ currRegion +' to be greyscale in print';
                        //data.region['_attributes']['data-color'] = ''
                    }
                    updateChangeHistory(histObj);
                }
             })
        }
        if (data.color) {
            // if we have more than one figure then data.color will be array of object else will be an object
            if (data.color.constructor.toString().indexOf("Array") > -1) {
                data.color.forEach(function (dc) {
                    if (dc._attributes['ref-id'] == figRef) {
                        if (color) {
                            histObj.change = 'changed ' + dc._attributes.label + ' to be color in print';
                        }
                        else {
                            histObj.change = 'changed ' + dc._attributes.label + ' to be greyscale in print';
                        }
                        updateChangeHistory(histObj);
                        dc._attributes.color = color;
                    }
                });
            }
            else {
                if (color) {
                    histObj.change = 'changed ' + data.color._attributes.label + ' to be color in print';
                }
                else {
                    histObj.change = 'changed ' + data.color._attributes.label + ' to be greyscale in print';
                }
                updateChangeHistory(histObj);
                data.color._attributes.color = color;
            }
        }
            $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data', data);
            eventHandler.general.actions.saveData();
        })
		// when layout is changed, change the layout and populate the img src
		.on('change', 'select.layoutOption', function(e){
			var layoutType = '', layoutValue = 0;
			var layoutHTML = {};
            var dataCID = $('#adDataDiv').attr('data-c-rid');
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
                layoutType = $(this).attr('data-type');
                layoutValue = this.value; // get the changed layout value 
               // this.attributes["data-layout"]= layoutValue;
               this.selectedOptions[0].attributes['selected']=true;
            }
            var regionId =e.currentTarget.attributes["data-region-id"].value; // get the current region id
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
            var files = '';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                    var currRegionFlag = 'false';
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                                files = currDataObj.file;
                            }
                        }
                    })
                }
                    if(currRegionFlag == 'false'){
                        files = ({"_attributes":{"type": "file","data-region":currentReg, "data-layout":"a" ,"path":""}})
                    }
                }
            })
            var layoutHTML = $('#'+ regionId +'').html();
			$('#layout').removeClass('hide');
			$(targetNode).addClass('active');
			var regionHTML = '<div>'+ $('#'+ regionId +'').html() +'</div>';
			var layoutId = layoutValue.toLowerCase();
		    layoutId = layoutId.replace(/\s/,'');
			$('#'+ regionId +'').find(">div.layoutBlock").removeClass('hide');
			$('#'+ regionId +'').find(">div.layoutBlock").html(eventHandler.general.components.populateAdvert(currentReg,$('#'+layoutId+'').find('.layoutsection'),$('#'+ layoutId +'').html(),files)); // populate img path and change the layout
			//$('#'+ regionId +'').find(">div.layoutBlock").attr('id',layoutId); //change the layout id
			$('#'+ regionId +'').find(">div.layoutBlock").attr('data-layout',layoutValue); // change the data-layout
            //eventHandler.general.actions.saveData();
        })
        //when float-placement is changed, update the float-placement value in xml based on region
        .on('change', 'select.floatPlacement', function(e){
			var floatPlacementType = '', floatPlacementValue = 0;
			
            var dataCID = $('#adDataDiv').attr('data-c-rid');
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
               floatPlacementValue = this.value; // get the changed float-placement value 
              // this.attributes['data-floatPlacement'] = floatPlacementValue;
              this.selectedOptions[0].attributes['selected']=true;
              
               
            }
            var regionId =e.currentTarget.attributes["data-region-id"].value; // get the current region id
            var myData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
            var currRegionFlag = 'false';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                                //change the float placement value
                                currDataObj._attributes['data-float-placement'] = floatPlacementValue;
                            }
                        }
                    })
                }
                
                }
            })
            if(currRegionFlag == 'false'){
                var newRegion = ({"_attributes":{"type": currentReg,"layout":"","data-float-placement":floatPlacementValue},"file":{"_attributes":{"type": "file","data-region":currentReg, "data-layout": "a","path":""}}})
                if(currentData){
                currentData.push(newRegion)
                myData.region = currentData;
                }
                else{
                    myData.region = newRegion;
                }
               // eventHandler.general.actions.saveData();    
            }
            //$(this).closest('select').attr('data-float-placement',floatPlacementValue)
           // save the float placement value
            eventHandler.general.actions.saveData();
        })
        
        //when editable data is changed, update the data based on region
        .on('blur', '.af-data-editable', function(e){
			 // get the current region id
            var regionId =e.currentTarget.attributes["data-region-id"].value;
            // get the key-path
             var key =e.currentTarget.attributes["data-key-path"].value;
             var curVal = e.currentTarget.textContent;
            var myData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
                var currRegionFlag = 'false';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                   
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                               
                                //change the value
                                currDataObj._attributes[''+key+''] = curVal;
                            }
                        }
                    })
                }
                }
            })
            if(currRegionFlag == 'false'){
                var newRegion = ({"_attributes":{"type": currentReg,"layout":"" },"file":{"_attributes":{"type": "file","data-region":currentReg, "data-layout": "a","path":""}}})
                newRegion._attributes[''+key+''] = curVal;
                if(currentData){
                currentData.push(newRegion)
                myData.region = currentData;
                }
                else{
                    myData.region = newRegion;
                }
               // eventHandler.general.actions.saveData();  
            }
           // save the float placement value
            eventHandler.general.actions.saveData();
        })
        // when the editable data is changed update the corresponding data
        .on('blur', '.editable', function () {
            if (($(this).closest('.tabContent').length > 0) && ($(this).attr('data-dirty') == 'true')) {
                //if ($(this).closest('.tabContent').length > 0) {
                var myMeta = $('#tocContainer').data('binder')['container-xml'].meta;
                var keyPath = $(this).attr('data-key-path');
				var dataFlag = false
                var prevVal = '';
                if (keyPath) {
                    var keyPathArr = keyPath.split('.');
                    // the last key is required to update the data, if you include the key directly then we will get only the value
                    var lastKey = keyPathArr.pop();
                    keyPathArr.forEach(function (key) {
                        if( myMeta[key]){
                            myMeta = myMeta[key];
                            dataFlag = true;
                        }
                        else{
                            myMeta[key] = {}
                            myMeta = myMeta[key]
                        }
                    })
                    prevVal = myMeta[lastKey];
                    myMeta[lastKey] = $(this).text();
                }
                var histObj = {
                    "change": "changed <b>" + $(this).attr('data-label').replace(/^[\s\t\r\n]+|[\s\t\r\n\:]+$/, '') + "</b> from <u>" + prevVal + '</u> to <u>' + $(this).text() + '</u>',
                    "file": "",
                    "fileType": "",
                    "id-type": ""
                };
                updateChangeHistory(histObj);
                //call page re-numbering when start page is changed
                var label =$(this).attr('data-label').replace(/^[\s\t\r\n]+|[\s\t\r\n\:]+$/, '')
                label = label.replace(/[\s\t\r\n]+/,'')
                if(label.toLowerCase() == 'startpage'){
                    var firstNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:first');
                    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
                    updatePageNumber(firstNodeID,$(this).text(),'body');
                    eventHandler.general.components.populateTOC();
                }
                else if(label.toLowerCase() == 'e-startpage'){
                    var firstNode = $('#tocBodyDiv div[data-grouptype="epage"][data-c-id]:first');
                    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
                    var ePage = $(this).text().replace('e','');
                    updatePageNumber(firstNodeID,ePage,'epage');
                    eventHandler.general.components.populateTOC();
                }
				eventHandler.general.actions.saveData();
                $(this).attr('data-dirty', 'false');
            }
            // if its not in
            else {

            }
        })
        // when the contenteditable data is changed, update the corresponding data
        /* text with formats like bold/italic are saved */
        .on('blur', '.contenteditable', function () {
            //if (($(this).closest('#msDataDiv').length > 0) && ($(this).attr('data-dirty') == 'true')) {
            if (($(this).closest('.tabContent').length > 0) && ($(this).attr('data-dirty') == 'true')) {
                var dataCID = $('#msDataDiv').attr('data-c-rid');
                // get data from the corresponding article
                var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                var id = myData._attributes.id;
                var containerHTML = '';
                var dataFlag = false
                var keyPath = $(this).attr('data-key-path');
                if (keyPath) {
                    var keyPathArr = keyPath.split('.');
                    // the last key is required to update the data, if you include the key directly then we will get only the value
                    var lastKey = keyPathArr.pop();
                    keyPathArr.forEach(function (key) {
                        if( myData[key]){
                            myData = myData[key];
                            dataFlag = true;
                        }
                        else{
                            myData[key] = {}
                            myData = myData[key]
                        }
                        
                    })
                    var dArr = [];
                    var updatedHTML = $(this).html();
                    $(this).contents().each(function () {
                        if (this.nodeType == 3) {
                            var ceData = this.nodeValue;
                            ceData = ceData.replace(/^[\s\t\r\n\u00A0]+|[\s\t\r\n\u00A0]+$/gi, '');
                        }
                        else {
                            var ceData = $(this).html();
                            ceData = ceData.replace(/[\s\t\r\n\u00A0]*<\/?(p|br)\/?>[\s\t\r\n\u00A0]*/gi, '');
                            ceData = ceData.replace(/</g, '[[').replace(/>/g, ']]');
                        }
                        dArr.push({ _text: ceData });
                    });
                    var orgData = ''
                    if( myData[lastKey] && myData[lastKey]._text){
                        orgData = myData[lastKey]._text 
                    }
					if(orgData == '' && myData[lastKey] && myData[lastKey][0] &&  myData[lastKey][0]._text) {
						orgData = myData[lastKey][0]._text
					}
                    myData[lastKey] = dArr;
                    var histObj = {
                        "change": "changed <b>" + lastKey + "</b> of <i>"+myData._attributes.id +"</i> from <u>" + orgData + '</u> to <u>' + dArr[0]._text + '</u>',
                        "file": "",
                        "fileType": "",
                        "id-type": ""
                    };
                    updateChangeHistory(histObj);
                }
                eventHandler.general.actions.saveData();
                $(this).attr('data-dirty', 'false');
            }
            // if its not in
            else {

            }
        })
        // when the contenteditable data is changed update the corresponding data
        .on('input', '.editable, .contenteditable', function () {
            if ($(this).closest('.tabContent').length > 0 || ($(this).closest('#msDataDiv').length > 0)) {
                $(this).attr('data-dirty', 'true');
            }
        })
        .on('click', '.fileChooser', function () {
            $(this).parent().find('input:file').val('');
            $(this).parent().find('input:file').trigger('click');
            $('.loading-status').remove();
        })
        .on('change', 'input:file', function (e) {
            if($(this).closest('p').length > 0){
                $('<div class="loading-status">0%</div>').insertAfter($(this).closest('p'));
            }
            else if($(this).parents('.tabContent').length >0 && $(this).parents('.tabContent').find('p').length >0){
                $('<div class="loading-status">0%</div>').insertAfter($(this).parents('.tabContent').find('p').first());
            }
            var fileObj = e.target.files[0];
            var keyPath = $(this).attr('data-key-path');
            var advertLayoutSec = $(this).attr('data-layout-sec');
            var manualUpload = $(this).attr('data-manualUpload');
	    var proofType = $(this).attr('data-proofType') ? $(this).attr('data-proofType') :'';
            var currRegionVal = $(this).closest('div').parent().attr('data-region-id')
            var currLayoutVal = $(this).closest('div').parent().attr('data-layout')
            if(currRegionVal){
                currRegionVal = currRegionVal.toLowerCase();
            }
            var fileEle = this;
            var param = {
                file: fileObj,
                success: function (resp) {
                    if (resp.status.code == 200) {
                        showMessage({ 'text': eventHandler.messageCode.successCode[200][1], 'type': 'success' });
                        var filePath = '/resources/' + resp.fileDetailsObj.dstKey;
                        var fileExtension ="pdf"
                        if($('.loading-status').parents('.tabContent').length >0 && $('.loading-status').parents('.tabContent').find('object').length >0 && proofType=='print'){
                            $('.loading-status').parents('.tabContent').find('object').attr('data',filePath)
                            
                        }
                        $('.loading-status').remove();
                        if (keyPath) {
                            var dataCID = $('#msDataDiv').attr('data-c-rid');
                            // get data from the corresponding article
                            var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                              // if the type is manuscript and the uploaded file is xml , then save the xml in AIP
                            if((myData._attributes)&&(myData._attributes.type)&&(myData._attributes.type == 'manuscript')){
                                var fileExtension =  filePath.split('.');
                                fileExtension = fileExtension.pop();
                                if(fileExtension == 'xml'){
                                    if (typeof (FileReader) != "undefined") {
                                        var reader = new FileReader();
                                        reader.onload = function (e) {
                                            //var xmlDoc = $.parseXML(e.target.result);
                                            eventHandler.general.actions.saveXML('manuscript',e.target.result)
                                        }
                                        reader.readAsText(fileObj);
                                    }
                                    
                                }
								if(fileExtension == 'pdf'){
                                    var fileFlag = false;
                                    var files = myData['file']
                                    if (files.constructor.toString().indexOf("Array") < 0){
                                        files = [files];
                                    }
                                    files.forEach(function (file) {
                                        var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
                                        if(currProofType == proofType){
                                            file['_attributes']['path'] = filePath;
                                            fileFlag = true
                                        }
                                    })
                                    if(fileFlag == false){
                                        var file = ({"_attributes":{"type": "pdf","path": filePath,"proofType":proofType}})
                                        files.push(file)
                                        myData['file'] = files;
                                    }
                                    //on upload of manuscript pdf, get pageExtent from user as input, and update pageNumber
                                    var notify = new PNotify({
                                        text:'<div id="notifyForm" class="pageExtentText"><div class="pageExtent"><p>Type the page extent for the uploaded pdf</p><input class="pageExtent_update" type="text"/><input class=" pf-button btn btn-small" style="margin-right: 10px;" type="button" name="pageExtent_update" value="Save"/></div></div>',
                                            icon: false,
                                            width: 'auto',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            },
                                            addclass: 'stack-modal',
                                            stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
                                            insert_brs: false
                                        });
                                        notify.get().find('.pageExtentText').on('click', '[name=cancel]', function() {
                                            notify.remove();
                                        })
                                        notify.get().find('.pageExtentText').on('click', '[name=pageExtent_update]', function() {
                                            var pageExtent = $('.pageExtentText').find('input[class=pageExtent_update]').val()
                                            var startPage = parseInt(myData._attributes.fpage)
                                            var endPage = startPage + parseInt(pageExtent) - 1;
                                            myData._attributes.pageExtent = pageExtent;
                                            myData._attributes.lpage = parseInt(endPage);
                                            eventHandler.general.actions.saveData();
                                            var prevNode = $('[data-c-id="' + (dataCID - 1) + '"]');
                                             if(prevNode){
                                                var prevNodeData = $(prevNode).data('data')._attributes;
                                            //if prevNode groupType is same get fpage as startpage
                                                 if(prevNodeData.type && prevNodeData.type == 'manuscript'){
                                                    startPage =parseInt(prevNodeData.lpage)+1;
                                                 }
                                            }
                                        updatePageNumber(dataCID,startPage,myData._attributes.grouptype);
                                      //  eventHandler.general.actions.saveData();
                                        notify.remove();
                                    })
                                        
                                    if(proofType == 'print'){
                                        $(fileEle).closest('div').find('object').attr('data', filePath);
                                       var param={"processType" : "PDFPreflight","pdfLink" : filePath} 
                                       //on upload of print pdf, trigger preflight
                                       eventHandler.pdf.export.exportToPDF(param,targetNode);
                                       eventHandler.general.actions.saveData();
                                    }
                                  
                                }
                            }
                            else if(((myData._attributes)&&(myData._attributes.type)&&(myData._attributes.type == 'front-matter')) || (fileExtension == 'pdf' && $('#filterProject .active').attr('data-project-type') == 'book') || (fileExtension == 'pdf' && proofType != '')){
                                var files = myData['file']
                                var pdfFileFlag = false
                                    if (files.constructor.toString().indexOf("Array") < 0){
                                        files = [files];
                                    }
                                    files.forEach(function (file) {
                                        var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
                                        if(currProofType == proofType){
                                            file['_attributes']['path'] = filePath;
                                            pdfFileFlag = true
                                        }
                                    })
                                    if(pdfFileFlag == false){
                                        var file = ({"_attributes":{"type": "pdf","path": filePath,"proofType":proofType}})
                                        files.push(file)
                                        myData['file'] = files;
                                    }
                                    eventHandler.general.actions.saveData();
                            }
                            else{
                                var keyPathArr = keyPath.split('.');
                                // the last key is required to update the data, if you include the key directly then we will get only the value
                                var lastKey = keyPathArr.pop();
                                keyPathArr.forEach(function (key) {
                                   if( myData[key]){
                                        myData = myData[key];
                                    }
                                    else{
                                        myData[key] = {}
                                        myData = myData[key]
                                    }
                                })
                                myData[lastKey] = filePath;
                                if(manualUpload){
                                    var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                                    var manualUploadArr = manualUpload.split('.');
                                    var lastKey = manualUploadArr.pop();
                                    manualUploadArr.forEach(function (mkey) {
                                        myData = myData[mkey];
                                    }) 
                                    myData[lastKey] = "true";
                                }
                                $(fileEle).closest('div').find('object').attr('data', filePath);
                                eventHandler.general.actions.saveData();
                            }
                        }
                        else if (advertLayoutSec) { // if layout section is specifed, then loop through the region,layout and update the filepath 
						    var dataCID = $('#msDataDiv').attr('data-c-rid');
                            // get data from the corresponding article
                            var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                            var fileFlag = false;
                            var regionArr = myData? myData.region: ''
                            if(regionArr){
                            if (regionArr.constructor.toString().indexOf("Array") < 0){
                                regionArr = [regionArr];
                            }}
                            else{
                                regionArr = [];
                            }
                            var regionObj= {};
                            var rcIndex = 0;
                            var fileArray = [];
                            var regionFlagVal = false;
                            regionArr.forEach(function(regionObj, rcIndex){
                                if ((regionObj)&&(regionObj._attributes['type'] == currRegionVal) ){
                                    regionFlagVal = true;
                                    fileArray = regionObj.file; // get files from data
                                    if (fileArray.constructor.toString().indexOf("Array") < 0){
                                        fileArray = [fileArray];
                                    }
                                //match the layout sec of file uploaded with the layout sec of file in issueXML
                                    fileArray.forEach(function (file) {
                                        if(file._attributes['data-layout'] == advertLayoutSec){
                                            file._attributes['path'] = filePath;
                                            fileFlag = true;
                                        }
                                    })
                                    if (fileFlag == false){ // if file for the specified section is not available, then push the file into file Array
                                        var file = ({"_attributes":{"type": "file","data-region":currRegionVal, "data-layout": advertLayoutSec,"path":filePath}})
                                        fileArray.push(file)
                                    }
                                    regionObj["_attributes"]["layout"] = currLayoutVal; //change the layout value in xml
                                    regionObj["_attributes"]["value"] = "1";
                                    regionObj.file = fileArray;
                                    $(fileEle).closest('div').find('object').attr('data', filePath);
                                    eventHandler.general.actions.saveData();    
                                }
                            })
                            if (regionFlagVal == false){ // if region info is not available in data,add a new region
                                var newRegion = ({"_attributes":{"type": currRegionVal,"layout":currLayoutVal},"file":{"_attributes":{"type": "file","data-region":currRegionVal, "data-layout": advertLayoutSec,"path":filePath}}})
                                regionArr.push(newRegion)
                                if(myData){
                                    myData.region = regionArr;
                                }
                                else{
                                   $('[data-c-id="' + dataCID + '"]').data('data', regionArr);
                                }
                                $(fileEle).closest('div').find('object').attr('data', filePath);
                                eventHandler.general.actions.saveData();    
                            }
                        }
                    }
                    else {
                        showMessage({ 'text': eventHandler.messageCode.errorCode[501][3] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
                    }
                },
                progress: function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = Math.round(event.loaded / event.total * 100) + "%";
                        $('.loading-status').css('width', percentComplete).text(percentComplete);
                    }
                },
                error: function (err) {
                    showMessage({ 'text': eventHandler.messageCode.errorCode[501][3] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
                    $('.loading-status').remove();
			        }
                }
                var filePath = eventHandler.upload.uploadFile(param);
            });
    $('#dataContainer')
        //when signature/page type is changed, update the data in meta information
        .on('change', 'select.signature, .select_option select', function(e){
            var myMeta = $('#tocContainer').data('binder')['container-xml'].meta;
            var keyPath = $(this).attr('data-key-path');
            var prevVal = '';
            var currVal;
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
               currVal = this.value; // get the changed value 
               this.selectedOptions[0].attributes['selected']=true;
            }
            if (keyPath) {
                var keyPathArr = keyPath.split('.');
                // the last key is required to update the data, if you include the key directly then we will get only the value
                var lastKey = keyPathArr.pop();
                keyPathArr.forEach(function (key) {
					if( myMeta[key]){
						myMeta = myMeta[key];
						dataFlag = true;
					}
					else{
						myMeta[key] = {}
						myMeta = myMeta[key];
					}
                })
                prevVal = myMeta[lastKey] ? myMeta[lastKey]: '';
                myMeta[lastKey] = currVal;
            }
            var histObj = {
                "change": "changed <b>" + $(this).attr('data-label').replace(/^[\s\t\r\n]+|[\s\t\r\n\:]+$/, '') + "</b> from <u>" + prevVal + '</u> to <u>' + currVal + '</u>',
                "file": "",
                "fileType": "",
                "id-type": ""
            };
            updateChangeHistory(histObj);
             // push updated data to server
             eventHandler.general.actions.saveData();
        })        
    $('#tocBodyDiv')
        .on('mouseenter', '.sectionDataChild', function(e){
            // if you do not have another article following the current article then movedown button should not be visible
            if ($(this).next().hasClass('sectionDataChild')){
             //   $('.sectionDataChild').not('.hide').find('.controlIcons.icon-arrow_downward').removeClass('hide');
            }
            else{
               // $(this).find('.controlIcons.icon-arrow_downward').addClass('hide');
            }
        })
        .on('click', '.controlIcons', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var targetNode = $(e.target);
            if (targetNode.hasClass('move_down')) {//icon-arrow_downward
                // swap the article info in the container array
                var currNode = targetNode.closest('[data-c-id]');
                var currNodeIndex = parseInt(currNode.attr('data-c-id'));
                var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex] = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex + 1];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex + 1] = tempData;
                var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
                var nextNodeData = $(nextNode).data('data')._attributes;
               
                var startPage =histObj.fpage;
                if(histObj["data-run-on"] && histObj["data-run-on"] == 'true'){
                    if ((histObj.type=='filler')&&(/^(blank|advert|filler|filler-toc)$/gi.test($(nextNode).data('data')._attributes.type))){
                          // filler should run-on only after manuscript
                        showMessage({ 'text': eventHandler.messageCode.errorCode[502][2], 'type': 'error' ,'hide':false});
                        return false;
                    }
                    else{ 
                        startPage = parseInt(histObj.fpage) + 1;// if run-on article, then add 1 to fpage
                    }
                }
              
                histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' after ' + (nextNodeData['id-type'] ? nextNodeData['id-type'] : nextNodeData.type) + ' - ' + nextNodeData.id;
                currNode.attr('data-c-id', currNodeIndex + 1);
                nextNode.attr('data-c-id', currNodeIndex);
                currNode.insertAfter(nextNode);
                if(histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true'){
                    updatePageNumber(currNodeIndex,startPage,currNode.attr('data-grouptype'));
				}
                updateChangeHistory(histObj);
                // push updated data to server
                eventHandler.general.actions.saveData();
            }
            else if (targetNode.hasClass('move_up')) {//icon-arrow_upward
                // swap the article info in the container array
                var currNode = targetNode.closest('[data-c-id]');
                var currNodeIndex = parseInt(currNode.attr('data-c-id'));
                var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex-1];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex - 1] = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex] = tempData;
                var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                var prevNodeData = $(prevNode).data('data')._attributes;
                var startPage =prevNodeData.fpage;
                if(histObj["data-run-on"] && histObj["data-run-on"] == 'true'){ //check if run-on article
                    var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
                    var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
                    // filler should run-on only after manuscript
                    if ((histObj.type=='filler')&&(/^(blank|advert|filler)$/gi.test($(secondPrevNode).data('data')._attributes.type))){
                        showMessage({ 'text': eventHandler.messageCode.errorCode[502][2], 'type': 'error' ,'hide':false});
                        return false;
                    }
                    else{ // if run-on article, then set 2nd previous node lpage as fpage
                        startPage = parseInt(secondPrevNodeData.lpage);
                    }
                }
                 if(prevNodeData["data-unpaginated"] && prevNodeData["data-unpaginated"] == 'true'){
                    var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
                    if(secondPrevNode){
                        var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
                        startPage = parseInt(secondPrevNodeData.lpage) +1;
                    }
                    else{
                        startPage = parseInt(histObj.fpage);
                    }
                 }
                histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' before ' + (prevNodeData['id-type'] ? prevNodeData['id-type'] : prevNodeData.type)  + ' - ' + prevNodeData.id;
                currNode.attr('data-c-id', currNodeIndex - 1);
                prevNode.attr('data-c-id', currNodeIndex);
                currNode.insertBefore(prevNode);
                //if next node is filler, move filler along with article
                /*if(nextNodeData.type == 'filler'){
                    prevNode.attr('data-c-id', currNodeIndex + 1);
                    nextNode.attr('data-c-id', currNodeIndex);
                    nextNode.insertAfter(currNode);
                }*/
                updateChangeHistory(histObj);
                if((histObj["data-unpaginated"] != 'true') && (histObj["data-run-on"] != 'true')){
                    eventHandler.general.actions.saveData();
                  updatePageNumber(currNodeIndex - 1,startPage,currNode.attr('data-grouptype'));
                }
                // push updated data to server
                eventHandler.general.actions.saveData();
            }
            else if (targetNode.hasClass('downloadK1Article')) {//icon-download2
                $('#insertK1ArticleModal').find('select[id=sectionList]').html('')
                $('#insertK1ArticleModal').find('input[id=insert_K1Article]').val('')
                var currentSectionhead = $(targetNode).parent().parent().text();
                var sectionHTML='';
                var flagValues = eventHandler.general.components.getAvailableSections()
                flagValues.forEach(function(selectValue, sIndex){
                    var isSelected = '';
                    sectionHTML += '<option value="' + selectValue + '"' + isSelected +'>' + selectValue + '</option>'
                })
                $('#insertK1ArticleModal').find('select[id=sectionList]').append(sectionHTML)
                $('#insertK1ArticleModal').modal('show'); 
            }
            else if (targetNode.hasClass('remove_card')) {//icon-close
                (new PNotify({
                    title: 'Confirmation Needed',
                    text: 'Are you sure you want to remove the manuscript?',
                    hide: false,
                    confirm: { confirm: true },
                    buttons: { closer: false, sticker: false },
                    history: { history: false },
                    addclass: 'stack-modal',
                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                })).get()
                    .on('pnotify.confirm', function () {
                        var currNode = targetNode.closest('[data-c-id]');
                        var currNodeIndex = currNode.attr('data-c-id');
                        // remove the deleted manuscript's from the container
                        var removedData = JSON.parse(JSON.stringify($('#tocContainer').data('binder')['container-xml'].contents.content.splice(currNodeIndex, 1)));
                        removedData[0]._attributes.lpage = removedData[0]._attributes.lpage - removedData[0]._attributes.fpage + 1;
                        removedData[0]._attributes.fpage = 1;
                        var removedDataID = $('#uaaDiv > div:last').attr('id');
                        var removedDataID = removedDataID ? (parseInt(removedDataID) + 1) : 100000;
                        if (removedData[0]._attributes.type == 'manuscript'){
                        $('#uaaDiv').append(populateUAA(removedData[0], removedDataID));
                            $('#' + removedDataID).data('data', removedData[0]);
                        }
                        var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                        histObj.change = 'Removed ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id;
                        updateChangeHistory(histObj);
                        var nextNodeIndex = parseInt(currNodeIndex)+1
                        var nextNode =  $('[data-c-id="' + nextNodeIndex + '"]')
                        // remove manuscript node from UI
                        currNode.remove();
                        var lastNode = $('#tocBodyDiv div[data-c-id]:last');
                        var lastNodeID = parseInt(lastNode.attr('data-c-id'));
                        var tempID = parseInt(currNodeIndex) + 1 
                        var seqID = parseInt(lastNode.attr('data-c-id')) -1;
                        var entry = nextNode.data('data')
                        //change data-c-id for contents below the removed content 
                        if(lastNode.length > 0 && nextNode.length >0){
                            lastNodeID = parseInt(lastNode.attr('data-c-id'));
                            while(lastNodeID != tempID){
                                lastNode.attr('data-c-id', seqID--);
                                if($(lastNode).prevAll('[data-c-id]:first').length > 0){
                                    lastNode = $(lastNode).prevAll('[data-c-id]:first')
                                }
                                else if($(lastNode).parent().prevAll().find('[data-c-id]:last').length >0){
                                    lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
                                } 
                                else{
                                    lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
                                }
                               lastNodeID = parseInt(lastNode.attr('data-c-id'));
                            }
                            lastNode.attr('data-c-id', seqID)
                        }
                        // update page number for the contents below removed Node
                        if (nextNode){
                            var startPage =histObj.fpage;
                            if(histObj["data-run-on"] && histObj["data-run-on"]=='true'){ // if run-on article, add 1 to the fpage
                                var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                                var prevNodeData = $(prevNode).data('data')._attributes;
                                startPage = parseInt(prevNodeData.lpage) + 1;
                            }
                            if(histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true'){
                                updatePageNumber(nextNode.attr('data-c-id'),startPage ,histObj['data-grouptype']);
                            }
                        }
                        // push updated data to server
                        eventHandler.general.actions.saveData();
                    })
                    .on('pnotify.cancel', function () { });
            }
            //icon-add
            else if (targetNode.hasClass('addCard') || targetNode.hasClass('addContent')) {
                var currNode = targetNode.closest('[data-c-id]');
                if(currNode.length == 0){
                    var currSec = targetNode.closest('.sectionData')
                    currNode = currSec.next()
                }
                var currNodeIndex = parseInt(currNode.attr('data-c-id'));
                var groupType = 'body'
                if(currNode.length > 0 && currNode.data('data')._attributes && currNode.data('data')._attributes.grouptype && currNode.data('data')._attributes.grouptype!='' ){
                    groupType = currNode.data('data')._attributes.grouptype
                }
                loadBlankOrAdvertOrFiller($('#filterProject .active').attr('data-project-type'),groupType)
                $('#metaHistory, #manuscriptData, #advertsData').addClass('hide');
				$('#unassignedArticles').removeClass('hide');
            }
			// to edit section head
			else if (targetNode.hasClass('editSectionHead')) {//icon-edit
                var currentSectionhead = $(targetNode).parent().prev('.sectionHead').text()
                if(currentSectionhead != ''|| currentSectionhead != undefined){
                    currentSectionhead = currentSectionhead.trim()
                }
                $('#changeSectionHead').find('input[class~=sectionHead_change]').val(currentSectionhead)
                $('#changeSectionHead').find('input[class~=sectionHead_change]').attr('data-org-data',currentSectionhead)
                $('#changeSectionHead').modal('show');
            }
        })
        .on('dragenter', '.sectionDataChild', dragEnter)
        .on('dragleave', '.sectionDataChild', dragLeave)
        .on('dragover', '.sectionDataChild', dragOver)
        .on('drop', '.sectionDataChild', drop)
        .on('dragenter', '.sectionData', dragEnter)
        .on('dragleave', '.sectionData', dragLeave)
        .on('dragover', '.sectionData', dragOver)
        .on('drop', '.sectionData', drop)
        .on('dragenter', '.sectionHeader', dragEnter)
        .on('dragleave', '.sectionHeader', dragLeave)
        .on('dragover', '.sectionHeader', dragOver)
        .on('drop', '.sectionHeader', drop)
    $('#uaaDiv, #advertsFillersTab, #bookCardsTab')
        .on('dragstart', '.uaa', dragStart)
        .on('dragend', '.uaa', dragEnd)
}
function showAddChapter(){
    if ($('#filterProject').find('.active').length > 0 && $('#filterProject').find('.active').attr('data-project-type')=="book"){
        $('.addJob').removeClass('hide');
    }
    else{
        $('.addJob').addClass('hide');
    }
}
function loadUnassignedArticles(){
    $('#uaaDiv').html('');
   // var cName = $('.list.folder.active').attr('data-customer');
   // var pName = $('.list.folder.active').attr('data-project');
    var cName = $('#projectVal').attr('data-customer');
    var pName = $('#projectVal').attr('data-project');
    var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&process=getArticlesXML', 'objectName': 'list' };
    eventHandler.general.actions.getDataFromURL(obj)
        .then(function (project) {
            if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
                showMessage({ 'text': eventHandler.messageCode.errorCode[500][10] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
                return false;
            }
            try{
                var pData = project.data['container-xml'].contents;
                if (typeof(pData.content) === 'undefined'){
                    return true;
                }
                var pData = project.data['container-xml'].contents.content;
                if (pData.constructor.toString().indexOf("Array") == -1) {
                    pData = [pData];
                }
                var pLen = pData.length;
                var pHTML = '';
                var tempID = 10000
                pData.forEach(function(entry){
                    $('#uaaDiv').append(populateUAA(entry, tempID));
                    $('#' + tempID++).data('data', entry);
                });
                $('#uaaDiv').css('height', (window.innerHeight - $('#uaaDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
            }
            catch(e){
                showMessage({ 'text': eventHandler.messageCode.errorCode[500][10] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
                $('.la-container').fadeOut();
            }
        })
        .catch(function (err) {
            showMessage({ 'text': eventHandler.messageCode.errorCode[500][10] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
        })
}
// function to load card for blankpage/advert/filler 
function loadBlankOrAdvertOrFiller(projectType,groupType){
    $('advertsFillersTab').html('')
    var container =  $('#advertsFillersTab').html('')
    if(projectType && projectType == 'book'){
        container = $('#bookCardsTab').html('')
    }
    var sectionConfig = configData['section-cards'] ? JSON.parse(JSON.stringify(configData['section-cards'])).card : ''
    if (sectionConfig.constructor.toString().indexOf("Array") < 0){
        sectionConfig = [sectionConfig];
    }
    var scObj = {}, scIndex = 0;
    var sectionHTML = '';
    var tempID = '21000';
    var prevFlagSection = ''
    sectionConfig.forEach(function(scObj, scIndex){
        var entryId = parseInt(tempID) + parseInt(scIndex)
        if(scObj){
            var section = scObj._attributes['label'] ? scObj._attributes['label'] : '';
            var fpage = lpage = 1;
        var dataUuid = uuid.v4();
            var pageCount = lpage - fpage + 1
            var dataType = scObj._attributes['data-type']
            if (dataType && dataType != undefined && dataType.match(groupType)){
                if(scObj._attributes.groupType == 'roman'){
                    fpage = lpage = 'i'
                    pageCount = fromRoman(lpage) - fromRoman(fpage) + 1
                }
                var cName = $('#projectVal').attr('data-customer');
                var pName = $('#projectVal').attr('data-project');
                var blankPdfPath =''
                // var entry = ({"_attributes":{"id": entryId, "section": section, "grouptype": scObj._attributes.groupType,"type": scObj._attributes.type, "fpage": fpage,"lpage": lpage},"file":{"_attributes":{"proofType":"print","type":"pdf","path":blankPdfPath}},"title":{"_text":scObj._attributes['label']},"file":{"_attributes":{"proofType":"online","type":"pdf","path":blankPdfPath}}});
                var entry = ({"_attributes":{"id": entryId, "data-uuid":dataUuid, "section": section, "grouptype": scObj._attributes.groupType,"type": scObj._attributes.type, "fpage": fpage,"lpage": lpage,"pageExtent":pageCount},"title":{"_text":scObj._attributes['label']}});
                var fileObj = []
                var file = ({"_attributes":{"proofType":"online","type":"pdf","path":""}})
                if(scObj._attributes.type == 'blank'){
                    blankPdfPath = 'https://kriya2.kriyadocs.com/resources/' + cName + '/' + pName +'/resources/' + cName.toUpperCase() + '_' + pName.toUpperCase() + '_BLANK.pdf?method=server';
                    file = ({"_attributes":{"type":"pdf","path":blankPdfPath}})
                }
                else if(scObj._attributes.type == 'advert' || scObj._attributes.type == 'advert_unpaginated'){
                    file = ''
                }
                if(file !=''){
                    entry['file'] = file
                }
                sectionHTML = $('#contentCard').find('.uaa').clone();
    
                if(scObj._attributes['data-unpaginated'] && scObj._attributes['data-unpaginated']== 'true'){
                $(sectionHTML).attr('data-unpaginated','true')
                 entry._attributes['data-unpaginated'] = 'true'
                }
                $(sectionHTML).attr('id',tempID)
                $(sectionHTML).attr('fpage',entry._attributes.fpage)
                $(sectionHTML).attr('lpage',entry._attributes.lpage)
                $(sectionHTML).find('.msType').attr('title',entry._attributes.type)
                $(sectionHTML).find('.msType').text(scObj._attributes['label'])
                $(sectionHTML).find('.msID').text(entry._attributes.id)
                $(sectionHTML).find('.msPCount .msPCountVal').text(pageCount)
                container.append(sectionHTML)
                $('#' + tempID++).data('data', entry);
            
            }
        }
        scIndex++;
    })
    $('#unassignedArticles #bookCardsTab').css('height', (window.innerHeight -  $('#detailsContainer .bodyDiv').offset().top - $('#uaaTab').offset().top - $('#footerContainer').height() - 10) + 'px');
    $("#unassignedArticles #bookCardsTab").mCustomScrollbar({
        axis:"y", // vertical scrollbar
        theme:"dark"
    });
}
function loadSectionCards(){
    var container = $('#bookCardsTab');
    var sectionConfig = JSON.parse(JSON.stringify(configData.sections)).section;
    if (sectionConfig.constructor.toString().indexOf("Array") < 0){
        sectionConfig = [sectionConfig];
    }
    var scObj = {}, scIndex = 0;
    var sectionHTML = '';
    var tempID = '11000';
    var prevFlagSection = ''
    sectionConfig.forEach(function(scObj, scIndex){
        var entryId = parseInt(tempID) + parseInt(scIndex)
        var section = scObj._attributes['label'] ? scObj._attributes['label'] : '';
        if(scObj._attributes['section']){
            section = scObj._attributes['section']
        }
        var fpage = lpage = 1;
        var pageCount = lpage - fpage + 1
        if(scObj._attributes.groupType == 'roman'){
            fpage = lpage = 'i'
            pageCount = fromRoman(lpage) - fromRoman(fpage) + 1
        }
        var dataUuid = uuid.v4();
        var entry = ({"_attributes":{"id": entryId,"data-uuid":dataUuid, "section": section, "grouptype": scObj._attributes.groupType,"type": scObj._attributes.type, "fpage": fpage,"lpage": lpage,"pageExtent" : pageCount},"file":{"_attributes":{"proofType":"print","type":"pdf","path":""}},"title":{"_text":scObj._attributes['label']},"file":{"_attributes":{"proofType":"online","type":"pdf","path":""}}});
        sectionHTML = '<div class="uaa" draggable="true" id="' + tempID + '" fpage="'+ entry._attributes.fpage +'" lapge="'+ entry._attributes.lpage +'">';
        sectionHTML += '<p class="row">';
        sectionHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType"  title="' + entry._attributes.type + '">'+scObj._attributes['label']+'</span>';
        sectionHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
        sectionHTML += '</p>'
        sectionHTML += '<p class="row">';
        sectionHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msPCount">Page Count: <b>' + pageCount + '</b></span>';
        sectionHTML += '</p>'
        sectionHTML += '</div>';
        container.append(sectionHTML)
        $('#' + tempID++).data('data', entry);
        scIndex++;
    })
    $('#unassignedArticles #bookCardsTab').css('height', (window.innerHeight -  $('#detailsContainer .bodyDiv').offset().top - $('#uaaTab').offset().top - $('#footerContainer').height() - 10) + 'px');
    $("#unassignedArticles #bookCardsTab").mCustomScrollbar({
        axis:"y", // vertical scrollbar
        theme:"dark"
    });
}
function populateUAA(entry, tempID){
    if(entry._attributes.type == 'blank'){
        containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
        containerHTML += '<p class="row">';
        containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry._attributes.type + '">' + entry._attributes.type + '</span>';
        containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
        containerHTML += '</p>';
        containerHTML += '<p class="row">';
    }
    containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry.type._text + '">' + entry.type._text + '</span>';
    containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
    var accDate = '';
    if (typeof(entry.accDate) != 'undefined'){
        accDate = new Date(entry.accDate._text);
        accDate = (accDate ? accDate : '').toDateString().replace(/^[a-z]+ /gi, '');
    }
    containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msAccDate" title="' + accDate + '">Acc. Date: <b>' + accDate + '</b></span>';
    containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msPCount">Page Count: <b>' + (entry._attributes.lpage - entry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
    var titleText = 'Title Text';
    if (entry.title && entry.title._text) {
        titleText = entry.title._text;
    }
    containerHTML += '<span class="col-sm-12 col-md-12 col-lg-12 msText">' + titleText + '</span>';
    containerHTML += '</p>'
    containerHTML += '</div>';
    return containerHTML;
}

/**
 * update the page number starting from a given node
 * the renumbering starts from a particular node either a manuscript was dropped, moved up/down the order, new article has been added, when the pageExtent of manuscript is modified while proofing
 * input will be the node to start numbering from ,start page number from where numbering should start and groupType of the node
 */
function updatePageNumber(startNodeIndex, startPage, groupType){
    var firstNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:first');
    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
    var lastNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:last');
    var lastNodeID = parseInt(lastNode.attr('data-c-id'));
    startNodeIndex = parseInt(startNodeIndex);
    if (/^(e)/gi.test(startPage)) { 
        startPage = startPage.replace('e',''); 
    }
    else if(groupType!='' && groupType!=undefined && groupType== 'roman' && typeof startPage != "number"){
        startPage = fromRoman(startPage)
    }
    startPage = parseInt(startPage);
     //get pagecount of front contents
    var frontContents = $('#tocBodyDiv div[data-c-id][data-grouptype="front"]');
    var frontContentsCount = parseInt(0);
        frontContents.each(function(){
            var currFrontNode = $(this);
            var currFrontNodeID = parseInt(currFrontNode.attr('data-c-id'));
            var currFrontNodeData = currFrontNode.data('data');
            var pageExtent = parseInt(currFrontNodeData._attributes.pageExtent);
            frontContentsCount += pageExtent;
        });
    var datagroupType = '';
    if(groupType!='' && groupType!=undefined){
        datagroupType = "[data-grouptype='"+groupType+"']";
    }
    /* if group type is roman, update only the articles/adverts with same sectionhead*/
    if(groupType!='' && groupType!=undefined && groupType== 'roman'){
        var current = $('[data-c-id="' + (startNodeIndex) + '"]');
        var currentData = $(current).data('data')._attributes;
        var section = currentData["section"]
        if(section!='' && section!=undefined ){
          datagroupType += "[data-section='"+section+"']";
        }
    }
    var nodesToUpdate = $("#tocBodyDiv [data-c-id][data-type!='advert_unpaginated'][data-run-on!='true']"+datagroupType).filter(function() {
        return parseInt($(this).attr("data-c-id")) >= startNodeIndex;
    });
    nodesToUpdate.each(function(){
        var currNode = $(this);
        var currNodeID = parseInt(currNode.attr('data-c-id'));
        var currNodeData = currNode.data('data');
        try{
            var fpage = currNodeData._attributes.fpage;
            var lpage = currNodeData._attributes.lpage;
            if (/^(e)/gi.test(fpage)) { 
               fpage =  fpage.replace('e','');
            }
            if (/^(e)/gi.test(lpage)) { 
                lpage = lpage.replace('e','');
            }
            if( groupType== 'roman' && typeof fpage != "number"){
                fpage = fromRoman(fpage)
                lpage = lpage ? fromRoman(lpage) :lpage
            }
            fpage = parseInt(fpage);
            lpage = parseInt(lpage);
            lpage = lpage ? lpage : fpage;
            var pageCount = lpage - fpage + 1;
            if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
                pageCount =  currNodeData._attributes['pageExtent'];
            }
            pageCount = parseInt(pageCount);
            var runonFlag = currNodeData._attributes['data-run-on'];
            runonFlag = runonFlag ? runonFlag : 'false';
            currNodeData._attributes['pageExtent'] = pageCount;
            /*if (fpage == startPage){
                return true;
            }*/
            //if (currNodeData._attributes.type == 'manuscript'){
            if (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(currNodeData._attributes.type))) {   
                // if fresh-recto and pagecount of front contents is odd, return true for first article 
                if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (currNodeID == firstNodeID) && (frontContentsCount%2 != 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
               // if the configuration says fresh-recto is true, then startPage should always start on odd page, if not show error  
                else if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (startPage%2 == 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
					$(currNode).find('.pageRange').attr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
                else if ((configData['page']) && configData['page']._attributes['type'].toLowerCase() == 'continuous-publication') {
                    if((configData['page']._attributes['first-page'] == 'recto') ){
                        // if first page is recto  and pagecount of front contents is odd, return true for first article
                        if((currNodeID == firstNodeID) && (frontContentsCount %2 != 0) ){
                            $(currNode).find('.pageRange').addClass('incorrectPage');
							$(currNode).find('.pageRange').attr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'true';
                        }
                         //if the configuration says continuous-publication and first page is recto , first page should always starts on odd page, if not show error
                       else if((currNodeID == firstNodeID) && (startPage %2 == 0) ){
                            $(currNode).find('.pageRange').addClass('incorrectPage');
							$(currNode).find('.pageRange').attr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'true';
                        }
                        else{
                            $(currNode).find('.pageRange').removeClass('incorrectPage');
							$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'false';
                        }
                    }
                    else{
					//if the configuration says continuous-publication and first page is not recto , page can start in odd or even
                        $(currNode).find('.pageRange').removeClass('incorrectPage');
						$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                        currNodeData._attributes.incorrectPage = 'false';
                    }
                }
                else{
                    $(currNode).find('.pageRange').removeClass('incorrectPage');
					$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'false';
                }
            }
           
            if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
                currNodeData._attributes.fpage = 'e'.concat(startPage);
                var endPage = startPage + pageCount - 1;
                 // if config data says e-lpage as false, set fpage and lpage as same 
                if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')){
                    endPage = startPage;
                }
                currNodeData._attributes.lpage  = 'e'.concat(endPage);
             }
             else{
                var endPage = startPage + pageCount - 1;
                currNodeData._attributes.fpage = startPage;
                currNodeData._attributes.lpage = endPage;
                if( groupType== 'roman' && typeof startPage == "number"){
                    currNodeData._attributes.fpage = toRoman(startPage);
                    currNodeData._attributes.lpage = toRoman(endPage);
                   
                }
             }
            var unpaginated = currNodeData._attributes['data-unpaginated'];
                unpaginated = unpaginated ? unpaginated : 'false';
            // end page of last artricle should always be even
            if(unpaginated != 'true'){
                if((currNodeID == lastNodeID) && (endPage %2 != 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
                    $(currNode).find('.pageRange').attr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
            } 
               //if page range is modified, add class pageModified 
              //if(currNode.find('.rangeStart').text() != startPage || currNode.find('.rangeEnd').text() != endPage){
                if(currNode.find('.pageRange').attr('data-fpage') != startPage || currNode.find('.pageRange ').attr('data-lpage') != endPage){
					if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
						currNode.find('.rangeStart').text('e'.concat(startPage));
						currNode.find('.rangeEnd').text('e'.concat(endPage));
						currNode.attr('data-modified', 'true');
						currNodeData._attributes['data-modified'] = 'true';
                        $(currNode).find('.pageRange').attr('title', 'pageModified');
                        currNode.find('.pageRange').attr('data-fpage','e'.concat(startPage));
                        currNode.find('.pageRange').attr('data-lpage','e'.concat(endPage));
					   
					}
					else{
						currNode.find('.rangeStart').text(startPage);
                        currNode.find('.rangeEnd').text(endPage);
                        currNode.find('.pageRange').attr('data-fpage',startPage);
                        currNode.find('.pageRange').attr('data-lpage',endPage);
                            if( groupType== 'roman' && typeof startPage == "number"){
                                currNode.find('.rangeStart').text(toRoman(startPage));
                                currNode.find('.rangeEnd').text(toRoman(endPage));
                                currNode.find('.pageRange').attr('data-fpage',toRoman(startPage));
                                currNode.find('.pageRange').attr('data-lpage',toRoman(endPage));
                               
                            }
						if(unpaginated != 'true' && currNodeData._attributes && currNodeData._attributes.type != 'blank'){  
							currNode.attr('data-modified', 'true');
							$(currNode).find('.pageRange').addClass('pageModified');
							$(currNode).find('.pageRange').attr('title', 'Page is Modified');
							currNodeData._attributes['data-modified'] = 'true';
						}
					}
				}
            var nextNodeData = $('[data-c-id="' + (parseInt(currNode.attr('data-c-id')) + 1) + '"]').data('data');
            var nextRunOn = nextNodeData._attributes['data-run-on'];
            nextRunOn = nextRunOn ? nextRunOn : 'false';
            var nextUnpaginated = nextNodeData._attributes['data-unpaginated'];
                nextUnpaginated = nextUnpaginated ? nextUnpaginated : 'false';
          
                startPage = endPage + 1    // next manuscript should start from next page
        
            return true;
        }
        catch(e){
            return false;
        }
    });
    eventHandler.general.actions.saveData();
    //calculate total pages
    updateTotalPages();
}
/*  calculate - total page number add the extent of articles,adverts,fillers*/
function updateTotalPages(){
    //get all contents - add pageExtent for each contents
    var totalPages =parseInt(0);
    var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
    if(content){
        if (content.constructor.toString().indexOf("Array") == -1) {
            content = [content];
        }
        content.forEach(function (entry) {
            if(entry){
            var grouptype = entry._attributes.grouptype ? entry._attributes.grouptype : '';
            var section = entry._attributes.section ? entry._attributes.section : '';
            var runOn = entry._attributes['data-run-on'] ? entry._attributes['data-run-on'] : 'false';
            if(!(/^(cover|ifc|ibc|obc)/gi.test(section)) && runOn == 'false' && grouptype!='epage'){
                var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : parseInt(0);
                pageExtent = parseInt(pageExtent);
                totalPages += pageExtent;
            }
            }
        });
    }    
    //update the total page number value in MetaData
    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
    var pagecount = meta["page-count"] ?  meta["page-count"]['_text'] : parseInt(0);
    if(totalPages != null && totalPages != 0 && meta["page-count"]){
        meta["page-count"]['_text'] = totalPages
    }
    // update last page number in meta info
    var lastNode = $('#tocBodyDiv div[data-c-id][data-type="manuscript"][data-grouptype="body"][data-run-on!="true"]:last')
    var lastNodeData;
    if(lastNode && $(lastNode).data('data') && $(lastNode).data('data')._attributes){
        lastNodeData = $(lastNode).data('data')._attributes
    }
    else{
        $(lastNode)._attributes
    }
    if(lastNodeData && meta["last-page"]){
        var lastNodeLPage = lastNodeData.lpage
        meta["last-page"]['_text'] = lastNodeLPage
    }
}
/*https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
Function to convert integer to roman 
input - intger val e.g, toRoman(4)*/
function toRoman(num) {  
    var result = '';
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
    for (var i = 0;i<=decimal.length;i++) {
      while (num%decimal[i] < num) {     
        result += roman[i];
        num -= decimal[i];
      }
    }
    return result;
  }
  /*https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
Function to convert roman to integer 
input - roman numeral  e.g, fromRoman('iii')*/
function fromRoman(str) {  
	var result = 0;
	// the result is now a number, not a string
	var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
	var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
	for (var i = 0;i<=decimal.length;i++) {
	  while (str.indexOf(roman[i]) === 0){
		result += decimal[i];
		str = str.replace(roman[i],'');
	  }
	}
	return result;
}

// to export excel from issue xml data
(function ($) {
    var $defaults = {
        containerid: null
        , datatype: 'table'
        , dataset: null
        , columns: null
        , returnUri: false
        , worksheetName: "My Worksheet"
        , encoding: "utf-8"
    };
    var $settings = $defaults;
    $.fn.excelexportjs = function (options) {
        $settings = $.extend({}, $defaults, options);
        var gridData = [];
        var excelData;
        return Initialize();		
		function Initialize() {            
            gridData = $settings.dataset;
            excelData = Export(ConvertDataStructureToTable());               
            if ($settings.returnUri) {
                return excelData;
            }
            else {
                if (!isBrowserIE())
                {
                    window.open(excelData); 
                }  
            }
        }      
        function ConvertDataStructureToTable() {            
            var metaInfo = $('#tocContainer').data('binder')['container-xml']['meta'];                    
            var totalPages =  metaInfo["totalPageNo"];
            var result = "<table border='2px'><thead><tr><td colspan='8' style='font-weight:bold; font-size:24pt; text-align:center;'>";                
            result += "Issue make up</tr><tr><td colspan='8' style='font-weight:bold; font-size:18pt; text-align:center;'>";
            result += metaInfo['journal-name']._text+"</tr><tr><td colspan='8' style='font-weight:bold; font-size:18pt; text-align:center;'>";   
            var monthDetials = ["Jan","Feb","March","April","May","June","July","August","Sep","Oct","Nov","Dec"];
            var month = parseInt(metaInfo['month']._text);   
            month = monthDetials[month-1];
            result += "Volume "+metaInfo['volume']._text+", Issue"+metaInfo['issue']._text+", "+month+" "+metaInfo['year']._text+"</tr><tr><td colspan='8' style='font-weight:bold; font-size:18pt;text-align:center;'>";            
            result += "Covers:4; Others:"+(totalPages-4)+"; Total:"+totalPages+"</td></tr></thead></table>";
            result += "<table border='2px'><thead><tr>"
            $($settings.columns).each(function (key, value) {
                if (this.ishidden != true) {
                    result += "<th";
                    if (this.width != null) {
                        result += " style='width: " + this.width + "'";
                    }
                    result += ">";
                    if(this.headertext == "lpage"){
                        result += "End page";
                    }
                    else if(this.headertext == "fpage"){
                        result += "Start page";
                    }
                    else if(this.headertext == "id"){
                        result += "Manuscript_Id";
                    } 
                    else{
                        result += this.headertext;
                    }    
                    result += "</th>";
                }
            });           
            result += "</tr></thead>";

            result += "<tbody>";
            $(gridData).each(function (key, value) {
                result += "<tr>";
                $($settings.columns).each(function (k, v) {
                    if (value.hasOwnProperty(this.datafield)) {
                        if (this.ishidden != true) {                           
                            if(this.datafield != "id"){
                                result += "<td style='text-align:center;";
                                if (this.width != null) {
                                    result += " width: " + this.width + ";";
                                }
                                result += "'";
                            }
                            else{
                                result += "<td style='text-align:left;";
                                if (this.width != null) {
                                    result += " width: " + this.width + ";";
                                }
                                result += "'";
                            }   
                            if((this.datafield == "Pages")&&(value.hasOwnProperty("mergecell"))){ // handling row span for run on articles
                                result += "rowspan='"+value["mergecell"]+"'";
                            }                            
                            result += ">";
                            result += value[this.datafield];
                            result += "</td>";                            
                        }
                    }
                });
                result += "</tr>";
            });
            result += "</tbody>";
            result += "</table>";
            // handling advert details
            result +="<table><thead><tr><td></td></tr><tr></tr></thead></table>" // to create space between tables
            result += "<table border='2px'><thead><th>Advert details</th><th>Pages</th></thead>";
            result += "<tbody>";
            var currRow ="";
            var advertTitle = [{"headertext":"adverts", "datatype":"string","datafield":"adverts"},{"headertext":"advert_Pages", "datatype":"string","datafield":"advert_Pages"}]; 
            $(gridData).each(function (key, value) {
                currRow = "<tr>";
                $(advertTitle).each(function (k, v) {
                    if (value.hasOwnProperty(this.datafield)) {
                        if (this.ishidden != true) {                            
                            result += "<td style='text-align:left;";
                            if (this.width != null) {
                                result += " width: " + this.width + ";";
                            }
                            result += "'>";
                            result += value[this.datafield];
                            result += "</td>";                                                        
                        }
                    }
                });
                currRow = "</tr>";
                if(currRow!="<tr></tr>"){
                    result += currRow;
                }                
            });
            result += "</tbody>";
            result += "</table>";
            return result;
        }
        function Export(htmltable) {

            if (isBrowserIE()) {
        
                exportToExcelIE(htmltable);
            }
            else {
                var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'>";
                excelFile += "<head>";
                excelFile += '<meta http-equiv="Content-type" content="text/html;charset=' + $defaults.encoding + '" />';
                excelFile += "<!--[if gte mso 9]>";
                excelFile += "<xml>";
                excelFile += "<x:ExcelWorkbook>";
                excelFile += "<x:ExcelWorksheets>";
                excelFile += "<x:ExcelWorksheet>";
                excelFile += "<x:Name>";
                excelFile += "{worksheet}";
                excelFile += "</x:Name>";
                excelFile += "<x:WorksheetOptions>";
                excelFile += "<x:DisplayGridlines/>";
                excelFile += "</x:WorksheetOptions>";
                excelFile += "</x:ExcelWorksheet>";
                excelFile += "</x:ExcelWorksheets>";
                excelFile += "</x:ExcelWorkbook>";
                excelFile += "</xml>";
                excelFile += "<![endif]-->";
                excelFile += "</head>";
                excelFile += "<body>";
                excelFile += htmltable.replace(/"/g, '\'');
                excelFile += "</body>";
                excelFile += "</html>";
                var uri = "data:application/vnd.ms-excel;base64,";
                var ctx = { worksheet: $settings.worksheetName, table: htmltable };
                return (uri + base64(format(excelFile, ctx)));
            }
        }
        function base64(s) {
            return window.btoa(unescape(encodeURIComponent(s)));
        }
        function format(s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; });
        }
        function isBrowserIE() {
            var msie = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            if (msie > 0) {  // If Internet Explorer, return true
                return true;
            }
            else {  // If another browser, return false
                return false;
            }
        }
        function exportToExcelIE(table) {
            var el = document.createElement('div');
            el.innerHTML = table;
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            var tab;
            if ($settings.datatype.toLowerCase() == 'table') {            
                tab = document.getElementById($settings.containerid);  // get table              
            }
            else{
                tab = el.children[0]; // get table
            }
            for (j = 0 ; j < tab.rows.length ; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand('SaveAs', true, 'main');
                console.log(txtArea1.document);
            }
            else                
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
                console.log(sa);
            return (sa);
        }
    };
})(jQuery);
//get columns
function getColumns(paramData){
	var header = [];
	$.each(paramData[0], function (key, value) {		
		var obj = {}
		obj["headertext"] = key;
		obj["datatype"] = "string";
        obj["datafield"] = key;            
        if((key != "mergecell")&&(key != "grouptype") && (key != "data-preflight") && (key != "type") && (key != "section") && (key != "pageExtent")& (key != "lpage_runon")& (key != "fpage_runon")){
            header.push(obj);
        }		
	}); 
	return header;
}
function resize(){
    $('#tocContainer,#detailsContainer,.bodyDiv').css('height', (window.innerHeight - $('#explorerDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#tocContainer #flatPlanBodyDiv,#detailsContainer.bodyDiv').css('height', (window.innerHeight - $('#tocContainer .bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#tocContainer #tocBodyDiv').css('height', (window.innerHeight - $('#tocContainer .bodyDiv').offset().top  - $('#footerContainer').height() - 10) + 'px');
    $('#detailsContainer .tabContent').css('height', (window.innerHeight -  $('#detailsContainer .bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#manuscriptData .tabContent').css('height', (window.innerHeight -  $('#manuscriptData .bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#uaaDiv').css('height', (window.innerHeight - $('#uaaDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#cList').css('height', (window.innerHeight - $('#cList').offset().top - $('#footerContainer').height() - 10) + 'px');
    $('#tocBodyDiv .tocBodyContainer').css('height',  $('#tocBodyDiv').height());
    $('#tocBodyDiv .tocBodyContainer').mCustomScrollbar("update");
    $('#flatPlanBodyDiv .container').css('height',  $('#flatPlanBodyDiv').height());
    $('#flatPlanBodyDiv .container').mCustomScrollbar("update");
    $('#changeHistory .changeHistoryContainer').css('height',  $('#changeHistory').height());
    $('#metaHistory .metadata').css('height',  $('#metadata').height());
    $('#changeHistory .changeHistoryContainer').mCustomScrollbar("update");
    $('#contentsLits #artListTab').css('height',  $('#contentsLits .bodyDiv').height());
    $('#contentsLits #artListTab').mCustomScrollbar("update");
    $("#detailsContainer .metadata").mCustomScrollbar({
        axis:"y", // vertical scrollbar
        theme:"dark"
    });
    $('#unassignedArticles .tabContent,#manuscriptData.bodyDiv').css('height', (window.innerHeight -  $('#unassignedArticles .bodyDiv').offset().top - $('#uaaTab').offset().top - $('#footerContainer').height() - 10) + 'px');

}
$(document).ready(function () {
	$('.readOnly').on('keypress keyup keydown change',function(e){
        var code = e.keyCode || e.which;
        if(code != 9) { 
            e.preventDefault();
            return false
        }
        return true;
    });
    // load xml-js into browser using systemjs and jspm
    /*System.import('npm:xml-js').then(function (c) {
        convert = c;
    });*/
    //Initialize the poste and subscriber
    eventHandler.publishers.add();
    eventHandler.subscribers.add();
    window.addEventListener("resize", function () {
       resize()
    });
    initEvents();
    //set session username
    $('#headerContainer #kUserName').html($('.userinfo:first').attr('data-name'))
    eventHandler.general.actions.getCustomerList()
        .then(function (data) {
            resize()
            $('#tocContainer #flatPlanBodyDiv,#contentsLits .bodyDiv,#detailsContainer.bodyDiv').css('height', (window.innerHeight - $('#tocContainer .bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
            $('#tocContainer .bodyDiv,#manuscriptData .bodyDiv,#contentsLits .bodyDiv,.tabContent').css('height', (window.innerHeight - $('.bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
            $('#changeHistory .changeHistoryContainer').mCustomScrollbar("update");
          })
        .catch(function (err) {
            showMessage({ 'text': eventHandler.messageCode.errorCode[501][2] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
            console.log(err);
        })
        $('#artListTab').mCustomScrollbar({
            axis:"y", // vertical scrollbar
            theme:"dark"
        });
})