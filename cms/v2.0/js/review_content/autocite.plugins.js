(function ($) {
	var AutoUrlDetectState,changedNode,autoCiteTriggered = false,autoCiteKeyPressed = true,getprenode,prenodecloned,buildspan, selectedFigArr = {};
    $.autoCite = function (options) {
		
        var defaults = {
                regexToFind: '^(Fig|Vid|Tab|Tbl|ima|sch|chart|box|Supplement|Appendix|Equation)',
                classToAdd: 'jrnlFigRef'
            },
            plugin = this,
            options = options || {};

        plugin.init = function () {
            var settings = $.extend({}, defaults, options);
            $.data(document, 'autoCite', settings);
        }

        plugin.init();
		
		$(kriya.config.containerElm)[0].addEventListener("click", function(e) {
			updateCitation();
		});
		$(kriya.config.containerElm)[0].addEventListener("keyup", function(e) {
			if($(e.target).closest('.front').length > 0){
				return false;
			}
			if ((e.keyCode == 32) || (e.keyCode == 188) || (e.keyCode == 221) || ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))) {
				return autoTrack(e);
			}
			if ((autoCiteKeyPressed) && (e.keyCode >= 65 && e.keyCode <= 90)) {
				return autoTrack(e);
			}
		});
					
		$(kriya.config.containerElm).scroll(function (){
			updateCitation();
		});

    }

    function getReferenceCitations(rng, text, prevMatches){
		rngheight = rng.getBoundingClientRect().height;
		rngtop = rng.getBoundingClientRect().bottom;
		rngleft = rng.getBoundingClientRect().left;
		var range = rangy.getSelection();
		/*****Build the suggetions for citation*****/
		newList = $("<ul class='autoCiteSuggestion dropdown-content' style='display:block;opacity:1;position: absolute; z-index: 99999;overflow-y:auto;top:"+rngtop+"px;left:"+rngleft+"px';/>");
		var blockFloat;
		$(kriya.config.containerElm + " .jrnlRefText").each( function(){
			if ((citeJS.settings.R.citationType == '0') && (/^[\[\(]?[0-9]+[0-9\,\;\s\-\u2013]*[\]\)]?$/.test(text))){
				labelText = $(this).attr('data-id').replace('R', '');
				if ($(this).find('.RefSlNo').length > 0){
					labelText = $(this).find('.RefSlNo').text().replace(/\./g, '');
				}
				$(newList).append("<li id='"+$(this).attr('data-id')+"' style='padding:3px; cursor:pointer;text-align:left;' data-text-val='"+val+"' class='jrnlBibRef'><p class='jrnlBibRef suggestions'>" + labelText + "</p></li>");
			}else{
				/*****Remove the Space from text ****/
				var val = $(this).find('.RefSurName').first().text().replace(/\u00A0/gi, ' ');
				val = val.replace(/^[\u00A0\s]+|[\u00A0\s\.\:]+$/g, '');
				val = val.replace(/[\u2013\u2014]/g, '-');
				var checkText = text.replace(/[\u2013\u2014]/g, '-');
				var regex = new RegExp('^' + checkText , 'gi');
				/***Checking for regexp match***/
				var citedArray = [];
				if ((regex.test(val)) || (prevMatches)) {
					if ($(this).find('.RefAuthor').length == 1){
						labelText = $(this).find('.RefSurName').text() + ', ' + $(this).find('.RefYear').text();
					}
					else if ($(this).find('.RefAuthor').length == 2){
						labelText = $(this).find('.RefSurName').first().text() + ' and ' + $(this).find('.RefSurName').last().text() + ', ' + $(this).find('.RefYear').text();
					}else{
						labelText = $(this).find('.RefSurName').first().text() + ' et al., ' + $(this).find('.RefYear').text();
					}
					$(newList).append("<li id='"+$(this).attr('data-id')+"' style='padding:3px; cursor:pointer;text-align:left;' data-text-val='"+val+"' class='jrnlBibRef'><p class='jrnlBibRef suggestions'>" + labelText + "</p></li>");
				}
				$('.jrnlBibRef[data-cite-type]').removeAttr('data-cite-type');
			}
		});
		/***show the suggesion in closest the cursor point***/
		if ($(newList).find('li').length > 0){
			$('body').append($(newList));	
			$(newList).attr('data-text-rng', rng.toString());
			if ((rngtop + 280) > $(window).height()){
				rngtop = rng.getBoundingClientRect().top - $(newList).height() - 10;
				$(newList).css('top', rngtop);
			}
		}
		$('.autoCiteSuggestion .suggestions').off('click');
		$('.autoCiteSuggestion .suggestions').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			var citeClass = 'jrnlBibRef';
			$(this).toggleClass('selected');
			if ($(this).hasClass('plablels') && $(this).hasClass('selected') && ($(this).closest('div').find('p.selected').length == 0)){
				$(this).closest('div').find('p.suggestions').addClass('selected');
			}
			if ((!$(this).hasClass('plablels')) && (!$(this).hasClass('selected'))){
				$(this).closest('div').find('.selected').removeClass('selected');
			}
			var labelText = '', dataCiteString = ' ';
			$('.autoCiteSuggestion .selected').each(function(){
				labelText += ',' + $(this).html();
				dataCiteString += $(this).parent().attr('id') + ' ';
			});
			labelText = labelText.replace(/^,/gi, '');
		if (($(range.anchorNode.parentElement).closest('[class*="Ref"]').length > 0) || ($('[class*="Ref"][data-cite-type]').length >0)){
			var getprenode = $(range.anchorNode.parentElement);
			if ($(range.anchorNode.parentElement).find('[class*="Ref"]').length >0){
				getprenode = $(range.anchorNode.parentElement).find('[class*="Ref"]')[0];
			}
			if ($('[class*="Ref"][data-cite-type]').length >0){
				getprenode = $('[class*="Ref"][data-cite-type]')[0];
			}
			if (labelText == ''){
				$(getprenode).remove();
			}else{
				$(getprenode).attr('data-citation-string', dataCiteString).html(labelText);
			}
		}else{
			if (labelText != ''){

				var newCite = $("<span class='"+citeClass+"' data-citation-string=' "+ dataCiteString +" ' data-cite-type='insert'>"+ labelText + "</span>");
				newCite = newCite[0];

				range.setSingleRange(rng);
				range.getRangeAt(0).deleteContents();
				range.getRangeAt(0).insertNode(newCite);
				
				var getprenode = range.anchorNode.parentElement;
				if ($(getprenode).closest('sup').length > 0 && $(getprenode).find('sup').length > 0){
					$(getprenode).find('sup').contents().unwrap();
				}
				if ($(range.anchorNode.parentElement).find('[class*="Ref"]').length >0){
					getprenode = $(range.anchorNode.parentElement).find('[class*="Ref"]')[0];
				}
			}
		}
		if ((kriya.config.content && kriya.config.content.citation.R.citationType == '1') || citeJS.settings.R.citationType == '1'){
			var currNode = range.anchorNode.parentElement;
			if (($(currNode).attr('data-citation-string') != dataCiteString) && ($(currNode).find('[data-citation-string]').length != 1)){
				currNode = $(currNode).find('[data-citation-string=" '+dataCiteString+' "]')[0];
			}
			var prevNode = currNode.previousSibling;
			if (prevNode && (prevNode.nodeName == '#text') && (/^[\s\.\;\[\]\(\)\u00A0]+$|^$/i.test(prevNode.nodeValue))) {
				prevTextNode = prevNode;
				prevNode = prevNode.previousSibling;
				if (prevNode && ((prevNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevNode.getAttribute('class')))){
					prevTextNode.nodeValue = '; ';
				}else{
					prevTextNode.nodeValue = ' ';
				}
			}else if (prevNode && ((prevNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevNode.getAttribute('class')))){
				$(currNode).before('; ');
			}else{
				$(currNode).before(' ');
			}

			var nextNode = currNode.nextSibling;
			if ((nextNode.nodeName == '#text') && (/^[\s\.\;\[\]\(\)\u00A0]+$|^$/.test(nextNode.nodeValue))) {
				nextTextNode = nextNode;
				nextNode = nextNode.nextSibling;
				if (nextNode && ((nextNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(nextNode.getAttribute('class')))){
					nextTextNode.nodeValue = '; ';
				}else{
					nextTextNode.nodeValue = ' ';
				}
			}else if (nextNode && ((nextNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(nextNode.getAttribute('class')))){
				$(currNode).after('; ');
			}else{
				$(currNode).after(' ');
			}

			updateCitation();
		}

			
		});

	}

    function autoTrack(key, end_offset, delimiter, enter) {
        var rng, end, start, endContainer, bookmark, text, matches, prev, next, len, rngText, prevText = '', autoCiteKeyPressed = false;
		end_offset = 1, delimiter ='', enter = true;
		
		function scopeIndex(container, index) {
			if (index < 0) {index = 0;}
			if (container.nodeType == 3) {
				var len = container.data.length;
				if (index > len) {index = len;}
			}
			return index;
		}

		function setStart(container, offset) {
			if (container.nodeType != 1 || container.hasChildNodes()) {
				rng.setStart(container, scopeIndex(container, offset));
			} else {
				rng.setStartBefore(container);
			}
		}

		function setEnd(container, offset) {
			if (container.nodeType != 1 || container.hasChildNodes()) {
				rng.setEnd(container, scopeIndex(container, offset));
			} else {
				rng.setEndAfter(container);
			}
		}

		var prevMatches = false;
		var prevEndContainer = false;
		var range = rangy.getSelection();
		var sel = range.getRangeAt(0);
		kriya.selection = sel;
		sel.normalizeBoundaries()
		sel.commonAncestorContainer.parentNode.normalize()
		rng = sel.cloneRange();
		/***test to get the actual rng string***/
		endContainer = rng.endContainer;
		if (endContainer.nodeType != 3 && endContainer.firstChild) {
			while (endContainer.nodeType != 3 && endContainer.firstChild) {
				endContainer = endContainer.firstChild;
			}
			// Move range to text node
			if (endContainer.nodeType == 3) {
				setStart(endContainer, 0);
				setEnd(endContainer, endContainer.nodeValue.length);
			}
		}
		prev = rng.endContainer.previousSibling;//get previous sibling of the current range in order to check the range falls behind any citations
		if (!prev) {
			prev = rng.endContainer.parentElement.previousSibling;
		}
		if (!prev) {
			if (!rng.endContainer.firstChild || !rng.endContainer.firstChild.nextSibling) {
				//return;
			}else{
				prev = rng.endContainer.firstChild.nextSibling;
			}
		}
		var prevTextContent = rng.startContainer.textContent;
		
		var patternMatched = true;
		if (prevTextContent != ''){
			if (!(
				(/^[\,\;\s0-9\u00A0\-\u2013\u2014]+$|^[\,\;\sa-z\u00A0\-\u2013\u2014]+$|^[\,\;\s\u00A0]*and[\,\;\s0-9\u00A0]+$/gi.test(prevTextContent)) || 
				((/(figure|supplement|source|data|code)/i.test(prevTextContent)) && (! /[\s\u00A0](figure|table)/i.test(prevTextContent)))
				)){
				patternMatched = false;
			}
		}
		if ((prev) && (prev.nodeType == 1) && (prev.attributes.class) && (patternMatched)){
			prevMatches = prev.attributes.class.textContent.match(/(Fig|Bib|Vid|Tbl|ima|sch|chart|box|suppl|Eq)Ref/gi);
		}
		
		if ((prev) && (!prevMatches) && (prev.previousSibling) && (prev.previousSibling.nodeType == 1) && (prev.previousSibling.attributes.class)){
			searchSatisfied = true;
			//keep checking previous sibling text untill our pattern matches
			while ((searchSatisfied) && (prev) && (patternMatched)){
				prevPrevNode = prev.previousSibling;
				if (!( (prevPrevNode) && (prevPrevNode.nodeType == 1) && (prevPrevNode.attributes.class) && (prevPrevNode.attributes.class.textContent.match(/^del/gi)))){
					prevTextContent = prev.textContent + prevTextContent;//considering the text which are not deleted
				}
				if ((prevTextContent != '') && (!((/^[\,\;\s0-9\u00A0\-\u2013\u2014]+$|^[\,\;\sa-z\u00A0\-\u2013\u2014]+$|^[\,\;\s\u00A0]*and[\,\;\s0-9\u00A0]+$/i.test(prevTextContent)) || (/figure|supplement|source|data|code/.test(prevTextContent))))){
					patternMatched = false;//checking if the text so far set as range matches our pattern
				}
				if ((prevPrevNode) && (prevPrevNode.nodeType == 1) && (prevPrevNode.attributes.class) && (prevPrevNode.attributes.class.textContent.match(/^del/gi))){
					prev = prevPrevNode;
				}else if ((! /\)/.test(prevTextContent)) && (prevTextContent.split(/[\s\u00A0]/g).length < 4) && (patternMatched)){
					if ((prevPrevNode) && (prevPrevNode.nodeType == 1) && (prevPrevNode.attributes.class)){
						prevMatches = prevPrevNode.attributes.class.textContent.match(/(Fig|Bib|Vid|Tbl|ima|sch|chart|box|eq|suppl)Ref/gi);
						if (prevMatches){//breaking the check once any citation is found
							setStart(prev, 0);//setting up the start for range --- from start index (0) of the last iterated previous sibling
							prevEndContainer = prev;
							searchSatisfied = false;
						}else{
						}
					}
					prev = prevPrevNode;
				}else{
					searchSatisfied = false;
				}
			}
		}
		$('[data-auto-cite]').removeAttr('data-auto-cite');
		if (prevMatches){//get the content of the citation found
			if (!((prev.attributes.class.textContent == 'jrnlBibRef') && (citeJS.settings.R.citationType == '1'))){
				prevText = prev.textContent + prevText;
				$(prev).attr('data-auto-cite', 'true');
				autoCiteTriggered = false;
			}else{
				prevMatches = false;
			}
		}
		if ((rng.endContainer.parentNode) && (rng.endContainer.parentNode.nodeType == 1) && (rng.endContainer.parentNode.attributes.class) && (patternMatched)){
			if(rng.endContainer.parentNode.attributes.class.textContent.match(/(Fig|Bib|Vid|Tbl|ima|sch|chart|box|suppl|Eq)Ref/gi)){
				prevMatches = true;
				prev = rng.endContainer.parentNode;
				//prevText = prev.textContent;
				$(prev).attr('data-auto-cite', 'true');
				autoCiteTriggered = false;
			}
		}
		if (rng.endOffset == 1) {
			end = 2;
		} else {
			if (rng.endOffset < rng.endContainer.length){
				end = rng.endContainer.length - end_offset;
			}else{
				//end = rng.endOffset - end_offset;
				end = rng.endOffset;
			}
		}
		start = end;
		if (prevMatches){
			end = 0;
		}
		//if check is made for the first time, get the range till it matche a space
		if ((! autoCiteTriggered) && (!prevMatches)){
			do {
				rng.setStart(endContainer, end >= 2 ? end - 2 : 0);// Move the selection one character backwards.
				rng.setEnd(endContainer, end >= 1 ? end - 1 : 0);
				end -= 1;
				// Loop until one of the following is found: a blank space, &nbsp;, delimiter, (end-2) >= 0
			} while (rng.toString() != ' ' && rng.toString() !== '' &&
				rng.toString().charCodeAt(0) != 160 && (end -2) >= 0 && rng.toString() != delimiter);
		}
		//if the previously entered character matches patter, the current range is set till the previously set range
		//say first time 'Figure' is typed and next '1' is typed, so the range will be set as 'Figure 1'
		if (autoCiteTriggered){
			while ((! /[\s\u00A0\(\[](Fig|Vid|Tab|Tbl|ima|sch|chart|box|Supplement|Equation)/gi.test(rng.toString())) && (end - 1) >= 0) {
				// Move the selection one character backwards.
				setStart(endContainer, end >= 2 ? end - 1 : 0);
				end -= 1;
			}// Loop until one of the following is found: a blank space, &nbsp;, delimiter, (end-2) >= 0
			if (/appendix/i.test(rng.endContainer.textContent)){
				while ((! /[\s\u00A0\(\[](Appendix)/gi.test(rng.toString())) && (end - 1) >= 0) {
					// Move the selection one character backwards.
					setStart(endContainer, end >= 2 ? end - 1 : 0);
					end -= 1;
				}// Loop until one of the following is found: a blank space, &nbsp;, delimiter, (end-2) >= 0
			}
			if (/^[\s\u00A0\(\[]/.test(rng.toString())){
				setStart(endContainer, end >= 1 ? end + 1 : 1);
				end += 1;
			}
			if (! /[\s\u00A0]$/.test(rng.toString())){
				//start += 1;
			}
		}
		if ((! prevMatches) && (/^[\s\u00A0]/.test(rng.toString()))){//if the range start with punctuation, its trimmed
			setStart(endContainer, end >= 1 ? end + 1 : 1);
			//end += 1;
		}
		if (/^[\s\u00A0]$/.test(rng.toString())){//if the range start with punctuation, its trimmed
			setStart(endContainer, end >= 1 ? end + 1 : 1);
			//end += 1;
		}

		if (rng.toString().charCodeAt(0) == 160) {
			setStart(endContainer, end);
			setEnd(endContainer, start);
			end += 1;
		} else if (prevEndContainer){ //if prev has a citation in it, to set range from its end
			setStart(prevEndContainer, end);
			setEnd(endContainer, start);
		} else if ((rng.startOffset === 0) || (prevMatches)) {
			setStart(endContainer, 0);
			setEnd(endContainer, start);
		} else {
			setStart(endContainer, end);
			setEnd(endContainer, start);
		}
		
		text = rng.toString().replace(/\u00A0/gi, ' ');
		if (prevMatches){//get text from prev. citations
			text = prevText + text;
		}
		text = text.replace(/^[\u00A0\s]+|[\u00A0\s]+$/g, '');
		text = text.replace(/([/\(\[])/g, '\\$1');
		
		matches = text.match(/^(Fig|Vid|Tab|Tbl|ima|sch|chart|box|Supplement|Appendix|Equation)/gi);
		jQuery.expr[":"].containsIgnoreCase = jQuery.expr.createPseudo(function(arg) {
				return function( elem ) {
				return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
				};
			});
			
		citeEndOffset = true;
		
		if ((!matches) && (/[0-9]{4}/.test(text))){//when 4 digit numbers is typed, considering it may be an author year
			console.log(text);
			var regex = new RegExp(text , 'gi');
			var matchRefText = $(".jrnlRefText .RefYear").filter(function () {
				return regex.test($(this).text()); 
			});
			var authorString = '';
			var firstAuthorString = '';
			$(matchRefText).parent().each(function(){
				firstAuthorString +=  '^' + $(this).find('.RefSurName:first').text() + '|';
				$(this).find('.RefSurName').each(function(){//:not(":first")
					var sn = $(this).text().split(' ');
					sn = sn.join('|');
					authorString += sn + '|';
				})
			})
			firstAuthorString = '(' + firstAuthorString.replace(/\|$/, '') + ')';
			authorString = '(' + authorString.replace(/\|$/, '') + ')';
			
			if (matchRefText.length > 0){
				var stringMatch = true;
				while (stringMatch){
					do {
						rng.setStart(endContainer, end >= 2 ? end - 2 : 0);// Move the selection one character backwards.
						rng.setEnd(endContainer, end >= 1 ? end - 1 : 0);
						end -= 1;
						// Loop until one of the following is found: a blank space, &nbsp;, delimiter, (end-2) >= 0
					} while (rng.toString() != ' ' && rng.toString() !== '' &&
						rng.toString().charCodeAt(0) != 160 && (end -2) >= 0 && rng.toString() != delimiter);
					if (end == 1){
						end -= 1;
					}
					if (/^[\s\u00A0\(\[]/.test(rng.toString())){//if the range start with punctuation, its trimmed
						setStart(endContainer, end + 1);
					}
					setStart(endContainer, end);
					setEnd(endContainer, start);
					if (/^[\s\u00A0\(\[]/.test(rng.toString())){//if the range start with punctuation, its trimmed
						setStart(endContainer, end + 1);
					}
					text = rng.toString().replace(/\u00A0/gi, ' ');
					if (end == 0){
						stringMatch = false;
					}else {
						var roa = new RegExp(authorString , 'gi');
						var fa = new RegExp(firstAuthorString , 'gi');
						if (/^(et al|and |al[\.\,\s]+[0-9])/.test(text)){
						}
						else if (fa.test(text)){
							stringMatch = false;
						}
						else if (! roa.test(text)){
							stringMatch = false;
						}
					}
				}
				var getRIDNode = $('<span class="jrnlBibRef">' + rng.toString() + '</span>');
				range.setSingleRange(rng);
				commonjs.general.updateCitationData(getRIDNode);
				var labelText = range.toString();
				if (getRIDNode.attr('data-citation-string') != undefined){
					var ref = $(kriya.config.containerElm + ' #' + getRIDNode.attr('data-citation-string').replace(/^ | $/g, ''));
					if ($(ref).find('.RefAuthor').length == 1){
						labelText = $(ref).find('.RefSurName').text() + ', ' + $(ref).find('.RefYear').text();
					}
					else if ($(ref).find('.RefAuthor').length == 2){
						labelText = $(ref).find('.RefSurName').first().text() + ' and ' + $(ref).find('.RefSurName').last().text() + ', ' + $(ref).find('.RefYear').text();
					}else{
						labelText = $(ref).find('.RefSurName').first().text() + ' et al., ' + $(ref).find('.RefYear').text();
					}
					
					newNode = $("<span class='jrnlBibRef' data-citation-string='"+ getRIDNode.attr('data-citation-string')+"'>" + labelText + "</span>");
					newNode = newNode[0]
					range.getRangeAt(0).deleteContents()
					range.getRangeAt(0).insertNode(newNode);
					newNode.after(document.createTextNode(' '));
					newNode.before(document.createTextNode(' '));
					/*var currNode = tinyMCE.activeEditor.selection.getNode();
					var prevNode = currNode.previousSibling;
					if (prevNode && (prevNode.nodeName == '#text') && (/^[\s\.\;\[\]\(\)\u00A0]+$/i.test(prevNode.nodeValue))) {
						prevTextNode = prevNode;
						prevNode = prevNode.previousSibling;
						if (prevNode && ((prevNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevNode.getAttribute('class')))){
							prevTextNode.nodeValue = '; ';
						}
					}else if (prevNode && ((prevNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(prevNode.getAttribute('class')))){
							$(currNode).before('; ');
					}// end of if prevNode

					var nextNode = currNode.nextSibling;
					if ((nextNode.nodeName == '#text') && (/^[\s\.\;\[\]\(\)\u00A0]+$/.test(nextNode.nodeValue))) {
						nextTextNode = nextNode;
						nextNode = nextNode.nextSibling;
						if (nextNode && ((nextNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(nextNode.getAttribute('class')))){
							nextTextNode.nodeValue = '; ';
						}
					}else if (nextNode && ((nextNode.nodeName).toLocaleLowerCase() == 'span') && (/jrnlBibRef/.test(nextNode.getAttribute('class')))){
						$(currNode).after('; ');
					}*/
				}
				//editor.selection.collapse(false);
			}
		}
		
		/***Remove the Suggestion***/
		if (matches){
			$('.autoCiteSuggestion').remove(); 
            var ltext = '',newList ='',rngheight,rngtop,rngleft,id,pclass,test,$pList = '';

            rngheight = rng.getBoundingClientRect().height;
            rngtop    = rng.getBoundingClientRect().bottom;
            rngleft   = rng.getBoundingClientRect().left;

            /*****Build the suggetions for citation*****/
			newList = $("<ul class='autoCiteSuggestion dropdown-content' style='display:block;opacity:1;position: absolute; z-index: 99999;overflow-y:auto;top:"+rngtop+"px;left:"+rngleft+"px';/>");
   			var blockFloat;
			$(kriya.config.containerElm + " .label").each( function(){
   				/*****Remove the Space from text ****/
   				var val = $(this).text().replace(/\u00A0/gi, ' ');
   				val = val.replace(/^[\u00A0\s]+|[\u00A0\s\.\:\|]+$/g, '');
   				val = val.replace(/[\u2013\u2014]/g, '-');
				var checkText = text.replace(/[\u2013\u2014]/g, '-');
				//checkText = checkText.replace(/Appendix\s?/g, '');//to check if appendix labels are captured
				checkText = checkText.replace(/[\,\ ]$/g, '');
				var regex = new RegExp(checkText , 'gi');
   				/***Checking for regexp match***/
   				var citedArray = [];
				if (val && (regex.test(val) || prevMatches )) {
					if (($(sel.endContainer.parentNode).closest('[class*="Ref"]').length > 0) || ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0) || ($('[class*="Ref"][data-cite-type]').length >0)){
						var getprenode = $(sel.endContainer.parentNode);
						if ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0){
							getprenode = $(sel.endContainer.parentNode).find('[class*="Ref"]')[0];
						}
						else if ($('[class*="Ref"][data-cite-type]').length >0){
							getprenode = $('[class*="Ref"][data-cite-type]')[0];
						}
						if ($(getprenode).attr('data-citation-string') != undefined){
							citedArray = $(getprenode).attr('data-citation-string').replace(/^ | $/, '').split(' ');
						}
					}
					if (prevMatches){
						citedArray = $(prev).attr('data-citation-string').replace(/^ | $/, '').split(' ');
						blockFloat 	= $('#'+ citedArray[0]).closest('*[class*="Block"]').attr('class').split(' ')[0];
					}
					if (! prevMatches){
						blockFloat 	= $(this).closest('*[class*="Block"]').attr('class').split(' ')[0];
					}
					//console.log(blockFloat);

					$(kriya.config.containerElm + ' .' + blockFloat).find(".label").each( function(){
						/*****Remove the Space from text ****/
						var val = $(this).text().replace(/\u00A0/gi, ' ');
						val = val.replace(/^[\u00A0\s]+|[\u00A0\s\.\:\|]+$/g, '');
						val = val.replace(/[\u2013\u2014]/g, '-');
						var checkText = text.replace(/[\u2013\u2014]/g, '-');
						var regex = new RegExp('^' + checkText , 'gi');
						id = $(this).closest('[data-id]').attr('data-id');
						pclass = $(this).closest('[data-id]').attr('class');
						pchildren = $(this).closest('*[class*="Block"]').find('.partLabel');
						var labelNode = $(this).clone();
						$(labelNode).find('.del').remove();
						$(labelNode).find('.ins').contents().wrap();
						var labelText = $(labelNode).text().replace(/[\. ,:-\s\|]+$/gi, '');
						/***Checking for regexp match***/
						if ((citedArray.length > 0) && (citedArray.indexOf(id) != -1)){
							$(newList).append("<li id='"+id+"' style='padding:3px; cursor:pointer;text-align:left;' data-text-val='"+val+"' class='"+pclass+"'><p class='"+pclass+" selected suggestions'>" + labelText + "</p></li>");
						}else{
							$(newList).append("<li id='"+id+"' style='padding:3px; cursor:pointer;text-align:left;' data-text-val='"+val+"' class='"+pclass+"'><p class='"+pclass+" suggestions'>" + labelText + "</p></li>");
						}
					});
					return false;
   				}
   			});
   			/***show the suggesion in closest the cursor point***/
			if ($(newList).find('li').length > 0){
				$('body').append($(newList));
				$(newList).attr('data-text-rng', rng.toString());
				if ((rngtop + 280) > $(window).height()){
					rngtop = rng.getBoundingClientRect().top - $(newList).height() - 10;
					$(newList).css('top', rngtop);
				}
			}
			autoCiteTriggered = true;
			if ((autoCiteTriggered) && (/[0-9][a-z]{0,1}([,\-\u2013\u2014][\s\u00A0]*[a-z])*([,\-\u2013\u2014][\s\u00A0]*[0-9]+[a-z]{0,1})*$/gi.test(text)) && (! /([0-9][a-z][\s\u00A0]*[0-9])|([0-9],[\s\u0A00][a-z]$)/gi.test(text)) && (! /^[\,\s\u00A0]+$/gi.test(rng.toString()))){//((! /[\,\s\u00A0]$/gi.test(rng.toString())) && (!prevMatches))
				$node = editor.selection.getNode();
				if($node.hasAttribute('data-citation-string')){
					//editor.selection.setRng(rng);
					//$('.autoCiteSuggestion').remove();
					//$('body').append($(newList));
				}else if (citeEndOffset){
					
					var citeClass ='',selnode,selnodeattr;
					$this = $(".autoCiteSuggestion [data-text-val='" + text + "']");
					
					if (prevMatches){
						//setStart(endContainer, 0);
						if (/[\s\u00A0]$/.test(rng.toString())){
							start -= 1;
							setEnd(endContainer, start);
						}
						//editor.selection.setRng(rng);
						if ($(sel.endContainer.parentNode).hasClass('ins')){
							if (sel.endContainer.parentNode.textContent == rng.toString()){
								var node = sel.endContainer.parentNode;
								setStart(node, 0);
								//editor.selection.setRng(rng);
							}
						}
						var getRIDNode = $('<span class="'+$(prev).attr('class')+'">' + prev.textContent + rng.toString() + '</span>');
						var citeClass = commonjs.general.validateCitationData(getRIDNode);
						if (citeClass[0]){
							commonjs.general.updateCitationData(getRIDNode, false);
							if (getRIDNode.attr('data-citation-string') != undefined){
								$(prev).attr('data-citation-string', getRIDNode.attr('data-citation-string'));
								$(prev).attr('data-cite-type', 'insert');
								if ($(range.anchorNode.parentElement).find(prev).length > 0){
									var rngContents = rng.extractContents();
									var ecl = rngContents.childNodes.length;
									for (var ec = 0; ec < ecl; ec++){
										$(prev).append($(rngContents.childNodes[0]))
									}
								}else if (rng.commonAncestorContainer.textContent == rng.toString()){
									$(prev).append($(range.anchorNode.parentElement))
								}else{
									var rngContents = rng.extractContents();
									var ecl = rngContents.childNodes.length;
									for (var ec = 0; ec < ecl; ec++){
										$(prev).append($(rngContents.childNodes[0]))
									}
								}
							}
						}
					}
				}
			 	getprenode 	= $(sel.endContainer.parentNode).clone();
			}//enD Of autoCiteTriggered check
			else{
				if (! ((prevMatches) && (/^[\s\,\;]+[a-z]*[\s,\,\;]*$/.test(rng.toString())) || (/Fig|Tab|Tbl|Vid|Suppl/i.test(rng.toString())) ) ){
					$('.autoCiteSuggestion').remove();
				}
			}
			$('.autoCiteSuggestion .suggestions').off('click');
			$('.autoCiteSuggestion .suggestions').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				
				
				range.setSingleRange(rng);
				var citeClass = 'jrnlFigRef';
				if (($(sel.endContainer.parentNode).closest('[class*="Ref"]').length > 0) || ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0) || ($('[class*="Ref"][data-cite-type]').length >0) || ($('[class*="Ref"][data-auto-cite]').length >0)) {
					var getprenode = $(sel.endContainer.parentNode);
					if ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0){
						getprenode = $(sel.endContainer.parentNode).find('[class*="Ref"]')[0];
					}
					if ($('[class*="Ref"][data-cite-type]').length >0){
						getprenode = $('[class*="Ref"][data-cite-type]')[0];
					}
					if ($('[class*="Ref"][data-auto-cite]').length >0){
						getprenode = $('[class*="Ref"][data-auto-cite]')[0];
					}
					var citeID = $(getprenode).attr('data-citation-string').replace(/^\s+|\s+$/g, '').split(/\s+/);
					var citeString = $(getprenode).text();
					var citeArray = citeString.split(',');
					var cl =citeArray.length;
					var currIndex = citeID[0];
					var ci = 0;
					selectedFigArr = {};
					for (var i=0; i<cl; i++){
						var citationString = citeArray[i];
						 citationString =  citationString.replace(/^.*?([0-9A-Z]+)$/i, '$1');
						 if (/^[0-9]+[A-Z]*$/i.test(citationString)){
							 currIndex = citeID[ci++];
							 selectedFigArr[currIndex] = '';
						 }
						 if (/[A-Z]+$/i.test(citationString)){
							citationString =  citationString.replace(/[0-9]+/, '');
							citationString = selectedFigArr[currIndex] + ',' + citationString;
							citationString = citationString.replace(/^,/, '');
							selectedFigArr[currIndex] = citationString;
						 }
					}
				}
				
				$(this).toggleClass('selected');
				if ($(this).hasClass('plablels') && $(this).hasClass('selected') && ($(this).closest('li').find('p.selected').length == 0)){
					$(this).closest('li').find('p.suggestions').addClass('selected');
				}
				if ((!$(this).hasClass('plablels')) && (!$(this).hasClass('selected'))){
					$(this).closest('li').find('.selected').removeClass('selected');
				}
				var labelText = '', dataCiteString = ' ';
				$('.autoCiteSuggestion .selected').each(function(){
					$(this).html($(this).html().replace('&nbsp;', ' '));
					if ($(this).hasClass('plablels')){
						if (/[0-9]$/.test(labelText)){
							labelText += $(this).html();
						}else if (/[A-Z]$/i.test(labelText)){
							labelText += ',' + $(this).html();
						}
					}else if ((labelText != '') && (/(Figure|Table|Video)[\s\u00A0]([0-9\,\s]+)$/.test(labelText)) && (/(Figure|Table|Video)[\s\u00A0]([0-9\,\s]+)$/.test($(this).html()))){
						labelText += ',' + $(this).html().replace(/^(Figure|Table|Video)\s([0-9]+)$/gi, '$2');
					}else{
						labelText += '; ' + $(this).html();
					}
					if (typeof(selectedFigArr[$(this).parent().attr('id')]) != 'undefined'){
						labelText += selectedFigArr[$(this).parent().attr('id')];
					}
					dataCiteString += $(this).parent().attr('id') + ' ';
				});
				labelText = labelText.replace(/^[,;] /gi, '');
				var citeClass = commonjs.general.validateCitationData($('<span>' + labelText + '</span>'));
				if (citeClass[0]){//tag it as citation only response id true
					citeClass = citeClass[1];
				}
				if (($(sel.endContainer.parentNode).closest('[class*="Ref"]').length > 0) || ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0) || ($('[class*="Ref"][data-cite-type]').length >0) || ($('[class*="Ref"][data-auto-cite]').length >0)){
					var getprenode = $(sel.endContainer.parentNode);
					if ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0){
						getprenode = $(sel.endContainer.parentNode).find('[class*="Ref"]')[0];
					}
					if ($('[class*="Ref"][data-cite-type]').length >0){
						getprenode = $('[class*="Ref"][data-cite-type]')[0];
					}
					if ($('[class*="Ref"][data-auto-cite]').length >0){
						getprenode = $('[class*="Ref"][data-auto-cite]')[0];
						if ($(sel.endContainer.parentNode).find(prev).length > 0){
							var rngContents = rng.extractContents();
							var ecl = rngContents.childNodes.length;
							for (var ec = 0; ec < ecl; ec++){
								$(prev).append($(rngContents.childNodes[0]))
							}
						}else if (rng.commonAncestorContainer.textContent == rng.toString()){
							$(prev).append($(sel.endContainer.parentNode))
						}else{
							var rngContents = rng.extractContents();
							var ecl = rngContents.childNodes.length;
							for (var ec = 0; ec < ecl; ec++){
								$(prev).append($(rngContents.childNodes[0]))
							}
						}
					}
					if (labelText == ''){
						$(getprenode).remove();
						range.collapse(false);
						node = document.createTextNode($('.autoCiteSuggestion').attr('data-text-rng'))
						sel.insertNode(node);
						rangy.getSelection().setSingleRange(range);
					}else{
						$(getprenode).attr('data-citation-string', dataCiteString).html(labelText);
						$(getprenode).attr('data-cite-type', 'insert');
					}
					//kriya.general.triggerCitationReorder($(getprenode), 'insert');
				}else{
					$('.activeElement').removeClass('activeElement');
					if (labelText == ''){
						range.collapse(false);
						node = document.createTextNode($('.autoCiteSuggestion').attr('data-text-rng'));
						sel.insertNode(node);
						rangy.getSelection().setSingleRange(range);
					}else{
						newNode = $(" <span class='"+citeClass+"' data-citation-string='"+ dataCiteString +"' data-cite-type='insert'>"+ labelText +"</span> ");
						newNode = newNode[0];
						range.getRangeAt(0).deleteContents();
						range.getRangeAt(0).insertNode(newNode);
						
						newNode.after(document.createTextNode(' '));
						newNode.before(document.createTextNode(' '));

						var getprenode = sel.endContainer.parentNode;
						if ($(sel.endContainer.parentNode).find('[class*="Ref"]').length >0){
							getprenode = $(sel.endContainer.parentNode).find('[class*="Ref"]')[0];
						}
					}
					//kriya.general.triggerCitationReorder($(newNode), 'insert');
					//$('.autoCiteSuggestion').remove(); 
				}
			});
		}else{
			if (! ((prevMatches) && (/^[\s\,\;]+[a-z]*[\s,\,\;]*$/.test(rng.toString())))){
				$('.autoCiteSuggestion').remove(); 
			}
			text = text.replace('\\[', '[');
			text = text.replace('\\(', '(');
			text = text.replace('\\)', ')');
			text = text.replace('\\]', ']');
			if (/^[a-z][a-z]+[\s,\,\;]+$|^[\[\(][0-9]+[\)\]]$/i.test(rng.toString())){
				getReferenceCitations(rng, text, prevMatches);
			}
			
			if ((prevMatches) && (/^[\,\-\u2013]*[\s\u00A0]*[0-9]+$/i.test(rng.toString()))){
				//setStart(endContainer, 0);
				if (/[\s\u00A0]$/.test(rng.toString())){
					start -= 1;
					setEnd(endContainer, start);
				}
				//editor.selection.setRng(rng);
				if ($(range.anchorNode.parentElement).hasClass('ins')){
					if (range.anchorNode.parentElement.textContent == rng.toString()){
						var node = range.anchorNode.parentElement;
						setStart(node, 0);
						//editor.selection.setRng(rng);
					}
				}
				var getRIDNode = $('<span class="'+$(prev).attr('class')+'">' + prev.textContent + rng.toString() + '</span>');
				commonjs.general.updateCitationData(getRIDNode, false);
				if (getRIDNode.attr('data-citation-string') != undefined){
					$(prev).attr('data-citation-string', getRIDNode.attr('data-citation-string'));
					$(prev).attr('data-cite-type', 'insert');
					if ($(range.anchorNode.parentElement).find(prev).length > 0){
						var rngContents = rng.extractContents();
						var ecl = rngContents.childNodes.length;
						for (var ec = 0; ec < ecl; ec++){
							$(prev).append($(rngContents.childNodes[0]))
						}
					}else if (rng.commonAncestorContainer.textContent == rng.toString()){
						$(prev).append($(range.anchorNode.parentElement))
					}else{
						var rngContents = rng.extractContents();
						var ecl = rngContents.childNodes.length;
						for (var ec = 0; ec < ecl; ec++){
							$(prev).append($(rngContents.childNodes[0]))
						}
					}
				}
			}
		
			autoCiteTriggered = false;
		}
    }
    /**
     * Function to update the citation after selecting the dropdown
     */
    function updateCitation(){
    	$('.autoCiteSuggestion').remove();
		if($(kriya.config.containerElm + ' [data-cite-type="insert"]').length > 0){
			var citeNode = $(kriya.config.containerElm + ' [data-cite-type="insert"]')[0];
			//kriya.general.triggerCitationReorder($(citeNode), 'insert');
			//citeJS.floats.renumberStatus();
			//kriyaEditor.init.addUndoLevel('insert-auto-citation');
		}
    }

}(jQuery));