/*https://codepen.io/sagarpatil/pen/LEZLav*/
// function that handles the dragstart event
function dragStart(e) {
    e.stopPropagation();
    var boxObj = $(this);
    boxObj.addClass('is-dragged');

    // when an element is dragged the preview image is needed to show that the element is being dragged
    //  for some reason the default image is not clear, so did some tweaks to get the working
    var width = boxObj.width() / 1.5;
    var height = boxObj.height() / 1.5;

    var dragImage = boxObj.clone();
    dragImage.removeAttr('id').removeClass('uaa').addClass('uaaDrag');
    $('#dragImage').removeClass('hide')
        .width(width + 20).height(height + 20)
        .html(dragImage)
        .find('.uaaDrag').width(width - 10).height(height + 10);

    e.originalEvent.dataTransfer.setDragImage($('#dragImage .uaaDrag').get(0), 0, 0);
    e.originalEvent.dataTransfer.effectAllowed = 'copy';
}

function dragEnd(e) {
    e.stopPropagation();
    $('#dragImage').html('').addClass('hide');
    $(this).removeClass('is-dragged');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
    $('.dropTop').removeClass('dropTop');
    $('.dropBottom').removeClass('dropBottom');
}

function dragEnter(e) {
    if (e.stopPropagation) e.stopPropagation();
    $('.dropArrow.icon-arrow_forward').removeClass('hide');
}

function dragLeave(e) {
    if (e.stopPropagation) e.stopPropagation();
    $(this).removeClass('dropTop').removeClass('dropBottom');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
}

/**
 * based on were you are dragging over the droppable element, show a clue as to whether
 *  the dropped element is to be placed above or below the droppable element
 * @param {*} e
 */
function dragOver(e) {
    if (e.stopPropagation) e.stopPropagation();
    e.preventDefault();
    e.originalEvent.dataTransfer.dropEffect = 'copy';
    var clientX = e.originalEvent.clientX;
    var clientY = e.originalEvent.clientY;
    var thisPosition = $(this).position();
    var halfHeight = thisPosition.top + ($(this).height() / 2)
    var leftValue = thisPosition.left + parseInt($(this).css('margin-left')) - parseInt($('.dropArrow').css('font-size'));
    var topValue = thisPosition.top;
    if (halfHeight > clientY) {
        $(this).addClass('dropTop').removeClass('dropBottom');
    }
    else {
        topValue += $(this).height();
        $(this).addClass('dropBottom').removeClass('dropTop');
    }
    $('.dropArrow.icon-arrow_forward').removeClass('hide').css({ top: topValue, left: leftValue })
    return false;
}

function drop(e) {
    $('.la-container').fadeIn();
    if (e.stopPropagation) e.stopPropagation();
    var droppedOnNode = $(e.currentTarget);
    var dropTop = false;
    if (droppedOnNode.hasClass('dropTop')) {
        dropTop = true;
    }

    var draggedNode = $('.is-dragged');
    $(currentTab + '.dropArrow.icon-arrow_forward').addClass('hide');
    droppedOnNode.removeClass('dropTop').removeClass('dropBottom');
    var entry = '';
    if (draggedNode.attr('data-obj')) {
        var entryData = draggedNode.attr('data-obj');
        entryData = entryData.replace(/\'/gi, '"');
        entry = JSON.parse(entryData);
    }
    else {
        entry = draggedNode.data('data');
    }
    entry = JSON.parse(JSON.stringify(entry));
    var dataType = (entry && entry._attributes && entry._attributes.type) ? entry._attributes.type : "manuscript";
    var dataGroupType = '';
    if (entry._attributes && entry._attributes.grouptype) {
        dataGroupType = entry._attributes.grouptype;
    }
    var runOnFlag = 'false'; // check if run-on article
    if (entry._attributes && entry._attributes['data-run-on']) {
        runOnFlag = entry._attributes['data-run-on'];
    }
    //check if unpaginated
    var unpaginated = 'false';
    if (entry._attributes && entry._attributes['data-unpaginated']) {
        unpaginated = entry._attributes['data-unpaginated']
    }
    // get the last MS container
    var lastNode = $(currentTab + '#tocContentDiv *[data-c-id]:last');
    var lastNodeID, tempID;
    if (lastNode.length > 0) {
        lastNodeID = parseInt(lastNode.attr('data-c-id'));
        tempID = lastNodeID + 1;
    }
    else {
        tempID = parseInt(0)
    }
    if (!(/^(manuscript)/gi.test(entry._attributes.type))) {
        entry._attributes['id'] += (Math.floor(Math.random() * 90000) + 10000);
    }
    if(entry._attributes.type == 'filler'){
        entry._attributes['data-run-on'] = 'true';
        let runOnflagObj = ({ "_attributes": { "type": "run-on", "value": "1" }});
        entry['flag']=runOnflagObj;
    }
    if (!(entry._attributes.fpage) || entry._attributes.fpage == '') {
        entry._attributes.fpage = 1;
    }
    if (!(entry._attributes.fpage) || entry._attributes.lpage == '') {
        entry._attributes.lpage = entry._attributes.fpage;
    }
    var secIndex = ''
    if (droppedOnNode.attr('data-sec')) {
        secIndex = droppedOnNode.attr('data-sec').replace('sec', '')
    }
    else if (droppedOnNode.attr('id')) {
        secIndex = droppedOnNode.attr('id').replace('sec', '')
    }
    else {
        secIndex = parseInt(0)
    }
    var msConObj = {
        "dataType": dataType,
        "tempID": tempID,
        "sectionIndex": secIndex,
        "dataGroupType": dataGroupType
    }
    // var msContainer = eventHandler.general.components.getMSContainer(entry, msConObj);
    var populateTOCFlag = false;
    if ($(droppedOnNode).data('data') == undefined && entry._attributes.section == '' && (/^(manuscript|chapter)/gi.test(entry._attributes.type))) {
        entry._attributes.section = entry.type ? entry.type._text : ''
    }
    else if (entry._attributes && entry._attributes.type && $(droppedOnNode) && $(droppedOnNode).attr('customer-type') && $(droppedOnNode).attr('customer-type')!='book' && (/^(cover|table\-of\-contents|editorial\-board)$/gi.test(entry._attributes.type))) {
        entry._attributes.section = entry._attributes.type;
        populateTOCFlag = true;
    }
    else if ($(droppedOnNode).data('data')) {
        entry._attributes.section = $(droppedOnNode).data('data')._attributes.section;
    } else if ($(droppedOnNode)[0].hasAttribute('data-section')) {
        entry._attributes.section = $(droppedOnNode).attr('data-section');
    }
    if ($(droppedOnNode).data('data') != undefined &&  $(droppedOnNode).data('data')._attributes && $(droppedOnNode).data('data')._attributes.type && (/^(cover|table\-of\-contents|editorial\-board)/gi.test( $(droppedOnNode).data('data')._attributes.type))) {
        populateTOCFlag = true;
    }
    var data = {};
    data.entry = entry;
    data.msConObj = msConObj;
    var createTemplate = 'makeCardWhileDroppingSectionCard';
    if (dataType.match(/manuscript/g) && draggedNode.find('.msAccDate')) {
        data = {};
        data.customerType = 'issue';
        data.dconfig = dashboardConfig;
        data.count = 0;
        data.disableFasttrack = disableFasttrack;
        data.userDet = JSON.parse($('#userDetails').attr('data'));
        data.filterObj = {};
        data.filterObj['customer'] = {};
        data.filterObj['project'] = {};
        data.filterObj['articleType'] = {};
        data.filterObj['typesetter'] = {};
        data.filterObj['publisher'] = {};
        data.filterObj['author'] = {};
        data.filterObj['editor'] = {};
        data.filterObj['copyeditor'] = {};
        data.filterObj['support'] = {};
        data.filterObj['supportstage'] = {};
        data.filterObj['contentloading'] = {};
        data.filterObj['others'] = {};
        data.filterObj['supportlevel'] = {};
        data.info = unassignedArticlesList.filter(function (articles) {
            return articles._source.id === draggedNode.first().find('.msID').text()
        })
        createTemplate = 'manuscriptTemplate';
        entry._attributes['id-type'] = 'doi';
        entry._attributes['grouptype'] = 'body';
    } else if (draggedNode.attr('customer-type') == 'issue') {
        createTemplate = 'issueDroppingSectionCard';
    }
    var pagefn = doT.template(document.getElementById(createTemplate).innerHTML, undefined, undefined);
    var msContainer = pagefn(data);
    if ((msContainer == '') || (typeof (msContainer) === 'undefined')) {
        var parameters = { notify: {} };
        parameters.notify.notifyTitle = 'Error';
        parameters.notify.notifyType = 'error';
        parameters.notify.notifyText = 'Unable to add the requested file. Please try again or contact support';
        eventHandler.common.functions.displayNotification(parameters);
        $('.la-container').fadeOut();
        return false;
    }
    var romanFlag = false;
    if ($(droppedOnNode).data('data') && ($(droppedOnNode).data('data')._attributes.grouptype) && (/^(roman)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))) {
        romanFlag = true;
    } else if ($(droppedOnNode).next().data('data') && ($(droppedOnNode).next().data('data')._attributes.grouptype) && (/^(roman)/gi.test($(droppedOnNode).next().data('data')._attributes.grouptype))) {
        romanFlag = true;
    }
    //for book drag drop based on grouptype
    var configData = $(currentTab + '#manuscriptsData').data('config');
    if (configData._attributes && configData._attributes['type'] && configData._attributes['type'] == 'book') {
        if ($(droppedOnNode).data('data') && dataType != 'blank' && entry._attributes.grouptype != $(droppedOnNode).data('data')._attributes.grouptype) {
            // showMessage({ 'text': 'Card can oly be added to the same grouptype ', 'type': 'error' });
            $('.la-container').fadeOut();
            return false;
        }
    }
    if (!(dataType.match(/filler/g))) {
        if (droppedOnNode.hasClass('noChild')) {
            $(msContainer).insertAfter(droppedOnNode);
            droppedOnNode.removeClass('noChild')
            droppedOnNode.addClass('hasChild')
        }

        else if (droppedOnNode.hasClass('dropTop')) {
            $(msContainer).insertBefore(droppedOnNode);
        }
        else if (droppedOnNode.hasClass('sectionHeader')) {
            $(msContainer).insertAfter(droppedOnNode);
        }
        else {
            $(msContainer).insertAfter(droppedOnNode);
        }
    }
    else {  //if the drop node is filler/filler-toc, then add it only after manuscript
        if (droppedOnNode.attr("data-type") == 'manuscript' ||droppedOnNode.attr("data-type") == 'filler') {
            $(msContainer).insertAfter(droppedOnNode);
        }
        else { 
            /*if filler/filler-toc is added after blank/advert display error msg*/
            var parameters = { notify: {} };
            parameters.notify.notifyTitle = 'Error';
            parameters.notify.notifyType = 'error';
            parameters.notify.notifyText = 'Filler/Filler TOC can oly be added below Manuscript';
            eventHandler.common.functions.displayNotification(parameters);
            $('.la-container').fadeOut();
            return false;
        }
    }
    //if filler add attrib data-run-on-with attrib and add data-sub-articles attrib to its prev manuscript
    let prevManuscript;
    
    if(entry._attributes.type && entry._attributes.type == 'filler'){
        prevManuscript = $(lastNode).prevAll('[data-type="manuscript"]:first');
        let prevManuscriptDoi = prevManuscript.attr('data-doi');
        entry._attributes['data-runon-with'] = prevManuscriptDoi;
        let prevRunOnArticles = prevManuscript.attr('data-subarticles');
        let prevContent =  prevManuscript.data('data');
        if(prevRunOnArticles && prevContent){
            prevContent._attributes['data-subarticles'] = prevRunOnArticles+','+$(lastNode).attr('data-doi');
           //prevManuscript.attr('data-subarticles',prevRunOnArticles+','+$(lastNode).attr('data-doi'));
        }
        else if(prevContent){
            prevManuscript.attr('data-subarticles',$(lastNode).attr('data-doi'));
        }
        eventHandler.flatplan.action.saveData();
    }
    
    if (dataType.match(/manuscript/g) && draggedNode.find('.msAccDate')) {
        droppedOnNode.next().attr({
            'data-grouptype': 'body',
            'data-type': 'manuscript',
            'data-issue-section': droppedOnNode.attr('data-issue-section') || droppedOnNode.attr('data-section')
        });
    }
    if ($(droppedOnNode).data('data') && ($(droppedOnNode).data('data')._attributes.grouptype) && (/^(roman|front|back)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))) {
        entry._attributes.grouptype = $(droppedOnNode).data('data')._attributes.grouptype;
        eventHandler.flatplan.action.saveData();
    }
    $(droppedOnNode).next().attr('data-c-id', tempID);
    $(currentTab + '[data-c-id="' + tempID + '"]').removeClass('hide');
    var lastNode = $(currentTab + '#tocContentDiv *[data-c-id]:last');
    var lastNodeID;
    var seqID = tempID;
    if (lastNode.length > 0) {
        lastNodeID = parseInt(lastNode.attr('data-c-id'));
        //push the 'entry' data into array as if its the last node, then push it through the array to it actual position
        while (lastNodeID != tempID && !isNaN(lastNodeID)) {
            $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[lastNodeID + 1] = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[lastNodeID];
            lastNode.attr('data-c-id', seqID--);
            if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
                lastNode = $(lastNode).prevAll('[data-c-id]:first');
            }
            else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
                lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last();
            }
            else {
                //lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
                lastNode = $(lastNode).parent().prevAll('[data-c-id]:first');
                // lastNode = $(lastNode).parent().nextAll().find('[data-c-id]:last').last();
            }
            lastNodeID = parseInt(lastNode.attr('data-c-id'));
        }
        lastNode.attr('data-c-id', seqID).data('data', entry);
    }
    else {
        $(currentTab + '[data-c-id="' + tempID + '"]').data('data', entry);
    }
    $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[seqID] = entry;
    // to determine the start page number of the currently added MS look for page number in the previous node
    // previous node will be something like manuscript or blank or ..., else it would be section, if its a section get the first page, else get the last page
    var prevNode = lastNode.prev();
    var startPage = 1;
    if (prevNode) {
        if (prevNode.attr('data-type') == 'section') {
            if ($(prevNode).find('.rangeStart').length > 0) {
                if (unpaginated == 'true') {  // if run-on article, set the prev node lpage as fpage
                    startPage = parseInt($(prevNode).find('.rangeStart')[0].textContent) - 1;
                }
                else if (romanFlag == true) {
                    startPage = eventHandler.flatplan.components.fromRoman($(prevNode).find('.rangeStart')[0].textContent);
                    startPage = parseInt(startPage);
                }
                else {
                    startPage = parseInt($(prevNode).find('.rangeStart')[0].textContent);
                }
            }
        }
        else if (/(sectionHeader)/gi.test(prevNode.attr('class'))) {
            startPage = parseInt(1)
            populateTOCFlag = true;
        }
        else {
            if (runOnFlag == 'true') {
                startPage = parseInt(prevNode.data('data')._attributes.lpage);
            }
            else if (romanFlag == true) {
                startPage = eventHandler.flatplan.components.fromRoman(prevNode.data('data')._attributes.lpage);
                startPage = parseInt(startPage) + 1;
            } else if (draggedNode.attr('data-section') == "Unpaginated Advert") {
                startPage = 1;
            }
            else {
                startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
            }
            // startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
        }
    }
    else {
        startPage = parseInt(1);
    }
    if (romanFlag == true) {
        lastNode.find('.rangeStart').text(eventHandler.flatplan.components.toRoman(startPage));
        lastNode.data('data')._attributes.fpage = lastNode.data('data')._attributes.lpage = eventHandler.flatplan.components.toRoman(startPage);
    } else {
        lastNode.find('.rangeStart').text(startPage);
        lastNode.find('.rangeEnd').text(startPage);
        lastNode.data('data')._attributes.fpage = lastNode.data('data')._attributes.lpage = startPage;
    }
    if (unpaginated != 'true' &&  entry._attributes.type!='filler') {
        eventHandler.flatplan.components.updatePageNumber(seqID, startPage, entry._attributes.grouptype);
    }
    if (!draggedNode.attr('data-obj')) {
        //dont remove the card for blank/advert/filler
        if ((entry._attributes.type != 'blank') && (entry._attributes.type != 'advert') && (entry._attributes.type != 'advert_unpaginated') && (entry._attributes.type != 'filler') && (entry._attributes.type != 'filler-toc')) {
            draggedNode.remove();
        }
    }
    //if filler save and retrigger the current issue 
    if(entry._attributes.type!='filler' || populateTOCFlag == true){
        eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#projectIssueList .active').trigger('click')" });
    }
    else{
        eventHandler.flatplan.action.saveData();
    }
    $('.la-container').fadeOut();
    return false;
}