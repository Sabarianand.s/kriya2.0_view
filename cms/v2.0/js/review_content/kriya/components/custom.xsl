<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" exclude-result-prefixes="xsi xs xlink mml">
    <xsl:output method="xhtml" encoding="utf-8"/>
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="//div[@class='front']">
        <div class="front">
            <xsl:apply-templates select=".//*[@class='jrnlArtType']"/>
            <xsl:apply-templates select=".//*[@class='jrnlSubjectGroup']"/>
            <xsl:apply-templates select=".//*[@class='jrnlID' and @data-journal-id-type='publisher-id']"/>
            <xsl:apply-templates select="//*[@class='jrnlPubDate' and @data-pub-type='epub']"/>
            <xsl:apply-templates select=".//*[@class='jrnlDOI']"/>
            <xsl:apply-templates select=".//*[@class='jrnlArtTitle']"/>
            <xsl:apply-templates select=".//*[@class='jrnlAuthors']"/>
            <xsl:apply-templates select=".//*[@class='jrnlAffGroup']"/>
            <xsl:apply-templates select=".//*[@class='jrnlAbsGroup']"/>
            <xsl:apply-templates select=".//*[@class='jrnlMetaName']"/>
            <xsl:apply-templates select=".//*[@class='jrnlMetaValue']"/>
            <xsl:apply-templates select="//*[@class='author-notes']"/>
        </div>
    </xsl:template>
    <xsl:template match="//div[@class='body']">
        <div class="body">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="//div[@class='back']">
        <div class="back">
            <xsl:apply-templates select=".//*[@class='jrnlAckGroup']"/>
            <xsl:apply-templates select=".//*[@class='jrnlFNGroup' and ./*[@data-fn-type='fn']]"/>
            <xsl:apply-templates select=".//*[@class='jrnlRefHead']"/>
            <xsl:apply-templates select=".//*[@class='jrnlRefText']"/>
            <xsl:apply-templates select="//*[@class='jrnlKwdGroup']"/>
            <xsl:apply-templates select="//*[@class='jrnlHistory']/*[@class='jrnlReDate']"/>
            <xsl:apply-templates select="//*[@class='jrnlHistory']/*[@class='jrnlAcDate']"/>
            <xsl:apply-templates select="//*[@class='jrnlPubDate' and @data-pub-type='epub']"/>
            <xsl:apply-templates select="//*[@class='author-notes']/*[@data-fn-type='edited-by']"/>
            <xsl:apply-templates select="//*[@class='jrnlPermission']/*[@class='jrnlCopyrightStmt']"/>
            <xsl:apply-templates select="//*[@class='jrnlPermission']/*[@class='jrnlLicense']"/>
            <xsl:apply-templates select="//*[@class='author-notes']/*[@data-fn-type!='edited-by']"/>
        </div>
    </xsl:template>
    <xsl:template match="*[@class='jrnlMonth']">
        <xsl:variable name="mmm" select="."/>
        <span class="jrnlMonth">
            <xsl:choose>
                <xsl:when test="$mmm = '01'">JANUARY</xsl:when>
                <xsl:when test="$mmm = '02'">FEBRUARY</xsl:when>
                <xsl:when test="$mmm = '03'">MARCH</xsl:when>
                <xsl:when test="$mmm = '04'">APRIL</xsl:when>
                <xsl:when test="$mmm = '05'">MAY</xsl:when>
                <xsl:when test="$mmm = '06'">JUNE</xsl:when>
                <xsl:when test="$mmm = '07'">JULY</xsl:when>
                <xsl:when test="$mmm = '08'">AUGUST</xsl:when>
                <xsl:when test="$mmm = '09'">SEPTEMBER</xsl:when>
                <xsl:when test="$mmm = '10'">OCTOBER</xsl:when>
                <xsl:when test="$mmm = '11'">NOVEMBER</xsl:when>
                <xsl:when test="$mmm = '12'">DECEMBER</xsl:when>
            </xsl:choose>
        </span>
    </xsl:template><!--<xsl:template match="*/text()[normalize-space()]">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>
    <xsl:template match="*/text()[not(normalize-space())]"/>-->
</xsl:stylesheet>