var config = {
    "defaultUnits":"pt",
    "baseLeading":11.5,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":false,
    "landscapeFloatOnFirstPage":false,
	"articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY,KEY_BACK",
    "minStackSpace":18,
    "placeStyle":"Sandwich",
	"figCaptionMinLines": 0,
    "minNoLinesOnPag":4,
    "applyTableLeftRightBorder":false,
    "applyTableBorderWidth":0,
    "applyTableBorderColor":'WHITE',
    "pageSize":{
        "width":595.276,
        "height":782.362
    },
	"exportPDFpreset":{
		"print":{
            "crop_marks":true, 
            "bleed_marks":false, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":8.504
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":18,
      "right":18,
      "gutter":25.512
    },
	"geoBoundsVerso":[45.354,68.03,504.567,663.307
    ],
    "geoBoundsRecto":[45.354,68.03,504.567,663.307
    ],
	"articleTypeDetails":{
    "undefined":"LAYOUT1",
    "RESEARCH":"LAYOUT1",
    "RHAE":"LAYOUT1",
    "RNSY":"LAYOUT1",
    "TSRE":"LAYOUT1",
    "INSIGHT":"LAYOUT2",
    "EDITORIAL":"LAYOUT2",
    "FEAE":"LAYOUT2",
    "MISCELLANEOUS":"LAYOUT1",
    },
"pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":520.05,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":520.05,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
    },
	"jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":360, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
	"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":["jrnlHead1", "jrnlHead2", "jrnlHead3", "jrnlHead4", "BL-T", "NL-T", "BL-O", "NL-O", "QUOTE-T"],
            "after":["BL-B", "NL-B", "BL-O", "NL-O", "QUOTE-O", "QUOTE-B"]
         },
        "decrease":{
            "before":["jrnlHead1", "jrnlHead2", "jrnlHead3", "jrnlHead4", "BL-T", "NL-T", "BL-O", "NL-O", "QUOTE-T"],
            "after":["BL-B", "NL-B", "BL-O", "NL-O", "QUOTE-O", "QUOTE-B"]
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
	"detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsHead,jrnlHead1,jrnlHead2,jrnlHead3",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "relinkFigures":['LOGO_1.eps'],
    "replaceColors":['BASECOLOR'],
	"nonDefaultLayers":["PRIM_SUR"],
	"assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCRRH']", "frame-name":"CRRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCLRH']", "frame-name":"CLRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRH']", "frame-name":"RRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRH']", "frame-name":"LRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlMetaInfo']", "frame-name":"METAINFO", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlRelatedInfo']", "frame-name":"RELATED_ARTICLE_INFO", "action":"move", "styleOverride":null}
       ]       
     },
	  "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"100,100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"30,20,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"175,189,225"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
		    "RHAE":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"100,100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"30,20,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"175,189,225"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
            "RESEARCH":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"100,100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"30,20,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"175,189,225"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
          "MISCELLANEOUS":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"100,100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"30,20,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"100,100,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"175,189,225"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
          
            }
        },
	         "tableSetter" :{
            "thead": {
                "css" : {
                    "font-family" : "'MinionPro-Bold', Times New Roman",
                    "font-size" : "9.34px",
                    "line-height" : "12.45px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionPro-Bold.otf",
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'MinionPro-Regular', Times New Roman",
                    "font-size" : "9.34px",
                    "line-height" : "12.45px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionPro-Regular.otf",
                "align" : "left",
                "valign": "bottom"
            }
        },
      "equation" : {
          "default" : {
              "page":{
                  "size" : "595.276"
              },
              "font" : {
                  "size"    : "9pt",
                  "ledding" : "12pt",
                  "path"    : "/MinionPro/",
                  "ext"     : "otf",
                  "bold"    : "MinionPro-Bold",
                  "italic"  : "MinionPro-Italic",
                  "bold_italic":"MinionPro-BoldItalic",
                  "main"    : "MinionPro-Regular"
              }
           },
        },
    "stubColObj":{
        "top": {
            "STUB_COLUMN": false,
            "STUB_STMT": false
        }	
    }
}
this.config = config;