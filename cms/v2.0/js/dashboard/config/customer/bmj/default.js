﻿var dashboardConfig = {
    "editpage":{
        "sage_books":"proof_review",
        "stylus":"proof_review",
        "emerald":"proof_review"
    },
    "editpagebystage":{
        "bmj":{
            "Typesetter Review" : "proof_review",
            "Typesetter Check" : "proof_review",
        },
        "thebmj":{
            "Typesetter Review" : "proof_review",
            "Typesetter Check" : "proof_review",
        }
    },
    "removeEdit": {
        "stage": {
            "addJob": "false",
            "Convert Files": "false",
            "File download": "false",
            "Hold": "false"
        },
        "version":{
            "v1.0": "false"
        }
    },
    "icons": [
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "stage": "Final Deliverables",
                "role": "publisher",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "stage": "Archive",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "stage": "Banked",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "stage": "Author Review",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "stage": "Author Revision",
                "role": "publisher",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "stage": "Author proof",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-play",
        "hovertext": "Unhold",
        "function": "unhold",
        "rules": {
            "condition": {
                "stage": "Hold",
                "role": "publisher|production",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-pause",
        "hovertext": "Hold",
        "function": "hold",
        "rules": {
            "condition": {
                "stage": "Hold",
                "role": "publisher|production",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": false,
                    "clickable": false
                },
                "false": {
                    "visible": true,
                    "clickable": true
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "stage": "addJob",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "stage": "File download",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "stage": "Convert Files",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "stage": "Content-Loading",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "stage": "Support",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-times",
        "hovertext": "Withdraw",
        "function": "withdraw",
        "rules": {
            "condition": {
                "stage": "Archive|Banked",
                "role": "publisher|production",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": false,
                    "clickable": false
                },
                "false": {
                    "visible": true,
                    "clickable": true
                }
            }
        }
    },
]
}
var popups = {
    "hold": {
        "title": "Put on Hold - {doi}",
        "options": ["On Hold awaiting payment", "On Hold with Editor", "On Hold awaiting linked paper", "On Hold awaiting query response", "On Hold other"],
        "commentbox": true,
        "dropdown": true,
        "function": "eventHandler.components.actionitems.saveHold",

    },
    "unhold": {
        "title": "Release Hold - {doi}",
        "descriptiontext": "Hold",
        "options": ["Payment received", "Editor query acknowleged", "Linked paper processed", "Query responded", "Other"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.saveHold"
    },
    "revoke": {
        "title": "Revoke article - {doi}",
        "descriptiontext": "Hold",
        "options": ["Author declined"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.revokeArticle"
    },
    "resupply": {
        "title": "Resupply for  {doi}",
        "descriptiontext": "Hold",
        "options": ["test", "test2"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.resupplyArticle"
    },
    "withdraw":{
        "title": "Withdraw {doi}",
        "descriptiontext": "Withdraw",
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.withdrawArticle"
    }
}
var notifyConfig = {
    "sendRevokeArticle": {
        "notify": {
            "notifyTitle": "Revoke article",
            "successText": "{doi} has been revoked successfully",
            "errorText": "Error while revoke {doi}"
        }
    },
    "saveFastTrack": {
        "notify": {
            "notifyTitle": "Fast Track article",
            "successText": "Flag status for {doi} had been updated",
            "errorText": "Error while update flag status for {doi}"
        }
    },
    "resupplyArticle": {
        "notify": {
            "notifyTitle": "Resupply Article",
            "successText": "Resupply triggered for {doi}",
            "errorText": "Error while trigger resupply for {doi}"
        }
    },
    "saveWFDate": {
        "notify": {
            "notifyTitle": "Update due date",
            "successText": "Due date updated for {doi}",
            "errorText": "Error while update due date for {doi}"
        }
    },
    "addJob": {
        "notify": {
            "notifyTitle": "Add job",
            "successText": "Job added successfully",
            "errorText": "Error while add job"
        }
    },
    "assignUser":{
        "notify": {
            "notifyTitle": "Assigning User",
            "successText": "User assigned successfully for {doi}",
            "errorText": "Error while assigning user for {doi}",
            "openedArticle": "{doi} is opened by {name}"
        },
        "bulkUpdate":{
            "notifyTitle": "Assigning User",
            "successText": "User assigned successfully for {doi}",
            "errorText": "Error while assigning user for {doi}",
        }
    },
    "blkUpdate":{
        "notify":{
            "openedArticle": "{doi} is opened by {name}",
            "unAccessible": "You cannot assign user to {doi}"
        }
    },
    "withdrawArticle": {
        "notify": {
            "notifyTitle": "Withdraw Article",
            "successText": "{doi} has been withdrawn",
            "errorText": "Error while withdrawn {doi}"
        }
    },
}
var filterMappingOther={
    "0-1d":{"attr":"data-overdue-type","name":"0-1d","val":"0-1d"},
    "1-2d":{"attr":"data-overdue-type","name":"1-2d","val":"1-2d"},
    "2-3d":{"attr":"data-overdue-type","name":"2-3d","val":"2-3d"},
    "3-5d":{"attr":"data-overdue-type","name":"3-5d","val":"3-5d"},
    "5-10d":{"attr":"data-overdue-type","name":"5-10d","val":"5-10d"},
    ">10d":{"attr":"data-overdue-type","name":">10d","val":">10d"},
    "FastTrack":{"attr":"data-fast-track","name":"FastTrack","val":"true"}
}
var filterMapping = {
    "project-sub": "data-project",
    "customer-sub": "data-customer",
    "articleType-sub": "data-article-type",
    "articleType": "all",
    "customer": "all",
    "project": "all",
    "typesetter": "data-stage-owner",
    "typesetter-sub": "data-stage-name",
    "publisher": "data-stage-owner",
    "publisher-sub": "data-stage-name",
    "copyeditor": "data-stage-owner",
    "copyeditor-sub": "data-stage-name",
    "author": "data-stage-owner",
    "author-sub": "data-stage-name",
    "editor": "data-stage-owner",
    "editor-sub": "data-stage-name",
    "support": "data-stage-owner",
    "support-sub": "data-support-category",
    "supportstage":"data-substage-owner",
    "supportstage-sub":"data-supportstage-category",
    "others": "data-others",
    "default": "data-stage-name",
    "others-sub": filterMappingOther,
}
var sortmapping = {
    'Doi' : 'sort-doi',
    'AcceptDate' : 'accept-date',
    'Article' : 'article-type',
    'Customer' : 'customer',
    'DaysInProd' : 'days-in-prod',
    'DueDate' : 'duedate',
    'Owner' : 'owner',
    'Stage': 'stage-name',
    'sortType':{
        'string': ['sort-doi', 'accept-date', 'article-type', 'customer', 'duedate', 'owner','stage-name'],
        'number': ['days-in-prod']
    }
}


var chartTableReport = {
    "default": {
        "columns": [{
            "columnName": "Article",
            "attribute": {
                "data-filter": "DOI",
                "col-size": "15",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Page Count",
            "attribute": {
                "data-filter": "ProofPageCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count",
            "attribute": {
                "data-filter": "WordCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Prod. Days",
            "attribute": {
                "data-filter": "DaysInProd",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Current Stage",
            "attribute": {
                "data-filter": "Stage Name",
                "col-size": "9",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Type",
            "attribute": {
                "data-filter": "Manuscript Type",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Start Date",
            "attribute": {
                "data-filter": "startDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "End Date",
            "attribute": {
                "data-filter": "endDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla Start Date",
            "attribute": {
                "data-filter": "slaStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla End Date",
            "attribute": {
                "data-filter": "slaEndDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned Start Date",
            "attribute": {
                "data-filter": "plannedStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned End Date",
            "attribute": {
                "data-filter": "Stage Due",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Accepted Date",
            "attribute": {
                "data-filter": "AcceptedDate",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Exported Date",
            "attribute": {
                "data-filter": "ExportedDate",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Assigned To",
            "attribute": {
                "data-filter": "assignUser",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage History",
            "attribute": {
                "data-filter": "reportHistory",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }]
    },
    "movementChart": {
        "columns": [{
            "columnName": "Article",
            "attribute": {
                "data-filter": "DOI",
                "col-size": "15",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Page Count",
            "attribute": {
                "data-filter": "ProofPageCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count",
            "attribute": {
                "data-filter": "WordCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Prod. Days",
            "attribute": {
                "data-filter": "DaysInProd",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Chart Stage",
            "attribute": {
                "data-filter": "Stage Name",
                "col-size": "9",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Type",
            "attribute": {
                "data-filter": "Manuscript Type",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Start Date",
            "attribute": {
                "data-filter": "startDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "End Date",
            "attribute": {
                "data-filter": "endDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla Start Date",
            "attribute": {
                "data-filter": "slaStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla End Date",
            "attribute": {
                "data-filter": "slaEndDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned Start Date",
            "attribute": {
                "data-filter": "plannedStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned End Date",
            "attribute": {
                "data-filter": "Stage Due",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage Duration(In Days)",
            "attribute": {
                "data-filter": "stageDays",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Delay(In Days)",
            "attribute": {
                "data-filter": "delayDays",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Assigned To",
            "attribute": {
                "data-filter": "assignUser",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage History",
            "attribute": {
                "data-filter": "reportHistory",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }]
    }
}

var ShowHideCol = {
    'default': {
        'ProofPageCount': { 'name': 'Page Count', 'index': '3', 'defaultShow': true },
        'WordCount': { 'name': 'Word Count', 'index': '4', 'defaultShow': true },
        'Prodn.Days': { 'name': 'Prodn. Days', 'index': '5', 'defaultShow': true },
        'CurrentStage': { 'name': 'Current Stage', 'index': '6', 'defaultShow': true },
        'Type': { 'name': 'Type', 'index': '7', 'defaultShow': true },
        'StartDate': { 'name': 'Start Date', 'index': '8', 'defaultShow': true },
        'EndDate': { 'name': 'End Date', 'index': '9', 'defaultShow': true },
        'SlaStartDate': { 'name': 'Sla-Start Date', 'index': '10', 'defaultShow': true },
        'SlaEndDate': { 'name': 'Sla-End Date', 'index': '11', 'defaultShow': true },
        'PlannedStartDate': { 'name': 'Planned-Start Date', 'index': '12', 'defaultShow': true },
        'PlannedEndDate': { 'name': 'Planned -End Date', 'index': '13', 'defaultShow': true },
        'AcceptedDate': { 'name': 'Accepted Date', 'index': '14', 'defaultShow': false },
        'ExportedDate': { 'name': 'Exported Date', 'index': '15', 'defaultShow': false },
        'AssignedTo': { 'name': 'Assigned To', 'index': '16', 'defaultShow': true }
    },
    'movementChart':{
        'ProofPageCount': { 'name': 'Page Count', 'index': '3', 'defaultShow': true },
        'WordCount': { 'name': 'Word Count', 'index': '4', 'defaultShow': true },
        'Prodn.Days': { 'name': 'Prodn. Days', 'index': '5', 'defaultShow': true },
        'ChartStage': { 'name': 'Chart Stage', 'index': '6', 'defaultShow': true },
        'Type': { 'name': 'Type', 'index': '7', 'defaultShow': true },
        'StartDate': { 'name': 'Start Date', 'index': '8', 'defaultShow': true },
        'EndDate': { 'name': 'End Date', 'index': '9', 'defaultShow': true },
        'SlaStartDate': { 'name': 'Sla-Start Date', 'index': '10', 'defaultShow': true },
        'SlaEndDate': { 'name': 'Sla-End Date', 'index': '11', 'defaultShow': true },
        'PlannedStartDate': { 'name': 'Planned-Start Date', 'index': '12', 'defaultShow': true },
        'PlannedEndDate': { 'name': 'Planned -End Date', 'index': '13', 'defaultShow': true },
        'StageDiff': { 'name': 'Stage Duration(In Days)', 'index': '14', 'defaultShow': true },
        'DelayDiff': { 'name': 'Delay(In Days)', 'index': '15', 'defaultShow': true },
        'AssignedTo': { 'name': 'Assigned To', 'index': '16', 'defaultShow': true }
    }
}

var tableReportFilterConfig = {
    "default":{
        "rowtype":['none', 'caseinsensitivestring', 'number', 'number',
        'number', 'caseinsensitivestring', 'caseinsensitivestring',
        {
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{yyyy}-{MM}-{dd}'
        },
        "none", "none"],
        "filter":['col_0', 'col_15', 'col_16']
    },
    "movementChart":{
        "rowtype":['none', 'caseinsensitivestring', 'number', 'number',
        'number', 'caseinsensitivestring', 'caseinsensitivestring',
        {
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },{
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },{
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },'number','number','caseinsensitivestring'],
        "filter":['col_0', 'col_16']
    }
}


var refreshButtonConfig = {
    "WIP": {
        "workflowStatus": "in-progress OR completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        "excludeStage": "inCardView",
        'version': 'v2.0'
    },
    "Withdrawn": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Withdrawn',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "Archive": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Archive',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "Banked": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Banked',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "All": {
        "workflowStatus": "in-progress OR completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        "excludeStage": "inCardView",
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Assigned to me": {
        'workflowStatus':'in-progress',
        'urlToPost':'getAssigned',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'user':'user',
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Unassigned": {
        'workflowStatus': 'in-progress' ,
        'urlToPost':'getUnassigned',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'user':'user',
        'userLevelStageAccess':true,
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Other": {
        'workflowStatus': 'in-progress',
        'urlToPost':'getOtherarticles',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'userLevelStageAccess':true,
        'optionforassigning':true,
        'version': 'v2.0'
    }
}
var userAuthorization = {
    'publisher':{
        'production': {
            'toShow': [],
            'toHide': []
        },
        'manager': {
            'toShow': [],
            'toHide': []
        },
    },
    'production': {
        'production': {
            'toShow': [],
            'toHide': ['#manuscriptContent #allCards']
        },
        'manager': {
            'toShow': [],
            'toHide': ['#manuscriptContent #allCards']
        },
        'support': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'admin': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'developer': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'reviewer': {
            'toShow': [],
            'toHide': ['#manuscriptContent #allCards,.othersCard', '#manuscriptContent .assignAccess #filterStatus div:gt(0)']
        }
    },
    'copyeditor': {
        'copyeditor': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard']
        },
        'manager': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard']
        }
    }
}

var excludeStage = {
    'inCardView': ['Archive', 'Banked', 'Withdrawn']
}
var addJobConfig = {
    'customer': ['thebmj', 'jb']
}

/**
 * Based on role-type and access-level of user.
 */
var userAssignAccessRoleStages = {
    'author': {
        'author': ['Author Review', 'Author Revision']
    },
    'production':{
        'admin':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'developer':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'production':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'support':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'manager':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'reviewer': ['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
    },
    'publisher':{
        'publisher':['Publisher Check', 'Publisher Review'],
        'manager':['Publisher Check', 'Publisher Review','Editor Check','Editor Review']
    },
    'copyeditor': {
        'copyeditor': ['Copyediting'],
        'manager': ['Waiting for CE', 'Copyediting', 'Copyediting QC', 'Copyediting Check']
    }
}

var xpath = {
    'assignUser': 'data(//workflow/stage[last()]/job-logs/log[last()]/status[text()="open"]/../username)',
    'blkUpdate': 'data(//workflow/stage[last()]/job-logs/log[last()]/status[text()="open"]/../username)'
}

var articleLoopCountInTableReport = 500;

var hideRepeatedStage = {
    "default":['addJob', 'File download', 'Convert Files', 'Support', 'Hold']
}

var cardViewFilterSeperator = {
    "support": {
        "SecondaryFilterNames": ["supportstage"]
    }
}
