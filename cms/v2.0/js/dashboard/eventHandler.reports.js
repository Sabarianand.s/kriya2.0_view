/**
 * This file will display the Report tables in dashboard
 */
var reportDoiList = {};
var articles = '';
(function (eventHandler) {
    eventHandler.tableReportsSummary = {
        getCustomerData: function (param) {
            if (!param && (!param.customer || !param.project)) {
                return false;
            }
            $('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 50) - $('#footerContainer').height()) + 'px');
            var defaultTable = JSON.parse(JSON.stringify(eventHandler.tableReportsSummary.tableConfig));
            defaultTable.forEach(function (config) {
                var configuration = config;
                $('#dashboardReports #' + configuration.tableId).html('<div  class="chartBoxDiv" style="min-height:30%;">    </div>')
                $('#reportContent .chartBoxDiv').css('background-color', 'white');
                $('#reportContent .chartBoxDiv').html('<div class="chartLoader"></div>');
                // $('#dashboardReports #' + configuration.tableId).html('');
                if (configuration.apiDetails) {
                    var result = [];
                    configuration.apiDetails.forEach(function (api, apiIndex) {
                        for (parameter in api) {
                            if (param[parameter]) {
                                api[parameter] = param[parameter];
                            } else if (param['project'] && parameter == "regex") {
                                api['project'] = param['project'].replace(/ OR /g, '|');
                            }
                        }
                        configuration.tableName = configuration.tableName.replace('{from}', moment(api.fromDate).format('MMM D, YYYY'))
                        configuration.tableName = configuration.tableName.replace('{to}', moment(api.toDate).format('MMM D, YYYY'))
                        $.ajax({
                            type: "POST",
                            url: '/api/getarticlelist',
                            data: api,
                            dataType: 'json',
                            success: function (respData) {
                                if (!respData || respData.length == 0) {
                                    return;
                                }
                                if (api.mainDataXpath) {
                                    var input = JSONPath({ json: respData, path: api.mainDataXpath })[0];
                                }
                                if (configuration.apiDetails.length == 1) {
                                    eval(configuration.functionToCall)(input, configuration, param.customer, param.roleType, param.accessLevel);
                                } else {
                                    // result.push(input);
                                    // var pushObj = { 'stage': api.stageName, 'data': input };
                                    for (each of input) {
                                        each['currentStage'] = api.stageName;
                                    }
                                    Array.prototype.push.apply(result, input);
                                    if (configuration.apiDetails.length - 1 == apiIndex) {
                                        eval(configuration.functionToCall)(result, configuration, param.customer, param.roleType, param.accessLevel);
                                    }
                                }
                            },
                            error: function (respData) {
                                console.log(respData);
                            }
                        });
                    })
                }
            });
        },
        getDailyReport: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            var tableId = configuration.tableId;
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                    })
                    fullTable += '</tr></thead>'
                }
            }
            fullTable += '<tbody></tbody></table>'
            var dotObj = [];
            var averageTimeTaken = 0;
            for (var i in input) {
                var currData = input[i];
                if (currData && currData._source && currData._source.stage) {
                    var stages = currData._source.stage;
                    if (stages.constructor !== Array) {
                        stages = [stages];
                    }
                    [currData.currentStage].forEach(function (eachStage) {
                        var minutesTaken = proofCount = 0;
                        var signedOffBy = '';
                        var stageEndDate = '';
                        var stageFound = false;
                        for (stageIndex in stages) {
                            stageIndex = Number(stageIndex);
                            var currentStage = stages[stageIndex];
                            if (/^(add\s?Job|File\s?download|Convert\s?Files|Support|hold)$/i.test(currentStage.name)) {

                            } else if (currentStage.name == eachStage) {
                                stageFound = true;
                                if (currentStage['job-logs'] && currentStage['job-logs'].log) {
                                    var currentLogs = currentStage['job-logs'].log;
                                    if (currentLogs.constructor !== Array) {
                                        currentLogs = [currentLogs];
                                    }
                                    for (eachLog of currentLogs) {
                                        var startDate = eachLog['start-date'] + ' ' + eachLog['start-time'];
                                        startDate = new Date(startDate);
                                        var endDate = eachLog['end-date'] + ' ' + eachLog['end-time'];
                                        endDate = new Date(endDate);
                                        minutesTaken += parseInt((endDate - startDate) / 1000) / 60;
                                        averageTimeTaken += Math.floor(parseInt((endDate - startDate) / 1000) / 60);
                                        // minutesTaken += moment(endDate, 'YYYY-MM-DD HH:mm:ss').diff(moment(startDate, 'YYYY-MM-DD HH:mm:ss'), 'minutes');
                                    }
                                    signedOffBy = currentStage.assigned.to;
                                    stageEndDate = currentStage['end-date'];
                                }
                                if (currentStage.object && currentStage.object.proof && currentStage.object.proof.pdf) {
                                    if (currentStage.object.proof.pdf.constructor !== Array) {
                                        proofCount += Object.keys(currentStage.object.proof.pdf).length;
                                    } else {
                                        proofCount += currentStage.object.proof.pdf.length;
                                    }
                                }
                            } else if (stageFound && currentStage.name != eachStage) {
                                stageFound = false;
                                break;
                            }
                        }
                        // var hours = Math.trunc(minutesTaken / 60);
                        // var minutes = (minutesTaken / 60).toFixed(2);
                        // minutes = Math.ceil(minutes.split('.')[1] * 0.6);
                        var hours = Math.floor(minutesTaken / 60);
                        var minutes = Math.floor(minutesTaken % 60);
                        var objParam = {
                            "manuscriptType": currData._source.articleType,
                            "date": stageEndDate,
                            "doi": currData._source.id,
                            "stage": eachStage,
                            "hours": hours,
                            "minutes": minutes,
                            "signedOffBy": signedOffBy,
                            "proofCount": proofCount
                        }
                        dotObj.push(objParam);
                    })
                }
            }
            var articleLength = input.length;
            fullTable = '<h4>' + configuration.tableName + ' ( ' + input.length + ' articles)<div style="display: inline;float: right;"> Average Time : ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) / 60)) : '0') + ' h ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) % 60)) : '0') + ' m</div></h4><table class="table" style="background-color:white;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">' + fullTable;
            $('#dashboardReports #' + tableId).html($(fullTable));
            var pagefn = doT.template(document.getElementById('dailyReport').innerHTML, undefined, undefined);
            $('#dashboardReports #' + tableId).find('tbody').append(pagefn(dotObj));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
        },
        createTable: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            var tableId = configuration.tableId;
            reportDoiList[tableId] = {};
            var tableName = configuration.tableName.replace(/\'/g,'\\\'');
            var fullTable = '<h4>' + configuration.tableName + '</h4><table class="table" style="background-color:white;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\''+ tableName +'\')">' + eachCell.name + '</a></th>'
                    })
                    fullTable += '</tr></thead>'
                    // fullTable += configuration.headings.rowHeading
                }
            }
            if (configuration.order && configuration.order.array && configuration.order.stageVariable) {
                var oldStage = input;
                var input = [];
                configuration.order.array.forEach(function (stageOrder) {
                    if (oldStage.find(o => o[configuration.order.stageVariable] === stageOrder) != undefined)
                        input.push(oldStage.find(o => o[configuration.order.stageVariable] === stageOrder))
                })
            }
            input.forEach(function (keyValue) {
                var table = '';
                var totalCount = 0;
                var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                reportDoiList[tableId][firstRowName] = {};
                fullTable += '<tr><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..doi\',\'' + firstRowName + '\',\''+ tableName +'\')">' + firstRowName + '</a></td>';
                var percentage = 0;
                var innerInput = JSONPath({ json: keyValue, path: configuration.columnvalueXpath })[0];
                if (configuration.innerOrder && configuration.innerOrder.array && configuration.innerOrder.stageVariable) {
                    var oldStage = innerInput;
                    var innerInput = [];
                    configuration.innerOrder.array.forEach(function (stageOrder) {
                        if (oldStage.find(o => o[configuration.innerOrder.stageVariable] === stageOrder) != undefined)
                            innerInput.push(oldStage.find(o => o[configuration.innerOrder.stageVariable] === stageOrder))
                    })
                }
                innerInput.forEach(function (values, keyCount) {
                    var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                    var articleCount = JSONPath({ json: values, path: '$..doc_count' })[0];
                    reportDoiList[tableId][firstRowName][columnHeading] = {};
                    reportDoiList[tableId][firstRowName][columnHeading]['doi'] = [];
                    JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                        reportDoiList[tableId][firstRowName][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                    })
                    percentage = ((articleCount / JSONPath({ json: keyValue, path: configuration.percentageXpath })[0]) * 100);
                    totalCount += articleCount;
                    var title = configuration.headings.rowHeading.filter(obj => {
                        return obj.value === columnHeading
                    })
                    if (articleCount) {
                        if (configuration.percentage) {
                            table += '<td><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '.' + columnHeading + '.doi\',\'' + firstRowName + ' - ' + title[0].name + '\',\''+ tableName +'\')">' + articleCount + ' (' + percentage.toFixed(2) + '%)</a></td>'
                        } else {
                            table += '<td><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '.' + columnHeading + '.doi\',\'' + firstRowName + ' - ' + title[0].name + '\',\''+ tableName +'\')">' + articleCount + '</a></td>'
                        }
                    } else {
                        if (configuration.percentage) {
                            table += '<td>' + articleCount + ' (' + percentage.toFixed(2) + '%)</td>'
                        } else {
                            table += '<td>' + articleCount + '</td>'
                        }
                    }
                })
                if (configuration.totalCountFront) {
                    table = '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..doi\',\'' + firstRowName + ' - In-hand \',\''+ tableName +'\')">' + totalCount + '</a></td>' + table + '</tr>';
                } else if (configuration.totalCountBack) {
                    table += '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..doi\',\'' + firstRowName + ' - Total\',\''+ tableName +'\')">' + totalCount + '</a></td></tr>'
                }
                fullTable += table;
            })
            fullTable += '</table>'
            if($(fullTable).find('tbody').length == 0){
                fullTable = fullTable.replace(/onclick=".*?"/g, '');
            }
            $('#dashboardReports #' + tableId).html($(fullTable));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
        },
	createTableWithSpanAttributes: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            var tableId = configuration.tableId;
            reportDoiList[tableId] = {};
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '<h4>' + configuration.tableName + '</h4><table class="table table-bordered" style="background-color:white;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    var sub = '<tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell, index) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        if (index == 0) {
                            fullTable += '<th scope="col" rowspan="2"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                        } else {
                            fullTable += '<th scope="col" colspan="2"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                        }
                        if (index == 1) {
                            sub += '<th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..Production..doi\',\'In-hand - Production\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">Production</a></th><th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..Support..doi\',\'In-hand - Support\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">Support</a></th>'
                        } else if (index) {
                            ['Production', 'Support'].forEach(function (value) {
                                sub += '<th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..' + eachCell.value + '..' + value + '..doi\',\'' + eachCell.name + ' - ' + value + '\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">' + value + '</a></th>'
                            })
                        }
                    })
                    sub += '</tr></thead>'
                    fullTable += '</tr>' + sub;
                }
            }
            if (configuration.order && configuration.order.array && configuration.order.stageVariable) {
                var oldStage = input;
                var input = [];
                configuration.order.array.forEach(function (stageOrder) {
                    if (oldStage.find(o => o[configuration.order.stageVariable] === stageOrder) != undefined)
                        input.push(oldStage.find(o => o[configuration.order.stageVariable] === stageOrder))
                })
            }
            input.forEach(function (keyValue) {
                var table = '';
                var totalCount = 0;
                var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                reportDoiList[tableId][firstRowName] = {};
                fullTable += '<tr><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..doi\',\'' + firstRowName + '\',\'' + tableName + '\')">' + firstRowName + '</a></td>';
                var percentage = 0;
                var innerInput = JSONPath({ json: keyValue, path: configuration.columnvalueXpath })[0];
                if (configuration.innerOrder && configuration.innerOrder.array && configuration.innerOrder.stageVariable) {
                    var oldStage = innerInput;
                    var innerInput = [];
                    configuration.innerOrder.array.forEach(function (stageOrder) {
                        if (oldStage.find(o => o[configuration.innerOrder.stageVariable] === stageOrder) != undefined)
                            innerInput.push(oldStage.find(o => o[configuration.innerOrder.stageVariable] === stageOrder))
                    })
                }
                var categoryCount = {};
                innerInput.forEach(function (values, keyCount) {
                    var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                    reportDoiList[tableId][firstRowName][columnHeading] = {};
                    var value = {};
                    Object.keys(values.Articles).sort().forEach(function (key) {
                        value[key] = values.Articles[key];
                    });
                    Object.keys(value).forEach(function (category, categoryIndex) {
                        if (category != 'doc_count') {
                            var currentCategory = values.Articles[category];
                            reportDoiList[tableId][firstRowName][columnHeading][category] = {};
                            reportDoiList[tableId][firstRowName][columnHeading][category]['doi'] = [];
                            reportDoiList[tableId][firstRowName][columnHeading][category]['doi'] = currentCategory.buckets.map((key) => { return key.key; });
                            var articleCount = currentCategory.buckets.length;
                            if (categoryCount[category] == undefined) {
                                categoryCount[category] = articleCount;
                            } else {
                                categoryCount[category] += articleCount;
                            }
                            var title = configuration.headings.rowHeading.filter(obj => {
                                return obj.value === columnHeading
                            })
                            if (articleCount) {
                                table += '<td><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '.' + columnHeading + '.' + category + '.doi\',\'' + firstRowName + ' - ' + title[0].name + ' - ' + category + '\',\'' + tableName + '\')">' + articleCount + '</a></td>'
                            } else {
                                table += '<td> - </td>';
                            }
                        }
                    })
                })
                if (configuration.totalCountFront) {
                    table = '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..Production..doi\',\'' + firstRowName + ' - In-hand - Production\',\'' + tableName + '\')">' + ((categoryCount['Production'] == 0) ? '-' : categoryCount['Production']) + '</a></td><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..Support..doi\',\'' + firstRowName + ' - In-hand - Support\',\'' + tableName + '\')">' + ((categoryCount['Support'] == 0) ? '-' : categoryCount['Support']) + '</a></td>' + table + '</tr>';
                }
                fullTable += table;
            })
            fullTable += '</table>'
            if ($(fullTable).find('tbody').length == 0) {
                fullTable = fullTable.replace(/onclick=".*?"/g, '');
            }
            $('#dashboardReports #' + tableId).html($(fullTable));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
        },
        daysInProduction: function (input, configuration, customer, roleType, accessLevel) {
            input = input.map((eachArray) => {
                return JSONPath({ json: eachArray, path: '$..script_score' })[0][0]
            })
            var dotData = {};
            var dotOutput = [];
            var dotOutputDoi = [];
            var tableId = configuration.tableId;
            reportDoiList[tableId] = {};
            dotData.data = input;
            dotData.output = dotOutput;
            dotData.outputDoi = dotOutputDoi;
            var pagefn = doT.template(document.getElementById('reportDaysInProdCalc').innerHTML, undefined, undefined);
            pagefn(dotData);
            reportDoiList[tableId]['doi'] = dotOutputDoi;
            if (dotOutput.length) {
                var overAllDays = dotOutput.reduce((previous, current) => current += previous);
                var path = '$..' + tableId + '..doi';
                var averageDay = Math.ceil(overAllDays / dotOutput.length) + ' days / <span style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'Articles\',\'' + configuration.tableName + '\')">' + dotOutput.length + ' articles</span>';
            } else {
                var averageDay = 'No articles'
            }
            var fullTable = '<h4>' + configuration.tableName + ' : ' + averageDay + '</h4>';
            $('#dashboardReports #' + tableId).html($(fullTable));
        },
        reportCompletedStageCalculation: function (input, configuration, customer, roleType, accessLevel) {
            input = input.map((eachArray) => {
                return JSONPath({ json: eachArray, path: '$..script_score' })[0][0]
            })
            var dotData = {};
            var dotOutput = {};
            var tableId = configuration.tableId;
            dotData.data = input;
            dotData.output = dotOutput;
            var pagefn = doT.template(document.getElementById('reportCompletedStageCalculation').innerHTML, undefined, undefined);
            pagefn(dotData);
            reportDoiList[tableId] = dotData.output;
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '<h4>' + configuration.tableName + '( '+ input.length +' articles )</h4><table class="table" style="background-color:white;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                    })
                    fullTable += '</tr></thead>'
                }
            }
            configuration.order.array.forEach(function (stageName) {
                var stage = dotData.output[stageName];
                if (stage != undefined) {
                    fullTable += '<tr><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + stageName + '..doi\',\' '+ stageName +' \',\'' + tableName + '\')">' + stageName + '</a></td>'
                    configuration.innerOrder.array.forEach(function (innerObj) {
                        var title = configuration.headings.rowHeading.filter(obj => {
                            return obj.value === innerObj
                        })
                        if (stage[innerObj] == undefined) {
                            fullTable += '<td><a style="cursor: unset;">0</a></td>';
                        } else {
                            var length = stage[innerObj]['doi'].length;
                            fullTable += '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + stageName + '.' + innerObj + '.doi\',\'' + stageName + ' - ' + title[0].name + '\',\'' + tableName + '\')">' + length + '</a></td>';
                        }
                    })
                    fullTable += '</tr>';
                }
            });
            fullTable += '</table>';
            if ($(fullTable).find('tbody').length == 0) {
                fullTable = fullTable.replace(/onclick=".*?"/g, '');
            }
            $('#dashboardReports #' + tableId).html($(fullTable));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
        },
        articlesReport: function (tableId, customer, xPath, titleName, tableName, fromTotal, toTotal) {
            if(fromTotal == undefined){
                $('.la-container').css('display', '');
            }
            // By referring https://stackoverflow.com/questions/49035237/how-to-find-and-match-a-specific-value-in-a-json-object-array
            var configuration = eventHandler.tableReportsSummary.tableConfig.find(o => Object.entries(o).find(([k, value]) => k === 'tableId' && value === tableId) !== undefined);
            var param = {};
            articles = [].concat.apply([], JSONPath({ json: reportDoiList, path: xPath }))
            if (fromTotal == undefined && toTotal == undefined) {
                var fromTotal = 0;
                var toTotal = articleLoopCountInTableReport;
            }
            var doiString = articles.slice(fromTotal, toTotal).join(' OR ').replace(/,/g, ' OR ');
            if(doiString == ''){
                $('.la-container').css('display', 'none');
                return;
            }
            $('#modalReport #titleText').text(tableName);
            $('#modalReport #clickedText').text(titleName);
            if (configuration.displayReport) {
                Object.keys(configuration.displayReport).forEach(function (config) {
                    if (config == 'doistring') {
                        param[config] = doiString;
                    } else {
                        param[config] = configuration.displayReport[config];
                    }
                })
            }
            if (configuration && configuration.displayReport && configuration.displayReport.tableType) {
                param[configuration.displayReport.tableType] = true;
            }
            param.customer = customer;
            var fromReportTable = {
                "tableId": tableId,
                "xPath": xPath
            }
            eventHandler.bulkedit.export.exportTable(param, customer, fromTotal, toTotal, articles.length, configuration.displayReport.tableType, configuration.chartTableType, configuration.chartTableTemplateName, '', configuration,'', fromReportTable, titleName, tableName);
        },
        tableConfig: [
            {
                'apiDetails': [{
                    'urlToPost': 'getReportDaysInProduction',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'stage': 'Archive',
                    'workflowStatus': 'completed',
                    'date': 'stageDue'
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'daysInProd',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.daysInProduction',
                'tableName': 'Average Days in Production for Published Articles ({from} - {to}) ',
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getPlannedArticles',
                    'workflowStatus': '*',
                    'date': 'stage.planned-end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..project.buckets',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'planned',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.createTable',
                'tableName': 'Planned Articles Summary ({from} - {to}) ',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Stage', 'value': '' }, { 'name': '11:00', 'value': '11h' }, { 'name': '13:00', 'value': '13h' }, { 'name': '15:00', 'value': '15h' }, { 'name': '17:00', 'value': '17h' }, { 'name': '19:00', 'value': '19h' }, { 'name': '24:00', 'value': '24h' }, { 'name': 'Total', 'value': '' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..year.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getPlannedArticles',
                    'workflowStatus': 'completed',
                    'date': 'stage.end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..project.buckets',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'actualCompleted',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.createTable',
                'tableName': 'Actual Completed Articles Summary ({from} - {to})',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Stage', 'value': '' }, { 'name': '11:00', 'value': '11h' }, { 'name': '13:00', 'value': '13h' }, { 'name': '15:00', 'value': '15h' }, { 'name': '17:00', 'value': '17h' }, { 'name': '19:00', 'value': '19h' }, { 'name': '24:00', 'value': '24h' }, { 'name': 'Total', 'value': '' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..year.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check']
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getCompletedArticlesStageDuration',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'stage': 'Archive',
                    'workflowStatus': 'completed',
                    'date': 'stageDue'
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'publishedArticleDurationStage',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.reportCompletedStageCalculation',
                'tableName': 'Delay in Each Stage for Published Articles ({from} - {to})',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Stage', 'value': '' }, { 'name': 'No delay', 'value': 'No Overdue' }, { 'name': '1 day(s) delay at stage', 'value': '1Day' }, { 'name': '2 day(s) delay at stage', 'value': '2Days' }, { 'name': '3 day(s) delay at stage', 'value': '3Days' }, { 'name': '>=4 day(s) delay at stage', 'value': '4Days' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days', '3Days', '4Days'],
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getInHandArticlesSeggregation',
                    'workflowStatus': 'in-progress',
                    'date': 'stage.sla-end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..project.buckets',
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'overallPlanned',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.createTableWithSpanAttributes',
                'tableName': 'Today\'s Status Of Articles In Hand With Exeter',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Stage', 'value': '' }, { 'name': 'In-hand', 'value': '' }, { 'name': 'No delay', 'value': 'No Overdue' }, { 'name': '1 day(s) delay at stage', 'value': '1Day' }, { 'name': '2 day(s) delay at stage', 'value': '2Days' }, { 'name': '3 day(s) delay at stage', 'value': '3Days' }, { 'name': '>=4 day(s) delay at stage', 'value': '4Days' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days', '3Days', '4Days'],
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Pre-editing',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Copyediting',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Check',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Review',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                }],
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'tableId': 'dailyReport',
                'functionToCall': 'eventHandler.tableReportsSummary.getDailyReport',
                'tableName': 'Articles Report Summary ({from} - {to}) ',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Date', 'value': '' }, { 'name': 'DOI', 'value': '' }, { 'name': 'Manuscript Type', 'value': '' }, { 'name': 'Stage', 'value': '' }, { 'name': 'Hours', 'value': '' }, { 'name': 'Minutes', 'value': '' }, { 'name': 'Proof Count', 'value': '' }, { 'name': 'Signed-off By', 'value': '' }]
                }
            }
        ]
    }
})(eventHandler || {});
