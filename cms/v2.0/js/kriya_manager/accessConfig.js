var accessConfig = {
    "production": {
        "developer": {
            "production": [
                "developer",
                "support",
                "production",
                "admin",
                "manager"
            ],
            "publisher": [
                "production",
                "manager"
            ],
            "copyeditor": [
                "copyeditor",
                "vendor",
                "production"
            ]
        },
        "manager": {
            "production": [
                "production"
            ]
        }
    },
    "copyeditor": {
        "manager": {
            "copyeditor": [
                "manager",
                "vendor",
                "production"
            ]
        }
    }
}
