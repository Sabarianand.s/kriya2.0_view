eventHandler.langTrans = {
    pagination : function(param, onSuccess, onError){
        var parameters = {
            "client": param['customer'],
            "project": 'default',
            "idType": "doi",
            "doi":	param['doi'],
            "id": param['doi']+".table",
            "processType":"MicroService",
            "service" : 'langtrans',
            "tableURL" : param['url'],
            "apiKey":"cde4c89b-e452-4ba5-b493-01c691033b72"
        };
        
        $.ajax({
            type: 'POST',
            url: "https://kriya2.kriyadocs.com/api/pagination",
            data: parameters,
            crossDomain: true,
            success: function(data){
                console.log("from pagination ");
                if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
                    eventHandler.langTrans.getJobStatus(parameters.client, data.message.jobid,parameters.doi, onSuccess, onError);
                }
                else if(data == ''){
                    console.log('Failed..');
                }else{
                    console.log(data.status.message);
                    if(data.status.message == "job already exist"){
                        setTimeout(function() {
                            eventHandler.langTrans.getJobStatus(parameters.client, data.message.jobid,parameters.doi,onSuccess, onError);
                        }, 1000 );
                    }
                }
            },
            error: function(error){
                onError(error)
            }
        });
    },
    
    getJobStatus : function(customer, jobID, doi, onSuccess, onError){
        //console.log('Job ID '+jobID);
        $.ajax({
            type: 'POST',
            url: "https://kriya2.kriyadocs.com/api/jobstatus",
            data: {"id": jobID, "doi":doi, "customer":customer,"apiKey":"cde4c89b-e452-4ba5-b493-01c691033b72"},
            crossDomain: true,
            success: function(data){
                if(data && data.status && data.status.code && data.status.message && data.status.message.status){
                    var status = data.status.message.status;
                    var code = data.status.code;
                    var currStep = 'Queue';
                    if(data.status.message.stage.current){
                    currStep = data.status.message.stage.current;
                    }    
                    var loglen = data.status.message.log.length;    
                    var process = data.status.message.log[loglen-1];
    
                    if (/completed/i.test(status)){
                        var loglen = data.status.message.log.length;	
                        var process = data.status.message.log[loglen-1];	
                        onSuccess(data);
    
                    }else if (/failed/i.test(status) || code != '200'){
                        onError(process);
                        console.log("process failed");
                    }else if (/no job found/i.test(status)){
                        onError(status);
                    }else{
                        var loglen = data.status.message.log.length;	
                        if(loglen > 1){
                            var process = data.status.message.log[loglen-1];
                            console.log(process);
                        }
                        setTimeout(function() {
                            eventHandler.langTrans.getJobStatus(customer, jobID, doi, onSuccess, onError);
                        }, 1000 );
                    }
                }
                else{
                    onError('Failed');
                }
            },
            error: function(error){
                onError(error);
            }
        });
    },
    langTrans : function(tbl){
        $('.la-container').fadeIn();
        var fileStr = '';
        
        $(tbl).each(function(i,val){
            fileStr +=val.outerHTML;
        });
        
        var blob = new Blob([fileStr], {type : 'text/html'});
        var fileToBeUp = new File([blob], Math.round((new Date()).getTime() / 1000)+'.html');

        var param = {
            'file': fileToBeUp, 
            'convert':'false',
            'success': function(res){
                /**
                 * Callback function for upload file success
                 * Update the url in image 
                 * Remove the progress bar in block
                 */
                var fileDetailsObj = res.fileDetailsObj;
                var filePath = '/resources/' + fileDetailsObj.dstKey;
                var paramPagination = kriya.config.content;
                paramPagination['url'] = "https://kriya2.kriyadocs.com"+filePath;

                eventHandler.langTrans.pagination(paramPagination, function(res){
                    result = res.status.message.output;

                    if((typeof(result)!='object') && (result.length<=0)) {
                        $('.la-container').fadeOut();
                        
                        kriya.notification({
                            title : 'LanguageTranslator',
                            type  : 'warning',
                            content : 'everything fine, have to find others',
                        });
                    }

                    console.log(result);
                    eventHandler.langTrans.regenerateTable(result);
                },
                function(err){
                    console.log(err);
                });
                
            },
            'error': function(err){
                blockNode.progress('', true);
                kriya.notification({
                    title: 'Failed',
                    type: 'error',
                    content: 'Uploading file failed.',
                    timeout: 8000,
                    icon : 'icon-info'
                });
            }
        }

        console.log("calling uploadFileFunc");
        eventHandler.menu.upload.uploadFile(param, "");
        console.log("comp");

    },
    langTransTables : function(){
        var currSelectionNode = kriya.selection.startContainer;
        if(kriya && currSelectionNode && $(currSelectionNode).closest('[data-type="Example"]').find('table').length > 0){

            var clonedTable = $(currSelectionNode).closest('[data-type="Example"]').find('table').clone(true);
            $(clonedTable).each(function(){
                if(this.rows.length!=2){
                    kriya.notification({
                        title: 'Failed',
                        type: 'error',
                        content: 'unHandeled Row Count',
                        timeout: 8000,
                        icon : 'icon-info'
                    });
                    $('.la-container').fadeOut();
                    return false;
                } 
            });

            return clonedTable;
        }else{
            $('.la-container').fadeOut();
            kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'TableNot Found', icon: 'icon-warning2'});
            return false;
        }
    },
    regenerateTable: function(result){
        var emptyCheck = false;
        if(result.length>1) emptyCheck = true;
            var tables = eventHandler.langTrans.langTransTables();

/*             if(!emptyCheck && eventHandler.langTrans.validateTable(tables,result[0].length)){
                $(currSelectionNode).closest('table').attr('data-col-widths-px',eventHandler.langTrans.colWidthStr(result[0]));
                $('.la-container').fadeOut();
            }
 */
                eventHandler.langTrans.formTable(tables,result);

        
    },
    validateTable:function(table,colLen){
        var cols=0;
        $(table.rows).each(function(i,val){
            cols = Math.max(val.cells.length,cols);
        });
        if(cols!=colLen) return false;
        return true;
    },

    colWidthStr:function(arr){
        var returnStr= [];
        $(arr).each(function(i,val){
            returnStr.push(val.width);
        });
        return returnStr.toString();
    },

    mergeTable:function(inputTable){
        console.log('merging tables...');
        var tblLen = inputTable.length;
        for(t=1;t<tblLen;t++){
            var curTable = inputTable[t];
            if(inputTable[t].rows.length!=2){
                console.log('found more rows');
                return false;
            }
            else{
                var curLength = curTable.rows.length;
				if ($(curTable).find('tr:first-child td:eq(0)').text() == "" && $(curTable).find('tr:last-child td:eq(0)').text() == ""){
					$(curTable).find('tr:first-child td:eq(0)').remove();
					$(curTable).find('tr:last-child td:eq(0)').remove();
				}
				if ($(curTable).find('tr:first-child td:eq(0)').text() == "" && $(curTable).find('tr:last-child td:eq(0)').text() == ""){
					$(curTable).find('tr:first-child td:eq(0)').remove();
					$(curTable).find('tr:last-child td:eq(0)').remove();
				}
                for(row=0;row<curLength;row++){
                    $('#construction>table:first-child tr:eq('+row+')').append(curTable.rows[row].cells);
                }
            }
        }
        eventHandler.langTrans.removeEmptyTables(1,inputTable);
    },

    appendTable:function(){
        $('#construction>#result').append('<table><tr></tr><tr></tr></table>');
    },

    /* removing empty table from #inputTable */
    removeEmptyTables:function(count,inputTable){
        console.log('removing Empty tables...');
        var len = inputTable.length;
        var del = true,rowLen = 0;
        for(t=count;t<len;t++){
            rowLen = inputTable[t].rows;
            for(row = 0;row < rowLen;row++){
                if(inputTable[t].rows[row].cells.length!=0) del = false;
            }   
            if(del)
            inputTable[t].remove();
        }
    },

    finalize:function(){
        var currSelectionNode = kriya.selection.startContainer;
        var parentDiv = $(currSelectionNode).closest('[data-type="Example"]');
        parentDiv.html($('#construction>#result').html());
        $('#construction').remove();
        kriyaEditor.settings.undoStack.push(parentDiv);
        kriyaEditor.init.addUndoLevel('save-component');

        $('.la-container').fadeOut();
    },

    formTable:function(table,result){
        
        $('body').append('<div id="construction" style="display:none"></div>');
        $('#construction').append(table);

        eventHandler.langTrans.mergeTable(table);
        $('#construction').append('<div id="result"></div>');

        var inputFirstTable = '#construction>table:first-child';
        var tableAppend = '#construction>#result table:last-child';
        $(result).each(function(i,val){
                eventHandler.langTrans.appendTable();
                var colWidthArr = eventHandler.langTrans.colWidthStr(val);
                $(tableAppend).attr({'data-col-widths-px':colWidthArr,'data-table-continue':i});

                $(val).each(function(j,value){
                    if(value.data){
                        $(tableAppend +' tr:first-child').append($(inputFirstTable+' tr:first-child td:first-child'));
                        $(tableAppend +' tr:last-child').append($(inputFirstTable+' tr:last-child td:first-child'));
                    }
                    else{
                        $(tableAppend +' tr:first-child ').append('<td></td>');
                        $(tableAppend +' tr:first-child td:last-child').attr('width',value.width);
                        $(tableAppend +' tr:last-child ').append('<td></td>');
                        $(tableAppend +' tr:last-child td:last-child').attr('width',value.width);
                    }

                    $(tableAppend +' tr:first-child td:last-child').attr('data-col-index',value.colIndex+1);
                    $(tableAppend +' tr:last-child td:last-child').attr('data-col-index',value.colIndex+1);
                });
            });
            eventHandler.langTrans.finalize();
        },
};