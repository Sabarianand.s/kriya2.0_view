var binder = {
    meta: {
        "journal-name._text": {
            "component": "text",
            "label": "Journal Name: test data addedddd "
        },
        "test" :
        {
            "data":"sabari"
        },
        "test 2":{
          "data" : "tamil"  
        },
        "journal-abbrev._text": {
            "component": "text",
            "label": "Journal Abbreviation: "
        },
        "volume._text": {
            "component": "text",
            "label": "Volume: "
        },
        "issue._text": {
            "component": "text",
            "label": "Issue: "
        },
        "day._text": {
            "component": "text",
            "label": "Publication Day: "
        },
        "month._text": {
            "component": "text",
            "label": "Publication Month: "
        },
        "year._text": {
            "component": "text",
            "label": "Publication Year: "
        },
        "page-count._text": {
            "component": "text",
            "label": "Page Count: "
        }
    },
    "cover": {
        "data.p": {
            "component": "contenteditable",
            "label": "Cover text"
        },
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "editorial-board": {
        "file._attributes.path": {
            "component": "file",
            "label": "File"
        }
    },
    "advert": {
        "region": {
            "component": "region",
            "label": "Region"
        }
    },
    "table-of-contents": {
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "filler-toc": {
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        }
    },
    "blank": {
        "file._attributes.path": {
            "component": "file",
            "label": "File"
        }
    },
    "manuscript": {
        "_attributes.id": {
            "component": "text",
            "label": "ID"
        },
        "file._attributes.path": {
            "component": "pdf",
            "label": "PDF"
        },
        "color._attributes": {
            "component": "color",
            "label": "Color in print"
        }
    }
}
var convert = '';
var configData = {};
var added_after_changes;
function showMessage(obj) {
    var pnObj = {
        text: obj.text, //'That thing that you were trying to do worked.',
        type: obj.type,
        buttons: { closer: obj.closer, sticker: false },
    }
    if (obj.title){
        pnObj.title = obj.title
    }
    new PNotify(pnObj);
}

function resetData() {
    $('#listProject, #listIssue').remove();
    $('#newIssue').remove();
    $('#detailsContainer').find(">div").addClass('hide');
    $('#search').val('');
    $('.header .icons').addClass('hide');
    $('.header .mergePdfIcon').addClass('hide');
    $('#tocContainer .bodyDiv, #uaaDiv').html('')
}

/**
 * general get method to fetch JSON data using input url
 * @param {*} obj.url - url to fetch data from, obj.objectName - data to return from response
 */
function getDataFromURL(obj) {
    return new Promise(function (resolve, reject) {
        jQuery.ajax({
            type: "GET",
            url: obj.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg[obj.objectName] && msg[obj.objectName].response){
                    var response = msg[obj.objectName].response;
                    if (response.status.code._text == 200){
                        resolve({ 'code': 200, 'data': response.data });
                    }else{
                        showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                        reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
                    }
                }
                else if (msg[obj.objectName]) {
                    resolve({ 'code': 200, 'data': msg[obj.objectName] });
                }
                else {
                    showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                    reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
                }
            },
            error: function (xhr, errorType, exception) {
                showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
            }
        });
    })
}

function getCustomerList() {
    return new Promise(function (resolve, reject) {
        resetData();

        var obj = { 'url': '/api/customers', 'objectName': 'customer' };
        getDataFromURL(obj)
            .then(function (customer) {
                if ((typeof (customer) == 'undefined') || (typeof (customer.data) == 'undefined') || (customer.data.length == 0)) {
                    showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                    reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
                    return false;
                }
                var cData = customer.data;
				if (cData.constructor.name == 'Object') cData = [cData];
                var cLen = cData.length;
                var cHTML = '';
                for (var cIndex = 0; cIndex < cLen; cIndex++) {
                    cHTML += '<p class="customer folder" data-customer="' + cData[cIndex].name + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}" ><span class="cSpan"><span class="icon-folder-o"/>&#x00A0;' + cData[cIndex].name + '</span></p>';
                }
                $('#cList').html(cHTML);
                resolve(true);
            })
            .catch(function (err) {
                showMessage({ 'text': 'Something went wrong when fetching customer details. Please try again or contact support', 'type': 'error' });
                reject(err)
            })
    })
}

function getProjectList(cName) {
    return new Promise(function (resolve, reject) {
        resetData();
        $('#explorerNav').append('<li class="customer" id="listProject" data-customer="' + cName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + cName + '</span>&#x00A0;</li>');
        var obj = { 'url': '/api/projects?customerName=' + cName, 'objectName': 'project' };
        getDataFromURL(obj)
            .then(function (project) {
                if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
                    showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                    reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
                    return false;
                }
                var pData = project.data
                var pLen = pData.length;
                var pHTML = '';
                for (var pIndex = 0; pIndex < pLen; pIndex++) {
                    pHTML += '<p class="project folder" data-customer="' + cName + '" data-project="' + pData[pIndex].name + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}" ><span class="cSpan"><span class="icon-folder-o"/>&#x00A0;' + pData[pIndex].name + '</span></p>';
                }
                $('#cList').html(pHTML);
                resolve(true);
            })
            .catch(function (err) {
                showMessage({ 'text': 'Something went wrong when fetching data. Please try again or contact support', 'type': 'error' });
                reject(err)
            })
    })
}

// function to create new issue xml
/* create a button - "create new issue" which creates a popup box to get the volume,issue and gets the dropped customer issue xml
call the api getIssue data to convert the customer issue xml to kriya issue xml, save the issue xml in the folder
reload the issue list
*/
function addNewIssue(cName, pName) {
    var newIssuecompObj = {
        type: 'issueUpload',
        data: '',
        label: 'Create new Issue',
    }
  $('#newIssue').removeClass('hide');
   var issueHTML = getComponent(newIssuecompObj);
    $('#newIssue').html(issueHTML);
}

function getIssueList(cName, pName) {
    return new Promise(function (resolve, reject) {
        resetData();
        $('#explorerNav')
            .append('<li class="customer" id="listProject" data-customer="' + cName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + cName + '</span>&#x00A0;</li>')
            .append('<li class="customer" id="listIssue" data-customer="' + cName + '" data-project="' + pName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + pName + '</span>&#x00A0;</li>')
            .append('<li class="newIssue" id="newIssue"><span class="controlIcons icon-add" data-message="{\'click\':{\'funcToCall\': \'generateIssueForm\',\'channel\':\'explorer\',\'topic\':\'customer\'}}"></span></li>');
        var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&process=getIssuesList', 'objectName': 'list' };
        getDataFromURL(obj)
            .then(function (project) {
                if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
                    showMessage({ 'text': 'Something went wrong when retrieving issue list. Please try again or contact support', 'type': 'error' });
                    reject({ 'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url });
                    return false;
                }
                try{
                    var pData = project.data.list;
                    if (pData.constructor.toString().indexOf("Array") == -1) {
                    pData = [pData];
                }
                var pLen = pData.length;
                var pHTML = '';
                for (var pIndex = 0; pIndex < pLen; pIndex++) {
                        var issueName = pData[pIndex]._text;
                        var fileName = issueName.replace(/^[a-z]+\_/gi, 'Volume ').replace(/\.xml$/gi, '');
                    fileName = fileName.replace(/\_/gi, ' Issue ');
                        pHTML += '<p draggable="true" class="list folder" data-customer="' + cName + '" data-project="' + pName + '" data-file="' + issueName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\'}" ><span class="cSpan"><span class="icon-file-text2"/>&#x00A0;' + fileName + '</span></p>';
                }
                $('#cList').html(pHTML);
                resolve(true);
                }
                catch(e){
                    reject('unable to parse issue list');
                }
            })
            .catch(function (err) {
                showMessage({ 'text': 'Something went wrong when retrieving issue list. Please try again or contact support', 'type': 'error' });
                reject(err)
            })
    })
}

function getIssueData(cName, pName, fileName) {
    return new Promise(function (resolve, reject) {
        $('.la-container').fadeIn();
        // hide the right panel and enable it after populating data
        $('#detailsContainer').find(">div").addClass('hide');
        var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&fileName=' + fileName + '&process=getIssueXML', 'objectName': 'list' };
        getDataFromURL(obj)
            .then(function (issueData) {
                configData = JSON.parse(JSON.stringify(issueData.data.issue.config));
                console.log(configData);
                delete(issueData.data.issue.config);
                issueData = issueData.data.issue;
                if (typeof(issueData['container-xml']['change-history']) == 'undefined'){
                    issueData['container-xml']['change-history'] = [];
                }
                else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
                    issueData['container-xml']['change-history'] = [];
                }
                $('#tocContainer').data('binder', issueData);
                $('#tocContainer').data('config', configData);
                $('.icons').removeClass('hide');
                $('.mergePdfIcon').removeClass('hide');
                populateMetaContainer();
                populateHistory();
                populateTOC();
                loadUnassignedArticles();
                loadBlankOrAdvertOrFiller();
				populateLayouts();
                $('.la-container').fadeOut();
                resolve(true);
            })
            .catch(function (err) {
                $('.la-container').fadeOut();
                showMessage({ 'text': 'Something went wrong when fetching issue data. Please try again or contact support', 'type': 'error' });
                reject(err)
            })
    })
}

function populateMetaContainer() {
    $('#metaHistory').removeClass('hide');
    $('#manuscriptData').addClass('hide');
    $('#advertsData').addClass('hide');
    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
    if ((typeof (meta) == 'undefined') || (typeof (binder.meta) == 'undefined')) {
        return false;
    }
    populateData($('#metadata'), 'meta', $('#tocContainer').data('binder')['container-xml'].meta);
}
function populateData(container, binderKey, data) {
    // destory any previously initialized select components
    //$('#metaHistory:visible, #manuscriptData:visible, #advertsData:visible, #unassignedArticles:visible').find('select').material_select('destroy');
    $('#metaHistory:visible, #manuscriptData:visible, #unassignedArticles:visible').find('select').material_select('destroy');
    $('#manuscriptData:visible, #advertsData:visible').find('.dropdown-button').dropdown('destroy');
    var config = binder[binderKey];
    var containerHTML = '';
    var tabhtml ='';
    var tabs = configData.binder[binderKey].tab;
    if (tabs.constructor.toString().indexOf("Array") < 0) {
        tabs = [tabs];
    }
    tabs.forEach(function (currTabObj) {//for each tab in the config, loop through each path and get the corresponding component data
        containerHTML = '';
        var tabName = currTabObj._attributes["type"]; //get the tab name from config
        var paths = currTabObj.path;
        if (paths.constructor.toString().indexOf("Array") < 0) {
            paths = [paths];
        }
        paths.forEach(function (currConfigObj) {
            var compPath = currConfigObj._attributes.string;
            var keyArr = compPath.split('.');
            var myData = data;
            keyArr.forEach(function (key) {
                if (myData[key]) {
                    myData = myData[key];
                }
                else if (myData.constructor.toString().indexOf("Array") > -1) {
                    var mData = [];
                    myData.forEach(function (mKey) {
                        mData.push(mKey[key]);
                    })
                    myData = mData;
                }
                else {
                    myData = '';
                }
            })
            //if (myData) {
            var compObj = {
                type: currConfigObj.component._text,
            data: myData,
                label: currConfigObj.label._text,
                key: compPath
            }
		    containerHTML += getComponent(compObj);
        });
        tabhtml += '<div class="'+ tabName +'">' + containerHTML +'</div>';
    });
    if (tabhtml) {
        // if the function is called to display manuscript data then do something special for the layout
        if ($(container).attr('id') == 'msDataDiv'){
            $('#pdfTab, #figsTab, #flagsTab').html('');
            tabhtml = $('<div>' + tabhtml + '</div>');
		    $('#pdfTab').html(tabhtml.find('.pdfTab').html());
            var figTabHTML = tabhtml.find('.figsTab').html();
            if (figTabHTML){
                $('#figsTab').html(tabhtml.find('.figsTab').html())
                $('[data-href="figsTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
            }
            else{
                $('[data-href="figsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
            }
            $('#flagsTab').html(tabhtml.find('.flagsTab').html());
        }
		else if ($(container).attr('id') == 'adDataDiv'){
            $('#regionTab').html('');
            $('#adDataDiv #psflagsTab').html('');
            tabhtml = $('<div>' + tabhtml + '</div>');
            var regTabHTML = tabhtml.find('.regionTab').html();
            if (regTabHTML){
                $('#regionTab').html(tabhtml.find('.regionTab').html())
                var flagtab = tabhtml.find('.flagsTab').html();
                if(flagtab){
                    $('#adDataDiv #psflagsTab').html(tabhtml.find('.flagsTab').html());
                    $('[data-href="psflagsTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
                }
                else{
                    $('[data-href="psflagsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
                }
                $('[data-href="regionTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
                
            }
            else{
                $('[data-href="regionTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
                $('[data-href="psflagsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
            }
           //  $('#pdfTab').html(tabhtml.find('.pdfTab').html());
            
        }
        else{
        $(container).html(tabhtml);
        }
    }
   // $('#metaHistory:visible, #manuscriptData:visible, #advertsData:visible, #unassignedArticles:visible').find('select').material_select();
    // initilaize materialize css for selct and dropdown button
   $('#metaHistory:visible, #manuscriptData:visible, #unassignedArticles:visible').find('select').material_select();
    $('#manuscriptData:visible, #advertsData:visible').find('.dropdown-button').dropdown();
}

function getComponent(compObj) {
    switch (compObj.type) {
        case "file":
            var fileUpload = '<span class="btn btn-small right"><input data-key-path="' + compObj.key + '" type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser"><span class="icon-cloud-upload"/>&#x00A0;Upload</span></span>'
            var compLabel = compObj['label'] ? compObj['label'] : '';
            return '<p style="overflow:auto;" onClick="$(this).parent().find(\'object\').toggleClass(\'hide\');"><span class="label" data-type="label"><b>' + compLabel + '</b></span>' + fileUpload + '</p><object onload="alert(\'hai\');" data="' + compObj.data + '" width="98%" height="90%"/>';
			break;
        case "fileUpload":
            var compLabel = compObj['label'] ? compObj['label'] : '';
            var region = compObj['regionIDVal'] ? compObj['regionIDVal'] : '';
            if(compLabel != 'layout'){
                compLabel = '<div class="'+compLabel+'" >'+ compLabel+ '</div>';
                var key = ' data-key-path="'+compObj.key+'"';
            }
            else{
                compLabel = '';
                var key = ' data-layout-sec="'+compObj.key+'"';
            }
            var fileUpload = '<span class="btn btn-small right"><input type="file" '+ key +' region="' + region + '" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser"><span class="icon-cloud-upload"/> Upload</span></span>'
            return compLabel+'<p>'+fileUpload +'</p><object class="uploadObject"  data="' + compObj.data + '"/>';
            break;
        /*case "issueUpload":
           var issueUpload = '<span class="btn btn-small left" data-message="{\'click\':{\'funcToCall\': \'generateIssueForm\',\'channel\':\'explorer\',\'topic\':\'customer\'}}"><span class="icon-cloud-upload"/>&#x00A0;Create New Issue</span></span>'    
           return '<p style="overflow:auto;" >' + issueUpload + '</p>';
                break;    */	
        case "pdf":
            var pdfGenerate = '<span class="btn btn-small right"><input data-key-path="' + compObj.key + '" type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser"><span class="icon-cloud-upload"/>&#x00A0;Generate PDF</span></span>'
            var compLabel = compObj['label'] ? compObj['label'] : '';
            return '<p style="overflow:auto;" onClick="$(this).parent().find(\'object\').toggleClass(\'hide\');"><span class="label" data-type="label"><b>' + compLabel + '</b></span>' + pdfGenerate + '</p><object onload="alert(\'hai\');" data="' + compObj.data + '" width="98%" height="90%"/>';
            break;
        case "mspdf":
        //get sitename from current data. If sitename  is kriya1/kriya2 display generate pdf and upload pdf.
        //if sitename  is non-kriya, display upload pdf/xml
            var currMsdata = $(targetNode).data('data');
            var myData = [].concat(compObj.data);
            var siteName ='';
            if((currMsdata._attributes)&&(currMsdata._attributes.siteName)){
                siteName = currMsdata._attributes.siteName;
            }
            var runOnFlag = false;
            if((currMsdata._attributes)&&(currMsdata._attributes['data-run-on'])){
                runOnFlag = currMsdata._attributes['data-run-on'];
            }
            var pdfGenerate;
            var printSrc;
            var onlineSrc;
			var preflightSrc;
            var proofType,printDownload,onlineDownload;
            myData.forEach(function (mdObj, mdIndex) {
                proofType = mdObj['proofType'] ? mdObj['proofType'] : 'print';
                var label;
                if(proofType == 'print' && mdObj['type'] != 'preflightPdf'){
                    printSrc = mdObj['path'] ? mdObj['path'] : '';
                    printDownload = mdObj['path'] ? ' target="_blank" href="'+ printSrc +'"' : ' ';
                }
                else if(proofType == 'online'){
                    onlineSrc = mdObj['path'] ? mdObj['path'] : '';
                    onlineDownload =  mdObj['path'] ? ' target="_blank" href="'+ onlineSrc +'"' : ' ';
                }
			    else if(mdObj['type'] == 'preflightPdf'){
                    preflightSrc =  mdObj['path'] ? ' target="_blank" href="'+ mdObj['path'] +'"' : ' ';
                }
            })
           var pdfGenerate = '<span class="btn btn-small right" style="margin-left: 5px" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'online\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}">Export PDF</span></span><span class="btn btn-small right" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'print\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}">Export Print PDF</span></span>' 
          // var pdfUpload = '<span class="btn btn-small right" style="margin-left: 5px;margin-right:5px"><input type="file" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser"><span class="icon-upload2" style="line-height:inherit"/></span></span>'
          var pdfUpload='';
          var pdfDownload ='<a class="dropdown-button btn btn-small" href="#" style="margin-left:5px;" data-activates="dropdown_download" stopPropagation="false">Download PDF</a><ul id="dropdown_download" class="dropdown-content download"  style="margin-left:5px;"><li data-proofType="online"><a "'+onlineDownload +'" >Online</a></li><li data-proofType="print"><a "'+ printDownload +'">Print</a></li><li data-preflight="true"><a " '+preflightSrc +'" >Preflight</a></li></ul>'
 
          if (/^(non\-kriya)$/gi.test(siteName)) {
               // pdfUpload = '<span class="btn btn-small right" style="margin-left: 5px"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser"><span class="icon-cloud-upload"/> Upload Files</span></span>'
			   pdfUpload = '<span class="dropdown-button btn btn-small right" style="margin-left: 5px" data-activates="dropdown_upload"><span class="icon-cloud-upload"/>Upload Files<ul id="dropdown_upload" class="dropdown-content upload"  style="margin-left:5px;"><li data-proofType="online"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" data-type="xml" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser">Upload XML</span></li><li data-proofType="online"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" data-proofType="online" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser">Upload Online PDF</span></li><li data-proofType="print"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" data-proofType="print" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser">Upload Print PDF</span></li></ul>'
               return '<p style="overflow:auto;">' + pdfUpload +'</p><object  data="' + printSrc + '" width="98%" height="90%"/>';
            }
            // if run-on disable export option
            else if(runOnFlag == 'true'){
                return '<p style="overflow:auto;">' +  pdfDownload + '<object  data="' + printSrc + '" width="98%" height="90%"/>'

            }
            else{
                pdfUpload = '<span class="dropdown-button btn btn-small right" style="margin-left: 5px" data-activates="dropdown_upload1"><span class="icon-upload2" style="line-height:inherit"/></span>'
                return '<p style="overflow:auto;">' + pdfUpload + pdfGenerate + pdfDownload + '<ul id="dropdown_upload1" class="dropdown-content upload"  style="margin-left:5px;"><li data-proofType="online"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" data-proofType="online" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser">Upload Online PDF</span></li><li data-proofType="print"><input type="file" data-manualUpload ="file._attributes.data-manualUpload" data-key-path="' + compObj.key + '" name="files[]" multiple="" style="display:none;" data-proofType="print" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/><span class="fileChooser">Upload Print PDF</span></li></ul><object  data="' + printSrc + '" width="98%" height="90%"/>'
            }
            break;    
        case "pspdf":
		var pdfGenerate = '<span class="btn btn-small right" style="margin-left: 10px" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'online\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}"><span class="icon-cloud-upload"/>&#x00A0;Export PDF</span></span><span class="btn btn-small right" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'print\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}"><span class="icon-cloud-upload"/>&#x00A0;Export Print PDF</span></span>'
            return '<p style="overflow:auto;">' + pdfGenerate + '</p><object onload="alert(\'hai\');" data="' + compObj.data + '" width="98%" height="90%"/>';
            break; 
        case "advertOrFillerPdf":
			var myData = [].concat(compObj.data);
			var printSrc;
            var onlineSrc;
			var preflightSrc;
            var proofType,printDownload,onlineDownload;
            //download pdf based on proof Type(print/online) or preflight
			myData.forEach(function (mdObj, mdIndex) {
                 if(mdObj["_attributes"] && mdObj["_attributes"]["proofType"]){
                    proofType = mdObj["_attributes"]["proofType"]
                } 
                var label;
                if(proofType == 'print' && mdObj["_attributes"]['type'] != 'preflightPdf'){
                  printSrc = mdObj["_attributes"]["path"] ? mdObj["_attributes"]["path"]  : '';
                  printDownload = mdObj["_attributes"]["path"] ? ' target="_blank" href="'+ printSrc +'"' : ' ';
                }
                else if(proofType == 'online'){
                    onlineSrc =mdObj["_attributes"]["path"]  ? mdObj["_attributes"]["path"]  : '';
                    onlineDownload = mdObj["_attributes"]["path"] ? ' target="_blank" href="'+ onlineSrc +'"' : ' ';
                }
                if(mdObj["_attributes"] && mdObj["_attributes"]['type'] == 'preflightPdf'){
                    preflightSrc =  mdObj["_attributes"]['path'] ? ' target="_blank" href="'+ mdObj["_attributes"]['path'] +'"' : ' ';
                  }
				  proofType = '';
              })
              var downloadclass = compObj["regionIDVal"] ? "dropdown_download_"+compObj["regionIDVal"] : "dropdown_download";
              var pdfDownload ='<a class="dropdown-button btn btn-small right" href="#" style="margin-left:5px;margin-right:5px" data-activates='+downloadclass+' stopPropagation="false">Download PDF</a><ul id='+downloadclass+' class="dropdown-content download"  style="margin-left:5px;"><li data-proofType="online"><a " '+onlineDownload +'" >Online</a></li><li data-proofType="print" ><a "'+ printDownload +'">Print</a></li><li data-preflight="true"><a " '+preflightSrc +'" >Preflight</a></li></ul>'
              var pdfGenerate = '<span class="btn btn-small right" style="margin-left: 5px" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'online\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}">Export PDF</span></span><span class="btn btn-small right" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'param\':{\'type\': \'print\',\'processType\': \'InDesignSetter\'},\'channel\':\'pdf\',\'topic\':\'export\'}}">Export Print PDF</span></span>' 
           
              // var pdfGenerate = '<span class="btn btn-small right" data-message="{\'click\':{\'funcToCall\': \'exportToPDF\',\'channel\':\'pdf\',\'topic\':\'export\'}}"><span class="icon-cloud-upload"/>&#x00A0;Generate PDF</span></span>'
            return pdfGenerate + pdfDownload
            break;         
        case "contenteditable":
            var myData = [].concat(compObj.data);
            var containerHTML = '';
            myData.forEach(function (mdObj, mdIndex) {
                var contentHTML = mdObj['_text'] ? mdObj['_text'].replace(/\[\[/g, '<').replace(/\]\]/g, '>'):'';
                containerHTML += '<p>' + contentHTML + '</p>';
            });
			return '<div class="coverText"><div class="col s4 m4 l4 label">' + compObj.label + '</div><div class="col s8 m8 l8 data contenteditable" contenteditable="true" data-key-path="' + compObj.key + '" data-label="' + compObj.label + '">' + containerHTML + '</div></div>';
            break;
        case "select":
            //var myData = [].concat(compObj.data);
            var compLabel = compObj['label'] ? compObj['label'] : '';
            var region = compObj['regionIDVal'] ? compObj['regionIDVal'] : '';
            var compObjValues = compObj['values'] ? compObj['values'] : '';
            compObjValues = compObjValues.split(',');
            var defaultValue = compObj.data ? compObj.data : compObj['defaultValue'];
            var compHTML ='';
            compHTML += '<label class="layoutLabel">' + compLabel + '</label>';
            compHTML += '<select class="floatPlacement" data-region-id="'+ region +'">';
            compObjValues.forEach(function(selectValue, sIndex){
                            var isSelected = '';
                            if (defaultValue == selectValue){
                                isSelected = ' selected="true"';
                            }
                            compHTML += '<option value="' + selectValue + '"' + isSelected +'>' + selectValue + '</option>'
                        })
                        compHTML += '</select>';
                        
            return compHTML 
            break;
        case "color":
            if (compObj.data == '') return '';
            var myData = [].concat(compObj.data);
            var colorHTML = '';
            myData.forEach(function (mdObj, mdIndex) {
                var colorChecked = parseInt(mdObj['color']) ? ' checked="checked"' : '';
				var src = mdObj['path'] ? mdObj['path'].replace(/\.(eps|tiff?)/gi, '.jpg') : '';
				colorHTML += '<p onClick="$(this).addClass(\'active\').parent().find(\'p:not(.active) img\').addClass(\'hide\');$(this).removeClass(\'active\').find(\'img\').toggleClass(\'hide\');" rid="' + mdObj['ref-id'] + '"><span class="fig"><span data-type="label"><i>' + mdObj['label'] + '</i></span><span class="right" data-type="color"><input type="checkbox" class="color filled-in" id="fb' + mdIndex + '"' + colorChecked + '/><label for="fb' + mdIndex + '">Color in print</label></span></span><img src="' + src + '" alt="' + mdObj['path'] + '"/></p>';
				})
            if (colorHTML) {
                //return '<div class="figsTab">' + colorHTML + '</div>';
				return colorHTML;
            }
            break;
		case "flag":
            // default flags are present in the config and the articles individual flags are present in the article
            // take a copy of the default flags and update its default-value with the articles' flag value
            // then generate the UI based on the updated default flags object
            var myData = [].concat(compObj.data);
            var flagsConfig = JSON.parse(JSON.stringify(configData.flags)).flag;
            if (flagsConfig.constructor.toString().indexOf("Array") < 0){
                flagsConfig = [flagsConfig];
            }
            var dataCID = $('#msDataDiv').attr('data-c-rid');
            // get data from the corresponding article
            var currData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
            var contentType = currData._attributes['type'];
            var fcObj = {}, fcIndex = 0;
            var flagsHTML = '';
            flagsConfig.forEach(function(fcObj, fcIndex){
                if(fcObj._attributes['content-type'] == contentType){
                myData.forEach(function(mdObj, mdIndex){
                    if (mdObj.type == fcObj._attributes.type){
                        fcObj._attributes['default-value'] = mdObj.value;
                    }
                });
                switch (fcObj._attributes['component-type']) {
                    case "checkbox":
                        var flagChecked = parseInt(fcObj._attributes['default-value']) ? ' checked="checked"' : '';
                        flagsHTML += '<div class="stripes"><input type="checkbox" data-flag-type="' + fcObj._attributes.type + '" class="flag filled-in" id="fg' + fcIndex + '"' + flagChecked + ' /><label for="fg' + fcIndex + '"><i>' + fcObj._attributes.label + '</i></label></div>';
                            break;
                    case "select":
                        var flagValues = fcObj._attributes.values.split(',');
                        var defaultValue = fcObj._attributes['default-value'];
                        flagsHTML += '<div class="stripes" style="display: table; width:100%;"><div class="input-field col s12" style="display: table-row;">';
                        flagsHTML += '<select data-flag-type="' + fcObj._attributes.type + '" class="flag">';
                        flagValues.forEach(function(selectValue, sIndex){
                            var isSelected = '';
                            if (defaultValue == selectValue){
                                isSelected = ' selected="true"';
                            }
                            flagsHTML += '<option value="' + selectValue + '"' + isSelected +'>' + selectValue + '</option>'
                        })
                        flagsHTML += '</select>';
                        flagsHTML += '<label>' + fcObj._attributes.label + '</label>';
                        flagsHTML += '</div></div>';
                        break;
                    default:
                }
            }
            })
            if (flagsHTML) {
               return  flagsHTML;
            }
            break;
			case "region":
			// default region and layout are present in the config. Individual advert/filler layout, region are specified in issueXML 
            // generate the UI based on the region,layout from the config and from the Individual advert/filler info specied in issueXML
            var myData = [].concat(compObj.data);
            var regionsConfig = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionsConfig.constructor.toString().indexOf("Array") < 0){
                regionsConfig = [regionsConfig];
            }
			var rcObj = {}, rcIndex = 0;
			var regionHTML = '';
			var layouts = '';
            var regionFlagVal ='false';
            regionsConfig.forEach(function(rcObj, rcIndex){
                regionFlagVal = 'false';
                var files ='';
                var curLayoutVal = '';
                var lcObj = {}, lcIndex = 0;
                var ccObj = {}, ccIndex = 0;
                var layoutsConfig = rcObj.layout;
                if (layoutsConfig.constructor.toString().indexOf("Array") < 0){
                    layoutsConfig = [layoutsConfig];
                }
                //componenet to add float-placement/advertname
                if(rcObj.component){
                    var componentConfig = rcObj.component;
                    if (componentConfig.constructor.toString().indexOf("Array") < 0){
                        componentConfig = [componentConfig];
                    }
            }
                if (rcObj._attributes['content-type'] == $(targetNode).attr('data-type')){
                    myData.forEach(function(mdObj, mdIndex){
                        if(mdObj !=''){
                        if (mdObj._attributes.type == rcObj._attributes.type){ 
                            rcObj._attributes['default-value'] = mdObj._attributes.type;
                            curLayoutVal = mdObj._attributes.layout; // if region info is available in issueXML, get layout value
                            regionFlagVal ='true';
                            var regionId = rcObj._attributes.label;
                            regionHTML += '<div id = "'+rcObj._attributes.label+'" data-region = "'+rcObj._attributes.label+'"><div class="stripes" ><p><label onclick="$(\'#regionTab\').find(\'layoutBlock[data-region-id='+ rcObj._attributes.label +']\').addClass(\'hide\');$(\'#regionTab\').find(\'.layoutblock[data-region-id='+ rcObj._attributes.label +']\').toggleClass(\'hide\');" for="rg' + rcIndex + '"><i>' + rcObj._attributes.label + '</i></label>';
                            var pdfComponent = {
                                type: 'advertOrFillerPdf',
                               // data: mdObj._attributes['path'],
                               data: mdObj['file'],
                                regionIDVal: mdObj._attributes['type'],
                                key: mdObj._attributes['path']
                            }    
                            regionHTML += getComponent(pdfComponent);
                            //add float-placement/advert name
                            regionHTML += '</p><p>';
                            if(componentConfig){
                                componentConfig.forEach(function(ccbj, ccIndex){
                                var key =ccbj._attributes['string']
                                var afcomponent = {
                                    label: ccbj._attributes['label'],
                                    data: mdObj._attributes[''+ key +''],
                                    type: ccbj._attributes['component-type'],
                                    values: ccbj._attributes['values'],
                                    regionIDVal: mdObj._attributes['type'],
                                    defaultValue: ccbj._attributes['default-value'],
                                    key: key
                                } 
                                regionHTML += getComponent(afcomponent);
                                })  
                            }
                            regionHTML += '<span class="layoutSelect" data-type="region"><label class="layoutLabel">Layout</label><select class="layoutOption" data-type="' + mdObj._attributes['layout'] +'" data-region-id = "'+rcObj._attributes.label+'">';
                            layoutsConfig.forEach(function(lcbj, lcIndex){ //get the layout values from config based on content type
                                if (lcbj._attributes['content-type'] == $(targetNode).attr('data-type')){ // match the layout type with target node type 1.e, advert/filler
                                    layouts = lcbj._attributes.values; // if region info is available in config, get layout value
                                    layouts = layouts.split(',');
                                    if(curLayoutVal == '') { //if current layout value is not available in xml, then set the default layout value as the current value
                                        curLayoutVal = lcbj._attributes['default-value'];
                                    }
                                    var lvalObj = {}, lvalIndex = 0;
                                    layouts.forEach(function(lvalObj, lvalIndex){
                                        var layoutValue = lvalObj.toLowerCase();
                                            layoutValue = layoutValue.replace(/\s/,'');
                                        var isSelected = '';
                                        if (layoutValue == curLayoutVal){
                                            isSelected = ' selected="true"';
                                        }
                                        var layoutId = layoutValue.toLowerCase();
                                        layoutId = layoutId.replace(/\s+/,'');
                                        regionHTML += '<option value="' + layoutId + '"' + isSelected +'>' + lvalObj + '</option>'
                                    })
                                }
                            })
                            files = mdObj.file;
                            curLayoutVal = curLayoutVal.toLowerCase();
                            curLayoutVal = curLayoutVal.replace(/\s/,'');
                            regionHTML += '</select></span>' 
                            regionHTML += '</p></div>' 
                            regionHTML += '<div class="layoutBlock"  data-region-id="'+ rcObj._attributes.label +'" data-layout="' + mdObj._attributes.layout + '">';
                            var layoutHTML = $('#'+curLayoutVal+'').html(); // get the layout html based on the layout specified in issueXML
                            regionHTML += populateAdvert(rcObj._attributes.type,$('#'+curLayoutVal+'').find('.layoutsection'),layoutHTML,files); // populate img data
                            regionHTML += '</div></div>';
                            }
                        }
                        });
                        // if region info is not available in xml
                        if(regionFlagVal == 'false'){
                            var regionId = rcObj._attributes.label;
                            var pdfComponent = {
                                type: 'advertOrFillerPdf',
                                data: '',
                                regionIDVal: rcObj._attributes['type'],
                                key: ''
                        }    
                        regionHTML += '<div id = "'+rcObj._attributes.label+'" data-region = "'+rcObj._attributes.label+'"><div class="stripes" ><p><label onclick="$(\'#regionTab\').find(\'layoutBlock[data-region-id='+ rcObj._attributes.label +']\').addClass(\'hide\');$(\'#regionTab\').find(\'.layoutblock[data-region-id='+ rcObj._attributes.label +']\').toggleClass(\'hide\');" for="rg' + rcIndex + '"><i>' + rcObj._attributes.label + '</i></label>';
                        regionHTML += getComponent(pdfComponent);
                        regionHTML += '</p><p>';
                         //add float-placement/advert name
                        if(componentConfig){
                            componentConfig.forEach(function(ccbj, ccIndex){
                                var key =ccbj._attributes['string']
                                var afcomponent = {
                                    label: ccbj._attributes['label'],
                                    data: rcObj._attributes[''+ key +''],
                                    type: ccbj._attributes['component-type'],
                                    values: ccbj._attributes['values'],
                                    regionIDVal: rcObj._attributes['type'],
                                    defaultValue: ccbj._attributes['default-value'],
                                    key: key
                                } 
                            regionHTML += getComponent(afcomponent);
                            })  
                        }
                        regionHTML +='<span class="layoutSelect" data-type="region"><label class="layoutLabel">Layout</label><select class="layoutOption" data-type="" data-region-id = "'+rcObj._attributes.label+'">';
                        layoutsConfig.forEach(function(lcbj, lcIndex){ //get the layout values from config based on content type
                            if (lcbj._attributes['content-type'] == $(targetNode).attr('data-type')){ // match the layout type with target node type 1.e, advert/filler
                                layouts = lcbj._attributes.values; // if region info is available in config, get layout value
                                layouts = layouts.split(',');
                                if(curLayoutVal == '') { //if current layout value is not available in xml, then set the default layout value as the current value
                                    curLayoutVal = lcbj._attributes['default-value'];
                                    }
                                var lvalObj = {}, lvalIndex = 0;
                                layouts.forEach(function(lvalObj, lvalIndex){
                                var layoutValue = lvalObj.toLowerCase();
                                    layoutValue = layoutValue.replace(/\s/,'');
                                var isSelected = '';
                                if (layoutValue == curLayoutVal){
                                    isSelected = ' selected="true"';
                                    }
                                var layoutId = layoutValue.toLowerCase();
                                layoutId = layoutId.replace(/\s+/,'');
                                regionHTML += '<option value="' + layoutId + '"' + isSelected +'>' + lvalObj + '</option>'
                                })
                            }
                        })
                        var files = ({"_attributes":{"type": "file","data-region":rcObj._attributes.type, "data-layout":"a" ,"path":""}})
                         var layoutLabel = curLayoutVal;
                        curLayoutVal = curLayoutVal.toLowerCase();
                        curLayoutVal = curLayoutVal.replace(/\s/,'');
                        regionHTML += '</select></span></p></div>';
                        regionHTML += '<div class="layoutBlock"  data-region-id="'+ rcObj._attributes.label +'" data-layout="' + layoutLabel + '">';
                        var layoutHTML = $('#'+curLayoutVal+'').html(); // get the layout html based on the layout specified in issueXML
                        regionHTML += populateAdvert(rcObj._attributes.type,$('#'+curLayoutVal+'').find('.layoutsection'),layoutHTML,files); // populate img data
                        regionHTML += '</div></div>';
                    }
                }
                
            })
          //  regionHTML += '</div>';
            if (regionHTML) {
                return regionHTML;
            }
            break;
        case "checkbox":
        var value =  compObj.data ? compObj.data : compObj['defaultValue'];
            
        var flagChecked = '';
        if(value == 1){
                flagChecked =' checked="checked"';}
        return '<input type="checkbox" data-flag-type="' + compObj.type + '" data-key-path="' + compObj.key +'" class="color filled-in" data-region-id="' + compObj['regionIDVal'] +'" id="fg' + compObj['regionIDVal'] + '"' + flagChecked + ' /><label for="fg' + compObj['regionIDVal'] + '"><i>' + compObj.label + '</i></label>';
            break;        
        case "afeditable": 
        var compData = compObj.data ? compObj.data : compObj['defaultValue'];
            return '<label class="layoutLabel">' + compObj.label + '</label><span contenteditable="true" class="af-data-editable" data-region-id="' + compObj['regionIDVal'] + '" data-key-path="' + compObj.key + '" data-label="' + compObj.label + '" contenteditable="true">' + compData + '</span>';
            break;    
        case "editable":
        /*//check if previous issue available in db (i.e, prevIssue attribute), if available make non-editiable
        if(compObj.data['_attributes'] && compObj.data['_attributes']["prevIssue"] && compObj.data['_attributes']["prevIssue"].toLowerCase() == 'true'){
            var key = compObj.key.concat('._text')
            var data = compObj.data['_text'] ? compObj.data['_text'] : ''
            return '<div class="row stripes"><div class="col s4 m4 l4 label">' + compObj.label + '</div><div class="col s8 m8 l8 data" data-key-path="' + key + '" data-label="' + compObj.label + '">' + compObj.data['_text'] + '</div></div>';
            
        }
        else{
            var data = compObj.data['_text'] ? compObj.data['_text'] : compObj.data
            return '<div class="row stripes"><div class="col s4 m4 l4 label">' + compObj.label + '</div><div class="col s8 m8 l8 data editable" data-key-path="' + compObj.key + '" data-label="' + compObj.label + '" contenteditable="true">' + data + '</div></div>';
        }*/
		var data = compObj.data['_text'] ? compObj.data['_text'] : compObj.data
            if(JSON.stringify(data) == '{}') data=''
            return '<div class="row stripes"><div class="col s4 m4 l4 label">' + compObj.label + '</div><div class="col s8 m8 l8 data editable" data-key-path="' + compObj.key + '" data-label="' + compObj.label + '" contenteditable="true">' + data + '</div></div>';
        break;
        default:
            return '<div class="row stripes"><div class="col s4 m4 l4 label">' + compObj.label + '</div><div class="col s8 m8 l8 data">' + compObj.data + '</div></div>';
    }
}
function populateAdvert(regionId,layoutsections,layoutHTML,files) {
//loop through each layout section , get the file path from issueXML and update the path in the layout sec
    var layoutLen = layoutsections.length;
    var regionHTML = $('<div>' + layoutHTML + '</div>');
    if (files.constructor.toString().indexOf("Array") < 0){
					files = [files];
    }
    var layoutSecObj = {}, lsIndex = 0;
	var fileObj = {}, acIndex = 0;
    var advertImg ='';
    var layoutFlag =false;
    for(var i=0;i<layoutLen;i++){
        layoutFlag = false;
		files.forEach(function(fileObj, acIndex){
            if( layoutsections[i].attributes['layout-sec'].value == fileObj._attributes['data-layout']){
                layoutFlag = true;
                var layoutsec = fileObj._attributes['data-layout'];
                var compObj = {
                    type: 'fileUpload',
                    data: fileObj._attributes['path'],
                    label: 'layout',
                    regionIDVal: fileObj._attributes['data-region'],
                    key: fileObj._attributes['data-layout']
                }
                var fileUploads = getComponent(compObj);
                var advertImg = '<img src="' + fileObj._attributes['path'] + '" alt="fig' + layoutsec + '"/>';
                regionHTML.find('[layout-sec="'+ layoutsec +'"]').html(regionHTML.find('[layout-sec="'+ layoutsec +'"]').html() + fileUploads)
            }
        });
        //if file path is not available in xml,
        if(layoutFlag == false){
            var layoutsec = layoutsections[i].attributes['layout-sec'].value;
            var compObj = {
                type: 'fileUpload',
                data: '',
                label: 'layout',
                regionIDVal: regionId,
                key: layoutsections[i].attributes['layout-sec'].value
                }
                var fileUploads = getComponent(compObj);
                regionHTML.find('[layout-sec="'+ layoutsec +'"]').html(regionHTML.find('[layout-sec="'+ layoutsec +'"]').html() + fileUploads)
            }
        }
        if (regionHTML) {
            return regionHTML.html();
        }
    }
//pull advert and filler layout from the config and append it at the end of #tocContainer
//config of advert/filler layout is based on the customer    
function populateLayouts() {
    var cName = $('.list.folder.active').attr('data-customer');
    var pName = $('.list.folder.active').attr('data-project');
    var fileName = $('.list.folder.active:visible').attr('data-file');
    var obj = { 'url': '/api/getlayouthtml?client=' + cName + '&jrnlName=' + pName + '&fileName=' + fileName + '&process=getLayouts','objectName': 'list' };
        getDataFromURL(obj)
            .then(function (templateData) {
				if(templateData !=''){  
					//var advertLayouts =templateData;
					//var container = $('#tocContainer .bodyDiv');
					$('#dataContainer').append(templateData.data)
				}
            })
            .catch(function (err) {
                showMessage({ 'text': 'Something went wrong when fetching Advert/Filler layouts. Please try again or contact support', 'type': 'error' });
            })
               
    }	
function populateTOC() {
    var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
    var prevSection = '';
    var container = $('#tocContainer .bodyDiv');
    container.html('<div class="row sectionHeader"><div class="col s6 m6 l6">Section Name</div><div class="col s6 m6 l6">Page Range</div></div>');
    var colStart = '<div class="col s6 m6 l6">';
    var closeDiv = '</div>';
    var containerHTML = '';
    var sectionIndex = 1;
    var tempID = -1;
    contentHash = {}
    content.forEach(function (entry) {
        var id = entry._attributes.id ? entry._attributes.id : '';
        var type = entry._attributes.type;
        var child =false;
        contentHash[++tempID] = content[tempID];
        if (/^(cover|editorial\-board|table\-of\-contents)$/gi.test(entry._attributes.type)) {
            addSection(container, entry, 'noChild', sectionIndex++, tempID);
        }
        else {
            if (prevSection != entry._attributes.section) {
                addSection(container, entry, 'hasChild', sectionIndex++);
            }
            // add child true when prevSection is equal to entry._attributes.section
           else {
                child = true;
            }
            prevSection = entry._attributes.section;
            var dataType = ' data-type="manuscript"';
            if (entry._attributes && entry._attributes.type) {
                dataType = ' data-type="' + entry._attributes.type + '"';
            }
            var dataGroupType = 'data-grouptype=""';
            if (entry._attributes && entry._attributes.grouptype) {
                dataGroupType = ' data-grouptype="' + entry._attributes.grouptype + '"';
            }
            var msConObj = {
                "dataType": dataType,
                "tempID": tempID,
                "sectionIndex": sectionIndex,
                "dataGroupType":dataGroupType,
                "child":child
            }
            container.append(getMSContainer(entry, msConObj));
            if (tempID == 0 || tempID) {
                $('[data-c-id="' + tempID + '"]').data('data', entry);
            }
        }
    });
}
function getMSContainer(entry, msConObj){
    var className = ' class="row sectionDataChild hide"';
    var modifiedAttr = (entry._attributes['data-modified'])?' data-modified="'+entry._attributes['data-modified']+'" ':'';
    var runOnAttr = (entry._attributes['data-run-on'])?' data-run-on="'+entry._attributes['data-run-on']+'" ':'';
    var runOnWithAttr = entry._attributes['data-runon-with'] ?' data-runon-with="'+entry._attributes['data-runon-with']+'" ':'';
    var tempIDVal = entry._attributes.id ? entry._attributes.id : '';
    var sectionChild = msConObj.child ? msConObj.child : false;
    var siteName = entry._attributes.siteName ? entry._attributes.siteName : '';
    containerHTML = '<div' + msConObj.dataType + msConObj.dataGroupType + ' data-c-id="' + msConObj.tempID + '"' + className + ' data-sec="sec' + (msConObj.sectionIndex - 1) + '" ' + modifiedAttr + runOnAttr + runOnWithAttr + ' data-section="'+ entry._attributes['section'] +'" data-channel="toc" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'panel\'}">';
   // containerHTML += '<div class="col s3 m3 l3 msID">' + (entry._attributes.id ? entry._attributes.id : '') + '</div>';
    containerHTML += '<div class="col s3 m3 l3 msID">' + tempIDVal + '</div>';
    var titleText = 'Title Text';
    if (entry.title && entry.title._text) {
        titleText = entry.title._text;
        }
    else if (/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)) {
        titleText = entry._attributes.type.toUpperCase();
        }
    containerHTML += '<div class="col s7 m7 l7 titleText" title="' + titleText + '">' + titleText + '</div>';
    var incorrectPageClass = '';
	var pageModifiedClass = '';
    var hidePageRange = '';
    var title ='';
    if (entry._attributes.incorrectPage && (entry._attributes.incorrectPage.toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() == 'manuscript')){
        incorrectPageClass = ' incorrectPage';
		title = ' title="incorrectPage"';
    }
    if (entry._attributes['data-unpaginated'] && (entry._attributes['data-unpaginated'].toLowerCase() == 'true')){
        hidePageRange = ' hide';
    }
    if (entry._attributes['data-run-on'] && (entry._attributes['data-run-on'].toLowerCase() == 'true')){
        hidePageRange = ' hide';
    }
	else if (entry._attributes['data-modified'] && (entry._attributes['data-modified'].toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() != 'advert_unpaginated') && (entry._attributes.type.toLowerCase() != 'blank')){
        pageModifiedClass = ' pageModified';
        title = ' title="Page is Modified"';
    }
    if(hidePageRange != ' hide'){
        containerHTML += '<div class="col s2 m2 l2 pageRange' + incorrectPageClass + pageModifiedClass + '" ' + title +'>' + '<span class="rangeStart'+ hidePageRange + ' ">' + entry._attributes.fpage + '</span>';
            if (entry._attributes.fpage != entry._attributes.lpage) {
                containerHTML += '&#x2013;<span class="rangeEnd '+ hidePageRange +'">' + entry._attributes.lpage + '</span>';
           //if rangeEnd is not available append it after rangeStart
                if ($('#tocContainer .bodyDiv .sectionData:last .rangeEnd').length == 0) {
                    $('#tocContainer .bodyDiv .sectionData:last .rangeStart').append('&#x2013;<span class="rangeEnd'+ hidePageRange + '">' + entry._attributes.lpage + '</span>')
                }
                 else{
               // change rangeEnd
                    $('#tocContainer .bodyDiv .sectionData:last .rangeEnd').text(entry._attributes.lpage);
                }
            }
        // change RangeENd of sectionData whose sectionChild has fpage and lpage equal 
            else if(entry._attributes.fpage == entry._attributes.lpage && sectionChild == true){
                if ($('#tocContainer .bodyDiv .sectionData:last .rangeEnd').length == 0) {
                    $('#tocContainer .bodyDiv .sectionData:last .rangeStart').append('&#x2013;<span class="rangeEnd">' + entry._attributes.lpage + '</span>')
                }
                else{
                    $('#tocContainer .bodyDiv .sectionData:last .rangeEnd').text(entry._attributes.lpage);
                }
            }
    }
        //set flag in page range of sectionhead when any of the child has incorrect page sequence / when page range is modified
    if (entry._attributes.incorrectPage && (entry._attributes.incorrectPage.toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() == 'manuscript')){
        $('#tocContainer .bodyDiv .sectionData:last .sectionRange').addClass('incorrectPage');
        $('#tocContainer .bodyDiv .sectionData:last .sectionRange').attr('title','incorrectPage');
    }
    else if (entry._attributes['data-modified'] && (entry._attributes['data-modified'].toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() != 'advert_unpaginated') && (entry._attributes.type.toLowerCase() != 'blank')){
        $('#tocContainer .bodyDiv .sectionData:last .sectionRange').addClass('pageModified');
        $('#tocContainer .bodyDiv .sectionData:last .sectionRange').attr('title','Page is Modified');
    }
    containerHTML += '<span class="site">' +siteName +'</span>';
    containerHTML += '<span class="controls"><span class="controlIcons icon-add"/><span class="controlIcons icon-arrow_downward"/><span class="controlIcons icon-arrow_upward"/><span class="controlIcons icon-close"/></span>';
    containerHTML += '</div>';
    containerHTML += '</div>';
    return containerHTML;
}			
function addSection(container, entry, className, secIndex, cid) {
    var colStart = '<div class="col s6 m6 l6">';
    var closeDiv = '</div>';
    var containerHTML = '';
    var cidData = typeof (cid) != 'undefined' ? ' data-c-id="' + cid + '"' : '';
    // if it does not have a child, then clicking the section should bring up the details on the right else open the child nodes
    var funcName = 'section';
    var openIcon = '<span class="openclose icon-arrow_drop_up">&thinsp;</span>';
    var dataType = ' data-type="section"';
    if (className == 'noChild') {
        funcName = 'panel';
        openIcon = '';
        if (entry._attributes && entry._attributes.type) {
            dataType = ' data-type="' + entry._attributes.type + '"';
        }
    }
    var dataGroupType = '';
    if (entry._attributes && entry._attributes.grouptype) {
        dataGroupType = ' data-grouptype="' + entry._attributes.grouptype + '"';
    }
	var sectionHead =  '<span class="sectionHead">'+entry._attributes.section+'<span class="controls"><span class="controlIcons icon-edit"/></span></span>' ;
    var funcName = className == 'noChild' ? 'panel' : 'section';
    containerHTML = '<div' + dataType + cidData +  dataGroupType + ' id="sec' + secIndex + '" class="sectionData ' + className + ' ' + entry._attributes.type + '" data-section="' + entry._attributes.section + '" data-channel="toc" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'' + funcName + '\'}">';
    containerHTML += '<div class="row">';
    containerHTML += colStart + openIcon + sectionHead + closeDiv;
    containerHTML += '<div class="col s6 m6 l6 sectionRange">' + '<span class="rangeStart">' + entry._attributes.fpage + '</span>'
    if (entry._attributes.fpage != entry._attributes.lpage) {
        containerHTML += '&#x2013;<span class="rangeEnd">' + entry._attributes.lpage + '</span>';
    }
    containerHTML += closeDiv;
    //containerHTML += '<div class="col s2 m2 l2"><span class="right icons icon-bin hide"/><span class="right icons icon-add"/></div>';
    containerHTML += closeDiv;
    containerHTML += closeDiv;
    container.append(containerHTML);
    if (cid == 0 || cid) {
        $('[data-c-id="' + cid + '"]').data('data', entry);
    }
}

function populateHistory(chData) {
	var changeHistoryData = [];
    if (typeof (chData) != 'undefined') {
        changeHistoryData = [{
            "user": { "_text": chData.user},
            "change": { "_text": chData.change},
            "time": { "_text": chData.time},
            "file": { "_text": chData.file},
            "fileType": { "_text": chData.fileType}
        }]
    }
    else {
		$('#changeHistory').html('')
        changeHistoryData = $('#tocContainer').data('binder')['container-xml']['change-history'];
    }
    var chHTML = '';
        changeHistoryData.forEach(function (chData) {
            var chTime = chData.time._text
            chTime = chTime.replace(/^[a-z]+ (.*:[0-9]{2}) .*$/gi, "$1");
        chHTML += '<p class="chData">';
        chHTML += '<span class="user">' + chData.user._text + '</span> <span class="change">' + chData.change._text + '</span> on <span class="time" title="' + chData.time._text + '">' + chTime + '</span>';
        if (chData.file && chData.file._text){
            chHTML += 'for <span class="file">' + chData.file._text + '</span>&#x00A0;(<span class="type">' + chData.fileType._text + '</span>)';
    }
        chHTML += '</p>';
    });
    $('#changeHistory').append(chHTML);
}

function updateChangeHistory(histObj) {
    var userName = $('.userinfo:first').attr('data-name');
    var currTime = new Date().toString();
    var changeText = histObj.change;
    var fileID = histObj.id;
    var fileType = histObj.type;
    var changeObj = { 'user': userName, 'time': currTime, 'change': changeText, 'file': fileID, 'fileType': fileType, 'id-type': histObj['id-type'] };
    $('#tocContainer').data('binder')['container-xml']['change-history'].push(changeObj);
    populateHistory(changeObj);
}

function saveData() {
    var customer = $('#listIssue').attr('data-customer');
    var project = $('#listIssue').attr('data-project');
    var fileName = $('.list.folder.active:visible').attr('data-file');
    $.ajax({
        type: 'POST',
        url: '/api/postissuedata?client=' + customer + '&jrnlName=' + project + '&fileName=' + fileName,
        data: JSON.stringify($('#tocContainer').data('binder')),
        success: function (data) {
            showMessage({ 'text': 'All changes saved', 'type': 'success' });
        },
        error: function (data) {
            showMessage({ 'text': 'Saving failed. Please try again or contact support', 'type': 'error' });
        },
        contentType: "application/json",
        dataType: 'json'
    });
}
function saveXML(type,fileContents) {
    var customer = $('#listIssue').attr('data-customer');
    var project = $('#listIssue').attr('data-project');
    if(type == 'manuscript'){
        $.ajax({
            type: 'POST',
            url: '/api/generateissuedata',
            data: {'client': customer,'jrnlName': project, 'issueXML': fileContents,'process':'saveArticleXML'},
            success: function (response) {
                showMessage({ 'text': 'XML saved', 'type': 'success' });
            },
            error: function (response) {
                showMessage({ 'text': 'Error. Please try again or contact support', 'type': 'error' });
            }
         });
        }
   else if(type == 'issueXML'){
        $('.la-container').fadeIn();
        $.ajax({
            type: 'POST',
            url: '/api/generateissuedata',
            data: {'client': customer,'jrnlName': project, 'issueXML': fileContents,'process':'generateContainerXml'},
            success: function (response) {
                showMessage({ 'text': 'Issue created', 'type': 'success' });
                $('.la-container').fadeOut();
            },
            error: function (response) {
                if(response.responseJSON && response.responseJSON.status &&  response.responseJSON.status.message){
                    showMessage({ 'text': response.responseJSON.status.message, 'type': 'error' });
                }
                else{
                showMessage({ 'text': 'Error. Please try again or contact support', 'type': 'error' });
                }
                $('.la-container').fadeOut();
            }
        });
    }
}
//refresh option to get latest articles xml
function getUpdatedAricle(fileName) {
    var customer = $('#listIssue').attr('data-customer');
    var project = $('#listIssue').attr('data-project');
        return new Promise(function (resolve, reject) {
            $('.la-container').fadeIn();
            // hide the right panel and enable it after populating data
            $('#detailsContainer').find(">div").addClass('hide');
            var obj = { 'url': '/api/reloadissuedata?client=' + customer + '&jrnlName=' + project + '&fileName=' + fileName + '&process=getUpdatedArticlesXML', 'objectName': 'list' };
            getDataFromURL(obj)
                .then(function (issueData) {
                    configData = JSON.parse(JSON.stringify(issueData.data.issue.config));
                    console.log(configData);
                    delete(issueData.data.issue.config);
                    issueData = issueData.data.issue;
                    if (typeof(issueData['container-xml']['change-history']) == 'undefined'){
                        issueData['container-xml']['change-history'] = [];
                    }
                    else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
                        issueData['container-xml']['change-history'] = [];
                    }
                    $('#tocContainer').data('binder', issueData);
                    $('#tocContainer').data('config', configData);
                    $('.icons').removeClass('hide');
                    $('.mergePdfIcon').removeClass('hide');
                    $('.la-container').fadeOut();
                    resolve(true);
                })
                .catch(function (err) {
                    $('.la-container').fadeOut();
                    showMessage({ 'text': 'Something went wrong when fetching issue data. Please try again or contact support', 'type': 'error' });
                    reject(err)
                })
        })
    }

// init change events for data save
function initEvents() {
    $('#detailsContainer')
        // when the flags are changed
        .on('change', 'select.flag, .flag', function(e){
            var flagType = '', flagValue = 0;
            var dataCID = $('#msDataDiv').attr('data-c-rid');
            var currNodeIndex = parseInt(dataCID);
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
                flagType = $(this).attr('data-flag-type');
                flagValue = this.value;
            }
            else if (e.currentTarget.nodeName.toLowerCase() === 'input'){
                flagType = $(this).attr('data-flag-type');
                if ($(this).is(':checked')) {
                    flagValue = 1;
                }
            }
            // flagType is not set then do nothing, this check is used to skip the extra event on select due to materialisecss
            if (flagType == '') return true;
            console.log('flag: ', flagType, flagValue);
            // get data from the corresponding article
            var data = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
            var histObj = JSON.parse(JSON.stringify(data._attributes));
            histObj.change = 'changed flag <u>' + flagType + '</u> to be ' + flagValue.toString();
            // the current article does not have a flag yet. directly set one
            if (typeof(data.flag) === 'undefined'){
                data.flag = [{"_attributes":{"type": flagType, "value": flagValue}}];
            }
            else {
                // the current article has one flag set, convert it into array
                if (typeof(data.flag[0]) === 'undefined'){
                    data.flag = [{"_attributes":{"type": data.flag._attributes.type, "value": data.flag._attributes.value}}];
                }
                // loop through the available flags for current article.
                // if the flag type matches the current flagType then update its value, else add one
                var flagsArray = data.flag;
                var flagSet = false;
                var fpageVal = histObj.fpage;
                flagsArray.forEach(function(currFlagObj){
                    if (currFlagObj._attributes.type == flagType){
                        currFlagObj._attributes.value = flagValue;
                        flagSet = true;
                    }
                });
                
                if (!flagSet){
                    data.flag.push({"_attributes":{"type": flagType, "value": flagValue}});
                }
            }
            if(flagType == 'run-on'){
            //if run-on flag is set, hide pagerange, set current fpage as start page for next node
                if(flagValue == 1){
                    data._attributes["data-run-on"]='true';
                    saveData();
                    populateTOC()
                    var currNode = $('[data-c-id="' + currNodeIndex + '"]');
                    var currNodeSection = currNode.attr('data-section');
                    var sectionFirstNode = $('#tocBodyDiv div[data-type="manuscript"][data-section="'+ currNodeSection +'"]:first');
                    var sectionFirstNodeIndex = parseInt(sectionFirstNode.attr('data-c-id'));
                    var sectionFirstData = $(sectionFirstNode).data('data')._attributes;
                    var sectionFirstFpageVal = parseInt(sectionFirstData.fpage)
                    updatePageNumber(sectionFirstNodeIndex,sectionFirstFpageVal,data._attributes["grouptype"]);
                }
                else{
                    data._attributes["data-run-on"]='false';
                    saveData();
                    populateTOC()
                    var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                    var prevNodeData;
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                        fpageVal = parseInt(prevNodeData.lpage) + 1;
                    }
                    updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
                }
               
                populateTOC()
            }
            else if(flagType == 'unpaginated'){
                if(flagValue == 1){
                    data._attributes["data-unpaginated"]='true';
                   // data._attributes["type"]= data._attributes["type"].replace(/\_unpaginated/g,'');
                    data._attributes["type"]= data._attributes["type"].concat('_unpaginated');
                    if(/^(body|epage)$/gi.test(data._attributes["grouptype"])){
                        saveData();
                        populateTOC();
                        var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                        var prevNodeData,nextNodeData;
                        if(prevNode){
                            prevNodeData = $(prevNode).data('data')._attributes;
                            fpageVal = parseInt(prevNodeData.lpage) + 1;
                        }
                        else{
                            fpageVal = parseInt(histObj.fpage)
                        }
                         //fpageVal = parseInt(histObj.fpage) - 1;// 
                        updatePageNumber(currNodeIndex + 1 ,parseInt(fpageVal), data._attributes["grouptype"]);
                        
                    }    
                }
                else{
                    data._attributes["data-unpaginated"]='false';
                    data._attributes["type"]= data._attributes["type"].replace(/\_unpaginated/g,'');
                    //if(data._attributes["grouptype"] == 'body'){
                    if(/^(body|e\-page)$/gi.test(data._attributes["grouptype"])){
                        saveData();
                        populateTOC()
                        var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                        var prevNodeData,nextNodeData;
                        if(prevNode){
                            prevNodeData = $(prevNode).data('data')._attributes;
                            fpageVal = parseInt(prevNodeData.lpage) + 1;
                        }
                        else{
                            var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
                                if(nextNode){
                                    nextNodeData = $(nextNode).data('data')._attributes;
                                    fpageVal = parseInt(nextNodeData.fpage);    
                                }
                        }
                        //fpageVal = parseInt(histObj.fpage) + 1;     // 
                        updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
                        
                    }    
                }
               // updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
            }
			//change grouptype as 'epage' when epage flag is set
			else if(flagType == 'e-page'){
                if(flagValue == 1){
                    //  if previous node as epage, take prev node endPage as startpage and add 'e' as prefix
                // if previous node is not e-page, then take e-start page as fpage, fpage and lpage will be same  
                    var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                    var prevNodeData = '';
                    var endPageVal;
                        fpageVal = histObj.fpage;
                    var pageCount = data._attributes["pageExtent"] ? data._attributes["pageExtent"] : parseInt(0)
                        pageCount = parseInt(pageCount);
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                    }
                  // if previous node is epage, 'add 1 to previous node lpage' as fpage
                    if(prevNodeData && prevNodeData["grouptype"] == 'epage'){
                        fpageVal = prevNodeData['lpage']
                        if (/^(e)/gi.test(fpageVal)) { 
                            fpageVal = fpageVal.replace('e','')
                        } 
                        fpageVal = parseInt(fpageVal) + 1;
                     }
                    else{
                        // get e-start page as fpage
                        // if e-start page is not available get current fpage
                        var meta = $('#tocContainer').data('binder')['container-xml'].meta;
                        fpageVal = meta["e-first-page"] ?  meta["e-first-page"]['_text'] : histObj.fpage;
                       // endPageVal = fpageVal;// fpage as lpage based on config
                    }
                    data._attributes["grouptype"]='epage';
                    saveData(); // change grouptype as epage and save
                    if (/^(e)/gi.test(fpageVal)) { 
                        fpageVal = fpageVal.replace('e','')
                    }  
                    // if config data says e-lpage as false, set fpage and lpage as same 
                    if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')){
                        endPageVal = fpageVal;
                    }
                    else{
                         // calculate endpage based on page count
                        endPageVal = parseInt(fpageVal) + pageCount - 1; 
                    }
                    // add e as prefix for fpage and lpage
                    if (!(/^(e)/gi.test(fpageVal))) { 
                        fpageVal = 'e'.concat(fpageVal)
                    }
                    if (!(/^(e)/gi.test(endPageVal))) { 
                        endPageVal = 'e'.concat(endPageVal)
                    } 
                    // update fpage and lpage
                    data._attributes["fpage"]=fpageVal;
                    data._attributes["lpage"]=endPageVal;
                    var startpage = fpageVal.replace('e','')
                    updatePageNumber(currNodeIndex + 1 ,parseInt(startpage) + 1, 'epage');
                   //updatePageNumber(currNodeIndex ,fpageVal, 'epage');
                }
                else{
				//change grouptype as 'body' when epage flag is unset
                    data._attributes["grouptype"]='body';
                    var epage = parseInt(histObj.fpage)
                    fpageVal = histObj.fpage;
                   var endPageVal = histObj.lpage;
                    var pageCount = data._attributes["pageExtent"] ? data._attributes["pageExtent"] : parseInt(0)
                        pageCount = parseInt(pageCount)
                    var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                    var prevNodeData = '';
                    if(prevNode){
                        prevNodeData = $(prevNode).data('data')._attributes;
                    }
                    if(prevNodeData && prevNodeData["grouptype"] == 'body'){
                        fpageVal = parseInt(prevNodeData['lpage'])+1;
                       // endPageVal = prevNodeData['lpage'];
                       
                    }
					// remove prefix 'e' from fpage and lpage
                    if (/^(e)/gi.test(fpageVal)) { 
                        fpageVal = fpageVal.replace('e','')
                   }
                   fpageVal = parseInt(fpageVal)
                    if(pageCount && pageCount != 0){
                        endPageVal = fpageVal + pageCount - 1; 
                    }
                   
                   if (/^(e)/gi.test(endPageVal)) { 
                       endPageVal = endPageVal.replace('e','')
                   }
                    data._attributes["fpage"]= fpageVal;
                    data._attributes["lpage"]= endPageVal;
                    saveData();
                    //var firstNode = $('#tocBodyDiv div[data-grouptype="epage"][data-c-id]:first');
                    //var firstNodeID = parseInt(firstNode.attr('data-c-id'));
					// update the page number following epage articles 
                    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
                       var ePage = meta["e-first-page"] ?  meta["e-first-page"]['_text'] : parseInt(endPageVal)+1;
                    updatePageNumber(currNodeIndex + 1,ePage,'epage');
                }
                populateTOC();
               // updatePageNumber(currNodeIndex,fpageVal,data._attributes["grouptype"]);
            }
            updateChangeHistory(histObj);
            $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data', data);
            saveData();
        })
        // when the color checkbox is clicked update the figure status on the corresponding article data
        .on('click', '.color', function () {
            var figRef = $(this).closest('p').attr('rid');
            var color = 0;
            if ($(this).is(':checked')) {
                color = 1;
            }
            var dataCID = $('#msDataDiv').attr('data-c-rid');
            // get data from the corresponding article
            var data = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
            var histObj = JSON.parse(JSON.stringify(data._attributes));
          
        //for adverts/fillers
        if(data.region){
            var currRegion = $(this).attr('data-region-id');
            var regions = data.region;
            if (regions.constructor.toString().indexOf("Array") < 0){
                regions = [regions];
                }
            var regionsObj = {}, currIndex = 0;   
            regions.forEach(function(regionsObj, currIndex){
                if (regionsObj._attributes['type'] == currRegion){ 
                    regionsObj['_attributes']['data-color'] = color
                    if(regionsObj['file']){
                        var files = regionsObj['file']
                        if (files.constructor.toString().indexOf("Array") < 0){
                            files = [files];
                        }
                        var file = {}, fIndex = 0;
                    
                        files.forEach(function(file, fIndex){
                            if (file._attributes['type'] == 'file'){ 
                                if (color) {
                                    file['_attributes']['data-color'] = 'cmyk'
                                }
                                else{
                                    file['_attributes']['data-color'] = ''
                                }
                            }
                        })
                    }
                    if (color) {
                        histObj.change = 'changed Advert/Filler <b>' + data._attributes.id +'</b> of region <b>'+ currRegion +'</b> to be color in print';
                        //data.region['_attributes']['data-color'] = 'cmyk'
                    }
                    else {
                        histObj.change = 'changed Advert/Filler ' + data._attributes.id + ' of region '+ currRegion +' to be greyscale in print';
                        //data.region['_attributes']['data-color'] = ''
                    }
                    updateChangeHistory(histObj);
                }
             })
        }
        if (data.color) {
            // if we have more than one figure then data.color will be array of object else will be an object
            if (data.color.constructor.toString().indexOf("Array") > -1) {
                data.color.forEach(function (dc) {
                    if (dc._attributes['ref-id'] == figRef) {
                        if (color) {
                            histObj.change = 'changed ' + dc._attributes.label + ' to be color in print';
                        }
                        else {
                            histObj.change = 'changed ' + dc._attributes.label + ' to be greyscale in print';
                        }
                        updateChangeHistory(histObj);
                        dc._attributes.color = color;
                    }
                });
            }
            else {
                if (color) {
                    histObj.change = 'changed ' + data.color._attributes.label + ' to be color in print';
                }
                else {
                    histObj.change = 'changed ' + data.color._attributes.label + ' to be greyscale in print';
                }
                updateChangeHistory(histObj);
                data.color._attributes.color = color;
            }
        }
            $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data', data);
            saveData();
        })
		// when layout is changed, change the layout and populate the img src
		.on('change', 'select.layoutOption', function(e){
			var layoutType = '', layoutValue = 0;
			var layoutHTML = {};
            var dataCID = $('#adDataDiv').attr('data-c-rid');
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
                layoutType = $(this).attr('data-type');
                layoutValue = this.value; // get the changed layout value 
               // this.attributes["data-layout"]= layoutValue;
               this.selectedOptions[0].attributes['selected']=true;
            }
            var regionId =e.currentTarget.attributes["data-region-id"].value; // get the current region id
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
            var files = '';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                    var currRegionFlag = 'false';
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                                files = currDataObj.file;
                            }
                        }
                    })
                }
                    if(currRegionFlag == 'false'){
                        files = ({"_attributes":{"type": "file","data-region":currentReg, "data-layout":"a" ,"path":""}})
                    }
                }
            })
            var layoutHTML = $('#'+ regionId +'').html();
			$('#layout').removeClass('hide');
			$(targetNode).addClass('active');
			var regionHTML = '<div>'+ $('#'+ regionId +'').html() +'</div>';
			var layoutId = layoutValue.toLowerCase();
		    layoutId = layoutId.replace(/\s/,'');
			$('#'+ regionId +'').find(">div.layoutBlock").removeClass('hide');
			$('#'+ regionId +'').find(">div.layoutBlock").html(populateAdvert(currentReg,$('#'+layoutId+'').find('.layoutsection'),$('#'+ layoutId +'').html(),files)); // populate img path and change the layout
			//$('#'+ regionId +'').find(">div.layoutBlock").attr('id',layoutId); //change the layout id
			$('#'+ regionId +'').find(">div.layoutBlock").attr('data-layout',layoutValue); // change the data-layout
            //saveData();
        })
        //when float-placement is changed, update the float-placement value in xml based on region
        .on('change', 'select.floatPlacement', function(e){
			var floatPlacementType = '', floatPlacementValue = 0;
			
            var dataCID = $('#adDataDiv').attr('data-c-rid');
            if (e.currentTarget.nodeName.toLowerCase() === 'select'){
               floatPlacementValue = this.value; // get the changed float-placement value 
              // this.attributes['data-floatPlacement'] = floatPlacementValue;
              this.selectedOptions[0].attributes['selected']=true;
              
               
            }
            var regionId =e.currentTarget.attributes["data-region-id"].value; // get the current region id
            var myData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
            var currRegionFlag = 'false';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                                //change the float placement value
                                currDataObj._attributes['data-float-placement'] = floatPlacementValue;
                            }
                        }
                    })
                }
                
                }
            })
            if(currRegionFlag == 'false'){
                var newRegion = ({"_attributes":{"type": currentReg,"layout":"","data-float-placement":floatPlacementValue},"file":{"_attributes":{"type": "file","data-region":currentReg, "data-layout": "a","path":""}}})
                if(currentData){
                currentData.push(newRegion)
                myData.region = currentData;
                }
                else{
                    myData.region = newRegion;
                }
               // saveData();    
            }
            //$(this).closest('select').attr('data-float-placement',floatPlacementValue)
           // save the float placement value
            saveData();
        })
        //when editable data is changed, update the data based on region
        .on('blur', '.af-data-editable', function(e){
			 // get the current region id
            var regionId =e.currentTarget.attributes["data-region-id"].value;
            // get the key-path
             var key =e.currentTarget.attributes["data-key-path"].value;
             var curVal = e.currentTarget.textContent;
            var myData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
			var currentData =  $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')].region;
            var regionData = JSON.parse(JSON.stringify(configData.regions)).region;
            if (regionData.constructor.toString().indexOf("Array") < 0){
                regionData = [regionData];
                }

            if ((currentData)&&(currentData.constructor.toString().indexOf("Array") < 0)){
                currentData = [currentData];
                }
                var currRegionFlag = 'false';
            var currentReg =regionId.toLowerCase()
            var regObj = {}, rcIndex = 0;
            var currDataObj = {}, currIndex = 0;
            regionData.forEach(function(regObj, rcIndex){
                if (regObj._attributes['content-type'] == $(targetNode).attr('data-type')){ 
                   
                    if(currentData){
                    currentData.forEach(function(currDataObj, currIndex){ 
                        if(currDataObj._attributes['type'] == regObj._attributes['type']) {   
                            if (currDataObj._attributes['type'] == currentReg ){
                                currRegionFlag = 'true';
                               
                                //change the value
                                currDataObj._attributes[''+key+''] = curVal;
                            }
                        }
                    })
                }
                }
            })
            if(currRegionFlag == 'false'){
                var newRegion = ({"_attributes":{"type": currentReg,"layout":"" },"file":{"_attributes":{"type": "file","data-region":currentReg, "data-layout": "a","path":""}}})
                newRegion._attributes[''+key+''] = curVal;
                if(currentData){
                currentData.push(newRegion)
                myData.region = currentData;
                }
                else{
                    myData.region = newRegion;
                }
               // saveData();  
            }
           // save the float placement value
            saveData();
        })
        // when the editable data is changed update the corresponding data
        .on('blur', '.editable', function () {
            if (($(this).closest('.tabContent').length > 0) && ($(this).attr('data-dirty') == 'true')) {
                //if ($(this).closest('.tabContent').length > 0) {
                var myMeta = $('#tocContainer').data('binder')['container-xml'].meta;
                var keyPath = $(this).attr('data-key-path');
                var prevVal = '';
                if (keyPath) {
                    var keyPathArr = keyPath.split('.');
                    // the last key is required to update the data, if you include the key directly then we will get only the value
                    var lastKey = keyPathArr.pop();
                    keyPathArr.forEach(function (key) {
                        myMeta = myMeta[key];
                    })
                    prevVal = myMeta[lastKey];
                    myMeta[lastKey] = $(this).text();
                }
                var histObj = {
                    "change": "changed <b>" + $(this).attr('data-label').replace(/^[\s\t\r\n]+|[\s\t\r\n\:]+$/, '') + "</b> from <u>" + prevVal + '</u> to <u>' + $(this).text() + '</u>',
                    "file": "",
                    "fileType": "",
                    "id-type": ""
                };
                updateChangeHistory(histObj);
                //call page re-numbering when start page is changed
                var label =$(this).attr('data-label').replace(/^[\s\t\r\n]+|[\s\t\r\n\:]+$/, '')
                label = label.replace(/[\s\t\r\n]+/,'')
                if(label.toLowerCase() == 'startpage'){
                    var firstNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:first');
                    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
                    updatePageNumber(firstNodeID,$(this).text(),'body');
                    populateTOC();
                }
                else if(label.toLowerCase() == 'e-startpage'){
                    var firstNode = $('#tocBodyDiv div[data-grouptype="epage"][data-c-id]:first');
                    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
                    var ePage = $(this).text().replace('e','');
                    updatePageNumber(firstNodeID,ePage,'epage');
                    populateTOC();
                }
            }
            // if its not in
            else {

            }
            saveData();
        })
        // when the contenteditable data is changed, update the corresponding data
        /* text with formats like bold/italic are saved */
        .on('blur', '.contenteditable', function () {
            //if (($(this).closest('#msDataDiv').length > 0) && ($(this).attr('data-dirty') == 'true')) {
            if (($(this).closest('.tabContent').length > 0) && ($(this).attr('data-dirty') == 'true')) {
                var dataCID = $('#msDataDiv').attr('data-c-rid');
                // get data from the corresponding article
                var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                var containerHTML = '';
                
                var keyPath = $(this).attr('data-key-path');
                if (keyPath) {
                    var keyPathArr = keyPath.split('.');
                    // the last key is required to update the data, if you include the key directly then we will get only the value
                    var lastKey = keyPathArr.pop();
                    keyPathArr.forEach(function (key) {
                        myData = myData[key];
                    })
                    var dArr = [];
                    var updatedHTML = $(this).html();
                    $(this).contents().each(function () {
                        if (this.nodeType == 3) {
                            var ceData = this.nodeValue;
                            ceData = ceData.replace(/^[\s\t\r\n\u00A0]+|[\s\t\r\n\u00A0]+$/gi, '');
                        }
                        else {
                            var ceData = $(this).html();
                            ceData = ceData.replace(/[\s\t\r\n\u00A0]*<\/?(p|br)\/?>[\s\t\r\n\u00A0]*/gi, '');
                            ceData = ceData.replace(/</g, '[[').replace(/>/g, ']]');
                        }
                        dArr.push({ _text: ceData });
                    });
                    myData[lastKey] = dArr;
                }
                saveData();
                $(this).attr('data-dirty', 'false');
            }
            // if its not in
            else {

            }
        })
        // when the contenteditable data is changed update the corresponding data
        .on('input', '.editable, .contenteditable', function () {
            if ($(this).closest('.tabContent').length > 0 || ($(this).closest('#msDataDiv').length > 0)) {
                $(this).attr('data-dirty', 'true');
            }
        })
        .on('click', '.fileChooser', function () {
            $(this).parent().find('input:file').val('');
            $(this).parent().find('input:file').trigger('click');
            $('.loading-status').remove();
        })
        .on('change', 'input:file', function (e) {
            $('<div class="loading-status">0%</div>').insertAfter($(this).closest('p'));
            var fileObj = e.target.files[0];
            var keyPath = $(this).attr('data-key-path');
            var advertLayoutSec = $(this).attr('data-layout-sec');
            var manualUpload = $(this).attr('data-manualUpload');
			var proofType = $(this).attr('data-proofType');
            var currRegionVal = $(this).closest('div').parent().attr('data-region-id')
            var currLayoutVal = $(this).closest('div').parent().attr('data-layout')
            if(currRegionVal){
                currRegionVal = currRegionVal.toLowerCase();
            }
            var fileEle = this;
            var param = {
                file: fileObj,
                success: function (resp) {
                    if (resp.status.code == 200) {
                        showMessage({ 'text': 'Upload succeeded', 'type': 'success' });
                        $('.loading-status').remove();
                        var filePath = '/resources/' + resp.fileDetailsObj.dstKey;
                        var fileExtension ="pdf"
                        if (keyPath) {
                            var dataCID = $('#msDataDiv').attr('data-c-rid');
                            // get data from the corresponding article
                            var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                              // if the type is manuscript and the uploaded file is xml , then save the xml in AIP
                            if((myData._attributes)&&(myData._attributes.type)&&(myData._attributes.type == 'manuscript')){
                                var fileExtension =  filePath.split('.');
                                fileExtension = fileExtension.pop();
                                if(fileExtension == 'xml'){
                                    if (typeof (FileReader) != "undefined") {
                                        var reader = new FileReader();
                                        reader.onload = function (e) {
                                            //var xmlDoc = $.parseXML(e.target.result);
                                            saveXML('manuscript',e.target.result)
                                            form.remove();
                                        }
                                        reader.readAsText(fileObj);
                                    }
                                    //saveXML('manuscript',fileObj);
                                }
								if(fileExtension == 'pdf'){
                                    var files = myData['file']
                                    if (files.constructor.toString().indexOf("Array") < 0){
                                        files = [files];
                                    }
                                    files.forEach(function (file) {
                                        var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
                                        if(currProofType == proofType){
                                            file['_attributes']['path'] = filePath;
                                        }
                                    })
                                    //on upload of manuscript pdf, get pageExtent from user as input, and update pageNumber
                                    var notify = new PNotify({
                                        text:'<div id="notifyForm" class="pageExtentText"><div class="pageExtent"><p>Type the page extent for the uploaded pdf</p><input class="pageExtent_update" type="text"/><input class=" pf-button btn btn-small" style="margin-right: 10px;" type="button" name="pageExtent_update" value="Save"/></div></div>',
                                            icon: false,
                                            width: 'auto',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            },
                                            addclass: 'stack-modal',
                                            stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
                                            insert_brs: false
                                        });
                                        notify.get().find('.pageExtentText').on('click', '[name=cancel]', function() {
                                            notify.remove();
                                        })
                                        notify.get().find('.pageExtentText').on('click', '[name=pageExtent_update]', function() {
                                            var pageExtent = $('.pageExtentText').find('input[class=pageExtent_update]').val()
                                            var startPage = parseInt(myData._attributes.fpage)
                                            var endPage = startPage + parseInt(pageExtent) - 1;
                                            myData._attributes.pageExtent = pageExtent;
                                            myData._attributes.lpage = parseInt(endPage);
                                            saveData();
                                            var prevNode = $('[data-c-id="' + (dataCID - 1) + '"]');
                                             if(prevNode){
                                                var prevNodeData = $(prevNode).data('data')._attributes;
                                            //if prevNode groupType is same get fpage as startpage
                                                 if(prevNodeData.type && prevNodeData.type == 'manuscript'){
                                                    startPage =parseInt(prevNodeData.lpage)+1;
                                                 }
                                            }
                                        updatePageNumber(dataCID,startPage,myData._attributes.grouptype);
                                      //  saveData();
                                        notify.remove();
                                    })
                                        
                                    if(proofType == 'print'){
                                        $(fileEle).closest('div').find('object').attr('data', filePath);
                                       var param={"processType" : "PDFPreflight","pdfLink" : filePath} 
                                       //on upload of print pdf, trigger preflight
                                       eventHandler.pdf.export.exportToPDF(param,targetNode);
                                       saveData();
                                    }
                                  
                                }
                            }
                            else{
                                var keyPathArr = keyPath.split('.');
                                // the last key is required to update the data, if you include the key directly then we will get only the value
                                var lastKey = keyPathArr.pop();
                                keyPathArr.forEach(function (key) {
                                    myData = myData[key];
                                })
                                myData[lastKey] = filePath;
                                if(manualUpload){
                                    var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                                    var manualUploadArr = manualUpload.split('.');
                                    var lastKey = manualUploadArr.pop();
                                    manualUploadArr.forEach(function (mkey) {
                                        myData = myData[mkey];
                                    }) 
                                    myData[lastKey] = "true";
                                }
                                $(fileEle).closest('div').find('object').attr('data', filePath);
                                saveData();
                            }
                        }
                        else if (advertLayoutSec) { // if layout section is specifed, then loop through the region,layout and update the filepath 
						    var dataCID = $('#msDataDiv').attr('data-c-rid');
                            // get data from the corresponding article
                            var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
                            var fileFlag = false;
                            var regionArr = myData.region;
                            if(regionArr){
                            if (regionArr.constructor.toString().indexOf("Array") < 0){
                                regionArr = [regionArr];
                            }}
                            else{
                                regionArr = [];
                            }
                            var regionObj= {};
                            var rcIndex = 0;
                            var fileArray = [];
                            var regionFlagVal = false;
                            regionArr.forEach(function(regionObj, rcIndex){
                                if ((regionObj)&&(regionObj._attributes['type'] == currRegionVal) ){
                                    regionFlagVal = true;
                                    fileArray = regionObj.file; // get files from data
                                    if (fileArray.constructor.toString().indexOf("Array") < 0){
                                        fileArray = [fileArray];
                                    }
                                //match the layout sec of file uploaded with the layout sec of file in issueXML
                                    fileArray.forEach(function (file) {
                                        if(file._attributes['data-layout'] == advertLayoutSec){
                                            file._attributes['path'] = filePath;
                                            fileFlag = true;
                                        }
                                    })
                                    if (fileFlag == false){ // if file for the specified section is not available, then push the file into file Array
                                        var file = ({"_attributes":{"type": "file","data-region":currRegionVal, "data-layout": advertLayoutSec,"path":filePath}})
                                        fileArray.push(file)
                                    }
                                    regionObj["_attributes"]["layout"] = currLayoutVal; //change the layout value in xml
                                    regionObj["_attributes"]["value"] = "1";
                                    //myData.region.file = fileArray;
                                    regionObj.file = fileArray;
                                    $(fileEle).closest('div').find('object').attr('data', filePath);
                                    saveData();    
                                }
                            })
                            if (regionFlagVal == false){ // if region info is not available in data,add a new region
                                var newRegion = ({"_attributes":{"type": currRegionVal,"layout":currLayoutVal},"file":{"_attributes":{"type": "file","data-region":currRegionVal, "data-layout": advertLayoutSec,"path":filePath}}})
                                regionArr.push(newRegion)
                                myData.region = regionArr;
                                $(fileEle).closest('div').find('object').attr('data', filePath);
                                saveData();    
                            }
                        }
                    }
                    else {
                        showMessage({ 'text': 'Upload failed. Please try again or contact support', 'type': 'error' });
                    }
                },
                progress: function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = Math.round(event.loaded / event.total * 100) + "%";
                        $('.loading-status').css('width', percentComplete).text(percentComplete);
                    }
                },
                error: function (err) {
                    showMessage({ 'text': 'Upload failed. Please try again or contact support', 'type': 'error' });
                    $('.loading-status').remove();
			        }
                }
                var filePath = eventHandler.upload.uploadFile(param);
            });
    $('#tocBodyDiv')
        .on('mouseenter', '.sectionDataChild', function(e){
            // if you do not have another article following the current article then movedown button should not be visible
            if ($(this).next().hasClass('sectionDataChild')){
                $('.sectionDataChild').not('.hide').find('.controlIcons.icon-arrow_downward').removeClass('hide');
            }
            else{
                $(this).find('.controlIcons.icon-arrow_downward').addClass('hide');
            }
        })
        .on('click', '.controlIcons', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var targetNode = $(e.target);
            if (targetNode.hasClass('icon-arrow_downward')) {
                // swap the article info in the container array
                var currNode = targetNode.closest('[data-c-id]');
                var currNodeIndex = parseInt(currNode.attr('data-c-id'));
                var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex] = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex + 1];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex + 1] = tempData;
                var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
                var nextNodeData = $(nextNode).data('data')._attributes;
               
                var startPage =histObj.fpage;
                if(histObj["data-run-on"] && histObj["data-run-on"] == 'true'){
                    if ((histObj.type=='filler')&&(/^(blank|advert|filler|filler-toc)$/gi.test($(nextNode).data('data')._attributes.type))){
                          // filler should run-on only after manuscript
                        showMessage({ 'text': 'Filler can oly be added below Manuscript ', 'type': 'error' });
                        return false;
                    }
                    else{ 
                        startPage = parseInt(histObj.fpage) + 1;// if run-on article, then add 1 to fpage
                    }
                }
              
                histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' after ' + (nextNodeData['id-type'] ? nextNodeData['id-type'] : nextNodeData.type) + ' - ' + nextNodeData.id;
                currNode.attr('data-c-id', currNodeIndex + 1);
                nextNode.attr('data-c-id', currNodeIndex);
                currNode.insertAfter(nextNode);
                if(histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true'){
                    updatePageNumber(currNodeIndex,startPage,currNode.attr('data-grouptype'));
				}
                updateChangeHistory(histObj);
                // push updated data to server
                saveData();
            }
            else if (targetNode.hasClass('icon-arrow_upward')) {
                // swap the article info in the container array
                var currNode = targetNode.closest('[data-c-id]');
                var currNodeIndex = parseInt(currNode.attr('data-c-id'));
                var tempData = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex-1];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex - 1] = $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex];
                $('#tocContainer').data('binder')['container-xml'].contents.content[currNodeIndex] = tempData;
                var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                var prevNodeData = $(prevNode).data('data')._attributes;
               // var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
                //var nextNodeData = $(nextNode).data('data')._attributes;
                var startPage =prevNodeData.fpage;
                if(histObj["data-run-on"] && histObj["data-run-on"] == 'true'){ //check if run-on article
                    var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
                    var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
                    // filler should run-on only after manuscript
                    if ((histObj.type=='filler')&&(/^(blank|advert|filler)$/gi.test($(secondPrevNode).data('data')._attributes.type))){
                        showMessage({ 'text': 'Filler can oly be added below Manuscript ', 'type': 'error' });
                        return false;
                    }
                    else{ // if run-on article, then set 2nd previous node lpage as fpage
                        startPage = parseInt(secondPrevNodeData.lpage);
                    }
                }
                
                 if(prevNodeData["data-unpaginated"] && prevNodeData["data-unpaginated"] == 'true'){
                    var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
                    if(secondPrevNode){
                        var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
                        startPage = parseInt(secondPrevNodeData.lpage) +1;
                    }
                    else{
                        startPage = parseInt(histObj.fpage);
                    }
                 }
                histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' before ' + (prevNodeData['id-type'] ? prevNodeData['id-type'] : prevNodeData.type)  + ' - ' + prevNodeData.id;
                currNode.attr('data-c-id', currNodeIndex - 1);
                prevNode.attr('data-c-id', currNodeIndex);
                currNode.insertBefore(prevNode);
                //if next node is filler, move filler along with article
                /*if(nextNodeData.type == 'filler'){
                    prevNode.attr('data-c-id', currNodeIndex + 1);
                    nextNode.attr('data-c-id', currNodeIndex);
                    nextNode.insertAfter(currNode);
                }*/
                updateChangeHistory(histObj);
                if((histObj["data-unpaginated"] != 'true') && (histObj["data-run-on"] != 'true')){
                    saveData();
                  updatePageNumber(currNodeIndex - 1,startPage,currNode.attr('data-grouptype'));
                }
                // push updated data to server
                saveData();
            }
            else if (targetNode.hasClass('icon-close')) {
                (new PNotify({
                    title: 'Confirmation Needed',
                    text: 'Are you sure you want to remove the manuscript?',
                    hide: false,
                    confirm: { confirm: true },
                    buttons: { closer: false, sticker: false },
                    history: { history: false },
                    addclass: 'stack-modal',
                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                })).get()
                    .on('pnotify.confirm', function () {
                        var currNode = targetNode.closest('[data-c-id]');
                        var currNodeIndex = currNode.attr('data-c-id');
                        // remove the deleted manuscript's from the container
                        var removedData = JSON.parse(JSON.stringify($('#tocContainer').data('binder')['container-xml'].contents.content.splice(currNodeIndex, 1)));
                        removedData[0]._attributes.lpage = removedData[0]._attributes.lpage - removedData[0]._attributes.fpage + 1;
                        removedData[0]._attributes.fpage = 1;
                        var removedDataID = $('#uaaDiv > div:last').attr('id');
                        var removedDataID = removedDataID ? (parseInt(removedDataID) + 1) : 100000;
                        if (removedData[0]._attributes.type == 'manuscript'){
                        $('#uaaDiv').append(populateUAA(removedData[0], removedDataID));
                            $('#' + removedDataID).data('data', removedData[0]);
                        }
                        var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
                        histObj.change = 'Removed ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id;
                        updateChangeHistory(histObj);
                        var nextNode = currNode.nextAll('[data-c-id]:first');
                        if (nextNode){
                            var startPage =histObj.fpage;
                            if(histObj["data-run-on"] && histObj["data-run-on"]=='true'){ // if run-on article, add 1 to the fpage
                                var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
                                var prevNodeData = $(prevNode).data('data')._attributes;
                                startPage = parseInt(prevNodeData.lpage) + 1;
                            }
                           
                            if(histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true'){
                                updatePageNumber(nextNode.attr('data-c-id'),startPage ,currNode.attr('data-grouptype'));
                            }
                        }
                        // push updated data to server
                        saveData();
                        // remove manuscript node from UI
                        currNode.remove();
                    })
                    .on('pnotify.cancel', function () { });
            }
            else if (targetNode.hasClass('icon-add')) {
				$('#metaHistory, #manuscriptData, #advertsData').addClass('hide');
				$('#unassignedArticles').removeClass('hide');
            }
			// to edit section head
			else if (targetNode.hasClass('icon-edit')) {
                var currentSectionhead = $(targetNode).parent().parent().text();
                var notify = new PNotify({
                    text:'<div id="notifyForm" class="sectionHeadText"><div class="sectionHeadChange"><p>Change Section Head</p><input class="sectionHead_change" type="text"/><input class=" pf-button btn btn-small" style="margin-right: 10px;" type="button" name="sectionHead_change" value="Change"/><input class="sectionHead_cancel pf-button btn btn-small" type="button" name="cancel" value="Cancel"/></div></div>',
                        icon: false,
                        width: 'auto',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        },
                        addclass: 'stack-modal',
                        stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
                        insert_brs: false
                    });
                    notify.get().find('.sectionHeadChange').on('click', '[name=cancel]', function() {
                        notify.remove();
                    })
                    notify.get().find('.sectionHeadChange').on('click', '[name=sectionHead_change]', function() {
                    var changedContent = $('.sectionHeadChange').find('input[class=sectionHead_change]').val()
                    var contents = $('#tocContainer').data('binder')['container-xml'].contents.content;
					//get all contents that matches cuurent sectionhead and replace with changed value
                    contents.forEach(function (entry) {
                        var sectionType = entry._attributes.section ? entry._attributes.section : '';
                        if(sectionType ==  currentSectionhead){
                            entry._attributes.section  = changedContent
                        }
                    });
                    notify.remove();
                    var histObj = {
                        "change": "changed <b> sectionhead </b> from <u>" + currentSectionhead + '</u> to <u>' + changedContent + '</u>',
                        "file": "",
                        "fileType": "",
                        "id-type": ""
                    };
                    updateChangeHistory(histObj);
                    saveData();
                    populateTOC();
                    });
            }
        })
        .on('dragenter', '.sectionDataChild', dragEnter)
        .on('dragleave', '.sectionDataChild', dragLeave)
        .on('dragover', '.sectionDataChild', dragOver)
        .on('drop', '.sectionDataChild', drop)
    $('#uaaDiv, #advertsFillersTab')
        .on('dragstart', '.uaa', dragStart)
        .on('dragend', '.uaa', dragEnd)
  }
function loadUnassignedArticles(){
    $('#uaaDiv').html('');
    var cName = $('.list.folder.active').attr('data-customer');
    var pName = $('.list.folder.active').attr('data-project');
    var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&process=getArticlesXML', 'objectName': 'list' };
    getDataFromURL(obj)
        .then(function (project) {
            if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
                showMessage({ 'text': 'Something went wrong when retrieving articles list. Please try again or contact support', 'type': 'error' });
                return false;
            }
            try{
                var pData = project.data['container-xml'].contents;
                if (typeof(pData.content) === 'undefined'){
                    return true;
                }
                var pData = project.data['container-xml'].contents.content;
                if (pData.constructor.toString().indexOf("Array") == -1) {
                    pData = [pData];
                }
                var pLen = pData.length;
                var pHTML = '';
                var tempID = 10000
                pData.forEach(function(entry){
                    $('#uaaDiv').append(populateUAA(entry, tempID));
                    $('#' + tempID++).data('data', entry);
                });
                $('#uaaDiv').css('height', (window.innerHeight - $('#uaaDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
            }
            catch(e){
                showMessage({ 'text': 'Something went wrong when retrieving articles list. Please try again or contact support', 'type': 'error' });
                $('.la-container').fadeOut();
            }
        })
        .catch(function (err) {
            showMessage({ 'text': 'Something went wrong when retrieving articles list. Please try again or contact support', 'type': 'error' });
        })
}
// function to load card for blankpage/advert/filler 
function loadBlankOrAdvertOrFiller(){
    $('#advertsFillersTab').html('');
    var regionsConfig = JSON.parse(JSON.stringify(configData.regions)).region;
    if (regionsConfig.constructor.toString().indexOf("Array") < 0){
        regionsConfig = [regionsConfig];
    }
    //var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
    var tempID = '1000';
    
    //for blank page
    var blankId = '123';
	var cName = $('.list.folder.active').attr('data-customer');
    var pName = $('.list.folder.active').attr('data-project');
	var blankPdfPath = 'https://kriya2.kriyadocs.com/resources/' + cName + '/' + pName +'/resources/' + cName.toUpperCase() + '_' + pName.toUpperCase() + '_BLANK.pdf?method=server';
    var blankEntry = ({"_attributes":{"id": blankId, "section": "workplace","grouptype": "body","type": "blank", "fpage": "1","lpage": "1"},"file":{"_attributes":{"type":"pdf","path":blankPdfPath}}});
	containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '" fpage="'+ blankEntry._attributes.fpage +'" lapge="'+ blankEntry._attributes.lpage +'">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col s6 m6 l6 msType"  title="' + blankEntry._attributes.type + '">Blank Page</span>';
    containerHTML += '<span class="col s6 m6 l6 msID">' + blankEntry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
	containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (blankEntry._attributes.lpage - blankEntry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
	containerHTML += '</div>';
	$('#advertsFillersTab').append(containerHTML);
	$('#' + tempID++).data('data', blankEntry);
	
	// for advert
	var advertId = '154';
	 var fillerarr = [];
    var  advertEntry=({"_attributes":{"id": advertId, "section": "workplace","grouptype": "body","type":"advert","fpage":"1","lpage":"1"}});
    containerHTML = '<div class="uaa"  draggable="true" id="' + tempID + '" fpage="'+ advertEntry._attributes.fpage +'" lapge="'+ advertEntry._attributes.lpage +'">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col s6 m6 l6 msType"  title="' + advertEntry._attributes.type + '">Advert</span>';
    containerHTML += '<span class="col s6 m6 l6 msID">' + advertEntry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
	containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (advertEntry._attributes.lpage - advertEntry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
	containerHTML += '</div>';
	$('#advertsFillersTab').append(containerHTML);
    $('#' + tempID++).data('data', advertEntry);  
    
    // for unpaginated advert
   var unPaginatedAdvertId = '164';
   var fillerarr = [];
   var  unPaginatedAdvertEntry = ({"_attributes":{"id": unPaginatedAdvertId, "section": "workplace","grouptype": "body","type":"advert_unpaginated", "data-unpaginated":"true","fpage":"1","lpage":"1"}});
   containerHTML = '<div class="uaa"  draggable="true" id="' + tempID + '" fpage="'+ unPaginatedAdvertEntry._attributes.fpage +'" lapge="'+ unPaginatedAdvertEntry._attributes.lpage +'">';
   containerHTML += '<p class="row">';
   containerHTML += '<span class="col s6 m6 l6 msType"  title="' + unPaginatedAdvertEntry._attributes.type + '">Advert_Unpaginated</span>';
   containerHTML += '<span class="col s6 m6 l6 msID">' + unPaginatedAdvertEntry._attributes.id + '</span>';
   containerHTML += '</p>'
   containerHTML += '<p class="row">';
   containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (unPaginatedAdvertEntry._attributes.lpage - unPaginatedAdvertEntry._attributes.fpage + 1) + '</b></span>';
   containerHTML += '</p>'
   containerHTML += '</div>';
   $('#advertsFillersTab').append(containerHTML);
   $('#' + tempID++).data('data', unPaginatedAdvertEntry);  
	
    // for filler
    var fillerId = '174';
	var fillerEntry = ({"_attributes":{"id": fillerId, "section": "workplace","grouptype": "body","type":"filler", "data-run-on":"true","fpage":"1","lpage":"1"},"flag":{"_attributes":{"type":"run-on", "value":"1"}}});
    containerHTML = '<div class="uaa"   draggable="true" id="' + tempID + '" fpage="'+ fillerEntry._attributes.fpage +'" lapge="'+ fillerEntry._attributes.lpage +'">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col s6 m6 l6 msType"  title="' + fillerEntry._attributes.type + '">Filler</span>';
    containerHTML += '<span class="col s6 m6 l6 msID">' + fillerEntry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
	containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (fillerEntry._attributes.lpage - fillerEntry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
	containerHTML += '</div>';
	$('#advertsFillersTab').append(containerHTML);
    $('#' + tempID++).data('data', fillerEntry);
    
    // for filler toc
    var fillerTocId = '1774';
	var fillerTocEntry = ({"_attributes":{"id": fillerTocId, "section": "workplace","grouptype": "body","type":"filler-toc","data-run-on":"true","fpage":"1","lpage":"1"},"file":{"_attributes":{"type":"pdf","path":""}},"flag":{"_attributes":{"type":"run-on", "value":"1"}}});
    containerHTML = '<div class="uaa"   draggable="true" id="' + tempID + '" fpage="'+ fillerTocEntry._attributes.fpage +'" lapge="'+ fillerTocEntry._attributes.lpage +'">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col s6 m6 l6 msType"  title="' + fillerTocEntry._attributes.type + '">Filler TOC</span>';
    containerHTML += '<span class="col s6 m6 l6 msID">' + fillerTocEntry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
	containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (fillerTocEntry._attributes.lpage - fillerTocEntry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
	containerHTML += '</div>';
	$('#advertsFillersTab').append(containerHTML);
	$('#' + tempID++).data('data', fillerTocEntry);
}

function populateUAA(entry, tempID){
    if(entry._attributes.type == 'blank'){
        containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
        containerHTML += '<p class="row">';
        containerHTML += '<span class="col s6 m6 l6 msType" title="' + entry._attributes.type + '">' + entry._attributes.type + '</span>';
        containerHTML += '<span class="col s6 m6 l6 msID">' + entry._attributes.id + '</span>';
        containerHTML += '</p>';
        containerHTML += '<p class="row">';
    }
    //containerHTML = '<p class="uaatest" draggable="true" style="background: rgba(51,153,204,0.5) !important;margin: 0px; padding: 4px;"><span>Testing</span></p>';
    //return containerHTML;
    containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
    containerHTML += '<p class="row">';
    containerHTML += '<span class="col s6 m6 l6 msType" title="' + entry.type._text + '">' + entry.type._text + '</span>';
    containerHTML += '<span class="col s6 m6 l6 msID">' + entry._attributes.id + '</span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
    var accDate = '';
    if (typeof(entry.accDate) != 'undefined'){
        accDate = new Date(entry.accDate._text);
        accDate = (accDate ? accDate : '').toDateString().replace(/^[a-z]+ /gi, '');
    }
    containerHTML += '<span class="col s6 m6 l6 msAccDate" title="' + accDate + '">Acc. Date: <b>' + accDate + '</b></span>';
    containerHTML += '<span class="col s6 m6 l6 msPCount">Page Count: <b>' + (entry._attributes.lpage - entry._attributes.fpage + 1) + '</b></span>';
    containerHTML += '</p>'
    containerHTML += '<p class="row">';
    var titleText = 'Title Text';
    if (entry.title && entry.title._text) {
        titleText = entry.title._text;
    }
    containerHTML += '<span class="col s12 m12 l12 msText">' + titleText + '</span>';
    containerHTML += '</p>'
    containerHTML += '</div>';
    return containerHTML;
}

/**
 * update the page number starting from a given node
 * the renumbering starts from a particular node either a manuscript was dropped, moved up/down the order, new article has been added, when the pageExtent of manuscript is modified while proofing
 * input will be the node to start numbering from ,start page number from where numbering should start and groupType of the node
 */
function updatePageNumber(startNodeIndex, startPage, groupType){
    var firstNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:first');
    var firstNodeID = parseInt(firstNode.attr('data-c-id'));
    var lastNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id]:last');
    var lastNodeID = parseInt(lastNode.attr('data-c-id'));
    startNodeIndex = parseInt(startNodeIndex);
    if (/^(e)/gi.test(startPage)) { 
        startPage = startPage.replace('e',''); 
    }
    else if(groupType!='' && groupType!=undefined && groupType== 'roman' && typeof startPage != "number"){
        startPage = fromRoman(startPage)
    }
    startPage = parseInt(startPage);
     //get pagecount of front contents
    var frontContents = $('#tocBodyDiv div[data-c-id][data-grouptype="front"]');
    /*if (frontContents.constructor.toString().indexOf("Array") < 0) {
        frontContents = [frontContents];
    }*/
    var frontContentsCount = parseInt(0);
    frontContents.each(function(){
        var currFrontNode = $(this);
        var currFrontNodeID = parseInt(currFrontNode.attr('data-c-id'));
        var currFrontNodeData = currFrontNode.data('data');
        var pageExtent = parseInt(currFrontNodeData._attributes.pageExtent);
     frontContentsCount += pageExtent;
        
    });
    var datagroupType = '';
    if(groupType!='' && groupType!=undefined){
        datagroupType = "[data-grouptype='"+groupType+"']";
    }
    /* if group type is roman, update only the articles/adverts with same sectionhead*/
    if(groupType!='' && groupType!=undefined && groupType== 'roman'){
        var current = $('[data-c-id="' + (startNodeIndex) + '"]');
        var currentData = $(current).data('data')._attributes;
        var section = currentData["section"]
        if(section!='' && section!=undefined ){
          datagroupType += "[data-section='"+section+"']";
        }
    }
    var nodesToUpdate = $("#tocBodyDiv [data-c-id][data-type!='advert_unpaginated'][data-run-on!='true']"+datagroupType).filter(function() {
        return parseInt($(this).attr("data-c-id")) >= startNodeIndex;
    });
    nodesToUpdate.each(function(){
        var currNode = $(this);
        var currNodeID = parseInt(currNode.attr('data-c-id'));
        var currNodeData = currNode.data('data');
        try{
            var fpage = currNodeData._attributes.fpage;
            var lpage = currNodeData._attributes.lpage;
            if (/^(e)/gi.test(fpage)) { 
               fpage =  fpage.replace('e','');
            }
            if (/^(e)/gi.test(lpage)) { 
                lpage = lpage.replace('e','');
            }
            if( groupType== 'roman' && typeof fpage != "number"){
                fpage = fromRoman(fpage)
                lpage = lpage ? fromRoman(lpage) :lpage
            }
            fpage = parseInt(fpage);
            lpage = parseInt(lpage);
            lpage = lpage ? lpage : fpage;
            var pageCount = lpage - fpage + 1;
            if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
                pageCount =  currNodeData._attributes['pageExtent'];
            }
            pageCount = parseInt(pageCount);
            var runonFlag = currNodeData._attributes['data-run-on'];
            runonFlag = runonFlag ? runonFlag : 'false';
            currNodeData._attributes['pageExtent'] = pageCount;
            
            //if (currNodeData._attributes.type == 'manuscript'){
            if (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(currNodeData._attributes.type))) {   
                // if fresh-recto and pagecount of front contents is odd, return true for first article 
                if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (currNodeID == firstNodeID) && (frontContentsCount%2 != 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
               // if the configuration says fresh-recto is true, then startPage should always start on odd page, if not show error  
                else if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (startPage%2 == 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
					$(currNode).find('.pageRange').attr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
                else if ((configData['page']) && configData['page']._attributes['type'].toLowerCase() == 'continuous-publication') {
                    if((configData['page']._attributes['first-page'] == 'recto') ){
                        // if first page is recto  and pagecount of front contents is odd, return true for first article
                        if((currNodeID == firstNodeID) && (frontContentsCount %2 != 0) ){
                            $(currNode).find('.pageRange').addClass('incorrectPage');
							$(currNode).find('.pageRange').attr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'true';
                        }
                         //if the configuration says continuous-publication and first page is recto , first page should always starts on odd page, if not show error
                       else if((currNodeID == firstNodeID) && (startPage %2 == 0) ){
                            $(currNode).find('.pageRange').addClass('incorrectPage');
							$(currNode).find('.pageRange').attr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'true';
                        }
                        else{
                            $(currNode).find('.pageRange').removeClass('incorrectPage');
							$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                            currNodeData._attributes.incorrectPage = 'false';
                        }
                    }
                    else{
					//if the configuration says continuous-publication and first page is not recto , page can start in odd or even
                        $(currNode).find('.pageRange').removeClass('incorrectPage');
						$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                        currNodeData._attributes.incorrectPage = 'false';
                    }
                }
                else{
                    $(currNode).find('.pageRange').removeClass('incorrectPage');
					$(currNode).find('.pageRange').removeAttr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'false';
                }
            }
           
            if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
                currNodeData._attributes.fpage = 'e'.concat(startPage);
                var endPage = startPage + pageCount - 1;
                 // if config data says e-lpage as false, set fpage and lpage as same 
                if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')){
                    endPage = startPage;
                }
                currNodeData._attributes.lpage  = 'e'.concat(endPage);
             }
             else{
                var endPage = startPage + pageCount - 1;
                currNodeData._attributes.fpage = startPage;
                currNodeData._attributes.lpage = endPage;
                if( groupType== 'roman' && typeof startPage == "number"){
                    currNodeData._attributes.fpage = toRoman(startPage);
                    currNodeData._attributes.lpage = toRoman(endPage);
                   
                }
             }
            var unpaginated = currNodeData._attributes['data-unpaginated'];
                unpaginated = unpaginated ? unpaginated : 'false';
            // end page of last artricle should always be even
            if(unpaginated != 'true'){
                if((currNodeID == lastNodeID) && (endPage %2 != 0)){
                    $(currNode).find('.pageRange').addClass('incorrectPage');
                    $(currNode).find('.pageRange').attr('title','incorrectPage');
                    currNodeData._attributes.incorrectPage = 'true';
                }
            } 
               //if page range is modified, add class pageModified 
                if(currNode.find('.rangeStart').text() != startPage || currNode.find('.rangeEnd').text() != endPage){
					if(currNodeData._attributes && currNodeData._attributes.grouptype == 'epage'){
						currNode.find('.rangeStart').text('e'.concat(startPage));
						currNode.find('.rangeEnd').text('e'.concat(endPage));
						currNode.attr('data-modified', 'true');
						currNodeData._attributes['data-modified'] = 'true';
						$(currNode).find('.pageRange').attr('title', 'pageModified');
					   
					}
					else{
						currNode.find('.rangeStart').text(startPage);
						currNode.find('.rangeEnd').text(endPage);
                            if( groupType== 'roman' && typeof startPage == "number"){
                                currNode.find('.rangeStart').text(toRoman(startPage));
                                currNode.find('.rangeEnd').text(toRoman(endPage));
                               
                            }
						if(unpaginated != 'true' && currNodeData._attributes && currNodeData._attributes.type != 'blank'){  
							currNode.attr('data-modified', 'true');
							$(currNode).find('.pageRange').addClass('pageModified');
							$(currNode).find('.pageRange').attr('title', 'Page is Modified');
							currNodeData._attributes['data-modified'] = 'true';
						}
					}
				}
            var nextNodeData = $('[data-c-id="' + (parseInt(currNode.attr('data-c-id')) + 1) + '"]').data('data');
            var nextRunOn = nextNodeData._attributes['data-run-on'];
            nextRunOn = nextRunOn ? nextRunOn : 'false';
            var nextUnpaginated = nextNodeData._attributes['data-unpaginated'];
                nextUnpaginated = nextUnpaginated ? nextUnpaginated : 'false';
          
                startPage = endPage + 1    // next manuscript should start from next page
        
            return true;
        }
        catch(e){
            return false;
        }
    });
    saveData();
    //calculate total pages
    updateTotalPages();
}
/*  calculate - total page number add the extent of articles,adverts,fillers*/
function updateTotalPages(){
    //get all contents - add pageExtent for each contents
    var totalPages =parseInt(0);
    var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
    content.forEach(function (entry) {
        if(entry){
        var grouptype = entry._attributes.grouptype ? entry._attributes.grouptype : '';
        var section = entry._attributes.section ? entry._attributes.section : '';
        //if(grouptype == 'body' || grouptype == 'epage' || grouptype == 'roman'){
        if(!(/^(cover|ifc|ibc|obc)/gi.test(section))){
			var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : parseInt(0);
            pageExtent = parseInt(pageExtent);
			var runOn = entry._attributes['data-run-on'] ? entry._attributes['data-run-on'] : 'false';
            /*if(runOn == 'true'){ // run-on articles starts with previous article, so, reduce 1 from the pageExtent
                pageExtent = pageExtent - 1
            }*/
            if(runOn != 'true'){
                totalPages += pageExtent;
            }
		}
        }
    });

    //update the total page number value in MetaData
    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
    var pagecount = meta["page-count"]['_text'] ?  meta["page-count"]['_text'] : parseInt(0);
    if(totalPages != null && totalPages != 0){
        meta["page-count"]['_text'] = totalPages
        console.log(totalPages)
        saveData();
    }
}
/*https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
Function to convert integer to roman 
input - intger val e.g, toRoman(4)*/
function toRoman(num) {  
    var result = '';
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
   // var roman = ["M", "CM","D","CD","C", "XC", "L", "XL", "X","IX","V","IV","I"];
   var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
    for (var i = 0;i<=decimal.length;i++) {
      while (num%decimal[i] < num) {     
        result += roman[i];
        num -= decimal[i];
      }
    }
    return result;
  }
  /*https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
Function to convert roman to integer 
input - roman numeral  e.g, fromRoman('iii')*/
  function fromRoman(str) {  
    var result = 0;
    // the result is now a number, not a string
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  //var roman = ["M", "CM","D","CD","C", "XC", "L", "XL", "X","IX","V","IV","I"];
  var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
    for (var i = 0;i<=decimal.length;i++) {
      while (str.indexOf(roman[i]) === 0){
        result += decimal[i];
        str = str.replace(roman[i],'');
      }
    }
    return result;
  }
$(document).ready(function () {
    // load xml-js into browser using systemjs and jspm
    System.import('npm:xml-js').then(function (c) {
        convert = c;
    });
    //Initialize the poste and subscriber
    eventHandler.publishers.add();
    eventHandler.subscribers.add();
    window.addEventListener("resize", function () {
        $('#explorerContainer, #tocContainer, .vertDots, #metaHistory, #unassignedArticles').css('height', (window.innerHeight - $('#explorerContainer').offset().top - $('#footerContainer').height() - 10) + 'px');
        $('.bodyDiv, .tabContent').css('height', (window.innerHeight - $('.bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
        $('#uaaDiv').css('height', (window.innerHeight - $('#uaaDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
    });
    initEvents();
    getCustomerList()
        .then(function (data) {
            $('#explorerContainer, #tocContainer, .vertDots, #metaHistory, #unassignedArticles').css('height', (window.innerHeight - $('#explorerContainer').offset().top - $('#footerContainer').height()) + 'px');
            $('.bodyDiv, .tabContent').css('height', (window.innerHeight - $('.bodyDiv').offset().top - $('#footerContainer').height() - 10) + 'px');
        })
        .catch(function (err) {
            showMessage({ 'text': 'Something went wrong when fetching customer data. Please try again or contact support', 'type': 'error' });
            console.log(err);
        })
})
