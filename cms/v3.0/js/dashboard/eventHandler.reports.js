/**
 * This file will display the Report tables in dashboard
 */
var paramDate;
var reportDoiList = {};
var report = {};
var articles = '';
var dataToPost;
var exportTableButton = '<i class="fa fa-download exportTable" aria-hidden="true" onclick="eventHandler.bulkedit.export.exportExcel({\'tableID\':\'{tableID}\'})"></i>';
(function (eventHandler) {
    eventHandler.tableReportsSummary = {
        getCustomerData: function (param) {
            paramDate = param;
            if (!param && (!param.customer || !param.project)) {
                return false;
            }
            $('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 50) - $('#footerContainer').height()) + 'px');
            var defaultTable = JSON.parse(JSON.stringify(eventHandler.tableReportsSummary.tableConfig));
            defaultTable.forEach(function (config) {
            if(param.target.text().trim() == config.tableName || config.tableName == "Stage History"){
                var configuration = config;
                $('#dashboardReports #' + configuration.tableId).show();
                if (configuration.customerToDisplayTable != 'all') {
                    var tableToHide = configuration.customerToDisplayTable.split(',');
                    if (tableToHide.indexOf(param.customer) == -1) {
                        $('#dashboardReports #' + configuration.tableId).hide();
                        return true;
                    }
                }
                $('#dashboardReports #' + configuration.tableId + ' .chartBoxDiv').remove()
                $('#dashboardReports #' + configuration.tableId).append('<div  class="chartBoxDiv" style="min-height:30%;">    </div>')
                $('#reportContent .chartBoxDiv').css('background-color', 'white');
                $('#reportContent .chartBoxDiv').html('<div class="chartLoader"></div>');
                if (configuration.apiDetails) {
                    var result = [];
                    var count = 0;
                    configuration.apiDetails.forEach(function (api, apiIndex) {
                        for (parameter in api) {
                            if (param[parameter] && !api[parameter].match(/^\{(.*)\}$/)) {
                                api[parameter] = param[parameter];
                            } else if (param['project'] && parameter == "regex") {
                                api['project'] = param['project'].replace(/ OR /g, '|');
                            }
                            if (param[parameter] && api[parameter].match(/^\{(.*)\}$/)) {
                                api[parameter] = api[parameter].replace(/^\{(.*)\}$/, '$1');
                            }
                        }
                        configuration.tableName = configuration.tableName.replace('{from}', moment(api.fromDate).format('MMM D, YYYY'))
                        configuration.tableName = configuration.tableName.replace('{to}', moment(api.toDate).format('MMM D, YYYY'))
                        if(configuration.tableId == "userReport"){
                            var date = new Date(param.fromDate);
                            var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                            fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
                            toDate = dateFormat(lastDay,'yyyy-mm-dd');
                            api.fromDate = fromDate,
                            api.toDate = toDate
                      }
                        $.ajax({
                            type: "POST",
                            url: '/api/getarticlelist',
                            data: api,
                            dataType: 'json',
                            success: function (respData) {
                                if (!respData || respData.length == 0) {
                                    return;
                                }
                                if (api.mainDataXpath) {
                                    var input = JSONPath({ json: respData, path: api.mainDataXpath })[0];
                                }
                                if (configuration.apiDetails.length == 1) {
                                    count = 0;
                                    eval(configuration.functionToCall)(input, configuration, param.customer, param.roleType, param.accessLevel, param.fromDate, param.toDate, param.stageName);
                                } else {
                                    // result.push(input);
                                    // var pushObj = { 'stage': api.stageName, 'data': input };
                                    for (each of input) {
                                        each['currentStage'] = api.stageName;
                                        if (api.name) {
                                            each['apiName'] = api.name;
                                        }
                                    }
                                    Array.prototype.push.apply(result, input);
                                    if (configuration.apiDetails.length - 1 == count) {
                                        count = 0;
                                        eval(configuration.functionToCall)(result, configuration, param.customer, param.roleType, param.accessLevel, param.fromDate, param.toDate);
                                    }
                                }
                                count++;
                            },
                            error: function (respData) {
                                console.log(respData);
                            }
                        });
                    })
                }
            }
            });
        },
        getHoldReport: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            var tableId = configuration.tableId;
            var input1 = JSONPath({ json: input, path: configuration.apiDetails[0].mainDataXpath })[0];
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '';
            if (configuration.headings) {
                var columnCount = 1;
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col" colNum="' + columnCount++ + '"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a><input type="text" class="form-control" style="margin-top: 11px;"  oninput="eventHandler.tableReportsSummary.columnFilterForHold(this);" placeholder=""></th>'
                    })
                    fullTable += '</tr></thead>'
                }
            }
            fullTable += '<tbody></tbody></table>'
            var dotObj = [];
            for (var i in input1) {
                var currData = input1[i];
                if (currData._source["stage"][currData._source["stage"].length - 1] && currData._source["stage"][currData._source["stage"].length - 1].name == 'Hold') {
                    var startDateForHold = moment(currData._source["stage"][currData._source["stage"].length - 1]["start-date"] + ' UTC').format('YYYY-MM-DD')
                    var comments = currData._source["stage"][currData._source["stage"].length - 1]["comments"];
                    var holdby = '';
                    if(comments && comments.match(/Hold by\:(.*) /)) {
                      holdby = comments.replace(/^(.*)(Hold by\:\s?)([^;]*)(.*)/, '$3')
                    }
                    var heldStage = currData._source["stage"][currData._source["stage"].length - 2].name
                    var toDate = moment().utc().format("YYYY-MM-DD")
                }
                if (comments == null) {
                    comments = '-'
                }
                else{
                    comments = comments
                }
                var dateDiff = dateDistance(startDateForHold, toDate, daysForCalculation.holidays['default']);

                var objParam = {
                    "startdate": startDateForHold,
                    "doi": currData._source.id,
                    "daysinhold": dateDiff,
                    "comment": comments,
                    "held": heldStage,
                    "holdby": holdby
                }
                dotObj.push(objParam);

            }
            var articleLength = input1.length;
            fullTable = '<h4>' + configuration.tableName + '<span id="totalCountForHold"> (' + input1.length + ' articles)' + '</span> <span id="filterCount1"></span><button style="float: right;" type="button" class="btn-primary btn-sm reportExportExcel" onclick="eventHandler.bulkedit.export.exportExcel({\'tableID\':\'holdTableDownload\',\'excludeRow\': \'.collapse,.fltrow\', \'excludeColumn\': \'.reportCheckBox\'})" class="fa fa-file-excel-o btn-pill btn btn-danger btn-sm" style="border-radius: 1rem 0rem 0rem 1rem; margin-left: 1rem;"> Export Excel</button></h4><table id="holdTableDownload" class="table table-striped table-bordered table-hover" style="background-color:white;">' + fullTable;
            $('#dashboardReports #' + tableId).html($(fullTable));
            var pagefn = doT.template(document.getElementById('holdReport').innerHTML, undefined, undefined);
            $('#dashboardReports #' + tableId).find('tbody').append(pagefn(dotObj));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);

        },
        getDailyReport: function (input, configuration, customer, roleType, accessLevel) {
            var plannedCompleted=false;
            if (!input && !configuration) {
                return;
            }
            var tableId = configuration.tableId;
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '';
            if (configuration.headings) {
                var columnCount = 1;
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col" colNum="' + columnCount++ + '"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a><input type="text" class="form-control" style="margin-top: 11px;"  oninput="eventHandler.tableReportsSummary.columnFilter(this);" placeholder=""></th>'
                    })
                    fullTable += '</tr></thead>'
                }
            }
            fullTable += '<tbody></tbody></table>'
            var dotObj = [];
            var averageTimeTaken = 0;
            for (var i in input) {
                var currData = input[i];
                if (currData && currData._source && currData._source.stage) {
                    var stages = currData._source.stage;
                    if (stages.constructor !== Array) {
                        stages = [stages];
                    }
                    [currData.currentStage].forEach(function (eachStage) {
                        var minutesTaken = proofCount = 0;
                        var signedOffBy = '';
                        var stageEndDate = '';
                        var stagePlannedEndDate = '';
                        var stageFound = false;
                        var returnedData = $.grep(stages, function (element, index) {
                            return (element.name == eachStage || (element.name == 'Support' && stages[index-1].name == eachStage)) ;
                        });
                        for (stageIndex in returnedData) {
                            stageIndex = Number(stageIndex);
                            var currentStage = returnedData[stageIndex];
                            var currentComment = currentStage["comments"]
                            var validDate = moment(currentStage["end-date"] + ' UTC').isBetween(paramDate.fromDate, paramDate.toDate, 'days', '[]')

                            if (/^(add\s?Job|File\s?download|Convert\s?Files|hold)$/i.test(currentStage.name)) {

                            }else if ((currentComment && currentComment.match(/.*(Moved to Hold|Moved to addJob)/))) {

                            }
                            else if ((currentStage["status"] == 'completed') || (currentStage["status"] == 'in-progress')) {
                                stageFound = true;
                                var currentStatus = currentStage["status"];
                                //if (currentStatus == 'in-progess' && currentComment != 'support') {
                                if (currentStatus == 'completed' && currentStage['job-logs'] && currentStage['job-logs'].log) {
                                    var currentLogs = currentStage['job-logs'].log;
                                    var status = 'completed'
                                    var dueDate = moment(currentStage["planned-end-date"] + ' UTC').format('YYYY-MM-DD')
                                    if (currentLogs.constructor !== Array) {
                                        currentLogs = [currentLogs];
                                    }
                                    for (eachLog of currentLogs) {
                                        var startDate = eachLog['start-date'] + ' ' + eachLog['start-time'];
                                        startDate = new Date(startDate);
                                        var endDate = eachLog['end-date'] + ' ' + eachLog['end-time'];
                                        endDate = new Date(endDate);
                                        minutesTaken += parseInt((endDate - startDate) / 1000) / 60;
                                        averageTimeTaken += Math.floor(parseInt((endDate - startDate) / 1000) / 60);

                                        // minutesTaken += moment(endDate, 'YYYY-MM-DD HH:mm:ss').diff(moment(startDate, 'YYYY-MM-DD HH:mm:ss'), 'minutes');
                                    }
                                    signedOffBy = currentStage.assigned.to;
                                    stageEndDate = moment(currentStage['end-date'] + ' UTC').format('YYYY-MM-DD');
                                    stagePlannedEndDate = moment(currentStage['planned-end-date'] + ' UTC').format('YYYY-MM-DD');
                                    if (currentStage.object && currentStage.object.proof && currentStage.object.proof.pdf) {
                                        if (currentStage.object.proof.pdf.constructor !== Array) {
                                            proofCount += Object.keys(currentStage.object.proof.pdf).length;
                                        } else {
                                            proofCount += currentStage.object.proof.pdf.length;
                                        }
                                    }

                                }
                                else if (currentStatus == 'in-progress') {
                                    status = 'in-progress'
                                    var dueDate = moment(currentStage["sla-end-date"] + ' UTC').format('YYYY-MM-DD')
                                    if (currentStage['job-logs'] && currentStage['job-logs'].log) {
                                        var currentLogs = currentStage['job-logs'].log;

                                        if (currentLogs.constructor !== Array) {
                                            currentLogs = [currentLogs];
                                        }
                                        for (eachLog of currentLogs) {
                                            if (eachLog['start-date'] && eachLog['start-time'] && eachLog['end-date'] && eachLog['end-time']) {
                                                var startDate = eachLog['start-date'] + ' ' + eachLog['start-time'];
                                                startDate = new Date(startDate);
                                                var endDate = eachLog['end-date'] + ' ' + eachLog['end-time'];
                                                endDate = new Date(endDate);
                                                minutesTaken += parseInt((endDate - startDate) / 1000) / 60;
                                                averageTimeTaken += Math.floor(parseInt((endDate - startDate) / 1000) / 60);
                                            }
                                            // minutesTaken += moment(endDate, 'YYYY-MM-DD HH:mm:ss').diff(moment(startDate, 'YYYY-MM-DD HH:mm:ss'), 'minutes');
                                        }


                                        if (currentStage.object && currentStage.object.proof && currentStage.object.proof.pdf) {
                                            if (currentStage.object.proof.pdf.constructor !== Array) {
                                                proofCount += Object.keys(currentStage.object.proof.pdf).length;
                                            } else {
                                                proofCount += currentStage.object.proof.pdf.length;
                                            }
                                        }
                                        if (eachLog["status"]["#text"] == 'open') {
                                            status = 'live'
                                        }
                                        else {
                                            status = 'in-progress'
                                        }
                                    }
                                    signedOffBy = currentStage.assigned.to;
                                    stagePlannedEndDate = moment(currentStage['planned-end-date'] + ' UTC').format('YYYY-MM-DD');
                                    stageEndDate = '-';
                                }

                                else if (stageFound && currentStage.name != eachStage) {
                                    stageFound = false;
                                    break;
                                }
                                // var hours = Math.trunc(minutesTaken / 60);
                                // var minutes = (minutesTaken / 60).toFixed(2);
                                // minutes = Math.ceil(minutes.split('.')[1] * 0.6);
                                var plannedCompleted = false;
                                if (stageEndDate == dueDate && currentStatus == 'completed') plannedCompleted = true;
                                var hours = Math.floor(minutesTaken / 60);
                                var minutes = Math.floor(minutesTaken % 60);
                                var finalMinutes = (hours * 60) + minutes
                                if(currentComment && currentComment.match(/.*(Moved to Support)/)){

                                }
                                else if(validDate && currentStage.name != "Support"){
                                var objParam = {
                                    "manuscriptType": currData._source.articleType,
                                    "planneddate": stagePlannedEndDate,
                                    "completeddate": stageEndDate,
                                    "doi": currData._source.id,
                                    "stage": eachStage,
                                    "minutes": finalMinutes,
                                    "signedOffBy": signedOffBy,
                                    "proofCount": proofCount,
                                    "word-count": currData._source['word-count'],
                                    "word-count-without-ref": currData._source['word-count-without-ref'],
                                    "status": status,
                                    "dueDate": dueDate,
                                    "plannedCompleted": plannedCompleted
                                }
                                dotObj.push(objParam);

                            }
                            }
                        }

                    })
                }

            }
            var articleLength = input.length;
            fullTable = '<h4>' + configuration.tableName + '<span id="totalCountForDaily"> (' + input.length + ' articles)' + '</span> <div class="averageTime" style="display: inline;"> Average Time : ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) / 60)) : '0') + ' h ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) % 60)) : '0') + ' m</div><span id="filterCount" style="margin-left: 530px"></span><span id="filterTime"></span><button type="button" class="btn-primary btn-sm reportExportExcel" onclick="eventHandler.bulkedit.export.exportExcel({\'tableID\':\'dailyTableDownload\',\'excludeRow\': \'.collapse,.fltrow\', \'excludeColumn\': \'.reportCheckBox\'})" class="fa fa-file-excel-o btn-pill btn btn-danger btn-sm" style="border-radius: 1rem 0rem 0rem 1rem; margin-left: 1rem; float: right;"> Export Excel</button></h4><table id="dailyTableDownload" class="table table-bordered table-hover" style="background-color:white;">' + fullTable;
            $('#dashboardReports #' + tableId).html($(fullTable));
            var pagefn = doT.template(document.getElementById('dailyReport').innerHTML, undefined, undefined);
            $('#dashboardReports #' + tableId).find('tbody').append(pagefn(dotObj));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);
        },
        createTable: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            for (var i in input) {
                if (i == 'planned' && configuration.plannedDataXpath) {
                    var input1 = JSONPath({ json: input, path: configuration.plannedDataXpath })[0];
                    var totalCount = 0;
                    var tableId = configuration.tableId;
                    reportDoiList[tableId] = {};
                    input1.forEach(function (keyValue) {
                        var tableId = configuration.tableId;
                        var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                        if (typeof (reportDoiList[tableId][firstRowName]) == "undefined") {
                            reportDoiList[tableId][firstRowName] = {};
                        }
                        reportDoiList[tableId][firstRowName]['actualplanned'] = {};
                        var innerInput = JSONPath({ json: keyValue, path: configuration.columnvalueXpath })[0];
                        innerInput.forEach(function (values, keyCount) {
                            var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                            var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                            reportDoiList[tableId][firstRowName]['actualplanned'][columnHeading] = {};
                            reportDoiList[tableId][firstRowName]['actualplanned'][columnHeading]['doc_count'] = values['doc_count']
                            reportDoiList[tableId][firstRowName]['actualplanned'][columnHeading]['doi'] = [];
                            totalCount += articleCount;
                            JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                reportDoiList[tableId][firstRowName]['actualplanned'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                            })
                        })
                        reportDoiList[tableId][firstRowName]['actualplanned']['totalCount'] = totalCount;
                        totalCount = 0;
                    })
                }
                if (i == 'plannedCompleted' && configuration.completedDataXpath) {
                    var totalCount1, totalCount2 = 0;
                    var input1 = JSONPath({ json: input, path: configuration.completedDataXpath })[0];
                    input1.forEach(function (keyValue, index) {
                        var tableId = configuration.tableId;
                        if (keyValue.Completed) {
                            var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                            var plannedHit = JSONPath({ json: input1[index], path: configuration.columnvalueXpath })[0]
                            if (typeof (reportDoiList[tableId][firstRowName]) == "undefined") {
                                reportDoiList[tableId][firstRowName] = {};
                            }
                            reportDoiList[tableId][firstRowName]['on-time'] = {};
                            reportDoiList[tableId][firstRowName]['delay'] = {};
                            reportDoiList[tableId][firstRowName]['ahead'] = {};
                            totalCount = totalCount1 = totalCount2 = 0
                            plannedHit.forEach(function (key, i) {
                                var endHit = JSONPath({ json: plannedHit[i], path: configuration.endvalueXpath })[0];
                                endHit.forEach(function (values, keyCount) {
                                    if (i == keyCount) {
                                        if (!totalCount) totalCount = 0
                                        var columnHeading = JSONPath({ json: plannedHit[i], path: '$..key' })[0];
                                        var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                                        if (!reportDoiList[tableId][firstRowName]['on-time'][columnHeading]) reportDoiList[tableId][firstRowName]['on-time'][columnHeading] = {};
                                        reportDoiList[tableId][firstRowName]['on-time'][columnHeading]['doc_count'] = values['doc_count']
                                        if (!reportDoiList[tableId][firstRowName]['on-time'][columnHeading]['doi']) reportDoiList[tableId][firstRowName]['on-time'][columnHeading]['doi'] = [];

                                        totalCount += articleCount;
                                        JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                            reportDoiList[tableId][firstRowName]['on-time'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                                        })
                                        reportDoiList[tableId][firstRowName]['on-time']['totalCount'] = totalCount;
                                    }
                                    else if (i > keyCount) {
                                        if (!totalCount1) totalCount1 = 0
                                        var columnHeading = JSONPath({ json: plannedHit[i], path: '$..key' })[0];
                                        var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                                        if (!reportDoiList[tableId][firstRowName]['ahead'][columnHeading]) reportDoiList[tableId][firstRowName]['ahead'][columnHeading] = {};
                                        reportDoiList[tableId][firstRowName]['ahead'][columnHeading]['doc_count'] = values['doc_count']
                                        if (!reportDoiList[tableId][firstRowName]['ahead'][columnHeading]['doi']) reportDoiList[tableId][firstRowName]['ahead'][columnHeading]['doi'] = [];
                                        totalCount1 += articleCount;
                                        JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                            reportDoiList[tableId][firstRowName]['ahead'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                                        })
                                        reportDoiList[tableId][firstRowName]['ahead']['totalCount'] = totalCount1;
                                    }
                                    else if (i < keyCount) {
                                        if (!totalCount2) totalCount2 = 0;
                                        var columnHeading = JSONPath({ json: plannedHit[i], path: '$..key' })[0];
                                        var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                                        if (!reportDoiList[tableId][firstRowName]['delay'][columnHeading]) reportDoiList[tableId][firstRowName]['delay'][columnHeading] = {};
                                        //reportDoiList[tableId][firstRowName]['delay'][columnHeading]['doc_count'] = reportDoiList[tableId][firstRowName]['delay'][columnHeading]['doc_count'] + values['doc_count']
                                        if (!reportDoiList[tableId][firstRowName]['delay'][columnHeading]['doi']) reportDoiList[tableId][firstRowName]['delay'][columnHeading]['doi'] = [];
                                        totalCount2 += articleCount;
                                        JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                            reportDoiList[tableId][firstRowName]['delay'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                                        })
                                        reportDoiList[tableId][firstRowName]['delay']['totalCount'] = totalCount2;
                                    }
                                })
                            })
                        }
                    })

                }
            }
            var dotData = {};
            var dotOutput = {};
            dotData.data = reportDoiList[tableId];
            dotData.output = dotOutput;
            dotData.customer = customer;
            dotData.config = configuration;
            var pagefn = doT.template(document.getElementById('plannedArticlesReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);
            return;

        },
        createTableForCompleted: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            for (var i in input) {
                if (i == 'completed' && configuration.plannedDataXpath) {
                    var input1 = JSONPath({ json: input, path: configuration.plannedDataXpath })[0];
                    var totalCount = 0;
                    var tableId = configuration.tableId;
                    reportDoiList[tableId] = {};

                    input1.forEach(function (keyValue) {
                        var tableId = configuration.tableId;
                        var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                        if (typeof (reportDoiList[tableId][firstRowName]) == "undefined") {
                            reportDoiList[tableId][firstRowName] = {};
                        }
                        reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name] = {};
                        var innerInput = JSONPath({ json: keyValue, path: configuration.TodayXpath })[0];
                        innerInput.forEach(function (values, keyCount) {
                            var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                            var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                            reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name][columnHeading] = {};
                            reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name][columnHeading]['doc_count'] = values['doc_count']
                            reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name][columnHeading]['doi'] = [];
                            totalCount += articleCount;
                            JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                            })
                        })
                        reportDoiList[tableId][firstRowName][configuration.apiDetails[0].name]['totalCount'] = totalCount;
                        totalCount = 0;
                    })
                }
                if (i = 'plannedCompleted' && configuration.completedDataXpath) {
                    var input1 = JSONPath({ json: input, path: configuration.completedDataXpath })[0];
                    input1.forEach(function (keyValue, index) {
                        var tableId = configuration.tableId;
                        if (!reportDoiList[tableId]) reportDoiList[tableId] = {};
                        if (keyValue.Completed) {
                            var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                            var plannedHit = JSONPath({ json: input1[index], path: configuration.columnvalueXpath })[0]
                            if (typeof (reportDoiList[tableId][firstRowName]) == "undefined") {
                                reportDoiList[tableId][firstRowName] = {};
                            }
                            reportDoiList[tableId][firstRowName]['plannedcompleted'] = {};
                            totalCount = totalCount1 = totalCount2 = 0
                            plannedHit.forEach(function (values, i) {
                                if (!totalCount) totalCount = 0
                                var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                                var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                                if (!reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading]) reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading] = {};
                                reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading]['doc_count'] = values['doc_count']
                                if (!reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading]['doi']) reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading]['doi'] = [];

                                totalCount += articleCount;
                                JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                    reportDoiList[tableId][firstRowName]['plannedcompleted'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                                })
                                reportDoiList[tableId][firstRowName]['plannedcompleted']['totalCount'] = totalCount;
                            })

                        }

                    })

                }
                if (i = 'unplannedCompleted' && configuration.unplannedCompletedDataXpath) {
                    var input1 = JSONPath({ json: input, path: configuration.unplannedCompletedDataXpath })[0];
                    input1.forEach(function (keyValue, index) {
                        var tableId = configuration.tableId;
                        if (!reportDoiList[tableId]) reportDoiList[tableId] = {};
                        if (keyValue.Completed) {
                            var firstRowName = JSONPath({ json: keyValue, path: configuration.columnHeadingXpath })[0]
                            var plannedHit = JSONPath({ json: input1[index], path: configuration.columnvalueXpath })[0]
                            if (typeof (reportDoiList[tableId][firstRowName]) == "undefined") {
                                reportDoiList[tableId][firstRowName] = {};
                            }
                            reportDoiList[tableId][firstRowName]['unplannedcompleted'] = {};

                            totalCount = totalCount1 = totalCount2 = 0
                            plannedHit.forEach(function (values, i) {
                                if (!totalCount) totalCount = 0
                                var columnHeading = JSONPath({ json: values, path: '$..key' })[0];
                                var articleCount = JSONPath({ json: values, path: '$..doi.buckets.length' })[0];
                                if (!reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading]) reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading] = {};
                                reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading]['doc_count'] = values['doc_count']
                                if (!reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading]['doi']) reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading]['doi'] = [];

                                totalCount += articleCount;
                                JSONPath({ json: values, path: '$..doi.buckets' })[0].forEach(function (doi) {
                                    reportDoiList[tableId][firstRowName]['unplannedcompleted'][columnHeading]['doi'].push(JSONPath({ json: doi, path: '$.key' })[0])
                                })
                                reportDoiList[tableId][firstRowName]['unplannedcompleted']['totalCount'] = totalCount;
                            })

                        }

                    })

                }
            }
            var dotData = {};
            var dotOutput = {};
            dotData.data = reportDoiList[tableId];
            dotData.output = dotOutput;
            dotData.customer = customer;
            dotData.config = configuration;
            var pagefn = doT.template(document.getElementById('completedArticlesReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);
            return;

        },
        createTableWithSpanAttributes: function (input, configuration, customer, roleType, accessLevel) {
            if (!input && !configuration) {
                return;
            }
            input = input.map((articles) => {
                return articles.fields.script_score[0];
            })
            var outputData = {};
            input.forEach(function (data, index) {
                Object.keys(data).forEach(function (stage, stageKey) {
                    if (typeof (data[stage]) == 'object') {
                        var value = data[stage];
                        var fromDate = value.fromDate;
                        var toDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
                        var dateDiff = dateDistance(fromDate, toDate, daysForCalculation.holidays['default']);
                        //dateDiff = Number(value.daysInProd) + ((dateDiff < 0) ? 0 : dateDiff);
                        var stageName = stage;
                        customer = (customer != undefined) ? customer : 'default';
                        customerForTAT = (!daysForCalculation.TAT[customer]) ? 'default' : customer;
                        dateDiff = dateDiff - daysForCalculation.TAT[customerForTAT][stageName];
                        if (outputData[stageName] == undefined) {
                            outputData[stageName] = {};
                            outputData[stageName]['totalSupportArticles'] = 0;
                            outputData[stageName]['totalProductionArticles'] = 0;
                        }
                        if (data[stage].status && data[stage].status == 'in-progress') {
                            if (outputData[stageName]['in-progress'] == undefined) {
                                outputData[stageName]['in-progress'] = {}
                            }
                            if (outputData[stageName]['in-progress'][value.stageType] == undefined) {
                                outputData[stageName]['in-progress'][value.stageType] = {};
                                outputData[stageName]['in-progress'][value.stageType]['doi'] = [];
                            }
                            outputData[stageName]['in-progress'][value.stageType]['doi'].push(data.id);
                            var dateDiffLabel = '';
                            if (dateDiff <= 0) dateDiffLabel = 'No Overdue';
                            else if (dateDiff > 0 && dateDiff < 1) dateDiffLabel = '1Day';
                            else if (dateDiff >= 1 && dateDiff < 2) dateDiffLabel = '2Days';
                            else if (dateDiff >= 2 && dateDiff < 3) dateDiffLabel = '3Days';
                            else dateDiffLabel = '4Days';
                            if (outputData[stageName][dateDiffLabel] == undefined) {
                                outputData[stageName][dateDiffLabel] = {};
                            }
                            if (outputData[stageName][dateDiffLabel][value.stageType] == undefined) {
                                outputData[stageName][dateDiffLabel][value.stageType] = {};
                                outputData[stageName][dateDiffLabel][value.stageType]['doi'] = [];
                            }

                            if (value.stageType == 'Production') outputData[stageName]['totalProductionArticles']++;
                            else outputData[stageName]['totalSupportArticles']++;
                            outputData[stageName][dateDiffLabel][value.stageType]['doi'].push(data.id);
                        }


                        if (value.priority && value.priority == 'on') {
                            if (outputData[stageName]['urgent'] == undefined) {
                                outputData[stageName]['urgent'] = {}
                            }
                            if (outputData[stageName]['urgent'][value.stageType] == undefined) {
                                outputData[stageName]['urgent'][value.stageType] = {};
                                outputData[stageName]['urgent'][value.stageType]['doi'] = [];
                            }
                            outputData[stageName]['urgent'][value.stageType]['doi'].push(data.id);
                        }
                        var plannedTodayDate = moment(value.plannedEndDate+ ' UTC').isSame(moment(), 'days', '[]')

                        if (plannedTodayDate && (!value.comments || (value.comments && !value.comments.match(/.*(Moved to Support|Moved to Hold|Moved to addJob)/)))) {
                            if (outputData[stageName]['planned'] == undefined) {
                                outputData[stageName]['planned'] = {};
                            }
                            if (outputData[stageName]['planned'][value.stageType] == undefined) {
                                outputData[stageName]['planned'][value.stageType] = {};
                                outputData[stageName]['planned'][value.stageType]['doi'] = [];
                            }
                            outputData[stageName]['planned'][value.stageType]['doi'].push(data.id);
                        }
                        var completedTodayDate = moment(value.endDate+ ' UTC').isSame(moment(), 'days', '[]')
                        if (completedTodayDate && (!value.comments || (value.comments && !value.comments.match(/.*(Moved to Support|Moved to Hold|Moved to addJob)/)))) {
                            if (outputData[stageName]['completed'] == undefined) {
                                outputData[stageName]['completed'] = {};
                            }
                            if (outputData[stageName]['completed'][value.stageType] == undefined) {
                                outputData[stageName]['completed'][value.stageType] = {};
                                outputData[stageName]['completed'][value.stageType]['doi'] = [];
                            }
                            outputData[stageName]['completed'][value.stageType]['doi'].push(data.id);
                            if (plannedTodayDate) {

                                if (outputData[stageName]['completed']['sub-completedToday'] == undefined) {
                                    outputData[stageName]['completed']['sub-completedToday'] = {};
                                }
                                if (outputData[stageName]['completed']['sub-completedToday'][value.stageType] == undefined) {
                                    outputData[stageName]['completed']['sub-completedToday'][value.stageType] = {};
                                    outputData[stageName]['completed']['sub-completedToday'][value.stageType]['doi'] = [];
                                }
                                outputData[stageName]['completed']['sub-completedToday'][value.stageType]['doi'].push(data.id);
                            }
                        }


                    }
                })
            })
            var dotData = {};
            var dotOutput = {};
            dotData.data = input;
            dotData.output = outputData;
            dotData.customer = customer;
            dotData.config = configuration;
            var pagefn = doT.template(document.getElementById('todaysStatusReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            reportDoiList[tableId] = dotData.output;
            $('.reportExportExcel').click(false);
            return false;
            /*
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '<h4>' + configuration.tableName + '</h4><table class="table table-striped table-bordered table-hover" style="background-color:white;">';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    var sub = '<tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell, index) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        if (index == 0) {
                            fullTable += '<th scope="col" rowspan="2"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                        } else {
                            fullTable += '<th scope="col" colspan="2"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                        }
                        if (index == 1) {
                            sub += '<th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..Production..doi\',\'In-hand - Production\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">Production</a></th><th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..Support..doi\',\'In-hand - Support\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">Support</a></th>'
                        } else if (index) {
                            ['Production', 'Support'].forEach(function (value) {
                                sub += '<th><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'overallPlanned\',\'bmj\',\'$..overallPlanned..' + eachCell.value + '..' + value + '..doi\',\'' + eachCell.name + ' - ' + value + '\',\'Today\\\'s Status Of Articles In Hand With Exeter\')">' + value + '</a></th>'
                            })
                        }
                    })
                    sub += '</tr></thead>'
                    fullTable += '</tr>' + sub;
                }
            }
            configuration.order.array.forEach(function (keyValue) {
                if (reportDoiList[tableId][keyValue] == undefined) {
                    return false;
                }
                var table = '';
                var firstRowName = keyValue
                fullTable += '<tr><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..doi\',\'' + firstRowName + '\',\'' + tableName + '\')">' + firstRowName + '</a></td>';
                var eachStage = reportDoiList[tableId][keyValue];
                var categoryCount = {};
                configuration.innerOrder.array.forEach(function (values, keyCount) {
                    if (eachStage[values]) {
                        var columnHeading = values;
                        var value = eachStage[values];
                        ['Production', 'Support'].forEach(function (category, categoryIndex) {
                            if (value[category]) {
                                var currentCategory = value[category];
                                var articleCount = currentCategory['doi'].length;
                                if (categoryCount[category] == undefined) {
                                    categoryCount[category] = articleCount;
                                } else {
                                    categoryCount[category] += articleCount;
                                }
                                var title = configuration.headings.rowHeading.filter(obj => {
                                    return obj.value === columnHeading
                                })
                                if (articleCount) {
                                    table += '<td><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '.' + columnHeading + '.' + category + '.doi\',\'' + firstRowName + ' - ' + title[0].name + ' - ' + category + '\',\'' + tableName + '\')">' + articleCount + '</a></td>'
                                } else {
                                    table += '<td> - </td>';
                                }
                            } else {
                                table += '<td> - </td>';
                            }

                        })
                    } else {
                        table += '<td> - </td><td> - </td>';
                    }
                })
                if (configuration.totalCountFront) {
                    table = '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..Production..doi\',\'' + firstRowName + ' - In-hand - Production\',\'' + tableName + '\')">' + ((categoryCount['Production'] == 0 || categoryCount['Production'] == undefined) ? '-' : categoryCount['Production']) + '</a></td><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + firstRowName + '..Support..doi\',\'' + firstRowName + ' - In-hand - Support\',\'' + tableName + '\')">' + ((categoryCount['Support'] == 0 || categoryCount['Support'] == undefined) ? '-' : categoryCount['Support']) + '</a></td>' + table + '</tr>';
                }
                fullTable += table;
            })
            fullTable += '</table>'
            if ($(fullTable).find('tbody').length == 0) {
                fullTable = fullTable.replace(/onclick=".*?"/g, '');
            }
            $('#dashboardReports #' + tableId).html($(fullTable));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            */
        },
        createSupportTable: function (input, configuration, customer, roleType, accessLevel, reportDate) {
            if (!input && !configuration) {
                return;
            }
            input = input.map((articles) => {
                return articles.fields.script_score[0];
            })
            if(!reportDate) {
                reportDate = moment();
            }else{
                reportDate = moment(reportDate)
            }
            var outputData = {};
            input.forEach(function (data, index) {
                Object.keys(data).forEach(function (stage, stageKey) {
                    if (typeof (data[stage]) == 'object') {
                        var value = data[stage];
                        var fromDate = value.fromDate;
                        var toDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
                        var dateDiff = dateDistance(fromDate, toDate, daysForCalculation.holidays['default']);
                        var supportLevel = 'L2';
                        var stageName = stage;
                        var supportType = 'editor';
                        customer = (customer != undefined) ? customer : 'default';
                        customerForTAT = (!daysForCalculation.TAT[customer]) ? 'default' : customer;
                        dateDiff = dateDiff - daysForCalculation.TAT[customerForTAT][stageName];
                        if(stageName != 'Support') value.prevStage = stageName;
                        if (value.comments) {
                            var supportInfo = value.comments.split(',')
                            if (supportInfo.length > 2) {
                                supportType = supportInfo[0];
                                if (!supportType || !supportType.match(/proofing|editor/)) supportType = 'editor'
                                supportLevel = supportInfo[3];
                                if (!supportLevel || !supportLevel.match(/level-2|level-3/)) supportLevel = 'level-2'
                                supportLevel = supportLevel.replace(/level\-/, 'L')
                            }
                        }
                        if (outputData[value.prevStage] == undefined) {
                            outputData[value.prevStage] = {};
                            outputData[value.prevStage]['total'] = 0;
                        }
                        if (data[stage].status && data[stage].status == 'in-progress') {
                            if (outputData[value.prevStage]['in-hand'] == undefined) {
                                outputData[value.prevStage]['in-hand'] = {};
                            }
                            if (outputData[value.prevStage]['in-hand'][supportType] == undefined) {
                                outputData[value.prevStage]['in-hand'][supportType] = {}
                            }
                            if (outputData[value.prevStage]['in-hand'][supportType][supportLevel] == undefined) {
                                outputData[value.prevStage]['in-hand'][supportType][supportLevel] = {};
                                outputData[value.prevStage]['in-hand'][supportType][supportLevel]['doi'] = [];
                            }
                            outputData[value.prevStage]['in-hand'][supportType][supportLevel]['doi'].push(data.id);
                            var dateDiffLabel = '';
                            if (dateDiff <= 0) dateDiffLabel = 'No Delay';
                            else if (dateDiff > 0 && dateDiff < 1) dateDiffLabel = '1Day';
                            else dateDiffLabel = '2Days';
                            if (outputData[value.prevStage][dateDiffLabel] == undefined) {
                                outputData[value.prevStage][dateDiffLabel] = {};
                            }
                            if (outputData[value.prevStage]['overdue'] == undefined) {
                                outputData[value.prevStage]['overdue'] = {};
                            }
                            if (outputData[value.prevStage]['overdue']['in-progress'] == undefined) {
                                outputData[value.prevStage]['overdue']['in-progress'] = {};
                            }
                            if (outputData[value.prevStage][dateDiffLabel][supportType] == undefined) {
                                outputData[value.prevStage][dateDiffLabel][supportType] = {};
                            }
                            if (outputData[value.prevStage]['overdue']['in-progress'][supportType] == undefined) {
                                outputData[value.prevStage]['overdue']['in-progress'][supportType] = {};
                            }
                            if (outputData[value.prevStage][dateDiffLabel][supportType][supportLevel] == undefined) {
                                outputData[value.prevStage][dateDiffLabel][supportType][supportLevel] = {};
                                outputData[value.prevStage][dateDiffLabel][supportType][supportLevel]['doi'] = [];
                            }
                            if (outputData[value.prevStage]['overdue']['in-progress'][supportType][supportLevel] == undefined) {
                                outputData[value.prevStage]['overdue']['in-progress'][supportType][supportLevel] = {};
                                outputData[value.prevStage]['overdue']['in-progress'][supportType][supportLevel]['doi'] = [];
                            }
                            if(dateDiffLabel!='No Delay' && moment(fromDate).isAfter(reportDate) && moment(value.startDate).isBefore(reportDate)){
                                outputData[value.prevStage]['overdue']['in-progress'][supportType][supportLevel]['doi'].push(data.id);
                            }
                            outputData[value.prevStage]['total']++;
                            outputData[value.prevStage][dateDiffLabel][supportType][supportLevel]['doi'].push(data.id);
                        }
                        var plannedTodayDate = moment(value.plannedEndDate + ' UTC').isSame(reportDate, 'days', '[]')
                        var currentStatus = value.status;
                        if (plannedTodayDate) {
                            if (outputData[value.prevStage] == undefined) {
                                outputData[value.prevStage] = {};
                            }
                            if (outputData[value.prevStage]['planned'] == undefined) {
                                outputData[value.prevStage]['planned'] = {};
                            }
                            if (outputData[value.prevStage]['planned'][currentStatus] == undefined) {
                                outputData[value.prevStage]['planned'][currentStatus] = {};
                            }
                            if (outputData[value.prevStage]['planned'][currentStatus][supportType] == undefined) {
                                outputData[value.prevStage]['planned'][currentStatus][supportType] = {};
                            }
                            if (outputData[value.prevStage]['planned'][currentStatus][supportType][supportLevel] == undefined) {
                                outputData[value.prevStage]['planned'][currentStatus][supportType][supportLevel] = {};
                                outputData[value.prevStage]['planned'][currentStatus][supportType][supportLevel]['doi'] = [];
                            }
                            var today = reportDate.format("YYYY-MM-DD") + ' 10:30:00 UTC';
                            if(moment(value.plannedEndDate + ' UTC').isSame(reportDate, 'days', '[]') && moment(value.plannedEndDate + ' UTC').isAfter(moment(today))){
                                if (outputData[value.prevStage] == undefined) {
                                    outputData[value.prevStage] = {};
                                }
                                if (outputData[value.prevStage]['after5'] == undefined) {
                                    outputData[value.prevStage]['after5'] = {};
                                }
                                if (outputData[value.prevStage]['after5'][currentStatus] == undefined) {
                                    outputData[value.prevStage]['after5'][currentStatus] = {};
                                }
                                if (outputData[value.prevStage]['after5'][currentStatus][supportType] == undefined) {
                                    outputData[value.prevStage]['after5'][currentStatus][supportType] = {};
                                }
                                if (outputData[value.prevStage]['after5'][currentStatus][supportType][supportLevel] == undefined) {
                                    outputData[value.prevStage]['after5'][currentStatus][supportType][supportLevel] = {};
                                    outputData[value.prevStage]['after5'][currentStatus][supportType][supportLevel]['doi'] = [];
                                }
                                outputData[value.prevStage]['after5'][currentStatus][supportType][supportLevel]['doi'].push(data.id);
                            }
                            outputData[value.prevStage]['planned'][currentStatus][supportType][supportLevel]['doi'].push(data.id);
                        }
                        var completedTodayDate = moment(value.endDate + ' UTC').isSame(reportDate, 'days', '[]')
                        if (completedTodayDate && value.status == 'completed' && (!value.comments || (value.comments && !value.comments.match(/.*(Moved to Hold|Moved to addJob)/)))) {
                            if (outputData[value.prevStage] == undefined) {
                                outputData[value.prevStage] = {};
                            }
                            if (outputData[value.prevStage]['completed'] == undefined) {
                                outputData[value.prevStage]['completed'] = {};
                            }
                            if (outputData[value.prevStage]['completed'][supportType] == undefined) {
                                outputData[value.prevStage]['completed'][supportType] = {};
                            }
                            if (outputData[value.prevStage]['completed'][supportType][supportLevel] == undefined) {
                                outputData[value.prevStage]['completed'][supportType][supportLevel] = {};
                                outputData[value.prevStage]['completed'][supportType][supportLevel]['doi'] = [];
                            }
                            outputData[value.prevStage]['completed'][supportType][supportLevel]['doi'].push(data.id);
                        }
                    }
                })
            })
            var dotData = {};
            dotData.data = input;
            dotData.output = outputData;
            dotData.customer = Object.keys(userData.kuser.kuroles)[0];
            dotData.config = configuration;
            dotData.configName = 'supportReportConfig';
            var pagefn = doT.template(document.getElementById('todaysSupportReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            reportDoiList[tableId] = dotData.output;
            var dot2Data = {};
            dot2Data.data = input;
            dot2Data.output = outputData;
            dot2Data.configName = 'supportCompletedSummary';
            dot2Data.customer = Object.keys(userData.kuser.kuroles)[0];
            dot2Data.config = eventHandler.tableReportsSummary.supportCompletedSummary[0];
            var pagefn = doT.template(document.getElementById('todaysSupportReport').innerHTML, undefined, undefined);
            var dot2DataOutput = pagefn(dot2Data);
            var tableId = dot2Data.config.tableId;
            $('#dashboardReports #' + tableId).html(dot2DataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $("#supportCompletedSummary tr td").each(function(){
                if($(this).text().match(/\s+0\s+\(\s+0\s+\)/)){
                    $(this).text('-')
                }
            })
            return false;
        },
        columnFilter: function (event) {
            var columnNumber = $(event).closest('th').attr('colNum');
            var minuteCol, hoursCol;
            $(event).closest('table').find('tbody tr td:nth-child(' + columnNumber + ')').each(function () {
                if (!($(this).text().toLowerCase().startsWith(event.value.toLowerCase()))) {
                    $(this).closest('tr').addClass('colFilter' + columnNumber);
                } else {
                    $(this).closest('tr').removeClass('colFilter' + columnNumber);
                }
            })
            $(event).closest('table').find('thead th').each(function (i) {
                if ($(this).text() == "Minutes") {
                    minuteCol = i;
                }
            })

            var filterTime = 0;

            $(event).closest('table').find('tbody tr:not([class^=colFilter])').each(function () {
                if ($(this).find('td').length >= minuteCol) {
                    var time = $($(this).find('td')[minuteCol]).text();
                    filterTime = filterTime + parseInt(time);
                
                }
            })

            var filterCount = $(event).closest('table').find("tbody tr:not([class^=col])").length;
            //var filterTime = $(event).closest('table').find("tbody tr:not([class^=time])").length;
            var showFilter = false;
            $(event).closest('thead').find('input').each(function () {
                if ($(this).val()) {
                    showFilter = true;
                    return false;
                }
            });
            var totalFilterTime = ((filterCount) ? (Math.floor((filterTime / filterCount) / 60)) : '0') + ' h ' + ((filterCount) ? (Math.floor((filterTime / filterCount) % 60)) : '0') + 'm'
            var filterText = '';
            var filterText1 = '';
            if (showFilter) {
                filterText = '  ( Filter Count: ' + filterCount + ' )';
                filterText1 = ' ( Filter time: ' + totalFilterTime + ' )';

            }
            $(event).parentsUntil('h4').find('#filterCount').text(filterText)
            $(event).parentsUntil('h4').find('#filterTime').text(filterText1)

        },
        columnFilterForHold: function (event) {
            var columnNumber = $(event).closest('th').attr('colNum');
            $(event).closest('table').find('tbody tr td:nth-child(' + columnNumber + ')').each(function () {
                if (!($(this).text().startsWith(event.value))) {
                    $(this).closest('tr').addClass('colFilter' + columnNumber);
                } else {
                    $(this).closest('tr').removeClass('colFilter' + columnNumber);
                }
            })
            var filterCount1 = $(event).closest('table').find("tbody tr:not([class^=col])").length;

            var showFilter = false;

            $(event).closest('thead').find('input').each(function () {
                if ($(this).val()) {
                    showFilter = true;
                    return false;
                }
            });
            var filterText = '';
            if (showFilter) {
                filterText = '  ( Filter Count: ' + filterCount1 + ' )';

            }
            $(event).parentsUntil('h4').find('#filterCount1').text(filterText)


        },
        averageTimeUser: function (input, configuration, customer, roleType, accessLevel, fromDate, toDate) {
            if (!input && !configuration) {
                return;
            }

            var preEditorFlagAverage = true, typeEditorFlagAverage = true, typesetterEditorFlagAverage = true;
            var date = new Date(fromDate);

                var userID = userData.kuser.kuemail;
                var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                if (!input.fromDate) fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
                if (!input.toDate) toDate = dateFormat(lastDay, 'yyyy-mm-dd');
            for (i in input) {
                var stageName = input[i].currentStage
                var dataToPost = {
                    customer: customer,
                    urlToPost: "getDailyArticleReport",
                    stageName: stageName,
                    version: "v2.0",
                    mainDataXpath: "$..hits",
                    time_zone: "+05:30",
                    fromDate: fromDate,
                    toDate: toDate
                }
                if (input && (preEditorFlagAverage && stageName == "Pre-editing") || (typeEditorFlagAverage && stageName == "Typesetter Review") || (typesetterEditorFlagAverage && stageName == "Typesetter Check")) {
                    overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(input, stageName, dataToPost);
                    if (stageName == 'Typesetter Check') {
                        typesetterEditorFlagAverage = false;
                    }
                    else if (stageName == 'Typesetter Review') {
                        typeEditorFlagAverage = false
                    }
                    else if (stageName == 'Pre-editing') {
                        preEditorFlagAverage = false
                    }
                
                }
                data = overAllAverage
                if (!report) {
                    var report = {};
                }
                if (!report['user']) {
                    report['user'] = {}
                    report['user'] = overAllAverage["user"];
                }
                if (!userName) {
                    var userName = [];
                }
                if (data["user"]) {
                    Object.keys(data["user"]).forEach(key => {
                        userName.push(key);
                        if (!report['user'][key]) {
                            report['user'][key] = {}
                        }
                        if (!report['user'][key][stageName]) {
                            report['user'][key][stageName] = {}

                        }
                        if (!report['user'][key][stageName]['count']) {
                            report['user'][key][stageName]['count'] = {}
                            report['user'][key][stageName]['count'] = overAllAverage['user'][key]["count"]

                        }
                        if (!report['user'][key][stageName]['time']) {
                            report['user'][key][stageName]['time'] = {}
                            report['user'][key][stageName]['time'] = overAllAverage['user'][key]["time"]

                        }
                        if (!report['user'][key][stageName]['average']) {
                            report['user'][key][stageName]['average'] = {}
                            report['user'][key][stageName]['average'] = overAllAverage['user'][key]["average"]

                        }
                    });
                }
                if (userName) {
                    var uniqueNames = [];
                    $.each(userName, function (index, unique) {
                        if ($.inArray(unique, uniqueNames) === -1) uniqueNames.push(unique);
                    });
                    if (!report['username']) {
                        report['username'] = []

                    }
                    report['username'] = uniqueNames.sort()
                }
                else {
                    userName = ''
                }

            }
            var dotData = {};
            var dotOutput = {};
            dotData.data = report;
            dotData.output = dotOutput;
            dotData.customer = customer;
            dotData.config = configuration;
            var pagefn = doT.template(document.getElementById('userArticlesReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);
            return;
        },
        getDailyUserReport: function (input, configuration, customer, roleType, accessLevel, fromDate, toDate) {
            if (!input && !configuration) {
                return;
            }
            reportDoiList["userDailyReport"] = {}
            var preEditorFlag = true, typeEditorFlag = true, typesetterEditorFlag = true, preEditorFlagAverage = true, typeEditorFlagAverage = true, typesetterEditorFlagAverage = true;
            for (i in input) {
                var stageName = input[i].currentStage
                var userID = userData.kuser.kuemail;

                if ((preEditorFlag && stageName == "Pre-editing") || (typeEditorFlag && stageName == "Typesetter Review") || (typesetterEditorFlag && stageName == "Typesetter Check")) {
                    var dataToPost = {
                        customer: customer,
                        urlToPost: "getUserDailyReport",
                        stageName: "Pre-editing|Typesetter Review|Typesetter Check|Support",
                        version: "v2.0",
                        mainDataXpath: "$..hits",
                        time_zone: "+05:30",
                        fromDate: fromDate,
                        toDate: toDate
                    }
                    lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
                }
                if (stageName) {
                    typesetterEditorFlag = false;
                    typeEditorFlag = false
                
                    preEditorFlag = false
                }
                if ((preEditorFlagAverage && stageName == "Pre-editing") || (typeEditorFlagAverage && stageName == "Typesetter Review") || (typesetterEditorFlagAverage && stageName == "Typesetter Check")) {
                    overAllAverage = eventHandler.mainMenu.toggle.getUserDataReport(lastMonthList.hits, stageName, dataToPost);
                    if (stageName == 'Typesetter Check') {
                        typesetterEditorFlagAverage = false;
                    }
                    else if (stageName == 'Typesetter Review') {
                        typeEditorFlagAverage = false
                    }
                    else if (stageName == 'Pre-editing') {
                        preEditorFlagAverage = false
                    }
                }

            }
            if (!userName) {
                var userName = [];
            }
            Object.keys(reportDoiList["userDailyReport"]).forEach(key => {
                if(key != 'username'){
                Object.keys(reportDoiList["userDailyReport"][key]).forEach(key1 => {
                    userName.push(key1)
                })
            }
            })
            if (userName) {
                var uniqueNames = [];
                $.each(userName, function (index, unique) {
                    if ($.inArray(unique, uniqueNames) === -1) uniqueNames.push(unique);
                });
                if (!reportDoiList["userDailyReport"]['username']) {
                    reportDoiList["userDailyReport"]['username'] = []

                }
                reportDoiList["userDailyReport"]['username'] = uniqueNames.sort()
            }
            else {
                userName = ''
            }


            var dotData = {};
            var dotOutput = {};
            dotData.data = reportDoiList["userDailyReport"];
            dotData.output = dotOutput;
            dotData.customer = customer;
            dotData.config = configuration;
            var pagefn = doT.template(document.getElementById('userDailyReport').innerHTML, undefined, undefined);
            var dotDataOutput = pagefn(dotData);
            var tableId = configuration.tableId;
            $('#dashboardReports #' + tableId).html(dotDataOutput);
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
            $('.reportExportExcel').click(false);
            return;
        },
        daysInProduction: function (input, configuration, customer, roleType, accessLevel) {
            input = input.map(function (eachArray) {
                return JSONPath({ json: eachArray, path: '$..script_score' })[0][0];
            })
            var dotOutput = input.map(function (eachArray) {
                return eachArray.daysInProd;
            })
            var dotOutputDoi = input.map(function (eachArray) {
                return eachArray.doi;
            })
            var tableId = configuration.tableId;
            reportDoiList[tableId] = {};
            reportDoiList[tableId]['doi'] = dotOutputDoi;
            if (dotOutput.length) {
                var overAllDays = dotOutput.reduce((previous, current) => current += previous);
                var path = '$..' + tableId + '..doi';
                var averageDay = Math.ceil(overAllDays / dotOutput.length) + ' days / <span style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'Articles\',\'' + configuration.tableName + '\')">' + dotOutput.length + ' articles</span>';
            } else {
                var averageDay = 'No articles'
            }
            var fullTable = '<h4>' + configuration.tableName + ' : ' + averageDay + '</h4>';
            $('#dashboardReports #' + tableId).html($(fullTable));
        },
        reportCompletedStageCalculation: function (input, configuration, customer, roleType, accessLevel) {
            input = input.map((eachArray) => {
                return JSONPath({ json: eachArray, path: '$..script_score' })[0][0]
            })
            var dotData = {};
            var dotOutput = {};
            var tableId = configuration.tableId;
            dotData.data = input;
            dotData.output = dotOutput;
            dotData.customer = customer;
            var pagefn = doT.template(document.getElementById('reportCompletedStageCalculation').innerHTML, undefined, undefined);
            pagefn(dotData);
            reportDoiList[tableId] = dotData.output;
            var tableName = configuration.tableName.replace(/\'/g, '\\\'');
            var fullTable = '<h4>' + configuration.tableName + '( ' + input.length + ' articles )</h4><table class="table table-striped table-bordered table-hover" style="background-color:white;">';
            if (configuration.headings) {
                if (configuration.headings.default && configuration.headings.rowHeading) {
                    fullTable += '<thead><tr>';
                    configuration.headings.rowHeading.forEach(function (eachCell) {
                        var path = '';
                        if (eachCell.value) {
                            path = '$..' + tableId + '..' + eachCell.value + '..doi'
                        } else {
                            path = '$..' + tableId + '..doi'
                        }
                        fullTable += '<th scope="col"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                    })
                    fullTable += '</tr></thead>'
                }
            }
            configuration.order.array.forEach(function (stageName) {
                var stage = dotData.output[stageName];
                if (stage != undefined) {
                    fullTable += '<tr><td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + stageName + '..doi\',\' ' + stageName + ' \',\'' + tableName + '\')">' + stageName + '</a></td>'
                    configuration.innerOrder.array.forEach(function (innerObj) {
                        var title = configuration.headings.rowHeading.filter(obj => {
                            return obj.value === innerObj
                        })
                        if (stage[innerObj] == undefined) {
                            fullTable += '<td><a style="cursor: unset;">0</a></td>';
                        } else {
                            var length = stage[innerObj]['doi'].length;
                            fullTable += '<td scope="row"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'$..' + tableId + '.' + stageName + '.' + innerObj + '.doi\',\'' + stageName + ' - ' + title[0].name + '\',\'' + tableName + '\')">' + length + '</a></td>';
                        }
                    })
                    fullTable += '</tr>';
                }
            });
            fullTable += '</table>';
            if ($(fullTable).find('tbody').length == 0) {
                fullTable = fullTable.replace(/onclick=".*?"/g, '');
            }
            $('#dashboardReports #' + tableId).html($(fullTable));
            $('#reportContent [data-type="reportTable"]').css('background-color', '');
        },
        articlesReport: function (tableId, customer, xPath, titleName, tableName, fromTotal, toTotal,configuration) {
            if (fromTotal == undefined) {
                $('.la-container').css('display', '');
            }
            // By referring https://stackoverflow.com/questions/49035237/how-to-find-and-match-a-specific-value-in-a-json-object-array
            if (configuration) {
                configuration = eventHandler.tableReportsSummary[configuration].find(o => Object.entries(o).find(([k, value]) => k === 'tableId' && value === tableId) !== undefined);
            } else {
                configuration = eventHandler.tableReportsSummary.tableConfig.find(o => Object.entries(o).find(([k, value]) => k === 'tableId' && value === tableId) !== undefined);
            }
            var param = {};
            articles = [].concat.apply([], JSONPath({ json: reportDoiList, path: xPath }))
            if (fromTotal == undefined && toTotal == undefined) {
                var fromTotal = 0;
                var toTotal = articleLoopCountInTableReport;
            }
            var doiString = articles.slice(fromTotal, toTotal).join(' OR ').replace(/,/g, ' OR ');
            if (doiString == '') {
                $('.la-container').css('display', 'none');
                return;
            }
            $('#modalReport #titleText').text(tableName);
            $('#modalReport #clickedText').text(titleName);
            if (configuration.displayReport) {
                Object.keys(configuration.displayReport).forEach(function (config) {
                    if (config == 'doistring') {
                        param[config] = doiString;
                    } else {
                        param[config] = configuration.displayReport[config];
                    }
                })
            }
            if (configuration && configuration.displayReport && configuration.displayReport.tableType) {
                param[configuration.displayReport.tableType] = true;
            }
            param.customer = customer;
            var fromReportTable = {
                "tableId": tableId,
                "xPath": xPath
            }
            eventHandler.bulkedit.export.exportTable(param, customer, fromTotal, toTotal, articles.length, configuration.displayReport.tableType, configuration.chartTableType, configuration.chartTableTemplateName, '', configuration, '', fromReportTable, titleName, tableName);
        },
        stageHistory: function (input, configuration, customer) {
            //To display loading icon when Export-XLSX button is clicked
            $('.la-container').css('display', '');
            var defaultTable = JSON.parse(JSON.stringify(eventHandler.tableReportsSummary.con));
            var TableAPIConfig = "";
            defaultTable.forEach(function (config) {
                        if(config.tableId == "stageHistory"){
                              configuration = config;
                        }
            });
            configuration.apiDetails.forEach(function (api, apiIndex) {
                for (parameter in api) {
                    if (paramDate[parameter] && !api[parameter].match(/^\{(.*)\}$/)) {
                        api[parameter] = paramDate[parameter];
                    } else if (paramDate['project'] && parameter == "regex") {
                        api['project'] = paramDate['project'].replace(/ OR /g, '|');
                    }
                    if (paramDate[parameter] && api[parameter].match(/^\{(.*)\}$/)) {
                        api[parameter] = api[parameter].replace(/^\{(.*)\}$/, '$1');
                    }
                }
                configuration.tableName = configuration.tableName.replace('{from}', moment(api.fromDate).format('MMM D, YYYY'))
                configuration.tableName = configuration.tableName.replace('{to}', moment(api.toDate).format('MMM D, YYYY'))
            $.ajax({
                type: "POST",
                url: '/api/getarticlelist',
                data: api,
                dataType: 'json',
                success: function (respData) {
                    if (!respData || respData.length == 0) {
                        return;
                    }
                    if (api.mainDataXpath) {
                        var input = JSONPath({ json: respData, path: api.mainDataXpath})[0];
                    }
                    if (configuration.apiDetails.length == 1) {
                        count = 0;
                        //eval(configuration.functionToCall)(input, configuration, param.customer, param.roleType, param.accessLevel);
                    } else {
                        // result.push(input);
                        // var pushObj = { 'stage': api.stageName, 'data': input };
                        for (each of input) {
                            each['currentStage'] = api.stageName;
                            if (api.name) {
                                each['apiName'] = api.name;
                            }
                        }
                        Array.prototype.push.apply(result, input);
                        if (configuration.apiDetails.length - 1 == count) {
                            count = 0;
                            //(configuration.functionToCall)(result, configuration, param.customer, param.roleType, param.accessLevel);
                        }
                    }
                    count++;
                    var tableId = configuration.tableId;
                    reportDoiList[tableId] = {};
                    var tableName = configuration.tableName.replace(/\'/g, '\\\'');
                    exportTableButton = exportTableButton.replace(/{tableID}/gi, tableId);
                    var fullTable = '';
                    if (configuration.headings) {
                        if (configuration.headings.default && configuration.headings.rowHeading) {
                            fullTable += '<thead><tr>';
                            configuration.headings.rowHeading.forEach(function (eachCell) {
                                var path = '';
                                if (eachCell.value) {
                                    path = '$..' + tableId + '..' + eachCell.value + '..doi'
                                } else {
                                    path = '$..' + tableId + '..doi'
                                }
                                fullTable += '<th scope="col"><a style="cursor:pointer;" onclick="eventHandler.tableReportsSummary.articlesReport(\'' + tableId + '\',\'' + customer + '\',\'' + path + '\',\'' + eachCell.name + '\',\'' + tableName + '\')">' + eachCell.name + '</a></th>'
                            })
                            fullTable += '</tr></thead>';
                        }
                    }
                    var stagesToDisplay = ["Pre-editing", "Copyediting", "Typesetter Check", "Publisher Check", "Author Review", "Typesetter Review", "Publisher Review", "Final Deliverables", "Archive"];
                    var sCount = 0;
                    for (let vIndex = 0; vIndex < input.length; vIndex++) {
                        var data = input[vIndex]['_source'];
                        var pubID = data["pubID"];
                        if(data["corresp-info"])
                        {
                        if(data["corresp-info"].name.length)
                        {
                        var author = data["corresp-info"].name[0]['surname'];
                        var email = data["corresp-info"].name[0]['email']
                        var licenseType = data["license-type"]

                        }
                        }
                        if (!data.stage) {
                            continue;
                        }
                        if (data.stage.constructor !== Array) {
                            data.stage = [data.stage];
                        }
                        var overAllStartDay = data.stage[0]['start-date'];
                        if (data.workflowStatus == 'in-progress') {
                            var overAllEndDate = moment().utc().format('YYYY-MM-DD HH:mm:ss')
                        } else {
                            var overAllEndDate = data.stage[data.stage.length - 1]['end-date']
                        }
                        var overAllDelay = moment(overAllEndDate, 'YYYY-MM-DD').diff(moment(overAllStartDay, 'YYYY-MM-DD'), 'days');
                        fullTable += '<tr><td>' + pubID + '</td><td>' + data.doi + '</td><td>' + data.stageName + '</td><td>' + data.workflowStatus + '</td><td>' + data.articleType + '</td><td>' + data.stage[0]['start-date'] + '</td><td>' + ((data.acceptedDate) ? data.acceptedDate : ' - ') + '</td><td>' + overAllDelay + '</td><td>' + licenseType + '</td><td>' + author + '</td><td>' + email + '</td>';
                        var firstStage = true;
                        var startDate;
                        var stageCount = 0;
                        var stageObj = {};
                        for (let aIndex = 0; aIndex < data.stage.length; aIndex++) {
                            var eachStage = data.stage[aIndex];
                            var stageIndex = stagesToDisplay.indexOf(eachStage.name);
                            if (stagesToDisplay.indexOf(eachStage.name) == -1 && firstStage) {
                                startDate = eachStage['start-date'];
                                firstStage = false;
                            } else if (data.stage[aIndex + 1] && data.stage[aIndex + 1].name.match(/Support|Hold/gi)) {
                                if (data.stage.length == aIndex + 2) {
                                    var delayIncludeWeekOff = moment(data.stage[aIndex + 1]['end-date'], 'YYYY-MM-DD').diff(moment(startDate, 'YYYY-MM-DD'), 'days');
                                    stageObj[eachStage.name] = {};
                                    stageObj[eachStage.name]['diff'] = delayIncludeWeekOff;
                                    stageObj[eachStage.name]['startDate'] = startDate;
                                }
                            } else if (stagesToDisplay.indexOf(eachStage.name) > -1) {
                                firstStage = true;
                                if (startDate == '') {
                                    startDate = eachStage['start-date'];
                                   
                                }
                                if (eachStage.status == 'in-progress') {
                                    var endDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
                                } else {
                                    var endDate = eachStage['end-date'];
                                }
                                var delayIncludeWeekOff = moment(endDate, 'YYYY-MM-DD').diff(moment(startDate, 'YYYY-MM-DD'), 'days');
                                if (stageObj[eachStage.name]) {
                                    stageObj[eachStage.name]['diff'] = Number(stageObj[eachStage.name]['diff']) + delayIncludeWeekOff;
                                } else {
                                    stageObj[eachStage.name] = {};
                                    stageObj[eachStage.name]['diff'] = delayIncludeWeekOff;
                                    stageObj[eachStage.name]['startDate'] = startDate;
                                }
                                stageCount++;
                                startDate = '';
                            }

                           
                        }
                        for (e of stagesToDisplay) {
                            if (stageObj[e]) {
                                fullTable += '<td>' + stageObj[e]['startDate'] + '</td><td>' + stageObj[e]['diff'] + '</td>';
                            } else {
                                fullTable += '<td> - </td><td> - </td>';
                            }
                        }
                       

                        sCount++;
                        fullTable += '</tr>'
                    }
                    fullTable += '</tbody></table></div>'
                    fullTable = '<tbody><table><div class="reportWidthHeight"><table class="table table-striped table-bordered table-hover" style="background-color:white;">' + fullTable;

                    if ($(fullTable).find('tbody').length == 0) {
                        fullTable = fullTable.replace(/onclick=".*?"/g, '');
                    }
                    $('#dashboardReports #' + tableId).html($(fullTable));
                    $('#dashboardReports #' + tableId).attr("class", "hidden");
                    $('#reportData').html('');
                    $("#chartReportTable tr").html(' ');
                    $('.la-container').css('display', 'none');

                   eventHandler.bulkedit.export.exportExcel({
                    'tableID': 'stageHistory',
                    'excludeRow': '.collapse',
                    'excludeColumn': '.reportCheckBox'
                })

                  //$('#dashboardReports #' + tableId).html($(fullTable));
                   //$('#reportContent [data-type="reportTable"]').css('background-color', '');

                },
                error: function (respData) {
                    console.log(respData);
                }
                });
            });
        },
        showSupportReport: function (targetNode) {
            var selected = $(targetNode).text()
            $('#supportReport .supportTabToggle').removeClass('active')
            if (selected.match(/card/i)) {
                $(currentTab + ".supportCardView").css('display', '');
                $(currentTab + ".supportReportView").css('display', 'none');
                $(targetNode).addClass('active')
            } else if (selected.match(/report/i)) {
                $(currentTab + ".supportCardView").css('display', 'none');
                $(currentTab + ".supportReportView").css('display', '');
                $(targetNode).addClass('active');
                sDate = moment().format('YYYY-MM-DD');
                var time_zone =  getTimeZone();
                eventHandler.tableReportsSummary.getSupportReport({ customer: 'customer', project: '*', roleType: "production", 'fromDate': sDate, 'toDate': sDate, 'time_zone': time_zone});
                $("#supportfromDatePicker").datepicker({
        					defaultDate: "+1w",
        					changeMonth: true,
        					numberOfMonths: 2,
        					dateFormat: 'd M, yy',
        					language: {
        						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        						dateFormat: 'dd.mm.yyyy'
        					},
        				})
                $("#supportfromDatePicker").data('datepicker').selectDate(new Date())
            }
        },
        getSupportReportByDate: function(targetNode){
            if (targetNode) {
                var sDate = $(targetNode).siblings('input#supportfromDatePicker').val();
                sDate = moment(sDate).format('YYYY-MM-DD');
                var time_zone =  getTimeZone();
                eventHandler.tableReportsSummary.getSupportReport({ customer: 'customer', project: '*', roleType: "production", 'fromDate': sDate, 'toDate': sDate, 'time_zone': time_zone});
            }
        },
        getSupportReport: function (param) {
            $('.la-container').css('display', '');
            paramDate = param;
            if (!param && (!param.customer || !param.project)) {
                return false;
            }
            $('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 50) - $('#footerContainer').height()) + 'px');
            var defaultTable = JSON.parse(JSON.stringify(eventHandler.tableReportsSummary.supportReportConfig));
            defaultTable.forEach(function (config) {
                var configuration = config;
                $(currentTab + '#' + configuration.tableId).show();
                if (configuration.customerToDisplayTable != 'all') {
                    var tableToHide = configuration.customerToDisplayTable.split(',');
                    if (tableToHide.indexOf(param.customer) == -1) {
                        $(currentTab + '#' + configuration.tableId).hide();
                        return true;
                    }
                }
                $(currentTab + '#' + configuration.tableId).html('<div  class="chartBoxDiv" style="min-height:30%;">    </div>')
                $(currentTab + '#reportContent .chartBoxDiv').css('background-color', 'white');
                $(currentTab + '#reportContent .chartBoxDiv').html('<div class="chartLoader"></div>');
                if (configuration.apiDetails) {
                    var result = [];
                    var count = 0;
                    configuration.apiDetails.forEach(function (api, apiIndex) {
                        for (parameter in api) {
                            if (param[parameter] && !api[parameter].match(/^\{(.*)\}$/)) {
                                api[parameter] = param[parameter];
                            } else if (param['project'] && parameter == "regex") {
                                api['project'] = param['project'].replace(/ OR /g, '|');
                            }
                            if (param[parameter] && api[parameter].match(/^\{(.*)\}$/)) {
                                api[parameter] = api[parameter].replace(/^\{(.*)\}$/, '$1');
                            }
                        }
                        $.ajax({
                            type: "POST",
                            url: '/api/getarticlelist',
                            data: api,
                            dataType: 'json',
                            success: function (respData) {
                                if (!respData || respData.length == 0) {
                                    return;
                                }
                                if (api.mainDataXpath) {
                                    var input = JSONPath({ json: respData, path: api.mainDataXpath })[0];
                                }
                                if (configuration.apiDetails.length == 1) {
                                    count = 0;
                                    eval(configuration.functionToCall)(input, configuration, param.customer, param.roleType, param.accessLevel, param.fromDate, param.toDate, param.stageName);
                                } else {
                                    for (each of input) {
                                        each['currentStage'] = api.stageName;
                                        if (api.name) {
                                            each['apiName'] = api.name;
                                        }
                                    }
                                    Array.prototype.push.apply(result, input);
                                    if (configuration.apiDetails.length - 1 == count) {
                                        count = 0;
                                        eval(configuration.functionToCall)(result, configuration, param.customer, param.roleType, param.accessLevel, param.fromDate, param.toDate);
                                    }
                                }
                                count++;
                                $('.la-container').css('display', 'none');
                            },
                            error: function (respData) {
                                $('.la-container').css('display', 'none');
                                console.log(respData);
                            }
                        });
                    })
                }
        });
        },
        tableConfig: [
            {
                'apiDetails': [{
                    'urlToPost': 'getReportDaysInProduction',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'stage': 'Archive',
                    'workflowStatus': 'completed',
                    'date': 'stageDue'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'daysInProd',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.daysInProduction',
                'tableName': 'Average Days in Production for Published Articles ({from} - {to}) ',
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getPlannedArticles',
                    'workflowStatus': '*',
                    'workflowStatus1': 'completed',
                    'plannedenddate': 'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    "time_zone_name": "time_zone_name",
                    'mainDataXpath': '$..stage.filtered',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'name': 'planned'
                }
                ],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'planned',
                'plannedDataXpath': '$..planned.stages.buckets',
                'completedDataXpath': '$..plannedCompleted.stages.buckets',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.createTable',
                'tableName': 'Planned Articles Summary',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Stage', 'rowspan': 3 }, { 'name': '11h', 'colspan': 4 }, { 'name': '13h', 'colspan': 4 }, { 'name': '15h', 'colspan': 4 }, { 'name': '17h', 'colspan': 4 }, { 'name': '19h', 'colspan': 4 }, { 'name': '24h', 'colspan': 4 }, { 'name': 'total', 'colspan': 4 }],

                        [{ 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }, { 'name': 'Planned', 'rowspan': 2 }, { 'name': 'Completed', 'colspan': 3 }],

                        [{ 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }, { 'name': 'A' }, { 'name': 'O' }, { 'name': 'D' }]
                    ]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..plannedDate.buckets',
                'endvalueXpath': '$..endDate.buckets',
                'TodayXpath': '$.Today.year.buckets',
                'YesterdayXpath': '$.Yesterday.year.buckets',
                'tomorrowXpath': '$.tomorrow.year.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "totalArticleSubOrder": {
                    "array": ['actualplanned', 'ahead', 'on-time', 'delay']
                },
                "time": {
                    "array": ['11h', '13h', '15h', '17h', '19h', '24h']
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getCompleted',
                    'workflowStatus': 'completed',
                    'sdate': 'stage.end-date',
                    'date': 'stage.planned-end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    "time_zone_name": "time_zone_name",
                    'mainDataXpath': '$..stage.filtered',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'name': 'actual'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'completed',
                'plannedDataXpath': '$..planned.buckets',
                'TodayXpath': '$..year.buckets',
                'completedDataXpath': '$..plannedCompleted.filtered.stages.buckets',
                'unplannedCompletedDataXpath':'$..unplannedCompleted.filtered.stages.buckets',
                'percentageXpath': '$.doc_count',
                'functionToCall': 'eventHandler.tableReportsSummary.createTableForCompleted',
                'tableName': 'Completed Articles Summary',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Stage', 'rowspan': 2 }, { 'name': '11h', 'rowspan': 2 }, { 'name': '13h', 'rowspan': 2 }, { 'name': '15h', 'rowspan': 2 }, { 'name': '17h', 'rowspan': 2 }, { 'name': '19h', 'rowspan': 2 }, { 'name': '24h', 'rowspan': 2 }, { 'name': 'Total Completed', 'colspan': 3 }],
                    ]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..plannedDate.buckets',
                'endvalueXpath': '$..doi.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "totalArticleSubOrder": {
                    "array": ['actual']
                },
                "totalArticleSubCompleted":{
                    "array": ['plannedcompleted']
                },
                "totalArticleSubUnplannedCompleted":{
                    "array": ['unplannedcompleted']
                },
                "time": {
                    "array": ['11h', '13h', '15h', '17h', '19h', '24h']
                }
            },

            {
                'apiDetails': [{
                    'urlToPost': 'getCompletedArticlesStageDuration',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d',
                    'stage': 'Archive',
                    'workflowStatus': 'completed',
                    'date': 'stageDue'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'publishedArticleDurationStage',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.reportCompletedStageCalculation',
                'tableName': 'Delay in Each Stage for Published Articles ',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Stage', 'value': '' }, { 'name': 'No delay', 'value': 'No Overdue' }, { 'name': '1 day(s) delay at stage', 'value': '1Day' }, { 'name': '2 day(s) delay at stage', 'value': '2Days' }, { 'name': '3 day(s) delay at stage', 'value': '3Days' }, { 'name': '>=4 day(s) delay at stage', 'value': '4Days' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days', '3Days', '4Days'],
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getInHandArticlesSeggregation',
                    'workflowStatus': 'in-progress',
                    'date': 'sla-end-date',
                    'customer': 'customer',
                    'project': 'project',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    // 'mainDataXpath': '$..project.buckets',
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'overallPlanned',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.createTableWithSpanAttributes',
                'tableName': 'Today Status Of Articles In Hand With Exeter',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Stage', 'rowspan': 2 }, { 'name': 'Production In-hand', 'colspan': 3 }, { 'name': 'Support In-hand', 'colspan': 3 }, { 'name': 'No delay', 'colspan': 2 }, { 'name': '1 day(s) delay at stage', 'colspan': 2 }, { 'name': '2 day(s) delay at stage', 'colspan': 2 }, { 'name': '3 day(s) delay at stage', 'colspan': 2 }, { 'name': '>=4 day(s) delay at stage', 'colspan': 2 }],

                        [{ 'name': 'Total' }, { 'name': 'Urgent' }, { 'name': 'Planned' }, { 'name': 'Total' }, { 'name': 'Urgent' }, { 'name': 'Planned' },  { 'name': 'Production' }, { 'name': 'Support' }, { 'name': 'Production' }, { 'name': 'Support' }, { 'name': 'Production' }, { 'name': 'Support' }, { 'name': 'Production' }, { 'name': 'Support' }, { 'name': 'Production' }, { 'name': 'Support' }]
                    ]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days', '3Days', '4Days'],
                },
                "totalArticleSubOrder": {
                    "array": ['in-progress','urgent', 'planned']
                }
            },
            
            {
                'apiDetails': [{
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Pre-editing',
                    'version': 'v2.0',
                    'plannedenddate':'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    'mainDataXpath': '$..hits',
                    "time_zone" : "time_zone",
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Copyediting',
                    'version': 'v2.0',
                    'plannedenddate':'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    'mainDataXpath': '$..hits',
                    "time_zone" : "time_zone",
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Copyediting Check',
                    'version': 'v2.0',
                    'plannedenddate':'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },
                {
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Check',
                    'version': 'v2.0',
                    'plannedenddate': 'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },{
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Review',
                    'version': 'v2.0',
                    'plannedenddate': 'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },{
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Validation Check',
                    'version': 'v2.0',
                    'plannedenddate': 'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                },{
                    'urlToPost': 'getArticleReport',
                    'customer': 'customer',
                    'stageName': 'Final Deliverables',
                    'version': 'v2.0',
                    'plannedenddate': 'stage.planned-end-date',
                    'enddate': 'stage.end-date',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/d',
                    'toDate': 'now/d'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'tableId': 'dailyReport',
                'functionToCall': 'eventHandler.tableReportsSummary.getDailyReport',
                'tableName': 'Articles Report Summary',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Planned Date', 'value': '' }, { 'name': 'Completed Date', 'value': '' }, { 'name': 'Due Date', 'value': '' }, { 'name': 'DOI', 'value': '' }, { 'name': 'Manuscript Type', 'value': '' }, { 'name': 'Stage', 'value': '' }, { 'name': 'Status', 'value': '' },{ 'name': 'Minutes', 'value': '' }, { 'name': 'Word Count', 'value': '' }, { 'name': 'Word Count Without Ref', 'value': '' }, { 'name': 'Proof Count', 'value': '' }, { 'name': 'Signed-off By / Assignee', 'value': '' }],
                }
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getDailyArticleReport',
                    'customer': 'customer',
                    'stageName': 'Pre-editing',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    "time_zone": "time_zone",
                    'fromDate': 'now/M-1',
                    'toDate': 'now/M-1'
                },
                {
                    'urlToPost': 'getDailyArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Check',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/M-1',
                    'toDate': 'now/M-1'
                },
                {
                    'urlToPost': 'getDailyArticleReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Review',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now/M-1',
                    'toDate': 'now/M-1'
                }],
                'tableId': 'userAssignment',
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'tableId': 'userReport',
                'functionToCall': 'eventHandler.tableReportsSummary.averageTimeUser',
                'tableName': 'User Report Summary For Previous Month',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Employee Name', 'rowspan': 2 }, { 'name': 'Pre-editing', 'colspan': 3 }, { 'name': 'Typesetter-check', 'colspan': 3 }, { 'name': 'Typesetter-Review', 'colspan': 3 }],

                        [{ 'name': 'Completed Articles' }, { 'name': 'Overall Time (M)' }, { 'name': 'Average Time' }, { 'name': 'Completed Articles' }, { 'name': 'Overall Time (M)' }, { 'name': 'Average Time' }, { 'name': 'Completed Articles' }, { 'name': 'Overall Time (M)' }, { 'name': 'Average Time' }]
                    ]

                },
                "stages": {
                    "array": ['Pre-editing', 'Typesetter Check', 'Typesetter Review']
                },
                "totalArticleSubOrder": {
                    "array": ['count', 'time', 'average']
                },
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getUserDailyReport',
                    'customer': 'customer',
                    'stageName': 'Pre-editing',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    "time_zone": "time_zone",
                    'fromDate': 'now',
                    'toDate': 'now'
                },
                {
                    'urlToPost': 'getUserDailyReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Check',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now',
                    'toDate': 'now'
                },
                {
                    'urlToPost': 'getUserDailyReport',
                    'customer': 'customer',
                    'stageName': 'Typesetter Review',
                    'version': 'v2.0',
                    "time_zone": "time_zone",
                    'mainDataXpath': '$..hits',
                    'fromDate': 'now',
                    'toDate': 'now'
                }],
                'tableId': 'userAssignment',
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'tableId': 'userDailyReport',
                'functionToCall': 'eventHandler.tableReportsSummary.getDailyUserReport',
                'tableName': 'User Daily Report',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Employee Name', 'rowspan': 2 }, { 'name': 'Pre-editing', 'colspan': 3 }, { 'name': 'Typesetter-check', 'colspan': 3 }, { 'name': 'Typesetter-Review', 'colspan': 3 }],

                        [{ 'name': 'Planned' }, { 'name': 'Completed' }, { 'name': 'Support' }, { 'name': 'Planned' }, { 'name': 'Completed' }, { 'name': 'Support' }, { 'name': 'Planned' }, { 'name': 'Completed' }, { 'name': 'Support' }]
                    ]

                },
                "stages": {
                    "array": ['Pre-editing', 'Typesetter Check', 'Typesetter Review']
                },
                "totalArticleSubOrder": {
                    "array": ['Planned', 'completed', 'support']
                },
            },
            {
                'apiDetails': [{
                    'urlToPost': 'getHold',
                    'customer': 'customer',
                    'workflowStatus': 'in-progress',
                    'stageName': 'Hold',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits',
                    'from': '0',
                    'size': '500'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': true,
                'totalCountFront': false,
                'totalCountBack': true,
                'tableId': 'holdReport',
                'functionToCall': 'eventHandler.tableReportsSummary.getHoldReport',
                'tableName': 'Hold Report Summary',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'Start Date', 'value': '' }, { 'name': 'DOI', 'value': '' }, { 'name': 'Days In Hold', 'value': '' }, { 'name': 'Comments', 'value': '' }, { 'name': 'HeldStage', 'value': '' }, { 'name': 'Hold by', 'value': '' }],
                }
            }
        ],
        con : [
            {
                'apiDetails': [{
                    'urlToPost': 'getArticlesList',
                    'workflowStatus': 'in-progress OR completed',
                    'customer': 'mbs',
                    'project': '*',
                    'version': 'v2.0',
                    'excludeStageArticles': 'null',
                    'mainDataXpath': '$..hits',
                    'from': '0',
                    'size': '1000'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '1000',
                    'tableType': 'exportTable'
                },
                'tableId': 'stageHistory',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.stageHistory',
                'tableName': 'Stage History',
                'headings': {
                    'default': true,
                    'rowHeading': [{ 'name': 'DOI', 'value': '' }, { 'name': 'Article number', 'value': '' }, { 'name': 'Article Stage', 'value': '' }, { 'name': 'Article Status', 'value': '' }, { 'name': 'Article Type', 'value': '' }, { 'name': 'Exported Date', 'value': '1Day' }, { 'name': 'Accepted Date', 'value': '2Days' }, { 'name': 'Total Duration (days)', 'value': '3Days' }, { 'name': 'License Type', 'value': '' }, { 'name': 'Corresponding Author', 'value': '' }, { 'name': 'Corresponding Author Email', 'value': '' },
                    { 'name': 'Pre-editing', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }, { 'name': 'Copy-editing', 'value': '' }, { 'name': 'TAT (days)', 'value': '' },
                    { 'name': 'Typesetter Check', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }, { 'name': 'Publisher Check', 'value': '' }, { 'name': 'TAT (days)', 'value': '' },
                    { 'name': 'Author Review', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }, { 'name': 'Typesetter Review', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }, { 'name': 'Publisher Review', 'value': '' }, { 'name': 'TAT (days)', 'value': '' },
                    { 'name': 'Final Deliverables', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }, { 'name': 'Archive', 'value': '' }, { 'name': 'TAT (days)', 'value': '' }]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Waiting for CE', 'Copyediting', 'Copyediting Check', 'Typesetter Check', 'Editor Check', 'Associate Editor Check', 'Proofreading', 'Publisher Check', 'Author Review', 'Editor Review', 'Typesetter Review', 'Author Revision', 'Publisher Review', 'Final Deliverables', 'Banked', 'Upload for Online First', 'Archive', 'Resupply', 'Hold', 'Support', 'Proofreading', 'Issue Makeup', 'Validation Check'],
                },
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days', '3Days', '4Days'],
                }
            }

        ],
        supportReportConfig: [
            {
                'apiDetails': [{
                    'urlToPost': 'getSupportReport',
                    'workflowStatus': 'in-progress',
                    'date': 'sla-end-date',
                    'customer': '*',
                    'project': '*',
                    'version': 'v2.0',
                    'mainDataXpath': '$..hits.hits',
                    'fromDate': 'fromDate',
                    'toDate': 'toDate',
                    'time_zone': 'time_zone'
                }],
                'customerToDisplayTable': 'all',
                'chartTableType': 'default',
                'chartTableTemplateName': 'chartReportTemplate',
                'percentage': false,
                'totalCountFront': true,
                'totalCountBack': false,
                'displayReport': {
                    'url': '/api/getarticlelist',
                    'tableType': true,
                    'doistring': 'doiString',
                    'urlToPost': 'getDoiArticles',
                    'from': '0',
                    'size': '500',
                    'tableType': 'exportTable'
                },
                'tableId': 'supportOverallPlanned',
                'percentageXpath': '$.doc_count',
                'insertValidation': true,
                'functionToCall': 'eventHandler.tableReportsSummary.createSupportTable',
                'tableName': 'Today Status Of Support Articles In Hand With Exeter',
                'headings': {
                    'default': true,
                    'rowHeading': [
                        [{ 'name': 'Stage', 'rowspan': 3 }, { 'name': 'Editor', 'colspan': 9 }, { 'name': 'Proofing', 'colspan': 9 }],
                        [{ 'name': 'In-hand', 'colspan': 3 }, { 'name': 'Nodelay', 'colspan': 2 }, { 'name': '1 day Delay', 'colspan': 2 }, { 'name': '>=2 days delay', 'colspan': 2 }, { 'name': 'In-hand', 'colspan': 3 }, { 'name': 'Nodelay', 'colspan': 2 }, { 'name': '1 day Delay', 'colspan': 2 }, { 'name': '>=2 days delay', 'colspan': 2 }],
                        [{ 'name': 'Total' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'Total' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }]
        ]
                },
                'columnHeadingXpath': '$..key',
                'columnvalueXpath': '$..days.buckets',
                "order": {
                    "stageVariable": "key",
                    "array": ['addJob', 'File download', 'Convert Files', 'Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Final Deliverables', 'Upload for Online First', 'Validation Check'],
                },
                "colArray": ["$..in-hand.editor..doi", "$..in-hand.editor.L2.doi", "$..in-hand.editor.L3.doi", "$..No Delay.editor.L2.doi", "$..No Delay.editor.L3.doi", "$..1Day.editor.L2.doi", "$..1Day.editor.L3.doi", "$..2Days.editor.L2.doi", "$..2Days.editor.L3.doi", "$..in-hand.proofing..doi", "$..in-hand.proofing.L2.doi", "$..in-hand.proofing.L3.doi", "$..No Delay.proofing.L2.doi", "$..No Delay.proofing.L3.doi", "$..1Day.proofing.L2.doi", "$..1Day.proofing.L3.doi", "$..2Days.proofing.L2.doi", "$..2Days.proofing.L3.doi"],
                "innerOrder": {
                    "stageVariable": "key",
                    "array": ['No Overdue', '1Day', '2Days'],
                },
                "totalArticleSubOrder": {
                    "array": ['in-progress', 'L1', 'L2']
                }
            }
        ],
        supportCompletedSummary: [{
            'customerToDisplayTable': 'all',
            'chartTableType': 'default',
            'chartTableTemplateName': 'chartReportTemplate',
            'percentage': false,
            'totalCountFront': true,
            'totalCountBack': false,
            'displayReport': {
                'url': '/api/getarticlelist',
                'tableType': true,
                'doistring': 'doiString',
                'urlToPost': 'getDoiArticles',
                'from': '0',
                'size': '500',
                'tableType': 'exportTable'
            },
            'tableId': 'supportCompletedSummary',
            'percentageXpath': '$.doc_count',
            'insertValidation': true,
            'functionToCall': 'eventHandler.tableReportsSummary.createSupportTable',
            'tableName': 'Completed Summary of Support Articles',
            'headings': {
                'default': true,
                'rowHeading': [
                    [{ 'name': 'Stage', 'rowspan': 3 }, { 'name': 'Editor Summary', 'colspan': 6 }, { 'name': 'Proofing Summary', 'colspan': 6 }],
                    [{ 'name': 'Overdue', 'colspan': 2 }, { 'name': 'Planned', 'colspan': 2 }, { 'name': 'After 5pm', 'colspan': 2 }, { 'name': 'Overdue', 'colspan': 2 }, { 'name': 'Planned', 'colspan': 2 }, { 'name': 'After 5pm', 'colspan': 2 }],
                    [{ 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }, { 'name': 'L2' }, { 'name': 'L3' }]
                ]
            },
            'columnHeadingXpath': '$..key',
            'columnvalueXpath': '$..days.buckets',
            "order": {
                "stageVariable": "key",
                "array": [' addJob', 'File download', 'Convert Files', 'Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Final Deliverables', 'Upload for Online First', 'Validation Check'],
            },
            "colArray": ["$..overdue.completed.editor.L2.doi,$..overdue..editor.L2.doi", "$..overdue.completed.editor.L3.doi,$..overdue..editor.L3.doi", "$..planned.completed.editor.L2.doi,$..planned..editor.L2.doi", "$..planned.completed.editor.L3.doi,$..planned..editor.L3.doi", "$..after5.completed.editor.L2.doi,$..after5..editor.L2.doi", "$..after5.completed.editor.L3.doi,$..after5..editor.L3.doi","$..overdue.completed.proofing.L2.doi,$..overdue..proofing.L2.doi", "$..overdue.completed.proofing.L3.doi,$..overdue..proofing.L3.doi", "$..planned.completed.proofing.L2.doi,$..planned..proofing.L2.doi", "$..planned.completed.proofing.L3.doi,$..planned..proofing.L3.doi", "$..after5.completed.proofing.L2.doi,$..after5..proofing.L2.doi", "$..after5.completed.proofing.L3.doi,$..after5..proofing.L3.doi"]
        }]
    }
})(eventHandler || {});
