/**
 * eventHandler - this javascript holds all the functions required for Reference handling
 *				 so that the functionalities can be turned on and off by just calling the required functions
 *				 Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
 */
var eventHandler = function () {
	//default settings
	var settings = {};
	var proofInterval, proofStartTime;

	settings.subscriptions = {};
	settings.subscribers = ['menu', 'query', 'components', 'welcome'];
	//add or override the initial setting
	var initialize = function (params) {
		function extend(obj, ext) {
			if (obj === undefined) {
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

   

    var getSettings = function() {
        var newSettings = {};
        for (var prop in settings) {
            newSettings[prop] = settings[prop];
        }
        return newSettings;
    };

    return {
        init: initialize,
        getSettings: getSettings,
        settings: settings
    };
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;
	var proofInterval = 120000; //120secs
	var proofStage = '', proofStartTime = new Date().getTime(); 

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel', 
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);
			
			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		domInsertCBFunc: function(event) {
			if (/ins\b/.test(event.target.className) || /del\b/.test(event.target.className)){
				if (!$('#changesDivNode').length){
					$('#navigationContentDiv container-for-navigation').append('<div id="changesDivNode" class="sub-menu-divs"></div>');
				}
				var changeID = event.target.getAttribute('data-cid');
				var userName = event.target.getAttribute('data-username');
				var objDate = new Date(new Date(parseInt(event.target.getAttribute('data-time'))));
				var locale = "en-us",
				month = objDate.toLocaleString(locale, { month: "short" });
				var objDate = objDate.getDate() + ' ' + month + ' ' + (objDate.getYear() + 1900) + ' (' + objDate.getHours() + ':' + objDate.getMinutes() + ')'; 
				var changeHTML = '<span class="';
				if (/ins\b/.test(event.target.className)){
					changeHTML = changeHTML + 'insertion" data-rid="' + changeID + '"><b>' + userName + '</b> on ' + objDate + '<br/><b>Inserted: </b><span class="changeData">&nbsp;</span></span>' 
				}
				else if (/del\b/.test(event.target.className)){
					changeHTML = changeHTML + 'deletion" data-rid="' + changeID + '"><b>' + userName + '</b> on ' + objDate + '<br/><b>Deleted: </b><span class="changeData">&nbsp;</span></span>' 
				}
				var changeNode = $('#navigationContentDiv [data-rid="' + changeID + '"]');
				if (changeNode.length > 0){
					changeNode.replaceWith(changeHTML);
				}else{
					$('#changesDivNode').append(changeHTML);
				}
			}
		},
		eventCallbackFunction: function(event) {
			if (typeof kriya == "undefined") return false;
			// event.target is the clicked element!
			kriya.evt = event;
			if (typeof(event.pageX) != "undefined"){
				kriya.pageXaxis = event.pageX;
				kriya.pageYaxis = event.pageY;
			}
			
			var highlighter = false;
			//check if the cuurent click position has a underlay behind it
			var elements = elementsFromPoint(event.pageX, event.pageY);
			$(elements).each(function(){
				//when we close author query popup if behind there any components(Author group or keyword group) at that time contentEvt() is triggering but no need to trigger contentEvt() because of that code is breaking in contentEvt()
				//if ($(this).hasClass('highlight') && !$(this).hasClass('textSelection')){
				if ($(this).hasClass('highlight') && !$(this).hasClass('textSelection') && !$(this).hasClass('queryText')){
					highlighter = true;
				}
			});
			if (! highlighter){
				elements = $(event.target);
			}

			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}
								
								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
			eventHandler.settings.subscriptions['citation_reorder'] = postal.subscribe({
				channel: 'citejs',
				topic: "reorder",
				callback: function(data, envelope) {
					if((data.element && data.element.length > 0 && data.type) || data.citeClassArray){
						var renumberStatus = [];
						var renumberStatusString = "";

						if(!data.citeClassArray){
							$('[data-cite-type]').removeAttr('data-cite-type');
							$(data.element).attr('data-cite-type', data.type);
							var classNameArray = [];
							var trackNode = $('[data-cite-type]').closest('.ins, .del, [data-track="ins"], [data-track="del"]');
							$(data.element).each(function(){
								var ridString = $(this).attr('data-citation-string');
								var className = $(this).attr('class');
								className     = className.replace(/^\s+|\s+$/g, '').split(' ')[0];
								if (!ridString){
									return;
								}
								ridString = ridString.replace(/[\.\,](?![0-9])|[\:\;]+/g, '');
								ridString = ridString.replace(/^\s+|\s+$/g, '').split('-');
								var citeConfig = citeJS.floats.getConfig(ridString[0]);
								if(citeConfig && citeConfig.reorder != false && ($(this).closest('.'+ citeConfig.reorder).length > 0 || !citeConfig.reorder) && classNameArray.indexOf(className) < 0){
									classNameArray.push(className);
								}
							});
						}else{
							classNameArray = data.citeClassArray;
						}

						$.each(classNameArray, function(i, classVal){
							if(classVal == 'jrnlSupplRef'){
								var suppIdArray = [];
								$(kriya.config.containerElm + ' .' + classVal).each(function(){
									if($(this).closest('.del, [data-track="del"]').length > 0){
										return;
									}
									var cId = $(this).attr('data-citation-string');
									if(!cId.match(/\-/g)){
										cId = citeJS.general.getPrefix(cId);
										if(suppIdArray.indexOf(cId) < 0){
											suppIdArray.push(cId);
											citeSeqObject  = citeJS.floats.checkSequence(classVal, cId);
											var reorderObj = citeJS.floats.reorder();
											if(renumberStatus.length == 0){
												renumberStatus = renumberStatus.concat(reorderObj);
											}else{
												for(var r = 0;r<reorderObj.length;r++){
													renumberStatus[r] = renumberStatus[r].concat(reorderObj[r]);
												}
											}
											
										}
									}
								});
							}else{
								citeSeqObject  = citeJS.floats.checkSequence(classVal);
								renumberStatus = citeJS.floats.reorder();
							}

							

							// Add the changed citations in the stack to save the content
							if(renumberStatus && renumberStatus[0] && renumberStatus[0].length > 0){
								for (var c = 0; c < renumberStatus[0].length ; c++) {
									var citeElement = $(kriya.config.containerElm + ' #'+renumberStatus[0][c]);
									citeElement = (citeElement.length == 0)?$('#navContainer #'+renumberStatus[0][c]):citeElement;
									kriyaEditor.settings.undoStack.push(citeElement);
								}
							}
							//Add the changed floats in stack to save the content
							if(renumberStatus && renumberStatus[1] && renumberStatus[1].length > 0){
								for (var f = 0; f < renumberStatus[1].length ; f++) {
									var citeElement = $(kriya.config.containerElm + ' #'+renumberStatus[1][f]);
									citeElement = (citeElement.length == 0)?$('#navContainer #'+ renumberStatus[1][f]):citeElement;
									kriyaEditor.settings.undoStack.push(citeElement);
								}
							}
							renumberStatusString += renumberStatus[2];
							//Remove the data-cite-type attribute for the reordered class
							if($('.'+classVal+'[data-cite-type]').length > 0){
								$('.'+classVal+'[data-cite-type]').each(function(){
									$(this).removeAttr('data-cite-type');
									kriyaEditor.settings.undoStack.push(this);
								});
							}
						});
						

						if(trackNode && trackNode.length > 0){
							if(renumberStatusString && renumberStatusString != ""){
								trackNode.attr('data-track-detail', renumberStatusString);
							}
							//$(trackNode).attr('data-callback','kriya.general.rejectReorder');
							//eventHandler.menu.observer.nodeChange({'node':trackNode});
							kriyaEditor.settings.undoStack.push(trackNode);
						}
						//Update the status after citation reorder
						citeJS.general.updateStatus();

					}else{
						console.log('Data missing reorder failed.');
					}
				}
			});
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.welcome = {
		begin: {
			/**
			 * Function to open the authendication window
			 * if selected author has loop id else show warning msg
			 */
			authenticateBegin: function(param, targetNode){
				var selectedAuthor = $('.authorList').val();
				var authType = $('#welcomeContainer').attr('data-auth-type');
				if(selectedAuthor){
					if(authType){
						var selectedData = $('.authorList').find(':selected').attr('data-' + authType);
						if(selectedData || $('#welcomeContainer').attr('data-if-not-data-save') == "true"){
							kriya.loopWin  = window.open('/loop_authenticate', 'loop_authenticate', 'height=500, width=1000');
						}else{
							kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Manuscript doesn\'t have ' + authType + ' id' ,icon: 'icon-warning2'});
						}
					}
					/*if(authType == "loop"){
						var selectedLoop = $('.authorList').find(':selected').attr('data-loop');
						if(selectedLoop){
							kriya.loopWin  = window.open('/loop_authenticate', 'loop_authenticate', 'height=500, width=1000');
						}else{
							kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Manuscript doesn\'t have loop id' ,icon: 'icon-warning2'});
						}
					}else if(authType == "orcid"){
						var selectedOrcid = $('.authorList').find(':selected').attr('data-orcid');
						if(selectedOrcid){
							kriya.loopWin  = window.open('/loop_authenticate', 'loop_authenticate', 'height=500, width=1000');
						}else{
							kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Manuscript doesn\'t have orcid id' ,icon: 'icon-warning2'});
						}						
					}*/
				}else{
					kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Please select the user to login',icon: 'icon-warning2'});
				}
			},
			/**
			 * Function to authenticate and show article to user
			 * if response value match with manuscript loop then show article content else error waring msg
			 */
			authenticate: function(res){
				if(typeof(res) == "string"){
					res = JSON.parse(res);
				}
				var authType = $('#welcomeContainer').attr('data-auth-type');
				var resVariable = $('#welcomeContainer').attr('data-res-variable');
				var selectedAuthor = $('.authorList').val();
				var selectedEmail = $('.authorList').find(':selected').attr('data-email');

				//If author doesn't have email then consider the OAuth Id as email
				if(!selectedEmail){
					selectedEmail = res[resVariable];
				}
				
				var parameters = {
					'customer' : kriya.config.content.customer,
					'project'  : kriya.config.content.project,
					'doi'      : kriya.config.content.doi,
					'stage'    : kriya.config.content.stage,
					'userEmail': selectedEmail,
					'userName' : selectedAuthor,
					'currStageName' : kriya.config.content.stageFullName
				};
				parameters.outhData = res[resVariable];

				//Get the author node using id
				var selectedLoop = $('.authorList').find(':selected').attr('data-' + authType);
				var authorID = $('.authorList').find(':selected').attr('data-auth-id');
				var authNode = null;
				if(authorID){
					authNode = $(kriya.config.containerElm + ' #'+authorID);
				}

				//Save the Oauth ID in author if data-if-not-data-save is true
				if(!selectedLoop && $('#welcomeContainer').attr('data-if-not-data-save') == "true"  && $('#welcomeContainer').attr('data-tag-class') && authNode && $(authNode).length > 0){
					var prefix = $('#welcomeContainer').attr('data-save-prefix');
					prefix = (prefix)?prefix:'';
					selectedLoop = parameters.outhData;
					var dataNode = $('<span class="' + $('#welcomeContainer').attr('data-tag-class') + '">' + prefix + parameters.outhData + '</span>');
					authNode.append(dataNode);
					kriyaEditor.init.save(authNode);
				}

				parameters.msData = selectedLoop;

				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('verifyauthentication', parameters, function(res){
					if(res && res.verified == true){

						/**
						 * Added by jagan 
						 * set the user details in tracker and kriyaEditor.user object
						 */
						if(selectedAuthor){
							
							$('#headerContainer .userProfile .username').text(selectedAuthor);
							$('#headerContainer .userProfile .username').attr('data-user-email', selectedEmail);
							$('#contentContainer').attr('data-user-email', selectedEmail);

							var userName = '';
							if(kriya.config.content.role == 'publisher'){
								userName = selectedAuthor + ' ('+kriya.config.content.customer.toUpperCase()+')';
							}else{
								userName = selectedAuthor + ' ('+kriya.config.content.role.toUpperCase()+')';
							}

							kriyaEditor.user.name    = userName;
							tracker.currentUser.name = userName;
							kriyaEditor.user.name = userName;
							
							if(parameters.outhData){
								$('#headerContainer .userProfile .username').attr('data-user-id', parameters.outhData);
								tracker.currentUser.id   = kriya.config.content.role + ' ' + parameters.outhData;
							}
							
							//save the authenticate attribute if authentication success
							if($('#welcomeContainer').attr('data-save-authenticate') == "true" && $('#welcomeContainer').attr('data-tag-class') && authNode && $(authNode).length > 0){
								var dataNode = $(authNode).find('.' + $('#welcomeContainer').attr('data-tag-class'));
								if($(dataNode).length > 0 && !$(dataNode).attr('data-authenticated')){
									$(dataNode).attr('data-authenticated', 'true');
									kriyaEditor.init.save(authNode);
								}
							}

						}

						store.remove('kriyaLogoutFlag');
						//set the cookie for 24 hours
						var d = new Date();
					    d.setTime(d.getTime() + (1*24*60*60*1000));
					    var expires = "expires=" + d.toGMTString();
					    var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
						document.cookie = cname + "=true;" + expires + ";path=/";
						
						eventHandler.welcome.begin.startIntro();

						$('#welcomeContainer').hide();
						$('.la-container').fadeOut();
					}else if (res && res.status && res.status.message){
						kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : res.status.message ,icon: 'icon-warning2'});
						$('.la-container').fadeOut();
					}else if(res && res.token){
						var msg = '<div class="row">Article was already login in another system/browser.do you want to login?</div><div class="row">NOTE:If yes other session will be logout.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium" data-message="{\'click\':{\'funcToCall\': \'loginConfirmation\',\'param\':{\'token\':\'' + res.token + '\', \'authType\':\'' + authType + '\'},\'channel\':\'welcome\',\'topic\':\'begin\'}}">Yes</span><span class="btn btn-danger btn-medium" onclick="kriya.removeNotify(this)">No</span></div></div>';
						kriya.notification({
							title : 'Confirmation',
							type  : 'success',
							content : msg,
							icon : 'icon-info',
							closeIcon : false
						});
					}else{
						kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : 'Verification Failed...' ,icon: 'icon-warning2'});	
						$('.la-container').fadeOut();
					}
					
				},function(res){
					if(res.responseText){
						var resObj = JSON.parse(res.responseText);
						if(resObj.status && resObj.status.message){
							kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : resObj.status.message ,icon: 'icon-warning2'});
						}
					}
					$(targetNode).text('Verify').removeClass('disabled');
					$('.la-container').fadeOut();
				});

				if(kriya.loopWin){
					setTimeout(function(){
						kriya.loopWin.close();
					}, 1000);
				}
			},
			/**
			 * Function to confirm the login 
			 * if the article already opened in another browser/system
			 */
			loginConfirmation: function(param, targetNode){
				
				//If token not found then return error
				if(!param.token){
					kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : 'Token missing. Please contact support.' ,icon: 'icon-warning2'});
					return false;
				}
				
				kriya.removeNotify(targetNode);
				var selectedAuthor = $('.authorList').val();
				var selectedEmail  = $('.authorList').find(':selected').attr('data-email');
				
				var parameters = {
					'customer' : kriya.config.content.customer,
					'project'  : kriya.config.content.project,
					'doi'      : kriya.config.content.doi,
					'stage'    : kriya.config.content.stage,
					'userEmail': selectedEmail,
					'userName' : selectedAuthor,
					'currStageName' : kriya.config.content.stageFullName,
					'cToken' : param.token
				};

				kriya.general.sendAPIRequest('verifyauthentication', parameters, function(res){
					if(res && res.verified == true){
						//set the cookie for 24 hours
						var d = new Date();
					    d.setTime(d.getTime() + (1*24*60*60*1000));
					    var expires = "expires=" + d.toGMTString();
					    var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
						document.cookie = cname + "=true;" + expires + ";path=/";
						store.remove('kriyaLogoutFlag');
						eventHandler.welcome.begin.startIntro();
						$('#welcomeContainer').hide();
						$('.la-container').fadeOut();
					}else if (res && res.status && res.status.message){
						kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : res.status.message ,icon: 'icon-warning2'});
						$('.la-container').fadeOut();
					}else{
						kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : 'Invalid token' ,icon: 'icon-warning2'});	
						$('.la-container').fadeOut();
					}
					
				},function(res){
					if(res.responseText){
						var resObj = JSON.parse(res.responseText);
						if(resObj.status && resObj.status.message){
							kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : resObj.status.message ,icon: 'icon-warning2'});
						}
					}
					$('.la-container').fadeOut();
				});
			},
			/**
			 * Function to send teh veification code to email
			 * Author name and email are mandatory
			 */
			sendAuthCode: function(param, targetNode){
				var selectedAuthor = $('.authorList').val();
				if(selectedAuthor){
					var selectedEmail = $('.authorList').find(':selected').attr('data-email');
					if(selectedEmail){
						$(targetNode).text('sending...').addClass('disabled');
						$('.la-container').fadeIn();
						
						var parameters = {
							'customer'  : kriya.config.content.customer,
							'journalID' : kriya.config.content.project,
							'articleID' : kriya.config.content.doi,
							'stagename' : kriya.config.content.stage,
							'currStage' : 'sendauthcode',
							'to'        : selectedEmail,
							'name'      : selectedAuthor,
							'projectName': $('#contentContainer').attr('data-project-name')
						};

						kriya.general.sendAPIRequest('sendauthcode', parameters, function(res){
							$(targetNode).text('Send Code').removeClass('disabled').addClass('hidden');
							
							$('.authorList').addClass('hidden');
							$('.codeVerifyBtn').removeClass('hidden');
							$('.codeInput').removeClass('hidden');
							$('#welcomeContainer .userMsg').html('Paste code in the box above and click "VERIFY" to begin your review');

							$('.la-container').fadeOut();
						},function(res){
							if(res.responseText){
								var resObj = JSON.parse(res.responseText);
								if(resObj.status.message){
									kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : resObj.status.message ,icon: 'icon-warning2'});	
								}
							}
							$(targetNode).text('Send Code').removeClass('disabled');
							$('.la-container').fadeOut();
						});
					}else{
						kriya.notification({title : 'WARNING',type  : 'warning',timeout : 8000,content : 'Manuscript doesn\'t have email id' ,icon: 'icon-warning2'});
					}
				}else{
					kriya.notification({title : 'WARNING',type  : 'warning',timeout : 8000,content : 'Please select the user',icon: 'icon-warning2'});
				}
			},
			/**
			* Function to verify the code
			* Author name, email and verification code are mandatory
			*/
			verifyCode: function(param, targetNode){
				var verifyCode = $('.codeInput').val();
				var selectedAuthor = $('.authorList').val();
				var selectedEmail = $('.authorList').find(':selected').attr('data-email');
				if(verifyCode != ""){
					$('.la-container').fadeIn();
					$(targetNode).text('Verifying...').addClass('disabled');
					
					var parameters = {
						'customer' : kriya.config.content.customer,
						'project'  : kriya.config.content.project,
						'doi'      : kriya.config.content.doi,
						'stage'    : kriya.config.content.stage,
						'code'     : verifyCode,
						'userEmail': selectedEmail,
						'userName' : selectedAuthor,
						'currStageName' : kriya.config.content.stageFullName
					};

					kriya.general.sendAPIRequest('verifyauthentication', parameters, function(res){
						if(res && res.verified == true){
							//set the cookie for 24 hours
							var d = new Date();
						    d.setTime(d.getTime() + (1*24*60*60*1000));
						    var expires = "expires=" + d.toGMTString();
						    var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
						    document.cookie = cname + "=true;" + expires + ";path=/";
							store.remove('kriyaLogoutFlag');
							eventHandler.welcome.begin.startIntro();
							$('#welcomeContainer').hide();
							$(targetNode).text('Verify').removeClass('disabled');
						}else if (res && res.status && res.status.message){
							kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : res.status.message ,icon: 'icon-warning2'});
						}else{
							kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : 'Verification Failed...' ,icon: 'icon-warning2'});	
						}

						$('.la-container').fadeOut();
					},function(res){
						if(res.responseText){
							var resObj = JSON.parse(res.responseText);
							if(resObj.status.message){
								kriya.notification({title : 'ERROR',type  : 'error',timeout : 8000,content : resObj.status.message ,icon: 'icon-warning2'});	
							}
						}
						$(targetNode).text('Verify').removeClass('disabled');
						$('.la-container').fadeOut();
					});

				}else{
					$('.codeInput').attr('data-error', 'Input required');
				}
			},
			changedAuthorName: function(param, targetNode){
				var selectedEmail = $('.authorList').find(':selected').attr('data-email');
				if(selectedEmail){
					$('.userMsg').html('Click "SEND CODE" to get the code to <b>' + selectedEmail + '</b>');
				}else{
					$('.userMsg').html('Select the user and click "SEND CODE" to get the code to your mail.');
				}
			},
			beginIntro: function(){
				$('#welcomeContainer').hide();
				if(location.pathname=='/proof_review/'){
					if($('span[data-name="MenuBtnPreview"]').length > 0){$('span[data-name="MenuBtnPreview"]').trigger('click')};
					return true;
				}
				eventHandler.welcome.begin.startIntro();
				var d = new Date();
			    d.setTime(d.getTime() + (10*24*60*60*1000));
			    var expires = "expires=" + d.toGMTString();
			    var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
			    document.cookie = cname + "=true;" + expires + ";path=/";
			    
			},
				startEditing: function () {
					$('#welcomeContainer').hide();
					$('#pdfviewContainer').addClass('hidden');
				},
				closeEditor: function () {
					$('#welcomeContainer').hide();
					$('#pdfviewContainer').removeClass('hidden');
					var role = kriya.config.content.role;
					if(role!=''){
						$('.unresolvedCount').text($('#queryDivNode .query-div.'+role+':not([data-replied="true"]):not([data-answered="true"])').length);
					}
					console.log($('#queryDivNode .query-div.author:not([data-replied="true"])').length)
				},
				fullviewEditor: function () {
					if($('#navContainer').hasClass('hidden')){
						$('#editorContainer').removeClass('m12');
						$('#navContainer').removeClass('hidden');
						$('#editorContainer').addClass('m6');
						$('#pfFullScreenExit').addClass('hidden')
						$('#pfFullScreen').removeClass('hidden')
					}else{
						$('#editorContainer').removeClass('m6');
						$('#navContainer').addClass('hidden');
						$('#editorContainer').addClass('m12');
						$('#pfFullScreen').addClass('hidden')
						$('#pfFullScreenExit').removeClass('hidden')
					}
				},
			startIntro: function(){
				var keyVal = "demo-"+kriya.config.content.doi+'-flag';
				 if(store.get(keyVal)){
					return false;
				 }
				store.set(keyVal, 'true');
				var enjoyhint_instance = new EnjoyHint({
					onStart : function(){
						eventHandler.menu.edit.toggleNavPane("helpDivNode", $(".help-icon"));
					},
					onEnd : function(){
						eventHandler.menu.edit.toggleNavPane("queryDivNode", $(".notes-icon"));
					}, 
					onSkip : function(){
						eventHandler.menu.edit.toggleNavPane("queryDivNode", $(".notes-icon"));
					}
				});
				var enjoyhint_script_steps = [
				  {
					'next .notes-icon' : 'Click here to answer "Queries" raised for this paper.'				    
				  },{
				    'next .change-icon' : 'Click here to review "Changes" made to this paper.'
				  },{
				    'next [data-name="ExportPDF"]' : 'Click "Export PDF" to generate the corrected PDF.'
				  },{
				    'next [data-name="Approve"]' : 'Click "Approve" to complete your review.'
				  },{
				  	'next #quickDemo' : 'Click here to view this demo again',
					'showSkip' : false,
					'nextButton' : {className: "myNext", text: "Got it!"}
				  }
				];
				var verifiedObj = [];
				//In author view export view option not there but enjoyhint.min.js try to show that at that time code breaking
				//the following code will select only prasented and visible elements as a cofig.
				$(enjoyhint_script_steps).each(function(){
					var manuplatedKey = Object.keys(this)[0].replace('next ','');
					if($(manuplatedKey+":visible").length > 0){
						verifiedObj.push(this);
					}
				});
				//enjoyhint_instance.set(enjoyhint_script_steps);
				enjoyhint_instance.set(verifiedObj);
				enjoyhint_instance.run();
			},
			skipIntro: function(){
				$('#welcomeContainer').hide();
			},
			sendOfflineMail: function(param, targetNode){
				$('.la-container').fadeIn();
				var parameters = {
					'customer'  :  kriya.config.content.customer,
					'journalID' :  kriya.config.content.project,
					'articleID' :  kriya.config.content.doi,
					'stagename' :  kriya.config.content.stageFullName,
					'currStage' :  'offlinelink',
					'to'        :  $('#headerContainer .userProfile .username').attr('data-user-email'),
					'name'      :  $('#headerContainer .userProfile .username').html(),
					'projectName': $('#contentContainer').attr('data-project-name')
				};
				$('#welcomeContainer').html('<div><p style="text-align: center;">The PDF is being generated and will be sent to your email on record. Please wait till you receive a success message and do not close this window.</p></div>');
				kriya.general.sendAPIRequest('send_offline_link', parameters, function(res){
					$('#welcomeContainer').html('<div><p style="text-align: center;color: green;">Thanks for waiting. The PDF has been generated successfully and sent to your email on record.</p></div>');
					$('.la-container').fadeOut();
				},function(err){
					$('.la-container').fadeOut();
				});
			},
			changeCorRadio: function(param, targetNode){
				var targetVal = $(targetNode).attr('data-value');
				$('#welcomeContainer .welUploadPdfBtn, #welcomeContainer .welSignOffBtn').addClass('disabled');
				if(targetVal == "no-correction"){
					$('#welcomeContainer .welSignOffBtn').removeClass('disabled');
				}else if(targetVal == "correction"){
					$('#welcomeContainer .welUploadPdfBtn').removeClass('disabled');
				}
			},
			uploadPdfFileSelection: function(param, targetNode){
				$(targetNode).parent().find('input:file').val('');
				$(targetNode).parent().find('input:file').trigger('click');
			},
			uploadPdfFile: function(param, targetNode){
				var file = $(targetNode)[0].files;
				if(!file || !file[0]){
					return false;
				}
				$('.la-container').fadeIn();
				var param = {
					'file': file[0],
					'convert': 'false',
					'success': function(res){
						var fileDetailsObj = res.fileDetailsObj;
						var fileName = fileDetailsObj.name+fileDetailsObj.extn;
						var filePath = '/resources/' + fileDetailsObj.dstKey;
						var d = new Date();
						var currTime = d.toLocaleDateString() +" " + d.toLocaleTimeString();
						var commentJSON = {
							"content" : '<p>Author has uploaded the revised pdf.</p><p><span class="comment-file"><a href="' + filePath + '" target="_blank">' + fileName + '</a></span></p>',
							"fullname": $('#headerContainer .userProfile .username').html(),
							"created" : currTime
						};
						$.ajax({
							type: 'POST',
							url: '/api/save_note_new',
							processData: false,
							contentType: 'application/json; charset=utf-8',
							data: JSON.stringify({
								"content" : commentJSON,
								"doi" : kriya.config.content.doi,
								"customer" : kriya.config.content.customer,
								"journal" : kriya.config.content.project
							}),
							success: function(res) {
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Uploaded successfully.',
									type: 'success',
									content: fileName + ' file was uploaded successfully.',
									icon : 'icon-info'
								});
								$('#welcomeContainer .welSignOffBtn').removeClass('disabled');
							},
							error: function(err){
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed.',
									type: 'error',
									content: 'Saving the uploaded file path in notes was failed.',
									timeout: 8000,
									icon : 'icon-info'
								});
							}
						});
					},
					'error': function(err){
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed.',
							type: 'error',
							content: 'Uploading file failed.',
							icon : 'icon-info'
						});
					}
				}
				eventHandler.menu.upload.uploadFile(param);
			},
			authorCorSignOff: function(param, targetNode){
				$('.la-container').fadeIn();
				var params = 'customerName='+kriya.config.content.customer+'&doi='+kriya.config.content.doi+'&journalID='+kriya.config.content.project+'&currStage='+kriya.config.content.stage;
				$.ajax({
					type: "GET",
					url : "/api/getsignoffoptions?" + params,
					success: function (data) {
						var res = $('<div>' + data + '</div>');
						var selectedVal = $('#welcomeContainer [name="wel_radio"]:checked').attr('data-value');
						var attrName = (selectedVal == "no-correction")?'data-check-no-correction':'data-check-correction';
						if(res.find('p[' + attrName + '] input[type="radio"][data-reviewer]').length > 0 && selectedVal){
							var nextStage = res.find('p[' + attrName + '] input[type="radio"][data-reviewer]').first().attr('data-reviewer');
							//get email recipients info from the sign-off workflow html
							if (res.find('trigger[name="' + nextStage + '"]').length > 0){
								var parameters = {
									'customer':kriya.config.content.customer,
									'journalID':kriya.config.content.project, 
									'articleID':kriya.config.content.doi, 
									'currStage':nextStage
								};
								var trigger = res.find('trigger[name="' + nextStage + '"][action="sendMail"]').first();
								if (typeof(parameters['to']) == "undefined" && trigger.find('to').text() != ""){
									parameters['to'] = trigger.find('to').text();
								}

								if (typeof(parameters['recipientName']) == "undefined" && trigger.find('recipient').text() != ""){
									if(trigger.find('recipient .given-names').length > 0){
										parameters['recipientName'] = trigger.find('recipient .given-names').text() + ' '+ trigger.find('recipient .surname').text();
									}else{
										parameters['recipientName'] = trigger.find('recipient').text();
									}			
								}

								if (trigger.find('cc').text() != ""){
									parameters['cc'] = trigger.find('cc').text();
								}
								if (trigger.find('bcc').text() != ""){
									parameters['bcc'] = trigger.find('bcc').text();
								}
								if (trigger.find('recipientName').text() != ""){
									parameters['recipientName'] = trigger.find('recipientName').text();
								}
								if (res.find('trigger[name="' + nextStage + '"][action="addStage"]:first stage').text() != ""){
									parameters['stagename'] = res.find('trigger[name="' + nextStage + '"][action="addStage"] stage').text();
								}

								//return error if to address is not found
								if (typeof(parameters['to']) == "undefined" || parameters['to'] == ""){
									kriya.notification({
										title : 'ERROR',
										type  : 'error',
										content : 'Email address not found!<br>Contact Administrator',
										icon: 'icon-warning2'
									});
									return false;
								}

								kriya.general.sendAPIRequest('signoff', parameters, function(res){
									$('.la-container').fadeOut();
									$('body .messageDiv .message').html('You have successfully signed off the article to the next stage.<br> Please close the browser to exit the system.');
									$('body .messageDiv').removeClass('hidden');
									$('body .messageDiv .feedBack').removeClass('hidden');
			
									kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
								},function(res){
									$('.la-container').fadeOut()
									kriya.notification({
										title: 'Failed.',
										type: 'error',
										content: 'Signoff article was failed.',
										icon : 'icon-info'
									});
								});
							}	
						}else{
							kriya.notification({
								title: 'Failed.',
								type: 'error',
								content: 'Signoff configurations not found.',
								icon : 'icon-info'
							});	
						}
					},
					error: function(res){
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed.',
							type: 'error',
							content: 'Getting signoff options was failed.',
							icon : 'icon-info'
						});
					}
				});
			}
		},
		signoff: {
			validationXML: function(param, targetNode){
				$('.la-container').fadeIn();
				$('#infoDivContent #validationDivNode').html('');
				var parameters  = {
					'doi': kriya.config.content.doi,
					'customer': kriya.config.content.customer,
					'project':kriya.config.content.project,
					'role':'preeditor',
					'stage':kriya.config.content.stage,
					'xmltype':'raw',
				};
				kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
					if(res.error){
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Validation Failed. Please contact support.',
							timeout: 8000,
							icon : 'icon-info'
						});
					}else if(res){
							res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); //change self closed column tag to open and close tag
							res = res.replace(/dmp/, '');
							var popper = $('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]');
							var resNode = $(res);
							var stataus = true;
							if((resNode.find('message').length > 0) && (resNode.find('message signOff-allow:contains("false")').length > 0)){
								var container = $('<div />');
								var ruleTypes = [];
								resNode.find('rule-type').each(function(){
									var ruleType = $(this).text();
									if(ruleTypes.indexOf(ruleType) < 0){
										ruleTypes.push(ruleType);
										var msgLength = $(this).closest('probe-result').find('rule-type:contains(' + ruleType + ')').length;
										var tabeNode = $('<input id="_probe_' + ruleType + '_" type="radio" name="tabs_probe"/><label for="_probe_' + ruleType + '_">' + ruleType + '<span class="badge">' + msgLength + '</span></label>');
										if(ruleTypes.length == 1){
											tabeNode.filter('input[type="radio"]').attr('checked', 'true');
										}
										container.append(tabeNode);
									}
								});
								var styleString = '';
								for(var r=0;r<ruleTypes.length;r++){
									var tabId = 'probe_' + ruleTypes[r];
									var ruleDiv = $('<div id="' + tabId + '" class="carousel-item" />');
									container.append(ruleDiv);
									styleString += '#compDivContent #_' + tabId + '_:checked ~ #' + tabId + ',';
								}
								styleString = styleString.replace(/\,$/g, '');
								styleString += '{display:block;}';
								var styleNode = $('<style>' + styleString + '</style>');
								resNode.find('message').each(function(){
									var row = $('<p data-clone-id="' + $(this).find('node-id').text() + '">' + $(this).find('probe-message').text() + '</p>');
									var ruleType = $(this).find('rule-type').text();
									container.find('#probe_' + ruleType).append(row);
								});
								$('#infoDivContent > [id*=validationDivNode]').remove();
								$('#infoDivContent > [id*=DivNode]').removeClass('active');
								$('#infoDivContent').append($('<div id="validationDivNode" class="active" style="height: 668px;"><p>Validation Errors</p></div>'));
								$('#validationDivNode').append(container)
								$('#validationDivNode').append(styleNode);
								$('#validationDivNode label[for]').remove();
								$('.la-container').fadeOut();
								return false;
							}else{
								if(param){
									$('.la-container').fadeOut();
									if(typeof(param) == "string"){
										param = getStringObj(param);
									}
									param();
								}
							}
					}
				}, function(err){
					$('.la-container').fadeOut();
					if(onError && typeof(onError) == "function"){
						onError(err);
					}
				});
			},
			signoffPage: function(param, targetNode){
				//if(kriya.config.content.changes){
					//kriya.notification({title : 'ERROR',type  : 'error',content : 'You have modified the content, but have not generated the proof. Please generate the PDF to ensure the changes have reflected in the PDF before approving the article.',icon: 'icon-warning2'});
					//return false;
				//}
				eventHandler.query.action.regenerateActionQuery();
				var validate = eventHandler.welcome.signoff.validate();
				if (! validate) return false;
				
				//add internal validation to all stages updated by vijayakumar on 13-02-2019
				var invalidElements = eventHandler.welcome.signoff.internalValidation();
				if (! invalidElements){
					eventHandler.welcome.signoff.acceptInternalValidation()
				}
				if($('#contentContainer').attr('data-citation-trigger') == "true"){
					var reorderStatus = citeJS.floats.renumberConfirmation('signoffPage');
					if(reorderStatus == false){
						return false;
					}
				}
				if ($(targetNode).closest('[data-validate-' + kriya.config.content.role + ']').length > 0){
					var validateFunctions = $(targetNode).closest('[data-validate-' + kriya.config.content.role + ']').attr('data-validate-' + kriya.config.content.role).split(/,\s?/);
					var validate = true;
					for (var v = 0, vl = validateFunctions.length; v < vl; v++){
						var functionName = validateFunctions[v];
						if (typeof(window['eventHandler']['welcome']['signoff'][functionName]) == 'function'){
							var validationResult = window['eventHandler']['welcome']['signoff'][functionName](param, targetNode);
							if (!validationResult){
								validate = false;
							}
						}
					}
					if (! validate) return false;
				}
				
				$('.la-container').fadeIn();
				/*var params = 'customerName='+kriya.config.content.customer+'&doi='+kriya.config.content.doi+'&journalID='+kriya.config.content.project+'&currStage='+kriya.config.content.stage;
				$.ajax({
					type: "GET",
					url: "/api/getsignoffoptions?" + params,
					success: function (data) {
						$('.la-container').fadeOut();
						var res = $('<div>'+data+'</div>');
						$('[data-component="signoff_edit"] [data-wrapper]').html($(res).find('options').html());
						$(res).find('trigger').attr('style', 'display:none');
						$('[data-component="signoff_edit"] [data-wrapper]').append($(res).find('trigger'));
						$(res).children('option[name]').each(function(){
							$('[data-component="signoff_edit"] [data-wrapper]').append('<p>'+$(this).html()+'</p>');
						})
						$('[data-component="signoff_edit"] [data-wrapper] input[type="radio"]').attr('name', 'signoff-radio');
						kriya.popUp.openPopUps($('[data-component="signoff_edit"]'));
					},
					error: function(res){
						$('.la-container').fadeOut()
						console.error('ERROR:saving content.'+res);
					}
				});*/
				var validate = (param && param.validate)?param.validate:'';
				if(validate == "false"){
					kriya.popUp.closePopUps($('[data-component="PROBE_VALIDATION_edit"]'));
				}
				eventHandler.welcome.signoff.probeValidation('exeter', validate, function(status){
					eventHandler.welcome.signoff.dtdValidation();
					if(status){
						eventHandler.welcome.signoff.probeValidation(kriya.config.content.customer, validate, function(status){
							if(status){
								if($('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]').length > 0 && $('[data-type="popUp"][data-component="PROBE_VALIDATION_CHECK"]').length == 0){
									if($('#contentDivNode[data-noEntityMissing="false"], #contentDivNode[data-noFontMissing="false"]').length > 0){
										kriya.popUp.openPopUps($('[data-component="PROBE_VALIDATION_edit"]'));
										$('.probeResultContainer').html('Proof QC Failed. Please contact support')
										$('.la-container').fadeOut();
										return;
									}
								}
								var parameters = {
									'customer': kriya.config.content.customer,
									'project' : kriya.config.content.project, 
									'doi'     : kriya.config.content.doi,
									'xpath'   : '//workflow/stage[last()]'
								};
								//Send the request to get the current stage xml
								keeplive.sendAPIRequest('getdatausingxpath', parameters, function(res){
									if(res){
										var stageNode = $(res);
										var currentStatus = stageNode.find(' > status').text();
										if(currentStatus == "completed"){
											$('.la-container').fadeOut();
											kriya.notification({title : 'ERROR',type  : 'error',content : 'Looks like there is an issue with workflow. You will not be able to approve the manuscript at the moment. Please contact support/publisher.',icon: 'icon-warning2'});
										}else{
											var params = 'customerName='+kriya.config.content.customer+'&doi='+kriya.config.content.doi+'&journalID='+kriya.config.content.project+'&currStage='+kriya.config.content.stage;
											$.ajax({
												type: "GET",
												url: "/api/getsignoffoptions?" + params,
												success: function (data) {
													$('.la-container').fadeOut();
													var res = $('<div>' + data + '</div>');
													$('[data-component="signoff_edit"] [data-wrapper]').html($(res).find('options').html());
													$(res).find('trigger').attr('style', 'display:none');
													$('[data-component="signoff_edit"] [data-wrapper]').append($(res).find('trigger'));
													$(res).children('option[name]').each(function(){
														$('[data-component="signoff_edit"] [data-wrapper]').append('<p>'+$(this).html()+'</p>');
													})
													$('[data-component="signoff_edit"] [data-wrapper] input[type="radio"]').attr('name', 'signoff-radio');
													
													//If the configured role made any correction then remove that node wich has data-check-no-correction attribute - jagan
													$('[data-component="signoff_edit"] [data-wrapper] [data-check-no-correction]').each(function(){
														var checkCor       = $(this).attr('data-check-no-correction');
														var correctionNode = $('#changesDivNode .change-div.card[data-role="' + checkCor + '"], #historyDivNode .historyCard.card[data-role="' + checkCor + '"]');
														if(correctionNode.length > 0){
															$(this).remove();
														}
													});

													//Show article information to sign-off popup added by vijayakumar on 18-03-2019
													var articleinfo = $('[data-name="sign-off-info"]')
													if(articleinfo.length > 0){
														var updated = false;
														$(articleinfo).find('*[data-selector]').each(function(){
															var selectnode = $(this).attr('data-selector');
															$(this).closest('.split-info').removeClass('hidden')
															$(articleinfo).removeClass('hidden')
															var editElement = kriya.xpath('//*[@class="jrnlSplIssue"]');
															if($(editElement).length > 0){
																var nodevalue = $(editElement).text();
																if(nodevalue == "")$(this).parent().addClass('hidden');
																$(this).text(nodevalue)
																updated = true;
															}else{
																$(this).closest('.split-info').removeClass('hidden')
															}
														})
														if(updated == false){
															$(articleinfo).addClass('hidden')
														}
													}
													//to check the valiadation for validatin check
													var validationcheck = $('[data-component="PROBE_VALIDATION_CHECK"]')
													if(validationcheck.length > 0){
														var currentstage = $(validationcheck).attr('data-check-stage')
														var movestage = $(validationcheck).attr('data-move-stage');
														if(movestage != "" && currentstage != ""){
															var spagearray = currentstage.split(',');
															var validation = false;
															$(spagearray).each(function(i,val){
																if(kriya.config.content.stage == val){
																	validation = true;
																}
															})
															if(validation == true && $('[data-component="PROBE_VALIDATION_CHECK"][data-validation-error="true"]').length > 0){
																var changestage = $(validationcheck).attr('data-current-stage').split(',');
																$(changestage).each(function(i,val){
																	$('[data-component="signoff_edit"] [data-reviewer="'+val+'"]').closest('p').addClass('hidden')
																	$('[data-component="signoff_edit"] [data-reviewer^="'+movestage+'"]').closest('p').removeClass('hidden')
																})
															}
															}
														}
													//If the configured role didn't made any correction then remove that node wich has data-check-correction attribute - jagan
													$('[data-component="signoff_edit"] [data-wrapper] [data-check-correction]').each(function(){
														var checkCor       = $(this).attr('data-check-correction');
														var correctionNode = $('#changesDivNode .change-div.card[data-role="' + checkCor + '"], #historyDivNode .historyCard.card[data-role="' + checkCor + '"]');
														if(correctionNode.length == 0){
															$(this).remove();
														}
													});

													kriya.popUp.openPopUps($('[data-component="signoff_edit"]'));
												},
												error: function(res){
													$('.la-container').fadeOut()
													console.error('ERROR:saving content.'+res);
												}
											});
										}
									}
								},function(res){
									$('.la-container').fadeOut();
									kriya.notification({title : 'ERROR',type  : 'error',content : 'Unable to fetch current workflow status. Please try again or contact support/publisher.',icon: 'icon-warning2'});
								});
							}else{
								$('.la-container').fadeOut()
							}
						}, function(){
							//Error callback
							$('.la-container').fadeOut();
							kriya.notification({title : 'ERROR',type  : 'error',content : 'Probe Validation Failed.',icon: 'icon-warning2'});
						});
					}else{
						$('.la-container').fadeOut()
					}
				}, function(){
					//Error callback
					$('.la-container').fadeOut();
					kriya.notification({title : 'ERROR',type  : 'error',content : 'Probe Validation Failed.',icon: 'icon-warning2'});
				});
			},
			reTryExportPdf:function(){
				eventHandler.menu.export.exportToPDF('','','',function(rescallback,errorMsg,toastid){
					if(rescallback == 'success'){
						$('[data-name="signoff_edit"]').find('.disableMsg,.disableOpt').remove();
						$('[data-name="signoff_edit"]').find('input').removeClass('hidden');
					}else if(rescallback == 'error'){
						if($('#'+toastid).find('.kriya-notice-body').find('a').length>0){
							$('#'+toastid).find('.kriya-notice-body').append('Would you like to retry?</br>');
							$('#'+toastid).find('.kriya-notice-body').append($('#'+toastid).find('a'));
							$('#'+toastid).find('a').html('DOWNLOAD PDF')
							$('#'+toastid).find('.kriya-notice-body').append('<span class="retry" onClick = "eventHandler.welcome.signoff.reTryExportPdf()">RETRY</span><span class="cancel" onclick="kriya.removeNotify(this)">CANCEL</span>')
						}else{
							$('#'+toastid).find('.kriya-notice-body').append('</br>Would you like to retry?');
							$('#'+toastid).find('.kriya-notice-body').append('</br><span class="retry" onClick = "eventHandler.welcome.signoff.reTryExportPdf()">RETRY</span><span class="cancel" onclick="kriya.removeNotify(this)">CANCEL</span>')
						}
					}
				});
			},
			packageCreator:function(param,packageType, onSuccess, onError){				
				var doiString = param.articleID;								
				var jrnlName = param.journalID;
				var customer = param.customer;
				var volume = param.volume;
				var issue = param.issue;
				console.log('publishFile', doiString, jrnlName);
				var notificationID = kriya.notification({
					title : 'Information',
					type  : 'success',					
					content : 'generating final package',
					icon : 'icon-info'				
				});
				$.ajax({
					type: 'GET',
					url: "/api/packagecreator?doi=" + doiString + '&jrnlName=' + jrnlName + '&packageType=' + packageType + '&customer=' + customer + '&volume=' + volume + '&issue=' + issue,
					success: function(data){
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
							eventHandler.welcome.signoff.packageCreatorJobStatus(data.message.jobid, param.customer, notificationID, onSuccess, onError);
						}
						else if(data == ''){
							$('#'+notificationID+' .kriya-notice-body').html('Failed..');
							if(onError && typeof(onError) == "function"){
								onError();
							}
						}else{
							$('#'+notificationID+' .kriya-notice-body').html(data.status.message);							
							if(data.status.message == "job already exist"){
								setTimeout(function() {
									eventHandler.welcome.signoff.packageCreatorJobStatus(data.message.jobid, param.customer, notificationID, onSuccess, onError);
								}, 1000);
							}							
						}
					},
					error: function(error){						
						if(onError && typeof(onError) == "function"){
							onError();
						}	
					}
				});
			},			
			packageCreatorJobStatus:function(jobID, customer, notificationID, onSuccess, onError){	
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.status){
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if(data.status.message.stage.current){
								currStep = data.status.message.stage.current;
							}    
							var loglen = data.status.message.log.length;    
							var process = data.status.message.log[loglen-1];
							if (/completed/i.test(status)){
								var loglen = data.status.message.log.length;	
								var process = data.status.message.log[loglen-1];
								$('#'+notificationID+' .kriya-notice-body').html(process);
								$('#'+notificationID+' .kriya-notice-header').html('Publishing file completed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
								if(onSuccess && typeof(onSuccess) == "function"){
									onSuccess();
								}
							}else if (/failed/i.test(status) || code != '200'){
								$('#'+notificationID+' .kriya-notice-body').html(process);
								$('#'+notificationID+' .kriya-notice-header').html('Publishing file failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
								if(onError && typeof(onError) == "function"){
									onError();
								}
							}else if (/no job found/i.test(status)){
								$('#'+notificationID+' .kriya-notice-body').html(status);
							}else{
								$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;	
								if(loglen > 1){
									var process = data.status.message.log[loglen-1];
									if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								}
								setTimeout(function() {
									eventHandler.welcome.signoff.packageCreatorJobStatus(jobID, customer, notificationID, onSuccess, onError);
								}, 1000 );
							}
						}
						else{							
							if(onError && typeof(onError) == "function"){
								onError();
							}
						}
					},
					error: function(error){
						console.log('Getting job status was failed...');
						if(onError && typeof(onError) == "function"){
							onError();
						}
					}
				});
			},
			signOffConfirm: function(param, targetNode){	
				//prevent sign-off for support stage - JAI:03-09-2018
				if (kriya.config.content.stage == 'support'){
					kriya.notification({title : 'ERROR',type  : 'error',content : 'You cannot signoff in Support stage.',icon: 'icon-warning2'});
					return true;
				}
				if ($('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').length == 0) return true;
				var type;
				if($('[data-component="signoff_edit"] [data-wrapper]').find('[data-highwireType]').length > 0){
					type = $('[data-component="signoff_edit"] [data-wrapper]').find('[data-highwireType]').attr('data-highwireType');
				}	
				var volNo, issueNo = '';			
				if($('.jrnlPubData .jrnlVolume').length > 0){
					volNo = $('.jrnlPubData .jrnlVolume')[0].innerHTML;
				}
				if($('.jrnlPubData .jrnlIssue').length > 0){
					issueNo = $('.jrnlPubData .jrnlIssue')[0].innerHTML
				}	
				var parametersToPackageGen = {
					'customer':kriya.config.content.customer, 
					'journalID':kriya.config.content.project, 
					'articleID':kriya.config.content.doi,					
					'volume' : volNo,
					'issue' : issueNo
				};
				if($('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').attr('data-export-pdf') != undefined){
					var popper = $('[data-component="retryProofing_edit"]');
					if($('[data-component="retryProofing_edit"]').hasClass('hidden')){
						kriya.popUp.openPopUps(popper);
						setTimeout(function(){
							$('[data-component="retryProofing_edit"]').progress('Generating Pdf..');
							$(popper).find('.loadingProgress').css('bottom', '0px');
							$(popper).find('.loading-wraper').css('opacity', '0');
						},550);
					}else{
						$('[data-component="retryProofing_edit"]').progress('Generating Pdf..');
						$(popper).find('.loadingProgress').css('bottom', '0px');
						$(popper).find('.loading-wraper').css('opacity', '0');
					}
					eventHandler.menu.export.exportToPDF('','','',function(rescallback,errorMsg,toastid){
						if(rescallback == 'success'){
							eventHandler.welcome.signoff.signOffToNextStage(param, targetNode);
						}else{
							popper.progress('',true);
						}
					},popper);
				}else if(type == undefined){
					eventHandler.welcome.signoff.signOffToNextStage(param, targetNode);
				}else{
					eventHandler.welcome.signoff.packageCreator(parametersToPackageGen,type,function(){					
						eventHandler.welcome.signoff.signOffToNextStage(param, targetNode);
					},function(){
						kriya.notification({
							title : 'ERROR',
							type  : 'error',
							content : 'package generation failed',
							icon : 'icon-warning2'
						});
					});					
				}				
			},
			signOffToNextStage: function(param, targetNode){				
				var nextStage = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').attr('data-reviewer');			
					var parameters = {
						'customer':kriya.config.content.customer, 
						'journalID':kriya.config.content.project, 
						'articleID':kriya.config.content.doi, 
						'role':kriya.config.content.role,
						'currStage':nextStage
					};
	
					var selectedOption = $('[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]:checked').closest('p');
					// to stop sign-off it the user haven't added the time for process - Jai
					var timeTracker = true;
					var timeTrackerDetails = "";
					$('.timeTracker').find('.input-type input').each(function(){
						$(this).removeAttr('data-error');
						if ($(this).val() == ""){
							$(this).attr('data-error', 'Please fill this field');
							timeTracker = false;
						}else{
							var processName = $(this).parent().attr('data-process-name');
							timeTrackerDetails += '<process type="'+processName+'">' + $(this).val() + '</process>';
						}
					});
					if (! timeTracker){
						return true;
					}
					if (timeTrackerDetails != ""){
						timeTrackerDetails = '<time-tracker>'+timeTrackerDetails+'</time-tracker>';
						var trackParam = {'customerName':kriya.config.content.customer, 'projectName':kriya.config.content.project, 'doi':kriya.config.content.doi, 'skipElasticUpdate': 'true'};
						trackParam.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus>' + timeTrackerDetails + '</stage>';
						kriya.general.sendAPIRequest('updatedatausingxpath', trackParam, function(res){});
					}
					//to check if the current selected has an email selection option and is been selected
					if (selectedOption.find('.recipient').length > 0 && selectedOption.find('.recipient').val() == "---"){
						return true;
					}
					if (selectedOption.find('.recipient').length > 0 && selectedOption.find('.recipient').val() != "---"){
						parameters['to'] = selectedOption.find('.recipient').val();
						parameters['recipientName'] = selectedOption.find('.recipient option:checked').text();
					}
					//get email recipients info from the sign-off workflow html
					if ($('trigger[name="' + nextStage + '"]').length > 0){
						var trigger = $('trigger[name="' + nextStage + '"][action="sendMail"]').first();
						if (typeof(parameters['to']) == "undefined" && trigger.find('to').text() != ""){
							parameters['to'] = trigger.find('to').text();
						}
	
						if (typeof(parameters['recipientName']) == "undefined" && trigger.find('recipient').text() != ""){
							if(trigger.find('recipient .given-names').length > 0){
								parameters['recipientName'] = trigger.find('recipient .given-names').text() + ' '+ trigger.find('recipient .surname').text();
							}else{
								parameters['recipientName'] = trigger.find('recipient').text();
							}						
						}
	
						if (trigger.find('cc').text() != ""){
							parameters['cc'] = trigger.find('cc').text();
						}
						if (trigger.find('bcc').text() != ""){
							parameters['bcc'] = trigger.find('bcc').text();
						}
						if (trigger.find('recipientName').text() != ""){
							parameters['recipientName'] = trigger.find('recipientName').text();
						}
						if ($('trigger[name="' + nextStage + '"][action="addStage"]:first stage').text() != ""){
							parameters['stagename'] = $('trigger[name="' + nextStage + '"][action="addStage"] stage').text();
						}
						if ($('.email-component').length > 0){
							if (trigger.find('.email-body').html() != $('.email-component').find('.email-body').html()){
								parameters['emailBody'] = $('.email-component').find('.email-body').html();
							}
						}
					}
					//return error if to address is not found
					if (typeof(parameters['to']) == "undefined" || parameters['to'] == ""){
						kriya.notification({
							title : 'ERROR',
							type  : 'error',
							timeout : 10000,
							content : 'Email address not found!<br>Contact Administrator',
							icon: 'icon-warning2'
						});
						return false;
					}
					$(targetNode).addClass('disabled');
					$('.la-container').fadeIn();
					kriya.general.sendAPIRequest('signoff', parameters, function(res){
						if(res && res.status && res.status.message == "failed"){
							$('.la-container').fadeOut();
							kriya.notification({
								title : 'ERROR',
								type  : 'error',
								content : 'Signoff failed.',
								icon: 'icon-warning2'
							});
						}else{
							$('[data-component="signoff_edit"] .btn').addClass('hidden');
							$('[data-component="signoff_edit"] [data-wrapper]').html('<p style="text-align: center;">You have <strong>signed off</strong> this manuscript successfully. <em>No further changes</em> can be made to the manuscript.</p>');
							setTimeout(function(){
								$('.la-container').fadeOut();
								$('body .messageDiv .message').html('You have successfully signed off the article to the next stage.<br> Please close the browser to exit the system.');
								$('body .messageDiv').removeClass('hidden');
								$('body .messageDiv .feedBack').removeClass('hidden');
	
								kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
								//window.close();
							},3000);

							if($('#contentContainer').attr('article-access-type') == "linkwithkey"){
								$.ajax({
									type: 'GET',
									url : '/logout?key=true',
									success: function (res) {
										$('.la-container').fadeOut();
									},
									error: function(err){
										$('.la-container').fadeOut()
									}
								});
							}
						}
					},function(res){
						$('.la-container').fadeOut();
						$(targetNode).removeClass('disabled');
						kriya.notification({
							title : 'ERROR',
							type  : 'error',
							content : 'Signoff failed. Please try again.',
							icon: 'icon-warning2'
						});
					});
			},
			cancelSignOff: function(){
				kriya.popUp.closePopUps($('[data-component="signoff_edit"]'));
			},
			logOut: function(){
				// to prevent closing article while content to be saved is in save queue - aravind
				if(kriya.config.saveQueue && kriya.config.saveQueue.length > 0){
					if(!(confirm("You have unsaved data on this page! Are you sure you want to logout? Click Cancel to go back and save, or Ok to logout."))){
						return false;
					}
				}
				if ($('#contentContainer').attr('data-stage-name') == undefined) return false;
				$('.la-container').fadeIn();
				var parameters = {'customerName':kriya.config.content.customer, 'projectName':kriya.config.content.project, 'doi':kriya.config.content.doi};
				var d = new Date();
				var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status type="user">logged-off</status></log></job-logs></stage>';
				kriya.general.sendAPIRequest('updatedatausingxpath', parameters, function(res){
					setTimeout(function(){

						if($('#contentContainer').attr('article-access-type') == "linkwithkey"){
							$.ajax({
								type: 'GET',
								url: '/logout?key=true',
								success: function (res) {
									//Reset the welcome page cookie when logout the article
									var d = new Date();
									var expires = "expires=" + d.toGMTString();
									var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
									document.cookie = cname + "=true;" + expires + ";path=/";
									
									$('.la-container').fadeOut();
									$('body .messageDiv .message').html('You have successfully logged off.<br> Please close the browser to exit the system.');
									$('body .messageDiv').removeClass('hidden');
									$('body .messageDiv .feedBack').addClass('hidden');
									kriya.config.pageStatus = "logged-off"; //This flag used in on window close in keeplive.js
								},
								error: function(err){
									$('.la-container').fadeOut()
								}
							});
						}else{
							//Reset the welcome page cookie when logout the article
							var d = new Date();
							var expires = "expires=" + d.toGMTString();
							var cname = kriya.config.content.doi + '-' + kriya.config.content.role + '-welcome-page';
							document.cookie = cname + "=true;" + expires + ";path=/";
							
							$('.la-container').fadeOut();
							$('body .messageDiv .message').html('You have successfully logged off.<br> Please close the browser to exit the system.');
							$('body .messageDiv').removeClass('hidden');
							$('body .messageDiv .feedBack').addClass('hidden');
							kriya.config.pageStatus = "logged-off"; //This flag used in on window close in keeplive.js
						}
					},1000);
				},function(res){
					$('.la-container').fadeOut()
				});
			},
			rulesOpenInNewTab: function(param, targetElement){
				var popper = $(targetElement).closest('[data-type="popUp"]');
				var html = $('<html/>');
				var materializeCss = $('link[href*="materialize.min.css"]').clone(true);
				var pageCss = $('link[href*="page.css"]').clone(true);

				var headNode = $('<head />');
				headNode.append(materializeCss);
				headNode.append(pageCss);
				
				var bodyNode = $('<body />');
				var content = popper.clone(true);
				content.find('.popup-menu').remove();
				content.find('.proceedingSignoff').closest('.row').remove();
				content.find('[data-input-editable="true"]').css({
					'width' : '100%',
					'min-height' : '100%', 
					'top' : '0%'
				});
				content.find('.carousel-item').css({
					'min-height' : '80%', 
				});
				
				bodyNode.append('<div id="compDivContent" />');
				bodyNode.find('#compDivContent').append(content);

				html.append(headNode);
				html.append(bodyNode);
				var newWindow = window.open();
				newWindow.document.write(html[0].outerHTML);
			},
			
			dtdValidation: function(validate, onSuccess, onError){
				var parameters  = {
					'doi': kriya.config.content.doi,
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project,
					'role': kriya.config.content.role,
					'type': 'kriya_raw',
					'stage':kriya.config.content.stage,
					'log': 'true',
					'xmltype': 'raw'				};
				kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
					if(res && res.error){
						if(onError && typeof(onError) == "function"){
							onError(res);
						}
					}else if(res){
						console.log('dtd validate success')
					}else{
						if(onError && typeof(onError) == "function"){
							onError("Empty response.");
						}	
					}
				}, function(err){
					if(onError && typeof(onError) == "function"){
						onError(err);
					}
				});
			},
			probeValidation: function(customer,validate, onSuccess, onError){
				var popper = $('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]');
				$('#contentDivNode .activeElement').removeClass('activeElement')
				if(popper.length == 0 || validate == "false"){
					if(onSuccess && typeof(onSuccess) == "function"){
						onSuccess(true);
						return true;
					}
				}
				var parameters  = {
					'doi': kriya.config.content.doi,
					'customer': kriya.config.content.customer,
					'rules': customer,
					'project':kriya.config.content.project,
					'role': kriya.config.content.role,
					'stage':kriya.config.content.stage,
				};
				if(customer == "exeter"){
					parameters['content'] = $(kriya.config.containerElm).html();
				}
				if(customer == "proof"){
					parameters['xmltype'] = 'raw'
					parameters['type'] = 'proof'
					parameters['customer'] = kriya.config.content.customer
				}
				kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
					if(res && res.error){
						if(onError && typeof(onError) == "function"){
							onError(res);
						}
					}else if(res){
						//remove the probe validation notification
						kriya.removeNotify('#'+$('.kriya-notice').attr('id'))
						res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); //change self closed column tag to open and close tag
						var resNode = $(res);
						var stataus = true;
						var showerror = true;
						var validationcheck = $('[data-component="PROBE_VALIDATION_CHECK"]')
						if(validationcheck.length > 0){
							if(resNode.find('message:not(:empty)').length > 0){
								$('[data-component="PROBE_VALIDATION_CHECK"]').attr('data-validation-error', 'true');
							}
							showerror = false;
						}
						if(resNode.find('message:not(:empty)').length > 0 && showerror == true){
							
							var container = $('<div />');
							var ruleTypes = [];
							
							resNode.find('rule-type').each(function(){
								var ruleType = $(this).text();
								if(ruleTypes.indexOf(ruleType) < 0){
									ruleTypes.push(ruleType);
									var msgLength = $(this).closest('probe-result').find('rule-type:contains(' + ruleType + ')').length;
									var tabeNode = $('<input id="_probe_' + ruleType + '_" type="radio" name="tabs_probe"/><label for="_probe_' + ruleType + '_">' + ruleType + '<span class="badge">' + msgLength + '</span></label>');
									if(ruleTypes.length == 1){
										tabeNode.filter('input[type="radio"]').attr('checked', 'true');
									}
									container.append(tabeNode);
								}
							});

							var styleString = '';
							for(var r=0;r<ruleTypes.length;r++){
								var tabId = 'probe_' + ruleTypes[r];
								var ruleDiv = $('<div id="' + tabId + '" class="carousel-item" />');
								//var displayTable = $('<table />');
								//displayTable.append('<thead><tr><th>Rule ID</th><th width="10%">Position</th><th>Message</th><th>Xpath</th></tr></thead>');
								//displayTable.append('<tbody />');
								//ruleDiv.html(displayTable);
								container.append(ruleDiv);

								styleString += '#compDivContent #_' + tabId + '_:checked ~ #' + tabId + ',';
							}
							styleString = styleString.replace(/\,$/g, '');
							styleString += '{display:block;}';
							var styleNode = $('<style>' + styleString + '</style>');
							$('.la-container').fadeOut();
							if(customer == "exeter"){
								if(resNode.find('message signOff-allow:contains("false")').length > 0){
									resNode.find('message').each(function(){
									var row = $('<p data-clone-id="' + $(this).find('node-id').text() + '">' + $(this).find('probe-message').text() + '</p>');
									var ruleType = $(this).find('rule-type').text();
									container.find('#probe_' + ruleType).append(row);
									});
									$('#infoDivContent > [id*=validationDivNode]').remove();
									$('#infoDivContent > [id*=DivNode]').removeClass('active');
									$('#infoDivContent').append($('<div id="validationDivNode" class="active" style="height: 668px;"><p>Validation Errors</p></div>'));
									$('#validationDivNode').append(container)
									$('#validationDivNode').append(styleNode);
									$('#validationDivNode label[for]').remove();
									return false;
								}
							}else{	
								resNode.find('message').each(function(){
									
									if($(this).find('remove-display').text() == "true"){
										return;
									}
									
									var ruleType = $(this).find('rule-type').text();
									var errorId = $(this).find('error-id').text();
									var queryTo = $(this).find('query-role').text();

									var row = $('<div class="row" />');
									var left = $('<div class="col s2" />');
									var right = $('<div class="col s10" />');
									
									if(ruleType == "DTD-Validator"){
										left.append('<p> Rule ID: ' + $(this).find('rule-id').text() + '</p>');
										left.append('<p> Line: ' + $(this).find('line').text() + '</p>');
										left.append('<p> Column: ' + $(this).find('column').text() + '</p>');	
										row.append(left);
									}else if(ruleType == "Schematron-Validator"){
										left.append('<p> Error ID: ' + errorId + '</p>');
										row.append(left);
									}
									
									
									right.append('<p>' + $(this).find('probe-message').text() + '</p>');
									right.append('<pre>' + $(this).find('xpath').text() + '</pre>');
									if($(this).find('text').text() != ""){
										right.append('<pre><b>' + $(this).find('text').text() + '</pre></b>');
									}else if($(this).find('xml').text() != ""){
										right.append('<pre><b>' + $(this).find('xml').text() + '</pre><b>');
									}
									row.append(right);
									container.find('#probe_' + ruleType).append(row);

									if($(this).find('query:not(:empty)').length > 0 && $(this).find('xpath:not(:empty)').length > 0){
										var queryText = $(this).find('query:not(:empty)').html();
										var xpath = $(this).find('xpath:not(:empty)').text();
										if(xpath.match(/ref\[(\d+)\]$/)){
											var refPosition = xpath.match(/ref\[(\d+)\]$/)[1];
											refPosition = (refPosition)?parseInt(refPosition)-1:null;
											if($(kriya.config.containerElm + ' .jrnlRefText:eq(' + refPosition + ')').length > 0){
												var refNode = $(kriya.config.containerElm + ' .jrnlRefText:eq(' + refPosition + ')');
												if(refNode.find('.jrnlQueryRef[data-error-id="' + errorId + '"]').length == 0){
													var refDataID = refNode.attr('data-id');
													if(refDataID && queryText.match(/(X+){2}/)){
														var newCiteHTML   = citeJS.floats.getReferenceHTML(refDataID,'',true , '', '', '', '','');
														queryText = queryText.replace(/(X+){2}/, newCiteHTML);
													}

													//var queryTo = 'Publisher';
													queryTo = (queryTo)?queryTo:'publisher';
													//add query after re-ordering in body's first element
													var qid = eventHandler.query.action.createQueryDiv(queryTo, 'action');
													$('#' + qid).find('.query-from').html(kriyaEditor.user.name);
													$('#' + qid).attr('data-query-action', 'probe-validation');
													$('#' + qid).attr('data-error-id', errorId);

													var queryContent = $('#' + qid).find('.query-content');
													queryContent.attr('data-assigned-by', kriyaEditor.user.name);
													$(queryContent).html(queryText);
													kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);
													
													var startNode = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
													$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', queryTo);
													$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
													$(startNode).attr('data-query-type', 'action');
													$(startNode).attr('data-query-action', 'probe-validation');
													$(startNode).attr('data-error-id', errorId);
													
													$(refNode).prepend(startNode);
													kriyaEditor.settings.undoStack.push(refNode[0]);
												}
											}
										}
									}
								});

								popper.find('.probeResultContainer').html(container);
								popper.find('.probeResultContainer').append(styleNode);

								if(resNode.find('message signOff-allow:contains("false")').length == 0){
									popper.find('.proceedingSignoff').removeClass('hidden');
								}else{
									popper.find('.proceedingSignoff').addClass('hidden');
								}

								kriya.popUp.showEmptyPopUp(popper, $);

								if(kriyaEditor.settings.undoStack && kriyaEditor.settings.undoStack.length > 0){
									kriyaEditor.init.addUndoLevel('probe-validation');
								}

								stataus = false;
							}
						}
						if(onSuccess && typeof(onSuccess) == "function"){
							onSuccess(stataus);
						}
					}else{
						if(onError && typeof(onError) == "function"){
							onError("Empty response.");
						}	
					}
				}, function(err){
					if(onError && typeof(onError) == "function"){
						onError(err);
					}
				});
			},
			validate: function(){
				var validatedMessage = '';
				var normalQueries = $('.query-div.' + kriya.config.content.role + '[data-type="action"]');
				var notReplied = 0;
				$('.query-div.unanswered').removeClass('unanswered');
				$(normalQueries).each(function(){
					if ($(this).find('.query-holder.data-reply, .query-holder.data-answered').length == 0){
						$(this).addClass('unanswered');
						notReplied++;
					}
				});

				/*var actionQueries = $('.query-div[data-query-action]').not(normalQueries);
				$(actionQueries).each(function(){
					$(this).removeClass('unanswered');
					if ($(this).find('.query-holder.data-answered').length == 0){
						$(this).addClass('unanswered');
						notReplied++;
					}
				});*/

				if (notReplied > 0) {
					validatedMessage += '<p>There are still ' + notReplied + ' unanswered queries. Please answer them<br/></p>';
					if ($('.query-div.unanswered').length > 0){
						//$('.nav-action-icon.notes-icon').trigger('click');
						$('.query-div.unanswered')[0].scrollIntoView()
					}
				}

				

				if (validatedMessage == ""){
					return true;
				}else{
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						content : validatedMessage,
						icon: 'icon-warning2'
					});
					return false;
				}
			},
			checkAuthorLogin: function(param, targetNode){
				//to check if the logged in author is the corresponding author, else stopping him from Approving the article
				var userEmail = $('#contentContainer').attr('data-user-email');
				if ($(kriya.config.containerElm).find('.jrnlAuthorGroup[data-contact-author="yes"]').length > 0){
					var authorEmail = $(kriya.config.containerElm).find('.jrnlAuthorGroup[data-contact-author="yes"] .jrnlEmail').text();
					var authorName = $(kriya.config.containerElm).find('.jrnlAuthorGroup[data-contact-author="yes"] .jrnlAuthor .jrnlSurName').text();
					if (authorEmail != userEmail){
						kriya.notification({title:'Warning', type:'error', timeout:10000, content:'Please ask the corresponding Author (' + authorName + ') to Approve this article', icon:'icon-warning2'});
						return false;
					}else{
						return true;
					}
				}else if ($(kriya.config.containerElm).find('.jrnlAuthorGroup[corresp="yes"]').length > 0){
					var authorEmail = $(kriya.config.containerElm).find('.jrnlAuthorGroup[corresp="yes"] .jrnlEmail').text();
					var authorName = $(kriya.config.containerElm).find('.jrnlAuthorGroup[corresp="yes"] .jrnlAuthor .jrnlSurName').text();
					if (authorEmail != userEmail){
						kriya.notification({title:'Warning', type:'error', timeout:10000, content:'Please ask the corresponding Author (' + authorName + ') to Approve this article', icon:'icon-warning2'});
						return false;
					}else{
						return true;
					}
				}else{
					kriya.notification({title:'Warning', type:'error', timeout:10000, content:'Please ask the Publisher to Approve this article', icon:'icon-warning2'});
					return false;
				}
			},
			internalValidation: function(param, targetNode){
				$('#infoDivContent > [id*=validationDivNode]').remove();
				$('#infoDivContent > [id*=DivNode]').removeClass('active');
				$('#infoDivContent').append($('<div id="validationDivNode" class="active" style="height: 668px;"><p>Validation Errors</p></div>'));

				$('#contentDivNode > div').each(function(){
					var clonedNode = $(this);
					//get all para nodes
					$(clonedNode).find('p,h1,h2,h3,h4,h5,h6,span[class^="Ref"], p[class$="Caption"] span.label').each(function(){
						if (/jrnlDeleted|Authors|jrnlCitation|Copyright|floatHeader/i.test($(this).attr('class'))){
							return true;
						}
						if(($(this).attr('data-track') == "del") || ($(this).closest('.del,.forceJustify,.jrnlQueryRef,[data-track="del"],.label:not(:parent("p[class$=Caption]")),.removeNode,.hidden').length > 0)){
							return true;
						}
						var re = $(this).clone();
						$(re).find('.del,.forceJustify,.jrnlQueryRef,[data-track="del"],.label,.removeNode,.hidden').remove();
						var appendForUser = false;
						//check if the node has double space, leading or trailing space
						if (/[\u0020|\u2009][\u0020|\u2009]|^[\u0020|\u2009]|[\u0020|\u2009]$/.test($(re).text())){
							var leading = false, trailing = false;
							if (/^[\u0020|\u2009]/.test($(re).text())){
								leading = true;
							}
							if (/[\u0020|\u2009]$/.test($(re).text())){
								trailing = true;
							}
							re = getTralingSpaces($(this).clone()[0], leading, trailing);
							re = displayInvalidNodes(re, false);
							if ($(re[0]).find('.user-intervention').length > 0){
								appendForUser = true;
								$(re[0]).find('.user-intervention[data-type]').removeAttr('data-type')
								re = $(re)[0]
							}
						}
						if (!appendForUser){
							re = $(this).clone()[0];
						}
						$(re).find('[class$=Ref],i,em,b,strong').each(function(){
							if(($(this).attr('data-track') == "del") || ($(this).closest('.del,.forceJustify,.jrnlQueryRef,[data-track="del"],.label,.removeNode,.hidden').length > 0)){
								return true;
							}
							var xref = $(this).clone();
							xref.find('.del,.forceJustify,.jrnlQueryRef,[data-track="del"],.hidden').remove();
							if (/^[\u0020|\u2009]|[\u0020|\u2009]$/.test($(xref).text())){
								var leading = false, trailing = false;
								if (/^[\u0020|\u2009]/.test($(xref).text())){
									leading = true;
								}
								if (/[\u0020|\u2009]$/.test($(xref).text())){
									trailing = true;
								}
								xref = getTralingSpaces($(this)[0], leading, trailing);
								appendForUser = true;
								$(xref).find('.user-intervention[data-type]').each(function(){
									var x = $(this).closest('[class$=Ref],i,em,b,strong');
									if ($(this).attr('data-type') == 'leading'){
										x.before($(this))
									}else if ($(this).attr('data-type') == 'trailing'){
										x.after($(this))
									}
								})
							}
						});
						if (appendForUser){
							if ($(this)[0].hasAttribute('id')){
								$(re)[0].setAttribute('data-clone-id', $(this).attr('id'));
								$('#validationDivNode').append($(re)[0]);
							}else if ($(this).hasClass('jrnlTblFootText')){
								$(re)[0].setAttribute('data-parent-clone-id', $(this).parent().attr('id'));
								$('#validationDivNode').append($(re)[0]);
							}else if ($(this).hasClass('jrnlFNPara')){
								$(re)[0].setAttribute('data-parent-clone-id', $(this).parent().parent().attr('id'));
								$('#validationDivNode').append($(re)[0]);
							}else{
								// do nothing - do not add to validation
							}
							$(re)[0].setAttribute('class', 'user-validation');
							$(re)[0].removeAttribute('id');
						}
					})
				})
				if ($('#validationDivNode .user-validation').length > 0){
					var content = $('div.WordSection1')[0].outerHTML;
					var parameters = {'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'doi':kriya.config.content.doi, 'content':encodeURIComponent(content), 'fileName': 'internalValidation'};
					kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
						console.log('Data Saved');
					})
					$('#validationDivNode p:first').append('<span class="btn btn-small validate-all-ref-btn" data-channel="welcome" data-topic="signoff" data-event="click" data-message="{\'funcToCall\': \'acceptInternalValidation\'}">Remove All Spaces</span>');
					return false;
				}else{
					return true;
				}
				// loop through every node and check for double space and tag it
				function displayInvalidNodes(re, doubleSp){
					var c = re.childNodes;
					for (var i = 0, cl = c.length; i < cl; i++){
						var node = c[i];
						if (node.nodeType == 3){
							if (/^[\u0020|\u2009]$/.test(node.nodeValue)){
								if (doubleSp){
									//node.nodeValue = '';
									$(node).wrap('<span class="user-intervention">')
									doubleSp = false;
								}else{
									doubleSp = true;
								}
							}else if (/^[\u0020|\u2009]/.test(node.nodeValue)){
								if (doubleSp){
									var thispace = false;
									if(node.nodeValue.match(/^[\u2009]/)){
										thispace = true
									}
									if(thispace && $(node).parent()){
										var r = $(node).parent();
										var newnode = r[0].previousSibling
										if(newnode && newnode.nodeType == 3 && newnode.nodeValue.match(/[\u0020]$/)){
											newnode.nodeValue = newnode.nodeValue.replace(/[\u0020]$/, '');
											$(newnode).after('<span class="user-intervention"> </span>')
										}
									}else{
										node.nodeValue = node.nodeValue.replace(/^[\u0020|\u2009]/, '');
										$(node).before('<span class="user-intervention"> </span>')
									}
									doubleSp = false;
								}
							}else{
								doubleSp = false;
							}
							if (/[\u0020|\u2009]$/.test(node.nodeValue)){
								doubleSp = true;
							}
						}else{
							if(doubleSp && $(node)[0].nodeName == "IMG"){
								doubleSp = false;
							}
							if ($(node).hasClass('del') || $(node).hasClass('forceJustify') || $(node).hasClass('hidden') || $(node).hasClass('label')  || $(node).hasClass('removeNode') || $(node).hasClass('jrnlQueryRef') || ($(node)[0].hasAttribute('data-track') && $(node).attr('data-track')=="del")){
								//skip
							}else{
								nodeDetails = displayInvalidNodes(node, doubleSp)
								node = nodeDetails[0];
								doubleSp = nodeDetails[1];
							}
						}
					}
					return [re, doubleSp];
				}
				// loop through every node and check for leading/trailing space and tag it
				function getTralingSpaces(node, leading, trailing){
					var textNodes = textNodesUnder(node);
					var tnLength = textNodes.length;
					if (leading){
						for(var t=0;t<tnLength;t++){
							var tNode = textNodes[t];
							if ($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef, .label:not(:parent("p[class$=Caption]")), .removeNode').length == 0){
								var firstNodeText = tNode.nodeValue;
								if (firstNodeText.match(/^([\u0020]+)/g)){
									var matchVal = firstNodeText.match(/^([\u0020]+)/g);
									tNode.nodeValue = firstNodeText.replace(/^([\u0020]+)/, '');
									var trackNode = $('<span class="user-intervention" data-type="leading">' + matchVal[0] + '</span>');
									$(trackNode).insertBefore(tNode);
									break;
								}else{
									break;
								}
							}
						}
					}
					if (trailing){
						for(var t=tnLength-1;t>=0;t--){
							var tNode = textNodes[t];
							if($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef, .label:not(:parent("p[class$=Caption]")), .removeNode').length == 0){
								var lastNodeText = tNode.nodeValue;
								if(lastNodeText.match(/([\u0020]+)$/g)){
									var matchVal = lastNodeText.match(/([\u0020]+)$/g);
									tNode.nodeValue = lastNodeText.replace(/([\u0020]+)$/, '');
									var trackNode = $('<span class="user-intervention" data-type="trailing">' + matchVal[0] + '</span>');
									$(trackNode).insertAfter(tNode);
									break;
								}else{
									break;
								}
							}
						}
					}
					return node
				}
				function getNodes(re, trailing){
					var c = re.childNodes;
					for (var i = 0, cl = c.length; i < cl; i++){
						var node = c[i];
						if (node.nodeType == 3){
							if (/^ $/.test(node.nodeValue)){
								if (trailing){
									node.nodeValue = '';
									trailing = false;
								}else{
									trailing = true;
								}
							}else if (/^ /.test(node.nodeValue)){
								if (trailing){
									node.nodeValue = node.nodeValue.replace(/^ /, '');
									trailing = false;
								}
							}else{
								trailing = false;
							}
							if (/ $/.test(node.nodeValue)){
								trailing = true;
							}
						}else{
							if ($(node).hasClass('del')){
								//skip
							}else{
								nodeDetails = getNodes(node, trailing)
								node = nodeDetails[0];
								trailing = nodeDetails[1];
							}
						}
					}
					return [re, trailing];
				}
			},
			acceptInternalValidation: function(param, targetNode){
				$('#validationDivNode .user-validation').each(function(){
					var dataID = $(this).attr('data-clone-id');
					if (dataID == undefined){
						dataID = $(this).attr('data-parent-clone-id');
					}
					if ($('#contentDivNode *[id="' + dataID + '"]').length > 0){
						$(this).find('.user-intervention[data-type]').contents().unwrap();
						$(this).find('.user-intervention').remove();
						if ($(this)[0].hasAttribute('data-parent-clone-id') && $('#contentDivNode *[id="' + dataID + '"]').hasClass('jrnlTblFoot')){
							$('#contentDivNode *[id="' + dataID + '"]').find('.jrnlTblFootText').html($(this).html());
						}else if (!$(this)[0].hasAttribute('data-parent-clone-id')){
							$('#contentDivNode *[id="' + dataID + '"]').html($(this).html());
						}else if ($(this)[0].hasAttribute('data-parent-clone-id') && $('#contentDivNode *[id="' + dataID + '"] .jrnlFNPara').length > 0){
							$('#contentDivNode *[id="' + dataID + '"] .jrnlFNPara').html($(this).html());
						}
						$(this).remove();
						if ($('#contentDivNode *[id="' + dataID + '"]').closest('[class$=Block]').length > 0){
							kriyaEditor.settings.undoStack.push($('#contentDivNode *[id="' + dataID + '"]').closest('[class$=Block]'));
						}else{
						kriyaEditor.settings.undoStack.push($('#contentDivNode *[id="' + dataID + '"]'));
						}
					}
				});
				kriyaEditor.init.addUndoLevel('Delete space');
			},
			reportIssueModal: function(param, targetNode){
				if (kriya.config.content.stage == 'support'){
					jQuery.ajax({
						type: "GET",
						url: "/api/articlenode?customerName=" + kriya.config.content.customer + "&projectName=" + kriya.config.content.project + "&doi=" + kriya.config.content.doi + "&xpath=//production-notes/note[@type='support'][last()]",
						contentType: "application/xml; charset=utf-8",
						dataType: "xml",
						success: function (msg) {
							$('[data-component="resolveIssue_edit"]').removeAttr('data-parent-note-id');
							// to get comments information from xml and updating it in category option - aravind
							jQuery.ajax({
								async: false,
								type: "GET",
								url: "/api/articlenode?customerName=" + kriya.config.content.customer + "&projectName=" + kriya.config.content.project + "&doi=" + kriya.config.content.doi + "&xpath=//workflow/stage[last()]/comments",
								contentType: "application/xml; charset=utf-8",
								dataType: "xml",
								success:function (result){
									if (result && !result.error) {
										if($(result).find('comments').length && $(result).find('comments').text() != ''){
											var presentCategory = $(result).find('comments').text().split(',');
											if(presentCategory.length > 0){
												var priorityDetials = $(result).find('comments').text().replace(presentCategory[0],'');
											}
											if(priorityDetials && presentCategory.length > 0){
												if($('[data-component="resolveIssue_edit"] select[data-class="category"]').length > 0){
													$('[data-component="resolveIssue_edit"] select[data-class="category"]').val(presentCategory[0]);
													$('[data-component="resolveIssue_edit"] select[data-class="category"]').attr('data-focusin',presentCategory[0]).attr('data-priority',priorityDetials);
												}
											}
										}
									}
								}
							});
							if (msg && !msg.error) {
								var commentObj = $(msg);
								if ($(commentObj).find('note').length > 0){
									var parentID = $(commentObj).find('note').attr('id');
									$('[data-component="resolveIssue_edit"]').attr('data-parent-note-id', parentID);
								}
								kriya.popUp.openPopUps($('[data-component="resolveIssue_edit"]'));
							}else{
								kriya.popUp.openPopUps($('[data-component="resolveIssue_edit"]'));
							}
						}, error: function(){
							kriya.popUp.openPopUps($('[data-component="resolveIssue_edit"]'));
						}
					})
				}else{
					// to handle if report issue is activated from duplicate query popup / aravind
					if($(targetNode).closest('[data-component="confirmation_edit"]').length > 0){
						$('[data-component="reportIssue_edit"]').find('[data-class="category"]').val('editor').addClass('disabled');
						if(param && param.msg){
							$('[data-component="reportIssue_edit"]').find('.notesContainer').text(param.msg);
						}else{
							$('[data-component="reportIssue_edit"]').find('.notesContainer').text('Query Duplication');
						}
						$('[data-component="reportIssue_edit"]').find('.com-close').addClass('hidden');
						kriya.popUp.closePopUps($('[data-component="confirmation_edit"]'));
						kriya.popUp.openPopUps($('[data-component="reportIssue_edit"]'));
					}else{
						$('[data-component="reportIssue_edit"]').find('[data-class="category"],[data-class="priority"]').val('');
						kriya.popUp.openPopUps($('[data-component="reportIssue_edit"]'));
					}					
				}
			},
			openUploadNotesFile: function(param, targetNode){
				kriya.popUp.getSlipPopUp($('[data-component="uploadNotesFile_edit"]')[0], $('[data-component="reportIssue_edit"]'));
			},
			uploadNotesFile: function(targetNode){
				if ($(targetNode).attr('src') != undefined || $(targetNode).attr('src') != ""){
					var fileContent = '<span class="comment-file"><img src="' + $(targetNode).attr('src') + '" data-original-name="' + $(targetNode).attr('data-original-name') + '" data-primary-extension="' + $(targetNode).attr('data-primary-extension') + '"/></span>';
					$('[data-component="reportIssue_edit"]').find('div[contenteditable].notesContainer').append(fileContent)
				}
				kriya.popUp.closePopUps($('[data-component="uploadNotesFile_edit"]'));
			},
			reportIssue: function(param, targetNode, info){
				if(param != undefined){
					$('.la-container').fadeIn();
					var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
					category = param.category+','+param.priority+ ',' + kriya.config.content.stageFullName;
					var issueData = '<div class="issue-data">';
					issueData += '<div class="category-info"><span class="cause">' + param.cause + '</span><span class="category">' + param.category + '</span><span class="priority">' + param.priority + '</span><span class="stage">' + kriya.config.content.stageFullName + '</span></div>';
					issueData += '<div class="issue-notes">' + param.info + '</div>';
				}else if(!info){
					var component = $(targetNode).closest('[data-component]');
					var notesContainer = $(component).find('div[contenteditable].notesContainer');
					var category = "", categoryInfo = "";
					if (component.attr('data-component') == 'reportIssue_edit'){
						if ( !/[a-z]/i.test(notesContainer.html()) || $(component).find('[data-class="cause"]').val() == "" || $(component).find('[data-class="category"]').val() == "" || $(component).find('[data-class="priority"]').val() == ""){
							return false;
						}
						var cause = $(component).find('[data-class="cause"] option:selected').text();
						if ($(component).find('[data-class="cause"]').val() == 'others'){
							if ($(component).find('[data-class="other-reason"]').text() == ''){
								return false;
							}
							cause = $(component).find('[data-class="other-reason"]').text();
						}
						category = $(component).find('[data-class="category"]').val() + ',' + $(component).find('[data-class="priority"]').val() + ',' + kriya.config.content.stageFullName;
						categoryInfo = '<div class="category-info"><span class="cause">' + cause + '</span><span class="category">' + $(component).find('[data-class="category"] option:selected').text() + '</span><span class="priority">' + $(component).find('[data-class="priority"] option:selected').text() + '</span><span class="stage">' + kriya.config.content.stageFullName + '</span></div>';
					}
					if ( !/[a-z]/i.test(notesContainer.html())){
						return false;
					}
					if (category == "" && component.attr('data-component') != 'reportIssue_edit'){
						if ($(component).find('[data-class="what-category"]').val() == "" || $(component).find('[data-class="who-category"]').val() == "" || $(component).find('[data-class="why-category"]').val() == "" || $(component).find('[data-class="where-category"]').val() == ""){
							return false;
						}
						category = $(component).find('[data-class="what-category"]').val() + ',' + kriya.config.content.stageFullName;
						categoryInfo = '<div class="category-info"><span class="what">' + $(component).find('[data-class="what-category"] option:selected').text() + '</span><span class="who">' + $(component).find('[data-class="who-category"] option:selected').text() + '</span><span class="why">' + $(component).find('[data-class="why-category"] option:selected').text() + '</span><span class="where">' + $(component).find('[data-class="where-category"] option:selected').text() + '</span><span class="stage">' + kriya.config.content.stageFullName + '</span></div>';
					}
					$('.la-container').fadeIn();
					var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
					var issueData = '<div class="issue-data">';
					issueData += categoryInfo;
					issueData += '<div class="issue-notes">' + notesContainer.html().replace(/(<img[^>]+)>/g, '$1/>') + '</div>';
				}else{
					$('.la-container').fadeIn();
					var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
					var issueData = '<div class="issue-data">';
					issueData += '<div class="category-info"><span class="cause">Could not progress</span><span class="category">editor</span><span class="priority">High</span><span class="stage">' + kriya.config.content.stageFullName + '</span></div>';
					issueData += '<div class="issue-notes">'+info+'</div>';
				}
				issueData += '</div>';
				var commentJSON = {
					"content": issueData,
					"fullname": kriyaEditor.user.name,
					"created": currTime
				}
				if (component && component.attr('data-component') == 'resolveIssue_edit' && $(component)[0].hasAttribute('data-parent-note-id')){
					commentJSON.parent = $(component).attr('data-parent-note-id');
				}
				var parameters = {
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project, 
					'doi':kriya.config.content.doi 
				};
				if (component && component.attr('data-component') == 'resolveIssue_edit'){
					parameters.releaseHold = 'true';
					parameters.holdComment = category;
				}else{
					parameters.stageName = 'Support';
					parameters.holdComment = category;
				}
				kriya.general.sendAPIRequest('addstage', parameters, function(res){
					$.ajax({
						type: 'POST',
						url: '/api/save_note_new',
						processData: false,
						contentType: 'application/json; charset=utf-8',
						data: JSON.stringify({
							"content": commentJSON,
							"doi": kriya.config.content.doi,
							"customer": kriya.config.content.customer,
							"journal": kriya.config.content.project,
							"noteType": "support",
							"logging": 'true'
						}),
						success: function (comment) {
							if (comment && comment.id){
								if(info && param == undefined){
									$('.la-container').fadeOut();
									$('[data-component="confirmation_edit"] .popupFoot').html('');
									$('[data-component="confirmation_edit"] .popupFoot').append('Your issue has been reported successfully.<br> Please close the browser to exit the system.');
									kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
								}else{
									$('.la-container').fadeOut();
									$('body .messageDiv .message').html('Your issue has been reported successfully.<br> Please close the browser to exit the system.');
									$('body .messageDiv').removeClass('hidden');
									$('body .messageDiv .feedBack').removeClass('hidden');
									kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
								}
							}else{
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed.',
									type: 'error',
									content: 'Issue in adding ticket to support. Please try again!',
									icon : 'icon-info'
								});
							}
						},
						error: function(err){
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Failed.',
								type: 'error',
								content: 'Issue in adding ticket to support. Please try again!',
								icon : 'icon-info'
							});
						}
					});
				},function(res){
					$('.la-container').fadeOut()
					kriya.notification({
						title: 'Failed.',
						type: 'error',
						content: 'Signoff article was failed.',
						icon : 'icon-info'
					});
				});
			},
			// modified issue category saved to xml and logoff the article
			changeIssueCategory: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var presentCategory = $(popper).find('select[data-class="category"]').attr('data-priority');
				if($(popper).find('select[data-class="category"]').length > 0){
					var selectedCategory = $(popper).find('select[data-class="category"]').val();
					if(selectedCategory == ''){
						return false;
					}
					if(presentCategory != undefined){
						selectedCategory = selectedCategory + presentCategory;
					}
					$('.la-container').fadeIn();
					$.ajax({
						type: "POST",
						url: "/api/updatearticle",
						data: {
							'customerName':kriya.config.content.customer,
							'projectName':kriya.config.content.project,
							'doi':kriya.config.content.doi,
							'xpath':'//workflow/stage[last()]/comments',
							'xmlFrag':'<comments>'+selectedCategory+'</comments>',
							'insertInto':'//custom-meta-group'
						},
						success: function(res){
							eventHandler.welcome.signoff.logOut();
						},
						error: function(err){
							eventHandler.welcome.signoff.logOut();
						}
					});
				}
			},
			restoreVersion: function(param, targetNode){
				if (/version\?/.test(window.location.href)){
					$('.la-container').fadeIn();
					$.ajax({
						type: "GET",
						url: "/api/articlenode?customerName="+kriya.config.content.customer+"&projectName="+kriya.config.content.project+"&bucketName=AIP&xpath=//workflow/stage[last()]&doi=" + kriya.config.content.doi,
						contentType: "application/xml; charset=utf-8",
						dataType: "xml",
						success: function (data) {
							if (data){
								dataNode = data.documentElement;
								if ($(dataNode).find('> name').text() == $('#contentContainer').attr('data-stage-name')){
									if (! /logged-off/i.test($(dataNode).find('job-logs log').last().find('status').text())){
										var message = 'Article is either opened or in in-active stage by ' + $(dataNode).find('job-logs log').last().find('username').text() + '. Please log-out from the article and try restoring this version!';
										kriya.notification({
											title : 'ERROR',
											type  : 'error',
											timeout : 10000,
											content : message,
											icon: 'icon-warning2'
										});
										$('.la-container').fadeOut();
									}else{
										var getArray = {};
										if(window.location.toString().indexOf('?') !== -1) {
											var query = window.location.toString().replace(/^.*?\?/, '').replace(/#.*$/, '').split('&');
											for(var i=0, l=query.length; i<l; i++) {
											   var aux = decodeURIComponent(query[i]).split('=');
											   getArray[aux[0]] = aux[1];
											}
										}
										var articleLocation = '/review_content?doi='+kriya.config.content.doi+'&customer='+kriya.config.content.customer+'&project='+kriya.config.content.project
										var param = {"customer": kriya.config.content.customer, "project": kriya.config.content.project, "doi": kriya.config.content.doi, 'versions': getArray.versions}
										$.ajax({
											url: '/api/restoreversion',
											type: 'POST',
											data: param,
											success: function(data){
												if (data && data.status && data.status == "updated"){
													$('#lock-screen').show();
													$('#lock-screen').html('<div class="row errorPage" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">The version has been restored and will be re-directed shortly.</div>');
													setTimeout(function(){
														window.location.href = articleLocation;
													}, 2000);
												}else{
													kriya.notification({
														title : 'ERROR',
														type  : 'error',
														timeout : 10000,
														content : 'Failed to restore',
														icon: 'icon-warning2'
													});
												}
												$('.la-container').fadeOut();
											},
											error: function(e){
												kriya.notification({
													title : 'ERROR',
													type  : 'error',
													timeout : 10000,
													content : 'Failed to restore',
													icon: 'icon-warning2'
												});
												$('.la-container').fadeOut();
											},
										});
									}
								}else{
									var message = 'Currently this article is in ' + $(dataNode).find('> name').text() + ' stage but you are trying to restore a version from ' + $('#contentContainer').attr('data-stage-name');
									kriya.notification({
										title : 'ERROR',
										type  : 'error',
										timeout : 10000,
										content : message,
										icon: 'icon-warning2'
									});
									$('.la-container').fadeOut();
								}
							}
						}
					})
				}
			}
		}
	},
	eventHandler.menu = {
		edit: {
		    undoLastAction: function() {
				$(kriyaEditor.settings.undo).trigger('click');
				$('.undo-modal').remove();
			},
			redoLastAction: function() {
				$(kriyaEditor.settings.redo).trigger('click');
			},
			addUndoModal: function(message){
				if (message != undefined){
					$('.undo-modal').remove();
					$('body').append('<div class="undo-modal">' + message + ' <span data-channel="menu" data-topic="edit" data-event="click" data-message="{\'funcToCall\': \'undoLastAction\'}">Undo</span></div>');
					setTimeout(function(){
						$('.undo-modal').remove();
					}, 8000);
				}
			},
			clearFormating: function(param,targetNode){
				/*if(kriya.selection){
					var formatTag = kriya.selection.startContainer.parentElement.tagName;
					if(formatTag.match(/b|strong|i|sup|sub|em/)){
						if($(kriya.selection.startContainer.parentElement).parent('.sty')){
							var cid = $(kriya.selection.startContainer.parentElement).parent('.sty').attr('data-cid');
							$('#changesDivNode'+' [data-rid="' + cid + '"]').find('.reject-changes').trigger('click');
						}else{
							$(kriya.selection.startContainer.parentElement).contents().unwrap();
						}
					}
				}*/
			},
			toggleNavPane: function(param,targetNode){
				$('.class-highlight').addClass('hidden');
				$('.highlight.findOverlay').remove(); // remove search highlights - priya 
				//get active nodes and remove class "active" 
				$('.nav-action-icon').removeClass('active');
				$('.navDivContent > div').removeClass('active');
				//add "active" class to current nodes
				$('#'+param).addClass('active');
				targetNode.addClass('active');
				if(param == 'queryDivNode'){ // to highlight all unanswered query
					var normalQueries = $('.query-div[data-type="action"]');
					var scrollToFirstUnansweredQuery = true;
					$(normalQueries).each(function(){
						$(this).removeClass('unanswered');
						var qRole = $(this).find('.query-content').attr('data-assigned-by-role');
						qRole = qRole.toLowerCase();
						if ($(this).find('.query-holder.data-reply').length == 0&& qRole != kriya.config.content.role){
							if(scrollToFirstUnansweredQuery){
								$(this)[0].scrollIntoView();
								scrollToFirstUnansweredQuery = false;
							}							
							$(this).addClass('unanswered');					
						}
					});
				}
				console.log('Within showHideMenu function');
			},
			showHideMenu: function(param,targetNode){
				//get active nodes and remove class "active" 
				if (! $(targetNode).closest('.kriyaMenuControl').hasClass('active')){
					$('.kriyaMenuControl.active').removeClass('active');
					$('[data-menu-selector]').addClass('hidden');
					$('[data-menu-selector]:first').removeClass('hidden');
					if (kriya.selection && $(targetNode).closest('.kriyaMenuControl').find('[data-menu-selector]').length > 0){
						var node = kriya.selection.commonAncestorContainer;
						if ($(node).closest('div.front,div.body,div.back,div.sub-article').length > 0){
							$('[data-menu-selector]').addClass('hidden');
							var blockClass = $(node).closest('div.front,div.body,div.back,div.sub-article').attr('class');
							$('*[data-menu-selector*=" ' + blockClass + ' "]').removeClass('hidden');
						}
					}
					$(targetNode).closest('.kriyaMenuControl').addClass('active');
				}else{
					$('.kriyaMenuControl.active').removeClass('active');
				}
				/*targetNode.closest('.tabMenu').find('.active').removeClass('active');
				$('#'+param.datarid).closest('.subMenu').find('.active').removeClass('active');
				//add "active" class to current nodes
				$('#'+param.datarid).addClass('active');
				targetNode.addClass('active');
				console.log('Within showHideMenu function');*/
			},
			content: function(param, targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;								
				if(param && param.command){					
					// to convert list option as unlist option if selected para is already an list type - aravind
					if(($(target).closest('[data-message]').length > 0) && ($(target).closest('[data-message]').attr('data-unlist')) && ($(target).closest('[data-message]').hasClass('selectedList'))){
						param.command = 'outdent';
					}				
					if(param.command.match(/color/i)){
						param.argument = $(target).attr('data-color');
					}
					//#535
					if(param.command.match(/continueList/i)){
						this.continueList(param, targetNode);
					}else{
						kriya.actions.execCommand(param.command,param.argument);
					}		
				}
			},
			continueList:function(param, targetNode){
				if(param.argument === 'updateStartValue'){
					if($(kriya.config.activeElements).closest('ol').length > 0){

						var value = $('[data-component="jrnlContinueList"] [data-input="true"]')[0].innerText;
						var type = $(kriya.config.activeElements).closest('ol').attr('type');
						if((value.match(/[a-z]/) && type == 'lower-alpha') || (value.match(/[A-Z]/) && type == 'upper-alpha') || (value.match(/[0-9]/) && type == 'decimal') || (value.match(/[α-ω]/) && type == 'lower-greek') || (value.match(/^m{0,4}(cm|cd|d?c{0,3})(xc|xl|l?x{0,3})(ix|iv|v?i{0,3})$/) && type == 'lower-roman') || (value.match(/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/) && type == 'upper-roman')){
							var number = 1;
							switch(type){
								case 'lower-alpha':
								case 'upper-alpha':
									number = fromAlpha(value);
									break;
								case 'lower-greek':
									number = fromGreek(value);
									break;
								case 'lower-roman':
								case 'upper-roman':
									number = fromRoman(value);
									break;
								case 'decimal':
									number = value;
									break;
							}
							$(kriya.config.activeElements).closest('ol').attr('start', number);
						}else{
							var message = '<div class="row">Invalid characters.</div>';
							kriya.notification({
								title : 'ERROR',
								type  : 'error',
								timeout : 10000,
								content : message,
								icon: 'icon-warning2'
							});
							
						}
					}
					kriya.popUp.closePopUps($(targetNode).closest('[data-type]'));
					kriyaEditor.settings.undoStack.push($(kriya.config.activeElements).closest('ol[id]'));
					kriyaEditor.init.addUndoLevel('continue-list');
				}else{
					var componentName = param.command;
					//kriya.config.activeElements = targetNode;
					var component = $('[data-component="jrnlContinueList"]');
					kriya['styles'].init($(kriya.config.activeElements), 'jrnlContinueList');
					component.find('[data-input]').removeClass('hidden');
					component.find('[data-class]').addClass('hidden');
					//Hide query button for add keyword
					component.find('.popup-menu [data-pop-type="edit"]').addClass('hidden');
					component.find('.popupHead').addClass('hidden');
					//if($(kriya.config.activeElements).attr("data-name")=="jrnlContinueList"){
						component.find('.popupHead[data-pop-type="new"]').removeClass('hidden');
					//}
					if($(kriya.config.activeElements).closest('ol').attr('start')){
						$('[data-component="jrnlContinueList"] [data-input="true"]').val($(kriya.config.activeElements).closest('ol').attr('start'));
					}
					
				}
			},
			//End of #535
			formatting: function(param,targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;
				if(param && param.command){
					if(param.command.match(/color/i)){
						var argument = $(target).attr('data-color');
					}
					tracker.insertFormats(param.command, argument, param.action);
					eventHandler.menu.style.updateMenuFormat();
				}
			}
		},
		query: {
			deleteQuery: function(param,targetNode){
				//if the cursor placed at the tracked node
				var trackParent = $(kriya.selection.startContainer).parents('[data-rid *="Q"]');
				var clonedSelection = kriya.selection.cloneContents();
				var selectedQuery = $('[data-rid *= "Q"].selected');
				if(trackParent.length > 0){
					$('#queryDivNode #' + trackParent.attr('data-rid').replace(/\s+/,'') + ' .query-delete').trigger('click');
				}else if($(kriya.selection.startContainer).prev('.jrnlQueryRef').length > 0){
					//if cursor is within the query start and end tag
					var rid = $(kriya.selection.startContainer).prev('.jrnlQueryRef').attr('data-rid').replace(/\s+/,'');
					$('#queryDivNode #' + rid + ' .query-delete').trigger('click');
				}else if($(clonedSelection).find('[data-rid *= "Q"]').length > 0){
					//if the track elements with in the content selection
					$(clonedSelection).find('[data-rid *= "Q"]').each(function(){
						$('#queryDivNode #' + $(this).attr('data-rid').replace(/\s+/,'') + ' .query-delete').trigger('click');
					});
				}else if(selectedQuery.length > 0){
					$('#queryDivNode #' + selectedQuery.attr('data-rid').replace(/\s+/,'') + ' .query-delete').trigger('click');
					this.moveQuery({'type':'next'});
				}else{
					this.moveQuery({'type':'next'});
				}
			},
			moveQuery: function(param,targetNode){
				var queryEle = $("#queryDivNode .query-div");
				var selectedTrack = queryEle.filter(".selected");
				//if not the last track element
				if(param && param.type == "next" && queryEle.index(selectedTrack)+1 != queryEle.length && selectedTrack.length > 0){
					selectEle = queryEle.eq( queryEle.index(selectedTrack) + 1 );
				}else if(param && param.type == "prev" && queryEle.index(selectedTrack) != 0 && selectedTrack.length > 0){
					selectEle = queryEle.eq( queryEle.index(selectedTrack) - 1 );
				}else if(param && param.type == "prev"){
					selectEle = queryEle.last();
				}else{
					selectEle = queryEle.first();	
				}
				
				if(selectEle.length > 0){
					$('#'+selectEle.attr('id')).trigger('click');
				}
			}
		},
		track_changes: {
			showHideTrackChanges: function(param,targetNode){
				if($('#contentContainer[data-track-display="false"]').length > 0){
					$(targetNode).find('.icon-eye-blocked').addClass('icon-eye').removeClass('icon-eye-blocked');
					$('#contentContainer').attr('data-track-display','true');
					$('#changesDivNode').removeClass('hidden');
					if($("#clonedContent").find(".highlight.spelling").length>0){
						eventHandler.menu.spellCheck.checkDocument('',$(".icon-spell-check").parent()[0]); //to remove spell check highlights
					}
				}else{
					$(targetNode).find('.icon-eye').addClass('icon-eye-blocked').removeClass('icon-eye');
					$('#contentContainer').attr('data-track-display','false');
					$('#changesDivNode').addClass('hidden');
				}
				//to re-trigger find
				if ($('.highlight.findOverlay').length > 0 && $('.findSection .search').val() != ""){
					$('.highlight.findOverlay').remove();
					eventHandler.menu.findReplace.searchText();
				}
			},
			acceptRejectChanges: function(param,targetNode){
				if(param && param.type){
					var trackSelector   = '.del, .ins, .sty, .styrm, [data-track="del"], [data-track="ins"]';

					var trackParent     = $(kriya.selection.startContainer).parents(trackSelector);
					var clonedSelection = kriya.selection.cloneContents();
					
					var selectedNode    = $('.del.selected, [data-track="del"].selected, .ins.selected,[data-track="ins"].selected .sty.selected, .styrm.selected');
					if(trackParent.length > 0){
						$('#changesDivNode .change-div[data-rid="' + trackParent.attr('data-cid') + '"] .'+param.type).trigger('click');
					}else if($(clonedSelection).find(trackSelector).length >  0){
						//if the track elements with in the content selection
						$(clonedSelection).find(trackSelector).each(function(){
							$('#changesDivNode .change-div[data-rid="' + $(this).attr('data-cid') + '"] .'+param.type).trigger('click');
						});
					}else if(selectedNode.length > 0){
						$('#changesDivNode .change-div[data-rid="' + selectedNode.attr('data-cid') + '"] .'+param.type).trigger('click');
						this.moveSelection({'type':'next'});
					}else{
						this.moveSelection({'type':'next'});
					}
				}
			},
			highlightChange: function(param, targetNode){
				$('.change-div.selected').removeClass('selected');
				var trackId = $(targetNode).attr('data-rid');
				var trackNode = $(kriya.config.containerElm + ' [data-cid="'+trackId+'"]');
				$('.textSelection').remove();
				$(trackNode).each(function(){
					range = rangy.getSelection();
					sel  = range.getRangeAt(0);
					sel.selectNode($(this)[0]);
					//when we hide track changes at that time if we click on change cards in right navigation panal at that time code is breaking.
					// findReplace.general.highlight(range, 'textSelection');
					// $(this)[0].scrollIntoView();
					if($('#contentContainer[data-track-display="false"]').length > 0 || !$(this).is(':visible')){
						$(this).closest(':visible')[0].scrollIntoView();
					}else{
						findReplace.general.highlight(range, 'textSelection');
						$(this)[0].scrollIntoView();
					}
				});
				$(targetNode).addClass('selected');
				if (trackNode.length > 0) {
					sel.collapse(true);
					trackNode[0].scrollIntoView();
				}
			},
			moveSelection: function(param,targetNode){
				$('.textSelection').remove();
				var trackEle = $('.sty, .styrm, .ins, .del, [data-track="del"], [data-track="ins"]');
				var selectedTrack = trackEle.filter(".selected");
				//if not the last track element
				if(param && param.type == "next" && trackEle.index(selectedTrack)+1 != trackEle.length && selectedTrack.length > 0){
					selectEle = trackEle.eq( trackEle.index(selectedTrack) + 1 );
				}else if(param && param.type == "prev" && trackEle.index(selectedTrack) != 0 && selectedTrack.length > 0){
					selectEle = trackEle.eq( trackEle.index(selectedTrack) - 1 );
				}else if(param && param.type == "prev"){
					selectEle = trackEle.last();
				}else{
					selectEle = trackEle.first();	
				}
				
				if(selectEle.length > 0){
					selectedTrack.removeClass('selected');
					selectEle.addClass('selected');
					$('#changesDivNode .change-div[data-rid="' + selectEle.attr('data-cid') + '"]').trigger('click');
				}
			}
		},
		navigation:{
			scrollToObj: function(param,targetNode){
				$('.class-highlight').addClass('hidden');
				var panelId = $(targetNode).closest('[data-panel-id]').attr('data-panel-id');
				var blockNode = $(kriya.config.containerElm+' [data-id="'+ panelId +'"]');
				/*if(blockNode.closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').length > 0){
					blockNode = blockNode.closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup');
					panelId = blockNode.closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').attr('data-id');
				}*/
				
				//panelId = panelId.replace(/BLK_/g, '');
				var blockClass = $(blockNode).attr('class');
				if ($(kriya.config.containerElm+' [data-id="' + panelId + '"]').length > 0){
					if(blockNode.length > 0 && blockNode.find('.floatHeader').length > 0){
						blockNode.find('.floatHeader')[0].scrollIntoView();
					}else{
						$(kriya.config.containerElm+' [data-id="'+ panelId +'"]')[0].scrollIntoView();	
					}
					
					//to check whether the user has selected something in the card, if so do not highlight reference - JAI 06-04-2018
					range = rangy.getSelection();
					if (blockClass == 'jrnlRefText' && range.text().length == 0){
						var ref= $(kriya.config.containerElm+' [data-id="'+ panelId +'"]')[0];
						sel = range.getRangeAt(0);
						sel.setStart(ref, 0);
						sel.setEndAfter(ref);
						var h = findReplace.general.highlight(range, 'glow-element');
						range.removeAllRanges();
						setTimeout(function(){
							$(h).remove();
						},'3000	')
					}
				}
				
				var blockCitations = $(kriya.config.containerElm).find('[data-citation-string*=" '+ panelId.replace('BLK_', '') +' "]');
				elementOnScrollBar(blockCitations, 'data-citation-string', '');
			},
			scrollElementOnContent : function(param, targetNode){
				var dataChild = targetNode[0];
				var rid = dataChild.getAttribute('data-rid');
				var nodeXpath = dataChild.getAttribute('data-node-xpath');
				var elements = kriya.xpath(nodeXpath);
				if (elements.length > 0){
					$(elements)[0].scrollIntoView();
				}
				$('.highlight.queryText').remove();
				$('#contentDivNode').find('[data-citation-string*="'+ rid +'"]').each(function(){
					if ($(this).attr('data-track') == "del") return true;
					var range = rangy.createRange();
					range.selectNodeContents($(this)[0]);
					var sel = rangy.getSelection();
					sel.setSingleRange(range);
					findReplace.general.highlight(sel, 'queryText');
					sel.removeAllRanges()
				});
			},
			uncitedObjects: function(param, targetNode){//to display uncited objects - jai 04-11-2017
				if ($(targetNode).closest('div').find('[data-uncited="true"]').length > 0){
					if ($(targetNode).hasClass('active')){
						$(targetNode).closest('div').find('> div').removeClass('hidden');
					}else{
						$(targetNode).closest('div').find('> div').addClass('hidden');
						$(targetNode).closest('div').find('[data-uncited="true"]').removeClass('hidden');
					}
					$(targetNode).toggleClass('active')
				}else{
					$(targetNode).closest('div').find('> div').removeClass('hidden');
				}
			},
			datepicker: function(param, targetNode){
				var thisObj = this;
				var dateComponent = $(targetNode).pickadate({
					format: 'mm/dd/yyyy',
					clear: 'Cancel',
					close: 'Save',
					container: '#compDivContent',
					onClose: function() {
						var dateText = this.get('select', 'mm/dd/yyyy');
						$(targetNode).val(dateText);
						var startDate = $(targetNode).parent().find('.startDateFilter');
						var endDate   = $(targetNode).parent().find('.endDateFilter');
						if($(targetNode).hasClass('endDateFilter')){
							if(startDate.val() == ""){
								startDate.val(dateText);
							}
						}
						thisObj.filterCards({
							'type': 'datetime',
							'startDate': startDate.val(),
							'endDate'  : endDate.val()
						});
						picker.close();
						picker.stop();
					},
					onSet: function(context) {
						//console.log('Just set stuff:', context);
					}
				});
				var picker = dateComponent.pickadate('picker');
				picker.open();
			},
			checkAll: function(param, targetNode){
				$(targetNode).closest('ul').find('input[type="checkbox"]').each(function(){
					if($(targetNode).find('input:checked').length > 0){
						this.checked = true;
					}else{
						this.checked = false;
					}
				});
				this.filterCards('', $(targetNode).closest('.dropdown-content'));
			},
			queriesTab: function(param, targetNode){
				$(targetNode).parent().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				var tabId = $(targetNode).attr('data-tab-id');
				$('#openQueryDivNode, #resolvedQueryDivNode').addClass('hidden');
				$('#' + tabId).removeClass('hidden');
			},
			filterCards: function(param, targetNode){
				if($(targetNode).attr('data-card-selector')){
					var trackCards = $($(targetNode).attr('data-card-selector'));
				}else{
					return false;
				}
				
				var filteredCards = trackCards;
				
				//Updat the selected value in button
				if(targetNode && $(targetNode).length > 0){

					//Uncheck checkAll checkbox if anyone checkbox is uncheck
					//else check checkAll checkbox
					var checkAllComp = $(targetNode).find('[data-value="All"] input[type="checkbox"]');
					if($(targetNode).find('li:not([data-value="All"]) input:not(:checked)').length > 0 && checkAllComp.length > 0){
						checkAllComp[0].checked = false;
					}else if(checkAllComp.length > 0){
						checkAllComp[0].checked = true;
					}

					//Update status in dropdown button
					var targetId = $(targetNode).attr('id');
					var dropdownBtn = $('[data-dropdown="' + targetId + '"]');
					var checkedLength = $(targetNode).find('li:has(input:checked)').length;
					if(checkedLength == $(targetNode).find('li:has(input)').length){
						dropdownBtn.html('All');
					}else if(checkedLength > 0){
						dropdownBtn.html(checkedLength+' selected');
					}else{
						dropdownBtn.html('None');
					}
				}
				var selectedFilter = [];
				var compBlock = $(targetNode).closest('.filterOptions');
				compBlock.find('.dropdown-content').each(function(){
					$(filteredCards).addClass('hidden');
					var checkAttr = $(this).attr('data-check-selector');
					var filtered = [];
					$(this).find('li:not([data-value="All"]):has(input:checked)').each(function(){
						var dataValue = $(this).attr('data-value');
						var selctor   = checkAttr.replace(/\$value/, dataValue);
						selectedFilter.push(selctor);
						var fc = $(filteredCards).filter(selctor).removeClass('hidden');
						filtered = $.merge(filtered, fc);
					});
					filteredCards = filtered;
				});

				if($(filteredCards).length == 0 || $(filteredCards).closest('#changesDivNode').length > 0){
				// to show only selected track changes - aravind
				// this done by injecting css to head of the document
				var filtterColl = $('#changesDivNode .filterOptions ul[data-check-selector]');
				if($('head style[id="trackFilter"]').length > 0 ){
					$('head style[id="trackFilter"]').html('');
				}
				$(filtterColl).each(function(){
					var selAttr = $(this).attr('data-check-selector');
					selAttr = selAttr.replace(/='\$value']/, '').replace('[','');
					$(this).find('li').each(function(){
						var dataVal = $(this).attr('data-value');
						if(dataVal != 'All'){
							if(selectedFilter.indexOf("["+selAttr+"="+"'"+dataVal+"']") < 0){
								if($('head style[id="trackFilter"]').length == 0 ){
									var styleNode = document.createElement('style');
									$(styleNode).attr('id','trackFilter');
									$('head').append(styleNode);
								}
								if(selAttr == 'data-track-class'){
									if(dataVal == 'del'){
										$('head style[id="trackFilter"]').append("#contentDivNode [data-track-detail]."+dataVal+"{display:none !important}");
									}else{
										$('head style[id="trackFilter"]').append("#contentDivNode [data-track-detail]."+dataVal+"{background:none !important}");
									}
								}else if(selAttr == 'data-role'){
									$('head style[id="trackFilter"]').append("#contentDivNode "+"[data-userid^="+"'"+dataVal+"'].del{display:none !important}");
									$('head style[id="trackFilter"]').append("#contentDivNode "+"[data-userid^="+"'"+dataVal+"']:not(.del){background:none !important}");
									$('head style[id="trackFilter"]').append("#contentDivNode "+"[data-userid^="+"'"+dataVal+"'].sty:not(.del){border-bottom:unset !important}");
								}else{
									$('head style[id="trackFilter"]').append("#contentDivNode "+"["+selAttr+"="+"'"+dataVal+"'].del{display:none !important}");
									$('head style[id="trackFilter"]').append("#contentDivNode "+"["+selAttr+"="+"'"+dataVal+"']:not(.del){background:none !important}");
									$('head style[id="trackFilter"]').append("#contentDivNode "+"["+selAttr+"="+"'"+dataVal+"'].sty:not(.del){background:none !important}");
								}
							}
						}
					});
				});
				}
			}
		},
		format: {
			showHideMenu: function(param,targetNode){
				//get active nodes and remove class "active" 
				targetNode.closest('.tabMenu').find('.active').removeClass('active');
				$('#'+param.datarid).closest('.subMenu').find('.active').removeClass('active');
				//add "active" class to current nodes
				$('#'+param.datarid).addClass('active');
				targetNode.addClass('active');
				console.log('Within showHideMenu function');
			}
		},
		insert: {
			insertProduct: function(param, targetNode){
				var component = $('[data-type="popUp"][data-component="jrnlProduct_edit"]');								
				$(component).find('img').remove();
				$(component).find('[data-pop-type="edit"]').addClass('hidden');
				$(component).find('[data-pop-type="new"]').removeClass('hidden');
				kriya.popUp.showEmptyPopUp(component);								
			},
			newObject: function(param, targetNode){
				if (param && param.type){
                    componentName = param.type + '_edit';
				   var component = $("*[data-type='popUp'][data-component='" + componentName + "']");
				     /*#174- Inserting a new table the column and row value is set 3 as default- Prabakaran.A-(prabakaran.a@focusite.com)*/
                     //component.find('.addRowTemplate').val(3); //commented by jai
                     //component.find('.addColumnTemplate').val(3);
                     /* end of the code #174 - Inserting a new table the column and row value is set 3 as default- Prabakaran.A-(prabakaran.a@focusite.com)*/
					if (targetNode.closest('.query-div').length > 0){
						var rid = targetNode.closest('.query-div').attr('id');
						if ($('[data-rid="' + rid + '"]').length > 0){
							kriya.config.activeElements = $('[data-rid="' + rid + '"]');
						}else{
							return;
						}
					}
					if (param.type == 'jrnlRefText' && kriya.config.activeElements.hasClass('back')){
						kriya['styles'].init($('<div>'), componentName);
                    }
                    //#108 - ALL Customers : Objects(Figures, Tables, Videos and animation). - Helen.J - helen.j@focusite.com
                    // #108 - Removing the kriya active element- Helen.J - helen.j@focusite.com
                    else {
                        kriya['styles'].init($('<div>'), componentName);
                    }
                     $(component).find("img[src]").removeAttr("src");
                    // End of #108 - Helen.J - helen.j@focusite.com//


					//When float object popup open for add then default object type is display
					if(component.find('.objectType').length > 0){
						component.find('.objectType').val('display');
						// to remove non related attribute while reatining state - aravind
						if($(component).attr('data-component') == "BOX_edit" || $(component).attr('data-component') == "FIGURE_edit"){
							$(component).removeAttr('data-tmp-selector').removeAttr('data-remove-label');
							$(component).find('[data-object-type="display-withoutlabel"] [data-selector]').attr('data-save-type','false');
							$(component).find('[data-object-type="display"] [data-selector]').removeAttr('data-save-type');
						}
					}

					component.addClass('manualSave');
					if (param.class){
						component.attr('data-class',param.class);	
					}
					//Change the popup heading
					$(component).find('[data-pop-type]').addClass('hidden');
					$(component).find('[data-pop-type="new"]').removeClass('hidden');

					if (param.type == 'jrnlRefText'){
						$(component).find('.searchCrossRef,.insertNewRef,.searchPubMed,.popupHead[data-type="new"],.popupFoot[data-type="new"],[data-ref-type="Journal"]').removeClass('hidden');
						$('.ref-type [data-class="RefType"]').val("Journal");
						$(component).find('.popupHead[data-type="edit"],.popupFoot[data-type="edit"]').addClass('hidden');
						$(component).find('[data-ref-type="Journal"] [data-class="RefAuthor"][data-clone="true"]:empty').remove();
						var templateAuthor = $(component).find('[data-ref-type="Journal"] [data-template="true"][data-class="RefAuthor"]');	
						var clonedAuthor = templateAuthor.clone(true);
						clonedAuthor.removeAttr('data-template').attr('data-clone','true');
						clonedAuthor.insertAfter(templateAuthor);
					}
				}
			},
			primaryCite: function(param, targetNode){
				var nodeXpath = $(targetNode).closest('[data-type="popUp"]').attr('data-node-xpath');
				if(nodeXpath){			
					var currSelectedEle = kriya.xpath(nodeXpath);
					var citeID = $(targetNode).closest('[data-type]').attr('data-cite');
					var blockCiteNodes = $(kriya.config.containerElm+' [data-citation-string=" ' + citeID + ' "]');
					blockCiteNodes.filter('[data-anchor]').removeAttr('data-anchor');
					$(currSelectedEle).attr('data-anchor', 'true');
					$(blockCiteNodes).each(function(){
						kriyaEditor.settings.undoStack.push(this);
					});
					kriyaEditor.init.addUndoLevel('To add primary Citation');
				}	
			},
			newCite: function(param, targetNode){
				componentName = param.type;
				if (componentName == "QUERY"){
					$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('data-class-selector', 'default');
					$('[data-component="QUERY"]').find('.queryContent').html('');
					$('[data-component="QUERY"]').find('[data-type="file"]').remove();
					$('[data-component="QUERY"]').removeAttr('data-display').removeAttr('data-sub-type').removeAttr('style');
					$('[data-component="QUERY"]').find("#queryPerson option").removeClass("hidden")
					$('[data-component="QUERY"]').find("#queryPerson option").each(function(){
						if($(this).attr("value").toLowerCase() == kriya.config.content.role.toLowerCase()){
							$(this).addClass("hidden");
						}
					});
				}
				if (targetNode.closest('.query-div').length > 0){
					var rid = targetNode.closest('.query-div').attr('id');
					if ($(kriya.config.containerElm + ' [data-rid="' + rid + '"]').length > 0){
						kriya.config.activeElements = $(kriya.config.containerElm + ' [data-rid="' + rid + '"]').parent();
						$(kriya.config.activeElements)[0].scrollIntoView();
					}
				}
				if ($('[data-component]:visible').length > 0){
					targetNode = kriya.config.activeElements;
					if ( !$('[data-component]:visible').hasClass('autoWidth') && componentName == "QUERY"){//to display query pop-up over another pop-up
						$('[data-component="QUERY"]').attr('data-display', "modal");
						$('[data-component="QUERY"]').attr('data-sub-type', $('[data-component]:visible').attr('data-component'))
					}
				}
				kriya['styles'].init($(targetNode), componentName);
				//Added by jagan
				//Scroll to the selected item and select the selected item tab
				if (componentName == "CITATION_edit" && $('[data-component="CITATION_edit"] .selected').length > 0){
					var carouselID = $('[data-component="CITATION_edit"] .selected').closest('.carousel-item').attr('id');
					var radioBox = $('[data-component="CITATION_edit"] input[type="radio"]#_'+carouselID+'_');
					if(radioBox.length > 0){
						radioBox[0].checked = true;
					}
					setTimeout(function () {
						$('[data-component="CITATION_edit"] .selected')[0].scrollIntoView();
					}, 1000);
				}

				if (componentName == "CITATION_edit"){
					$('[data-component="CITATION_edit"] label[data-if-selector]:visible:first').trigger('click');
				}

				//Prevent adding footnote citation inside footnote
				if(componentName == "CITATION_edit" && $(kriya.selection.startContainer).closest('.jrnlFootNote').length > 0){
					$('[data-component="CITATION_edit"] [for="_cite_fn_tab_"],[data-component="CITATION_edit"] [for="_cite_bfn_tab_"]').addClass('hidden');
				}
				if (componentName == "QUERY"){
					$('[data-component="QUERY"]').removeAttr('data-sub-type');
					$('[data-component="QUERY"] .reply-needed input').prop('checked', true);
					//$('[data-component="QUERY"] [data-type="action"]').removeClass('disabled');
					$('[data-component="QUERY"] [data-class-selector].selected').removeClass('selected');
					$('[data-component="QUERY"] .tabs .tab[data-type]').removeClass('active');
					$('[data-component="QUERY"] .tabs .tab[data-type="freeform"]').addClass('active');
					
					$('[data-component="QUERY"] [data-query-type]').addClass('hidden');
					$('[data-component="QUERY"] [data-query-type="freeform"]').removeClass('hidden');

					/*if (queryType == undefined || queryType == "freeform"){
						//$('[data-component="QUERY"] [data-type="action"]').addClass('disabled');
						$('*[data-query-type="freeform"]').removeClass('hidden');
						$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('contenteditable', 'true');
					}else{
						$('*[data-query-type]').removeClass('context');
						$('*[data-query-type]:not(".hidden")').addClass('context');
					}*/
				}
			},
			indexTerm: function(param, targetNode){
				$('.nav-action-icon').removeClass('active');
				$('.navDivContent > div').removeClass('active');
				$('.index-icon').addClass('active');
				$('#indexPanel').addClass('active');
				if (typeof (tree) != 'undefined'){
					tree.jstree("deselect_all");
				}
				if (! kriya.selection) return;
				selectedIndexTerm = kriya.selection.text().replace(/^[ ,\.\:\;]+|[ ,\.\:\;]+$/g, '');
				if (selectedIndexTerm.length == 0) return;
				$('#indexInput').removeClass('hidden');
				if (selectedIndexTerm.length > 40) {selectedIndexTerm = '';};
				$('#indexTerm').val(selectedIndexTerm);
				$('#indexTerm').focus();
			},
			addIndexEntries:function(param, targetNode){
				$('#indexInput').addClass('hidden'); 
				var indexTerm = $('#indexTerm').val();
				if (!indexTerm.length) {
					console.log('Please provide an index term');
					return;
				}
				var indId = 'index_' + $('#indexPanelContent ul li.jstree-node').length + 1;
				selection = rangy.getSelection();
				selection.removeAllRanges();
				var currSelection = kriya.selection.cloneRange();
				selection.addRange(currSelection);
				if (selection.rangeCount > 0) {		//code block for add end querynode
					range = selection.getRangeAt(0);
					var queryLinker = $('<span class="jrnlIndexRef" data-rid="' + indId + '">');
					var endNode = queryLinker.clone()[0];
					endNode.setAttribute("data-type","end");	
					range.collapse(false);
					range.insertNode(endNode);
					selection.addRange(kriya.selection);
					//selection.addRange(range);
					if (selection.rangeCount > 0) {  		//code block for add end querynode
						range = selection.getRangeAt(0);
						var startNode = queryLinker.clone()[0];
						startNode.setAttribute("data-type", "start");
						range.collapse(true);
						range.insertNode(startNode);
					}
					kriyaEditor.settings.undoStack.push(startNode.parentNode);
					kriyaEditor.init.addUndoLevel('navigation-key');
				}
				
				var listNode = '<li id="' + indId + '">' + indexTerm + '</li>';	
				if ($('#indexPanelContent .jstree-container-ul').length == 0){
					$('#indexPanelContent').append('<ul class="indexMainEntry">' + listNode + '</ul>');
					tree = $("#indexPanelContent").jstree({
						core: {check_callback: true}, 
						plugins: ["dnd"]
					});
				}
				else{
					leaf_node = tree.jstree(true).get_selected();
					if (typeof(leaf_node[0]) == 'undefined'){
						tree.jstree(true).create_node(tree, { "li_attr": { 'id': indId }, "text": indexTerm });
					}
					else{
						tree.jstree(true).create_node(leaf_node, { "li_attr": { 'id': indId }, "text": indexTerm });
					}            
				}
				tree.jstree("deselect_all").jstree('open_all');
				$('li.jstree-node').attr('data-channel', 'query').attr('data-topic', 'action').attr('data-event', 'click').attr('data-message', "{'funcToCall': 'highlightIndex'}");
				$('#indexPanelContent').show().css('height', (window.innerHeight - $('#indexPanelContent').offset().top )+ 'px');
				$('li.jstree-node[id="' +indId+'"]').trigger('click');
				var indexContent = '<index>' + $('#indexPanelContent').html() + '</index>';
				var parameters  = {'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'doi':kriya.config.content.doi, 'data': indexContent};
				kriya.general.sendAPIRequest('updateindex', parameters, function(res){
					console.log(res)
				});
			},
			newSpecialChar : function(param, targetNode){
				/*if ($('[data-type="popUp"]:visible').length > 0 && $('[data-type="popUp"]:visible [contenteditable="true"]').length > 0 && typeof(kriya.popupSelection) == "undefined"){
					return false;
				}*/
				var range = rangy.getSelection();
				var currentSelection = range.getRangeAt(0);
				$('#specialCharacter').data("currentSelection",currentSelection);
				//var sp_char_win = window.open('/special_character', 'Special Character Window', 'height=340, width=400');
				if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}
				$('#specialCharacter .spl-char.active').removeClass('active');
				document.querySelector('[id="specialCharacter"]').classList.remove("hidden");
				TomloprodModal.openModal('specialCharacter');									
			},
			changeAdvanedSplChar: function(param, targetNode){
				var popper = $(targetNode).closest('#specialCharacter');
				var selectedVal = $(targetNode).val();
				popper.find('.symb-container').addClass('hidden');
				popper.find('.symb-container[data-spl-group="' + selectedVal + '"]').removeClass('hidden');
			},
			basicSpecialCharacter: function(param, targetNode){
				var popper = $(targetNode).closest('#specialCharacter');
				
				popper.find('.popupHead .btn').removeClass('active');
				$(targetNode).addClass('active');

				popper.find('.basic-style').removeClass('hidden');
				popper.find('.advanced-symbols').addClass('hidden');
			},
			advancedSpecialCharacter: function(param, targetNode){
				var popper = $(targetNode).closest('#specialCharacter');
				
				popper.find('.popupHead .btn').removeClass('active');
				$(targetNode).addClass('active');

				popper.find('.basic-style').addClass('hidden');
				popper.find('.advanced-symbols').removeClass('hidden');
				if(popper.find('.advanced-symbols:empty').length > 0){
					popper.find('.advanced-symbols').prepend('<div class="symb-container"/>');
					popper.find('.advanced-symbols').progress('Fetching...');
					popper.find('.popupFooter .btn').addClass('disabled');
					$.ajax({
						url: '/special_character',
						method: "GET",
						cache: false,
						success: function(data){
							popper.find('.advanced-symbols .symb-container').remove();
							popper.find('.advanced-symbols').progress('', true);
							popper.find('.popupFooter .btn').removeClass('disabled');
							var symbols = $(data);
							var advancedDropDown = $('<select class="spl_char_drop_down" data-channel="menu" data-topic="insert" data-event="change" data-message="{\'funcToCall\': \'changeAdvanedSplChar\'}"/>');
							symbols.find('.unicode_table').each(function(){
								var optionText = $(this).find('.separator').text();
								advancedDropDown.append('<option value="' + optionText + '">' + optionText + '</option>');

								var symbContainer = $('<div class="symb-container hidden" data-spl-group="' + optionText + '"/>');
								var symbNodes = $(this).find('div.symb');
								symbContainer.append(symbNodes);
								popper.find('.advanced-symbols').append(symbContainer);
							});
							popper.find('.advanced-symbols').prepend('<div class="advanced_toolbar row"><div class="col s6"/><div class="col s6"/></div>');
							popper.find('.advanced-symbols .advanced_toolbar .col:eq(0)').append(advancedDropDown);
							popper.find('.advanced-symbols .advanced_toolbar .col:eq(1)').append('<input class="reset-css special_char_search" type="text" placeholder="Search ..."><ul id="special_char_search_list" class="dropdown-content"></ul>');

							popper.find('.advanced-symbols .symb-container:first').removeClass('hidden');
							popper.find('.advanced-symbols .symb').attr('class', 'spl-char');
							popper.find('.advanced-symbols .spl-char').renameElement('span');
							
						},
						error: function(err){
							popper.find('.advanced-symbols .symb-container').remove();
							popper.find('.advanced-symbols').progress('', true);
						}
					});
				}
				/*if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}
				var sp_char_win = window.open('/special_character', 'Special Character Window', 'height=340, width=400');*/
			},
			closeSpecialChar : function(param, targetNode){
				if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}
			},
			insertSpecialChar : function(param, targetNode){
				if ($('#specialCharacter .spl-char.active').length == 0){
					return false;
				}else{
					specialChar = $('#specialCharacter .spl-char.active').html();;
				}
				if ($('[data-type="popUp"]:visible').length > 0 && $('[data-type="popUp"]:visible [contenteditable="true"]').length > 0){
					/*if (typeof(kriya.popupSelection) != "undefined"){
						var selection = rangy.getSelection();
						selection.removeAllRanges();*/
						currRange = $("#specialCharacter").data('currentSelection');
						//#537 - All| Special characters| Special charterers are not replaced - Prabakaran A(prabakaran.a@focusite.com)
						if(!currRange.collapsed) {
							tracker.deleteContents(true, currRange);
							tracker._moveRangeToValidTrackingPos(currRange);
						}
						//End of #537
						selection.addRange(currRange);
					/*}else{
						return false;
					}*/
				}else{
					//var selection = rangy.getSelection();
					//selection.removeAllRanges();
					var currRange = $("#specialCharacter").data('currentSelection');
					//#537 - All| Special characters| Special charterers are not replaced - Prabakaran A(prabakaran.a@focusite.com)
					if(!currRange.collapsed) {
						tracker.deleteContents(true, currRange);
						tracker._moveRangeToValidTrackingPos(currRange);
					}
					//End of #537
					selection.addRange(currRange);
				}
				specialChar = specialChar.replace(/\&amp;/gi,'&');
				if(specialChar){
					specialChar = document.createTextNode(specialChar);
					currRange.insertNode(specialChar);
					currRange.setEndAfter(specialChar);
					selection.removeAllRanges();
					selection.addRange(currRange);
					currRange.collapse(false);
					kriya.selection = currRange;
					//Add tracker and undo level if only inserted in editor content
					if($(specialChar).closest(kriya.config.containerElm).length > 0){
						$(specialChar).kriyaTracker('ins');
						$(specialChar).closest('.ins,[data-track="ins"]').attr('data-inserted', 'true');
						/* Start Issue ID - #415 - Dhatshayani . D Jan 02, 2018 - Since non-breakable space save as normal text updated this part. replaced the text &amp;nbsp; as &nbsp; and sent the inserted span to save instead of specialChar */
						var spCharClosestNode = $(specialChar).closest('.ins,[data-track="ins"]');
						var insertedHTML = $(spCharClosestNode).html();
						if(insertedHTML && insertedHTML != ''){
							insertedHTML = insertedHTML.replace(/\&amp;nbsp;/gi,'&nbsp;');
							$(spCharClosestNode).html(insertedHTML);
						}
						//when non-printable characters typede by user at that time showNonPrintChar() will fire.
						if(spCharClosestNode != undefined && specialChar.nodeValue.match(/\u2002|\u2003|\u2009/,'gi') ){
							kriya.general.showNonPrintChar(spCharClosestNode[0]);
						}
						//kriyaEditor.init.addUndoLevel('special-character', specialChar);
						kriyaEditor.init.addUndoLevel('special-character', spCharClosestNode);
					}else if($(specialChar).closest('#compDivContent').length > 0){
						var spCharClosestNode = $(specialChar).closest('*');
						var insertedHTML = $(spCharClosestNode).html();
						if(insertedHTML && insertedHTML != ''){
							insertedHTML = insertedHTML.replace(/\&amp;nbsp;/gi,'&nbsp;');
							$(spCharClosestNode).html(insertedHTML);
						}
					}/* End Issue Id - #415*/
				}
				kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;
			}
		},
		table:{
			alignTable: function(param, targetNode){
				if(param && param.align && $('.wysiwyg-tmp-selected-cell').length > 0){
					var tableEle = $(editor.composer.tableSelection.table);
					$('.wysiwyg-tmp-selected-cell').each(function(){
						$(this).attr('align', param.align);
						$(this).find('p').attr('align', param.align);
					});
					kriyaEditor.settings.undoStack.push(tableEle);
					kriyaEditor.init.addUndoLevel('Align-table-cell');
				}
			},
			insertRowAndCol: function(param, targetNode){
				if(param && param.insertAt){
					var saveNode = $('.wysiwyg-tmp-selected-cell').closest('table').closest('[id]');
					var tableEle = $(editor.composer.tableSelection.table);
					$(tableEle).find('.headerRow, .leftHeader').remove();
					editor.composer.commands.exec("addTableCells", param.insertAt);
					setTableIndex(tableEle);
					kriyaEditor.settings.undoStack.push(saveNode);
					kriyaEditor.init.addUndoLevel('insertRowAndCol');
				}
			},
			deleteRowAndCol: function(param, targetNode){
				if(param && param.deleteItem){
					var saveNode = $('.wysiwyg-tmp-selected-cell').closest('table').closest('[id]');
					var tableEle = $(editor.composer.tableSelection.table);
					editor.composer.commands.exec("deleteTableCells", param.deleteItem);
					setTableIndex(tableEle);
					kriyaEditor.settings.undoStack.push(saveNode);
					kriyaEditor.init.addUndoLevel('deleteRowAndCol');
				}
			},
			setCellBackground: function(param, targetNode){
				var tableEle = $(editor.composer.tableSelection.table);
				var target = kriya.evt.target || kriya.evt.srcElement;
				var color = $(target).attr('data-color');
				
				tracker.setCellBackgroundColor(color, $('.wysiwyg-tmp-selected-cell'));
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('cell-background-color');
				editor.composer.tableSelection.unSelect();
			},
			setCellTextColor : function(param, targetNode){
				var tableEle = $(editor.composer.tableSelection.table);
				var target = kriya.evt.target || kriya.evt.srcElement;
				var color = $(target).attr('data-color');
				
				tracker.setCellTextColor(color, $('.wysiwyg-tmp-selected-cell p'));
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('cell-text-color');
				editor.composer.tableSelection.unSelect();
			},
			moveToHead: function(param, targetNode){
				var tableEle = $(editor.composer.tableSelection.table);
				var colLength = tableEle.getTotalColumn();
				kriya.config.selectedRows = [];
				//collect the selected cells row elements
				$('.wysiwyg-tmp-selected-cell').closest('tr').each(function(){
					if($(this).parent('thead').length < 1){
						if($(this).parent().prev('thead').length > 0){
							$(this).parent().prev('thead').attr('data-append-row', 'true');
						}
						$(this).attr('collected-index',$(this).index());
						kriya.config.selectedRows.push(this);
					}
				});
				getRows(tableEle,kriya.config.selectedRows);
				kriya.config.selectedRows = kriya.config.selectedRows.sort(function (a, b) {
				  return parseInt($(a).attr('collected-index')) - parseInt($(b).attr('collected-index'));
				});
				//tag as head to the collected rows
				$.each(kriya.config.selectedRows,function(){
					var cIndex = $(this).attr('collected-index');
					moveRow = tableEle.find('tr[collected-index="'+cIndex+'"]');
					if(tableEle.find('thead').length < 1){
						tableEle.prepend('<thead />');	
					}
					var row = moveRow.detach();
					if(tableEle.find('thead').length > 1 && tableEle.find('thead[data-append-row]').length > 0){
						tableEle.find('thead[data-append-row]').append(row);

					}else{
						tableEle.find('thead').append(row);
					}
					tableEle.find('thead[data-append-row]').removeAttr('data-append-row');
					moveRow.removeAttr('collected-index');
				});
				kriya.config.selectedRows = null;
				setTableIndex(tableEle);
				tableEle.find('tbody:empty,thead:empty').remove();
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('table-move-to-head');
				//collect collect the rowspan element from the collected elements
				function getRows(tableEle,rowElements){
					var count = 0;
					var rowElementsLength = rowElements.length;
					while (count != rowElementsLength) {
						var ele = rowElements[count];
						var index = $(ele).index();
						if($(ele).find('[rowspan]').length > 0){
							var rowLength = $(ele).find('[rowspan]').map(function(o){return $(this).attr('rowspan');}).sort(function(a, b) { return b - a })[0];
							for (var i = 1; i <= rowLength; i++) {
								var row = tableEle.find('tr:eq(' + (i+index) + ')');
								if(row.length > 0 && $.inArray(row[0], kriya.config.selectedRows) < 0){
									row.attr('collected-index',row.index());
									kriya.config.selectedRows.push(row[0]);
								}
							}
						}
						if(tableEle.getTotalColumn() != $(ele).getRowColumn()){
							$(ele).prevAll('tr:has([rowspan])').each(function(o){
								if($(this).index()+parseInt($(this).find('[rowspan]').attr('rowspan')) > $(ele).index()){
									if($.inArray(this, kriya.config.selectedRows) < 0){
										$(this).attr('collected-index',$(this).index());
										kriya.config.selectedRows.push(this);
									}
									getRows(tableEle,[this]);
								}
							});	
						}
						rowElementsLength = rowElements.length;
						count++;
					}
				}
			},
			markAsHead: function(param, targetNode){
				var tableEle = $(editor.composer.tableSelection.table);
				kriya.config.selectedRows = [];
				//collect the selected cells row elements
				$('.wysiwyg-tmp-selected-cell').closest('tr').each(function(){
					$(this).attr('collected-index',$(this).index());
					kriya.config.selectedRows.push(this);
				});
				tableEle.find('*[colspan="1"]').removeAttr('colspan')
				tableEle.find('*[rowspan="1"]').removeAttr('rowspan')
				getRows(tableEle,kriya.config.selectedRows);
				kriya.config.selectedRows = kriya.config.selectedRows.sort(function (a, b) {
				  return parseInt($(a).attr('collected-index')) - parseInt($(b).attr('collected-index'));
				});
				//tag as head to the collected rows
				$.each(kriya.config.selectedRows,function(){
					var cIndex = $(this).attr('collected-index');
					moveRow = tableEle.find('tr[collected-index="'+cIndex+'"]');
					moveRow.removeAttr('collected-index');
					var appendNode = null;
					if(moveRow.parent('tbody').length > 0){
						if(moveRow.index() == 0 && moveRow.parent('tbody').prev('thead:not([data-track="del"],.jrnlDeleted)').length > 0){
							appendNode = moveRow.parent('tbody').prev('thead');
							var row = moveRow.detach();
							appendNode.append(row);
						}else{
							var tableHml = tableEle.html();
							tableHml = tableHml.replace(moveRow[0].outerHTML, "</tbody><thead>" + moveRow[0].outerHTML + "</thead><tbody>");
							tableEle.html(tableHml);
						}
					}else if(moveRow.parent('thead').length > 0){
						if(moveRow.index() == 0 && moveRow.parent('thead').prev('tbody:not([data-track="del"],.jrnlDeleted)').length > 0){
							appendNode = moveRow.parent('thead').prev('tbody');
							var row = moveRow.detach();
							appendNode.append(row);
						}else{
							var tableHml = tableEle.html();
							tableHml = tableHml.replace(moveRow[0].outerHTML, "</thead><tbody>" + moveRow[0].outerHTML + "</tbody><thead>");
							tableEle.html(tableHml);
						}
					}
				});
				kriya.config.selectedRows = null;
				tableEle.find('tbody:empty,thead:empty').remove();
				tableEle.find('tbody').each(function(){
					if($(this).prev('tbody').length > 0){
						$(this).prev('tbody').append($(this).html());
						$(this).html('');
					}
				});
				tableEle.find('thead').each(function(){
					if($(this).prev('thead').length > 0){
						$(this).prev('thead').append($(this).html());
						$(this).html('');
					}
				});
				setTableIndex(tableEle);
				tableEle.find('tbody:empty,thead:empty').remove();
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('table-mark-as-head');
				//collect collect the rowspan element from the collected elements
				function getRows(tableEle,rowElements){
					var count = 0;
					var rowElementsLength = rowElements.length;
					while (count != rowElementsLength) {
						var ele = rowElements[count];
						var index = $(ele).index();
						if($(ele).find('[rowspan]').length > 0){
							var rowLength = $(ele).find('[rowspan]').map(function(o){return $(this).attr('rowspan');}).sort(function(a, b) { return b - a })[0];
							for (var i = 1; i <= rowLength; i++) {
								var row = tableEle.find('tr:eq(' + (i+index) + ')');
								if(row.length > 0 && $.inArray(row[0], kriya.config.selectedRows) < 0){
									row.attr('collected-index',row.index());
									kriya.config.selectedRows.push(row[0]);
								}
							}
						}
						if(tableEle.getTotalColumn() != $(ele).getRowColumn()){
							$(ele).prevAll('tr:has([rowspan])').each(function(o){
								if($(this).index()+parseInt($(this).find('[rowspan]').attr('rowspan')) > $(ele).index()){
									if($.inArray(this, kriya.config.selectedRows) < 0){
										$(this).attr('collected-index',$(this).index());
										kriya.config.selectedRows.push(this);
									}
									getRows(tableEle,[this]);
								}
							});
						}
						rowElementsLength = rowElements.length;
						count++;
					}
				}
			},
			insertTableFootNote_old: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var tableEle = $(editor.composer.tableSelection.table);
				var tableBlock = $(tableEle).closest('[data-id^="BLK_"]');
				if(tableBlock.length > 0){
					var footNote = $('<p class="jrnlTblFoot" data-inserted="true" />');
					footNote.attr('id', uuid());
					if(tableBlock.find('.jrnlTblFootGroup').length == 0){
						tableBlock.append('<div class="jrnlTblFootGroup">');
					}
					tableBlock.find('.jrnlTblFootGroup').append(footNote);
					moveCursor(footNote[0]);
				}
				kriya.popUp.closePopUps(popper);
			},
			mergeAndSplit: function(param, targetNode){
				// while cell with col span is selected an extra cell will be introduced - aravind
				// to handle this reassiging start and end of table selection in editor.composer
				if(param && param.command && param.command == "merge"){
					if(editor.composer.tableSelection.end && editor.composer.tableSelection.end.hasAttribute('colspan')){
						var lastCellColSpan = parseInt($(editor.composer.tableSelection.end).attr('colspan'));
						if(lastCellColSpan && lastCellColSpan > 1){
							if($('.wysiwyg-tmp-selected-cell').length > 1){
								setCursorPosition($('.wysiwyg-tmp-selected-cell').last()[0],0);
								editor.composer.tableSelection.end = $('.wysiwyg-tmp-selected-cell').last()[0];
								editor.composer.tableSelection.start = $('.wysiwyg-tmp-selected-cell').first()[0];
							}
						}
					}
				}
				var tableEle = $(editor.composer.tableSelection.table);
				//#440 - All Customers | Table | Merge Cells - Prabakaran.A (prabakaran.a@focusite.com)
				editor.composer.commands.exec(param.command == 'split'?"splitTableCells":"mergeTableCells");
				//End of #440
				$('.wysiwyg-tmp-selected-cell').each(function(){
					//Remove the all empty para if cell has para with content
					//else skip the first empty para then remove all para
					if($(this).find('.jrnlTblBody:empty').length > 0 && $(this).find('.jrnlTblBody:not(:empty)').length > 0){
						$(this).find('.jrnlTblBody:empty').remove();
					}else if($(this).find('.jrnlTblBody:empty').length > 0 && $(this).find('.jrnlTblBody:not(:empty)').length < 1){
						$(this).find('.jrnlTblBody:empty:not(:first)').remove();
					}
				});
				setTableIndex(tableEle);
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('table-merge-split');
				editor.composer.tableSelection.unSelect();
			},
			zebraStyle: function(param, targetNode){
				var tableEle = $(editor.composer.tableSelection.table);
				$(tableEle).find('.headerRow, .leftHeader').remove();
				if(editor.composer.tableSelection.cells.length > 1){
					var selectedRows = $(editor.composer.tableSelection.cells).closest('tr');
					selectedRows.each(function(){
						if($(this).attr('data-fill') == "true"){
							$(this).removeAttr('data-fill');
						}else{
							$(this).attr('data-fill', 'true');
						}
					});
				}else{
					if(tableEle.find('[data-fill="true"]').length > 0){
						tableEle.find('[data-fill="true"]').removeAttr('data-fill');
					}else{
						kriya.general.applyZebra(tableEle);
					}	
				}
				setTableIndex(tableEle);
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('zebra-style');
			}
		},
		style: {
			updateMenuBar: function(param, targetNode){
				$('[data-menu-selector]').addClass('hidden');
				$('[data-menu-selector]:first').removeClass('hidden');
				$("[for*='menu']").removeClass('disabled');
				if (kriya.selection){
					var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					$('.mnuButtons.insertQuery').addClass('disabled');
					if (range.toString() != ""){
						$('.mnuButtons.insertQuery').removeClass('disabled');
					}
					var node = kriya.selection.commonAncestorContainer;
					//to disable tool bar menu when clicked on deleted elements
					if ($(node).closest('[data-track="del"],.del').length > 0){
						$('[data-context-btn]').addClass('disabled');
					}else{
						$('[data-context-btn]').removeClass('disabled');
					}
					if ($(node).closest('div.front,div.body,div.back,div.sub-article').length > 0){
						$('[data-menu-selector]').addClass('hidden');
						$('.kriyaMenuBtn[data-selector]').addClass('disabled');
						var blockClass = $(node).closest('div.front,div.body,div.back,div.sub-article').attr('class');
						$('*[data-menu-selector*=" ' + blockClass + ' "]').removeClass('hidden');
						$('*[data-selector*=" ' + blockClass + ' "]').removeClass('disabled');
					}
					//to disable insert from menu - jai
					if ($(node).closest('.jrnlRefText').length > 0){
						$('[data-name="Insert"]').addClass('disabled');
					}
					//indent/outdent only active for list and table cells - jai
					$('.kriyaMenuBlock [data-name="MenuBtnIncIndent"].mnuButtons').addClass('disabled');
					$('.kriyaMenuBlock [data-name="MenuBtnDecIndent"].mnuButtons').addClass('disabled');
					if ($(node).closest('ol,ul,table').length > 0){
						$('.kriyaMenuBlock [data-name="MenuBtnIncIndent"].mnuButtons').removeClass('disabled');
						$('.kriyaMenuBlock [data-name="MenuBtnDecIndent"].mnuButtons').removeClass('disabled');
						//#535
						if($(node).closest('ol').length > 0){
							var type = $(node).closest('ol')[0].getAttribute('type');
							if($(node).closest('ol').attr('start') || $(node).closest('ol').prevUntil('ol').last().prev().siblings('ol[type="'+type+'"]').length > 0){
								if($('[id="order-list-dropdown"]').find("[data-type='continueList']").length > 0){
									$('[id="order-list-dropdown"]').find("[data-type='continueList']").addClass('active');
								}else{
									$('[id="order-list-dropdown"]').append("<li data-type='continueList' data-message=\"{'click':{'funcToCall': 'content', 'param':{'command': 'continueList','argument':'continue'},'channel':'menu','topic':'edit'}}\">Continue</li>");
								}
							}else if($('[id="order-list-dropdown"]').find("[data-type='continueList']").length > 0){
								$('[id="order-list-dropdown"]').find("[data-type='continueList']").remove();
							}
						}
					}else if($('[id="order-list-dropdown"]').find("[data-type='continueList']").length > 0){
						$('[id="order-list-dropdown"]').find("[data-type='continueList']").remove();
					}
					// to highlight selected list type - aravind
					$('.kriyaMenuBlock [data-name="MenuBtnNL"][data-unlist="true"].mnuButtons').removeClass('selectedList');
					$('.kriyaMenuBlock [data-name="MenuBtnUL"][data-unlist="true"].mnuButtons').removeClass('selectedList');
					if ($(node).closest('ol').length > 0){
						$('.kriyaMenuBlock [data-name="MenuBtnNL"][data-unlist="true"].mnuButtons').addClass('selectedList');						
					}else if($(node).closest('ul').length > 0){
						$('.kriyaMenuBlock [data-name="MenuBtnUL"][data-unlist="true"].mnuButtons').addClass('selectedList');
					}
					//End of #535
					if ($(node).closest('.jrnlTblBlock, .jrnlInlineTable').length > 0){
						if ($('.kriyaMenuBtn:contains("Table")').hasClass('disabled')){
							$('.kriyaMenuBtn:contains("Table")').removeClass('disabled');
							$('.kriyaMenuBtn:contains("Table")').prev('input').prop("checked", true);
						}
					}else{
						$('.kriyaMenuBtn:contains("Table")').addClass('disabled');
					}

					//Display the node if content has selected else hide
					var editproper = $('.kriyaMenuBlock');
					$(editproper).find('[data-display-selection]:not([data-class-selector])').each(function(){
						if(kriya.selection.getNodes().length == 0){
							$(this).addClass('hidden');
						}else{
							$(this).removeClass('hidden');
						}
					});

					//Display floating option on menu
					var parentNodeClass = "";
					if($(kriya.selection.startContainer).length > 0){	
						if($(kriya.selection.startContainer).closest('p').length!=0 && $(kriya.selection.startContainer).closest('p').attr('class')){
							parentNodeClass = $(kriya.selection.startContainer).closest('p').attr('class').split(' ')[0];
						}else if($(kriya.selection.startContainer).closest('h1').length!=0 && $(kriya.selection.startContainer).closest('h1').attr('class')){
							parentNodeClass = $(kriya.selection.startContainer).closest('h1').attr('class').split(' ')[0];
						}else if($(kriya.selection.startContainer).closest('h2').length!=0 && $(kriya.selection.startContainer).closest('h2').attr('class')){
							parentNodeClass = $(kriya.selection.startContainer).closest('h2').attr('class').split(' ')[0];
						}
						
					}
					if(parentNodeClass!=""){
						$(editproper).find('*[data-class-selector]:not([data-display-selection])').each(function(){
							var selectorDiv = $(this);
							var ifSelector = $(this).attr('data-class-selector');
							//if current(<em>) element dont have any class at that time some menu items(Quote attrib,Quote para) not showing-#2672
							if((ifSelector).match(parentNodeClass) && $(kriya.selection.startContainer.parentElement).closest('[class]').attr('class').split(' ')[0]!="del" && $(kriya.selection.endContainer.parentElement).closest('[class]').attr('class').split(' ')[0]!="del" && kriya){
								selectorDiv.removeClass('hidden');
							}else{
								selectorDiv.addClass('hidden');
							}
							var ifSelector = $(this).attr('data-closest-selector');
							if($(kriya.selection.startContainer.parentElement).closest(ifSelector).length > 0 && $(kriya.selection.startContainer.parentElement).closest('[class]').attr('class').split(' ')[0]!="del" && $(kriya.selection.endContainer.parentElement).closest('[class]').attr('class').split(' ')[0]!="del" && kriya){
								selectorDiv.removeClass('hidden');
							}else if (ifSelector != undefined){
								selectorDiv.addClass('hidden');
							}
						});

						$(editproper).find('*[data-class-selector][data-display-selection]').each(function(){
							var selectorDiv = $(this);
							var ifSelector = $(this).attr('data-class-selector');
							if((ifSelector).match(parentNodeClass) && ($(kriya.selection.startContainer.parentElement).attr('class')==undefined||$(kriya.selection.startContainer.parentElement).attr('class').split(' ')[0]!="del") && ($(kriya.selection.endContainer.parentElement).attr('class')==undefined||$(kriya.selection.endContainer.parentElement).attr('class').split(' ')[0]!="del") && kriya.selection.getNodes().length > 0){
								selectorDiv.removeClass('hidden');
							}else{
								selectorDiv.addClass('hidden');
							}
						});
					}

					this.updateMenuFormat();
				}
			},
			updateMenuFormat: function(param, targetNode){
				if(editor && editor.composer && editor.composer.commands){
					$('[data-menu-style]').each(function(){
						var command = $(this).attr('data-menu-style');
						if(command.match(/color/i)){
							$(this).find('[data-color]:not(.noColor)').each(function(){
								var color = $(this).attr('data-color');
								var status  = editor.composer.commands.state(command, color);
								if(status.length > 0){
									$(this).append('<i class="icon-check"></i>');
								}else{
									$(this).find('.icon-check').remove();
								}
							});
						}
						//#535-If we click on the list,system should enable or display the type of list selected-helen.j@focusite.com
						else if(command.match(/list/i)){
							if((kriya.selection && $(kriya.selection.commonAncestorContainer).closest('ol').length > 0 && $(this).attr('data-menu-style') == 'insertOrderedList')||(kriya.selection && $(kriya.selection.commonAncestorContainer).closest('ul').length > 0 && $(this).attr('data-menu-style') == 'insertUnorderedList')){
								$(this).addClass('active');
							}else{
								$(this).removeClass('active');
							}
						}
						//End of #535-helen.j@focusite.com
						else{
							var status  = editor.composer.commands.state(command);
							if(status.length > 0){
								$(this).addClass('active');
							}else{
								$(this).removeClass('active');
							}
						}
					});
				}
			},
			// to wrap selected text inside a span / aravind
			// if selection doesn't has any text then wrap all text in para inside a span
			applyFormatOnSelection: function(param, targetNode){
				if (kriya.selection){
					range = rangy.getSelection();
					range.removeAllRanges();
					range.addRange(kriya.selection);
				}
				if(range != undefined){
					if(range.text()!=''){
						this.applyPartLabel(param, targetNode);
					}else{
						if($(kriya.selection.commonAncestorContainer).closest('span').length > 0){
							var selectionNode = $(kriya.selection.commonAncestorContainer).closest('span[class="jrnlStyledContent"], span[class="jrnlNamedContent"]');
							if(selectionNode != undefined && $(selectionNode).length > 0){
								$(selectionNode).attr('class',param.class);
								kriyaEditor.settings.undoStack.push(selectionNode);
								kriyaEditor.init.addUndoLevel('style-change');
								return true;
							}
						}
						if($(kriya.selection.commonAncestorContainer).closest('p').length > 0){
							var selectedNodes = $(kriya.selection.commonAncestorContainer).closest('p');
							$(selectedNodes).html('<span>'+$(selectedNodes).html()+'</span>');
							var saveNodes = $(selectedNodes).find('> span');
							if (saveNodes == undefined || saveNodes.length < 1) return false;
							$(saveNodes).attr('id',uuid.v4());
							$(saveNodes).addClass(param.class);
							kriyaEditor.settings.undoStack.push(saveNodes);
							kriyaEditor.init.addUndoLevel('style-change');
						}else{
							return false;
						}
					}
				}
			},
			splitSpeechBlock: function(node, className, action){
				var parentNode, newParentNode, nextNode, cloneNode, tempNode;
				var saveNode = [];

				if (node.length > 0 && $(node).prev().length > 0 && action == "split"){
					parentNode = $(node).closest('div[class="jrnlSpeech"]');
					newParentNode = document.createElement('div');
					$(newParentNode).attr('class',className);
					$(newParentNode).attr('id',uuid.v4());
					$(newParentNode).attr('data-inserted','true');
					$(newParentNode).insertAfter(parentNode);
					
					$(node).nextAll().each(function(){
						$(newParentNode).append($(this));
					});
					$(newParentNode).prepend(node);
					saveNode.push(parentNode[0]);
					saveNode.push(newParentNode);
				}else if(action == "merge" && $(node).prev().length == 0 && $(node).closest('div[class="jrnlSpeech"]').prev().length > 0 && $(node).closest('div[class="jrnlSpeech"]').prev().attr("class") == "jrnlSpeech"){
					parentNode = $(node).closest('div[class="jrnlSpeech"]').prev();
					newParentNode = $(node).closest('div[class="jrnlSpeech"]');
					
					$(node).nextAll().each(function(){
						tempNode = $(this);
					});
					$(parentNode).append(node);
					$(parentNode).append(tempNode);
					$(newParentNode).attr('data-removed','true');
					saveNode.push(parentNode[0]);
					saveNode.push(newParentNode[0]);
					$(newParentNode).remove();
				}else {
					saveNode.push(node[0]);
				}
				return saveNode;
			},
			applyAttribute: function(param, targetNode){
				var selection = rangy.getSelection();
				var range = selection.getRangeAt(0);				
				if(param && param.type == "block"){
					var selectedNode = $(range.commonAncestorContainer).closest('div');
					if( param.name != ""){
						if(param.removeAttribute == "false"){
							$(selectedNode).attr(param.name, param.value);
						}else if(param.removeAttribute == "true"){
							$(selectedNode).removeAttr(param.name);
						}
					}
					kriyaEditor.settings.undoStack.push(selectedNode);
					kriyaEditor.init.addUndoLevel('style-change');
				}
			},
			applyFormat: function(param, targetNode){
				if (kriya.selection){
					range = rangy.getSelection();
					range.removeAllRanges();
					range.addRange(kriya.selection);
				}
				//kriya.evt.preventDefault();
				//http://stackoverflow.com/a/4220880/3545167
				//get block node with rangy
				if (param.type == "inline"){
					var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					console.log(range.endContainer);
					if (range.endContainer == range.commonAncestorContainer){
						//If parentNode or comman ancestor is a span and span node is a track change node then wrap the track node with span
						//else set the class in the soan node 
						//modify by jagan
						if (range.commonAncestorContainer.parentNode.nodeName == 'SPAN' || range.commonAncestorContainer.nodeName == 'SPAN'){
							var rangeParentNode = (range.commonAncestorContainer.parentNode.nodeName == 'SPAN')?range.commonAncestorContainer.parentNode:range.commonAncestorContainer;
							if(rangeParentNode.classList.contains('ins') || rangeParentNode.classList.contains('del') || rangeParentNode.classList.contains('sty') || rangeParentNode.classList.contains('styrm')){
								var newNode = document.createElement('span');
								newNode.setAttribute('class', param.class);
								newNode.innerHTML = rangeParentNode.outerHTML;
								rangeParentNode.parentNode.insertBefore(newNode, rangeParentNode);
								rangeParentNode.parentNode.removeChild(rangeParentNode);
								kriya.selection = newNode;
							}else{
								rangeParentNode.setAttribute('class', param.class);	
							}
						}
						else {
								var newNode = document.createElement('span');
								newNode.setAttribute('class', param.class);
								newNode.innerHTML = range.toHtml();
								kriya.selection = newNode;
								selection.getRangeAt(0).deleteContents();
								selection.getRangeAt(0).insertNode(newNode);
							}
						} 
					else{
						var newNode = document.createElement('span');
						newNode.setAttribute('class', param.class);
						newNode.innerHTML = range.toHtml();
						var formattingEls = newNode.getElementsByTagName("*");
						// Remove the formatting elements
						for (var i = 0, c = 0, l = formattingEls.length; i < l; i++ ) {
							el = formattingEls[c];
							if (/^(I|B|EM|STRONG)/.test(el.nodeName)){
								eventHandler.menu.style.DOMRemove(el);
							}else{
								c++;
							}
						}
						selection.getRangeAt(0).deleteContents()
						selection.getRangeAt(0).insertNode(newNode);
					}
				
					
			}else{
					var blockNodeRegex = /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i;
					function isBlockNode(node) {
						if (!node) {
							return false;
						}
						var nodeType = node.nodeType;
						return nodeType == 1 && blockNodeRegex.test(node.nodeName)
							|| nodeType == 9 || nodeType == 11;
					}
					
					function isBlockElement(el) {
						// You may want to add a more complete list of block level element
						// names on the next line
						return /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i.test(el.tagName);
					}
					var sel = rangy.getSelection();
					var range = sel.getRangeAt(0);
					if (sel.rangeCount) {
						// get All selected elements including text nodes
						var selectedNodes = [];
						for (var i = 0; i < sel.rangeCount; ++i) {
							//http://stackoverflow.com/a/10075894/3545167
							//this gives all nodes including text and childnodes of the selected blocknodes
							//selectedNodes = selectedNodes.concat( sel.getRangeAt(i).getNodes() );
							
							//http://stackoverflow.com/a/4220880/3545167
							//this returns only the block level nodes, which we actually need here
							selectedNodes = range.getNodes([1], isBlockElement);
						}
						//check if the first node is a blockNodeRegex
						//if not get the first possible block node
						if (selectedNodes.length == 0){
							ancestor = range.commonAncestorContainer;
							var blockElements = isBlockNode(ancestor);
							if (! blockElements){
								do {
									ancestor = ancestor.parentNode;
									var blockElements = isBlockNode(ancestor);
								}while (! blockElements);
							}
							selectedNodes = [ancestor];
						}
					}
					selectedNodes = eventHandler.menu.style.renameNode(selectedNodes, param.name, param.class, param.subClass, param.wrapNode, param.speechBlockAction, param.parentClass);
					//if (selectedNodes == undefined) return false; selectedNodes will come as undefined or array if there is no elements in side that array then also it is not equal to undefined
					if (selectedNodes == undefined || selectedNodes.length < 1) return false;
					//if the abstract convert to any format  data-level is not updating-kirankumar
					var datalevel=$(selectedNodes[0]).prop('tagName').replace(/[A-Z]+/ig,'');
					if(datalevel !='')
					{
						$(selectedNodes[0]).attr('data-level',datalevel);
					}else{
						$(selectedNodes[0]).removeAttr('data-level');
					}
					//to retain the selection
						if ($(selectedNodes[0]).attr('data-removed') == undefined) {
					if (selectedNodes.length > 0){
						sel = rangy.getSelection();
						range = sel.getRangeAt(0);
						range.setStart(selectedNodes[0], 1);
						var len = selectedNodes.length - 1;
						range.setEnd(selectedNodes[len], 1);
						sel.removeAllRanges();
						sel.addRange(range);
						kriya.selection = range;
							}
					}
					kriyaEditor.settings.undoStack.push(selectedNodes);
				}
				//added by kirankumar for update TOC
				this.updateTOC(param,selectedNodes);
				kriyaEditor.init.addUndoLevel('style-change');
			},
			//added by kirankumar for update table of contents when we made changes
			updateTOC:function(param,targetNode){
				var targetId=$(targetNode).attr('id');

				var tarclass=$(targetNode).attr('class');
				if(tarclass){
					var datalevel=tarclass.replace(/[a-z]+/ig,'');
				}else{
					return false;
				}
				var allHEle=$('#dataContainer').find(":header:not([class^='del'])");
				if(datalevel){
					if($('#infoDivNode').find('[data-id='+targetId+']').length){
					$('#infoDivNode').find('[data-id='+targetId+']').attr('data-level',datalevel);
					}else{
						var prvEles=allHEle.index(targetNode[0])-1;
						prvEles=allHEle[prvEles];
						if(prvEles){
						$("<li data-id="+targetId+" data-level="+datalevel+"><span class='toc-data' onclick=\"document.getElementById('"+targetId+"').scrollIntoView();\">"+$(targetNode).html()+"</span></li>").insertAfter($('#infoDivNode').find('[data-id="'+$(prvEles).attr('id')+'"]'));
						}else{
							$('#infoDivNode').find('ol').prepend("<li data-id="+targetId+" data-level="+datalevel+"><span class='toc-data' onclick=\"document.getElementById('"+targetId+"').scrollIntoView();\">"+$(targetNode).html()+"</span></li>");
						}
					}
				}else if($(targetNode)[0].nodeName == 'P'){
					$('#infoDivNode').find('[data-id='+targetId+']').remove();
				}else if($(targetNode).attr('data-level')!=undefined&&$(targetNode).attr('data-level').length!=0){
					datalevel=$(targetNode).attr('data-level');
					if($('#infoDivNode').find('[data-id='+targetId+']').length){
						$('#infoDivNode').find('[data-id='+targetId+']').attr('data-level',datalevel);
						}else{
							var prvEles=allHEle.index(targetNode[0])-1;
							var prvEle=[];
							if(prvEles!=0){
								for(var i=prvEles;(prvEle.length == 0)&&(i>=0);i--){
									prvEle =$('#infoDivNode').find('[data-id="'+$($(allHEle[i])[0]).attr('id')+'"]')
								}
							}
							if(prvEle){
							$("<li data-id="+targetId+" data-level="+datalevel+"><span class='toc-data' onclick=\"document.getElementById('"+targetId+"').scrollIntoView();\">"+$(targetNode).html()+"</span></li>").insertAfter($('#infoDivNode').find(prvEle));
							}else{
								$('#infoDivNode').find('ol').prepend("<li data-id="+targetId+" data-level="+datalevel+"><span class='toc-data' onclick=\"document.getElementById('"+targetId+"').scrollIntoView();\">"+$(targetNode).html()+"</span></li>");
							}

						}
					}
				},
			//#513 -All| Change Case| To add option Small caps-Helen.j-Helen.j@focuiste.com
			smallCaps: function(param, targetNode){
				//#513 -All| Change Case| To add option Small caps-Helen.j-Helen.j@focuiste.com
					// to block case styling in author block / aravind
					var range = rangy.getSelection();
					var selection = range.getRangeAt(0);
					if (selection && kriya.config.preventCasing && $(selection.commonAncestorContainer).closest(kriya.config.preventCasing).length > 0) {
						return false;
					}
					if(($(selection.commonAncestorContainer).closest('[data-type="popUp"]').length > 0)&&($(selection.commonAncestorContainer).closest('.jrnlSmallCaps').length == 0)){
						selection.pasteHtml('<span class="jrnlSmallCaps">'+range.text()+'</span>');
						selection.removeAllRanges();
						return false;
					}
				if($(kriya.selectedNodes).parents('.jrnlSmallCaps').length > 0){
					$(kriya.selectedNodes).parents('.jrnlSmallCaps').each(function(index ,value){
						$(value).removeClass('jrnlSmallCaps');
						$(value).unwrap();
						$(value).addClass('styrm');
						$(value).attr('data-track-hint','SmallCaps Removed');
						tracker.addChange('styrm',[value]);
						$(value).attr('title', $(value).attr('data-track-hint')+' by '+kriyaEditor.user.name+ ' - ' + ice.dom.date('m/d/Y h:ia', parseInt(value.getAttribute(tracker.timeAttribute))));
					});
				}else{
					this.applyFormat(param, targetNode);
					$(kriya.selection).attr('data-track-hint','SmallCaps Added');
					tracker.addChange('sty',[kriya.selection]);
					$(kriya.selection).attr('title', $(kriya.selection).attr('data-track-hint')+' by '+kriyaEditor.user.name+ ' - ' + ice.dom.date('m/d/Y h:ia', parseInt(kriya.selection.getAttribute(tracker.timeAttribute))));
				}
				if (selection.rangeCount) {
					var selectedNodes = [];
					range = selection.getRangeAt(0);
					for (var i = 0; i < selection.rangeCount; ++i) {
						selectedNodes = range.getNodes([1]);
					}
					selectedNodes = eventHandler.menu.style.renameNode(selectedNodes, param.name, param.class);
					if (selectedNodes.length > 0 || param.class.match(/sty/g)){
						selection.removeAllRanges();
						selection.addRange(range);
						kriya.selection = range;
						kriyaEditor.settings.undoStack.push(selection.anchorNode);
					}
				}else if(kriya.selection){
					kriyaEditor.settings.undoStack.push(kriya.selection);
				}
				kriyaEditor.init.addUndoLevel('style-change');
			},//End of #513 -All| Change Case| To add option Small caps-Helen.j-Helen.j@focuiste.com
			applyPartLabel: function(param, targetNode){
				//to add/remove part label 
				if ($(kriya.selectedNodes).parents('.' + param.class).length > 0){
					kriyaEditor.settings.undoStack.push($(kriya.selectedNodes).parents('.' + param.class)[0].parentNode);
					$(kriya.selectedNodes).parents('.' + param.class).each(function(index ,value){
						$(this).contents().unwrap();
					});
				}else{
					//to combine the text nodes
					if (kriya.selection && kriya.selection.commonAncestorContainer){
						$(kriya.selection.commonAncestorContainer).html($(kriya.selection.commonAncestorContainer).html());
					}
					this.applyFormat(param, targetNode);
					if(kriya.selection && kriya.selection.nodeType == 1){
						kriyaEditor.settings.undoStack.push($(kriya.selection));
					}else if(kriya.selection && kriya.selection.commonAncestorContainer){
						kriyaEditor.settings.undoStack.push($(kriya.selection.commonAncestorContainer));
					}
				}
				kriyaEditor.init.addUndoLevel('style-change');
			},
			/**
			 * Function to change the text case
			 */
			//#45 - Track Changes: Formating - Prabakaran. A(prabakaran.a@focusite.com)
            changeCase: function(param, targetNode){
				if(!param || !param.toCaseOption){
					return false;
				}
				//#443-All Customers | Content Format | Change Case-Helen.j -helen.j@focusite.com
				var range = rangy.getSelection();
				var selection = range.getRangeAt(0);
					// to block case styling in author block / aravind
					if (selection && kriya.config.preventCasing && $(selection.commonAncestorContainer).closest(kriya.config.preventCasing).length > 0) {
						return false;
					}
				if(selection.getHTMLContents() == ""){
					//when we chenge case at that time if any author queries is there that will not open after changecase because code is breaking.this will handile only upper and lowercases
					if(param.toCaseOption == "upperCase" || param.toCaseOption == "lowerCase"){
						var saveNode = $(selection.startContainer).closest('p, h1, h2, h3, h4, h5, h6, td, th, pre');
						var charPos = getCaretCharacterOffsetWithin($(saveNode)[0]);
						var selectionText = saveNode[0].cloneNode(true);
						var PreviousformatNode = $('<span class="hidden" hidden-track="track">'+selectionText.innerHTML+'</span>');
							textNodesUnder(selectionText).forEach(function(ele){
								$(ele).replaceWith($.caseChange({
									textToChangeCase: ele.textContent,
									toCase:param.toCaseOption

								}));
							});
							var formatNode = $('<span>'+selectionText.innerHTML+'</span>');
							$(formatNode[0]).append( $( PreviousformatNode ) );
							$(formatNode[0]).addClass("changeCase");
							var trackHint = $.caseChange({
								textToChangeCase: param.toCaseOption,
								toCase:param.toCaseOption
							});
							$(formatNode[0]).attr('data-track-hint', trackHint+' Added');
						tracker.addChange('sty',[formatNode[0]]);
						$(formatNode[0]).attr('title', $(formatNode[0]).attr('data-track-hint')+' by '+kriyaEditor.user.name+ ' - ' + ice.dom.date('m/d/Y h:ia', parseInt(formatNode[0].getAttribute(tracker.timeAttribute))));
						saveNode[0].innerHTML = ''
						$(saveNode[0]).append(formatNode[0].outerHTML);
						setCursorPosition($(saveNode)[0], charPos)
						kriyaEditor.settings.undoStack.push(saveNode[0]);
					}else{
						//End of #443-All Customers | Content Format | Change Case-Helen.j -helen.j@focusite.com
						//#45 (3.b) - Added new functionality - Apply format to the entire paragraph when cursor is placed anywhere in the paragraph - Prabakaran. A(prabakaran.a@focusite.com)
						//some times kriya.selection.startContainer is not getting update.
						//var saveNode = $(kriya.selection.startContainer).closest('p, h1, h2, h3, h4, h5, h6, td, th, pre');
						var saveNode = $(selection.startContainer).closest('p, h1, h2, h3, h4, h5, h6, td, th, pre');
						var selectionNodes = kriya.selection.getNodes();
						var selectionText = saveNode[0].innerHTML;
						//if(param.toCaseOption=='sentenceCase'){
							var PreviousformatNode = $('<span class="hidden" hidden-track="track">'+selectionText+'</span>');
							selectionText = $.caseChange({
								textToChangeCase: selectionText,
								toCase:param.toCaseOption

							});
							var formatNode = $('<span>'+selectionText+'</span>');
							$(formatNode[0]).append( $( PreviousformatNode ) );
							$(formatNode[0]).addClass("changeCase");
						// $(formatNode[0]).addClass(param.toCaseOption);
						//  $(formatNode[0]).attr('data-track-hint', 'Sentence case Added');
					// }else{
							//var formatNode = $('<span>'+selectionText+'</span>');
						// $(formatNode[0]).addClass(param.toCaseOption);
							var trackHint = $.caseChange({
								textToChangeCase: param.toCaseOption,
								toCase:param.toCaseOption
							});
							$(formatNode[0]).attr('data-track-hint', trackHint+' Added');
						//}
						tracker.addChange('sty',[formatNode[0]]);
						$(formatNode[0]).attr('title', $(formatNode[0]).attr('data-track-hint')+' by '+kriyaEditor.user.name+ ' - ' + ice.dom.date('m/d/Y h:ia', parseInt(formatNode[0].getAttribute(tracker.timeAttribute))));
						saveNode[0].innerHTML = ''
						$(saveNode[0]).append(formatNode[0].outerHTML);
						//here need to save saveNode[0] not selectionNodes
						//kriyaEditor.settings.undoStack.push(selectionNodes);
						kriyaEditor.settings.undoStack.push(saveNode[0]);
					}
				}else{
					//If the selection has the elemnt then change the case for the element else if it only has the text element then change the case for text
					//#443-All Customers | Content Format | Change Case-Helen.j -helen.j@focusite.com												 
					var selectionNodes = selection.getNodes();
					//End of #443-All Customers | Content Format | Change Case-Helen.j -helen.j@focusite.com
					var filterNodes = $(selectionNodes).filter(function(){if(this.nodeType != 3)return this;});
					    if($(filterNodes).length > 0){ 
                        //#45 (7) - Added new functionality - Applying format to multiple paragraphs - Prabakaran. A(prabakaran.a@focusite.com)
                        var range = tracker.getCurrentRange();
                        var changeid = tracker.startBatchChange(tracker.changeTypes['formatType'].alias);
                        if (range.collapsed === false) {
                            //this._deleteSelection(range);
                            var bookmark = new ice.Bookmark(tracker.env, range), 
                            elements = ice.dom.getElementsBetween(bookmark.start, bookmark.end),
                            b1 = ice.dom.parents(range.startContainer, tracker.blockEls.join(', '))[0],
                            b2 = ice.dom.parents(range.endContainer, tracker.blockEls.join(', '))[0],
                            betweenBlocks = new Array(); 

                            for (var i = 0; i < elements.length; i++) {
                                var elem = elements[i];

                                if (ice.dom.isBlockElement(elem)) {
                                      betweenBlocks.push(elem);
                                      if (!ice.dom.canContainTextElement(elem)) {
                                        // Ignore containers that are not supposed to contain text. Check children instead.
                                        for (var k = 0; k < elem.childNodes.length; k++) {
                                          elements.push(elem.childNodes[k]);
                                        }
                                        continue;
                                      }
                                    }
                                // Ignore empty space nodes
                                 if (elem.nodeType === ice.dom.TEXT_NODE && ice.dom.getNodeTextContent(elem).length === 0) continue;
																  

                                 if (!tracker._getVoidElement(elem)) {
                                    // If the element is not a text or stub node, go deeper and check the children.
                                     if (elem.nodeType !== ice.dom.TEXT_NODE) {
                                        // Browsers like to insert breaks into empty paragraphs - remove them
                                            if (ice.dom.BREAK_ELEMENT == ice.dom.getTagName(elem)) {
                                              continue;
                                            }

                                            if (ice.dom.isStubElement(elem)) {
                                              tracker._addNodeTracking(elem, false, true);
                                              continue;
                                            }
                                            if (ice.dom.hasNoTextOrStubContent(elem)) {
                                              ice.dom.remove(elem);
                                            }

                                            for (j = 0; j < elem.childNodes.length; j++) {
                                              var child = elem.childNodes[j];
                                              elements.push(child);
                                            }
                                            continue;
                                     }
                                      ctNode = tracker.createIceNode('formatType');
                                     // $(ctNode).addClass(param.toCaseOption);
                                      //if(param.toCaseOption=='sentenceCase'){
										$(ctNode).addClass("changeCase");
											var PreviousformatNode = $('<span class="hidden" hidden-track="track">'+elem.textContent+'</span>');
											elem.textContent = $.caseChange({
												textToChangeCase: elem.textContent,
												toCase:param.toCaseOption
											});
											 //$(ctNode).attr('data-track-hint', 'Sentence case Added');
											 $(ctNode).append( $( PreviousformatNode ) );
										//}else{
											 var trackHint = $.caseChange({
												textToChangeCase: param.toCaseOption,
												toCase:param.toCaseOption
											 });
											 $(ctNode).attr('data-track-hint', trackHint+' Added');
										//}
										elem.parentNode.insertBefore(ctNode, elem);
										ctNode.appendChild(elem);
                                 }
                            }
                            bookmark.selectBookmark();
                            range.collapse(true);
                        }
                        tracker.endBatchChange(changeid);
                        kriyaEditor.settings.undoStack.push($(selectionNodes));
                        kriyaEditor.init.addUndoLevel('case-change');
                        
					}else{
                       //#45 (3.a) - Added new functionality - Apply format to the selected text - Prabakaran. A(prabakaran.a@focusite.com)
						var selectionText = selection.getHTMLContents();
                       // if(param.toCaseOption=='sentenceCase'){
                            var PreviousformatNode = $('<span class="hidden" hidden-track="track">'+selectionText+'</span>');
                            selectionText = $.caseChange({
                                textToChangeCase: selectionText,
                                toCase:param.toCaseOption
                            });
							if($(rangy.getSelection().getRangeAt(0).commonAncestorContainer).closest('[data-type="popUp"]').length == 0){
                            var formatNode = $('<span>'+selectionText+'</span>');
							$(formatNode[0]).append( $( PreviousformatNode ) );
							$(formatNode[0]).addClass("changeCase");
                           // $(formatNode[0]).addClass(param.toCaseOption);
                            //$(formatNode[0]).attr('data-track-hint', 'Sentence case Added');
                        //}else{
                           // var formatNode = $('<span>'+selectionText+'</span>');
                           // $(formatNode[0]).addClass(param.toCaseOption);
                            var trackHint = $.caseChange({
                                textToChangeCase: param.toCaseOption,
                                toCase:param.toCaseOption
                            });
                            $(formatNode[0]).attr('data-track-hint', trackHint+' Added');
                       // }
                        tracker.addChange('sty',[formatNode[0]]);
                        $(formatNode[0]).attr('title', $(formatNode[0]).attr('data-track-hint')+' by '+kriyaEditor.user.name+ ' - ' + ice.dom.date('m/d/Y h:ia', parseInt(formatNode[0].getAttribute(tracker.timeAttribute))));
                        kriya.selection.pasteHtml(formatNode[0].outerHTML);
                        eventHandler.menu.observer.addTrackChanges(formatNode[0], 'case-change');
                        kriyaEditor.settings.undoStack.push($(selectionNodes));
							}else{
								rangy.getSelection().getRangeAt(0).pasteHtml(selectionText);
							}
                    }
                    kriyaEditor.settings.undoStack.push(saveNode);
				}
				
				kriyaEditor.init.addUndoLevel('case-change');
            },
			// End of #45
			// update to handle block quote and pull quote / aravind
			renameNode: function(node, name, className, subClass, wrapNode, speechBlockAction, parentClass){
				var newNodes = [];
				if(wrapNode){
					var prvnode = $(node).prev('.'+className);
					var nextnode = $(node).next('.'+className);
					var CheckFrontQuoteAttrib = true;
					//this code will check "FrontQuoteAttrib" class in blockQuote and pullQuote
					//if there "FrontQuoteAttrib" class as last element or first element based on that blockQuote or pullQuote will create
					//start
					if(prvnode.length > 0 || nextnode.length > 0){
						if(prvnode.length > 0 && nextnode.length > 0){
							var prvtemp = $(prvnode).find('p').last();
							var nexttemp = $(nextnode).find('p').first();
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib') && nexttemp.length>0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else if(prvnode.length > 0){
							var temp = $(prvnode).find('p').last();
							if(temp.length > 0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else{
							var temp = $(nextnode).find('p').first();
							if(temp.length>0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}
					}
					if(CheckFrontQuoteAttrib && (prvnode.length > 0 || nextnode.length > 0) && $(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length == 0){
						var newParentNode =	$(node).prev('.'+className);
						if($(node).prev('.'+className).length > 0 && $(node).next('.'+className).length > 0){
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib')){
								var parent ="next";
								newParentNode =	$(node).next('.'+className);
							}else if(nexttemp.length > 0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								var parent ="prev";
							}else{//end
								var parent = "both";
							}
						}else if($(node).prev('.'+className).length > 0){
							var parent ="prev";
						}else{
							var parent ="next";
							newParentNode =	$(node).next('.'+className);
						}
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
							}
							if(parent == "both"){
								var next = $(nextnode[0]).clone();
								$(newParentNode).append($(currNode)).append($(next).contents().unwrap());
								$(nextnode).attr('data-removed',true);
								kriyaEditor.settings.undoStack.push($(nextnode));
								$(nextnode).remove();
							}else if(parent == "next"){
								$(newParentNode).prepend($(currNode))
							}else{
								$(newParentNode).append($(currNode))
							}

						}
						//need to return newNodes if new nodes is empty save will not trigger
						newNodes.push($(newParentNode)[0]);
						kriyaEditor.settings.undoStack.push(newParentNode)
					}else if($(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length > 0){
						var parentnode = $(node).closest('.jrnlBlockQuote , .jrnlPullQuote')
						$(parentnode).attr('class',className);
						var prvnode = $(parentnode).prev('.'+className);
						var nextnode = $(parentnode).next('.'+className);
						if(prvnode.length > 0 && nextnode.length > 0){
							var newParentNode = prvnode;
							var next = $(nextnode[0]).clone();
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap()).append($(next).contents().unwrap());
							$(nextnode).attr('data-removed',true);
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(nextnode));
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
							$(nextnode).remove();
						}else if(prvnode.length > 0){
							var newParentNode = prvnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}else if(nextnode.length > 0){
							var newParentNode = nextnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).prepend($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}
						kriyaEditor.settings.undoStack.push($(parentnode));
						// if block already block quote are pull then, save is not tiggered
						// so we are intiating undolevel
						if(kriyaEditor.settings.undoStack.length != 0){
							kriyaEditor.init.addUndoLevel('style-change');
						}
					}else{
						var newParentNode = document.createElement(name);
						$(newParentNode).attr('class',className);
						$(newParentNode).attr('id',uuid.v4());
						$(newParentNode).attr('data-inserted','true');
						$(newParentNode).insertBefore(node[0]);
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
							}
							$(newParentNode).append($(currNode))
						}
						newNodes.push(newParentNode);
					}

				}else{
						for (var r = 0, rl = node.length; r < rl; r++){
							var currNode = node[r];						
							if (currNode.nodeName == "DIV") return;
							if (typeof(className) != "undefined"){
								currNode.setAttribute('class', className);
							}
							if (currNode.nodeName != name){
								var newNode = document.createElement(name);
								Array.prototype.slice.call(currNode.attributes).forEach(function(a) {
									newNode.setAttribute(a.name, a.value);
								});
								while (currNode.firstChild) {
									newNode.appendChild(currNode.firstChild);
								}
								currNode.parentNode.replaceChild(newNode, currNode);
								newNodes.push(newNode);
							}else{
								newNodes.push(currNode);
							}							
						}						
						var unWarpFromNode = ['jrnlBlockQuote','jrnlPullQuote'];
						var allowedParaNode = ['jrnlQuoteAttrib','jrnlQuotePara'];
						var parentSelNode;
						var savedNodes = [];
						for(var uw = 0, nn = newNodes.length;  uw < nn; uw++){
							if((unWarpFromNode.indexOf($(newNodes[uw]).closest('div').attr('class'))>=0)&&(allowedParaNode.indexOf($(newNodes[uw]).attr('class')) < 0)){
								parentSelNode = $(newNodes[uw]).closest('div');																												
								var nextNode = $(newNodes[uw]).next();
								var prevNode = $(newNodes[uw]).prev();
								if(nextNode.length > 0 && prevNode.length > 0){
									var allNextNodes = $(newNodes[uw]).nextAll();
									$(newNodes[uw]).insertAfter($(parentSelNode));									
									$(newNodes[uw]).attr('data-inserted','true');
									kriyaEditor.settings.undoStack.push($(parentSelNode));
									kriyaEditor.settings.undoStack.push($(newNodes[uw]));
									var className = $(parentSelNode).attr('class') ;
									var newParentNode = document.createElement('div');
									$(newParentNode).attr('class',className);
									$(newParentNode).attr('id',uuid.v4());
									$(newParentNode).attr('data-inserted','true');
									$(newParentNode).insertAfter(newNodes[uw]);										
									$(newParentNode).append($(allNextNodes));
								}
								else{
									if(nextNode.length == 0 && prevNode.length == 0){
										$(newNodes[uw]).attr('data-inserted', 'true');
										$(newNodes[uw]).insertBefore($(parentSelNode));	
										var currNode = $(newNodes[uw]);
										newNodes.splice(uw);
									}
									else {
										if (nextNode.length > 0) {
											$(newNodes[uw]).attr('data-inserted', 'true');
											$(newNodes[uw]).insertBefore($(parentSelNode));
										}
										else if(prevNode.length > 0){
											$(newNodes[uw]).attr('data-inserted', 'true');
											$(newNodes[uw]).insertAfter($(parentSelNode));	
										}
									}
									if ($(parentSelNode).find('p').length == 0) {
										kriyaEditor.settings.undoStack.push($(parentSelNode).attr('data-removed', 'true'));
										if(newNodes.length == 0 && currNode != undefined){
											kriyaEditor.settings.undoStack.push(currNode);
										}
										$(parentSelNode).remove();
										kriyaEditor.init.addUndoLevel('style-change');
									}
								}
							}
						}																
				}	
				if(speechBlockAction){
					newNodes = eventHandler.menu.style.splitSpeechBlock(newNodes, parentClass, speechBlockAction);
				}			
				return newNodes;
			},
			DOMRemove: function(el) {
				var parent = el.parentNode;
				while (el.hasChildNodes()) {
					parent.insertBefore(el.firstChild, el);
				}
				parent.removeChild(el);
			},
			/*
			*When we click on show hide non-printing characters button at that time showHideNonPrintChar() this function will show non-printable characters
			*/
			showHideNonPrintChar: function(param, targetNode){
				if($(targetNode).hasClass('active')){
					$(targetNode).removeClass('active');
					$('#contentDivNode').removeAttr('data-non-printchar-show');
				}else{
					$(targetNode).addClass('active');
					$('#contentDivNode').attr('data-non-printchar-show','true');
				}
			}
		},
		findReplace:{
			resetPanel: function(param, targetNode){
				if(kriya.evt.key && kriya.evt.key.match(/(Alt|Control|Shift|CapsLock)/)){
					return false;
				}
				$("#searchError").html("").css("display","none");

				if($(targetNode).hasClass("matchformat")){
					if($('.matchformat').val()!="" || ($('.matchformat').val()=="" && targetNode.html() != "")){
						$(".findAction .searchBtn").removeClass("disabled");
						return true;
					}
				} 
				// search button for finding text - priya #119
				var fromSearchBtn = 0;
				
				if(kriya.evt.keyCode==undefined){
					if(!targetNode.hasClass('search')){
						targetNode = $('.searchModal .search');
					}
					fromSearchBtn = 1;
				}
				
				if(targetNode.html() == "" && $('.matchformat').val()==""){
					$(".findAction .searchBtn, .findAction .replaceBtn, .findAction .replaceAllBtn").addClass('disabled');
				}else{
					$(".findAction .searchBtn, .findAction .replaceBtn, .findAction .replaceAllBtn").removeClass('disabled');
				}
				//If text change in search text box need to clear already highleted search text in editor otherwise if we give enter in search textbox it will not search changed text.
				var resetPanel = false;
				if($('.highlight.findOverlay').length > 0 && targetNode.text().toLowerCase() != $($('.highlight.findOverlay')[0]).attr('data-content').toLowerCase()){
					resetPanel = true;
				}
				if(kriya.evt.keyCode != 13 && kriya.evt.keyCode != 8 && fromSearchBtn!=1 && kriya.evt.keyCode != 46 && !resetPanel){
					return false;
				}else if(kriya.evt.keyCode==8 || kriya.evt.keyCode ==46 || resetPanel){
					$('.highlight.findOverlay').remove();
					$('*[class="highlight"]').remove();
					$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123
					$('.findSection .findCount').text(0);
					$('.findSection .findIndex').text(0);
					$('.matchformat').val("");
					return true;
				}else if(kriya.evt.keyCode==13){
					if($('.highlight.findOverlay').length>0){
						$(".findAction .searchBtn").addClass('disabled');
					}else if(targetNode.html() != ""){
						$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123	
					}else{
						return false;
					}
				}

				if ($(targetNode).hasClass('toggleReplace') && $('.findSection').hasClass('active')){
					resetPanel = true;
				}else{
					if (!$('.search-icon').hasClass('active') || $(targetNode).hasClass('search-icon')){
						if(!$('.search-icon').hasClass('active')){
							var prevIcon = $('.nav-action-icon.active').attr("class").replace("nav-action-icon",'').replace("active",'').trim();
							var prevNodeId = $('.navDivContent > div.active').attr('id');
							$(".findSection .closePanel").attr('data-prevIcon',prevIcon).attr('data-prevNodeId',prevNodeId);
						}
						$('.nav-action-icon').removeClass('active');
						$('.navDivContent > div').removeClass('active');
						$('.search-icon').addClass('active');
						$('.findSection').addClass('active');
						$('.findSection .reset-css.search').focus();
						$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123
						$('.matchformat').val("");
						return true;
					}
				}
				if (kriya.evt.keyCode == 27){
					resetPanel = true;
				}
				if(targetNode.html() == "" && (kriya.evt.keyCode == 8 || kriya.evt.keyCode == 46)){
					resetPanel = true;
				}
					
				if (resetPanel){
					//$('.search-icon').removeClass('active');
					$('.highlight.findOverlay').remove();
					$('*[class="highlight"]').remove();
					$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123
					//$('.findSection').removeClass('active');
					$('.findSection .findCount').text(0);
					$('.findSection .findIndex').text(0);
					$('.findSection .reset-css').html('');
					$('.matchformat').val("");
					return true;
				}
				if((targetNode.html() != "" && kriya.evt.keyCode != 27) || $('.matchformat').val()!=""){
					var keycode = kriya.evt.keyCode;
					if (
					(keycode > 47 && keycode < 58)   || // number keys
					keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
					keycode == 46 || keycode == 8   || // spacebar & return key(s) (if you want to allow carriage returns)
					(keycode > 64 && keycode < 91)   || // letter keys
					(keycode > 95 && keycode < 112)  || // numpad keys
					(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
					(keycode > 218 && keycode < 223)  || // [\]' (in order)
					fromSearchBtn == 1
					){
						if(kriya.evt.keyCode != 13){
							$('.highlight.findOverlay').remove();
							$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123
						}
						if(kriya.evt.keyCode==13 && kriya.evt.shiftKey){ //shift+enter should highlight previous - priya #119
							eventHandler.menu.findReplace.searchText("prev");
						}else{
							eventHandler.menu.findReplace.searchText();
						}
						
					}
				}
				/*if (targetNode.hasClass('wholeWord') || targetNode.hasClass('matchCase')){
					$('.highlight.findOverlay').remove();
					eventHandler.menu.findReplace.searchText();
				}*/
			},
			searchText: function(param, targetNode){
				//It will go to previous active tab
				if (param == "clear"){
					$('.highlight.findOverlay').remove();
					$('*[class="highlight"]').remove();
					$('#contentDivNode [data-replaced="true"]').removeAttr('data-replaced'); //removing the replaced word attribute to get it find in next search - priya #119 - #123
					$('.findSection .findCount').text(0);
					$('.findSection .findIndex').text(0);
					$('.findSection .reset-css').html('');
					$('.searchInObj').jstree().deselect_all();
					eventHandler.menu.edit.toggleNavPane($('.findSection .closePanel').attr('data-prevNodeId'), $("."+$('.findSection .closePanel').attr('data-prevIcon')));
					return true;
				}
				if ($('.highlight.findOverlay').length == 0){
					$('.la-container').show();
					setTimeout(function() {
						
						$('.findSection .findCount').text(0);
						$('.findSection .findIndex').text(0);
						
						var xy = $('#contentDivNode').scrollTop();
						if (!kriya.selection){
							var selection = rangy.getSelection();
							kriya.selection = selection.getRangeAt(0);
						}
						$('#contentDivNode').focus();
						// to add previous selection to range - JAI 16-02-2018
						var selection = rangy.getSelection();
						selection.removeAllRanges();
						if (!kriya.selection){
							kriya.selection = selection.getRangeAt(0);
						}
						var currSelection = kriya.selection.cloneRange();
						selection.addRange(currSelection);
						$('#contentDivNode').scrollTop(xy);
						//$('.findSection .loadingIcon').css("display", "inline-block");
						$('.findSection .findOccurrence').css("display", "none");
						$('.findSection .search-dropdown').css("display", "none");
						
						var seacrhInObj = $('.searchInObj').jstree().get_selected(true);
						
						if($('.findSection .inselection:checked').length > 0){
							if($('.wysiwyg-tmp-selected-cell').length > 0){
								var foundNodes = [];
								$('.wysiwyg-tmp-selected-cell').each(function(){
									var fNodes = findReplace.general.find({'searchText':$('.reset-css.search').html() ,'contextNode' : this ,'wholeWord': $('.wholeWord').prop('checked'), 'matchCase':$('.matchCase').prop('checked'), 'matchFormat':$('.matchformat').val()});
									foundNodes = fNodes.concat(foundNodes);
								});
							}else if(kriya.selection){
								var betweenNodes = ice.dom.getElementsBetween($(kriya.selection.startContainer).closest('p, :header')[0], $(kriya.selection.endContainer).closest('p, :header')[0]);
								betweenNodes = betweenNodes.filter(function(obj){if(obj.nodeType == 1) return this;});
								var selecttionParas = [];
								if(betweenNodes.length == 0){
									selecttionParas = $(kriya.selection.startContainer).closest('p');
								}else{
									selecttionParas.push($(kriya.selection.startContainer).closest('p')[0]);
									selecttionParas.push(betweenNodes);
									selecttionParas.push($(kriya.selection.endContainer).closest('p')[0]);
								}
								var foundNodes = [];
								$(selecttionParas).each(function(){
									var fNodes = findReplace.general.find({'searchText':$('.reset-css.search').html() ,'contextNode' : this ,'wholeWord': $('.wholeWord').prop('checked'), 'matchCase':$('.matchCase').prop('checked'), 'matchFormat':$('.matchformat').val()});
									foundNodes = fNodes.concat(foundNodes);
								});
							}
						}else if(seacrhInObj && seacrhInObj.length > 0){
							var foundNodes = [];
							$(seacrhInObj).each(function(){
								var dataId = this.id;
								dataId = (dataId)?dataId.replace(/jstree-/, ''):'';
								if(!dataId.match(/all-/) && $('[data-id="' + dataId + '"]').length > 0){
									var fNodes = findReplace.general.find({'searchText':$('.reset-css.search').html() ,'contextNode' : $('[data-id="' + dataId + '"]')[0] ,'wholeWord': $('.wholeWord').prop('checked'), 'matchCase':$('.matchCase').prop('checked'), 'matchFormat':$('.matchformat').val()});
									foundNodes = fNodes.concat(foundNodes);
								}
							});
						}else{
							var foundNodes = findReplace.general.find({'searchText':$('.reset-css.search').html(),'wholeWord': $('.wholeWord').prop('checked'), 'matchCase':$('.matchCase').prop('checked'), 'matchFormat':$('.matchformat').val()});
						}
						


						$('.findSection .reset-css.search').focus();
						
						if(!foundNodes || foundNodes.length==0){
							$("#searchError").html("No Match Found").css("display","block");
						}
						if (foundNodes && foundNodes.length > 0){
							var r = 0;
							$(foundNodes).each(function(){
								if ($(this).attr('data-cont') != undefined){
									$(this).attr('data-rid', r);
								}else{
									$(this).attr('data-rid', ++r);
								}
							});						
						
						$('.findSection .findCount').text(r);
							//$('.findSection .loadingIcon').css("display", "none");
								$('.findSection .findOccurrence').css("display", "inline-block");
								$('.findSection .search-dropdown').css("display", "inline-block");
								//$('.findSection .icon-close').css("display", "inline-block");
							
							if (kriya.selection){
								//var topOffset = kriya.selection.getBoundingClientRect().top  + $('#contentDivNode').scrollTop();
								//var topPos = topOffset - $('#contentDivNode').offset().top;
								var topPos = $('#contentDivNode').scrollTop(); // To highlight the first occurrence in visible content - priya #119
								$(foundNodes).each(function(){
									var currTop = parseInt($(this).css('top'))
									if (currTop >= topPos){
										$(this).addClass('active');
										return false;
									}
								});
							}
							//disabled the button id data-replace is false #383 - priya
							if ($('.highlight.findOverlay.active').length == 0){
								$(foundNodes).first().addClass('active'); 
								if($(foundNodes).first().attr('data-replace')=="false"){
									$(".findAction .replaceBtn,.findAction .replaceAllBtn").addClass('disabled');
								}
							}else if($('.highlight.findOverlay.active').attr('data-replace')=="false"){
									$(".findAction .replaceBtn,.findAction .replaceAllBtn").addClass('disabled');
							}
						}else{
							//$('.findSection .loadingIcon').css("display", "none");
							$('.findSection .findOccurrence').css("display", "inline-block");
							$('.findSection .search-dropdown').css("display", "inline-block");
							$('.findSection .findCount').text(0);
							$('.findSection .findIndex').text(0);
						}

						//to scroll into current selection
						if ($('.highlight.findOverlay.active').length > 0){
							var curr = $('.highlight.findOverlay.active');
							$('.findSection .findIndex').text(curr.attr('data-rid'));
							$('#clonedContent').scrollTop($('#contentDivNode').scrollTop());
							if ($(curr).offset().top < 0 || ($(curr).offset().top + $(curr).height()) > $('#contentDivNode').height()){
								var scrollTop = ($('#contentDivNode').scrollTop() + $(curr).parent().offset().top) - $('#contentDivNode').offset().top + $(curr).position().top;
								$('#contentDivNode').scrollTop(scrollTop);
							}
						}
						
						$('.la-container').hide();
					}, 0);
				}else{
					var curr = $('.highlight.findOverlay.active');
					$(".findAction .replaceBtn,.findAction .replaceAllBtn").removeClass('disabled');
					if (param == "prev"){
						var nextRid = parseInt(curr.attr('data-rid')) - 1;
						//kirankumar:-back navigation not working in searching with Match whole word after replace between node
						//start
						for(var i=nextRid;i>=0;i--){
							if ($('.highlight.findOverlay[data-rid="' + i + '"]').length != 0){
								nextRid=i;
								break;
							}
						}
					}
					else{
						var nextRid = parseInt(curr.attr('data-rid')) + 1;
						var arr=[];
						$('.highlight.findOverlay').each(function(index){arr[index]=parseInt($(this).attr('data-rid'))});
						var lastRid=Math.max.apply(null,arr);
						//front navigation-kirankumar
						for(var i=nextRid;i<=lastRid;i++){
							if ($('.highlight.findOverlay[data-rid="' + i + '"]').length != 0){
								nextRid=i;
								break;
							}
						}
						if ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length == 0 && nextRid >= $('.highlight.findOverlay').length){
							nextRid = 1;
						}
						/*if ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length == 0 && nextRid >= $('.highlight.findOverlay').length){
							nextRid = 1;
							while ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length == 0 && nextRid < $('.highlight.findOverlay').length){
								nextRid++;
							}
						}*/
						//end
					}
					if ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length > 0){
						$(curr).removeClass('active');
						$('.highlight.findOverlay[data-rid="' + nextRid + '"]').addClass('active');
						if($('.highlight.findOverlay[data-rid="' + nextRid + '"]').attr('data-replace')=="false"){
							$(".findAction .replaceBtn,.findAction .replaceAllBtn").addClass('disabled');
						}
					}

					//to scroll into current selection
					if ($('.highlight.findOverlay.active').length > 0){
						var curr = $('.highlight.findOverlay.active');
						$('.findSection .findIndex').text(curr.attr('data-rid'));
						$('#clonedContent').scrollTop($('#contentDivNode').scrollTop());
						if ($(curr).offset().top < 0 || ($(curr).offset().top + $(curr).height()) > $('#contentDivNode').height()){
							var scrollTop = ($('#contentDivNode').scrollTop() + $(curr).parent().offset().top) - $('#contentDivNode').offset().top + $(curr).position().top;
							$('#contentDivNode').scrollTop(scrollTop);
						}
					}
				}
				
			},
			replaceText: function(param, targetNode){
				if($('.reset-css.replace').html()==""){
					$("#searchError").html("Replace value should not be empty").css("display","block");
					return false;
				}
				if (param == "all"){
					var replacedNodes = findReplace.general.replace({'replaceText': $('.reset-css.replace').html(), 'type': 'all'});
					$('.highlight.findOverlay').remove();
					if(replacedNodes.length>0){
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : replacedNodes.length+' Replacements have been made',
							icon : 'icon-info'
						});
					}
				}else{
					var replacedNodes = findReplace.general.replace({'replaceText': $('.reset-css.replace').html()});
					if(replacedNodes==false){
						return false;
					}
					//if replaced text is deleted text no need to do all process.inserted by kiran
					if ($('.highlight.findOverlay.active').length > 0){
						$(replacedNodes).attr("data-replaced","true"); //Current search the word should not get highlight if a word is replaced by same wors - priya #119 -#123
						var curr = $('.highlight.findOverlay.active');
						var currParent = curr.parent();
						var currentNode = $('#contentDivNode #' + currParent.attr('clone-id'))[0];
						currParent.height($(currentNode).outerHeight());
						while (currParent[0].parentNode.nodeName != "DIV"){
							currParent = currParent.parent();
							var currentNode = $('#contentDivNode #' + currParent.attr('clone-id'))[0];
							currParent.height($(currentNode).outerHeight());
							if ($(currParent)[0].nodeName == "TABLE"){
								$(currParent).find('tr').each(function(){
									var currRow = $('#contentDivNode #' + $(this).attr('clone-id'))[0];
									$(this).height($(currRow).outerHeight());
								});
							}
						}
						if (currParent[0].parentNode.nodeName == "DIV"){
							currParent = currParent.parent();
							var currentNode = $('#contentDivNode #' + currParent.attr('clone-id'))[0];
							currParent.height($(currentNode).height());
						}
						var currRid = parseInt(curr.attr('data-rid'));
						var nextRid = currRid + 1;
						//inserted by kirankumar:-if replace text and go back then replace text then it not highlet next search text
						//start
						var arr=[];
						$('.highlight.findOverlay').each(function(index){arr[index]=parseInt($(this).attr('data-rid'))});
						var lastRid=Math.max.apply(null,arr);
						for(var i=nextRid;i<=lastRid;i++){
							if ($('.highlight.findOverlay[data-rid="' + i + '"]').length != 0){
								nextRid=i;
								break;
							}
						}
						/*var nextFound = true;
						if ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length == 0 && nextRid > $('.highlight.findOverlay').length){
							nextRid = 1;
							nextFound = false;
							while ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length == 0 && nextRid < $('.highlight.findOverlay').length){
								nextRid++;
							}
						}*/
						//end
						$('.findSection .findIndex').text(curr.attr('data-rid'));
						if ($('.highlight.findOverlay[data-rid="' + nextRid + '"]').length > 0){
							//var next = $('.highlight.findOverlay[data-rid="' + nextRid + '"]');
							var parentID = curr.attr('data-parent-id');
							var currentNode = $('#contentDivNode #' + parentID)[0];
							if ($('.highlight.findOverlay[data-parent-id="' + parentID + '"]').length > 0){
								//commented by kirankumar:-Replace is stuck when we replace with formattings(with Match whole word) (250)
								if($('.wholeWord').prop('checked')){
									var r = parseInt($('.highlight.findOverlay[data-parent-id="' + parentID + '"]').last().attr('data-rid'));
									}else{
									var r = parseInt($('.highlight.findOverlay[data-parent-id="' + parentID + '"]').last().attr('data-rid')-1);
									}
								var clone = $('.highlight.findOverlay[data-parent-id="' + parentID + '"]').last().clone();
								clone.removeAttr('class');
								$('.highlight.findOverlay[data-parent-id="' + parentID + '"]').last().before(clone)
								if(replacedNodes==false){
									$('.highlight.findOverlay[data-parent-id="' + parentID + '"]').removeClass('active');
								}else{
									$('.highlight.findOverlay[data-parent-id="' + parentID + '"]').remove();
								}

								foundNodes = findReplace.general.find({'searchText': $('.reset-css.search').html(), 'contextNode': currentNode,'matchFormat':$('.matchformat').val(),'wholeWord': $('.wholeWord').prop('checked'), 'matchCase':$('.matchCase').prop('checked')});
								if (foundNodes.length > 0){
									$(foundNodes).each(function(){
										if ($(this).attr('data-cont') != undefined){
											$(this).attr('data-rid', r);
										}else{
											$(this).attr('data-rid', ++r);
										}
										clone.after($(this))
									});
								}
								clone.remove();
								$('.highlight.findOverlay[data-rid="' + nextRid + '"]').addClass('active'); // To highlight the next occurrence after find - priya #119-#123
								$('.highlight.findOverlay[data-parent-id="' + parentID + '"]').each(function(){
									console.log($(this).attr('data-rid'))
								})
							}else{
								curr.removeClass('active');
							}
							//$('.highlight.findOverlay[data-rid="' + currRid + '"]').removeClass('findOverlay');
							//curr.removeClass('findOverlay');
							var curr = $('.highlight.findOverlay.active');
							if (curr.length > 0 && ($(curr).position().top < 0 || ($(curr).position().top + $(curr).height()) > $('#contentDivNode').height())){
								var scrollTop = ($('#contentDivNode').scrollTop() + $(curr).parent().offset().top) - $('#contentDivNode').offset().top + $(curr).position().top;
								setTimeout(function () {
									$('#contentDivNode').scrollTop(scrollTop);
								}, 100);
							}
						}else{
							curr.removeClass('active');
						}
					}
				}
				$(replacedNodes).each(function(){
					eventHandler.menu.observer.addTrackChanges($(this)[0], 'added');
					kriyaEditor.settings.undoStack.push($(this)[0]);
				});
				kriyaEditor.init.addUndoLevel('navigation-key');
			},
			reSearch: function(parentNode,nodeId,wholeWordStatus){
				var eArray = [];
				var parentNodeData = $('[data-parent-id="'+nodeId+'"]');
				var r = parseInt(parentNode.find('.highlight').first().attr('data-rid')) - 1;
				var currentNode = $('#contentDivNode #' + parentNode.attr('id'))[0];
				parentNode.height($(currentNode).height());
				var spellIgnoreNode = $('[id="spellCheck_edit"][data-ignore-class][data-class-elements]');
				var spellInlineEle;
				if(spellIgnoreNode.length>0){
					spellInlineEle = $(spellIgnoreNode)[0].getAttributeNode('data-class-elements').value;
				}
				parentNodeData.each(function(){
					var textTofind = $(this).attr('data-content');
					if ((typeof(textTofind) != "undefined") && eArray.indexOf(textTofind) < 0){
						eArray.push(textTofind);
						var attributes = $(this)[0].attributes;
						var className = $(this).attr('class').replace('highlight ', '');
						if(wholeWordStatus=="false"){
							foundNodes = findReplace.general.find({'searchText': textTofind, 'contextNode': $('[id="'+nodeId+'"]'), 'className': className , 'spellInlineEle':spellInlineEle});
						}else{
							foundNodes = findReplace.general.find({'searchText': textTofind, 'contextNode': $('[id="'+nodeId+'"]'), 'className': className , "wholeWord" : "true",'spellInlineEle':spellInlineEle});
						}
						
						if (foundNodes.length > 0){
							$(foundNodes).each(function(){
								if ($(this).attr('data-cont') != undefined){
									$(this).attr('data-rid', r);
								}else{
									$(this).attr('data-rid', ++r);
								}
								var newNode = $(this)[0]
								Array.prototype.slice.call(attributes).forEach(function(a) {
									if (! newNode.hasAttribute(a.name)){
										newNode.setAttribute(a.name, a.value);
									}
								});
							});
						}
					}
					$(this).remove();
				});
				if($(".spellcheckOrder").length>0){
					var dataOrder = $(".spellcheckOrder").attr("data-id-order").split(","); 
					$(dataOrder).each(function(i,val){
						var sortEle = $('#clonedContent').find('.highlight.spelling[data-parent-id="'+val+'"]').sort(function (a, b) {
							return +b.getAttribute('start-offset') - +a.getAttribute('start-offset');
						})
						if(sortEle.length>0){
							$('#clonedContent').find(".highlight.spelling[data-parent-id='"+val+"']").remove();
							$('#clonedContent').prepend(sortEle);
						}
					});
				}	
			}
		},
		spellCheck:{
			checkDocument: function(param, targetNode){
				if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}

				if ($(targetNode).hasClass('active')){
					$(targetNode).removeClass('active');
					$('.highlight.spelling,.highlight.suggestion,.highlight.grammar,.suggestions.spellcheck,.spellcheckOrder').remove(); // added .suggestions.spellcheck to remove suggestion list
					if((($('[data-track-display="false"]').length > 0) && ($('[data-track-display="false"]').find(kriya.config.containerElm).length > 0))){ //enable track changes
						eventHandler.menu.track_changes.showHideTrackChanges('',$(".icon-eye").parent()[0]);
					}
				}else{
					$('#clonedContent').find(".highlight.spelling,.spellcheckOrder").remove();
					$('[data-component="contextMenu"]').addClass("hidden");
					var orderKey = "";
					try {
						$('.la-container').fadeIn(function(){
							if(!(($('[data-track-display="false"]').length > 0) && ($('[data-track-display="false"]').find(kriya.config.containerElm).length > 0))){ //disable track changes
								eventHandler.menu.track_changes.showHideTrackChanges('',$(".icon-eye").parent()[0]);
							}
							$(targetNode).addClass('active');

							var r = 0;
							var spellIgnoreClass,spellInlineEle;
							var spellIgnoreNode = $('[id="spellCheck_edit"][data-ignore-class][data-class-elements]');
							if(spellIgnoreNode.length>0){
								spellIgnoreClass = $(spellIgnoreNode)[0].getAttributeNode('data-ignore-class').value;
								spellInlineEle = $(spellIgnoreNode)[0].getAttributeNode('data-class-elements').value;
							}
							var nodes = $('#contentDivNode').find('p,h1,h2,h3,h4,h5,li:not(:has(p)),td:not(:has(p))').not($('sup, sub, .jrnlDeleted'));
							if (kriya.selection && kriya.selection.text() != ""){
								var nodesEle = stylePalette.actions.getBlockNodes();	
								var nodesEleClone = $(nodesEle).clone(true);
								//nodes = $(nodes).find('p,h1,h2,h3,h4,h5,li:not(:has(p)),td:not(:has(p))').not('sup');
								var getNodes = document.createElement('div');
								$(getNodes).append(nodesEleClone);
								nodes = $(getNodes).find('p,h1,h2,h3,h4,h5,li:not(:has(p)),td:not(:has(p))').not($('sup, sub, .jrnlDeleted'));
							}
							var nodeLen = nodes.length;
							var hightlightFound = 0;
							var cloneNodes = $(nodes).clone(true);
							var processNodes  = cloneNodes.not(':gt(300)');
							var dictionaryVal = $('[data-class="spellCheckDic"]')[0].options[$('[data-class="spellCheckDic"]')[0].selectedIndex].value; // Dictionary value
							if($('#contentContainer').attr('data-spelling-language')){
								dictionaryVal = $('#contentContainer').attr('data-spelling-language');
								$('[data-class="spellCheckDic"]').val(dictionaryVal);
							}
							if($('.la-container').find('.la-container-status').length > 0){
								$('.la-container').find('.la-container-status').remove();
							}
							$('.la-container').append('<div class="la-container-status" style="width: 100%;top: 60%;position: absolute;"><span class="statusBackground" style="background: #e1e1e1;">Spellcheck request processing...</span></div>');
							var intialPercentage = 0;
							processSpelling(processNodes, dictionaryVal);
							function processSpelling(inputNodes, dictVal){
								inputNodes.attr('data-spellcheck', 'true');
								var nodes = inputNodes;
								var promiseArr = [];
								var leadingSpace = [];
								var callBackVar = [];
								var data ={};
								// to provide space bettween label content and text next to it
								// this done bcoz during spell check, if there is no space it is considered as one word and highlighed as spell error
								$(nodes).find('.label').each(function(){
									if($(this).closest('[class$="Block"]').length > 0){
										if(!($(this).text().match('/\s$/') || ($(this)[0].nextSibling && $(this)[0].nextSibling.nodeValue && $(this)[0].nextSibling.nodeValue.match('/^\s/')))){
											$(this).append(' ');
										}
									}
								});
								$(nodes).each(function(index){
									if(spellIgnoreClass!='undefined' && $(this).is(spellIgnoreClass)){
										// $(clonedata).remove(spellIgnoreClass);
									}else if(!$(this).attr("id")){
										return;
									}
									else if($(this)[0].textContent.trim()=="" || ($(this)[0].getAttributeNode("id").value!=null && callBackVar.indexOf($(this)[0].getAttributeNode("id").value)>=0)){
										return;
									}else{
										callBackVar.push($(this)[0].getAttributeNode("id").value);
										clonedata = $(this).clone(true);
										if(spellInlineEle!="undefined"){
											clonedata.find(spellInlineEle).remove();
										}
										var dataContent = clonedata.text();								
										if(dataContent.search(/\S/)>0){
											leadingSpace[$(this)[0].getAttributeNode("id").value] = dataContent.search(/\S/);
											dataContent = dataContent.replace(/^\s+/, '');
										}else{
											leadingSpace[$(this)[0].getAttributeNode("id").value] = 0;
										}
										data[$(this)[0].getAttributeNode("id").value] = dataContent;
										if($(this)[0].getAttributeNode("id").value!=undefined){
										orderKey += $(this)[0].getAttributeNode("id").value+",";
										}		
									}
								});
								
								$.ajax({
									url: '/api/spellChecknodehun',
									data: {
										'customer' : kriya.config.content.customer,
										'project'  : kriya.config.content.project,
										'doi'      : kriya.config.content.doi,
										'data': JSON.stringify(data),
										"dictionaryMode":dictVal
									},
									method: "POST",
									cache: false,
									success: function(res){
										highlightRes(res);
										var nextProcees = cloneNodes.not('[data-spellcheck]').not(':gt(300)');
										if(nextProcees.length > 0){
											var getPercentage = 100 - (((cloneNodes.length - nextProcees.length)/cloneNodes.length)*100);
											getPercentage = Math.round(getPercentage);
											intialPercentage = intialPercentage + getPercentage;
											$('.la-container').find('.la-container-status span').text('Spellcheck process completed '+intialPercentage+'%...');
											processSpelling(nextProcees, dictVal);
										}else{
											$('.la-container').find('.la-container-status span').text('Spellcheck process completed 100%');
											showModel();
											if($('.la-container').find('.la-container-status').length > 0){
												$('.la-container').find('.la-container-status').remove();
											}
										}
									},
									error : function(err){
										if($('.la-container').find('.la-container-status').length > 0){
											$('.la-container').find('.la-container-status').remove();
										}
										$('.la-container').fadeOut(function(){
											if ($(targetNode).hasClass('active')){
												$(targetNode).removeClass('active');
											}
											$('.highlight.spelling,.highlight.suggestion,.highlight.grammar,.suggestions.spellcheck,.spellcheckOrder').remove();
											document.querySelector('[id="spellCheck_error"]').classList.remove("hidden");
											$("#spellCheck_error").find(".tm-content")[0].innerHTML="Sorry, Unable to process the spell check";
											TomloprodModal.openModal('spellCheck_error');
										});
									}
								});

								function highlightRes(data){
									if(data.status && data.status.code==200 && typeof data.status.message !== 'undefined'){
										var datas = new Array();
										var resDataArr = data.status.message;
										var datalen = Object.keys(resDataArr).length;
										for(var mainKey in resDataArr){
											var sortData = {};
											var doiValue = $("#contentContainer")[0].getAttributeNode("data-doi").value;
											var ignoredList;
											// Setting ignore words in cookie
											var getIgnoredWords = eventHandler.menu.spellCheck.getCookieValue(doiValue);
											if(getIgnoredWords!="" && getIgnoredWords!=null && getIgnoredWords!=undefined){
												ignoredList = getIgnoredWords.split(",");
											} 
											var resData = resDataArr[mainKey]["typos"];
											if(resData!=undefined && resData.length>0)
											{
												$('#spellCheck_edit .excludeignore').prop('checked', true);												
												for(var key in resData){
													/*if(typeof ignoredList !== 'undefined' && ignoredList.length > 0 && ignoredList.indexOf(resData[key].word)>=0){
														delete resData[key];
													}else{*/
														var positionlen = resData[key].positions.length;
														for(var i=0; i < positionlen; i++){
															sortData[resData[key].positions[i].from]={"word":resData[key].word,"suggestions":resData[key].suggestions,"positions":[{"from":resData[key].positions[i].from,"to":resData[key].positions[i].to,"length":resData[key].positions[i].length}]};              
														}
													//}				
												}
												var objKeys = Object.keys(sortData).sort(function (a, b) {
														return a - b;
												});
												
												for(var key in objKeys){												
													datas.push(sortData[objKeys[key]]);
													// to prevent highlight dot(.) during spellcheck - aravind
													// 1. using regex confirming presence of dot at end word
													// 2. removing dot from end of the word
													// 3. modifiying range and length present in repective object
													var currWord = datas[datas.length-1].word;
													if((currWord && currWord.match(/\.$/))){
														datas[datas.length-1].word = currWord.replace(/\.$/,'');
														datas[datas.length-1].positions[datas[datas.length-1].positions.length-1]['to'] = datas[datas.length-1].positions[datas[datas.length-1].positions.length-1]['to'] -1;
														datas[datas.length-1].positions[datas[datas.length-1].positions.length-1]['length'] = datas[datas.length-1].positions[datas[datas.length-1].positions.length-1]['length'] -1;
													}
												}
												var currNode = $("#"+mainKey);
												var leadSpaceAdd = 0;
												var url ='';
												var	description = "Spelling";
												var textNodes = textNodesUnder(currNode[0]);
												var textlen = textNodes.length;
												var processedTextLen = 0;
												var tillPrevTextLen = 0;
												var range = rangy.getSelection();
												var sel = range.getRangeAt(0);	
												var startFind = 0;
												var skipArr = 0;
												outerLoop : for(var i=0 ; i<textlen ; i++){
													if($(textNodes[i]).parent().hasClass("del") || !$(textNodes[i]).parent().is(':visible')){
														continue;
													}
													else if(spellInlineEle!="undefined" && $(textNodes[i]).closest(spellInlineEle).length>0){
														//console.log("textnode exists"+textNodes[i].data); //closest node skip deletion
														continue;
													}else{
														if(skipArr!=1){
															var len = textNodes[i].data.length;	
															tillPrevTextLen = processedTextLen;										
															processedTextLen = processedTextLen + len;
														}									
														for(var key in datas){
															if(skipArr==1){
																skipArr = 0;
																	if(startFind!=1)												
																	continue outerLoop;
															}
															var suggestions = ''; var foundNodes;
															var lenadd = srtrang = endrang = 0;
															var startNode = false, endNode = false;
															for(var keys in datas[key]["suggestions"]){
																suggestions += datas[key]["suggestions"][keys] +',';
															}
															suggestions = suggestions.replace(/,$/, '');																			
															if(startFind!=1){
																srtrang = datas[key]["positions"][0]["from"] + leadingSpace[mainKey];
															}		
															endrang = datas[key]["positions"][0]["to"] + leadingSpace[mainKey];
															
															var characterLen = textNodes[i].data.length;	
															if(startFind !=1 && srtrang < processedTextLen && endrang <= processedTextLen && characterLen >= Math.abs(tillPrevTextLen - srtrang) && !textNodes[i].nodeValue.match(/^\s+$/)){
																sel.setStart(textNodes[i], Math.abs(tillPrevTextLen - srtrang));
																sel.setEnd(textNodes[i], Math.abs(tillPrevTextLen - endrang));
																startNode = textNodes[i];
																endNode = textNodes[i];
																foundNodes = findReplace.general.highlight(range, 'spelling'); // with in this node
															}else if(startFind!=1 && srtrang < processedTextLen && characterLen >= Math.abs(tillPrevTextLen - srtrang) && !textNodes[i].nodeValue.match(/^\s+$/)){
																sel.setStart(textNodes[i], Math.abs(tillPrevTextLen - srtrang)); // starting position in this node
																startFind = 1;
																startNode = textNodes[i];
																break;
															}else if(endrang <= processedTextLen && characterLen >= Math.abs(tillPrevTextLen - endrang) && !textNodes[i].nodeValue.match(/^\s+$/)){
																sel.setEnd(textNodes[i], Math.abs(tillPrevTextLen - endrang)); // ending position in this node
																foundNodes = findReplace.general.highlight(range, 'spelling');
																startFind = 0;
																endNode = textNodes[i];					
															}else{
																	//no match in this node
																break;
															}
															
															if(foundNodes!=undefined && foundNodes.length > 0){
																var addDisableattr = 0;
																if(typeof ignoredList !== 'undefined' && ignoredList.length > 0 && ignoredList.indexOf(datas[key].word)>=0){
																	$(foundNodes).attr('data-ignoreall',"true");
																}
																if ($(startNode,endNode).closest('.del').length > 0 || $(startNode,endNode).closest('.floatLabel').length > 0 || ($(startNode,endNode).closest('.front').length > 0 && $(startNode,endNode).closest('.jrnlArtTitle').length == 0 && $(startNode,endNode).closest('.jrnlAbsGroup').length == 0) || ($(startNode,endNode).closest('.back').length > 0 && $(startNode,endNode).closest('.jrnlAppGroup').length == 0) || $(startNode,endNode).closest('[class$="Ref"]').length>0){ // Replace should not happen in component/citation/reference - priya #93
																	addDisableattr = 1;
																	if($(startNode,endNode).closest('.back').length > 0 && $(startNode,endNode).closest('[data-id^="BLK_"]').length > 0 &&  $(startNode,endNode).closest('.floatLabel').length == 0){
																		addDisableattr = 0;
																	}
																}
																if(addDisableattr==1){
																	$(foundNodes).attr('data-replace',"false");
																}
																hightlightFound = 1;
																$(foundNodes).attr('data-channel', 'menu').attr('data-topic', 'spellCheck').attr('data-event', 'click').attr('data-message', '{\'funcToCall\': \'suggest\'}').attr('data-suggestion', suggestions).attr('data-url', url).attr('data-desc', description).attr('start-offset',datas[key]["positions"][0]["from"] + leadingSpace[mainKey]).attr('end-offset',datas[key]["positions"][0]["to"] + leadingSpace[mainKey]);
																if ($(foundNodes)[0].hasAttribute('data-cont')){
																	$(foundNodes).attr('data-rid', r);
																}else{
																	$(foundNodes).attr('data-rid', ++r);
																}
																delete datas[key];
															}
														}
													}
												}	
											}
										}
									}
								}

								function showModel(){
									var clonedContentdata = $('[data-topic="spellCheck"][data-rid]');
									$('.la-container').fadeOut(function(){
										if(clonedContentdata.length>0){
											var clonedContentLen = clonedContentdata.length;
											var contentActivePosition = clonedContentLen-1;
											if(clonedContentLen==1 && $(clonedContentdata).attr('data-ignoreall')=="true"){
												$(".icon-spell-check").parent().removeClass("active");
												$(".spellcheckOrder").remove();
												TomloprodModal.closeModal();
											}else{
												var foundNextActive = true;
												while(foundNextActive){
													if($(clonedContentdata[contentActivePosition]).attr('data-ignoreall')=="true"){
														contentActivePosition--;
													}else{
														foundNextActive = false;
													}		
												}
												$('#spellCheck_edit .change, #spellCheck_edit .changeall').removeClass("disabled");
												if($(clonedContentdata[contentActivePosition]).attr('data-replace')=="false"){
													$('#spellCheck_edit .change, #spellCheck_edit .changeall').addClass("disabled");
												}
												//when we check the perticular text at that time there is no words is there for replacing at that time code will break
												if(clonedContentdata[contentActivePosition]){
													var parentNode = $("#"+clonedContentdata[contentActivePosition].getAttributeNode("data-parent-id").value);
													var parentNodeclone = $(parentNode).clone(true);
													var strtOffset = clonedContentdata[contentActivePosition].getAttributeNode("start-offset").value;
													var edOffset = clonedContentdata[contentActivePosition].getAttributeNode("end-offset").value;
													var displyWrd = clonedContentdata[contentActivePosition].getAttributeNode("data-content").value;
													var concatcontent = "";
													if(spellInlineEle!='undefined'){
														$(parentNodeclone).find(spellInlineEle).remove();
													}
													var textNodeValue = parentNodeclone[0].textContent;
													if(strtOffset==0){
														concatcontent = "<span id='myTextArea' style='color:red;font-size:17px;'>"+displyWrd+"</span>"+textNodeValue.substr(edOffset,15).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s-)([\W\w]*)(\b\s*$)/g, '$2');
													}else{
														var stend = 15;
														if(strtOffset-15 < 0){
															stend = strtOffset;
															strtOffset = 0;
														}else{
															strtOffset = strtOffset - 15;
														}
														concatcontent = textNodeValue.substr(strtOffset,stend).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2')+" <span id='myTextArea' style='color:red;font-size:17px;'>"+displyWrd+"</span>"+textNodeValue.substr(edOffset ,15).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s-)([\W\w]*)(\b\s*$)/g, '$2');
													}
													$('[id="spellCheck_edit"] [data-spell-text]').html(concatcontent);
													$(clonedContentdata[contentActivePosition]).addClass('active');
													var suggList = clonedContentdata[contentActivePosition].getAttributeNode("data-suggestion").value.split(",");
													var suggData = "";
													for(var keys in suggList){
														suggData += "<p data-channel='menu' data-topic='spellCheck' data-event='click' data-message='{\"funcToCall\" : \"selectSuggestions\"}'>"+suggList[keys]+"</p>"
													}
													$('[id="spellCheck_edit"] [data-spell-suggList]').html(suggData);
													$('#clonedContent').prepend("<span class='spellcheckOrder' data-id-order='"+orderKey.replace(/,+$/, '')+"'></span>");
													document.querySelector('[id="spellCheck_edit"]').classList.remove("hidden");
													TomloprodModal.openModal('spellCheck_edit');
													var curr = $('.highlight.spelling.active');
													$('#clonedContent').scrollTop($('#contentDivNode').scrollTop());
													if ($(curr).offset().top < 0 || ($(curr).offset().top + $(curr).height()) > $('#contentDivNode').height()){
														var scrollTop = ($('#contentDivNode').scrollTop() + $(curr).parent().offset().top) - $('#contentDivNode').offset().top + $(curr).position().top;
														$('#contentDivNode').scrollTop(scrollTop);
													}
												}else{//this for showing modal(No modifications are needed
													document.querySelector('[id="spellCheck_error"]').classList.remove("hidden");
													$(".icon-spell-check").parent().removeClass("active");
													$("#spellCheck_error").find(".tm-content")[0].innerHTML="No modifications are needed";
													TomloprodModal.openModal('spellCheck_error');
												}
											}
										}else{
											document.querySelector('[id="spellCheck_error"]').classList.remove("hidden");
											$(".icon-spell-check").parent().removeClass("active");
											$("#spellCheck_error").find(".tm-content")[0].innerHTML="No modifications are needed";
											TomloprodModal.openModal('spellCheck_error');
										}
									});
								}

							}

						});
					}catch(e){
						console.log("Error :",e);
						$('.la-container').fadeOut(function(){
							if ($(targetNode).hasClass('active')){
								$(targetNode).removeClass('active');
							}
							$('.highlight.spelling,.highlight.suggestion,.highlight.grammar,.suggestions.spellcheck,.spellcheckOrder').remove();
							document.querySelector('[id="spellCheck_error"]').classList.remove("hidden");
							$("#spellCheck_error").find(".tm-content")[0].innerHTML="Sorry, Unable to process the spell check";
							TomloprodModal.openModal('spellCheck_error');
						});
					}
				}
			},
			spellLangChange: function(param, targetNode){
				var spellLang = $(targetNode).val();
				$('#contentContainer').attr('data-spelling-language', spellLang);
				$.ajax({
					type: "POST",
					url: "/api/updatearticle",
					data: {
						'customerName':kriya.config.content.customer,
						'projectName':kriya.config.content.project,
						'doi':kriya.config.content.doi,
						'xmlFrag': '<custom-meta><meta-name>Spelling language</meta-name><meta-value>' + spellLang + '</meta-value></custom-meta>',
						'xpath' : '//article//article-meta//custom-meta-group/custom-meta[meta-name[.="Spelling language"]]',
						'insertInto' : '//article//article-meta//custom-meta-group'
					},
					success: function(res){
						if(res.error){
							console.log(res);
						}else{
							console.log('Spelling language update success.');
						}
					},
					error: function(err){
						console.log(err);
					}
				});
			},
			selectSuggestions: function(param, targetNode){
				$('#mySuggestions').find(".spellSelect").removeClass("spellSelect");
				$(targetNode).addClass("spellSelect");
			},
			suggest: function(param, targetNode){
					$('.suggestions.spellcheck').remove();
					$('.highlight.active').removeClass('active');
					$(targetNode).addClass('active');
					var suggestion = $('<div class="suggestions spellcheck">');
					suggestion.append('<span class="desc">' + $(targetNode).attr('data-desc') + '</span>');
					if ($(targetNode).attr('data-suggestion') != ''){
						var suggests = $(targetNode).attr('data-suggestion').split(',');
						for (var i = 0, l = suggests.length; i < l; i++) { 
							suggestion.append('<span class="replaceOption" data-channel="menu" data-topic="spellCheck" data-event="click" data-message="{\'funcToCall\': \'replaceOption\'}">' + suggests[i] + '</span>');
						}
					}
					if ($(targetNode).attr('data-url') != ''){
						var suggests = $(targetNode).attr('data-url').split(',');
						for (var i = 0, l = suggests.length; i < l; i++) { 
							suggestion.append('<span class="url-link" data-channel="menu" data-topic="spellCheck" data-event="click" data-message="{\'funcToCall\': \'explainURL\'}" data-href="' + suggests[i] + '">Explain...</span>');
						}
					}
					suggestion.append('<span class="ignore" data-channel="menu" data-topic="spellCheck" data-event="click" data-message="{\'funcToCall\': \'ignoreSuggestion\'}">Ignore suggestion</span>');
					suggestion.append('<span class="ignoreAll" data-channel="menu" data-topic="spellCheck" data-event="click" data-message="{\'funcToCall\': \'ignoreSuggestion\', \'param\': \'all\'}">Ignore all</span>');
					$(suggestion).css({'top': ($(targetNode).offset().top + $(targetNode).height()), 'left': $(targetNode).offset().left})
					$('body').append(suggestion);
			},
			replaceOption: function(param, targetNode){
				var curr = $('.highlight.active');
				var dataCidAtrr;
				var spellIgnoreNode = $('[id="spellCheck_edit"][data-ignore-class][data-class-elements]');
				var spellInlineEle;
				if(spellIgnoreNode.length>0){
					spellInlineEle = $(spellIgnoreNode)[0].getAttributeNode('data-class-elements').value;
				}	
				var replaceText;
				if(typeof(targetNode)=='string'){
					replaceText = targetNode;
				}else{
					replaceText = $(targetNode).text();
				}
				var replacedNodes = findReplace.general.replace({'replaceText': replaceText, 'replaceNodes': curr,'skipTrack':true, 'spellInlineEle' :spellInlineEle });
				//re-tag all elements to adjust the underlay positions
				var currentNode = $('#contentDivNode #' + curr.parent().attr('id'))[0];
				curr.parent().height($(currentNode).height());
				if (curr.nextAll().length > 0){
					var parentNode = curr.parent();
					$(curr).remove();
					eventHandler.menu.findReplace.reSearch(parentNode,$(curr)[0].getAttributeNode("data-parent-id").value,"false");
				}else{
					$(curr).remove();
				}
				$('.suggestions.spellcheck').remove();
				$(replacedNodes).each(function(){
					dataCidAtrr = $(this)[0].getAttributeNode("data-cid").value;
					eventHandler.menu.observer.addTrackChanges($(this)[0], 'added');
					kriyaEditor.settings.undoStack.push($(this)[0]);
				});
				
				if(dataCidAtrr!=undefined){
					var trackedNode = $('#contentDivNode [data-cid="' + dataCidAtrr + '"]');
					if(trackedNode!=undefined && trackedNode.length>0){
						$(trackedNode).each(function(){
							$(this).attr("data-type","spelling");
						});
					}
				}		
				kriyaEditor.init.addUndoLevel('navigation-key');
			},
			explainURL: function(param, targetNode){
				kriya.evt.preventDefault();
				$.get( "/api/getURL?url=" + $(targetNode).attr('data-href'), function( data ) {
					$('#url-modal').remove();
					var urlModal = $('<div id="url-modal" class="card">');
					data = '<h2>' + $(targetNode).parent().find('.desc').text() + '<i class="icon-cross" onclick="$(\'#url-modal\').remove()" style="float: right;cursor: pointer;"> </i></h2>' + '<div>'+data+'</div>';
					$(urlModal).html(data);
					$('body').append(urlModal);
					$('.suggestions.spellcheck').remove();
					//console.log(data);
				});
			},
			ignoreSuggestion: function(param, targetNode){
				var content = $('.highlight.active').attr('data-content');
				if (param == "all"){					
					$('.highlight[data-content="'+content+'"]').remove();

					var doiValue = $("#contentContainer")[0].getAttributeNode("data-doi").value;
					var getIgnoreWords = eventHandler.menu.spellCheck.getCookieValue(doiValue);
					if(getIgnoreWords!="" && getIgnoreWords!=null && getIgnoreWords!=undefined){
						getIgnoreWords = getIgnoreWords+","+content;
					}else{
						getIgnoreWords = content;
					}
					document.cookie = doiValue + "=" + getIgnoreWords + ";";
				}else{
					var rid = $('.highlight.active').attr('data-rid');
					$('.highlight[data-rid="'+rid+'"]').remove();
				}
				$('.suggestions.spellcheck').remove();
			},
			showignoredwords: function(param, targetNode){
				var showIgnore = "false";
				if($(targetNode).prop('checked') == true){
					showIgnore = "true";
				}
				$('#clonedContent .highlight[data-ignoreall]').each(function(){
					$(this).attr('data-ignoreall',showIgnore);
				})
			},
			loadSpellCheckPopup: function(param){
				var changeSuggestions;
				$("#spellSuggErr").hide();
				var spellIgnoreNode = $('[id="spellCheck_edit"][data-ignore-class][data-class-elements]');
				var spellInlineEle;
				if(spellIgnoreNode.length>0){
					spellInlineEle = $(spellIgnoreNode)[0].getAttributeNode('data-class-elements').value;
				}
				var ignoreWord = $('#myTextArea').html();			
				if(param.method=="ignoreall"){
					var doiValue = $("#contentContainer")[0].getAttributeNode("data-doi").value;
					var getIgnoreWords = eventHandler.menu.spellCheck.getCookieValue(doiValue);
					if(getIgnoreWords!="" && getIgnoreWords!=null && getIgnoreWords!=undefined){
						getIgnoreWords = getIgnoreWords+","+ignoreWord;
					}else{
						getIgnoreWords = ignoreWord;
					}
					document.cookie = doiValue + "=" + getIgnoreWords + ";";
				}
				if(param.method=="ignoreall"){						
					$('[data-topic="spellCheck"][data-content="'+ignoreWord+'"]').remove();
				}else if(param.method=="ignore"){
					var cloneddata = $('[data-topic="spellCheck"][data-rid]');
					var dataRidId = cloneddata[cloneddata.length-1].getAttributeNode("data-rid").value;
					$('[data-topic="spellCheck"][data-rid="'+dataRidId+'"]').remove();					
				}else if(param.method=="change"){
					if($('[id="spellCheck_edit"]').find('.spellSelect').length>0){
						changeSuggestions = $('[id="spellCheck_edit"]').find('.spellSelect').html();
					}else{
						$("#spellSuggErr").html("please select a suggestion.").css("color", "red");;
						$("#spellSuggErr").show();
						return;
					}
					var cloneddata = $('[data-topic="spellCheck"][data-rid]');
					//$(cloneddata[0]).addClass('active');
					eventHandler.menu.spellCheck.replaceOption('',changeSuggestions);
				}else if(param.method=="changeall"){
					if($('[id="spellCheck_edit"]').find('.spellSelect').length>0){
						changeSuggestions = $('[id="spellCheck_edit"]').find('.spellSelect').html();
					}else{
						$("#spellSuggErr").html("please select a suggestion.").css("color", "red");
						$("#spellSuggErr").show();
						return;
					}
					var nodeLen = $('[data-topic="spellCheck"][data-content="'+ignoreWord+'"]').length;
					if(nodeLen>0){
						for(var n=0; n<nodeLen;n++){
							var cloneddata = $('[data-topic="spellCheck"][data-content="'+ignoreWord+'"]');
							$(cloneddata[0]).addClass('active');
							eventHandler.menu.spellCheck.replaceOption('',changeSuggestions);
						}
					}	
				}
				$('.suggestions.spellcheck').remove();
				var clonedContentdata = $('[data-topic="spellCheck"][data-rid]');
				if(clonedContentdata.length>0){
					var clonedContentLen = clonedContentdata.length;
					var contentActivePosition = clonedContentLen-1;
					if(clonedContentLen==1 && $(clonedContentdata).attr('data-ignoreall')=="true"){
						$(".icon-spell-check").parent().removeClass("active");
						$(".spellcheckOrder").remove();
						TomloprodModal.closeModal();
					}else{
						var foundNextActive = true;
						while(foundNextActive){
							if($(clonedContentdata[contentActivePosition]).attr('data-ignoreall')=="true"){
								contentActivePosition--;
							}else{
								foundNextActive = false;
							}		
						}
						$('#spellCheck_edit .change, #spellCheck_edit .changeall').removeClass("disabled");
						if($(clonedContentdata[contentActivePosition]).attr('data-replace')=="false"){
							$('#spellCheck_edit .change, #spellCheck_edit .changeall').addClass("disabled");
						}
						//if we dont check clonedContentdata[contentActivePosition] value is undefined or not when replace or ignore... of last spellcheck node at that time code will be break
						if(clonedContentdata[contentActivePosition]){
							var parentNode = $("#"+clonedContentdata[contentActivePosition].getAttributeNode("data-parent-id").value);
							var parentNodeclone = $(parentNode).clone(true);
							var strtOffset = clonedContentdata[contentActivePosition].getAttributeNode("start-offset").value;
							var edOffset = clonedContentdata[contentActivePosition].getAttributeNode("end-offset").value;
							var displyWrd = clonedContentdata[contentActivePosition].getAttributeNode("data-content").value;
							var concatcontent = "";
							if(spellInlineEle!='undefined'){
								$(parentNodeclone).find(spellInlineEle).remove();
							}
							var textNodeValue = parentNodeclone[0].textContent;
							if(strtOffset==0){
								concatcontent = "<span id='myTextArea' style='color:red;font-size:17px;'>"+displyWrd+"</span>"+textNodeValue.substr(edOffset,15).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s-)([\W\w]*)(\b\s*$)/g, '$2');
							}else{
								var stend = 15;
								if(strtOffset-15 < 0){
									stend = strtOffset;
									strtOffset = 0;
								}else{
									strtOffset = strtOffset - 15;
								}
								concatcontent = textNodeValue.substr(strtOffset,stend).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2')+" <span id='myTextArea' style='color:red;font-size:17px;' >"+displyWrd+"</span>"+textNodeValue.substr(edOffset ,15).replace(/(?!\w|\s)./g, '').replace(/\s+/g, ' ').replace(/^(\s-)([\W\w]*)(\b\s*$)/g, '$2');
							}
							$('[id="spellCheck_edit"] [data-spell-text]').html(concatcontent);
							$(clonedContentdata[contentActivePosition]).addClass('active');
							var suggList = clonedContentdata[contentActivePosition].getAttributeNode("data-suggestion").value.split(",");
							var suggData = "";
							for(var keys in suggList){
								suggData += "<p data-channel='menu' data-topic='spellCheck' data-event='click' data-message='{\"funcToCall\" : \"selectSuggestions\"}' style='margin:0px;cursor: pointer;font-size:17px;'>"+suggList[keys]+"</p>";
							}
							$('[id="spellCheck_edit"] [data-spell-suggList]').html(suggData);
							var curr = $('.highlight.spelling.active');
							$('#clonedContent').scrollTop($('#contentDivNode').scrollTop());
							if ($(curr).offset().top < 0 || ($(curr).offset().top + $(curr).height()) > $('#contentDivNode').height()){
								var scrollTop = ($('#contentDivNode').scrollTop() + $(curr).parent().offset().top) - $('#contentDivNode').offset().top + $(curr).position().top;
								$('#contentDivNode').scrollTop(scrollTop);
							}
						}else{//this is for close spell check modal
							$(".icon-spell-check").parent().removeClass("active");
							$(".spellcheckOrder").remove();
							TomloprodModal.closeModal();
						}
					}
				}else{
					//kriya.popUp.closePopUps($('[id="spellCheck_edit"]'));
					$(".icon-spell-check").parent().removeClass("active");
					$(".spellcheckOrder").remove();
					TomloprodModal.closeModal();
				}
			},
			getCookieValue : function(cname) {
				var name = cname + "=";
				//when we are decoding cookie values,if cookie values have any wrong data at that time code breaking.
				try{
					var decodedCookie = decodeURIComponent(document.cookie);
					var ca = decodedCookie.split(';');
					for(var i = 0; i < ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0) == ' ') {
							c = c.substring(1);
						}
						if (c.indexOf(name) == 0) {
							return c.substring(name.length, c.length);
						}
					}
				}catch(e){
					console.log("Error :",e);
					/*$('.la-container').fadeOut(function(){
						if ($(targetNode).hasClass('active')){
							$(targetNode).removeClass('active');
						}
						$('.highlight.spelling,.highlight.suggestion,.highlight.grammar,.suggestions.spellcheck,.spellcheckOrder').remove();
						document.querySelector('[id="spellCheck_error"]').classList.remove("hidden");
						$("#spellCheck_error").find(".tm-content")[0].innerHTML="Sorry, Unable to process the spell check";
						TomloprodModal.openModal('spellCheck_error');
					});*/
				}
				return "";
			},
		},
		upload: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				//If the param has block node then show the progess bar on the block
				if(block){
					block.progress('Uploading to server');
				}
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {'filename': file.name},
					success: function(response){
						if (response && response.upload_url){
							var formData = new FormData();
							$.each(response.params, function(k, v){
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.menu.upload.uploadToServer(param, targetNode);
						}
					},
					error: function(err){
						if(param.error && typeof(param.error) == "function"){
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								if(block){
									block.progress('Uploading to server ' + percentComplete);
								}
							}
						}, false);
						return xhr;
					},
					success: function(res){
						if(block){
							block.progress('Converting for web view');
						}
						if (res && res.hasChildNodes()){
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
										srcBucket: uploadResult.find('Bucket,bucket').text(),
										srcKey: uploadResult.find('Key,key').text(),
										dstBucket: "kriya-resources-bucket",
										dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
										convert: 'true'
									}
								if(param.convert){
									params.convert = param.convert;
								}
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function(status){
										//Call the sucess function if req was successfully completed
										if(param.success && typeof(param.success) == "function"){
											param.success(status);
										}
									},
									error: function(err){
										//Call the error function if req failed
										if(param.error && typeof(param.error) == "function"){
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			}
		},
		export:{
			exportToPDF: function(param, targetNode, checkCitation, callbackFn, popper){
				var callBackExst = (typeof callbackFn == "function") ? true : false;
				//var doi = $('.jrnlDOI a').text().replace('https://doi.org/10.3389/','');
				if(checkCitation != false && $('#contentContainer').attr('data-citation-trigger') == "true"){
					var reorderStatus = citeJS.floats.renumberConfirmation('exportToPDF');
					if(reorderStatus == false){
						return false;
					}
				}
				//add to html validation
				eventHandler.components.general.validationHTML();
				
				//An alert message to confirm that figures are relabeled
				if ($('[data-component="figure_relabel_confirmation_edit"]').length > 0){
					if(param && param.confirmation == "true"){
						kriya.popUp.closePopUps($('[data-component="figure_relabel_confirmation_edit"]'));
					}else{
						if($('[data-component="figure_relabel_confirmation_edit"]').attr('jquerySelector') && $($('[data-component="figure_relabel_confirmation_edit"]').attr('jquerySelector')).text() == $('[data-component="figure_relabel_confirmation_edit"]').attr('valueToCheck')){
							kriya.popUp.openPopUps($('[data-component="figure_relabel_confirmation_edit"]'));
							return false;
						}
					}
				}

				var proofType = 'online'; //deafault proof type
				// to ask user about the reason to proof - JAI
				if ($('[data-component="proofing-issue-tracker_edit"]').length > 0){
					if ($('[data-component="proofing-issue-tracker_edit"]').hasClass('hidden')){
						$('[data-component="proofing-issue-tracker_edit"]').removeAttr('data-proof-type');
						$('[data-component="proofing-issue-tracker_edit"] proof-tracker').remove();
						$('#proofingIssueTracker').val('---');
						$('#proofingIssueTracker').closest('[data-wrapper]').find('[data-class="other-reason"]').text('').addClass('hidden');
						if(param && param.type){
							$('[data-component="proofing-issue-tracker_edit"]').attr('data-proof-type', param.type);
						}
						kriya.popUp.openPopUps($('[data-component="proofing-issue-tracker_edit"]'));
						return true;
					}else if ($('#proofingIssueTracker').val() == "---"){
						$('[data-component="proofing-issue-tracker_edit"] proof-tracker').remove();
						//do nothing, as the user has not selected any option
						return false;
					}else if ($('#proofingIssueTracker').val() != "---"){
						if ($('#proofingIssueTracker').val() == "others" && /^\s*$/.test($('#proofingIssueTracker').closest('[data-wrapper]').find('[data-class="other-reason"]').text())){
							return true;
						}
						if($('[data-component="proofing-issue-tracker_edit"]').attr('data-proof-type')){
							proofType = $('[data-component="proofing-issue-tracker_edit"]').attr('data-proof-type');
						}
						$('[data-component="proofing-issue-tracker_edit"] proof-tracker').remove();

						var pdfReason = '<node data-xpath="//workflow//stage[last()]//pdf[last()]" data-type="add-attribute" data-name="reason" data-value="' + $('#proofingIssueTracker').val() + '"></node>';
						if ($('#proofingIssueTracker').val() == "others"){
							pdfReason += '<node data-xpath="//workflow//stage[last()]//pdf[last()]" data-type="add-attribute" data-name="description" data-value="' + $('#proofingIssueTracker').closest('[data-wrapper]').find('[data-class="other-reason"]').text() + '"></node>';
						}
						pdfReason = $('<proof-tracker>' + pdfReason + '</proof-tracker>');
						$('[data-component="proofing-issue-tracker_edit"]').append(pdfReason);
						kriya.popUp.closePopUps($('[data-component="proofing-issue-tracker_edit"]'));
					}
				}

				//add probe validation before exporttopdf added by vijayakumar on 9-11-2018
				$('#contentDivNode *[data-validation-error="true"]').removeAttr('data-validation-error', 'true');
				if(popper){
					popper.progress('Probe validation in progress... Please wait.');
					$(popper).find('.loadingProgress').css('bottom', '0px');
					$(popper).find('.loading-wraper').css('opacity', '0');
				}else{
					var notificationID = kriya.notification({
						title : 'Probe validation in progress... Please wait.',
						type  : 'success',
						content : ''
					});
				}
				eventHandler.welcome.signoff.probeValidation('exeter', '', function(status){
				if(status){
					eventHandler.welcome.signoff.probeValidation('proof', '', function(status){
					if(status){
						var doi = kriya.config.content.doi;
						var customer = kriya.config.content.customer;
						var project = kriya.config.content.project;
						var userName = kriyaEditor.user.name;
						var userRole = kriya.config.content.role;
						if(popper){
							popper.progress('PDF generation in progress... Please wait.');
							$(popper).find('.loadingProgress').css('bottom', '0px');
							$(popper).find('.loading-wraper').css('opacity', '0');
						}else{
							$('#toast-container .toast').remove();
							var notificationID = kriya.notification({
									title : 'PDF generation in progress... Please wait.',
									type  : 'success',
									content : ''
								});
						}
						if (param && param.type) proofType = param.type;
						var parameters = {
							"client": customer, 
							"project": project, 
							"idType": "doi", 
							"id":doi, 
							"processType": 
							"InDesignSetter", 
							"proof": proofType, 
							"userName": userName, 
							"userRole": userRole 
						};
						if (param && param.pdfType) parameters.pdfType = param.pdfType;

						$.ajax({
							type: 'POST',
							url: "/api/pagination",
							data: parameters,
							crossDomain: true,
							success: function(data){
								if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
									eventHandler.menu.export.getJobStatus(customer, data.message.jobid, notificationID, userName, userRole, doi, callbackFn, popper);
								}
								else if(data == ''){
									$('#'+notificationID+' .kriya-notice-body').html('Failed..');
								}else{
									$('#'+notificationID+' .kriya-notice-body').html(data.status.message);
									//#481 All Customers | PDF Generation | PDF Generation Popup Message Prabakaran.A(prabakaran.a@focusite.com)
									//Update job status while the job already exist.
									if(data.status.message == "job already exist"){
										setTimeout(function() {
											eventHandler.menu.export.getJobStatus(customer, data.message.jobid, notificationID, userName, userRole, doi, callbackFn, popper);
										}, 1000 );
									}
									//End of #481
								}
							},
							error: function(error){
								if(popper){
									popper.progress('Failed..');
									popper.progress('',true);
								}else{
									$('#'+notificationID+' .kriya-notice-body').html('Failed..');
								}
								if(callBackExst) callbackFn('error','Export to pdf failed',notificationID);
							}
						});
					}else{
						$('.la-container').fadeOut()
					}}, function(){
						if(popper){
							popper.progress('Probe Validation Failed.');
							popper.progress('',true);
						}else{
							kriya.notification({title : 'ERROR',type  : 'error',content : 'Probe Validation Failed.',icon: 'icon-warning2'});
						}
				});
				}else{
					$('.la-container').fadeOut()
				}}, function(){
					if(popper){
						popper.progress('Probe Validation Failed.');
						popper.progress('',true);
					}else{
						kriya.notification({title : 'ERROR',type  : 'error',content : 'Probe Validation Failed.',icon: 'icon-warning2'});
					}
				});
			},
			exportToOnlinePDF: function(param, targetNode, checkCitation, callbackFn, popper){
				var notificationID = kriya.notification({
					title : 'Online PDF generation in progress... Please wait.',
					type  : 'success',
					content : ''
				});
				var doi = kriya.config.content.doi;
				var customer = kriya.config.content.customer;
				var project = kriya.config.content.project;
				var userName = kriyaEditor.user.name;
				var userRole = kriya.config.content.role;
				var parameters = {
					"client": customer, 
					"project": project, 
					"idType": "doi", 
					"id":doi, 
					"processType": 	"InDesignSetter", 
					"proof": 'online', 
					"userName": userName, 
					"userRole": userRole 
				};
				$.ajax({
					type: 'POST',
					url: "/api/pagination",
					data: parameters,
					crossDomain: true,
					success: function(data){
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
							eventHandler.menu.export.getJobStatus(customer, data.message.jobid, notificationID, userName, userRole, doi, callbackFn, popper);
						}
						else if(data == ''){
							$('#'+notificationID+' .kriya-notice-body').html('Failed..');
						}else{
							$('#'+notificationID+' .kriya-notice-body').html(data.status.message);
							//#481 All Customers | PDF Generation | PDF Generation Popup Message Prabakaran.A(prabakaran.a@focusite.com)
							//Update job status while the job already exist.
							if(data.status.message == "job already exist"){
								setTimeout(function() {l
									eventHandler.menu.export.getJobStatus(customer, data.message.jobid, notificationID, userName, userRole, doi, callbackFn, popper);
								}, 1000 );
							}
							//End of #481
						}
					},
					error: function(error){
						if(popper){
							popper.progress('Failed..');
							popper.progress('',true);
						}else{
							$('#'+notificationID+' .kriya-notice-body').html('Failed..');
						}
						if(callBackExst) callbackFn('error','Export to pdf failed',notificationID);
					}
				});
			},
			exportToXML: function(param, targetNode, checkCitation){
				
				/*if(checkCitation != false && $('#contentContainer').attr('data-citation-trigger') == "true"){
					var reorderStatus = citeJS.floats.renumberConfirmation('exportToXML');
					if(reorderStatus == false){
						return false;
					}
				}*/

				var doi = kriya.config.content.doi;
				var customer = kriya.config.content.customer;
				var project = kriya.config.content.project;
				var userName = kriyaEditor.user.name;
				var userRole = kriya.config.content.role;
				var url = "/api/getxml?doi="+doi+"&project="+project+"&customer="+customer;
				window.open(url);
			},
			exportToWord: function(param, targetNode){
				var doi = kriya.config.content.doi;
				var customer = kriya.config.content.customer;
				var project = kriya.config.content.project;
				var url = "/api/htmltodoc?doi="+doi+"&project="+project+"&customer="+customer;
				window.open(url);
			},
			editXmlcmd: function (param, targetNode){
				if (param.action == "wordwrap"){
					var wrapMode = $(targetNode).attr('data-word-wrap');
					if(wrapMode == "true"){
						xmlEditor.getSession().setUseWrapMode(true);
						$(targetNode).attr('data-word-wrap','false');
					}else{
						xmlEditor.getSession().setUseWrapMode(false);
						$(targetNode).attr('data-word-wrap','true');
					}
				}else{
					xmlEditor.execCommand(param.action);
				}
			},
			editXml: function (param, targetNode){
				var parameters  = {
					'doi': kriya.config.content.doi,
					'customer': kriya.config.content.customer,
					'project':kriya.config.content.project,
					'role': kriya.config.content.role,
					'stage':kriya.config.content.stage,
					'xmltype':"raw"
				};
				var popper  = $('[data-component="jrnlXmlEditor"]')
				$(popper).find('[data-word-wrap]').attr('data-word-wrap','true');
				kriya['styles'].init(targetNode,'jrnlXmlEditor');
				$(popper).progress('Opening Document')
				kriya.general.sendAPIRequest('editasxml', parameters, function(res){
					$(popper).progress('',true);

						xmlEditor = ace.edit("xmlEditor");
						var formatted = ''; // to beautify xml / source:https://gist.github.com/minhd/4057197
						var reg = /(>)(<)(\/*)/g;
						xml = res.replace(reg, '$1\r\n$2$3');
						var pad = 0;
						jQuery.each(xml.split('\r\n'), function (index, node) {
							var indent = 0;
							if (node.match(/.+<\/\w[^>]*>$/)) {
								indent = 0;
							} else if (node.match(/^<\/\w/)) {
								if (pad != 0) {
									pad -= 1;
								}
							} else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
								indent = 1;
							} else {
								indent = 0;
							}
							var padding = '';
							for (var i = 0; i < pad; i++) {
								padding += '  ';
							}
							formatted += padding + node + '\r\n';
							pad += indent;
						});
						xmlEditor.setValue(formatted, -1);
						xmlEditor.setTheme("ace/theme/chrome");
						xmlEditor.getSession().setMode("ace/mode/xml");
					});
				},
				getJobStatus: function (customer, jobID, notificationID, userName, userRole, doi, callbackFn, popper) {
					var callBackExst = (typeof callbackFn == "function") ? true : false;
					//console.log('Job ID '+jobID);
					$.ajax({
						type: 'POST',
						url: "/api/jobstatus",
						data: { "id": jobID, "userName": userName, "userRole": userRole, "doi": doi, "customer": customer },
						crossDomain: true,
						success: function (data) {
							if (data && data.status && data.status.code && data.status.message && data.status.message.status) {
								var status = data.status.message.status;
								var code = data.status.code;
								var currStep = 'Queue';
								if (data.status.message.stage.current) {
									currStep = data.status.message.stage.current;
								}
								var loglen = data.status.message.log.length;
								var process = data.status.message.log[loglen - 1];
								if (/completed/i.test(status)) {
									if (location.pathname == '/proof_review/') {
										eventHandler.components.pdfviewer.reloadIframe();
										$('.pdfViewHeader .pdfViewMenu').addClass('disabled');
										$('.pdfViewHeader #pdfNotice').addClass('hidden');
									}
									var loglen = data.status.message.log.length;
									var process = data.status.message.log[loglen - 1];
									if(popper){
										popper.progress(process);
										$(popper).find('.loadingProgress').css('bottom', '0px');
										$(popper).find('.loading-wraper').css('opacity', '0');
									}else{
										$('#' + notificationID + ' .kriya-notice-body').html(process);
									}
									kriya.config.content.changes = false;
									if(popper){
										popper.progress('PDF generation completed');
										$(popper).find('.loadingProgress').css('bottom', '0px');
										$(popper).find('.loading-wraper').css('opacity', '0');
										popper.progress('',true);
									}else{
										$('#' + notificationID + ' .kriya-notice-header').html('PDF generation completed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
									}
									var proofstatus = $('#' + notificationID + ' .kriya-notice-body a[data-json]').attr('data-json')
									if(proofstatus.match(/\'noFontMissing\'\:false/)){
										$('[data-component="proofing-issue-tracker_edit"] proof-tracker').append($('<node data-xpath="//workflow//stage[last()]//pdf[last()]" data-type="add-attribute" data-name="data-noFontMissing" data-value="false"></node>'));
										$('#contentDivNode').attr('data-noFontMissing', 'false')
									}
									if(proofstatus.match(/\'noEntityMissing\'\:false/)){
										$('[data-component="proofing-issue-tracker_edit"] proof-tracker').append($('<node data-xpath="//workflow//stage[last()]//pdf[last()]" data-type="add-attribute" data-name="data-noEntityMissing" data-value="false"></node>'));
										$('#contentDivNode').attr('data-noEntityMissing', 'false');
									}
									if(data.status.message.input.proof && data.status.message.input.proof == "print"){
										eventHandler.menu.export.exportToOnlinePDF();
									}
									// to send the reason for proofing to workflow - JAI
									if ($('[data-component="proofing-issue-tracker_edit"] proof-tracker').length > 0) {
										$('[data-component="proofing-issue-tracker_edit"] proof-tracker').append($('<node data-xpath="//workflow//stage[last()]//pdf[last()]" data-type="add-attribute" data-name="job-id" data-value="' + jobID + '"></node>'));
										var proofParam = { 'customer': kriya.config.content.customer, 'project': kriya.config.content.project, 'doi': kriya.config.content.doi };
										proofParam.data = '<root>' + $('[data-component="proofing-issue-tracker_edit"] proof-tracker').html() + '</root>';
										kriya.general.sendAPIRequest('updateattribute', proofParam, function (res) { });
									}
									var jsondata = $(process).attr('data-json').replace(/\'/g, '"');
									try {
										jsondata = $.parseJSON(jsondata);
									} catch (e) {
										if (callBackExst) callbackFn('error', '', notificationID);
										return;
									}
									if (jsondata && jsondata.baseAlignCertified == true && jsondata.allFloatsFound == true && jsondata.noEntityMissing == true && jsondata.noFontMissing == true && jsondata.noTableCellOverFlowFound == true) {
										if (callBackExst) callbackFn('success', '', notificationID);
									} else {
										var errormsg = '';
										if (!jsondata.baseAlignCertified == true) {
											errormsg += 'Base align certified Failed';
										}
										if (!jsondata.allFloatsFound == true) {
											errormsg += ', All Floats not Found';
										}
										if (!jsondata.noEntityMissing == true) {
											errormsg += ', Entity Missing';
										}
										if (!jsondata.noFontMissing == true) {
											errormsg += ', Font Missing';
										}
										if (!jsondata.noTableCellOverFlowFound == true) {
											errormsg += ', Table Cell OverFlow Found';
										}
										if (callBackExst) callbackFn('error', errormsg.replace(/^,/g, ''), notificationID);
									}

							}else if (/failed/i.test(status) || code != '200'){
								$('#'+notificationID+' .kriya-notice-body').html(process);
								$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
								if(callBackExst) callbackFn('error','PDF generation failed',notificationID);
							}else if (/no job found/i.test(status)){
								$('#'+notificationID+' .kriya-notice-body').html(status);
								if(callBackExst) callbackFn('error','No job found',notificationID);
							}else{
								$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;	
								if(loglen > 1){
									var process = data.status.message.log[loglen-1];
									if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								}
								setTimeout(function() {
									eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callbackFn);
								}, 1000 );
							}
						}
						else{
							$('#'+notificationID+' .kriya-notice-body').html('Failed');
							if(callBackExst) callbackFn('error','',notificationID);
							
						}
					},
					error: function(error){
						if(error.status == 502){
							setTimeout(function() {
								eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callbackFn);
							}, 1000 );
						}else{
							$('#'+notificationID).find('.kriya-notice-body').html('Get JobStatus Failed.');
						}
					}
				});
			},
			proofTimer: function(){
				proofInterval = setInterval(function(){
				   //Timer
				},120000);
				proofStartTime = new Date().getTime();
				},
				downloadPDF: function () {
					var iframePath = $('iframe[id="pdfViewer"]').attr('src');
					pdfPath = iframePath.replace(/.*file=/, '');
					pdfPath = iframePath.replace(/#zoom=page-width/, '');
					pdfPath = decodeURIComponent(pdfPath);
					window.frames["pdfIFrame"].PDFViewerApplication.pdfViewer.downloadManager.downloadUrl(pdfPath, kriya.config.content.doi + '.pdf');
			}
		},
		/*export:{
			exportToPDF: function(param, targetNode){
				var cmsID = "63492";
				var doi = $.urlParam('doi');
				var customer = $.urlParam('customer');
				var journal = $.urlParam('journal');
				$('.pdfStatus').remove();
				$('body').append('<span class="pdfStatus btn btn-primary">Generating PDF...</span>');
				$.ajax({
					xhr: function(){
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.addEventListener("progress", function(evt){
							var lines = evt.currentTarget.response.split("\n");
							if(lines.length){
								var progress = lines[lines.length-1];
								if (/^warning/i.test(progress)){
									progress = false;
								}
							}else{
								var progress = 0;
							}
							if (progress) $('.pdfStatus').html(progress);
							
						}, false);
						return xhr;
					},
					type: 'POST',
					url: "http://api.elife.exetercs.com/export.api/exportPDF?articleID="+cmsID+"&doi="+doi+"&apiKey=6c8e740baa82f222c5e63b39ffac2613&accountKey=1",
					//url: "http://api.elife.exetercs.com/export.api/exportPDF?articleID=99082&apiKey=6c8e740baa82f222c5e63b39ffac2613&accountKey=1",
					data: {},
					success: function(data){
						var lines = data.split("\n");
						if(lines.length){
							var progress = lines[lines.length-1];
						}
						if (/Uploaded/i.test(progress)){
							$('.pdfStatus').html('<a style="color:#fff;" href="'+progress.replace('Uploaded:','')+'" target="_blank"><i class="fa fa-download"></i> Download PDF</a>');
						}
					},
					error: function(error){
						alert('error; ' + eval(error));
					}
				});
			}
		},*/
		observer:{
			nodeChange: function(param, targetNode){
				var addedArray = param.node;
				if (addedArray.length > 100) return false;
				var type = param.type;
				//console.log(addedArray);
				for(var i = 0, l = addedArray.length; i < l; i++){
					var newNode = addedArray[i];
					
					
					
					//if (newNode.nodeType == 3 && newNode.nodeValue == "") continue;
					//console.log(newNode, type)
					if (newNode.nodeType != 3){
						//console.log(newNode.nodeType + ' ' + newNode.outerHTML)
						var parentNode = newNode.parentNode;
						if ($(newNode).hasClass('del') || $(newNode).hasClass('ins') || $(newNode).hasClass('sty') || $(newNode).hasClass('styrm') || $(newNode).attr('data-track')){
							eventHandler.menu.observer.addTrackChanges(newNode, type);
						}else if (parentNode && ($(parentNode).hasClass('del') || $(parentNode).hasClass('ins') || $(parentNode).hasClass('sty') || $(parentNode).hasClass('styrm') || $(parentNode).attr('data-track'))){
							eventHandler.menu.observer.addTrackChanges(parentNode, type);
						}
					}else{
						var parentNode = newNode.parentNode;
						if (parentNode){
							if ($(parentNode).hasClass('del') || $(parentNode).hasClass('ins') || $(parentNode).hasClass('sty') || $(parentNode).hasClass('styrm') || $(parentNode).attr('data-track')){
								eventHandler.menu.observer.addTrackChanges(parentNode, type);
							}
						}
					}//End of Text nodes
				}// END OF FOR LOOP
			},
			addTrackChanges: function(nodeToAdd, type){
				//In track changes, not adding front, jrnlEditorsGroup, jrnlReviewersGroup.
				//#42 - Front matter changes made by any user are not listed under changes panel. - Vimala.J(vimala.j@focusite.com)
                //#42 - Added conditional which article title and abstract section should be present- Vimala.J ( vimala.j@focusite.com)
				if($(nodeToAdd).closest('.front, .jrnlEditorsGroup, .jrnlReviewersGroup jrnlGroupAuthorsGroup, .jrnlDefList').length != 0 && ($(nodeToAdd).closest('.jrnlArtTitle, .jrnlAbsGroup ').length == 0)){
					return false;
				}
				//End of #42
				if (!$('#changesDivNode').length){
					$('#navigationContentDiv container-for-navigation').append('<div id="changesDivNode" class="sub-menu-divs"></div>');
				}
				
				if (nodeToAdd.nextSibling && nodeToAdd.nextSibling.nodeType==1 && (($(nodeToAdd.nextSibling).hasClass('del') && $('#changesDivNode [data-rid="'+nodeToAdd.nextSibling.getAttribute('data-cid')+'"][data-type="deleted"]').length<1) || ($(nodeToAdd.nextSibling).hasClass('ins') && $('#changesDivNode [data-rid="'+nodeToAdd.nextSibling.getAttribute('data-cid')+'"][data-type="inserted"]').length<1))){ // changes nextelementsibling to nextsibling, While taking nextelement sibling track changes is getting updated wrongly - priya #119  
					//Add track changes for the inserted and deleted element when select the text and start typing
					this.addTrackChanges(nodeToAdd.nextSibling, 'added');
				}
				
				var changeID = nodeToAdd.getAttribute('data-cid');
				var userName = nodeToAdd.getAttribute('data-username');
				var currentClassName = nodeToAdd.getAttribute('class');
				//#93 - priya
				var changeDataType = '';
				if($(nodeToAdd).hasClass('del')){
					changeDataType = 'deleted';
				}else if($(nodeToAdd).hasClass('ins')){
					changeDataType = 'inserted';
				}
				var objDate = new Date(new Date(parseInt(nodeToAdd.getAttribute('data-time'))));
				var locale = "en-us",
				month = objDate.toLocaleString(locale, { month: "short" });
				time = objDate.toLocaleString(locale, { hour: 'numeric',minute:'numeric', hour12: true });
				//var objDate = objDate.getDate() + ' ' + month + ' ' + (objDate.getYear() + 1900) + ' (' + objDate.getHours() + ':' + objDate.getMinutes() + ')'; 
				//#473 - ALL| Tables| Add text and background colour to Table cells -Prabakaran.A (prabakaran.a@focusite.com)
				var objDate = month + ' ' + objDate.getDate() + ' ' + (objDate.getYear() + 1900) + ' '+time; 
				//End of #473
				var trackChangeHTML = nodeToAdd.innerHTML.replace(/\&nbsp;/g, ' ');
				
				//get the tracked changed outer html to show the format in changes tab
				if($(nodeToAdd).hasClass('sty')){ 
					var clonedNode = nodeToAdd.cloneNode(true);
					$(clonedNode).removeAllAttr(['style']);
					trackChangeHTML = clonedNode.outerHTML;
					//When the table cell color or background color added
					if(clonedNode.nodeName && clonedNode.nodeName.match(/td|th/i)){
						trackChangeHTML = trackChangeHTML.replace(/(<|<\/)(td|th)/g, '$1div');
					}
				}else if($(nodeToAdd).hasClass('styrm')){
					var clonedNode = nodeToAdd.cloneNode(true);
					$(clonedNode).removeAllAttr(['style']);
					trackChangeHTML = clonedNode.outerHTML;//nodeToAdd.nextSibling.data;
					//When the table cell color or background color added
					if(clonedNode.nodeName && clonedNode.nodeName.match(/td|th/i)){
						trackChangeHTML = trackChangeHTML.replace(/(<|<\/)(td|th)/g, '$1div');
					}
				}else if(nodeToAdd.nodeName == "IMG"){
					var clonedNode = nodeToAdd.cloneNode(true);
					$(clonedNode).cleanTrackAttributes();
					$(clonedNode).removeAttr('id');
					trackChangeHTML = clonedNode.outerHTML;
				}
				
				//to add both deleted and inserted node as same change
				if (nodeToAdd.previousSibling && nodeToAdd.previousSibling.nodeType == 1 && /^(ins) /.test(nodeToAdd.previousSibling.getAttribute('class')) && nodeToAdd.previousSibling.hasAttribute('data-cid')){
					if (nodeToAdd.getAttribute('data-cid') == nodeToAdd.previousSibling.getAttribute('data-cid')){
						trackChangeHTML = '<span class="' + nodeToAdd.previousSibling.getAttribute('class') + '">' + nodeToAdd.previousSibling.innerHTML.replace(/\&nbsp;/g, ' ') + '</span><span class="' + nodeToAdd.getAttribute('class') + '">' + nodeToAdd.innerHTML.replace(/\&nbsp;/g, ' ') + '</span>';
					}
				}

				if(nodeToAdd.getAttribute('data-track-detail')){
					trackChangeHTML = nodeToAdd.getAttribute('data-track-detail');
				}
                
				//#45 (2) - Track Changes: Formating - When any content is applied with formats such as Bold, Italic, Superscript, Subscript, Underline, Change case, Text color, Background color, the same was not being tracked in track changes -  Prabakaran A(prabakaran.a@focusite.com)
                //Adding "Formatted: <format type>" in the track changes card
				//#45 While unformatting the word, the word is not captured, So its not highlighted when we click on the track changes.  -  Prabakaran A(prabakaran.a@focusite.com)
				/* var changeNode = $('#changesDivNode [data-rid="' + changeID + '"] .changeContent').filter(function(){ return $(this).text() == trackChangeHTML.replace(/(<([^>]+)>)/ig, "");});
                var trackValue = '';
                if(/del\b/.test(nodeToAdd.getAttribute('data-track')) || /del\b/.test(nodeToAdd.getAttribute('data-track')?action:nodeToAdd.className)){
                    trackValue = '<br><b>Deleted</b>' 
                }else{
                    trackValue = '<br>'+(nodeToAdd.getAttribute('data-track-hint')?'<b>Formatted : </b>'+nodeToAdd.getAttribute('data-track-hint').replace(" Added", ''):'<b>Inserted</b>');
                    if(!nodeToAdd.getAttribute('data-track-hint')){
                    	changeNode = $('#changesDivNode [data-rid="' + changeID + '"]');
                    }
				}*/

				//Clean up the track html
				var trackContentNode = $('<div>' + trackChangeHTML + '</div>');
				trackContentNode.find('.linkCount,.linkNowBtn,.removeNode').remove();
				trackChangeHTML = trackContentNode.html();

				//check with the data-type if its available #93 priya
				var changeNode = (changeDataType=='')?$('#changesDivNode [data-rid="' + changeID + '"]'):$('#changesDivNode [data-rid="' + changeID + '"][data-type="'+changeDataType+'"]');
               // var changeHTML = '<span class="changeData"><span class="changeContent">' + trackChangeHTML.replace(/(<([^>]+)>)/ig, "")+'</span>'+trackValue + '</span>';
			   var changeHTML = '<span class="changeData">' + trackChangeHTML.replace(/(<([^>]+)>)/ig, "") + '</span>';	
			   //End of #45 
                
				if (changeNode.length > 0){
					if (type == "removed" && $(kriya.config.containerElm + ' [data-cid="' + changeID + '"]').length == 0){
						changeNode.remove();
					}else{
						changeNode.find('.change-content .changeData').replaceWith(changeHTML);
					}
					//if alredy add reject button for citation change card that button will remove here.
					if($(nodeToAdd).find(kriya.config.citationSelector).length > 0){
						$(changeNode).find('.reject-changes').remove();
					}
				}else if (type != "removed"){
					var changeDiv    = $('<div class="change-div card" data-rid="' + changeID + '" data-message="{\'click\':{\'funcToCall\':\'highlightChange\',\'channel\':\'menu\',\'topic\':\'track_changes\'}}">');
					var changeHeader = $('<span class="change-head"><span class="row" /><span class="row" /></span>');
					changeHeader.find('.row:eq(0)').append('<span class="icon-user" style="font-size: 16px;color:#0d618b;"> </span><span class="change-person">' + userName + '</span>');
					//<span class="icon-safari" style="font-size: 16px;color:#0d618b;padding-left: 14px;"> </span>
					changeHeader.find('.row:eq(0)').append('<span class="change-time">' + objDate + '</span>');
					var action = nodeToAdd.getAttribute('data-track');
					action = (action)?action:nodeToAdd.className;
					$(changeDiv).attr('data-track-class', action.replace(/\s.*/, ''));
					$(changeDiv).attr('data-username', userName);
					$(changeDiv).attr('data-role', kriya.config.content.role);

					if (/ins\b/.test(action)){
						//changeHeader.find('.row:eq(0)').append('<span class="change-type">INSERTED</span>');
						
						$(changeDiv).attr('data-type', 'inserted');
					}else if (/del\b/.test(action)){
						//changeHeader.find('.row:eq(0)').append('<span class="change-type">DELETED</span>');
						$(changeDiv).attr('data-type', 'deleted');
					}else if(/sty/.test(action)){
						//changeHeader.find('.row:eq(0)').append('<span class="change-type">FORMATTED</span>');
						$(changeDiv).attr('data-type', 'formatted');
					}

					//changeHeader.find('.row:eq(1)').append('<span class="change-time">' + objDate + '</span>');
					/*if(nodeToAdd.getAttribute('data-track-hint')){
						changeHeader.find('.row:eq(0)').append('<span class="change-hint">' + nodeToAdd.getAttribute('data-track-hint') + '</span>');
					}*/
					
					//Modify by jagan to don't show accept track change option
					//it will skip reject button for citation string track change.
					if(!($(nodeToAdd).attr('data-citation-string') || $(nodeToAdd).find('[data-citation-string]').length > 0 )){
						changeHeader.find('.row:eq(0)').append('<span class="ui-btn reject-changes" data-channel="menu" data-topic="observer" data-event="click" data-message="{\'funcToCall\': \'acceptReject\', \'param\': {\'method\': \'reject\'}}"><span class="icon-cancel pull-right" style="font-size: 20px;padding-right: 4px;color:#f36262;cursor:pointer;"></span></span>');
					}
					/*if(kriya.config.content.role=="author"){
						changeHeader.find('.row:eq(0)').append('<span class="ui-btn reject-changes" data-channel="menu" data-topic="observer" data-event="click" data-message="{\'funcToCall\': \'acceptReject\', \'param\': {\'method\': \'reject\'}}"><span class="icon-cancel pull-right" style="font-size: 20px;padding-right: 4px;color:#f36262;cursor:pointer;"></span></span>');
					}else{
						changeHeader.find('.row:eq(0)').append('<span class="ui-btn reject-changes" data-channel="menu" data-topic="observer" data-event="click" data-message="{\'funcToCall\': \'acceptReject\', \'param\': {\'method\': \'reject\'}}"><span class="icon-cancel pull-right" style="font-size: 20px;padding-right: 4px;color:#f36262;cursor:pointer;"></span></span><span class="ui-btn accept-changes" data-channel="menu" data-topic="observer" data-event="click" data-message="{\'funcToCall\': \'acceptReject\', \'param\': {\'method\': \'accept\'}}"><span class="icon-check_circle pull-right" style="font-size: 20px;padding-right: 4px;color:#5d965d;cursor:pointer;"></span></span>');
					}*/

					if($('#changesDivNode .change-div.card').length > 0){
						$(changeDiv).insertBefore($('#changesDivNode .change-div.card')[0]);
					}else{
						$(changeDiv).insertAfter($('#changesDivNode .filterOptions'));
					}
					
					$(changeDiv).append(changeHeader);
					$(changeDiv).append('<span class="change-content">' + changeHTML + '</span>');
					//$(changeDiv).find('.changeData').addClass(action);
					$(changeDiv).find('.activeElement').removeClass('activeElement');
					
					kriya.general.iniFilterOptions($('#changesDivNode .filterOptions .dropdown-content'), $(changeDiv));
					//add the previous element as different tag #93 priya 
					if(nodeToAdd.previousSibling && nodeToAdd.previousSibling.nodeType==1 && $(nodeToAdd.previousSibling).hasClass('del') && $('#changesDivNode [data-rid="'+nodeToAdd.previousSibling.getAttribute('data-cid')+'"][data-type="deleted"]').length<1){
						this.addTrackChanges(nodeToAdd.previousSibling, 'added');
					}
				}
			},
			 //if all text deleted in a parent element then add class jrnlDeleted-kirankumar
			addJrnlDeleted:function(parentBlocks){
				if(parentBlocks.length > 0){
					if(kriya.config.preventJrnlDelete && $(parentBlocks).closest(kriya.config.preventJrnlDelete).length <= 0){ // to prevent article title delete in whole / aravind
						for(var i=0;i<parentBlocks.length;i++){
							var blockNode = parentBlocks[i];
							var parentele = $(blockNode).clone();
							$(parentele).find('.del,.iceBookmark,[data-track="del"],.jrnlDeleted').remove();
							//if($(parentele).text().replace(/\s/g,"")==""){
							//if any para have only spaces at that time it deleting total para.
							if($(parentele).text()==""){
								//when we are deleting total text inside a node at that time if any block node dont have class at that time code is breaking.
								//if($(blockNode).attr('class') != "jrnlDeleted"){
								if($(blockNode).attr('class') && $(blockNode).attr('class') != "jrnlDeleted"){
									$(blockNode).attr('old-class', $(blockNode).attr('class').replace(/ activeElement/g,''));
								}
								$(blockNode).attr('class','jrnlDeleted').attr('data-role',kriya.config.content.role);
								$(blockNode).parents('div, table, p, ul, ol, li').each(function(){
									if(!$(this).is(kriya.config.preventTyping) && !$(this).is(kriya.config.invalidSaveNodes) && $('#contentDivNode').find(this).length > 0 && !$(this).hasClass('jrnlTblContainer')){
										var parentele = $(this).clone();
										$(parentele).find('.del,.iceBookmark,[data-track="del"],.jrnlDeleted, .headerRow, .leftHeader').remove();
										//if($(parentele).text().replace(/\s/g,"")==""){
										//if any para have only spaces at that time it deleting total para.
										if($(parentele).text() == ""){
											if($(this).attr('class') != "jrnlDeleted"){
												//if prasent element dont have activeElement it is breaking code -kirankumar
												var oldclass = $(this).attr('class');
												if(oldclass && oldclass.match(/ activeElement/g) && oldclass.match(/ activeElement/g).length > 0){
													//oldclass is string
													oldclass = oldclass.replace(/ activeElement/g,'');
												}
												$(this).attr('old-class',oldclass);
											}
											$(this).attr('class','jrnlDeleted').attr('data-role',kriya.config.content.role);
											kriyaEditor.settings.undoStack.push($(this));
										}else{
											return false;
										}
									}else{
										return false;
									}
								});
								kriyaEditor.settings.undoStack.push(blockNode);
							}
						}
					}
				}
			},
			shortPage: function(param, targetNode){
				if($('[data-component="shortPage_edit"]').length > 0){
					$('[data-component="shortPage_edit"]').removeAttr('style').removeClass('hidden').attr('style','top: 0px; left: 0px; width: 100%; height: 100%; opacity: 1;')
					$('[data-component="shortPage_edit"] *[data-input-editable="true"]').removeAttr('style').removeClass('hidden').attr('style','width: 70%;')
					var parameters = {
						'customer': kriya.config.content.customer,
						'project' : kriya.config.content.project, 
						'doi'     : kriya.config.content.doi,
						'xpath'   : '//custom-meta-group/custom-meta[@specific-use="shortPage"]'
					};
					keeplive.sendAPIRequest('getdatausingxpath', parameters, function(res){
						if(res){
							res = '<root>'+res+'</root>';
							if($(res).find('custom-meta').length>0){
								var component = $("*[data-type='popUp'][data-component='shortPage_edit']");
								$(component).find('*[data-wrapper="true"] > div:not(".com-label, .tmp-com-short, .com-button")').remove()
								$(res).find('custom-meta').each(function(){
									var nodeid = $(this).attr('id');
									var addcom = $(component).find('.tmp-com-short').clone()
									var page  = $(this).find('meta-name').text()
									var line  = $(this).find('meta-value').text()
									$(addcom).find('*[data-class="sortPageNum"]').val(page)
									$(addcom).find('*[data-class="sortLineNum"]').val(line)
									$(addcom).addClass('com-short').removeClass('tmp-com-short hidden').attr('com-id', nodeid)
									$(component).find('*[data-wrapper="true"]').append(addcom);
								})
								if($(component).find('.com-add').length > 0){
									$(component).find('.com-add:not(:last)').remove()	
								}
							}	
						}
					},function(res){
						var component = $("*[data-type='popUp'][data-component='shortPage_edit']");
						$(component).find('*[data-wrapper="true"] > div:not(".com-label, .tmp-com-short, .com-button")').remove()
						var addcom = $(component).find('.tmp-com-short').clone()
						$(addcom).addClass('com-short').removeClass('tmp-com-short hidden')
						$(component).find('*[data-wrapper="true"]').append(addcom);
					});
				}
			},
			addNewShortPage: function(param, targetNode){
				if($('[data-component="shortPage_edit"]').length > 0){
					var component = $("*[data-type='popUp'][data-component='shortPage_edit']");
						var addcom = $(component).find('.tmp-com-short').clone()
						$(addcom).addClass('com-short').removeClass('tmp-com-short hidden')
						$(component).find('*[data-wrapper="true"]').append(addcom);
				}
			},
			saveShortPaga: function(param, targetNode){
				if($('[data-component="shortPage_edit"]').length > 0){
					var component = $("*[data-type='popUp'][data-component='shortPage_edit']");
					if($(component).find('.com-short').length > 0){
						$(component).find('.com-short *[data-error="Input Required"]').removeAttr('data-error')
						$(component).find('.com-short:not([data-del]) *[data-class="sortPageNum"], .com-short:not([data-del]) *[data-class="sortLineNum"]').each(function(){
							if($(this).val()==""){
								$(this).attr('data-error','Input Required');
							}
						})
						var leg = 0;
						$(component).find('.com-short:not([data-del]) *[data-class="sortPageNum"]').each(function(){
							 var dup = $(this).val();
							$(component).find('.com-short:not([data-del]) *[data-class="sortPageNum"]:not(:eq('+leg+'))').each(function(){
								if($(this).val()!="" && $(this).val() == dup){
									$(this).attr('data-error','Input Required');
								}
							});
							leg++;
						})
						if($(component).find('.com-short *[data-error="Input Required"]').length > 0){
							return false;
						}
						$(component).find('.com-short').each(function(){
							var page = $(this).find('[data-class="sortPageNum"]').val()
							var line = $(this).find('[data-class="sortLineNum"]').val()
							var newnode = "";
 							if($(this).attr('com-id')){
								var nodeid = $(this).attr('com-id')
								if($(this).attr('data-del')){
									newnode = '<custom-meta id="'+nodeid+'" data-removed="true" specific-use="shortPage"><meta-name>'+page+'</meta-name><meta-value>'+line+'</meta-value></custom-meta>';
								}else{
									newnode = '<custom-meta id="'+nodeid+'" data-inserted="true" specific-use="shortPage"><meta-name>'+page+'</meta-name><meta-value>'+line+'</meta-value></custom-meta>';
								}
							 }else{
								if(!$(this).attr('data-del')){
									newnode = '<custom-meta section="front" id="'+uuid()+'" section="article-meta" node-xpath="custom-meta-group" specific-use="shortPage"><meta-name>'+page+'</meta-name><meta-value>'+line+'</meta-value></custom-meta>';
								}
							 }	
							 if(newnode!=""){
								kriyaEditor.init.save(newnode);
								kriya.popUp.closePopUps($('[data-component="shortPage_edit"]'));
							 }
							 
						})

					}
				}
			},
			acceptReject: function(param, targetNode){
				if (typeof(param) == undefined) return;
				if (typeof(param.method) == undefined) return;
				var changeID;
				if ($(targetNode).parent()[0].hasAttribute('data-component')) {
					$(targetNode).parent().addClass('hidden');
					changeID = $(targetNode).parent().find('[data-class][data-clone]').text();
				}else{
					changeID    = $(targetNode).closest('div.change-div').attr('data-rid');
				}
				//by kirankumar:-when accept or reject change after tracked changes do not change in referencelist
				//start
				var refid;
				if($('#contentDivNode [data-cid="' + changeID + '"]').closest('p.jrnlRefText')){
					 refid=$('#contentDivNode [data-cid="' + changeID + '"]').closest('p.jrnlRefText').attr('data-id');
				}
				//end
				var trackedNode = $('#contentDivNode [data-cid="' + changeID + '"]');
				var changesCard = $('#changesDivNode [data-rid="' + changeID + '"]');
				var trackContent  = []; //Track node inner elements

				//temporary condition to avoid break
				if (trackedNode.length == 0) {
					$(targetNode).closest('div.change-div').remove();
					return false;
				}

				//Validate the keyword maximum and minimum
				var className = ($(trackedNode).attr('class'))?$(trackedNode).attr('class').split(' ')[0]:'';
				if(className == "jrnlKeyword"){
					var action = '';
					if (param.method == "reject" && ($(trackedNode).hasClass('del') || $(trackedNode).attr('data-track') == "del")){
						action = 'insert';
					}else if (param.method == "reject" && ($(trackedNode).hasClass('ins') || $(trackedNode).attr('data-track') == "ins")){
						action = 'delete';
					}
					if(!kriya.general.validateInlineElement("jrnlKeyword",'jrnlKeyword', action)){
						return false;
					}
				}
				
				var callback =  trackedNode.attr('data-callback');

				if (param.method == "accept"){
					trackedNode.each(function(){
						var dataTypeAtrr;
						var parentEle = $(this).closest('[id]')[0];
						if($(this).hasClass('del')){
							$(this).attr('data-removed', 'true');
							$(this).remove();
							if($(this)[0].hasAttribute('data-type')){
								dataTypeAtrr = $(this)[0].getAttributeNode("data-type").value;
								if(dataTypeAtrr!=null && dataTypeAtrr!="" && dataTypeAtrr!=undefined && dataTypeAtrr=="spelling"){
									eventHandler.menu.findReplace.reSearch($("#clonedContent"),parentEle.getAttributeNode("id").value);
								}
							}
							//to ensure we are sending only element with id for saving - Jai 13-06-2017
							if ($(this)[0].hasAttribute('id')){
								trackContent.push(this);
							}else{
								trackContent.push(parentEle);
							}
						}else if($(this).hasClass('ins')){
							var unwrapEle = $(this).contents().unwrap();
						}else if($(this).hasClass('sty')){
							if($(this).hasClass('changeCase')){
								if($(this).find('[hidden-track="track"]').length>0)
									$(this).find('[hidden-track="track"]').remove();
							}
							//#473 - ALL| Tables| Add text and background colour to Table cells -Prabakaran.A (prabakaran.a@focusite.com)
							if ($(this)[0].hasAttribute('id') && ($(this)[0].hasAttribute('data-table-background-color') || $(this).is('[data-track-hint="Background Colored"]'))){
								var unwrapEle = $(this).cleanTrackAttributes();
								unwrapEle = unwrapEle.parent().closest('[id]');
							}else{
								var unwrapEle = $(this).cleanTrackAttributes();
							}
							//End Of #473
						}else if($(this).hasClass('styrm')){
							
							var unwrapEle = $(this).contents().unwrap();
						}

						if($(this).attr('data-track')){
							var trackAction = $(this).attr('data-track');
							if (/ins\b/.test(trackAction)){
								//Remove the track attributes
								$(this).cleanTrackAttributes();
							}else if (/del\b/.test(trackAction)){
								$(this).attr('data-removed', 'true');
								$(this).remove();
								if($(this)[0].hasAttribute('data-type')){
									dataTypeAtrr = $(this)[0].getAttributeNode("data-type").value;
									if(dataTypeAtrr!=null && dataTypeAtrr!="" && dataTypeAtrr!=undefined && dataTypeAtrr=="spelling"){
										eventHandler.menu.findReplace.reSearch($("#clonedContent"),parentEle.getAttributeNode("id").value);
									}
								}
								
							}
							//to ensure we are sending only element with id for saving - Jai 13-06-2017
							if ($(this)[0].hasAttribute('id')){
								trackContent.push(this);
							}else{
								trackContent.push(parentEle);
							}
						}
						trackContent = (unwrapEle)?$.merge( trackContent, unwrapEle):trackContent;
					});
				}
				if (param.method == "reject"){
					//if any track change reject in jarnDeleted then class will replace with old-class
					var jrnlDelNode=trackedNode.closest('.jrnlDeleted');
					if(jrnlDelNode != null){
						for(var i = 0;i<jrnlDelNode.length;i++)
						if(jrnlDelNode[i].hasAttribute('old-class')){
							$(jrnlDelNode[i]).attr('class',$(jrnlDelNode[i]).attr('old-class'));
							$(jrnlDelNode[i]).removeAttr('old-class');
						}
					}
					trackedNode.each(function(){
						var dataTypeAtrr;
						var parentEle = $(this).closest('[id]')[0];
						if($(this).hasClass('del')){
							if (this.hasAttribute('data-old-class')){
								var dataClass = $(this).attr('data-old-class');
								while(this.attributes.length > 0){
									this.removeAttribute(this.attributes[0].name);
								}
								$(this).attr('class', dataClass);
								//to ensure we are sending only element with id for saving - Jai 13-06-2017
								if ($(this)[0].hasAttribute('id')){
									trackContent.push(this);
								}else{
									trackContent.push(parentEle);
								}
							}else{
								var unwrapEle = $(this).contents().unwrap();
								trackContent = $.merge( trackContent, unwrapEle);
							}
						}else if($(this).hasClass('ins')){
							$(this).attr('data-removed', 'true');
							$(this).remove();
							if($(this)[0].hasAttribute('data-type')){
								dataTypeAtrr = $(this)[0].getAttributeNode("data-type").value;
								if(dataTypeAtrr!=null && dataTypeAtrr!="" && dataTypeAtrr!=undefined && dataTypeAtrr=="spelling"){
									eventHandler.menu.findReplace.reSearch($("#clonedContent"),parentEle.getAttributeNode("id").value);
								}
							}
							//to ensure we are sending only element with id for saving - Jai 13-06-2017
							if ($(this)[0].hasAttribute('id')){
								trackContent.push(this);
							}else{
								trackContent.push(parentEle);
							}
						}else if($(this).hasClass('sty')){

							//Check if attribute is data-table-background-color then remove the Backgroundcolor for table. Other than the table formatting use in else to remove the element.  
							//#45 (3) - Bug fix - On rejection of sentence case, restore the previous value - Prabakaran. A(prabakaran.a@focusite.com)
							if($(this).hasClass('changeCase')){
								if($(this).find('[hidden-track="track"]').length>0)
									$(this)[0].innerHTML = $(this).find('[hidden-track="track"]')[0].innerHTML
								var unwrapEle = $(this).contents().unwrap();
								$(unwrapEle).cleanTrackAttributes();
							}else{
								if($(this)[0].hasAttribute('data-text-color')){
									var unwrapEle = $(this).removeClass('sty');
									var prevColor = unwrapEle.attr('data-prev-text-color' );
								   if(prevColor){
									   unwrapEle.attr('data-text-color', prevColor);
									   unwrapEle.css('color', prevColor);
									} else {
									   unwrapEle.removeAttr('data-text-color');
									   unwrapEle.css('color', '');
									}
								   unwrapEle.removeAttr('data-prev-text-color');
								   $(unwrapEle).cleanTrackAttributes();
						 		}else if($(this)[0].hasAttribute('data-table-background-color')){
									var unwrapEle = $(this).removeClass('sty');
									var prevColor = unwrapEle.attr('data-prev-table-background-color' );
								   if(prevColor){
									   unwrapEle.attr('data-table-background-color', prevColor);
									   unwrapEle.css('background-color', prevColor);
									} else {
									   unwrapEle.removeAttr('data-table-background-color');
									   unwrapEle.css('background-color', '');
									}
								   //#473 - ALL| Tables| Add text and background colour to Table cells -Prabakaran.A (prabakaran.a@focusite.com)
								   unwrapEle.removeAttr('data-prev-table-background-color' );  
								   $(unwrapEle).cleanTrackAttributes();
									//if ($(unwrapEle)[0].hasAttribute('id')){
									//              unwrapEle = unwrapEle.parent().closest('[id]');
									//}
					 			}else if(!$(this).hasClass('changeCase')){
									var unwrapEle = $(this).contents().unwrap();
									$(unwrapEle).cleanTrackAttributes();
								}
							}
																															 
							trackContent = $.merge( trackContent, unwrapEle);
							//End of #473
						}else if($(this).hasClass('styrm')){							
							//Reset the format node
							var formatCommand = $(this).attr('data-format-command');
							if(formatCommand){
								var range = rangy.createRange();
								range.selectNodeContents(this);
								var sel = rangy.getSelection();
								sel.setSingleRange(range);
								editor.composer.commands.exec(formatCommand);
							}
							var unwrapEle = $(this).contents().unwrap();
							trackContent = $.merge( trackContent, unwrapEle);
						}

						if($(this).attr('data-track')){
							var trackAction = $(this).attr('data-track');
							if (/ins\b/.test(trackAction)){
								$(this).attr('data-removed', 'true');
								$(this).remove();
								if($(this)[0].hasAttribute('data-type')){
									dataTypeAtrr = $(this)[0].getAttributeNode("data-type").value;
									if(dataTypeAtrr!=null && dataTypeAtrr!="" && dataTypeAtrr!=undefined && dataTypeAtrr=="spelling"){
										eventHandler.menu.findReplace.reSearch($("#clonedContent"),parentEle.getAttributeNode("id").value);
									}
								}
							}else if (/del\b/.test(trackAction)){
								//remove the track attributes
								$(this).cleanTrackAttributes();
							}
							//to ensure we are sending only element with id for saving - Jai 13-06-2017
							if ($(this)[0].hasAttribute('id')){
								trackContent.push(this);
							}else{
								trackContent.push(parentEle);
							}
						}
					});

					if(callback){
						var execFn = getStringObj(callback);
						execFn($(trackContent), param.method);
					}

					if(kriya.config.citationSelector && ($(trackedNode).closest(kriya.config.citationSelector).length > 0 || $(trackedNode).find(kriya.config.citationSelector).length > 0)){
						
						var citeNodes = ($(trackedNode).closest(kriya.config.citationSelector).length > 0)?$(trackedNode).closest(kriya.config.citationSelector): $(trackedNode).find(kriya.config.citationSelector);
						$( citeNodes).each(function(){
							var citeString = $(this).attr('data-citation-string');
							if(citeString){
								citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
								for(var c=0;c<citeString.length;c++){
									var citeID = citeString[c];
									var anchorNode = citeJS.floats.addAnchorTag(citeID, kriya.config.containerElm);
									if(anchorNode){
										kriyaEditor.settings.undoStack.push(anchorNode);
									}

									//Move the float object when insert citation
									citeJS.general.moveFloat(citeID, kriya.config.containerElm);

									//Update data-uncited attribute
									var currentCitationLength = $(kriya.config.containerElm + ' [data-citation-string*=" ' + citeID + ' "]').filter(function(){
										if($(this).closest('.del, [data-track="del"], .jrnlDeleted').length == 0){
											return this;
										}
										return false;
									}).length;
									
									var blockNode = $(kriya.config.containerElm + ' [data-id="' + citeID + '"]');
									blockNode = (blockNode.closest('[data-id^="BLK_"]').length > 0)?blockNode.closest('[data-id^="BLK_"]'):blockNode;
									if(currentCitationLength == 0 && !blockNode.attr('data-uncited')){
										blockNode.attr('data-uncited', 'true');
										$('#navContainer [data-panel-id="' + blockNode.attr('data-id') + '"]').attr('data-uncited', 'true');
										kriyaEditor.settings.undoStack.push(blockNode);
									}else if(currentCitationLength > 0 && blockNode.attr('data-uncited')){
										blockNode.removeAttr('data-uncited');
										$('#navContainer [data-panel-id="' + blockNode.attr('data-id') + '"]').removeAttr('data-uncited');
										kriyaEditor.settings.undoStack.push(blockNode);
									}
								}
							}
						});
						eventHandler.query.action.regenerateActionQuery();
						citeJS.general.updateStatus();
					}

					if($(trackedNode).closest('.jrnlTblFNRef').length > 0 || $(trackedNode).find('.jrnlTblFNRef').length > 0){
						var linkNode = ($(trackedNode).closest('.jrnlTblFNRef').length > 0)?$(trackedNode).closest('.jrnlTblFNRef'):$(trackedNode).find('.jrnlTblFNRef');
						kriya.general.updateTblFNLinkCount(linkNode);
					}
				}
				//by kirankumar issue:-when accept or reject change after tracked changes do not change in referencelist
				//start
				if(refid){
					kriya.general.updateRightNavPanel(refid, kriya.config.containerElm);
					//when track changes made in ref content saving is failed-kirankumar
					kriyaEditor.settings.undoStack = $.merge( $(".jrnlRefGroup>[data-id='"+refid+"']"), kriyaEditor.settings.undoStack);
				}else{
					kriyaEditor.settings.undoStack = $.merge( trackContent, kriyaEditor.settings.undoStack);
				}
				//end
				//kriyaEditor.settings.undoStack = $.merge( trackContent, kriyaEditor.settings.undoStack); //commented by kiran for saving failed in ref
				// Save the content if the accter or removed
				kriyaEditor.init.addUndoLevel('accept-reject change');
				//User names do not disappear from the tracked changes list once all changes by that user accepted-kirankumar
				//start
				var changesCard=$('#changesDivNode').find('div[data-rid="'+changeID+'"]');
				var uName=changesCard.attr('data-username');
				//changesCard.remove();
				changesCard.remove();
				//$(targetNode).closest('div.change-div').remove();
				if($('.change-div.card[data-username="'+uName+'"]').length==0){
					$("#track-name-filter>[data-value='"+uName+"']").remove();
					$('[data-dropdown="track-name-filter"]').html("None");
				}
				//end
				//if we reject any change at that time element replace with old element at that time kriya.selection not updating
				kriya.selection = rangy.getSelection().getRangeAt(0);
				kriya.selectedNodes = kriya.selection.getNodes();
			},
			getblockelements : function(){
				//var blockElementList = $('#contentContainer [data-id^="BLK_"][data-current-page]');
				var blockElementList = $(' #contentContainer [data-id^="BLK_"]');
				if(blockElementList.length>0){
					//data-current-page, data-override-page, data-position, data-column-start, data-column-span, data-width-factor
					var columnLen = 1;
					var articleTyp = config["articleTypeDetails"][($(".jrnlArtType").text()!="")?$(".jrnlArtType").text().toUpperCase():"undefined"];
					if(config["pageColumnDetails"][articleTyp]!=undefined){
						columnLen = config["pageColumnDetails"][articleTyp]["columnDetails"].length;
					}else{
						columnLen =  config["pageColumnDetails"]["LAYOUT1"]["columnDetails"].length;
					}

					$(blockElementList).each(function(){
						var dimenWidth = $(this).attr('data-width');
						var dimenHeight = $(this).attr('data-height');
						var currPag = $(this).attr('data-current-page');
						var overridPag = $(this).attr('data-override-page');
						var position = $(this).attr('data-position');
						var colstart = $(this).attr('data-column-start');
						var colspan = $(this).attr('data-column-span');
						var widthFac = $(this).attr('data-width-factor');	
						var cloneNodeData = $(this).clone(true);						
						if($(cloneNodeData).find(".label .del").length>0){
							$(cloneNodeData).find(".label .del").remove();
							var floatEle = cloneNodeData.find(".label").text();
						}else{
							var floatEle = $(this).find(".label").text();
						}
							
						if(floatEle==null || floatEle==undefined || floatEle=="" ){
							floatEle = $(this).attr("id");
						}
						
						var getDime = "";	
						var rowToAdd = "";
						
						rowToAdd += '<div class="row blockadded" data-class="'+$(this).attr('id')+'" style="text-align:center" ><div class="col m1">'+floatEle+'</div>';
						
						if(dimenWidth!="" && dimenWidth!=null && dimenWidth!=undefined && dimenHeight!="" && dimenHeight!=null && dimenHeight!=undefined){
							rowToAdd += '<div class="col m1">'+dimenWidth+' X '+dimenHeight+'</div>';
						}else{
							rowToAdd += '<div class="col m1"></div>';
						}
						
						if(currPag!="" && currPag!=null && currPag!=undefined){
							rowToAdd += '<div class="col m1">'+currPag+'</div>';
						}else{
							rowToAdd += '<div class="col m1"></div>';
						}
						if(overridPag!="" && overridPag!=null && overridPag!=undefined){
							rowToAdd += '<div class="col-md-1"><input type="text" style="max-width:45%;text-align:center;height:1.5rem;" value="'+overridPag+'" onkeydown="return isNumber(event)"></div>';
						}else{
							rowToAdd += '<div class="col m1"><input type="text" style="max-width:45%;text-align:center;height:1.5rem;" onkeydown="return eventHandler.menu.observer.isNumber(event)"></div>';
						}
						
						var positionTagVal = ['Select','Top','Bottom'];
						var positionTag = '<select class="selectblockoptions" id="positionval">';
						for(var i=0;i<positionTagVal.length;i++){
							var possel = "";
							if(position!=undefined && position!=null && position!="" && position.toLowerCase()==positionTagVal[i].toLowerCase()){
								possel = "selected";
							}
							if(positionTagVal[i]=="Select"){
								positionTag += '<option value="">'+positionTagVal[i]+'</option>';
							}else{
								positionTag += '<option value="'+positionTagVal[i].toLowerCase()+'" '+possel+'>'+positionTagVal[i]+'</option>'
							}				
						}
						positionTag += "</select>";
						rowToAdd += '<div class="col m2" style="padding-right: 45px !important;">'+positionTag+'</div>';
						
						var colstartlog  = '<select class="selectblockoptions"><option value="">Select</option>';					
						for(var i=1;i<=columnLen;i++){
							var colstrtsel = "";
							if((parseInt(colstart)+1)==i){
								colstrtsel = "selected";
							}
							colstartlog += '<option value="'+(i-1)+'" '+colstrtsel+'>'+i+'</option>'
						}
						colstartlog += '</select>';
						rowToAdd += '<div class="col m2" style="padding-right: 45px !important;">'+colstartlog+"</div>";
						
						var colspanlog  = '<select class="selectblockoptions"><option value="">Select</option>';					
						for(var i=1;i<=columnLen;i++){
							var colspansel = "";
							if((parseInt(colspan)+1)==i){
								colspansel = "selected";
							}
							colspanlog += '<option value="'+(i-1)+'" '+colspansel+'>'+i+((i<=1)?' - column':' - columns')+'</option>'
						}
						colspanlog += "</select>"
						rowToAdd += '<div class="col m2" style="padding-right: 45px !important;">'+colspanlog+'</div>';
											
						var widthfact  = '<select class="selectblockoptions"><option value="">Select</option>';					
						for(var i=70;i<=150;i=i+10){
							var widthsel = "";
							if(widthFac==i){
								widthsel = "selected";
							}
							widthfact += '<option value="'+i+'" '+widthsel+'>'+i+'</option>'
						}
						widthfact += "</select>";
						rowToAdd += '<div class="col m2" style="padding-right: 45px !important;">'+widthfact+'</div>';

						
						rowToAdd += '</div>';
						
						$('[data-component="blockelement_edit"] #appenddata').append(rowToAdd);	
					});
					kriya.popUp.openPopUps($('[data-component="blockelement_edit"]'));
				}else{
					$("#blockelemenu span").addClass("disabled");
				}				
			},
			rulesetChange: function(param, targetNode){
			//to reject the changes made by rulesets - jai
				if (typeof(param) == undefined) return;
				if (typeof(param.method) == undefined) return;
				var changeID;
				if ($(targetNode).parent()[0].hasAttribute('data-component')) {
					$(targetNode).parent().addClass('hidden');
					changeID = $(targetNode).parent().find('[data-class][data-clone]').text();
				}
				var trackedNode = $('#contentDivNode [data-cid="' + changeID + '"]');
				var oldHTML = $(trackedNode).attr('data-track-change');
				$(trackedNode).html(oldHTML).removeAttr('data-track-change');
				kriyaEditor.settings.undoStack = $.merge( trackedNode, kriyaEditor.settings.undoStack);
				// Save the content if the accter or removed
				kriyaEditor.init.addUndoLevel('accept-reject change');
			},
			clearblockelepopup : function(param, targetNode){
				$('[data-component="blockelement_edit"]').find(".blockadded").remove();
				kriya.popUp.closePopUps($('[data-component="blockelement_edit"]'));
			},
			saveasblockeleattr : function(param, targetNode){
				//data-current-page, data-override-page, data-position, data-column-start, data-column-span, data-width-factor
				var blockeledata = $('[data-component="blockelement_edit"]').find(".blockadded");
				if(blockeledata.length>0){
					$(blockeledata).each(function(){
						var saveData = 0;
						var blockId = $(this).attr("data-class");					
						var overridpag = $($(this).children()[3]).find("input").val();
						if(overridpag!="" && overridpag!=null && overridpag!=undefined){
							$("#"+blockId).attr("data-override-page",overridpag);
							saveData =1;
						}
						var position = $($(this).children()[4]).find("select").val();
						if(position!="" && position!=null && position!=undefined){
							$("#"+blockId).attr("data-position",position);
							saveData =1;
						}
						var colstart = $($(this).children()[5]).find("select").val();
						if(colstart!="" && colstart!=null && colstart!=undefined){
							$("#"+blockId).attr("data-column-start",colstart);
							saveData =1;
						}
						var colspan = $($(this).children()[6]).find("select").val();
						if(colspan!="" && colspan!=null && colspan!=undefined){
							$("#"+blockId).attr("data-column-span",colspan);
							saveData =1;
						}
						var widthfact = $($(this).children()[7]).find("select").val();
						if(widthfact!="" && widthfact!=null && widthfact!=undefined){
							$("#"+blockId).attr("data-width-factor",widthfact);
							saveData =1;
						}
						if(saveData==1){
							kriyaEditor.settings.undoStack.push($("#"+blockId));					
						}			
					})
					if(kriyaEditor.settings.undoStack.length>0){
						kriyaEditor.init.addUndoLevel('add-attributes');
					}			
				}
				$('[data-component="blockelement_edit"]').find(".blockadded").remove();
				kriya.popUp.closePopUps($('[data-component="blockelement_edit"]'));
			},
			isNumber : function(e) {
				if (e.shiftKey) e.preventDefault();
				else {
					var nKeyCode = e.keyCode;
					//Ignore Backspace and Tab keys
					if (nKeyCode == 8 || nKeyCode == 9) return;
					if (nKeyCode < 95) {
						if (nKeyCode < 48 || nKeyCode > 57) e.preventDefault();
					} else {
						if (nKeyCode < 96 || nKeyCode > 105) e.preventDefault();
					}
				}
			}
		},save:{//kriyaEditor.lastSelection some times comming as undefined for that this function is helpfull for checking is it undefined or not at the time of save button clicking.
			saveContent: function(){//when we click on save button at that time this function will trigger
				if(kriyaEditor.lastSelection && kriyaEditor.lastSelection.anchorNode){
					kriyaEditor.settings.undoStack.push(kriyaEditor.lastSelection.anchorNode);
				}
				kriyaEditor.init.addUndoLevel('save-btn-click');
			}
		}
	};
	eventHandler.components = {
		pdfviewer: {
			scrollPDF: function (x, y, currPageNum) {
				currPageNum = parseInt(currPageNum) - 1;
				if (x == 'NaN') x = 0;
				if (y == 'NaN') y = 0;
				if (x == undefined || y == undefined || currPageNum == undefined || coordinate == undefined) {
					return false;
				}
				console.log('Page: ', currPageNum, '; X: ', x, '; Y: ', y);
				var currPageObj = coordinate[currPageNum];
				if (currPageObj) {
					Object.keys(currPageObj).forEach(function (k) {
						var currParaObj = currPageObj[k];
						if (x >= currParaObj[1] && x <= currParaObj[3]) {
							console.log('Matched X value');
							if (y >= currParaObj[0] && y <= currParaObj[2]) {
								console.log('Matched Y value: ', k);
								k = k.replace(/(\_[0-9]+)$/, '');
								if ($('#' + k).length > 0) {
									$('#' + k)[0].scrollIntoView();
								} else if ($('[data-id=' + k + ']').length > 0) {
									$('[data-id=' + k + ']')[0].scrollIntoView();
								}
								console.log(k, currParaObj);
							}
						}
					});
				}
			},
			reloadIframe: function () {
				//based on https://stackoverflow.com/questions/4249809/reload-an-iframe-with-jquery
				
				var environment = location.hostname.replace(/(kriya2|demo)\.kriyadocs\.com/,'') + '/';
				if(environment&& environment!='/'){
					environment = '/' + environment;
				}
				var head = document.getElementsByTagName('head')[0];
					var script = document.createElement('script');
					var jsSrc = '/resources/' +kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources' + environment + 'proofing/' + kriya.config.content.doi + '.js?method=server';
					script.src = jsSrc;
					head.appendChild(script);
					
				$('#pdfViewer')[0].contentWindow.location.reload(true);
				$('#pdfViewers')[0].contentWindow.location.reload(true);
				
				/*$("iframe[id='pdfViewer'], iframe[id='pdfViewers']").each(function () {
					var src = $(this).attr('src');
					var currDate = new Date().getTime();
					$(this).attr('src', '');
					$(this).attr('src', src);
					console.log(src);
					var head = document.getElementsByTagName('head')[0];
					var script = document.createElement('script');
					var jsSrc = '/resources/' +kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/proofing/' + kriya.config.content.doi + '.js?method=server';
                    //method server is not working in local system
					script.src = jsSrc;
					head.appendChild(script);
				})*/
			}
		},
		PopUp : {
			addPopUp  : function(param, targetNode){
				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				$('.activeElement').removeClass('activeElement');
				kriya.config.activeElements = false;
				
				var componentType = param.component + '_edit';
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				$(component).find('*[data-if-selector].hidden').removeClass('hidden');
				$(component).find('*[remove-class]').each(function(){
					var className = $(this).attr('remove-class');
					$(this).removeClass(className);
				})
				$(component).find('input').prop('checked',false);
				var ele = $(targetNode).parent().find('.'+param.component);
				if(ele.length == 0){
					ele = $(kriya.config.containerElm+' .'+param.component);
				}
				if(ele.length == 0){
					ele = $(kriya.config.containerElm);
				}
				
				//when related add show the first if selector
				if(param.component == "jrnlRelArt"){
					$(component).find('*[data-if-selector]').addClass('hidden');
					var relArtType = $(component).find('select[data-class="RelType"]').val();
					if(relArtType){
						$(component).find('*[data-if-selector][data-rel-art-type="' + relArtType + '"]').removeClass('hidden');
					}else{
						$(component).find('*[data-if-selector]:first').removeClass('hidden');
					}
				}
				if($(component).attr('data-component')=="jrnlAuthorGroup_edit"){  
					$(component).find('span[data-type="AuthorBioImage"]').children('img').remove();			
				}	
				kriya.popUp.showEmptyPopUp(component, ele.first());
				$(component).find('[data-wrapper]').scrollTop(0)
				
				if (targetNode.hasClass('btn')){
					var nodeXpath = kriya.getElementXPath(targetNode.parent());
					component.attr('data-node-xpath', nodeXpath);
				}
				$(component).find('[data-error]').removeAttr('data-error');
				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="new"]').removeClass('hidden');
				
				//if related article popup is opened then show the search button and hide reset button
				if(componentType == "jrnlRelArt_edit"){
					$(component).find('.searchRel').removeClass('hidden');
					$(component).find('.resetRel').addClass('hidden');
				}
				
				/*Start - Issue ID - #113 - Dhatshayani . D - Nov 25, 2017 - To add attribute "data-tmp-selector" in the compoenet to select the appropriate template of footnote */
				if(param.callback && param.callback!=''){
					var execFn = getStringObj(param.callback);
					execFn(param, targetNode);
					//eventHandler.components.PopUp.addDataTempSelector(param, targetNode);
				}/*End Issue ID - #113 */
			},
			modifyAttributes : function(param, targetNode){ // to edit all attributes in selected element
				var currSelNode = $(kriya.selection.startContainer.parentNode);
				kriya['styles'].init($(currSelNode), 'jrnlModifyAttributes');
				if($('[data-component="jrnlModifyAttributes"]').find('[data-container="true"] div').length > 0){
					$('[data-component="jrnlModifyAttributes"]').find('[data-container="true"] div').remove();
				}
				var fieldTemp = $('[data-component="jrnlModifyAttributes"]').find('.template');
				$(currSelNode).each(function() {
					$.each(this.attributes, function() {
					  if(this.specified && this.name != "class" && this.name != "id" && this.name != "data-id" && this.name != "contenteditable" && this.name != "data-citation-string" && this.name != "data-reftype" && this.name != "data-editable" && this.name != "data-lang" && this.name != "data-style-type" && this.name != "data-cont-type") {
						var newFields = fieldTemp.clone();
						$(newFields).removeClass('hidden').removeClass('template');						
						var attrName = this.name.replace('data-','');
						attrName = attrName.replace('costumer-','');
						$(newFields).find('[data-type="attribute"]').text(attrName);												
						$(newFields).find('[data-type="value"]').text(this.value);
						$('[data-component="jrnlModifyAttributes"]').find('[data-container="true"]').append($(newFields));						
					  }else if(this.name == "data-lang"){
						$('[data-component="jrnlModifyAttributes"] .lang-dropdown').val(this.value);
					  }else if(this.name == "data-cont-type"){
						$('[data-component="jrnlModifyAttributes"] [data-content-type="true"] [data-type="value"]').text(this.value);  
					  }else if(this.name == "data-style-type"){
						$('[data-component="jrnlModifyAttributes"] [data-style-type="true"] [data-type="value"]').text(this.value);  
					  }
					});
				  });
				  if($(currSelNode).hasClass('jrnlTblCaption') || $(currSelNode).closest('.jrnlTblCaption').length > 0){
					$('[data-component="jrnlModifyAttributes"] .content-type-dropdown').val('');
					$('[data-component="jrnlModifyAttributes"] .content-type-dropdown').val($(currSelNode).closest('.jrnlTblCaption').attr('data-cont-type'));
					$('[data-component="jrnlModifyAttributes"] [data-content-field="options"]').removeClass('hidden');
					$('[data-component="jrnlModifyAttributes"] [data-content-field="freeText"]').addClass('hidden');
				  }else{
					$('[data-component="jrnlModifyAttributes"] [data-content-field="options"]').addClass('hidden');
					$('[data-component="jrnlModifyAttributes"] [data-content-field="freeText"]').removeClass('hidden');
				  }
				  var nodeXpath = kriya.getElementXPath(currSelNode);
				  if((currSelNode.attr('id').length > 0)&&(nodeXpath != undefined)){						
						$('[data-component="jrnlModifyAttributes"]').attr('data-node-xpath',nodeXpath);						
					 }							
			},
			showPopUp  : function(param, targetNode){
				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				$('.activeElement').removeClass('activeElement');
				kriya.config.activeElements = false;
				var componentType = param.component;
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				var ele = $(targetNode);
				if(param && param.container){
					ele = kriya.xpath(param.container, targetNode);
					ele = $(ele);
					kriya.config.popUpContainer = ele;
				}
				if(param && param.sourceSelector && componentType == "modifyOrder_edit"){
					$(component).find('.collection').attr('data-source-selector', param.sourceSelector);
				}
				
				kriya['styles'].init(ele, componentType);
				// add manualSave class to component to call converttemplate - priya #180
				if($(targetNode).attr("data-add-saveclass")!=undefined){
					$('[data-type="popUp"]').addClass($(targetNode).attr("data-add-saveclass"));
				}
				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="new"]').removeClass('hidden');

				//If proof control component is open then set the val of selected control
				if(componentType == "proofControls"){
					var selectedType  = $(component).find('#proofControlAction').find(':selected').val();
					var controlAttr  = $(component).find('#proofControlAction').find(':selected').attr('data-attr');
					if(controlAttr){
						var val = $(kriya.selection.startContainer).closest('[' + controlAttr + ']').attr(controlAttr);						
						val = parseFloat(val);
						if(selectedType == "splCharacterStyle"){							
							val = $(kriya.selection.startContainer).closest('span[' + controlAttr + ']').attr(controlAttr);													
						}else if(selectedType == "splParagraphStyle"){
							val = $(kriya.selection.startContainer).closest('p[' + controlAttr + ']').attr(controlAttr);							
						}else if(selectedType == "removeParaIndent"){
							if($(kriya.selection.startContainer).closest('[' + controlAttr + ']').length){
								val = "Indent removed for this paragraph";
							}						
						}
						if(val){
							if(controlAttr=="data-spl-style"||controlAttr=="data-no-indent"){
								$(component).find('.splStyle').val(val);
							}
							else{
								$(component).find('.controlVal').val(val);
							}							
						}
					}
				}
			},
			deletePopUp : function(param, targetNode){
				var paramString = (param) ? "'param':"+JSON.stringify(param) : '';
				var deleteFunction = (param && param.delFunc)?param.delFunc : 'deleteElement';
				var nodeXpath = $(targetNode).closest('[data-node-xpath]').attr('data-node-xpath');
				if (! kriya.config.activeElements || targetNode.hasClass('removeNode')){
					kriya.config.activeElements = targetNode.parent();
				}

				//prasent address deletion not saving-kirankumar
				if(targetNode.closest('[data-fn-type][id]').length > 0){
					kriya.config.activeElements = targetNode.closest('[data-fn-type]');
				}

				//To check replied query; If data-replied is false then not allowed to delete.
				var isCheck = true;
				$(kriya.config.activeElements).find('.jrnlQueryRef').each(function(){
					if($(this)[0].hasAttribute('data-replied')==false){
						isCheck = false;
						return;
					}
				});
				if(isCheck==false){
					var message = '<div class="row">Please ensure that you have replied to the quries raised before you can perform this action.</div>';
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : message,
							icon : 'icon-info'
						});
					return;
				}
				var attrObj = {};
				var message = "Are you sure you want to delete?";
				if(nodeXpath){
					var selectorNode = kriya.xpath(nodeXpath);
					if($(selectorNode).closest('[class^="jrnl"][class*="Block"]').length > 0 && $(selectorNode).closest('.jrnlInlineFigure').length == 0){
						var blockNode = $(selectorNode).closest('[class^="jrnl"][class*="Block"]');
						var label = blockNode.find('.label:first');
						if(label.length > 0){
							label = label.clone(true).cleanTrackChanges().text();
							message = "Are you sure you want to delete " + label + "? Deleting " + label + " would also remove all citations associated with it.";
						}
					}else if($(selectorNode).closest('.jrnlRefText').length > 0){
						var refNode = $(selectorNode).closest('.jrnlRefText');
						var refId = refNode.attr('data-id');
						message = "Are you sure you want to delete the reference" + refId + "? Deleting " + refID + " would also remove all citations associated with it.";
					}
					attrObj['data-node-xpath'] = nodeXpath;
				}
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': '" + deleteFunction + "','channel':'components','topic':'general'," + paramString + "}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
			},
			autoAddEqualContrib : function(param,targetNode){
				$('.kriya-tags-suggestions').find('.jrnlAuthor').each(function(){
					if(param.indexOf($(this).text()) >= 0){
						var insertThisNode = '<span class="tags" contenteditable="false">'+$(this).html()+'<span class="removeKriyaTag"></span></span>&nbsp;'
						$('[data-class="jrnlEqContribRef"]').find('.kriya-tagsinput').append(insertThisNode);									
					}
				});
				eventHandler.components.PopUp.closePopUp(param, targetNode);
			},
			/*In author popup while removing the affiliation or present address display the confirmation message - rajesh*/
			deleteConfirmation : function(param, targetNode){
				var rid = targetNode.parent().attr('data-rid');
				var dataClass = targetNode.closest('ul').attr('data-class');
				var className = targetNode.closest('[data-type="popUp"]').attr('data-class');
				var ridCount = 0;
				$('.'+className).find('.'+dataClass).each(function(i){
					var ridArray = $(this).attr('data-rid').split(', ');
					if(ridArray.indexOf(rid)!=-1){
						ridCount = ridCount+1;
					}
				});
				if(ridCount>1){
					var message = "Are you sure you want to remove from this author?";
				}else{
					var message = "Are you sure you want to delete?";
				}
				var nodeXpath = kriya.getElementXPath(targetNode.parent());
				var attrObj = {};
				attrObj['data-node-xpath'] = nodeXpath;
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': 'removeLinks','channel':'components','topic':'general'}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings,targetNode);
			},
			editPopUp : function(param, targetNode){
				var popper  = $(targetNode).closest('[data-component]');
				var componentName = (param && param.component)?param.component:$(targetNode).closest("[data-component]").attr('data-component') + '_edit';
				var editElement = false;
				if (popper.length > 0 && popper[0].hasAttribute('data-node-xpath')){
					var nodeXpath = popper.attr('data-node-xpath');
					editElement = kriya.xpath(nodeXpath);
				}else if($(targetNode).attr('data-selector')!=undefined){
					var nodeXpath = $(targetNode).attr('data-selector');
					editElement = kriya.xpath(nodeXpath,$(targetNode));	
				}else if (param && param.id){
					editElement = $('#' + param.id);
					// to re-assign image as active element while replacing image - jai
					kriya.config.activeElements = editElement;
					$('.activeElement').removeClass('activeElement');
					kriya.config.activeElements.addClass('activeElement');
				}
				
				//If it is a block element like figure block table block
				//Don't get block if the edit element is inline image -jagan , 31/01/18
				if($(editElement).closest('[data-id^="BLK_"]').length > 0 && !$(editElement).hasClass('jrnlInlineFigure')){
						editElement = $(editElement).closest('[data-id^="BLK_"]');
						kriya.config.popUpContainer = editElement;
				}
				
				var component = $("*[data-type='popUp'][data-component='" + componentName + "']");
				// Start Issue Id - #448 - Dhatshayani . D Jan 10, 2018 - Change the component type based on the type of figure.
				if(component && component.length > 0 && kriya.config.activeElements && $(kriya.config.activeElements)[0].nodeName == 'IMG' && $(kriya.config.activeElements)[0].hasAttribute('class') && $(kriya.config.activeElements)[0].getAttribute('class').match(/figure/ig)){
					eventHandler.components.PopUp.changeFigureType(component,kriya.config.activeElements);
				}else if(component && component.length > 0 && targetNode[0].hasAttribute('data-change-type')){
					var currActiveElements = $(editElement).find('.jrnlFigure:not([data-track="del"])');
					eventHandler.components.PopUp.changeFigureType(component,currActiveElements);
				}// End Issue Id - #448 
				if(kriya.isUndefined(kriya.componentList[componentName]) && $(editElement).length > 0){
					//kriya['styles'].init($(kriya.config.activeElements), componentName);
					kriya['styles'].init($(editElement), componentName);
				}

                //Change the popup heading
                /*#174 -While editing the existing table the column and row value is set as default- Prabakaran.A-Focusteam */
                //commented by jai as this requirement is no longer needed
		//$(component).find('.addColumnTemplate').val((($(component).find('tr>th').length + $(component).find('tr>td').length) / $(component).find('tr').length));
                //$(component).find('.addRowTemplate').val($(component).find('tr').length);
                /*end of the code #174--While editing the existing table the column and row value is set as default- Prabakaran.A-Focusteam*/

				$(component).find('[data-pop-type]').addClass('hidden');
				$(component).find('[data-pop-type="edit"]').removeClass('hidden');
			},
			// to get cslmapping object - aravind
			getCslMappingObj: function(onSuccess){
				if((kriya.config.cslMappingObj != undefined) && (Object.keys(kriya.config.cslMappingObj).length == 0)){
					$.ajax({
						crossDomain: true,
						url: "/js/review_content/cslMapping.json",
						method: "GET",
						dataType: "JSON",
						success: function (data) {
							kriya.config.cslMappingObj = data;
							onSuccess(true);
						}
					});
				}else{
					onSuccess(true);
				}
			},
			// to handle reference with csl / aravind
			editCslRef: function(param, targetNode, repCsl) {
				eventHandler.components.PopUp.getCslMappingObj(function(status){
					if(status){
						var mappingObj = kriya.config.cslMappingObj;
						$('[data-component="jrnlRefCsl_edit"]').find('.refContent').find('.row').each(function () { // cleaning up popUp
							if (!($(this).hasClass('titleText'))) {
								$(this).closest('.row').remove();
							}
						});
						var doNotShowField = '[value="citation-number"][selected="selected"], [value="etal"][selected="selected"]'
						if (targetNode) {
							var popper = $(targetNode).closest('[data-component]');
							$('[data-component="jrnlRefCsl_edit"]').find('.cslReferenceOutput').html('');
							$('[data-component="jrnlRefCsl_edit"]').find('.validated').addClass('hidden');
							$('[data-component="jrnlRefCsl_edit"]').find('.validationFailed').addClass('hidden');
							if (popper.length > 0 && popper[0].hasAttribute('data-node-xpath')) {
								var nodeXpath = popper.attr('data-node-xpath');
								editElement = kriya.xpath(nodeXpath);
							}
						} else {
							editElement = repCsl;
						}
						//constructing refTypes from csl mapping
						$('[data-component="jrnlRefCsl_edit"]').find('[data-class="CslRefType"]').find('option').remove();
						for (var key in mappingObj['cslRefType']) {
							var newRefTypes = document.createElement('option');
							$(newRefTypes).attr('value', key);
							$(newRefTypes).text(mappingObj['cslRefType'][key].name);
							$('[data-component="jrnlRefCsl_edit"]').find('[data-class="CslRefType"]').append($(newRefTypes));
						}
						// selecting ref type
						var refType = $(editElement).attr('data-reftype');
						if (mappingObj['kriyaRefTypeToCslType'][refType] != undefined) {
							refType = mappingObj['kriyaRefTypeToCslType'][refType];
						}
						$('[data-component="jrnlRefCsl_edit"]').find('.ref-type').find('[selected]').removeAttr('selected');
						$('[data-component="jrnlRefCsl_edit"]').find('.ref-type').find('select').find('[value="' + refType + '"]').attr('selected', 'selected');
						if ($('[data-component="jrnlRefCsl_edit"]').length > 0) {
							var objEle = mappingObj['kriyaClassToUi'][refType];
							var lookUpObj = mappingObj['cslFieldsMapping'][refType];
							// contructing options according to csl mapping
							var optionNode = $('.cslRefTemples').find('[data-temp="dataLine"]').clone().removeAttr('data-temp');
							$(optionNode).append($('.cslRefTemples').find('[data-temp="option"]').clone().removeAttr('data-temp'));
							// $(optionNode).attr('class','row dataLine').append('<div class="col s4 refData"><span class="input-type" style="padding-left:12px;" remove-class="hidden"><select data-class="refDataType"></select></span><div>');
							for (var key in objEle) {
								var currNode = document.createElement('option');
								$(currNode).attr('value', objEle[key]).text(lookUpObj[objEle[key]].name);
								$(optionNode).find('[data-class="refDataType"]').append(currNode);
							}
							// constructing fields
							$(editElement).find('> *').each(function () {
								var currOptionNode = $(optionNode).clone();
								var currFieldVal = $(this).clone();
								if ($(currFieldVal).find('.del').length > 0) {
									$(currFieldVal).find('.del').remove();
								}
								if (objEle[$(currFieldVal).attr('class')] != undefined) {
									$(currOptionNode).find('[value="' + objEle[$(currFieldVal).attr('class')] + '"]').attr('selected', 'selected');
									$(currOptionNode).find('[data-class="refDataType"]').attr('prev-data', objEle[$(currFieldVal).attr('class')]);
									var currFieldNode = document.createElement('div');
									$(currFieldNode).attr('class', 'col s8');
									if ((objEle[$(currFieldVal).attr('class')] == "author") || (objEle[$(currFieldVal).attr('class')] == "editor")) {
										$(currOptionNode).find('select').attr('data-track-id', $(currFieldVal).attr('data-track-id'));
										var currFieldNode = $('.cslRefTemples').find('[data-temp="authorField"]').clone().removeAttr('data-temp');
										$(currFieldNode).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
										var givenNameField = $(currFieldNode).find('[data-class="jrnlGivenName"]');
										$(givenNameField).text($(currFieldVal).find('.RefGivenName').text());
										$(givenNameField).attr('data-focusin-data', $(currFieldVal).find('.RefGivenName').text());
										$(givenNameField).attr('data-node-xpath', '//*[@id="' + $(currFieldVal).find('.RefGivenName').attr('id') + '"]');
										var surNameField = $(currFieldNode).find('[data-class="jrnlSurName"]');
										$(surNameField).text($(currFieldVal).find('.RefSurName').text());
										$(surNameField).attr('data-focusin-data', $(currFieldVal).find('.RefSurName').text());
										$(surNameField).attr('data-node-xpath', '//*[@id="' + $(currFieldVal).find('.RefSurName').attr('id') + '"]');
									} else if (objEle[$(currFieldVal).attr('class')] == "DOI") {
										var currFieldNode = $('.cslRefTemples').find('[data-temp="dataField"]').clone().removeAttr('data-temp');
										$(currFieldNode).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
										$(currFieldNode).find('.text-line').append('<a href="http://dx.doi.org/' + $(currFieldVal).text() + '" target="_blank">' + $(currFieldVal).text() + '</a>');
										$(currFieldNode).find('.text-line').attr('data-focusin-data', $(currFieldVal).text());
										$(currFieldNode).find('.text-line').attr('data-node-xpath', '//*[@id="' + $(currFieldVal).attr('id') + '"]');
									}
									else {
										if (lookUpObj[objEle[$(currFieldVal).attr('class')]].required) {
											var currFieldNode = $('.cslRefTemples').find('[data-temp="dataField"]').clone().removeAttr('data-temp');
											$(currFieldNode).find('span[class="text-line"]').attr('data-validate', 'true');
											$(currFieldNode).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
										} else {
											var currFieldNode = $('.cslRefTemples').find('[data-temp="dataField"]').clone().removeAttr('data-temp');
											$(currFieldNode).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
										}
										if ($(currFieldVal).children('.ins').length > 0) {
											$(currFieldNode).find('.text-line').append($(currFieldVal).children()[0].innerHTML);
											$(currFieldNode).find('.text-line').attr('data-focusin-data', $(currFieldVal).children().text());
										}
										else if ($(currFieldVal).children().length > 0) {
											if ($(currFieldVal).children('em').length > 0) {
												$(currFieldVal).children('em').renameElement('i');
											}
											$(currFieldNode).find('.text-line').append($(currFieldVal).children());
											$(currFieldNode).find('.text-line').attr('data-focusin-data', $(currFieldVal).children().text());
										} else {
											$(currFieldNode).find('.text-line').text($(currFieldVal).text());
											$(currFieldNode).find('.text-line').attr('data-focusin-data', $(currFieldVal).text());
										}
										$(currFieldNode).find('.text-line').attr('data-node-xpath', '//*[@id="' + $(currFieldVal).attr('id') + '"]');
									}
									$('[data-component="jrnlRefCsl_edit"]').find('.refContent').append($(currOptionNode));
									$(currOptionNode).append($(currFieldNode));
									if ($(currFieldNode).find('[data-validate="true"]').length > 0) {
										$(currOptionNode).find('.refData').addClass('cslRequiredField');
									}
								}
							});
							$('[data-component="jrnlRefCsl_edit"]').find(doNotShowField).closest('.row').addClass('hidden');
							var containerId = $(editElement).attr('id');
							var cards = $('#historyDivNode .historyCard[data-content-id="' + containerId + '"]').clone(true);
							$('[data-component="jrnlRefCsl_edit"]').find('.historyTab .historyCard').remove();
							$('[data-component="jrnlRefCsl_edit"]').find('.historyTab').append(cards);
							if ($('[data-component="jrnlRefCsl_edit"]').hasClass('hidden')) {
								$('[data-component="jrnlRefCsl_edit"]').attr('data-node-xpath', nodeXpath);
								$('[data-component="jrnlRefText"]').addClass('hidden');
								kriya.popUp.openPopUps($('[data-component="jrnlRefCsl_edit"]'));
							}
						}
					}
				});
			},
			changeFigureType : function(component,currActiveElements){// Start Issue Id - #448 - Dhatshayani . D Jan 10, 2018 - Change the component type based on the type of figure.
				var activeNodeClass = $(currActiveElements).clone(true).removeClass('activeElement').attr('class');
				//commented by kirankumar:-unwanted attributes there in figure repalace popup
				/*if($(currActiveElements).length > 0 && $(currActiveElements)[0].hasAttribute('id')){
					component.find('.fileList').find('*[type="objFile"]').attr('data-replace-imgid',$(currActiveElements)[0].getAttribute('id'));
				}*/
				var compImgClass = component.find('.fileList').find('*[type="objFile"]').attr('data-class');
				if(activeNodeClass && compImgClass && compImgClass != activeNodeClass){
					var componentClass = component.find('*[data-img-class="'+activeNodeClass+'"]').attr('data-object-class');
					if(componentClass && componentClass != ''){
						component.attr('data-class',componentClass);
					}
					component.find('.fileList').find('*[type="objFile"]').attr('data-class', activeNodeClass);
					var checkAttr = 'data-selector-' +activeNodeClass;
					var fileListNode = component.find('.fileList').find('*[type="objFile"]');
					if(fileListNode && fileListNode.length > 0 && fileListNode.attr('data-selector') && fileListNode.attr(checkAttr)){
						var selectorChangeTo = fileListNode.attr(checkAttr);
						fileListNode.attr('data-selector',selectorChangeTo);
					}
				}
			},// End Issue Id - #448
			editSubPopUp : function(param, targetNode){
				var popper = $(targetNode).closest("[data-component]");
				var componentName = (param && param.component)?param.component: '';
				if (kriya.isUndefined(kriya.componentList[componentName])){
					//kriya.config.popUpContainer = $(targetNode).closest("li").find('[data-input="true"]');
					$(targetNode).attr('data-for-edit','true');
					var component = $('[data-type="popUp"][data-component="'+componentName+'"]');
					component.attr('data-sub-type', 'true');

					kriya.config.popUpContainer = $(targetNode);
					kriya['styles'].init(kriya.config.popUpContainer, componentName);
					kriya.config.subPopUpCards = $('<div />');
					//If popup is a edit popup
					if($(targetNode).html() != ""){
						$(component).find('[data-pop-type]').addClass('hidden');
						$(component).find('[data-pop-type="edit"]').removeClass('hidden');
					}else{
						$(component).find('[data-pop-type]').addClass('hidden');
						$(component).find('[data-pop-type="new"]').removeClass('hidden');
					}
				}
			},
			closePopUp : function(param, targetNode){
				var popper = $(targetNode).closest("[data-component]");
				kriya.popUp.closePopUps(popper);
				if (popper.attr('data-component') == 'QUERY'){
					$('[data-component="QUERY"]').removeAttr('data-display').removeAttr('data-sub-type').removeAttr('style');
				}
				if (popper.attr('data-component') == 'validateRef_edit'){
					popper.find('.validateAllBtn').removeClass('disabled');
				}
				popper.find('[data-type="dropDown"]').addClass("hidden"); //hide the dropdown in component
				if (kriyaEditor.settings.undoStack.length > 0){
					kriyaEditor.init.addUndoLevel('save-component');
				}
				if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}
				//when we are open publication history at that time kriya.config.popUpContainer value setting if we close that popup without save and open
				//open copyright statement and if change anything in that that history will adding in publication history popup.
				//that's i am given null to kriya.config.popUpContainer
				kriya.config.popUpContainer	= null;
			},
			// aravind to save uploaded author bio to component
			saveAuthorBio: function(param, targetNode){					
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				if(($(comp).find('img').length)&&($(comp).find('img').attr('src')!=undefined)){
					var savePath = $($(comp).find(".fileList").html()).clone(true);
					var currImageTag = $('[data-component="jrnlAuthorGroup_edit"]').find("span[data-class=jrnlFigure]");
					$(currImageTag).html(savePath);
				}					
				eventHandler.components.PopUp.closePopUp(param, targetNode);												
			},
			// aravind to save uploaded product info to component
			saveProductInfo: function(param, targetNode){					
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				if(($(comp).find('img').length)&&($(comp).find('img').attr('src')!=undefined)){
					var savePath = $($(comp).find(".fileList").html()).clone(true);
					var currImageTag = $('[data-component="jrnlProduct_edit"]').find("span[data-class=jrnlFigure]");
					$(currImageTag).html(savePath);
				}					
				eventHandler.components.PopUp.closePopUp(param, targetNode);												
			},
			populateAbbrevFootNote: function(param, targetNode){
				if($(targetNode).closest('span[class="jrnlTblDefItem"]').attr('data-track') == undefined){
					eventHandler.components.PopUp.showPopUp(param, targetNode);
					var componentType = param.component;
					var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
					$(component).find('[data-pop-type]').addClass('hidden');
					$(component).find('[data-pop-type="edit"]').removeClass('hidden');
				}				
			},			
			addAbbrevFootNote: function(param, targetNode){				
				var parentDivNode = $(targetNode).closest('[data-node-xpath]');
				var parentDivNodeClone = parentDivNode.clone(true);
				var currSelectedNode = kriya.xpath(parentDivNode.attr("data-node-xpath"));				
				var tableBlock = $(currSelectedNode).closest('[data-id^="BLK_"]');
				if(tableBlock.length < 1){ //if tableblock is not there no need to do all and it will break the code.
					return false;
				}
				var tableId = tableBlock.attr('data-id');
				tableId = tableId.replace('BLK_', '');
				var abbrevTerm = $(parentDivNode).find('[data-class="jrnlAbbrevTerm"]').html();
				var abbrevDef = $(parentDivNode).find('[data-class="jrnlFootDefinition"]').html();
				//Get the maxium id and construct the data-id value
				var dataId = tableId + '_FN1';								
				var comp = $("*[data-type='popUp'][data-component='jrnlAbbrevFoot']");
				var validated = kriya.saveComponent.validateComponents(comp);
				if(validated){
					var endSuffix = $('[tmp-class="jrnlFootDefinition"]').attr('tmp-node-suffix');
					var removeLastNodeSuffix = $('[tmp-class="jrnlFootDefinition"]').attr('tmp-remove-last-node-suffix');
					var setLastNodeSuffix = $('[tmp-class="jrnlFootDefinition"]').attr('tmp-set-last-node-suffix');					
					var alphabeticalOrder = $('[data-name="tmp_jrnlTblFootGroup"]').find('[tmp-class="jrnlTblDefItem"]').attr('tmp-alphabetical-order');
					//remove last node suffix
					if(setLastNodeSuffix != undefined){
						// if $(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition') not there code is breaking
						if($(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length && $(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').length > 0){
							$(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).nextSibling.remove();
						}
					}
					//to add suffix to last node
					// if $(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition') not there code is breaking
					if(removeLastNodeSuffix){
						if($(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length && $(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').length > 0){
							$(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).insertAdjacentHTML("afterend", endSuffix);
						}								
					}
					if($(currSelectedNode).closest('span[class="jrnlTblDefItem"]').length){
						$(currSelectedNode).find('.jrnlAbbrevTerm').html($(parentDivNode).find('[data-class="jrnlAbbrevTerm"]').html());
						$(currSelectedNode).find('.jrnlFootDefinition').html($(parentDivNode).find('[data-class="jrnlFootDefinition"]').html());								
					}
					else{	
						if(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[data-id]:not([data-track="del"])').length > 0){
							var idArr = [];
							var maxNum = 0
							tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[data-id]:not([data-track="del"])').each(function(){
								var dataId = $(this).attr('data-id');
								if(dataId.match(/\d+$/)){
									dataId = parseInt(dataId.match(/\d+$/)[0]);
									idArr.push(dataId);
								}
							});
							if(idArr.length > 0){
								var maxNum = Math.max.apply(null, idArr);
								maxNum = maxNum+1;
							}
							dataId = tableId + '_FN'+maxNum;
						}
						if(tableBlock.length > 0){ //inserting tabel foot abbrevation
							var saveNode;
							var footNote;							
							var fnType = $(kriya.xpath(parentDivNode.attr("data-node-xpath"))).closest('.jrnlTblFootHead').attr('data-fn-type');												
							var footNote = kriya.general.convertTemplate("jtnlTblDefItem", "", "", $('#compDivContent [tmp-class="jrnlTblFootText"] [tmp-class="jrnlTblDefItem"]').clone(true));
							footNote.attr('id', uuid());
							footNote.attr('fn-type',fnType);
							footNote.attr('data-id', dataId);							
							$(footNote).find('.jrnlAbbrevTerm').html(abbrevTerm);
							$(footNote).find('.jrnlFootDefinition').html(abbrevDef);
							// if($(kriya.xpath(parentDivNode.attr("data-node-xpath"))).closest('.jrnlTblFootHead').next().find('data-def-list').length){
							if($(tableBlock).find('[data-def-list]').length){
								saveNode = footNote.insertAfter($(tableBlock).find('div[fn-type="abbrev"]').find('span[class="jrnlTblDefItem"]').last());
							}
							else{	
								if($(kriya.xpath(parentDivNode.attr("data-node-xpath"))).closest('.jrnlTblFootHead').next().attr('fn-type')=="abbrev"){
									// saveNode = $('<div class="jrnlTblFoot" fn-type="abbrev" data-def-list="true"><div class="jrnlTblFootText">'+footNote[0].outerHTML+'</div></div>').insertAfter($(kriya.xpath(parentDivNode.attr("data-node-xpath"))).closest('.jrnlTblFootHead').next());
									//Table abbrev footnote is editable-kirankumar
									saveNode = $('<div class="jrnlTblFoot" fn-type="abbrev" data-def-list="true"><div class="jrnlTblFootText" data-editable="false">'+footNote[0].outerHTML+'</div></div>').insertAfter($(tableBlock).find('div[fn-type="abbrev"]').find('span[class="jrnlTblDefItem"]').last());
								}
								else{
									//Table abbrev footnote is editable-kirankumar
									saveNode = $('<div class="jrnlTblFoot" fn-type="abbrev" data-def-list="true"><div class="jrnlTblFootText" data-editable="false">'+footNote[0].outerHTML+'</div></div>').insertAfter($(kriya.xpath(parentDivNode.attr("data-node-xpath"))).closest('.jrnlTblFootHead'));
								}
							}						
						}					
					}					
					// Sorting ascending order	
					if(alphabeticalOrder != undefined){
						var tblAbbrevList = $(tableBlock).find('div[fn-type="abbrev"]').find('span[class="jrnlTblDefItem"]');										
						var reA = /[^a-zA-Z]/g;
						var reN = /[^0-9]/g;							
						function sortAlphaNum(a,b) {
							a = $(a).find('.jrnlAbbrevTerm').text();
							b = $(b).find('.jrnlAbbrevTerm').text();
							// to sort case insensitive - aravind
							if(alphabeticalOrder == "noncasesensitive"){
								a = a.toLowerCase();
								b = b.toLowerCase();
							}
							var aA = a.replace(reA, "");
							var bA = b.replace(reA, "");
							if(aA === bA) {
								var aN = parseInt(a.replace(reN, ""), 10);
								var bN = parseInt(b.replace(reN, ""), 10);
								return aN === bN ? 0 : aN > bN ? 1 : -1;
							}
							else {
								return aA > bA ? 1 : -1;
							}
						}
						var sortedDivs = tblAbbrevList.sort(sortAlphaNum);
						var parentNode = $(tableBlock).find('div[fn-type="abbrev"]').find('span[class="jrnlTblDefItem"]').parent();
						if(parentNode.length>0){
							$(parentNode).find('div[fn-type="abbrev"]').find('span[class="jrnlTblDefItem"]').remove();
							$(parentNode).append(sortedDivs);
							savedNode = parentNode;
						}
					}
					//start Here i minimized bellow commented selector and it will give same result of Commented selector and i stored this in tableBlockNode variable
					//insted of using total selector i am using tableBlockNode variable and if in abrevation footnote text dont have ; or . at that time  $(tableBlockNode)[0].nextSibling is comming as undefined at that time code is breaking
					var tableBlockNode = $(tableBlock).find('div[fn-type="abbrev"] span:not([data-track="del"])[class="jrnlTblDefItem"]:last .jrnlFootDefinition');
					//remove last node suffix
					if(removeLastNodeSuffix){
						//if($(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length){
							//$(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).nextSibling.remove();
						//}
						if(tableBlockNode.length > 0 && $(tableBlockNode)[0].nextSibling){
							tableBlockNode[0].nextSibling.remove();
						}
					}
					// to set last node suffix
					if(setLastNodeSuffix != undefined){
						// if($(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length){
						// 	$(tableBlock).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).insertAdjacentHTML("afterend", setLastNodeSuffix);
						// }
						if(tableBlockNode.length > 0){
							tableBlockNode[0].insertAdjacentHTML("afterend", setLastNodeSuffix)
						}
						//end
					}
					kriyaEditor.settings.undoStack.push(tableBlock);
					kriyaEditor.init.addUndoLevel('new-linked-table-fn');											
					kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));						
				}
			},
			shortTitleCharCount: function(param, targetNode){
				var maxLimit = $(targetNode).attr('data-maxLimit');
				if(maxLimit){
					var lengthCount = $(targetNode).text().length;              
					if (lengthCount > maxLimit) {
						$(targetNode).attr('data-error','Characters count exceed more than '+maxLimit);
					}
					else{
						$(targetNode).removeAttr('data-error');
					}
				}										
			},
			saveArticleInfo: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				var triggerSave = false;
				if($(comp).find('[data-name="shortTitle"]').length){
					var shortTitleNode = $(comp).find('[data-name="shortTitle"]').find('[data-class="jrnlRRH"]');
					var maxLimit = $(shortTitleNode).attr('data-maxLimit');
					if(maxLimit){
						var lengthCount = $(shortTitleNode).text().length;              
						if (lengthCount > maxLimit) {
							$(shortTitleNode).attr('data-error','Characters count exceed more than '+maxLimit);
						}
						else{
							$(shortTitleNode).removeAttr('data-error');
							triggerSave = true;
						}
					}										
				}
				else{
					triggerSave = true;
				}				
				if(triggerSave){
					eventHandler.components.PopUp.saveComponent(param,targetNode);
				}
			},
			// to save edited xml on ace editor
			saveEditedXml: function (param, targetNode) {
				var popper = $(targetNode).closest('[data-type="popUp"]');
				if ($(popper).find("#xmlEditor").length == 0) {
					return false;
				}
				var modifiedXml = ace.edit("xmlEditor").getValue();
				modifiedXml = modifiedXml.replace(/[\n\r]+[ \t]*/g, '');
				$(popper).progress('Saving');
				if (modifiedXml) {
					$.ajax({
						type: "POST",
						url: "/api/updatearticle",
						data: {
							'customerName':kriya.config.content.customer,
							'projectName':kriya.config.content.project,
							'doi':kriya.config.content.doi,							
							'replaceXml':'true',
							'xmlFrag':modifiedXml
						},
						success: function(res){
							if(res.error){
								kriya.notification({
									title : 'ERROR',
									type  : 'error',								
									content : 'Failed to save changes to xml',
									icon: 'icon-warning2'
								});
								$(popper).progress('', true);
							} else {
								kriya.notification({
									title : 'Information',
									type  : 'success',
									timeout : 5000,
									content : 'All changes saved to xml',
									icon : 'icon-info'
								});
								$(popper).progress('', true);
								kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
							}							
						},
						error: function(err){
							kriya.notification({
								title : 'ERROR',
								type  : 'error',								
								content : 'Failed to save changes to xml',
								icon: 'icon-warning2'
							});
							$(popper).progress('', true);
						}
					});
				}
			},
			saveMultipleAuthor: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				var inputAuthors = $(comp).find('.text-line').text();
				comp.progress('Validating');
				var params = {"authorNodeString" : '<p>'+ inputAuthors +'</p>', "classPrefix" : "jrnl"};
				kriya.general.sendAPIRequest('parseauthors', params, function(res){
					var newNode;
					if (res.body == ''){
						comp.progress('',true);
						kriya.notification({
							title : 'ERROR',
							type  : 'error',								
							content : 'Unable to parse Author',
							icon: 'icon-warning2'
						});
					}else if ($(res.body).find('.jrnlAuthor').length > 0){
						comp.progress('',true);						
						newNode = $(res.body).find('.jrnlAuthor');
						if(newNode.length > 0){
							$(newNode).each(function(){
								var clonedTemp = $('[data-element-template="true"] [tmp-class="jrnlGroupAuthor"]');
								clonedTemp.attr('tmp-insert-at', 'false');
								clonedTemp.removeAttr('tmp-insert-citation');
								var newAuthorNode = kriya.general.convertTemplate('jrnlGroupAuthor', comp, '', clonedTemp);
								$(newAuthorNode).find('.jrnlGivenName').text($(this).find('.jrnlGivenName').text());
								$(newAuthorNode).find('.jrnlSurName').text($(this).find('.jrnlSurName').text());
								if($('[data-type="popUp"][data-component="jrnlAuthorGroup_edit"]').find('.jrnlGroupAuthor').length > 0){
									$(newAuthorNode).insertAfter($('[data-type="popUp"][data-component="jrnlAuthorGroup_edit"]').find('.jrnlGroupAuthor').last());
								}else if($('[data-type="popUp"][data-component="jrnlAuthorGroup_edit"]').find('[data-class="jrnlGroupAuthorsGroup"]').length > 0){									
									$('[data-type="popUp"][data-component="jrnlAuthorGroup_edit"]').find('[data-class="jrnlGroupAuthorsGroup"]').append(newAuthorNode);									
								}								
							});
							kriya.popUp.closePopUps(comp);
						}else{
							kriya.notification({
								title : 'ERROR',
								type  : 'error',								
								content : 'Unable to parse Author',
								icon: 'icon-warning2'
							});
						}						
					}else{
						comp.progress('',true);
						kriya.notification({
							title : 'ERROR',
							type  : 'error',								
							content : 'Parsing Authors Failed',
							icon: 'icon-warning2'
						});
					}					
				});						
			},
			saveAbstract: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				if(comp == undefined){
					return false;
				}
				var absPara = $(comp).find('[data-class="jrnlAbsPara"]').text();
				var savedNode, newBlock;
				if($('.jrnlAbsGroup').length > 0){
					newBlock = kriya.general.convertTemplate('jrnlAbsPara');					
					savedNode = $(newBlock).html(absPara);
				}else{
					newBlock = kriya.general.convertTemplate('jrnlAbsGroup');
					$(newBlock).find('.jrnlAbsPara').html(absPara);
					savedNode = $(newBlock);
				}					
				if(savedNode){
					savedNode.attr('data-inserted', 'true');
					kriyaEditor.settings.undoStack.push(savedNode);
					kriyaEditor.init.addUndoLevel('save-abstract');
					kriya.popUp.closePopUps(comp);
				}						
			},			
			/**
			 * Function to save the pop and sub popup
			 */
			saveComponent: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				/*var elm;
				if(comp.find('*[data-for-edit]').length > 0){
					elm = comp.find('*[data-for-edit]').closest('li');
				}*/
				if($('[data-type="popUp"]:visible').length > 1 && comp.find('*[data-for-edit]').length == 0 && comp.attr('data-class') != 'jrnlCorrAff'){
					if (kriya.saveComponent.validateComponents(comp[0])){
						if(comp.attr('data-class') == 'jrnlGroupAuthor'){
							var savedNode = kriya.saveComponent.init(comp[0]);
						}else{
							var savedNode = kriya.saveComponent.addNewComponent(comp[0]);
						}
					}
				}else if($('[data-type="popUp"]:visible').find('.authorReorder').length > 0){
					var savedNode = kriya.saveComponent.init(comp[0],$(kriyaEditor.settings.contentNode).find('.jrnlAuthors')[0]);
				}else if(kriya.config.popUpContainer){
					//To save the compoenet Ex: edit the affiliation from the author popup or figure
					var savedNode = kriya.saveComponent.init(comp[0], kriya.config.popUpContainer);
					if(savedNode){
						kriya.config.popUpContainer	= null;
						kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
						//If the save node is content element then return false don't add undo level
						if($(savedNode).closest(kriya.config.containerElm).length == 0){
							return false;
						}
					}
				}else{
					var savedNode = kriya.saveComponent.init(comp[0]);
					//Trigger the citation reorder after inserting the figure,table etc.
					/*if($(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]').length > 0){
						var reorderNode = $(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]');
						reorderNode.removeAttr('citation-reorder');
						//kriya.general.triggerCitationReorder($(reorderNode), 'insert');
						citeJS.general.updateStatus();
					}*/
				}
				//callback functions - jai 21-08-2017
				if(savedNode && comp[0].hasAttribute('data-callback')){
					var execFn = getStringObj(comp.attr('data-callback'));
					execFn(savedNode);
				}

				//Remove the empty related article author - jagan
				if($(comp).attr('data-component') == "jrnlRelArt_edit"){
					$(savedNode).find('.jrnlRelArtAuthorGroup,.jrnlRelArtAuthor').each(function(){
						if($(this).text() == ""){
							$(this).remove();
						}
					});
				}
				//Remove the empty name in product info - aravind	
				if($(comp).attr('data-component') == "jrnlProduct_edit"){
					$(savedNode).find('.jrnlProNameGroup,.jrnlProName').each(function(){
						if(!(/^[a-zA-Z]/).test($(this).text())){
							$(this).remove();
						}
					});					
				}

				//Add id to the save for closest save node of newly added node - jagan
				if(savedNode && $(savedNode).closest('[data-save-node]:not([id])').length > 0){
					$(savedNode).closest('[data-save-node]:not([id])').attr('id', uuid.v4());
				}
				//when we add first abbreviation in back at that time id will add and data-insert attr will add to parent div for saving
				if(savedNode && $(savedNode).closest('[data-save-abbr]:not([id])').length > 0){
					$(savedNode).closest('[data-save-abbr]:not([id])').attr('id', uuid.v4());
					savedNode = $(savedNode).closest('[data-save-abbr]');
					$(savedNode).find('h1:not([id])').attr('id',uuid.v4());
					$(savedNode).attr('data-inserted','true');
					$(savedNode).removeAttr('data-save-abbr');
				}

				// sorting abbreviations - priya #180
				if(savedNode && comp[0].hasAttribute('data-sort')){
					var divList = $(kriya.config.containerElm).find("."+comp.attr('data-class'));
					/*var sortedDivs = divList.sort(function (a, b) {
						return String.prototype.localeCompare.call($(a).find("."+comp.attr('data-sort')).text().toLowerCase() , $(b).find("."+comp.attr('data-sort')).text().toLowerCase());
					});*/
					// Sorting ascending order from filterArray value - rajesh
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;
					function sortAlphaNum(a,b) {
						a = $(a).find("."+comp.attr('data-sort')).text();
						b = $(b).find("."+comp.attr('data-sort')).text();
						// to sort case insensitive - aravind
						if(comp[0].hasAttribute('data-alphabetical-order') && $(comp[0]).attr("data-alphabetical-order") == "noncasesensitive"){
							a = a.toLowerCase();
							b = b.toLowerCase();
						}
		    			var aA = a.replace(reA, "");
		    			var bA = b.replace(reA, "");
		    			if(aA === bA) {
		        			var aN = parseInt(a.replace(reN, ""), 10);
		        			var bN = parseInt(b.replace(reN, ""), 10);
		        			return aN === bN ? 0 : aN > bN ? 1 : -1;
		    			} else {
		        		return aA > bA ? 1 : -1;
		    			}
					}
					var sortedDivs = divList.sort(sortAlphaNum);
					var parentNode = $(kriya.config.containerElm).find("."+comp.attr('data-class')).parent();
					if(parentNode.length>0){
						$(parentNode).find("."+comp.attr('data-class')).remove();
						$(parentNode).append(sortedDivs);
						savedNode = parentNode;
					}	
				}
				if(kriya.config.subPopUpCards && $(kriya.config.subPopUpCards).find('.historyCard').length > 0){
					$(kriya.config.subPopUpCards).find('.historyCard').each(function(){
						$('#historyDivNode').prepend(this);
						kriyaEditor.settings.undoStack.push(this);
						kriya.config.subPopUpCards = undefined;
					});
				}
				if(savedNode && savedNode != true && $(savedNode).length > 0 && $(savedNode)[0].hasAttribute('data-id') && $(savedNode)[0].hasAttribute('data-inserted')){
					var dataId = $(savedNode).attr('data-id');
					
					var savedXpath = kriya.getElementXPath($(savedNode)[0]);
					//#274 - Inline table Insertion: Citation popup must not be shown after insertion -Helen.J(helen.j@focusite.com)
                    //#274 - Removing and adding citation after a table insert -Helen.J(helen.j@focusite.com)
                    if (dataId.match(/^BLK_/) && savedNode.attr('insert-citation') == "true") {
						var newFlatID = $(savedNode).find('.label').attr('data-block-id');
						if(newFlatID){
							$(savedNode).find('.label').removeAttr('data-block-id');
							savedNode.attr('data-id', 'BLK_' + newFlatID);
							savedNode.find('[class*="Caption"]' ).attr('data-id', newFlatID);
						}
                    //End of #274
						$(savedNode)[0].scrollIntoView();
						kriya.general.updateRightNavPanel(dataId, kriya.config.containerElm);
						var floatLabel = $(savedNode).find('.label').text();
						floatLabel = floatLabel.replace(/([\.\,\:]+)$/, '');
						var message = '<div class="row">' + floatLabel + ' has been inserted. Please inset a citation for ' + floatLabel + ', as all Objects must have at least one citation in the text. Please note that our system bases Figure/Table/Video/Supplementary file numbering on where they are first cited in the text.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : message,
							icon : 'icon-info'
						});
					}else if(dataId.match(/^B?FN/)){
						var message = '<div class="row">A new footnote has been added. Please insert a citation in the text for this note.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : message,
							icon : 'icon-info'
						});

						//Scroll to inserted foot note
						$(savedNode)[0].scrollIntoView();
					}
					else if($(savedNode).attr('class') == "jrnlAppBlock"){
						//Scroll to inserted foot note
						$(savedNode)[0].scrollIntoView();
					}
				}
				
				if(TomloprodModal.isOpen){
					TomloprodModal.closeModal();
				}
				
				if (savedNode) {
					kriya.popUp.closePopUps(comp);
					$(comp).find('[data-type="dropDown"]').addClass("hidden"); //hide the dropdown in component
					//Dont save the content when group author is added in author popup
					if(comp.attr('data-class') == 'jrnlGroupAuthor' && $(comp).hasClass('manualSave') && $('[data-component]:visible').length>1){
						return false;
					}
					if (savedNode != true){
						kriyaEditor.settings.undoStack.push(savedNode);
						var saveNodesList = kriyaEditor.settings.undoStack;
						kriyaEditor.init.addUndoLevel('save-component');
						callUpdateArticleCitation($(saveNodesList), '.jrnlCitation');
					}
				}
				//added by kiran:-when insert new image uncited list count not updated
				citeJS.general.updateFloatDetails();
			},
			//Function to save package pdf path to article meta of xml			
			uploadPackagePdf : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var srcAop = $(popper).find('[src]');
				var pathAop;
				if(srcAop.length > 0){
					pathAop = '<custom-meta data-content-type="AOP"><meta-name>AOP PDF</meta-name><meta-value>'+$(srcAop).attr("src")+'</meta-value></custom-meta>';
					$.ajax({
						type: "POST",
						url: "/api/updatearticle",
						data: {
							'customerName':kriya.config.content.customer,
							'projectName':kriya.config.content.project,
							'doi':kriya.config.content.doi,
							'xpath':'//custom-meta-group/custom-meta[@data-content-type="AOP"]',
							'xmlFrag':pathAop,
							'insertInto':'//custom-meta-group'
						},
						success: function(res) {
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Uploaded successfully.',
								type: 'success',
								content: 'PDF uploaded successfully.',
								icon : 'icon-info'
							});
							$('#welcomeContainer .welSignOffBtn').removeClass('disabled');
						},
						error: function(err){
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Failed.',
								type: 'error',
								content: 'Saving the uploaded pdf was failed.',
								timeout: 8000,
								icon : 'icon-info'
							});
						}
					});
				}
				kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
			},
			/**
			 * Function to insert the graphical Abstract added by vijayakumar on 27-03-2019
			 */
			uploadGraAbstract : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var srcAop = $(popper).find('[src]');
				var absPara = $(popper).find('[data-class="jrnlAbsPara"]');
				var pathAop;
				if(srcAop.length > 0){
					if(srcAop.attr('src') == "")return;
					var timest = uuid();
					var graptemp = "";
					if(absPara.html() ==""){
						graptemp = "<div class='jrnlAbsGroup' id='"+timest+"' data-inserted='true' data-abstract-type='graphical'><div class='jrnlFigBlock' data-id='BLK_FG1' id='BLK_FG1' data-stream-name='a_FG1' data-inline='true'><img class='jrnlFigure' id='"+uuid()+"' src='"+srcAop.attr('src')+"'/></div></div>";
					}else{
						graptemp = "<div class='jrnlAbsGroup' id='"+timest+"' data-inserted='true' data-abstract-type='graphical'><div class='jrnlFigBlock' data-id='BLK_FG1' id='BLK_FG1' data-stream-name='a_FG1' data-inline='true'><img class='jrnlFigure' id='"+uuid()+"' src='"+srcAop.attr('src')+"'/></div><h1 class='jrnlAbsHead' id='"+uuid()+"'>"+absPara.html()+"</h1></div>";
					}
					if($('#contentDivNode .front .jrnlPermission').length > 0){
						$('#contentDivNode .front .jrnlPermission').first().after(graptemp);
					}else{
						$('#contentDivNode .front').append(graptemp);
					}
					kriyaEditor.init.save($('#'+timest));
					$('#contentDivNode .front *[data-abstract-type="graphical"]').remove();
					if($('#contentDivNode .front .jrnlAbsGroup').length > 0){
						$('#contentDivNode .front .jrnlAbsGroup').first().before(graptemp);
					}else{
						$('#contentDivNode .front').append(graptemp);
					}
				}
				kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
			},
			/**
			 * Function to insert Video abstract added by vijayakumar 03-06-2019
			 */
			uploadVidAbstract : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var imagePath = $(popper).find('*[data-class="image"]').text();
				var video = $(popper).find('*[data-class="video"]').text();
				var label = $(popper).find('*[data-class="label"]').text();
				if(imagePath == "" || video == "" || label == ""){
					return;
				}
				var timest = uuid();
				var pathAop;
				graptemp = '<div class="jrnlAbsGroup" id="'+timest+'" data-inserted="true" data-abstract-type="video"><div class="jrnlVidBlock" id="BLK_VA1" data-abstract-type="video" data-id="BLK_VA1" data-inline="true"><p class="jrnlVidCaption" data-id="VA1" data-id="VA1"><span class="label">'+label+'</span></p><video class="jrnlVideo" preload="none" poster="'+imagePath+'" src="'+video+'" controls="controls"><source src="dasd1" data-primary-extension="jpg" type="video/mp4; codecs=&amp;quot;avc1.42E01E, mp4a.40.2&amp;quot;" alt="'+video+'" mimetype="video" mime-subtype="mp4"><source type="video/ogg; codecs=&amp;quot;avc1.42E01E, mp4a.40.2&amp;quot;" src=""></video></div>';
				$('.front').append(graptemp);
				kriyaEditor.init.save($('#'+timest));
				kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
			},
			/**
			 * Function to insert the supplementary file
			 */
			saveSupplementary : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var selectedSuppOf = popper.find('#float-list-dropdown li.selected');
				var selectedSuppType = popper.find('#supplement-type-dropdown li.selected');
				var supplClass = selectedSuppType.attr('data-block-class');
				supplClass = (supplClass)?supplClass:'jrnlSupplBlock';

				popper.attr('data-class', supplClass);
				var captionClass = supplClass.replace(/Block/,'Caption');
				var footClass    = supplClass.replace(/Block/,'Foot');

				//Change the class name and selector for caption and footnote
				popper.find('[data-type="htmlComponent"][data-class$="Caption"]').attr('data-class', captionClass);
				popper.find('[data-type="htmlComponent"][data-class$="Caption"]').attr('data-selector', ".//*[@class='" + captionClass + "']");

				popper.find('[data-type="htmlComponent"][data-class$="Foot"]').attr('data-class', footClass);
				popper.find('[data-type="htmlComponent"][data-class$="Foot"]').attr('data-selector', ".//*[@class='" + footClass + "']");

				if(kriya.config.popUpContainer){
					var savedNode = kriya.saveComponent.init(popper[0], kriya.config.popUpContainer);
				}else{
					var savedNode = kriya.saveComponent.init(popper[0]);
				}
				

				if(savedNode){
					kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
					//Trigger the citation reorder after inserting supplementary
					/*if($(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]').length > 0){
						var reorderNode = $(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]');
						reorderNode.removeAttr('citation-reorder');
						$(reorderNode).attr('data-citation-string',' ' + newCiteId + ' ');
						$(reorderNode).html(newFloatCiteString);
						//kriya.general.triggerCitationReorder($(reorderNode), 'insert');
						citeJS.general.updateStatus();
					}*/
					if($(savedNode)[0].hasAttribute('data-inserted')){
						var newSupplId = $(savedNode).find('.label').attr('data-block-id');
						var mainFloat = $(savedNode).find('.label').attr('data-main-float');
						if(newSupplId){
							$(savedNode).find('.label').removeAttr('data-block-id');
							savedNode.attr('data-id', 'BLK_'+newSupplId);
							savedNode.find('[class*="Caption"]').attr('data-id', newSupplId);
						}
						if(mainFloat){
							var mainFloatBlock = $(kriya.config.containerElm + ' [data-id="' + mainFloat + '"]').not('.del, [data-track="del"]').not(savedNode).last();
							$(savedNode).insertAfter(mainFloatBlock);
						}
						$(savedNode)[0].scrollIntoView();

						//Trigger the cite now notification when supplementary block inserted
						var floatLabel = $(savedNode).find('.label').text();
						floatLabel = floatLabel.replace(/([\.\,\:]+)$/, '');
						var savedXpath = kriya.getElementXPath($(savedNode)[0]);
						var message = '<div class="row">' + floatLabel + ' has been inserted. Please inset a citation for ' + floatLabel + ', as all Objects must have at least one citation in the text. Please note that our system bases Figure/Table/Video/Supplementary file numbering on where they are first cited in the text.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : message,
							icon : 'icon-info'
						});
					}
					kriya.general.updateRightNavPanel($(savedNode).attr('data-id'), kriya.config.containerElm);
					kriyaEditor.settings.undoStack.push(savedNode);
					kriyaEditor.init.addUndoLevel('save-supplementary');
				}
			},
			insertHyperLink : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var contentHtml = kriya.selection.getHTMLContents();
				var href = popper.find('[data-class="href"]').text();
				var res = href.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
				//inserted by kiran for showing message
				if(res == null){
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						timeout : 5000,
						content : "Invalid link",
						icon: 'icon-warning2'
					});
				}else{
					$.ajax({
						type: "GET",
						url:  "/api/geturl?url="+href+"&validate=true",
						success: function (data) {
							if(data==""){
								kriya.notification({
									title : 'ERROR',
									type  : 'error',
									timeout : 5000,
									content : "Link validation failed",
									icon: 'icon-warning2'
								});
								return false;
							}else{
								var saveNode = $(kriya.selection.startContainer).closest('[id]');
								if(href){
									//href = href.trim().replace(/\s/gu,"%20");
									if($(kriya.selection.startContainer).closest('.jrnlExtLink').length > 0){
										$(kriya.selection.startContainer).closest('.jrnlExtLink').attr('data-href', href);
									}else{
										//kriya.selection.deleteContents();
										//kriya.selection.insertNode($(LinkHtml)[0]);
										var LinkHtml = '<span class="jrnlExtLink" data-message="{\'click\':{\'funcToCall\': \'contentEvt\',\'channel\':\'components\',\'topic\':\'general\'}}" data-href="' + href + '" data-inserted="true">' + contentHtml + '</span>';
										if(kriya.selection && kriya.selection.startContainer && $(kriya.selection.startContainer).hasClass('jrnlQueryRef') && contentHtml){
											var linkNode = $.parseHTML(LinkHtml);
											if(linkNode.length > 0 && $(linkNode[0].firstChild).hasClass('jrnlQueryRef')){
												$(linkNode[0].firstChild).remove();
												LinkHtml = linkNode[0].outerHTML;
											}
										}
										
										kriya.selection.pasteHtml(LinkHtml);
									}
								}
								kriya.popUp.closePopUps(popper);
								kriyaEditor.settings.undoStack.push(saveNode);
								kriyaEditor.init.addUndoLevel('insert/edit-Link');
							}
						},
						error: function(res){
								kriya.notification({
									title : 'ERROR',
									type  : 'error',
									timeout : 5000,
									content : "Link validation failed",
									icon: 'icon-warning2'
								});
							return false;
						}
					});
				}	
			},
			removeHyperLink : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var saveNode = $(kriya.selection.startContainer).closest('.jrnlExtLink').parents('[id]:first');
				if($(kriya.selection.startContainer).closest('.jrnlExtLink').length > 0){
					$(kriya.selection.startContainer).closest('.jrnlExtLink').contents().unwrap();
				}
				kriya.popUp.closePopUps(popper);
				kriyaEditor.settings.undoStack.push(saveNode);
				kriyaEditor.init.addUndoLevel('remove-Link');
			},
			savePlaceFloats : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				popper.find('table tbody tr:not([data-template="true"])').each(function(){
					var floatXpath = $(this).attr('data-node-xpath');
					var floatObj = kriya.xpath(floatXpath);
					if($(floatObj).length > 0){
						$(this).find('[data-selector]:not(.floatLabel)').each(function(){
							var selector = $(this).attr('data-selector');
							var matches = selector.match(/(?:(\/+)\@)([a-z0-9\-]+)$/ig);
							if(matches){
								var attrName = matches[0].replace(/(\/+)\@/ig, '');
								if($(this).text()){
									$(floatObj).attr(attrName, $(this).text());
								}else{
									$(floatObj).removeAttr(attrName);
								}
							}
						});
						kriyaEditor.settings.undoStack.push(floatObj);
					}
				});
				kriya.popUp.closePopUps(popper);
				kriyaEditor.init.addUndoLevel('update-proof-control');
			},
			removeClinicalTrial: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var nodeXpath = popper.attr('data-node-xpath');
				var dataNode = kriya.xpath(nodeXpath);
				if($(dataNode).length > 0){
					var saveNode = $(dataNode).parents('[id]:first');
					$(dataNode).contents().unwrap();
					kriyaEditor.settings.undoStack.push(saveNode);
					kriyaEditor.init.addUndoLevel('remove-clinical-trial');
					kriya.popUp.closePopUps(popper);
				}
			},
			saveClinicalTrial: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var nodeXpath = popper.attr('data-node-xpath');
				var dataNode = kriya.xpath(nodeXpath);
				var clinicalTrialType = popper.find('#clinicalTrialType').val();
				var clinicalresultType = popper.find('#clinicalresultType').val();
				if(clinicalTrialType && clinicalresultType){
					if($(dataNode).length == 0 || $(kriya.config.containerElm).find(dataNode).length == 0){
						if (kriya.selection){
							range = rangy.getSelection();
							range.removeAllRanges();
							range.addRange(kriya.selection);
						}
						var selection = rangy.getSelection();
						var range = selection.getRangeAt(0);

						var dataNode = document.createElement('span');
						dataNode.setAttribute('id', uuid.v4());
						dataNode.setAttribute('class', 'jrnlClinicalTrial');
						dataNode.innerHTML = range.toHtml();
						dataNode.setAttribute('data-href', dataNode.innerText);	
						selection.getRangeAt(0).deleteContents()
						selection.getRangeAt(0).insertNode(dataNode);
					}else{
						dataNode = dataNode[0];
					}

					dataNode.setAttribute('ext-link-type', clinicalTrialType);
					dataNode.setAttribute('data-result-type', clinicalresultType);
					dataNode.setAttribute('specific-use', 'clinicaltrial ' + clinicalresultType);
					
					kriyaEditor.settings.undoStack.push(dataNode);
					kriyaEditor.init.addUndoLevel('save-clinical-trial');

					kriya.popUp.closePopUps(popper);
				}else{
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						timeout : 5000,
						content : "Please select clinical trial type and result type.",
						icon: 'icon-warning2'
					});
				}
			},
			saveProofControl : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var selectedType = popper.find('#proofControlAction').val();
				var controlAttr  = popper.find('#proofControlAction').find(':selected').attr('data-attr');
				var pointVal     = popper.find('.controlVal').val() || popper.find('.splStyle').val();
				popper.find('.controlVal').removeAttr('data-error');
				var editNode = $(kriya.selection.startContainer).closest('p,div,table,h1,h2,h3,h4,h5,h6,ol,ul');
				if(selectedType == "forceJustify"){
					var editNode = $('<span class="forceJustify" title="force justification used for typesetting">&nbsp;</span>');
					kriya.selection.insertNode(editNode[0]);
				}else if(selectedType == "columnBreak"){
					var editNode = $('<span class="forceColBrk" title="column break used for typesetting">&nbsp;</span>');
					kriya.selection.insertNode(editNode[0]);
				}else if(selectedType == "nbsp"){
					var editNode = document.createTextNode('\u00A0');
					kriya.selection.insertNode(editNode);
				}else if(selectedType == "margin" && controlAttr){
					$(editNode).attr(controlAttr, pointVal+'pt');
				}else if(selectedType == "wordSpacing" && controlAttr){
					if(pointVal < 0.23 || pointVal > 0.43){
						popper.find('.controlVal').attr('data-error', 'Invalid Value');
						return false;
					}
					$(editNode).attr(controlAttr, pointVal+'w');
				}else if(selectedType == "verticalJustification" && controlAttr){
					$(editNode).attr(controlAttr, pointVal);
				}else if(selectedType == "splCharacterStyle"){
					if(pointVal){
						var currSelection = $.parseHTML("<span>"+kriya.selection.getHTMLContents()+"</span>");
						$(currSelection).attr(controlAttr,pointVal);
						kriya.selection.pasteHtml(currSelection[0].outerHTML);
					}					
				}else if(selectedType == "splParagraphStyle"){					
					$(editNode).attr(controlAttr,pointVal);
				}else if(selectedType == "removeParaIndent"){					
					$(editNode).attr(controlAttr,'true');
				}
				
				kriyaEditor.settings.undoStack.push(editNode);
				kriyaEditor.init.addUndoLevel('add-proof-control');

				kriya.popUp.closePopUps(popper);
			},
			updatePDFProofControls: function(param, targetNode){
				console.log(param, targetNode);
				var id= $('#pdfProofControls').attr('data-id')
				if($('input.data-word-spacing:checked').val()!=0){
					$('#'+id).attr('data-word-spacing', $('input.data-word-spacing:checked').val()+'w');
				}else{
					$('#'+id).removeAttr('data-word-spacing');
				}
				if($('input.data-top-gap:checked').val()){
					$('#'+id).attr('data-top-gap', $('input.data-top-gap:checked').val());
				}
				kriyaEditor.init.save('#'+id);
				eventHandler.menu.insert.closeSpecialChar()
			},
			updateFloatProofControls: function(param, targetNode){
				console.log(param, targetNode);
				var id= $('#floatProofControls').attr('data-id')
				$('#floatProofControls input[type="radio"]:checked').each(function(e){
					$('#'+id).attr($(this).attr('class'), $(this).val());
				})
				$('#floatProofControls input[type="number"]').each(function(e){
					$('#'+id).attr($(this).attr('class'), $(this).val());
				})
				kriyaEditor.init.save('#'+id);
				eventHandler.menu.insert.closeSpecialChar()
			},
			removeProofControl : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var selectedType = popper.find('#proofControlAction').val();
				var controlAttr  = popper.find('#proofControlAction').find(':selected').attr('data-attr');

				var editNode = $(kriya.selection.startContainer).closest('p,div,table,h1,h2,h3,h4,h5,h6,ol,ul');
				var selectionNodes = kriya.selection.getNodes();
				if(selectedType == "forceJustify"){
					$(selectionNodes).filter('.forceJustify').remove();
				}else if(selectedType == "columnBreak"){
					$(selectionNodes).filter('.forceColBrk').remove();
				}else if(selectedType == "nbsp"){
					//Get the textNodes
					selectionNodes = $(selectionNodes).filter(function(){if(this.nodeType == 3)return this;});
					$(selectionNodes).each(function(){
						var nodeValue = this.nodeValue;
						if(nodeValue.match(/\u00A0/gi)){
							nodeValue = nodeValue.replace(/\u00A0/gi, '');
							this.nodeValue = nodeValue;
						}
					});
				}else if(selectedType == "splCharacterStyle"){
					if($(kriya.selection.startContainer).closest('span['+controlAttr+']').length){
						$(kriya.selection.startContainer).unwrap();
					}							
				}else if(controlAttr){
					$(editNode).removeAttr(controlAttr);
				}
				
				kriyaEditor.settings.undoStack.push(editNode);
				kriyaEditor.init.addUndoLevel('remove-proof-control');

				kriya.popUp.closePopUps(popper);
			},
			/**
			 * Function to add the editable row in the popup Ex: competing intrest
			 */
			addEditableRow : function(param, targetNode){
				
				var component = $(targetNode).closest('.carousel-item, [data-type="popUp"]');

				if (param.group == undefined) return;

				var group    = $('.collection[data-class="'+param.group+'"]');
				var idPrefix = group.attr('data-id-prefix');

				var selector       = component.find('.dropdown-link-content').attr('data-source-selector');
				var sourceSelector = kriya.xpath(selector);
				var sourceLength   = sourceSelector.length;
				var rid = idPrefix+(sourceLength+1);

				if (group.find('[data-template]').length > 0){
					var template = group.find('[data-template]')[0].cloneNode(true);
					template.removeAttribute('data-template');
				}else{
					var template = $('<li class="collection-item dismissable"></li>');
					$(template).html('<span data-input="true"></span>');
				}
				$(template).attr('data-rid',rid);
				$(template).find('[data-input="true"]').attr('contenteditable', 'true');
				$(group).append(template);
				$(group).find('[data-input="true"]').focus();
			},
			showDatePicker : function(param, targetNode){
				var dateClass = $(targetNode).attr('data-class');
				kriya.general.getDatePicker($(targetNode), {'name':dateClass});
			},
			removeDate: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var dateNode = $(targetNode).closest('[data-type="htmlComponent"]');
				if(dateNode.length > 0){
					var dataClass = dateNode.attr('data-class');
					//Remove date if it's newly added in popup
					if(dateNode.attr('data-new-date') == "true"){
						dateNode.remove();	
					}else{
						dateNode.html(''); //Remove the date values
						dateNode.attr('data-removed', 'true');
						dateNode.parent().append(dateNode); //move to end	
					}
					
					//Reorder the date versions
					var reorderNodes = popper.find('[data-class="' + dataClass + '"]:not([data-template]):not([data-removed])').not(dateNode);
					reorderNodes.each(function(i){
						var dateLabelNode = $(this).find('.dateLabel');
						var dateLabelText = dateLabelNode.text();
						dateLabelText = dateLabelText.replace(/(\d+)$/, '');
						dateLabelText = dateLabelText+(i+1);
						dateLabelNode.text(dateLabelText);
					});
				}
			},
			addDate: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				if(param && param.class){
					var templateDate = popper.find('[data-template="true"][data-class="' + param.class + '"]');
					if(templateDate.length > 0){
						var dateLength = popper.find('[data-class="' + param.class + '"]:not([data-template]):not([data-removed])').length;
						var clonedDate = templateDate.clone(true);
						clonedDate.removeAttr('data-template');
						clonedDate.attr('data-new-date', 'true');
						clonedDate.attr('data-clone', 'true');
						clonedDate.attr('data-save-type', 'saveDate');
						templateDate.parent().append(clonedDate);
						if(param.label){
							var dateLabel = param.label+(dateLength+1);
							clonedDate.find('.dateLabel').text(dateLabel);
						}
					}
				}
			},
			editEquation : function(param, targetNode){
				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				var latex = $(kriya.config.activeElements).attr('data-tex');
				if(!latex){
					return false;
				}
				kriya.config.editEqNode = kriya.config.activeElements;
				var mathType = 'inline';
            	if($(kriya.config.editEqNode).closest('.jrnlEqnPara').length > 0){
					mathType = 'display';
				}
				latex  = latex.replace(/%/g, '|PERCENTAGE|');
				latex  = latex.replace(/;/g, '|SEMICOLON|');
				latex  = latex.replace(/#/g, '|HASH|');
				latex  = latex.replace(/@/g, '|AT|');
				latex  = latex.replace(/&/g, '|AMP|');
				this.openEquationEditor({'latex':latex, 'mathType' : mathType});
			},
			openEquationEditor : function(param, targetNode){
				var latex = "";
				if(param && param.latex){
					latex = param.latex;
				}
				var url = '/equation_editor/?codeType=Latex&encloseAllFormula=false&style=aguas&localType=en_US&equation='+latex+'&mathType='+param.mathType+'&customer='+kriya.config.content.customer+'&journal='+kriya.config.content.project;
				eqnWin = window.open(url, 'Equation Editor', 'height=500, width=1000');
			},
			gotoFloat: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var floatXpath = $(targetNode).closest('[data-type="getLinkedCitations"][data-clone="true"]').attr('data-node-xpath');
				var scrollTop = $(kriya.config.containerElm).scrollTop();
				if(floatXpath){
					var floatnode = kriya.xpath(floatXpath);
					if(floatnode.length > 0){
						if ($(floatnode[0]).closest('[class$="Block"]').length > 0 && $(floatnode[0]).closest('.jrnlTblFoot').length == 0){
							floatXpath = kriya.getElementXPath($(floatnode[0]).closest('[class$="Block"]'));
							floatnode = $(floatnode[0]).closest('[class$="Block"]');
						}
						$(kriya.config.containerElm).scrollTo(floatnode[0]);
						if (floatnode[0].nodeName == 'P'){
							range = rangy.getSelection();
							sel = range.getRangeAt(0);
							sel.setStart(floatnode[0], 0);
							sel.setEndAfter(floatnode[0]);
							var h = findReplace.general.highlight(range, 'glow-element');
							range.removeAllRanges();
							setTimeout(function(){
								$(h).remove();
							},'3000	')
						}
						$('.getBack').remove();
						var arrowType = 'arrow_drop_up';
						if (scrollTop > $(kriya.config.containerElm).scrollTop()){
							arrowType = 'arrow_drop_down';
						}
						var getBack = $('<span class="getBack" style="top:' + $(kriya.config.containerElm).offset().top + '" onclick="$(\''+kriya.config.containerElm+'\').scrollTop('+scrollTop+');$(\'.getBack\').remove();"><i class="material-icons">' + arrowType + '</i></span>');
						getBack.attr('data-float-xpath', floatXpath);
						$('body').append(getBack);
					}
				}
			},
			gotoLink: function(param, targetNode){
				var urlLink = $(targetNode).closest('[data-type="popUp"]').find('[data-class="href"]').html();
				if(urlLink!=undefined && urlLink!=""){
					var div = document.createElement('div');
					div.innerHTML = urlLink;
					var decoded = div.firstChild.nodeValue;
					window.open(decoded, '_blank');
				}
			},
			showImageVersions: function(param, targetNode){
				var component = $('[data-component="imageVersion"]');
				component.removeClass('hidden').attr('style', 'max-width: 70% !important');
				component.find('.version-section').html('')
				if (param && param.id){
					targetNode = $('#' + param.id);
				}
				$(targetNode).closest('.jrnlFigBlock[data-id]').find('img.jrnlFigure').each(function(){
					clonedImage = $('<span class="img-version" style="margin:5px;"><img/></span>');
					clonedImage.find('img').wrap('<span class=jrnlVersionImg data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'selectImageVersion\'}"></span>');
					if ($(this)[0].hasAttribute('data-old-src')){
						if (! /pushing-pixels/.test($(this).attr('data-old-src'))){
							clonedImage.find('img').attr('src', $(this).attr('data-old-src'));
						}else{
							clonedImage = false;
						}
					}else{
						clonedImage.find('img').attr('src', $(this).attr('src')).attr('class', 'selected');
					}
					if (clonedImage){												
						if ($(this)[0].hasAttribute('data-original-name')&&$(this)[0].hasAttribute('data-primary-extension')){
							clonedImage.find('.jrnlVersionImg').append('<br><span class="jrnlOriginalName">'+$(this).attr('data-original-name')+'.'+$(this).attr('data-primary-extension')+'</span>');
						}
						if ($(this)[0].hasAttribute('title')){
							clonedImage.append('<br>' + $(this).attr('title'));
						}
						component.find('.version-section').append(clonedImage)
					}
				})
			},
			selectImageVersion: function(param, targetNode){
				$(targetNode).closest('.version-section').find('.selected').removeClass('selected');
				$(targetNode).addClass('selected');
			},
			changeProofControlType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var selectedType = $(targetNode).val();
				var controlAttr  = $(targetNode).find(':selected').attr('data-attr');
				popper.find('.splStyle').val('');
				popper.find('.controlVal').val('');
				if(selectedType == "forceJustify" || selectedType == "columnBreak" || selectedType == "nbsp"){
					popper.find('.controlVal').addClass('hidden');
				}else{
					popper.find('.controlVal').removeClass('hidden');
				}
				if(selectedType == "splCharacterStyle"||selectedType == "splParagraphStyle"||selectedType=="removeParaIndent"){					
					popper.find('.controlVal').addClass('hidden');
					popper.find('.splStyle').removeClass('hidden');
				}
				else{
					popper.find('.splStyle').addClass('hidden');
					popper.find('.controlVal ').removeClass('hidden');
				}
				if(selectedType=="removeParaIndent"){
					popper.find('.splStyle').addClass('disabled');
				}else{
					popper.find('.splStyle').removeClass('disabled');
				}
				var startContainer = kriya.selection.startContainer;				
				if(selectedType == "wordSpacing"){
					popper.find('.controlVal').attr('step', '0.01').attr('min', '0.23').attr('max', '0.43');
				}else{
					popper.find('.controlVal').removeAttr('step').removeAttr('min').removeAttr('max');
				}

				if(selectedType == "margin"){
					popper.find('.controlVal').attr('step', '0.1').attr('min', '-6').attr('max', '12');
				}else{
					popper.find('.controlVal').removeAttr('step').removeAttr('min').removeAttr('max');
				}

				if(controlAttr){
					var val = $(startContainer).closest('[' + controlAttr + ']').attr(controlAttr);
					val = parseFloat(val);										
					if(selectedType == "splCharacterStyle"){
						val = $(startContainer).closest('span[' + controlAttr + ']').attr(controlAttr);						
					}else if(selectedType == "splParagraphStyle"){
						val = $(startContainer).closest('p[' + controlAttr + ']').attr(controlAttr);
					}else if(selectedType == "removeParaIndent"){
						if($(startContainer).closest('[' + controlAttr + ']').length){
							val = "Indent removed for this paragraph";
						}						
					}
					if(val){
						if(controlAttr=="data-spl-style" || controlAttr=="data-no-indent"){
							popper.find('.splStyle').val(val)
						}
						else{
							popper.find('.controlVal').val(val);
						}						
					}
				}

			},
			changeRelArtType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var selectedType = $(targetNode).val();
				if(selectedType){
					var visibleNode = popper.find('[data-rel-art-type]:visible');
					visibleNode.find('[data-type="htmlComponent"][data-class="jrnlRelArtAuthor"]:not([data-template="true"])').remove();
					visibleNode.find('[data-type="htmlComponent"][type="text"]:not([data-template="true"])').html('');
					
					popper.find('[data-rel-art-type]').addClass('hidden');
					var placeNode = popper.find('[data-rel-art-type="' + selectedType + '"]');
					placeNode.removeClass('hidden');
				}
			},
			changeCopyRightType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var selectedType    = $(targetNode).attr('id');
				var licenseTemplate = popper.find('.licenseTemplate[data-template="true"] [license-type="' + selectedType + '"]').html();
				licenseTemplate = (licenseTemplate)?licenseTemplate:'';
				popper.find('[data-class="jrnlLicense"]').html(licenseTemplate);
				//this will be update copyright statement in edit copyright popup when we change permission type radio buttons at that time copyright statement will update automatically.
				var copyRightTemplate = popper.find('.licenseTemplate[data-template="true"] [copyRight-type="' + selectedType + '"]').html();
				if(copyRightTemplate){
					var copyRightYear = popper.find('[data-class="jrnlCopyrightYear"]').val()? popper.find('[data-class="jrnlCopyrightYear"]').val():'';
					if(copyRightYear){
						copyRightTemplate = copyRightTemplate.replace(/\[year\]|\[year\]|\b[0-9]{4}\b/,copyRightYear);
					}
					popper.find('[data-class="jrnlCopyrightStmt"]').html(copyRightTemplate);
				}
			},
			changeObjectType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var objectType = $(targetNode).val();
				var objectClass =  $(targetNode).find(':selected').attr('data-object-class');
				var objectFileClass = $(targetNode).find(':selected').attr('data-img-class');
				var removeLabel = $(targetNode).find(':selected').attr('data-remove-label');
				var tempSelector = $(targetNode).find(':selected').attr('data-tmp-selector');
				//popper.find('[data-object-type]').filter('[data-object-type != ' + objectType + ']').html('');
				popper.attr('data-class', objectClass);
				// to handle box without label -aravind
				if(removeLabel){
					popper.attr('data-remove-label', removeLabel);
					popper.attr('data-tmp-selector', tempSelector);
				}else{
					popper.removeAttr('data-remove-label');
					popper.removeAttr('data-tmp-selector');
				}
				if(popper && (popper.attr('data-class') == "jrnlBoxBlock" || popper.attr('data-class') == "jrnlFigBlock")){
					if(removeLabel){
						if($(popper).find('[data-object-type="display"] [data-selector]').length > 0){
							$(popper).find('[data-object-type="display"] [data-selector]').attr('data-save-type','false');
							$(popper).find('[data-object-type="display-withoutlabel"] [data-selector]').removeAttr('data-save-type');
						}
					}else{
						if($(popper).find('[data-object-type="display-withoutlabel"] [data-selector]').length > 0){
							$(popper).find('[data-object-type="display-withoutlabel"] [data-selector]').attr('data-save-type','false');
							$(popper).find('[data-object-type="display"] [data-selector]').removeAttr('data-save-type');
						}
					}
				}
				// Start - Dhatshayani . D Jan 05, 2018 - change object file class based on object type.
				if(objectFileClass && objectFileClass != '' && popper.find('.fileList').find('*[type="objFile"]').length > 0){
					popper.find('.fileList').find('*[type="objFile"]').attr('data-class', objectFileClass);
					var checkAttr = 'data-selector-' +objectFileClass;
					if(popper.find('.fileList').find('*[type="objFile"]').attr('data-selector') && popper.find('.fileList').find('*[type="objFile"]').attr(checkAttr)){
						var selectorChangeTo = popper.find('.fileList').find('*[type="objFile"]').attr(checkAttr);
						popper.find('.fileList').find('*[type="objFile"]').attr('data-selector',selectorChangeTo);
					}// End Change class of object file.
				}
			},
			changeFloatOfType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var val    = $(targetNode).val();
				var captionClass = popper.find('[data-class$="Caption"]').attr('data-class');
				var floatClass = popper.attr('data-class');
				var templateNode = $('[data-element-template="true"] [tmp-class="' + captionClass + '"]');
				var labelId = templateNode.attr('tmp-data-id');
				labelId = labelId.replace(/\$position/g, '');
				if(val && templateNode.length > 0 && templateNode.attr('tmp-data-id')){
					var blockNode  = $(kriya.config.containerElm + ' [data-id="' + val + '"]');
					
					var lastElements = blockNode.find('.'+ floatClass).last();
					var uncitedFloatLen = blockNode.find('.'+ floatClass +'[data-uncited]').length;
					labelId = val + '-' + labelId;
				}else{
					var lastElements = $(kriya.config.containerElm + ' .' + floatClass).last();
					var uncitedFloatLen = $(kriya.config.containerElm + ' .' + floatClass + '[data-uncited]').length ;
				}
				
				var lastIdNum = 0;
				if(lastElements.length > 0){
					var lastElementId = lastElements.attr('data-id');
					if(lastElementId && lastElementId.match(/(\d+)$/g)){
						lastIdNum = parseInt(lastElementId.match(/(\d+)$/g)[0]);
					}
				}
				labelId = labelId + (lastIdNum+1);

				var newFloatLabelString = citeJS.floats.getCitationHTML([labelId], [], 'renumberFloats');
				newFloatLabelString =  newFloatLabelString.replace(/(\d+)([\.\s])$/g, (uncitedFloatLen+1) + '$2');

				newFloatLabelString =  'Uncited ' + newFloatLabelString;
				popper.find('[data-class$="Caption"]').find('.label').attr('data-block-id', labelId);
				popper.find('[data-class$="Caption"]').find('.label').html(newFloatLabelString);
			},
			changeDateType: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				if($(targetNode).is(':checked') && param && param.className){
					popper.attr('data-class', param.className);
					popper.find('.addDatepick').attr('data-class', param.className);
				}
			},
			changeCiteTab: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				if ($(targetNode).parent('.citeHolder').hasClass('for-deletion')){
					$(targetNode).toggleClass('active');
				}else if (!$(targetNode).hasClass('active')){
					$('.citeHolder .cite.active').removeClass('active');
					$(targetNode).addClass('active');
					var citeID = $(targetNode).attr('data-cite-id');
					$(targetNode).closest('[data-type][data-component]').find('[data-type="getLinkedCitations"][data-clone]').addClass('hidden')
					$(targetNode).closest('[data-type][data-component]').find('[data-type="getLinkedCitations"][data-clone][data-cite="'+ citeID +'"]').removeClass('hidden')
				}
				/*var popHeight = $('[data-component="jrnlBibRef"]').outerHeight();
				var citeNodeTop = $('.activeElement').offset().top;
				popper.css('top', Math.abs(popHeight-citeNodeTop-10));*/
			},
      filterHistoryChange: function(param, targetNode){
				var popper    = $(targetNode).closest('[data-component]');
				var labelNode = $(targetNode).closest('label');
				var labelFor  = labelNode.attr('for');
				var field     = popper.find('#'+labelFor);
				if(field.length == 0){
					field = ( $(labelNode).next('[data-selector]').length > 0 )?$(labelNode).next('[data-selector]'):( $(labelNode).prev('[data-selector]').length > 0 )?$(labelNode).prev('[data-selector]'):'';
				}
				/**
				 * If the filed is already filtered then remove the filter
				 * else filter the history card for the field
				 */
				if($(targetNode).hasClass('filtered')){
					popper.find('.historyTab .historyCard').removeClass('hide');
					$(targetNode).removeClass('filtered');
				}else{
					var selector = field.attr('data-node-xpath');
					var dataNode = kriya.xpath(selector);
					if(dataNode && $(dataNode).length > 0){
						var dataId = $(dataNode).attr('id');
						popper.find('.historyTab .historyCard').addClass('hide');
						popper.find('.historyTab .historyCard[data-h-id="' + dataId + '"]').removeClass('hide');
						popper.find('.filtered.fieldChange').removeClass('filtered');
						$(targetNode).addClass('filtered');
					}
				}
			},
			hightlightField: function(param, targetNode) {
				var popper = $(targetNode).closest('[data-component]');
				var card = $(targetNode).closest('.historyCard');
				if (card.length > 0) {
					var hId = $(card).attr('data-h-id');
					var field;
					if ($(popper).attr('data-component') != "jrnlRefCsl_edit") {
						field = popper.find('[data-node-xpath*="' + hId + '"]').closest('.input-field');
					} else {
						eventHandler.components.PopUp.getCslMappingObj(function (status) {
							if (status) {
								var mappingObj = kriya.config.cslMappingObj;
								var contId = $(card).attr('data-content-id');
								var fieldClass;
								if ($(popper).find('.cslReferenceOutput > p').length) {
									fieldClass = $(popper).find('.cslReferenceOutput > p [data-track-id=' + hId + ']').closest('[class]:not(.del, .ins)').attr('class');
								} else {
									fieldClass = $('.jrnlRefGroup #' + contId).find('[data-track-id=' + hId + ']').closest('[class]:not(.del, .ins)').attr('class')
								}
								var refType = $('[data-component="jrnlRefCsl_edit"]').find(':selected').val();
								if (mappingObj['kriyaRefTypeToCslType'][refType] != undefined) {
									refType = mappingObj['kriyaRefTypeToCslType'][refType];
								}
								var objEle = mappingObj['kriyaClassToUi'][refType];
								if ($(popper).find('option:selected[value="' + objEle[fieldClass] + '"]').length > 0) {
									field = $(popper).find('option:selected[value="' + objEle[fieldClass] + '"]').closest('.row').find('.text-line');
									if (field && $(field).length > 1) {
										field = $(popper).find('select[data-track-id=' + hId + ']').closest('.row').find('.text-line');
									}
								}
							}
						});
					}
					if (field) {
						field.addClass('yellow');
						setTimeout(function () {
							field.removeClass('yellow');
						}, 3000);
					}
				}
			},
			/*Start - Issue ID - #113 - Dhatshayani . D - Nov 25, 2017 - To add attribute "data-tmp-selector" in the compoenet to select the appropriate template of footnote */
			addDataTempSelector : function(param, targetNode){
				var componentType = param.component + '_edit';
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				var fnType = '';
				if(targetNode[0].hasAttribute('class') && $(targetNode).find('.'+param.component).length > 0){
					var htmlElement = $(targetNode).find('.'+param.component);
					$(targetNode).find('.'+param.component).find('.jrnlFNPara:empty').html('<br>');
					//add break tags to para's when it has empty text and query inside it - Jai
					$(targetNode).find('.'+param.component).find('.jrnlFNPara').each(function(){
						if ($(this).text().trim() == '' && $(this).find('br').length == 0){
							$(this).append('<br>');
						}
					})
					$(targetNode).find('.'+param.component).find('.jrnlFNPara').attr('contenteditable','true');
					if($(targetNode).find('.'+param.component).length > 0 && $(targetNode).find('.'+param.component)[0].hasAttribute('add-data-inserted')){
						$(targetNode).find('.'+param.component).attr('data-inserted','true');
					}
					if(htmlElement.length > 0){
						kriya.evt.target = htmlElement[0];
					}
					eventHandler.components.general.contentEvt(param, htmlElement);
				}else if(targetNode[0].hasAttribute('class') && $(targetNode).attr('class').split(' ')[0] == param.component){
					var htmlElement = $(targetNode).find('.'+param.component);
					htmlElement = (htmlElement.length > 0)?htmlElement:$(targetNode);
					$(targetNode).find('.jrnlFNPara:empty').html('<br>');
					//add break tags to para's when it has empty text and query inside it - Jai
					$(targetNode).find('.jrnlFNPara').each(function(){
						if ($(this).text().trim() == '' && $(this).find('br').length == 0){
							$(this).append('<br>');
						}
					})
					$(targetNode).find('.jrnlFNPara').attr('contenteditable','true');
					if($(targetNode)[0].hasAttribute('add-data-inserted')){
						$(targetNode).attr('data-inserted','true');
					}
					if(htmlElement.length > 0){
						kriya.evt.target = htmlElement[0];
					}
					eventHandler.components.general.contentEvt(param, htmlElement);
				}else{
					if(targetNode[0].hasAttribute('data-tmp-selector')){
						var dataTmpSelector = $(targetNode).attr('data-tmp-selector');
						fnType = $(targetNode).attr('data-fn-type');
						if(component.length > 0){
							$(component).attr('data-tmp-selector',dataTmpSelector);
						}
					}
					else if(targetNode.parent().attr('data-fn-type') && targetNode.parent().attr('data-fn-type')!=''){
						fnType = targetNode.parent().attr('data-fn-type');
						var componentDataClass = $("*[data-type='popUp'][data-component='" + componentType + "']").attr('data-class');
						var dataTmpSelector = "//*[@data-element-template='true']//*[@tmp-class='"+componentDataClass+"'][@tmp-data-fn-type='"+fnType+"']"
						if(component.length > 0){
							$(component).attr('data-tmp-selector',dataTmpSelector);
						}
					}
					if(component.length > 0 && $(component).find('*[data-selector]').length > 0 && fnType && fnType!=''){
						var fnTypeNodes = $(component).find('*[data-selector]');
						fnTypeNodes.each(function(index,object){
							kriya.popUp.htmlComponent($(this)[0], targetNode);
						});
						
						if(targetNode[0].hasAttribute('data-xpath')){
							$(component).attr('data-node-xpath',$(targetNode).attr('data-xpath'));
						}
					}
				}
			},
			/* Function to remove "add foot note" button once foot note added */
			removeFootNoteBtn : function(dataToSave){
				if($('div[data-component]:visible').length > 0 && $('div[data-component]:visible')[0].hasAttribute('data-node-xpath') && $('div[data-component]:visible').attr('data-node-xpath')!=''){
					var nodeXpath = $('div[data-component]:visible').attr('data-node-xpath');
					var elementClass = $('div[data-component]:visible').attr('data-class');
					newNodeXpath = kriya.xpath(nodeXpath);
					var targetElement = $(newNodeXpath);
					var fnTYpe = $(targetElement).attr('data-fn-type');
					$(targetElement).find('.contentBtn').remove();
					if($(targetElement).nextAll('.jrnlAddFNPara:first').length > 0){
						$(targetElement).nextAll('.jrnlAddFNPara:first').remove();
					}
					if(elementClass && elementClass!=''){
						var fnDivNode = $(targetElement).next('.'+elementClass);
						if(fnDivNode.length > 0){
							if(targetElement[0].hasAttribute('data-remove-btn') && $(targetElement).attr('data-remove-btn') == 'true'){
								$(targetElement).remove();
							}
						}else if($(targetElement) && $(targetElement).hasClass(elementClass) && ($(targetElement).children().length == 0 || ($(targetElement).find(' > .jrnlFNText >.jrnlFNPara').length == 0))){
							if(fnTYpe && fnTYpe!=''){
								var templateNodePath = "//*[@data-element-template='true']//*[@tmp-class='"+elementClass+"'][@tmp-data-fn-type='"+fnTYpe+"']";
								var templateNode = kriya.xpath(templateNodePath);
								if(templateNode.length > 0 ){
									var existNode = $(targetElement).children()[0];
									var existNodeClass = $(existNode).attr('class');
									var childTempNodes = $(templateNode).children();
									childTempNodes.each(function(index,object){
										if((!existNodeClass || (this.hasAttribute('tmp-class') && $(this).attr('tmp-class') != existNodeClass)) || !this.hasAttribute('tmp-class')){
											var clonedNode = $(this).clone();
											if($(clonedNode)[0].hasAttribute('tmp-class')){
												$(clonedNode).attr('class',$(clonedNode).attr('tmp-class'));
												$(clonedNode).removeAttr('tmp-class');
											}
											$(targetElement).find('.jrnlFNText').append(clonedNode);
										}
									});
								}
							}
						}
					}
					
					if(dataToSave.length > 0){
						if($(dataToSave)[0].hasAttribute('add-data-inserted') || $(dataToSave)[0].hasAttribute('data-inserted') ){
							dataToSave.kriyaTracker('ins');
							dataToSave.attr('id', uuid.v4());
						}
						dataToSave.each(function(index,object){
							if(this.hasAttribute('data-edited-node') && this.getAttribute('data-edited-node') == 'true'){
								$(this).parent('div[add-edited-node]').attr('data-edited-node','true');
								if($(this).closest('div[add-edited-node]').length > 0 && $(this).closest('div[add-edited-node]')[0].hasAttribute('data-track')){
									$(this).closest('div[add-edited-node]').removeAttr('data-track');
								}
							}
							if(this.hasAttribute('add-data-inserted')){
								if($(this).closest('div[add-edited-node]').length > 0 && !this.hasAttribute('data-edited-node')){
									$(this).closest('div[add-edited-node]').attr('data-track','ins');
								}
								this.removeAttribute('add-data-inserted');
								$(this).find('*[add-data-inserted]').removeAttr('add-data-inserted');
							}
							/* Set the data label as per customer specification */
							/*if(this.parentNode && this.hasAttribute('data-label') && this.getAttribute('data-label') != ''){
								$(this).parent().find('.label').html(this.getAttribute('data-label'));
							}*/
							
							if($(this).find('p').find('br').length > 0){
								$(this).find('p').find('br').remove();
							}
						});
					}
				}
			},
			/*End Issue ID - #113 */
			/**
			* Function to manipulate multiple para in foot note component on enter, delete and paste.
			*/
			createNewElement : function(param, targetNode){
				//event will not support directly in mozilla browser-kirankumar
				var event = kriya.evt;
				if(event && event.keyCode == 13){
					if(param && param.eleName){
						var currentNode = event.target;
						if(currentNode && currentNode.nodeType==1 && $(currentNode).hasClass(param.eleClass)){
							$(currentNode).find('br').remove();
							if(currentNode.hasAttribute('id') && $(targetNode).find('*[id="'+currentNode.getAttribute('id')+'"]').length > 1){
								var duplicateIdNodes = $(targetNode).find('*[id="'+currentNode.getAttribute('id')+'"]');
								duplicateIdNodes.each(function(index,object){
									if(index > 0){
										var newId = uuid.v4();
										$(this).removeAttr('id');
										$(this).attr('id',newId);
									}
								});
							}else{
								var newNodeHTML = $(currentNode).find('div').html();
								var newNode = document.createElement(param.eleName);
								newNode.setAttribute('class',param.eleClass);
								var newId = uuid.v4();
								newNode.setAttribute('id',newId);
								newNode.setAttribute('contenteditable','true');
								if($(currentNode).find('div').length > 0){
									$(currentNode).find('div').remove();
									$(newNode).html(newNodeHTML);
								}
								$(currentNode).after(newNode);
								$(newNode).focus();
							}
						}else{
							var newNodes = $(targetNode).find('.'+param.eleClass);
							newNodes.each(function(index,object){
								if(this.hasAttribute('id') && $(targetNode).find('*[id="'+this.getAttribute('id')+'"]').length > 1){
									var duplicateIdNodes = $(targetNode).find('*[id="'+this.getAttribute('id')+'"]');
									duplicateIdNodes.each(function(index,object){
										if(index > 0){
											var newId = uuid.v4();
											$(this).removeAttr('id');
											$(this).attr('id',newId);
											$(this).attr('contenteditable','true');
										}
									});
								}
							});
						}
					}
				}else if(event && (event.keyCode == 8 || event.keyCode == 46) && param && param.eleClass && param.eleClass != '' && targetNode && targetNode.length > 0 && $(targetNode).find('.'+param.eleClass).length == 0){
					$(targetNode).find('br').remove();
					var newNode = document.createElement(param.eleName);
					$(newNode).attr('class',param.eleClass);
					var newId = uuid.v4();
					$(newNode).attr('id',newId);
					if($(targetNode)[0].hasAttribute('data-node-xpath') && $(targetNode).attr('data-node-xpath') != ''){
						var activeNode = kriya.xpath($(targetNode).attr('data-node-xpath'));
						$(newNode).attr('contenteditable','true');
					}
					$(newNode).html('<br>');
					$(targetNode).append(newNode);
				}else if(event && (event.keyCode == 8 || event.keyCode == 46) && param && param.eleClass && param.eleClass != '' && targetNode && targetNode.length > 0 && $(targetNode).find('.'+param.eleClass).length > 1){
					$(event.target).find('br').remove();
					var prevElement = $(event.target).prev();
					var nextElement = $(event.target).next();
					if(event.target && event.target.nodeType == 1 && $(event.target).hasClass(param.eleClass) && $(event.target).is(':empty')){
						$(event.target).remove();
						if(event.keyCode == 46 && nextElement && nextElement.length > 0){
							$(nextElement).focus();
						}else if(event.keyCode == 8){
							if(prevElement && prevElement.length > 0){
								$(prevElement).focus();
								eventHandler.components.PopUp.setFocusAtEnd(prevElement[0]);
							}else if(nextElement && nextElement.length > 0){
								$(nextElement).focus();
							}
						}
					}else{
						var sel = rangy.getSelection();
						var selectedFocusOffSet = sel.focusOffset;
						if(event.keyCode == 8 && selectedFocusOffSet == 0 && prevElement && prevElement.length > 0){
							$(prevElement).focus();
							eventHandler.components.PopUp.setFocusAtEnd(prevElement[0]);
						}else if(event.keyCode == 46 && selectedFocusOffSet && selectedFocusOffSet == $(event.target).text().length && nextElement && nextElement.length > 0){
							$(nextElement).focus();
						}
					}
				}else if(event && event.keyCode == 17 && param && param.eleClass && param.eleClass != '' && param.eleName && param.eleName != '' && targetNode && targetNode.length > 0){
					if($(targetNode)[0].hasAttribute('data-node-xpath') && $(targetNode).attr('data-node-xpath') != ''){
						var activeNode = kriya.xpath($(targetNode).attr('data-node-xpath'));
						var targetNodeContents = $(targetNode).contents();
						var newNode = '';
						targetNodeContents.each(function(index,object){
							if($(this).prop('nodeName') && $(this).prop('nodeName').toLowerCase() == param.eleName){
								newNode = '';
							}else{
								if(newNode == '' && (this.nodeName && this.nodeName != param.eleName)){
									newNode = document.createElement(param.eleName);
									$(newNode).attr('class',param.eleClass);
									$(this).before(newNode);
								}
								if(newNode && newNode!=''){
									$(newNode).append(this);
								}
							}
						});
						var popuChildNodes = $(targetNode).find(param.eleName);
						popuChildNodes.each(function(index,object){
							$(this).attr('class',param.eleClass);
							$(this).attr('contenteditable','true');
							if(activeNode && $(activeNode)[0].hasAttribute('data-inserted')){
								$(this).attr('data-inserted','true');
							}
						});
					}
				}
			},
			setFocusAtEnd: function(elem){
				var elemLen = $(elem).text().length;
				setCursorPosition(elem,elemLen);
			},
			/** Start Issue Id - #173 - Function to data out of parent and tag following contents as seperate node with parent style. 
			*   e.g - <p>Test data <table/> following test data</p> => <p>Test data</p><table/><p>following test data</p>
			*/
			splitAndTagSibling : function(dataToSave){
				dataToSave.each(function(index,object){
					var closestElementName = '';
					closestElementName = 'p, h1, h2, h3, h4, h5, h6, ol, ul';
					if(this.hasAttribute('data-split-assibling') && this.getAttribute('data-split-assibling') == 'true' && (closestElementName && closestElementName!='' && $(this).closest(closestElementName).length > 0)){
						var prevNode = this.previousSibling;
						var closestNode = $(this).closest(closestElementName)[0];
						if(prevNode && prevNode!=''){
							var newParaNode = '';
							var closestNodeAttrs = closestNode.attributes;
							newParaNode = document.createElement(closestNode.nodeName);
							$.each(closestNodeAttrs,function(){
								if(this.name !='id'){
									newParaNode.setAttribute(this.name,this.value);
								}
							});
							var id = uuid.v4();
							newParaNode.setAttribute('id',id);
							newParaNode.setAttribute('data-inserted','true');
							$(closestNode).after(newParaNode);
							var parentNodes = $(this).parentsUntil(closestNode.nodeName);
							var newInsertedElement = this;
							var nodeId = $(newInsertedElement).attr('id');
							// Traverse upwards and split data until the specified parent
							parentNodes.each(function(index,object){
								var targetElement = this;
								var nodeContents = $(targetElement).contents(); var createParentNode = false;
								if(targetElement && targetElement.nodeName && targetElement.nodeName != closestNode.nodeName && $(this).closest(closestNode.nodeName).length > 0){
									createParentNode = true;
								}
								// create a copy of the parent "nodeToSplit", append following sibling of "newInsertedElement" in the newNode(nodeToSplit) and insert the "nodeToSplit" element after the current parent. Then insert the "newInsertedElement" after the current.
								//*  Where it will look like "<p><b>hasdfhzdf<table/>ochvo<i>dshfld</i></b></p>" => "<p><b>hasdfhzdf</b><table/><b>ochvo<i>dshfld</i></b></p>"
								if(createParentNode && nodeContents.length > 0){
									var nodeToSplit = document.createElement($(targetElement).prop('nodeName').toLowerCase());
									var parentAttrs = targetElement.attributes;
									$.each(parentAttrs, function(){ 
										if(this.name != 'id'){
											$(nodeToSplit).attr(this.name,this.value);
										}
									});
									nodeContents.each(function(index,object){
										if($(this).prev('#'+nodeId).length > 0){
											$(nodeToSplit).append($(this));
										}
									});
									$(targetElement).after($(nodeToSplit));
									$(nodeToSplit).removeClass('activeElement');
								}
								$(targetElement).after($(newInsertedElement));
							});
							
							// append following sibling of inserted data into newly created element and push the newParaNode and actual para node into undoStack
							if(newInsertedElement && newParaNode){
								var nodeContents = $(closestNode).contents();
								nodeContents.each(function(index,object){
									if($(this).prev('#'+nodeId).length > 0){
										$(newParaNode).append($(this));
									}
								});
								$(closestNode).after($(newInsertedElement));
								// Push modified data into undoStack 
								kriyaEditor.settings.undoStack.push($(closestNode));
								kriyaEditor.settings.undoStack.push($(newInsertedElement));
								if($(newParaNode)){
									kriyaEditor.settings.undoStack.push($(newParaNode));
								}
							}
						}else{
							$(closestNode).before(this);
						}
						this.removeAttribute('data-split-assibling');
					}
				});
			},/* End Issue Id - #173*/
			changeSelectedValue : function(param, targetNode){
				//event will not support directly in mozilla browser-kirankumar
				var event = kriya.evt;
				if(event && $(event.target).length > 0){
					var selectedText = $(event.target).text();
					if($(event.target)[0].hasAttribute('data-value')){
						selectedText = $(event.target)[0].getAttribute('data-value');
					}
					if($(event.target)[0].hasAttribute('data-remove-data')){
						selectedText = '<span class="deletedNode">' + selectedText + '</span>';
					}
					
					if(targetNode && targetNode.length > 0 && selectedText && selectedText!=''){
						var textLineDiv = $(targetNode).prev('.text-line').html(selectedText).attr('data-focusout-data',selectedText);
						$(targetNode).removeAttr('data-display');
					}
				}
			},
			showDropDownList: function(param, targetNode){
				//event will not support directly in mozilla browser-kirankumar
				var event = kriya.evt;
				if(event && $(event.target).length > 0){
					var nodeClass = $(targetNode).attr('data-class');
					var optionsName = nodeClass + '_options';
					if($(targetNode).closest('.input-field').find('*[data-name="'+optionsName+'"]').length > 0){
						$(targetNode).closest('.input-field').find('*[data-name="'+optionsName+'"]').attr('data-display','true');
					}
				}
			}
		},
		table: {
			/* toBeDiscussed : about 'jb' */
			openTableSetter : function(param, targetNode){
				if($('[data-name="Tblsetter"]').attr('data-lang-trans') && (table = eventHandler.langTrans.langTransTables())){
					eventHandler.langTrans.langTrans(table);
				}
				else{
					if($(targetNode).closest('.jrnlTblBlock').length > 0){
						moveCursor($(targetNode).closest('.jrnlTblBlock').find('td:first,th:first')[0], 0);
						var range = rangy.getSelection();
						kriya.selection = range.getRangeAt(0);
					}					
					//var tblWin  = window.open('/table_setter/?customer='+kriya.config.content.customer+'&journal='+kriya.config.content.project, '');
					var tblWin  = window.open('/table_setter/?customer='+kriya.config.content.customer+'&journal='+kriya.config.content.project+'&doi='+kriya.config.content.doi , "_blank", "width=5000,height=5000");
				}
                
			},
			deleteInlineTable : function(){
				if($(editor.composer.tableSelection.table).length > 0 && $(editor.composer.tableSelection.table).closest('.jrnlTblBlock').length < 1){
					$(editor.composer.tableSelection.table).remove();
				}
			},
			/*#174 -logic modified for table popup while changing rows and columns -*Prabakaran A-Focusteam*/
            addColumnTemplate: function(param, targetNode) {
                var value = $(targetNode).val();
                var popper = $(targetNode).closest('[data-component]');
                if (popper.find('.tableClone table  tr:first td').length >= parseInt(value) || popper.find('.tableClone table  tr:first th').length >= parseInt(value)) {
                    popper.find('.tableClone table tr').each(function() {
                        if ($(this).find('th').length > 0)
                            for (var i = $(this).find('th').length; i > 0; i--) {
                                if (parseInt(value) < $(this).find('th').length)
                                    $(this).find('th:last').remove();
                                else
                                    break;
                            }
                        if ($(this).find('td').length > 0)
                            for (var i = $(this).find('td').length; i > 0; i--) {
                                if (parseInt(value) < $(this).find('td').length)
                                    $(this).find('td:last').remove();
                                else
                                    break;
                            }
                    });
                } else {
                    popper.find('.tableClone table tr').each(function() {
                        if ($(this).find('td').length > 0) {
                            for (var i = (parseInt(value) - $(this).find('td').length); i > 0; i--) {
                                var cloneColumn = $(this).find('td').last()[0].cloneNode(true);
                                if ($(cloneColumn).find('p').length > 0) {
                                    $(cloneColumn).find('p')[0].innerHTML = "";
                                }
                                $(this).append(cloneColumn);
                            }
                        } else {
                            for (var i = (parseInt(value) - $(this).find('th').length); i > 0; i--) {
                                var cloneColumn = $(this).find('th').last()[0].cloneNode(true);
                                if ($(cloneColumn).find('p').length > 0) {
                                    $(cloneColumn).find('p')[0].innerHTML = "";
                                }
                                $(this).append(cloneColumn);
                            }
                        }
                    });
                }
            },
            addRowTemplate: function(param, targetNode) {
                var value = $(targetNode).val();
                var popper = $(targetNode).closest('[data-component]');
                if (popper.find('.tableClone table tr').length >= parseInt(value)) {
                    for (var i = popper.find('.tableClone table tr').length; i > 0; i++)
                        if (popper.find('.tableClone table tr').length > parseInt(value))
                            popper.find('.tableClone table tr').last().remove()
                        else
                            break;
                } else {
                    if (popper.find('.tableClone table tbody tr').length > 0) {
                        for (var i = parseInt(value) - (popper.find('.tableClone table tr').length); i > 0; i--) {
                            var cloneRow = popper.find('.tableClone table tr').last()[0].cloneNode(true);
                            $(cloneRow).find('p').each(function() {
                                this.innerHTML = '';
                            })
                            $(cloneRow).find('th').wrapInner('<td />').contents().unwrap();
                            popper.find('.tableClone table tr').last().after(cloneRow);
                        }
                    } else {
                        var cloneRow = popper.find('.tableClone table tr').last()[0].cloneNode(true);
                        $(cloneRow).find('p').each(function() {
                            this.innerHTML = '';
                        })
                        $(cloneRow).find('th').wrapInner('<td />').contents().unwrap();

                        popper.find('.tableClone table tbody').append(cloneRow);
                    }
                }
            },
            /*End of #174 -logic modified for table popup while changing rows and columns -*Prabakaran A-Focusteam */
			pasteTable: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				if(document.queryCommandEnabled('paste')){
					//If the borwser support clipboard access	
				}else{
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						timeout : 5000,
						content : "Your browser doesn't support direct access to the clipboard. Please use the keyboard shortcuts (Ctrl+V) instead.",
						icon: 'icon-warning2'
					});
					popper.find('table[data-type="floatComponent"]').addClass('hidden');
					popper.find('.pasteTableDiv').removeClass('hidden');
					popper.find('.pasteBtn').addClass('disabled');
					popper.find('.resetTableBtn').removeClass('disabled');
					$('.pasteTableDiv').focus();
				}
			},
			resetTable: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				if(popper.find('[data-pop-type="edit"]:visible').length > 0){
					popper.find('table[data-type="floatComponent"]').removeClass('hidden');
					var selector = popper.attr('data-node-xpath');
					var editBlock = kriya.xpath(selector);
					var popupTable = popper.find('table[data-type="floatComponent"]');
					if($(editBlock).length > 0){
						$.each($(editBlock)[0].attributes,function(i,attr){
							var attrVal = $(editBlock).attr(attr.nodeName);
							if(attr.nodeName != "id" && attr.nodeName != "class"){
								$(popupTable).attr(attr.nodeName, attrVal);	
							}
						});
						
						var tableHTML = $(editBlock).find('table').html();
						popupTable.html(tableHTML);
						popupTable.removeAttr('data-focusout-data');
						popper.find('.pasteTableDiv').addClass('hidden');
						popper.find('.resetTableBtn').addClass('disabled');	
						popper.find('.pasteBtn').removeClass('disabled');
					}
					
				}else{
					popper.find('table[data-type="floatComponent"]').addClass('hidden').html();
					popper.find('.pasteTableDiv').removeClass('hidden');
					popper.find('.resetTableBtn').addClass('disabled');	
					popper.find('.pasteBtn').addClass('disabled');
				}
				
				//reload the empty table from  element template
				/*var emptyTableTemp = $('#compDivContent [data-element-template="true"] [tmp-class="jrnlTable"]').clone(true);
				if(emptyTableTemp.length > 0){
					emptyTableTemp.removeAttr('tmp-insert-after').removeAttr('tmp-insert-before').attr('tmp-insert-at', 'false');
					emptyTableTemp = kriya.general.convertTemplate('jrnlTable', '', '', emptyTableTemp);
					if(emptyTableTemp){
						popper.find('table[data-type="floatComponent"]').html(emptyTableTemp.html());
					}
				}*/
			},
			/**
			 * Function to copy the table footnote link
			 * data-id mantory in footnote element else return false
			 */
			tableFnLinkNow: function(param, targetNode){
				var footNote = $(targetNode).closest('.jrnlTblFoot');
				var symbol = footNote.find('.jrnlFNLabel:first').html();
				if(footNote.attr('data-id')){
					kriya.config.content.copiedTableLink = '<span class="jrnlTblFNRef" data-citation-string=" ' + footNote.attr('data-id') + ' " id="' + uuid.v4() + '">' + symbol + '</span>';
					$('[data-component="pasteFootNoteLink"]').find('.currentFnLink').html(kriya.config.content.copiedTableLink);
					kriya.removeAllNotify();
					kriya.notification({
						title : 'Information',
						type  : 'success',
						content : 'To insert a link to this Table footnote, please click on the desired location in the table, and select ‘Insert’ when prompted.',
						icon : 'icon-info'
					});
				}else{
					return false;
				}
			},
			/**
			 * Function to paste the copied table footnote link
			 */
			pasteCopiedLink: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var citeNodes = $(kriya.config.content.copiedTableLink).clone(true);
				$(citeNodes).attr('data-inserted', 'true');
				$(citeNodes).find('span.jrnlQueryRef').remove();
				//Insert the cite node is text
				kriya.general.insertCitationText(citeNodes,'false');

				kriya.general.updateTblFNLinkCount($(kriya.config.content.copiedTableLink));
				/*var citeString = $(kriya.config.content.copiedTableLink).attr('data-citation-string');
				if(citeString){
					citeString = citeString.replace(/^\s|\s$/g, '');
					var fnNode = $(kriya.config.containerElm + ' .jrnlTblBlock .jrnlTblFoot[data-id="' + citeString + '"]');
					var fnLinkLength = $(kriya.config.containerElm + ' .jrnlTblBlock [data-citation-string=" ' + citeString + ' "]:not([data-track="del"])').length;
					fnNode.find('.linkCount').html(fnLinkLength);
				}*/

				//save the table
				sel = rangy.getSelection();
				range = sel.getRangeAt(0);
				var saveNode = range.commonAncestorContainer;	
				saveNode = $(saveNode).closest('table');
				kriyaEditor.settings.undoStack.push(saveNode);
				kriyaEditor.init.addUndoLevel('save-footnote-link');

				//Reset the copied table link
				kriya.config.content.copiedTableLink = false;
				//close the popup and notification
				popper.addClass('hidden');
				kriya.removeAllNotify();
			},
			/**
			 * Function to cancel the copied table footnote link
			 */
			skipLinkNow: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				kriya.config.content.copiedTableLink = false;
				popper.addClass('hidden');
				kriya.removeAllNotify();
			},
			showMarkAsDropDown: function(param, targetNode){
				var dropDownMenu = $('[data-component="jrnlTblFoot_menu"]');
				var fn = $(targetNode).closest('.jrnlTblFoot');
				var fnType = fn.attr('fn-type');
				fnType = (!fnType)?'general':fnType;

				//Hide the target foot note type
				dropDownMenu.find('[data-fn-type]').removeClass('hidden');
				dropDownMenu.find('[data-fn-type="' + fnType + '"]').addClass('hidden');

				var nodeXpath = kriya.getElementTreeXPath(fn[0]);
				dropDownMenu.attr('data-node-xpath', nodeXpath);
				dropDownMenu.removeClass('hidden');
				var pos = $(targetNode)[0].getBoundingClientRect();
				$(dropDownMenu).css({
					'opacity' : '1',
					'left' : pos.left,
					'top' : pos.top+pos.height,
				});
				$(dropDownMenu).fadeIn();
			},
			markAsConfirmation: function(param, targetNode){
				var dropDownMenu = $(targetNode).closest('[data-component="jrnlTblFoot_menu"]');
				var nodeXpath = dropDownMenu.attr('data-node-xpath');
				var fnType = $(targetNode).attr('data-fn-type');
				var fn = $(kriya.xpath(nodeXpath));
				if(fnType == "linked-fn" || fn.attr("fn-type") != "linked-fn"){
					this.fnMarkAs(param, targetNode);
					return false;
				}
				var message = "Are you sure you want to move the linked footnote? Moving footnote will also remove all links associated with it.";
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : {
								'data-node-xpath' : nodeXpath,
								'data-fn-type' : fnType
							},
							'message' : "{'click':{'funcToCall': 'fnMarkAs','channel':'components','topic':'table'}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
			},
			fnMarkAs: function(param, targetNode){
				var dropDownMenu = $(targetNode).closest('[data-component="jrnlTblFoot_menu"]');
				var nodeXpath = dropDownMenu.attr('data-node-xpath');
				if($(targetNode).closest('[data-component][data-type="popUp"]').length > 0){
					var nodeXpath = $(targetNode).attr('data-node-xpath');
					kriya.popUp.closePopUps($(targetNode).closest('[data-component][data-type="popUp"]'));
				}
				var fn = $(kriya.xpath(nodeXpath));
				var tableBlock = $(fn).closest('.jrnlTblBlock');
				var fnType = $(targetNode).attr('data-fn-type');

				if(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[fn-type="' + fnType + '"]').length > 0){
					fn.insertAfter(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[fn-type="' + fnType + '"]').last());
				}else if(fnType == "general" && tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot:not([fn-type])').length > 0){
					fn.insertAfter(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot:not([fn-type])').last());
				}else{
					fn.insertAfter(tableBlock.find('.jrnlTblFootGroup .jrnlTblFootHead[data-fn-type="' + fnType + '"]'));
				}
				
				if(fnType == "linked-fn"){
					if(fn.find('.jrnlFNLabel').length == 0){
						fn.find('.jrnlTblFootText').prepend('<span class="jrnlFNLabel" />');
					}
					$('<span class="linkCount">0</span>').insertBefore(fn.find('.markAs'));
					$('<span class="linkNowBtn" data-message="{\'click\':{\'funcToCall\': \'tableFnLinkNow\',\'channel\':\'components\',\'topic\':\'table\'}}"><i class="icon-link"></i>link Now</span>').insertBefore(fn.find('.markAs'));
					fn.attr('fn-type', fnType);
					kriya.general.reorderTblFN(tableBlock);
				}else{
					fn.find('.linkNowBtn,.linkCount,.jrnlFNLabel').remove();
					if(fn.attr('fn-type') == "linked-fn"){
						var fnId = fn.attr('data-id');
						var fnLinks = tableBlock.find('[data-citation-string*=" '+ fnId +' "]');
						fnLinks.kriyaTracker('del');
					}
					fn.attr('fn-type', fnType);
				}
				
				
				kriyaEditor.settings.undoStack.push(tableBlock);
				kriyaEditor.init.addUndoLevel('mark-as-fn');

				$(dropDownMenu).css({
					'display' : '',
					'opacity' : '0',
					'left' : '',
					'top' : '',
				});
			},
			addTableFootNote: function(param, targetNode){
				var tableBlock = $(targetNode).closest('[data-id^="BLK_"]');
				var tableId = tableBlock.attr('data-id');
				tableId = tableId.replace('BLK_', '');
				
				//Get the maxium id and construct the data-id value
				var dataId = tableId + '_FN1';
				if(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[data-id]:not([data-track="del"])').length > 0){
					var idArr = [];
					var maxNum = 0
					tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[data-id]:not([data-track="del"])').each(function(){
						var dataId = $(this).attr('data-id');
						if(dataId.match(/\d+$/)){
							dataId = parseInt(dataId.match(/\d+$/)[0]);
							idArr.push(dataId);
						}
					});
					if(idArr.length > 0){
						var maxNum = Math.max.apply(null, idArr);
						maxNum = maxNum+1;
					}
					dataId = tableId + '_FN'+maxNum;
				}

				if(tableBlock.length > 0){
					var fnType = $(targetNode).closest('.jrnlTblFootHead').attr('data-fn-type');
					
					//if(fnType == "general" || fnType == "abbrev"){
					var footNote = $('<div class="jrnlTblFoot" data-inserted="true" data-id="' + dataId + '" id="' + uuid.v4() + '" fn-type="' + fnType + '"><p class="jrnlTblFootText" /></div>');
					footNote.attr('id', uuid());
					
					footNote.append('<span class="removeNode" data-channel="components" data-topic="table" data-event="click" data-message="{\'funcToCall\': \'deleteFNConfirmation\'}">x</span>');
					if(fnType == "linked-fn"){
						footNote.find('.jrnlTblFootText').append('<span class="jrnlFNLabel" />');	
						footNote.append('<span class="linkCount">0</span>');
						footNote.append('<span class="linkNowBtn" data-message="{\'click\':{\'funcToCall\': \'tableFnLinkNow\',\'channel\':\'components\',\'topic\':\'table\'}}"><i class="icon-link"></i>link Now</span>');
					}
					footNote.append('<span class="markAs" data-message="{\'click\':{\'funcToCall\': \'showMarkAsDropDown\',\'channel\':\'components\',\'topic\':\'table\'}}">Mark as</span>');

					if(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[fn-type="' + fnType + '"]').length > 0){
						footNote.insertAfter(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot[fn-type="' + fnType + '"]').last());
					}else if(fnType == "general" && tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot:not([fn-type])').length > 0){
						footNote.insertAfter(tableBlock.find('.jrnlTblFootGroup .jrnlTblFoot:not([fn-type])').last());
					}else{
						footNote.insertAfter($(targetNode).closest('.jrnlTblFootHead'));
					}

					if(fnType == "linked-fn"){
						kriya.general.reorderTblFN(tableBlock);
						var textLength = footNote.find('.jrnlFNLabel').text().length;
						setCursorPosition(footNote.find('.jrnlTblFootText')[0], textLength);	
					}else{
						moveCursor(footNote.find('.jrnlTblFootText')[0]);
					}
					kriyaEditor.settings.undoStack.push(tableBlock);
					kriyaEditor.init.addUndoLevel('new-linked-table-fn');
					
					/*}else{
						//kriya['styles'].init($(targetNode), 'tbl_linked_foot');
					}*/
				}
			},
			deleteFNLinkConfirmation: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var message = "Are you sure you want to delete the table footnote link?";
				var nodeXpath = popper.attr('data-node-xpath');
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : {
								'data-node-xpath' : nodeXpath
							},
							'message' : "{'click':{'funcToCall': 'deleteTableFNLink','channel':'components','topic':'general'}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
			},
			deleteFNConfirmation: function(param, targetNode){
				var fnNode = $(targetNode).closest('.jrnlTblFoot');
				if(fnNode.attr('fn-type') == "linked-fn"){
					var message = "Are you sure you want to delete the table footnote <strong>" + fnNode.find('.jrnlFNLabel:first').text() + "</strong>? Deleting footnote will also remove all links associated with it.";
				}else if(fnNode.attr('data-def-list')=="true"){
					var message = "Are you sure you want to delete the table footnote <strong>" + $(targetNode).closest('span[class="jrnlTblDefItem"]').find('.jrnlAbbrevTerm').text() + "</strong>?";
				}
				else{
					var message = "Are you sure you want to delete the table footnote?";
				}
				if(fnNode.attr('data-def-list')=="true"){
					fnNode = $(targetNode).closest('span[class="jrnlTblDefItem"]');
				}
				var nodeXpath = kriya.getElementTreeXPath(fnNode[0]);
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : {
								'data-node-xpath' : nodeXpath
							},
							'message' : "{'click':{'funcToCall': 'deleteTableFN','channel':'components','topic':'general'}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
			}
		},
		general : {
			linkObjects : function(param, targetNode){
				var dataChild = targetNode[0];
				kriya.general['linkObjects'](dataChild);
			},
			removeLinks : function(param, targetNode){
				// Check if xpath is available then get the element else get the parent element
				var nodeXpath = targetNode.attr('data-node-xpath');
				if(nodeXpath){
					var editElement = kriya.xpath(nodeXpath);
					if(editElement.length>0){
						var dataChild = editElement[0];
						var popper = $(targetNode).closest('[data-type="popUp"]');
						kriya.popUp.closePopUps(popper);
					}
				}else{
					var dataChild = $(targetNode).parent()[0];
				}
				kriya.general['removeLinks'](dataChild);
			},
			addFootnote : function(param, targetNode){
				var parentTemp = $(targetNode).closest('.footnote-section');
				var emptyNotesavail = 0;
				//if (parentTemp.length > 0 && parentTemp.find('.collection li[data-template]').length > 0){
				parentTemp.find('.collection li').not(' li[data-template]').each(function(d, a) { if($(a).find('span').text()==''){emptyNotesavail=1} })
				if (emptyNotesavail!=1 && parentTemp.length > 0 && parentTemp.find('.collection li[data-template]').length > 0){
					var temp = parentTemp.find('.collection li[data-template]').clone(true);
					temp.removeAttr('data-template')
					parentTemp.find('.collection').append(temp);
					temp.find('[contenteditable="true"]').focus();
				}
			},
			addMultipleAuthor : function(param, targetNode){
				$('.kriya-tags-suggestions').addClass('hidden');
				if(param && param.element){					
					var component = $("*[data-type='popUp'][data-component='" + param.element + "']");
					if (component.length > 0){
						if (targetNode.closest('[data-type="popUp"]').length > 0){
							kriya.popUp.getSlipPopUp(component[0], targetNode.closest('[data-type="popUp"]'));
						}else{
							kriya.popUp.getSlipPopUp(component[0], kriya.xpath(targetNode.attr('data-xpath')));
						}
					}	
				}
			},
			addAffil : function(param, targetNode){ 
				$('.kriya-tags-suggestions').addClass('hidden');
				if(param && param.element){
					var componentType = param.element + "_edit";
					var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
					if (component.length > 0){
						if (targetNode.closest('[data-type="popUp"]').length > 0){
							targetNode.closest('[data-type="popUp"]').find('.saveComponent.com-save').addClass('disabled');
							kriya.popUp.getSlipPopUp(component[0], targetNode.closest('[data-type="popUp"]'));
						}else{
							kriya.popUp.getSlipPopUp(component[0], kriya.xpath(targetNode.attr('data-xpath')));
						}
						$(component).find('*[data-if-selector].hidden').removeClass('hidden');
						$(component).find('*[remove-class]').each(function(){
							var className = $(this).attr('remove-class');
							$(this).removeClass(className);
						})
						$(component).find('input').prop('checked',false);
						$(component).find('[data-wrapper]').scrollTop(0)
						$(component).find('[data-pop-type]').addClass('hidden');
						$(component).find('[data-pop-type="new"]').removeClass('hidden');
						$(component).find('[data-error]').removeAttr('data-error');
						$(component).find('.authorAffLinks').addClass('hidden');
						component.attr('data-component-xpath', targetNode.closest('[data-type="popUp"]').attr('data-component'));
						if ($(targetNode)[0].hasAttribute('data-save-type') && $(targetNode)[0].getAttribute('data-save-type') == "true"){
							if ($(targetNode).closest('.row').find('[data-temp-container]').length > 0){
								$(targetNode).closest('.row').find('[data-temp-container]').attr('data-component-type', componentType);
							}
						}
						if ($(component).find('.addExistingbtn').length > 0){
							$(component).find('.addExistingbtn').trigger('click');
							$(component).find('.addExistingbtn').find('li:not("[data-template]")').remove()
						}
					}else if ($(targetNode)[0].hasAttribute('data-class-name')){
						var popUp = $(targetNode).closest('[data-type="popUp"]');
						var className = $(targetNode).attr('data-class-name');
						if (popUp.find('.'+className).length > 0){
							popUp.find('.'+className).toggleClass('hidden');
						}
					}
				}
			},
			addExist : function(param, targetNode){
				if (targetNode.closest('.dropdown-link-content').length > 0) return;
				var container = kriya.config.activeElements;
				var dataChild = targetNode.find('ul.dropdown-link-content')[0];
				var eleType = dataChild.getAttribute('data-type');
				if (typeof(kriya[eleType]) != 'undefined'){
					kriya[eleType].init(container, '', dataChild);
				}else if (typeof(kriya.general[eleType]) != 'undefined'){
					kriya.general[eleType](container, '', dataChild);
				}
				$(dataChild).removeClass('hide');
				// click everywhere else closes the box
				document.onclick = function (e){
					if (dataChild && !$(dataChild).hasClass('hide')) {
						if ($(e.target).closest('[data-onclick="dropdown-link"]').length == 0) $(dataChild).addClass('hide');
					}
				}
			},
			deleteInlineElement: function(param, targetNode){
				var selector = targetNode.closest('[data-type="popUp"]').attr('data-node-xpath');
				var dataChild = kriya.xpath(selector);
				kriya.general['deleteInlineElement'](dataChild[0]);
				targetNode.closest('[data-type="popUp"]').addClass('hidden');
			},
			addInlineElement: function(param, targetNode){
				var selector = targetNode.closest('[data-type="popUp"]').attr('data-node-xpath');
				var selector = kriya.xpath(selector);
				kriya.general['addInlineElement'](selector[0], '', param);
				targetNode.closest('[data-type="popUp"]').addClass('hidden');
			},
			// to handle topic code - aravind
			addTopicCode: function(param,tabeNode){
				var closestRootElement = $(targetNode).closest('*[data-inserted], *[add-data-inserted]');
				if($(closestRootElement).length > 0 && !$(closestRootElement)[0].hasAttribute('id')){
					$(closestRootElement).attr('id',uuid());
					$(closestRootElement).attr('data-inserted','true');
				}
				var componentName = param.component;
				kriya.config.activeElements = targetNode;				
				var component = $('[data-component="'+componentName+'"]');
				kriya['styles'].init($(targetNode), componentName);
				component.find('[data-input]').removeClass('hidden');
				component.addClass('manualSave');				
				component.removeAttr("data-focus");
				var suggestionsList = [];
				var suggName = $(targetNode).attr("data-suggestionslist");
				var hideChangeHistory = $(targetNode).attr("data-hide-change");
				if(suggName!=undefined && suggName!=null && suggName!=""){
					$('[data-name="'+suggName+'"] li').each(function(){
						suggestionsList.push($(this));
					});
					if(suggestionsList.length == 0){
						component.find('[data-input]').attr('data-freetext',true);						
					}else{
						component.find('[data-input]').removeAttr('data-freetext');
						component.find('[data-input]').removeAttr('contenteditable');
						component.attr('data-suggestionslist','true');
					}					
				}else{
					component.find('[data-input]').attr('data-freetext',true);
					component.attr('data-suggestionslist','true');
				}
				
				if(hideChangeHistory && hideChangeHistory!=undefined && hideChangeHistory!=null && hideChangeHistory!=''){
					component.attr('data-hide-change','true');
				}else{
					if(component && component.length > 0 && component[0].hasAttribute('data-hide-change')){
						component.removeAttr('data-hide-change');
					}
				}
				component.find('.popup-menu [data-pop-type="edit"]').addClass('hidden');
				component.find('.popupHead').addClass('hidden');				
				if($(targetNode).hasClass("contentBtn")){
					component.find('.popupHead[data-pop-type="new"]').removeClass('hidden');
				}

				if(targetNode && targetNode.length > 0 && $(targetNode)[0].hasAttribute("data-focus")){  // #161 - priya
					component.attr('data-focus',"true");
				}

				component.find('.kriya-tagsinput').remove();
				kriya.inputTags.kriyaInputTags([],suggestionsList, component.find('[data-input]'), false, false, 1);
				if(suggestionsList.length == 0){
					component.find('.kriya-chips').focus();
				}else{	
					component.find('.kriya-tagsinput').focus();
				}
				if(component.find('.text-line').find('.kriya-tagsinput').length == 0){
					component.find('.text-line').empty();
				}				
			},
			addKeyword: function(param, targetNode){
				var closestRootElement = $(targetNode).closest('*[data-inserted], *[add-data-inserted]');
				if($(closestRootElement).length > 0 && !$(closestRootElement)[0].hasAttribute('id')){
					$(closestRootElement).attr('id',uuid());
					$(closestRootElement).attr('data-inserted','true');
				}
				var componentName = param.component;
				kriya.config.activeElements = targetNode;
				/* Start Issue Id - #331 - Dhatshayani . D - Jan 29, 2018 - To get the component name dynamically and changed the popup style */
				var component = $('[data-component="'+componentName+'"]');
				kriya['styles'].init($(targetNode), componentName);// End Issue Id - #331
				component.find('[data-input]').removeClass('hidden');
				component.addClass('manualSave');
				//component.find('[data-class]').addClass('hidden');// Issue Id - #331
				component.removeAttr("data-focus"); // #161 - priya
				var suggestionsList = [];
				var suggName = $(targetNode).attr("data-suggestionslist");
				var hideChangeHistory = $(targetNode).attr("data-hide-change");// Issue Id - #331
				if(suggName!=undefined && suggName!=null && suggName!=""){
					$('[data-name="'+suggName+'"] li').each(function(){
						suggestionsList.push($(this).html());
					});
					if(suggestionsList.length == 0){
						component.find('[data-input]').attr('data-freetext',true);						
					}else{
						component.find('[data-input]').removeAttr('data-freetext');
						component.find('[data-input]').removeAttr('contenteditable');
						component.attr('data-suggestionslist','true');// Issue Id - #331
					}					
				}else{
					component.find('[data-input]').attr('data-freetext',true);
					component.attr('data-suggestionslist','true');// Issue Id - #331
				}
				/* Start Issue Id - #331 - Dhatshayani . D - Jan 29, 2018 - To show and hide change history between research organism and keywords */
				if(hideChangeHistory && hideChangeHistory!=undefined && hideChangeHistory!=null && hideChangeHistory!=''){
					component.attr('data-hide-change','true');
				}else{
					if(component && component.length > 0 && component[0].hasAttribute('data-hide-change')){
						component.removeAttr('data-hide-change');
					}
				}// End Issue Id - #331
				//Hide query button for add keyword
				component.find('.popup-menu [data-pop-type="edit"]').addClass('hidden');
				component.find('.popupHead').addClass('hidden');
				/* Start Issue Id - #331 - Dhatshayani . D - Jan 29, 2018 */
				/*if($(targetNode).attr("data-name")=="addKeywordBtn"){
					component.find('.popupHead[data-pop-type="new"]').removeClass('hidden');
				}*/
				if($(targetNode).hasClass("contentBtn")){
					component.find('.popupHead[data-pop-type="new"]').removeClass('hidden');
				}

				if(targetNode && targetNode.length > 0 && $(targetNode)[0].hasAttribute("data-focus")){  // #161 - priya
					component.attr('data-focus',"true");
				}// End Issue Id - #331

				component.find('.kriya-tagsinput').remove();
				kriya.inputTags.kriyaInputTags([],suggestionsList, component.find('[data-input]'), false, false, 1);
				if(suggestionsList.length == 0){
					component.find('.kriya-chips').focus();
				}else{	
					component.find('.kriya-tagsinput').focus();
				}
				if(component.find('.text-line').find('.kriya-tagsinput').length == 0){
					component.find('.text-line').empty();
				}// End Issue Id - #331					
			},
			addSubDisplayChannel: function(param, targetNode){
				if($(kriya.config.containerElm + ' .jrnlSubDisplay:empty').length > 0){
					var subDisplayNode = $(kriya.config.containerElm + ' .jrnlSubDisplay:empty');
				}else{
					var subDisplayNode = $('<span class="jrnlSubDisplay" subj-group-type="sub-display-channel" data-inserted="true" id="' + uuid.v4() + '"><span data-class="subject"/><span class="removeNode" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deletePopUp\'}">x</span></span>');
					subDisplayNode.insertBefore(targetNode);
				}
				moveCursor(subDisplayNode.find('[data-class="subject"]')[0]);
			},
			saveTopicCode: function(param, targetNode){
				var component = $(targetNode).closest('[data-type]');
				var dataTagNode = component.find('[data-input]');				
					if(component.find('[data-freetext]').length > 0){
						if(dataTagNode.text()!=""){
							if($(dataTagNode).find(".kriya-tagsinput").length>0){
								dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
								dataHTML = $(dataTagNode).find(".kriya-tagsinput").html();								
							}else{
								dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
								dataHTML = dataTagNode.html();
							}		
						}else{
							return false;
						}							
					}else{
						dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
						dataHTML = dataTagNode.find('.tags').first().html();
					}				
				$(kriya.config.activeElements).find('.jrnlQueryRef').each(function(){
					dataHTML = $(this)[0].outerHTML + dataHTML;
				})				
				if(dataHTML == undefined){
					dataHTML = dataTagNode.text();
				}
				if($(kriya.config.activeElements).hasClass("jrnlTopicCode")){
					var temp =$(kriya.config.activeElements).html(dataHTML);
				}else{
					if($(dataTagNode).find('[data-topic-code]').length > 0){
						var topicCode = $(dataTagNode).find('[data-topic-code]').attr('data-topic-code');
						var temp = $('<span class="jrnlTopicCode" data-inserted="true" data-topic-code="'+topicCode+'" >' + dataHTML + '</span>');
					}else{
						var temp = $('<span class="jrnlTopicCode" data-inserted="true">' + dataHTML + '</span>');
					}					
					temp.kriyaTracker('ins');
				}				
				
				if(temp.find('.removeNode').length == 0){
					temp.append('<span class="removeNode" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deletePopUp\'}">x</span>');
				}
				
				if(!$(kriya.config.activeElements).hasClass("jrnlTopicCode")){
					temp.attr('id', uuid());
				}				
				kriya.popUp.closePopUps(component);
				
				if(!$(kriya.config.activeElements).hasClass("jrnlTopicCode")){
					// Issue Id #331 - Dhatshayani . D Jan 29, 2018 - CHanged the component name
					if(kriya.general.validateInlineElement('jrnlTopicCode_edit','jrnlTopicCode', 'insert', $("[data-name='addKeywordBtn']"))){
						kriya.config.activeElements.before(temp);						
					}
					$(kriya.config.activeElements).closest('*[add-data-inserted]').removeAttr('add-data-inserted');
				}
				/* Start Issue Id - #331 - Dhatshayani . D Jan 29, 2018 - To track the change history */
				if(temp.length > 0){
					kriya.general.addToHistory(dataTagNode, temp, temp);
				}
				if(component && component.length > 0 && component[0].hasAttribute('data-hide-change')){
					component.removeAttr('data-hide-change');
				}// End Issue Id - #331
				//if content has keyword element then save
				if($(kriya.config.containerElm).find(temp).length > 0){
					if($('.jrnlTopicCodePara .jrnlTopicCode').length > 1){
						kriyaEditor.settings.undoStack.push(temp);
					}else{
						if($(kriya.config.activeElements.closest('.jrnlTopicGroup')).attr('id') == undefined){
							$(kriya.config.activeElements.closest('.jrnlTopicGroup')).attr('id',uuid())
						}
							kriyaEditor.settings.undoStack.push(kriya.config.activeElements.closest('.jrnlTopicGroup'));
					}
					kriyaEditor.init.addUndoLevel('save-component');
				}			
			},
			saveKeyword: function(param, targetNode){
				var component = $(targetNode).closest('[data-type]');
				var dataTagNode = component.find('[data-input]');
				if(component.find('[data-freetext]').length == 0 && dataTagNode.find('.tags').length == 0){
					return false;
				}else{
					if(component.find('[data-freetext]').length > 0){
						if(dataTagNode.text()!=""){
							if($(dataTagNode).find(".kriya-tagsinput").length>0){
								dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
								dataHTML = $(dataTagNode).find(".kriya-tagsinput").html();
								//#292 - stripping all tags while pasting in components - Mohammed Navaskhan (mohammednavaskhan.a@focusite.com)
								//commented by jai -should be done in onpaste function in kriyacomponent.js
								//dataHTML = dataHTML.replace(/(<([^>]+)>)/ig, "");
								//End of #292
							}else{
								dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
								dataHTML = dataTagNode.html();
							}		
						}else{
							return false;
						}							
					}else{
						dataTagNode.find('.tags .removeKriyaTag, .jrnlQueryRef ').remove();
						dataHTML = dataTagNode.find('.tags').first().html();
					}
				}
				// To avoid duplication of keywords - aravind #1156
				var presentKeywords = [];				
					$('.jrnlKwdGroup').find('span:not([data-track="del"])[class="jrnlKeyword"]').each(function(){
						var thisNode = $(this).clone();
						$(thisNode).find('.removeNode').remove();
						presentKeywords.push($(thisNode).text().toLowerCase());					
					});				
				if(presentKeywords.length > 0 && presentKeywords.indexOf(dataHTML.toLowerCase()) >= 0){
					$('[data-component="jrnlKeyword_edit"]').find('[data-class="jrnlKeyword"]').attr('data-error','Duplicate Keyword');			
				}	
				else{		
				$('[data-component="jrnlKeyword_edit"]').find('[data-class="jrnlKeyword"]').removeAttr('data-error');		
				$(kriya.config.activeElements).find('.jrnlQueryRef').each(function(){
					dataHTML = $(this)[0].outerHTML + dataHTML;
				})				
				if($(kriya.config.activeElements).hasClass("jrnlKeyword")){
					var temp =$(kriya.config.activeElements).html(dataHTML);
				}else{
					var temp = $('<span class="jrnlKeyword" data-inserted="true">' + dataHTML + '</span>');
					temp.kriyaTracker('ins');
				}				
				
				if(temp.find('.removeNode').length == 0){
					temp.append('<span class="removeNode" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deletePopUp\'}">x</span>');
				}
				
				if(!$(kriya.config.activeElements).hasClass("jrnlKeyword")){
					temp.attr('id', uuid());
				}				
				kriya.popUp.closePopUps(component);
				
				if(!$(kriya.config.activeElements).hasClass("jrnlKeyword")){
					// Issue Id #331 - Dhatshayani . D Jan 29, 2018 - CHanged the component name
					if(kriya.general.validateInlineElement('jrnlKeyword_edit','jrnlKeyword', 'insert', $("[data-name='addKeywordBtn']"))){
						kriya.config.activeElements.before(temp);						
					}
					$(kriya.config.activeElements).closest('*[add-data-inserted]').removeAttr('add-data-inserted');
				}
				/* Start Issue Id - #331 - Dhatshayani . D Jan 29, 2018 - To track the change history */
				if(temp.length > 0){
					kriya.general.addToHistory(dataTagNode, temp, temp);
				}
				if(component && component.length > 0 && component[0].hasAttribute('data-hide-change')){
					component.removeAttr('data-hide-change');
				}// End Issue Id - #331
				//if content has keyword element then save
				if($(kriya.config.containerElm).find(temp).length > 0){
					kriyaEditor.settings.undoStack.push(temp);
					kriyaEditor.init.addUndoLevel('save-component');
				}
			}
			},
			saveOnbehalfof: function(param, targetNode){
				var component = $(targetNode).closest('[data-type]');
				var dataTextNode = component.find('[data-class]');
				dataTextNode.find('.removeNode').remove();
				if (dataTextNode.text() == "" || /^[\s\u00A0]*$/.test(dataTextNode.text())) {
					$(dataTextNode).attr('data-error', 'Input Required');
					return;
				}else{
					if (kriya.config.activeElements && kriya.config.activeElements.hasClass('jrnlOnBehalfOf')){
						kriya.config.activeElements.html(dataTextNode.text());
						temp = kriya.config.activeElements;
					}else{
						var temp = $('<span class="jrnlOnBehalfOf" data-inserted="true">' + dataTextNode.text() + '</span>');
						temp.attr('id', uuid());
						temp.kriyaTracker('ins');
						$('.jrnlAuthorsGroup .jrnlAuthors').append(temp);
					}
					temp.append('<span class="removeNode" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deletePopUp\', \'param\': {\'afterDelete\':\'eventHandler.components.general.checkOnbehalfof\'}}">x</span>')
					kriya.general.alignAuthorLinks(temp);
					eventHandler.components.general.checkOnbehalfof($(temp));
					kriya.popUp.closePopUps(component);
					kriyaEditor.settings.undoStack.push(temp);
					kriyaEditor.init.addUndoLevel('save-component');
				}
			},
			checkOnbehalfof: function(param, targetNode){
				if (param && param.attr('data-track') == "del"){
					param.parent().find('.btn:contains("behalf")').removeClass('hidden');
				}else if (param){
					param.parent().find('.btn:contains("behalf")').addClass('hidden');
				}
			},
			// aravind to upload author bio					
			uploadAuthorBio: function(param, targetNode){
				$('[data-component="uploadAuthorBio_edit"]').find('img').removeAttr('src');
				var currPopUpComp =	kriya.popUp.showEmptyPopUp($('[data-component="uploadAuthorBio_edit"]'), $(targetNode));				
			},
			//to get package pdf path from xml and updating the same in popup 
			//if package pdf path not available opens up empty popup
			getPackagePdf: function(param, targetNode){
				if(param && param.component){
					var parameters = {
						'customer': kriya.config.content.customer,
						'project' : kriya.config.content.project,
						'doi'     : kriya.config.content.doi,
						'xpath'   : '//custom-meta-group/custom-meta[@data-content-type="AOP"]/meta-value'
					};
						//Send the request to get the current package pdf
						//if information present in xml then info is updated in popup
						keeplive.sendAPIRequest('getdatausingxpath', parameters, function(res){
							if(res){
								if($('[data-component="'+param.component+'"]').find('img').length > 0){
									$('[data-component="'+param.component+'"]').find('img').attr('src', $(res).text());
								}
								kriya.popUp.showEmptyPopUp($('[data-component="'+param.component+'"]'), $(targetNode));
							}else{
								kriya.popUp.showEmptyPopUp($('[data-component="'+param.component+'"]'), $(targetNode));
							}
						},function(res){
							kriya.popUp.showEmptyPopUp($('[data-component="'+param.component+'"]'), $(targetNode));
						});
				}
			},
			// aravind to upload product Info
			uploadProductInfo: function(param, targetNode){
				$('[data-component="uploadProductInfoImage_edit"]').find('img').removeAttr('src');
				var currPopUpComp =	kriya.popUp.showEmptyPopUp($('[data-component="uploadProductInfoImage_edit"]'), $(targetNode));				
			},												
			//to add new role to an Author - Jai 02-02-2018
			addAuthorRole: function(param, targetNode){
				var parentComponent = $(targetNode).closest('[data-component]');
				if ($(parentComponent).find('#affiliation-field li[data-rid]').length > 0){
					$('[data-component="jrnlAuthorRole_edit"]').addClass('manualSave')
					//eventHandler.components.PopUp.editSubPopUp(param, targetNode);
					kriya.popUp.showEmptyPopUp($('[data-component="jrnlAuthorRole_edit"]'), $);
					
					$('[data-component="jrnlAuthorRole_edit"]').removeAttr('data-content-xpath');
					$('[data-component="jrnlAuthorRole_edit"]').find('[data-pop-type]').addClass('hidden');
					$('[data-component="jrnlAuthorRole_edit"]').find('[data-pop-type="new"]').removeClass('hidden');

					$('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').html('')
					$(parentComponent).find('#affiliation-field li[data-rid]').each(function(){
						var clone = $(this).clone(true);
						clone.find('.deleteLink').remove();
						$(clone).attr('data-channel', 'components').attr('data-topic', 'general').attr('data-message','{\'funcToCall\': \'selectRolesAff\'}').attr('data-event', 'click').attr('data-clone', 'true');
						$('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').append(clone);
					})
				}
			},
			editAuthorRole: function(param, targetNode){
				var parentComponent = $(targetNode).closest('[data-component]');
				var targetRole = $(targetNode).html();
				var roleAffId  = $(targetNode).attr('data-rid');
				var contentXpath = $(targetNode).attr('data-node-xpath');
				if ($(parentComponent).find('#affiliation-field li[data-rid]').length > 0){
					$('[data-component="jrnlAuthorRole_edit"]').addClass('manualSave')
					eventHandler.components.PopUp.editSubPopUp(param, targetNode);
					
					$('[data-component="jrnlAuthorRole_edit"]').attr('data-content-xpath', contentXpath);
					$('[data-component="jrnlAuthorRole_edit"]').find('[data-pop-type]').addClass('hidden');
					$('[data-component="jrnlAuthorRole_edit"]').find('[data-pop-type="edit"]').removeClass('hidden');

					$('[data-component="jrnlAuthorRole_edit"]').find('[data-class="jrnlRole"]').html(targetRole);
					$('[data-component="jrnlAuthorRole_edit"]').find('[data-class="jrnlRole"] .deleteLink').remove();
					$('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').html('');
					$(parentComponent).find('#affiliation-field li[data-rid]').each(function(){
						var clone = $(this).clone(true);
						if(clone.attr('data-rid') == roleAffId){
							clone.addClass('active');
						}
						clone.find('.deleteLink').remove();
						$(clone).attr('data-channel', 'components').attr('data-topic', 'general').attr('data-message','{\'funcToCall\': \'selectRolesAff\'}').attr('data-event', 'click').attr('data-clone', 'true');
						$('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').append(clone);
					})
				}
			},
			//save role to an Author along with linked affiliation- Jai 02-02-2018
			saveAuthorRole: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				if ($('[data-component="jrnlAuthorRole_edit"]').find('[data-class="jrnlRole"]').text().trim() == "" || $('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').find('li.active').length == 0){
					return false;
				}
				var roleText = $('[data-component="jrnlAuthorRole_edit"]').find('[data-class="jrnlRole"]').text().trim();
				var dataRid  = $('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').find('li.active').attr('data-rid');
				var nodeXpath = popper.attr('data-node-xpath');
				var contentXpath = popper.attr('data-content-xpath');
				var dataNode = null;
				if(nodeXpath){
					dataNode = kriya.xpath(nodeXpath);
				}
				if(dataNode && $(dataNode).length > 0){
					if(contentXpath){
						dataNode = $(dataNode).filter("[data-node-xpath='" + contentXpath + "']");
					}
					$(dataNode).html(roleText);
					$(dataNode).attr('data-rid', dataRid);
					$(dataNode).attr('data-focusout-data', roleText);
					$(dataNode).append($('<i class="material-icons deleteLink" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deleteConfirmation\'}">remove_circle_outline</i>'));
				}else{
					var role = $('.role-section .collection.role-holder li[data-template="true"]').clone(true);
					$(role).html(roleText);
					$(role).attr('data-rid', dataRid);
					$(role).attr('data-node-xpath', 'new-node');
					if ($(role).attr('data-append-button') == 'deleteLink'){
						$(role).append($('<i class="material-icons deleteLink" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deleteConfirmation\'}">remove_circle_outline</i>'));
					}
					$(role).removeAttr('data-template');
					$('.role-section .collection.role-holder').append(role);
					$(role).attr('data-clone', 'true');
					$(role).attr('data-focusout-data', roleText);
				}
				
				kriya.config.popUpContainer = null;
				kriya.popUp.closePopUps($('[data-component="jrnlAuthorRole_edit"]'));
			},
			selectRolesAff: function(param, targetNode){
				$('[data-component="jrnlAuthorRole_edit"]').find('.collection.affiliation-field').find('li.active').removeClass('active');
				$(targetNode).addClass('active');
			},
			/**
			 * Function to delete the active element
			 * @param param - parameters to the function
			 * @param param.afterDelete - Trigger the function after delete the element
			 */
			deleteElement: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				kriya.popUp.closePopUps(popper);
				//$('[data-type="popUp"]').addClass('hidden');
				var callback = (param && param.callback)?param.callback:null;
				var className = kriya.config.activeElements[0].getAttribute('class').replace(/^jrnl|Group$| .*$/g,'');
				if(className == "Keyword"){
					var suggcheck = $(targetNode).siblings('[data-suggestionslist]');
					if(suggcheck!=undefined && suggcheck!=null && suggcheck!=""){
						if($('[data-name="'+suggcheck+'"]').attr("data-maximum")!=undefined && $('[data-name="'+suggcheck+'"]').attr("data-minimum")!=undefined){
							$('[data-component="jrnlKeyword"]').attr("data-maximum",$('[data-name="'+suggcheck+'"]').attr("data-maximum")); 
							$('[data-component="jrnlKeyword"]').attr("data-minimum",$('[data-name="'+suggcheck+'"]').attr("data-minimum")); 
						}
					}	
					// Issue Id #331 - Dhatshayani . D Jan 29, 2018 - Changed the component name.
					if(!kriya.general.validateInlineElement('jrnlKeyword_edit','jrnlKeyword', 'delete',$("[data-name='addKeywordBtn']"))){
						return false;
					}
				}
				
				$(kriya.config.activeElements).kriyaTracker('del', callback);
				
				if($(kriya.config.activeElements).closest('.jrnlEqnPara').attr('data-id')){
					kriya.general.updateRightNavPanel($(kriya.config.activeElements).closest('.jrnlEqnPara').attr('data-id'), kriya.config.containerElm)
				}

				if($(kriya.config.containerElm).find(kriya.config.activeElements).length > 0){
					kriyaEditor.settings.undoStack.push(kriya.config.activeElements);
				}
				
				if(param && param.afterDelete){
					var execFn = getStringObj(param.afterDelete);
					execFn($(kriya.config.activeElements));
				}
				if (kriyaEditor.settings.undoStack.length > 0){
					kriyaEditor.init.addUndoLevel('delete-element');
				}
				var msg = 'The ' + className + ' has been deleted.';
				if ($('[data-component*="' + className + '"][data-delete-message]').length > 0){
					msg = $('[data-component*="' + className + '"][data-delete-message]').attr('data-delete-message');
				}
				eventHandler.menu.edit.addUndoModal(msg);
				callUpdateArticleCitation($(kriya.config.activeElements), '.jrnlCitation');
			},
			deleteAff: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				$('[data-type="popUp"]').addClass('hidden');
				if(param && param.callback){
					var node = $(kriya.config.activeElements).kriyaTracker('del', param.callback);
				}else{
					var node = $(kriya.config.activeElements).kriyaTracker('del');
				}
				var savedNode = kriya.general.deleteLinksfromObjects(comp[0], node);
				if(param && param.afterDelete){
					var execFn = getStringObj(param.afterDelete);
					execFn($(kriya.config.activeElements));
				}
				if (kriyaEditor.settings.undoStack.length > 0){
					kriyaEditor.init.addUndoLevel('delete-aff');
					kriyaEditor.settings.undoStack.push($(this)[0]);
				}
				eventHandler.menu.edit.addUndoModal('The Affiliation has been deleted.');
			},
			deleteBlockConfirmation: function(param, targetNode){
				var paramString = (param) ? "'param':"+JSON.stringify(param) : '';
				var deleteFunction = (param && param.delFunc)?param.delFunc : 'deleteElement';
				var blockNode = null;
				var nodeXpath = null;
				if($(targetNode).closest('[data-node-xpath]').length > 0){
					nodeXpath = $(targetNode).closest('[data-node-xpath]').attr('data-node-xpath');
					blockNode = kriya.xpath(nodeXpath);
				}else if($(targetNode).closest('[data-id^="BLK_"]').length > 0){
					blockNode = $(targetNode).closest('[data-id^="BLK_"]');
					nodeXpath = kriya.getElementXPath(blockNode[0]);
				}else if($(targetNode).closest('.jrnlFootNote').length > 0){
					blockNode = $(targetNode).closest('.jrnlFootNote');
					nodeXpath = kriya.getElementXPath(blockNode[0]);
				}else if($(targetNode).closest('[data-panel-id]').length > 0){
					var rid = $(targetNode).closest('[data-panel-id]').attr('data-panel-id');
					blockNode = $(kriya.config.containerElm).find('[data-id="'+rid+'"]');
					nodeXpath = kriya.getElementXPath(blockNode[0]);
				}else if($(targetNode).closest('[data-float-id]').length > 0){
					var rid = $(targetNode).closest('[data-float-id]').attr('data-float-id');
					blockNode = $(kriya.config.containerElm).find('[data-id="'+rid+'"]');
					nodeXpath = kriya.getElementXPath(blockNode[0]);
				}

				var attrObj = {};
				var message = '';
				if($(blockNode).length > 0 && nodeXpath){
					var blockId = $(blockNode).attr('data-id');
					if($(blockNode).hasClass('jrnlRefText')){
						var refId = $(blockNode).attr('data-id');
						var citeText = kriya.general.getNewCitation([blockId], '', '', false);
						message = "Are you sure you want to delete the reference " + citeText + "? Deleting this would also remove all citations associated with it.";
					}else if($(blockNode).hasClass('jrnlFootNote')){
						var fnId = $(blockNode).attr('data-id');
						var citeText = kriya.general.getNewCitation([fnId], '', '', false);
						message = "Are you sure you want to delete the footnote " + citeText + "? Deleting this would also remove all citations associated with it.";
					}else{
						blockId = blockId.replace(/^BLK_/g, '');
						var floatLabelString = '';
						if($(blockNode).find('[class*="Caption"] .label').length > 0){
							floatLabelString = 	$(blockNode).find('[class*="Caption"] .label').clone(true).cleanTrackChanges().html();
						}else{
							floatLabelString = citeJS.floats.getCitationHTML([blockId], [], 'renumberFloats');	
						}
						floatLabelString = floatLabelString.replace(/([\.\,\:\;\|\s]+)$/, '');
						var blockCitations = $(kriya.config.containerElm).find('[data-citation-string*=" '+ blockId +' "]');
						message = "Are you sure you want to delete " + floatLabelString + "? Deleting " + floatLabelString + " will also remove all citations associated with it.";
					}
					attrObj['data-node-xpath'] = nodeXpath;
				}
				var settings = {
					'icon'  : '<i class="material-icons">warning</i>',
					'title' : 'Confirmation Needed',
					'text'  :  message,
					'size'  : 'pop-sm',
					'buttons' : {
						'cancel' : {
							'text'    : 'No',
							'class'   : 'btn-danger pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
						},
						'ok' : {
							'text'    : 'Yes',
							'class'   : 'btn-success pull-right',
							'attr'    : attrObj,
							'message' : "{'click':{'funcToCall': '" + deleteFunction + "','channel':'components','topic':'general'," + paramString + "}}"
						}
					}
 				}
 				kriya.actions.confirmation(settings);
				//alert('Work is in progress..');
			},
			deleteFloatBlock : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var nodeXpath = $(targetNode).attr('data-node-xpath');
				if(nodeXpath){
					var deleteBlock = kriya.xpath(nodeXpath);
					deleteBlock = deleteBlock[0];
					if($(deleteBlock).length < 1) return false;
					var blockId = $(deleteBlock).attr('data-id');
					var blockClass = $(deleteBlock).attr('class');
					if(blockId && blockClass){
						//$('.la-container').fadeIn();
						popper.progress('Deleting');
						$('[data-component="confirmation_edit"] .loadingProgress').css('bottom', '0px');
						$('[data-component="confirmation_edit"] .loading-wraper').css('opacity', '0');
						var citeId = blockId.replace(/BLK_/g, '');
						var blockCitations = $(kriya.config.containerElm).find('[data-citation-string*=" '+ citeId +' "]');
						var labelTxt = blockId;
						if($(deleteBlock).find('[class*="Caption"] .label').length > 0){
							labelTxt = $(deleteBlock).find('[class*="Caption"] .label').clone(true).cleanTrackChanges().text();
						}else if($(deleteBlock).find('.RefSlNo').length > 0){
							labelTxt = "reference " + $(deleteBlock).find('.RefSlNo').clone(true).cleanTrackChanges().text();
						}else if(blockClass == "jrnlRefText"){
							labelTxt = "reference " + citeJS.floats.getReferenceHTML(blockId);
						}

						syncFunction(function(){
							//Delete the citations of the float object						
							var msg = "Removing " + labelTxt + "... (0%) <br> Removing corresponding citation(s)...";
							$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
							blockCitations.each(function(){
								if($(this).closest('[data-track="del"],.del, .jrnlDeleted').length > 0) return;
								kriya.general.deleteCitationElement(this, citeId);
							});
							
							//Delete the float object
							syncFunction(function(){
								var msg = "Removing " + labelTxt + "... (18%) <br> Adding to tracking info...";
								$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
								var trackMsg = ($(deleteBlock).hasClass('jrnlRefText'))?'The reference ' + citeId + ' was deleted.':'The float object ' + citeId + ' was deleted.';
								$(deleteBlock).kriyaTracker('del', 'kriya.general.reCallFloat', '', trackMsg);
								
								syncFunction(function(){
									var msg = "Removing " + labelTxt + "... (36%) <br> Adding reply to query...";
									$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
									//reply to uncited objects - jai
									if ($(deleteBlock).find('.jrnlQueryRef[data-query-action*="uncited"]:not([data-replied])').length > 0){
										var qid = $(deleteBlock).find('.jrnlQueryRef').attr('data-rid');
										var queryNode = $('.query-div#' + qid);
										if (queryNode.length > 0 && /^uncited/.test(queryNode.attr('data-query-action')) && queryNode.attr('data-float-id') == citeId){
											var target = $(queryNode).find('.query-holder').first();
											$(queryNode).find('.query-action').remove();
											$(queryNode).attr('data-remove-edit', 'true');
											eventHandler.query.action.addReply({'undoLevel':'false','content':'<i>This has been deleted</i>', 'reply' : true}, target); //prevent undolevel function call for #346 - priya
										}
									}
	
									syncFunction(function(){
										var msg = "Removing " + labelTxt + "... (54%) <br> Moving object as needed...";
										$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
										blockClass = blockClass.split(/\s+/)[0];
										var sameBlockElements = $(kriya.config.containerElm + ' .' + blockClass + '');
										//If deleted block is reference then move at the end of reference
										//elese move the block at the end of same block 
										//if same block is not found then move at the end of the body
										if($(deleteBlock).hasClass('jrnlRefText') || $(deleteBlock).hasClass('jrnlFootNote')){
											var idPrefix = blockId.replace(/(\d+)$/, '');
											sameBlockElements = $(kriya.config.containerElm + ' .' + blockClass + '[data-id^="' + idPrefix + '"]');
											if(sameBlockElements.length > 1){
												$(deleteBlock).insertAfter(sameBlockElements.last());
												$(deleteBlock).attr('data-moved', 'true');
											}
										}else{
											if($(deleteBlock).closest('.body').length > 0){
												$(kriya.config.containerElm).find('.body').append(deleteBlock);
											}else if($(deleteBlock).closest('.back').length > 0){
												$(kriya.config.containerElm).find('.back').append(deleteBlock);
											}
											$(deleteBlock).attr('data-moved', 'true');
										}
										syncFunction(function(){
											var msg = "Removing " + labelTxt + "... (72%) <br> Checking citation sequence...";
											$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
											//Update the citation status if the citation is deleted
											if(blockCitations.length > 0){
												if(citeJS.settings.showMsg == true && $('#contentContainer').attr('data-citation-trigger') == "true"){
													citeJS.floats.renumberConfirmation('delete-float-object');
												}
												citeJS.general.updateStatus();
											}
											kriya.general.updateRightNavPanel(blockId, kriya.config.containerElm);
											
											syncFunction(function(){
												var msg = "Removing " + labelTxt + "... (100%) <br> Saving changes...";
												$('[data-component="confirmation_edit"] .loadingProgress [data-label="progress"]').html(msg);
												$(deleteBlock)[0].scrollIntoView();
												kriyaEditor.settings.undoStack.push(deleteBlock);
												kriyaEditor.init.addUndoLevel('delete-float-object');
												popper.progress('Deleting', true);
												kriya.popUp.closePopUps(popper);
											});
										});
									});
								});
							});
						});
					}
				}
			},
			deleteTableFNLink: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var nodeXpath = $(targetNode).attr('data-node-xpath');
				if(nodeXpath){
					var fnLinkNode = kriya.xpath(nodeXpath);
					if($(fnLinkNode).length > 0){
						$(fnLinkNode).kriyaTracker('del');
						kriya.general.updateTblFNLinkCount(fnLinkNode);
						kriyaEditor.settings.undoStack.push(fnLinkNode);
						kriyaEditor.init.addUndoLevel('delete-table-footnote-link');
					}
				}
				kriya.popUp.closePopUps(popper);
			},
			deleteTableFN: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var nodeXpath = $(targetNode).attr('data-node-xpath');
				if(nodeXpath){
					var fnNode = kriya.xpath(nodeXpath);
					if($(fnNode).length > 0){
						var tableNode = $(fnNode).closest('.jrnlTblBlock');
						var fnId = $(fnNode).attr('data-id');
						$(fnNode).kriyaTracker('del');
						tableNode.find('[data-citation-string=" ' + fnId + ' "]').kriyaTracker('del');
						//to handle tbl foot abbrev
						if($('[tmp-class="jrnlFootDefinition"]').attr('tmp-remove-last-node-suffix')){
							//if $(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition') not there code will break
							if($(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length && $(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').length > 0){
								$(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).nextSibling.remove();
							}							
						}
						// to set last node suffix for tbl abbrev foot
						var setLastNodeSuffix = $('[tmp-class="jrnlFootDefinition"]').attr('tmp-set-last-node-suffix');
						if(setLastNodeSuffix != undefined){
							//if $(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition') not there it will break code
							if($(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').length && $(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').length > 0){
								$(tableNode).find('div[fn-type="abbrev"]').find('span:not([data-track="del"])[class="jrnlTblDefItem"]').last().find('.jrnlFootDefinition').get(0).insertAdjacentHTML("afterend", setLastNodeSuffix);
							}
						}
						kriya.general.reorderTblFN(tableNode);
						kriyaEditor.settings.undoStack.push(tableNode);
						kriyaEditor.init.addUndoLevel('delete-table-footnote');
					}
				}
				kriya.popUp.closePopUps(popper);
			},
			reorderFunding: function(param, targetNode){
				if (param){//on deleting funding, remove the links from authors - jai
					var id = $(param).attr('data-id');
					if ($(kriya.config.containerElm).find('[data-rid="'+id+'"]').length > 0){
						//var container = $(kriya.config.containerElm).find('[data-rid="'+id+'"]').first().parent();
						//kriya.general.reorderLinks('jrnlFundRef', 'award-group', container[0]);
						$(kriya.config.containerElm).find('[data-rid="'+id+'"]').each(function(){
							var rid = $(this).attr('data-rid');
							$(this).remove();
						});
					}
				}
			},
			//validationHTML: function(param, targetNode){
			//If we did not give onError parameter code will break in this function when we are using onError()
			//if any where if call this function and need onError() for handling any failure in api call then pass that callback function as 3rd parameter
			validationHTML: function(param, targetNode, onError){
				$('#infoDivContent #validationDivNode').html('');
				$('#contentDivNode .activeElement').removeClass('activeElement')
				eventHandler.components.general.addDel();
				var parameters  = {
					'doi': kriya.config.content.doi,
					'customer': 'kriyahtml',
					'project':kriya.config.content.project,
					'role': kriya.config.content.role,
					'stage':kriya.config.content.stage,
					'content':$(kriya.config.containerElm).html()
				};
				kriya.general.sendAPIRequest('probevalidation', parameters, function(res){
					$('.la-container').fadeOut();
					if(res.error){
						if(onError && typeof(onError) == "function"){
							onError(res);
						}
					}else if(res){
						res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); //change self closed column tag to open and close tag
						var popper = $('[data-type="popUp"][data-component="PROBE_VALIDATION_edit"]');
						var resNode = $(res);
						var stataus = true;
						if((resNode.find('message').length > 0) && (resNode.find('func').length > 0)){
							resNode.find('message').each(function(){
								if(resNode.find('func').length > 0 && resNode.find('node-id').length > 0) {
									var func = $(this).find('func').text();
									var nodeid = $(this).find('node-id').text();
									if(func && nodeid){
										if(func){
											eventHandler.components.general[func](nodeid);
										}
									}
								}
							})
						}else{
							//in place of param putted pram
 							if(param && param.onSuccess && (typeof(param.onSuccess) == "function")){
								param.onSuccess();
							}
						}
					}
				}, function(err){
					if(onError && typeof(onError) == "function"){
						onError(err);
					}
				});
			},
			//remove Empty tag from content 
			removeEmpTag: function(param){
				$('#contentDivNode #'+param).each(function(){
						var r = $(this).contents().length;
						if(r == 0){
							$(this).attr('class','jrnlDeleted')
							$(this).attr('data-track','del')
							$(this).attr('data-modified','Remove empty tag')
							kriyaEditor.settings.undoStack.push(this);
							kriyaEditor.init.addUndoLevel('save update data');
						}
					})
			},
			
			addDel: function(param){
				$('#contentDivNode p:not(".jrnlDeleted"), #contentDivNode  div:not(".jrnlDeleted"), #contentDivNode  h1:not(".jrnlDeleted"), #contentDivNode  h2:not(".jrnlDeleted"), #contentDivNode  h3:not(".jrnlDeleted")').each(function(){
					var cileg = $(this).contents().length
					var dileg = $(this).find(' > .del, > *[data-track="del"], > .jrnlDeleted').length
					if(dileg != 0 && dileg == cileg){
						$(this).attr('class','jrnlDeleted');
						$(this).attr('data-track','del')
						$(this).attr('data-modified','Remove empty tag')
						kriyaEditor.settings.undoStack.push(this);
						kriyaEditor.init.addUndoLevel('save update data');
					}
				})
			},
			
			//Remore start, end space and double spaces from content.
			removespace: function(param){
				return false;				
				if(param !=''){
						var currentNode = $('#contentDivNode #' + param)[0];
						var textNodes = textNodesUnder(currentNode);
						var tnLength = textNodes.length;
						if(tnLength != 0){
						var firstText = 	textNodes[0].nodeValue;
						if(firstText.match(/((^(\s|&nbsp;))|(^(\s|&nbsp;)$))/)){
							if(firstText.match(/^(\s|&nbsp;)$/) && textNodes[0].parentElement && textNodes[0].parentElement.nodeName == "SPAN"){
								$(textNodes[0].parentElement).removeAttr('class')
								$(textNodes[0].parentElement).addClass('del')
							}else{
							firstText = firstText.replace(/^(\s|&nbsp;)/, '');
							textNodes[0].nodeValue = firstText;
							}
							kriyaEditor.settings.undoStack.push(textNodes[0]);
						}
						var lastText = 	textNodes[tnLength-1].nodeValue;  
						
						if(lastText.match(/(\s|&nbsp;)$/)){
							if(firstText.match(/^(\s|&nbsp;)$/) && textNodes[tnLength-1].parentElement && textNodes[tnLength-1].parentElement.nodeName == "SPAN"){
								$(textNodes[0].parentElement).removeAttr('class')
								$(textNodes[0].parentElement).addClass('del')
							}else{
								lastText = lastText.replace(/(\s|&nbsp;)$/, '');
								textNodes[tnLength-1].nodeValue = lastText;
							}
							kriyaEditor.settings.undoStack.push(textNodes[tnLength-1]);
						}
						$(textNodes).each(function(i){
							if(textNodes[i+1] && textNodes[i+1].nodeValue){
								var nextnode  = textNodes[i+1].nodeValue
							}
							
							var textnode = this.nodeValue;
							if(nextnode && nextnode !='' && textnode !='' && textnode.match(/(\s|&nbsp;)$/) && nextnode.match(/^(\s|&nbsp;)/)){
								if(textnode.match(/^(\s|&nbsp;)$/) && textnode.parentElement && textnode.parentElement.nodeName == "SPAN"){
								$(textnode.parentElement).removeAttr('class')
								$(textnode.parentElement).addClass('del')
								kriyaEditor.settings.undoStack.push(this);
							}else{
							textnode = textnode.replace(/(\s|&nbsp;)$/, '');
							this.nodeValue = textnode;
							}
							kriyaEditor.settings.undoStack.push(this);
							} 
							if(textnode.match(/\s\s|&nbsp;&nbsp;|\s&nbsp;|&nbsp;\s/g)){
								textnode = textnode.replace(/\s\s|&nbsp;&nbsp;|\s&nbsp;|&nbsp;\s/g, ' ');
								this.nodeValue = textnode;
								kriyaEditor.settings.undoStack.push(this);
							}
						})
						}
						kriyaEditor.init.addUndoLevel('save update data');
				}
				
			},
			
			reorderAff: function(param, targetNode){
				$('[data-component="jrnlAuthorGroup_edit"] .collection[data-selector]').each(function(){
					kriya.general.reorderLinks($(this).attr('data-class'), $(this).attr('data-source'), param[0]);
				});
				//only for deleted authors
				if ($(param).attr('data-track') == 'del'){
					if ($(param).find('.jrnlGroupAuthorRef').length > 0){
						var rid = $(param).find('.jrnlGroupAuthorRef').attr('data-rid');
						if ($(kriya.config.containerElm + ' .jrnlGroupAuthorsGroup[data-id="' + rid + '"]').length > 0){
							$(kriya.config.containerElm + ' .jrnlGroupAuthorsGroup[data-id="' + rid + '"]').kriyaTracker('del');
							kriyaEditor.settings.undoStack.push($(kriya.config.containerElm + ' .jrnlGroupAuthorsGroup[data-id="' + rid + '"]'));
						}
					}
					if ($(param).find('.jrnlEqContribRef').length > 0){
						var rid = $(param).find('.jrnlEqContribRef').attr('data-rid');
						if ($(kriya.config.containerElm + ' .jrnlEqContribRef[data-rid="' + rid + '"]').length == 2){
							$(kriya.config.containerElm + ' .jrnlEqContribRef[data-rid="' + rid + '"]').each(function(){
								$(this).kriyaTracker('del');
								kriyaEditor.settings.undoStack.push($(this));
							});
							if ($(kriya.config.containerElm + ' .jrnlEqContrib[data-id="' + rid + '"]').length > 0){
								$(kriya.config.containerElm + ' .jrnlEqContrib[data-id="' + rid + '"]').kriyaTracker('del');
								kriyaEditor.settings.undoStack.push($(kriya.config.containerElm + ' .jrnlEqContrib[data-id="' + rid + '"]'));
							}
						}
					}
					//If the author is deleted then delete related corresponding affiliation
					var corID = $(param).find('.jrnlCorrRef').attr('data-rid');
					if(corID){
						var corAff = $(kriya.config.containerElm + ' [data-id="' + corID + '"]:not([data-track="del"],.del)');
						$(corAff).each(function(){
							$(this).attr('data-removed', 'true');
							$(this).kriyaTracker('del');
							kriyaEditor.settings.undoStack.push($(this));
						});
					}
				}
			},
			checkCollabAuthor: function(param, targetNode){
				if ($(targetNode).attr('data-class') == 'jrnlCollaboration'){
					if ($(targetNode).text() == ""){
						$(targetNode).closest('[data-wrapper]').find('.author-section,.orcid-section,.loop-section').removeClass('hidden');
					}else{
						$(targetNode).closest('[data-wrapper]').find('.author-section,.orcid-section,.loop-section').addClass('hidden');
						$(targetNode).closest('[data-wrapper]').find('.orcid-section').find('[data-class="jrnlOrcid"]').html('');
						$(targetNode).closest('[data-wrapper]').find('.author-section').find('[data-class="jrnlGivenName"],[data-class="jrnlSurName"]').html('')
						$(targetNode).closest('[data-wrapper]').find('.loop-section').find('[data-class="jrnlLoopLink"]').html('');
					}
				}else{
					var parentRow = $(targetNode).closest('.author-section');
					if (parentRow.find('[data-class="jrnlGivenName"]').text() == "" && parentRow.find('[data-class="jrnlSurName"]').text() == ""){
						$(targetNode).closest('[data-wrapper]').find('.collab-section,.group-author-section').removeClass('hidden');
					}else{
						$(targetNode).closest('[data-wrapper]').find('.collab-section,.group-author-section').addClass('hidden');
						$(targetNode).closest('[data-wrapper]').find('.collab-section').find('[data-class="jrnlCollaboration"]').html('')
					}
				}
			},
			objectMoveUp : function(param, targetNode){
				var component = targetNode.closest('li.collection-item');
				kriya.general.rearrangeObject(component, 'up');
			},
			objectMoveDown : function(param, targetNode){
				var component = targetNode.closest('li.collection-item');
				kriya.general.rearrangeObject(component, 'down');
			},
			checkField : function(param, targetNode){
				var checkField = targetNode.attr('data-check-field');
				var component = targetNode.closest('[data-type="popUp"]');
				if(!checkField){
					return;
				}
				var checkNode = component.find('[data-class="'+checkField+'"]');
				if(checkNode.length > 0){
					if (targetNode[0].nodeName == "INPUT" && (targetNode.is(":checked") == false)){
						checkNode.removeAttr('contenteditable');
					}else if (targetNode[0].nodeName == "INPUT" && (targetNode.is(":checked") == true)){
						checkNode.attr('contenteditable','true');
					}
				}	
			},
			validateElement : function(param, targetNode){
				var dataChild = $(targetNode)[0];
				kriya.validate.init(dataChild);
			},
			initInstitutionDropDown : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var keycode = kriya.evt.keyCode;
				$('ul.kriya-tags-suggestions').addClass('hidden');
				if (
					(keycode > 47 && keycode < 58) || // number keys
					keycode == 32 || keycode == 13 || // spacebar & return key(s) (if you want to allow carriage returns)
					keycode == 46 || keycode == 8 || // spacebar & return key(s) (if you want to allow carriage returns)
					(keycode > 64 && keycode < 91) || // letter keys
					(keycode > 95 && keycode < 112) || // numpad keys
					(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
					(keycode > 218 && keycode < 223) // [\]' (in order)
					){
						popper.find('[data-class="institution-id"]').text('');
						popper.find('.verified').prop('checked', false);
						var topCss = $(targetNode).outerHeight(true);
						var cp = $(targetNode).offset().left;
						$("#suggesstion-box").css({'top':topCss + 'px'});

						kriya.general.sendAPIRequest('readFunderDetails', {'keyword' : $(targetNode).text()}, function(res){
							if(res){
								var funder = JSON.parse(res);
								var funderList = $('<ul />');
								$.each(funder, function(i, obj){
									var li = $('<li />');
									li.attr('data-fundref-id', obj.doi);
									li.attr('data-message', "{'click':{'funcToCall': 'setFunder', 'channel':'components', 'topic':'general'}}");
									if(obj.prefLabel){
										li.html('<a>' + (obj.prefLabel) + '</a>');	
									}else if(obj.altLabel){
										li.html('<a>' + (obj.altLabel) + '</a>');	
									}
									
									funderList.append(li);
								});
								if(funderList.find('li').length > 0){
									//Remove the verification
									funderList.attr('class', 'dropdown-content');
									funderList.css({'display':'block','opacity':'1','position':'relative'});
									$("#suggesstion-box").html(funderList);
									$("#suggesstion-box").removeClass('hidden');
								}
							}
						});
					}
			},
			setFunder : function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				var funder = $(targetNode).text();
				var funderId = $(targetNode).attr('data-fundref-id');
				popper.find('[data-class="institution-id"]').text(funderId);
				popper.find('[data-class="institution"]').text(funder);
				//Verify true
				popper.find('.verified').prop('checked', true);

				$(targetNode).closest("#suggesstion-box").addClass('hidden');
			},
			addEthicNote: function(param, targetNode){
				var ethicGroup = $(kriya.config.containerElm + ' .jrnlEthicsGroup');
				if(ethicGroup.find('.jrnlEthicsFN[data-save-node] p:empty').length == 0){
					var ethicNode = kriya.general.convertTemplate('jrnlEthicsFN');
					if(ethicGroup.length > 0 && ethicNode){
						ethicGroup.append(ethicNode);
						ethicNode.attr('data-inserted', 'true');
						ethicNode.attr('data-save-node', 'true');
						moveCursor(ethicNode.find('p')[0]);
					}	
				}else{
					moveCursor(ethicNode.find('p')[0]);
				}
				
			},
			checkProofSelectors: function(param, targetNode){
				targetNode.closest('[data-wrapper]').find('[data-class="other-reason"]').addClass('hidden');
				if (targetNode.val() == "others"){
					targetNode.closest('[data-wrapper]').find('[data-class="other-reason"]').removeClass('hidden');
				}
			},
			contentEvt: function(param, targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;
				var range = rangy.getSelection();
				if (range.anchorNode == null && range.rangeCount == 0){//clicked some where outside the content - JAI 19-02-2018
					return false;
				}

				var blockNode = $(range.anchorNode).closest('[id]:not(span)');
				kriyaEditor.settings.startCaretID = blockNode.attr('id');
				kriyaEditor.settings.startCaretPos = getCaretCharacterOffsetWithin(blockNode[0]);

				kriya.selection = range.getRangeAt(0);
				kriya.selectedNodes = kriya.selection.getNodes();
					
				//to change all style menu and header to match context
				//select the tab in the header when click on the element
				eventHandler.menu.style.updateMenuBar();
				// to unselect seleted cells if selection is outside the table - aravind
				if($(kriya.selection.startContainer).closest('table').length == 0){
					kriya.actions.removeSelectionFromTableCell();
				}	
				//Get the closest element which has class	
				if($(target).closest('[class]').length > 0){
					target = $(target).closest('[class]')[0];
				}

				if($(target).closest('.jrnlKwdGroup,.jrnlAuthors,.jrnlCitation,.jrnlHistory,.jrnlPermission,.jrnlCopyrightStmt,.jrnlCorrAff,.jrnlDOI,.jrnlReDate,.jrnlRevDate,.jrnlAcDate,.jrnlPubDate,.jrnlKwdGroup,.jrnlKeywordPara,.jrnlAff').length > 0){
					if(!TomloprodModal.isOpen){ // to prevent hiding spell check results on click to front matter if spell check popup is open
						$(".icon-spell-check").parent().addClass('disabled')
						if($(".icon-spell-check").parent().hasClass('active')){
							eventHandler.menu.spellCheck.checkDocument('',$(".icon-spell-check").parent()[0]); //to remove spell check highlights
						}
					}
				}else if($(".icon-spell-check").parent().hasClass('disabled')){				
					$(".icon-spell-check").parent().removeClass('disabled')
				}
				
				//Condition modify by jagan - 30/01/18
				//Don't return false if target node is inline figure
				//i added this kriya.config.citationSelector to selector for when we click on deleted citation no need open edit citation popup.
				if (($(target).closest('[data-track="del"]').length > 0 && $(target).closest('.front,.jrnlFigBlock,.jrnlTblBlock,.award-group,'+kriya.config.citationSelector).length > 0 && $(target).closest('.jrnlInlineFigure').length == 0) || $(target).parent().closest('[data-type="predefined"]').length>0){//to avoid track changes in front matter elements like author, aff... - JAI 19-08-2017 and to avoid other context menu's on deleted objects
					return false;
				}
				//some times target come as null
				if(target && target.className.split(" ").length > 0){
					var componentName = target.className.split(" ")[0];
				}
				// updated below condtion to show reject option for deleted citation / aravind #847
				// bellow condition will not allow track changes(reject) for citation strings /#2588
				if($(target).closest('.del,.ins,[data-track="del"],[data-track="ins"],[data-track="sty"],[data-track="styrm"],.sty,.styrm').length > 0 && (($(target).closest(kriya.config.preventTrackComp).length <= 0)) && $(target).find(kriya.config.citationSelector).length <= 0 || $(target).closest('.jrnlInlineFigure[data-track="del"]').length > 0){
					var trackComp = $('#compDivContent [data-type="popUp"][data-component="trackChange"]');
					trackComp.find('.btn').removeClass('hidden');
					if(kriya.config.content.role=="author" && $(target).closest('[data-userid]').length>0 && $(target).closest('[data-userid]').attr('data-userid').indexOf('author')<0){
						return false;
					}else if(kriya.config.content.role=="author" && $(target).closest('[data-userid]').length > 0 && $(target).closest('[data-userid]').attr('data-userid').indexOf('author') >= 0){
						trackComp.find('.btn[data-name="accept_trackChange"]').addClass('hidden');
					}
					if($(target).closest(kriya.config.checkForComp).length>0){
						var compClass = $(target).closest(kriya.config.checkForComp).attr("class").split(" ")[0];
						if(!kriya.isUndefined(kriya.componentList[compClass]) && !kriya.isUndefined(kriya.componentList[compClass+'_edit'])){
							componentName = 'trackChange';
						}else if($(target).closest('.del,[data-track="del"]').length > 0){
							return false; // if the component data is deleted no need to show component - priya #180
						}
					}else{
						componentName = 'trackChange';
					}		
				}

				if (componentName != ""){
					while ((!kriya.isUndefined(kriya.componentList[componentName]) && !kriya.isUndefined(kriya.componentList[componentName+'_edit'])) && (target.nodeName.toLocaleLowerCase() != 'div')){
					   var target = target.parentNode;
					   if(target && target.className){//if target or target.class not there code will break we need check these are there are not
							var componentName = target.className.split(" ")[0];
					   }
					   //var componentName = target.className.split(" ")[0];
					}
					$('.templates > div:not(.tm-modal)').addClass('hidden');
					$('.resolve-query').remove();
					if (!kriya.isUndefined(kriya.componentList[componentName]) && kriya.isUndefined(kriya.componentList[componentName+'_edit'])){
						componentName += '_edit';
					}
					if (kriya.isUndefined(kriya.componentList[componentName]) && $('.templates [data-component="' + componentName + '"]').length == 0){
						eventHandler.components.general.buildComponent(componentName);
					}
					kriya.config.activeElements = $(target);
					if (kriya.isUndefined(kriya.componentList[componentName])){
						$('.activeElement[contenteditable]').removeAttr('contenteditable');
						$('.activeElement').removeClass('activeElement');
						//Remove the validation css in content added by vijayakumar on 9-11-2018
						$('#contentDivNode *[data-validation-error="true"]').removeAttr('data-validation-error', 'true');
						kriya.config.activeElements.addClass('activeElement');
						if ($('[data-component="'+componentName+'"]').attr('data-selection') != undefined){
							selection = rangy.getSelection();
							kriya.selection = selection.getRangeAt(0);
							if (kriya.selection.toString() == ""){
								return;
							}
						}
						if ($('[data-component="'+componentName+'"]').attr('data-type') != undefined || $('[data-component="'+componentName+'"]').attr('data-type') == "freetext"){
								$('[data-component="'+componentName+'"]').find('[data-input]').attr('data-freetext',true);
						}
						if ($('[data-component="'+componentName+'"]').find('[data-contenteditable]').length>0 || $('[data-component="'+componentName+'"]').find('[data-contenteditable]').attr("data-contenteditable")=="true"){
								$('[data-component="'+componentName+'"]').find('[data-contenteditable]').attr('contenteditable',true);
						}
						if($(target).closest('[prevent-editing]').length==0 && kriya.selection == ""){
							kriya['styles'].init($(target), componentName);
						}	
					}
				}

				//Show/hide the new and edit elements
				var component = $('[data-component="'+componentName+'"]');
				if(component.length > 0 && $(component).find('[data-pop-type]').length > 0){
					$(component).find('[data-pop-type]').addClass('hidden');
					$(component).find('[data-pop-type="edit"]').removeClass('hidden');	
				}
				// for context menu
				var componentName = 'contextMenu';
				selection = rangy.getSelection();
				if ($('#contentContainer').attr('data-state') != 'read-only' && $('[data-component="'+componentName+'"]').attr('data-selection') != undefined){
					kriya.selection = selection.getRangeAt(0);
					/*if (kriya.selection.toString() == ""){
						return;
					}*/
					if (kriya.selection.toString() != ""){
						kriya.config.activeElements = $(target);
						// If target is span element then pass the parent element - Rajesh
						//if target have deleted node no need to show contextmenu.for that added in if(&&!$(kriya.selection.getNodes()).hasClass('del'))-kirankumar
						if($(kriya.selection.getNodes()).last().closest('.del').length==0&&$(kriya.selection.getNodes()).first().closest('.del').length==0){
							/*if(kriya.config.activeElements[0].tagName == "SPAN" || kriya.config.activeElements[0].tagName == "B" || kriya.config.activeElements[0].tagName == "I" || kriya.config.activeElements[0].tagName == "U" || kriya.config.activeElements[0].tagName == "SUP" || kriya.config.activeElements[0].tagName == "SUB"){
								kriya['styles'].init($(target).parent(), componentName);
							}else{
								kriya['styles'].init($(target), componentName);
							}*/
							//When we select Formated text at that time insertquery option not come-kirankumar
							var targetele = $(target);
							var tags = ["SPAN","B","I","U","SUP","SUB","EM"];
							//while(tags.includes(targetele[0].tagName)){ some times hear returning this error Object does not support 'includes' property or method.
							while(tags.indexOf(targetele[0].tagName) >= 0){
								targetele=$(targetele[0]).parent();
							}
							kriya['styles'].init(targetele, componentName);
						}
						if ($('.templates [data-component]:visible').find('[data-menu-type]:not(.hidden)').length == 0){
							$('.templates [data-component]:visible').addClass('hidden');
						}
					}
				}
				if (kriya.selection.toString() == ""){
					componentName = 'pasteCitation';
				}
				//to avoid showing up cite now in front matter and in reference - jai
				if ($('[data-component="'+componentName+'"]').length > 0 && kriya.config.content.copiedCitation && $(target).closest('.front,.jrnlRefText').length == 0){
					if(kriya.config.preventTyping && ($(target).closest(kriya.config.preventTyping).length > 0)){
						kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Place the cursor in valid position.', icon: 'icon-warning2'});
					}else{
						kriya['styles'].init($(target), componentName);	
					}
				}

				
				var menuTarget = kriya.evt.target || kriya.evt.srcElement;
				if($('[data-component="pasteFootNoteLink"]').length > 0 && kriya.config.content.copiedTableLink && $(menuTarget).closest('.del,[data-track="del"]').length < 1){
					if($(menuTarget).closest('table').length > 0 || $(menuTarget).closest('.jrnlTblCaption').length > 0){
						var linkId = $(kriya.config.content.copiedTableLink).attr('data-citation-string');
						if(linkId){
							linkId = linkId.replace(/^\s+|\s$/g, '');
							linkId = linkId.replace(/_FN(\d+)/g, '');
							if($(target).closest('[data-id="BLK_' + linkId + '"]').length > 0){
								kriya['styles'].init($(target), 'pasteFootNoteLink');
							}
						}
					}else{
						kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Place the cursor in valid position.', icon: 'icon-warning2'});
					}
				}
				
				//For table floating menu
				/* Sathiyad : for LangTrans table Applicable for jb  */
				/*if($(menuTarget).closest('table').length > 0 && $(menuTarget).closest('.del,[data-track="del"]').length < 1){
					
					kriya['styles'].init($(menuTarget).closest('table'), 'tableMenu');	
				}*/
				

				//$('.class-highlight').text($(kriya.selection.startContainer).closest('*[class]').attr('class').split(' ')[0].replace(/^(jrnl|Ref)/,'').replace(/([a-z])([A-Z])/g, '$1 $2'));
				//if closest block node dont have class then class highlighter will swow empty string.
				$('.class-highlight').removeClass('hidden');
				if($(kriya.selection.startContainer).closest(kriya.config.blockElements.toString()).attr('class')){
					$('.class-highlight').text($(kriya.selection.startContainer).closest(kriya.config.blockElements.toString()).attr('class').split(' ')[0].replace(/^(jrnl|Ref)/,'').replace(/([a-z])([A-Z])/g, '$1 $2'));
				}else{
					$('.class-highlight').text('');
				}
			},
			buildComponent: function(componentName){
				var component = kriya.componentList[componentName];
				if (!component.type) return false;
				var comp = document.createElement('div');
				comp.setAttribute('data-type', component.type);
				comp.setAttribute('data-component', componentName);
				comp.setAttribute('class', 'hidden z-depth-2');
				if (component.class){
					comp.setAttribute('class', component.class + ' ' + comp.getAttribute('class'));
				}
				$(comp).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
				for (var btn in component.buttons){
					var newButton = document.createElement('span');
					newButton.innerHTML = btn;
					newButton.setAttribute('class', 'btn btn-hover');
					if (component.buttons[btn].channel) newButton.setAttribute('data-channel', component.buttons[btn].channel);
					if (component.buttons[btn].topic) newButton.setAttribute('data-topic', component.buttons[btn].topic);
					if (component.buttons[btn].event) newButton.setAttribute('data-event', component.buttons[btn].event);
					if (component.buttons[btn].message) newButton.setAttribute('data-message', component.buttons[btn].message);
					comp.appendChild(newButton);
				}
				
				$('.templates').append(comp);
			},
			resetRelatedArticle: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				popper.find('.searchRel').removeClass('hidden');
				popper.find('.resetRel').addClass('hidden');
				popper.find('[data-type="htmlComponent"]:not([data-template="true"])').html('');
				popper.find('[data-type="htmlComponent"][data-class="jrnlRelArtAuthor"]:not([data-template="true"])').remove();
			},
			searchRelatedArticle: function(param, targetNode){
				var queryString = $('<search>');

				var component = $(targetNode).closest('[data-type="popUp"]');
				var validated = kriya.saveComponent.validateComponents(component);
				if(!validated){
					return;
				}
				component.find('[data-type="htmlComponent"]').each(function(){
					var type = $(this).attr('data-class');
					if($(this).text() != "" && type != "jrnlRelArtAuthor"){
						var field = $('<field>');
						queryString.append(field);
						field.append($(this).text());
						field.attr('data-class', type);
					}
				});

				component.progress('Searching');
				
				var paremeter = {
					'data': queryString[0].outerHTML
				};
				kriya.general.sendAPIRequest('search_related_article', paremeter, function(res){
						component.progress('', true);
						var resTable = $('<table />')
						var thead = '<thead><tr><th width="5%"></th><th width="25%">Authors</th><th width="40%">Title</th><th width="15%">Source</th><th>Year</th><th>Doi</th></tr></thead>';
						resTable.append(thead);
						var tbody = $('<tbody />')
						var doiArr = [];
						var currentDOI = $('#contentDivNode').attr('data-org-doi');
						currentDOI = (currentDOI)?currentDOI.toLowerCase():'';
						if(res.kriya){
							var kriyaRes = $('<div>'+res.kriya+'</div>');
							if(kriyaRes.find('table').length > 0){
								kriyaRes.find('table tbody tr').each(function(){
									var doiText = $(this).find('.article-id').text();
									doiText = (doiText)?doiText.toLowerCase():'';
									if(doiText != currentDOI){
										doiArr.push(doiText);
										tbody.append(this);
									}
								});
							}
						}
						if(res.crossref){
							var corossRes = $('<div>'+res.crossref+'</div>');
							if(corossRes.find('table').length > 0){
								corossRes.find('table tbody tr').each(function(){
									var doiText = $(this).find('.RefDOI').text();
									doiText = (doiText)?doiText.toLowerCase():'';
									if(doiArr.indexOf(doiText) < 0 && doiText != currentDOI){
										tbody.append(this);
									}
								});
							}
						}
						resTable.append(tbody);
						if(resTable.find('tbody tr').length > 0){
							//change the class name and tag name of kriya seacrh output
							resTable.find('.journal-title').attr('class', 'jrnlRelArtSource');
							resTable.find('.article-title').attr('class', 'jrnlRelArtTitle');
							resTable.find('.article-id').attr('class', 'jrnlRelArtDOI');
							resTable.find('.volume').attr('class', 'jrnlRelArtVolume');
							resTable.find('.issue').attr('class', 'jrnlRelArtIssue');
							resTable.find('.fpage').attr('class', 'jrnlRelArtFPage');
							resTable.find('.lpage').attr('class', 'jrnlRelArtLPage');
							resTable.find('.elocation-id').attr('class', 'jrnlRelArtELocation');
							resTable.find('.contrib-group').find('aff,xref,contrib-id,email').remove();
							resTable.find('.contrib-group').find('contrib').contents().unwrap();
							resTable.find('.contrib-group').find('name').attr('class', 'jrnlRelArtAuthor').renameElement('span');
							resTable.find('.contrib-group').find('surname').attr('class', 'jrnlRelArtSurName').renameElement('span');
							resTable.find('.contrib-group').find('given-names').attr('class', 'jrnlRelArtGivenName').renameElement('span');
							resTable.find('.contrib-group').attr('class', 'jrnlRelArtAuthor');
							resTable.find('.subject').attr('class', 'data-art-type');
							
							resTable.find('.RefDOI').removeAttr('style');
							resTable.find('.RefID').remove();
							
							resTable.find('[class^="Ref"]').each(function(){
								var relClass = $(this).attr('class');
								relClass = relClass.replace(/^Ref/, 'jrnlRelArt');
								relClass = relClass.replace(/ArticleTitle$/, 'Title');
								relClass = relClass.replace(/JournalTitle$/, 'Source');
								$(this).attr('class', relClass)
							});

							
							var searchPop = $("*[data-type='popUp'][data-component='searchRef_edit']");
							searchPop.find('.com-save').attr('data-message', "{'click':{'funcToCall': 'setSearchedRel','channel':'components','topic':'general'}}");
							searchPop.find('.popupHead').html('Select The Related Article');

							kriya.popUp.showEmptyPopUp(searchPop, $);
							var refCollection = searchPop.find('.refSearchContainer');
							refCollection.html('');
							refCollection.html(resTable[0].outerHTML);
							refCollection.find('span.RefAuthor span.RefAuthor').contents().unwrap();
						}else{
							kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Unable to retrieve the data for related article', icon: 'icon-warning2'});
						}
				}, function(){
					component.progress('', true);
					kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Unable to retrieve the data for related article', icon: 'icon-warning2'});
				});
			},
			setSearchedRel: function(param, targetNode){
				var component = $(targetNode).closest('[data-type="popUp"]');
				var placeNode = $('[data-component="jrnlRelArt_edit"] [data-rel-art-type]:visible');
				if(component.find('.refSearchContainer tbody tr').length > 0){
					placeNode.find('[data-class="jrnlRelArtAuthor"]:not([data-template="true"])').remove();
					if(component.find('.refSearchContainer tbody tr.active').length > 0){
						$('[data-component="jrnlRelArt_edit"] .resetRel').removeClass('hidden');
						$('[data-component="jrnlRelArt_edit"] .searchRel').addClass('hidden');
					}
					component.find('.refSearchContainer tbody tr.active td[class]').each(function(){
						var valNode   = $(this);
						var nodeClass = valNode.attr('class');
						$(this).find('named-content').remove();
						if(placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]').length > 0){
							if (nodeClass == "jrnlRelArtAuthor"){
								var doiNodes = $(this).find('.jrnlRelArtAuthor');
							}else{
								var doiNodes = $(this);
							}
							$(doiNodes).each(function(i){
								var thisNode = $(this);
								var templateNode = placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]');
								var cloneTemplate = templateNode[0].cloneNode(true);
								$(cloneTemplate).removeAttr('data-template');
								$(cloneTemplate).attr('data-clone', 'true')
								if(i != 0){
									$(cloneTemplate).attr('data-added', 'true');
								}
								$(cloneTemplate).insertBefore(templateNode);
								if ($(cloneTemplate).find('[data-selector]').length > 0 && valNode.children().length > 0){
									$(cloneTemplate).find('[data-selector]').each(function(){
										var eleClass = $(this).attr('data-class');
										var nodeValue = $(thisNode).find('.' + eleClass).html();
										$(this).attr('data-focusout-data', nodeValue);
										$(this).html(nodeValue);
									});
								}else{
									var nodeValue = $(this).html();
									$(cloneTemplate).attr('data-focusout-data', nodeValue);
									$(cloneTemplate).html(nodeValue);
								}
							});
						}else{
							var nodeValue = $(this).html();
							if(nodeClass == "data-art-type" && placeNode.find('[data-name="articleTypeMatch"] [data-curr-art-type]').length > 0){
								var currArtType = $(kriya.config.containerElm + ' .jrnlArtType').text();
								//if the related article type match with treatAs then change article type from config
								if(placeNode.find('[data-name="articleTypeMatch"] [data-treat-as*="|' + currArtType + '|"]').length > 0){
									currArtType = placeNode.find('[data-name="articleTypeMatch"] [data-treat-as*="|' + currArtType + '|"]').text();
								}
								if(placeNode.find('[data-name="articleTypeMatch"] [data-treat-as*="|' + nodeValue + '|"]').length > 0){
									nodeValue = placeNode.find('[data-name="articleTypeMatch"] [data-treat-as*="|' + nodeValue + '|"]').text();
								}
								
								//if the current article type and related article type is math the condition then change article type								
								var ruleMatchNode = placeNode.find('[data-name="articleTypeMatch"] [data-curr-art-type="' + currArtType + '"][data-rel-art-type="' + nodeValue+ '"]');
								if(ruleMatchNode.length > 0){
									nodeValue = ruleMatchNode.text();
								}
							}
							placeNode.find('[data-class="' + nodeClass + '"]').attr('data-focusout-data', nodeValue);
							placeNode.find('[data-class="' + nodeClass + '"]').html(nodeValue);
						}
					});
					kriya.popUp.closePopUps(component);
				}
			}
		},
		citation: {
			insertCitation: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');

				//Return false and throw error if cursor placed in invalid position - jagan
				var target = null;
				if(kriya.selection && kriya.selection.startContainer){
					target = kriya.selection.startContainer;
				}
				if($(target).closest('.front,.jrnlRefText').length > 0 || kriya.config.preventTyping && ($(target).closest(kriya.config.preventTyping).length > 0)){
					popper.find('.selectedCitation option').html('');
					kriya.popUp.closePopUps(popper);

					kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Place the cursor in valid position to insert citation.', icon: 'icon-warning2'});
					return;
				}

				var floatLength = popper.find('.collection .collection-item.active').length;
				if(floatLength > 0){
					var selectedCitationNode = popper.find('.selectedCitation');
					if(selectedCitationNode.find(':selected').length > 0){
						var citationHTML = selectedCitationNode.find(':selected').html();
					}else{
						var citationHTML = selectedCitationNode.html();
					}
					
					citeNodes = $.parseHTML( citationHTML );
					//Remove the start and end punctuation if the active element is citation
					if(($(kriya.config.activeElements).attr('data-citation-string') || $(kriya.config.activeElements).hasClass('jrnlUncitedRef')) && citeNodes.length > 0){
						if(citeNodes[0].nodeType == 3){
							citeNodes.splice(0,1);
						}
						if(citeNodes[citeNodes.length-1].nodeType == 3){
							citeNodes.splice(citeNodes.length-1,1);
						}
					}
					

					var firstCitation = $(citeNodes).filter(kriya.config.citationSelector).first();
					if((caretIsFirstInSentence() && firstCitation.attr('data-citation-string')) || ($(kriya.config.activeElements).attr('data-citation-string') && nodeIsFirstInSentence($(kriya.config.activeElements)[0]))){
						var citeString = firstCitation.attr('data-citation-string');
						citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
						var citeConfig = citeJS.floats.getConfig(citeString[0]);
						if(citeConfig && (citeConfig.sentenceStartSingular || citeConfig.sentenceStartPlural)){
							var replaceCite = kriya.general.getNewCitation(citeString, kriya.config.containerElm, '', '', true);
							replaceCite = $.parseHTML(replaceCite);
							
							var index = citeNodes.indexOf(firstCitation[0]);
							citeNodes[index] = replaceCite[0];
						}
					}

					// remove the text node for citation reorder
					var citeElements = $(citeNodes).filter(function() {
						return this.nodeType === 1;
					}); 

					if(citeNodes.length > 0){
						kriya.general.insertCitationText(citeNodes,'false');
						$(citeElements).attr('data-cite-type', 'insert');
						var saveNode = $(citeElements).parents('[id]:first');
						$(citeElements).closest('.ins,[data-track="ins"]').attr('data-inserted', 'true');
						kriyaEditor.settings.undoStack.push(saveNode);
						//kriya.general.triggerCitationReorder($(citeElements), 'insert');


						//Add anchor attribute to the citation
						$(citeElements).each(function(){
							var citeString = $(this).attr('data-citation-string');
							if(citeString){
								citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
								for(var c=0;c<citeString.length;c++){
									var citeId = citeString[c];
									var anchorNode = citeJS.floats.addAnchorTag(citeString[c], kriya.config.containerElm);
									if(anchorNode){
										kriyaEditor.settings.undoStack.push(anchorNode);
									}

									//Move the float object when insert citation
									citeJS.general.moveFloat(citeId, kriya.config.containerElm);

									if(citeJS.settings.R.citationType == "1"){
										$(kriya.config.containerElm + ' .jrnlBibRef[data-citation-string=" ' + citeId + ' "]').not('.del, [data-track="del"]').each(function(i){
											var citeString = $(this).attr('data-citation-string');
											citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
											var clonedThis = $(this).clone();
											clonedThis.find('.jrnlQueryRef').remove();
											var pageNum  = clonedThis.find('.jrnlCitePageNum').text();
											clonedThis.find('.jrnlCitePageNum').removeAttr('data-content-type');
											clonedThis.find('.jrnlCitePageNum').removeAttr('id');
											var citeHtml = clonedThis.cleanTrackChanges().html();
											var isDirect = false;
											if($(this).html().match(/\(\d{4}[a-z]?\)/)){
												isDirect = true;
											}
											if(citeHtml.match(/^\d{4}[a-z]?$|^[a-z]$/)){ // to return if current element has year alone and continue loop
												return;
											}
											var isFollowing = (i != 0)?true:false;
											var abbrCollab  = (citeJS.settings.R.abbrCollab && parseFloat(citeJS.settings.R.abbrCollab) <= i)?true:false; 
											var newCiteHTML   = citeJS.floats.getReferenceHTML(citeString.join(' '),'','',isDirect, '', isFollowing, '',abbrCollab, pageNum);
											if(newCiteHTML && newCiteHTML != citeHtml){
												var clonQRef = $(this).find('.jrnlQueryRef').clone(true);
												$(this).html(newCiteHTML);
												$(this).prepend(clonQRef);
												kriyaEditor.settings.undoStack.push($(this));
											}
										});
									}

									var blockNode = $(kriya.config.containerElm + ' [data-id="' + citeId + '"]').closest('[data-id^="BLK_"]');
									if(blockNode.length > 0 && blockNode.attr('data-uncited') == "true"){
										var newLabelString = citeJS.floats.getCitationHTML([citeId], [], 'renumberFloats');
										blockNode.find('[class$="Caption"]').find('.label').html(newLabelString);
										blockNode.find('.floatLabel').html(newLabelString);
										kriya.general.updateRightNavPanel(blockNode.attr('data-id'), kriya.config.containerElm);
										blockNode.removeAttr('data-uncited');
										$('#navContainer [data-panel-id="' + blockNode.attr('data-id') + '"]').removeAttr('data-uncited');
										kriyaEditor.settings.undoStack.push(blockNode);
	
										$(kriya.config.containerElm + ' [data-citation-string*=" ' + citeId +' "]').each(function(){
											var newCiteString = $(this).attr('data-citation-string');
											newCiteString = newCiteString.replace(/^\s+|\s+$/g, '').split(' ');
											var newCiteText = citeJS.floats.getCitationHTML(newCiteString, [], 'getNewCitation');
											$(this).html(newCiteText);
											kriyaEditor.settings.undoStack.push(this);
										});
									}else if($(kriya.config.containerElm + ' [data-id="' + citeId + '"][data-uncited]').length > 0){
										blockNode = $(kriya.config.containerElm + ' [data-id="' + citeId + '"][data-uncited]');
										blockNode.removeAttr('data-uncited');
										$('#navContainer [data-panel-id="' + citeId + '"]').removeAttr('data-uncited');
										kriyaEditor.settings.undoStack.push(blockNode);
									}
								}
							}
						});
						
						if(citeJS.settings.showMsg == true && $('#contentContainer').attr('data-citation-trigger') == "true"){
							citeJS.floats.renumberConfirmation('insert-citation');	
						}
						kriyaEditor.init.addUndoLevel('insert-citation');
						citeJS.general.updateStatus();
					}
				}
				//remove the constrcuted citation from select box
				popper.find('.selectedCitation option').html('');
				kriya.popUp.closePopUps(popper);
			},
			deleteCitePopup : function(param, targetNode){
				var paramString = (param) ? "'param':"+JSON.stringify(param) : '';
				if($(kriya.config.activeElements).closest('[class*=Ref]').length > 0){
					var activeElementText = $(kriya.config.activeElements).clone(true).cleanTrackChanges().text();
					var citeString = $(kriya.config.activeElements).attr('data-citation-string').replace(/^\s+|\s+$/g, '');
					citeString     = citeString.split(/\s+/);

					var citeText = kriya.general.getNewCitation([param.citeId], '', '', false);
					var displyMsg = 'Are you sure you want to delete the citation ';
					if(citeText){
						displyMsg += $(citeText).text();
					}
					if(citeString.length > 1){
						displyMsg += ' from '+activeElementText;
					}
					displyMsg += '?';

					var settings = {
						'icon'  : '<i class="material-icons">warning</i>',
						'title' : 'Confirmation Needed',
						'text'  : displyMsg,
						'size'  : 'pop-sm',
						'buttons' : {
							'cancel' : {
								'text' : 'No',
								'class' : 'btn-danger pull-right',
								'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
							},
							'ok' : {
								'text' : 'Yes',
								'class' : 'btn-success pull-right',
								'message' : "{'click':{'funcToCall': 'deleteCitation','channel':'components','topic':'citation'," + paramString + "}}"
							}
						}
	 				}
	 				kriya.actions.confirmation(settings);
 				}
			},
			/**
			 * Function to delete citation
			 */
			deleteCitation: function(param, targetNode){
				var popper = $(targetNode).closest('[data-type="popUp"]');
				kriya.popUp.closePopUps(popper);
				//#333 - Frontiers| Reference Citation| Deleting Reference Citation - Rajasekar T(rajasekar.t@focusite.com)
				//Deleting 1 citation in multi-citations - the other citation strings to be updated 
			if($(kriya.config.activeElements)[0].nextElementSibling && $(kriya.config.activeElements)[0].nextElementSibling.hasAttribute('data-citation-string')){
					var nextCitation = $(kriya.config.activeElements)[0].nextElementSibling;
					var nextCitationRef = nextCitation.getAttribute('data-citation-string').replace(/^ | $/g, '').split(' ');
					
					nextCitation.outerHTML = kriya.general.getNewCitation(nextCitationRef, '', '').replace(/^[(]|[)]$/g, '');
					}
					
	 //End of #333
				if($(kriya.config.activeElements)[0].hasAttribute('data-citation-string')){
					kriya.general.deleteCitationElement(kriya.config.activeElements, param.citeId);
					
					var currentCitationLength = $(kriya.config.containerElm + ' [data-citation-string*=" ' + param.citeId + ' "]').filter(function(){
						if($(this).closest('.del, [data-track="del"], .jrnlDeleted').length == 0){
							return this;
						}
						return false;
					}).length;

					if(currentCitationLength == 0){
						var blockNode = $(kriya.config.containerElm + ' [data-id="' + param.citeId + '"]');
						blockNode = (blockNode.closest('[data-id^="BLK_"]').length > 0)?blockNode.closest('[data-id^="BLK_"]'):blockNode;
						blockNode.attr('data-uncited', 'true');
						$('#navContainer [data-panel-id="' + blockNode.attr('data-id') + '"]').attr('data-uncited', 'true');
						kriyaEditor.settings.undoStack.push(blockNode);
					}

					//Add anchor tag to the citation
					var anchorNode = citeJS.floats.addAnchorTag(param.citeId, kriya.config.containerElm);
					if(anchorNode){
						kriyaEditor.settings.undoStack.push(anchorNode);
					}

					//move the float when delete citation
					citeJS.general.moveFloat(param.citeId, kriya.config.containerElm);
				}
				
				if(param && param.afterDelete){
					var execFn = getStringObj(param.afterDelete);
					execFn($(kriya.config.activeElements));
				}
				kriyaEditor.init.addUndoLevel('delete-citation');
				
				eventHandler.query.action.regenerateActionQuery();
				if(citeJS.settings.showMsg == true && $('#contentContainer').attr('data-citation-trigger') == "true"){
					citeJS.floats.renumberConfirmation('delete-citation');
				}
				citeJS.general.updateStatus();
			},
			unmarkCitation: function(param, targetNode){
				if (targetNode.closest('.query-div').length > 0){
					var rid = targetNode.closest('.query-div').attr('id');
					if ($('[data-rid="' + rid + '"]').length > 0){
						kriya.config.activeElements = $('[data-rid="' + rid + '"]').parent();
					}else{
						return;
					}
				}
				var activeElement = kriya.config.activeElements;
				//if (activeElement.hasClass('jrnlUncitedRef')){
				var parentNode = activeElement.parent();
				if (kriya.config.activeElements.find('.jrnlQueryRef').length > 0){
					//add reply if is orphan citation - auto reply as not a citation
					var qid = $(kriya.config.activeElements).find('.jrnlQueryRef').attr('data-rid');
					var queryNode = $('.query-div#' + qid);
					if (queryNode.length > 0 && queryNode.attr('data-query-action') == "uncited-citation"){
						var target = $(queryNode).find('.query-holder').first();
						$(queryNode).find('.query-action').remove();
						$(queryNode).attr('data-remove-edit','true');
						eventHandler.query.action.addReply({'undoLevel':'false','content':'<i>This is not a citation</i>', 'reply' : true}, target); //prevent undolevel function call for #346 - priya
					}
					var endQuery = $(kriya.config.activeElements).find('.jrnlQueryRef').clone(true);
					endQuery.attr('data-type', 'end').removeAttr('id').removeAttr('data-query-for').removeAttr('data-channel').removeAttr('data-topic').removeAttr('data-event').removeAttr('data-message').removeAttr('data-query-ref');
					activeElement.append(endQuery)
				}
				activeElement.contents().unwrap();
				$(targetNode).closest('[data-component]').addClass('hidden');
				kriyaEditor.settings.undoStack.push(parentNode[0]);
				kriyaEditor.init.addUndoLevel('unwrap-citation');
				//}
			},
			removeProcessingInstruction: function(param, tabeNode){
				var popper = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				var currNode  = kriya.xpath(nodeXpath);				
				$(currNode).attr('data-removed','true');				
				var saveNode = $(currNode).closest('p');
				$(currNode).remove();
				kriyaEditor.settings.undoStack.push(saveNode);
				kriyaEditor.init.addUndoLevel('delete processing instruction');				
				kriya.popUp.closePopUps(popper);
			},
			saveProcessingInstruction: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var piTarget = $(popper).find('[data-class="jrnlPiTarget"]').val();
				var piInstruction = $(popper).find('[data-class="jrnlPiInstruction"]').text();
				if($(popper).find('[data-pop-type="new"]').hasClass('hidden')){
					var nodeXpath = popper.attr('data-node-xpath');
					var currNode  = kriya.xpath(nodeXpath);
					$(currNode).attr('data-target',piTarget);
					$(currNode).attr('data-instruction',piInstruction);				
					kriyaEditor.settings.undoStack.push(currNode);
				}else{
					var newNode = document.createElement('span');
					$(newNode).addClass('jrnlPi');					
					$(newNode).attr('data-target',piTarget).attr('id',uuid.v4());
					$(newNode).attr('data-instruction',piInstruction);				
					var editNode = document.createTextNode('\u00A0');
					$(newNode).append(editNode);				
					kriya.selection.insertNode(newNode);				
					kriyaEditor.settings.undoStack.push(newNode);
				}								
				kriyaEditor.init.addUndoLevel('insert processing instruction');
				kriya.popUp.closePopUps(popper);
			},
			addCostumAttributes: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var newFields = $(popper).find('.template').clone();
				$(newFields).removeClass('hidden').removeClass('template');				
				$('[data-component="jrnlModifyAttributes"]').find('[data-container="true"]').append($(newFields));
			},
			saveModifiedAttributes: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');				
				var nodeXpath = popper.attr('data-node-xpath');				
				var currNode  = kriya.xpath(nodeXpath);								
				$(popper).find('[data-container="true"] > div').each(function(){
					if(($(this).find('[data-type="attribute"]').text()!='')&&($(this).find('[data-type="value"]').text()!='')){						
						if($(this).find('[data-type="attribute"]').text() == 'id'){
							$(currNode).attr('data-costumer-'+$(this).find('[data-type="attribute"]').text(),$(this).find('[data-type="value"]').text());
						}else{
							$(currNode).attr('data-'+$(this).find('[data-type="attribute"]').text(),$(this).find('[data-type="value"]').text());
						}
					}
				});
				$(popper).find('[data-predefinedAttr="true"]').each(function(){
					$(this).find('>div').each(function(){
						if(($(this).find('[data-type="attribute"]').text()!='')&&($(this).find('[data-type="value"]').text()!='')){
							if($(this).find('[data-type="attribute"]').text() == 'content-type'){
								$(currNode).attr('data-cont-type',$(this).find('[data-type="value"]').text());
							}else{
								$(currNode).attr('data-'+$(this).find('[data-type="attribute"]').text(),$(this).find('[data-type="value"]').text());
							}
						}	
					});
				});				
				if($(popper).find('.lang-dropdown').val()){
					$(currNode).attr('data-lang', $(popper).find('.lang-dropdown').val());
				}else{
					$(currNode).removeAttr('data-lang');
				}				
				if($(currNode).hasClass('jrnlTblCaption') || $(currNode).closest('.jrnlTblCaption').length > 0){
					currNode = $(currNode).closest('.jrnlTblCaption');
					if($(popper).find('.content-type-dropdown').val()){
						$(currNode).attr('data-cont-type', $(popper).find('.content-type-dropdown').val());
					}else{
						$(currNode).removeAttr('data-cont-type');
					}
				}
				kriyaEditor.settings.undoStack.push(currNode);
				kriyaEditor.init.addUndoLevel('modified attributes of selected node');
				kriya.popUp.closePopUps(popper);
			},
			removeAttributes: function(param, targetNode){				
				var popper    = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				var currSelectedNode  = kriya.xpath(nodeXpath);				
				$(currSelectedNode).removeAttr($(targetNode).closest('div[class="flex"]').find('[data-type="attribute"]').text());
				if($(targetNode).closest('div[class="flex"]').find('.lang-dropdown').length > 0){
					$(targetNode).closest('div[class="flex"]').find('.lang-dropdown').val('');
				}else{
					$(targetNode).closest('div[class="flex"]').remove();
				}
			},
			saveCitationPageNo: function(param, targetNode){
				var popper    = $(targetNode).closest('[data-component]');
				var pageNum   = popper.find('[data-class="jrnlCitePageNum"]').text();
				var nodeXpath = popper.attr('data-node-xpath');
				var citeNode  = kriya.xpath(nodeXpath);
				if($(citeNode).length > 0 && $(citeNode).attr('data-citation-string')){
					citeJS.general.updatePageNumInCitation(citeNode, pageNum);
					kriyaEditor.init.addUndoLevel('save-page-no', citeNode);
				}
				kriya.popUp.closePopUps(popper);
			},
			saveCitationPartlabel: function(param, targetNode){
				var popper    = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				var citeNode  = kriya.xpath(nodeXpath);
				if($(citeNode).length > 0 && $(citeNode).attr('data-citation-string')){

					var cloneNode = $(citeNode).clone(true);
					cloneNode.find('.del,[data-track="del"]').remove();
					cloneNode.find('.ins').contents().unwrap();
					var citeHtml  = cloneNode.html();
					var partLabelObj = kriya.general.getCitationLabel($(citeNode));

					var linkedNodes = popper.find('[data-type="getLinkedCitations"]:not([data-template="true"])');
					linkedNodes.each(function(){
						var citeID = $(this).attr('data-cite');
						var partLabelEle = $(this).find('[data-class="partLabel"]:not([data-template="true"])');
						if(citeID.match(/\d+$/)){
							var citeNum = citeID.match(/\d+$/)[0];
							//Construct the part label text
							var partLabelText = '';
							var citeConfig = citeJS.floats.getConfig(citeID);
							partLabelEle.each(function(){
								// to handle custom punctution in part label - aravind
								if(citeConfig.partLabelSpacing && citeConfig.partLabelSpacing != 'false'){
									if($(this).text()){
										partLabelText = partLabelText + $(this).html() + ','+citeConfig.partLabelSpacing;
									}
								}else{
									if($(this).text()){
										partLabelText = partLabelText + $(this).html() + ',';
									}
								}
							});
							// to handle custom punctution in part label - aravind
							if(citeConfig.partLabelSpacing && citeConfig.partLabelSpacing != 'false'){
								var puncPattern = new RegExp(citeConfig.partLabelSpacing + '$')
								partLabelText = partLabelText.replace(puncPattern, '');
							}
							partLabelText = partLabelText.replace(/[\s\,]$/, '');
							var newLabel = citeNum+partLabelText;
							//replace the part label with number
							if(partLabelObj[citeNum] && partLabelObj[citeNum]['match-val'] && newLabel != partLabelObj[citeNum]['match-val']){
								if(citeID.match(/\-/g)){
									var citeIdText = citeID.replace(/\d+$/g, '');
									var citeTextWithoutNum = citeJS.floats.getCitationHTML([citeIdText]);
									if(citeTextWithoutNum){
										citeHtml = citeHtml.replace(citeTextWithoutNum, '');
										citeHtml = citeHtml.replace(partLabelObj[citeNum]['match-val'], newLabel); //replace the new label
										citeHtml = citeTextWithoutNum + citeHtml;
									}
								}else{
									citeHtml = citeHtml.replace(partLabelObj[citeNum]['match-val'], newLabel);
								}
							}
						}
					});
					//if new cite html not equal to old then update the new cite html
					if(citeHtml != $(citeNode).html()){
						citeHtml = $.parseHTML(citeHtml);
						//delete the exsting text
						$(citeNode).contents().kriyaTracker('del');
						$(citeNode).append(citeHtml);
						$(citeHtml).kriyaTracker('ins');
						kriyaEditor.init.addUndoLevel('add-part-label', citeNode);
					}
				}
				kriya.popUp.closePopUps(popper);
			},
			deletePartLabel: function(param, targetNode){
				var selector = $(targetNode).closest('[data-type="popUp"]').attr('data-node-xpath');
				var dataChild = kriya.xpath(selector);
				$(dataChild).remove();
				$(targetNode).closest('[data-type="popUp"]').addClass('hidden');
			},
			addPartLabel: function(param, targetNode){
				componentName = 'partLabel_add';
				target = $(targetNode)[0];
				if (target.hasAttribute('data-deleted')) return false;
				if (target.textContent != ""){
					if (kriya.isUndefined(kriya.componentList[componentName])){
						kriya['styles'].init($(target), componentName);
					}
				}		
			},
			possessiveCitation: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var nodeXpath = popper.attr('data-node-xpath');
				var dataNode = kriya.xpath(nodeXpath);
				if($(dataNode).length > 0){
					if(param == undefined || param.possessive == undefined){ // to handle duplication in inserting possessive citation
						if($(dataNode).attr('data-possessive') != undefined){
							$(dataNode).removeAttr('data-possessive');
						}else{
							kriya.popUp.closePopUps(popper);
							return false;
						}
					}else if(param && param.possessive == 'true'){
						if($(dataNode).attr('data-possessive') != undefined){
							kriya.popUp.closePopUps(popper);
							return false;
						}
					}
					var citeString = $(dataNode).attr('data-citation-string');
					citeString = citeString.replace(/^\s+|\s+$/g, '');
					var currentCiteText = $(dataNode).clone().cleanTrackChanges().html();
					
					var isDirect = (currentCiteText.match(/\(\d{4}[a-z]?.*\)/i))?true:false;
					var isPossessive = (param && param.possessive == "true")?true:false;
					
					var abbrCollab = false;
					if(citeJS.settings.R.abbrCollab){
						$(kriya.config.containerElm + ' [data-citation-string*=" ' + citeString +' "]').each(function(i){
							if(dataNode[0] == this){
								abbrCollab  = (citeJS.settings.R.abbrCollab && parseFloat(citeJS.settings.R.abbrCollab) <= i)?true:false; 		
								return false;
							}
						});
					}
					
					//Update page number for possessive citaion by tamil selvan on 13-Jun-19
					var pageNo;
					var temp = $(dataNode).find('.jrnlCitePageNum:not([data-track="del"]):last').text();
					if(temp && !isNaN(parseInt(temp))){
						pageNo = parseInt(temp);
					}
					var newCiteString = citeJS.floats.getReferenceHTML(citeString, $(dataNode), '', isDirect, '', '', isPossessive, abbrCollab, pageNo);

					$(dataNode[0].childNodes).each(function(){ // to add track changes while inserting possessive citation
						if(!$(this).hasClass('del') && $(this).attr('data-track') != 'del'){
							$(this).kriyaTracker('del');
						}
					});
					var newElement = document.createElement('span');
					$(newElement).append(newCiteString);
					$(dataNode).append($(newElement).kriyaTracker('ins'));
					if(isPossessive){
						$(dataNode).attr('data-possessive', 'true');
					}
					kriyaEditor.settings.undoStack.push($(dataNode).closest('[id]'));
					kriyaEditor.init.addUndoLevel('possessive-citation');

				}
				kriya.popUp.closePopUps(popper);
			},
			citationConfirmation: function(param, targetNode){
				var renumberArray = param.renumberClass;
				if(renumberArray){
					renumberArray = renumberArray.split(',');
					kriya.general.triggerCitationReorder('', '', renumberArray);
					
					if(param && param.action){
						if(typeof(eventHandler.menu.export[param.action]) == "function"){
							kriya.config.exportOnsave = param.action;
						}
						if(targetNode.closest('.kriya-notice').length > 0){
							var parentNode = targetNode.closest('.kriya-notice');
							var reorderedSeq = $('<span>The sequence of citations has changed and the referred objects are reordered<br/></span>');
							parentNode.find('.renumberStatus').each(function(){
								reorderedSeq.append($('<br/>'));
								reorderedSeq.append($(this).html().replace(' will be ', ' has been '));
							})
							
							//var queryTo = (kriya.config.content.role == "author")?'Publisher':'Author';
							var queryTo = 'Publisher';
							//add query after re-ordering in body's first element
							var qid = eventHandler.query.action.createQueryDiv(queryTo, 'info');
							$('#' + qid).find('.query-from').html(kriyaEditor.user.name);
							var queryContent = $('#' + qid).find('.query-content');
							queryContent.attr('data-assigned-by', kriyaEditor.user.name);
							$(queryContent).html(reorderedSeq[0].outerHTML);
							$('#' + qid).attr('data-type', 'citation');

							/*var startNode = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
							$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', 'author');
							$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
							
							$(startNode).attr('data-query-type', 'info');
							var actionNode = $('.body').children()[0];
							$(actionNode).prepend(startNode);
							kriyaEditor.settings.undoStack.push(actionNode[0]);*/

							kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);
						}
					}
					kriyaEditor.init.addUndoLevel('citation-reorder');
					kriya.removeNotify(targetNode);
				}
			},
			deleteObjects: function(param, targetNode){
				if (param.type != 'jrnlRefText'){
					var activeElement = kriya.config.activeElements;
				}else{
					var activeElement = kriya.config.activeElements;
				}
				var objID = activeElement.attr('id');
				var citations = $(kriya.config.containerElm).find('*[data-citation-string*=" '+ objID + ' "]');
				if (citations.length == 1){
					
				}
				$(citations).each(function(){
					console.log($(this).attr('data-citation-string'));
				});
				activeElement.attr('data-deleted-object', 'true').attr('data-old-class', activeElement.attr('class'));
			},
			highlightOnScrollBar: function(param, targetNode){
				var floatBlock = $(targetNode).closest('[data-id^="BLK_"]');
				if(floatBlock.length > 0){
					if(floatBlock.closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').length > 0){
						floatBlock = floatBlock.closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup');
					}
					var floatId = floatBlock.attr('data-id');
					floatId = floatId.replace(/^BLK_/g, '');
					var blockCitations = $(kriya.config.containerElm).find('[data-citation-string*=" '+ floatId +' "]');
					elementOnScrollBar(blockCitations, 'data-citation-string', '');
				}
			},
			//#476 - All Customers | Reference and Content | Copy paste Numeric Values - Rajasekar T(rajasekar.t@focusite.com)
			// retaining citation when the user click convert to citation on notification
			retainCitation: function(obj){
				$('#'+obj.id).removeClass('checkIfRef');
				$('#'+obj.id).addClass('jrnlBibRef');
			},					 
			//End of #476	
			citeNow: function(param, targetNode){
				var blockNode = null;
				var dataId = "";
				if($(targetNode).closest('[data-node-xpath]').length > 0){
					var nodeXpath = $(targetNode).closest('[data-node-xpath]').attr('data-node-xpath');
					blockNode = kriya.xpath(nodeXpath);
					dataId = $(blockNode).attr('data-id');
				}else if($(targetNode).closest('[data-id^="BLK_"]').length > 0){
					blockNode = $(targetNode).closest('[data-id^="BLK_"]');
					dataId = $(blockNode).attr('data-id');
				}else if($(targetNode).closest('[data-panel-id]').length > 0){
					dataId = $(targetNode).closest('[data-panel-id]').attr('data-panel-id');
					blockNode = $(kriya.config.containerElm).find('[data-id="'+ dataId +'"]');
				}else if($(targetNode).closest('[data-float-id]').length > 0){
					dataId = $(targetNode).closest('[data-float-id]').attr('data-float-id');
					blockNode = $(kriya.config.containerElm).find('[data-id="'+ dataId +'"]');
				}

				//get the table/figure bloack group id
				/*if($(blockNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').length > 0){
						dataId = $(blockNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').attr('data-id');
						dataId = dataId.replace(/BLK_/g, '');
				   }*/
				if(dataId){
					dataId = dataId.replace(/BLK_/g, '');
					// Get the indirect citation - Rajesh
					var citeText = kriya.general.getNewCitation([dataId], '', '');
					var citeNode = $.parseHTML(citeText);
					$('.indirectCitation .currentCitation').html(citeNode);
					// Get the direct citation - Rajesh
					var citeText = kriya.general.getNewCitation([dataId], '', true, false);
					var citeNode = $.parseHTML(citeText);
					$('.directCitation .currentCitation').html(citeNode);
					// Hide citation for numeric reference - Rajesh  
					if($(citeNode).find('sup').length > 0){
						$('.directCitation').hide();
					}else{
						$('.directCitation').show();
					}
					kriya.config.content.copiedCitation = citeText;
					if(citeText){
																  
																																			 
						kriya.removeAllNotify();
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : 'To insert a citation to this Reference/Figure/Table/Video/Supplementary file, please click on the desired location in the text, and select ‘Insert’ when prompted.',
							icon : 'icon-info'
						});
					}
				}
			},
			skipCiteNow: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				kriya.config.content.copiedCitation = false;
				popper.addClass('hidden');
				kriya.removeAllNotify();
			},
			insertCopiedCitation: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				sel = rangy.getSelection();
				range = sel.getRangeAt(0);
				var saveNode = range.commonAncestorContainer;
				// Get current citation html - Rajesh 
				kriya.config.content.copiedCitation = $.parseHTML($(targetNode).find('.currentCitation').html());
				if(kriya.config.content.copiedCitation){
					var citeNodes = $(kriya.config.content.copiedCitation).clone(true);

					if(caretIsFirstInSentence() && citeNodes.filter(kriya.config.citationSelector).attr('data-citation-string')){
						var citeString = citeNodes.filter(kriya.config.citationSelector).attr('data-citation-string');
						citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
						var citeConfig = citeJS.floats.getConfig(citeString[0]);
						if(citeConfig && (citeConfig.sentenceStartSingular || citeConfig.sentenceStartPlural)){
							citeNodes = kriya.general.getNewCitation(citeString, kriya.config.containerElm, '', $(targetNode).hasClass('indirectCitation'), true);
							citeNodes = $.parseHTML(citeNodes);
						}
					}

					$(citeNodes).attr('data-inserted', 'true');
					$(citeNodes).attr('data-cite-type', 'insert');
					if ($(saveNode).closest('*[class$=Ref], *[class*=Ref ]').length > 0){
						//citeNodes.unshift($(saveNode).closest('*[class$=Ref], *[class*=Ref ]')[0])
						var citationNode = $(citeNodes).filter(function(){
							return this.nodeType != 3
						})
						//data-citation-string this attr come as empty at that time code is breaking code
						//var floatIds = ($(saveNode).closest('*[class$=Ref], *[class*=Ref ]').attr('data-citation-string').replace(/^ | $/g, '') + $(citationNode).attr('data-citation-string').replace(/ $/g, '')).split(' ');
						var floatIds = '';
						if($(saveNode).closest('*[class$=Ref], *[class*=Ref ]').attr('data-citation-string')){
							floatIds = $(saveNode).closest('*[class$=Ref], *[class*=Ref ]').attr('data-citation-string').replace(/^ | $/g, '');
						}
						if($(citationNode).attr('data-citation-string')){
							floatIds += $(citationNode).attr('data-citation-string').replace(/ $/g, '');
						}
						floatIds = floatIds.length >0 ? floatIds.split(' ') : undefined;
						citeNodes = kriya.general.getNewCitation(floatIds, kriya.config.containerElm, '');
						citeNodes = $.parseHTML(citeNodes);
						if(citeNodes[0].nodeType == 3){
							citeNodes.splice(0,1);
						}
						if(citeNodes[citeNodes.length-1].nodeType == 3){
							citeNodes.splice(citeNodes.length-1,1);
						}
					}
					kriya.general.insertCitationText(citeNodes,'false');

					//if the citation insert for uncited float then update the citation text and float label
					var msg = '';
					$(kriya.config.content.copiedCitation).each(function(){
						if(this.nodeType == 3){
							return;
						}

						var citeText = $(this).html();
						if(!$(this).hasClass('jrnlBibRef')){
							var citeString = $(this).attr('data-citation-string');
							citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
							for(c = 0; c < citeString.length;c++){
								var citeId   = citeString[c];

								//Add anchor tag to citation
								var anchorNode = citeJS.floats.addAnchorTag(citeId, kriya.config.containerElm);
								//commenting adding the inserted citation to undo stack, as we have to send the inserted parargraph to save - JAI 12-March-2019
								/*if(anchorNode){
									kriyaEditor.settings.undoStack.push(anchorNode);
								}*/

								//move the float when insert citation
								citeJS.general.moveFloat(citeId, kriya.config.containerElm);
								
								var blockNode = $(kriya.config.containerElm + ' [data-id="' + citeId + '"]').closest('[data-id^="BLK_"]');
								if(blockNode.length > 0 && blockNode.attr('data-uncited') == "true"){
									var newLabelString = citeJS.floats.getCitationHTML([citeId], [], 'renumberFloats');
									blockNode.find('[class$="Caption"]').find('.label').html(newLabelString);
									blockNode.find('.floatLabel').html(newLabelString);
									kriya.general.updateRightNavPanel(blockNode.attr('data-id'), kriya.config.containerElm);
									blockNode.removeAttr('data-uncited');
									$('#navContainer [data-panel-id="' + blockNode.attr('data-id') + '"]').removeAttr('data-uncited');
									kriyaEditor.settings.undoStack.push(blockNode);

									$(kriya.config.containerElm + ' [data-citation-string*=" ' + citeId +' "]').each(function(){
										var newCiteString = $(this).attr('data-citation-string');
										newCiteString = newCiteString.replace(/^\s+|\s+$/g, '').split(' ');
										var newCiteText = citeJS.floats.getCitationHTML(newCiteString, [], 'getNewCitation');
										$(this).html(newCiteText);
										kriyaEditor.settings.undoStack.push(this);
									});
								}

								msg += 'The float object ' + citeText + ' was cited as ' + citeJS.floats.getCitationHTML([citeId], [], 'getNewCitation');
								
							}
						}else if($(this).hasClass('jrnlBibRef')){
							//if(citeJS.settings.R.citationType == "1"){
								var citeString = $(this).attr('data-citation-string');
								citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
								for(c = 0; c < citeString.length;c++){
									var citeId = citeString[c];

									if($(kriya.config.containerElm + ' [data-id="' + citeId + '"][data-uncited]').length > 0){
										var blockNode = $(kriya.config.containerElm + ' [data-id="' + citeId + '"][data-uncited]');
										blockNode.removeAttr('data-uncited');
										$('#navContainer [data-panel-id="' + citeId + '"]').removeAttr('data-uncited');
										kriyaEditor.settings.undoStack.push(blockNode);
									}
									if(citeJS.settings.R.citationType == "1"){//added by kirankumar:-when i insert cite for uncited reference at that time uncited count is not update
									$(kriya.config.containerElm + ' .jrnlBibRef[data-citation-string=" ' + citeId + ' "]').not('.del, [data-track="del"]').each(function(i){
										var citeString = $(this).attr('data-citation-string');
										citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
										var pageNum  = $(this).find('.jrnlCitePageNum').text();
										var cloneNode = $(this).clone(true);
										cloneNode.find('.jrnlCitePageNum').removeAttr('data-content-type');
										cloneNode.find('.jrnlCitePageNum').removeAttr('id');
										var citeHtml  = cloneNode.cleanTrackChanges().html();
										var isDirect = false;
										if($(this).html().match(/\(\d{4}[a-z]?\)/)){
											isDirect = true;
										}
										var isFollowing = (i != 0)?true:false;
										var abbrCollab  = (citeJS.settings.R.abbrCollab && parseFloat(citeJS.settings.R.abbrCollab) <= i)?true:false; 
										var newCiteHTML = citeJS.floats.getReferenceHTML(citeString.join(' '),'','',isDirect, '', isFollowing, '',abbrCollab, pageNum);
										if(newCiteHTML && newCiteHTML != citeHtml){
											$(this).html(newCiteHTML);
											kriyaEditor.settings.undoStack.push($(this));
										}
									});
								}
							}
						}
					});
					kriya.removeAllNotify();
					if(msg){
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : msg,
							icon : 'icon-info'
						});
					}
					
					kriya.config.content.copiedCitation = false;
				}
				
				kriyaEditor.settings.undoStack.push(saveNode);
				kriyaEditor.init.addUndoLevel('insert-citation');
				popper.addClass('hidden');
				if(citeJS.settings.showMsg == true && $('#contentContainer').attr('data-citation-trigger') == "true"){
					citeJS.floats.renumberConfirmation('insert-citation');
				}
				citeJS.general.updateStatus();
			},
			changeMsgStatus: function(param, targetNode){
				var checkBox = $(targetNode).parent().find('input[type="checkbox"]');
				if(checkBox.is(':checked')){
					citeJS.settings.showMsg = false;
				}else{
					citeJS.settings.showMsg = true;
				}
			},
			initCitationConfig: function(param, targetNode){
				var param = {
					'customer': kriya.config.content.customer,
					'project': kriya.config.content.project
				};
				kriya.general.sendAPIRequest('getcitationconfig', param, function (res) {
					//if the responds came as null no need to do these all process and it will break code
					if (res && res != "null") {
						var resObj = JSON.parse(res);
						var citeConf = {};
						$.each(resObj.citation, function (className, obj) {
							if(className == "param" && obj.type && obj['#text']){
								citeConf[obj.type] = obj['#text'];
							}else if(typeof(obj) == "string"){
								citeConf[className] = obj;
							}else if (obj.type) {
								addToObj(className, obj);
							}else if (typeof (obj[0]) == "object"){
								$.each(obj, function (i, sobj) {
									addToObj(className, sobj);
								});
							}
						});
						//Initialize the citation settings
						if(Object.keys(citeConf).length > 0){
							citeJS.init(citeConf);
							$('#contentContainer').attr('data-citation-config-loaded', 'true');
						}
						
						//Check the citation stataus
						citeJS.general.updateStatus();

						function addToObj(className, obj) {
							if (obj.type) {
								citeConf[obj.type] = {};
								if (className == "jrnlBibRef" && obj.direct && obj.indirect) {
									citeConf[obj.type]['authorFormat'] = {
										'direct': getAuthorFormat(obj.direct),
										'indirect': getAuthorFormat(obj.indirect)
									};
								}
								$.each(obj, function (p, v) {
									if (p != "type" && p != "format" && p != "direct" && p != "indirect" && p != "prefix-chapter-number") {
										citeConf[obj.type][p] = v;
									} else if (p == "format") {
										$.each(v, function (i, o) {
											citeConf[obj.type][o.type] = {};
											$.each(o, function (fp, fv) {
												citeConf[obj.type][o.type][fp] = fv;
											});
										});
									} else if(p == "prefix-chapter-number") {
										var chapNum = $('#contentDivNode .jrnlChapNumber').text();
										var chapNUmSuffux = '.';
										if(obj['chapNumSuffix']){
											chapNUmSuffux = obj['chapNumSuffix'];
										}
										citeConf[obj.type]['numberPrefix'] = chapNum + chapNUmSuffux;
										citeConf[obj.type][p] = v;
									}
								});
							}
						}
						function getAuthorFormat(obj) {
							var returnObj = {};
							$.each(obj.format, function (i, val) {
								if (val.authorNo) {
									val.pattern = val.pattern.replace(/Author/g, 'Surname');
									if(val['pattern-with-page']){
										val['pattern-with-page'] = val['pattern-with-page'].replace(/Author/g, 'Surname');
									}
									if(val['following-pattern']){
										val['following-pattern'] = val['following-pattern'].replace(/Author/g, 'Surname');
									}
									returnObj[val.authorNo] = val;
								} else if (val.etalFormat) {
									returnObj['etalFormat'] = val.etalFormat;
								}
							});
							return returnObj;
						}
					}else{
						console.log('getcitationconfig responds came as null');
					}
				}, function(err){
					var screenNode = $('<div id="lock-screen"/>');
					screenNode.append('<div class="row configNotLoaded" style="background-color: #f5b113;padding: 25px !important;text-align: center;font-size: 2rem;">It seems that the page didn\'t load correctly. Please refresh and try again.</div>');
					screenNode.css({
						'background': '#f5f5f5',
						'height': '100%',
						'left': '0px',
						'overflow': 'auto',
						'position': 'fixed',
						'pointer-events': 'all',
						'padding': '10px',
						'top': '0px',
						'width': '100%',
						'z-index': '10000',
						'display': 'block'
					});
					$('body').append(screenNode);
				});
			}
		},
		reference: {
			addNewField: function(param, targetNode){
				target = targetNode.closest('.row.input-field');
				var addClass = $(targetNode).attr('data-label');
				kriya.general.addNewField($(target), addClass);
			},
			addAuthor: function(param, targetNode){
				componentName = (param && param.component)?param.component:'RefAuthor_add';
				target = $(targetNode)[0];
				if (target.hasAttribute('data-deleted')) return false;
				if (target.textContent != ""){
					if (kriya.isUndefined(kriya.componentList[componentName])){
						kriya['styles'].init($(target), componentName);
					}
				}
			},
			saveReference: function(param, targetNode){
				var comp  = $(targetNode).closest('[data-type="popUp"]');
				$('[data-component="RefAuthor_add"]').addClass('hidden');
				if ($('[data-component="jrnlRefText_edit"] .input-type.ref-type')[0].hasAttribute('data-old-ref-type')){
					var currRefType = $('[data-component="jrnlRefText_edit"] .input-type.ref-type').attr('data-old-ref-type');
					var refType = $('.ref-type select').val();
					if (currRefType != refType && comp.find('.validatedRefHTML').hasClass('hidden')){
						$('.validateBtn').trigger('click');
						return false;
					}
				}
				if (! comp.find('.validatedRefHTML').hasClass('hidden')){
					var validatedRefEle = comp.find('.validatedRefHTML .jrnlRefText');
					var cid = $(kriya.config.containerElm + ' [data-cid]').length;
					if(validatedRefEle.length > 0){
						/*validatedRefEle.find('.ins,.del').each(function(i){
							$(this).attr('data-cid', cid+(i+1));
							eventHandler.menu.observer.nodeChange({'node':$(this)});
						});*/
						if ($(kriya.config.activeElements[0]).hasClass('jrnlRefText')){
							kriya.config.activeElements.find('> .jrnlQueryRef').each(function(){
								validatedRefEle.prepend($(this))
							});
							//Add id to the reference childrens	
							validatedRefEle.find('[class]').each(function(){
								var id = uuid.v4();
								$(this).attr('id', id);
							});
							kriya.config.activeElements.html(validatedRefEle.html());
							if(validatedRefEle.attr('data-doi')){
								kriya.config.activeElements.attr('data-doi', validatedRefEle.attr('data-doi'));
								if (kriya.config.activeElements.find('.RefDOI').length == 0){
									kriya.config.activeElements.append('<span class="RefDOI" pub-id-type="doi">' + validatedRefEle.attr('data-doi') + '</span>');
								}
							}
							if(validatedRefEle.attr('data-pmid')){
								kriya.config.activeElements.attr('data-pmid', validatedRefEle.attr('data-pmid'));
								//kriya.config.activeElements.append('<span class="RefPMID">' + validatedRefEle.attr('data-pmid') + '</span>')
							}
							comp.find('.validatedRefHTML').html('');
							var savedNode = kriya.config.activeElements;
						}else{
							var savedNode = kriya.saveComponent.init(comp[0]);
						}
					}else{
						var savedNode = false;
					}
				}else if (! comp.find('.popupHead[data-type="new"]').hasClass('hidden')){
					eventHandler.components.reference.validate(param, targetNode);
				}else{
					

					//Remove teh site name and get only the id in doi and pubmed id - jagan
					if(comp.find('[data-class="RefDOI"]:visible').length > 0){
						var doiVal = comp.find('[data-class="RefDOI"]:visible').html();
						doiVal = doiVal.replace(/https?:\/\/dx.doi.org\//g, '');
						comp.find('[data-class="RefDOI"]:visible').html(doiVal);
					}
					if(comp.find('[data-class="RefPMID"]:visible').length > 0){
						var pubmedVal = comp.find('[data-class="RefPMID"]:visible').html();
						pubmedVal = pubmedVal.replace(/https?:\/\/(\S+)\//g, '');
						comp.find('[data-class="RefPMID"]:visible').html(pubmedVal);
					}

					var savedNode = kriya.saveComponent.init(comp[0]);
					//when we remove author or contrib in edit reference popup at that time need to validate because of that this if condition will work
					if ($(kriya.config.activeElements[0]).attr('data-element-added') == "true" || $(kriya.config.activeElements[0]).attr('data-element-removed') == "true" ){
						//these two attr we are not using any where if that attr if save maybe give problem that's why i am removing these attrs
						//these attr only added for validate the reference when we are trigger save in edit reference.
						$(kriya.config.activeElements[0]).removeAttr('data-element-added');
						$(kriya.config.activeElements[0]).removeAttr('data-element-removed');
						var response = eventHandler.components.reference.editReference(param, targetNode);
						return false;
					}
				}
				$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type');
				if (savedNode) {
					var refId = $(savedNode).attr('data-id');

					//If citation type is author year then move the new reference new thw alphabetical position
					if(citeJS.settings.R.citationType == "1"){
						var refPosition = citeJS.general.getRefAlphaPosition(refId);
						if(refPosition){
							//if response has nextId then insertBefore to the next element
							if(refPosition.nextId && $(savedNode).next().attr('data-id') != refPosition.nextId){
								$(savedNode).insertBefore($(kriya.config.containerElm + ' .jrnlRefText[data-id="' + refPosition.nextId + '"]'));
								if (kriya.config.activeElements.attr('id') == savedNode.attr('id')){
									$(savedNode).attr('data-moved', 'true');
								}
								if(!$(savedNode).attr('data-inserted')){
									$(savedNode)[0].scrollIntoView();
								}
							}else if(refPosition.prevId && $(savedNode).prev().attr('data-id') != refPosition.prevId){
								//if response has prevId then insertAfter to the prev element
								$(savedNode).insertAfter($(kriya.config.containerElm + ' .jrnlRefText[data-id="' + refPosition.prevId + '"]'));
								if (kriya.config.activeElements.attr('id') == savedNode.attr('id')){
									$(savedNode).attr('data-moved', 'true');
								}
								if(!$(savedNode).attr('data-inserted')){
									$(savedNode)[0].scrollIntoView();
								}
							}
						}
					}

					if (kriya.config.activeElements.attr('id') != savedNode.attr('id')){
						
						//set the data-id for the new reference - jagan
						var refArray = [];
						$(kriya.config.containerElm + ' .jrnlRefText').not(savedNode).each(function(){
							refArray.push($(this).attr('data-id').replace(/[a-z]+/gi, ''));
						});
						var max = Math.max.apply( Math, refArray );
						//when we enter first ref at that time it giving as a -infinite-kirankumar
						if(max == -Infinity){
							var refId = 'R1';
						}else{
							var refId = 'R' + (max + 1);
						}   //get the reference list count
						$(savedNode).attr('data-id', refId);
						
						$(savedNode).attr('data-insert-new', 'true');
						
						/* Start Issue Id - #346 - Dhatshayani . D Jan 06, 2017 - To save reference citation in the orphaned citation while reference added from the author query cue. */
						if ($(kriya.config.activeElements[0]).hasClass('jrnlUncitedRef') || ($(kriya.config.activeElements[0]).hasClass('jrnlQueryRef') && $(kriya.config.activeElements[0]).closest('.jrnlUncitedRef').length > 0)){
							if($(kriya.config.activeElements[0]).hasClass('jrnlQueryRef') && $(kriya.config.activeElements[0]).closest('.jrnlUncitedRef').length > 0){
								kriya.config.activeElements = $(kriya.config.activeElements[0]).closest('.jrnlUncitedRef');
							}/* End Issue Id - #346 */
							if (kriya.config.activeElements.find('.jrnlQueryRef').length > 0){
								//add reply if is orphan citation - auto reply as new reference has been added - jai
								var qid = $(kriya.config.activeElements).find('.jrnlQueryRef').attr('data-rid');
								var queryNode = $('.query-div#' + qid);
								if (queryNode.length > 0 && queryNode.attr('data-query-action') == "uncited-citation"){
									var target = $(queryNode).find('.query-holder').first();
									$(queryNode).find('.query-action').remove();
									$(queryNode).attr('data-remove-edit','true');

									var citationHTML = kriya.general.getNewCitation([savedNode.attr('data-id')], '', '');
									var citeNodes = $.parseHTML( citationHTML );
									var citeElements = $(citeNodes).filter(function() {
										return this.nodeType === 1;
									});
									$(citeElements).first().prepend(kriya.config.activeElements.find('.jrnlQueryRef'));
									$(kriya.config.activeElements).replaceWith($(citeElements)[0]);																		
									
									//Modify by jagan - add citation text in query to identify the reference
									var refText = savedNode.text();
									refText = refText.substring(0, 100) + '...';
									if($(citeElements).length > 0){
										refText = $(citeElements)[0].innerText;
									}
									
									eventHandler.query.action.addReply({'undoLevel':'false','content':'<i>This citation has been linked to a new reference: ' + refText + '</i>', 'reply' : true}, target); //prevent undolevel function call for #346 - priya

									kriyaEditor.settings.undoStack.push($(citeElements).parent());
								}
							}
							//var savedNode = kriya.saveComponent.init(comp[0]); // Issue Id - #346 - Dhatshayani . D Jan 06, 2017 - Commented this part since it initiate the savecomponent after get the "savedNode". Which is unnecessary. This causes the issue mentioned in this card.
						}
						
						//$(savedNode)[0].scrollIntoView();
						moveCursor($(savedNode)[0]);

						kriyaEditor.settings.undoStack.push(savedNode);
					}else{
						//If citation type is author year existing reference is saved then updat the citation
						if(citeJS.settings.R.citationType == "1" && refId){
							$(kriya.config.containerElm + ' [data-citation-string=" ' + refId + ' "]').each(function(i){
								var citeString = $(this).attr('data-citation-string');
								citeString = citeString.replace(/^\s+|\s+$/g, '').split(' ');
								var citeHtml = $(this).html();
								var isDirect = false;	
								if($(this).html().match(/\(\d{4}[a-z]?\)/)){
									isDirect = true;
								}
								var isFollowing = (i != 0)?true:false;
								var abbrCollab  = (citeJS.settings.R.abbrCollab && parseFloat(citeJS.settings.R.abbrCollab) <= i)?true:false; 
								var newCiteHTML   = citeJS.floats.getReferenceHTML(citeString.join(' '),'','',isDirect, '', isFollowing, '',abbrCollab);
								if(newCiteHTML && newCiteHTML != citeHtml){
									$(this).html(newCiteHTML);
									kriyaEditor.settings.undoStack.push($(this));
								}
							});
						}
						kriyaEditor.settings.undoStack.push(kriya.config.activeElements);
					}
					
					kriya.general.updateRightNavPanel(refId, kriya.config.containerElm);
					// Display the notification after insert the reference - rajesh
					//#487 - All | Reference | Display Pop-up message for inserting reference - Prabakaran.A(prabakaran.afocusite.com)
					if(savedNode && $(comp).hasClass('manualSave') && $('[data-citation-string=" '+refId+' "]').length < 1 ){
					//End of #487
						var savedXpath = kriya.getElementXPath($(savedNode)[0]);
						var message = '<div class="row"> A new reference has been added. Please insert a citation for this reference.</div><div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
						kriya.notification({
							title : 'Information',
							type  : 'success',
							content : message,
							icon : 'icon-info'
						});
					}
					//It will count no.of references re-validated in review content-kirankumar
					if (! comp.find('.validatedRefHTML').hasClass('hidden')){
						var parameters = {
							'customer': kriya.config.content.customer,
							'project' : kriya.config.content.project,
							'doi'     : kriya.config.content.doi,
							'type'	  : "tracker",
							'logMsg'  : "<div><p>This Reference is re-validated : </p><p>"+$(savedNode).attr('data-id')+"</p></div>"
						};
						keeplive.sendAPIRequest('logerrors', parameters, function(res){
						});
					}
					kriya.popUp.closePopUps($(targetNode).closest("[data-component]"));
					//Trigger the citation reorder after inserting the figure,table etc.
					/*if($(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]').length > 0){
						var reorderNode = $(kriya.config.containerElm + ' [class$=Ref][citation-reorder="true"]');
						reorderNode.removeAttr('citation-reorder');
						kriya.general.triggerCitationReorder($(reorderNode), 'insert');
					}*/
					kriyaEditor.init.addUndoLevel('save-reference');
				}
			},
			changeType: function(param, targetNode){
				var refType = $(targetNode).val();
				$('[data-component="RefAuthor_add"]').addClass('hidden');
				var refModal = $('[data-component="jrnlRefText_edit"] [data-if-selector][data-ref-type="' + refType + '"]');
				if (refModal.length > 0){
					var currRefModal = $('[data-component="jrnlRefText_edit"] [data-if-selector]:not(".hidden")');
					var currRefType = $(currRefModal).attr('data-ref-type');
					if (! $('[data-component="jrnlRefText_edit"] .input-type.ref-type')[0].hasAttribute('data-old-ref-type')){
						$('[data-component="jrnlRefText_edit"] .input-type.ref-type').attr('data-old-ref-type', currRefType);
					}else if ($('[data-component="jrnlRefText_edit"] .input-type.ref-type').attr('data-old-ref-type') == refType){
						//if the original reference type and selected type are same, then cancel the ref type
						eventHandler.components.reference.cancelRefType();
					}
					if (! $('.searchCrossRef').hasClass('hidden')){
						//$('[data-component="jrnlRefText_edit"] [data-if-selector]').addClass('hidden');
						//$(refModal).removeClass('hidden');
						//return false;
						
						//Move current ref type field values to changed fields - jagan
						$(refModal).find('[data-class="RefAuthor"]:not([data-template="true"])').remove();
						$(refModal).find('[data-class="RefCollaboration"]:not([data-template="true"])').remove();
						$(refModal).find('[data-type="htmlComponent"][data-class]').each(function(){
							var className = $(this).attr('data-class');
							if($(this).attr('data-template') != "true"){
								var nodeVal = currRefModal.find('[data-class="' + className + '"][data-type="htmlComponent"]').html();
								$(this).html(nodeVal);
							}else{
								var clonedNodes = currRefModal.find('[data-class="' + className + '"][data-type="htmlComponent"]:not([data-template="true"])').clone(true);
								$(clonedNodes).insertAfter(this);
							}
						});
					}

					var options = $('<span class="input-type"><select data-channel="components" data-topic="reference" data-event="change" data-message="{\'funcToCall\': \'setElementClass\'}" data-selected="untagged"/></span>');
					$(options).find('select').append('<option value="Untagged">Untagged</option>');
					$(refModal).find('label').each(function(){
						if($(this).attr('data-label') == "RefAuthor" && $(this).parent().find('.dropdown-content').length > 0){
							$(this).parent().find('.dropdown-content li').each(function(){
								$(options).find('select').append('<option value="' + $(this).attr('data-label') + '">' + $(this).text() + '</option>');
							});
						}else{
							$(options).find('select').append('<option value="' + $(this).attr('data-label') + '">' + $(this).text() + '</option>');
						}
					});
					
					if ($('.refStyler').hasClass('hidden')){
						$('.refStyler .wrapper').html('');
						var fieldRow = $('<div class="row"><div class="col s3"></div><div class="col s7 refField" contenteditable="true"></div></div>')
						var authorRow = fieldRow.clone(true);
						$(authorRow).find('.col.s3').html('Author');
						$(authorRow).find('.col.s7').html(currRefModal.find('[data-class="RefAuthor"]').first().parent().html());
						//$(options).find('option[value="RefAuthor"]').remove();
						//$('.refStyler .wrapper').append(authorRow);
						refModalClone = currRefModal.clone(true);
						refModalClone.find('[data-class="RefAuthor"]').first().closest('.row').remove();
						$(refModalClone).find('[contenteditable="true"]').each(function(){
							if ($(this).text() != ""){
								var newField = fieldRow.clone(true);
								$(newField).find('.col.s3').addClass('refFieldName').append(options.clone(true));
								$(newField).find('.col.s7').html($(this).html());
								$(newField).append('<span class="btn btn-small deleteField" data-channel="components" data-topic="reference" data-event="click" data-message="{\'funcToCall\': \'deleteRefField\'}">X</span>');
								if (newField.find('option[value="' + $(this).attr('data-class') + '"]').length == 0){
									//newField.find('select').val($(this).attr('data-class')).attr('data-selected', $(this).attr('data-class'));
									$('.refStyler .wrapper').append(newField);
								}else{
									$('.refFieldName').find('option[value="' + $(this).attr('data-class') + '"]').remove();
								}
							}
						});
					}else{
						$('.refStyler .wrapper').find('.refFieldName').each(function(){
							var className = $(this).find('select').attr('data-selected');
							$(this).html(options.clone(true));
							if ($(this).find('option[value="' + className + '"]').length > 0){
								$(this).find('select').val(className).attr('data-selected', className);
							}
						});
					}

					
					$('[data-component="jrnlRefText_edit"] [data-input-editable="true"][data-pop-type="edit"]').find('[data-wrapper] [data-ref-type],.com-save,.ref-types,.validateBtn').addClass('hidden');
					$('.refStyler .saveRefType').addClass('hidden');
					$('[data-component="jrnlRefText_edit"]').find('[data-wrapper] [data-ref-type="' + refType + '"]').removeClass('hidden');
					$('.refStyler').removeClass('hidden');
					if ($('.refStyler .wrapper .row:not([class*=hidden])').length == 0){
						$('[data-component="jrnlRefText_edit"]').find('[data-wrapper],.com-save,.com-close,.ref-type,.validateBtn').removeClass('hidden');
						$('.refStyler').addClass('hidden');
						$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type')
						$('.refStyler .saveRefType').removeClass('disabled');
					}
				}
			},
			deleteRefField: function(param, targetNode){
				var parentStyle = $(targetNode).closest('.row').parent()
				$(targetNode).closest('.row').remove();
				if (parentStyle.find('.row:not([class*=hidden])').length == 0){
					
					$('[data-component="jrnlRefText_edit"]').find('[data-wrapper],.com-save,.com-close,.ref-type').removeClass('hidden');
					//If edititing reference then show save reference btn else show insert reference btn
					if($('[data-component="jrnlRefText_edit"] .popupHead[data-type="edit"]:visible').length > 0){
						$('[data-component="jrnlRefText_edit"]').find('.validateBtn').removeClass('hidden');
					}else{
						$('[data-component="jrnlRefText_edit"]').find('.insertNewRef').removeClass('hidden');
					}

					$('.refStyler').addClass('hidden');
					$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type')
					$('.refStyler .saveRefType').removeClass('disabled');
				}
			},
			setElementClass: function(param, targetNode){
				var refElementType = $(targetNode).val();
				//$(targetNode).attr('data-selected', refElementType.toLocaleLowerCase());
				$(targetNode).attr('data-selected', refElementType);
				$(targetNode).closest('.row').find('.refField').attr('data-class', refElementType);
				$(targetNode).closest('.row').addClass('hidden');
				var refType = $('.ref-type select').val();
				var refEditor = $('[data-component="jrnlRefText_edit"]').find('[data-wrapper] [data-ref-type="' + refType + '"]');
				var appendVal = $(targetNode).closest('.row').find('.refField').html();
				
				if (refEditor.find('[contenteditable][data-class="' +refElementType + '"][data-template="true"]').length > 0){
					var tempNode = refEditor.find('[contenteditable][data-class="' +refElementType + '"][data-template="true"]');
					var clonedtempNode = tempNode.clone(true);
					clonedtempNode.removeAttr('data-template');
					clonedtempNode.html(appendVal);					
					clonedtempNode.insertAfter(refEditor.find('[contenteditable][data-class="' +refElementType + '"]:last'));
				}else if (refEditor.find('[type="checkbox"][data-class="' +refElementType + '"]').length > 0){
					refEditor.find('[type="checkbox"][data-class="' +refElementType + '"]').trigger('click');
				}else if (refEditor.find('[contenteditable][data-class="' +refElementType + '"]').length > 0){
					refEditor.find('[contenteditable][data-class="' +refElementType + '"]').html(appendVal);
				}
				var untagged = false;
				$('.refStyler select').each(function(){
					if ($(this).val() == "Untagged") untagged = true;
				})
				if (untagged) {
					$('.refStyler .saveRefType').addClass('disabled');
				}else{
					$('[data-component="jrnlRefText_edit"]').find('[data-wrapper],.com-save,.com-close,.ref-type').removeClass('hidden');
					
					//If edititing reference then show save reference btn else show insert reference btn
					if($('[data-component="jrnlRefText_edit"] .popupHead[data-type="edit"]:visible').length > 0){
						$('[data-component="jrnlRefText_edit"]').find('.validateBtn').removeClass('hidden');
					}else{
						$('[data-component="jrnlRefText_edit"]').find('.insertNewRef').removeClass('hidden');
					}

					$('.refStyler').addClass('hidden');
					$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type')
					$('.refStyler .saveRefType').removeClass('disabled');
				}
			},
			saveRefType: function(param, targetNode){
				var refType = $('.ref-type select').val();
				var refModal = $('[data-component="jrnlRefText_edit"] [data-if-selector][data-ref-type="' + refType + '"]');
				$('[data-component="jrnlRefText_edit"] [data-if-selector]').addClass('hidden');
				$(refModal).removeClass('hidden');
				$(refModal).find('[contenteditable="true"]').each(function(){
					var className = $(this).attr('data-class');
					if ($('.refStyler .wrapper').find('.refField[data-class="' + className + '"]').length > 0){
						$(this).html($('.refStyler .wrapper').find('.refField[data-class="' + className + '"]').html());
					}
				});
				$('.refStyler').addClass('hidden');
				$('[data-component="jrnlRefText_edit"]').find('[data-wrapper],.com-save,.com-close,.ref-type,.validateBtn').removeClass('hidden');
			},
			cancelRefType: function(param, targetNode){
				$('.refStyler').addClass('hidden');
				$('[data-component="jrnlRefText_edit"]').find('.com-save,.com-close,.ref-type').removeClass('hidden');
				
				//If edititing reference then show save reference btn else show insert reference btn
				if($('[data-component="jrnlRefText_edit"] .popupHead[data-type="edit"]:visible').length > 0){
					$('[data-component="jrnlRefText_edit"]').find('.validateBtn').removeClass('hidden');
				}else{
					$('[data-component="jrnlRefText_edit"]').find('.insertNewRef').removeClass('hidden');
				}

				$('[data-component="jrnlRefText_edit"]').find('[data-wrapper] [data-ref-type]').addClass('hidden');
				var oldRefType = $('[data-component="jrnlRefText_edit"] .input-type.ref-type').attr('data-old-ref-type');
				$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type')
				$('[data-component="jrnlRefText_edit"] [data-wrapper] [data-ref-type="' + oldRefType + '"]').removeClass('hidden');
				$('.ref-type select').val(oldRefType);
			},
			validateBack: function(param, targetElement){
				var popper = $(targetNode).closest('[data-component]');
				popper.find('.ref-type').removeClass('hidden');
				$(targetElement).closest('.validatedRefHTML').parent().find('[data-wrapper="true"]').removeClass('hidden');
				$(targetElement).closest('.validatedRefHTML').parent().find('.refBtns').removeClass('hidden');
				$(targetElement).closest('.validatedRefHTML').addClass('hidden');
				$(targetElement).closest('.validatedRefHTML').html('');
			},
			insertBack: function(param, targetElement){
				var popper     = $(targetNode).closest('[data-component]');
				var inputBlock = $(targetNode).closest('[data-input-editable="true"]');
				inputBlock.addClass('hidden');
				inputBlock.prev('[data-input-editable="true"]').removeClass('hidden');
				popper.find('.refSearchContainer').html('');
				popper.find('.refStyler .wrapper').html('');
			},
			editReference: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				popper.progress('Saving...');
				$(popper).find('.validateCancel').addClass('hidden');
				if ($(kriya.config.activeElements[0]).hasClass('jrnlRefText')){
					//var data = kriya.config.activeElements;
					var refID = kriya.config.activeElements.attr('id');
					var data = $('<p class="jrnlRefText" id="'+ refID + '">');
					$('[data-component="jrnlRefText_edit"] [data-wrapper] [data-deleted]').remove();
					$('[data-component="jrnlRefText_edit"] [data-wrapper] [data-type="htmlComponent"]:visible').each(function(){
						if ($(this).parent('[data-type="htmlComponent"]').length == 0){
							if ($(this).html() != ""){
								var comp = $('<span class="' + $(this).attr('data-class') + '">'+ $(this).html() + '</span>');
								$(comp).find('[data-class]').each(function(){
									var dataClass = $(this).attr('data-class');
									while($(this)[0].attributes.length > 0)
										$(this)[0].removeAttribute($(this)[0].attributes[0].name);
									$(this).attr('class', dataClass);
								});
								data.append(comp)
							}
						}
					})
					data = data[0].outerHTML;
					var httpContent = '<editref projectname="' + kriya.config.content.customer + '">' + data + '</editref>';
					var parameters  = {'processType': 'editReference', 'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'data': httpContent};
					kriya.general.sendAPIRequest('process_reference', parameters, function(res){
						var response = $('<div>'+res.body+'</div>');
						if($(response).find('.jrnlRefText').length < 1){
							return false;
						}
						popper.progress('',true);
						$(kriya.config.activeElements).html($(response).find('.jrnlRefText').html());
						// to add doi into existing reference - JAI 27-03-2018
						if ($(response).find('.jrnlRefText').attr('data-doi') != undefined){
							$(kriya.config.activeElements).attr('data-doi', $(response).find('.jrnlRefText').attr('data-doi'))
						}
						kriyaEditor.settings.undoStack.push(kriya.config.activeElements);
						$('[data-component="jrnlRefText_edit"] .input-type.ref-type').removeAttr('data-old-ref-type');
						kriya.popUp.closePopUps(popper);
						kriyaEditor.init.addUndoLevel('navigation-key');
					},function(){
						popper.progress('',true);
						$('.validatedRefHTML').prepend('<p>Data Request Failed!.</p><p style="text-align: center;"><span class="btn btn-medium" data-message="{\'click\':{\'funcToCall\': \'validateBack\',\'channel\':\'components\',\'topic\':\'reference\'}}">Back</span></p>');
					});
				}
			},
			validateAll: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				popper.find('.validateAllBtn').addClass('disabled');
				popper.find('.com-save').addClass('disabled');
				popper.find('[data-wrapper]').progress('Validating...');
				
				var validateSuccess = 0;
				var validateError = 0;
				var refLength = popper.find('.collection li:not([data-template="true"])').length;
				//First send 10 req to validate after one req is completed send another req
				popper.find('.collection li:not([data-template="true"]):lt(5)').each(function(i){
					validateRef($(this));
				});

				function updateValidateStatus(){
					var valText = validateSuccess + ' out of ' + refLength + ' was validated';
					if(validateError > 0){
						valText = valText + ' and ' + validateError + ' error.';
					}
					if(validateError+validateSuccess == refLength){
						popper.find('[data-wrapper]').progress('', true);
						popper.find('.com-save').removeClass('disabled');
					}
					popper.find('.validateStatus').text(valText);
				}

				function validateRef(refNode){
					if(refNode.length > 0){
						$(refNode).attr('data-validate-ref', 'true');
						var nodeXpath = refNode.attr('data-node-xpath');
						var refDataNode = kriya.xpath(nodeXpath);
						if($(refDataNode).length > 0){
							var data = $('<p class="jrnlRefText" id="'+ $(refDataNode).attr('data-id') + '" />');
							$(refDataNode).find(' > [class^="Ref"]').each(function(){
								var nodeHTML  = $(this).html();
								if(nodeHTML){
									var className = $(this).attr('class');
									if( className == "RefDOI"){
										nodeHTML = nodeHTML.replace(/https?:\/\/dx.doi.org\//g, '');
									}else if(className == "RefPMID"){
										nodeHTML = nodeHTML.replace(/https?:\/\/(\S+)\//g, '');
									}

									var comp = $('<span class="' + className + '">'+ nodeHTML + '</span>');
									data.append(comp);
								}
							});
							

							if(data.text() == ""){
								return false;
							}

							var httpContent = '<validate projectname="' + kriya.config.content.customer + '">' + data[0].outerHTML + '</validate>';
							var parameters  = {'processType': 'editReference', 'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'data': httpContent};
							kriya.general.sendAPIRequest('process_reference', parameters, function(res){
								var response = $('<div>'+res.body+'</div>');
								if(response.find('.jrnlRefText').length > 0){
									var refHTML = response.find('.jrnlRefText').html();
									refNode.find('[data-class="jrnlRefText"]').html(refHTML);
									
									if(response.find('.jrnlRefText').attr('data-doi')){
										refNode.find('[data-class="data-doi"]').html(response.find('.jrnlRefText').attr('data-doi'));
									}else{
										refNode.find('[data-class="data-doi"]').html('');
									}

									if(response.find('.jrnlRefText').attr('data-pmid')){
										var pmID = response.find('.jrnlRefText').attr('data-pmid');
										refNode.find('[data-class="data-pmid"]').html(pmID);
									}else{
										refNode.find('[data-class="data-pmid"]').html('');
									}
									validateSuccess = validateSuccess+1;
								}else{
									validateError = validateError+1;
								}
								updateValidateStatus();
								if(popper.find('.collection li:not([data-template="true"]):not([data-validate-ref="true"]):first').length > 0){
									validateRef(popper.find('.collection li:not([data-template="true"]):not([data-validate-ref="true"]):first'));
								}
							}, function(err){
								validateError = validateError+1;
								updateValidateStatus();
								if(popper.find('.collection li:not([data-template="true"]):not([data-validate-ref="true"]):first').length > 0){
									validateRef(popper.find('.collection li:not([data-template="true"]):not([data-validate-ref="true"]):first'));
								}
							});
						}
					}
				}
			},
			// to move csl field / aravind
			moveCslFields: function(param,tabeNode){
				var filedToMove = $(targetNode).closest('.dataLine');
				if(param.nav == 'moveUp'){
					var prevNode = $(filedToMove).prev();
					if(prevNode){
						$(filedToMove).insertBefore($(prevNode));
					}
				}else if(param.nav == 'moveDown'){
					var nextNode = $(filedToMove).next();
					if(nextNode){
						$(filedToMove).insertAfter($(nextNode));
					}
				}
			},
			// to csl field on change / aravind
			changeCslField: function(param, targetNode) {
				eventHandler.components.PopUp.getCslMappingObj(function (status) {
					if (status) {
						var mappingObj = kriya.config.cslMappingObj;
						var fieldType = $(targetNode).val();
						var prevData = $(targetNode).attr('prev-data');
						var multipleAllowed = mappingObj['multipleFieldsAllowed'];
						var existingOptions = [];
						$(targetNode).closest('[data-component]').find('[data-class="refDataType"]').each(function () {
							existingOptions.push($(this).attr('prev-data'));
						});
						if ((existingOptions.indexOf(fieldType) >= 0) && (!(multipleAllowed[fieldType]))) {
							var duplicateField = $(targetNode).find('[value="' + fieldType + '"]').text();
							$(targetNode).val(prevData);
							kriya.notification({
								title: 'ERROR',
								type: 'error',
								timeout: 5000,
								content: duplicateField + " field duplication not allowed",
								icon: 'icon-warning2'
							});
						} else {
							$(targetNode).attr('prev-data', $(targetNode).val());
						}
						var authorStyle = ['author', 'editor'];
						if (authorStyle.indexOf(fieldType) >= 0) {
							$(targetNode).closest('.refData').next().remove();
							var authorField = $('.cslRefTemples').find('[data-temp="authorField"]').clone().removeAttr('data-temp');
							$(authorField).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
							$(targetNode).closest('.dataLine').append($(authorField));
						} else {
							if ($(targetNode).find('[data-class="jrnlSurName"]').length > 0) {
								$(targetNode).closest('.refData').next().remove();
								var dataField = $('.cslRefTemples').find('[data-temp="dataField"]').clone().removeAttr('data-temp');
								$(dataField).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
								$(targetNode).closest('.dataLine').append($(dataField));
							}
						}
						var popper = $(targetNode).closest('[data-component]');
						var refType = $(popper).find('[data-class="CslRefType"]').val();
						var fieldFormat = mappingObj['cslFieldsMapping'][refType];
						var currField = $(targetNode).find(':selected').val()
						var currDataLine = $(targetNode).closest('.dataLine');
						if (fieldFormat[currField].required) {
							$(currDataLine).find('.refData').addClass('cslRequiredField');
							$(currDataLine).find('.text-line').attr('data-validate', 'true');
						}
						else {
							$(currDataLine).find('.text-line').removeAttr('data-validate');
							$(currDataLine).find('.refData').removeClass('cslRequiredField');
						}
					}
				});
			},
			deleteCslField: function(param, targetNode){
				if($(targetNode).closest('.dataLine').find('[data-validate]').length == 0){
					$(targetNode).closest('.dataLine').remove();
				}
				else{
					kriya.notification({
						title : 'ERROR',
						type  : 'error',
						timeout : 5000,
						content : "This is required field can't be removed",
						icon: 'icon-warning2'
					});
				}				
			},
			//to add author in csl reference / aravind
			addCslField: function(param, targetNode) {
				eventHandler.components.PopUp.getCslMappingObj(function (status) {
					if (status) {
						var mappingObj = kriya.config.cslMappingObj;
						var refType = $('[data-component="jrnlRefCsl_edit"]').find(':selected').val();
						if (mappingObj['kriyaRefTypeToCslType'][refType] != undefined) {
							refType = mappingObj['kriyaRefTypeToCslType'][refType];
						}
						var fieldFormat = mappingObj['cslFieldsMapping'][refType];
						var objEle = mappingObj['kriyaClassToUi'][refType];
						// constructing options according to csl mapping
						var optionNode = $('.cslRefTemples').find('[data-temp="option"]').clone().removeAttr('data-temp');
						for (var key in objEle) {
							var currNode = document.createElement('option');
							$(currNode).attr('value', objEle[key]).text(fieldFormat[objEle[key]].name);
							$(optionNode).find('[data-class="refDataType"]').append(currNode);
						}
						// constructing empty field
						var newField = $('.cslRefTemples').find('[data-temp="dataLine"]').clone().removeAttr('data-temp');
						$(newField).append($(optionNode));
						var newDataField = $('.cslRefTemples').find('[data-temp="dataField"]').clone().removeAttr('data-temp');
						$(newDataField).append($('.cslRefTemples').find('[data-temp="addAndDeleteButton"]').clone().removeAttr('data-temp'));
						$(newField).append($(newDataField));
						$(newField).insertAfter($(targetNode).closest('.dataLine'));
					}
				});
			},
			//to handle ref type field / aravind
			refTypeFieldMapping: function(param, targetNode) {
				eventHandler.components.PopUp.getCslMappingObj(function (status) {
					if (status) {
						var mappingObj = kriya.config.cslMappingObj;
						var refType = $(targetNode).find(':selected').val();
						if (mappingObj['kriyaRefTypeToCslType'][refType] != undefined) {
							refType = mappingObj['kriyaRefTypeToCslType'][refType];
						}
						var fieldFormat = mappingObj['cslFieldsMapping'][refType];
						var objEle = mappingObj['kriyaClassToUi'][refType];
						// contructing options according to csl mapping
						var optionNode = $('.cslRefTemples').find('[data-temp="option"]').clone().removeAttr('data-temp');
						for (var key in objEle) {
							var currNode = document.createElement('option');
							$(currNode).attr('value', objEle[key]).text(fieldFormat[objEle[key]].name);
							$(optionNode).find('[data-class="refDataType"]').append(currNode);
						}
						if (fieldFormat) {
							$('[data-component="jrnlRefCsl_edit"]').find('.refContent').find('.dataLine').each(function () {
								var currField = $(this).find('[selected]').val();
								$(this).find('.refData').remove();
								$(this).prepend($(optionNode).clone());
								if ($(this).find('[value=' + currField + ']').length > 0) {
									$(this).find('[value=' + currField + ']').attr('selected', 'selected');
								}
								if (fieldFormat[currField].required) {
									$(this).find('.refData').addClass('cslRequiredField');
									$(this).find('.text-line').attr('data-validate', 'true');
								}
								else {
									$(this).find('.text-line').removeAttr('data-validate');
								}
							});
						}
					}
				});
			},
			// validate reference through csl / aravind
			validateRefThroughCsl: function(param, targetNode) {
				eventHandler.components.PopUp.getCslMappingObj(function (status) {
					if (status) {
						var mappingObj = kriya.config.cslMappingObj;
						var popper = $(targetNode).closest('[data-component]');
						if (param == undefined || (param && param.save != "saveToRef")) {
							if ($('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput').length > 0) {
								$('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput').remove();
							}
						}
						if (popper.length > 0 && popper[0].hasAttribute('data-node-xpath')) {
							var nodeXpath = popper.attr('data-node-xpath');
							var editElement = kriya.xpath(nodeXpath);
						}
						//if editElement is come as undefined at that time code will break
						if (editElement && editElement.length > 1) {
							for (var r = 1, rl = editElement.length; r < rl; r++) {
								if ($(editElement[r]).closest('[data-component="jrnlRefCsl_edit"]').length == 0) {
									currTargetEle = editElement[r];
								}
							}
						} else if (editElement) {
							var currTargetEle = $(editElement).clone();
						}
						$(currTargetEle).html('');
						//getting ref type
						var cslRefType = $('[data-component="jrnlRefCsl_edit"]').find(':selected').val();
						// constructing as html
						var objEle = mappingObj['kriyaClassToUi'][cslRefType];
						if (objEle == undefined) {
							kriya.notification({
								title: 'ERROR',
								type: 'error',
								content: "For <b>" + cslRefType + "</b> type reference, config not present",
								icon: 'icon-warning2'
							});
							return false;
						}
						var objEleKeys = Object.keys(objEle);
						$(popper).find('.refContent').find('.refData').each(function () {
							var newNode = document.createElement('span');
							var keyOfValue = objEleKeys[Object.values(objEle).indexOf($(this).find('[selected]').val())];
							var targetElementXpath = $('[data-component="jrnlRefCsl_edit"]').attr('data-node-xpath');
							var contentNode = kriya.xpath(targetElementXpath);
							if ($(this).next().find('.text-line').text() != '') {
								var nodeXpath = $(this).next().find('.text-line').attr('data-node-xpath');
								var dataElement = kriya.xpath(nodeXpath);
								if ($(this).find('[selected]').val() == 'author') {
									$(newNode).attr('class', keyOfValue);
									$(newNode).append('<span class="RefSurName">' + $(this).next().find('[data-class="jrnlSurName"]').text() + '</span>');
									var surrNameNodeXpath = $(this).next().find('[data-class="jrnlSurName"]').attr('data-node-xpath');
									var surNameDataElement = kriya.xpath(surrNameNodeXpath);
									kriya.general.addToHistory($(this).next().find('[data-class="jrnlSurName"]'), surNameDataElement, contentNode);
									$(newNode).append('<span class="RefGivenName">' + $(this).next().find('[data-class="jrnlGivenName"]').text() + '</span>');
									var givenNameNodeXpath = $(this).next().find('[data-class="jrnlGivenName"]').attr('data-node-xpath');
									var givenNameDataElement = kriya.xpath(givenNameNodeXpath);
									kriya.general.addToHistory($(this).next().find('[data-class="jrnlGivenName"]'), givenNameDataElement, contentNode);
								} else {
									if ($(this).next().find('.text-line a').length > 0) {
										$(newNode).attr('class', keyOfValue).append($(this).next().find('.text-line').text());
									} else {
										$(newNode).attr('class', keyOfValue).append($(this).next().find('.text-line')[0].innerHTML);
									}
									kriya.general.addToHistory($(this).next().find('.text-line'), dataElement, contentNode);
								}
								$(currTargetEle).append($(newNode));
							} else if (($(this).find('[selected]').val() != 'author') && ($(this).next().find('span')[0].hasAttribute('data-validate')) && ($(this).next().text() != '')) {
								$(this).next().find('span[class="text-line"]').attr('data-error', 'Required Field');
							}
						});
						// to save processed node
						if (param && param.save == "saveToRef") {
							currTargetEle = $('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput .jrnlRefText')
							var refParaId = $(currTargetEle).attr('id')
							if ($('.jrnlRefGroup').find('[id=' + refParaId + ']').length > 0) {
								$(currTargetEle).find('.undefined').remove();
								$('.jrnlRefGroup').find('[id=' + refParaId + ']').replaceWith($(currTargetEle)[0]);
								// save history cards while saving validated reference
								$('[data-component="jrnlRefCsl_edit"]').find('.historyTab > div').each(function () {
									var hisCard = $(this).clone();
									$('#historyDivNode').prepend(hisCard);
									kriyaEditor.settings.undoStack.push(hisCard);
								});
								kriyaEditor.settings.undoStack.push($('.jrnlRefGroup').find('[id=' + refParaId + ']'));
								kriyaEditor.init.addUndoLevel('style-change');
							}
							kriya.popUp.closePopUps(popper);
						} else {
							if ($('[data-component="jrnlRefCsl_edit"]').find('[data-error]').length > 0) {
								return false;
							}
							// calling biblio api
							popper.progress('Validating');
							var refType = $(currTargetEle).attr('data-reftype');
							var cslStyle = $('[data-component="jrnlRefCsl_edit"]').attr('data-csl-style');

							var parameters = {
								"style": cslStyle,
								"locale": "locales-en-US.xml",
								"type": "html",
								"refJournalType": refType,
								"data": $(currTargetEle)[0].outerHTML,
								"processType": 'cslEditReference'
							}
							if ($('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput').length > 0) {
								$('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput').remove();
							}
							kriya.general.sendAPIRequest('process_reference', parameters, function (res) {
								if (res && res != '' && res != 'error')  {
									if ((res.status != undefined) && $(res.status == 'success') && (typeof(res.message) == "object") && res.message.Output != undefined) {
										var finalRefData = res.message.Output;
										var validatedReference;
										for(var key in finalRefData){
											var refDataArray = finalRefData[key];
											refDataArray.forEach(function (ref) {
												if(Object.keys(ref).length > 0){
													validatedReference = ref[Object.keys(ref)[0]].BibliographyString;
												}
											});
										}
										if(validatedReference == undefined){
											$(popper).find('.validated').addClass('hidden');
											$(popper).find('.validationFailed').removeClass('hidden');
											popper.progress('', true);
											kriya.notification({
												title: 'ERROR',
												type: 'error',
												timeout: 5000,
												content: "Unable to convert CSL, validation failed",
												icon: 'icon-warning2'
											});
											return;
										}
										popper.progress('', true);
										$(popper).find('.validationFailed').addClass('hidden');
										$(popper).find('.validated').removeClass('hidden');
										$('[data-component="jrnlRefCsl_edit"]').append('<div class="cslReferenceOutput hidden"></div>');
										$('[data-component="jrnlRefCsl_edit"] .cslReferenceOutput').append($(validatedReference))
										eventHandler.components.PopUp.editCslRef('', '', validatedReference); // populate validated reference in popup
										kriya.general.cslAddToHistory(validatedReference, $('[data-component="jrnlRefCsl_edit"]')); // to generate track changes history cards
									} else {
										$(popper).find('.validated').addClass('hidden');
										$(popper).find('.validationFailed').removeClass('hidden');
										popper.progress('', true);
										kriya.notification({
											title: 'ERROR',
											type: 'error',
											timeout: 5000,
											content: "Unable to convert CSL, validation failed",
											icon: 'icon-warning2'
										});
										return;
									}
								} else {
									popper.progress('', true);
									kriya.notification({
										title: 'ERROR',
										type: 'error',
										timeout: 5000,
										content: "Validation failed, Please try again",
										icon: 'icon-warning2'
									});
									return;
								}
							});
						}
					}
				});
			},
			validate: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				
				var validated = kriya.saveComponent.validateComponents(popper);
				if (!validated){
					return;
				}
				popper.progress('Validating');
				$(popper).find('.validateCancel').addClass('hidden');
				if ($(kriya.config.activeElements[0]).hasClass('jrnlRefText')){
					var refID = kriya.config.activeElements.attr('id');
				}else{
					var refArray = [];
					$(kriya.config.containerElm + ' .jrnlRefText').each(function(){
						refArray.push($(this).attr('data-id').replace(/[a-z]+/gi, ''));
					});
					var max = Math.max.apply( Math, refArray );
					//When we insert first ref at that time it will take as -infinity-kirankumar
					if(max == -Infinity){
						var refID = 'R1';
					}else{
						var refID = 'R' + (max + 1);
					}   //get the reference list count
				}
				var data = $('<p class="jrnlRefText" id="'+ refID + '">');
				$('[data-component="jrnlRefText_edit"] [data-wrapper] [data-deleted]').remove();
				$('[data-component="jrnlRefText_edit"] [data-wrapper]').find('[data-type="htmlComponent"]:visible, [data-type="checkBoxComponent"]:checked:visible').each(function(){
					if ($(this).parent('[data-type="htmlComponent"]').length == 0){
						var nodeVal = $(this).html();
						if(this.nodeName == "INPUT" && $(this).attr('data-node-value')){
							nodeVal = $(this).attr('data-node-value');
						}
						var className = $(this).attr('data-class');
						
						//Remove teh site name and get only the id in doi and pubmed id - jagan
						if( className == "RefDOI"){
							nodeVal = nodeVal.replace(/https?:\/\/dx.doi.org\//g, '');
						}else if(className == "RefPMID"){
							nodeVal = nodeVal.replace(/https?:\/\/(\S+)\//g, '');
						}
						
						if (nodeVal != ""){
							var comp = $('<span class="' + $(this).attr('data-class') + '">'+ nodeVal + '</span>');
							$(comp).find('[data-class]').each(function(){
								var dataClass = $(this).attr('data-class');
								while($(this)[0].attributes.length > 0)
									$(this)[0].removeAttribute($(this)[0].attributes[0].name);
								$(this).attr('class', dataClass);
							});
							//Remoev the deleted content and if component text not empty then append
							comp.find('.del').remove();
							if(comp.text() != ""){
								// to add previous text node to retain it for comments
								if ($(this).closest('[data-previous-text]').length > 0){
									data.append($(this).closest('[data-previous-text]').attr('data-previous-text'));
								}
								data.append(comp)
							}
						}
					}
				});
				if (data.text() == ""){
					popper.progress('',true);
					return false;
				}
				if ($('#contentDivNode [id="' + refID + '"]').find('.RefSlNo').length > 0){
					var slNo = $('#contentDivNode [id="' + refID + '"]').find('.RefSlNo').clone(true)
					data.prepend(slNo)
				}else{//added by kirankumar:-issue:when insert new reference it showing refid as 1
					var sNo = refID.replace(/[A-Z]+/g, '');
					data.append('<span class="RefSlNo" >'+sNo+'</span>');
				}
				data = data[0].outerHTML;
				
				var httpContent = '<validate projectname="' + kriya.config.content.customer + '">' + data + '</validate>';
				var parameters  = {'processType': 'editReference', 'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'data': httpContent};

				kriya.general.sendAPIRequest('process_reference', parameters, function(res){
					var response = $('<div>'+res.body+'</div>');
					var popper = $('[data-component="jrnlRefText_edit"]');
					if($(response).find('.jrnlRefText').length < 1){
						popper.progress('',true);
						return false;
					}
					//validate the newly inserting reference against existing reference for duplicates
					// Start Issue Id - #246 - Dhatshayani . D Jan 05, 2017 - Show alert if the reference already exists. This alert was worked only if the reference has doi. Current updation is to check reference without doi also. 
					var duplicateReference = false;var existRefNodes = '';
					if (!popper.find('.popupHead[data-type="new"]').hasClass('hidden') && ($(response).find('.jrnlRefText')[0].hasAttribute('data-doi') || $(response).find('.jrnlRefText')[0].hasAttribute('data-pmid'))){
						var newRefDoi = $(response).find('.jrnlRefText')[0].getAttribute('data-doi'); 
						var newRefPmid = $(response).find('.jrnlRefText')[0].getAttribute('data-pmid'); 
						if(newRefDoi && newRefDoi !='' && $('#contentDivNode .jrnlRefText:not([data-track="del"])[data-doi="' + newRefDoi + '"]').length > 0){
							duplicateReference = true; // Set the variable "true" if duplicate exist.
						}else if(newRefPmid && newRefPmid !='' && $('#contentDivNode .jrnlRefText:not([data-track="del"])[data-pmid="' + newRefPmid + '"]').length > 0){
							duplicateReference = true;
						}
						
						if(!duplicateReference){
							var compareElements = $(response).find('.jrnlRefText:eq(0)').find('> *:not(.RefAuthor,.RefCollaboration,.RefEditor,.RefEdCollab,.RefEtal)');
							// Get the existing reference nodes which match the compare check list content.
							existRefNodes = eventHandler.components.reference.getMatchingRefNodes(compareElements,existRefNodes);
						}
						// Compare new reference content with the existing reference content.
						if(existRefNodes && existRefNodes.length > 0){
							var newRefText = $(response).find('.jrnlRefText:eq(0)').clone(true).cleanTrackChanges().text().replace(/([\s]+|\&nbsp;*)/ig,'');
							existRefNodes.each(function(index,object){
								var existRefText = $(this).clone(true).cleanTrackChanges().text();
								existRefText = existRefText.replace(/([\s]+|\&nbsp;*)/ig,'');
								if(newRefText!='' && existRefText!='' && (newRefText == existRefText)){
									duplicateReference = true; // Set the variable "true" if duplicate exist.
									
								}
							});
						}
					}
					if (duplicateReference){
						popper.progress('',true);
						kriya.notification({
							title : 'ERROR',
							type  : 'error',
							timeout : 5000,
							content : 'This Reference already exists, please check',
							icon: 'icon-warning2'
						});
						return false;
					}// End Issue Id - #246
					$(popper).find('.ref-type').addClass('hidden');
					$(popper).find('[data-wrapper], .refBtns').addClass('hidden');
					$(popper).find('.validateCancel').removeClass('hidden');
					$('.validatedRefHTML').removeClass('hidden').html($(response).find('.jrnlRefText')[0].outerHTML);
					$('.validatedRefHTML').find('.jrnlRefText').attr('class', 'jrnlRefText');
					if ($('.validatedRefHTML').find('.jrnlRefText').attr('data-doi') != undefined){
						$('.validatedRefHTML').prepend('<p>DOI: <a target="_blank" href="http://dx.doi.org/'+$('.validatedRefHTML').find('.jrnlRefText').attr('data-doi')+'">'+$('.validatedRefHTML').find('.jrnlRefText').attr('data-doi')+'</a></p>')
					}
					if ($('.validatedRefHTML').find('.jrnlRefText').attr('data-pmid') != undefined){
						$('.validatedRefHTML').prepend('<p>PubMed: <a target="_blank" href="'+$('.validatedRefHTML').find('.jrnlRefText').attr('data-pmid')+'">'+$('.validatedRefHTML').find('.jrnlRefText').attr('data-pmid')+'</a></p>')
					}
					$('.validatedRefHTML').append('<p style="text-align: center;"><span class="btn btn-medium" data-message="{\'click\':{\'funcToCall\': \'validateBack\',\'channel\':\'components\',\'topic\':\'reference\'}}">Back</span></p>');
					//to save the reference after validating when inserting a new reference, instead displaying to the user which will be done at the time of validating an existing reference - jai 19-06-2017
					popper.progress('',true);
					if (! popper.find('.popupHead[data-type="new"]').hasClass('hidden')){
						//eventHandler.components.reference.saveReference(param, targetNode);
					}
				},function(){
					popper.progress('',true);
					$('.validatedRefHTML').prepend('<p>Data Request Failed!.</p><p style="text-align: center;"><span class="btn btn-medium" data-message="{\'click\':{\'funcToCall\': \'validateBack\',\'channel\':\'components\',\'topic\':\'reference\'}}">Back</span></p>');
				});
			},
			getMatchingRefNodes: function(compareElements,existRefNodes){// #246 - Return list of matching reference nodes of specific child nodes.
				if(compareElements.length > 0){
					compareElements.each(function(index,object){
						if(!existRefNodes || (existRefNodes && existRefNodes == "")){
							var currEleClass = $(this).attr('class');
							if(currEleClass.match(/reftexterror/gi)){
								currEleClass = currEleClass.replace(/reftexterror/gi,'');
								currEleClass = currEleClass.replace(/^\s+|\s+$/,'');
							}
							var newRefSearchText = $(this).clone(true).cleanTrackChanges().text();
							//#487 - All | Reference | Display Pop-up message for inserting reference - Prabakaran.A(prabakaran.afocusite.com)
							if(newRefSearchText && newRefSearchText!='' && currEleClass && currEleClass !='') {
							//End of #487
								existRefNodes = $('.'+currEleClass + ':not([data-track="del"]):contains("'+newRefSearchText+'")').closest('.jrnlRefText:not([data-track="del"])');
							}
						}
					});
				}
				return existRefNodes;
			},
			cancelValidate: function(param, targetNode){
				$('.validatedRefHTML').addClass('hidden');
				var popper = $(targetNode).closest('[data-component]');
				$(popper).find('.validateCancel').addClass('hidden');
				$(popper).find('[data-wrapper], .refBtns').removeClass('hidden');
			},
			searchRef_new:  function(param, targetNode){
				var popper    = $(targetNode).closest('[data-component]');
				var inputWrap = $(targetNode).closest('[data-input-editable="true"]');
				
				var searchClass = inputWrap.find('input[name="refSearch"]:checked').attr('data-value');
				//if checkbox not check and click on next button in insertref popup at that time code is breaking
				if(searchClass){
					var type = searchClass.replace(/^Ref/,'');
					inputWrap.find('input[name="refSearch"]:checked').closest('[data-error]').removeAttr('data-error');
				}
				var searchVal = inputWrap.find('.searchField').text();

				if(popper.find('.refSearchContainer:visible').length > 0 && popper.find('.refSearchContainer table tr').length > 0){
					if(popper.find('.refSearchContainer table tr.active').length > 0){
						var clonedTable = popper.find('.refSearchContainer table').clone(true);
						clonedTable.find('tbody tr:not(.active)').remove();
						var refData = $('<div>' + clonedTable[0].outerHTML + '</div>');
						eventHandler.components.reference.placeSearchedRef(type, refData, inputWrap);
					}else{
						kriya.notification({title : 'Info', type : 'error', content : 'Please select the reference listed in in the table.', icon: 'icon-warning2'});
					}
					return true;	
				}

				//Validate components  
				var validated = kriya.saveComponent.validateComponents(inputWrap);
				if(validated){
					popper.progress('Searching');
					
					var queryString = $('<editreference>');
					if(type == "by-pasteref"){
						var database = $('<parse>');
						database.append(searchVal);
						queryString.append(database);
					}else{	
						var database = $('<search name="' + param.dataBase + '">');
						queryString.append(database);

						var field = $('<field>');
						database.append(field);
						field.append(searchVal);
						field.attr('type', type);
					}
					
					var paremeter = {'data': queryString[0].outerHTML, 'format':'html', 'processType': 'editReference'};
					kriya.general.sendAPIRequest('process_reference', paremeter, function(res){
						popper.progress('',true);
							var response = $('<div>'+res.body+'</div>');
							
							if(type != "by-pasteref" && type != "by-partialref" && response.find('.' + searchClass).text().toLowerCase() != searchVal.toLowerCase()){
								kriya.notification({title : 'Info', type : 'error', content : 'Unable to retrieve the reference from Pubmed and Crossref based on the information provided.', icon: 'icon-warning2'});
								return false;
							}

							if(type == "by-pasteref" && response.find('table, p').length == 0){
								//kriya.notification({title : 'Info', type : 'error', content : 'Unable to tag the given ref text', icon: 'icon-warning2'});
								response = $('<res><p>' + searchVal + '</p></res>');
							}else if(response.find('table, p').length == 0){
								kriya.notification({title : 'Info', type : 'error', content : 'Unable to retrieve the reference from Pubmed and Crossref based on the information provided.', icon: 'icon-warning2'});
							}
							
							if(type == "by-partialref" && response.find('table').length > 0){
								popper.find('.refSearchContainer').removeClass('hidden');
								popper.find('.refSearchContainer').html(response.find('table')[0].outerHTML);
							}else{
								eventHandler.components.reference.placeSearchedRef(type, response, inputWrap);
							}
					}, function(e){
						popper.progress('',true);
						if(type == "by-pasteref"){
							//kriya.notification({title : 'Info', type : 'error', content : 'Unable to tag the given ref text', icon: 'icon-warning2'});
							response = $('<res><p>' + searchVal + '</p></res>');
							eventHandler.components.reference.placeSearchedRef(type, response, inputWrap);
						}else{
							kriya.notification({title : 'Info', type : 'error', content : 'Unable to retrieve the reference from Pubmed and Crossref based on the information provided.', icon: 'icon-warning2'});
						}
					});
				}
			},
			/**
			 * Function place the searched reference in reference edit popup
			 */
			placeSearchedRef: function(searchType, response, inputWrap){
				var popper = $(inputWrap).closest('[data-component]');
				if(response.find('table, p').length > 0){
					inputWrap.addClass('hidden');
					inputWrap.find('.searchField').text('');
					inputWrap.next('[data-pop-type="edit"][data-input-editable="true"]').removeClass('hidden');
					popper.find('.insertNewRef').removeClass('hide');

					
					var placeNode = $('[data-component="jrnlRefText_edit"] [data-ref-type]:first');
					$(popper).find('*[data-ref-type][data-if-selector]').each(function(){
						var ifSelector = $(this).attr('data-if-selector').split('|');
						var ifIdentified = true;
						for (var x = 0, xl = ifSelector.length; x < xl; x++){
							var noflag = false;
							if (/^!/.test(ifSelector[x])){
								ifSelector[x] = ifSelector[x].replace('!', '');
								noflag = true;
							}
							var condition = kriya.xpath(ifSelector[x], response[0]);
							if (condition.length > 0 && noflag){
								ifIdentified = false;
							}else if (condition.length == 0 && !noflag){
								ifIdentified = false;
							}else if (condition.length > 0 && $(this)[0].hasAttribute('data-if-value') && condition[0].nodeType == 2){
								if (condition[0].nodeValue != $(this).attr('data-if-value')){
									ifIdentified = false;
								}
							}
						}
						if (ifIdentified){
							placeNode = $(this);
							return true;
						}
					});
					var refType   = placeNode.attr('data-ref-type');
					$('[data-class="RefType"]').val(refType);
					$('[data-component="jrnlRefText_edit"] [data-ref-type]').addClass('hidden');

					//Reset the place node
					placeNode.removeClass('hidden');
					placeNode.find('[data-class="RefAuthor"]:not([data-template="true"])').remove();
					placeNode.find('[data-class="RefCollaboration"]:not([data-template="true"])').remove();
					placeNode.find('[data-type="htmlComponent"]').html('');
					placeNode.find('input[type="checkbox"][data-selector]:checked').trigger('click');
					

					var selector = "table tbody tr:first td[class]";
					if(searchType == "by-pasteref"){
						selector = "p [class]";
					}

					var fpageVal = response.find(selector).filter('.RefFPage').text();
					var lpageVal = response.find(selector).filter('.RefLPage').text();
					var refIssue = response.find(selector).filter('.RefIssue').text();
					
					//If fpage and lpage is equal then mark the fpage as elocation id
					if((fpageVal == lpageVal) || (!refIssue && !lpageVal && fpageVal)){
						response.find(selector).filter('.RefLPage').text('');
						response.find(selector).filter('.RefFPage').attr('class', 'RefELocation');
					}

					response.find(selector).each(function(){
						var valNode = $(this);
						var nodeClass = valNode.attr('class');
						if (/RefID/.test(nodeClass)) return true;
						if(placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]').length > 0){
						
							if (nodeClass == "RefAuthor"){
								if(searchType == "by-pasteref"){
									if(placeNode.find('[data-class="' + nodeClass + '"]:not([data-template="true"])').length > 1){
										return;
									}
									var doiNodes = response.find('p .RefAuthor');
								}else{
									var doiNodes = $(this).find('.RefAuthor,.RefCollaboration');	
								}
							}else{
								var doiNodes = $(this);
							}
							
							$(doiNodes).each(function(){
								var thisNode = $(this);
								var templateNode = placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]');
								var cloneTemplate = templateNode[0].cloneNode(true);
								$(cloneTemplate).removeAttr('data-template');
								$(cloneTemplate).attr('data-clone', 'true');
								$(cloneTemplate).insertBefore(templateNode);
								if(thisNode.attr('class') == "RefCollaboration"){
									$(cloneTemplate).attr('data-class', 'RefCollaboration');
									$(cloneTemplate).attr('data-placeholder', 'Collaboration');
								}
								if ($(cloneTemplate).find('[data-selector]').length > 0 && valNode.children().length > 0){
									$(cloneTemplate).find('[data-selector]').each(function(){
										var eleClass = $(this).attr('data-class');
										var nodeValue = $(thisNode).find('.' + eleClass).html();
										$(this).html(nodeValue);
									});
								}else{
									var nodeValue = $(this).html();
									$(cloneTemplate).html(nodeValue);
								}
								$(this).remove();
							});
						}else if(placeNode.find('[data-class="' + nodeClass + '"]').length > 0){
							placeNode.find('[data-class="' + nodeClass + '"]').html($(this).html());
							$(this).remove();
						}
					});
					
					
					$('.refStyler .wrapper').html('');
					if(searchType == "by-pasteref"){
						var options = $('<span class="input-type"><select data-channel="components" data-topic="reference" data-event="change" data-message="{\'funcToCall\': \'setElementClass\'}" data-selected="untagged"/></span>');
						$(options).find('select').append('<option value="Untagged">Untagged</option>');
						$(placeNode).find('label').each(function(){
							if($(this).attr('data-label') == "RefAuthor" && $(this).parent().find('.dropdown-content').length > 0){
								$(this).parent().find('.dropdown-content li').each(function(){
									$(options).find('select').append('<option value="' + $(this).attr('data-label') + '">' + $(this).text() + '</option>');
								});
							}else{
								$(options).find('select').append('<option value="' + $(this).attr('data-label') + '">' + $(this).text() + '</option>');
							}
						});

						//if ($('.refStyler').hasClass('hidden')){
							var fieldRow = $('<div class="row"><div class="col s3"></div><div class="col s7 refField" contenteditable="true"></div></div>')
							//$(options).find('option[value="RefAuthor"]').remove();
							
							var textNodes = $(response).find('p')[0].childNodes;
							textNodes = $(textNodes).each(function(){
								var nodeVal = (this.nodeType == 3)? this.nodeValue :$(this).html();
								if(nodeVal != "" && nodeVal.replace(/[\.\s\,\:\;\-\–\)\()]+/g, '') != ""){
									var newField = fieldRow.clone(true);
									$(newField).find('.col.s3').addClass('refFieldName').append(options.clone(true));
									$(newField).find('.col.s7').html(nodeVal);
									$(newField).append('<span class="btn btn-small deleteField" data-channel="components" data-topic="reference" data-event="click" data-message="{\'funcToCall\': \'deleteRefField\'}">X</span>');
									$('.refStyler .wrapper').append(newField);	
								}
							});
						//}
					}

					if($('.refStyler .wrapper .refField').length > 0){
						$('.refStyler .saveRefType').addClass('hidden');
						$('.refStyler').removeClass('hidden');
						$('[data-component="jrnlRefText_edit"] [data-input-editable="true"][data-pop-type="edit"]').find('.com-save,.validateBtn,.insertNewRef').addClass('hidden');
					}else{
						$('[data-component="jrnlRefText_edit"] [data-input-editable="true"][data-pop-type="edit"]').find('.com-save,.insertNewRef').removeClass('hidden');
					}
					
					//$('.refSearchContainer').css('height', $('.refSearchContainer').closest('[data-input-editable]').height())
					/*var component = $("*[data-type='popUp'][data-component='searchRef_edit']");
					component.find('.com-save').attr('data-message', "{'click':{'funcToCall': 'setSearchedRef','channel':'components','topic':'reference'}}");
					component.find('.popupHead').html('Select The Reference');
					kriya.popUp.showEmptyPopUp(component, $);
					var refCollection = component.find('.refSearchContainer');
					refCollection.html('');
					refCollection.html(response.html());
					refCollection.find('span.RefAuthor span.RefAuthor').contents().unwrap();*/
				}
			},
			searchRef: function(param, targetNode){
				var queryString = $('<editreference>');
				var database = $('<search name="' + param.dataBase + '">');
				queryString.append(database);

				var component = $(targetNode).closest('[data-type="popUp"]');
				var validated = kriya.saveComponent.validateComponents(component);
				if(!validated){
					return;
				}
				component.find('.insertNewRef').removeClass('hide');
				component.find('[data-type="htmlComponent"]').each(function(){
					var type = $(this).attr('data-class');
					if($(this).text() != "" && !type.match(/RefSurName|RefGivenName/)){
						var fieldVal = $(this).text();
						type = type.replace(/^Ref/,'');
						type = type.replace(/^Article(Title)/, '$1');
						type = type.replace(/^(Journal)Title/, '$1');
						
						//Remove teh site name and get only the id in doi and pubmed id - jagan
						if(type == "DOI"){
							fieldVal = fieldVal.replace(/https?:\/\/dx.doi.org\//g, '');
						}else if(type == "PMID"){
							fieldVal = fieldVal.replace(/https?:\/\/(\S+)\//g, '');
						}

						var field = $('<field>');
						database.append(field);
						field.append(fieldVal);
						field.attr('type', type);
					}
				});

				component.progress('Searching');
				
				var paremeter = {'data': queryString[0].outerHTML, 'format':'html', 'processType': 'editReference'};
				kriya.general.sendAPIRequest('process_reference', paremeter, function(res){
						$('[data-component="jrnlRefText_edit"]').progress('',true);
						var response = $('<div>'+res.body+'</div>');
						if(response.find('table').length > 0){
							//$('.refSearchContainer').css('height', $('.refSearchContainer').closest('[data-input-editable]').height())
							var component = $("*[data-type='popUp'][data-component='searchRef_edit']");
							component.find('.com-save').attr('data-message', "{'click':{'funcToCall': 'setSearchedRef','channel':'components','topic':'reference'}}");
							component.find('.popupHead').html('Select The Reference');
							kriya.popUp.showEmptyPopUp(component, $);
							var refCollection = component.find('.refSearchContainer');
							refCollection.html('');
							refCollection.html(response.html());
							refCollection.find('span.RefAuthor span.RefAuthor').contents().unwrap();
						}else{
							kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Unable to retrieve the reference from Pubmed and Crossref based on the information provided.', icon: 'icon-warning2'});
						}
				}, function(){
					$('[data-component="jrnlRefText_edit"]').progress('',true);
					kriya.notification({title : 'Info', type : 'error', timeout : 5000, content : 'Unable to retrieve the reference from Pubmed and Crossref based on the information provided.', icon: 'icon-warning2'});
				});
			},
			setSearchedRef : function(param, targetNode){
				var component = $(targetNode).closest('[data-type="popUp"]');
				var refType   = $('[data-class="RefType"]').val();
				var placeNode = $('[data-component="jrnlRefText_edit"] [data-ref-type="'+refType+'"]');
				if(component.find('.refSearchContainer tbody tr').length > 0){
					placeNode.find('[data-class="RefAuthor"]:not([data-template="true"])').remove();
					component.find('.refSearchContainer tbody tr.active td[class]').each(function(){
						var valNode = $(this);
						var nodeClass = valNode.attr('class');
						if (/RefID/.test(nodeClass)) return true;
						if(placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]').length > 0){
							if (nodeClass == "RefAuthor"){
								var doiNodes = $(this).find('.RefAuthor');
							}else{
								var doiNodes = $(this);
							}
							$(doiNodes).each(function(){
								var thisNode = $(this);
								var templateNode = placeNode.find('[data-class="' + nodeClass + '"][data-template="true"]');
								var cloneTemplate = templateNode[0].cloneNode(true);
								$(cloneTemplate).removeAttr('data-template');
								$(cloneTemplate).attr('data-clone', 'true');
								$(cloneTemplate).insertBefore(templateNode);
								if ($(cloneTemplate).find('[data-selector]').length > 0 && valNode.children().length > 0){
									$(cloneTemplate).find('[data-selector]').each(function(){
										var eleClass = $(this).attr('data-class');
										var nodeValue = $(thisNode).find('.' + eleClass).html();
										$(this).html(nodeValue);
									});
								}else{
									var nodeValue = $(this).html();
									$(cloneTemplate).html(nodeValue);
								}
							});
						}else{
							placeNode.find('[data-class="' + nodeClass + '"]').html($(this).html());
						}
					});
					kriya.popUp.closePopUps(component);
				}

			},
			getReferenceModal: function(){
				jQuery.ajax({
					type: "GET",
					url: "/api/referenceModal",
					data: {'customer':kriya.config.content.customer, 'project':kriya.config.content.project},
					success: function (data) {
						data = data.replace(/&apos;/g, "\'");
						var response = $(data);
						if (response.find('[contenteditable]').length > 0){
							$('[data-component="jrnlRefText_edit"] [data-wrapper]').html(data);
							$('select[data-class="RefType"]').html('');
							
							$('[data-component="jrnlRefText_edit"] [data-wrapper] label[data-label="RefAuthor"]').each(function(i){
								var dropDownButton = $('<i class="material-icons addNewField" data-activates="author_dropdown_' + i + '" style="position: absolute;top: 0.8rem;font-size: 1.3rem;right: 3rem;color: #2a79ae;cursor:pointer;">add_box</i>');
								$(this).after('<ul class="dropdown-content dropdown-list" id="author_dropdown_' + i + '"><li data-message="{\'click\':{\'funcToCall\': \'addNewField\',\'channel\':\'components\',\'topic\':\'reference\'}}" data-label="RefAuthor">Author</li><li data-message="{\'click\':{\'funcToCall\': \'addNewField\',\'channel\':\'components\',\'topic\':\'reference\'}}" data-label="RefCollaboration">Collaboration</li></ul>');
								$(this).after(dropDownButton);
								$(dropDownButton).dropdown();
							});

							//$('').after($('<i class="material-icons addNewField" style="position: absolute;top: 0.8rem;font-size: 1.3rem;right: 3rem;color: #2a79ae;cursor:pointer;" data-channel="components" data-topic="reference" data-event="click" data-message="{\'funcToCall\': \'addNewField\'}">add_box</i>'));
							$('[data-component="jrnlRefText_edit"] [data-wrapper] label[data-label="RefEditor"]').after($('<i class="material-icons addNewField" style="position: absolute;top: 0.8rem;font-size: 1.3rem;right: 3rem;color: #2a79ae;cursor:pointer;" data-channel="components" data-topic="reference" data-event="click" data-message="{\'funcToCall\': \'addNewField\'}">add_box</i>'));

							$('[data-component="jrnlRefText_edit"] [data-wrapper] [data-ref-type]').each(function(){
								$('select[data-class="RefType"]').append('<option value="' + $(this).attr('data-ref-type') + '">' + $(this).attr('data-ref-type') + '</option>');
							});
						}
					}
				});
			}
		}
	};
	return eventHandler;

})(eventHandler || {});

// returns a list of all elements under the cursor
function elementsFromPoint(x, y) {
    var elements = [],
        previousPointerEvents = [],
        current, i, d;
    if (typeof(x) == 'undefined' || typeof(y) == undefined) {
        return elements;
    }
    if (typeof document.elementsFromPoint === "function")
        return document.elementsFromPoint(x, y);
    if (typeof document.msElementsFromPoint === "function")
        return document.msElementsFromPoint(x, y);

    // get all elements via elementFromPoint, and remove them from hit-testing in order
    while ((current = document.elementFromPoint(x, y)) && elements.indexOf(current) === -1 && current != null) {

        // push the element and its current style
        elements.push(current);
        previousPointerEvents.push({
            value: current.style.getPropertyValue('pointer-events'),
            priority: current.style.getPropertyPriority('pointer-events')
        });

        // add "pointer-events: none", to get to the underlying element
        current.style.setProperty('pointer-events', 'none', 'important');
    }

    // restore the previous pointer-events values
    for (i = previousPointerEvents.length; d = previousPointerEvents[--i];) {
        elements[i].style.setProperty('pointer-events', d.value ? d.value : '', d.priority);
    }

    // return our results
    return elements;
}

function textNodesUnder(el) {
    var n, a = [],
        walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null, false);
    while (n = walk.nextNode()) a.push(n);
    return a;
}

$.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    } else {
        return results[1] || 0;
    }
}
