/**
* eventHandler - this javascript holds all the functions required for Reference handling
*					so that the functionalities can be turned on and off by just calling the required functions
*					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['menu', 'upload', 'pdf', 'general'];
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;
	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel',
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);

			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}

								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.menu = {
        toggle: {
			//eventHandler.explorer.toggle.panel
            explorer: function(param, targetNode){
                $('.epToggle').css('transform', 'scale(1)');
                if ($('#tocContainer').hasClass('fullWidth')){
                    $("#tocContainer").animate({
                        width: '73%'
                     }, { duration: 500, queue: false })
                     .toggleClass('fullWidth');

                     $("#explorerContainer").animate({
                        width: 'toggle'
                     }, { duration: 500, queue: false });
                }
                else{
                    $("#tocContainer").animate({ width: '98%' }, { duration: 500, queue: false }).toggleClass('fullWidth');
                    $("#explorerContainer").animate({
                        width: 'toggle'
                     }, { duration: 500, queue: false });
                }
                console.log('toggle explorer clicked');
            },
			//eventHandler.tabs.toggle.panel
			tabs: function (param, targetNode) {
				$(targetNode).parent().find('.tabHead').removeClass('active');
				if ($(targetNode).attr('data-href') == "tocBodyDiv"){
					$('[data-href="flatPlanBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
					$(targetNode).addClass('active').removeClass('btn-default').addClass('btn-primary');
				}
				var tabID = $(targetNode).addClass('active').attr('data-href');
				$('#' + tabID).parent().find('.tabContent').addClass('hide');
				$('#' + tabID).removeClass('hide');
			},
			//eventHandler.explorer.customer.toggle
			customer: function(param, targetNode){
				$("#tocContainer, #detailsContainer, #tocheader, #issueDetails, #exportArticles").addClass('hide');
				$('#explorerContainer .bodyDiv').removeClass('hide').addClass('active');
				$('#issue,.addNewIssue,.addNewBook').addClass('hide');
				$('#issue').parent().prev('.supportCard').addClass('hide');
				$('#tocContainer .newStatsRow').addClass('hide');
                if ($(targetNode).hasClass('customer')) {
					$('#filterCustomer').find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#project,.addJob').addClass('hide');
					$('#project').parent().prev('.supportCard').addClass('hide')
					$('#customerVal').text($(targetNode).text()).attr('data-customer', $(targetNode).text());
					$('.showEmpty').removeClass('hidden');
					$('.showEmpty .watermark-text').html('Please select a Journal');
                    eventHandler.general.actions.getProjectList($(targetNode).attr('data-customer'), $(targetNode))
						.catch(function(err){
							console.log('error getProjectList')
						})
                }
                else if ($(targetNode).hasClass('project')) {
					//addNewIssue($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'))
					$('#filterProject').find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#issueVal').text('Issues');
					$('#projectVal').text($(targetNode).text()).attr('data-customer', $('#customerVal').attr('data-customer')).attr('data-project', $(targetNode).text());
					$('.showEmpty').removeClass('hidden');
					$('.showEmpty .watermark-text').html('Please select an Issue');
                    eventHandler.general.actions.getIssueList($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'))
                    .catch(function(err){
                        console.log('error getIssueList')
                    })
                }
            },
			//eventHandler.toc.toggle.section
			tocSection: function (param, targetNode) {
				if ($(targetNode).hasClass('sectionData')) {
					$('.sectionDataChild.active').removeClass('active');
					$('.sectionData.active').removeClass('active').find('.openclose').toggleClass('icon-arrow_drop_down').toggleClass('icon-arrow_drop_up');
					if ($('[data-sec="' + $(targetNode).attr('id') + '"]:visible').length) {
						$('[data-sec]').addClass('hide');
						$('[data-sec="' + $(targetNode).attr('id') + '"]').addClass('hide');
					}
					else {
						$(targetNode).addClass('active').find('.openclose').toggleClass('icon-arrow_drop_down').toggleClass('icon-arrow_drop_up');
						$('[data-sec]').addClass('hide');
						$('[data-sec="' + $(targetNode).attr('id') + '"]').removeClass('hide');
					}
				}
				else if ($(targetNode).hasClass('icon-fullscreen')) {
					$('.sectionData').addClass('active').find('.openclose').addClass('icon-arrow_drop_down').removeClass('icon-arrow_drop_up');
					$('[data-sec]').removeClass('hide');
					$(targetNode).removeClass('icon-fullscreen').addClass('icon-fullscreen_exit');
				}
				else if ($(targetNode).hasClass('icon-fullscreen_exit')) {
					$('.sectionData').removeClass('active').find('.openclose').addClass('icon-arrow_drop_up').removeClass('icon-arrow_drop_down');
					$('[data-sec]').addClass('hide');
					$(targetNode).removeClass('icon-fullscreen_exit').addClass('icon-fullscreen');
				}
			},
			//eventHandler.toc.toggle.panel
			manuscript: function (param, targetNode) {
				$('#metaHistory, #unassignedArticles').addClass('hide');
				$('#manuscriptData').removeClass('hide');
				if ($(targetNode).attr('data-c-id')) {
					$('#msDataDiv').attr('data-c-rid', $(targetNode).attr('data-c-id'));
				}
				if ($(targetNode).attr('data-uuid')) {
					$('#msDataDiv').attr('data-r-uuid', $(targetNode).attr('data-uuid'));
				}
				if ($(targetNode).attr('data-doi')) {
					$('#msDataDiv').attr('data-dc-doi', $(targetNode).attr('data-doi'));
				}
				$('.sectionDataChild.active').removeClass('active');
				$(targetNode).addClass('active');
				var dataType = $(targetNode).attr('data-type');
				if ((dataType == 'advert') || (dataType == 'advert_unpaginated') || (dataType == 'filler')) {
					$('#manuscriptData').addClass('hide');
					$('#advertsData').removeClass('hide');
					if ($(targetNode).attr('data-c-id')) {
						$('#adDataDiv').attr('data-c-rid', $(targetNode).attr('data-c-id'));
					}
					if ($(targetNode).attr('data-uuid')) {
						$('#adDataDiv').attr('data-r-uuid', $(targetNode).attr('data-uuid'));
					}
					if ($(targetNode).attr('data-doi')) {
						$('#adDataDiv').attr('data-dc-doi', $(targetNode).attr('data-doi'));
					}
					eventHandler.general.components.populateData($('#adDataDiv'), $(targetNode).attr('data-type'), $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')]);
				}
				else {
					$('#manuscriptData #msDataDiv').find('.tabContent').html('');
					eventHandler.general.components.populateData($('#msDataDiv'), $(targetNode).attr('data-type'), $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')]);
				}
			}
        },
        customer: {
            list: function(param, targetNode){
				//$('#newIssue').addClass('hide');
				$("#tocContainer").addClass('hide');
				$('#explorerContainer .bodyDiv').removeClass('hide');
				$('#explorerContainer .bodyDiv').addClass('active');
				$('#detailsContainer').addClass('hide');
				$('#tocheader').addClass('hide');
				$('.add').addClass("hide");
                eventHandler.general.actions.getCustomerList()
                .catch(function(err){
                    console.log('error getIssueData')
                })
            },
            load: function(param, targetNode){
				$('.showEmpty').addClass('hidden');
				$('#tocContainer .newStatsRow').removeClass('hide');
                $(targetNode).parent().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				// to show the project selected in the nav-bar for books and select the clicked book as active in the dropdown
				if ($(targetNode).attr('data-project-type') == 'book'){
					$('#filterProject').find('.active').removeClass('active');
					$('#tocContainer .newStatsRow #exportArticles').find('#exportArticlesDropdown').text('Export All Chapters')
					$('#tocContainer .newStatsRow .articleCount').find('.label').text('No of Chapters: ')
					$('#filterProject').find('[data-project="' + targetNode.attr('data-project') + '"]').addClass('active');
					$('#project').parent().prev('.supportCard').removeClass('hide');
					$('#project').removeClass('hide');
					$('#projectVal').text($(targetNode).find('.msTitle').text()).attr('data-customer', $('#customerVal').attr('data-customer')).attr('data-project', $(targetNode).attr('data-project'));
					if ($(targetNode).hasClass('filter-list')){
						$('#projectVal').text($(targetNode).text())
					}
				}
				else{
					$('#issueVal').text($(targetNode).text());
					$('#tocContainer .newStatsRow #exportArticles').find('#exportArticlesDropdown').text('Export All Articles')
					$('#tocContainer .newStatsRow .articleCount').find('.label').text('No of Articles: ')
				}
				$('#issueVal').attr('data-customer', $(targetNode).attr('data-customer')).attr('data-project', $(targetNode).attr('data-project')).attr('data-file', $(targetNode).attr('data-file'));
				$('#tocheader').removeClass('hide');
				var type = 'journal'
				if(param && param.type && param.type){
					type = param.type
				}
			//	getProjectStages($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'), $(targetNode).attr('data-file'))
					if(type == 'book'){
						eventHandler.general.actions.resetData();
						eventHandler.general.actions.getIssueData($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'),$(targetNode).attr('data-project').concat('.xml'),'book')
							.catch(function(err){
								console.log('error getIssueData')
							})
						eventHandler.menu.bookview.jobType();			
					}
					else{
						//$('#listIssue').attr('data-file',$(targetNode).attr('data-file'))
						eventHandler.general.actions.getIssueData($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'), $(targetNode).attr('data-file'),'')
						.catch(function(err){
							console.log('error getIssueData')
						})
					}
				$("#explorerContainer .bodyDiv").addClass('hide');
				$('#tocContainer').removeClass('hide').addClass('active');
				$('#detailsContainer').removeClass('hide');
			},
			reload: function(param, targetNode){
				var fileName = $('#issueVal').attr('data-file')
               	eventHandler.general.actions.getUpdatedAricle(fileName)
				.catch(function(err){
                    console.log('error getIssueData')
                })
			},
			flatplan: function(param, targetNode){
			   eventHandler.general.components.populateFlatPlan()
			   eventHandler.general.components.populateContents()
				/*.catch(function(err){
                    console.log('error flatplan')
                })*/
			},
			addNewIssuePopUp:function(param, targetNode){
				$('#addNewIssue').find('input[id=upload_file]').val('')
				$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
				$('#addNewIssue').find('#draft0').prop('checked',false)
				$('#addNewIssue').find('input[id=volumeNo]').val('')
				$('#addNewIssue').find('input[id=issueNo]').val('')
				$('#addNewIssue').modal('show');		
			},	
			addNewIssue: function(param, targetNode){
				var file = $('#addNewIssue').find('input[id=upload_file]')
				var fileContents = file[0].files[0];
				var draftIssue = $('#addNewIssue').find('#draft0').is(':checked');
				if (typeof (FileReader) != "undefined" && fileContents != undefined) {
					$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
					var reader = new FileReader();
					reader.onload = function (e) {
						var param={
							"fileContents":e.target.result,
							"draftIssue" : draftIssue
						}
						eventHandler.general.actions.saveXML('issueXML',param)
						$('#addNewIssue').modal('hide');
					}
					reader.readAsText(file[0].files[0]);
				}
				else{
					$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
					var volume = $('#addNewIssue').find('input[id=volumeNo]').val()
					var issue = $('#addNewIssue').find('input[id=issueNo]').val()
					if(volume != '' && issue != '' ){
						var param={
							"volume": volume,
							"issue" : issue,
							"draftIssue" : draftIssue
						}
						eventHandler.general.actions.saveXML('issueXML',param)
						$('#addNewIssue').modal('hide');
					}
					else{
						$('#addNewIssue').find('label[id=invalidInput]').removeClass('hide')
					}
				}
			}
		},
		// sathiyaD
		bookview:{
			jobType: function(param, targetNode){
				$('.la-container').fadeIn();
				var forNow = ["Abstract","Acknowledgement","Chapters","Reference","Appendix"];
				var prepareSelect='<select><option value="">Choose a Type </option>';
				for(i=0; i<forNow.length; i++) {
					var type = forNow[i];
					prepareSelect += '<option value="' + type.toLowerCase() +'">' + type + '</option>';
				}
				prepareSelect +='</select>';
				$('#contentType').html(prepareSelect);
				$('.la-container').fadeOut();
				return true;
			},
			onFileUpload: function(param, targetNode){
				$('#uploader').find('.i-files').remove();
				var file = targetNode[0].files[0];
				eventHandler.settings.tempFileList = {};
				var cnt = $('#uploader').find('.i-files').length + 1;
				eventHandler.settings.tempFileList[cnt] = file;
				var ftype = file.name.replace(/^.*\./, '');
				var fname = file.name;
				$('#uploader .fileList').append('<span class="i-files" id="'+cnt+'"><span type="'+ftype+'"></span>'+fname+'</span>');
				$('#uploader').removeClass('active')
				$('.uploadJob').removeClass('disabled')
			},
			appendContent : 0,
			addJob: function(param,targetNode){
				var parameters = new FormData();
				var client = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				if ($('.manuscript-type .chapter-type-tab').hasClass('active')){
					doiSuffix = parseInt($('#chapterNumber').val());
				}else if ($('.manuscript-type .epub-type-tab').hasClass('active')){
					doiSuffix = false;
				}else{
					var doiExist = [];
					$('.tocBodyContainer .mCSB_container [data-doi]').each(function(){ 
						if($(this).attr('data-doi').match(/\d+$/g)){
							doiExist.push($(this).attr('data-doi').match(/\d+$/g)[0])
						}
					});
					doiSuffix = doiExist.length > 0 ? doiExist[doiExist.length-1] : '000';
					doiSuffix = parseInt(doiSuffix)+1;
				}
				if (! $('.manuscript-type .epub-type-tab').hasClass('active')){
					doiSuffix = ("000"+doiSuffix).slice(-3)
					var doi = project + '.' + doiSuffix;
					if ($('.tocBodyContainer .sectionDataChild[data-doi="' + doi + '"]').length > 0){
						showMessage({ 'text': 'Chapter with ' + doi + ' already exist', 'type': 'error', 'hide':false });
						return false;
					}
				}
				var typeOfJob = $('.tab-pane.fade.active.in').attr('id');
				
				var validated = true;
				$('#'+typeOfJob+' [data-required]').removeAttr('data-invalid');
				$('#'+typeOfJob+' [data-required]').each(function(){
					if (!$(this).val()){
						$(this).attr('data-invalid', 'true');
						validated = false;	
					}
				});
				

				if (!($("#chapFile")[0] && $("#chapFile")[0].files && $("#chapFile")[0].files.length > 0 && validated)){
					showMessage({ 'text': 'Please provide valid input', 'type': 'error', 'hide':false });
					return false;
				}
				
				$('.la-container').fadeIn();
				var parameters = new FormData();
				parameters.append('manuscript',$("#chapFile")[0].files[0]);
				if(typeOfJob == 'epubUpload'){
					// do not appened any parameter
					type = 'epub';
				}else if(typeOfJob == 'chapter'){
					chapterTitle = $('#chapterTitle').val();
					parameters.append('articleTitle', chapterTitle);
					chapterNumber = $('#chapterNumber').val();
					parameters.append('articleNumber', chapterNumber);
					type = 'chapter'
					parameters.append('articleType', type);
				}else{
					chapterTitle = 'Back Matter';
					chapterNumber ='';
					parameters.append('articleTitle', chapterTitle);
					type = $('backType').val();
					parameters.append('articleType', type);
				}
				
				parameters.append('customer', client);
				parameters.append('project', project);
				parameters.append('doi', doi);
				
				parameterToAddJobBook = {
					customer : client,
					project : project,
					doi : doi,
					chapterTitle : chapterTitle,
					chapterNumber : chapterNumber,
					type : type
				}
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						if(response){
							if (type == 'epub'){
								var res = $(response);
								var chapterItems = res.find('manifest item[media-type="application/xhtml+xml"]');
								if (chapterItems.length == 0){
									
								}else{
									$(chapterItems).each(function(){
										var tr = $('<tr/>');
										tr.append('<td>' + $(this).attr('id') + '</td>');
										tr.append('<td>' + $(this).attr('href') + '</td>');
										tr.append('<td><span class="btn-primary btn-sm btn-info" data-channel="menu" data-topic="bookview" data-event="click" data-message="{\'funcToCall\': \'addEpubJob\'}">Add Job</span></td>');
										$('#addEpubJobs .modal-body table tbody').append(tr);
									});
									$('#addJob').modal('hide');
									$('#addEpubJobs').modal('show');
								}
							}else{
								var newCahpter = {};
								newCahpter.title= {'_text': chapterTitle};
								newCahpter._attributes = {"section" : "Chapters", "type" : "manuscript", "id" : doi, "id-type": "doi", "grouptype" : "body", "siteName": "kriya2.0"}
								$('#tocContainer').data('binder')['container-xml']['contents'].content.push(newCahpter);
								eventHandler.general.actions.saveData({'onsuccess': "$('#filterProject .active').trigger('click')"});
								$('#addJob').modal('hide');
							}
							$('.la-container').css('display', 'none');
						}else{
							showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
							$('.la-container').css('display', 'none');
							$('#addJob').modal('hide');
						}
					},
					error: function (err) {
						$('#addJob').modal('hide');
						console.log(err);
						$('.la-container').css('display', 'none');
					}
				})
			},
			addEpubJob: function(param,targetNode){
				var client = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				var itemID = $(targetNode).closest('tr').find('td:first').html();
				var journalID = project + '.' + itemID;
				var addJobObj = {
					customer: client,
					journalID: project,
					articleID: journalID,
					fileName: journalID,
					ftpFileName: itemID,
					articleTitle: journalID,
					articleType: '',
					subject: '',
					specialInstructions: '',
					priority: '',
					customTag: '',
					status: {
						"code": 200
					}
				}
				$.ajax({
					type: 'POST',
					url: '/workflow?currStage=parseEmail',
					data: addJobObj,
					success: function (response) {
						if(response && response.status && response.status.code && response.status.code == 200){
							$(targetNode).addClass('hidden');
							$(targetNode).after('<span class="btn btn-sm">Job added successfully</span>')
							var newCahpter = {};
							newCahpter.title= {'_text': journalID};
							newCahpter._attributes = {"section" : "Chapters", "type" : "manuscript", "id" : journalID, "id-type": "doi", "grouptype" : "body", "siteName": "kriya2.0"}
							$('#tocContainer').data('binder')['container-xml']['contents'].content.push(newCahpter);
							eventHandler.general.actions.saveData({'onsuccess': "$('#filterProject .active').trigger('click')"});
						}else{
							showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
							$('.la-container').css('display', 'none');
						}
					},
					error: function (err) {
						console.log(err)
					}
				})
			},
			appendTemplate: function(param,targetNode){
				e = eventHandler.menu.bookview.appendContent;
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-template="true"]');
				var clone = template.clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				$(targetNode).closest('[data-parent-name]').after(clone);
				e+=1;
				eventHandler.menu.bookview.appendContent = e;
				//$('.'+$(targetNode).attr('id')).append(h);
			},
			removeTemplate: function(param, targetNode){
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-cloned="true"]');
				if (template.length > 1){
					$(targetNode).closest('[data-parent-name]').remove();
				}
			},
			bookType:function(param,targetNode){
				$(targetNode).toggleClass('custom-select');
				var a=[];
				$('ul .custom-select').each(function(){
					a.push($(this).text());
				});
				a = a.toString();
				$(targetNode).parent().siblings('input[type="text"]').val(a).focus();
				return false;
			},
			changeBookType: function(param,targetNode){
				$('.dynamic').remove();
				var bookType = $('#bookType').val();
				$('[data-cloned="true"]').remove();
				var clone = $('.'+bookType+'[data-template]').clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				$('.'+bookType+'[data-template]').after(clone)
			},
			addBookPopup:function(param,targetNode){
				param ={
					customer:$('.projectlist li').attr('data-customer')
				};
				$.ajax({
					type: "GET",
					url: "/api/getadbookmodal",
					data: param,
					success:function(data){
						if((data.statusCode == 200) && data.body){
							var properties = $(data.body);
							$(properties).find('[data-name]').each(function(){
								if(dataName = $(this).attr("data-name")){
									$('#addProj [data-name='+dataName+']').parent().removeClass('hide');
								}
							});
							$('#addProj .addProj').removeClass('hide');
							$('#addProj .saveProj').addClass('hide');
							$('#addProj [data-name]').val('');
							$('#addProj').find('select').each(function(){
								if ($(this).find('option[selected]').length > 0){
									$(this).val($(this).find('option[selected]').val())
								}
							});
							eventHandler.menu.bookview.changeBookType();
							var proofdetails = $(properties).find('proof-details')
							$(proofdetails).find('[data-name]').each(function(){
								dataName = $(this).attr("data-name")
								var currObj = $(this)
								if(dataName && $('#addProj select[data-name='+dataName+']').length >0){
									$('#addProj select[data-name='+dataName+']').find('option').remove()
									$('#addProj select[data-name='+dataName+']').append($('<option value=""></option>'))	
									$(currObj).find('project').each(function(){
										var project = $(this)
										var element = $('<option value="'+$(this).attr("value")+'">'+ $(this).attr("name")+'</option>')
										if($('#addProj select[data-name='+dataName+']').attr('data-siblings-name') && $('#addProj select[data-name='+dataName+']').attr('data-siblings-name')!=''){
											var siblings = $('#addProj select[data-name='+dataName+']').attr('data-siblings-name').split(',')
											siblings.forEach(function(sibling){
												if(project.attr(sibling) && project.attr(sibling)!=''){
													$(element).attr(sibling,project.attr(sibling))
												}
											})
										}
										$('#addProj select[data-name='+dataName+']').append($(element))	
									})
								}
								
							})
							$('#addProj').modal('show');
						}
					},
					error:function(err){
						console.log(data);
					},
				});
			},
			sibblingBuilder:function(s){
				sb = '';
				if(s||s.length>0){
					$(s).each(function(i,e){
						v = $('#'+e).val();
						t = $('#'+e).attr('data-inner-tag');
						a = $('#'+e).attr('data-form-details-attr') ? eventHandler.menu.bookview.attr($('#'+e).attr('data-form-details-attr').split('||')):'';
						sb+=eventHandler.menu.bookview.builder(v,t,a);
					});
					return sb;
				}
				return '';
			},
			builder:function(value,tag,attr){
				if(value&&tag&&attr) return '<'+tag+' '+attr+'>'+value+'</'+tag+'>';
				else if(value&&tag) return '<'+tag+'>'+value+'</'+tag+'>';
				else return value;
			},
			attr:function(attr){
				s = '';
				$(attr).each(function(i,e){
					v = e.split('|');
					if(v.length==2){
						s += v[0]+'="'+v[1]+'" ';
					}
				});
				return s;
			},
			addProj: function(param,targetNode){
				var projectExisting = $('.projectlist li').map(function() {return $(this).attr('data-project');}).get().filter(v => v) ;
				projectExisting = projectExisting ? projectExisting : [];
				params = {
				};
				var projectName = '';
				if($('#bookTitle').val() == '' || $('[data-cloned="true"] [data-name]').first().val() == ''){
					showMessage({ 'text': eventHandler.messageCode.errorCode[502][7], 'type': 'error' }); return false;
				}
				// create project name with some random number
				var projectName = $('.projectlist li').attr('data-customer') + '.' + (Math.floor(Math.random()*90000) + 10000);
				while ($('.projectList .project[data-project="' + projectName + '"]').length > 0){
					projectName = $('.projectlist li').attr('data-customer') + '.' + (Math.floor(Math.random()*90000) + 10000);
				}
				$('#journal-id').val(projectName);
				var empty = false;
				params.XML='<nodes>';

				$('#addProj [data-form-details]:not(.hide)').each(function(){
					if ($(this).closest('.hide').length > 0){
						return true;
					}
					var value = $(this).val();
					// if(!value && !($(this).parent().hasClass('hidden') || $(this).closest('div.row').hasClass('hide'))) {
					// 	// 	showMessage({ 'text': eventHandler.messageCode.errorCode[502][7], 'type': 'error' }); empty=true;
					// }
					var formDetail = $(this).attr('data-form-details').split('||');
					var attr = ($(this).attr('data-form-details-attr') && formDetail[3]) ? eventHandler.menu.bookview.attr($(this).attr('data-form-details-attr').split('||')) : '';
					if (attr != ""){
						attr = ' ' + attr;
					}
					if ($(this).closest('[data-parent-name]').length > 0){
						value = "";
						$(this).closest('[data-parent-name]').find('[data-name]').each(function(){
							if ($(this).val() != "" && $(this).val() != null){
								var tagName = $(this).attr('data-name');
								value += '<' + tagName + '>' + $(this).val() + '</' + tagName + '>';
							}
						})
						var tagName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						value += '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					}else{
						var tagName = $(this).attr('data-name');
						value = '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					}
					var currentElement = $(this)
					if ($(this).attr('data-siblings-name')&& $(this).attr('data-siblings-name')!=''){
						var siblings = $(this).attr('data-siblings-name').split(',')
						//var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
						siblings.forEach(function(sibling){
							var siblingTagName = sibling
							var siblingValue = $(currentElement).find('option[value="'+currentElement.val()+'"]').attr(sibling)
							if(siblingTagName && siblingValue){
								value += '<' + siblingTagName + attr + '>' + siblingValue + '</' + siblingTagName + '>';
							}	
						})
					}
					var innerTag = $(this).attr('data-inner-tag') ? $(this).attr('data-inner-tag') : '';
					if((value) && (value!=null) && (formDetail.length >= 3)){
						params.XML += '<node><id>' + $(this).attr('id') + '</id><section>' + formDetail[0] + '</section><xpath>' + formDetail[1] + '</xpath><tagname>'+formDetail[2]+'</tagname><value>'+value+'</value></node>';
					}
				});
				if(empty) return false;

				params.XML+='</nodes>';

				params.addProj = {
					customer : $('.projectlist li').attr('data-customer'),
					project : projectName,
					projectFullName : $('#bookTitle').val(),
					projectType : 'book'
				}

				// if the project name already Exists
				if(projectExisting.indexOf(params.addProj.project.trim().toLowerCase().replace(' ','_'))>-1){
					showMessage({ 'text': eventHandler.messageCode.errorCode[502][3], 'type': 'error' });
					return false;
				}

				if(params.addProj.project && params.addProj.customer){
					$('.la-container').fadeIn();
					$.ajax({
						type:"POST",
						url:"/api/addproject",
						data:params.addProj,
						success:function(data){
							$.ajax({
								type:"POST",
								url:"/api/bookmeta",
								data:params,
								success:function(data){
									if(data.statusCode==200){
										$('[type="text"][data-form-details],[type="text"][data-name]').val('');
										$('select[data-form-details] option[value=""]').attr('selected', true);
										$('.dropdown-menu .custom-select').removeClass('custom-select');
										$('#addProj .dynamic').remove();
										eventHandler.menu.bookview.appendContent = 0;
										$('#addProj').modal('hide');
										eventHandler.general.actions.getProjectList($('#projectVal').attr('data-customer'),$('div.filter-list.customer.active'))
										.then(function(){
											$('.la-container').fadeOut();
										})
										.catch(function(err){
											$('.la-container').fadeOut();
											console.log('error getProjectList')
										})
									}
									else{
										$('.la-container').fadeOut();
										showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
									}
								},
								error:function(err){
									$('.la-container').fadeOut();
									showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
								}
							});
						},
						error: function (data) {
							$('.la-container').fadeOut();
							showMessage({ 'text':  eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
						},
					});
				}
				else{
					$('.la-container').fadeOut();
					showMessage({ 'text': eventHandler.messageCode.errorCode[502][4], 'type': 'error' });
				}
			},
			saveProj: function(param, targetNode){
				var metaUpdate = false;
				var modifiedArray = [];
				$('#addProj [data-form-details]:not(.hide)').each(function(){
					if ($(this).closest('.hide').length > 0){
						return true;
					}
					var myMeta = $('#tocContainer').data('binder')['container-xml'].meta;
					var key = $(this).attr('data-name');
					if ($(this).closest('[data-parent-name]').length > 0){
						var tagName = $(this).closest('[data-parent-name]').attr('data-tag-name');
						var parentName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						if (modifiedArray.indexOf(tagName) < 0){
							modifiedArray.push(tagName);
							myMeta[tagName] = {};
							myMeta[tagName][parentName] = [];
						}
						var newObj = {}
						var attributes = $(this).closest('[data-parent-name]').attr('data-form-details-attr').split('||');
						if (attributes.length > 0){
							newObj._attributes = {}
							$(attributes).each(function(i,e){
								v = e.split('|');
								if (v.length == 2){
									newObj._attributes[v[0]] = v[1];
								}
							});
						}
						$(this).closest('[data-parent-name]').find('[data-name]').each(function(){
							if ($(this).val() != "" && $(this).val() != null){
								var value = $(this).attr('data-name');
								newObj[value] = {};
								newObj[value]._text = $(this).val();
							}
						})
						myMeta[tagName][parentName].push(newObj);
					}
					else if($('#addProj [data-form-details][data-name="' + key + '"]').length > 1){
						if (modifiedArray.indexOf(key) < 0){
							modifiedArray.push(key);
							myMeta[key] = [];
						}
						if ($(this).val() != '' && $(this).val() != null){
							var newObj = {}
							newObj._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr').split('||');
							if (attributes.length > 0){
								newObj._attributes = {}
								$(attributes).each(function(i,e){
									v = e.split('|');
									if (v.length == 2){
										newObj._attributes[v[0]] = v[1];
									}
								});
							}
							myMeta[key].push(newObj);
						}
					}
					else{
						if ($(this).val() == '' || $(this).val() == null){
							if (myMeta[key]){
								delete(myMeta[key])
							}
						}
						else if (myMeta[key]){
							myMeta[key]._text = $(this).val();
						}
						else{
							myMeta[key] = {};
							myMeta[key]._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
							if (attributes && attributes.length > 0){
								myMeta[key]._attributes = {}
								$(attributes).each(function(i,e){
									v = e.split('|');
									if (v.length == 2){
										myMeta[key]._attributes[v[0]] = v[1];
									}
								});
							}
						}
					}
					//
					var current = $(this)
					if ($(this).attr('data-siblings-name')&& $(this).attr('data-siblings-name')!=''){
						var siblings = $(this).attr('data-siblings-name').split(',')
						var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
						siblings.forEach(function(sibling){
							//console.log(sibling)
							if($(current).find('option[value="'+current.val()+'"]').length >0 && $(current).find('option[value="'+current.val()+'"]').attr(sibling) && $(current).find('option[value="'+current.val()+'"]').attr(sibling)!=''){
								myMeta[sibling] = {};
								myMeta[sibling]._text = $(current).find('option[value="'+current.val()+'"]').attr(sibling);
								if (attributes && attributes.length > 0){
									myMeta[sibling]._attributes = {}
									$(attributes).each(function(i,e){
										v = e.split('|');
										if (v.length == 2){
											myMeta[sibling]._attributes[v[0]] = v[1];
										}
									});
								}
							}
							
						})
					}
				});
				eventHandler.general.actions.saveData();
				$('#addProj').modal('hide');
			},
			editBookMeta: function (param, targetNode){
				$('#addProj .addProj').addClass('hide');
				$('#addProj .saveProj').removeClass('hide');
				$('#addProj').modal('show');
			}
		},
        data: {
            filter: function(param, targetNode){
                var input, filter, ul, li, a, i;
				filter = targetNode.val();
				var filterPath = targetNode.attr('data-filter-path');
				$(filterPath).removeClass('hide');
                // Declare variables
                if(filter.length>0){
                    $.expr[':'].containsIgnoreCase = function (n, i, m) {
                            return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) < 0;
                        };
					$(filterPath + ':containsIgnoreCase("' + filter + '")').addClass('hide');
				}
			},
			exportExcel: function () {
				var contentData = jQuery.extend(true, {}, ($('#tocContainer').data('binder')['container-xml']['contents']['content'])); // cloning to new object  
				var info = new Array();
				var totalPages = 0;
				var mergeCount=1;
				//seetha kumaraswamy{exeter} getting the value of color page and toc and edboard from issue config
				var coverColor=configData["binder"]["cover"]._attributes["data-preflight-profile"];
				var tocColor=configData["binder"]["table-of-contents"]._attributes["data-preflight-profile"];
				var edBoardColor=configData["binder"]["editorial-board"]._attributes["data-preflight-profile"];
				// Maniplating and collecting data from json 
				for (var data in contentData) {
					var fPage = contentData[data]._attributes["fpage"];
					var lPage = contentData[data]._attributes["lpage"];
					var id = contentData[data]._attributes["id"];
					if ((typeof (id) != "string") && (typeof (id) != "number")) { // to handle is manuscript id not present
						contentData[data]._attributes["id"] = "";
					}
					if (contentData[data]._attributes["type"] == "cover") {
						contentData[data]._attributes["id"] = "Cover";
					}
					else if (contentData[data]._attributes["type"] == "editorial-board") {
						contentData[data]._attributes["id"] = "Editorial board";
					}
					else if (contentData[data]._attributes["type"] == "table-of-contents") {
						contentData[data]._attributes["id"] = "TOC";
					}
					else if ((contentData[data]._attributes["type"] == "advert_unpaginated") || (contentData[data]._attributes["type"] == "advert")) {
						contentData[data]._attributes["adverts"] = contentData[data]._attributes["id"];
						contentData[data]._attributes["id"] = "Advert";
						contentData[data]._attributes["advert_Pages"] = " ";
					}
					if (/[a-zA-Z]/gi.test(fPage)) {
						fPage = 0;
					}
					if (/[a-zA-Z]/gi.test(lPage)) {
						lPage = 0;
					}
					var figColorData = contentData[data].color;
					var figColorDetails = "";
					var monoPages = "";
					var colorPg = "";
					var page_1=[],count_1=0;    
					var colorPageFlag = true;
					if ((typeof (figColorData) == "object") && (typeof (figColorData.length) == "undefined")) { // handling article with one figure
						figColorData = [figColorData];
					}
					for (var fig in figColorData) {
						if (typeof (figColorData[fig]._attributes) == "object") { // fetching color page details
							if (parseInt(figColorData[fig]._attributes["color"])) {
								if (fig == 0) {
									figColorDetails = figColorData[fig]._attributes["label"].replace(/[a-zA-Z]+\s/gi, "");
									page_1[count_1] =parseInt(figColorData[fig]._attributes["data-page-num"]);
								}
								else {
									figColorDetails = figColorDetails + ", " + figColorData[fig]._attributes["label"].replace(/[a-zA-Z]+\s/gi, "");
									page_1[count_1] =parseInt(figColorData[fig]._attributes["data-page-num"]);
								}
							}
							else if (figColorData[fig]._attributes["type"] == "page") {
								monoPages = " ";
								figColorDetails = "Colour";
							}
						}
						count_1=count_1+1;
					}
					//seetha kumaraswamy{exeter}page range for the color figure pages
					var counter=-1,index=0,colorfigpg = [];
					
					while (index<(page_1.length))
					{
						if(page_1[index]+1==page_1[index+1] && counter==-1)
						{
							colorfigpg[colorfigpg.length]=page_1[index]+"-";
    						counter=page_1[index];
						}
						while(page_1[index]==counter)
						{
							counter=counter+1;
							index=index+1;
							if(index==(page_1.length))
							{
								break;
							}
						}
						if(counter!=-1)
						{
							colorfigpg[(colorfigpg.length)-1]=colorfigpg[(colorfigpg.length)-1]+(page_1[index-1]);
						}
						else
						{
							colorfigpg[colorfigpg.length]=page_1[index];
							index=index+1;
						}
					counter=-1;	

					}
					//seetha kumaraswamy{exeter}page range program ends here	
					if (figColorDetails == "") {
						figColorDetails = " ";
						if (contentData[data]._attributes["type"] == "manuscript") { // collecting mono page details
							monoPages = fPage + "-" + lPage;
						}
						else {
							monoPages = "Mono";
						}
					}
					else
					{
						//seetha kumaraswamy{exeter}to find the pages without figures and store it into array
						var fPage1 = parseInt(fPage);
						var lPage1 = parseInt(lPage);
						var count_2,bwpg=[],counter_1=0,count,counter_1=0;
						for(count_1=fPage1;count_1<=lPage1;count_1++)
						{
							count=0;
							for(count_2=0;count_2<page_1.length;count_2++)
							{
								if(count_1!=page_1[count_2])
								{
									count=count+1
									
								}
								if(count==page_1.length)
								{
									bwpg[counter_1]=count_1;
									counter_1=counter_1+1;
								}
							}
							
						}
						//seetha kumaraswamy{exeter} to set the page range for the black&white pages
					counter=-1,index=0;
					var monoPages = [];
					
					while (index<(bwpg.length))
					{
						if(bwpg[index]+1==bwpg[index+1] && counter==-1)
						{
							monoPages[monoPages.length]=bwpg[index]+"-";
    						counter=bwpg[index];
						}
						while(bwpg[index]==counter)
						{
							counter=counter+1;
							index=index+1;
							if(index==(bwpg.length))
							{
								break;
							}
						}
						if(counter!=-1)
						{
							monoPages[(monoPages.length)-1]=monoPages[(monoPages.length)-1]+(bwpg[index-1]);
						}
						else
						{
							monoPages[monoPages.length]=bwpg[index];
							index=index+1;
						}
					counter=-1;	

					}
					//seetha kumaraswamy{exeter} end of program page range	
					}
					if ((monoPages == "") || (monoPages == "0-0")) {
						monoPages = " ";
					}
					fPage = parseInt(fPage);
					lPage = parseInt(lPage);
					contentData[data]._attributes["fpage_runon"] = fPage;
					contentData[data]._attributes["lpage_runon"] = lPage;
					//seetha kumaraswamy{exeter} to check for the advert coloured pages
					if ((typeof (contentData[data]["color"]) == "object") && ((contentData[data]._attributes["type"]) == "advert")) {
						if (typeof (contentData[data].color["_attributes"]) == "object") {
							if (contentData[data].color["_attributes"]["type"] == "page") {
								monoPages = " ";
								colorPg = "Colour";
							}
							else
							monoPages="mono";
						}
					}
					//seetha kumaraswamy{exeter}for filling up the color pages
					colorPg=colorfigpg;
					contentData[data]._attributes["Pages"] = (lPage - fPage) + 1;
					if (contentData[data]._attributes["type"] == "table-of-contents") { //toc page count are handled here 
						contentData[data]._attributes["Pages"] = parseInt(contentData[data]._attributes["pageExtent"]);
					}
					if (typeof (contentData[data]._attributes["data-run-on"]) == "string") { // handling runon article page count
						contentData[data - mergeCount]._attributes["Pages"] = (lPage - contentData[data - mergeCount]._attributes["fpage_runon"]) + 1;
						delete contentData[data]._attributes["Pages"];
						if (typeof (contentData[data - mergeCount]._attributes["mergecell"]) == "number") { // collecting data for cell merge
							contentData[data - mergeCount]._attributes["mergecell"] = contentData[data - mergeCount]._attributes["mergecell"] + 1;	
							mergeCount=mergeCount+1;
						}
						else {
							contentData[data - mergeCount]._attributes["mergecell"] = 2;
							mergeCount=mergeCount+1;
						}
					}
					else{
						mergeCount=1;
					}
					//seetha kumaraswamy{exeter}finding color pages for TOC,cover,Editorial Board
					if(contentData[data]._attributes["type"] != "editorial-board"&&contentData[data]._attributes["type"] != "cover"&&contentData[data]._attributes["type"] != "editorial-board"&&fPage1-lPage1==0 && typeof(contentData[data]["color"])=="object")
					{
						//if(contentData[data]["color"]._attributes["color"]=="1")
						{
						monoPages="-";
						colorPg=fPage1;
						colorfigpg=fPage1;
						}
					}
					if (contentData[data]._attributes["type"] == "cover")
					{
						if(coverColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}
					if (contentData[data]._attributes["type"] == "editorial-board")
					{
						if(edBoardColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}
					if (contentData[data]._attributes["type"] == "table-of-contents")
					{
						if(tocColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}

					contentData[data]._attributes["Mono Pages"] = monoPages;
					contentData[data]._attributes["Colour pages"] = colorPg;
					//seetha kumaraswamy{exeter}to makes the condition and not conside with others while filling up the remaining page of color page, color figure details and color figure pages 
					if(colorPg=="Colour")
						{
							contentData[data]._attributes["Colour figure details"] = "";
						}
					else 
						{
							contentData[data]._attributes["Colour figure details"] =figColorDetails;
						}	
					if(colorfigpg=="Colour")
						{
							contentData[data]._attributes["Colour figure pages"] = "";
						}
					else 
						{
							contentData[data]._attributes["Colour figure pages"] =colorfigpg;
						}
					
					if ((contentData[data]._attributes["Pages"] != " ") && (typeof (contentData[data]._attributes["Pages"]) != "undefined")) {
						totalPages = totalPages + contentData[data]._attributes["Pages"];
						//seetha kumarasway{exeter} to slter and fill for your requirement
					}
					info.push(contentData[data]._attributes);
				}
				var totalPageNo = new Object();
				totalPageNo["id"] = " ";
				totalPageNo["fpage"] = " ";
				totalPageNo["lpage"] = "Total";
				totalPageNo["Pages"] = totalPages;
				totalPageNo["Mono Pages"] = " ";
				totalPageNo["Colour pages"] = " ";
				totalPageNo["Colour figure details"] = " ";
				totalPageNo["Colour figure pages"] = " ";
				info.push(totalPageNo);
				$('#tocContainer').data('binder')['container-xml']['meta']["totalPageNo"] = totalPages;
				// json converted to html
				$("#dvjson").excelexportjs({
					containerid: "dvjson"
					, datatype: 'json'
					, dataset: info
					, columns: getColumns(info)
				});
			},
			changeSectionHead :function(param, targetNode){
				var changedContent = _.escape($('#changeSectionHead').find('input[class~=sectionHead_change]').val());
				changedContent = changedContent.replace(/[\u00A0-\u00FF\u0100-\u017F\u0180-\u024F\u2022-\u2135\u4e00-\u9fff\u0600-\u06FF\u0D80-\u0DFF\u20A0-\u20CF\u2190-\u21FF\u2200-\u22FF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u2600-\u26FF\u25A0-\u25FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0700-\u074F\u0780-\u07BF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u13A0-\u13FF\u1400-\u167F\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u19E0-\u19FF\u1D00-\u1D7F\u1E00-\u1EFF\u1F00-\u1FFF\u20D0-\u20FF\u2150-\u218F\u2070-\u209F\u2100-\u214F\u2000-\u206F\u2300-\u23FF\u2400-\u243F\u3000-\u303F\u0300-\u036F\u0370-\u03FF\u2440-\u245F\u2460-\u24FF\u2500-\u257F\u2580-\u259F\u2700-\u27BF\u27C0-\u27EF\u27F0-\u27FF\u2800-\u28FF\u2900-\u297F\u2980-\u29FF\u2A00-\u2AFF\u2B00-\u2BFF\u2E80-\u2EFF\u2F00-\u2FDF\u2FF0-\u2FFF\u3040-\u309F\u30A0-\u30FF\u3100-\u312F\u3130-\u318F\u3190-\u319F\u31A0-\u31BF\u31F0-\u31FF\u3200-\u32FF\u3300-\u33FF\u3400-\u4DBF\u4DC0-\u4DFF\uA000-\uA48F\uA490-\uA4CF\uAC00-\uD7AF\uD800-\uDB7F\uDB80-\uDBFF\uDC00-\uDFFF\uE000-\uF8FF\uF900-\uFAFF\uFB00-\uFB4F\uFB50-\uFDFF\uFE00-\uFE0F\uFE20-\uFE2F\uFE30-\uFE4F\uFE50-\uFE6F\uFE70-\uFEFF\uFF00-\uFFEF\uFFF0-\uFFFF\u0250-\u02AF\u02B0-\u02FF]/g, function (c) {
					return '&#x' + c.charCodeAt(0).toString(16) + ';';    
				});
				var currentSectionhead = $('#changeSectionHead').find('input[class~=sectionHead_change]').attr('data-org-data');
				var contents = $('#tocContainer').data('binder')['container-xml'].contents.content;
				//get all contents that matches cuurent sectionhead and replace with changed value
				contents.forEach(function (entry) {
					var sectionType = entry._attributes.section ? entry._attributes.section : '';
					if(sectionType != '' || sectionType !=undefined){
						sectionType = sectionType.trim();
					 } 
					if(sectionType ==  currentSectionhead){
						entry._attributes.section  = changedContent;
					}
				});
				$('#changeSectionHead').modal('hide');
				$('#changeSectionHead').find('input[class~=sectionHead_change]').val('');
				
                $('#changeSectionHead').find('input[class~=sectionHead_change]').removeAttr('data-org-data');		
				var histObj = {
					"change": "changed <b> sectionhead </b> from <u>" + currentSectionhead + '</u> to <u>' + changedContent + '</u>',
					"file": "",
					"fileType": "",
					"id-type": ""
				};
				updateChangeHistory(histObj);
				eventHandler.general.actions.saveData();
				eventHandler.general.components.populateTOC();
			}
		}
	},
	eventHandler.upload = {
		uploadFile: function(param, targetNode){
			var thisObj = this;
			var file = param.file;
			$.ajax({
				url: '/getS3UploadCredentials',
				type: 'GET',
				data: {'filename': file.name},
				success: function(response){
					if (response && response.upload_url){
						var formData = new FormData();
						$.each(response.params, function(k, v){
							formData.append(k, v);
						})
						formData.append('file', file, file.name);
						param.response = response;
						param.formData = formData;
						thisObj.uploadToServer(param, targetNode);
					}
				},
				error: function(response){
					if(param && typeof(param.error) == "function"){
						param.error(response);
					}
				}
			});
		},
		uploadToServer: function(param, targetNode){
			var file = param.file;
			var formData = param.formData;
			var response = param.response;
			$.ajax({
				url: response.upload_url,
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if(param && typeof(param.progress) == "function"){
							param.progress(evt);
						}
					}, false);
					xhr.addEventListener("progress", function (evt) {
						if(param && typeof(param.progress) == "function"){
							param.progress(evt);
						}
					}, false);
					return xhr;
				},
				success: function(res){
					if (res && res.hasChildNodes()){
						var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
						if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
							var ext = file.name.replace(/^.*\./, '')
							var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
							var customer = $('#projectVal').attr('data-customer');
							var project = $('#projectVal').attr('data-project');
							var volume = $('#tocContainer').data('binder')['container-xml'].meta.volume ? $('#tocContainer').data('binder')['container-xml'].meta.volume._text : ''
							var issue = $('#tocContainer').data('binder')['container-xml'].meta.issue ? $('#tocContainer').data('binder')['container-xml'].meta.issue._text : ''
							
							var params = {
								srcBucket: uploadResult.find('Bucket,bucket').text(),
								srcKey: uploadResult.find('Key,key').text(),
								dstBucket: "kriya-resources-bucket",
								dstKey: volume ? customer + '/' + project + '/' + volume + '_' + issue + '/resources/' + fileName : customer + '/' + project + '/resources/' + fileName,
								convert : 'false'
							}
							$.ajax({
								async: true,
								crossDomain: true,
								url: response.apiURL,
								method: "POST",
								headers: {
									accept: "application/json"
								},
								data: JSON.stringify(params),
								success: function(status){
									console.log(status);
									if(param && typeof(param.success) == "function"){
										param.success(status);
									}
									//var fileDetailsObj = status.fileDetailsObj;
									//$(targetNode).attr('src', '/resources/' + fileDetailsObj.dstKey);
								},
								error: function(err){
									if(param && typeof(param.error) == "function"){
										param.error(err);
									}
								}
							});
						}
					}
				},
				error: function(response){
					if(param && typeof(param.error) == "function"){
						param.error(response);
					}
				}
			});
		}
	}
	eventHandler.pdf = {
		export:{
			exportToPDF: function(param, targetNode){
				var metaObj = $('#tocContainer').data('binder')['container-xml'].meta;
				var firstMsCid = $('#tocContainer .bodyDiv [data-type="manuscript"]:eq(0)').attr('data-uuid');
				var cid = $('#manuscriptData #msDataDiv').attr('data-c-rid');
				console.log(cid)
				if(param && param.cid){
					cid = param.cid;
				}
				var uid = $('#manuscriptData #msDataDiv').attr('data-r-uuid');
				if(param && param.uid){
					uid = param.uid
				}
				var doinum =  $('#manuscriptData #msDataDiv').attr('data-dc-doi');
				var draftFlag = false
				if(metaObj.issue && metaObj.issue['_attributes'] && metaObj.issue['_attributes']["issue-type"] && metaObj.issue['_attributes']["issue-type"].toLowerCase() == 'draft'){
					draftFlag = true
				}	
				if (param && param.draftFlag){
					draftFlag = param.draftFlag
				}	
				//if draft flag is true and index is 0, call exportModofied with param.type='draft' to export all manuscript
				if(firstMsCid == uid && draftFlag == true){
					param.type = 'draft'
					eventHandler.pdf.export.exportModified(param, 0);
				}
				else{
					//var articleObj = $('[data-c-id="' + cid + '"]').data('data');
					var articleObj;
					if($('[data-uuid ="' + uid + '"]').length >0){
						articleObj = $('[data-uuid ="' + uid + '"]').data('data');
					}
					else {
						//unable to proof / preflight doi
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
						return false;
					}
					if(articleObj['_attributes']['type'] == "blank"){
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
						return false;
					}

					var doi      = articleObj['_attributes']['id'];
					var ms_id = articleObj['_attributes']['ms-id'] ? articleObj['_attributes']['ms-id'] : articleObj['_attributes']['id']
					var customer = $('#projectVal').attr('data-customer');
					var project  = $('#projectVal').attr('data-project');
					var projectID = $('#projectVal').attr('data-project-id')
					var fpage    = articleObj['_attributes']['fpage'];
					var lpage    = articleObj['_attributes']['lpage'];

					var userName = $('.userinfo:first').attr('data-name');
					var userRole = 'typesetter';

					if(param && param.doi){
						doi = param.doi;
					}
					var proofType = 'print';
					var processType = 'InDesignSetter';
					var exportAllarticleFlag = 'false';
					if (param && param.type) proofType = param.type;
					if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
					if (param && param.processType) processType = param.processType;
					parameters = {
						"client"     : customer,
						"project"    : project, 
						"idType"     : "doi", 
						"id"         : doi, 
						"msid"      : ms_id,
						//"processType": "InDesignSetter", 
						"processType": processType, 
						"proof"      : proofType, 
						//"volume"     : metaObj.volume._text, 
						//"issue"      : metaObj.issue._text, 
						"fpage"      : fpage,
						"lpage"      : lpage,					
						"userName"   : userName, 
						"userRole"   : userRole,
						"uuid"       : uid,
						"exportAllArticles": exportAllarticleFlag
					}
					if(projectID && projectID!=''){
						parameters['project-id'] = projectID
					}
					if(metaObj.volume){
						parameters.volume = metaObj.volume._text;
					}
					if(metaObj.issue){
						parameters.issue = metaObj.issue._text;
					}
					var colorObj  = articleObj['color']
					
					var dataColor = '';
					if(colorObj){
						if (colorObj.constructor.toString().indexOf("Array") == -1) {
							colorObj = [colorObj];
						}
						colorObj.forEach(function (color) {
							if(color['_attributes']['color'] == '1'){
								dataColor += color['_attributes']['ref-id']+',';
							}
						})
					}

					dataColor = dataColor.replace(/\,$/, '');
					if(dataColor){
						parameters['data-color'] = dataColor;
					}
					
					var title = '';
					var volume = metaObj.volume ? metaObj.volume._text :''
					var issue = metaObj.issue ? metaObj.issue._text :''
					var fileName = $('#issueVal').attr('data-file');
					if(doi){
						title = 'Proofing ' + doi;
						parameters.issueMakeup = 'true';
						if (fileName && (/(draft)/gi.test(fileName))) {
						parameters.draftIssue = 'true';
						}
					}
					if(articleObj['_attributes']['type'] == 'cover'){
						parameters.pheripherals = 'cover';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_cover';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_cover';
						}
						title = 'Proofing Cover';	
					}
					else if(articleObj['_attributes']['type'] == 'table-of-contents' || articleObj['_attributes']['type'] == 'toc'){
						parameters.pheripherals = 'toc';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_toc';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_toc';
						}
						title = 'Proofing Toc';
					}
					else if(articleObj['_attributes']['type'] == 'index'){
						parameters.index = 'true';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_index';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_index';
						}
						title = 'Proofing Index';
					}
					else if(articleObj['_attributes']['type'] == 'advert'  || articleObj['_attributes']['type'] == 'advert_unpaginated'){
						var currRegion = targetNode.closest('div').parent().attr('data-region')
						parameters.pheripherals = 'advert';
						parameters.advertID = doi;
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
						}
						else{
							parameters.id = project + '_'  + doi + '_' + currRegion;
						}
						title = 'Proofing Advert_' + doi + '_' + currRegion;
						parameters.region = currRegion;
						if(volume && issue){
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
						}
						else{
							parameters.doi = project + '_' + doi + '_' + currRegion;
						}
					}
					if(processType == 'PDFPreflight'){
						parameters.doi = parameters.doi ? parameters.doi : doi
						title = 'Preflight for '+ parameters.doi;
						parameters.pdfLink = param.pdfLink
						parameters.profile = param.profile;
						var type = articleObj['_attributes']['type']; 
					//if type is manuscript and exist dataColor change profile to cmyk
						if (type == 'manuscript' && dataColor && param.profile == 'bw'){
							parameters.profile = 'cmyk'
						}
						else if (type == 'manuscript' && dataColor && param.profile == 'bw+1'){
							parameters.profile = 'cmyk+1'
						}
					}
					
					var notifyObj = new PNotify({
						title : title,
						text: '<div class="progress"><div class="determinate" style="width: 0%"></div><span class="progress-text">Sending request..</span></div>',
						hide: false,
						type: 'success',
						buttons: { sticker: false },
						addclass: 'export-pdf',
						icon: false
					});
					var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
					var notifyContainer = notifyObj.container[0];
					$(notifyContainer).attr('data-c-nid', cid);
					$(notifyContainer).attr('data-c-nuid', uid);
					
					$.ajax({
						type: 'POST',
						url: "/api/pagination",
						data: parameters,
						crossDomain: true,
						success: function(data){
							if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
								eventHandler.pdf.export.getJobStatus(customer, data.message.jobid, notifyObj, userName, userRole, doi, param);
							}
							else if(data == ''){
								notifyText.innerHTML = 'Failed..';
								if(param && param.error && typeof(param.error) == "function"){
									param.error();
								}
							}else{
								notifyText.innerHTML = data.status.message;
								if(param && param.error && typeof(param.error) == "function"){
									param.error();
								}
							}
						},
						error: function(error){
							notifyText.innerHTML = 'Failed..';
							if(param && param.error && typeof(param.error) == "function"){
								param.error();
							}
						}
					});
				}	
			},
			getJobStatus: function(customer, jobID, notifyObj, userName, userRole, doi, param){
				var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
				var progressBar = $(notifyObj.text_container[0]).find('.determinate');
				var notifyTitle = notifyObj.title_container[0];
				var notifyContainer = notifyObj.container[0];

				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.input && data.status.message.input.uuid){
							var proofUUID = data.status.message.input.uuid
							if($('div[data-uuid="'+proofUUID+'"]').length > 0){
								if(data && data.status && data.status.code && data.status.message && data.status.message.status){
									var status = data.status.message.status;
									var code = data.status.code;
									var currStep = 'Queue';
									if(data.status.message.stage.current){
										currStep = data.status.message.stage.current;
									}
									if(data.status.message.progress){
										var barWidth = data.status.message.progress/3;
										if(data.status.message.stage.current == "collectfiles"){
											barWidth = barWidth+'%';
										}else if(data.status.message.stage.current == "proofing"){
											barWidth = (barWidth+33.3)+'%';
										}else if(data.status.message.stage.current == "uploadPDF"){
											barWidth = (barWidth+66.6)+'%';
										}

										progressBar.css('width', barWidth);
									}
									var loglen = data.status.message.log.length;    
									var process = data.status.message.log[loglen-1];
									if (/completed/i.test(status)){
										var loglen = data.status.message.log.length;	
										var process = data.status.message.log[loglen-1];
										var processType = param.processType;
										var proofDoi, proofId;
										if(data.status.message.input && data.status.message.input.doi){
											proofDoi = data.status.message.input.doi
										}
										if(data.status.message.input && data.status.message.input.id){
											proofId = data.status.message.input.id
										}
										//notifyText.innerHTML = process;
										if(processType == 'PDFPreflight'){
											var preflightLink = param.pdfLink;
											preflightLink = preflightLink.replace('.pdf','_preflight.pdf')
										
											//display preflight link to user
											notifyTitle.innerHTML = preflightLink;
											notifyObj.remove();
													//var nid = $(notifyContainer).attr('data-c-nid');
											var nid = proofUUID
											if(nid && nid !='' && $('[data-uuid="' + nid + '"]').length >0){
												//set preflight flag as true
												var dataObj = $('[data-uuid="' + nid + '"]').data('data');
													dataObj['_attributes']['data-preflight'] = 'true';
												$('[data-uuid="' + nid + '"]').attr('data-preflight','true');
												var fileFlag = false;
												// save preflight pdf path in xml
												// for advert update file name in region
												//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
												if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated' ) ){
													var currRegion = targetNode.closest('div').parent().attr('data-region')
													currRegion = currRegion.toLowerCase();
													var fileFlag = false;
													var regObj = dataObj['region'];
													if(regObj){
														if (regObj.constructor.toString().indexOf("Array") == -1) {
															regObj = [regObj];
														}
														regObj.forEach(function (region) {
															if(region['_attributes']['type'] == currRegion){
																//region['_attributes']['path'] = pdfLink;
																var fileObj = region['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	if(file['_attributes'] &&  file['_attributes']['type'] && file['_attributes']['type'] == 'preflightPdf'){
																		file['_attributes']['path'] = preflightLink;
																		fileFlag = true;
																	}
																})
																if(fileFlag == false){
																	var file = ({"_attributes":{"type": "preflightPdf","path": preflightLink}})
																	fileObj.push(file)
																	region['file'] = fileObj;
																}
															}
														})
														eventHandler.general.actions.saveData();
														// save the preflight link in li to download
														if(targetNode){
														targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('target', '_blank');
														targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('href', preflightLink)
														}	
													}
												}
												else { //else if(dataObj['file']){
													var fileFlag = false
													var fileObj = []
													if(dataObj['file']){
														fileObj = dataObj['file'];
														if (fileObj.constructor.toString().indexOf("Array") == -1) {
															fileObj = [fileObj];
														}
														fileObj.forEach(function (file) {
															var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
															if(pdfType && pdfType == 'preflightPdf'){
																file['_attributes']['path'] = preflightLink;
																fileFlag = true;
															}
														})
													}
													if(fileFlag == false)	{
														var file = ({"_attributes":{"type": "preflightPdf", "path": preflightLink}})
														fileObj.push(file)
														dataObj['file'] = fileObj;
													}
												}
												// save the preflight link in li to download
												if($('#msDataDiv[data-r-uuid="'+nid+'"]').length >0){
													$('#msDataDiv[data-r-uuid="'+nid+'"').find('ul li[data-preflight="true"] a').attr('target', '_blank');
													$('#msDataDiv[data-r-uuid="'+nid+'"').find('ul li[data-preflight="true"] a').attr('href', preflightLink);	
												}
										
												eventHandler.general.actions.saveData();
												//var exportAllarticleFlag = 'false';
												//if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
												/*if(exportAllarticleFlag == 'false'){
													//send request to online pdf
													if(nid){
														param.uid = nid
													}
													param.processType = 'InDesignSetter';
													param.type = 'online';
													eventHandler.pdf.export.exportToPDF(param,targetNode);
												}*/
											}		
										}
										else{
											notifyTitle.innerHTML = 'PDF generation completed.';
											var proofType = 'print';
											if (param && param.type) proofType = param.type;
										
											var pdfLink = $(process).attr('href');
											//pdfLink =pdfLink.replace('http:','https:');
											if (window.location.protocol == 'https:') {
												pdfLink = pdfLink.replace('http:','https:');
											}
											// get endPage and PageCount from JobManager
											var endPage,pageCount;
											var datajsonObj = $(process).attr('data-json');
											if(pdfLink && processType == 'InDesignSetter'){
												notifyObj.remove();
												//var nid = $(notifyContainer).attr('data-c-nid');
												var nid = proofUUID
												if(nid && nid !='' &&  $('div[data-uuid="'+ nid+'"]').length > 0){
													// if prooftype is print update the link in object , update download url 
													if(proofType == 'print') {
														if($('#msDataDiv[data-r-uuid="'+nid+'"]').length >0){
															$('#msDataDiv[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="print"] a').attr('target', '_blank');
															$('#msDataDiv[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="print"] a').attr('href', pdfLink);
															$('#msDataDiv[data-r-uuid="'+nid+'"]').find('object').attr('data', pdfLink);
														}
													}
													else{
														//update download url
														if($('#msDataDiv[data-r-uuid="'+nid+'"]').length >0){
															$('#msDataDiv[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="online"] a').attr('target', '_blank');
															$('#msDataDiv[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="online"] a').attr('href', pdfLink);
														}
													}
													if(nid){
														var dataObj = $('[data-uuid="' + nid + '"]').data('data');
														var currentDoi = dataObj._attributes.id;
														dataObj._attributes['data-proof-doi'] = proofDoi;
														//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
														if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated')){
															var currRegion = targetNode.closest('div').parent().attr('data-region')
															currRegion = currRegion.toLowerCase();
															var fileFlag = false;
															var regObj = dataObj['region'];
															if(regObj){
																if (regObj.constructor.toString().indexOf("Array") == -1) {
																	regObj = [regObj];
																}
																regObj.forEach(function (region) {
																	if(region['_attributes']['type'] == currRegion){
																		//region['_attributes']['path'] = pdfLink;
																		region['_attributes']['data-proof-doi'] = proofDoi;
																		var fileObj = region['file'];
																		if (fileObj.constructor.toString().indexOf("Array") == -1) {
																			fileObj = [fileObj];
																		}
																		fileObj.forEach(function (file) {
																			if(file['_attributes'] &&  file['_attributes']['proofType'] && file['_attributes']['proofType'] == proofType){
																				file['_attributes']['path'] = pdfLink;
																				fileFlag = true;
																			}
																		})
																		if(fileFlag == false)	{
																			var file = ({"_attributes":{"type": "pdf","proofType":proofType, "path": pdfLink}})
																			fileObj.push(file)
																			region['file'] = fileObj;
																		}
																	}
																})
																eventHandler.general.actions.saveData();
																// save the pdf link in li to download
																if(proofType == 'print' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('href', pdfLink);
																}
																else if(proofType == 'online' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('href', pdfLink);
																}
															}
														}
														//loop through each file and update pdflink in path if proofType matches 
														else{//dataObj['file'] 
														dataObj._attributes['data-proof-doi'] = proofDoi;
															var fileFlag = false
															var fileObj = []
															if(dataObj['file']){
																fileObj = dataObj['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																	var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																	if(currProofType == proofType && pdfType != 'preflightPdf'){
																		file['_attributes']['path'] = pdfLink;
																		fileFlag = true
																	}
																})
															}
															if(fileFlag == false)	{
																var file = ({"_attributes":{"type": "pdf","proofType":proofType, "path": pdfLink}})
																fileObj.push(file)
																dataObj['file'] = fileObj;
															}
														}	
														if(datajsonObj){
															datajsonObj = datajsonObj.replace(/\'/g,'"')
															var datajson = JSON.parse(datajsonObj)
															endPage = datajson['LPAGE']
															pageCount = datajson['PAGE_COUNT']
															//update the  page details of figures 
															var colorObj  = dataObj['color']
															if(colorObj){
																if (colorObj.constructor.toString().indexOf("Array") == -1) {
																	colorObj = [colorObj];
																}
																for(var a in datajson){
																	if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																		var pageNum = datajson[a];
																		a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/,'$1$2')	
																		colorObj.forEach(function (color) {
																			if(color['_attributes']&& color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a ){
																				color['_attributes']['data-page-num'] = pageNum
																			}
																		})
																	}
																}
															}
															var runOnPagesDetails = datajson['RunOnPagesDetails']
															//if endpage, update endpage in issue xml,  
															//update page range in UI and update the following article page number if there is change, call updatepage number function
															if(endPage){
																if ((!dataObj['_attributes']['data-run-on']) || dataObj['_attributes']['data-run-on'] != 'true'){
																//check the config and match the prooftype. if the prooftype is mentioned in config update pagenumber
																//if attrib is not available in config, update pagenumber
																//exclude pagenumber update when follow-on is 1
																	if ((configData['page']) && (configData['page']._attributes['pageNumberProofType'])){
																		var values =  configData['page']._attributes['pageNumberProofType'].toLowerCase()
																		values = values.split(',');
																		if (values.constructor.toString().indexOf("Array") < 0) {
																			values = [values];
																		}
																		values.forEach(function (value) {
																			if(value == proofType){
																				dataObj['_attributes']['lpage'] = endPage;
																				dataObj['_attributes']['pageExtent'] = pageCount;
																				var seqId = $('[data-uuid="' + nid + '"]').attr('data-c-id')
																				updatePageNumber(seqId,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
																			}
																		})
																	}
																	else{
																		dataObj['_attributes']['lpage'] = endPage;
																		dataObj['_attributes']['pageExtent'] = pageCount;
																		var seqId = $('[data-uuid="' + nid + '"]').attr('data-c-id')
																		updatePageNumber(seqId,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
																	}
																}
															}
															//if current artcile has run-on articles - update pdflink in all run-on articles, and update the f-page, l-page for run-on articles
															if(runOnPagesDetails){
																var currArtObj = $('[data-uuid="' + nid + '"]');
																var mainArtDoi =$(currArtObj).attr('data-doi')
																var subarticles = currArtObj.nextUntil('[data-c-id][data-type="manuscript"][data-run-on!="true"]')
																subarticles.each(function(){
																	var currNode = $(this).data('data');
																	if(currNode){
																		var doi = currNode._attributes.id
																		//if(doi == currNode._attributes.id){
																		if (doi && $('[data-c-id][data-doi="'+ doi +'"][data-runon-with="'+ mainArtDoi +'"]').length > 0) {
																			var followOnAricle = $('[data-c-id][data-doi="'+ doi +'"][data-runon-with="'+ mainArtDoi +'"]').data('data')
																			if(followOnAricle['file']){
																				var fileObj = followOnAricle['file'];
																				if (fileObj.constructor.toString().indexOf("Array") == -1) {
																					fileObj = [fileObj];
																				}
																				// update pdflink based on proofType
																				fileObj.forEach(function (file) {
																					var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																					var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																					if(currProofType == proofType && pdfType != 'preflightPdf'){
																						file['_attributes']['path'] = pdfLink;
																					}
																				})
																			}
																			// update the fpage,lpage, figure page details  for run-on articles
																			if( runOnPagesDetails[doi]){
																				followOnAricle['_attributes']['fpage'] = runOnPagesDetails[doi]['f-page']
																				followOnAricle['_attributes']['lpage'] = runOnPagesDetails[doi]['l-page']
																				if(followOnAricle['color']){
																					for (var a in runOnPagesDetails[doi]){
																						if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																							var pageNum = runOnPagesDetails[doi][a];
																							a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/,'$1$2')	
																							var runOnColorObj = followOnAricle['color']
																							if(runOnColorObj){
																								if (runOnColorObj.constructor.toString().indexOf("Array") == -1) {
																									runOnColorObj = [runOnColorObj];
																								}
																								runOnColorObj.forEach(function (color) {
																									if(color['_attributes']&& color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a ){
																										color['_attributes']['data-page-num'] = pageNum
																									}
																								})
																							}
																						}
																					}
																				}
																			}	
																		}
																		
																		//}
																	}
																})
															}
														}
														//remove data-modified attrib from xml,UI and remove title - pageModified
														if (dataObj._attributes['data-modified'] && (dataObj._attributes['data-modified'].toLowerCase() == 'true')){
															$('[data-uuid="' + nid + '"]').removeAttr('data-modified','true');
															$('[data-uuid="' + nid + '"]').find('.pageRange').removeAttr('title', 'Page is Modified');
															$('[data-uuid="' + nid + '"]').find('.pageRange').removeClass('pageModified');
															delete dataObj._attributes['data-modified'];
														}
														//set preflight flag as false
														dataObj['_attributes']['data-preflight'] = 'false';
														$('[data-uuid="' + nid + '"]').attr('data-preflight','false');
														eventHandler.general.actions.saveData();		
													}
													//	when proof type is print and processType is  InDesignSetter, send request to Preflight
													if( proofType == 'print' && param.processType == 'InDesignSetter'){
														param.processType = 'PDFPreflight';
														param.pdfLink = pdfLink;
														if(nid){
															param.uid = nid
														}
														//get preflight profile from config
														var type = dataObj['_attributes']['type']; 
														var preflightProfile = '';
														if(configData["binder"][type] && configData["binder"][type]['_attributes'] && configData["binder"][type]['_attributes']['data-preflight-profile']){
															preflightProfile = configData["binder"][type]['_attributes']['data-preflight-profile']
														}
														if(preflightProfile){
															param.profile = preflightProfile;
															eventHandler.pdf.export.exportToPDF(param,targetNode);
														}
													}	
												}
											}	
											if(param && param.success && typeof(param.success) == "function"){
												param.success();
												//
												//eventHandler.pdf.export.exportPDf(param,targetNode);
											}

										}
									}	
									else if (/failed/i.test(status) || code == '500'){
										notifyText.innerHTML = process;
										if(param.processType == 'InDesignSetter'){
											notifyTitle.innerHTML = 'PDF generation failed.';
											
										}
										else{
											notifyTitle.innerHTML = 'Preflight failed.';
										}
										if(param && param.error && typeof(param.error) == "function"){
											param.error();
										}
										//$('#'+notificationID+' .kriya-notice-body').html(process);
										//$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
									}
									else if (/no job found/i.test(status)){
										notifyText.innerHTML = status;
										//$('#'+notificationID+' .kriya-notice-body').html(status);
									}
									else{
										if(data.status.message.displayStatus){
										notifyText.innerHTML = data.status.message.displayStatus;
										}
										//$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
										var loglen = data.status.message.log.length;	
										if(loglen > 1){
											var process = data.status.message.log[loglen-1];
											if (process != '') notifyText.innerHTML = process;
											///if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
										}
										setTimeout(function() {
											eventHandler.pdf.export.getJobStatus(customer, jobID, notifyObj, userName, userRole, doi, param);
											//eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
										}, 1000 );
									}
								}	
								else{
									notifyText.innerHTML = 'Failed';
									//$('#'+notificationID+' .kriya-notice-body').html('Failed');
								}
							}
							else{
								//uuid is not present in current xml, so close the notify object
								notifyObj.remove();
							}
						}
						else{
							//uuid is not returned from job manager 
							notifyObj.remove();
						}
					},
					error: function(error){
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
					}
				});
			},
			exportModified: function(param, index){
				index = (index)?index:0;
				var thisObj = this;
				//var condition, if param.type is draft, condition is [data-type="manuscript"], else condition is [data-modofied="true"]
				var condition;
				if((param && param.type && param.type=='draft') || (param && param.exportType && param.exportType=='proofAllArticles')) {
					condition = ' [data-type="manuscript"][data-siteName!="Non-Kriya"]'	
				}
				else
				{
					condition = ' [data-modified="true"]'
				}
				var cid = $('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').attr('data-c-id');
				var uid = $('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').attr('data-uuid');
				
				var articleObj = $('[data-c-id="' + cid + '"]').data('data');

				var exportParam = {
					'draftFlag' :'false',
					'cid' : cid,
					'uid' : uid,
					'exportAllArticles': 'true',
					success: function(){
						$('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').removeAttr('data-modified', 'true');
						delete articleObj._attributes['data-modified'];
						eventHandler.general.actions.saveData();

						if($('#tocContainer .bodyDiv ' + condition +':eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv ' + condition + '').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					},
					error: function(){
						if($('#tocContainer .bodyDiv ' + condition +':eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv ' + condition +'').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					}
				}
				thisObj.exportToPDF(exportParam);
			},
			exportAllArticles: function(param){
				// export all manuscript based on proof Type and only when exportAllArticles flag is true
				var proofType = param.type
				param.exportAllArticles = 'true';
				var exportFlag = 'true'
				if(proofType == 'print'){
					exportFlag = $('#tocContainer').data('binder')['container-xml'].contents._attributes ? $('#tocContainer').data('binder')['container-xml'].contents._attributes.exportAllArticlesPrint : 'false'
				}
				else{
					exportFlag = $('#tocContainer').data('binder')['container-xml'].contents._attributes ? $('#tocContainer').data('binder')['container-xml'].contents._attributes.exportAllArticlesOnline : 'false'
				}
				if(exportFlag == 'false'){//true
					param.exportType = 'proofAllArticles'
					console.log(param)

					// check if any non-kriya article is avilable. If so, check whether PDF is uploaded 
					var pdfFlag = true
					var nonKriyaArticles = $('#tocBodyDiv div[data-c-id][data-type="manuscript"][data-siteName="Non-Kriya"]');
						nonKriyaArticles.each(function(){
							var currArticleNode = $(this);
							var currArticleNodeID = parseInt(currArticleNode.attr('data-c-id'));
							var  currArticleNodeData = currArticleNode.data('data');
							var currArticleNodeDOI = currArticleNodeData._attributes.id
							var fileObj = currArticleNodeData.file;
							fileObj.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
									if(currProofType == param.type){
										var pdfPath = file['_attributes']['path'];
										if(pdfPath == ''){
											pdfFlag = false
										}
									}
								})
						});
					// if pdf is not uploaded for non-kriya article, prompt user to upload pdf
					if(pdfFlag == false){
						showMessage({ 'text': param.type + '  ' + eventHandler.messageCode.errorCode[502][8] + ' ' + eventHandler.messageCode.errorCode[503][3], 'type': 'error' ,'hide':false});
					}
					//export all articles when pdf for techset articles are uploaded
					else{
						//set exportAllarticle flag as false
						if(proofType == 'print'){
							$('#tocContainer').data('binder')['container-xml'].contents._attributes.exportAllArticlesPrint = 'true'
						}
						else{
							$('#tocContainer').data('binder')['container-xml'].contents._attributes.exportAllArticlesOnline = 'true'
						}
						eventHandler.general.actions.saveData();
						eventHandler.pdf.export.exportModified(param, 0);
						
					}
				}
				else{
					//if exportFlag is true
					showMessage({ 'text':'Export All PDFs for ' + proofType + ' is '+ eventHandler.messageCode.errorCode[503][4], 'type': 'error','hide':false });
					return false;
				}	
			}
		},
		merge: {
			mergePDF: function(param, targetNode, checkModify){

				if($('#tocContainer .bodyDiv .sectionDataChild .pageRange.incorrectPage').length > 0){
					showMessage({ 'text': eventHandler.messageCode.errorCode[502][5], 'type': 'error','hide':false });
					return false;
				}

				if($('#tocContainer .bodyDiv [data-modified="true"]').length > 0 && checkModify != 'false'){
					(new PNotify({
	                    title: 'Confirmation Needed',
	                    text: 'Some articles was modified do you want to proof?',
	                    hide: false,
	                    confirm: { confirm: true },
	                    buttons: { closer: false, sticker: false },
	                    history: { history: false },
	                    addclass: 'stack-modal',
	                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                	})).get()
                    .on('pnotify.confirm', function () {
                      eventHandler.pdf.export.exportModified({
                      	success: function(){
                      		eventHandler.pdf.merge.mergePDF('', targetNode, 'false');	
                      	}
                      }); 
                    })
                    .on('pnotify.cancel', function () {
                    	eventHandler.pdf.merge.mergePDF('', targetNode, 'false');
                    });
                    return false;
				}

				var pdfLinks = [];
				//$('#tocContainer [data-c-id]').each(function(){
				//epage articles to be excluded while binding 
				$('#tocContainer [data-c-id][data-grouptype !="epage"]').each(function(){	
				var data = $(this).data('data');
					//loop through each file and update pdflink in path if proofType matches 
					if(data && data['file']){
						var fileObj = data['file'];
						if (fileObj.constructor.toString().indexOf("Array") == -1) {
							fileObj = [fileObj];
						}
						fileObj.forEach(function (file) {
							var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
								if(currProofType == 'print'){
									var pdfPath = file['_attributes']['path'];
									if(pdfPath){
										if(!pdfPath.match(/http(s)?:/)){
											pdfPath = 'http://'+ window.location.host + pdfPath;
										}else{
											pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
										}
										pdfLinks.push(pdfPath);
									}
								}
						
							})
						}

						else if(data && data['region']){
							var regObj = data['region']
							if(regObj){
								if (regObj.constructor.toString().indexOf("Array") == -1) {
									regObj = [regObj];
								}
								regObj.forEach(function (region) {
									if(region['file']){
										//region['_attributes']['path'] = pdfLink;
										var fileObj = region['file'];
										if (fileObj.constructor.toString().indexOf("Array") == -1) {
											fileObj = [fileObj];
										}
										fileObj.forEach(function (file) {
											var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
							
											if(currProofType== 'print'){
												var pdfPath = file['_attributes']['path'];
												if(pdfPath){
													if(!pdfPath.match(/http(s)?:/)){
														pdfPath = 'http://'+ window.location.host + pdfPath;
													}
													else{
														pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
													}
													pdfLinks.push(pdfPath);
												}	
											}
										})
										
									}
								})
							}

						}
				});
				
				var customer = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				var metaObj = $('#tocContainer').data('binder')['container-xml'].meta;
				//$('.la-container').fadeIn();
				var notifyObj = new PNotify({
					title : 'Combining PDF',
					text: 'Processing request..',
					hide: false,
					type: 'success',
					buttons: { sticker: false },
					icon: false
				});
				var notifyText = $(notifyObj.text_container[0]);

				$.ajax({
					type: "POST",
					url: "/api/mergepdf",
					data: {
						"customer": customer,
						"project" : project,
						"volume"  : metaObj.volume ? metaObj.volume._text : '', 
						"issue"   : metaObj.issue ? metaObj.issue._text : '',
						"pdfLinks": pdfLinks
					},
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.addEventListener("progress", function (evt) {
							var resText = evt.target.responseText;
							if(resText){
								resText = resText.split(/\n/).pop();
								if(!resText.match(/File_path/)){
									notifyText.html(resText);
								}else{
									notifyText.html('Completed.');
								}
							}
						}, false);
						return xhr;
					},
					success: function (data) {
						if(data){
							var filePath = data.split(/\n/).pop();
							if(filePath.match(/File_path/)){
								filePath = filePath.replace('File_path:', '');
								window.open(filePath, '_blank');
							}
						}
						//$('.la-container').fadeOut();
					},
					error: function(xhr, errorType, exception) {
						//$('.la-container').fadeOut();
						showMessage({ 'text': eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','hide':false });
					  return null;
					}
				});
			}
		}
	}
	eventHandler.general = {
		actions: {
			resetData: function(param, targetNode){
				$('#listProject, #listIssue, #listContent').remove();
				$('#issueDetails, #exportArticles, .addNewIssue, .addNewBook, #detailsContainer > div').addClass('hide')
				$('#search').val('');
				//$('#addProj #tblSetterConfig option[value],#addProj #proofConfig-journal-id option[value],#addProj #proofConfig option[value]').remove()
				$('#addProj select[data-form-details-attr="data-project-list|true"] option[value]').remove();
				$('.header .icons, .header .mergePdfIcon').addClass('hide');
				$('#tocContainer .bodyDiv .tocBodyDiv,#tocContainer .bodyDiv #flatPlanBodyDiv, #uaaDiv,#metadata,#changeHistory').html('')
			},
			/**
			 * general get method to fetch JSON data using input url
			 * @param {*} obj.url - url to fetch data from, obj.objectName - data to return from response
			*/
			getDataFromURL: function(obj){
				return new Promise(function (resolve, reject) {
					jQuery.ajax({
						type: "GET",
						url: obj.url,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (msg) {
							if (msg && msg[obj.objectName] && msg[obj.objectName].response){
								var response = msg[obj.objectName].response;
								if (response.status.code._text == 200){
									resolve({ 'code': 200, 'data': response.data });
								}else{
									showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1] , 'type': 'error', 'hide':false });
									reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
								}
							}
							else if (msg && msg[obj.objectName]) {
								resolve({ 'code': 200, 'data': msg[obj.objectName] });
							}
							else {
								//showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','hide':false });
								reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
							}
						},
						error: function (xhr, errorType, exception) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
							reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
						}
					});
				})
			},
			getCustomerList: function(){
				return new Promise(function (resolve, reject) {
					eventHandler.general.actions.resetData();
					var obj = { 'url': '/api/customers', 'objectName': 'customer' };
					eventHandler.general.actions.getDataFromURL(obj)
						.then(function (customer) {
							if ((typeof (customer) == 'undefined') || (typeof (customer.data) == 'undefined') || (customer.data.length == 0)) {
								//showMessage({ 'text': eventHandler.messageCode.errorCode[500][2] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
								reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
								return false;
							}
							var cData = customer.data;
							if (cData.constructor.name == 'Object') cData = [cData];
							var cLen = cData.length;
							if (cLen > 0) $('#filterCustomer').html('');
							for (var cIndex = 0; cIndex < cLen; cIndex++) {
								customerType = 'journal';
								if (cData[cIndex].type) {
									customerType = cData[cIndex].type;
								}
								var customerData = $('<div class="filter-list customer" data-customer="' + cData[cIndex].name + '" data-type="' + customerType + '" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'customer\'}">' + cData[cIndex].name + '</div>');
								$('#filterCustomer').append(customerData);
							}
							$('.add').removeAttr("data-target");
							if (cLen == 1){
								$('#filterCustomer').find('.filter-list.customer').addClass('active');
								$('#customerVal').text($('#filterCustomer').find('.active').text())
								$('#filterCustomer').find('.active').trigger('click');
							}
							resolve(true);
						})
						.catch(function (err) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][2] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
							reject(err)
						})
				})
			},
			getProjectList: function(cName, targetNode){
				return new Promise(function (resolve, reject) {
					eventHandler.general.actions.resetData();
					$('#cList').html('')
					var obj = { 'url': '/api/projects?customerName=' + cName, 'objectName': 'project' };
					eventHandler.general.actions.getDataFromURL(obj)
						.then(function (project) {
							if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
								//showMessage({ 'text': eventHandler.messageCode.errorCode[500][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
								reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
								return false;
							}
							var pData = project.data
							if (pData.constructor.toString().indexOf("Array") == -1) {
								pData = [pData];
							}
							
							// get assigned projects from user info and remove other projects from the total project list - JAI 07-Dec-2018
							if ($('.userinfo').length > 0 && $('.userinfo')[0].hasAttribute('data')){
								var userInfoData = JSON.parse($('.userinfo').attr('data'));
								var customerName = $(targetNode).attr('data-customer');
								if (userInfoData && userInfoData.kuser && userInfoData.kuser.kuroles && userInfoData.kuser.kuroles[customerName] && userInfoData.kuser.kuroles[customerName].project){
									var assignedProjects = userInfoData.kuser.kuroles[customerName].project;
									var newPData = [];
									var pLen = pData.length;
									for (var pIndex = 0; pIndex < pLen; pIndex++) {
										var regexPattern = new RegExp(' ' + pData[pIndex].name + ' ')
										if (regexPattern.test(assignedProjects)){
											newPData.push(pData[pIndex])
										}
									}
									pData = newPData;
								}
							}
							var pLen = pData.length;
							$('#filterProject').html('');
							$('#projectVal').html('');
							if (targetNode && targetNode.attr('data-type') == 'journal'){
								$('#project').parent().prev('.supportCard').removeClass('hide');
								$('#project').removeClass('hide');
								$('.addNewIssue').removeClass("hide");
							}else{
								$('.showEmpty').addClass('hidden');
								$('.addNewBook').removeAttr("data-target");
								$('.addNewBook').removeClass("hide");
							}
							var pHTML = $('<ul class="projectList" style="margin-left:10%;margin-right:10%">');
							for (var pIndex = 0; pIndex < pLen; pIndex++) {
								//if(pData[pIndex].type && pData[pIndex].type == 'book'){
								if (targetNode && targetNode.attr('data-type') == 'book'){
									var projectData = $('<div class="filter-list project" data-project-type="book" data-customer="' + cName + '" data-project="' + pData[pIndex].name + '" data-file="'+ pData[pIndex].name.concat('.xml') +'" data-channel="menu" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\',\'param\':{\'type\': \'book\'}}">' + pData[pIndex].fullName + '</div>');
									if (pIndex == 0){
										projectData.addClass('active');
									}
									if(pData[pIndex]['proofConfig-journal-id'] && pData[pIndex]['proofConfig-journal-id'] !=''){
										projectData.attr('data-project-id',pData[pIndex]['proofConfig-journal-id'])
									}
									$('#filterProject').append(projectData);
									var booksCard = $('<li class="project folder card" data-project-type="book" data-customer="' + cName + '" data-project="' + pData[pIndex].name + '" data-file="'+ pData[pIndex].name.concat('.xml') +'"  data-channel="menu" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\',\'param\':{\'type\': \'book\'}}" >');
									var booksCardRow = $('<div class="row">');
									
									var coverPath = '<span class="cover" style="position:relative;display: inline-block;width: 100px;height: 150px;/* background: linear-gradient(30deg, #E0F7FA, #64B5F6); */font-size: 20px;text-align: center;padding-top: 5px;font-weight: 600;color: black;border: 2px solid #d2d2d2;"><span class="cover-book-title" style="font-size: 7px;line-height: 130%;border-bottom: 2px solid #3F51B5;display: inline-block;padding-bottom: 4px;width: 95%;">' + pData[pIndex].fullName + '</span><span style="display: block;position: absolute;height: 25%;background: #f0f0f0;bottom: 15%;width: 100%;"><span style="display: block;border-bottom: 2px solid #8080804d;padding-top: 9px;width: 95%;margin-left: 2px;"></span><span style="display: block;border-bottom: 2px solid #8080804d;padding-top: 7px;width: 95%;margin-left: 2px;"></span><span style="display: block;border-bottom: 2px solid #8080804d;padding-top: 7px;width: 65%;margin-left: 2px;"></span></span><span class="cover-customer" style="position: absolute;display: block;bottom: 3%;font-size: 8px;color: #757575;right: 5px;">' + cName.toUpperCase() + '</span><span style="position: absolute;top: 35%;display: block;transform: rotate(-60deg);color: #a9a9a961;font-size: 18px;">COVER FPO</span></span>'
									//<img src="/../../images/9781119223795.gif" border="0" style="width: 100px;">
									booksCardRow.append('<div class="col-xs-2"><span class="kriya-project">' + coverPath +'</span></div>');
									//Book Title: 
									booksCardRow.append('<div class="col-xs-7"><span class="msTitle" style="display:block;font-size:20px;"><b>' + pData[pIndex].fullName + '</b></span><span class="msSubTitle" style="color:#747779; font-weight: 600;"></span><br/><span class="msAuthors" style="color:#747779"><b>Editors or Authors:</b>&nbsp;</span><br/><span class="kriya-project" style="color:#747779"><b>Kriya Project no.: </b>' + pData[pIndex].name + '</span></div>');
									booksCardRow.append('<div class="col-xs-3"><span class="chapNum" style="color:#747779"><strong>No. of chapters:</strong> </span><br/><span class="pageCount" style="color:#747779"><strong>Page Count:</strong> 0</span><br/><span class="msYear" style="color:#747779"><strong>Copyright:</strong> </span></div>');
									booksCard.append(booksCardRow);
									pHTML.append(booksCard);
									$('.add').removeAttr("data-target");
									$('.add').removeClass("hide");
									var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pData[pIndex].name + '&fileName=' + pData[pIndex].name.concat('.xml') + '&process=getIssueXML', 'objectName': 'list' };
									eventHandler.general.actions.getDataFromURL(obj)
										.then(function (issueData) {
											configData = JSON.parse(JSON.stringify(issueData.data.issue['container-xml']));
											if (configData.meta['project-name']){
												var projectName = configData.meta['project-name']._text;
											}else{
												var projectName = configData.meta['journal-id']._text;
											}
											var meta = configData.meta;
											var articleCount = 0;
											if (configData.contents.content){
												var contents = configData.contents.content;
												for (var c = 0, cl = contents.length; c < cl; c++){
													var content = contents[c];
													if (content._attributes && content._attributes.type && content._attributes.type == 'manuscript'){
														articleCount++;
													}
												}
											}
											$('.projectList li[data-project="' + projectName + '"]').find('.chapNum').append(articleCount);
											if (configData.meta['sub-title']){
												$('.projectList li[data-project="' + projectName + '"]').find('.msSubTitle').append(configData.meta['sub-title']._text);
											}
											if (configData.meta['copyright-year']){
												$('.projectList li[data-project="' + projectName + '"]').find('.msYear').append(configData.meta['copyright-year']._text);
											}
											if (configData.meta['contrib-group'] && configData.meta['contrib-group'].contrib){
												var contribs = configData.meta['contrib-group'].contrib;
												for (var c = 0, cl = contribs.length; c < cl; c++){
													var contrib = contribs[c];
													if (contrib.name && contrib.name._text){
														$('.projectList li[data-project="' + projectName + '"]').find('.msAuthors').append(contrib.name._text);
														if ((c+1) != cl){
															$('.projectList li[data-project="' + projectName + '"]').find('.msAuthors').append(', ');
														}
													}
												}
											}
										})
										.catch(function(){
											//failed to fetch data
										})
									// $('.add').attr("data-target","#addProj");
							   }else{
									var projectData = $('<div class="filter-list project" data-project-type="journal" data-customer="' + cName + '" data-project="' + pData[pIndex].name + '" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'customer\'}">' + pData[pIndex].name + '</div>');
									if (pIndex == 0){
										projectData.addClass('active');
									}
									if(pData[pIndex]['proofConfig-journal-id'] && pData[pIndex]['proofConfig-journal-id'] !=''){
										projectData.attr('data-project-id',pData[pIndex]['proofConfig-journal-id'])
									}
									$('#filterProject').append(projectData)
							   }
							}
							$('#projectVal').text($('#filterProject').find('.active').text()).attr('data-customer', cName).attr('data-project', $('#filterProject').find('.active').attr('data-project')).attr('data-project-id',$('#filterProject').find('.active').attr('data-project-id'));
							if (targetNode && targetNode.attr('data-type') == 'journal'){
								$('#filterProject').find('.active').trigger('click');
							}
							$('#cList').html(pHTML);
							resolve(true);
						})
						.catch(function (err) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
							reject(err)
						})
				})
			},
			getIssueList: function(cName, pName) {
				return new Promise(function (resolve, reject) {
					eventHandler.general.actions.resetData();
					$('#explorerNav')
						.append('<li class="customer" id="listProject" data-customer="' + cName + '" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'customer\'}">/&#x00A0;<span>' + cName + '</span>&#x00A0;</li>')
						.append('<li class="project " id="listIssue" data-customer="' + cName + '" data-project="' + pName + '" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'customer\'}">/&#x00A0;<span>' + pName + '</span>&#x00A0;</li>')
						.append('<li class="newIssue" id="newIssue"><span class="controlIcons icon-add" data-message="{\'click\':{\'funcToCall\': \'addNewIssuePopUp\',\'channel\':\'menu\',\'topic\':\'customer\'}}"></span></li>');

					$('.addNewIssue').removeClass("hide");
					var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&process=getIssuesList', 'objectName': 'list' };
					eventHandler.general.actions.getDataFromURL(obj)
						.then(function (project) {
							if ((typeof (project) == 'undefined') || (typeof (project.data) == 'undefined') || (project.data.length == 0)) {
								$('.showEmpty').removeClass('hidden');
								$('.showEmpty .watermark-text').html('Issue not uploaded');
								//showMessage({ 'text': eventHandler.messageCode.errorCode[500][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
								//reject({ 'code': 500, 'message': eventHandler.messageCode.errorCode[500][3] + obj.url });
								return false;
							}
							try{
								var pData = project.data.list;
								if (pData.constructor.toString().indexOf("Array") == -1) {
								pData = [pData];
							}
							var pLen = pData.length;
							if (pLen > 0){
								$('#issue').removeClass('hide');
								$('#issue').parent().prev('.supportCard').removeClass('hide');
								$('#filterIssue').html('')
							}
							var pHTML = '';
							for (var pIndex = 0; pIndex < pLen; pIndex++) {
								var issueName = pData[pIndex]._text;
								var fileName = issueName.replace(/^[a-z]+\_/gi, 'Volume ').replace(/\.xml$/gi, '');
								fileName = fileName.replace(/\_/gi, ' Issue ');
								pHTML += '<p draggable="true" class="list folder" data-customer="' + cName + '" data-project="' + pName + '"  data-project-type="journal"  data-file="' + issueName + '" data-channel="menu" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\'}" ><span class="cSpan">' + fileName + '</span></p>';
								var issueData = $('<div class="filter-list" data-customer="' + cName + '" data-project="' + pName + '"  data-project-type="journal"  data-file="' + issueName + '" data-channel="menu" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\'}">' + fileName + '</div>');
								$('#filterIssue').append(issueData);
							}
							//$('#cList').html(pHTML);
							resolve(true);
							}
							catch(e){
								reject('unable to parse issue list');
							}
						})
						.catch(function (err) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
							reject(err)
						})
				})
			},
			getIssueData: function(cName, pName, fileName, processType) {
				$('#newIssue').remove();
				return new Promise(function (resolve, reject) {
					$('.la-container').fadeIn();
					// hide the right panel and enable it after populating data
					$('#detailsContainer').find(">div").addClass('hide');
					var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&fileName=' + fileName + '&process=getIssueXML', 'objectName': 'list' };
					eventHandler.general.actions.getDataFromURL(obj)
						.then(function (issueData) {
							configData = JSON.parse(JSON.stringify(issueData.data.issue.config));
							eventHandler.general.actions.getProjectStages(cName, pName,fileName)
							var proofConfigDetails
							if(issueData.data.issue["proof-details"]){
								proofConfigDetails = issueData.data.issue["proof-details"]
								delete(issueData.data.issue["proof-details"]);
							}
							delete(issueData.data.issue.config);
							issueData = issueData.data.issue;
							if (typeof(issueData['container-xml']['change-history']) == 'undefined'){
								issueData['container-xml']['change-history'] = [];
							}
							else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
								issueData['container-xml']['change-history'] = [issueData['container-xml']['change-history']];
							}
							if (typeof(issueData['container-xml']['contents']['content']) == 'undefined'){
								issueData['container-xml']['contents']['content'] = [];
							}
							else if (issueData['container-xml']['contents']['content'].constructor.toString().indexOf("Array") == -1) {
								issueData['container-xml']['contents']['content'] = [issueData['container-xml']['contents']['content']];
							}
							$('#tocContainer').data('binder', issueData);
							$('#tocContainer').data('config', configData);
							$('#tocContainer').data('profConfigDetails', proofConfigDetails);
							//getProjectStages(cName, pName,fileName)
							$('.icons, .mergePdfIcon').removeClass('hide');
							$('.add').addClass("hide");
							if(processType && processType == 'book'){
								eventHandler.general.components.populateBookMeta();
							}else{
								//eventHandler.general.components.populateMetaContainer();
							}
							eventHandler.general.components.populateMetaContainer();
							eventHandler.general.components.populateHistory();
							eventHandler.general.components.populateTOC();
							eventHandler.general.components.populateIssueDetails()
							//eventHandler.general.components.populateFlatPlan();
							//eventHandler.general.components.populateContents();
							if(!processType || processType != 'book'){
								loadUnassignedArticles();
								//loadBlankOrAdvertOrFiller('');
								$('div[data-href="bookCardsTab"],#bookCardsTab').addClass('hide')
								$('div[data-href="uaaTab"],div[data-href="advertsFillersTab"],#uaaTab').removeClass('hide')
							}
							else if(processType && processType == 'book'){
								$('div[data-href="uaaTab"],div[data-href="advertsFillersTab"],#uaaTab').addClass('hide')
								$('div[data-href="metadata"],#metadata').addClass('hide')
								$('div[data-href="changeHistory"]').addClass('active')
								$('div[data-href="bookCardsTab"],#bookCardsTab,#changeHistory').removeClass('hide')
								//loadBlankOrAdvertOrFiller('book');
								//loadSectionCards();
								$('.add').removeAttr("data-target");
								$('.add').attr("data-target","#addJob");
							}
							eventHandler.general.components.populateLayouts();
							showAddChapter();
							$('.la-container').fadeOut();
							resolve(true);
						})
						.catch(function (err) {
							$('.la-container').fadeOut();
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][6] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','closer':'true', 'hide':false });
							reject(err)
						})
				})
			},
			getProjectStages: function(cName, pName,fileName){
			  return new Promise(function (resolve, reject) {
					$('.la-container').fadeIn();
					// hide the right panel and enable it after populating data
					var obj = { 'url': '/api/articles?customerName=' + cName + '&projectName=' + pName + '&bucketName=AIP&articleStatus=in-progress','objectName': 'articles'};
					eventHandler.general.actions.getDataFromURL(obj)
					   .then(function (data) {
							var pStageData = data.data
							$('#tocContainer').data('projectStages', pStageData);
							//eventHandler.general.components.populateMetaContainer();
							//eventHandler.general.components.populateHistory();
							eventHandler.general.components.populateTOC();
							//eventHandler.general.components.populateIssueDetails()
							//eventHandler.general.components.populateFlatPlan();
							//eventHandler.general.components.populateContents();
							//loadUnassignedArticles();
							//loadBlankOrAdvertOrFiller();
							eventHandler.general.components.populateLayouts();
							$('.la-container').fadeOut();
							//resolve(true);
						})
					   .catch(function (err) {
							$('.la-container').fadeOut();
							//showMessage({ 'text': eventHandler.messageCode.errorCode[500][7] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
							//reject(err)
						})
				})
			},
			getArticleDetails: function(doi,containerHTML){
				var stageData = $('#tocContainer').data('projectStages');
				if(stageData){
					console.log('here')
				if (stageData.constructor.toString().indexOf("Array") < 0) {
					stageData = [stageData];
				}
				var artTitle,linkedArticle,AcceptedDate,artType,curStage,CELevel,stageOwner,daysInProd,stageDue
				stageData.forEach(function (currObj){
					var currDoi = currObj['id'].replace(/\.xml$/, '')
					if(currDoi == doi ){
						artTitle = currObj['title']
						artType = currObj['type']
						AcceptedDate =currObj['acceptedDate']
						linkedArticle = currObj['relatedArticles']
						CELevel = currObj['CELevel']
						//curStage = currObj['workflow']?currObj['workflow']["current-stage"]:''
						curStage = eventHandler.general.actions.getStageDetails(currObj['workflow'])
					}

				})
				var cardHTML =  '<div class="col-sm-4 col-md-4 col-lg-4">'+artType+'</div>' 
					cardHTML += '<div class="col-sm-4 col-md-4 col-lg-4">'+AcceptedDate+'</div>'
					return cardHTML
				}
			   
			},
			getStageDetails: function(workflow){
				var stages = workflow["stage"]
				var owner = '';
				var curStage = '';
				if (stages.constructor.toString().indexOf("Array") < 0) {
					stages = [stages];
				}
				stages.forEach(function (stage) {
					if(stage['status'] == 'in-progress'){
						owner = stage['assigned']?stage['assigned']['to']:''
						curStage =stage['name']
					}
			  
				})
				return curStage
			},
			getArticleFig: function(doi,currUUID,colorHTML){

				var figures = $(colorHTML).find('.jrnlImage')
				var temphtml = colorHTML
				if(figures.length >0){
					for(var fig=0; fig<figures.length; fig++){
						var figID = figures[fig].attributes['data-fig-id'].value
						xpath = 'data(//fig[@data-id="'+figID+'"]/graphic[not(@data-track)or@data-track="ins"]/@xlink:href)'
				   		eventHandler.general.actions.getfigsrc(doi,figID,currUUID,colorHTML)
					}
				}
				//return temphtml
			},
			getfigsrc:function(doi,figID,currUUID,temphtml){
				var cName = $('#projectVal').attr('data-customer');
				var pName = $('#projectVal').attr('data-project');
				var xpath = 'data(//fig[@data-id="'+figID+'"]/graphic[not(@data-track)or@data-track="ins"]/@xlink:href)'
				$.ajax({
					type: 'POST',
					//async:false,
					url: '/api/getdatausingxpath',
					data: {'customer': cName,'project': pName,'doi':doi,'xpath':xpath},
					success: function (data) {
						if(data !=''&& $('#figsTab').find('.figSection[data-doi="'+doi+'"][data-art-uuid="'+currUUID+'"] img.jrnlImage[data-fig-id="'+figID+'"]').length>0){
							$('#figsTab').find('.figSection[data-doi="'+doi+'"][data-art-uuid="'+currUUID+'"] img.jrnlImage[data-fig-id="'+figID+'"]').attr('src',data)
							$('#figsTab').find('.figSection[data-doi="'+doi+'"][data-art-uuid="'+currUUID+'"] img.jrnlImage[data-fig-id="'+figID+'"]').attr('alt',data)
						}
					},
					error: function (data) {
						//return ''
					}	
				});
			},
			saveData: function(param) {
				var customer = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
			   // var fileName = $('.list.folder.active:visible').attr('data-file');
				//var fileName = $('.list.folder.active').attr('data-file');
				var fileName = $('#issueVal').attr('data-file')
				$.ajax({
					type: 'POST',
					url: '/api/postissuedata?client=' + customer + '&jrnlName=' + project + '&fileName=' + fileName,
					data: JSON.stringify($('#tocContainer').data('binder')),
					success: function (data) {
						if (param && param.onsuccess) {
							if (typeof (window[param.onsuccess]) == "function") {
								window[param.onsuccess]
							}else{
								eval(param.onsuccess)
							}
						}
						showMessage({ 'text': eventHandler.messageCode.successCode[200][3], 'type': 'success' });
					},
					error: function (data) {
						showMessage({ 'text': eventHandler.messageCode.errorCode[501][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
					},
					contentType: "application/json",
					dataType: 'json'
				});
			},
			saveNoteEditor: function(){
				var dataCID = $('#msDataDiv').attr('data-c-rid');
				// get data from the corresponding article
				var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
				var id = myData._attributes.id;
				var summerNoteData = $('.coverText')
				$(summerNoteData).each(function (){
					var currentData = this
					var keyPath = currentData['attributes']?currentData['attributes']["data-key-path"].textContent:''
					var myData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
					var id = myData._attributes.id;
					if (keyPath) {
						var keyPathArr = keyPath.split('.');
						// the last key is required to update the data, if you include the key directly then we will get only the value
						var lastKey = keyPathArr.pop();
						keyPathArr.forEach(function (key) {
							if( myData[key]){
								myData = myData[key];
								dataFlag = true;
							}
							else{
								myData[key] = {}
								myData = myData[key]
							}
							
						})
						if(myData){
							var dArr = '';
							var thisContent = $(currentData).find('.note-editable')
							$(thisContent).contents().each(function () {
								if (this.nodeType == 3) {
									var ceData = this.nodeValue;
									ceData = ceData.replace(/^[\s\t\r\n\u00A0]+|[\s\t\r\n\u00A0]+$/gi, '');
								}
								else {
									var ceData = this.outerHTML;
									ceData = ceData.replace(/[\s\t\r\n\u00A0]*<\/?(br)\/?>[\s\t\r\n\u00A0]*/gi, '');
									ceData = ceData.replace(/</g, '[[').replace(/>/g, ']]');
								}
								dArr += ceData
							});
							var orgData = ''
								orgData = myData[lastKey]?myData[lastKey]:''
								myData[lastKey] = dArr;
							
							var histObj = {
								"change": "changed <b>" + lastKey + "</b> of <i>"+ id +"</i> from <u> </u> to <u>" + dArr + "</u>",
								"file": "",
								"fileType": "",
								"id-type": ""
							};
							updateChangeHistory(histObj);
						}
				
					eventHandler.general.actions.saveData();
					$(this).attr('data-dirty', 'false');
					}
				});
			},
			saveXML: function(type, param) {
				var customer = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				if(type == 'manuscript'){
					var meta = $('#tocContainer').data('binder')['container-xml'].meta;
					var volume = meta["volume"]['_text'] ?  meta["volume"]['_text'] : parseInt(0);
					var issue = meta["issue"]['_text'] ?  meta["issue"]['_text'] : parseInt(0);
					$.ajax({
						type: 'POST',
						url: '/api/generateissuedata',
						data: {'client': customer,'jrnlName': project,'volume':volume,'issue':issue, 'issueXML': param, 'process':'saveArticleXML'},
						success: function (response) {
							showMessage({ 'text': eventHandler.messageCode.successCode[200][5], 'type': 'success' });
						},
						error: function (response) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[501][2] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
						}
					 });
					}
				else if(type == 'issueXML'){
					$('.la-container').fadeIn();
					var fileContents =  param.fileContents
					// if RO XML is uploaded
					if(fileContents){
						$.ajax({
							type: 'POST',
							url: '/api/generateissuedata',
							data: {'client': customer,'jrnlName': project, 'issueXML': param.fileContents,'draftIssue': param.draftIssue,'process':'generateContainerXml'},
							success: function (response) {
								showMessage({ 'text': eventHandler.messageCode.successCode[200][2], 'type': 'success' });
								eventHandler.general.actions.getIssueList(customer,project)
								$('.la-container').fadeOut();
							},
							error: function (response) {
								if(response.responseJSON && response.responseJSON.status &&  response.responseJSON.status.message){
									showMessage({ 'text': response.responseJSON.status.message, 'type': 'error' ,'hide':false});
								}
								else{
								showMessage({ 'text': eventHandler.messageCode.errorCode[501][6] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
								}
								$('.la-container').fadeOut();
							}
						});
					}
					//if RO XML is not uploaded. generate issue xml using volume and issue
					else{
						$.ajax({
							type: 'POST',
							url: '/api/generateissuedata',
							data: {'client': customer,'jrnlName': project, 'volume': param.volume,'issue': param.issue,'draftIssue': param.draftIssue,'process':'generateContainerXml'},
							success: function (response) {
								showMessage({ 'text': eventHandler.messageCode.successCode[200][2], 'type': 'success' });
								eventHandler.general.actions.getIssueList(customer,project)
								$('.la-container').fadeOut();
							},
							error: function (response) {
								if(response.responseJSON && response.responseJSON.status &&  response.responseJSON.status.message){
									showMessage({ 'text': response.responseJSON.status.message, 'type': 'error' ,'hide':false});
								}
								else{
								showMessage({ 'text': eventHandler.messageCode.errorCode[501][6] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
								}
								$('.la-container').fadeOut();
							}
						});
					}
				}
				else if(type == 'fetchK1Article'){
					var cName = $('#projectVal').attr('data-customer');
					var pName = $('#projectVal').attr('data-project');
			        var sectionName = param.sectionName
					var fileName = $('#issueVal').attr('data-file');
					var meta = $('#tocContainer').data('binder')['container-xml'].meta;
					var volume = meta["volume"]['_text'] ?  meta["volume"]['_text'] : parseInt(0);
					var issue = meta["issue"]['_text'] ?  meta["issue"]['_text'] : parseInt(0);
					
					return new Promise(function (resolve, reject) {
						$('.la-container').fadeIn();
						var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&fileName=' + fileName +'&volume='+ volume +'&issue='+ issue + '&msid='+ param.msid + '&insertSection='+ param.sectionName+ '&process=fetchK1Article', 'objectName': 'list' };
						eventHandler.general.actions.getDataFromURL(obj)
						.then(function (issueData) {
							newcontent = JSON.parse(JSON.stringify(issueData.data.issue.content));
							console.log(newcontent);
							//eventHandler.general.actions.insertK1Article(newcontent,param.sectionName)
							var dataType = "manuscript";
							if (newcontent._attributes && newcontent._attributes.type) {
								dataType = newcontent._attributes.type
							}
							var dataGroupType = '';
							if (newcontent._attributes && newcontent._attributes.grouptype) {
								dataGroupType = newcontent._attributes.grouptype
							}
							newcontent._attributes.section = sectionName
							var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
							
							var firstSecNode = $('#tocBodyDiv div[data-grouptype="body"][data-c-id][data-section="'+ sectionName +'"]:first');
							// get the last MS container
							var lastNode = $('#tocBodyDiv div[data-c-id]:last');
							var lastNodeID = parseInt(lastNode.attr('data-c-id'));
							var tempID = lastNodeID + 1;
							if(firstSecNode){
								var firstSecNodeID = parseInt(firstSecNode.attr('data-c-id'))
								// for blank/filler/filler-toc/advert add tempID to entry id to generate unique ID
								if (/^(blank|advert|filler|filler-toc)$/gi.test(newcontent._attributes.type)) {
									newcontent._attributes['id'] +=  tempID;
								}
								var msConObj = {
									"dataType": dataType,
									"tempID": tempID,
									"sectionIndex": firstSecNode.attr('data-sec').replace('sec',''),
									"dataGroupType":dataGroupType
								}
								var msContainer = eventHandler.general.components.getMSContainer(newcontent, msConObj);
								if ((msContainer == '') || (typeof(msContainer) === 'undefined')){
								showMessage({ 'text': eventHandler.messageCode.errorCode[502][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
									return false;
								}
								$(msContainer).insertBefore(firstSecNode);
								var startPage = parseInt(firstSecNode.data('data')._attributes.fpage)
								$('[data-c-id="' + tempID + '"]').removeClass('hide');
								//push the 'entry' data into array as if its the last node, then push it through the array to it actual position
								var seqID = tempID;
								while(lastNodeID != tempID){
									$('#tocContainer').data('binder')['container-xml'].contents.content[lastNodeID + 1] = $('#tocContainer').data('binder')['container-xml'].contents.content[lastNodeID];
									lastNode.attr('data-c-id', seqID--);
									if($(lastNode).prevAll('[data-c-id]:first').length > 0){
										lastNode = $(lastNode).prevAll('[data-c-id]:first')
									}
									else if($(lastNode).parent().prevAll().find('[data-c-id]:last').length >0){
										lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
									} 
									else{
										lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
									}
									lastNodeID = parseInt(lastNode.attr('data-c-id'));
								}
								lastNode.attr('data-c-id', seqID).data('data', newcontent);
								$('#tocContainer').data('binder')['container-xml'].contents.content[seqID] = newcontent;
								updatePageNumber(seqID, startPage,newcontent._attributes.grouptype)
								//update change history
								var histObj = {
									"change": "Inserted <b>" + newcontent._attributes.id + "</b> in section <u>" + sectionName + '</u>',
									"file": "",
									"fileType": "",
									"id-type": ""
								};
								updateChangeHistory(histObj);
								eventHandler.general.actions.saveData()
							}
							$('.la-container').fadeOut();
							resolve(true);
						})
						.catch(function (err) {
							$('.la-container').fadeOut();
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][9] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
							reject(err)
						})
					})
					
				}
			},
			insertK1Article: function(){
				var msid = $('#insertK1ArticleModal').find('input[id=insert_K1Article]').val()
				var sectionName = $('#insertK1ArticleModal').find('select[id=sectionList]').val();
				var param = {
					"msid"     : msid,
					"sectionName"    : sectionName,
				}
				eventHandler.general.actions.saveXML('fetchK1Article',param)
				$('#insertK1ArticleModal').modal('hide')
			},
			//refresh option to get latest articles xml
			getUpdatedAricle: function(fileName) {
				var customer = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				return new Promise(function (resolve, reject) {
					$('.la-container').fadeIn();
					// hide the right panel and enable it after populating data
					$('#detailsContainer').find(">div").addClass('hide');
					var obj = { 'url': '/api/reloadissuedata?client=' + customer + '&jrnlName=' + project + '&fileName=' + fileName + '&process=getUpdatedArticlesXML', 'objectName': 'list' };
					eventHandler.general.actions.getDataFromURL(obj)
						.then(function (issueData) {
							configData = JSON.parse(JSON.stringify(issueData.data.issue.config));
							console.log(configData);
							delete(issueData.data.issue.config);
							issueData = issueData.data.issue;
							if (typeof(issueData['container-xml']['change-history']) == 'undefined'){
								issueData['container-xml']['change-history'] = [];
							}
							else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
								issueData['container-xml']['change-history'] = [];
							}
							$('#tocContainer').data('binder', issueData);
							$('#tocContainer').data('config', configData);
							$('.icons').removeClass('hide');
							$('.mergePdfIcon').removeClass('hide');
							$('.la-container').fadeOut();
							resolve(true);
						})
						.catch(function (err) {
							$('.la-container').fadeOut();
							showMessage({ 'text': eventHandler.messageCode.errorCode[500][6] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
							reject(err)
						})
				})
			},
		},
		components: {
			populateMetaContainer: function() {
				$('#metaHistory').removeClass('hide');
				$('#manuscriptData').addClass('hide');
				$('#advertsData').addClass('hide');
				var meta = $('#tocContainer').data('binder')['container-xml'].meta;
				if ((typeof (meta) == 'undefined') || (typeof (binder.meta) == 'undefined')) {
					return false;
				}
				eventHandler.general.components.populateData($('#metadata'), 'meta', $('#tocContainer').data('binder')['container-xml'].meta);
				$('#detailsContainer .metadata').css('height',window.innerHeight - $('#metaHistory .bodyDiv').offset().top - $('#footerContainer').height() - 15 +'px')
				$("#detailsContainer .metadata").mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"dark"
				});
			},
			populateBookMeta: function(){
				var meta = $('#tocContainer').data('binder')['container-xml'].meta;
				if ((typeof (meta) == 'undefined') || (typeof (binder.meta) == 'undefined')) {
					return false;
				}
				$('#addProj [data-name]').val('');
				$('#addProj [data-cloned]').remove();
				for (dataName in meta){
					if (meta[dataName].constructor.toString().indexOf("Array") != -1){
						var dataValue = meta[dataName][0];
					}else{
						var dataValue = meta[dataName];
					}
					var component = $('#addProj [data-name='+dataName+']');
					if (component.length > 0){
						if (component.length > 1 && dataValue._text && dataValue._attributes){
							if (meta[dataName].constructor.toString().indexOf("Array") != -1){
								dataValue = meta[dataName]
							}else{
								dataValue = [meta[dataName]];
							}
							dataValue.forEach(function(item){
								var attrName = Object.keys(item._attributes);
								if (attrName.length == 1){
									var attrValue = item._attributes[attrName[0]];
									component = $('#addProj [data-name='+dataName+'][data-form-details-attr="' + attrName +'|' + attrValue + '"]')
								}
								if (component.length == 1 && item._text){
									component.val(item._text)
								}
							});
						}else if (component.length == 1 && meta[dataName]._text){
							component.val(meta[dataName]._text)
						}
					}else if (typeof(meta[dataName]) == 'object' && !meta[dataName]._text){
						for (childName in meta[dataName]){
							if (meta[dataName][childName].constructor.toString().indexOf("Array") != -1){
								var dataValue = meta[dataName][childName];
							}else{
								var dataValue = [meta[dataName][childName]];
							}
							component = $('#addProj [data-parent-name='+childName+']');
							if (component.length > 1 && !dataValue[0]._text && dataValue[0]._attributes){
								dataValue.forEach(function(childValue){
									var attrName = Object.keys(childValue._attributes);
									if (attrName.length == 1){
										var attrValue = childValue._attributes[attrName[0]];
										component = $('#addProj [data-parent-name='+childName+'][data-form-details-attr="' + attrName +'|' + attrValue + '"][data-template]');
									}
									if (component.length == 1 && !childValue._text){
										if (component.attr('data-template') == 'true'){
											clonedComponent = component.clone(true);
											component.parent().append(clonedComponent);
											component = clonedComponent.removeAttr('data-template').attr('data-cloned', 'true').removeClass('hide');
										}
										for (subChild in childValue){
											if (subChild != '_attributes'){
												subComponent = component.find('[data-name="' + subChild + '"]');
												if (subComponent.length == 1 && childValue[subChild]._text){
													subComponent.val(childValue[subChild]._text)
												}
											}
										}
									}
								});
							}
						}
					}
				}
				var proofConfigDetails = $('#tocContainer').data('profConfigDetails');
				if(proofConfigDetails){
					var proofConfigProperty = proofConfigDetails["property"]
					if(proofConfigProperty){
						if (proofConfigProperty.constructor.toString().indexOf("Array") < 0) {
							proofConfigProperty = [proofConfigProperty];
						}		
						proofConfigProperty.forEach(function (property) {
							var current = property._attributes['data-name']
							var component = $('#addProj [data-name='+current+'][data-proof-config-details]');
							if(component.length > 0 ){
								$(component).html('')
								var defaultval = ''
								if(meta[current]){
									defaultval = meta[current]._text
								}
								if (property['project']){
									var projects = property['project']
									if(defaultval == ''){
										$(component).append(($('<option value=""></option>')))
									}
									if (projects.constructor.toString().indexOf("Array") < 0) {
										projects = [projects];
									}
									projects.forEach(function (project) {
										var name = project._attributes.name
										var val = project._attributes.value
										var isSelected = '';
										if (defaultval == val){
											isSelected = ' selected="true"';
										}
										var element = $('<option value="'+val +'" '+ isSelected+'>'+name +'</option>')
										if($(component).attr('data-siblings-name')){
											var siblings = $(component).attr('data-siblings-name').split(',')
												siblings.forEach(function(sibling){
													if(project._attributes[sibling] && project._attributes[sibling]!=''){
														$(element).attr(sibling,project._attributes[sibling])
													}
												})
										}
										//$(component).append(($('<option value="'+val +'" '+ isSelected+'>'+name +'</option>')))
										$(component).append($(element))
									})
								}
							}
						})
					}
				}
			},
			populateData: function(container, binderKey, data) {
				// destory any previously initialized select components
				//$('#metaHistory:visible, #manuscriptData:visible, #advertsData:visible, #unassignedArticles:visible').find('select').material_select('destroy');
				//$('#metaHistory:visible, #manuscriptData:visible, #unassignedArticles:visible').find('select').material_select('destroy');
				$('#manuscriptData:visible, #advertsData:visible').find('.dropdown-button').dropdown('destroy');
				$('.summernote').summernote('destroy');
				
				var config = binder[binderKey];
				var containerHTML = '';
				var tabhtml ='';
				var tabs = configData.binder[binderKey].tab;
				if (tabs.constructor.toString().indexOf("Array") < 0) {
					tabs = [tabs];
				}
				tabs.forEach(function (currTabObj) {//for each tab in the config, loop through each path and get the corresponding component data
					containerHTML = '';
					var tabName = currTabObj._attributes["type"]; //get the tab name from config
					var paths = currTabObj.path;
					if (paths.constructor.toString().indexOf("Array") < 0) {
						paths = [paths];
					}
					paths.forEach(function (currConfigObj) {
						var compPath = currConfigObj._attributes.string;
						var keyArr = compPath.split('.');
						var myData = data;
						keyArr.forEach(function (key) {
							if (myData[key]) {
								myData = myData[key];
							}
							else if (myData.constructor.toString().indexOf("Array") > -1) {
								var mData = [];
								myData.forEach(function (mKey) {
									mData.push(mKey[key]);
								})
								myData = mData;
							}
							else {
								myData = '';
							}
						})
						
						var compObj = {
							type: currConfigObj.component._text,
							data: myData,
							label: currConfigObj.label._text,
							key: compPath
						}
						if(currConfigObj.values) compObj.values = currConfigObj.values._text;
						containerHTML += eventHandler.general.components.getComponent(compObj);
					});
					tabhtml += '<div class="'+ tabName +'">' + containerHTML +'</div>';
				});
				if (tabhtml) {
					// if the function is called to display manuscript data then do something special for the layout
					if ($(container).attr('id') == 'msDataDiv'){
						$('#pdfTab, #figsTab, #flagsTab').html('');
						tabhtml = $('<div>' + tabhtml + '</div>');
						$('#pdfTab').html(tabhtml.find('.pdfTab').html());
						var figTabHTML = tabhtml.find('.figsTab').html();
						if (figTabHTML){
							$('#figsTab').html(tabhtml.find('.figsTab').html())
							$('[data-href="figsTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
							var currDOI = $('#figsTab').find('.figSection').attr('data-doi')
							var currentUUID = $('#figsTab').find('.figSection').attr('data-art-uuid')
							if(currDOI && currentUUID){
								eventHandler.general.actions.getArticleFig(currDOI,currentUUID, tabhtml)
							}
						}
						else{
							$('[data-href="figsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
						}
						$('#flagsTab').html(tabhtml.find('.flagsTab').html());
					}
					else if ($(container).attr('id') == 'adDataDiv'){
						$('#regionTab').html('');
						$('#adDataDiv #psflagsTab').html('');
						tabhtml = $('<div>' + tabhtml + '</div>');
						var regTabHTML = tabhtml.find('.regionTab').html();
						if (regTabHTML){
							$('#regionTab').html(tabhtml.find('.regionTab').html())
							var flagtab = tabhtml.find('.flagsTab').html();
							if(flagtab){
								$('#adDataDiv #psflagsTab').html(tabhtml.find('.flagsTab').html());
								$('[data-href="psflagsTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
							}
							else{
								$('[data-href="psflagsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
							}
							$('[data-href="regionTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
							$('[data-href="flagsTab"]').removeClass('hide').parent().find('.tabHead').addClass('tabHead3');
						}
						else{
							$('[data-href="regionTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
							$('[data-href="psflagsTab"]').addClass('hide').parent().find('.tabHead').removeClass('tabHead3');
						}
						
					}
					else{
					$(container).html(tabhtml);
					}
				}
				$('#manuscriptData:visible, #advertsData:visible').find('.dropdown-button').dropdown();
				$('.summernote').summernote({
					toolbar: [
						// [groupName, [list of button]]
						['style',['style']],
						['style', ['bold', 'italic', 'underline', 'clear','header1','heading2','h3']],
						['fontsize', ['fontsize']],
						['color', ['color']],
						['para', ['ul', 'ol', 'paragraph']]
					  ]
				});
			},
			getComponent: function(compObj) {
				var componentHTML ='';
				if($('#compDivContent').find('[data-component-type="'+ compObj.type +'"]').length > 0){
					componentHTML = $('#compDivContent').find('[data-component-type="'+ compObj.type +'"]')
				}
				else if($('#compDivContent').find('[data-component-type="default"]').length >0){
					componentHTML = $('#compDivContent').find('[data-component-type="default"]')
				}
				if(componentHTML.length > 0){
					var flagsConfig = JSON.parse(JSON.stringify(configData.flags)).flag;
						if (flagsConfig.constructor.toString().indexOf("Array") < 0){
							flagsConfig = [flagsConfig];
						}
					var regionsConfig = JSON.parse(JSON.stringify(configData.regions)).region;
					if (regionsConfig.constructor.toString().indexOf("Array") < 0){
						regionsConfig = [regionsConfig];
					}	
					var dataCID = $('#msDataDiv').attr('data-c-rid');
					var siteName ='';
					var myData = [].concat(compObj.data);
					var component = $('<div></div>')
					var componentHTML = $(component).append(componentHTML.html())
					if($(component).find('[data-update-pdfPath]').length >0){
						var printSrc;
						var onlineSrc;
						var preflightSrc;
						var proofType;
						myData.forEach(function (mdObj, mdIndex) {
							proofType = mdObj['proofType'] ? mdObj['proofType'] : 'print';
							if(proofType == 'print' && mdObj['type'] != 'preflightPdf'){
								printSrc = mdObj['path'] ? mdObj['path'].concat('#view=fit') : '';
								if(printSrc != ''){
									$(componentHTML).find('li[data-prooftype="print"] a').attr('target','_blank')
									$(componentHTML).find('li[data-prooftype="print"] a').attr('href',printSrc)
									$(componentHTML).find('object').attr('data',printSrc)
								}
								else{
									printSrc="../images/NoImageFound.jpg"
									$(componentHTML).find('object').attr('data',printSrc)
								}
							}
							else if(proofType == 'online'){
								onlineSrc = mdObj['path'] ? mdObj['path'] : '';
								if(onlineSrc != ''){
									$(componentHTML).find('li[data-prooftype="online"] a').attr('target','_blank')
									$(componentHTML).find('li[data-prooftype="online"] a').attr('href',onlineSrc)
								}
							}
							else if(mdObj['type'] == 'preflightPdf'){
								preflightSrc =  mdObj['path'] ? mdObj['path'] : ' ';
								if(preflightSrc != ''){ //type=""preflightPdf
									$(componentHTML).find('li[data-preflight="true"] a').attr('target','_blank')
									$(componentHTML).find('li[data-preflight="true"] a').attr('href',preflightSrc)
								}
							}
							proofType = '';
						})
						var currMsdata = $(targetNode).data('data');
						if(compObj.type == 'mspdf'){
							if((currMsdata._attributes)&&(currMsdata._attributes.siteName)){
								siteName = currMsdata._attributes.siteName;
							}
							var runOnFlag = false;
							if((currMsdata._attributes)&&(currMsdata._attributes['data-run-on'])){
								runOnFlag = currMsdata._attributes['data-run-on'];
							}
							if (/^(non\-kriya)$/gi.test(siteName)) {
								var temp = $(componentHTML).find('div[data-upload-type="non-kriya"]')
								componentHTML = $('<div></div>').append($(componentHTML).find('div[id="upload_non_kriya"]'))	
							}
							//else if bmj-abstract enable export option for all articles
							else if($('#listProject').attr('data-customer') && $('#listProject').attr('data-customer')=='bmj_abstracts'){
								$(componentHTML).find('div[id="upload_non_kriya"]').remove()
							}
							// if run-on disable export option
							else if(runOnFlag == 'true'){
								$(componentHTML).find('[id="pdfDownload"],[id="exportPDF"],[id="exportOnlinePDF"],[id="download_pdf"]').remove()
							}
							else{
								$(componentHTML).find('div[id="upload_non_kriya"]').remove()
							}
						}	
					}
					else if($(component).find('[data-html-node]').length >0){
						var containerHTML = '';
						var mdIndex = 0;
						$(componentHTML).find('.summernote').html('')
						myData.forEach(function (mdObj, mdIndex) {
							var contentHTML = mdObj['_text'] ? mdObj['_text'].replace(/\[\[/g, '<').replace(/\]\]/g, '>'):'';
							if($(componentHTML).find('.summernote').length>0 & contentHTML !='' ){
								containerHTML += contentHTML
							}
							else if(contentHTML != ''){
								containerHTML += '<p>' + contentHTML + '</p>';
							}
						});
						if($(componentHTML).find('[data-contenteditable="true"]').length >0){
							$(componentHTML).find('[data-contenteditable="true"]').append(containerHTML)
						}
						else if($(componentHTML).find('.summernote').length>0){
							$(componentHTML).find('.summernote').append(containerHTML)
						}						
					}
					else if($(componentHTML).find('[checked="{checked}"]').length >0){	
						var value =  compObj.data ? compObj.data : compObj['defaultValue']
						if(value != '' && value !='None' && value !=0 ){
							$(componentHTML).html($(componentHTML).html().replace(/{checked}/g, 'checked'))
						}
						else{
							if ((value=='None'||value=='') && $(componentHTML).find('select[data-checkbox="true"]').length >0) {
								$(componentHTML).find('select[data-checkbox="true"]').addClass('hide')
							}
							$(componentHTML).find('[checked="{checked}"]').removeAttr('checked')
						}
						var currMsdata = $(targetNode).data('data');
						if(currMsdata && (currMsdata._attributes)&&(/advert|filler/gi.test(currMsdata._attributes['type'])) && (/color/gi.test(compObj.label))){
							$(componentHTML).find('input[type="checkbox"]').attr('data-layout-sec',compObj.layoutSec)
							$(componentHTML).find('input[type="checkbox"]').attr('data-key-path','data-color')
							$(componentHTML).find('input[type="checkbox"]').removeClass('flag').addClass('color')
						}
					}
					if($(componentHTML).find('select[data-update-option="true"]').length >0){
						var selectHTML = ''
						var defaultValue = compObj['defaultValue']
						var flagValues = compObj['values']
						flagValues.forEach(function(selectValue, sIndex){
							var isSelected = '';
							if (defaultValue == selectValue){
								isSelected = ' selected="true"';
							}
							selectHTML += '<option value="' + selectValue + '"' + isSelected +'>' + selectValue + '</option>'
						})
						if(compObj['defaultValue'] !='None' && $(componentHTML).find('input[type="checkbox"][data-select="true"]').length >0){
							$(componentHTML).html($(componentHTML).html().replace(/{checked}/g, 'checked'))
						}
						
						$(componentHTML).find('select').append(selectHTML)
					}
					else if(compObj.type == 'fileUpload' && compObj.regionIDVal && compObj.regionIDVal !=''){
						$(componentHTML).find('div[data-update-node="label"]').remove()
						$(componentHTML).find('input[type="file"]').removeAttr('data-key-path')
						$(componentHTML).find('input[type="file"]').attr('data-layout-sec',compObj['key'])
							
					}
					else if($(componentHTML).find('[data-foreach-fig="true"]').length >0){
						var currMsdata = $(targetNode).data('data');
						var siteName=''
						if(compObj.type == 'mspdf'){
							if((currMsdata._attributes)&&(currMsdata._attributes.siteName)){
								siteName = currMsdata._attributes.siteName;
							}
						}	
						var doi = ''
						var currUUID
						if((currMsdata._attributes)&&(currMsdata._attributes.siteName)){
							doi = currMsdata._attributes.id
						}
						if((currMsdata._attributes)&&(currMsdata._attributes['data-uuid'])){
							currUUID = currMsdata._attributes['data-uuid']
						}
						//componentHTML = $(componentHTML).html()
						var colorHTML = $('<div class="figSection" data-doi="'+ doi+'" data-art-uuid="'+currUUID +'"></div>')
						var colorComponentHTML = componentHTML.html();
						if(myData.length > 0 && myData[0] !=''){
							myData.forEach(function (mdObj, mdIndex) {
							var colorChecked = parseInt(mdObj['color']) ? 'checked' : '';
							//var src = mdObj['path'] ? mdObj['path'].replace(/\.(eps|tiff?)/gi, '.jpg') : '';
							var src = mdObj['path'] ? mdObj['path'] : '';
						    if(siteName == 'kriya1.0' && src !=''){
								//src = src.replace(/\.(eps|tiff?)/gi, '.jpg')
							  }
							var label = mdObj['label'] ? mdObj['label'] : mdObj['ref-id']
								$(colorComponentHTML).clone().appendTo($(colorHTML))
								var updateNode = $(colorHTML).find('[data-update-node]')
								if(updateNode.length > 0){
									updateNodeVal(updateNode,mdObj)
								}
								var value = parseInt(mdObj['color'])
								if(value == 1){
									$(colorHTML).html($(colorHTML).html().replace(/{colorChecked}/g, 'checked'))
								}
								else{
									$(colorHTML).find('[checked="{colorChecked}"]').removeAttr('checked')
								}
								$(colorHTML).html($(colorHTML).html().replace(/{mdObj\[\'ref-id\'\]}/g, mdObj['ref-id']))
								$(colorHTML).html($(colorHTML).html().replace(/{label}/g, label))
								$(colorHTML).html($(colorHTML).html().replace(/{mdIndex}/g, mdIndex))
								$(colorHTML).html($(colorHTML).html().replace(/{src}/g, src))
								var refId = mdObj['ref-id'] ? mdObj['ref-id'] : ''
								/*$(colorHTML).find('img:last').attr('data-fig-id',refId)
						$(colorHTML).find('img:last').attr('src',src)*/
						$(colorHTML).find('.jrnlImage:last').attr('data-fig-id',refId)
						$(colorHTML).find('.jrnlImage:last').attr('src',src)
						$(colorHTML).find('.jrnlImage:last').attr('onerror',"imgError(this)")
							})
							componentHTML = $('<div></div>').append($(colorHTML))
						}	
						else {
							componentHTML = ''
						}
					}
					else if($(componentHTML).find('[data-flag-config="true"]').length >0){	
						var flagComponentHTML = componentHTML.html();
						componentHTML = $('<div></div>');
						var prevFlagSection = ''
						var currData = $('#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
						var contentType = currData._attributes['type'];
						var currSection = currData._attributes['section'];
						flagsConfig.forEach(function(fcObj, fcIndex){
							if(fcObj._attributes['content-type'] == contentType){
								$(flagComponentHTML).clone().appendTo($(componentHTML))
								var flagSection = fcObj._attributes['data-section-group']
								myData.forEach(function(mdObj, mdIndex){
									if (mdObj.type == fcObj._attributes.type){
										fcObj._attributes['default-value'] = mdObj.value;
									}
								});
								if(flagSection != prevFlagSection){
									$(componentHTML).find('.flagSectionHead:last').text(flagSection)
								}
								else{
									$(componentHTML).find('.flagSectionHead:last').remove()		
								}
								var flagHTML = ''
								var fgcomponent = {
									label: fcObj._attributes['label'],
									data: fcObj._attributes,
									type: fcObj._attributes['component-type'],
									flagType:fcObj._attributes['type'],
									defaultValue: fcObj._attributes['default-value'],
									key: key
								}
								if(fcObj._attributes['component-type'] == 'checkbox'){
									var flagChecked = parseInt(fcObj._attributes['default-value']) ? 'checked' : '';
									fgcomponent['checked'] = flagChecked	
									fgcomponent['data'] = fcObj._attributes['value']
								}
								if(fcObj._attributes['component-type'] == 'select'){
									fgcomponent['data'] = fcObj._attributes;
									fgcomponent['values'] = fcObj._attributes['values'];
									if(/section\s+head/gi.test(fcObj._attributes['label'])){
										var flagValues = eventHandler.general.components.getAvailableSections()
										fgcomponent['defaultValue'] = currSection
										fgcomponent['values'] = flagValues
									}
									else{
										var flagValues = fcObj._attributes.values.split(',');
										fgcomponent['values'] = flagValues
									}
								}
								else if(fcObj._attributes['component-type'] == 'select_checkbox'){
									var flagChecked = ''
									if(fcObj._attributes['default-value'] && fcObj._attributes['default-value'] !='0' && fcObj._attributes['default-value'] !='' && fcObj._attributes['default-value']!='None'){
										flagChecked = 'checked'
									}
									fgcomponent['values'] = flagValues
									var flagValues = fcObj._attributes.values.split(',');
									fgcomponent['values'] = flagValues
									fgcomponent['checked'] = flagChecked
									fgcomponent['data'] = fcObj._attributes['value']
								}
								flagHTML = eventHandler.general.components.getComponent(fgcomponent)
								if(flagHTML.length > 0){
									$(flagHTML).html($(flagHTML).html().replace(/{fcObj._attributes.type}/g, fcObj._attributes.type))
									$(flagHTML).html($(flagHTML).html().replace(/{fg{fcIndex}}/g, 'fg'+fcIndex))
								}
								var updateNode = $(flagHTML).find('[data-update-node]')
								if(updateNode.length > 0){
									updateNodeVal(updateNode,fcObj)				
								}
								$(componentHTML).append(flagHTML)
							}
							prevFlagSection = fcObj._attributes['data-section-group']
						})
					} 
					else if($(componentHTML).find('[data-region-config="true"]').length >0){
						var myData = [].concat(compObj.data);
						$(componentHTML).find('.stripes,.layoutCompHTML,.layoutBlock').remove()
						var regionHTML = '';
						var layouts = '';
						var regionFlagVal ='false';
						regionsConfig.forEach(function(rcObj, rcIndex){
							regionFlagVal = 'false';
							var files ='';
							var curLayoutVal = '';
							if(rcObj.component){
								var componentConfig = rcObj.component;
								if (componentConfig.constructor.toString().indexOf("Array") < 0){
									componentConfig = [componentConfig];
								}
							}
							if (rcObj._attributes['content-type'] == $(targetNode).attr('data-type')){
								if($('#compDivContent').find('[data-component-type="'+ compObj.type +'"]').length > 0){
									regionHTML = $('#compDivContent').find('[data-component-type="'+ compObj.type +'"]').clone()
								}
								else if($('#compDivContent').find('[data-component-type="default"]').length >0){
									regionHTML = $('#compDivContent').find('[data-component-type="default"]').clone()
								}
								var lcbj = rcObj.layout;
								var regionId = rcObj._attributes.label;
								var pdfComponent = {
									type: 'advertOrFillerPdf',
									data: '',
									regionIDVal: rcObj._attributes['type'],
									key: ''
								}   
								if(regionHTML.length >0){
									myData.forEach(function(mdObj, mdIndex){
										if(mdObj !=''){
											if (mdObj._attributes.type == rcObj._attributes.type){ 
												rcObj._attributes['default-value'] = mdObj._attributes.type;
												curLayoutVal = mdObj._attributes.layout; // if region info is available in issueXML, get layout value
												regionFlagVal ='true';
												regionId = rcObj._attributes.label;
												pdfComponent = {
													type: 'advertOrFillerPdf',
												    data: mdObj['file'],
													regionIDVal: mdObj._attributes['type'],
													key: mdObj._attributes['path']
												}  
												pdfComponent['data'] = getDataFromKey('file._attributes',mdObj)
												$(regionHTML).find('.stripes:last').append(eventHandler.general.components.getComponent(pdfComponent));
												if(componentConfig){
													var container = $('<p style="margin:0"></p>')
													componentConfig.forEach(function(ccbj, ccIndex){
														var key =ccbj._attributes['string']
														var afcomponent = {
															label: ccbj._attributes['label'],
															data: mdObj._attributes[''+ key +''],
															type: ccbj._attributes['component-type'],
															flagType : ccbj._attributes['type'],
															values: ccbj._attributes['values'],
															regionIDVal: mdObj._attributes['type'],
															defaultValue: ccbj._attributes['default-value'],
															key: key
														}
														var compHTML = eventHandler.general.components.getComponent(afcomponent)
														if($(compHTML).length > 0 && $(compHTML).hasClass('stripes')){
															compHTML = $(compHTML).html() 
														}
														$(container).append(compHTML)
														
													})  
													$(regionHTML).find('.stripes:last').append(container);
												}  
												files = mdObj.file;
											}
										}
									});
									
									if(regionFlagVal == 'false'){
										$(regionHTML).find('.stripes:last').append(eventHandler.general.components.getComponent(pdfComponent));
										if(componentConfig){
											var container = $('<p style="margin:0"></p>')
											componentConfig.forEach(function(ccbj, ccIndex){
												var key =ccbj._attributes['string']
												var afcomponent = {
													label: ccbj._attributes['label'],
													data: rcObj._attributes[''+ key +''],
													type: ccbj._attributes['component-type'],
													flagType : ccbj._attributes['type'],
													values: ccbj._attributes['values'],
													regionIDVal: rcObj._attributes['type'],
													defaultValue: ccbj._attributes['default-value'],
													key: key
												} 
												var compHTML = eventHandler.general.components.getComponent(afcomponent)
												if($(compHTML).length > 0 && $(compHTML).hasClass('stripes')){
													compHTML = $(compHTML).html() 
												}
												$(container).append(compHTML)
											})  
											$(regionHTML).find('.stripes:last').append(container);
										}
										files = ({"_attributes":{"type": "file","data-region":rcObj._attributes.type, "data-layout":"a" ,"path":""}})
									}
									var layoutComponentObj=''
									var layoutCompHTML='';
									if(lcbj){
										layouts = lcbj._attributes.values; // if region info is available in config, get layout value
										layouts = layouts.toLowerCase();	
										layouts = layouts.split(',');
										if(curLayoutVal == '') { //if current layout value is not available in xml, then set the default layout value as the current value
											curLayoutVal = lcbj._attributes['default-value'];
										}
										layoutComponentObj = {
											type: 'select',
											data: lcbj._attributes.values,
											regionIDVal: rcObj._attributes['type'],
											label:lcbj._attributes.label,
											values : layouts,
											key: '',
											defaultValue : curLayoutVal
										}  
										var layoutComponent = eventHandler.general.components.getComponent(layoutComponentObj)
										if(layoutComponent.length >0){
											$(regionHTML).find('.stripes:last').append($(layoutComponent).html())
											$(regionHTML).find('.stripes:last select').append(layoutCompHTML)
											$(regionHTML).find('.stripes:last select').attr('class','layoutOption')
											$(regionHTML).find('.stripes:last select').attr('data-region-id',rcObj._attributes.label)
											$(regionHTML).find('.stripes:last select').attr('data-type',curLayoutVal)
										}
									}
									curLayoutVal = curLayoutVal.toLowerCase();
									curLayoutVal = curLayoutVal.replace(/\s/,'');
									var layoutHTML = $('#'+curLayoutVal+'').html(); // get the layout html based on the layout specified in issueXML
									regionHTML.find('.layoutBlock').append(eventHandler.general.components.populateAdvert(rcObj._attributes.type,$('#'+curLayoutVal+'').find('.layoutsection'),layoutHTML,files)); // populate img data
									regionHTML.find('.layoutBlock').attr('data-layout',layoutComponentObj['defaultValue'])		
									$(regionHTML).find('.stripes:last').find('.dropdown .dropdown-toggle').attr('id',"dropdown_download_"+regionId)
									$(regionHTML).find('.stripes:last').find('ul').attr('aria-labelledby',"dropdown_download_"+regionId)
									$(regionHTML).html($(regionHTML).html().replace(/{_attributes.label}/g, rcObj._attributes.label))
									var updateNode = $(regionHTML).find('[data-update-node]')
									if(updateNode.length > 0){
										updateNodeVal(updateNode,rcObj)
									}
									$(componentHTML).append(regionHTML.html())
								}
							}
							
						})
					} 
					var updateNode = $(componentHTML).find('[data-update-node]')
					if(updateNode.length > 0){
						updateNodeVal(updateNode,compObj)
					}
					if(componentHTML.length > 0){
						componentHTML = $(componentHTML).html()
						var label = compObj['label']
						var data = compObj.data
						if(compObj.data && compObj.data['_text']){
							data = compObj.data['_text']
						} 
						if(JSON.stringify(data) == '{}'){ data=''}
						var key = compObj.key
						var flagType= compObj['flagType']?  compObj['flagType'] :''
						var defaultVal= compObj['defaultValue']?  compObj['defaultValue'] :'' 
						componentHTML = componentHTML.replace(/{label}/g, label)
						componentHTML = componentHTML.replace(/{data}/g, data)
						componentHTML = componentHTML.replace(/{key}/g, key)
						componentHTML = componentHTML.replace(/{flagType}/g, flagType)
						componentHTML = componentHTML.replace(/{defaultValue}/g, defaultVal)
						componentHTML = componentHTML.replace(/{regionIDVal}/g, compObj['regionIDVal'])
					}
					return componentHTML
				}
			},
			populateAdvert: function(regionId,layoutsections,layoutHTML,files) {
			//loop through each layout section , get the file path from issueXML and update the path in the layout sec
				var layoutLen = layoutsections.length;
				var regionHTML = $('<div>' + layoutHTML + '</div>');
				if (files.constructor.toString().indexOf("Array") < 0){
					files = [files];
				}
				var advertImg ='';
				var layoutFlag =false;
				for(var i=0;i<layoutLen;i++){
					layoutFlag = false;
					files.forEach(function(fileObj, acIndex){
						if( layoutsections[i].attributes['layout-sec'].value == fileObj._attributes['data-layout']){
							layoutFlag = true;
							var layoutsec = fileObj._attributes['data-layout'];
							var colorcomponent = {
								label: 'Color in Print',
								data: fileObj._attributes['data-color'],
								type: 'checkbox',
								flagType : 'color',
								regionIDVal: fileObj._attributes['data-region'],
								defaultValue: '0',
								key: 'data-color',
								layoutSec : fileObj._attributes['data-layout']
							}
							var imgcolor = eventHandler.general.components.getComponent(colorcomponent);
							if($(imgcolor).length > 0 && $(imgcolor).hasClass('stripes')){
								imgcolor = $(imgcolor).html() 
							}
							var compObj = {
								type: 'fileUpload',
								data: fileObj._attributes['path'],
								label: 'layout',
								regionIDVal: fileObj._attributes['data-region'],
								key: fileObj._attributes['data-layout']
							}
							var fileUploads = eventHandler.general.components.getComponent(compObj);
							fileUploads = $('<p></p>').append(fileUploads)
							if($(imgcolor).length > 0 && $(fileUploads).length >0 && $(fileUploads).find('span').length >0){
								imgcolor = imgcolor
							}
							$(fileUploads).find('.fileObjUpload').append(imgcolor)
							var advertImg = '<img src="' + fileObj._attributes['path'] + '" alt="fig' + layoutsec + '"/>';
							regionHTML.find('[layout-sec="'+ layoutsec +'"]').html(regionHTML.find('[layout-sec="'+ layoutsec +'"]').html()  + $(fileUploads).html())
						}
					});
					//if file path is not available in xml,
					if(layoutFlag == false){
						var colorcomponent = {
							label: 'Color in Print',
							data: '',
							type: 'checkbox',
							flagType : 'color',
							regionIDVal: regionId,
							defaultValue: '0',
							key: 'data-color',
							layoutSec : layoutsections[i].attributes['layout-sec'].value
						}
						var imgcolor = eventHandler.general.components.getComponent(colorcomponent);
						if($(imgcolor).length > 0 && $(imgcolor).hasClass('stripes')){
							imgcolor = $(imgcolor).html() 
						}
						var layoutsec = layoutsections[i].attributes['layout-sec'].value;
						var compObj = {
							type: 'fileUpload',
							data: '',
							label: 'layout',
							regionIDVal: regionId,
							key: layoutsections[i].attributes['layout-sec'].value
							}
						var fileUploads = eventHandler.general.components.getComponent(compObj);
						fileUploads = $('<p></p>').append(fileUploads)
							
						$(fileUploads).find('.fileObjUpload').append(imgcolor)
						regionHTML.find('[layout-sec="'+ layoutsec +'"]').html(regionHTML.find('[layout-sec="'+ layoutsec +'"]').html() + $(fileUploads).html())
					}
				}
				if (regionHTML) {
					return regionHTML.html();
				}
			},
			//pull advert and filler layout from the config and append it at the end of #tocContainer
			//config of advert/filler layout is based on the customer    
			populateLayouts: function() {
				var cName = $('#projectVal').attr('data-customer');
				var pName = $('#projectVal').attr('data-project');
				var obj = {
					'url': '/api/getlayouthtml?client=' + cName + '&jrnlName=' + pName + '&process=getLayouts',
					'objectName': 'list'
				};
				eventHandler.general.actions.getDataFromURL(obj)
					.then(function (templateData) {
						if(templateData !=''){  
							$('#dataContainer').append(templateData.data)
						}
					})
					.catch(function (err) {
						showMessage({ 'text': eventHandler.messageCode.errorCode[500][8] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
					})
			},
			populateTOC: function() {
				$('#listContent').remove();
				$('#flatPlanBodyDiv').addClass('hide');
				$('#flatPlanBodyDiv').removeClass('active');
				$('#tocBodyDiv').addClass('active').removeClass('hide');
				$('[data-href="flatPlanBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$('[data-href="tocBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				
				var cName = $('#projectVal').attr('data-customer');
				var pName = $('#projectVal').attr('data-project');
				var fileName = $('#issueVal').attr('data-file');
				if(!(fileName)){
					fileName = $('.list.folder.active').attr('data-file');
				}
				$('<li class="customer" id="listContent" data-customer="' + cName + '" data-project="' + pName + '"  data-fileName="' + fileName.replace(/\.xml$/,'') + '" data-file="' + fileName + '"  data-channel="menu" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\'}">/&#x00A0;<span>' + fileName.replace(/\.xml$/,'') + '</span>&#x00A0;</li>').insertAfter('#listIssue')
				var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
				var prevSection = '';
				var tocContainer = $('#tocContainer .bodyDiv #tocBodyDiv');
					tocContainer.html('<div class="tocBodyContainer"></div>');
				var container = $('#tocContainer .bodyDiv #tocBodyDiv .tocBodyContainer');
				var containerHTML = '';
				container.html('<div class="row sectionHeader"><div class="col-xs-8">Section Name<span class="controlIcons addContent material-icons">add_circle</span></div><div class="col-xs-3">Page Range</div><div class="tocIcons col-xs-1"><span class="icon-file-excel" data-channel="menu" data-topic="data" data-event="click" data-message="{\'funcToCall\': \'exportExcel\'}" title="Export Excel"></span><span class="icon-book" data-channel="pdf" data-topic="merge" data-event="click" data-message="{\'funcToCall\': \'mergePDF\'}" title="Combine PDF"/><span class="icons icon-fullscreen" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'tocSection\'}" title="Toggle Section"/></div></div>');
				var colStart = '<div class="col-sm-6 col-md-6 col-lg-6">';
				var closeDiv = '</div>';
				var sectionIndex = 1;
				var tempID = -1;
				contentHash = {}
				var sectionChild = false;
				if(content){
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var type = entry._attributes.type;
						var child =false;
						contentHash[++tempID] = content[tempID];
						if (/^(cover|editorial\-board|table\-of\-contents)$/gi.test(entry._attributes.type)) {
							eventHandler.general.components.addSection(container, entry, 'noChild', sectionIndex++, tempID);
						}
						else {
							if (prevSection != entry._attributes.section) {
								eventHandler.general.components.addSection(container, entry, 'hasChild', sectionIndex++);
								sectionChild = true
							}
							// add child true when prevSection is equal to entry._attributes.section
						else {
								child = true;
							}
							prevSection = entry._attributes.section;
							var dataType = "manuscript";
							if (entry._attributes && entry._attributes.type) {
								dataType = entry._attributes.type
							}
							var dataGroupType = ''
							if (entry._attributes && entry._attributes.grouptype) {
								dataGroupType = entry._attributes.grouptype
							}
							var msConObj = {
								"dataType": dataType,
								"tempID": tempID,
								"sectionIndex": sectionIndex,
								"dataGroupType":dataGroupType,
								"child":child
							}
							var childData = eventHandler.general.components.getMSContainer(entry, msConObj) 
							$(container).find('.card[id="section'+ (sectionIndex-1) +'"]').append($(childData))
						//container.append(eventHandler.general.components.getMSContainer(entry, msConObj));
							if (tempID == 0 || tempID) {
								$('[data-c-id="' + tempID + '"]').data('data', entry);
							}
						}
					});
				}
				if(sectionChild === true){
					$('#tocContainer .bodyDiv .sectionHeader .addContent').addClass('hide');
					
				}
				$('#tocContainer #tocBodyDiv').css('height', (window.innerHeight - $('#tocContainer .bodyDiv').offset().top  - $('#footerContainer').height() - 10) + 'px');
				$('#tocBodyDiv .tocBodyContainer').css('height',  $('#tocBodyDiv').height());
				$('#tocBodyDiv .tocBodyContainer').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"dark"
				});
			},
			populateIssueDetails: function(){
				var meta = $('#tocContainer').data('binder')['container-xml'].meta;
				$('#issueDetails').html('');
				$('.bookInfo,.issueInfo').remove();
				if ($('#filterProject .active').attr('data-project-type') == 'book'){
					var infoBtn = $('<span class="btn btn-sm btn-primary bookInfo" data-channel="menu" data-topic="bookview" data-event="click" data-message="{\'funcToCall\': \'editBookMeta\'}"><i class="fa fa-edit"/> Book Info</span>');
					infoBtn.insertBefore($('#issueDetails'))
				}else{
					//var infoBtn = $('<span class="btn btn-sm btn-primary bookInfo" data-channel="menu" data-topic="bookview" data-event="click" data-message="{\'funcToCall\': \'editIssueMeta\'}"><i class="fa fa-edit"/> Issue Info</span>');
				}
				if(meta.volume && meta.issue){
					$('#issueDetails').append('<span class="fileName"><span class="label">Volume: </span>'+ $('#tocContainer').data('binder')['container-xml'].meta.volume._text+' <span class="label">Issue: </span>'+ $('#tocContainer').data('binder')['container-xml'].meta.issue._text+'</span>')
				}
				if($('#filterProject .active').attr('data-project-type') == 'book'){
					$('#issueDetails').append('<span class="btn articleCount"><span class="label">No of Chapters: </span>'+ $('#tocBodyDiv div[data-type="manuscript"]').length+'</span>')
				}
				else{
				   $('#issueDetails').append('<span class="btn articleCount"><span class="label">No of Articles: </span>'+ $('#tocBodyDiv div[data-type="manuscript"]').length+'</span>')
				}
				var exportFlag = $('#tocContainer').data('binder')['container-xml'].contents._attributes ? $('#tocContainer').data('binder')['container-xml'].contents._attributes.exportAllArticles : false
							
				$('#issueDetails, #exportArticles').removeClass('hide');
				var flagConfig =  JSON.parse(JSON.stringify(configData.flags)).flag;
				var issueFlag = {};
				if (flagConfig.constructor.toString().indexOf("Array") < 0){
					flagConfig = [flagConfig];
				}
				var signature =''
				flagConfig.forEach(function(fcObj, fcIndex){
					if(fcObj._attributes['content-type'] == 'issue'){
					  //  issueFlag.push(fcObj)
						var type= fcObj._attributes['type']
						var myData = meta[type]?meta[type]._text:''
						var compObj = {
							type: fcObj._attributes['component-type'],
							data: myData,
							label: fcObj._attributes.label,
							key: fcObj._attributes.string,
							values: fcObj._attributes.values?fcObj._attributes.values.split(','):'',
							defaultValue: fcObj._attributes['default-value']
						}
						signature += eventHandler.general.components.getComponent(compObj)
					}
				}); 
				$('#issueDetails').append(signature)
			},
			getMSContainer: function(entry, msConObj){
				var cName = $('#projectVal').attr('data-customer');
				var pName = $('#projectVal').attr('data-project');
				var containerHTML = $('#sectionChildCard').clone()
				var className = "row sectionDataChild hide";
				var modifiedAttr = (entry._attributes['data-modified'])? $(containerHTML).find('.sectionDataChild').attr('data-modified',entry._attributes['data-modified']):'';
				var runOnAttr = (entry._attributes['data-run-on'])?$(containerHTML).find('.sectionDataChild').attr('data-run-on',entry._attributes['data-run-on']):'';
				var runOnWithAttr = entry._attributes['data-runon-with'] ?$(containerHTML).find('.sectionDataChild').attr('data-runon-with',entry._attributes['data-runon-with']):'';
				
				var tempIDVal = entry._attributes.id ? entry._attributes.id : '';
				var sectionChild = msConObj.child ? msConObj.child : false;
				var siteName = entry._attributes.siteName ? entry._attributes.siteName : '';
				// if type is manuscript, then check if flag is set 
				var flags = entry.flag ? entry.flag : ''
				var flaglist='';
				if(flags){
					if (flags.constructor.toString().indexOf("Array") < 0){
						flags = [flags];
						}
					flags.forEach(function(flag, rcIndex){
						if (flag._attributes['value'] == 1 ){
							$(containerHTML).find('.flagList').append('<span data-flag-type="'+flag._attributes['type']+'">'+flag._attributes['type']+'</span>')
							
						}
						else if(/(CC)/gi.test(flag._attributes['value'])){
							$(containerHTML).find('.flagList').append('<span data-flag-type="'+flag._attributes['type']+'">'+flag._attributes['value']+'</span>')
						}           
					})
				}
				if(entry._attributes['data-uuid'] && entry._attributes['data-uuid'] !=''){
					$(containerHTML).find('.sectionDataChild').attr('data-uuid',entry._attributes['data-uuid'])	
				}
				$(containerHTML).find('.sectionDataChild').attr('data-type',msConObj.dataType)
				$(containerHTML).find('.sectionDataChild').attr('data-grouptype',msConObj.dataGroupType)
				$(containerHTML).find('.sectionDataChild').attr('data-c-id',msConObj.tempID)
				$(containerHTML).find('.sectionDataChild').attr('class',className)
				$(containerHTML).find('.sectionDataChild').attr('data-sec','sec' + (msConObj.sectionIndex - 1))
				$(containerHTML).find('.sectionDataChild').attr('data-doi',entry._attributes.id)
				$(containerHTML).find('.sectionDataChild').attr('data-section',entry._attributes['section'])
				
				var artTitle = linkedArticle = AcceptedDate = artType = curStage = CELevel = stageOwner = daysInProd = stageDue = '';
				if(/(manuscript)$/gi.test(entry._attributes.type)){
					var stageData = $('#tocContainer').data('projectStages');
					 if(stageData){
						if (stageData.constructor.toString().indexOf("Array") < 0) {
							stageData = [stageData];
						}
						stageData.forEach(function (currObj){   
							var currDoi = currObj['id'].replace(/\.xml$/, '')
							if(currDoi == tempIDVal ){
								artTitle = currObj['title']
								artType = currObj['type']?currObj['type']:''
								AcceptedDate =currObj['acceptedDate']?'<span class="acceptedDate">Accepted: '+ currObj['acceptedDate']+'</span>':''
								linkedArticle = currObj['relatedArticles']
								CELevel = currObj['CELevel']?'<span class="ceLevel">CE:level: '+ currObj['CELevel']+'</span>':'<span class="ceLevel">CE:level: null</span>'
							   // curStage = currObj['workflow']?currObj['workflow']["current-stage"]:''
							   	curStage = eventHandler.general.actions.getStageDetails(currObj['workflow'])

							  // stageOwner = getStageDetails(currObj['workflow'])
							}
						})
						$(containerHTML).find('.row .msID .currentStage').text(curStage);
					}
				}else if (/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)) {
					//containerHTML.prepend('<div class="cardColor redCard"></div>');
				}
				$(containerHTML).find('.row .msID .doi').text(tempIDVal);
				$(containerHTML).find('.row .msID .artType').text(artType)
				var titleText = 'Title Text';
				if (entry.title && entry.title._text) {
					titleText =entry.title._text ? entry.title._text.replace(/\[\[/g, '<').replace(/\]\]/g, '>'):''
				}else if ((/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)) || (/^(front\s+matter)$/gi.test(entry._attributes.section))) {
					titleText = entry._attributes.type.toUpperCase();
				}
				if(/(manuscript)$/gi.test(entry._attributes.type)){
					var articleLink = '';
					if(/(kriya1\.0)$/gi.test(siteName)){ 
						var cmsId = entry._attributes.cmsID;
						var articleLink = "http://bmj.kriyadocs.com/review_content/article/"+cmsId+"?reviewer=typesetter"
						$(containerHTML).find('.editArticle a').attr('href',articleLink);
						$(containerHTML).find('.editArticle a').attr('target',"_blank") 
					}
					else if((/^(non\-kriya)$/gi.test(siteName))){
						$(containerHTML).find('.editArticle,.editArticle .editArticleIcon').addClass('disabled')
					}
					else{
						var articleLinkStage = 'review_content';
						if(configData["binder"]["manuscript"] && configData["binder"]["manuscript"]['_attributes'] && configData["binder"]["manuscript"]['_attributes']['articleLinkToStage']){
							articleLinkStage = configData["binder"]["manuscript"]['_attributes']['articleLinkToStage']
						}
						articleLink = '/'+articleLinkStage+'/?doi='+tempIDVal+'&customer='+cName+'&project='+ pName +'';
						$(containerHTML).find('.editArticle a').attr('href',articleLink);
						$(containerHTML).find('.editArticle a').attr('target',"_blank") 
					}
					$(containerHTML).find('.titleText').attr('title', titleText)
					$(containerHTML).find('.msTitle').append(titleText)
					
				}
				else{
					$(containerHTML).find('.titleText').attr('title', titleText)
					$(containerHTML).find('.msTitle').append(titleText) 
					$(containerHTML).find('.editArticle,.editArticle .editArticleIcon').addClass('disabled')
				}
				var incorrectPageClass = '';
				var pageModifiedClass = '';
				var hidePageRange = '';
				var title ='';
				if (entry._attributes.incorrectPage && (entry._attributes.incorrectPage.toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() == 'manuscript')){
					$(containerHTML).find('.pageRange').addClass('incorrectPage');
					$(containerHTML).find('.pageRange').attr('title', 'incorrectPage');
				}
				if (entry._attributes['data-unpaginated'] && (entry._attributes['data-unpaginated'].toLowerCase() == 'true')){
					hidePageRange = ' hide';
				}
				if (entry._attributes['data-run-on'] && (entry._attributes['data-run-on'].toLowerCase() == 'true')){
					hidePageRange = ' hide';
				}
				else if (entry._attributes['data-modified'] && (entry._attributes['data-modified'].toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() != 'advert_unpaginated') && (entry._attributes.type.toLowerCase() != 'blank')){
					$(containerHTML).find('.pageRange').addClass('pageModified');
					$(containerHTML).find('.pageRange').attr('title', 'Page is Modified');
				}
				if(hidePageRange != ' hide'){
					$(containerHTML).find('.pageRange').attr('data-fpage', entry._attributes.fpage).attr('data-lpage', entry._attributes.lpage)
					$(containerHTML).find('.pageRange .rangeStart').addClass(hidePageRange).text(entry._attributes.fpage);
					if (entry._attributes.fpage != entry._attributes.lpage) {
						$(containerHTML).find('.pageRange .rangeEnd').addClass(hidePageRange).text(entry._attributes.lpage);
					
						//if rangeEnd is not available append it after rangeStart
						if ($('#tocContainer .bodyDiv .sectionData:last .rangeEnd').length == 0) {
							$(containerHTML).find('.pageRange .rangeEnd').addClass(hidePageRange).text(entry._attributes.lpage);
					
						}
						else{
						   // change rangeEnd
							 $('#tocContainer .bodyDiv .sectionData:last .rangeEnd').text(entry._attributes.lpage);
						}
					}
					else {
						$(containerHTML).find('.pageRange .rangeEndInfo').remove();
						
					}
					// change RangeENd of sectionData whose sectionChild has fpage and lpage equal 
					 if(entry._attributes.fpage == entry._attributes.lpage && sectionChild == true){
						if ($('#tocContainer .bodyDiv .sectionData:last .rangeEnd').length == 0) {
							$('#tocContainer .bodyDiv .sectionData:last .rangeEndInfo').append('<span class="rangeEnd">' + entry._attributes.lpage + '</span>');
						}else{
							$('#tocContainer .bodyDiv .sectionData:last .rangeEnd').text(entry._attributes.lpage);
						}
					}
				}
				else{
					$(containerHTML).find('.pageRange .rangeStartInfo,.pageRange .rangeEndInfo').addClass('hide')
				}
				//set flag in page range of sectionhead when any of the child has incorrect page sequence / when page range is modified
				if (entry._attributes.incorrectPage && (entry._attributes.incorrectPage.toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() == 'manuscript')){
					$('#tocContainer .bodyDiv .sectionData:last .sectionRange').addClass('incorrectPage');
					$('#tocContainer .bodyDiv .sectionData:last .sectionRange').attr('title','incorrectPage');
				}
				else if (entry._attributes['data-modified'] && (entry._attributes['data-modified'].toLowerCase() == 'true') && (entry._attributes.type.toLowerCase() != 'advert_unpaginated') && (entry._attributes.type.toLowerCase() != 'blank')){
					$('#tocContainer .bodyDiv .sectionData:last .sectionRange').addClass('pageModified');
					$('#tocContainer .bodyDiv .sectionData:last .sectionRange').attr('title','Page is Modified');
				}
				$(containerHTML).find('.pageRange .site').text(siteName);
				var k1Download= '';
				   //if customer is bmj, enable icon to download kriya1 articles
				if($('#projectVal').attr('data-customer') != 'bmj' || entry._attributes.type != 'manuscript'){ //to check
					//k1Download = '<span class="controlIcons icon-download2"/>'
					$(containerHTML).find('.controls .downloadK1Article').remove();
				}
				return $(containerHTML).html();
			},			
			addSection: function(container, entry, className, secIndex, cid) {
				// sathiyad
				entry._attributes.fpage = entry._attributes.fpage ? entry._attributes.fpage : '';
				entry._attributes.lpage = entry._attributes.lpage ? entry._attributes.lpage : '';
				if (entry._attributes.type && $('#sectionCard').find('[data-name="' + entry._attributes.type + '"]').length > 0){
					var containerHTML = $('#sectionCard').find('[data-name="' + entry._attributes.type + '"]').clone();
				}else{
					var containerHTML = $('#sectionCard').find('[data-name="default"]').clone();
				}
				// if it does not have a child, then clicking the section should bring up the details on the right else open the child nodes
				var funcName = 'section';
				var dataType = 'section';
				if (className == 'noChild') {
					funcName = 'panel';
					if (entry._attributes && entry._attributes.type) {
						dataType = entry._attributes.type;
						$(containerHTML).find('.openclose').remove()
						$(containerHTML).find('.sectionData').attr('data-doi',entry._attributes.id)
						if(entry._attributes['data-uuid'] && entry._attributes['data-uuid'] !=''){
							$(containerHTML).find('.sectionData').attr('data-uuid',entry._attributes['data-uuid'])	
						}
					}
				}
				var dataGroupType = '';
				if (entry._attributes && entry._attributes.grouptype) {
					dataGroupType = entry._attributes.grouptype;
					$(containerHTML).find('.sectionData').attr('data-grouptype',dataGroupType)
				
				}
				var funcName = className == 'noChild' ? 'manuscript' : 'tocSection';
				$(containerHTML).removeAttr('data-name');
				$(containerHTML).find('.sectionHead').html(entry._attributes.section)
				$(containerHTML).find('.sectionData').attr('data-type',dataType)
				$(containerHTML).find('.sectionData').attr('id','sec' + secIndex)
				$(containerHTML).attr('id','section' + secIndex)
				var cidData = typeof (cid) != 'undefined' ? $(containerHTML).find('.sectionData').attr('data-c-id',cid)  : '';
				$(containerHTML).find('.sectionData').attr('class','sectionData ' + className + ' ' + entry._attributes.type)
				$(containerHTML).find('.sectionData').attr('data-section',entry._attributes.section)
				$(containerHTML).find('.sectionData').attr('data-message','{\'funcToCall\': \'' + funcName + '\'}')
				$(containerHTML).find('.rangeStart').html(entry._attributes.fpage)
				if (entry._attributes.fpage != entry._attributes.lpage) {
					$(containerHTML).find('.rangeEnd').html(entry._attributes.lpage)
				}
				else{
					$(containerHTML).find('.rangeEnd').remove()
				}
				if($('#projectVal').attr('data-customer') != 'bmj' || entry._attributes.type != 'manuscript'){ //to check
					//k1Download = '<span class="controlIcons icon-download2"/>'
					$(containerHTML).find('.controls .downloadK1Article').remove();
				}
				container.append(containerHTML);
				
				if (cid == 0 || cid) {
					$('[data-c-id="' + cid + '"]').data('data', entry);
				}
			},
			getAvailableSections: function(){
				//get all section data, form a array with all available sectiondata
				var sections = $('#tocBodyDiv div.sectionData');
				var sectionArray = [];
					sections.each(function(){
						var currNode = $(this);
						var currSection = currNode.attr('data-section');
							sectionArray.push(currSection);
					})
			   
				var uniqueSectionArray = Array.from(new Set(sectionArray))
				return uniqueSectionArray
			},
			/* populate printer pdfs of manuscript, adverts, cover,toc and arrange the pdfs in flatplan view */
			populateFlatPlan: function() {
				$('#tocBodyDiv').addClass('hide');
				$('#flatPlanBodyDiv').addClass('active');
				$('[data-href="tocBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$('[data-href="flatPlanBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				var customer = $('#projectVal').attr('data-customer');
				var project = $('#projectVal').attr('data-project');
				var container = $('#tocContainer .bodyDiv #flatPlanBodyDiv');
					container.html('')
				var containerHTML = '<div class="container"><div class="wrapper">';
				var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
				var imgArray = [];
				if(content){
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					// push all pdfs in Imgarray, 
					content.forEach(function (entry) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : '';
						var fpage =  entry._attributes.fpage;
						var groupType = entry._attributes.grouptype
						var lpage =  entry._attributes.lpage;
						var pdf = entry.file;
						var type=entry._attributes.type;
						var runOnflag = false;
						var srcId = id;
						var fileFlag = false
						var fileObj = entry['file'];
						if(fileObj){
							if (fileObj.constructor.toString().indexOf("Array") == -1) {
								fileObj = [fileObj];
							}
							fileObj.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
								var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
								if(currProofType == 'print' && pdfType != 'preflightPdf' && file['_attributes']['path']!=''){
									fileFlag = true
								}
							})
						}	
                		if(type == 'cover' || (/(advert)/gi.test(type)) || type == 'table-of-contents' || type == 'index'){
                    		srcId = entry._attributes['data-proof-doi'] ? entry._attributes['data-proof-doi'] : id
                   		}
            			if(entry._attributes['data-run-on'] && entry._attributes['data-run-on'] == 'true'){
							runOnflag = true
						}
						//get the pageExtent of each content,  push each page in imgArray
						if(pageExtent && runOnflag == false){
						var currentPage = 0;
						var ePageFlag = false;
						var romanFlag = false;
							if(/(c)$/gi.test(fpage) || pageExtent == 1){
								var  currentPage = fpage
								var pagneNum = pad(1, 3)
								var src= '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/pdfThumbnails/page_'+pagneNum+'.png?method=server'
								if (!(/^(kriya|demo)/gi.test(window.location.host))) {
									src = '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/'+ window.location.host +'/pdfThumbnails/page_'+pagneNum+'.png?method=server'
								}
								var altSrc= src
								if(fileFlag == false && !(entry["region"])){
									src = '../images/NoImageFound.jpg'
								}
								var img= ({"_attributes":{"src": src,"alt":altSrc,"data-type":type,"data-section":entry._attributes.section,"data-current-page":currentPage,"id":id}})
									imgArray.push(img);
							}
							else{
								//set epage flag
								if(/(e)/gi.test(fpage)){
										fpage = fpage.replace('e','')
										lpage = lpage.replace('e','')
										ePageFlag = true
								}
								else if((groupType!='' && groupType!=undefined && groupType== 'roman' && typeof fpage != "number")||(type == 'table-of-contents' && typeof fpage != "number")){
									fpage = fromRoman(fpage)
									//roman set roman flag
									romanFlag = true
									}
									fpage = parseInt(fpage);  
								//parseint fpage ,lpage
						
								for(var i=0; i<pageExtent; i++){
									var currentPage = fpage+i
									var pagneNum = pad(i+1, 3)
									if(romanFlag == true){
										currentPage = toRoman(currentPage)
									 }
									 else if( ePageFlag == true){
										 currentPage = 'e'.concat(currentPage)
									 }
									var src= '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/pdfThumbnails/page_'+pagneNum+'.png?method=server'
									if (!(/^(kriya|demo)/gi.test(window.location.host))) {
										src = '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/'+window.location.host +'/pdfThumbnails/page_'+pagneNum+'.png?method=server'
									} 	
									var altSrc = src
									if(fileFlag == false && !(entry["region"])){
										src = '../images/NoImageFound.jpg'
									}
									var img= ({"_attributes":{"src": src,"alt":altSrc,"data-section":entry._attributes.section,"data-current-page":currentPage,"id":id}})
									imgArray.push(img)
								//   currIndex++;
								}
							}
						}
					
					});
				}
				//console.log(imgArray)
				//loop through each image in image array and arrange the images in flatplan view
				var currIndex = 1;   
				var openDiv ='<div style="height:100%">'
				var closeDiv = '</div>'
				var coverflag = false
				var bookletHTML ='<div class="modal fade " id="myModal1" role="dialog"> <button type="button" style="float:right"  class="btn btn-default" data-dismiss="modal">Close</button><div id="mybook">'
				if(imgArray){
					imgArray.forEach(function (img,currIndex) {
						var refID = img._attributes.id.replace(/\./gi, '')
						if(currIndex == 0){
							if(img._attributes['data-type'] && (/(cover)/gi.test(img._attributes['data-type']))){
								containerHTML += '<div><p class="imgblock"  style="float:right;display:grid;margin-left:50%"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+ currIndex +')" style="height:130px;float:right;width:100%;border-right: 1px solid grey;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p></div>';
								coverflag = true      
							}
							else{
								containerHTML += '<div style="height:100%"><p class="imgblock"  style="float:right;width:50%;display:grid;margin-left:50%" onClick="eventHandler.general.components.populateBooklet('+ currIndex +')"><span style="height: 130px;font-size: large;border: 1px solid black;padding: 40px 20px 20px 20px;"><span>COVER</span></span><span class="fpRange">C1</span></p></div>';
							var temp = parseInt(currIndex)+1
						
							containerHTML += '<div><p class="imgblock"  style="float:left;width:50%;display:grid;"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+ temp +')" style="height:130px;float:right;width:100%;border-right: 1px solid grey;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>';               
							}
						
						}
						if(currIndex == 0 && coverflag ==false){
							bookletHTML+= openDiv + '<p style="margin: 35%;font-size: -webkit-xxx-large;">COVER</p>' +closeDiv
							bookletHTML+= openDiv + '<img  class="img-responsive" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" style="background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/>' +closeDiv

						} 
						else{
							bookletHTML+= openDiv + '<img  class="img-responsive" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" style="background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/>' +closeDiv

						}
						if(coverflag ==false){
							if(currIndex !=0){
								var temp = parseInt(currIndex)+1
								if(currIndex %2 != 0 ){
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+ temp +')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>'
									containerHTML += closeDiv
								}
								else{
									containerHTML += openDiv   
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+temp +')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+ img._attributes['data-current-page']+'</span></p>'
								}
							}
						}
						else{
							if(currIndex !=0){
								if(currIndex %2 != 0 ){
									containerHTML += openDiv   
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+currIndex +')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>'
								}
								else{
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.general.components.populateBooklet('+currIndex +')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="imgError(this)"/><span class="fpRange">'+ img._attributes['data-current-page']+'</span></p>'
									containerHTML += closeDiv
								}
							}
						}
						currIndex++;
					})
				}
				containerHTML +='</div></div>'
				bookletHTML +='</div></div>'
				container.append(containerHTML+bookletHTML);
				$('#tocContainer #flatPlanBodyDiv').css('height', (window.innerHeight - $('#tocContainer .bodyDiv').offset().top  - $('#footerContainer').height() - 10) + 'px');
				$('#flatPlanBodyDiv .container').css('height',  $('#flatPlanBodyDiv').height());
				$('#flatPlanBodyDiv .container').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"dark"
				});
			},
			/* 	populate all contents from issueXML 
				group contents by articles , Asverts/Fillers  
				onClick of a content, scrolls to the corresponding pdf in flatplan
			*/
			populateContents: function() {
				var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
				var prevSection = '';
				var container = $('#detailsContainer #contentsLits .bodyDiv #artListDiv');
				container.html('');
				var containerHTML = '';
				var sectionIndex = 1;
				var tempID = -1;
				var currIndex = 0;
			   // contentHash = {}
			    if($('#filterProject .active').attr('data-project-type') == 'book'){
					containerHTML += '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Chapters</div>' 
				}
			    else {
					containerHTML += '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Articles</div>' 
				}
				// list all articles, cover,toc
				if(content){
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry,currIndex) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var type = entry._attributes.type;
						var child =false;
						var doi = entry._attributes.id
						doi = doi? "'".concat(doi).concat("'") : ''
						doi = doi.replace(/\./gi, '')
						var colStart = '<div class="col-sm-12 col-md-12 col-lg-12" onClick="goToView('+doi +')">';
						var closeDiv = '</div>';
						if (entry._attributes && entry._attributes.type && (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)))) {
							dataType = ' data-type="' + entry._attributes.type + '"';
							containerHTML += colStart
							var title = entry.title ? entry.title._text :entry._attributes.type
							var currentId = entry._attributes.id;
							if (entry._attributes && entry._attributes.type && ((/^(cover|table\-of\-contents|editorial\-board)$/gi.test(entry._attributes.type)))) {
								currentId = entry._attributes.section
							}
						
							containerHTML += '<p  class="col-sm-8 col-md-8 col-lg-8"  rid="' + currentId + '" >'+currentId+'</p>' 
							containerHTML += '<p  class="col-sm-4 col-md-4 col-lg-4" >'+entry._attributes.fpage
							if (entry._attributes.lpage && entry._attributes.fpage != entry._attributes.lpage) {
								containerHTML += '&#x2013;' + entry._attributes.lpage;
							}
							containerHTML += '</p>' 
							containerHTML += closeDiv
							currIndex++;
						}    
					});
				}    
				container.append(containerHTML)
				containerHTML = '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Adverts/Fillers</div>' 
				//list all adverts/fillers
				if(content){
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry,currIndex) {
						if (entry._attributes && entry._attributes.type && ((/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)))) {
							var colStart = '<div class="col-sm-12 col-md-12 col-lg-12" >';
							var closeDiv = '</div>';
							containerHTML += colStart
							var title = entry.title ? entry.title._text :entry._attributes.type
							var currentId = entry._attributes.id ?  entry._attributes.id.replace(/\./gi, '') : '';
							containerHTML += '<p  class="col-sm-8 col-md-8 col-lg-8"  rid="' + currentId + '" onClick="goToView('+ currentId+')">'+currentId+'</p>' 
							containerHTML += '<p  class="col-sm-4 col-md-4 col-lg-4" >'+entry._attributes.fpage
							if (entry._attributes.lpage && entry._attributes.fpage != entry._attributes.lpage) {
								containerHTML += '&#x2013;' + entry._attributes.lpage;
							}
							containerHTML += '</p>' 
							containerHTML += closeDiv
						}
					})
				}    
				container.append(containerHTML)
				$('#contentsLits #artListTab').css('height',  $('#contentsLits .bodyDiv').height());
				$('#artListTab').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"dark"
				});
			},
			populateHistory: function(chData) {
				//$('#changeHistory').html('')
				var changeHistoryData = [];
				if (typeof (chData) != 'undefined') {
					changeHistoryData = [{
						"user": { "_text": chData.user},
						"change": { "_text": chData.change},
						"time": { "_text": chData.time},
						"file": { "_text": chData.file},
						"fileType": { "_text": chData.fileType}
					}]
				}
				else {
					$('#changeHistory').html('')
					changeHistoryData = $('#tocContainer').data('binder')['container-xml']['change-history'];
				}
				var chHTML = '';
				changeHistoryData.forEach(function (chData) {
				var chTime = chData.time._text
					chTime = chTime.replace(/^[a-z]+ (.*:[0-9]{2}) .*$/gi, "$1");
					chHTML += '<p class="chData">';
					chHTML += '<span class="user">' + chData.user._text + '</span> <span class="change">' + chData.change._text + '</span> on <span class="time" title="' + chData.time._text + '">' + chTime + '</span>';
					if (chData.file && chData.file._text){
						chHTML += 'for <span class="file">' + chData.file._text + '</span>&#x00A0;(<span class="type">' + chData.fileType._text + '</span>)';
				}
					chHTML += '</p>';
				});
				$('#changeHistory').append('<div class="changeHistoryContainer">'+chHTML+'</div>');
				//$('#changeHistory .changeHistoryContainer').css('height',$('#changeHistory').height());
				$('#changeHistory .changeHistoryContainer').css('height',window.innerHeight - $('#metaHistory .bodyDiv').offset().top - $('#footerContainer').height() - 15 +'px')
				$('#changeHistory .changeHistoryContainer').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"dark"
				});
			},
			/* function to initiate modal for booklet*/
			populateBooklet: function(startPage) {
				var w = window.innerWidth;
				var h = window.innerHeight;
				//console.log("Width: " + w + "<br>Height: " + h);
				w = parseInt(w) - 450
				h = parseInt(h) - 35
				$('#myModal1').modal('show')
				$('#mybook').booklet({
					pagePadding: 0,
					tabs: true,
					overlays: true,
					manual: false,
					pageNumbers: false,
					closed: true,
					startingPage :startPage+1,
					height: h,
					width:w,
					speed: 1000,
					shadows:true
				});
				$('#myModal1').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"light-thick"
				});
			},
			closeBooklet: function() {
			   $('#myModal1').css('display','none')
			   $('#myModal1').css('padding-left','0px')
			   $('#myModal1').removeClass('in')
			},
		}
	}
	eventHandler.messageCode={
		errorCode:{
			//error in get method
			500: {
				1 : 'Something went wrong when fetching Data.',
				2 : 'Something went wrong when fetching Customer List.',
				3 : 'Something went wrong when fetching data from ',
				4 : 'Something went wrong when fetching Project List',
				5 : 'Something went wrong when retrieving Issue List.',
				6 : 'Something went wrong when fetching Issue Data.',
				7 : 'Something went wrong when fetching Project Stages.',
				8 : 'Something went wrong when fetching Advert/Filler layouts.',
				9 : 'Something went wrong when fetching Article.',
				10 : 'Something went wrong when retrieving Article List.',
			},
			//error in post method
			501: {
				1 : 'Saving failed.',
				2 : 'Error while saving article XML.',
				3 : 'Upload failed.',
				4 : 'Saving Meta Infofailed.',
				5 : 'Something went wrong when combining pdf.',
				6 : 'Error in Creating Issue.'
			},
			//incorrect info
			502: {
				1 : 'Unable to add the requested file.',
				2 : 'Filler can oly be added below Manuscript.',
				3 : 'Project already Exists.',
				4 : 'Book || Customer is Missing.',
				5 : 'Incorrect page sequence.',
				6 : 'Issue XML already Exists.',
				7 : 'Empty Value Found !',
				8 : 'PDF is not uploaded for Non-Kriya Articles.'
			
			},
			//prompt message
			503: {
			 1 : 'Please try again or contact support.',
			 2 : 'Please correct RO XML and re-upload.',
			 3 : 'Please upload PDF and try again',	
			 4 : 'already processed for this Issue. Pls export Individual PDF from List View'
			}
		},
		successCode:{
			200: {
				1 : 'Upload Succeeded',
				2 : 'Issue Created',
				3 : 'All Changes Saved',
				4 : 'Meta Info Saved',
				5 : 'Article XML Saved'
			}
		}
	}
	return eventHandler;
})(eventHandler || {});