
function dateDistance(fromDate, toDate, daysToExclude, excludeWeekoff=false) {
    fromDate = moment(moment(fromDate).format('YYYY-MM-DD HH:mm:ss'));
    toDate = moment(moment(toDate).format('YYYY-MM-DD HH:mm:ss'));
    var dateDiff = (toDate.diff(fromDate, 'hours')) / 24;
    while (toDate > fromDate) {
        if (!excludeWeekoff && (fromDate.isoWeekday() == 6 || fromDate.isoWeekday() == 7 || (daysToExclude && daysToExclude.indexOf(moment(fromDate).format('MMM DD')) > -1))) {
            dateDiff = (dateDiff - 1);
        }
        fromDate = fromDate.add(1, 'days');
    }
    return dateDiff;
}