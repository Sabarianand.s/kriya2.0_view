﻿var config = {
    "defaultUnits":"pt",
    "baseLeading":11.5,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY",
    "minStackSpace":18,
    "minNoLinesOnPag":3,
    "placeStyle":"Sandwich",
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.5,
    "applyTableBorderColor":'COLOR1',
    "pageSize":{
        "width":595.276,
        "height":841.89
    },
    "exportPDFpreset":{
        "print":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":18,
      "right":18,
      "gutter":18
    },
    "geoBoundsVerso":[86,43,757.36062992126,552.276
    ],
    "geoBoundsRecto":[86,638.275590551181,757.361,1147.55159055118
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1",
    "LRTOTEER":"LAYOUT2"
    },
    "pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
                  {"width":243.138, "height":656.09,"floatsCited":false,"gutter":0},
                  {"width":243.138, "height":656.09,"floatsCited":false,"gutter":23}
                ],
                "columnDetails":[
                  {"width":243.138, "height":671.361,"floatsCited":false,"gutter":0},
                  {"width":243.138, "height":671.361,"floatsCited":false,"gutter":23}
                ],
                "openerPageMargin":{
                    "top":68.5,
                    "bottom":59.8,
                    "inside":43,
                    "outside":43
                },
            "otherPageMargin":{
                "top":86,
                "bottom":84.5,
                "inside":43,
                "outside":43
                }
            },
        "LAYOUT2":{        
            "openerPageColumnDetails":[
                  {"width":243.138, "height":656.09,"floatsCited":false,"gutter":0},
                  {"width":243.138, "height":656.09,"floatsCited":false,"gutter":23}
                ],
                "columnDetails":[
                  {"width":243.138, "height":671.361,"floatsCited":false,"gutter":0},
                  {"width":243.138, "height":671.361,"floatsCited":false,"gutter":23}
                ],
                "openerPageMargin":{
                    "top":68.5,
                    "bottom":59.8,
                    "inside":43,
                    "outside":43
                },
            "otherPageMargin":{
                "top":86,
                "bottom":84.5,
                "inside":43,
                "outside":43
                }
            }
         },
    "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":339.517, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlArtType,jnrlArtTitle,jrnlAuthors,jrnlAbsHead,jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],
    
    "docFontsList":[
       {
          "Minion ": {
            "Regular ": {
                "Symbol (T1)": "Regular"
              },
            "Italic": {
                "Symbol (T1)": "Italic"
              },
            "Bold": {
                "Symbol (T1)": "Bold"
              },
            "Bold Italic": {
                "Symbol (T1)": "Bold"
              }
          }
      },
        {
          "Gotham": {
            "Book": {
                "Symbol (T1)": "Regular"
              },
                "Book Italic": {
                    "Symbol (T1)": "Italic"
              },
                "Light": {
                    "Symbol (T1)": "Regular"
            },
                "Light Italic": {
                    "Symbol (T1)": "Italic"
            },
                "Bold": {
                    "Symbol (T1)": "Bold"
            }
          }
        },
          {
          "Symbol (T1)": {
            "Regular": {
                "Arial Unicode MS": "Regular"
              },
                "Italic": {
                    "Arial Unicode MS": "Regular"
              },
                "Bold": {
                    "Arial Unicode MS": "Regular"
            }
          }
        }
    ],
    
   "replFonts":[
            {"fontFamily":"Minion ", "fontStyle":"Regular "},
            {"fontFamily":"Minion ", "fontStyle":"Italic"},
            {"fontFamily":"Minion ", "fontStyle":"Bold"},
            {"fontFamily":"Minion ", "fontStyle":"Bold Italic"},
            {"fontFamily":"Gotham", "fontStyle":"Book"},
            {"fontFamily":"Gotham", "fontStyle":"Book Italic"},
            {"fontFamily":"Gotham", "fontStyle":"Light"},
            {"fontFamily":"Gotham", "fontStyle":"Light Italic"},
            {"fontFamily":"Gotham", "fontStyle":"Bold"},
            {"fontFamily":"Symbol", "fontStyle":"Medium"},
            {"fontFamily":"Symbol", "fontStyle":"Italic"},
            {"fontFamily":"Symbol", "fontStyle":"Bold"}
    ],
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlReDateBlock']", "frame-name":"REDATE", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlRevDateBlock']", "frame-name":"REVDATE", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlAcDateBlock']", "frame-name":"ACDATE", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlToCite']", "frame-name":"TOCITE", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlLicenseStmtGrp']", "frame-name":"LICENSE-STMT", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlSTRH']", "frame-name":"STRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlARH']", "frame-name":"ARH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRF']", "frame-name":"LRF", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRF']", "frame-name":"RRF", "action":"move", "styleOverride":null}
       ]       
     },
 "colorDetails":{
         "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,80"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,60"
                    },
                  "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"88,89,91"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"128,130,133"
                    },
                 "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    }
                }            
            },
        "LRTOTEER":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,80"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,60"
                    },
                  "COLOR3":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,20"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"88,89,91"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"128,130,133"
                    },
                 "COLOR3":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,211,212"
                    }
                }            
            }
        },
         "tableSetter" :{
            "thead": {
                "css" : {
                    "font-family" : "'MinionPro-Semibold', Times New Roman",
                    "font-size" : "10px",
                    "line-height" : "12px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionPro.otf",
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'MinionPro-Regular', Times New Roman",
                    "font-size" : "8px",
                    "line-height"  : "9px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionPro.otf",
                "align" : "left",
                "valign": "bottom"
            },
            "charAlignCenter": true
        },
        "equation" : {
          "default" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                  "size"    : "9.5pt",
                  "ledding" : "11.5pt",
                  "path"    : "/MinionPro/",
                  "ext"     : "otf",
                  "bold"    : "MinionProBold",
                  "italic"  : "MinionProItalic",
                  "bold_italic":"MinionProBoldItalic",
                  "main"    : "MinionProRegular"
              }
           },
           "jrnlCitation" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "7pt",
                "ledding" : "8.4pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Book-Italic",
                "main"  : "Gotham-Book"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
          },
          "jrnlAbsPara" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "11pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book"
              }
          },
           "jrnlAbsGroup" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "11pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book",
              },
              "online" : {
                "textColor"    : "88, 89, 91"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
          },
           "jrnlFigCaption" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "11pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
           },
           "jrnlHead1" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "9.5pt",
                "ledding" : "11.5pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Bold"
              }
          },
          "jrnlHead2" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "9.5pt",
                "ledding" : "11.5pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Book",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book"
              }
          },
           "jrnlHead3" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "9.5pt",
                "ledding" : "11.5pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book-Italic"
              }
          },
           "jrnlTblCaption" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "10pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
           },
           "jrnlTblFoot" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "7pt",
                "ledding" : "9pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Book-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Book"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
           },
           "jrnlTblHead" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "10pt",
                "ledding" : "12pt",
                "path"    : "/MinionPro/",
                "ext"     : "otf",
                "bold"    : "MinionProSemibold",
                "italic"  : "MinionProSemiboldIt",
                "bold_italic":"MinionProSemibold",
                "main"  : "MinionProSemibold"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
                //"colorFormat":"RGB"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
          },
           "jrnlTblBody" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "10pt",
                "path"    : "/MinionPro/",
                "ext"     : "otf",
                "bold"    : "MinionProBold",
                "italic"  : "MinionProItalic",
                "bold_italic":"MinionProBoldItalic",
                "main"    : "MinionProRegular"
              },
              "online" : {
                "textColor"    : "88, 89, 91"
                //"colorFormat":"RGB"
              },
              "print" : {
                "textColor"    : "0,0,0,0.8"
              }
         },
           "txtCiteInfo" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "7pt",
                "ledding" : "9pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Light-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Light"
              },
              "online" : {
                "textColor"    : "109, 110, 113"
                //"colorFormat":"RGB"
              },
              "print" : {
                "textColor"    : "0,0,0,0.7"
              }
          },
           "jrnlArtTitle" : {
              "page":{
                  "size" : "509.276pt"
              },
              "font" : {
                "size"    : "16.5pt",
                "ledding" : "19.5pt",
                "path"    : "/Gotham/",
                "ext"     : "otf",
                "bold"    : "Gotham-Bold",
                "italic"  : "Gotham-Bold-Italic",
                "bold_italic":"Gotham-Bold-Italic",
                "main"  : "Gotham-Bold"
              }
          },
         "jrnlTblBlock" : {
            "page":{
                "size" : "509.276pt"
            },
            "font" : {
              "size"    : "8pt",
              "ledding" : "9pt",
              "path"    : "/MinionPro/",
              "ext"     : "otf",
              "bold"    : "MinionProBold",
              "italic"  : "MinionProItalic",
              "bold_italic":"MinionProBoldItalic",
              "main"    : "MinionProRegular",
            }
         }
        }
    }
    this.config = config;