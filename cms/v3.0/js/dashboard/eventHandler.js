var usersArr = {};
var cardData = {};
var assignUserData = {};
var bookData = [];
var utilizationReport = [];
var overAllAverage = {};
var lastMonthList = [];
var issueValData = [];
var unAssignData = [];
var chartData = {};
var manuScriptCardData = {};
var supportData = {};
var projectLists = {};
var usersList = {};
var usersLists = {};
var averageUserList = [];
var userData = JSON.parse($('#userDetails').attr('data'));
var currentCustomers = [];
var key = 0;
var chartProject = [];
var selectedCustomer;
var usersDetailsDiv;
var assign;
var stageNameForReportTable;
var blkSuccessDoi, blkFailureDoi, lastCount, successCount, failureCount, openedArticles = 0;
var searchedDoiList = [];
var tableReportData = [];
var currentTab = '.dashboardTabs.active ';
var resetTimer = 10;
/**
 * Added by Anuraja on 2018-08-01
 * Function to create Excel from Table data
 * Link: http://jsfiddle.net/cmewv/5623/
 */
var tableToExcel = (function () {
	var uri = 'data:application/vnd.ms-excel;base64,',
		template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
	base64 = function (s) {
			return window.btoa(unescape(encodeURIComponent(s)))
		},
		format = function (s, c) {
			return s.replace(/{(\w+)}/g, function (m, p) {
				return c[p];
			})
		}
	return function (table, name) {
		if (!table.nodeType)
			table = document.getElementById(table)

		var ctx = {
			worksheet: name || 'Worksheet',
			table: table.innerHTML
		}
		// window.location.href.download = 'ad.xls'
		var blob = new Blob([format(template, ctx)]);
		var a = window.document.createElement('a');
		a.href = window.URL.createObjectURL(blob);
		a.download = name + '.xls';
		// Append anchor to body.
		document.body.appendChild(a);
		a.click();
		// Remove anchor from body
		document.body.removeChild(a);
		return false;
	}

})()

/**
 * eventHandler - this javascript holds all the functions required for Reference handling
 *					so that the functionalities can be turned on and off by just calling the required functions
 *					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
 */
var filterData = {};
var eventHandler = function () {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['mainMenu', 'components', 'dropdowns', 'bulkedit', 'common', 'flatplan'];
	//add or override the initial setting
	var initialize = function (params) {
		function extend(obj, ext) {
			if (obj === undefined) {
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function () {
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function () {
			/**
			 * attach a click event listner to the body tag
			 * if the target node has the special attribute 'data-channel',
			 * then we need to publish some data using the data in the attributes
			 */
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function () {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function (event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);
			//Hide more action dropdowns
			$('.moreActionsBtns:not(.hidden)').addClass('hidden');

			$(elements).each(function () {
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length) {
					targetNode = $(targetNodes[0]);
					if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
						return;
					}
					var message = eval('(' + targetNode.attr('data-message') + ')');
					var eveDetails = message[event.type];
					if (!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type) {
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic = targetNode.attr('data-topic');
					} else if (typeof (eveDetails) == "string" && typeof (message[eveDetails]) == "object") {
						eveDetails = message[eveDetails];
					} else if (!eveDetails || typeof (eveDetails) == "string") {
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic: eveDetails.topic + '.' + event.type,
						data: {
							message: eveDetails,
							event: event.type,
							target: targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function () {
			var subscribers = eventHandler.settings.subscribers;
			for (var i = 0; i < subscribers.length; i++) {
				initSubscribe(subscribers[i], subscribers[i] + '_subscription');
			}

			function initSubscribe(subscribeChannel, subscribeName) {
				if (!eventHandler.settings.subscriptions[subscribeName]) {
					eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
						channel: subscribeChannel,
						topic: "*.*",
						callback: function (data, envelope) {
							//console.log(data, envelope);
							if (data.message != '') {
								/*http://stackoverflow.com/a/3473699/3545167*/
								if (typeof (data.message) == "object") {
									var array = data.message;
								} else {
									var array = eval('(' + data.message + ')');
								}
								var functionName = array.funcToCall;
								var param = array.param;
								var topic = envelope.topic.split('.');
								//var fn = 'eventHandler.menu.edit.'+functionName;
								if (typeof (window[functionName]) == "function") {
									window[functionName](param, data.target);
								} else if (typeof (window['eventHandler'][envelope.channel][topic[0]]) == "object") {
									if (typeof (window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function') {
										window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
									}
								} else {
									console.log(functionName + ' is not defined in ' + topic[0]);
								}
							} else {
								console.log('Message is empty');
							}

							// `data` is the data published by the publisher.
							// `envelope` is a wrapper around the data & contains
							// metadata about the message like the channel, topic,
							// timestamp and any other data which might have been
							// added by the sender.
						}
					});
				}
			}
		},
		remove: function () {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};

	return eventHandler;
})(eventHandler || {});

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.mainMenu = {
		toggle: {
			toggleContainer: function (param, targetNode) {
				if (targetNode != undefined) {
					$("#supportReport").addClass('hidden');
					$("#supportReportContainer").css('display', 'none')
					$('.dashboardTabs').removeClass('active');
					$('.issueViewButton').addClass('hidden');
					$('.sidebar .nav-item .nav-link').removeClass('active');
					var contNode = $('#' + $(targetNode).attr('data-href'));
					// targetNode.addClass('active');
					contNode.addClass('active');
				}
			},
			paging: function () {
				pageSize = 10;
				incremSlide = 5;
				startPage = 0;
				numberPage = 0;

				var pageCount = $(".line-content").length / pageSize;
				var totalSlidepPage = Math.floor(pageCount / incremSlide);
				for (var i = 0; i < pageCount; i++) {

					$("#pagin").append('<li class="page-item" style="display:inline-block"><a class="page-link" href="#">' + (i + 1) + '</a></li> ');
					if (i > pageSize) {
						$("#pagin li").eq(i).hide();
					}
				}


				var prev = $("<li/>").addClass("page-item page-link prev").html("Prev").click(function () {
					startPage -= 5;
					incremSlide -= 5;
					numberPage--;
					slide();
				});

				prev.hide();

				var next = $("<li/>").addClass("page-item page-link next").html("Next").click(function () {
					startPage += 5;
					incremSlide += 5;
					numberPage++;
					slide();
				});


				$("#pagin").prepend(prev).append(next);
				$("#pagin li").first().find("a").addClass("current");
				$("#pagin li").first().find("search").addClass("active")
				slide = function (sens) {
					$("#pagin li").hide();

					for (t = startPage; t < incremSlide; t++) {
						$("#pagin li").eq(t + 1).show();
					}
					if (startPage == 0) {
						next.show();
						prev.hide();
					} else if (numberPage == totalSlidepPage) {
						next.hide();
						prev.show();
					} else {
						next.show();
						prev.show();
					}


				}
				$("#pagin li").hide();

					for (t = startPage; t < incremSlide; t++) {
						$("#pagin li").eq(t + 1).show();
					}
					if (startPage == 0) {
						next.show();
						prev.hide();
					} else if (numberPage == totalSlidepPage) {
						next.hide();
						prev.show();
					} else {
						next.show();
						prev.show();
					}
				showPage = function (page) {
					$(".line-content").hide();
					$(".line-content").each(function (n) {
						if (n >= pageSize * (page - 1) && n < pageSize * page)
							$(this).show();
					});
				}

				showPage(1);
				$("#pagin li a").eq(0).addClass("current");
				$("#pagin li").eq(1).addClass("active");
				$("#manuscriptsData #assignedManuscript li").css('display','')
				$("#pagin li a").click(function () {
					$("#pagin li a").removeClass("current");

					$(this).addClass("current");
					$("#pagin li").removeClass("active");
					$(this).parent().addClass('active')
					showPage(parseInt($(this).text()));
					$("#manuscriptsData #assignedManuscript li").css('display','')
				});
			},
			// To display the Reports Page in the dashboard
			showReport: function (param, targetNode) {
				if (!$(targetNode).hasClass('active')) {
					$('#search').hide();
					$('.showEmpty .watermark-text').html('Please select customer <br/>to display Report');
					$('.showEmpty').show();
					if (!$('#reportContent table').length==0) {
						$('.showEmpty .watermark-text').html('Please select customer <br/>to display Report');
						$('.showEmpty').hide();
					}
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
					var customerName = '';
					if($('#reportCustomerVal').text() != 'Customer')
					$('.showempty').hide()
					var cardCustomer = $('#manuscriptContent #customerVal').text();
					var chartCustomer = $('#chartSelection #customerVal').text();
					if (!cardCustomer.match(/Customer|customer/g)) {
						customerName = cardCustomer;
					} else if (!chartCustomer.match(/Customer|customer/g)) {
						customerName = chartCustomer;
					}
					$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .windowHeight').offset().top) - $('#footerContainer').height()) + 'px');
					eventHandler.components.actionitems.initializeAddJob(param);
					eventHandler.dropdowns.project.reportCustomerProject(customerName);
				}
			},
			showDashboard: function (param, targetNode) {
				if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')){
					eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
				}
				$('#search').hide();
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display charts');
				$('.showEmpty').show();
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				$('#manuscriptContent #filterCustomer').find('.active')
				var cardCustCount = $('#manuscriptContent #filterCustomer').find('.active').length;
				var reportCustCount = $('#reportContent #reportCustomer').find('.active').length;
				var cardCust = $('#manuscriptContent #customerVal').text();
				var chartCust = $('#chartSelection #customerVal').text();
				var reportCust = $('#reportSelection #reportCustomerVal').text();
				var customer = '';
				if (!cardCust.match(/Customer|customer/g)) {
					customer = cardCust;
					// $('#chartSelection .showBreadcrumb').css('display','inline-block');
				} else if (!reportCust.match(/Customer|customer/g)) {
					customer = reportCust;
					// $('#chartSelection .showBreadcrumb').css('display','inline-block');
				}
				if (cardCustCount > 0 || reportCustCount > 0) {
					if (chartCust != customer) {
						$('#customerVal').text(customer);
						$('#chartSelection .showBreadcrumb').css('display', 'inline-block');
						$('#chartCustomer [value="' + customer + '"]').addClass('active');
						$('#chartCustomer [value="' + customer + '"]').siblings().removeClass('active');
						$('#chartSelection .showBreadcrumb').css('display', 'inline-block;');
						eventHandler.dropdowns.project.chartProject(customer);
						$('#chartSelection #projectVal').text('Project');
						$('#chartProject').children().addClass('active');
						eventHandler.dropdowns.project.updateChart({
							'onsuccess': 'makeCharts'
						}, 'showManuscript');
						$('.showEmpty').hide();
					}else if(customer.match(/customer/gi) || chartCust == customer){
						$('.showEmpty').hide();
					}
				}else if(customer.match(/customer/gi) ||  	 	chartCust == customer){
					$('.showEmpty').show();
				}
				$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 50) - $('#footerContainer').height()) + 'px');
				$('.la-container').css('display', 'none');
				eventHandler.components.actionitems.initializeAddJob(param);
			},
			showAssignTab: function (param, targetNode, from) {
				$('#search').hide();
				$('.showEmpty').hide();
				$('#autoAssignContent #sort,' + '#autoAssignContent .assign,' + '#autoAssignContent h3').hide()
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				var customerName = '';
				$(currentTab + '.filterOptions').hide();
				$(currentTab + '.progress-container').html('');
				var currentCustomer = $('#autoAssignContent #assignCustomerVal').text();
				var cardCustomer = $('#manuscriptContent #customerVal').text();
				var chartCustomer = $('#chartSelection #customerVal').text();
				if (!cardCustomer.match(/Customer|customer/g)) {
					customerName = cardCustomer;
				} else if (!chartCustomer.match(/Customer|customer/g)) {
					customerName = chartCustomer;
				} else if (!currentCustomer.match(/Customer|customer/g)) {
					customerName = currentCustomer;
				}
				eventHandler.components.actionitems.initializeAddJob({ 'customer': customerName });
				$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 30) - $('#footerContainer').height()) + 'px');
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display Project and Stage');
				$('.showEmpty').show();
				eventHandler.dropdowns.customer.assignCustomer(customerName);
			},

			getCustomerProjects: function(param, targetNode){
				var customer = $(targetNode).val();
				var uploadXMLHTML = ''
				if (customer == '---') return false;
				var projects = projectLists[customer];
				$('#uploadXML_modal .uploadXML .project select').html('')
					for (project in projects.value) {
						uploadXMLHTML += '<option data-customer="' + projects.value[project] + '">' + projects.value[project] + '</option>';
					}
				$('#uploadXML_modal .uploadXML .project select').append(uploadXMLHTML);

			},
			uploadXMLFile: function(param, targetNode){
				var customer = $('#uploadXML_modal .uploadXML .customer select').val();
				var project = $('#uploadXML_modal .uploadXML .project select').val();
				if (customer == '---' || project == '---' || $('#uploadXML_modal .uploadXML .fileData input[type="file"]')[0].files.length == 0){
					$('#uploadXML_modal .uploadXML .validation').text('Please select a customer, a project and upload a file and try again');
					$('#uploadXML_modal .uploadXML .validation').css('color','red');
					return false;
				}
				if ($('#uploadXML_modal .uploadXML .comment #comment_text').val().trim() == ""){
					$('#uploadXML_modal .uploadXML .validation').text('Please provide a reason and try again');
					$('#uploadXML_modal .uploadXML .validation').css('color','red');
					return false;
				}
				$('#uploadXML_modal .uploadXML .validation').css('color','red').css('color','green').text('');
				var xmlFile = $('#uploadXML_modal .uploadXML .fileData input[type="file"]')[0].files[0];
				var parameters = new FormData();
				parameters.append('customer', customer);
				parameters.append('project', project);
				parameters.append('xmlfile', xmlFile);
				parameters.append('comment', $('#uploadXML_modal .uploadXML .comment #comment_text').val());
				$('.la-container').fadeIn();
				$.ajax({
					type: 'POST',
					url: '/api/uploadxml',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						$('.la-container').fadeOut();
						$('#uploadXML_modal .uploadXML .fileData input[type="file"]').val('');
						$('#uploadXML_modal .uploadXML .comment #comment_text').val('')
						$('#uploadXML_modal .uploadXML .validation').text('File upload successfully');
						$('#uploadXML_modal .uploadXML .validation').css('color','green');
					},
					error: function (err) {
						$('.la-container').fadeOut();
						$('#uploadXML_modal .uploadXML .validation').text(err.responseText);
						$('#uploadXML_modal .uploadXML .validation').css('color','red');
					}
				})
			},
			
			getfeedbackreport: function (param, targetNode) {
				$('.la-container').fadeIn();
				var param = {};
				param.customer = 'customer';
				param.project = '*';
				param.fromDate = $(".from-datepicker").val();
				param.toDate = $(".to-datepicker").val();
				if(!param.fromDate || !param.toDate){
				var param = {
					'notify': {
						'notifyTitle': 'DatePicker',
						'notifyText': 'Please enter the date'
					}
				}
				eventHandler.common.functions.displayNotification(param);
			}
				param.version = 'v2.0';
				param.datefield = 'stage.end-date';
				param.time_zone = getTimeZone();
				param.urlToPost = 'getfeedbackreport'
				$.ajax({
					type: "POST",
					url: "/api/getarticlelist",
					data: param,
					success: function (reportList) {
						var input = reportList.map((articles) => {
							return articles.fields.script_score[0];
						})
						var inputs = input.filter(article => Object.keys(article).length > 0);
						var tableContent = [];
						inputs.forEach(function (articles) {
							Object.keys(articles).forEach(function (stage, stageKey) {
								if (typeof (articles[stage]) == 'object') {
									var article = articles[stage];
									if (article.comments && article.comments.match(/(Rt:\s)(\d)\;(RtCm\:\s)(.+?);(.*)/m)) {
										article.rating = article.comments.replace(/(Rt:\s)(\d)\;(RtCm\:\s)(.+?);(.*)/, '$2');
										article.ratingComment = article.comments.replace(/(Rt:\s)(\d)\;(RtCm\:\s)(.+?);(.*)/, '$4');
										article.id = articles.id;
										article.stage = stage;
										tableContent.push(article);
									}
								}
							})
						})
						var pagefn = doT.template(document.getElementById('feedBackList').innerHTML, undefined, undefined);
						$('#feedBackReport_modal .feedBackReport .feedbackTable').replaceWith(pagefn(tableContent));
						$('.la-container').fadeOut()
					},
					error: function (xhr, errorType, exception) {
						$('.la-container').fadeOut()
						return null;
					}
				});
			},
			showsendMails: function (param, targetNode, recursive) {
				$('.la-container').css('display', '');
				$('.showEmpty').hide();
				$('#emailsContent .userDisplay').addClass('hidden')
				$('#emailsContent #rightPanelContainer').css('background-color', '')
				$('#emailsContent #manuscriptsDataContent, #emailsContent #comment-list').html('')
				$('#emailsContent .watermark-text').addClass('hidden')
				$(currentTab + '#userDataContent').html('');
				$('#emailsContent #date').css('display','');
				$('#emailsCustomerVal').text(param.customerName);
				$('#emailsCustomer [value="' + param.customerName + '"]').addClass('active');
				$('#emailsCustomer [value="' + param.customerName + '"]').siblings().removeClass('active');
				var currDate = new Date().toISOString().replace(/T.*/, ' ').replace(/\..+| /, '')
				$("#emailDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (param.fromDate == undefined || param.fromDate == '') {
					param.fromDate = moment().format('YYYY-MM-DD');
					$("#emailDatePicker").val(moment(param.fromDate).format('D MMM, YY'));
				}
				if (param.toDate == undefined || param.toDate == '') {
					param.toDate = moment().format('YYYY-MM-DD');
					$("#emailDatePicker").val(moment(param.toDate).format('D MMM, YY'));
				}
				var param = {
					"customer": param.customerName,
					"from": param.fromDate,
					"to": param.toDate,
					"subject": "*",
				}
				$.ajax({
					type: "POST",
					url: '/api/getemaildata',
					data: param,
					dataType: 'json',
					success: function (respData) {
						if (respData && respData.hits && respData.hits.hits && respData.hits.hits.length > 0) {
							var issueList = respData.hits.hits.map(function (issue) {
								return issue._source;
							})
							var issueHTML = "";
							for (issue of issueList) {
								issueHTML += '<li class="mail-list card" data-customer="' + issue.customer + '" data-id="' + issue["notes-id"] + '" data-project="' + issue.project + '" data-time="' + issue.time + '" data-subject="' + issue.subject + '" data-doi="' + issue.doi + '" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'getMailInfo\',\'param\':{\'customer\':\'' + issue.customer + '\',\'project\':\'' + issue.project + '\',\'doi\':\'' + issue.doi + '\'}}" ><div class="row"><span class="mail-doi col-4">' + issue.doi + '</span><span class="mail-subject col-6">'+issue.subject+'</span><span class="mail-time col-2">' + dateFormat(issue.time+ ' UTC', "hh:MM TT") + '</span></div></li>';
							}
							$('#emailsContent .watermark-text').html('Please click a mail <br/>to view mail content');
							$('#emailsContent #manuscriptsDataContent').html(issueHTML);
							$('#emailsContent .email-msg').addClass('hidden')
							$('#emailsContent .watermark-text').removeClass('hidden')
							$('#emails .issuesList').css('display', 'inline-block');
							$('.la-container').css('display', 'none');
						}else{
							$('.la-container').css('display', 'none');
						}
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
					}
				});
			},
			getMailInfo: function (param, targetNode, recursive) {
				$('.la-container').css('display', '');
				var url = "/api/getdata?doi=#DOI#&project=#PROJECT#&customer=#CUSTOMER#&xpath=//article/production-notes";
				var notesURL = url.replace("#DOI#", param.doi).replace("#CUSTOMER#", param.customer).replace("#PROJECT#", param.project);
				notesURL = notesURL + '&bucketName=AIP';
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						var modalDiv = $('#emailsContent #comment-list').html("");
						if (msg && !msg.error) {
							var commentObj = $(msg);
							var notesDate = '', lastDate = '';
							var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
							var nl = $(commentObj).find('note').length - 1;
							$(commentObj).find('note').each(function (i, v) {
								var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + moment($(this).find('created').html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '">' + moment($(this).find('created').html()+ ' UTC').format('YYYY:MM:DD HH:MM:ss') + '</span><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
								commentDiv.find('*[data-class]').each(function () {
									$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
								});
								commentDiv.find('.jsonresp').each(function () {
									$(this).addClass('toggle');
								})
								// to change email-content to a html structure
								commentDiv.find('email-content').each(function () {
									$(this).find('> *').each(function () {
										var div = $('<div>');
										$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase());
										$(div).html($(this).html());
										$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
									});
									$(this).find(".mail-body p:contains('review_content/?key=')").each(function () {
										$(this).html($(this).html().replace(/(https?:\/\/[a-z\.]+\/review_content\/\?key=[a-z0-9]+)/, '<span class="email-link" data-href="$1">KRIYA-LINK</span>'))
									})
									var div = $('<div>');
									$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase()).addClass('toggle');
									$(div).html($(this).html());
									$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
								})
								if ($(this)[0].hasAttribute('type')) {
									$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="' + $(this).attr('type').toLocaleLowerCase() + '">' + $(this).attr('type').toLocaleUpperCase() + '</span>');
								}
								if ($(this).find('fullname').html() == 'Automation') {
									$(this).attr('type', 'automation');
								}
								$(modalDiv).append(commentDiv);
								$('#emailsContent .watermark-text').addClass('hidden')
								$('#emailsContent .email-msg').removeClass('hidden')
							});
							if($(targetNode).attr('data-id')){
								var id = $(targetNode).attr('data-id');
								$('li.comment[data-id="'+id+'"]').find('.email-content').removeClass('toggle')
								$('li.comment[data-id="'+id+'"]')[0].scrollIntoView({ block: 'end',  behavior: 'smooth' })
								$('.la-container').css('display', 'none');
								$('#emailsContent #rightPanelContainer').css('background-color','white').css('overflow','hidden')
							}
						} else {
							$('.la-container').css('display', 'none');
						}
					},
					error: function (xhr, errorType, exception) {
						$('.la-container').css('display', 'none');
					}
				});

			},
			showUnassigned: function (param, targetNode, recursive) {
				$('.la-container').css('display', '');
				$('.showEmpty').hide();
				$('#autoAssignContent .userDisplay').addClass('hidden')
				$(currentTab + '#userDataContent').html('');
				var date = new Date();
				var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
				var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
				var dataToPost = {
					customer: param.customerName,
					fromDate: dateFormat(firstDay, 'yyyy-mm-dd'),
					toDate: dateFormat(lastDay, 'yyyy-mm-dd'),
					stage: "Pre-editing"
				}
				$('#assignCustomerVal').text(param.customerName);
				$('#assignCustomer [value="' + customer + '"]').addClass('active');
				$('#assignCustomer [value="' + customer + '"]').siblings().removeClass('active');
				var param = {
					customer: param.customerName,
					urlToPost: "getStageUnassigned",
					stage: "Pre-editing",
					excludeUser: ".*com",
					version: "v2.0",
					mainDataXpath: "$..hits",
					time_zone: "+05:30",
					fromDate: param.fromDate,
					toDate: param.toDate,
					storeData: "unAssignData",
					workflowStatus: "in-progress",
					project: 'project'
				}
				//it should not call directly , needs to call by showManuscript
				// after selecting project, list articles
				$('.save-notice').addClass('hide');
				if (!param.storeData) {
					param.storeData = 'cardData';
				}
				window[param.storeData] = [];
				if (param) {
					if ($(targetNode).parent().attr('id') == 'filterStatus') {
						$(targetNode).parent().siblings().html($(targetNode).text());
						$(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					}
					if ($(targetNode).parent().attr('id') == 'filterCustomer') {
						$(currentTab+'#filterStatus').children().removeClass('active');
						$(currentTab+'#filterStatus').children().first().addClass('active');
					}
					if (!param.from || param.from == undefined) {
						param.from = 0;
					}
					if (!param.size || param.size == undefined) {
						param.size = 500;
					}
					if (param.excludeStage) {
						param.excludeStageArticles = excludeStage[param.excludeStage].join(' OR ');
					}
					param.user = 'userName';
					//param.stageName = 'stageName';
					if (!param.customer || param.customer == 'first' || param.customer == 'Customer') {
						try {
							param.customer = Object.keys(JSON.parse($('#userDetails').attr('data')).kuser.kuroles)[0];
						} catch (e) {
							console.log(e);
						}
					}
					if (!param.customer || (param.customer == 'selected' && param.custTag)) {
						try {
							var cs = [];
							$(param.custTag + ' .active').each(function (s) {
								cs.push($(this).text());
							});
							param.customer = cs.join(' OR ');
						} catch (e) {
							console.log(e);
						}
					}
					if (param.customer == 'customer' || selectedCustomer != param.customer) {
						eventHandler.components.actionitems.getUserDetails(param.customer);
					}
					if (!param.project) {
						param.project = 'project';
					}
					$(targetNode).addClass('active');
					$(targetNode).siblings().removeClass('active');
					if (recursive == undefined) {
						data = {};
					}
					if (!param.customer.match(/customer/i)) {
						if (userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['access-level']) {
							var roleType = userData.kuser.kuroles[param.customer]['role-type'];
							var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
							// For Copyeditors login
							if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].access.indexOf(accessLevel) > -1 && param.urlToPost.match(/getUnassigned/gi)) {
								var userDetails;
								$.ajax({
									type: "GET",
									url: "/api/getuserdata?customer=" + param.customer,
									async: false,
									dataType: 'json',
									success: function (respData) {
										if (respData) {
											userDetails = respData.find(function (user) {
												return user.email === userData.kuser.kuemail;
											})
										}
									},
									error: function (respData) {
										$('.la-container').css('display', 'none');
									}
								});
								var userPageCount = userDetails.roles.find(function (role) {
									return role['customer-name'] === param.customer
								})
								// param['pageCount'] = Number(userData.kuser.kuroles[param.customer]['page-limit']) - Number(userData.kuser.kuroles[param.customer]['assigned-pages']);
								param['pageCount'] = Number(userPageCount['page-limit']) - Number(userPageCount['assigned-pages']);
								param['fromWordCount'] = 0;
							}
						}
					}
				}
				if (param.customer && param.customer != 'customer') {
					selectedCustomer = param.customer;
				}
				//To raise query for first time and if we select different customer.
				if (manuScriptCardData.length == 0 || manuScriptCardData.length == undefined || param.stage == 'Support' || recursive == 1) {
					$.ajax({
						type: "POST",
						url: '/api/getarticlelist',
						data: param,
						dataType: 'json',
						success: function (respData) {
							if (!respData || respData.length == 0) {
								$('#manuscriptsDataContent').html('');
								$('#manuscriptContent .filterOptions').hide();
								$('div.navFilter').empty();
								if (selectedCustomer) {
									param = {};
									param.customer = selectedCustomer;
									eventHandler.filters.populateNavigater(param);
								}
								$(currentTab + '#msYtlCount, ' + currentTab + '#msCount').text('0');
								$('.la-container').css('display', 'none');
								$(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
								return;
							}
							var oldTotal = parseInt($(currentTab + '#msYtlCount').text());
							var currCount = respData.hits.length;
							if (oldTotal == NaN || oldTotal == undefined || isNaN(oldTotal) || oldTotal == 0) {
								oldTotal = 0;
								data = {};
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['supportlevel'] = {};
								data.filterObj['others'] = {};
							}
							if (param.stage == 'Support') {
								respData.hits = respData.hits.sort(function (a, b) {
									a = a._source.stageName.toUpperCase();
									b = b._source.stageName.toUpperCase();
									if ((b) < (a)) {
										return 1;
									} else {
										return -1;
									}
								})
								//Array.prototype.push.apply(supportData, respData.hits);
							} else {
								//Array.prototype.push.apply(manuScriptCardData, respData.hits);
							}
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
							var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
							if (!param.fromDate) param.fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
							if (!param.toDate) param.toDate = dateFormat(lastDay, 'yyyy-mm-dd');
							if(!lastMonthList || lastMonthList.length==0){
								var dataToPost = {
									customer: param.customer,
									urlToPost: "getDailyArticleReport",
									stageName: "Pre-editing",
									version: "v2.0",
									mainDataXpath: "$..hits",
									time_zone: "+05:30",
									fromDate: param.fromDate,
									toDate: param.toDate
								}
								lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
							}
							if (!overAllAverage || overAllAverage == undefined || Object.keys(overAllAverage).length == 0) {
								console.log("Overall average calling")
								overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(lastMonthList.hits, param.stage, dataToPost);
							}
							data.dconfig = dashboardConfig;
							var totalMsCount = respData.total;
							data.info = respData.hits;
							data.disableFasttrack = disableFasttrack;
							data.removeEditIcon = false;
							if (param.stage != 'Support' && roleType.match(/copyeditor/g) && accessLevel.match(/vendor/g)) {
								// To disable the action buttons in card for CE vendor
								data.ceVendor = true;
								if (param.urlToPost.match(/getUnassigned/g)) {
									data.removeEditIcon = true;
								}
							}
							data.userDet = JSON.parse($('#userDetails').attr('data'));
							data.count = oldTotal;
							data.overAllAverage = overAllAverage;
							if (param && param.stage != 'Support') {
								var userRole = userData.kuser.kuroles[param.customer]['role-type'];
								var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
								// For Copyeditor -> Vendor In unassigned Bucket the first card should be pickable and other cards are to be in disable mode.
								if (param && param.urlToPost && param.urlToPost.match(/getUnassigned/g) && cardDisable && cardDisable[userRole] && cardDisable[userRole][userAccess] && cardDisable[userRole][userAccess]['addClassName']) {
									data.disable = cardDisable[userRole][userAccess]['addClassName'];
								}
							}
							$(currentTab + '#manuscriptsDataContent').html('');
							manuscriptsData = $(currentTab + '#manuscriptsDataContent');
							var pagefn = doT.template(document.getElementById('unAssignedTemplate').innerHTML, undefined, undefined);
							manuscriptsData.append(pagefn(data));
							// $(currentTab  +'#manuscriptsData').height(window.innerHeight - $(currentTab  +'#manuscriptsData').position().top - $('#footerContainer').height());
							$(currentTab + '.windowHeight').height(window.innerHeight - ($(currentTab + '.layoutView').offset().top + 50) - $('#footerContainer').height());
							$(currentTab + '.windowHeight').css('overflow', 'auto');
							$(currentTab + '.msCount.loading.hidden').removeClass('hidden');
							if ((currCount + oldTotal) < totalMsCount) {
								$(currentTab + '.msCount').addClass('loading').removeClass('loaded');
								$(currentTab + '.la-container').css('display', 'none');
								$(currentTab + '#msYtlCount').text(data.info.length + oldTotal);
								$(currentTab + '#msCount').text(totalMsCount);
								$(currentTab + '#msYtlCount').removeClass('hidden');
								param.from = currCount + oldTotal;
								setTimeout(function () {
									eventHandler.mainMenu.toggle.showUnassigned(param, targetNode, 1);
								}, 0)
							} else {
								$(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
								$(currentTab + '#msCount').text(totalMsCount);
								param.filterObj = data.filterObj;
								if (!param.customer || param.customer == 'first') {
									//eventHandler.filters.populateNavigater(param);
								} else {
									//eventHandler.filters.populateNavigater(param);
								}
								//eventHandler.filters.populateFilters(data.filterObj, param.stage);
								// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
								if (!param.customer.match(/customer|first/gi)) {
									var userRole = userData.kuser.kuroles[param.customer]['role-type'];
									var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
									if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
										//eventHandler.filters.populateSort();
									}
								}
							}
							$('.la-container').css('display', 'none');
						},
						error: function (respData) {
							$('.la-container').css('display', 'none');
						}
					});
				}
			},
			/**
			 *
			 * @param {*} object
			 *
			 * Trigger while click on user Card under assignment tab
			 * select current card as selected user and remove previously selected user
			 * add doi and respective user and article data into global variable assignUserData
			 */
			selectUserToAssign: function(param){
				if(param){
					if($(param.target).closest('li').hasClass("selectedUser")){
						return;
					}
					$('#userDataContent li.selectedUser').removeClass("selectedUser");
					var assignedDoi = $(currentTab + ".highlightCard").attr('id');
					var customer = $(currentTab + ".highlightCard").attr('data-customer');
					var project = $(currentTab + ".highlightCard").attr('data-project');
					var radioValue = param.userName;
					if (radioValue) {
						if ($(currentTab + ".highlightCard").length == 1) {
							$(currentTab + ".highlightCard").addClass('assigned');
							assignUserData[assignedDoi] = { doi: assignedDoi, project: project, customer: customer, assignedto: radioValue };
						}
						$(param.target).closest('li').addClass("selectedUser")
					}
				}
			},
			showMyReport: function (param, targetNode, from) {
				$('.la-container').css('display', 'true');
				var userID = userData.kuser.kuemail;
				var date = new Date();
				var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
				var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
				if (!param.fromDate) param.fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
				if (!param.toDate) param.toDate = dateFormat(lastDay, 'yyyy-mm-dd');
				if(!lastMonthList || lastMonthList.length==0){
					var dataToPost = {
						customer: param.customer,
						urlToPost: "getArticleReport",
						stageName: param.stageName,
						version: "v2.0",
						mainDataXpath: "$..hits",
						time_zone: "+05:30",
						fromDate: param.fromDate,
						toDate: param.toDate
					}
					lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
				}
				if (!overAllAverage || overAllAverage == undefined || Object.keys(overAllAverage).length == 0) {
					console.log("Overall average calling")
					overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(lastMonthList.hits, param.stageName, dataToPost);
				}
				if(!averageUserList || averageUserList ==undefined || averageUserList.length==0){
					averageUserList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getuserdata?customer=' + param.customer, 'GET');
				}
				if(!utilizationReport || utilizationReport ==undefined || Object.keys(utilizationReport).length==0){
					var dataToPost = {
						customer: param.customer,
						urlToPost: "getUtilization",
						stageName: param.stageName,
						version: "v2.0",
						workflowStatus: "in-progress"
					}
					var utilizationReportData = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
					utilizationReport = eventHandler.mainMenu.toggle.getUtilizationData(utilizationReportData.hits)
				}
				var data = {};
				data.info = averageUserList;
				data.overAllAverage = overAllAverage;
				data.config = userAssignAccessRoleStages;
				data.stage = param.stageName;
				data.type = param.type;
				data.customer = param.customer;
				data.project = param.project;
				$(currentTab + '#userDataContent').html('');
				userDataContent = $(currentTab + '#userDataContent');
				var pagefn = doT.template(document.getElementById('unAssignedUserTemplate').innerHTML, undefined, undefined);
				userDataContent.append(pagefn(data));
				if (param.doi && assignUserData && assignUserData[param.doi]) {
					$(currentTab + "#userDataContent li[id='" + assignUserData[param.doi].assignedto + "']").addClass('selectedUser');
				}
				eventHandler.filters.sortlist({ id: "#userDataContent li", attribute: "data-type-average", replace: "#userDataContent" })
				$('#autoAssignContent .userDisplay').removeClass('hidden')
				$('.la-container').css('display', 'none');
			},
			showRecommendedUser: function (param, targetnode, from) {
				$('.la-container').css('display', 'true');
				$('.Recommended').removeClass('hidden')
				
				$(currentTab + '#rightPanelContainer').show()
				
				$(currentTab + 'h2').show()
				var userID = userData.kuser.kuemail;
				var date = new Date();
				var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
                toDate = dateFormat(lastDay, 'yyyy-mm-dd');
				if (!lastMonthList || lastMonthList.length == 0 || lastMonthList.hits == 0) {
					var dataToPost = {
						customer: param.customer,
						urlToPost: "getArticleReport",
						stageName: param.stageName,
						version: "v2.0",
						mainDataXpath: "$..hits",
						time_zone: "+05:30",
						fromDate: fromDate,
						toDate: toDate
					}
					lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
				}
				if (!overAllAverage || overAllAverage == undefined || Object.keys(overAllAverage).length == 0) {
					var dataToPost = {
						customer: param.customer,
						urlToPost: "getArticleReport",
						stageName: param.stageName,
						version: "v2.0",
						mainDataXpath: "$..hits",
						time_zone: "+05:30",
						fromDate: fromDate,
						toDate: toDate
					}
					overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(lastMonthList.hits, param.stageName, dataToPost);
				}
				if (!averageUserList || averageUserList == undefined || averageUserList.length == 0) {
					averageUserList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getuserdata?customer=' + param.customer, 'GET');
				}
				if (!utilizationReport || utilizationReport == undefined || Object.keys(utilizationReport).length == 0) {
					var dataToPost = {
						customer: param.customer,
						urlToPost: "getUtilization",
						stageName: param.stageName,
						version: "v2.0",
						workflowStatus: "in-progress"
					}
					var utilizationReportData = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
					utilizationReport = eventHandler.mainMenu.toggle.getUtilizationData(utilizationReportData.hits)
				}
				if (!recommendedUser)
					var recommendedUser = {}
				for (var usersList in overAllAverage["atypeByuser"]) {
					if (overAllAverage && overAllAverage["atypeByuser"] && overAllAverage["atypeByuser"][usersList] && overAllAverage["atypeByuser"][usersList][param.project] && overAllAverage["atypeByuser"][usersList][param.project][param.type] && overAllAverage["atypeByuser"][usersList][param.project][param.type].average) {
						if (!recommendedUser[usersList]) {
							recommendedUser[usersList] = {}
						}
						recommendedUser[usersList] = overAllAverage["atypeByuser"][usersList][param.project][param.type].average
					}
				}
				var recommendedSort = [];
				for (var recommended in recommendedUser) {
					recommendedSort.push([recommended, recommendedUser[recommended]]);
				}

				recommendedSort.sort(function (a, b) {
					return a[1] - b[1];
				});
				var userSort = recommendedSort.slice(0, 5)
				var objSorted = {}
				userSort.forEach(function (item) {
					objSorted[item[0]] = item[1]
				})
				var userNames = Object.keys(objSorted)
				var data = {};
				data.userName = userNames
				data.info = averageUserList;
				data.overAllAverage = overAllAverage;
				data.config = userAssignAccessRoleStages;
				data.stage = param.stageName;
				data.type = param.type;
				data.customer = param.customer;
				data.project = param.project;
				$(currentTab + '#userDataContent').html('');
				userDataContent = $(currentTab + '#userDataContent');
				var pagefn = doT.template(document.getElementById('RecommendedUserTemplate').innerHTML, undefined, undefined);
				userDataContent.append(pagefn(data));
				if (param.doi && assignUserData && assignUserData[param.doi]) {
					$(currentTab + "#userDataContent li[id='" + assignUserData[param.doi].assignedto + "']").addClass('selectedUser');
				}
				eventHandler.filters.sortlist({ id: "#userDataContent li", attribute: "data-type-average", replace: "#userDataContent" })
				$('#autoAssignContent .userDisplay').removeClass('hidden')
				$('#autoAssignContent #rightPanelContainer').css('display', '')
				$('.la-container').css('display', 'none');
			},
			getUtilizationData: function (utilizationData) {
				var returnedData = $.grep(utilizationData, function (element, index) {
					return element.fields.script_score[0]['articleType'];
				});
				returnedData.forEach(function(data){
					var average = 60;
					if(overAllAverage && overAllAverage.type && overAllAverage.type[data.fields.script_score[0].project] && overAllAverage.type[data.fields.script_score[0].project][data.fields.script_score[0].articleType] && overAllAverage.type[data.fields.script_score[0].project][data.fields.script_score[0].articleType].average){
						average = overAllAverage.type[data.fields.script_score[0].project][data.fields.script_score[0].articleType].average
					}
					if(overAllAverage.user[data.fields.script_score[0].assignedTo]){
						if(!overAllAverage.user[data.fields.script_score[0].assignedTo].util) overAllAverage.user[data.fields.script_score[0].assignedTo].util = 0;
						overAllAverage.user[data.fields.script_score[0].assignedTo].util = overAllAverage.user[data.fields.script_score[0].assignedTo].util + average;
					}
				})
				return utilizationReport = overAllAverage;
			},
			getAverageTime: function (articleData, stagesMatch, dataToPost) {
				var articleLength = articleData.length;
				var averageTimeTaken = 0;
				var dotObj = [];
				var averageArticleType = {};
				var averageUser = {};
				var averageUserType = {};
				if (articleData.length > 0) {
					articleData.forEach(function (currData) {
						if (currData && currData._source && currData._source.stage) {
							var stages = currData._source.stage;
							if (stages.constructor !== Array) {
								stages = [stages];
							}
							[stagesMatch].forEach(function (eachStage) {
								var proofCount = 0;
								var signedOffBy = '';
								var stageEndDate = '';
								var stageFound = false;
								var returnedData = $.grep(stages, function (element, index) {
									return element.name == eachStage;
								});
								for (stageIndex in returnedData) {
									stageIndex = Number(stageIndex);
									var currentStage = returnedData[stageIndex];
									var validDate = moment(currentStage["end-date"] + ' UTC').isBetween(dataToPost.fromDate, dataToPost.toDate, 'days', '[]')
									if (/^(add\s?Job|File\s?download|Convert\s?Files|Support|hold)$/i.test(currentStage.name)) {
										//do nothing
									} else if (validDate) {
										stageFound = true;
										if (currentStage['job-logs'] && currentStage['job-logs'].log) {
											var currentLogs = currentStage['job-logs'].log;
											if (currentLogs.constructor !== Array) {
												currentLogs = [currentLogs];
											}
											var minutesTaken = 0;
											for (eachLog of currentLogs) {
												if (eachLog['start-date'] && eachLog['start-time'] && eachLog['end-date'] && eachLog['end-time']) {
													var startDate = eachLog['start-date'] + ' ' + eachLog['start-time'];
													startDate = new Date(startDate);
													var endDate = eachLog['end-date'] + ' ' + eachLog['end-time'];
													endDate = new Date(endDate);
													//console.log('date ', Math.floor(parseInt((endDate - startDate) / 1000) / 60))
													minutesTaken += parseInt((endDate - startDate) / 1000) / 60;
													//console.log('minutesTaken ', minutesTaken)
													averageTimeTaken += parseInt(Math.floor(parseInt((endDate - startDate) / 1000) / 60));
													//console.log('averageTimeTaken' , averageTimeTaken);
												}
											}
											var signedOffBy = currentStage.assigned.to;
											var project = currData._source.project;
											stageEndDate = moment(currentStage['end-date'] + ' UTC').format('YYYY-MM-DD');
											if (!averageUser[signedOffBy]) averageUser[signedOffBy] = { "count": 0, time: 0 };
											if (!averageArticleType[project]) averageArticleType[project] = {};
											if (!averageArticleType[project][currData._source.articleType]) averageArticleType[project][currData._source.articleType] = { "count": 0, time: 0 };
											if (!averageUserType[signedOffBy]) averageUserType[signedOffBy] = {};
											if (!averageUserType[signedOffBy][project]) averageUserType[signedOffBy][project] = {};
											if (!averageUserType[signedOffBy][project][currData._source.articleType]) averageUserType[signedOffBy][project][currData._source.articleType] = { "count": 0, time: 0 };
											averageUserType[signedOffBy][project][currData._source.articleType].time = parseInt(averageUserType[signedOffBy][project][currData._source.articleType].time) + parseInt(minutesTaken);
											averageUserType[signedOffBy][project][currData._source.articleType].count = parseInt(averageUserType[signedOffBy][project][currData._source.articleType].count) + 1;

											averageUser[signedOffBy].time = parseInt(averageUser[signedOffBy].time) + parseInt(minutesTaken);
											averageUser[signedOffBy].count = parseInt(averageUser[signedOffBy].count) + 1;
											averageArticleType[project][currData._source.articleType].time = parseInt(averageArticleType[project][currData._source.articleType].time) + parseInt(minutesTaken);
											averageArticleType[project][currData._source.articleType].count = parseInt(averageArticleType[project][currData._source.articleType].count) + 1;
										}
										if (currentStage.object && currentStage.object.proof && currentStage.object.proof.pdf) {
											if (currentStage.object.proof.pdf.constructor !== Array) {
												proofCount += Object.keys(currentStage.object.proof.pdf).length;
											} else {
												proofCount += currentStage.object.proof.pdf.length;
											}
										}
									} else if (stageFound && currentStage.name != eachStage) {
										stageFound = false;
										break;
									}
								}
								var hours = Math.floor(minutesTaken / 60);
								var minutes = Math.floor(minutesTaken % 60);
								var objParam = {
									"manuscriptType": currData._source.articleType,
									"date": stageEndDate,
									"doi": currData._source.id,
									"stage": eachStage,
									"hours": hours,
									"minutes": minutes,
									"signedOffBy": signedOffBy,
									"proofCount": proofCount,
									"word-count": currData._source['word-count'],
									"word-count-without-ref": currData._source['word-count-without-ref'],
									"project": currData._source.project,
									"customer": currData._source.customer
								}
								dotObj.push(objParam);
							})
						}
					})
					var usertable = '<table><thead><tr><th>data1</th><th>data1</th><th>data1</th><th>data1</th></tr></thead><tbody>';
					Object.keys(averageUser).forEach(function (cUser) {
						var averageTime = (Math.floor((averageUser[cUser].time / averageUser[cUser].count) / 60)) + ' h ' + (Math.floor((averageUser[cUser].time / averageUser[cUser].count) % 60)) + ' m';
						var average = Math.floor(averageUser[cUser].time / averageUser[cUser].count);
						averageUser[cUser].average = average;
					});
					usertable += '</tbody></table>'
					var userTypeTable = '<table><thead><tr><th>data1</th><th>data1</th><th>data1</th><th>data1</th></tr></thead><tbody>';
					Object.keys(averageUserType).forEach(function (cUser) {
						Object.keys(averageUserType[cUser]).forEach(function (currProj) {
							Object.keys(averageUserType[cUser][currProj]).forEach(function (artaverageUserType) {
								var averageTime = (Math.floor((averageUserType[cUser][currProj][artaverageUserType].time / averageUserType[cUser][currProj][artaverageUserType].count) / 60)) + ' h ' + (Math.floor((averageUserType[cUser][currProj][artaverageUserType].time / averageUserType[cUser][currProj][artaverageUserType].count) % 60)) + ' m';
								var average = Math.floor(averageUserType[cUser][currProj][artaverageUserType].time / averageUserType[cUser][currProj][artaverageUserType].count);
								averageUserType[cUser][currProj][artaverageUserType].average = average;
								userTypeTable += '<tr><td>' + cUser + '</td><td>' + artaverageUserType + '</td><td>' + averageUserType[cUser][currProj][artaverageUserType].count + '</td><td>' + averageUserType[cUser][currProj][artaverageUserType].time + '</td><td>' + average + '</td></tr>'
							})
						})
					})
					userTypeTable += '</tbody></table>'
					var articleTypetable = '<table><thead><tr><th>data1</th><th>data1</th><th>data1</th></tr></thead><tbody>';
					Object.keys(averageArticleType).forEach(function (currProj) {
						Object.keys(averageArticleType[currProj]).forEach(function (cUser) {
							var averageTime = (Math.floor((averageArticleType[currProj][cUser].time / averageArticleType[currProj][cUser].count) / 60)) + ' h ' + (Math.floor((averageArticleType[currProj][cUser].time / averageArticleType[currProj][cUser].count) % 60)) + ' m';
							var average = Math.floor(averageArticleType[currProj][cUser].time / averageArticleType[currProj][cUser].count);
							averageArticleType[currProj][cUser].average = average;
							articleTypetable += '<tr><td>' + cUser + '</td><td>' + averageArticleType[currProj][cUser].count + '</td><td>' + averageArticleType[currProj][cUser].time + '</td><td>' + average + '</td></tr>'
						});
					});
					articleTypetable += '</tbody></table>'
					// console.log('averageTimeTaken ', ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) / 60)) : '0') + ' h ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) % 60)) : '0') + ' m');
					return { data: dotObj, average: ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) / 60)) : '0') + ' h ' + ((articleLength) ? (Math.floor((averageTimeTaken / articleLength) % 60)) : '0') + ' m', user: averageUser, type: averageArticleType, atypeByuser: averageUserType };
				}
			},
			getUserDataReport: function (articleData, stagesMatch, dataToPost) {
				var articleLength = articleData.length;
				var averageTimeTaken = 0;
				var dotObj = [];
				var averageArticleType = {};
				var averageUser = {};
				var doiListCompleted = []
				var doiListPlanned = []
				var averageUserType = {};
				if (articleData.length > 0) {
					articleData.forEach(function (currData) {
						if (currData && currData._source && currData._source.stage) {
							var stages = currData._source.stage;
							var id = currData._id;
							if (stages.constructor !== Array) {
								stages = [stages];
							}
							[stagesMatch].forEach(function (eachStage) {
								var proofCount = 0;
								var signedOffBy = '';
								var stageEndDate = '';
								var stageFound = false;
								var returnedData = $.grep(stages, function (element, index) {
									return element.name == eachStage;
								});
								for (stageIndex in returnedData) {
									stageIndex = Number(stageIndex);
									var currentStage = returnedData[stageIndex];
									var signedOffBy = currentStage.assigned.to;
									var currentComment = currentStage["comments"]
									var validDate = (moment(currentStage["end-date"] + ' UTC').isBetween(dataToPost.fromDate, dataToPost.toDate, 'days', '[]') || moment(currentStage["planned-end-date"] + ' UTC').isBetween(dataToPost.fromDate, dataToPost.toDate, 'days', '[]'))
									if (/^(add\s?Job|File\s?download|Convert\s?Files|Support|hold)$/i.test(currentStage.name)) {
										//do nothing
									}
									else if ((validDate)) {
										if(currentComment && currentComment.match(/.*(Moved to Support)/))
										{
											if(!reportDoiList["userDailyReport"]) reportDoiList["userDailyReport"] = {};
											if (!reportDoiList["userDailyReport"][currentStage.name]) reportDoiList["userDailyReport"][currentStage.name] = {};
											if (!reportDoiList["userDailyReport"][currentStage.name][signedOffBy]) reportDoiList["userDailyReport"][currentStage.name][signedOffBy] = {};
											if (!reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['support']) reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['support'] = [];
												if (id && reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['support']. indexOf(id) == -1)
												reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['support'].push(id);
										}

										stageFound = true;
										
											var project = currData._source.project;
											stageEndDate = moment(currentStage['end-date'] + ' UTC').format('YYYY-MM-DD');
											if(!reportDoiList["userDailyReport"]) reportDoiList["userDailyReport"] = {};
											if (!reportDoiList["userDailyReport"][currentStage.name]) reportDoiList["userDailyReport"][currentStage.name] = {};
											if (!reportDoiList["userDailyReport"][currentStage.name][signedOffBy]) reportDoiList["userDailyReport"][currentStage.name][signedOffBy] = {};
											if (currentStage.status == 'in-progress'  || currentStage.status == 'completed' ) {
												if (!reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['Planned']) reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['Planned'] = [];
												if (id && reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['Planned']. indexOf(id) == -1)
												reportDoiList["userDailyReport"][currentStage.name][signedOffBy]['Planned'].push(id);
											}
												if (currentComment && currentComment.match(/.*(Moved to Support|Moved to Hold)/)) {
												}else if(currentStage.status == 'completed'){
													if (!reportDoiList["userDailyReport"][currentStage.name][signedOffBy][currentStage.status]) reportDoiList["userDailyReport"][currentStage.name][signedOffBy][currentStage.status] = [];
													if (id && reportDoiList["userDailyReport"][currentStage.name][signedOffBy][currentStage.status]. indexOf(id) == -1)
													reportDoiList["userDailyReport"][currentStage.name][signedOffBy][currentStage.status].push(id);
												
											}
										
									} else if (stageFound && currentStage.name != eachStage) {
										stageFound = false;
										break;
									}
								}

							})

						}
					})

					
				}

			},
			showManuscript: function (param, targetNode, from) {
				if($(targetNode).hasClass('active')){
					return true;
				}
				
  			$(currentTab + ".supportCardView").css('display', '');
        $(currentTab + ".supportReportView").css('display', 'none');
				if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')){
					eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
				}
				$('#searchBox').val('');
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display cards');
				$('.showEmpty').hide();
				$('#manuscriptContent > .layoutView').removeClass('hidden');
				$('.issueLogOff').addClass('hidden');
				$('.issuelocked,.issueInactivePage').addClass('hide');
				$('.ulheight').height(window.innerHeight - $('.ulheight').position().top - $('footer').height())
				$('#manuscriptsData').removeData();
				$('.progress-container').html('');
				eventHandler.components.actionitems.hideRightPanel();
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				if(param) $('#manuscriptContent #customerVal').text(param.customer);
				var displayBookFlow = false;
				if ($('#filterCustomer .filter-list').length == 0 && $("#chartCustomer").children('.filter-list').length == 1){
					var customerNode = $("#chartCustomer").children('.filter-list');
					if (customerNode.attr('data-type') == 'book'){
						displayBookFlow = true;
					}
				}
				if (($(targetNode) && ($(targetNode).attr('data-type') == 'book' || $('#manuscriptContent #filterCustomer [value="'+ $('#manuscriptContent #customerVal').text() +'"]').attr('data-type') == 'book')) || displayBookFlow) {
					$('.la-container').css('display', '');
					if ($(targetNode).attr('data-type') == undefined && $('#manuscriptContent #filterCustomer [value="'+ $('#manuscriptContent #customerVal').text() +'"]').attr('data-type') == 'book') {
						var param = {
							'workflowStatus': 'in-progress',
							'urlToPost': 'getAssigned',
							'customer': $('#manuscriptContent #customerVal').text(),
							'custTag': '#filterCustomer',
							'prjTag': '#filterProject',
							'user': 'user',
							'optionforassigning': true,
							'version': 'v2.0'
						}
					}
					$('header .nav-items-journals').addClass('hidden');
					$('header .nav-items-books').removeClass('hidden');
					$('#manuscriptContent .showBreadcrumb').css('display', '');
					$('#manuscriptContent > .layoutView').addClass('hidden');
					$('.ulheight').height(window.innerHeight - $('.ulheight').position().top - $('footer').height())
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .newStatsRow').css('padding-bottom', '7px');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					// $(currentTab + '#manuscriptContent .filterOptions').hide();
					$(currentTab + '.newStatsRow .filterOptions').hide();
					$('#manuscriptContent #customerVal').text(param.customer);
					$('#filterCustomer').children().removeClass('active');
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					manuScriptCardData = [];
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
					eventHandler.flatplan.action.getBooks(param, targetNode);
					eventHandler.filters.populateNavigater(param);
					eventHandler.components.actionitems.initializeAddJob(param);
					$('.showEmpty').hide();
					$(currentTab + '.progress-container').html('');
					$(currentTab+' #manuscriptsData').removeAttr('style');
					return false;
				} else if (param) {
					// if(param.customer && )
					// eventHandler.components.actionitems.getUserDetails(param.customer);
					$('header .nav-items-books').addClass('hidden');
					$('header .nav-items-journals').removeClass('hidden');
					var startTime = new Date().getTime();
					$('.la-container').css('display', '');
					$('.showBreadcrumb').css('display', 'inline-block');
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .newStatsRow').css('padding-bottom', '7px');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					$('#manuscriptContent .filterOptions').show();
					manuScriptCardData = [];
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					setTimeout(function () {
						eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
						$('.assignAccess').hide();
						if (param.optionforassigning) {
							if (!$(targetNode).hasClass('assignAccess')) {
								if (!targetNode) {
									$('#manuscriptContent #statusVal').text('WIP');
									$('#manuscriptContent #filterStatus').children().removeClass('active');
									$('#manuscriptContent #filterStatus').children().first().addClass('active');
								}
								$('#manuscriptContent #assignVal').text('Assigned to me');
								$('#manuscriptContent #assignVal').siblings().children().removeClass('active');
								$('#manuscriptContent #assignVal').siblings().children().first().addClass('active');
							}
							$('.assignAccess').show();
						}
						var contNode = $('#' + $(targetNode).attr('data-href'));
						eventHandler.dropdowns.article.showArticles(param, targetNode);
						eventHandler.components.actionitems.initializeAddJob(param);
						$('.showEmpty').hide();
					}, 0)
				} else {
					$('header .nav-items-books').addClass('hidden');
					$('header .nav-items-journals').removeClass('hidden');
					if (!$('#dashboardContent').hasClass('active') && targetNode.hasClass('active')) {
						$('.la-container').css('display', 'none');
						return;
					}
					$('.la-container').css('display', '');
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#search').show();
					$('#sort').show();
					$('#searchResult').hide();
					$('#searchBar').siblings().val('');
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .filterOptions').hide();
					//$('.dashboardTabs').removeClass('active');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					setTimeout(function () {
						var contNode = $('#' + $(targetNode).attr('data-href'));
						targetNode.addClass('active');
						eventHandler.dropdowns.article.showArticles('', targetNode);
						eventHandler.components.actionitems.initializeAddJob({
							'customer': $('#filterCustomer > .filter-list.active').text()
						});
						$('#manuscriptContent').addClass('active');
						$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
						$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .layoutView').offset().top + 30) - $('#footerContainer').height()) + 'px');
						$('.showEmpty').hide();
					}, 0)

					//$('#sort .fa').remove();
					//$('#sort [sort-option="Doi"]')[0].addClass('active')
					//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
				}
				setTimeout(function () {
					// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
					var customer = $('#manuscriptContent #customerVal').text();
					if (!customer.match(/customer|selected/gi)) {
						$('#manuscriptContent .filterOptions').show();
						$('.showEmpty').hide();
						var userRole = userData.kuser.kuroles[customer]['role-type'];
						var userAccess = userData.kuser.kuroles[customer]['access-level'];
						if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
							eventHandler.filters.populateSort();
						}
					}else{
						$('.msCount.loading').removeClass('loading').addClass('loaded');
						// $('.showEmpty').show();
					}
				}, 0)
				$(currentTab+' #manuscriptsData').removeAttr('style');
				//$('#sort .fa').remove();
				//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
			},
			// Shows support articles.
			showSupport: function (param, targetNode) {
				$('#searchBox').val('');
				eventHandler.components.actionitems.hideRightPanel();
				$('.showEmpty').hide();
				if (!param) {
					return;
				}
				if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')){
					eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
				}
				$('.la-container').css('display', '');
				$('#assignUser').show();
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				$('#search').show();
				$('#searchResult').hide();
				$('#sort').show();
				$('#searchBar').siblings().val('');
				$('#manuscriptContent .supportcard').hide();
				$('#manuscriptContent .filterOptions').show();
				$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
				$('#manuscriptsDataContent').html('');
				$(currentTab + '.layoutView').removeClass('hidden');
				$('div.navFilter').empty();
				supportData = [];
				$('.msCount.loaded').removeClass('loaded').addClass('loading');
				$('#msYtlCount, #msCount').text('0');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				$('#supportReport .supportTabToggle').removeClass('active')
				$('#supportReport #supportCardButton').addClass('active')
				$("#supportReport").removeClass('hidden')
				$(currentTab + ".supportCardView").css('display', '');
        $(currentTab + ".supportReportView").css('display', 'none');
				eventHandler.dropdowns.article.showArticles(param, targetNode);
				eventHandler.components.actionitems.initializeAddJob(param);
				$(currentTab+'.progress-container').html('');
				$(currentTab + '.windowHeight').css('height', (window.innerHeight - ($(currentTab + '.layoutView').offset().top + 30) - $('#footerContainer').height()) + 'px');
			},
			// To display the Issue Page in the dashboard
			showIssues: function (param, targetNode) {
				if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')){
					eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
				}
				$('#search').hide();
				$('.showEmpty').hide();
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				var customerName = '';
				$(currentTab+'.filterOptions').hide();
				$(currentTab+'.progress-container').html('');
				var currentCustomer = $('#issueContent #issueCustomerVal').text();
				var cardCustomer = $('#manuscriptContent #customerVal').text();
				var chartCustomer = $('#chartSelection #customerVal').text();
				if (!cardCustomer.match(/Customer|customer/g)) {
					customerName = cardCustomer;
				} else if (!chartCustomer.match(/Customer|customer/g)) {
					customerName = chartCustomer;
				}else if(!currentCustomer.match(/Customer|customer/g)){
					customerName = currentCustomer;
				}
				eventHandler.components.actionitems.initializeAddJob({'customer': customerName});
				$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 30) - $('#footerContainer').height()) + 'px');
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display Issue');
				$('.showEmpty').show();
				eventHandler.dropdowns.customer.issueCustomer(customerName);
			},
			// To display the mail Page in the dashboard
			showEmails: function (param, targetNode) {
				if ($(targetNode).hasClass('active')) {
					return true;
				} else {
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#search').hide();
				}
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				$(currentTab + '#searchBox').val('');
				$('#emailsContent #manuscriptsDataContent, #emailsContent #comment-list').html('')
				$('#emailsContent #emailsCustomerVal').html('Customer')
				$('#emailsContent .watermark-text').addClass('hidden')
				$('#emailsContent #rightPanelContainer').removeAttr('style')
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display Emails List');
				$('.showEmpty').show();
				if ($(currentTab + '#manuscriptsDataContent li').length > 0) $('.showEmpty').hide();
				$(currentTab + '.ulheight').each(function () {
					$(this).height(window.innerHeight - $(this).position().top - $('footer').height())
				})
				eventHandler.components.actionitems.showRightPanel({ righpanelCol: 5 });
				eventHandler.dropdowns.customer.issueCustomer('Customer', '#emailsSelection #emailsCustomer', { functionName: 'showsendMails', channel: 'mainMenu', topic: 'toggle', event: 'click' });
			},
			//To display the customer articles when refresh is clicked.
			showRefreshed: function (targetNode) {
				eventHandler.components.actionitems.hideRightPanel();
				if (!targetNode) {
					return;
				}
				if ($('#manuscriptContent #customerVal').text() == "Customer" || $('#manuscriptContent #customerVal').text() == "customer") {
					return;
				}
				$('.la-container').css('display', '');
				var param = {};
				if ($('.assignAccess').css('display') == 'none') {
					var assignId = '#statusVal';
				} else {
					var assignId = '#assignVal';
					$('#manuscriptContent .supportcard').show();
				}
				param = JSON.parse(JSON.stringify(refreshButtonConfig[$(targetNode.targetNode).siblings().find(assignId).text().trim()]))
				param.from = 0;
				param.customer = $('#manuscriptContent #customerVal').text();
				if (!(/^(All|Project)$/.test($('#manuscriptContent #projectVal').text()))) {
					param.refreshedProject = $('#manuscriptContent #projectVal').text();
				}

				$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
				$('#manuscriptsDataContent').html('');
				$('div.navFilter').empty();
				manuScriptCardData = [];
				$('.msCount.loaded').removeClass('loaded').addClass('loading');
				$('#msYtlCount, #msCount').text('0');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode.targetNode);
				eventHandler.dropdowns.article.showArticles(param, targetNode.targetNode);
				param.project = param.refreshedProject;
				eventHandler.filters.updateFilters(param);
				//$('#sort .fa').remove();
				//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
			},
			showSearched: function (targetNode, action, event) {
				var searchDoiString = targetNode.value;
				if(searchDoiString.match(/^(-)/)) searchDoiString = searchDoiString.replace(/^(-)/, '');
				if(searchDoiString.match(/(-)$/)) searchDoiString = searchDoiString.replace(/(-)$/, '');
				if(!searchDoiString.match(/^[a-z0-9\.\-\_]+$/i) && searchDoiString.length >= 4){
					var notifyObj = {}
					notifyObj.notify = {};
					notifyObj.notify.notifyTitle = 'Invalid Search';
					notifyObj.notify.notifyText = 'Search string should be Alphanumeric, Dots and Hyphens only';
					notifyObj.notify.notifyType = 'info';
					eventHandler.common.functions.displayNotification(notifyObj);
					return;
				}
				if (searchDoiString.length <= 3 && event && event.inputType && event.inputType.match(/deleteContentBackward|deleteByCut/g)) {
					$('#searchdoi').hide();
					return;
				}
				if (action == 'search' || (event.keyCode == 13 || event.which == 13)) {
					eventHandler.components.actionitems.hideRightPanel();
					if (!targetNode) {
						return;
					}
					if (!targetNode.value) {
						$(targetNode).closest('#search').siblings().find('.active').removeClass('active');
						$(targetNode).closest('#search').siblings().find('#cardView').addClass('active');
						eventHandler.mainMenu.toggle.showManuscript('', $(targetNode).parent().siblings().find('#cardView'));
						return;
					}
					eventHandler.dropdowns.article.getSearchedArticles({
						'doi': (searchDoiString).trim(),
						'customer': 'customer',
						'project': 'project',
						'urlToPost': 'getSearchedArticles'
					});
				} else if (action.match(/oninput/g)) {
					$('#searchdoi').html('');
					if (searchDoiString.length >= 4) {
						$('#searchdoi').show();
						$('#searchdoi').html('<a>Searching...</a>');
						var param = {
							'customer': 'customer',
							'project': '*',
							'searchedWord': searchDoiString,
							'urlToPost': 'getSearchedArticlesDropDown'
						}
						$.ajax({
							type: "POST",
							url: '/api/getarticlelist',
							data: param,
							dataType: 'json',
							success: function (respData) {
								searchedDoiList = [];
								if (respData) {
									var searchDoi = JSONPath({
										json: respData,
										path: '$..doi.buckets'
									})[0];
									for (var doi of searchDoi) {
										searchedDoiList.push(doi.key)
									}
									searchedDoiList.sort();
									addDoi();
								} else {
									addDoi();
								}
							}
						})
					}
				}
				function addDoi() {
					var string = '';
					var count = 0;
					searchedDoiList.filter(function (num) {
						if ((num).match(new RegExp(targetNode.value + '.*', 'g')) && count < 10) {
							string += '<a onclick="eventHandler.dropdowns.article.getSearchedArticles({\'doi\':\'' + num + '\',\'customer\': \'customer\',\'project\': \'project\',\'urlToPost\': \'getSearchedArticles\'});">' + num + '</a>'
							count++;
						}
					});
					(string == '') ? string = '<a>No matches found</a>' : string = string;
					$('#searchdoi').html(string);
					$('#searchdoi').show();
				}
			},
			displaySearched: function (targetnode) {
				$(currentTab + '#manuscriptsDataContent li').hide()
				var value = $(targetnode).val();
				$("#pagin li").remove()
				$(currentTab + '#manuscriptsDataContent li:contains(' + value + ')').css('display', '');
				if (!targetnode.value) {
					eventHandler.mainMenu.toggle.paging()
				}
			},
			toggleReportView: function (targetNode) {
      //To toggle display card view and table view in chart
				console.log($(targetNode).attr('toggleView'));
				var toggleView = 'chart';
				toggleView = $(targetNode).attr('toggleView');
				if(toggleView ==  'table'){
					$('#reportColHideShow').removeClass('hidden');
					$('#chartReportTable').parent().removeClass('hidden');
					$('#cardReportData').addClass('hidden');
					$('#filterExportExcel').removeClass('hidden');
					$(targetNode).attr('toggleView','card').removeClass('fa-table').addClass('fa-list');
					$(targetNode).text(' View Card');
				}else if(toggleView ==  'card'){
					$('#filterExportExcel').addClass('hidden');
					$('#reportColHideShow').addClass('hidden');
					$('#chartReportTable').parent().addClass('hidden');
					$('#cardReportData').removeClass('hidden');
					$(targetNode).attr('toggleView','table').removeClass('fa-list').addClass('fa-table');
					$(targetNode).text(' View Table');
				}

				}
			},
			userLogin: {
				resetPassword: function (param, targetNode) {
					$("#password_modal .error").remove();
					var hasError = false;
					var currentpass = $("#password_modal #oldResetPassword").val();
					var newpass = $("#password_modal #newResetPassword").val();
					var cnfpass = $("#password_modal #confirmResetPassword").val();
					if (currentpass == '') {
						$("#password_modal #oldResetPassword").closest('.controls').after('<span class="error text-danger"><em>Please  enter your current password.</em></span>');
						hasError = true;
					} else if (newpass == '') {
						$("#password_modal #newResetPassword").closest('.controls').after('<span class="error text-danger"><em>Please enter a password.</em></span>');
						hasError = true;
					} else if (cnfpass == '') {
						$("#password_modal #confirmResetPassword").closest('.controls').after('<span class="error text-danger"><em>Please re-enter your password.</em></span>');
						hasError = true;
					} else if (newpass != cnfpass) {
						$("#password_modal #confirmResetPassword").closest('.controls').after('<span class="error text-danger"><em>Password doesn\'t match.</em></span>');
						hasError = true;
					} else if (newpass == currentpass) {
						$("#password_modal #confirmResetPassword").closest('.controls').after('<span class="error text-danger"><em>New password should not be same as old password.</em></span>');
						hasError = true;
					}
					if (hasError == true) {
						return false;
					}
					if (hasError == false) {
						$('.la-container').css('display', '');
						var param = { user: userData.kuser.kuemail, pass: currentpass, resetpassword: newpass, resetconfirmpassword: cnfpass }
						$.ajax({
							type: "POST",
							url: "/api/authenticate",
							data: param,
							success: function (msg) {
								if (msg && msg.redirectToPage) {
									$("#password_modal").modal("hide");
									$('.la-container').css('display', 'none');
									$("#resetConfirmation").modal("show");
									setInterval(eventHandler.mainMenu.userLogin.resetLogoutTimer, 1000);
								} else {
									var error = '';
									if (msg && msg.error && typeof (msg.error) == 'string') error = msg.error;
									$("#password_modal #confirmResetPassword").closest('.controls').after('<span class="error text-danger"><em>' + error + '.</em></span>');
									$('.la-container').css('display', 'none');
								}
							},
							error: function (xhr, errorType, exception) {
								var error = '';
								if (msg && msg.error && typeof (msg.error) == 'string') error = msg.error;
								$("#password_modal #confirmResetPassword").closest('.controls').after('<span class="error text-danger"><em>' + error + '.</em></span>');
								$('.la-container').css('display', 'none');
							}
						});
					}
				},
				resetLogoutTimer: function (param) {
					var minutes = Math.round((resetTimer - 30) / 60);
					var remainingSeconds = resetTimer % 60;
					document.getElementById('countdown').innerHTML = remainingSeconds;
					if (resetTimer == 0) {
						keeplive.logoutKriya()
					} else {
						resetTimer--;
					}
				}

		}
	}
	return eventHandler;
})(eventHandler || {});
eventHandler.common = {
		functions: {
			sendAPIRequest: function (funtionName, parameters, requestType, onSuccessFunc, onError, onProgress, blkUpdateCount, name) {
			if (!funtionName || !requestType) {
				console.log('Error finding Name');
				return;
			}
			$.ajax({
				url: '/api/' + funtionName,
				type: requestType,
				data: parameters,
				xhrFields: {
					onprogress: function (e) {
						if (onProgress) {
							onProgress(e);
						}
					}
				},
				error: function (res) {
					lastCount++;
					if (onError) {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkFailureDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'error';
								parameters.notify.notifyText = parameters.notify.errorText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
						onError(parameters);
					}
				},
				success: function (res) {
					lastCount++;
					parameters.response = res;
					if (typeof (res) == 'string' && res.match('<error>')) {
						if (onError) {
							if (parameters.notify && parameters.notify != undefined) {
								parameters.notify.notifyType = 'error';
								parameters.notify.notifyText = parameters.notify.errorText;
								// eventHandler.common.functions.displayNotification(parameters);
							}
							onError(parameters);
						}
					} else if (typeof (res) == 'object' && res.status != undefined) {
						if (res.status.code != '200') {
							if (onError) {
								if (parameters.notify && parameters.notify != undefined) {
									if (onError == 'conform') {
										blkFailureDoi.push(parameters.doi)
									} else {
										parameters.notify.notifyType = 'error';
										parameters.notify.notifyText = parameters.notify.errorText;
										eventHandler.common.functions.displayNotification(parameters);
									}
								}
								onError(parameters);
							}
						} else {
							if (parameters.notify && parameters.notify != undefined) {
								if (onError == 'conform') {
									blkSuccessDoi.push(parameters.doi)
								} else {
									parameters.notify.notifyType = 'success';
									parameters.notify.notifyText = parameters.notify.successText;
									eventHandler.common.functions.displayNotification(parameters);
								}
							}
							if (onSuccessFunc) {
								onSuccessFunc(parameters);
							}
						}
					} else if (onSuccessFunc) {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkSuccessDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'success';
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
						onSuccessFunc(parameters);
					} else {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkSuccessDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'success';
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
					}
					// For bulk updation to display the all articles in single notification.
					if (onError == 'conform' && blkUpdateCount == lastCount) {
						if (blkSuccessDoi.length) {
							parameters.notify.notifyType = 'success';
							parameters.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText.replace(/{doi}/g, blkSuccessDoi.join(', '));
							if(name == " "){
								parameters.notify.notifyText = parameters.notify.notifyText.replace(/assigned/g, 'unassigned');
							}
							eventHandler.common.functions.displayNotification(parameters);
						}
						if (blkFailureDoi.length) {
							parameters.notify.notifyType = 'error';
							parameters.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText.replace(/{doi}/g, blkFailureDoi.join(', '));
							eventHandler.common.functions.displayNotification(parameters);
						}
						$('.la-container').css('display', 'none');
					}
				},
			});
		},
		/**
		 *
		 * @param {string} url URL to post
		 * @param {object} parameters object to post data
		 * @param {string} requestType POST|GET
		 *
		 * Send AjaxRequest synchronously
		 */
		sendSyncAjaxRequest: function (url, requestType, parameters) {
			if (!url || !requestType) {
				console.log('Error finding Name');
				return;
			}
			var ajaxRequest = $.ajax({
				url: url,
				type: requestType,
				data: parameters,
				async: false,
				error: function (res) {
					return res;
				},
				success: function (res) {
					return res;
				}
			})
			return ajaxRequest.responseJSON;
		},
			writeLog: function (funtionName, parameters, onSuccessFunc, onError, onProgress) {
				$.ajax({
					url: '/api/' + funtionName,
					type: 'POST',
					data: parameters,
					xhrFields: {
						onprogress: function (e) {
							if (onProgress) {
								onProgress(e);
							}
						}
					},
					error: function (res) {
						if (onError) {
							onError(res);
						}
					},
					success: function (res) {
						if (onSuccessFunc) {
							onSuccessFunc(res);
						}
					},
				});
			},
			displayNotification: function (param) {
			if (param.notify) {
				if (!param.hideNotify) PNotify.removeAll();
				if (param.notify.confirmNotify) {
					new PNotify({
						title: param.notify.notifyTitle,
						text: param.notify.notifyText,
						type: param.notify.notifyType,
						functionToCall: forceLogOutFunction[param.notify.functionToCall],
						hide: false,
						parameters: param.parameter,
						icon: 'fa fa-question-circle',
						confirm: {
							confirm: true,
							buttons: [{
								text: 'Yes',
								click: function (param) {
									eval(param.options.functionToCall)(param.options.parameters);
									PNotify.removeAll();
								}
							},
							{
								text: 'No',
								click: function () {
									PNotify.removeAll();
								}
							}]
						}
					});
				} else {
					new PNotify({
						title: param.notify.notifyTitle,
						text: param.notify.notifyText,
						type: param.notify.notifyType,
						hide: false,
					});
				}
			} else {
				new PNotify({
					title: 'Error',
					text: 'Unable to process',
					type: 'error',
					hide: false
				});
			}
		},
			getUserData: function (customer) {
				usersArr = {};
				jQuery.ajax({
					type: "GET",
					url: "/api/getuserdata?customer=" + customer,
					success: function (msg) {
						var usersDiv = $(msg);
						var users = $('<select class="assign-users" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"/>');
						$(usersDiv).find('user').each(function (i, v) {
							users.append('<option value="' + $(this).find('first').text() + '">' + $(this).find('first').text() + '</option>');
						});
						usersArr.users = users;
					}
				})
			},
			getUserList: function () {
				//Added by Anuraja to get all users list
				jQuery.ajax({
					type: "GET",
					url: '/api/getuserlist',
					success: function (respData) {
						if (!respData || respData.length == 0) {
							$('.la-container').css('display', 'none');
							return;
						}
						var usersDiv = $(respData);
						$(usersDiv).find('user').each(function (i, v) {
							var userInfo = {};
							userInfo.name = $(this).find('first').text();
							$(this).find('role').each(function (i, v) {
								if (!usersLists[$(this).attr('customer-name')]) {
									usersLists[$(this).attr('customer-name')] = [];
								}
								userInfo.role = $(this).attr('role-type');
								usersLists[$(this).attr('customer-name')].push(userInfo);
							})
						});
						// console.log(usersList);
						$('.la-container').css('display', 'none');
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				});
			},
			pickDateNew: function (id, type, cardID, source, replaceCard, event) {
				$('.datepicker-inline').parent().hide();
				if (!source) {
					source = 'data-id';
				}
				if ($("[id='blkdate" + type + id + "']").length > 0) {
					$("[id='blkdate" + type + id + "']").attr('data-old-value', $("[id='blkdate" + type + id + "']").html());
				} else {
					$("[id='date" + type + id + "']").attr('data-old-value', $("[id='date" + type + id + "']").html());
				}
				var timeToSend = {
					time: '',
					date: ''
				}
				var timeToSet;

				var datePicker = $("[id='blkpickDate" + type + id + "']").datepicker({
					timepicker: true,
					todayButton: true,
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						today: 'Today',
						clear: 'Clear',
						dateFormat: 'dd.mm.yyyy',
						timeFormat: 'hh:ii',
						firstDay: 1
					},
					onShow: function (dp, animationCompleted) {
						if (!animationCompleted) {

						} else {

						}
					},
					onHide: function (dp, animationCompleted) {
						if (!animationCompleted) {

						} else {}
					},
					onSelect: function (formattedDate, date, inst) {
						timeToSend.date = formattedDate.slice(0, 10);
						timeToSend.time = formattedDate.slice(11, 16);
						timeToSet = date.toString().slice(4, 15);
					}
				})
				datePicker.show();
				var Dtop = $("[id='blkpickDate" + type + id + "']").position().top;
			if (Dtop + $("[id='blkpickDate" + type + id + "'] .datepicker-inline").height() < $('.modal.show').height()) {
			} else {
				Dtop = Dtop - (( Dtop + $("[id='blkpickDate" + type + id + "'] .datepicker-inline").height()) - $('.modal.show').height()) - 100;
			}
			$("[id='blkpickDate" + type + id + "'] .datepicker-inline").css('top', Dtop)
				if ($("[id='blkdate" + type + id + "']").length > 0) {
					var inputDate = new Date($("[id='blkdate" + type + id + "']").html());
				} else {
					var inputDate = new Date($("[id='date" + type + id + "']").html());
				}
				if (inputDate == 'Invalid Date') {
					var currentDate = new Date();
					datePicker.data('datepicker').selectDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()))
				} else {
					datePicker.data('datepicker').selectDate(inputDate)
				}
				$('.datepicker--button').text('CONFIRM');
				$('.datepicker--button').off('click');
				$('.datepicker--button').on('click', function () {
					datePicker.hide();
					if ($(this).closest('[data-on-success]').length > 0) {
						var onSuccess = $(this).closest('[data-on-success]').attr('data-on-success');
						if (typeof (window[onSuccess]) == "function") {
							if (datePicker.data('datepicker').selectedDates.length > 0) {
								window[onSuccess](datePicker.attr('id'), 'yes', cardID, source, replaceCard, '', id);
							} else {
								window[onSuccess](datePicker.attr('id'), 'delete', cardID, source, replaceCard, '', id);
							}
						} else if (typeof (eval(onSuccess)) == "function") {
							eval(onSuccess)(datePicker.attr('id'), 'yes', cardID, source, replaceCard, '', id);
						}

					}
				});
				event.preventDefault();
				event.stopPropagation();
			},
			makeChart: function (param) {
				var val = $(param.val).val();
				data = {};
				data.project = val;
				eventHandler.highChart[param.functionName](data);
				//Call Highchart to create chart
			},
			getOverDue: function (param) {}
		}
	},
	eventHandler.components = {
		actionitems: {
			undo: function (param, targetNode) {
				console.log(param)
			},
			moreActions: function (param, targetNode) {
				$(targetNode).parent().parent().find('.moreActionsBtns').toggleClass('hidden');
			},
			showAuthors: function (param, targetNode) {
				$('#authorList').modal();
				console.log(param)
			},
			toggleManuscriptLayout: function (param) {
				var data = {};
				data.filterObj = {};
				data.filterObj['customer'] = {};
				data.filterObj['project'] = {};
				data.filterObj['articleType'] = {};
				data.filterObj['typesetter'] = {};
				data.filterObj['publisher'] = {};
				data.filterObj['author'] = {};
				data.filterObj['copyeditor'] = {};
				data.filterObj['editor'] = {};
				data.filterObj['others'] = {};
				data.dconfig = dashboardConfig;
				data.info = cardData;
				data.disableFasttrack = disableFasttrack;
				data.count = 0;
				data.userDet = JSON.parse($('#userDetails[data]').attr('data'));
				manuscriptsData = $('#manuscriptsDataContent');
				var pagefn = doT.template(document.getElementById(param.type + 'Template').innerHTML, undefined, undefined);
				manuscriptsData.html(pagefn(data));
				$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
			},
			openPDF: function (param, targetNode) {
				console.log(param)
			},
			changeFileName: function(param, targetNode){
				if ($(targetNode).length > 0 && $(targetNode)[0].files && $(targetNode)[0].files.length > 0){
					var fileName = $(targetNode)[0].files[0].name;
					$(targetNode).parent().find('span').text(fileName);
				}
			},
			removeFileFromList: function(param, targetNode){
				/* Remove file option */
				var fileHolder = $(targetNode).closest('.file-holder');
				$(targetNode).closest('.row.file-list').remove();
				fileHolder.find('.row.file-list').last().find('.addFile').removeClass('disabled');
			},
			hideChapterNumber: function(param, targetNode){
				var selValue = $(targetNode).closest('select').val();
				if(selValue == "Appendix"){
					$(targetNode).closest('#backMatter').find('.doiNumber').removeClass('hidden')
					$(targetNode).closest('#backMatter').find('.doiNumber input').attr('data-required','required').attr('required', 'required').attr('min', '1').attr('data-validate','true').addClass('input-sm')
				}else{
					$(targetNode).closest('#backMatter').find('.doiNumber').addClass('hidden')
					$(targetNode).closest('#backMatter').find('.doiNumber input').removeAttr('data-required required min data-validate').removeClass('input-sm')
				}
			},
			addNewFileList: function(param, targetNode){
				/* Add another file option */
				var fileHolder = $(targetNode).closest('.file-holder');
				var newFileList = $(targetNode).closest('.row.file-list').clone(true);
				newFileList.find('input')[0].value = "";
				newFileList.find('input').parent().find('span').text('Browse');
				newFileList.find('.removeFile').removeClass('disabled');
				newFileList.find('.is-invalid').removeClass('is-invalid');
				targetNode.addClass('disabled');
				fileHolder.find('.row.file-list').last().after(newFileList);
			},
			initializeAddJob: function (param) {
				$('#addJobBtn,#submitJobBtn,#addBookBtn,#exportEpubBtn,#addIssueBtn,.issueLogOff').addClass('hidden');
				$('#submitJobBtn').addClass('hidden');
				$('.issuelocked,.issueInactivePage').addClass('hide');
				if (!param || !param.customer || param.customer.match(/customer/gi)) return false;
				var currentTab = $('header .nav-link.active').attr('id');
				//var customers = '^(' + addJobConfig.customer.join('|') + ')$';
				//var matchCustomer = new RegExp(customers, 'i');
				var customers = addJobConfig.customer[param.customer];
				var roleType = userData.kuser.kuroles[param.customer]['role-type'];
				var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
				if((addJobConfig.tabID && addJobConfig.tabID.match(currentTab)) || (addJobConfig.tabID == 'all')){
				var matchCustomerRole = false;
				if (customers){
					matchCustomerRole = new RegExp(customers, 'i');
				}
				var addJobButton = false;
				if (param && param.customer && customers && customers == 'all'){
					$('#addJobBtn').removeClass('hidden');
				}else if (param && param.customer && customers && userData && userData.kuser && userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['role-type'].match(matchCustomerRole)){
					$('#addJobBtn').removeClass('hidden');
				}
				}
				if((addBookConfig.tabID && addBookConfig.tabID.match(currentTab)) || (addBookConfig.tabID == 'all')){
				var customers = addBookConfig.customer[param.customer];
				var matchCustomerRole = false;
				if (customers){
					matchCustomerRole = new RegExp(customers, 'i');
				}
				var addJobButton = false;
				if (param && param.customer && customers && customers == 'all'){
					$('#addBookBtn').removeClass('hidden');
				}else if (param && param.customer && customers && userData && userData.kuser && userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['role-type'].match(matchCustomerRole)){
					$('#addBookBtn').removeClass('hidden');
				}
				//var sjCustomers = '^(' + submitJobConfig.customer.join('|') + ')$';
				//var matchSJCustomer = new RegExp(sjCustomers, 'i');
				/*if ($('#cardView').is('.active') && param && param.customer && addJobConfig && addJobConfig.customer && param.customer.match(matchCustomer)) {
					$('#addJobBtn').removeClass('hidden');
				} else */
				if ($('#cardView').is('.active') && param && param.customer && submitJobConfig && submitJobConfig[param.customer] && submitJobConfig[param.customer][roleType] && submitJobConfig[param.customer][roleType].indexOf(accessLevel) == -1) {
					$('#submitJobBtn').removeClass('hidden');
				}
			}
			if((addIssueConfig.tabID && addIssueConfig.tabID.match(currentTab)) || (addIssueConfig.tabID == 'all')){
				var customers = addIssueConfig.customer[param.customer];
				var matchCustomerRole = false;
				if (customers) {
					matchCustomerRole = new RegExp(customers, 'i');
				}
				var addJobButton = false;
				if (param && param.customer && customers && customers == 'all') {
					$('#addIssueBtn').removeClass('hidden');
					// } else if (param && param.customer && customers && userData && userData.kuser && userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['role-type'].match(matchCustomerRole)) {
				} else if (param && param.customer) {
					$('#addIssueBtn').removeClass('hidden');
				} else {
					return;
				}
				}
			},
			openSubmitJob: function (param) {
				/* Open submit job modal */
				var clientList = '';
				if (!param) var param = {};
				if (!param.customer) param.customer = $('#filterCustomer > .filter-list.active').text();
				clientList += '<option data-value="' + param.customer + '" value="' + param.customer + '">' + param.customer.toLocaleUpperCase() + '</option>';
				$('#submitJob').modal();
				$('#submitJob select[id="ajcustomer"]').html(clientList);
				$('#submitJob #multipleUpload').prop("checked", false);
				$('#submitJob [data-type="single"]').removeClass('hidden');
				$('#submitJob select[id="ajcustomer"]').trigger('click');
				$('#submitJob').find('.file-list').not(":first").remove();
				$('#submitJob input').val('');
				$('#submitJob input').parent().find('span').text('Browse');
				$('#submitJob').find('.file-list').find('[data-mandatory]:not(:first)').each(function(){
					var mandatoryVal = $(this).val();
					var row = $(this).closest('.file-list');
					var clone = $(row).clone();
					$(clone).find('.removeFile').removeClass('disabled');
					$(clone).find('select').val(mandatoryVal);
					$('#submitJob .file-holder .file-list:last').after(clone);
				});
				$('#submitJob').find('.row.file-list').find('.addFile').addClass('disabled');
				$('#submitJob').find('.row.file-list').last().find('.addFile').removeClass('disabled');
			},
			openAddJob: function (param, targetNode) {
				//var userObj = JSON.parse($('#userDetails[data]').attr('data'));
				var clientList = '';
				if (!param) var param = {};
				if (!param.customer) param.customer = $('#filterCustomer > .filter-list.active').text();
				var clientType = $('#filterCustomer > .filter-list.active').attr('data-type');
				if (clientType == 'book'){
					modal = 'addChapter';
				}else{
					modal = 'addJob';
				}
				clientList += '<option data-value="' + param.customer + '" value="' + param.customer + '">' + param.customer.toLocaleUpperCase() + '</option>';
				$('#' + modal + ' [data-customer]').addClass('hidden');
				$('#' + modal + ' [data-skip-customer]').removeClass('hidden');
				$('#' + modal + ' [data-customer*=" ' + param.customer + ' "]').removeClass('hidden');
				$('#' + modal + ' [data-skip-customer*=" ' + param.customer + ' "]').addClass('hidden');
				/*Object.keys(userObj.kuser.kuroles).forEach(function (key, index) {
					clientList += '<option data-value="' + key + '" value="' + key + '">' + key.toLocaleUpperCase() + '</option>';
				});*/
				$('#' + modal + ' select[id="ajcustomer"]').html(clientList);
				$('#' + modal + ' #multipleUpload').prop("checked", false);
				$('#' + modal + ' [data-type="single"]').removeClass('hidden');
				$('#' + modal + ' select[id="ajcustomer"]').trigger('click');
				$('#' + modal + ' input').val('');
				$('#' + modal + ' input').removeClass('is-invalid');
				$('#' + modal + '').modal();
			},
			addJobMultipleUpload: function (param, targetNode) {
				if (targetNode.is(":checked")) {
					$('#addJob [data-type="single"]').addClass('hidden');
				} else {
					$('#addJob [data-type="single"]').removeClass('hidden');
				}
			},
			addJob: function (param, targetNode) {
				var modal = '#' + param.modal;
				var validInput = true;
				$(modal + " [data-required]").each(function(){
					if ($(this).closest('.hidden').length == 0){
						$(this).removeClass('is-invalid');
						if ($(this).val() == "") {
							$(this).addClass('is-invalid');
							validInput = false;
						}
						if ($(this).attr('data-pattern')){
							var regexp = new RegExp($(this).attr('data-pattern'));
							if (! $(this).val().match(regexp)){
								$(this).addClass('is-invalid');
								validInput = false;
							}
						}
					}
				});
				if (! validInput){
					return false;
				}
				$('.la-container').css('display', '');
				var parameters = new FormData();
				parameters.append('customer', $(modal + " [id='ajcustomer']").val());
				parameters.append('project', $(modal + " [id='ajproject']").val());
				if ($('#addJob #multipleUpload').is(":checked")) {
					parameters.append('multiple', 'true');
				} else {
					if ($(modal + " input#ajdoi").val() == "") {
						$('.la-container').css('display', 'none');
						return false;
					}
					parameters.append('doi', $(modal + " input#ajdoi").val());
					parameters.append('articleTitle', $(modal + " input[id='ajtitle']").val());
					parameters.append('articleType', $(modal + " *[id='ajtype']").val());
				}
				$(modal + " input#manuscriptFile").removeClass('is-invalid');
				if ($(modal + " input#manuscriptFile")[0] && $(modal + " input#manuscriptFile")[0].files && $(modal + " input#manuscriptFile")[0].files.length > 0) {
					parameters.append('manuscript', $(modal + " input#manuscriptFile")[0].files[0]);
				}else if ($(modal + " input#manuscriptFile")[0] && $(modal + " input#manuscriptFile")[0].files && $(modal + " input#manuscriptFile")[0].files.length == 0) {
					$(modal + " input#manuscriptFile").addClass('is-invalid');
					$('.la-container').css('display', 'none');
					return false;
				}
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						$('#addJob').modal('hide');
						parameters.notify.notifyText = parameters.notify.successText;
						eventHandler.common.functions.displayNotification(parameters);
						$('.la-container').css('display', 'none');
					},
					error: function () {
						$('#addJob').modal('hide');
						parameters.notify.notifyText = parameters.notify.errorText;
						eventHandler.common.functions.displayNotification(parameters);
						$('.la-container').css('display', 'none');
					}
				})
				console.log(parameters);
			},
			submitJob: function (param, targetNode) {
				var modal = '#' + param.modal;
				var parameters = new FormData();
				parameters.append('customer', $(modal + " [id='ajcustomer']").val());
				parameters.append('project', $(modal + " [id='ajproject']").val());
				var validInput = true;
				$(modal + " [data-required]").each(function(){
					$(this).removeClass('is-invalid');
					if ($(this).val() == "") {
						$(this).addClass('is-invalid');
						validInput = false;
					}
					if ($(this).attr('data-pattern')){
						var regexp = new RegExp($(this).attr('data-pattern'));
						if (! $(this).val().match(regexp)){
							$(this).addClass('is-invalid');
							validInput = false;
						}
					}
				});
				if (! validInput){
					return false;
				}
				parameters.append('doi', $(modal + " input[id='ajdoi']").val());
				parameters.append('articleTitle', $(modal + " input[id='ajtitle']").val());
				parameters.append('addTitle', 'true');
				parameters.append('articleType', $(modal + " *[id='ajtype']").val());
				parameters.append('authorName', $(modal + " input[id='cauthor']").val());
				parameters.append('authorEmail', $(modal + " input[id='cemail']").val());
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify){
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				/* ZIP all uploaded files into a zip along with a manifest file */
				var zip = new JSZip();
				var files = $('.file-holder .file-list input');
				var fileList = '<files>';
				var manuscriptCount = 0;
				var validInput = true;
				var mustHaveFiles = [];
				$(modal + ' .file-holder .file-designation:first').find('option').each(function(){
					if ($(this)[0].hasAttribute('data-mandatory')){
						mustHaveFiles.push($(this).text());
					}
				});
				for (var f = 0, fl = files.length; f < fl; f++){
					var currFile = files[f];
					$(files[f]).removeClass('is-invalid');
					if (currFile.files.length == 0){
						$(files[f]).addClass('is-invalid');
						validInput = false;
					}
					var fileType = $(currFile).closest('.row').find('.file-designation').val();
					var fileTypeValue = $(currFile).closest('.row').find('.file-designation option:selected').text()
					var mindex = mustHaveFiles.indexOf(fileTypeValue);
					if (mindex > -1) {
						mustHaveFiles.splice(fileTypeValue, 1);
					}
					$(currFile).closest('.row').find('.file-designation').removeClass('is-invalid');
					if (fileType == 'manuscript'){
						if (manuscriptCount > 0){
							$(currFile).closest('.row').find('.file-designation').addClass('is-invalid');
							validInput = false;
						}
						manuscriptCount++;
					}
					if (currFile.files.length > 0){
						fileList += '<file file_name="' + currFile.files[0].name + '" file_size="' + currFile.files[0].size + '" file_designation="' + fileType + '"/>'
						zip.file(currFile.files[0].name, currFile.files[0]);
					}
				}
				fileList += '</files>';
				if (! validInput){
					return false;
				}
				if (mustHaveFiles.length > 0){
					$.each(mustHaveFiles, function (k, v) {

					});
					var notifyObj = {}
					notifyObj.notify = {};
					notifyObj.notify.notifyTitle = 'Files missing';
					notifyObj.notify.notifyText = 'Please upload the following file type : ' + mustHaveFiles.join(', ');
					notifyObj.notify.notifyType = 'info';
					eventHandler.common.functions.displayNotification(notifyObj);
					return false;
				}

				zip.file('kriya_job_manifest.xml', fileList);
				$('.la-container').css('display', '');
				zip.generateAsync({type:"blob"})
					.then(function(content) {
						var fileName = new Date().getTime() + '.zip';
						parameters.append('manuscript', content, fileName);
						$.ajax({
							type: 'POST',
							url: '/api/addjob',
							data: parameters,
							contentType: false,
							processData: false,
							success: function (response) {
								$('#submitJob').modal('hide');
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
								$('.la-container').css('display', 'none');
							},
							error: function () {
								$('#submitJob').modal('hide');
								parameters.notify.notifyText = parameters.notify.errorText;
								eventHandler.common.functions.displayNotification(parameters);
								$('.la-container').css('display', 'none');
							}
						})
					})
					.catch(function(err){
						console.log(err)
					})
			},
			/**
			 * to get resources from cloud and display as files and folder
			 **/
			getResource: function (param, targetNode) {
				if (!param.doi || !param.customer || !param.project) {
					return;
				}
				var parameters = 'customer=' + param.customer + '&project=' + param.project + '&doi=' + param.doi + '&process=new';
				$('.la-container').fadeIn();
				$.ajax({
					type: "GET",
					url: "/api/listbucket/?" + parameters,
					success: function (data) {
						$('.filemanager').find('.data').remove();
						$('.filemanager').find('.nav-wrapper').html('<div class="breadcrumb"><a href="javascript:;" class="breadcrumb-item">' + param.doi + '</a></div>');
						$('.filemanager').append(data);
						var userRole = userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'];
						var userAccess = userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['access-level'];
						if (resourceTab[userRole] && resourceTab[userRole][userAccess] && resourceTab[userRole][userAccess]) {
							var jqueryString = '';
							for (type of resourceTab[userRole][userAccess]) {
								jqueryString = '[data-name="' + type + '"],'
							}
							jqueryString = jqueryString.replace(/,$/g, '');
							$('.filemanager > ul > li:not(' + jqueryString + ')').hide();
						}
						$('.la-container').fadeOut();
						$('#fileBrowser').modal();
					},
					error: function (err) {
						console.log(err);
						$('.la-container').fadeOut();
					}
				});
			},
			showHistory: function (param, targetNode) {
				$('#articleHistory table tr.stages').remove();
				$('#historyDoi').text('article');
				//$('#articleHistory').attr('data', '');
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(msCard).attr('data-id');
				var storeData = window[$(msCard).attr('data-store-variable')];
				var parentId = $(msCard).attr('data-dashboardview');
				var dataID = 'data-id';
				if(parentId=='#cardReportData'){
					dataID = 'data-report-id';
				}
				var sourceData ;
				if(storeData && storeData[$(msCard).attr('data-id')] && storeData[$(msCard).attr('data-id')]._source){
					sourceData = storeData[$(msCard).attr('data-id')]._source;
				}
				//$('#articleHistory').attr('data', $(msCard).attr('data').replace(/\'/g, '"'));
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var stages = sourceData.stage;
				var collapseRow = '', hideCompletedRow = '';
				var hideRowId = 0;
				for (var s = 0, sl = $(stages).length; s < sl; s++) {
					var stage = stages[s] ? stages[s] : stages;
					var doi = sourceData.id.replace('.xml', '').replace('\.', '');
					var inprogressDayDiff = Math.floor(dateDistance(stage['sla-start-date'], moment().utc().format('YYYY-MM-DD HH:mm:ss'), daysForCalculation.holidays['default']));
					inprogressDayDiff = (inprogressDayDiff < 0) ? 0 : inprogressDayDiff;
					var stageRow = '';
					$('#historyDoi').text(sourceData.id.replace('.xml', ''));
					if (stage.status != 'in-progress' && stage.status != 'waiting') {
						var tempSlaStartDate = stage['sla-start-date'] ? moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempSlaEndDate = stage['sla-end-date'] ? moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaStartDate = stage['planned-start-date'] ? moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaEndDate = stage['planned-end-date'] ? moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var stageDuration = ((stage['stageduration-days'] != undefined) ? stage['stageduration-days'] : '');
						// var stageAssignedName = ((stage.assigned) ? ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) : '-');
						var stageAssignedName = (stage.assigned && stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : 'Unassigned';
						if (hideRepeatedStage.default.includes(stage.name)) {
							collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>' + collapseRow;
							continue;
						} else {
							if (stages.length != s + 1 && stages[s + 1].status == 'completed' && hideRepeatedStage.default.includes(stages[s + 1].name)) {
								collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>' + collapseRow;
								continue;
							} else {
								if (collapseRow) {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>'
								} else {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>'
								}
							}
						}
					}
					if (collapseRow || hideCompletedRow) {
						var hideRow = '';

						if (hideCompletedRow) {
							hideRow += hideCompletedRow;
						}
						if (collapseRow) {
							hideRow += collapseRow;
						}
						stageRow = $(hideRow);
						collapseRow = hideCompletedRow = '';
						$('#articleHistory table tbody').prepend(stageRow);
					}
					if (stage.status == 'in-progress') {
						stageRow = $('<tr class="stages" data-stage-name="' + stage.name + '"/>');
						usersHTML = "";
						usersArr = {};
						var users;
						var assignUserRole = userData.kuser.kuroles[sourceData.customer]['role-type'];
						var assignUserAccess = userData.kuser.kuroles[sourceData.customer]['access-level'];
						if (userAssignAccessRoleStages && userAssignAccessRoleStages[assignUserRole] && userAssignAccessRoleStages[assignUserRole][assignUserAccess] && !userAssignAccessRoleStages[assignUserRole][assignUserAccess].includes(stage.name)) {
							if (stage.assigned.to) {
								users = $('<span>' + stage.assigned.to + '</span>');
							} else {
								users = $('<span>Unassigned</span>');
							}
						} else {
							users = $('<select class="assign-users" onchange="eventHandler.components.actionitems.assignUser(this,' + cardID + ', \'' + dataID + '\', true, \'\', \'\', true, \'\',\'\',\'' + parentId + '\')" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"/>');
							// $(usersDetailsDiv).find('user').each(function (i, v) {
							// 	users.append('<option value="' + $(this).find('first').text() + ', ' + $(this).find('email').text() + '">' + $(this).find('first').text() + '</option>');
							// });
							if (usersList && usersList.users) {
								Object.keys(usersList.users).forEach(function (count, q) {
									var skillLevel = (usersList.users[count].additionalDetails && usersList.users[count].additionalDetails['skill-level']) ? usersList.users[count].additionalDetails['skill-level'] : "";
									// To show only particular users for assigning the articles for particular stage based on user role-type and access-level
									if (showAssignUserName[assignUserRole] && showAssignUserName[assignUserRole][assignUserAccess]) {
										var role = usersList.users[count].additionalDetails['role-type'];
										var access = usersList.users[count].additionalDetails['access-level'];
										if (showAssignUserName[assignUserRole][assignUserAccess][stage.name] && showAssignUserName[assignUserRole][assignUserAccess][stage.name].roleType.indexOf(role) > -1 && showAssignUserName[assignUserRole][assignUserAccess][stage.name].accessLevel.indexOf(access) > -1) {
											users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
										}
									} else {
										users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
									}
								})
							}
							if (users.find('option[value="' + stage.assigned.to + '"]').length > 0) {
								users.find('option[value="' + stage.assigned.to + '"]').attr('selected', true);
								users.append('<option value="' + " " + '">Unassign</option>');
							} else {
								if (stage.assigned.to == null) {
									users.append('<option value="' + " " + '" selected>Unassigned</option>');
								} else {
									users.append('<option value="' + stage.assigned.to + '"  selected>' + stage.assigned.to + '</option>');
									users.append('<option value="' + " " + '">Unassign</option>');
								}
							}
						}
						usersHTML = $(users)[0].outerHTML;
						//update by prasana
						if ($('#articleHistory table tbody tr').hasClass('hideRow' + hideRowId)) {
							stageRow.html('<td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td><span>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '" id="blkdateEnd' + doi + '">' + moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						} else {
							stageRow.html('<td></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td><span>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + moment(stage['planned-end-date'] + ' UTC' ).format('MMM DD, YYYY HH:mm:ss') + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'] + ' UTC', "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						}
						//update end by prasana
					}
					hideRowId++;
					// //if (usersArr.users) {
					$('#articleHistory table tbody').prepend(stageRow);
					//console.log(stages[s])
				}
				$('#articleHistory table').prepend('<thead><tr class="stages"><th/><th>Stage Name</th><th>Assigned to</th><th>Start Date</th><th>End Date</th><th>Sla-Start Date</th><th>Sla-End Date</th><th>Planned-Start Date</th><th>Planned-End Date</th><th>Status</th><th>Days</th></tr></thead>')
				$('#articleHistory').modal()
			},
			showUserList:function(param){
				if (param.workflowStatus == 'in-progress') {
					stageRow = $('<tr class="stages" data-stage-name="' + param.stage + '"/>');
					usersHTML = "";
					usersArr = {};
					var users;
					var assignUserRole = userData.kuser.kuroles[param.customer]['role-type'];
					var assignUserAccess = userData.kuser.kuroles[param.customer]['access-level'];
          if(assignUserRole && assignUserAccess){
						users = $('<select id="userList" class="assign-users" onchange="eventHandler.assignmentSummary.showAssigned(this)" style="display:inline;height:auto;padding: 2px 0px;"/>');
						users.append('<option selected="selected">Select the User </option>')
						// $(usersDetailsDiv).find('user').each(function (i, v) {
						// 	users.append('<option value="' + $(this).find('first').text() + ', ' + $(this).find('email').text() + '">' + $(this).find('first').text() + '</option>');
						// });
						if (usersList && usersList.users) {
							Object.keys(usersList.users).forEach(function (count, q) {
								var skillLevel = (usersList.users[count].additionalDetails && usersList.users[count].additionalDetails['skill-level']) ? usersList.users[count].additionalDetails['skill-level'] : "";
								// To show only particular users for assigning the articles for particular stage based on user role-type and access-level
								if (showAssignUserName[assignUserRole] && showAssignUserName[assignUserRole][assignUserAccess]) {
									var role = usersList.users[count].additionalDetails['role-type'];
									var access = usersList.users[count].additionalDetails['access-level'];
									if (showAssignUserName[assignUserRole][assignUserAccess][param.stage] && showAssignUserName[assignUserRole][assignUserAccess][param.stage].roleType.indexOf(role) > -1 && showAssignUserName[assignUserRole][assignUserAccess][param.stage].accessLevel.indexOf(access) > -1) {
										users.append('<option onchange="eventHandler.assignmentSummary.showAssigned(this) skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
									}
								} else {
									users.append('<option onchange="eventHandler.assignmentSummary.showAssigned(this) skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
								}
							})
						}
						if (users.find('option[value="' + usersList["users"][0].name
						+ '"]').length > 0) {
							users.find('option[value="' + usersList["users"][0].name
							+ '"]').attr('selected', true);
							users.append('<option onchange="eventHandler.assignmentSummary.showAssigned(this) value="' + " " + '">Unassign</option>');
						} else {
							if (usersList["users"][0].name
							== null) {
								users.append('<option onchange="eventHandler.assignmentSummary.showAssigned(this) value="' + " " + '" selected>Unassigned</option>');
							} else {
								
								users.append('<option onchange="eventHandler.assignmentSummary.showAssigned(this) value="' + " " + '">Unassign</option>');
							}
						}
					}
					usersHTML = $(users)[0].outerHTML;
					if($('#autoAssignContent .users .assign-users').length == '0')
					$('#autoAssignContent .users').append(usersHTML)
					eventHandler.assignmentSummary.showAssigned(targetNode);
					//update by prasana
					//update end by prasana
				}

			},
			articleInfo: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(msCard).attr('data-id');
				var storeData = window[$(msCard).attr('data-store-variable')];
				var sourceData;
				if (storeData && storeData[cardID] && storeData[cardID]._source) {
					sourceData = storeData[cardID]._source;
				}
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var infoTable = '';
				$('#articleInfo .header-tag span').html(sourceData.doi);
				$('#articleInfo tbody').html('');
				for (var info of articleInfoDetails) {
					if (sourceData[info.attribute] == null) {
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>0</td></tr>'
					} else {
						var infoValue = '';
						if (info.name == 'Authors') {
							if (sourceData[info.attribute].name.length) {
								for (var author of sourceData[info.attribute].name) {
									infoValue += author['given-names'] + ' ' + author.surname + ', '
								}
								infoValue = infoValue.slice(0, -2);
							} else {
								infoValue = sourceData[info.attribute].name['given-names'] + ' ' + sourceData[info.attribute].name.surname
							}
						} else {
							infoValue = sourceData[info.attribute]
						}
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>' + infoValue + '</td></tr>'
					}
				}
				$('#articleInfo tbody').html(infoTable);
				$('#articleInfo').modal();
				$('#articleInfo').on('hide.bs.modal', function (event) {
					$('#articleInfo .modal-body').scrollTop(0);
				});
			},
			collapseHistoryRow: function (node, buttonClass) {
				$('.' + buttonClass).slideToggle(0);
				if ($(node).hasClass("fa-plus-circle")) {
					$(node).addClass("fa-minus-circle").removeClass('fa-plus-circle')
				} else {
					$(node).addClass("fa-plus-circle").removeClass('fa-minus-circle')
				}
			},
			assignUserForceLogOff: function (param) {
				if(param && param.from == 'assignment')
				{
					if (!param) {
						return;
					}
					$.ajax({
						url: '/api/updatedata',
						type: 'POST',
						data: param,
						async: false,
						success: function (res) {
							if (res) {
								eventHandler.assignmentSummary.assignUser(param);
							}
						},
						error: function (res) {
						}
					});

				}
				else{
				if (!param) {
					return;
				}
				$.ajax({
					url: '/api/updatedata',
					type: 'POST',
					data: param,
					async: false,
					success: function (res) {
						if (res) {
							eventHandler.components.actionitems.assignUser(param.name, param.cardID, param.sourceID, true, '', '', true, '','', param.parentId);
						}
					},
					error: function (res) {
					}
				});
			}},
			/**
			 * To get user details based on our role and access level from elastic
			 */
			getUserDetails: function (customer) {
				jQuery.ajax({
					type: "GET",
					url: "/api/getassignuserdata?customer=" + customer,
					success: function (msg) {
						usersList.users = msg;
					}
				})
			},
			holdArticle: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(targetNode).closest('.msCard').attr('data-id');
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				if ($(""+parentId+"[data-id='" + cardID + "']").find('.msStageName').text() == 'Hold') {
					$('#modalUnHold').find('div.commentArea').text('');
					$('#modalUnHold #unholdid').text($(""+parentId+"[data-id='" + cardID + "']").attr('data-doi'));
					$('#modalUnHold .saveHold').attr('data-message', "{'funcToCall': 'saveHold', 'param':{'holdtype':'release', 'cardid':'" + cardID + "', 'parentid': '"+parentId+"', 'modal':'#modalUnHold'}}");
					$('#modalUnHold').modal();
				} else {
					$('#modalHold').find('div.commentArea').text('');
					$('#modalHold').find('select').val();
					$('#modalHold #holdid').text($(""+parentId+"[data-id='" + cardID + "']").attr('data-doi'));
					$('#modalHold .saveHold').attr('data-message', "{'funcToCall': 'saveHold', 'param':{'holdtype':'hold', 'cardid':'" + cardID + "', 'parentid': '"+parentId+"', 'modal':'#modalHold'}}");
					$('#modalHold').modal();
				}
			},
			fastTrack: function (param, targetNode) {
				if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
					return;
				}
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(targetNode).closest('.msCard').attr('data-id');
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				var modal = '#modalFastTrack';
				if(param.fastTrack == 'true'){
					$(modal).find('.FastTrackHeader').text('Remove Fast Track');
				} else {
					$(modal).find('.FastTrackHeader').text('Add Fast Track');
				}
				//var param = {};
				//param.cardID = $(targetNode).closest('.msCard').attr('data-id');
				param.modal = modal;
				param.parentId = parentId;
				$(modal).find('select').prop('selectedIndex', 0);
				$(modal).find('div .commentArea').text('');
				var buttonInfo = {
					'funcToCall': 'saveComment',
					'param': {
						'funcToPost': 'eventHandler.components.actionitems.saveFastTrack',
						'currentNode': param
					}
				};
				$(modal + ' .saveFastTrack').attr("data-message", JSON.stringify(buttonInfo));
				$(modal).modal();
			},
			conformationModal: function (type, targetNode) {
				if($(targetNode).parentsUntil('li').find('.actionBtns .fa.fa-flag').length){
					$('#blkupdate').find('.text').text('Article is in Fast-Track. Are you sure you want to put it on Hold?');
					$('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.openmodal(\''+type+'\',\''+ $(targetNode).attr('test') +'\')');
					$('#blkupdate').modal();
					return;
				} else {
					eventHandler.components.actionitems.openmodal(type, targetNode);
				}
			},
			openReviewerStatus: function(type, targetNode){
				var cardID   = $(targetNode).closest('.msCard').attr('data-id');
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				var customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				var project  = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				var doi      = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				$('#reviewerHistory #reviewerArticleDOI').text(doi);
				jQuery.ajax({
					type: "GET",
					url: "/api/getdata?doi=" + doi + "&project=" + project + "&customer=" + customer + "&xpath=//article//article-meta//contrib-group[@data-group='reviewer']",
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						$('#reviewerHistory').modal();
						$('#reviewerHistory table tbody').html("");
						$(msg).find('contrib[contrib-type="reviewer"]').each(function(){
							var trNode      = $('<tr />');
							var surName = $(this).find('surname').text();
							var givenName = $(this).find('given-names').text();
							trNode.append('<td>' + surName + ' ' + givenName + '</td>');

							var assigneddDate = "-";
							var completedDate = "-";
							var statusDate    = "-";
							if($(this).find('fn p:last').attr('data-assign-date')){
								var assDate = $(this).find('fn p:last').attr('data-assign-date');
								assigneddDate = moment(parseInt(assDate)).utc().format('MMM DD, YYYY');
								assigneddDate += "<br>";
								assigneddDate += moment(parseInt(assDate)).utc().format('HH:mm:ss');
							}
							if($(this).find('fn p:last').attr('data-completed-date')){
								var comDate = $(this).find('fn p:last').attr('data-completed-date');
								completedDate = moment(parseInt(comDate)).utc().format('MMM DD, YYYY');
								completedDate += "<br>";
								completedDate += moment(parseInt(comDate)).utc().format('HH:mm:ss');
							}
							if($(this).find('fn p:last').attr('data-decision-date')){
								var stDate = $(this).find('fn p:last').attr('data-decision-date');
								statusDate = moment(parseInt(stDate)).utc().format('MMM DD, YYYY');
								statusDate += "<br>";
								statusDate += moment(parseInt(stDate)).utc().format('HH:mm:ss');
							}
							trNode.append('<td>' + assigneddDate + '</td>');
							trNode.append('<td style="text-transform: capitalize;">' + $(this).find('fn p:last').text() + '</td>');
							trNode.append('<td>' + statusDate + '</td>');
							trNode.append('<td>' + completedDate + '</td>');
							$('#reviewerHistory table tbody').append(trNode);
						});
					},
					error: function (err) {
						console.log(err);
					}
				});
			},
			openmodal: function (type, targetNode) {
				console.log(targetNode);
				if(typeof (targetNode) == 'string'){
					targetNode = $('#manuscriptsDataContent').find('[test="'+ targetNode +'"][title="Hold"]');
					$('#blkupdate').modal('hide');
				} else {
					if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
						return;
					}
				}
				var modal = '#commonmodal';
				$(modal + ' .commonModalHeader').text();
				$(modal + ' .save').removeAttr('data-message');
				$(modal + ' .commentbox').addClass('hidden');
				$(modal + ' .dropdown').addClass('hidden');
				var msCard = $(targetNode).closest('.msCard');
				var param = {};
				param.cardID = $(targetNode).closest('.msCard').attr('data-id');
				param.parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				param.modal = modal;
				console.log(param.cardID);
				var doi = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-doi');
				var header = '';
				if (popups[type].title != undefined) {
					header = popups[type].title;
				}
				var descriptiontext = popups[type].descriptiontext;
				if (popups[type].function != undefined) {
					$(modal + ' .save').attr("data-message", "{'funcToCall': 'saveComment', 'param':{ 'funcToPost':'" + popups[type].function+"',  'currentNode':'" + JSON.stringify(param) + "'}}");
				}
				if (header.match('{doi}')) {
					header = header.replace('{doi}', doi);
				}
				if (popups[type].commentbox) {
					$(modal + ' .commentbox').removeClass('hidden');
				}
				if (popups[type].dropdown) {
					$(modal + ' .dropdown').removeClass('hidden');
					options = "<option value='' disabled='disabled' selected='selected'>CHOOSE OPTION</option>";
					popups[type].options.forEach(function (element) {
						console.log(element);
						options += "<option value='" + element + "' >" + element.toUpperCase() + "</option>";
					});
					$('#commonmodal .dropdown').find('select').html(options);
				}
				if (popups[type].confirmation) {}
				$(modal + ' .commonModalHeader').text(header);
				$(modal).modal();
			},
			saveHold: function (param, comments) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var hold = {};
				hold.type = $(modal).find('select').val();
				if ((hold.type == "" || hold.type == null || hold.type == undefined) && !$(modal).find('.dropdown').hasClass('hidden')) {
					$(modal).find('#modalError').html("Please select an option.");
					return;
				} else {
					var holdIcontooltip = "";
					if (hold.comment != null && hold.comment != "") {
						holdIcontooltip = 'data-tooltip="' + hold.comment + '"';
					}
					var parameters = {};
					var notify = {};
					// parameters.holdComment = $(modal).find('div.commentArea').text();
					if (hold.type != null && hold.type != undefined) {
						// parameters.holdComment = hold.type + ': ' + parameters.holdComment;
					}
					parameters.cardID = cardID;
					parameters.parentId = parentId;
					parameters.customer = $("" + parentId + " [data-id='" + cardID + "']").attr('data-customer');
					parameters.project = $("" + parentId + " [data-id='" + cardID + "']").attr('data-project');
					parameters.doi = $("" + parentId + " [data-id='" + cardID + "']").attr('data-doi');
					parameters.holdFrom = $(""+parentId+" [data-id='" + cardID + "']").attr('data-store-variable');
					var currStageName = $("" + parentId + " [data-id='" + cardID + "']").find('.msStageName').text();

					var d = new Date();
					var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
					var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
					if (currStageName == 'Hold') {
						parameters.releaseHold = 'true';
						notify.notifyTitle = "Remove Hold";
						notify.successText = parameters.doi + " has been removed from hold";
						notify.errorText = "Error while remove hold for " + parameters.doi;
					} else {
						var userName = '';
						if(userData && userData.kuser){
							if(userData.kuser.kuname && userData.kuser.kuname.first) userName+= userData.kuser.kuname.first
							if(userData.kuser.kuname && userData.kuser.kuname.last) userName+= ' ' +userData.kuser.kuname.last
							if(userData.kuser.kuemail && userData.kuser.kuname.last) userName+= ', ' +userData.kuser.kuemail
						}
						var userName  = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last+ ', ' +userData.kuser.kuemail;
						if(comments) {
							comments = comments.replace(/:\s(.+)/, ' ($1)').replace(/(\:\s)$/,'')
							parameters.holdComment = 'Hold for: '+ comments + '; Hold by: '+userName;
						}
						parameters.stageName = 'Hold';
						notify.notifyTitle = "Add Hold";
						notify.successText = parameters.doi + " has been put on hold";
						notify.errorText = "Error while hold article " + parameters.doi;
					}
					parameters.notify = notify;
					console.log(parameters);
					$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
					$(modal).find('#modalError').html("");
					$(modal).find('div .commentArea').text('');
					eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
					$(modal).modal('hide');
				}
			},
			revokeArticle: function (param, targetNode) {
				var modal = $(param.modal).closest('.modal');
				var parameters = {};
				var msCard = $(targetNode).closest('.msCard');
				parameters.cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
					return;
				}
				(new PNotify({
					title: '<b>Revoke access for Author</b>',
					text: 'You are trying to revoke the access from the Author. If you continue, Author will no longer be able to access the article using the link sent to him.<br/><br/>Click <b>OK</b> to continue !',
					icon: 'glyphicon glyphicon-question-sign',
					hide: false,
					confirm: {
						confirm: true
					},
					buttons: {
						closer: false,
						sticker: false
					},
					history: {
						history: false
					}
				})).get().on('pnotify.confirm', function () {
					eventHandler.components.actionitems.sendRevokeArticle(parameters);
				}).on('pnotify.cancel', function () {
					console.log('close');
					$(modal).modal('hide');
				});
			},
			sendRevokeArticle: function (param) {
				console.log(param);
				var cardID = param.cardID;
				var parentId = param.parentId;
				var parameters = {};
				var notify = {};
				parameters.cardID = param.cardID;
				parameters.parentId = param.parentId;
				parameters.customerName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.projectName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-stage-name');
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				eventHandler.common.functions.sendAPIRequest('revokearticle', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
			},
			saveFastTrack: function (param, commentText) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var fastTrack = {};
				fastTrack.type = param.fastTrack;
				// fastTrack.comment = $(modal).find('div .commentArea').text()
				fastTrack.comment = commentText;
				if ((fastTrack.type == "" || fastTrack.type == null || fastTrack.type == undefined) && !$(modal).find('.dropdown').hasClass('hidden')) {
					return;
				} else {
					var parameters = {};
					var notify = {};
					parameters.cardID = cardID;
					parameters.parentId = parentId;
					// parameters.fastTrackComment = $(modal).find('div .commentArea').text();
					parameters.fastTrackComment = commentText;
					parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
					parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
					parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
					var currStageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
					var fastTrackType = $(""+parentId+" [data-id='" + cardID + "']").attr('data-flag');
					var d = new Date();
					var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
					var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
					//parameters.data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><status>completed</status><end-date>' + currDate + '</end-date></stage>';
					if (fastTrack.type == 'false') {
						fastTrack.type == 'fastTrack'
						var priority = 'on';
					} else if (fastTrack.type == 'true') {
						fastTrack.type == 'remove'
						var priority = 'off';
					}
					parameters.data = {
						'process': 'update',
						'update': '//workflow/priority',
						'content': '<priority requested-date="'+ currDate + ' ' + currTime + '">' + priority + '</priority>',
						'append': '//workflow'
					}
					if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
						parameters.notify = notifyConfig[arguments.callee.name].notify;
						parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
					}
					console.log(parameters);
					$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
					$(modal).find('#holdError').html("");
					$(modal).find('div.commentArea').text('');
					eventHandler.common.functions.sendAPIRequest('updatedata', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
					$(modal).modal('hide');
				}
			},
			saveComment: function (param, source) {
				if (!param.currentNode) {
					return;
				}
				$(source).css("pointer-events", "none");
				var commentText = '';
				if (typeof (param.currentNode) == 'string') {
					var currentNode = JSON.parse(param.currentNode);
				} else {
					var currentNode = param.currentNode;
				}
				if (!param.commentText) {
					var modal = $(currentNode.modal).closest('.modal');
					if ($(modal).find('select').val() != null) {
						commentText += $(modal).find('select').val() + ': ';
					} else {
						commentText += $(modal).find('.modal-header').children().first().text() + ' : ';
					}
					if ($(modal).find('div .commentArea').text() != '') {
						commentText += $(modal).find('div .commentArea').text();
					}
				} else {
					commentText = param.commentText;
				}
				if (commentText != "") {
					eventHandler.components.actionitems.postComments(commentText, currentNode.cardID, ' ', source, currentNode.parentId);
				}
				if (param.funcToPost) {
					eval(param.funcToPost)(currentNode, commentText);
				}
				$(source).css("pointer-events", "auto");
			},
			resupplyArticle: function (param) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var resupply = {};
				resupply.comment = $(modal).find('div .commentArea').text()
				var parameters = {};
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				parameters.fastTrackComment = $(modal).find('div .commentArea').text();
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.currStage = 'papresupply';
				//Added by anuraja for git-1714 on 01-11-2018 - To add comment in previous stage
				parameters.addStageComment = 'true';
				// parameters.status = {'code':'200'};
				parameters.resupply = 'true';
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				console.log(parameters);
				$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
				$(modal).find('div.commentArea').text('');
				eventHandler.common.functions.sendAPIRequest('signoff', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
				$(modal).modal('hide');
			},
			packageCreation: function (btnNode, target) {
				if (!btnNode) return true;
				var cardID = $(target).closest('.msCard').attr('data-id');
				var storeData = $(target).closest('.msCard').attr('data-store-variable');
				var parentId = $(target).closest('.msCard').attr('data-dashboardview');
				var currentCard = window[storeData][cardID]._source;
				var customer = $(target).closest('.msCard').attr('data-customer')
				if (packageCreationParameter[customer]) {
					var parameter = {};
					parameter = JSON.parse(JSON.stringify(packageCreationParameter[customer]));
					var parameterString = '';
					Object.keys(parameter).forEach(function (value, key) {
						if (currentCard[parameter[value]]) {
							if (value == 'issueFileName') {
								parameter[value] = currentCard[parameter[value]] + '.xml'
							} else {
								parameter[value] = currentCard[parameter[value]]
							}
						}
						parameterString += value + '=' + parameter[value]
						if (Object.keys(parameter).length - 1 > key) parameterString += '&'
					})
				}
				$('#notifyBox .notifyHeaderContent').text("Publishing file... Please wait.");
				$('#notifyBox #reloadinfobody').text('');
				$('#notifyBox').show();
				$.ajax({
					type: 'GET',
					url: "/api/publisharticle?" + parameterString,
					success: function (data) {
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))) {
							eventHandler.components.actionitems.getJobStatus(parameter.client, data.message.jobid, '', '', '', parameter.id);
						}
						else if (data == '') {
							$('#notifyBox #reloadinfobody').text('Failed');
						} else {
							$('#notifyBox #reloadinfobody').text(data.status.message);
							if (data.status.message == "job already exist") {
									eventHandler.components.actionitems.getJobStatus(parameter.client, data.message.jobid, '', '', '', parameter.id);
							}
						}
					},
					error: function (error) {
						$('#notifyBox #reloadinfobody').text('Failed');
					}
				});
			},
			probeValidation: function (param, target) {
				if (!target || $(target).length == 0) {
					return false;
				}
				var parameter = {
					'doi': $(target).closest('.msCard').attr('data-doi'),
					'customer': $(target).closest('.msCard').attr('data-customer'),
					'project': $(target).closest('.msCard').attr('data-project'),
					'stage': $(target).closest('.msCard').attr('data-stage-name'),
					'role': 'typesetter'
				}
				$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
				$('.la-container').fadeIn();
				$.ajax({
					type: "POST",
					url: "/api/probevalidation",
					data: parameter,
					success: function (res) {
						$('.la-container').fadeOut();
						$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
						$('#probeResult h3 span').text(parameter.doi);
						eventHandler.components.actionitems.updateProbeStatus(res);
					},
					error: function (response) {
						$('.la-container').fadeOut();
						var errorParam = {
							'notify': {
								notifyTitle: 'ERROR',
								notifyType: 'error',
								notifyText: 'Probe validation failed.'
							}
						}
						eventHandler.common.functions.displayNotification(errorParam);
					}
				});
			},
			updateProbeStatus: function (res) {
				if (res && typeof(res) == 'string') {
					res = res.replace(/<column[\s\S]?\/>/g, '<column></column>');
					var resNode = $(res);
					if (resNode.find('message:not(:empty)').length > 0) {
						var old_ruleType = '';
						$('#probeResult ul,#probeResult .tab-content').html('');
						resNode.find('message').each(function (count) {
							if ($(this).find('remove-display').text() == "true") {
								return;
							}
							var ruleType = $(this).find('rule-type').text();
							if (old_ruleType == '' || old_ruleType != ruleType) {
								if (count == 0) {
									$('#probeResult ul').append('<li class="btn btn-success" onclick="eventHandler.components.actionitems.probeType(this,\'' + ruleType + '\');"><a>' + ruleType + '</a></li>');
									$('#probeResult .tab-content').append('<div id="' + ruleType + '" class="tab-pane active in"></div>');
								} else {
									$('#probeResult ul').append('<li class="btn" onclick="eventHandler.components.actionitems.probeType(this,\'' + ruleType + '\');"><a>' + ruleType + '</a></li>');
									$('#probeResult .tab-content').append('<div id="' + ruleType + '" class="tab-pane"></div>');
								}
							}
							old_ruleType = ruleType;
							var errorId = $(this).find('error-id').text();
							var queryTo = $(this).find('query-role').text();
							var row = $('<div class="row" id="probe-container"/>');
							var left = $('<div class="col-sm-3 small" />');
							var right = $('<div class="col-sm-9" />');
							if ($(this).find('node-id').text())
								left.append('<p> Node ID: <span class="label label-default">' + $(this).find('node-id').text() + '</span></p>');

							if ($(this).find('rule-id').text())
								left.append('<p> Rule ID: <span class="label label-default">' + $(this).find('rule-id').text() + '</span></p>');

							if ($(this).find('signoff-allow').text())
								left.append('<p> Signoff allow: <span class="label label-default">' + $(this).find('signoff-allow').text() + '</span></p>');

							if (ruleType == "DTD-Validator") {
								left.append('<p> Line: <span class="label label-default">' + $(this).find('line').text() + '</span></p>');
								left.append('<p> Column: <span class="label label-default">' + $(this).find('column').text() + '</span></p>');
							} else if (ruleType == "Schematron-Validator") {
								left.append('<p> Error ID: <span class="label label-default">' + errorId + '</span></p>');
							}
							row.append(left);
							right.append('<p>' + $(this).find('probe-message').text() + '</p>');
							if ($(this).find('text').text() != "") {
								right.append('<p>TEXT: ' + $(this).find('text').text() + '</p>');
							}
							if ($(this).find('xpath').text())
								right.append('<pre>' + $(this).find('xpath').text() + '</pre>');
							row.append(right);
							$('#' + ruleType).append(row);
						});
						$('#probeCount').text($('#probeResult .tab-pane.active').children().length)
						$('#probeResult #countTitle, #probeResult .modal-body ul').show();
						$('#probeResult .noProbeError').hide();
					} else {
						$('#probeResult .noProbeError').show();
						$('#probeResult #countTitle, #probeResult .modal-body ul').hide();
					}
					$('#probeResult').modal('show');
				}
			},
			probeType: function (node, probeType) {
				$(node).siblings().removeClass('btn-success');
				$(node).addClass('btn-success');
				$('#probeResult .tab-content').children().removeClass('active');
				$('#probeResult .tab-content #' + probeType).addClass('active');
				$('#probeCount').text($('#probeResult .tab-pane.active').children().length)
			},
			withdrawArticle: function (param) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var resupply = {};
				resupply.comment = $(modal).find('div .commentArea').text()
				var parameters = {};
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				parameters.fastTrackComment = $(modal).find('div .commentArea').text();
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = 'Withdrawn';
				parameters.addStageComment = 'true';
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				console.log(parameters);
				$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
				$(modal).find('div.commentArea').text('');
				eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
				$(modal).modal('hide');
			},
			markUrgent: function (param, targetNode) {
				$('#modalUrgent').modal();
			},
			//save workflow's stage due dates
			saveWFDate: function (datePicker, type, cardID, source, replaceCard, from, id, stageName, parentId) {
				if (cardID == undefined) {
					//var param = JSON.parse($('#articleHistory').attr('data').replace(/\'/g, '"'));
				}
				var parameters = {};
				var dueDateNode = $('[id="' + datePicker + '"]').parent().find('[data-date-type]');
				var dateType = dueDateNode.attr('data-date-type');
				if ($('[id="' + datePicker + '"]').data('datepicker').selectedDates.length == 0) return false;
				var dueDate = $('[id="' + datePicker + '"]').data('datepicker').selectedDates[0].toString().slice(4, 24);
				if ($('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == false && from != "conform") {
					$('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.blkSaveWFDate("' + datePicker + '","' + type + '","' + cardID + '","' + source + '",' + replaceCard + ',"conform","' + id + ', ' + parentId + '")')
					$('#blkupdate').modal();
					return;
				}

				//dueDateNode.html(dateFormat(new Date(dueDate), 'mmm dd yyyy HH:MM:ss'));
				// var formatDueDate = dateFormat(new Date(dueDate), 'yyyy-mm-dd');
				var formatDueDate = dateFormat(new Date(dueDate), 'yyyy-mm-dd HH:MM:ss');
				/*if($('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == "false"){
							$('#blkupdate').modal('hide');
						$('[type="checkbox"]:checked').closest('tr.reportTable').each(function(){
							if($("[id='dateRepEnd"+$(this).attr('data-doi')+"']").length > 0){
								dueDateNode = $("[id='dateRepEnd"+$(this).attr('data-doi')+"']");
							}else{
								dueDateNode =$("[id='blkdateRepEnd"+$(this).attr('data-doi')+"']")
							}
							$('[data-doi="'+$(this).attr('data-doi')+'"].Pickdate').html(dateFormat(new Date(dueDate), 'mmm dd, yyyy HH:MM:ss'));
						//dueDateNode.html(dateFormat(new Date(dueDate), 'mmm dd yyyy HH:MM:ss'));
							var currStageName = $(this).attr('data-stage-name');
							cardID=	$(this).attr('data-report-id');
							parameters.data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><' + dateType + '>' + formatDueDate + '</' + dateType + '></stage>';
							parameters.customerName = $("["+ source +"='" + cardID + "']").attr('data-customer');
							parameters.projectName = $("["+ source +"='" + cardID + "']").attr('data-project');
							parameters.doi = $("["+ source +"='" + cardID + "']").attr('data-doi');
							parameters.cardID = cardID;
						//$("[" + source + "='" + cardID + "']").find('[data-filter="Stage Due"]').text(dueDate);
							if (notifyConfig["saveWFDate"] && notifyConfig["saveWFDate"].notify) {
								parameters.notify = notifyConfig["saveWFDate"].notify;
								parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
							}
						eventHandler.common.functions.sendAPIRequest('updatedatausingxpath', parameters, "POST",function(params){
							//$('[data-report-id="'+params.cardID+'"] [data-filter="Stage Due"]').text($($('[data-report-id="'+params.cardID+'"]').closest('tbody').find('.Pickdate')[0]).text())
						});
						});
					}else{*/
				$('[data-doi="' + id + '"].Pickdate').html(dateFormat(new Date(dueDate), 'mmm dd, yyyy HH:MM:ss'));
				if (stageName == undefined) {
					var currStageName = $('[id="' + datePicker + '"]').closest('tr').attr('data-stage-name');
				} else {
					var currStageName = stageName;
				}
				//var currStageName = $('#' + datePicker).closest('tr').find('td.stage-name').text();
				parameters.type = 'mergeData';
				parameters.data = {
					'process': 'update',
					'xpath': '//workflow/stage[name[.="' + currStageName + '"] and status[.="in-progress"]]',
					'content': '<stage><' + dateType + '>' + formatDueDate + '</' + dateType + '></stage>'
				}
				parameters.customer = $("[" + source + "='" + cardID + "']").attr('data-customer');
				parameters.project = $("[" + source + "='" + cardID + "']").attr('data-project');
				parameters.doi = $("[" + source + "='" + cardID + "']").attr('data-doi');
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				if (replaceCard) {
					eventHandler.common.functions.sendAPIRequest('updatedata', parameters, "POST", eventHandler.dropdowns.article.getArticles)
				} else {
					eventHandler.common.functions.sendAPIRequest('updatedata', parameters, "POST", function (params) {
						for (var stage = 0; stage < chartData[cardID]._source.stage.length; stage++) {
							if (chartData[cardID]._source.stage[stage].name == currStageName) {
								chartData[cardID]._source.stage[stage][dateType] = formatDueDate;

							}
						}
						//$('[data-report-id="'+params.cardID+'"] [data-filter="Stage Due"]').text($($('[data-report-id="'+params.cardID+'"]').closest('tbody').find('.Pickdate')[0]).text());
					});
				}
				// jQuery.ajax({
				// 	type: "POST",
				// 	url: "/api/updatedatausingxpath",
				// 	data: {
				// 		'customerName': $('#customerselect').val(),
				// 		'projectName': $('#projectselect').val(),
				// 		'doi': dueDateNode.closest('.jobCards[data-doiid]').attr('data-doiid'),
				// 		"data": data
				// 	},
				// 	success: function (response) {
				// 		console.log(response)
				//}
				// });
			},
			BulkAssignment: function () {
				if (assignUserData && Object.keys(assignUserData).length > 0) {
					$('.la-container').css('display', '');
					var totalCompleted = 0;
					var blkSuccessDoi = [];
					var blkFailureDoi = [];
					var openedArticles = [];
					var totalArticleLength = Object.keys(assignUserData).length;
					Object.keys(assignUserData).forEach(function (index) {
						var param = {};
						var userEmail = userData.kuser.kuemail;
						var userName = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last + ', ' + userEmail;
						var ajaxUrl = "/api/assignuser";
						var name = assignUserData[index].assignedto;
						param = assignUserData[index];
						if (typeof name != "string") {
							name = name.value;
						}
						var email = '';
						if (name.split(',')[1]) {
							email = (name.split(',')[1]).replace(' ', '');
						}
						var updateParameter = {
							"email": email,
							"customer": param.customer,
							"project": param.project,
							"assign": (name == " ") ? false : true,
							"doi": param.doi,
							"assignedBy": userName,
							"assignTo": (name == " ") ? "" : name
						}
						var param = {
							'notify': {
								'notifyTitle': 'Assign User',
								'notifyText': (name == " ") ? 'User unassigned successfully for {doi}' : 'User assigned successfully for {doi}',
								'notifyType': 'success'
							}
						}
						jQuery.ajax({
							type: "POST",
							url: ajaxUrl,
							data: updateParameter,
							dataType: "json",
							success: function (message) {
								totalCompleted++;
								if (message.content || message.status) {
									(name == "") ? name = " " : name;
								}
								blkSuccessDoi.push(updateParameter.doi);
								delete (assignUserData[updateParameter.doi])
								if (totalCompleted >= totalArticleLength) {
									eventHandler.components.actionitems.bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi, true);
									$("#assignSelection #customer [value='" + updateParameter.customer + "']").trigger("click");
									$(currentTab+"#userDataContent").html('');
									$('.la-container').css('display', 'none');
								}
							},
							error: function (e) {
								totalCompleted++;
								if (e && e.responseText != undefined && e.responseText && e.responseText.match(/status|message/g)) {
									e.responseText = JSON.parse(e.responseText);
									var respText = JSONPath({ json: e.responseText, path: '$..message' });
									e.responseText = (respText && respText[1]) ? respText[1] : ' ';
								}
								if (e && e.responseText && e.responseText.match('opened')) {
									openedArticles.push({ 'name': (/(by )(.*)/g).exec(e.responseText)[2], 'doi': updateParameter.doi });
								} else {
									blkFailureDoi.push(updateParameter.doi);
								}
								if (totalCompleted >= totalArticleLength) {
									eventHandler.components.actionitems.bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi, true);
									$("#assignSelection #customer [value='" + updateParameter.customer + "']").trigger("click");
									$(currentTab+"#userDataContent").html('');
									$('.la-container').css('display', 'none');
								}
							}
						})
					})
				} else {
					return;
				}
			},
			assignUser: function (name, cardID, source, replaceCard, from, id, bulkUpdate, blkUpdateCount, bulkClickEvent, parentId) {
				if (cardID == undefined) {
				//var param = JSON.parse($('#articleHistory').attr('data').replace(/\'/g, '"'));
				}
				var currentClickEvent;
				if (!source || source == undefined) {
					source = 'data-id'
				}
				if(!parentId)
				{

					parentId = '#cardReportData'
					sourceAtt = 'data-id';
				}
				else if(parentId=='#cardReportData'){
					sourceAtt = 'data-id';
				}else{
					sourceAtt = source;
				}
				if (bulkUpdate == undefined || bulkUpdate == undefined ) {
				    bulkUpdate = true;
				}
				var customer = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-customer')
				var roleType = userData.kuser.kuroles[customer]['role-type'];
				var accessLevel = userData.kuser.kuroles[customer]['access-level'];
				if (typeof (name) == 'object' && $(name).parent().parent().find('[type="checkbox"]').prop("checked") == true && id == '#modalReport' && $('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == false && from != "conform") {
				    blkSuccessDoi = [], blkFailureDoi = [], openedArticles = [];
				    successCount = 0;
				    failureCount = 0;
				    $('#blkupdate').find('.text').text('Are you sure? you are trying to do bulk updation.');
				    $('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.blkUpdate("' + name.value + '","' + source + '",' + replaceCard + ', \''+parentId+'\')');
				    $('#blkupdate').find('.cancelBulkUpdate').attr('onclick', 'eventHandler.components.actionitems.cancelBlkUpdate("' + $(name).attr('data-doi') + '")');
				    $('#blkupdate').find('.cancelBulkUpdate').attr('onclick', 'eventHandler.components.actionitems.cancelBlkUpdate("' + $(name).attr('data-doi') + '")');
				    $('#blkupdate').modal();
				    return;
				}
				var userEmail = userData.kuser.kuemail;
				var userName = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last + ', ' + userEmail;
				if (typeof (name) == 'object') {
					currentClickEvent = name;
					if (name.className.match('msAssignToME')) {
						name.value = userName;
						var skillLevel = userData.kuser.kuroles[$("" + parentId + " [" + sourceAtt + "='" + cardID + "']").attr('data-customer')]['skill-level']
					} else {
						var skillLevel = $(name).find('[value="' + name.value + '"]').attr('skill-level');
						name = name.value;
					}
				}
				var parameters = {};
				var d = new Date();
				var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
				var currStageName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-stage-name');
				parameters.customerName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-customer');
				parameters.projectName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-doi');
				if (name.value == " ") {
					email = $(name.offsetParent).find('[selected="selected"]').val().replace(/.*, /g, '')
				}
				if (typeof name != "string") {
				    name = name.value;
				}
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				var email = '';
				if (name.split(',')[1]) {
				    email = (name.split(',')[1]).replace(' ', '');
				}
				var updateParameter = {
				    "email": email,
				    "customer": customer,
				    "project": parameters.projectName,
				    "assign": (name == " ") ? false : true,
				    "doi": parameters.doi,
				    "assignedBy": userName,
				    "assignTo": (name == " ") ? "" : name
				}
				var ajaxUrl = "/api/assignuser";
				if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].stage.indexOf(currStageName) > -1) {
					updateParameter = {};
					var storeData = $("" + parentId + " [" + sourceAtt + "='" + cardID + "']").attr('data-store-variable');
					var currentCard = window[storeData][cardID]._source;
					var currStage = currentCard.stage.find( function (stage) {
						return stage.status == 'in-progress'
					})
					updateParameter = {
						"customer": customer,
						"project": parameters.projectName,
						"doi": parameters.doi,
						"wordCountWithoutRef": Number(currStage['start-word-count-without-ref']),
						"wordCount": Number(currStage['start-word-count']),
						"copyeditingProcess": true,
						"assign": (name == " ") ? false : true,
						"assignTo": (name == " ") ? "" : name,
						"email": email,
						"assignedBy": userName,
						"assignType": (name == " ") ? "Unassigned" : "Assigned",
						"skillLevel": (name == " ") ? "" : skillLevel,
						"to": (accessLevel == 'manager') ? email : "",
						"status": {
							"code": 200,
							"message": "Added successfully"
						}
					}
					if (accessLevel == 'manager') {
						ajaxUrl = '/workflow?currStage=copyeditingManager';
					} else {
						ajaxUrl = '/workflow?currStage=copyeditingVendor';
					}
				}
				var param = {
				    'notify': {
					'notifyTitle': 'Assign User',
					'notifyText': (name == " ") ? 'User unassigned successfully for {doi}' : 'User assigned successfully for {doi}',
					'notifyType': 'success'
				    }
				}
				jQuery.ajax({
					type: "POST",
					url: ajaxUrl,
					data: updateParameter,
					dataType: "json",
					success: function (message) {
						if (message.content || message.status) {
							successCount++;
							(name == "") ? name = " " : name;
							var jQueryID = '', jQueryIDVal = '';
							if ($('#articleHistory.show').length || $('#reportsHistory.show').length) {
								if (sourceAtt == 'data-report-id') {
									jQueryID = '#reportsHistory .assign-users,#chartReportTable [data-doi="' + parameters.doi + '"].assign-users'
									jQueryIDVal = '#reportsHistory .assign-users [value="' + name + '"], #chartReportTable [data-doi="' + parameters.doi + '"].assign-users [value="' + name + '"]'
								} else {
									jQueryID = '#articleHistory .assign-users';
									jQueryIDVal = '#articleHistory .assign-users [value="' + name + '"]'
								}
							} else {
								jQueryID = '#chartReportTable [data-doi="' + parameters.doi + '"].assign-users'
								jQueryIDVal = '#chartReportTable [data-doi="' + parameters.doi + '"].assign-users [value="' + name + '"]'
							}
							$(jQueryID).find('[selected]').removeAttr('selected');
							$(jQueryIDVal).attr("selected", "true");
							$(jQueryID).val($(jQueryIDVal).val());
							if (!bulkUpdate) {
								changeUserNameInHistory(cardID);
								blkSuccessDoi.push(parameters.doi);
							} else {
								if (sourceAtt == 'data-report-id') {
									changeUserNameInHistory(cardID);
								}
								param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, parameters.doi);
								eventHandler.common.functions.displayNotification(param);

							}
							if (successCount == blkUpdateCount) {
								eventHandler.components.actionitems.bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi);
							}
						}
						parameters.unassign = (name == " ") ? true : false;
						if ($('#articleHistory.show').length || $('#manuscriptContent').hasClass('active')) eventHandler.dropdowns.article.getArticles(parameters);
						$('.la-container').css('display', 'none');
					},
					error: function (e) {
						if (e && e.responseText != undefined && e.responseText && e.responseText.match(/status|message/g)) {
							e.responseText = JSON.parse(e.responseText);
							var respText = JSONPath({ json: e.responseText, path: '$..message' });
							e.responseText = (respText && respText[1]) ? respText[1]:' ';
						}
						successCount++;
						if (!bulkUpdate) {
							if (e && e.responseText && e.responseText.match('opened')) {
								openedArticles.push({ 'name': (/(by )(.*)/g).exec(e.responseText)[2], 'doi': parameters.doi });
							} else {
								blkFailureDoi.push(parameters.doi);
							}
							$('[data-doi="' + parameters.doi + '"].assign-users').val($(bulkClickEvent).find('[selected]').val());
						} else {
							if (name == " " && e && e.responseText != undefined) {
								param.notify.notifyText = e.responseText.replace(/assigning/g, 'unassigning');
							}
							param.notify.notifyText = 'Error While Assigning ' + parameters.doi;
							if (e && e.responseText != undefined && !e.responseText.match(/<html>/g)) {
								param.notify.notifyText = e.responseText.replace(/{doi}/g, parameters.doi);
							}
							param.notify.notifyType = 'error';
							if ($('#articleHistory.show').length || $('#reportsHistory.show').length) {
								if (source == 'data-report-id') {
									$('#reportsHistory .assign-users').val($(currentClickEvent).find('[selected]').val());
								} else {
									$('#articleHistory .assign-users').val($(currentClickEvent).find('[selected]').val());
								}
							} else {
								$('[data-doi="' + parameters.doi + '"].assign-users').val($(currentClickEvent).find('[selected]').val());
							}
							if (e && e.responseText && typeof(e.responseText) == 'string' && e.responseText.match(/Force log-out/g)) {
								var xmlData = '<stage><name>' + currStageName + '</name><status>in-progress</status><job-logs><log><status type="system">logged-off</status></log></job-logs></stage>'
								param.notify.notifyType = 'warning';
								param.notify.doi = parameters.doi;
								param.notify.functionToCall = "updateXpath"
								param.parameter = {
									doi: parameters.doi,
									customer: parameters.customerName,
									project: parameters.projectName,
									functionToCall: "updateXpath",
									type: 'mergeData',
									data : {
										'process': 'update',
										'xpath': '//workflow/stage[name[.="' + currStageName + '"] and status[.="in-progress"]]',
										'content': '<stage><job-logs><log><status type="system">logged-off</status></log></job-logs></stage>'
									},
									cardID: cardID,
									parentId: parentId,
									name: (name == " ") ? " " : name,
									sourceID: source
								}
								param.notify.confirmNotify = true;
							}
							eventHandler.common.functions.displayNotification(param);
						}
						if (successCount == blkUpdateCount) {
							eventHandler.components.actionitems.bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi,true);
						}
						$('.la-container').css('display', 'none');
					}
				})
				function changeUserNameInHistory(cardID) {
					for (var stage = 0; stage < chartData[cardID]._source.stage.length; stage++) {
						if (chartData[cardID]._source.stage[stage].name == currStageName && chartData[cardID]._source.stage[stage].status == 'in-progress') {
							chartData[cardID]._source.stage[stage].assigned = {
								'to': (name == " ") ? "" : name,
								'on': currDate + ' ' + currTime,
								'by': userName + ', ' + userEmail,
								'reason': null
							};
						}
					}
				}
			},
			bulkNotification: function (openedArticles, blkSuccessDoi, blkFailureDoi, hideNotify) {
				var param = notifyConfig.blkUpdate;
				if (openedArticles.length) {
					var openNotification = '';
					openedArticles.forEach(function (openDetail) {
						openNotification += openDetail.doi + ' is opened by ' + openDetail.name + '\n';
					})
					// param.notity = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi))
					param['notify']['notifyTitle'] = 'Assign User',
						param['notify']['notifyText'] = openNotification;
					param['notify']['notifyType'] = 'error';
					if (hideNotify) param.hideNotify = hideNotify;
					eventHandler.common.functions.displayNotification(param);
				}
				if (blkSuccessDoi.length) {
					param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText;
					if (name == " ") {
						param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText.replace(/assigned/g, 'unassigned');
					}
					param.notify.notifyType = 'success';
					param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, blkSuccessDoi.join(', '));
					if (hideNotify) param.hideNotify = hideNotify;
					eventHandler.common.functions.displayNotification(param);
				}
				if (blkFailureDoi.length) {
					param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText;
					if (name == " ") {
						param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText.replace(/assigning/g, 'unassigning');
					}
					param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, blkFailureDoi.join(', '));
					param.notify.notifyType = 'error';
					if (hideNotify) param.hideNotify = hideNotify;
					eventHandler.common.functions.displayNotification(param);
				}
			},
			// To get the user name who opened the article.
			getOpenArticleUserName: function (parameter, parameters, bulkUpdate) {
				var openedUserName = '';
				openedUserName = eventHandler.components.actionitems.getCurrentArticleStatus(parameter);
				if (bulkUpdate && openedUserName && openedUserName != undefined && parameters.notify && parameters.notify.openedArticle) {
					var param = {};
					param.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{name}/g, openedUserName));
					param.notify['notifyText'] = param.notify.openedArticle;
					eventHandler.common.functions.displayNotification(param);
				}
				return openedUserName;
			},
			blkSaveWFDate: function (datePicker, type, cardID, source, replaceCard, from, id, parentId) {
				$('#blkupdate').modal('hide');
				$('[type="checkbox"]:checked').closest('tr.reportTable').each(function () {
					eventHandler.components.actionitems.saveWFDate(datePicker, type, $(this).attr('data-report-id'), source, replaceCard, from, $($(this).find('.Pickdate')).attr('data-doi'), $(this).attr('data-stage-name'), parentId);
				});
			},
			// To update bulk assign user in table report.
			blkUpdate: function (value, source, replaceCard) {
                $('#blkupdate').modal('hide');
                $('.la-container').css('display', '');
                var doiCount = $('[type="checkbox"]:checked').closest('tr.reportTable').length;
                lastCount = 0
                var openedArticle = [],
                    bulkArticles = [],
                    unAccessibleArticles = [],
                    currentName;
                setTimeout(function () {
                    $('[type="checkbox"]:checked').closest('tr.reportTable').each(function () {
                        var cardID = $(this).attr(source);
                        currentName = $(this);
                        var checkOpenArticleParameter = {
                            'customer': $("[" + source + "='" + cardID + "']").attr('data-customer'),
                            'project': $("[" + source + "='" + cardID + "']").attr('data-project'),
                            'doi': $("[" + source + "='" + cardID + "']").attr('data-doi'),
                            'xpath': xpath.blkUpdate
                        }
                        if (!$("[" + source + "='" + cardID + "'] [data-filter='assignUser'] select").length) {
                            unAccessibleArticles.push($("[" + source + "='" + cardID + "']").attr('data-doi'));
                        } else {
                            bulkArticles.push({ 'value': value, 'cardID': cardID, 'source': source, 'replaceCard': replaceCard, 'conform': 'conform', 'id': '#modalReport', 'clickEvent': currentName })
                        }
                    });
                    if (unAccessibleArticles.length > 0) {
                        var param = notifyConfig.blkUpdate;
                        param['notify']['notifyTitle'] = 'Access Denied',
                            param['notify']['notifyText'] = notifyConfig.blkUpdate.notify.unAccessible.replace(/{doi}/g, unAccessibleArticles.join(', '));
                        eventHandler.common.functions.displayNotification(param);
                    }
                    if (bulkArticles.length > 0) {
                        bulkArticles.forEach(function (articleDetails) {
                            eventHandler.components.actionitems.assignUser(articleDetails.value, articleDetails.cardID, articleDetails.source, articleDetails.replaceCard, articleDetails.conform, articleDetails.id, false, bulkArticles.length, articleDetails.clickEvent, articleDetails.parentId);
                        })
                    }
                }, 1);
            },
			// To reset the value in dropdown when we cancel the bulk update while assigning user
			cancelBlkUpdate: function (currentDoi) {
                var value = $('#modalReport').find('select[data-doi="' + currentDoi + '"] [selected]').val();
                if (value == "Un assigned") {
                    $('#modalReport').find('select[data-doi="' + currentDoi + '"] [selected]').val("Un assigned");
                }
                $('#modalReport').find('select[data-doi="' + currentDoi + '"].assign-users').val(value)
                $('#blkupdate').modal('hide');
            },
			getArticleStatus: function (btnNode, target) {
				if (!btnNode) return true;
				var jobCard = $(target).closest('.msCard');
				var doiString = $(target).closest('.msCard').attr('data-doi');
				var doiCustomer = $(target).closest('.msCard').attr('data-customer');
				var message = "";
				jQuery.ajax({
					type: "GET",
					url: "/api/getdata?customer=" + $(target).closest('.msCard').attr('data-customer') + "&project=" + $(target).closest('.msCard').attr('data-project') + "&xpath=//workflow&doi=" + doiString,
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if ($(data).find('workflow').attr('data-job-id') == undefined) {
							//cannot fetch status, so directly retry
							//reLoadNotification(doiString,' unknown','Cannot find current status');
							showArticleStatus('', '', '', doiString, {
								'status': 'unknown',
								'log': ['Cannot find current status']
							});
						} else {
							$('#notifyBox').show();
							$('#notifyBox .notifyHeaderContent').html('Getting job status please wait.');
							$('#notifyBox #reloadinfobody').html('');
							eventHandler.components.actionitems.getJobStatus(doiCustomer, $(data).find('workflow').attr('data-job-id'), '', '', '', doiString, {
								'process': 'showArticleStatus',
								'onfailure': 'showArticleStatus',
								'onsuccess': 'showArticleStatus'
							});
						}
					}
				})
			},
			getftpFilename: function (doi) {
				$('#notifyBox').show();
				$('#notifyBox .notifyHeaderContent').html('Reloading article...');
				$('#notifyBox #reloadinfobody').html('');
				$('#commonmodal').modal('hide');
				var jobCard = $('[data-doi="' + doi + '"].msCard')
				var parameters = {
					'customer': jobCard.attr('data-customer'),
					'project': jobCard.attr('data-project'),
					'doi': jobCard.attr('data-doi'),
					'xpath': '//workflow/@ftp-file-name'
				};
				eventHandler.common.functions.sendAPIRequest('getdata', parameters, 'GET', function (res) {
					ftpFileName =  res.response.replace(/.*="(.*)"$/, '$1');
					eventHandler.components.actionitems.reloadArticle(doi, ftpFileName);
				}, function (res) {
					$('#notifyBox').show();
					$('#notifyBox .notifyHeaderContent').html('Reload article failed.');
					$('#notifyBox #reloadinfobody').html('');
					//notify("Error getting workflow","error");
					console.log('Error getting workflow');
				});
			},
			reloadArticle: function (doiString, ftpFileName) {

				if (!doiString) return false;
				var jobCard = $('[data-doi="' + doiString + '"]');
				if (jobCard.length == 0) return false;
				//if (jobCard.find('[data-ftp-file-name]').length == 0) return false;
				//var ftpFileName = jobCard.find('[data-ftp-file-name]').attr('data-ftp-file-name');
				var articleType = jobCard.attr('data-article-type');
				var articleTitle = jobCard.find('[data-filter="Title"]').text();
				var params = {
					customer: jobCard.attr('data-customer'),
					project: jobCard.attr('data-project'),
					doi: doiString,
					ftpFileName: ftpFileName,
					articleTitle: articleTitle,
					articleType: articleType,
					subject: 'field',
					specialInstructions: 'specialInstructions',
					priority: 'priority',
					mandatoryFields: 'mandatoryFields',
					hasMandatoryData: 'hasMandatoryData',
					forceLoad: 'true',
					update: 'true',
					status: {
						code: 200
					}
				}

				//if download type is aws status code should 202 added by vijayakumar on 18-03-2019
				if($('li[data-doi="' + doiString + '"]').length > 0){
					var downloadtye = $('li[data-doi="' + doiString + '"]').attr('data-download-type');
					if(downloadtye == "aws"){
						params['status']['code'] = "202"
					}else if($('li[data-doi="' + doiString + '"]').attr('data-template-type').length > 0 && $('li[data-doi="' + doiString + '"]').attr('data-template-type') == "book"){
						params['status']['code'] = "202"
					}
				}
				//$(btnNode).addClass('disabled');
				jQuery.ajax({
					type: "POST",
					url: "/workflow?currStage=addJob",
					data: params,
					success: function (data) {
						if (data && data.status && data.status.code && data.status.code == 200) {
							$('#artRetry').modal('hide');
							//on success, get the job id from workflow for getting the status
							setTimeout(function () {
								eventHandler.components.actionitems.getWFStatus(doiString);
							}, 200);
						}
					},
					error: function (err) {
						$('#notifyBox').show();
						$('#notifyBox .notifyHeaderContent').html('Reload article failed.');
						$('#notifyBox #reloadinfobody').html('');

						console.log(err);
					}
				});
			},
			getWFStatus: function (doiString) {
				var message = "";
				var jobCard = $('[data-doi="' + doiString + '"]');
				jQuery.ajax({
					type: "GET",
					url: "/api/getdata?customer=" + jobCard.attr('data-customer') + "&project=" + jobCard.find('.msProject').text() + "&xpath=//workflow&doi=" + doiString,
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if ($(data).find('workflow').attr('data-job-id') == undefined) {
							reLoadNotification(doiString, ' unknown', 'There is no job id for this article');
							/*setTimeout(function () {
								eventHandler.components.actionitems.getWFStatus(doiString);
							}, 200);*/
						} else {
							$('#notifyBox').show();
							$('#notifyBox .notifyHeaderContent').html('Downloading in progress... Please wait.');
							$('#notifyBox #reloadinfobody').html('');
							//$('#reloadinfobody').text("Downloading in progress... Please wait.");
							eventHandler.components.actionitems.getJobStatus($('[data-doi="' + doiString + '"]').attr('data-customer'), $(data).find('workflow').attr('data-job-id'), '', '', '', doiString);
						}
					}
				})
			},
			getJobStatus: function (customer, jobID, notificationID, userName, userRole, doi, callback, type) {
				//console.log('Job ID '+jobID);
				if (callback == undefined) callback = false;
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {
						"id": jobID,
						"userName": userName,
						"userRole": userRole,
						"doi": doi,
						"customer": customer,
						"type": type
					},
					crossDomain: true,
					success: function (data) {
						if (data && data.status && data.status.code && data.status.message && data.status.message.status) {
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if (data.status.message.stage.current) {
								currStep = data.status.message.stage.current;
							}
							var loglen = data.status.message.log.length;
							var process = data.status.message.log[loglen - 1];
							$('#notifyBox').show();
							if(data.status.message.input && data.status.message.input.downloadType){
								var downtype = data.status.message.input.downloadType;
								var jobCard = $('li[data-doi="' + doi + '"]');
								if($(jobCard).length > 0){
									jobCard.attr('data-download-Type', downtype);
								}
							}
							if (/completed/i.test(status)) {
								currStep = data.status.message.stage.current;
								$('#notifyBox .notifyHeaderContent').text("Process completed");
								$('#notifyBox #reloadinfobody').html(process);
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//$('#upload_model').closeModal();
								/*if(callback && callback.onsuccess && typeof(window[callback.onsuccess]) == "function"){
									window[callback.onsuccess](customer, jobID, notificationID, doi, data.status.message);
								}*/
							} else if (/failed/i.test(status) || code != '200') {
								$('#notifyBox .notifyHeaderContent').html(currStep);
								$('#notifyBox #reloadinfobody').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								if (callback && callback.onfailure && typeof (window[callback.onfailure]) == "function") {
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							} else if (/no job found/i.test(status)) {
								$('#notifyBox .notifyHeaderContent').html(status);
								$('#notifyBox #reloadinfobody').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								if (callback && callback.onfailure && typeof (window[callback.onfailure]) == "function") {
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							} else {
								$('#notifyBox .notifyHeaderContent').html(data.status.message.displayStatus);
								//$('.notify').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;
								if (loglen > 1) {
									var process = data.status.message.log[loglen - 1];
									if (process != '') $('#notifyBox #reloadinfobody').html(process);
								}
								if (callback && callback.process && typeof (window[callback.process]) == "function") {
									window[callback.process](customer, jobID, notificationID, doi, data.status.message);
								} else {
									setTimeout(function () {
										eventHandler.components.actionitems.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback, type);
									}, 1000);
								}
							}
						} else {
							reLoadNotification(doi, ' unknown', 'There is no job status for this article');

						}
					},
					error: function (error) {
						reLoadNotification(doi, ' unknown', 'There is no job status for this article');

					}
				});
			},
			cardSort: function (ele, sortwith, highlightAttr) {
				$('.la-container').show();
				setTimeout(function () {
					if ($('#manuscriptsDataContent li:visible').length < 2) {
						$('.la-container').hide();
						$('#sort span').removeClass('active');
						$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$('#sort').hide();
						return;
					} else {
						$('#sort').show();
					}
					if ($(ele).find('.fa-caret-up').length > 0) {
						$($(ele).children('.fa-caret-up')).remove();
						$(ele).append('<i class="fa fa-caret-down fa-fw"></i>');
					} else {
						$($(ele).children('.fa-caret-down')).remove();
						$(ele).append('<i class="fa fa-caret-up fa-fw"></i>');
					}
					// Based on http://jsfiddle.net/fyh0nmdh/
					$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
					$('#manuscriptsDataContent li:visible').sort(sort_li).appendTo('#manuscriptsDataContent');
					function sort_li(a, b) {
						$(a).find(highlightAttr).addClass('highlightSort');
						$(b).find(highlightAttr).addClass('highlightSort');
						var a = $(a).data(sortwith);
						var b = $(b).data(sortwith);
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.string && sortmapping.sortType.string.indexOf(sortwith) > -1) {
							if (a == undefined) a = " ";
							if (b == undefined) b = " ";
							a = a.toString().toUpperCase();
							b = b.toString().toUpperCase();
						}
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.number && sortmapping.sortType.number.indexOf(sortwith) > -1) {
							if (a == " " || a == undefined) a = 0;
							if (b == " " || b == undefined) b = 0;
						}
						if ($(ele).children().hasClass('fa-caret-up')) {
							return (b) < (a) ? 1 : -1;
						} else {
							return (b) > (a) ? 1 : -1;
						}
					}
					$($(ele).siblings().find('.fa')).remove();
					$('#sort span').removeClass('active');
					eventHandler.components.actionitems.hideRightPanel();
					$(ele).addClass('active');
					$('.la-container').hide();
				}, 0);
			},
			cardSortAssign: function (ele, sortwith, highlightAttr) {
				$('.typeAverage').css('color','')
				$('.msStageDueReport').css('color','')
				$('.la-container').show();
				setTimeout(function () {
					if ($('#manuscriptsDataContent li').length < 2) {
						$('.la-container').hide();
						$('#sort span').removeClass('active');
						$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$('#sort').hide();
						return;
					} else {
						$('#sort').show();
					}
					if ($(ele).find('.fa-caret-up').length > 0) {
						$($(ele).children('.fa-caret-up')).remove();
						$(ele).append('<i class="fa fa-caret-down fa-fw"></i>');
					} else {
						$($(ele).children('.fa-caret-down')).remove();
						$(ele).append('<i class="fa fa-caret-up fa-fw"></i>');
					}
					// Based on http://jsfiddle.net/fyh0nmdh/
					$('#manuscriptsDataContent li').find('.highlightSort').removeClass('highlightSort')
					$('#manuscriptsDataContent li.mscard').sort(sort_li).prependTo('#autoAssignContent #manuscriptsDataContent');
					function sort_li(a, b) {
						$(a).find(highlightAttr).addClass('highlightSort');
						$(b).find(highlightAttr).addClass('highlightSort');
						var a = $(a).data(sortwith);
						var b = $(b).data(sortwith);
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.string && sortmapping.sortType.string.indexOf(sortwith) > -1) {
							if (a == undefined) a = " ";
							if (b == undefined) b = " ";
							a = a.toString().toUpperCase();
							b = b.toString().toUpperCase();
						}
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.number && sortmapping.sortType.number.indexOf(sortwith) > -1) {
							if (a == " " || a == undefined) a = 0;
							if (b == " " || b == undefined) b = 0;
						}
						if ($(ele).children().hasClass('fa-caret-up')) {
							return (b) < (a) ? 1 : -1;
						} else {
							return (b) > (a) ? 1 : -1;
						}
					}
					$($(ele).siblings().find('.fa')).remove();
					$('#sort span').removeClass('active');
					$(ele).addClass('active');
					$('.la-container').hide();
				}, 0);
			},
			cardUserSort: function (ele, sortwith, highlightAttr) {
				$('.la-container').show();
				setTimeout(function () {
					if ($(currentTab + '#manuscriptsDataContent li:visible').length < 2) {
						$('.la-container').hide();
						$(currentTab + '#sort span').removeClass('active');
						$(currentTab + '#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$(currentTab + '#sort').hide();
						return;
					} else {
						$(currentTab + '#sort').show();
					}
					if ($(ele).find('.fa-caret-up').length > 0) {
						$($(ele).children('.fa-caret-up')).remove();
						$(ele).append('<i class="fa fa-caret-down fa-fw"></i>');
					} else {
						$($(ele).children('.fa-caret-down')).remove();
						$(ele).append('<i class="fa fa-caret-up fa-fw"></i>');
					}
					$(currentTab + '#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
					$(currentTab + '#manuscriptsDataContent li:visible').sort(sort_li).appendTo('#manuscriptsDataContent');
					function sort_li(a, b) {
						$(a).find(highlightAttr).addClass('highlightSort');
						$(b).find(highlightAttr).addClass('highlightSort');
						var a = $(a).data(sortwith);
						var b = $(b).data(sortwith);
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.string && sortmapping.sortType.string.indexOf(sortwith) > -1) {
							if (a == undefined) a = " ";
							if (b == undefined) b = " ";
							a = a.toString().toUpperCase();
							b = b.toString().toUpperCase();
						}
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.number && sortmapping.sortType.number.indexOf(sortwith) > -1) {
							if (a == " " || a == undefined) a = 0;
							if (b == " " || b == undefined) b = 0;
						}
						if ($(ele).children().hasClass('fa-caret-up')) {
							return (b) < (a) ? 1 : -1;
						} else {
							return (b) > (a) ? 1 : -1;
						}
					}
					$($(ele).siblings().find('.fa')).remove();
					$(currentTab + '#sort span').removeClass('active');
					eventHandler.components.actionitems.hideRightPanel();
					$(ele).addClass('active');
					$('.la-container').hide();
				}, 0);
			},
			showRightPanel: function (param, targetNode) {
				var currentTab = '.dashboardTabs.active ';
				if (!param || !param.rightPanelCol) param.rightPanelCol = 4;
				if (param.rightPanelCol) {
					param.mainCol = 12 - param.rightPanelCol;
				}
				$(currentTab + '#manuscriptsData').addClass('col-'+param.mainCol).removeClass('col-12');
				$(currentTab + '#rightPanelContainer').addClass('col-'+param.rightPanelCol);
				$(currentTab + '#rightPanelContainer').find('[data-type]').addClass('hidden');
				if (param && param.target == 'pdfviewer') {
					$(currentTab + '#rightPanelContainer').find('[data-type*=" pdfviewer "]').removeClass('hidden');
					if (param && param.link) {
						$(currentTab + '#rightPanelContainer').find('*[data-type=" pdfviewer "] object').attr('data', param.link);
						$(currentTab + '#pdfins').hide();
						$(currentTab + '#pdfview').show();
					}
				} else if (param && !param.type && param.target) { // To handle Right side container in issue makeup
					var contents = {};
					if ($(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents && $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content) {
						contents = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content;
					}
					var binder = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content.filter(function (doiContent) {
						return (doiContent._attributes.id == $(targetNode).attr('data-doi')) ? doiContent._attributes.id : '';
					});
					$(currentTab + '#rightPanelContainer').removeData();
					$(currentTab + '#rightPanelContainer').data(binder[0]);
					var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
					var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
					$(currentTab + '#rightPanelContainer #upload_pdf').addClass('hidden');
					$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "]').removeClass('hidden');
					if (!param.target.match(/history|section-content/g)) {
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead').addClass('hide');
					}
					$(currentTab + '#rightPanelContainer').attr('data-c-rid', $(targetNode).attr('data-c-id')).attr('data-dc-doi', $(targetNode).attr('data-doi')).attr('data-r-uuid', $(targetNode).attr('data-uuid'));
					if (param.target.match(/manuscript|sectionCard/g)) {
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabContent').addClass('hide');
						$(currentTab + '[data-type=" ' + param.target + ' "] object').attr('data', '').html('');
						// var mainCardContent = $(currentTab + '#manuscriptsData *[data-c-id="' + $(targetNode).attr('data-c-id') + '"]').data('data');
						var mainCardContent = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
						var figObject = {};
						var figObjectArr = [];
						if (param.target.match(/manuscript/g)) {
							var urlToGetXML = "/api/getdata?customer=" + customer + "&project=" + project + "&doi=" + targetNode.attr('data-doi') + "&xpath=//fig";
							if (mainCardContent._attributes.siteName && mainCardContent._attributes.siteName.match(/kriya1.0/g)) {
								urlToGetXML = 'http://bmj.kriyadocs.com/cms/ecs_includes/v2/exportToFormatV2.php?articleID=' + mainCardContent._attributes.cmsID;
							}
							jQuery.ajax({
								type: "GET",
								url: urlToGetXML,
								async: false,
								crossDomain: true,
								success: function (msg) {
									msg = '<div>' + msg + '</div>';
									$(msg).find('fig').each(function () {
										figObject[$(this).attr('data-id')] = $(this).find('graphic[data-track="ins"]').attr('xlink:href');
										var colorAttr = ({ "_attributes": {"type":"figure","label":$(this).attr('data-id'),"color":"0", "ref-id": $(this).attr('data-id'), "path":  $(this).find('graphic[data-track="ins"]').attr('xlink:href')}})
										figObjectArr.push(colorAttr);
									})
								},
								error: function (err) {
									console.log(err);
								}
							})
							var dataChanged = false;
							if (mainCardContent['color'] && figObject) {
								var figuresSection = mainCardContent['color'];
								if (figuresSection.constructor !== Array) figuresSection = [figuresSection];
								for (figure of figuresSection) {
									if (figure._attributes['path'] != figObject[figure._attributes['ref-id']]) {
										figure._attributes['path'] = figObject[figure._attributes['ref-id']];
										dataChanged = true;
									}
									if (!figObject[figure._attributes['ref-id']]) {
										console.log(figure._attributes['ref-id'], figObject[figure._attributes['ref-id']]);
									}
								}
							}
							else if(figObjectArr && figObjectArr.length>0){
								$(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')]['color']=figObjectArr;
								eventHandler.flatplan.action.saveData();
							}
							if (dataChanged) {
								$(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')] = mainCardContent;
								eventHandler.flatplan.action.saveData();
							}
						}
						binder[0] = mainCardContent;
						$(currentTab + '#rightPanelContainer').removeData();
						$(currentTab + '#rightPanelContainer').data(binder[0]);
						var tabsComponent = $(currentTab + '#manuscriptsData').data('config').binder[param.target];
						var configuration = $(currentTab + '#manuscriptsData').data('config');
						var tabHTML = '';
						if (tabsComponent.tab.constructor !== Array) {
							tabsComponent.tab = [tabsComponent.tab];
						}
						for (tab of tabsComponent.tab) {
							var showTab = false;
							if (tab._attributes.type == 'flagsTab') {
								var components = tab.path;
								if (components.constructor !== Array) {
									components = [components];
								}
								var tabCom = '';
								if (tab.path.constructor !== Array) {
									tab.path = [tab.path];
								}
								for (comp of tab.path) {
									var ele = JSONPath({ json: configuration, path: '$..' + comp.component._text })[0];
									if (ele && ele.constructor !== Array) {
										ele = [ele];
									}
									if (ele && ele.length) {
										ele = ele.filter(function (component) {
											return component._attributes['content-type'] == param.target
										})
										for (flag of ele) {
											flag = flag._attributes;
											if (flag['data-section-group'] != oldDataSectionGroup) {
												oldDataSectionGroup = flag['data-section-group'];
												tabCom += '<div class="flagSectionHead" data-flag-config="true">' + flag['data-section-group'] + '</div>'
											}
											var template = $('.components [data-component-type="' + flag['component-type'] + '"]').children().clone();
											if (flag['component-type'] == 'checkbox') {
												var flagElement = mainCardContent['flag'];
												if (flagElement && flagElement.constructor !== Array) {
													flagElement = [flagElement];
												}
												if (flagElement) {
													flagElement = flagElement.find(function (element) {
														if (element._attributes.type == flag.type) {
															return element;
														}
													})
													if (flagElement == undefined || (flagElement && !Number(flagElement._attributes.value))) {
														template.find('input').removeAttr('checked');
													}
												} else if (flag['default-value']) {
													if (!Number(flag['default-value'])) {
														template.find('input').removeAttr('checked');
													}
												} else {
													template.find('input').removeAttr('checked');
												}
												template.find('input').attr('data-flag-type', flag.type);
											}
											var selectOption = '';
											if (flag['component-type'] == 'select') {
												if (flag.type == 'section-head') {
													var existingContent = '';
													var secValues = eventHandler.flatplan.components.getAvailableSections();
													secValues.forEach(function (value, i) {
														if (!value.match(/cover|ifb|ibc|obc|toc|Editorial Board/gi) && existingContent != value) {
															if (binder && binder[0] && binder[0]._attributes && binder[0]._attributes.section == value) {
																selectOption += '<option value="' + value + '" selected="true">' + value + '</option>';
															} else {
																selectOption += '<option value="' + value+ '">' + value + '</option>';
															}
															existingContent = value;
														}
													})
												} else {
													var flagElement = mainCardContent['flag'];
													if (flagElement && flagElement.constructor !== Array) {
														flagElement = [flagElement];
													}
													if (flagElement) {
														flagElement = flagElement.find(function (element) {
															if (element._attributes.type == flag.type) {
																return element;
															}
														})
													}
													flag.values.split(',').forEach(function (value) {
														if (flagElement && flagElement._attributes.value == value) {
															selectOption += '<option value="' + value + '" selected="true">' + value + '</option>';
														} else {
															selectOption += '<option value="' + value + '">' + value + '</option>';
														}
													})
												}
												template.find('select').attr('data-flag-type', flag.type)
												template.find('select').html(selectOption);
											}
											template.find('i').text(flag.label);
											tabCom += template[0].outerHTML;
										}
									}
									else if(comp && comp.component && comp.component._text =='contenteditable'){
										var ceTemplate = $('.components [data-component-type="' + comp.component._text + '"]').children().clone();
										ceTemplate.find('.contenteditable').attr('data-flag-type', comp._attributes.string);
										ceTemplate.find('.label').html('<b>' + comp.label._text + '</b>');

										if (binder[0] && binder[0][comp._attributes.string] && binder[0][comp._attributes.string]._text) {
											var contentEditableText = binder[0][comp._attributes.string]._text.replace(/\]\]/g,'>').replace(/\[\[/g,'<');
											ceTemplate.find('.data').html('<p>' + contentEditableText + '</p>');
										}
										tabCom += ceTemplate[0].outerHTML;
									}
									else {
										var template = $('.components [data-component-type="' + comp.component._text + '"]').children().clone();
										template.find('.contenteditable').attr('data-flag-type', comp._attributes.string);
										template.find('.label').html('<b>' + comp.label._text + '</b>');
										if (binder[0] && binder[0][comp._attributes.string] && binder[0][comp._attributes.string]._text) {
											template.find('.data').html('<p>' + binder[0][comp._attributes.string]._text + '</p>');
										}
										tabCom += template[0].outerHTML;
									}
								}
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabCom);
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "]').parent().siblings('.header').offset().top - $('#footerContainer').height() - 40) + 'px');
								showTab = true;
							} else if (tab._attributes.type == 'pdfTab') {
								$(currentTab + '#rightPanelContainer').find('#download_pdf,#pdfDownload,#exportPDF,#exportOnlinePDF').removeClass('hidden');
								$(currentTab + '#rightPanelContainer [data-type*=" ' + param.target + ' "] #upload_pdf').addClass('hidden');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('href');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('data-href');
								if (mainCardContent && mainCardContent['file']) {
									var file = mainCardContent['file'];
									if (file.constructor !== Array) {
										file = [file];
									}
									for (path of file) {
										if (path._attributes.path && path._attributes.proofType) {
											$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-prooftype="' + path._attributes.proofType + '"] a').attr('data-href', path._attributes.path);
										}
										else if(path._attributes.path && path._attributes.type && path._attributes.type=='preflightPdf') {
											$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-preflight="true"] a').attr('data-href', path._attributes.path);
										}
									}
								}
								showTab = true;
								if (mainCardContent && mainCardContent._attributes && mainCardContent._attributes['data-run-on'] == 'true') {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', 'hidden');;
								}
								else if (mainCardContent && mainCardContent._attributes && mainCardContent._attributes['siteName'] == 'Non-kriya') {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', 'hidden');;
								} else {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', '');;
								}
							} else if (tab && tab._attributes && tab._attributes.type && tab._attributes.type != 'pdfTab') {
								var components = tab.path;
								if (components.constructor !== Array) {
									components = [components];
								}
								var tabCom = '';
								for (component of components) {
									var template = $('.components [data-component-type="' + component.component._text + '"]').children().clone();
									for (element of binder) {
										if (element[component.component._text]) {
											if (component.component._text == 'flag') {
												var ele = JSONPath({ json: configuration, path: '$..' + component.component._text })[0];
												if (ele && ele.constructor !== Array) {
													ele = [ele];
												}
												ele = ele.filter(function (component) {
													return component._attributes['content-type'] == param.target
												})
												var oldDataSectionGroup = '';
												for (flag of ele) {
													flag = flag._attributes;
													if (flag['data-section-group'] != oldDataSectionGroup) {
														oldDataSectionGroup = flag['data-section-group'];
														tabCom += '<div class="flagSectionHead" data-flag-config="true">' + flag['data-section-group'] + '</div>'
													}
													var template = $('.components [data-component-type="' + flag['component-type'] + '"]').children().clone();
													template.find('i').text(flag.label);
													tabCom += template[0].outerHTML;
												}
											} else {
												var ele = element[component.component._text];
												if (ele.constructor !== Array) {
													ele = [ele];
												}
												for (item of ele) {
													var source;
													var newTemplate = template.clone();
													if (!Number(item._attributes.color)) {
														newTemplate.find('input').removeAttr('checked');
													}
													newTemplate.find('input').attr('ref-id', item._attributes['ref-id']);
													if (item._attributes.path) {
														source = item._attributes.path;
													} else {
														source = "../images/NoImageFound.jpeg";
													}
													newTemplate.find('img').attr('src', source);
													tabCom += newTemplate[0].outerHTML;
												}
											}
											$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabCom);
											$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "]').parent().siblings('.header').offset().top - $('#footerContainer').height() - 40) + 'px');
											showTab = true;
										}
									}
								}
							}
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass(function (index, className) {
								return (className.match(/col-[0-9]+\s?/g) || []).join(' ');
							});
							if (showTab) {
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).removeClass('hide');
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass('hide').addClass('col-' + (12 / tabsComponent.tab.length));
							}
						}
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead').removeClass('active');
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead:first').addClass('active');
						if (!mainCardContent || mainCardContent == undefined || !mainCardContent.file) {
							$(currentTab + '[data-type*=" ' + param.target + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').show();
							return false;
						}
						var content = mainCardContent.file;
						if (!content || content == undefined) {
							$(currentTab + '[data-type*=" ' + param.target + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').show();
							return false;
						}
						var noPDF = false;
						if (content.constructor.toString().indexOf("Array") < 0) {
							content = [content];
						}
						for (path in content) {
							if (content[path]._attributes && content[path]._attributes.proofType == 'print') {
								if (content[path]._attributes.path != '') {
									$(currentTab + '[data-type*=" ' + param.target + ' "] object').show();
									$(currentTab + '[data-type*=" ' + param.target + ' "] object').attr('data', content[path]._attributes.path);
									$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').hide();
									$(currentTab + '[data-type*=" ' + param.target + ' "] object').css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "] #exportOnlinePDF').offset().top - $('#footerContainer').height() - 40) + 'px');
									noPDF = false;
								} else {
									noPDF = true;
								}
								break;
							} else {
								noPDF = true;
							}
						}
						if (noPDF) {
							$(currentTab + '[data-type*=" ' + param.target + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').show();
						}
					} else if (param.target.match(/advert|advert_unpaginated|filler/g)) {
						var mainCardContent = $(currentTab + '#manuscriptsData *[data-c-id="' + $(targetNode).attr('data-c-id') + '"]').data('data');
						var tabsComponent = $(currentTab + '#manuscriptsData').data('config').binder[param.target];
						var configuration = $(currentTab + '#manuscriptsData').data('config');
						var tabHTML = '';
						if (tabsComponent.tab.constructor !== Array) {
							tabsComponent.tab = [tabsComponent.tab];
						}
						for (tab of tabsComponent.tab) {
							if (tab._attributes.type == 'regionTab') {
								var components = tab.path;
								if (components.constructor !== Array) {
									components = [components];
								}
								var tabCom = '';
								var regions = JSONPath({ json: configuration, path: '$..region' })[0];
								if (regions.constructor !== Array) {
									regions = [regions];
								}
								regions = regions.filter(function (eachRegion) {
									return eachRegion._attributes['content-type'] === param.target;
								})
								for (eachRegion of regions) {
									var template = $('.components [data-component-type="region"]').children().clone();
									var savedRegion;
									if (mainCardContent && mainCardContent.region) {
										if (mainCardContent.region.constructor !== Array) {
											mainCardContent.region = [mainCardContent.region];
										}
										savedRegion = mainCardContent.region.find(function (reg) {
											return eachRegion._attributes.type === reg._attributes.type
										})
									}
									if (savedRegion == undefined) {
										var createRegion = {};
										createRegion._attributes = {
											'data-advert-name': param.target.charAt(0).toUpperCase() + param.target.slice(1),
											'layout': (param.target == 'filler') ? "FillerLayout 1" : "Layout 1",
											'type': eachRegion._attributes.type
										}
										createRegion.file = {
											'_attributes': {
												'data-color': "",
												'data-layout': "a",
												'data-region': eachRegion._attributes.type,
												'path': "",
												'type': "file"
											}
										}
										if (binder[0]['region'] == undefined) {
											binder[0]['region'] = [];
										}
										binder[0]['region'].push(createRegion);
									}
									var advertOrFillerPdf = $('.components [data-component-type="advertOrFillerPdf"]').children().clone();
									if (savedRegion && eachRegion._attributes.type == savedRegion._attributes.type) {
										if (savedRegion.file.constructor !== Array) {
											savedRegion.file = [savedRegion.file];
										}
										advertOrFillerPdf.find('[data-prooftype] a').removeAttr('style').removeAttr('data-href');
										for (file of savedRegion.file) {
											if (file._attributes.proofType) {
												advertOrFillerPdf.find('[data-prooftype="' + file._attributes.proofType + '"] a').attr('data-href', file._attributes.path).css('color', '#36b2de');
											} else if (file._attributes.type == 'preflightPdf') {
												advertOrFillerPdf.find('[data-prooftype="' + file._attributes.proofType + '"] a').attr('data-href', file._attributes.path).css('color', '#36b2de');
											}
										}
									}
									template.find('.stripes').append(advertOrFillerPdf);
									Object.keys(eachRegion).forEach(function (eachFlag) {
										if (eachFlag != '_attributes') {
											var advertComponents = eachRegion[eachFlag];
											if (advertComponents.constructor !== Array) {
												advertComponents = [advertComponents];
											}
											for (eachComponent of advertComponents) {
												var tempHtml = $('.components [data-component-type="' + eachComponent._attributes['component-type'] + '"]').children().clone();
												if (eachComponent._attributes['component-type'] == 'select') {
													var eachOption = '';
													if (savedRegion && savedRegion._attributes.layout) {
														var presentLayout = savedRegion._attributes.layout.toLowerCase();
													} else {
														var presentLayout = eachComponent._attributes['default-value'].toLowerCase();
													}
													eachComponent._attributes.values.split(',').forEach(function (eachValue) {
														if (presentLayout == eachValue.toLowerCase()) {
															eachOption += '<option value="' + presentLayout.toLowerCase() + '" selected="true">' + presentLayout.toLowerCase() + '</option>';
															template.find('.layoutBlock').append($(currentTab + '#manuscriptsData .layouts.hide #' + presentLayout.toLowerCase().replace(/\s/g, '')).html());
															template.find('.layoutBlock #' + presentLayout.toLowerCase()).removeClass('hide');
															template.find('.layoutBlock').attr('data-layout', eachValue.toLowerCase());
															var layoutSection;
															if (savedRegion && savedRegion.file) {
																layoutSection = savedRegion.file.filter(function (section) {
																	return section._attributes['data-region'];
																});
															}
															template.find('.layoutsection').each(function (layoutIndex) {
																$(this).html('<p class="fileObjUpload"> <span class="btn-primary btn-sm"> <input type="file" onchange="eventHandler.flatplan.issue.uploadFile(this);" region="' + eachRegion._attributes.type + '" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" data-layout-sec="' + $(this).attr('layout-sec') + '"> <span data-channel="flatplan" data-topic="issue" data-event="click" data-message="{\'funcToCall\': \'uploadFilePopup\'}" class="fileChooser"> <span class="icon-cloud-upload"></span> Upload</span> </span>  <input type="checkbox" data-flag-type="color" class="filled-in color" id="fg{fcIndex}" data-region-id="' + eachRegion._attributes.type + '" data-layout-sec="' + $(this).attr('layout-sec') + '" data-key-path="data-color"> <label for="fg{fcIndex}"> <i>Color in Print</i> </label> </p>');
																var pathURL = '';
																if (savedRegion && eachRegion._attributes.type == savedRegion._attributes.type && layoutSection && layoutSection[layoutIndex] && layoutSection[layoutIndex]._attributes.path) {
																	pathURL = layoutSection[layoutIndex]._attributes.path;
																}
																$(this).append('<object class="uploadObject" data="' + pathURL + '"></object>');
																$(this).find('input[type="checkbox"]').attr({
																	"data-channel": "flatplan",
																	"data-topic": "issue",
																	"data-event": "click",
																	"data-message": "{'funcToCall': 'saveTabsData'}"
																});
																$(this).find('input[type="checkbox"]').addClass('advertLayoutColor').removeClass('color');
																if (savedRegion && eachRegion._attributes.type == savedRegion._attributes.type && layoutSection && layoutSection[layoutIndex] && layoutSection[layoutIndex]._attributes['data-color'] && layoutSection[layoutIndex]._attributes['data-color'].match(/cmyk/g)) {
																	$(this).find('input[type="checkbox"]').attr('checked', 'checked');
																}
															})
														} else {
															if (eachComponent._attributes.type == 'float-placement' && mainCardContent._attributes['data-float-placement'] == eachValue.toLowerCase()) {
																eachOption += '<option value="' + eachValue.toLowerCase() + '" selected="true">' + eachValue.toLowerCase() + '</option>';
															} else {
																eachOption += '<option value="' + eachValue.toLowerCase() + '">' + eachValue.toLowerCase() + '</option>'
															}
														}
													})
													tempHtml.find('select').attr('type', eachComponent._attributes.type);
													tempHtml.find('select').addClass('advertLayoutDropdown').removeClass('flag');
													tempHtml.find('select').attr('data-flag-type', eachRegion._attributes.type);
													tempHtml.find('select').html(eachOption);
													tempHtml.find('select').val(eachComponent._attributes['default-value']);
												} else {
													tempHtml.find('[contenteditable="true"]').text((savedRegion) ? savedRegion._attributes['data-advert-name'] : '');
												}
												tempHtml.find('label').text(eachComponent._attributes['label']);
												template.find('.stripes:last').append(tempHtml);
											}
										}
									})
									template.find('.stripes').attr('data-region',eachRegion._attributes.label.toUpperCase());
									template.find('.stripes p i').text(eachRegion._attributes.label.toUpperCase());
									template.attr('id', eachRegion._attributes.label).attr('data-region', eachRegion._attributes.label);
									template.find('.layoutBlock').attr('data-region-id', eachRegion._attributes.label);
									tabCom += template[0].outerHTML + '<hr>';
								}
								$(currentTab + '#rightPanelContainer').removeData();
								$(currentTab + '#rightPanelContainer').data(binder[0]);
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabCom);
							} else if (tab._attributes.type == 'flagsTab') {
								tabCom = '';
								var components = tab.path;
								var template = $('.components [data-component-type="' + components.component._text + '"]').children().clone();
								template.text('Page ' + components.label._text);
								var innerTemplate = $('.components [data-component-type="checkbox"]').children().clone();
								innerTemplate.find('label i').text((mainCardContent.flag) ? mainCardContent.flag._attributes.type : 'Unpaginated');
								innerTemplate.find('input[type="checkbox"]').removeAttr('checked');
								innerTemplate.find('input[type="checkbox"]').removeClass('flag').addClass('unpaginated');
								innerTemplate.find('input[type="checkbox"]').attr('data-flag-type', (mainCardContent.flag) ? mainCardContent.flag._attributes.type : 'unpaginated');
								if (mainCardContent.flag && Number(mainCardContent.flag._attributes.value)) {
									innerTemplate.find('input[type="checkbox"]').attr('checked', 'checked');
								} else if (param.target.match(/advert_unpaginated/g)) {
									innerTemplate.find('input[type="checkbox"]').attr('checked', 'checked');
								}
								tabCom += template[0].outerHTML + innerTemplate[0].outerHTML;
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #ps' + tab._attributes.type).html(tabCom);
							}
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass(function (index, className) {
								return (className.match(/col-[0-9]+\s?/g) || []).join(' ');
							});
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href]')
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass('hide').addClass('col-' + (12 / tabsComponent.tab.length));
						}
					} else if (param.target.match(/table-of-contents|cover|editorial-board|blank/gi)) {
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabContent').addClass('hide');
						var mainCardContent = $(currentTab + '#manuscriptsData *[data-c-id="' + $(targetNode).attr('data-c-id') + '"]').data('data');
						var tabsComponent = $(currentTab + '#manuscriptsData').data('config').binder[param.target];
						var configuration = $(currentTab + '#manuscriptsData').data('config');
						var data = contents[$(targetNode).attr('data-c-id')];
						var tabHTML = '';
						if (tabsComponent.tab.constructor !== Array) {
							tabsComponent.tab = [tabsComponent.tab];
						}
						for (tab of tabsComponent.tab) {
							if (tab._attributes.type == 'pdfTab') {
								$(currentTab + '#rightPanelContainer').find('#download_pdf,#pdfDownload,#exportPDF,#exportOnlinePDF').removeClass('hidden');
								$(currentTab + '#rightPanelContainer [data-type*=" ' + param.target + ' "] .blank_rightpanel').addClass('hidden');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('href');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('data-href');
								if (param.target.match(/blank/gi)) {
									var file = data['file'];
									if (file.constructor !== Array) {
										file = [file];
									}
									var url = '';
									for (path of file) {
										if (path._attributes.type == 'pdf' && path._attributes.proofType == 'print') {
											url = path._attributes.path;
											break;
										} else if (path._attributes.type == 'pdf') {
											url = path._attributes.path;
										}
									}
									$(currentTab + '#rightPanelContainer #pdfTab [data-type*=" ' + param.target + ' "] object').attr('data', url);
									$(currentTab + '[data-type*=" ' + param.target + ' "] object').show();
									$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').hide();
									$(currentTab + '#rightPanelContainer').find('#download_pdf,#pdfDownload,#exportPDF,#exportOnlinePDF').addClass('hidden');
									$(currentTab + '#rightPanelContainer [data-type*=" ' + param.target + ' "] #upload_pdf').removeClass('hidden');
									$(currentTab + '#rightPanelContainer [data-type*=" ' + param.target + ' "] #upload_pdf input').attr('data-type', param.target);
								} else {
									if (data['file']) {
										var file = data['file'];
										if (file.constructor !== Array) {
											file = [file];
										}
										var noPDF = true;
										for (path of file) {
											if (path._attributes.proofType) {
												$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-prooftype="' + path._attributes.proofType + '"] a').attr('data-href', path._attributes.path);
											}
											else if(path._attributes.path && path._attributes.type && path._attributes.type=='preflightPdf') {
												$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-preflight="true"] a').attr('data-href', path._attributes.path);
											}
										}
										for (path of file) {
											if (path._attributes.type == 'pdf' && path._attributes.path && path._attributes.path!='' && path._attributes.proofType == 'print') {
												$(currentTab + '#rightPanelContainer #pdfTab [data-type*=" ' + param.target + ' "] object').attr('data', path._attributes.path);
												$(currentTab + '[data-type*=" ' + param.target + ' "] object').show();
												$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').hide();
												noPDF = false;
												break;
											} else if (path._attributes.type == 'pdf' && path._attributes.path && path._attributes.path!='' && (path._attributes.proofType == undefined || path._attributes.proofType == '')) {
												$(currentTab + '#rightPanelContainer #pdfTab [data-type*=" ' + param.target + ' "] object').attr('data', path._attributes.path);
												$(currentTab + '[data-type*=" ' + param.target + ' "] object').show();
												$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').hide();
												noPDF = false;
												break;
											}
										}
										if (noPDF) {
											$(currentTab + '[data-type*=" ' + param.target + ' "] object').hide();
											$(currentTab + '[data-type*=" ' + param.target + ' "] .noPDF').show();
										}
									}
								}
								$(currentTab + '#rightPanelContainer #pdfTab').find('[data-type*=" ' + param.target + ' "]').removeClass('hidden');
							} else if (tab._attributes.type == 'flagsTab') {
								$('.summerNoteComponent').each(function( index ) {
  									$(this).summernote('destroy');
								});
								var path = tab['path'];
								if (path.constructor !== Array) {
									path = [path];
								}
								var tabcomp = '';
								var summernoteText = '';
								for (component of path) {
									var template = $('.components [data-component-type="' + component.component._text + '"]').children().clone();
									template.find('.label').text(component.label._text);
									if (component.component._text == 'summerNoteEditor') {
										summernoteText = (data[component._attributes.string] && Object.keys(data[component._attributes.string]).length && data[component._attributes.string]._text) ? data[component._attributes.string]._text.replace(/\[\[/g, '<').replace(/\]\]/g, '>') : '';
									} else {
										var coverPath = JSONPath({ json: mainCardContent, path: '$..' + component._attributes.string })[0];
										template.find('.uploadObject').attr('data', coverPath);
									}
									template.find('#summernote').attr('data-key-path', component._attributes.string);
									template.find('#summernote').html(summernoteText);
									tabcomp += template[0].outerHTML.replace(/{key}/g, component._attributes.string);
								}
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabcomp);
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "]').parent().siblings('.header').offset().top - $('#footerContainer').height() - 40) + 'px');
								$(currentTab + '#rightPanelContainer').find('.summerNoteComponent').summernote({
									width: 430,
									toolbar: [
										['style',['style']],
										['style', ['bold', 'italic', 'underline', 'clear','header1','heading2','h3']],
										['fontsize', ['fontsize']],
										['color', ['color']],
										['para', ['ul', 'ol', 'paragraph']]
										]
								});
							}
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass(function (index, className) {
								return (className.match(/col-[0-9]+\s?/g) || []).join(' ');
							});
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).removeClass('hide');
							$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass('hide').addClass('col-' + (12 / tabsComponent.tab.length));
						}
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead').removeClass('active');
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead:first').addClass('active');
					}
				} else if (param && param.target && param.type == 'book') { // To handle Right side container in book view
					var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
					var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
					$(currentTab + '#rightPanelContainer #upload_pdf').addClass('hidden');
					if (param.target == 'manuscript') {
						var tabType = 'manuscript';
					} else {
						var tabType = 'sectionCard';
					}
					$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + tabType + ' "]').removeClass('hidden');
					if (!param.target.match(/history|section-content/g)) {
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] .tabHead').addClass('hide');
					}
					$(currentTab + '#rightPanelContainer').attr('data-c-rid', $(targetNode).attr('data-c-id')).attr('data-dc-doi', $(targetNode).attr('data-doi')).attr('data-r-uuid', $(targetNode).attr('data-uuid'));
					if (param.target.match(/manuscript|sectionCard/g) || param.type == 'book') {
						var binder = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content.filter(function (doiContent) {
							return (doiContent._attributes.id == $(targetNode).attr('data-doi')) ? doiContent._attributes.id : '';
						});
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + tabType + ' "] .tabContent').addClass('hide');
						$(currentTab + '[data-type*=" ' + tabType + ' "] object').attr('data', '').html('');
						// var mainCardContent = $(currentTab + '#manuscriptsData *[data-c-id="' + $(targetNode).attr('data-c-id') + '"]').data('data');
						var mainCardContent = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')];
						var figObject = {};
						binder[0] = mainCardContent;
						$(currentTab + '#rightPanelContainer').removeData();
						$(currentTab + '#rightPanelContainer').data(binder[0]);
						var tabsComponent = $(currentTab + '#manuscriptsData').data('config').binder[param.target];
						var configuration = $(currentTab + '#manuscriptsData').data('config');
						var tabHTML = '';
						if (!tabsComponent) {
							return;
						}
						if (tabsComponent && tabsComponent.tab.constructor !== Array) {
							tabsComponent.tab = [tabsComponent.tab];
						}
						for (tab of tabsComponent.tab) {
							var showTab = false;
							if (tab._attributes.type == 'flagsTab') {
								var components = tab.path;
								if (components.constructor !== Array) {
									components = [components];
								}
								var tabCom = '';
								if (tab.path.constructor !== Array) {
									tab.path = [tab.path];
								}
								for (comp of tab.path) {
									var ele = JSONPath({ json: configuration, path: '$..' + comp.component._text })[0];
									if (ele && ele.constructor !== Array) {
										ele = [ele];
									}
									if (ele && ele.length) {
										ele = ele.filter(function (component) {
											return component._attributes['content-type'] == param.target
										})
										for (flag of ele) {
											flag = flag._attributes;
											if (flag['data-section-group'] != oldDataSectionGroup) {
												oldDataSectionGroup = flag['data-section-group'];
												tabCom += '<div class="flagSectionHead" data-flag-config="true">' + flag['data-section-group'] + '</div>'
											}
											var template = $('.components [data-component-type="' + flag['component-type'] + '"]').children().clone();
											if (flag['component-type'] == 'checkbox') {
												var flagElement = mainCardContent['flag'];
												if (flagElement && flagElement.constructor !== Array) {
													flagElement = [flagElement];
												}
												if (flagElement) {
													flagElement = flagElement.find(function (element) {
														if (element._attributes.type == flag.type) {
															return element;
														}
													})
													if (flagElement == undefined || (flagElement && !Number(flagElement._attributes.value))) {
														template.find('input').removeAttr('checked');
													}
												} else if (flag['default-value']) {
													if (!Number(flag['default-value'])) {
														template.find('input').removeAttr('checked');
													}
												} else {
													template.find('input').removeAttr('checked');
												}
												template.find('input').attr('data-flag-type', flag.type);
											}
											var selectOption = '';
											if (flag['component-type'] == 'select') {
												if (flag.type == 'section-head') {
													var existingContent = '';
													contents.map(function (value, i) {
														if (!value._attributes.section.match(/cover|ifb|ibc|obc|toc|Editorial Board/gi) && existingContent != value._attributes.section) {
															if (binder && binder[0] && binder[0]._attributes && binder[0]._attributes.section == value._attributes.section) {
																selectOption += '<option value="' + value._attributes.section + '" selected="true">' + value._attributes.section + '</option>';
															} else {
																selectOption += '<option value="' + value._attributes.section + '">' + value._attributes.section + '</option>';
															}
															existingContent = value._attributes.section;
														}
													})
												} else {
													var flagElement = mainCardContent['flag'];
													if (flagElement && flagElement.constructor !== Array) {
														flagElement = [flagElement];
													}
													if (flagElement) {
														flagElement = flagElement.find(function (element) {
															if (element._attributes.type == flag.type) {
																return element;
															}
														})
													}
													flag.values.split(',').forEach(function (value) {
														if (flagElement && flagElement._attributes.value == value) {
															selectOption += '<option value="' + value + '" selected="true">' + value + '</option>';
														} else {
															selectOption += '<option value="' + value + '">' + value + '</option>';
														}
													})
												}
												template.find('select').attr('data-flag-type', flag.type)
												template.find('select').html(selectOption);
											}
											template.find('i').text(flag.label);
											tabCom += template[0].outerHTML;
										}
									} else {
										var template = $('.components [data-component-type="' + comp.component._text + '"]').children().clone();
										template.find('.contenteditable').attr('data-flag-type', comp._attributes.string);
										template.find('.label').html('<b>' + comp.label._text + '</b>');
										if (binder[0] && binder[0][comp._attributes.string] && binder[0][comp._attributes.string]._text) {
											template.find('.data').html('<p>' + binder[0][comp._attributes.string]._text + '</p>');
										}
										tabCom += template[0].outerHTML;
									}
								}
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabCom);
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "]').parent().siblings('.header').offset().top - $('#footerContainer').height() - 40) + 'px');
								showTab = true;
							} else if (tab._attributes.type == 'pdfTab') {
								$(currentTab + '#rightPanelContainer').find('#download_pdf,#pdfDownload,#exportPDF,#exportOnlinePDF').removeClass('hidden');
								$(currentTab + '#rightPanelContainer [data-type*=" '+tabType+' "] #upload_pdf').addClass('hidden');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('href');
								$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li a[target="_blank"]').removeAttr('data-href');
								if (mainCardContent && mainCardContent['file']) {
									var file = mainCardContent['file'];
									if (file.constructor !== Array) {
										file = [file];
									}
									for (path of file) {
										if (path._attributes.path && path._attributes.proofType) {
											$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-prooftype="' + path._attributes.proofType + '"] a').attr('data-href', path._attributes.path);
										}
										else if(path._attributes.path && path._attributes.type && path._attributes.type=='preflightPdf') {
											$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li[data-preflight="true"] a').attr('data-href', path._attributes.path);
										}
									}
								}
								showTab = true;
								if (mainCardContent && mainCardContent._attributes && mainCardContent._attributes['data-run-on'] == 'true') {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', 'hidden');;
								} else {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', '');;
								}
								if (mainCardContent && mainCardContent._attributes && mainCardContent._attributes['siteName'] == 'Non-kriya') {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', 'hidden');;
								} else {
									$(currentTab + '#rightPanelContainer #pdfTab').find('#exportPDF,#exportOnlinePDF').css('visibility', '');;
								}
							} else if (tab && tab._attributes && tab._attributes.type && tab._attributes.type != 'pdfTab') {
								var components = tab.path;
								if (components.constructor !== Array) {
									components = [components];
								}
								var tabCom = '';
								for (component of components) {
									var template = $('.components [data-component-type="' + component.component._text + '"]').children().clone();
									for (element of binder) {
										if (element[component.component._text]) {
											if (component.component._text == 'flag') {
												var ele = JSONPath({ json: configuration, path: '$..' + component.component._text })[0];
												if (ele && ele.constructor !== Array) {
													ele = [ele];
												}
												ele = ele.filter(function (component) {
													return component._attributes['content-type'] == param.target
												})
												var oldDataSectionGroup = '';
												for (flag of ele) {
													flag = flag._attributes;
													if (flag['data-section-group'] != oldDataSectionGroup) {
														oldDataSectionGroup = flag['data-section-group'];
														tabCom += '<div class="flagSectionHead" data-flag-config="true">' + flag['data-section-group'] + '</div>'
													}
													var template = $('.components [data-component-type="' + flag['component-type'] + '"]').children().clone();
													template.find('i').text(flag.label);
													tabCom += template[0].outerHTML;
												}
											} else {
												var ele = element[component.component._text];
												if (ele.constructor !== Array) {
													ele = [ele];
												}
												for (item of ele) {
													var source;
													var newTemplate = template.clone();
													if (!Number(item._attributes.color)) {
														newTemplate.find('input').removeAttr('checked');
													}
													newTemplate.find('input').attr('ref-id', item._attributes['ref-id']);
													if (item._attributes.path) {
														source = item._attributes.path;
													} else {
														source = "../images/NoImageFound.jpeg";
													}
													newTemplate.find('img').attr('src', source);
													tabCom += newTemplate[0].outerHTML;
												}
											}
											$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).html(tabCom);
											$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + param.target + ' "] #' + tab._attributes.type).css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + param.target + ' "]').parent().siblings('.header').offset().top - $('#footerContainer').height() - 40) + 'px');
											showTab = true;
										}
									}
								}
							}
							$(currentTab + '#rightPanelContainer').find('[data-type*=" '+tabType+' "] [data-href*="' + tab._attributes.type + '"]').removeClass(function (index, className) {
								return (className.match(/col-[0-9]+\s?/g) || []).join(' ');
							});
							if (showTab) {
								// $(currentTab + '#rightPanelContainer').find('[data-type*=" '+ tabType +' "]').removeClass('hide');
								$(currentTab + '#rightPanelContainer').find('[data-type*=" '+ tabType +' "] #' + tab._attributes.type).removeClass('hide');
								$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + tabType + ' "] [data-href*="' + tab._attributes.type + '"]').removeClass('hide').addClass('col-' + (12 / tabsComponent.tab.length));
							}
						}
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + tabType + ' "] .tabHead').removeClass('active');
						$(currentTab + '#rightPanelContainer').find('[data-type*=" ' + tabType + ' "] .tabHead:first').addClass('active');
						if (!mainCardContent || mainCardContent == undefined || !mainCardContent.file) {
							$(currentTab + '[data-type*=" ' + tabType + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + tabType + ' "] .noPDF').show();
							return false;
						}
						var content = mainCardContent.file;
						if (!content || content == undefined) {
							$(currentTab + '[data-type*=" ' + tabType + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + tabType + ' "] .noPDF').show();
							return false;
						}
						var noPDF = false;
						if (content.constructor.toString().indexOf("Array") < 0) {
							content = [content];
						}
						for (path in content) {
							if (content[path]._attributes && content[path]._attributes.proofType == 'print') {
								if (content[path]._attributes.path != '') {
									$(currentTab + '[data-type*=" ' + tabType + ' "] object').show();
									$(currentTab + '[data-type*=" ' + tabType + ' "] object').attr('data', content[path]._attributes.path);
									$(currentTab + '[data-type*=" ' + tabType + ' "] .noPDF').hide();
									$(currentTab + '[data-type*=" ' + tabType + ' "] object').css('height', (window.innerHeight - $(currentTab + '[data-type*=" ' + tabType + ' "] #exportOnlinePDF').offset().top - $('#footerContainer').height() - 40) + 'px');
									noPDF = false;
								} else {
									noPDF = true;
								}
								break;
							} else {
								noPDF = true;
							}
						}
						if (noPDF) {
							$(currentTab + '[data-type*=" ' + tabType + ' "] object').hide();
							$(currentTab + '[data-type*=" ' + tabType + ' "] .noPDF').show();
						}
					}
				}
				$(currentTab + '#rightPanelContainer').show();
				if (param && param.doi) {
					$(currentTab + '#pdfDoi').text(param.doi);
				}
        			//Added by ANuraja to open pdf in new Tab
				$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li,' + currentTab + '#rightPanelContainer #regionTab ul.dropdown-menu  li').unbind();
				$(currentTab + '#rightPanelContainer #pdfTab #download_pdf li,' + currentTab + '#rightPanelContainer #regionTab ul.dropdown-menu  li').on('click', function () {
					if ($(this).children('a').attr('data-href')) {
						window.open($(this).children('a').attr('data-href'), '_blank');
					}
				})
			},
			disabledNotify: function (param) {
				eventHandler.components.actionitems.hideRightPanel();
				PNotify.removeAll();
				var notifyObj = {}
				notifyObj.notify = {};
				notifyObj.notify.notifyTitle = param.notifyHead;
				notifyObj.notify.notifyText = param.notifyText + ' (' + param.doi + ') ';
				notifyObj.notify.notifyType = 'info';
				eventHandler.common.functions.displayNotification(notifyObj);
			},
			hideRightPanel: function () {
				$(currentTab+'#manuscriptsData').removeClass('col-8');
				$(currentTab+'#manuscriptsData').addClass('col-12');
				$(currentTab+'#rightPanelContainer').hide();
				$(currentTab+'#pdfview').hide();
				$(currentTab+'#pdfins').show();
				$(currentTab+'#pdfDoi').text('');
				//$('#sort span').removeClass('active');
			},
			showProductionNotes: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var parameters = {};
				parameters.cardID = cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.cardID = param.cardID;
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				parameters.parentId = parentId;
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
				var url = "/api/getdata?doi=#DOI#&project=#PROJECT#&customer=#CUSTOMER#&xpath=//article/production-notes";
				var modalDiv = $(".comments-modal");
				if (modalDiv.length === 0) {
					$("body").append('<div class="modal" id="commentsmodal" style="display:none;"><div class="modal-dialog modal-dialog-centered comments-modal" role="document" style="width:400px;"><div class="modal-header"><h3 class="holdHeader">Production Notes for <span class="doiText"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-content"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field maincontent"><div class="textarea-wrapper"><div class="textarea-content" data-placeholder="Add a comment" contenteditable="true" style="height: 6.5em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="eventHandler.components.actionitems.postComments(this, \'\', \'\' )"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="eventHandler.components.actionitems.openFileUpload(this);" data-upload-type="Files" data-success="eventHandler.components.actionitems.attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
					modalDiv = $("#comments-modal");
				}
				$(modalDiv).find('.data-container').find('#comment-tabs,#comment-list').remove()
				$(modalDiv).find('.data-container').append('<ul id="comment-tabs" style="width:9%;float:left;padding: 0px 5px;height: 100%;border-right: 1px solid #ddd;" class="main"/><ul id="comment-list" style="width:90%;float:right;height: 100%;overflow-y: auto;" class="main"/>');
				var buttonInfo = "eventHandler.components.actionitems.postComments(''," + cardID + ",this, '', '"+parentId+"')";
				$(modalDiv).find('.sendNotes').attr("onclick", buttonInfo);
				$('.modal.comments-modal .notesHeader .doiText').text(parameters.doi);
				$('.modal.comments-modal').attr('data-customer', parameters.customer).attr('data-project', parameters.project).attr('data-doiid', parameters.doi).attr('data-role-type', userData.kuser.kuroles[parameters.customer]['role-type']).attr('data-access-level', userData.kuser.kuroles[parameters.customer]['access-level']);
				$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').removeClass('hidden');
				$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').removeClass('status-confirmed');
				$('.jquery-comments .commenting-field .toggleLabels').addClass('toggle');
				$('.jquery-comments .commenting-field .toggleLabels').removeClass('active');
				$('.jquery-comments .commenting-field .toggleLabels:first').addClass('active');
				$('.jquery-comments .commenting-field .issue-category-data').addClass('hidden');
				if (userData.kuser.kuroles[parameters.customer]['role-type'] != 'production' || !/Pre-editing|Typesetter/i.test(parameters.stageName)){
					$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').addClass('hidden');
					$('.jquery-comments .commenting-field .toggleLabels').removeClass('toggle');
				}
				var notesURL = url.replace("#DOI#", parameters.doi).replace("#CUSTOMER#", parameters.customer).replace("#PROJECT#", parameters.project);
				notesURL = notesURL + '&bucketName=AIP';
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						$(modalDiv).find('.data-container').find('#comment-list').html('');
						if (msg && !msg.error) {
							var commentObj = $(msg);
							var notesDate = '', lastDate = '';
							var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
							var nl = $(commentObj).find('note').length - 1;
							$(commentObj).find('note').each(function (i, v) {
								var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="copy-note-clipboard  pull-right" data-channel="components" title="Copy" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'copyNoteClipboard\', \'param\':this}">&nbsp;<i class="fa fa-files-o" aria-hidden="true">&nbsp;</i></span><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + moment($(this).find('created').html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '">' + moment($(this).find('created').html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
								commentDiv.find('*[data-class]').each(function () {
									$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
								});
								commentDiv.find('.jsonresp').each(function () {
									$(this).addClass('toggle');
								})
								// to change email-content to a html structure
								commentDiv.find('email-content').each(function () {
									$(this).find('> *').each(function () {
										var div = $('<div>');
										$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase());
										$(div).html($(this).html());
										$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
									});
									$(this).find(".mail-body p:contains('review_content/?key=')").each(function () {
										$(this).html($(this).html().replace(/(https?:\/\/[a-z\.]+\/review_content\/\?key=[a-z0-9]+)/, '<span class="email-link" data-href="$1">KRIYA-LINK</span>'))
									})
									var div = $('<div>');
									$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase()).addClass('toggle');
									$(div).html($(this).html());
									$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
								})
								if ($(this).find('fullname').html() == 'Automation') {
									$(this).attr('type', 'automation');
								}
								if ($(this)[0].hasAttribute('type')) {
									var commentType = $(this).attr('type');
									if ($(modalDiv).find('#comment-tabs').find('[data-rid="' + commentType + '"]').length == 0){
										$(modalDiv).find('#comment-tabs').append('<li data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'toggleProductionNotes\'}" data-rid="' + commentType + '"><i class="fa fa-comment" style="font-size: 14px;"></i><br/>'+ commentType + '</li>');
									}
									commentDiv.attr('data-type', commentType);
									commentDiv.find('.comment-reply').remove();
									$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="' + $(this).attr('type').toLocaleLowerCase() + '">' + $(this).attr('type').toLocaleUpperCase() + '</span>');
									if (commentType == 'support'){
										if (commentDiv.find('.issue-data .category-info .level').length > 0){
											$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="support-level" data-level="' + commentDiv.find('.issue-data .category-info .level').text() + '" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'changeSupportLevel\'}" data-rid="notes">' + commentDiv.find('.issue-data .category-info .level').text().toLocaleUpperCase() + '</span>');
										}
									}
								}else{
									if ($(modalDiv).find('#comment-tabs').find('[data-rid="notes"]').length == 0){
										$(modalDiv).find('#comment-tabs').append('<li data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'toggleProductionNotes\'}" data-rid="notes"><i class="fa fa-comment" style="font-size: 14px;"></i><br/>notes</li>');
									}
								}
								//it the current on is reply for some other, the add it next to it
								if ($(this).find('parent').length > 0 && $('li.comment[data-id="' + $(this).find('parent').html() + '"]').length > 0) {
									var parentID = $(this).find('parent').html();
									commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
									if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
									} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
									} else { //or aff next to parent
										$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
									}
								} else { //append to comment list
									notesDate = $(this).find('created').html().replace(/\s.*$/, '');
									if (lastDate == ''){
										lastDate = notesDate
									}
									if (notesDate != lastDate){
										var n = new Date(lastDate);
										var divider = '<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>';
										$(modalDiv).find('#comment-list').prepend(divider);
									}
									lastDate = notesDate;
									$(modalDiv).find('#comment-list').prepend(commentDiv);
								}
								if (i == nl){
									var n = new Date(notesDate);
									var divider = '<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>';
									$(modalDiv).find('#comment-list').prepend(divider);
								}
							});
							$(modalDiv).find('#comment-tabs li[data-rid]').each(function(){
								if ($(this).attr('data-rid') == 'notes'){
									$(this).append('<span class="badge" data-count="' + $(modalDiv).find('#comment-list > li:not([data-type])').length + '"></span>')
								}else{
									$(this).append('<span class="badge" data-count="' + $(modalDiv).find('#comment-list > li[data-type="' + $(this).attr('data-rid') + '"]').length + '"></span>')
								}
							})
							$(modalDiv).find('#comment-tabs').prepend('<li class="active" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'toggleProductionNotes\'}" data-rid="all"><i class="fa fa-comment" style="font-size: 14px;"></i><br/>all' + '<span class="badge" data-count="' + $(modalDiv).find('#comment-list > li').length + '"></span></li>')
							/*var dataType = $(modalDiv).find('#comment-tabs li[data-rid]').first().attr('data-rid');
							$(modalDiv).find('#comment-list > li').addClass('hidden');
							if (dataType == 'notes'){
								$(modalDiv).find('#comment-list > li:not([data-type])').removeClass('hidden');
							}else{
								$(modalDiv).find('#comment-list > li[data-type="' + dataType + '"]').removeClass('hidden');
							}*/

							if(modalDiv){
								modalDiv.modal();
							}
						} else {
							if(modalDiv){
								modalDiv.modal();
							}
						}
						eventHandler.components.actionitems.showSplInstructions(param, targetNode);
					},
					error: function (xhr, errorType, exception) {
						if(modalDiv){
							modalDiv.modal();
						}
						eventHandler.components.actionitems.showSplInstructions(param, targetNode);
					}
				});
			},
			showSplInstructions: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var parameters = {};
				parameters.cardID = cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.cardID = param.cardID;
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
				var url = "/api/getdata?doi=#DOI#&project=#PROJECT#&customer=#CUSTOMER#&xpath=//article//article-meta/custom-meta-group/custom-meta[./meta-name[.='Special Instructions']]/meta-value";
				var modalDiv = $(".comments-modal");
				if (modalDiv.length === 0) {
					$("body").append('<div class="modal" id="commentsmodal" style="display:none;"><div class="modal-dialog modal-dialog-centered comments-modal" role="document" style="width:400px;"><div class="modal-header"><h3 class="holdHeader">Production Notes for <span class="doiText"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-content"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field maincontent"><div class="textarea-wrapper"><div class="textarea-content" data-placeholder="Add a comment" contenteditable="true" style="height: 6.5em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="eventHandler.components.actionitems.postComments(this)"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="eventHandler.components.actionitems.openFileUpload(this);" data-upload-type="Files" data-success="eventHandler.components.actionitems.attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
					modalDiv = $("#comments-modal");
				}
				var notesURL = url.replace("#DOI#", parameters.doi).replace("#CUSTOMER#", parameters.customer).replace("#PROJECT#", parameters.project);
				notesURL = notesURL + '&bucketName=AIP';
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						if (msg && !msg.error) {
							var commentObj = $(msg);
							$(commentObj).each(function (key, comment) {
								var commentDiv = $('<li data-id="' + $(comment).html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">Special Instructions</span><span class="comment-time" data-original=""></span><span class="comment-label">Special Instructions</span></div><div class="wrapper"><div class="content"><pre>' + moment($(comment).html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '</pre></div></div></div></li>');
								$(modalDiv).find('#comment-list').append(commentDiv);
							});
						}
					},
					error: function (xhr, errorType, exception) {}
				});
			},
			toggleProductionNotes: function(param, targetNode){
				var dataType = $(targetNode).attr('data-rid');
				$('#comments-modal').find('#comment-tabs > li').removeClass('active');
				$(targetNode).addClass('active')
				$('#comments-modal').find('#comment-list > p.notes-description').remove();
				$('#comments-modal').find('#comment-list > li, #comment-list > p').addClass('hidden');
				if (dataType == 'all'){
					$('#comments-modal').find('#comment-list > li, #comment-list > p').removeClass('hidden');
				}else if (dataType == 'notes'){
					$('#comments-modal').find('#comment-list').prepend('<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;" class="notes-description"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>NOTES<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>')
					$('#comments-modal').find('#comment-list > li:not([data-type])').removeClass('hidden');
				}else{
					$('#comments-modal').find('#comment-list').prepend('<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;" class="notes-description"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>' + dataType.toUpperCase() + '<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>')
					$('#comments-modal').find('#comment-list > li[data-type="' + dataType + '"]').removeClass('hidden');
				}
			},
			changeSupportLevel: function(param, targetNode){
				var supportLevel = $(targetNode).attr('data-level');
				var dataID = $(targetNode).closest('li.comment').attr('data-id');
				var replyNodes = $('#comment-list > li.comment.reply[data-parent-id="' + dataID + '"]')
				if (replyNodes.length > 0 && replyNodes.find('.issue-data .category-info').length > 0){
					return false;
				}
				$('li.comment').removeClass('replying');
				$(targetNode).closest('li.comment').addClass('replying');
				var doiText = $(targetNode).closest('.comments-modal').attr('data-doiid');
				$('#support-level-modal').find('.modal-header .doiText').text(doiText);
				$('#support-level-modal').attr('data-level', supportLevel);
				$('#support-level-modal').modal();
			},
			saveSupportLevel: function(param, targetNode){
				var currentLevel = $('#support-level-modal').attr('data-level')
				var supportLevel = $('#support-level-modal .support-level-option').val();
				if (supportLevel == "---" || supportLevel == currentLevel){
					return false;
				}
				var comment = $('#support-level-modal #supportcommands').text();
				if (comment.trim() == ""){
					return false;
				}
				var currentNode = $('li.msCard[data-doi="' + $('.modal.comments-modal').attr('data-doiid') + '"]');
				$.ajax({
					type: "GET",
					url: "/api/getdata?customer=" + currentNode.attr('data-customer') + "&project=" + currentNode.attr('data-project') + "&doi=" + currentNode.attr('data-doi') + "&xpath=//workflow/stage[last()]/comments",
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if ($(data).find('comments').length > 0 && /,level-[0-9]$/.test($(data).find('comments').text())) {
							var supportComments = $(data).find('comments').text();
							supportComments = supportComments.replace(currentLevel, supportLevel)
							var parameters = {};
							parameters.data = [{
								'process': 'update',
								'update': '//production-notes/note[@id="' + $('li.comment.replying').attr('data-id') + '"]//span[@class="level"]',
								'content': '<span class="level">' + supportLevel + '</span>'
							},{
								'process': 'update',
								'update': '//workflow/stage[last()]/comments',
								'content': '<comments>' + supportComments + '</comments>'
							}]
							parameters.customer = currentNode.attr('data-customer');
							parameters.project = currentNode.attr('data-project');
							parameters.doi = currentNode.attr('data-doi');
							$.ajax({
								url: '/api/updatedata',
								type: 'POST',
								data: parameters,
								async: false,
								success: function (res) {
									if (res) {
										var currentNode = $('li.msCard[data-doi="' + $('.modal.comments-modal').attr('data-doiid') + '"]');
										var commentText = 'Moved from ' + currentLevel + ' to ' + supportLevel + ' : ' + $('#support-level-modal #supportcommands').html();
										eventHandler.components.actionitems.postComments(commentText, currentNode.attr('data-id'), ' ', 'data-id', currentNode.attr('data-dashboardview'), 'support');
										var currentNode = $('li.comment.replying').find('.comment-label[data-type="support-level"]').text(supportLevel.toUpperCase());
										$('#support-level-modal').modal('hide');
									}
								},
								error: function (res) {
								}
							});
						}
					},
					error:function(err){

					}
				})
			},
			attachFileToNotes: function (status) {
				console.log(status)
				if (typeof (status.fileDetailsObj.dstKey) == "undefined") return false;
				var fileContent = '<span class="comment-file" contenteditable="false"><a href="/resources/' + status.fileDetailsObj.dstKey + '" target="_blank">' + status.fileDetailsObj.name + status.fileDetailsObj.extn + '</a></span>&nbsp;';
				$('.commenting-field').find('.textarea-content').append(fileContent);
				$('#upload_model').modal('hide');
			},
			postComments: function (comment, cardID, targetNode, source, parentId, pnotesType) {
				if (comment == '' && targetNode) {
					comment = $(targetNode).closest('.modal').find('.textarea-content').html();
				}
				// var contentDiv = $(saveBtn).closest('.modal').find('.textarea-content');
				if (comment == '') return false;
				if (!source || typeof(source) == 'object' || source == undefined) {
					source = 'data-id';
				}
				if ($('.commenting-field .toggleLabels.active').attr('data-type') == 'support'){
					if (! $('.commenting-field .toggleLabels.active').hasClass('status-confirmed')){
						$.ajax({
							type: "GET",
							url: "/api/getdata?customer=" + $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-customer') + "&project=" + $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-project') + "&doi=" + $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-doi') + "&xpath=//workflow/stage[last()]",
							contentType: "application/xml; charset=utf-8",
							dataType: "xml",
							success: function (data) {
								var dataNode = data.documentElement;
								if ($(dataNode).find('job-logs').length > 0 && $(dataNode).find('job-logs log').length > 0) {
									if (! /logged-off/i.test($(dataNode).find('job-logs log').last().find('status').text())){
										var noticationInfo = {};
										noticationInfo.notify = {};
										noticationInfo.notify.notifyTitle = 'Warning';
										noticationInfo.notify.notifyText = 'Article is either opened or in in-active stage by ' + $(dataNode).find('job-logs log').last().find('username').text() + '. Please log-out from the article and try again!';
										noticationInfo.notify.notifyType = 'warning';
										PNotify.removeAll();
										eventHandler.common.functions.displayNotification(noticationInfo);
										return false;
									}else{
										$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').addClass('status-confirmed');
										$('#comments-modal .modal-footer .sendNotes').trigger('click');
									}
								}else{
									$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').addClass('status-confirmed');
									$('#comments-modal .modal-footer .sendNotes').trigger('click');
								}
							},
							error: function(err){

							}
						})
						return false;
					}else{
						pnotesType = 'support';
						var parentToggleNode = $('.commenting-field .toggleLabels.active').parent()
						if ($(parentToggleNode).find('[data-class="cause"]').val() == "" || $(parentToggleNode).find('[data-class="category"]').val() == "" || $(parentToggleNode).find('[data-class="priority"]').val() == ""){
							return false;
						}
						var category = $(parentToggleNode).find('[data-class="category"]').val() + ',' + $(parentToggleNode).find('[data-class="priority"]').val() + ',' + $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-stage-name') + ',level-2';
						comment = '<div class="issue-data"><div class="category-info"><span class="cause">' + $(parentToggleNode).find('[data-class="cause"] option:selected').text() + '</span><span class="category">' + $(parentToggleNode).find('[data-class="category"] option:selected').text() + '</span><span class="priority">' + $(parentToggleNode).find('[data-class="priority"] option:selected').text() + '</span><span class="stage">' + $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-stage-name') + '</span><span class="level">level-2</span></div><div class="issue-notes">' + comment.replace(/(<img[^>]+)>/g, '$1/>') + '</div>';
					}
				}
				if ($('#comment-tabs li[data-rid="all"]').length > 0 && !$('#comment-tabs li[data-rid="all"]').hasClass('active')){
					$('#comment-tabs li[data-rid="all"]').trigger('click');
				}
				var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
				var commentJSON = {
					"content": comment,
					"fullname": $('.username').html(),
					"created": currTime
				}
				if ($('li.comment.replying').length > 0) {
					commentJSON.parent = $('li.comment.replying').attr('data-id');
				}
				var noteType = $(targetNode).hasClass('sendNotes');

				if (pnotesType){
					noteType = pnotesType;
				}else if (noteType){
					noteType = '';
				}else{
					noteType = 'workflow';
				}
				$.ajax({
					type: 'POST',
					url: '/api/save_note',
					processData: false,
					contentType: 'application/json; charset=utf-8',
					data: JSON.stringify({
						"content": commentJSON,
						"doi": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-doi'),
						"customer": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-customer'),
						"project": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-project'),
						"noteType": noteType,
						"notesCount": comment.length
					}),
					success: function (comment) {
						//console.log(comment);
						if(comment=='') return;
						if(comment && comment.message){
							comment = comment.message;
							if(typeof(comment)==='object' && comment.length>0) comment = comment[0];
						}
						var commentDiv = $('<li data-id="' + comment.id + '" class="comment pulse"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + comment.fullname + '</span><span class="comment-time" data-original="' + moment((comment.created) + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '">' + moment((comment.created) + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + comment.content + '</div></div></div></li>');
						if (comment.parent && $('li.comment[data-id="' + comment.parent + '"]').length > 0) {
							var parentID = comment.parent;
							commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
							if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
								$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
							} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
								$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
							} else {
								$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
							}
							$('li.comment').removeClass('replying');
						} else {
							$('#comment-list').prepend(commentDiv);
						}
						setTimeout(function(){$(commentDiv).removeClass('pulse')},1000);
						$(commentDiv)[0].scrollIntoView({ block: 'end',  behavior: 'smooth' });
						//$(commentDiv)[0].scrollIntoView()
						var jobCard = $('.jobCards[data-doiid="' + $('.modal.comments-modal').attr('data-doiid') + '"]');
						if (jobCard.find('.notes-notify').length > 0) {
							var notesNotify = 0;
							$('.comment:not(.reply)').each(function () {
								var parentID = $(this).attr('data-id');
								if ($('.comment.reply[data-parent-id="' + parentID + '"]').length == 0) {
									notesNotify++;
								}
							});
							if (notesNotify == 0) {
								jobCard.find('.notes-notify').text("");
							} else {
								jobCard.find('.notes-notify').text(notesNotify);
							}
						}
						$('.commenting-field.maincontent').find('.textarea-content').html('');
						// To replace the card for notification
						if (noteType) {
							var param = {
								"doi": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-doi'),
								"customer": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-customer'),
								"journal": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-project'),
								"holdFrom": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-store-variable'),
								"parentId": parentId,
								"cardID": cardID
							}
							eventHandler.dropdowns.article.getArticles(param);
							if (noteType == 'support' && $('.commenting-field .toggleLabels.active').attr('data-type') == 'support'){
								var parameters = {
									'customer':$("" + parentId + " [" + source + "='" + cardID + "']").attr('data-customer'),
									'project':$("" + parentId + " [" + source + "='" + cardID + "']").attr('data-project'),
									'doi':$("" + parentId + " [" + source + "='" + cardID + "']").attr('data-doi')
								}
								parameters.stageName = 'Support';
								parameters.holdComment = category;
								eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST")
								$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').addClass('hidden');
								$('.jquery-comments .commenting-field .toggleLabels[data-type="support"]').removeClass('status-confirmed');
								$('.jquery-comments .commenting-field .toggleLabels').removeClass('toggle');
								$('.jquery-comments .commenting-field .toggleLabels').removeClass('active');
								$('.jquery-comments .commenting-field .toggleLabels:first').addClass('active');
								$('.jquery-comments .commenting-field .issue-category-data').addClass('hidden');
							}
						}
					}
				});
			},
			replyComment: function (replyBtn) {
				var commentBox = $(replyBtn).closest('li.comment');
				if (commentBox.hasClass('replying')) {
					commentBox.removeClass('replying')
				} else {
					$('li.comment').removeClass('replying');
					commentBox.addClass('replying');
					$('.commenting-field.maincontent').find('.textarea-content').focus();
				}
			},
			toggleCommentType: function(param, targetNode){
				var parentNode = $(targetNode).parent();
				if (parentNode.find('.toggleLabels:not(.hidden)').length == 1){
					return false;
				}
				if ($(targetNode).hasClass('active')){
					return false;
				}
				parentNode.find('.toggleLabels:not(.hidden)').removeClass('active');
				$(targetNode).addClass('active');
				parentNode.find('.issue-category-data').addClass('hidden');
				if ($(targetNode).attr('data-type') == 'support'){
					parentNode.find('.issue-category-data').removeClass('hidden');
				}
			},
			copyNoteClipboard: function (clipButton) {
				//Added by Anuraja for Notes-copy to clipboard function based on - http://jsfiddle.net/jdhenckel/km7prgv4/3
				var $temp = $("<div id='clipBoardHtml' style='display:none'>");
				$temp.html($(clipButton).closest('li.comment .comment-wrapper').find('.wrapper .content').html());
				$("body").append($temp);
				var str = document.getElementById('clipBoardHtml').innerHTML;
				function listener(e) {
					e.clipboardData.setData("text/html", str);
					e.clipboardData.setData("text/plain", str);
					e.preventDefault();
				}
				document.addEventListener("copy", listener);
				document.execCommand("copy");
				document.removeEventListener("copy", listener);
				$temp.remove();
				var noticationInfo = {};
				noticationInfo.notify = {};
				noticationInfo.notify.notifyTitle = 'Copy to clipboard';
				noticationInfo.notify.notifyText = 'Copied to clipboard';
				noticationInfo.notify.notifyType = 'info';
				PNotify.removeAll();
				eventHandler.common.functions.displayNotification(noticationInfo);
			},
			openFileUpload: function (e) {
				$('#upload_model').remove();
				var customer = $(e).closest('*[data-doiid]').attr('data-customer');
				var project = $(e).closest('*[data-doiid]').attr('data-project');
				var doi = $(e).closest('*[data-doiid]').attr('data-doiid');
				var uploadType = $(e).closest('span[data-message]').attr('data-upload-type');
				var successFunction = $(e).closest('span[data-message]').attr('data-success');
				if (typeof (doi) == "undefined") return false;
				$("body").append('<div id="upload_model" id="upload_modal" role="document" data-customer="' + project + '" data-project="' + project + '" data-doi="' + doi + '" data-upload-type="' + uploadType + '" data-success="' + successFunction + '" class="modal comments-modal-upload"><div class="modal-dialog modal-dialog-centered modal-upload"><div class="modal-content"><div class="modal-header"><h3 class="uploadHeader">Upload ' + uploadType + ' for ' + doi + '<span style="margin-left: 20px;font-size: 12px;background: #fafafa;padding: 3px 10px;color: #313131;border-radius: 6px;display:none;" class="notify"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div id="uploader" class="fileHolder"><div class="fileList" style="margin: 10px;text-align:center;"></div><div class="i-note"><p style="opacity: 0.6;">Drag and drop Files here to Upload</p><p><span class="btn btn-small btn-info" style="padding: 0px !important;"><input type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser" style="padding:0.375rem 0.75rem;">Or Select Files to Upload</span></span></p></div></div><div class="modal-caption hidden" data-type="Striking Image" contenteditable="true">metacontent</div><div class="modal-footer"><span class="btn btn-small btn-success upload_button save disabled highlight-background" data-channel="components" data-topic="actionitems" data-event="click" data-success="eventHandler.components.actionitems.attachFileToNotes" data-upload-type="Files" data-message="{\'funcToCall\': \'uploadFile\', \'param\':this}"><i class="fa fa-upload"> upload</i></span></div></div></div></div>');
				$('#upload_model').modal();
				tempFileList = {};
			},
			//upload file : gets upload path from server
			uploadFile: function (param, targetNode) {
				var tempFileList = {};
				var htmlID = 'upload_model';
				if (param && param.htmlID) {
					htmlID = param.htmlID;
				}
				var processType;
				var keyPath;
				if(param && param.processType && param.processType!=''){
					processType = param.processType;
				}
				if(param && param.keyPath && param.keyPath!=''){
					keyPath = param.keyPath;
				}
				$(targetNode).addClass('disabled');
				var fileHolder = $('#uploader input')[0];
				var file = false;
				if (param && param.file != undefined) {
					file = param.file;
				}
				if (fileHolder && fileHolder.files.length > 0) {
					file = fileHolder.files[0];
				}
				if (tempFileList && Object.keys(tempFileList).length > 0) {
					file = tempFileList[0];
				}
				if (!file) return;
				$('#' + htmlID).attr('data-file-size', file.size);
				$('.notify').show();
				$('.notify').html('Uploading to server');
				var param = {};
				param.htmlID = htmlID;
				if(keyPath && keyPath!=''){
					param.keyPath = keyPath;
				}
				if(processType && processType!=''){
					param.processType = processType;
				}
				param.file = file;
				param.service = $('#' + htmlID).attr('data-upload-type').replace(/\s/g, '').toLocaleLowerCase();
				param.success = $('#' + htmlID).attr('data-success');
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {
						'filename': file.name
					},
					success: function (response) {
						if (response && response.upload_url) {
							var formData = new FormData();
							$.each(response.params, function (k, v) {
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.components.actionitems.uploadToServer(param, targetNode);
						}
					},
					error: function (err) {
						if (param.error && typeof (param.error) == "function") {
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function (param, targetNode) {
				var file = param.file;
				var htmlID = 'upload_model';
				if (param && param.htmlID) {
					htmlID = param.htmlID;
				}
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								$('.notify').html('Uploading to server ' + percentComplete);
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								$('.notify').html('Uploading to server ' + percentComplete);
							}
						}, false);
						return xhr;
					},
					success: function (res) {
						$('.notify').html('Converting for web view');
						kriya = {};
						kriya.config = {};
						kriya.config.content = {};
						kriya.config.content.customer = $('#' + htmlID).attr('data-customer');
						kriya.config.content.project = $('#' + htmlID).attr('data-project');
						kriya.config.content.doi = $('#' + htmlID).attr('data-doi');
						if (res && res.hasChildNodes()) {
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0) {
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
									srcBucket: uploadResult.find('Bucket,bucket').text(),
									srcKey: uploadResult.find('Key,key').text(),
									dstBucket: "kriya-resources-bucket",
									dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
									convert: 'false'
								}
                //Updated by ANuraj to make upload path foe issuemakeup and book
								if(param && param.processType && param.keyPath && param.keyPath!=''){
									if(param.processType=='issueMakeup' ){
										params.dstKey = kriya.config.content.customer + '/' + kriya.config.content.project + '/'+ param.keyPath + '/'+ kriya.config.content.doi + '/' + fileName
									}else if(param.processType=='book' ){
										params.dstKey = kriya.config.content.customer + '/' + kriya.config.content.project + '/'+ param.keyPath + '/'+ kriya.config.content.doi + '/' + fileName
									}
								}
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function (status) {
										status.htmlID = htmlID;
										if (param.success && typeof (window[param.success]) == "function") {
											window[param.success](status, targetNode);
										} else if (typeof (eval(param.success)) == "function") {
											eval(param.success)(status, targetNode);
										}
										//uploadDigest(status)
									},
									error: function (err) {
										if (param.error && typeof (param.error) == "function") {
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			}
		}
	},
	eventHandler.dropdowns = {
		customer: {
			getCustomer: function (param, targetNode) {
				$('.la-container').css('display', 'none');
				$('div.bucket-filter').hide();
				jQuery.ajax({
					type: "GET",
					url: "/api/customers",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						if (msg) {
							var customerHTML = ``;
							if (msg.customer.length == 1) $('#customerselect ul.dropdown-list').attr('data-multiple-customer', 'false');
							else $('#customerselect ul.dropdown-list').attr('data-multiple-customer', 'true');
							for (const customer of msg.customer) {
								if (customer.status == "live") {
									customerHTML += `<li class="nav-link" data-channel="dropdowns" data-topic="project" id="currentProject" data-event="click" data-message="{'funcToCall': 'getProject','param': {'customer': '` + customer.name + `'}}"    data-value="` + customer.name + `">` + customer.fullName + `</li>`
								}
							}
							$('#customerselect ul.dropdown-list').empty();
							$('#customerselect ul.dropdown-list').append(customerHTML);
						} else {
							return null;
						}
					},
					error: function (xhr, errorType, exception) {
						return null;
					}
				});

			},
			issueCustomer: function (customerName, idToReplace, functionInfo) {
				var customers = '';
				if (!idToReplace) idToReplace = '#issueContent #issueCustomer';
				if (!functionInfo) functionInfo = { functionName: 'issueProjects', channel: "dropdowns", topic: "project", event: "click" };
				Object.keys(userData.kuser.kuroles).forEach(function (customer) {
					if (projectLists && projectLists[customer] && projectLists[customer].type && projectLists[customer].type.match(/journal/g)) {
						if (customerName == customer) {
							$(idToReplace).html(customerName);
							customers += '<div class="filter-list active" data-channel="' + functionInfo.channel + '" data-topic="' + functionInfo.topic + '" data-event="' + functionInfo.event + '" data-message="{\'funcToCall\': \'' + functionInfo.functionName + '\',\'param\':{\'customerName\':\'' + customer + '\'}}" value=' + customer + '>' + customer + '</div>';
						} else {
							customers += '<div class="filter-list" data-channel="' + functionInfo.channel + '" data-topic="' + functionInfo.topic + '" data-event="' + functionInfo.event + '" data-message="{\'funcToCall\': \'' + functionInfo.functionName + '\',\'param\':{\'customerName\':\'' + customer + '\'}}" value=' + customer + '>' + customer + '</div>';
						}
					}
				})
				$(idToReplace).html(customers);
				$(idToReplace + " [value='" + customerName + "']").trigger("click");
			},
			assignCustomer: function (customerName, idToReplace, functionInfo) {
				var customers = '';
				if (!idToReplace) idToReplace = '#autoAssignContent #assignCustomer';
				if (!functionInfo) functionInfo = { functionName: 'assignProjects', channel: "dropdowns", topic: "project", event: "click" };
				Object.keys(userData.kuser.kuroles).forEach(function (customer) {
					if (projectLists && projectLists[customer] && projectLists[customer].type && projectLists[customer].type.match(/journal/g)) {
						if (customerName == customer) {
							$(idToReplace).html(customerName);
							customers += '<div class="filter-list active" data-channel="' + functionInfo.channel + '" data-topic="' + functionInfo.topic + '" data-event="' + functionInfo.event + '" data-message="{\'funcToCall\': \'' + functionInfo.functionName + '\',\'param\':{\'customerName\':\'' + customer + '\'}}" value=' + customer + '>' + customer + '</div>';
						} else {
							customers += '<div class="filter-list" data-channel="' + functionInfo.channel + '" data-topic="' + functionInfo.topic + '" data-event="' + functionInfo.event + '" data-message="{\'funcToCall\': \'' + functionInfo.functionName + '\',\'param\':{\'customerName\':\'' + customer + '\'}}" value=' + customer + '>' + customer + '</div>';
						}
					}
				})
				$(idToReplace).html(customers);
				$(idToReplace + " [value='" + customerName + "']").trigger("click");
			},
			showCustomer: function (param, targetNode) {
				// toggle customers dropdown
				if ($('#customerselect ul.dropdown-list').hasClass('hide-dropdown')) {
					$('#customerselect ul.dropdown-list').removeClass('hide-dropdown');
				} else {
					$('#customerselect ul.dropdown-list').addClass('hide-dropdown');
				}
			},
			select: function (param, targetNode) {
				console.log(param)
			},
		},
		project: {
			getProject: function (param, targetNode) {
				// after selecting customer, list projects
				var customer = $(targetNode).val();
				if (customer == undefined || customer == '') {
					return;
				}
				jQuery.ajax({
					type: "GET",
					url: "/api/projects?customer=" + customer,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (projects) {
						if (projects && projects.length > 0 && projects[0]._source && projects[0]._source.projects) {
							var projectHTML = ``;
							projects = projects[0]._source.projects;
							for (const project of projects) {
								projectHTML += '<option id="ajprojectlist" value="' + project.name + '">' + project.fullName + '</option>';
							}
							$('[id="ajproject"]').html('');
							$('[id="ajproject"]').append(projectHTML);
						} else {
							$('[id="ajproject"]').html('');
							return null;
						}
					},
					error: function (xhr, errorType, exception) {
						$('[id="ajproject"]').html('');
						return null;
					}
				});
			},
			issueProjects: function (param, targetNode) {
				if (param.customerName) {
					// $('.dashboardTabs.active #issueCustomer').css('display', 'none');
					$('#issueContent .issueshowBreadcrumb').css('display', 'inline-block');
					$('#issueContent #issueCustomerVal').html(param.customerName);
					$('#issueContent #issueCustomer').children().removeClass('active');
					$('#issueContent #issueProjectVal').html('Project');
					$('#issueContent #projectIssueListVal').html('Issue').hide();
					$('.issue-Lock-screen').addClass('hidden');
					$('.issueLogOff').addClass('hidden');
					$('.issuelocked,.issueInactivePage').addClass('hide');
					$('.issue-Lock-screen').css('display','none')
					$('.showEmpty .watermark-text').html('Please select project <br/>to display Issues');
					$('.showEmpty').show();
					if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')!=''){
						eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
					}
					$('#issueContent #issueProjectVal').html('Project');
					$('.dashboardTabs.active #manuscriptsDataContent').html('');
					$(currentTab + '#manuscriptsData').removeClass('col-8');
					$(currentTab + '#manuscriptsData').addClass('col-12');
					eventHandler.components.actionitems.hideRightPanel();
					$('#issueContent #issueCustomer [value="' + param.customerName + '"]').addClass('active');
					var projectHTML = '';
					var projects = projectLists[param.customerName];
					for (project in projects.value) {
						projectHTML += '<div class="filter-list" data-channel="dropdowns" data-topic="issue" data-event="click" data-message="{\'funcToCall\': \'listProjectIssue\',\'param\':{\'customerName\': \'' + param.customerName + '\',\'projectName\':\'' + projects.value[project] + '\'}}" value=' + projects.value[project] + '>' + projects.name[project] + '</div>';
					}
					$('#issueContent #issueProject').html(projectHTML);
					eventHandler.components.actionitems.initializeAddJob({'customer': param.customerName});
					// $('.dashboardTabs.active #issueProject').css('display', 'block');
				}
				$('.save-notice').addClass('hide');
			},
			assignProjects: function (param, targetNode) {
				if (param.customerName) {
					// $('.dashboardTabs.active #issueCustomer').css('display', 'none');
					$(currentTab + '.assignshowBreadcrumb').css('display', 'inline-block');
					$(currentTab + ' #assignCustomerVal').html(param.customerName);
					$(currentTab + ' #assignCustomer').children().removeClass('active');
					$(currentTab + ' #assignProjectVal').html('Project');
					$(currentTab + '.progressassign').addClass('hidden')
					$(currentTab + '#rightPanelContainer').show()
					$(currentTab + 'h2').show()
					$(currentTab + ' #projectStageListVal').html('Stage').hide();
					$('.showEmpty .watermark-text').html('Please select project <br/>to display stages');
					$('.showEmpty').show();
					$(currentTab + '#sort, '+currentTab +'.assign,'+currentTab +'h3,'+currentTab + '.assign-users').hide()
					$(currentTab + '#assignProjectVal').html('Project');
					$('#searchAssign').css('display', 'none')
					$(currentTab + ' #manuscriptsDataContent').html('');
					$(currentTab + '#assignedManuscript li').remove()

					eventHandler.components.actionitems.hideRightPanel();
					$(currentTab + '#manuscriptsData').removeClass('col-12').addClass('col-8');
					$('#autoAssignContent #assignCustomer [value="' + param.customerName + '"]').addClass('active');
					var projectHTML = '';
					var projects = projectLists[param.customerName];
					for (project in projects.value) {

						projectHTML += '<div class="filter-list" data-channel="dropdowns" data-topic="assign" data-event="click" data-message="{\'funcToCall\': \'assignProjectIssue\',\'param\':{\'customerName\': \'' + param.customerName + '\',\'projectName\':\'' + projects.value[project] + '\'}}" value=' + projects.value[project] + '>' + projects.name[project] + '</div>';
					}
					$('#autoAssignContent #assignProject').html(projectHTML);
					if (!param.customerName) {
						return;
					}

					$('#autoAssignContent h2').hide();
					$('.Recommended').addClass('hidden')
					$('.save-notice').addClass('hide');
					$('.showEmpty .watermark-text').html('Please select project or stage <br/>to display list view');
					$('.showEmpty').show();
					$(currentTab + '#manuscriptsDataContent').html('');
					$(currentTab + '#assignedManuscript li').remove()

					eventHandler.components.actionitems.hideRightPanel();
					$('.dashboardTabs.active #assignCustomer').removeAttr('style');
					//$('#autoAssignContent #assignProjectVal').html(param.projectName);
					$(currentTab + '#projectStageListVal').html('Stage').show();
					$(currentTab + '#assignProject').children().removeClass('active');
					//$('#autoAssignContent #assignProject [value="' + param.projectName + '"]').addClass('active');
					var issueHTML = ''
					var userRole = userData.kuser.kuroles[param.customerName]['role-type'];
					var userAccess = userData.kuser.kuroles[param.customerName]['access-level'];
					if (userRole == 'production' && (userAccess == 'manager' || userAccess == "admin"))
						var issueList = ['Pre-editing', 'Typesetter Review', 'Typesetter Check', 'Validation Check', 'Final Deliverables']
					else if (userRole == 'publisher' && (userAccess == 'manager' || userAccess == "admin"))
						issueList = ['Publisher Review', 'Publisher Check']
					else if (userRole == 'copyeditor' && (userAccess == 'manager' || userAccess == "admin"))
						issueList = ['Copyediting', 'Copyediting Check']
					if (issueList) {
						for (issue of issueList) {
							issueHTML += '<div class="filter-list" data-customer="' + param.customerName + '" data-project="' + + '" data-file="' + issue + '" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'defaultdate\',\'param\':{\'type\':  \'journal\',\'projectName\':\'' + param.projectName + '\',\'customerName\':\'' + param.customerName + '\'}}" value=' + issue + '>' + issue + '</div>';
						}
						$('#autoAssignContent #projectStageList').html(issueHTML);
						$('#autoAssignContent .assignList').css('display', 'inline-block');
					} else {
						$('.showEmpty .watermark-text').html('Issue not uploaded');
						$('#autoAssignContent #projectStageList').html('');
					}
					eventHandler.components.actionitems.initializeAddJob({ 'customer': param.customerName });
					// $('.dashboardTabs.active #issueProject').css('display', 'block');
				}
				$('.save-notice').addClass('hide');
			},
			defaultdate: function (param, targetNode) {
				$('.showEmpty').hide();
				$('#userList option:nth-child(1)').prop("selected", true);

				$('#assignProjectVal').removeClass('highlightFilter')
				$('#assignProject div').removeClass('active')
				$('#autoAssignContent #date').show();
				$(currentTab + '.assign-users').show()
				$('.Recommended').removeClass('hidden')
				$(currentTab + '#rightPanelContainer').show()
				$(currentTab + 'h2').show()
				if(param.projectName == 'undefined')
				$('#autoAssignContent #assignProjectVal').html("All");

				$('#unAssignedTemplate #pagin').removeClass('hidden')

				$('.save-notice').addClass('hide');
				$("#assignDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (param.type == 'journal') {
					$(currentTab + '#projectStageListVal').html(targetNode.attr('data-file'));
					$(currentTab + '#projectStageList').children().removeClass('active');
					$(currentTab + ' #projectStageList [data-file="' + targetNode.html() + '"]').addClass('active');
				}
				if(param.projectName == "undefined")
				{
					param.projectName = '*';
				}
				if (!param.fromDate)
					var fromDate, toDate = '';
				if (param.fromDate == undefined || param.fromDate == '') {
					param.fromDate = moment().format('YYYY-MM-DD');
					$("#assignDatePicker").val(moment(param.fromDate).format('D MMM, YY'));
				}
				if (param.toDate == undefined || param.toDate == '') {
					param.toDate = moment().format('YYYY-MM-DD');
					$("#assignDatePicker").val(moment(param.toDate).format('D MMM, YY'));
				}
				$(currentTab + '#manuscriptsData').removeClass('col-12').addClass('col-8');
				$('#projectStageListVal').addClass('highlightFilter')
				// $('#assignProjectVal').html('All')


				eventHandler.assignmentSummary.showUnassigned(param, targetNode)


			},
			listProject: function (param, targetNode) {
				$('.fs-wrap').removeClass('fs-open');
				$('.fs-dropdown').addClass('hidden');
				$('.issue-Lock-screen').addClass('hidden')
				$('.issue-Lock-screen').css('display','none')
				window.fSelect.active_el = null;
				window.fSelect.active_id = null;
				window.fSelect.last_choice = null;
				if (param.val) {
					var val = $(param.val).val();
					param.customer = val;
				}
				var customer = param.customer;
				if (customer == undefined || customer == '') {
					return;
				}
				if (typeof customer == 'string') {
					customer = customer.split(',');
				}
				eventHandler.components.actionitems.initializeAddJob(param);
				var projectHTML = ``;
				var projectList = [];
				currentCustomers = [];
				customer.forEach(function (customerName) {
					currentCustomers.push(customerName);
					jQuery.ajax({
						type: "GET",
						url: "/api/projects?customerName=" + customerName,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (msg) {
							if (msg) {
								var projects;
								if (msg.project.constructor.name == 'Array') {
									projects = msg.project;
								} else {
									projects = [msg.project];
								}
								projectLists[customer] = [];
								for (const project of projects) {
									projectList.push(project);
									projectLists[customer].push(project.name);
									projectHTML += '<option id="dsprojectlist" value="' + project.name + '">' + project.fullName + '</option>';
								}
							} else {
								$('#ajproject').html('');
								return null;
							}
						},
						error: function (xhr, errorType, exception) {
							$('#ajproject').html('');
							return null;
						}
					});
				})
				$('.projectList').html('');
				$('.projectList').append(projectHTML);
				$('.projectList').fSelect();
				if (param.onsuccess) {
					eventHandler.highChart[param.onsuccess]({
						'customer': customer,
						'project': projectList
					});
				}
			},
			updateChart: function (param, from) {
				$('.showEmpty').hide();
				$('[data-type="chart"]').closest('.animated').show();
				$('#chartSelection .showBreadcrumb').css('display','inline-block');
				// if ($('#customerVal').text() == "Customer") {
				// 	return;
				// }
				$('[data-type="chart"]').css('background-color', 'white');
				$('[data-type="chart"]').html('<div class="chartLoader"></div>');
				$('#chartCustomer').hide();
				setTimeout(function () {
					$('#notifyprojectlist').find('[data-last-sele]').removeAttr('data-last-sele');
					$('#notifyprojectlist').hide();
					if (param.node) {
						$(param.node).closest('#chartSelection').find('#projectVal').html('Project');
					}
					var customer = [],
						project = [];
					$("#chartCustomer").children('.active').each(function (name) {
						customer.push($(this).text())
					});
					$("#chartProject").children('.active').each(function (name) {
						if ($(this).text() != 'All') {
							project.push($(this).attr('data-project'));
						}
					});
					if (param.onsuccess) {
						if (from == 'showManuscript' || $(from).text() == 'Yes' || $(from).closest('#dashboardContent').hasClass('active')) {
							eventHandler.highChart[param.onsuccess]({
								'customer': customer,
								'project': project
							});
							$('#dashboardContent #customerVal').text(customer[0]);
						}
						if (Object.keys(usersList).length == 0) {
							eventHandler.components.actionitems.getUserDetails(customer);
						}
						if (from != 'showManuscript') {
							$('.showBreadcrumb').css('display', 'inline-block');
							var roleAccess = (/^(production|copyeditor)$/.test(userData.kuser.kuroles[customer[0]]['role-type']));
							if (roleAccess) {
								eventHandler.mainMenu.toggle.showManuscript({
									'workflowStatus': 'in-progress',
									'urlToPost': 'getAssigned',
									'customer': customer[0],
									'custTag': '#filterCustomer',
									'project': 'project',
									'prjTag': '#filterProject',
									'user': 'user',
									'optionforassigning': true,
									'version': 'v2.0'
								}, undefined, 'updateChart');
							} else {
								eventHandler.mainMenu.toggle.showManuscript({
									'workflowStatus': 'in-progress OR completed',
									'project': 'project',
									'customer': customer[0],
									'excludeStage': 'inCardView',
									'optionforassigning': true,
									'version': 'v2.0'
								}, undefined, 'updateChart');
							}
						}
					}
					$('#chartCustomer').css('display', '');
					$('.la-container').css('display', 'none');
				}, 0);
			},
			// To create customer list and project list for dashboard
			listCustomer: function (param, targetNode) {
				var userObj = JSON.parse($("#userDetails").attr('data'));
				var clients = [],
					clientHTML = '',
					chartClient = '',
					projectList = '';
					uploadXMLClient = '';
				Object.keys(userObj.kuser.kuroles).forEach(function (key, index) {
					clients.push(key);
				});
				if (!userData || !userData.kuser || !userData.kuser.kuemail) {
					return null;
				}
				var fields = ["name", "projects", "type"];
				jQuery.ajax({
					type: "GET",
					url: "/api/projects?includeFields=" + fields,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						if (Object.keys(msg).length) {
							msg.map((customer) => {
								customer = customer._source;
								projectLists[customer.name] = {};
								projectLists[customer.name]['type'] = (customer.type == undefined) ? 'journal' : customer.type;
								projectLists[customer.name]['name'] = [];
								projectLists[customer.name]['value'] = [];
								if(customer && customer.projects && customer.projects.length>0){
								customer.projects.map((projectField) => {
									var customerType = (customer.type == 'book') ? 'fullName' : 'name';
									projectLists[customer.name]['name'].push(projectField[customerType]);
									projectLists[customer.name]['value'].push(projectField.name);
								})
								}
							})
							var i = 0;
							for (const client of clients) {
								if (projectLists && projectLists[client]) {
									var clientType = projectLists[client].type ? projectLists[client].type : 'journal';
									if (i == 0) {
										clientHTML += '<option selected="true" value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
										uploadXMLClient += '<option value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
										if (clients.length > 1) {
											chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
										} else {
											chartClient += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
											$('[data-type="chart"]').css('background-color', 'white');
											eventHandler.dropdowns.project.chartProject(client);
										}
									} else {
										clientHTML += '<option value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
										chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,   \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
										uploadXMLClient += '<option data-customer="' + client + '" data-type="' + clientType + '">' + client + '</option>'
									}
								
								}
								i++;
							}
							$('.customerList').html('');
							$('#chartCustomer').html('');
							$('.customerList').append(clientHTML);
							$('#chartCustomer').append(chartClient);
							$('#uploadXML_modal .uploadXML .customer select').append(uploadXMLClient)
							// $('#chartCustomer').fSelect({
							// 	placeholder: clients[0],
							// 	numDisplayed: 5,

							// 	searchText: 'Search',
							// 	showSearch: true
							// });
							if (clients.length == 1) {
								$('#sort [sort-option="Customer"]').remove();
								eventHandler.dropdowns.project.updateChart({
									'onsuccess': 'makeCharts'
								})
							} else {
								$('.showEmpty').show();
								$('[data-type="chart"]').closest('.animated').hide();
								var customer = {
									"customer": "Customer"
								};
								eventHandler.filters.populateNavigater(customer);
							}

						}
					},
					error: function (xhr, errorType, exception) {
						$('#ajproject').html('');
						return null;
					}
				});
			},
			reportCustomerProject: function (customerName) {
				var customer = '';
				var activeCustomer = '';
				$('#reportSelection #reportCustomer').html('');
				if (projectLists) {
					Object.keys(userData.kuser.kuroles).forEach(function (custm) {
						if (customerName == custm) {
							customer += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
							activeCustomer = '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
							$('#reportCustomerVal').text(customerName);
						} else {
							customer += '<div class="filter-list" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
						}
					})
					$('#reportSelection #reportCustomer').html(customer);
				}
				if (activeCustomer) {
					eventHandler.dropdowns.project.reportSelection(customerName, '', activeCustomer);
					$('#reportSelection').find('.reportshowBreadcrumb').css('display', 'inline-block');
				}

			},
			reportSelection: function (customer, projectName, targetNode, fromDate, toDate) {
				$('#dashboardReports .tableFixHead').removeClass('tableFixHead')
				var project = '';
				$(currentTab + 'table,' +currentTab + '#filterCount,' + currentTab + '#filterCount1,'+ currentTab + '#totalCountForDaily,' + currentTab + '#filterTime,' + currentTab +'#totalCountForHold,' + currentTab + '.tfoot,'+currentTab + '.averageTime').html('')
				$(currentTab + '.reportExportExcel').remove()
				if (targetNode) {
					$('#reportContent #date').show();
					if (!projectName) {
						$('#reportSelection #reportProject').html('');
						$(targetNode).siblings().removeClass('active');
						$(targetNode).parents('#reportSelection').find('#reportProjectVal').text('Project');
						$(targetNode).parent().siblings('#reportCustomerVal').text(customer);
						$(targetNode).addClass('active');
						$(targetNode).closest('#reportSelection').find('.reportshowBreadcrumb').css('display', 'inline-block');
						if (customer && projectLists && projectLists[customer] && projectLists[customer].name && projectLists[customer].value) {
							project += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + customer + '\',\'All\', this)" data-channel="dropdowns" data-topic="project" data-event="click" value=\'All\'>All</div>'
							projectLists[customer].value.forEach(function (proj, index) {
								project += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + customer + '\',\'' + projectLists[customer].value[index] + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" value=' + projectLists[customer].value[index] + '>' + projectLists[customer].name[index] + '</div>'
							})
						}
						$('#reportSelection #reportProject').html(project);
					} else {
						if (projectName == 'All') {
							$(targetNode).siblings().addClass('active');
							projectName = '*';
							$(targetNode).parent().siblings('#reportProjectVal').text('All');
						} else {
							$(targetNode).siblings().removeClass('active');
							$(targetNode).parent().siblings('#reportProjectVal').text(projectName);
						}
						$(targetNode).addClass('active');
					}
				}
				$('.showEmpty').hide();
				if (fromDate == undefined || fromDate == '') {
					fromDate = moment().format('YYYY-MM-DD');
					$("#fromDatePicker").val(moment(fromDate).format('D MMM, YY'));
					$("#fromDatePicker").data('datepicker').selectDate(new Date());
				}
				if (toDate == undefined || toDate == '') {
					toDate = moment().format('YYYY-MM-DD');
					$("#fromDatePicker").val(moment(toDate).format('D MMM, YY'));
					$("#fromDatePicker").data('datepicker').selectDate(new Date());
				}
				var time_zone = getTimeZone();
				var time_zone_name = Intl.DateTimeFormat().resolvedOptions().timeZone
				if($(currentTab).attr('id') == 'reportContent'){
				$('#reportContent #dashboardReports').css('display','')
				}

				$(document).on('click','#dashboardReports .row h4:not(>button)',function(evt){
					$('.reportExportExcel').click(false);
					var target = $(this)
					var customer = $('#reportCustomerVal').text()
					if($('#reportProjectVal').text() == 'Project')
					var projectName = '*';
					else
					var projectName = $('#reportProjectVal').text();
					var sDate = $('#fromDatePicker').val();
					var eDate = $('#fromDatePicker').val();
					sDate = moment(sDate).format('YYYY-MM-DD');
					eDate = moment(sDate).format('YYYY-MM-DD');
					var toToggle = $(target).parent().parent().attr('id') ? '#'+$(target).parent().parent().attr('id') : '#'+$(target).parent().attr('id')
					if(toToggle == '#dailyReport' || toToggle == "#holdReport")
					$('#dashboardReports '+ toToggle+ 'Div').toggleClass('tableFixHead')
					$(toToggle + ' #totalCountForDaily,' + toToggle + ' .averageTime,' + toToggle + ' #totalCountForHold').toggle()
					$(toToggle + ' table,'+ toToggle + ' .reportExportExcel,' + toToggle + ' .tfoot').toggle()
					eventHandler.tableReportsSummary.getCustomerData({ 'customer': customer, 'project': projectName,'target':target,'roleType': userData.kuser.kuroles[customer]['role-type'], 'accessLevel': userData.kuser.kuroles[customer]['access-level'], 'fromDate': sDate, 'toDate': eDate, 'time_zone': time_zone, 'time_zone_name': time_zone_name });
					evt.stopImmediatePropagation();
				});

			},
			// To show date range picker in report tab
			reportDateRangePicker: function (targetNode) {
				var sDate, eDate;
				$('#dashboardReports .tableFixHead').removeClass('tableFixHead')
				$("#fromDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (targetNode) {
					sDate = $(targetNode).siblings('input#fromDatePicker').val();
					eDate = $(targetNode).siblings('input#fromDatePicker').val();
					sDate = moment(sDate).format('YYYY-MM-DD');
					eDate = moment(eDate).format('YYYY-MM-DD');
					if (sDate > eDate || sDate == undefined || eDate == undefined || sDate == 'Invalid date' || eDate == 'Invalid date') {
						var notifyParam = {
							'notify': {
								'notifyTitle': 'Invalid Date Range',
								'notifyText': 'Please Select Valid Date Range',
								'notifyType': 'error'
							}
						}
						eventHandler.common.functions.displayNotification(notifyParam);
					} else {
						var customer = $('#reportCustomerVal').text();
						var project = ''
						if ($('#reportProjectVal').text().match(/Project|All/g)) {
							project = '*';
						} else {
							project = $('#reportProjectVal').text();
						}
						$(currentTab + 'table,' +currentTab + '#filterCount,' + currentTab + '#filterCount1,'+ currentTab + '#totalCountForDaily,' + currentTab + '#filterTime,' + currentTab +'#totalCountForHold,' + currentTab + '.tfoot,'+currentTab + '.averageTime').html('')
						$(currentTab + '.reportExportExcel').remove()
						eventHandler.dropdowns.project.reportSelection(customer, project, '', moment(sDate).format('YYYY-MM-DD'), moment(eDate).format('YYYY-MM-DD'))
					}
				} else {
					$("#fromDatePicker").data('datepicker').selectDate(new Date())
					//$("#fromDatePicker").data('datepicker').selectDate(new Date())
				}
			},
			assignDateRangePicker: function (targetNode) {
				var sDate, eDate;
				$("#assignDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (targetNode) {
					sDate = $(targetNode).siblings('input#assignDatePicker').val();
					eDate = $(targetNode).siblings('input#assignDatePicker').val();
					sDate = moment(sDate).format('YYYY-MM-DD');
					eDate = moment(eDate).format('YYYY-MM-DD');
					if (sDate > eDate || sDate == undefined || eDate == undefined || sDate == 'Invalid date' || eDate == 'Invalid date') {
						var notifyParam = {
							'notify': {
								'notifyTitle': 'Invalid Date Range',
								'notifyText': 'Please Select Valid Date Range',
								'notifyType': 'error'
							}
						}
						eventHandler.common.functions.displayNotification(notifyParam);
					} else {
						var customer = $('#assignCustomerVal').text();
						var project = ''
						if ($('#assignProjectVal').text().match(/Project|All/g)) {
							project = '*';
						} else {
							project = $('#assignProjectVal').text();
						}
						var stageName = $('#projectStageListVal').text()
						var param = {
							"customerName" : customer,
							"projectName" : project,
							"fromDate" : moment(sDate).format('YYYY-MM-DD'),
							"toDate":	moment(eDate).format('YYYY-MM-DD'),
							"stageName" : stageName
						}
						eventHandler.dropdowns.project.defaultdate(param)
					}
				} else {
					$("#assignDatePicker").data('datepicker').selectDate(new Date())
					//$("#fromDatePicker").data('datepicker').selectDate(new Date())
				}
			},
			emailDateRangePicker: function (targetNode) {
				var sDate, eDate;
				$("#emailDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (targetNode) {
					sDate = $(targetNode).siblings('input#emailDatePicker').val();
					eDate = $(targetNode).siblings('input#emailDatePicker').val();
					sDate = moment(sDate).format('YYYY-MM-DD');
					eDate = moment(eDate).format('YYYY-MM-DD');
					if (sDate > eDate || sDate == undefined || eDate == undefined || sDate == 'Invalid date' || eDate == 'Invalid date') {
						var notifyParam = {
							'notify': {
								'notifyTitle': 'Invalid Date Range',
								'notifyText': 'Please Select Valid Date Range',
								'notifyType': 'error'
							}
						}
						eventHandler.common.functions.displayNotification(notifyParam);
					} else {
						var customer = $('#emailsCustomerVal').text();
						var param = {
							"customerName": customer,
							"fromDate": moment(sDate).format('YYYY-MM-DD'),
							"toDate": moment(eDate).format('YYYY-MM-DD'),
						}
						eventHandler.mainMenu.toggle.showsendMails(param,'','')
					}
				} else {
					$("#assignDatePicker").data('datepicker').selectDate(new Date())
					//$("#fromDatePicker").data('datepicker').selectDate(new Date())
				}
			},
			chartProject: function (param, targetNode) {
				//$('#dashboardContent #customerVal').text(param);
				if (targetNode) {
					$(targetNode).addClass('active');
					$($(targetNode).siblings()).removeClass('active');
				}
				var projectList = '';
				chartProject = [];
				chartProject = projectLists[param];
				$('#chartProject').html('');
				projectList += '<div class="filter-list active" id="allproject" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this, \'attributeToCheck\':\'data-project\'})">All</div>';
				// for (var proj in chartProject) {
				// 	projectList += '<div class="filter-list active" data-project="' + chartProject[proj] + '" onclick="eventHandler.filters.projectFilters({\'project\':\'' + chartProject[proj] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})">' + chartProject[proj] + '</div>';
				// }
				if(chartProject && chartProject['name'] && chartProject['value']){
					chartProject['name'].forEach( function(project, count){
						projectList += '<div class="filter-list active" data-project="' + chartProject['value'][count] + '" onclick="eventHandler.filters.projectFilters({\'project\':\'' + chartProject['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})">' + project + '</div>';
					})
				}
				$('#chartProject').append(projectList);
			},
			toggleProject: function (param, targetNode) {
				// toggle project dropdown
				if ($('#projectselect ul.dropdown-list').hasClass('hide-dropdown')) {
					$('#projectselect ul.dropdown-list').removeClass('hide-dropdown');
				} else {
					$('#projectselect ul.dropdown-list').addClass('hide-dropdown');
				}
			},
			select: function (param, targetNode) {
				console.log(param)
			}
		},
		stage: {
			toggleStageFilter: function (param, targetNode) {
				// toggle filter dropdown - stageowner
				var ulNode = $(targetNode).siblings('div.dropdown').find('ul');
				if (ulNode.hasClass('hide-dropdown')) {
					ulNode.removeClass('hide-dropdown');
				} else {
					ulNode.addClass('hide-dropdown');
				}
			},
			plotNavFilter: function (param, targetNode) {
				var navHTML = ``;

				// code block to add stage owners article count dropdown
				for (const stageOwner in stageOwners) {
					if (stageOwners.hasOwnProperty(stageOwner)) {
						var navListTotal = 0;
						var navListHTML = ``;

						for (const owner in stageOwners[stageOwner]) {
							if (stageOwners[stageOwner].hasOwnProperty(owner) && stageOwners[stageOwner][owner]['article-count']) {
								navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${owner}','filterBy':'stageOwners'}}"    data-value="${owner}">${owner}:${stageOwners[stageOwner][owner]['article-count']}</li>`;
								navListTotal += stageOwners[stageOwner][owner]['article-count'];
							}
						}

						navHTML += `<span class="stage-filter">
							<span data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >${stageOwner} (${navListTotal}) <i class="fa fa-caret-down" aria-hidden="true"></i>
							</span>
							<div class="dropdown">
								<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
							</div>
						</span>`;

					}
				}


				// code block to add article type count dropdown
				{
					var navListTotal = 0;
					var navListHTML = ``;
					for (const articleType in articleTypesCount['article-type']) {
						if (articleTypesCount['article-type'].hasOwnProperty(articleType)) {
							navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="articleType" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${articleType}','filterBy':'articleTypesCount'}}"  >${articleType}:${articleTypesCount['article-type'][articleType]}</li>`;
							navListTotal += articleTypesCount['article-type'][articleType];
						}
					}

					navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Article Type (${navListTotal}) <i class="fa fa-caret-down" aria-hidden="true"></i>
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
						</div>
					</span>`;
				}

				$('div.navFilter').empty();
				$('div.navFilter').html(navHTML);
			},
			filterArticle: function (param, targetNode) {
				// filter article based on stage owners and article type
				var filteredArticleIDs = filterData[param.filterOwner][param.filter]['article-data-id'];
				$('div.msCard.card[data-id]').hide();
				$('[data-id=card-' + filteredArticleIDs.join('],[data-id=card-') + ']').show();
				// console.log();
			},
			smartFilterArticle: function (param, targetNode) {
				// filter article based on stage owners and article type
				var filteredArticleIDs = filterData[param.filterOwner][param.filter]['article-data-id'];
				$('div.msCard.card[data-id]').hide();
				$('[data-id=card-' + filteredArticleIDs.join('],[data-id=card-') + ']').show();
				// console.log();
			}
		},
		article: {
			showArticles: function (param, targetNode, recursive) {
				//it should not call directly , needs to call by showManuscript
				// after selecting project, list articles
				$('.save-notice').addClass('hide');
				cardData = [];
				if (param) {
					if ($(targetNode).parent().attr('id') == 'filterStatus') {
						$(targetNode).parent().siblings().html($(targetNode).text());
						$(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					} else if ($(targetNode).attr('id') == 'refresh') {
						// $(targetNode).closest('.newStatsRow').find('#statusVal').html($(targetNode).closest('.newStatsRow').find('#filterStatus .active').text())
						// $(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					} else {
						// $(targetNode).closest('.newStatsRow').find('#statusVal').html($(targetNode).closest('.newStatsRow').find('#filterStatus .active').text())
						$(targetNode).closest('.newStatsRow').find('#statusVal').html('WIP')
						$(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					}
					if ($(targetNode).parent().attr('id') == 'filterCustomer') {
						$('#filterStatus').children().removeClass('active');
						$('#filterStatus').children().first().addClass('active');
					}
					if (!param.from || param.from == undefined) {
						param.from = 0;
					}
					if (!param.size || param.size == undefined) {
						param.size = 500;
					}
					if (param.excludeStage) {
						param.excludeStageArticles = excludeStage[param.excludeStage].join(' OR ');
					}
					param.user = 'userName';
					param.stageName = 'stageName';
					if (!param.customer || param.customer == 'first' || param.customer == 'Customer') {
						try {
							param.customer = Object.keys(JSON.parse($('#userDetails').attr('data')).kuser.kuroles)[0];
						} catch (e) {
							console.log(e);
						}
					}
					if (!param.customer || (param.customer == 'selected' && param.custTag)) {
						try {
							var cs = [];
							$(param.custTag + ' .active').each(function (s) {
								cs.push($(this).text());
							});
							param.customer = cs.join(' OR ');
						} catch (e) {
							console.log(e);
						}
					}
					if (param.customer == 'customer' || selectedCustomer != param.customer) {
						eventHandler.components.actionitems.getUserDetails(param.customer);
					}
					if (param.optionforassigning) {
						eventHandler.dropdowns.article.showAssignOptions(param, targetNode);
					}
					if (!param.project) {
						param.project = 'project';
					}
					$(targetNode).addClass('active');
					$(targetNode).siblings().removeClass('active');
					eventHandler.dropdowns.project.toggleProject(param, targetNode);
					if (recursive == undefined) {
						data = {};
					}
					if (!param.customer.match(/customer/i)) {
						if (userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['access-level']) {
							var roleType = userData.kuser.kuroles[param.customer]['role-type'];
							var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
							// For Copyeditors login
							if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].access.indexOf(accessLevel) > -1 && param.urlToPost.match(/getUnassigned/gi)) {
								var userDetails;
								$.ajax({
									type: "GET",
									url: "/api/getuserdata?customer=" + param.customer,
									async: false,
									dataType: 'json',
									success: function (respData) {
										if (respData) {
											userDetails = respData.find( function (user) {
												return user.email === userData.kuser.kuemail;
											})
										}
									},
									error: function (respData) {
										$('.la-container').css('display', 'none');
									}
								});
								var userPageCount = userDetails.roles.find( function (role) {
									return role['customer-name'] === param.customer
								})
								// param['pageCount'] = Number(userData.kuser.kuroles[param.customer]['page-limit']) - Number(userData.kuser.kuroles[param.customer]['assigned-pages']);
								param['pageCount'] = Number(userPageCount['page-limit']) - Number(userPageCount['assigned-pages']);
								param['fromWordCount'] = 0;
							}
						}
					}
					$('#projectselect span[data-label="true"]>span').text($(targetNode).text());
					$('div.bucket-filter').show();
				}
				if (param.customer && param.customer != 'customer') {
					selectedCustomer = param.customer;
				}
				//To raise query for first time and if we select different customer.
				if (manuScriptCardData.length == 0 || manuScriptCardData.length == undefined || param.stage == 'Support' || recursive == 1) {
					$.ajax({
						type: "POST",
						url: '/api/getarticlelist',
						data: param,
						dataType: 'json',
						success: function (respData) {
							if (!respData || respData.length == 0) {
								$('#manuscriptsDataContent').html('');
								$('#manuscriptContent .filterOptions').hide();
								$('div.navFilter').empty();
								if (selectedCustomer) {
									param = {};
									param.customer = selectedCustomer;
									eventHandler.filters.populateNavigater(param);
								}
								$('#msYtlCount, #msCount').text('0');
								$('.la-container').css('display', 'none');
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								return;
							}
							var oldTotal = parseInt($('#msYtlCount').text());
							var currCount = respData.hits.length;
							if (oldTotal == NaN || oldTotal == undefined || isNaN(oldTotal) || oldTotal == 0) {
								oldTotal = 0;
								data = {};
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['supportlevel'] = {};
								data.filterObj['others'] = {};
							}
							if (param.stage == 'Support') {
								respData.hits = respData.hits.sort(function (a, b) {
									a = a._source.stageName.toUpperCase();
									b = b._source.stageName.toUpperCase();
									if ((b) < (a)) {
										return 1;
									} else {
										return -1;
									}
								})
								Array.prototype.push.apply(supportData, respData.hits);
							} else {
								Array.prototype.push.apply(manuScriptCardData, respData.hits);
							}
							if(userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['skill-level'])
							var skillLevel = userData.kuser.kuroles[param.customer]['skill-level'];
							if(param.urlToPost == "getUnassigned"){
							    for(var i in respData.hits){
									if(respData && respData.hits && respData.hits[i]._source && respData.hits[i]._source["lang-attention-paper"] && respData.hits[i]._source["lang-attention-paper"] == 'Yes'){
										if((roleType == "copyeditor") && ((accessLevel == 'manager') ||(accessLevel == "vendor" && respData.hits[i]._source.stageName && respData.hits[i]._source.stageName == "Copyediting" && skillLevel == 'LAPExpert'))){
											data.info = respData.hits.filter(function(){return true;});
										}
										else{
											delete respData.hits[i];
											data.info = respData.hits.filter(function(){return true;});
										}
						}
						else{
							data.info = respData.hits.filter(function(){return true;});
						}
						}
					}
							data.dconfig = dashboardConfig;
							var totalMsCount = respData.total;
							data.info = respData.hits.filter(function(){return true;});
							data.disableFasttrack = disableFasttrack;
							data.removeEditIcon = false;
							if (param.stage != 'Support' && roleType.match(/copyeditor/g) && accessLevel.match(/vendor/g)) {
								// To disable the action buttons in card for CE vendor
								data.ceVendor = true;
								if (param.urlToPost.match(/getUnassigned/g)) {
									data.removeEditIcon = true;
								}
							}
							data.userDet = JSON.parse($('#userDetails').attr('data'));
							data.count = oldTotal;
							if (param && param.stage != 'Support') {
								var userRole = userData.kuser.kuroles[param.customer]['role-type'];
								var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
								// For Copyeditor -> Vendor In unassigned Bucket the first card should be pickable and other cards are to be in disable mode.
								if (param && param.urlToPost && param.urlToPost.match(/getUnassigned/g) && cardDisable && cardDisable[userRole] && cardDisable[userRole][userAccess] && cardDisable[userRole][userAccess]['addClassName']) {
									data.disable = cardDisable[userRole][userAccess]['addClassName'];
								}
							}
							manuscriptsData = $('#manuscriptsDataContent');
							var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
							manuscriptsData.append(pagefn(data));
							// $(currentTab  +'#manuscriptsData').height(window.innerHeight - $(currentTab  +'#manuscriptsData').position().top - $('#footerContainer').height());
							$(currentTab  +'.windowHeight').height(window.innerHeight - ($(currentTab + '.layoutView').offset().top + 50) - $('#footerContainer').height());
							$(currentTab  +'.windowHeight').css('overflow', 'auto');
							$('.msCount.loading.hidden').removeClass('hidden');
							if ((currCount + oldTotal) < totalMsCount) {
								$('.msCount').addClass('loading').removeClass('loaded');
								$('.la-container').css('display', 'none');
								$('#msYtlCount').text(data.info.length + oldTotal);
								$('#msCount').text(totalMsCount);
								$('#msYtlCount').removeClass('hidden');
								param.from = currCount + oldTotal;
								setTimeout(function () {
									eventHandler.dropdowns.article.showArticles(param, targetNode, 1);
								}, 0)
							} else {
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								$('#msCount').text(totalMsCount);
								param.filterObj = data.filterObj;
								if (!param.customer || param.customer == 'first') {
									eventHandler.filters.populateNavigater(param);
								} else {
									eventHandler.filters.populateNavigater(param);
								}
								eventHandler.filters.populateFilters(data.filterObj, param.stage);
								// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
								if (!param.customer.match(/customer|first/gi)) {
									var userRole = userData.kuser.kuroles[param.customer]['role-type'];
									var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
									if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
										eventHandler.filters.populateSort();
									}
								}
								if (param.refreshedProject) {
									eventHandler.filters.projectFilters({
										'project': $(targetNode).closest('#manuscriptContent').find('#projectVal').text(),
										'node': targetNode,
										'attributeToCheck': 'data-project'
									})
									$('#msCount').text(param.filterObj.project[param.refreshedProject.replace(/[([0-9)]/g, '')]);
								}
							}
							$('.la-container').css('display', 'none');
						},
						error: function (respData) {
							$('.la-container').css('display', 'none');
						}
					});
				} else {
					//Do not reload the card view for second time
					if (param.stage != 'Support') {
						cardData = manuScriptCardData;
					}
					param = {};
					$('#manuscriptContent #projectVal').text('Project');
					var totalMsCount = cardData.length;
					data.info = cardData;
					data.disableFasttrack = disableFasttrack;
					data.filterObj = {};
					data.filterObj['customer'] = {};
					data.filterObj['project'] = {};
					data.filterObj['articleType'] = {};
					data.filterObj['typesetter'] = {};
					data.filterObj['publisher'] = {};
					data.filterObj['author'] = {};
					data.filterObj['editor'] = {};
					data.filterObj['copyeditor'] = {};
					data.filterObj['contentloading'] = {};
					data.filterObj['support'] = {};
					data.filterObj['supportstage'] = {};
					data.filterObj['supportlevel'] = {};
					data.filterObj['others'] = {};
					manuscriptsData = $('#manuscriptsDataContent');
					var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
					manuscriptsData.append(pagefn(data));
					// $(currentTab  +'#manuscriptsData').height(window.innerHeight - $(currentTab  +'#manuscriptsData').position().top - $('#footerContainer').height());
					// $(currentTab  +'.windowHeight').height(window.innerHeight - ($(currentTab + '.layoutView').offset().top + 50) - $('#footerContainer').height());
					$('.msCount.loading.hidden').removeClass('hidden');
					$('.msCount.loading').removeClass('loading').addClass('loaded');
					$('#msCount').text(totalMsCount);
					param.filterObj = data.filterObj;
					if (selectedCustomer) {
						eventHandler.components.actionitems.getUserDetails(selectedCustomer);
						param.customer = selectedCustomer;
					}
					if (!param.customer || param.customer == 'first') {
						eventHandler.filters.populateNavigater(param);
					} else {
						eventHandler.filters.populateNavigater(param);
					}
					eventHandler.filters.populateFilters(data.filterObj);
					//eventHandler.filters.populateSort();
					var contNode = $('#' + $(targetNode).attr('data-href'));
					contNode.addClass('active');
					$('.la-container').css('display', 'none');
				}
				if (param.stage == 'Support') {
					cardData = supportData;
				} else {
					cardData = manuScriptCardData;
				}
			},
			showSupportArticles: function () {
				Object.keys(userData.kuser.kuroles).forEach(function (role) {
					var supportRole = userData.kuser.kuroles[role]['role-type'];
					var supportAccess = userData.kuser.kuroles[role]['access-level'];
					if (showSupportTab[supportRole] && showSupportTab[supportRole].access && (showSupportTab[supportRole].access.indexOf(supportAccess) > -1 || showSupportTab[supportRole].access == 'All')) {
						$("#support").html('<a class="nav-link" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showSupport\', \'param\': {\'workflowStatus\':\'in-progress\' ,\'project\':\'project\', \'customer\': \'customer\',\'urlToPost\':\'getSupportStage\', \'stage\': \'Support\',\'showStages\':\'(Support) OR (addJob) OR (File download) OR (Convert Files)\', \'version\':\'v2.0\'}}" data-href="manuscriptContent"><i class="fa fa-ticket"/> Support</a>');
					}
					if (showAssignmentTab[supportRole] && showAssignmentTab[supportRole].access && (showAssignmentTab[supportRole].access.indexOf(supportAccess) > -1 || showAssignmentTab[supportRole].access == 'All')) {
						$("#assignmentTab").html('<a class="nav-link" id="autoAssign" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showAssignTab\'}" data-href="autoAssignContent"><i class="fa fa-file"/> Assignment</a>');
					}
					if(uploadXMLButton[supportRole] && uploadXMLButton[supportRole].access && (uploadXMLButton[supportRole].access.indexOf(supportAccess) > -1 || uploadXMLButton[supportRole].access == 'All')){
						$("#uploadXMLDrop").removeClass('hidden')
						$("#feedbackReportDrop").removeClass('hidden')
					}
				})
			},
			showAssignOptions: function (param, targetnode) {
				if (param.customer) {
					var roleType = userData.kuser.kuroles[param.customer]['role-type'];
					var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
					if (userAuthorization && userAuthorization[roleType] && userAuthorization[roleType][accessLevel] && (userAuthorization[roleType][accessLevel]['toShow'].length || userAuthorization[roleType][accessLevel]['toHide'].length)) {
						if (targetnode) {
							$(targetnode).closest('#customer').parent().siblings().find('#statusVal').siblings('#filterStatus').children().first().attr('data-message', "{'funcToCall': 'showManuscript', 'param' : { 'workflowStatus':'in-progress', 'urlToPost':'getAssigned', 'customer':'selected','custTag': '#filterCustomer', 'prjTag':'#filterProject', 'user':'user','optionforassigning':true ,'version': 'v2.0'}}")
						} else {
							$('#manuscriptContent #filterStatus').children().first().attr('data-message', "{'funcToCall': 'showManuscript', 'param' : { 'workflowStatus':'in-progress', 'urlToPost':'getAssigned', 'customer':'selected','custTag': '#filterCustomer', 'prjTag':'#filterProject', 'user':'user','optionforassigning':true, 'version': 'v2.0' }}")
						}
						if ($('#manuscriptContent #statusVal').text().trim() == "WIP") {
							$('.assignAccess').show();
						} else {
							$('.assignAccess').hide();
						}
						userAuthorization[roleType][accessLevel]['toShow'].forEach(function (id) {
							$(id).show();
						})
						userAuthorization[roleType][accessLevel]['toHide'].forEach(function (id) {
							$(id).hide();
						})
					} else {
						if (targetnode) {
							$(targetnode).closest('#customer').parent().siblings().find('#statusVal').siblings('#filterStatus').children().first().attr('data-message', '{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'selected\', \'project\': \'project\', \'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'excludeStage\': \'inCardView\', \'version\': \'v2.0\'}}')
						} else {
							$('#manuscriptContent #filterStatus').children().first().attr('data-message', '{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'selected\', \'project\': \'project\', \'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'excludeStage\': \'inCardView\', \'version\': \'v2.0\'}}')
						}
						if ($('#manuscriptContent #statusVal').text().trim() == "WIP") {
							$('.assignAccess').hide();
						} else {
							$('.assignAccess').show();
						}

					}
				}
			},
			showUserName: function () {
				$('#userName').attr('title', userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last);
				$('#userName').html(userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last);
			},
			/**
			 * To get searched articles in search bar.
			 */
			getSearchedArticles: function (param) {
				if (param == undefined || param.doi == undefined) {
					console.log('Empty parameters');
					return
				}
				//$('#searchBox').val(param.doi);
				$('#searchdoi').hide();
				$(currentTab + ".supportCardView").css('display', '');
        $(currentTab + ".supportReportView").css('display', 'none');
				$(currentTab + "#supportReport").addClass('hidden');
				$(currentTab + "#supportReportContainer").css('display', 'none')
				$('.la-container').css('display', '');
				$('#manuscriptContent .supportcard').hide();
				$('#manuscriptContent .filterOptions').hide();
				//To remove active class from header (Card view, Support)
				$('header .nav-items-journals').children().find('.active').removeClass('active');
				$('#sort').hide();
				$('#searchResult').show();
				if (!param.from) {
					param.from = 0;
				}
				if (!param.size) {
					param.size = 5;
				}
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						if (!respData || respData.hits.length == 0) {
							$('.la-container').css('display', 'none');
							$('#manuscriptsDataContent').html('NO RESULTS FOUND');
							$('#msCount').text('0');
							return;
						}
						$('.showEmpty').hide();
						var data = {};
						data.dconfig = dashboardConfig;
						// data.info = [];
						data.filterObj = {};
						data.filterObj['customer'] = {};
						data.filterObj['project'] = {};
						data.filterObj['articleType'] = {};
						data.filterObj['typesetter'] = {};
						data.filterObj['publisher'] = {};
						data.filterObj['author'] = {};
						data.filterObj['editor'] = {};
						data.filterObj['copyeditor'] = {};
						data.filterObj['support'] = {};
						data.filterObj['supportstage'] = {};
						data.filterObj['supportlevel'] = {};
						data.filterObj['contentloading'] = {};
						data.filterObj['others'] = {};
						data.disableFasttrack = disableFasttrack;
						data.userDet = JSON.parse($('#userDetails').attr('data'));
						$('#manuscriptsDataContent').html('');
						data.count = 0;
						cardData = [];
						cardData = respData.hits;
						data.info = cardData;
						manuscriptsData = $('#manuscriptsDataContent');
						var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
						manuscriptsData.append(pagefn(data));
						$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
						if(respData.hits && respData.hits[0] && respData.hits[0]._source && respData.hits[0]._source.customer){
							eventHandler.components.actionitems.getUserDetails(respData.hits[0]._source.customer);
						}
						$('.msCount.loading.hidden').removeClass('hidden');
						$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$('.msCount.loading').removeClass('loading').addClass('loaded');
						$('#msCount').text(respData.hits.length);
						$('.la-container').css('display', 'none');
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				})
			},
			getArticles: function (param) {
				if (param == undefined || param.doi == undefined || param.cardID == undefined || param.parentId==undefined) {
					console.log('Empty parameters');
					return
				}
				if (param.index == undefined) {
					param.index = 0;
				}
				if (!param.from) {
					param.from = 0;
				}
				if (!param.size) {
					param.size = 5;
				}
				var data = {
					'customer': 'customer',
					'project': '*',
					'doi': param.doi,
					'urlToPost': 'getSearchedArticles',
					'from': param.from,
					'size': param.size
				}
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: data,
					dataType: 'json',
					success: function (response) {
						respData = {'body':{}};
						respData.body = response.hits[0];
						var data = {};
						data.dconfig = dashboardConfig;
						data.info = [];
						data.filterObj = {};
						data.filterObj['customer'] = {};
						data.filterObj['project'] = {};
						data.filterObj['articleType'] = {};
						data.filterObj['typesetter'] = {};
						data.filterObj['publisher'] = {};
						data.filterObj['author'] = {};
						data.filterObj['editor'] = {};
						data.filterObj['copyeditor'] = {};
						data.filterObj['support'] = {};
						data.filterObj['supportstage'] = {};
						data.filterObj['supportlevel'] = {};
						data.filterObj['contentloading'] = {};
						data.filterObj['others'] = {};
						data.userDet = JSON.parse($('#userDetails').attr('data'));
						data.count = parseInt(param.cardID);
						data.dashboardview = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-dashboardview');
						data.storeVariable = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-store-variable');
						data.customerType = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-template-type');
						data.disableFasttrack = disableFasttrack;
						var customer = $("" + param.parentId + " [data-id='" + param.cardID + "']").attr('data-customer');
						var userRole = userData.kuser.kuroles[customer]['role-type'];
						var userAccess = userData.kuser.kuroles[customer]['access-level'];
						// To disable the action buttons in card for CE vendor
						if (userRole.match(/copyeditor/g) && userAccess.match(/vendor/g)) {
							data.ceVendor = true;
							if (param.unassign) {
								data.removeEditIcon = true;
							}
						}
						//data.customerType = projectLists[customer].type;
						data.info.push(respData.body);
						if (window[data.storeVariable] && window[data.storeVariable][param.cardID] && window[data.storeVariable][param.cardID]._source && respData.body._source && (JSON.stringify(window[data.storeVariable][param.cardID]._source) === JSON.stringify(respData.body._source)) && param.index != 6) {
							param.index++;
							setTimeout(function () {
								eventHandler.dropdowns.article.getArticles(param);
							}, 500);
							console.log(param.index);
						} else {
							window[data.storeVariable][param.cardID] = respData.body;
							if (data.customerType.match(/(book|issue)/)) {
								var bookChapter = $("" + param.parentId + " [data-id='" + param.cardID + "']");
								var bookChapterMetaData = bookChapter.data();
							}
							if(param.holdFrom !='unAssignData'){
							var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
							var content = pagefn(data);
							$(currentTab + '#manuscriptsData').height(window.innerHeight - $(currentTab + '#manuscriptsData').position().top - $('#footerContainer').height());
							$("" + param.parentId + " [data-id='" + param.cardID + "']").replaceWith(content);}
							if (data.customerType.match(/(book|issue)/)) {
								var bookChapterData = bookChapterMetaData.data._attributes;
								bookChapter = $("" + param.parentId + " [data-id='" + param.cardID + "']");
								bookChapter.find('.pageRange').attr('data-fpage', bookChapterData.fpage)
								bookChapter.find('.pageRange .rangeStart').text(bookChapterData.fpage)
								bookChapter.find('.pageRange').attr('data-lpage', bookChapterData.lpage)
								bookChapter.find('.pageRange .rangeEnd').text(bookChapterData.lpage)
								bookChapter.find('.msChNo').text(bookChapterData.chapterNumber)
								bookChapter.attr({
									'data-grouptype': bookChapterMetaData.grouptype,
									'data-uuid': bookChapterMetaData.uuid,
									'data-c-id': bookChapterMetaData.cId,
									'data-on-reorder': bookChapterMetaData.onReorder
								})
								bookChapter.data('data', bookChapterMetaData.data);
							}
						}
					},
					error: function (respData) {
						// alert('something went wrong while fetching data');
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				});
			}
		},
		issue: {
			listProjectIssue: function (param) {
				if (!param.customerName || !param.projectName) {
					return;
				}
				$('.issueLogOff').addClass('hidden');
				$('.save-notice').addClass('hide');
				$('.showEmpty .watermark-text').html('Please select issue <br/>to display list view');
				$('.showEmpty').show();
				$('.issue-Lock-screen').addClass('hidden')
					$('.issue-Lock-screen').css('display','none')
				$('.dashboardTabs.active #manuscriptsDataContent').html('');
				$(currentTab + '#manuscriptsData').removeClass('col-8');
				$(currentTab + '#assignedManuscript li').remove()
				if(currentTab && $(currentTab + '#manuscriptsData') && $(currentTab + '#manuscriptsData').data('binder') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog') && $(currentTab + '#manuscriptsData').data('currentIssueUserLog')!=''){
					eventHandler.flatplan.action.logOffUser({'issueData':$(currentTab + '#manuscriptsData').data('binder')});
				}
				$(currentTab + '#manuscriptsData').addClass('col-12');
				eventHandler.components.actionitems.hideRightPanel();
				$('.dashboardTabs.active #issueCustomer').removeAttr('style');
				$('#issueContent #issueProjectVal').html(param.projectName);
				$('#issueContent #projectIssueListVal').html('Issue').show();
				$('#issueContent #issueProject').children().removeClass('active');
				$('#issueContent #issueProject [value="' + param.projectName + '"]').addClass('active');
				var issueHTML = ''
				$.ajax({
					type: "get",
					url: '/api/getissuemakeupdata',
					data: param,
					dataType: 'json',
					success: function (respData) {
						if (respData && respData.hits && respData.hits.hits && respData.hits.hits.length) {
							var issueList = respData.hits.hits.map(function (issue) {
								return issue._source;
							})
							for (issue of issueList) {
								issueHTML += '<div class="filter-list" data-customer="' + param.customerName + '" data-project="' + param.projectName + '" data-file="' + issue.fileName + '" data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'getMetaXML\',\'param\':{\'type\':  \'journal\',\'projectName\':\'' + param.projectName + '\',\'issueId\':\'' + issue.id + '\'}}" value=' + issue.id + '>' + issue.fileName + '</div>';
							}
							$('#issueContent #projectIssueList').html(issueHTML);
							$('#issueContent .issuesList').css('display', 'inline-block');
						} else {
							$('.showEmpty .watermark-text').html('Issue not uploaded');
							$('#issueContent #projectIssueList').html('');
						}
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
					}
				});
			}
		},
		assign: {
			assignProjectIssue: function (param) {
				if (!param.customerName || !param.projectName) {
					return;
				}
				$('#userList option:nth-child(1)').prop("selected", true);
				$('#projectStageListVal').removeClass('highlightFilter')
				$('#projectStageList div').removeClass('active')
				$('#autoAssignContent #date').show();
				$('#autoAssignContent h2').hide();
				$(currentTab + '#rightPanelContainer').show()
				$(currentTab + 'h2').show()
				$('.Recommended').removeClass('hidden')
				$('.save-notice').addClass('hide');
				$('.showEmpty .watermark-text').html('Please select stage <br/>to display list view');
				$('.showEmpty').show();
				$('.dashboardTabs.active #manuscriptsDataContent').html('');
				$(currentTab + '#assignedManuscript li').remove()


				eventHandler.components.actionitems.hideRightPanel();
				$(currentTab + '#manuscriptsData').removeClass('col-12').addClass('col-8');
				$('.dashboardTabs.active #assignCustomer').removeAttr('style');
				$('#autoAssignContent #assignProjectVal').html(param.projectName);
				$('#autoAssignContent #projectStageListVal').html("All");
				$('#autoAssignContent #assignProject').children().removeClass('active');
				$('#autoAssignContent #assignProject [value="' + param.projectName + '"]').addClass('active');
				$("#assignDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if(!param.stageName)
				{
					param.stageName = "Typesetter Check|Pre-editing|Validation Check|Typesetter Review|Final Deliverables";
				}
				if (!param.fromDate)
					var fromDate, toDate = '';
				if (param.fromDate == undefined || param.fromDate == '') {
					param.fromDate = moment().format('YYYY-MM-DD');
					$("#assignDatePicker").val(moment(param.fromDate).format('D MMM, YY'));
				}
				if (param.toDate == undefined || param.toDate == '') {
					param.toDate = moment().format('YYYY-MM-DD');
					$("#assignDatePicker").val(moment(param.toDate).format('D MMM, YY'));
				}
				$('#assignProjectVal').addClass('highlightFilter')
				$('#autoAssignContent .assign-users').show()
				eventHandler.assignmentSummary.showUnassigned(param, targetNode)

			},
		}
	},
	eventHandler.filters = {
		populateSort: function () {
			var tempObj = {
				'MSID': 'MS ID',
				'Doi': 'Doi',
				'AcceptDate': 'Accept Date',
				'Article': 'Article Type',
				'Customer': 'Customer',
				'DaysInProd': 'Days In Prod',
				'DueDate': 'Due Date',
				'Owner': 'Owner',
				'Stage': 'Stage'
			}
			var sortopt = '<i>Sort cards by: </i>';
			for (var obj in tempObj) {
				if (obj == "MSID") {
					sortopt += '<span class="sort-item active" sort-option="' + obj + '" onclick="eventHandler.components.actionitems.cardSort(this,\'' + sortmapping[obj].sortAttr + '\',\'' + sortmapping[obj].highLightAttr + '\');" data-href="#">' + tempObj[obj] + '<i class="fa fa-caret-up fa-fw"></i></span>'
				} else {
					sortopt += '<span class="sort-item" sort-option="' + obj + '" onclick="eventHandler.components.actionitems.cardSort(this,\'' + sortmapping[obj].sortAttr + '\',\'' + sortmapping[obj].highLightAttr + '\');" data-href="#">' + tempObj[obj] + '</span>'
				}
			}
			var cards = $('#manuscriptsDataContent li:visible');
			var vals = [];
			cards.each(function () {
				vals.push($(this).attr('data-customer'));
			});
			vals = [...new Set(vals)];
			$('#sort').html(sortopt);
			if (vals.length <= 1) {
				$('#sort [sort-option="Customer"]').remove();
			}
			if (cards.length <= 1) {
				$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
				$('#sort').hide();
			} else {
				$('#sort').show();
			}
			//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');

		},
		sortUser: function(param){
			var reverse = false;
			var ele = param.targetNode;
			if ($(ele).find('.fa-caret-up').length > 0) {
				$($(ele).children('.fa-caret-up')).remove();
				$(ele).append('<i class="fa fa-caret-down fa-fw"></i>');
			} else {
				$($(ele).children('.fa-caret-down')).remove();
				$(ele).append('<i class="fa fa-caret-up fa-fw"></i>');
			}
			if ($(ele).children().hasClass('fa-caret-up')) {
				reverse= true;
			}
			if (param && param.attribute){
				eventHandler.filters.sortlist({'id':'#userDataContent li', 'attribute': param.attribute, 'replace': '#userDataContent', 'reverse': reverse})
				$($(ele).siblings().find('.fa')).remove();
				$(currentTab + '#usersort span').removeClass('active');
				$(ele).addClass('active');
				$('.la-container').hide();
			}
		},
		sortlist: function (param) {
			if (param && param.id && param.attribute && param.replace) {
				if (param.reverse) {
					var sorted = $(param.id).sort(function (b, a) {
						var contentA = parseInt($(a).attr(param.attribute));
						var contentB = parseInt($(b).attr(param.attribute));
						return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
					});
				} else {
					var sorted = $(param.id).sort(function (a, b) {
						var contentA = parseInt($(a).attr(param.attribute));
						var contentB = parseInt($(b).attr(param.attribute));
						return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
					});
				}
				$(param.replace).html(sorted);
			}
		},
		populateFilters: function (filterObj) {
			var tempObj = {
				'articleType': 'Article Type',
				'typesetter': 'Typesetter',
				'publisher': 'Publisher',
				'author': 'Author',
				'copyeditor': 'Copyeditor',
				'editor': 'Editor',
				'support': 'Support',
				'supportstage': 'Support Stages',
				'supportlevel': 'Support Levels',
				'customer': 'Customer',
				'contentloading': 'Content Loading',
				'others': 'Timeline'
			}
			$('div.filterOptions').empty();
			var navFilterHTML = '';
			var tableFilterHtml = '<table class="table table-bordered table-striped table-sm"><thead><tr  data-toggle="collapse" data-target="#dispFilter">';
			var tbodyHtml = '<tbody><tr  id="dispFilter" class="collapse out">';
			var tempHtml = '<table class="table table-bordered table-striped table-sm"><tr>';
			for (var obj in tempObj) {
				if (Object.keys(filterObj[obj]).length > 0) {
					tempNavFilterHTML = '';
					tempHtml += '<td><div class="container btn-group"><select name="multicheckbox[]" multiple="multiple" class="4col formcls">';
					tbodyHtml += '<td>';
					tableFilterHtml += "<th>" + tempObj[obj] + "</th>";
					var mainFiltAttr = 'no';
					var subFiltAtt = 'data-stage-name';
					if (filterMapping && filterMapping[obj]) {
						mainFiltAttr = filterMapping[obj];
					} else if (filterMapping && filterMapping['default']) {
						mainFiltAttr = filterMapping['default'];
					}
					/*if (Object.keys.length > 0) {
						tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + Object.keys(filterObj[obj]).length + '/' + Object.keys(filterObj[obj]).length + ')';
						tempNavFilterHTML += '</span>';
						tempNavFilterHTML += '<div class="filter-drop-content">';
					}*/
					var tempValue = 0;
					if (obj == "others") {
						if (Object.keys.length > 0) {
							tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + Object.keys(filterMapping["others-sub"]).length + '/' + Object.keys(filterMapping["others-sub"]).length + ')';
							tempNavFilterHTML += '</span>';
							tempNavFilterHTML += '<div class="filter-drop-content">';
						}
						for (var subObj in filterMapping["others-sub"]) {
							if (filterMapping["others-sub"][subObj] != undefined && filterMapping["others-sub"][subObj] != '') {
								tempHtml += ' <option value="' + subObj + '" main="' + obj + '">' + subObj + '</option>';
								tbodyHtml += '<label style=font-size:12px;"><input type="checkbox" name="fl-' + obj + '" value="' + subObj + '" id="' + subObj + '" />' + subObj + '</label>';
								var count = filterObj[obj][subObj] == undefined ? 0 : filterObj[obj][subObj];
								tempValue += count;
								tempNavFilterHTML += '<div class="filter-list active" data-filter-val="' + filterMapping["others-sub"][subObj]["val"] + '" data-filter-attr="' + filterMapping["others-sub"][subObj]["attr"] + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + filterMapping["others-sub"][subObj]["val"] + '\', \'' + filterMapping["others-sub"][subObj]["attr"] + '\' );" data-href="#">' + filterMapping["others-sub"][subObj]["name"] + '&nbsp;(' + count + ')</div>';
							}
						}
					} else {
						if (Object.keys.length > 0) {
							var filterLength = Object.keys(filterObj[obj]).length;
							if (cardViewFilterSeperator[obj] && cardViewFilterSeperator[obj].SecondaryFilterNames){
								cardViewFilterSeperator[obj].SecondaryFilterNames.forEach(append => {
									filterLength += Object.keys(filterObj[append]).length;
								})
							}
							tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" data-filter-root="' + obj + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + filterLength + '/' + filterLength + ')';
							tempNavFilterHTML += '</span>';
							tempNavFilterHTML += '<div class="filter-drop-content">';
						}
						if (cardViewFilterSeperator[obj]) {
							tempNavFilterHTML += '<div class="dropdown-header">' + tempObj[obj] + '</div><div class="divider"></div>'
						}
						for (var subObj in filterObj[obj]) {
							if (filterMapping && filterMapping[obj + '-sub']) {
								subFiltAttr = filterMapping[obj + '-sub'];
							} else if (filterMapping && filterMapping[obj]) {
								subFiltAttr = filterMapping[obj];
							} else if (filterMapping && filterMapping[subObj]) {
								subFiltAttr = filterMapping[subObj];
							} else if (filterMapping && filterMapping['default']) {
								subFiltAttr = filterMapping['default'];
							}
							tempHtml += ' <option value="' + subObj + '" main="' + obj + '">' + subObj + '</option>';
							tbodyHtml += '<label style=font-size:12px;"><input type="checkbox" name="fl-' + obj + '" value="' + subObj + '" id="' + subObj + '" />' + subObj + '</label>';
							tempValue += filterObj[obj][subObj];
							tempNavFilterHTML += '<div class="filter-list active" clickValue="' + obj + '" data-filter-val="' + subObj + '" data-filter-attr="' + subFiltAttr + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + subObj + '\', \'' + subFiltAttr + '\' );" data-href="#">' + subObj + '&nbsp;(' + filterObj[obj][subObj] + ')</div>';
						}
						if (cardViewFilterSeperator[obj] && cardViewFilterSeperator[obj].SecondaryFilterNames) {
							cardViewFilterSeperator[obj].SecondaryFilterNames.forEach(append => {
								if (Object.keys(filterObj[append]).length > 0) {
								tempNavFilterHTML += '<div class="dropdown-header">' + tempObj[append] + '</div><div class="divider"></div>'
								Object.keys(filterObj[append]).forEach(subObj => {
									tempNavFilterHTML += '<div class="filter-list active" clickValue="' + append + '" data-filter-val="' + subObj + '" data-filter-attr="' + filterMapping[append + '-sub'] + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + subObj + '\', \'' + filterMapping[append + '-sub'] + '\' );" data-href="#">' + subObj + '&nbsp;(' + filterObj[append][subObj] + ')</div>';
								})
								delete filterObj[append];
								delete tempObj[append];
							     }
							})
						}
					}
					tempHtml += '</td>';
					tbodyHtml += '</td>';
					if (Object.keys.length > 0) {
						tempNavFilterHTML += '</div></div>';
					}
					// tempNavFilterHTML = tempNavFilterHTML.replace('{count}', Object.keys(filterObj[obj]).length);
					if (tempValue > 0) {
						// navFilterHTML += tempNavFilterHTML.replace('{value}', tempValue);
						navFilterHTML += tempNavFilterHTML;
					}
				}
			}

			var tableContent = tableFilterHtml + '</thead>' + tbodyHtml + '</tbody></table>'
			$('div.filterOptions').html(navFilterHTML);
			var cards = $('#manuscriptsDataContent li:visible');
			var vals = [];
			cards.each(function () {
				vals.push($(this).attr('data-customer'));
			});
			vals = [...new Set(vals)];
			if (vals.length <= 1) {
				$('div.filterOptions [data-filter-root="customer"]').closest('div').remove();
			}
			return true;
			var navHTML = ``;
			for (const stageOwner in stageOwners) {
				if (stageOwners.hasOwnProperty(stageOwner) && (stageOwner != 'article-type' || stageOwner != 'total')) {
					var navListTotal = 0;
					var navListHTML = ``;
					for (const owner in stageOwners[stageOwner]) {
						if (stageOwners[stageOwner].hasOwnProperty(owner) && stageOwners[stageOwner][owner]['article-count']) {
							navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${owner}','filterBy':'stageOwners','filterOwner':'${stageOwner}'}}"    data-value="${owner}">${stageOwners[stageOwner][owner]['article-count']} : ${owner}</li>`;
							navListTotal += stageOwners[stageOwner][owner]['article-count'];
						}
					}
					navHTML += `<span class="stage-filter">
							<span data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >${stageOwner} (${navListTotal})
							</span>
							<div class="dropdown">
								<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
							</div>
						</span>`;
				}
			} {
				var navListTotal = 0;
				var navListHTML = ``;
				for (const articleType in stageOwners['article-type']) {
					if (stageOwners['article-type'].hasOwnProperty(articleType)) {
						navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${articleType}','filterBy':'stageOwners', 'filterOwner':'article-type'}}"  >${stageOwners['article-type'][articleType]['article-count']} : ${articleType}</li>`;
						navListTotal += stageOwners['article-type'][articleType]['article-count'];
					}
				}
				navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Article Type (${navListTotal})
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
						</div>
					</span>`;
				navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Total (5)
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown"></ul>
						</div>
					</span>`;
			}
			$('div.navFilter').empty();
			$('div.navFilter').html(navHTML);
		},
		populateNavigater: function (param) {
			var navigatePanel = '';
			var projectList = '';
			var projectArray;
			var cardAttribute = {
				'articleType': 'data-article-type',
				'typesetter': 'data-stage-name',
				'publisher': 'data-stage-name',
				'author': 'data-stage-name',
				'editor': 'data-stage-name',
				'copyeditor': 'data-stage-name',
			};
			$('#manuscriptContent #customerVal').text(param.customer);
			if (param) {
				if (param.refreshedProject && param.refreshedProject != undefined && (/(.*)\(/g).exec(param.refreshedProject) && (/(.*)\(/g).exec(param.refreshedProject)[1]) {
					param.project = (/(.*)\(/g).exec(param.refreshedProject)[1];
				}
				Object.keys(userData.kuser.kuroles).forEach(function (customer, index) {
					if (projectLists && projectLists[customer]) {
						var roleAccess = (/^(production|copyeditor)$/.test(userData.kuser.kuroles[customer]['role-type']));
						var clientType = (projectLists[customer].type) ? projectLists[customer].type : 'journal';
						if (index == 0 && !param.customer) {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true , \'version\': \'v2.0\'}}" class="filter-list active" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list active" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							}
							if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
								projectLists[customer]['name'].forEach(function (project, count) {
									projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
								});
							}
						} else if ((param.customer) && (param.customer == customer)) {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true ,\'version\': \'v2.0\'}}" class="filter-list active" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list active" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							}
							if (param.project && param.project != undefined && param.project != 'project') {
								projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this})">All</div>';
								if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
									projectLists[param.customer]['name'].forEach(function (project, count) {
										if (param.project == projectLists[customer]['value'][count]) {
											if (param.filterObj.project[projectLists[customer]['value'][count]] == undefined) {
												projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
											} else {
												projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
											}
										} else if (param.filterObj && param.filterObj.project[project]) {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
										} else {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
										}
									});
								}
							} else {
								projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this})">All</div>';
								if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
									projectLists[customer]['name'].forEach(function (project, count) {
										if (param.filterObj && param.filterObj.project[projectLists[customer]['value'][count]]) {
											projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
										} else {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
										}
									});
								}
							}
						} else {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true ,\'version\': \'v2.0\'}}" class="filter-list" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '"> ' + customer + '</div>';
							}

						}
					}
				});
				param.project = 'project'
				$('#filterCustomer').html(navigatePanel);
				$('#filterProject').html(projectList);
				$('.la-container').css('display', 'none');
			}
		},
		notifyNo: function (target) {
			if ($(target).attr('data-last-sele') == "All") {
				$('#allproject').addClass('active');
				$($("#allproject").siblings()).addClass('active');
			} else {
				$('#chartProject').find('.active').removeClass('active');
				$('#chartProject').find('[data-project="' + $(target).attr('data-last-sele') + '"]').addClass('active');
			}
			$('#notifyprojectlist').hide();
		},
		updateFilters: function (param) {
			if (param.project != "Project" && param.project != undefined) {
				$('[data-filter-attr]').each(function () {
					var attrval = [];
					var attr = $(this).attr('data-filter-attr');
					var attrval = $(this).attr('data-filter-val');
					param.project = param.project.replace(/\(.*\)/gm, '')
					if (param.project != "All") {
						var count = $('#manuscriptsDataContent li[data-project="' + param.project + '"][' + attr + '="' + attrval + '"]:visible').length;
					} else {
						var count = $('#manuscriptsDataContent li[' + attr + '="' + attrval + '"]:visible').length;
					}
					$(this).text($(this).text().replace(/\(.*\)/gm, '(' + count + ')'));
				});
			}
		},
		projectFilters: function (param) {
			eventHandler.components.actionitems.hideRightPanel();
			if ($(param.node).attr('id') == 'refresh') {
				// param.node = $(param.node).siblings().find('#filterProject .active');
				param.node = $(param.node).closest('#manuscriptContent').find('#filterProject .active');
			}
			$(param.node).parent().siblings().html($(param.node).text());
			if (param.project == "All") {
				if ($(param.node).hasClass('active')) {
					$(param.node).removeClass('active');
					$($(param.node).siblings()).removeClass('active');
					$('#manuscriptContent li').addClass('dashboardFilterHidden');
				} else {
					$(param.node).addClass('active');
					$($(param.node).siblings()).addClass('active');
					$('#manuscriptContent li').removeClass('dashboardFilterHidden');
				}
			} else {
				$(param.node).addClass('active');
				$(param.node).siblings('#allproject').removeClass('active');
				$($(param.node).siblings()).removeClass('active');
			}
			if (param.attributeToCheck) {
				var filterTag = '#manuscriptContent li';
				var filterAtt = param.attributeToCheck;
				// var filterAtt = 'data-category';
				if ($(param.node).hasClass('active')) {
					//$(param.node).removeClass('active');
					var filterText = $(param.node).attr('value');
					if ($(filterTag + '[' + filterAtt + '="' + filterText + '"]').length > 0) {
						$($(filterTag + '[' + filterAtt + '="' + filterText + '"]').siblings()).addClass('dashboardFilterHidden');
						$(filterTag + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
					} else {
						$(filterTag).addClass('dashboardFilterHidden');
					}
				} else {
					var filterText = $(param.node).attr('value');
					$(filterTag + '[' + filterAtt + '="' + filterText + '"]').addClass('dashboardFilterHidden');
				}

			}
			$('#msCount').text($('#manuscriptsDataContent').children().not('.dashboardFilterHidden').length);
			if ($(param.node).closest("#manuscriptContent").length == 0) {
				if ($('#chartProject').find('.active').length > 0) {
					$('#notifyprojectlist').show();
				} else {
					$('#notifyprojectlist').hide();
				}
			}
			//when loading at first time if cards not there in card view code is breaking
			if ($('#manuscriptsDataContent li:visible').length > 0) {
				$('#manuscriptsData').scrollTop($('#manuscriptsDataContent li').position().top);
			}
			eventHandler.filters.updateFilters(param);
			$('#sort .fa').remove();
			// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
			var customer = $('#manuscriptContent #customerVal').text();
			if (!customer.match(/customer|first/gi)) {
				var userRole = userData.kuser.kuroles[customer]['role-type'];
				var userAccess = userData.kuser.kuroles[customer]['access-level'];
				if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
					eventHandler.components.actionitems.cardSort($('#sort [sort-option="MSID"]')[0], 'sort-msid', '.msDOI');
				}
			}
		},
		customerFilter: function (type, send) {
			if (!type) {
				return;
			}
			var customer;
			$(type).data('clicked', true);
			if ($(type).hasClass('active')) {
				$(type).removeClass('active');
			} else {
				$(type).addClass('active')
			}
			if (send) {
				var customer = [];
				$("#chartfilterCustomer .active").each(function () {
					customer.push($(this).text());
				});
				$('.la-container').css('display', 'none');
				eventHandler.highChart.makeCharts({
					'customer': customer,
					'project': []
				})
			}
		},
		stageBasedArticles: function (articles) {
			var stageOwners = {
				'exeter': {
					'addJob': {},
					'pre-editing': {},
					'typesetter': {},
					'copyediting': {}
				},
				'publisher': {
					'publisher-check': {},
					'publisher-review': {},
				},
				'author': {
					'author-check': {},
					'review': {},
				},
				'article-type': {}
			};
			var addFilterData = {
				'accepted': [],
				'stage-due': [],
				'ce-level': [],
				'days-in-production': []
			};
			var stageArticleCount = 0;
			var articleTypeCount = 0;
			for (const article of articles) {
				for (const stageOwner in stageOwners) {
					var matchedStage = stageOwners[stageOwner][article._source.stageName.toLowerCase()];
					if (matchedStage) {
						if (matchedStage['article-count']) {
							matchedStage['article-count']++;
						} else matchedStage['article-count'] = 1;
						if (matchedStage['article-data-id']) {
							matchedStage['article-data-id'].push(stageArticleCount);
						} else matchedStage['article-data-id'] = [stageArticleCount];
						break;
					}
				}
				addFilterData['accepted'].push(article._source.acceptedDate);
				addFilterData['stage-due'].push(article._source.stageDueDate);
				addFilterData['ce-level'].push(article._source.CELevel);
				addFilterData['days-in-production'].push(article._source.daysInProduction);
				stageArticleCount++;
			}
			for (const article of articles) {
				var matchedArticleType = stageOwners['article-type'][article._source.articleType];
				if (matchedArticleType) {
					stageOwners['article-type'][article._source.articleType]['article-count']++;
					stageOwners['article-type'][article._source.articleType]['article-data-id'].push(articleTypeCount);
				} else {
					stageOwners['article-type'][article._source.articleType] = {};
					stageOwners['article-type'][article._source.articleType]['article-count'] = 1;
					stageOwners['article-type'][article._source.articleType]['article-data-id'] = [articleTypeCount];
				}
				articleTypeCount++;
			}
			return [stageOwners, filterData];
		},
		articleFilter: function (articles) {
			var stageArticleCount = 0;
			var articleTypeCount = 0;
			for (const article of articles) {
				for (const stageOwner in stageOwners) {
					var matchedStage = stageOwners[stageOwner][article._source.stageName.toLowerCase()];
					if (matchedStage) {
						if (matchedStage['article-count']) {
							matchedStage['article-count']++;
						} else matchedStage['article-count'] = 1;
						if (matchedStage['article-data-id']) {
							matchedStage['article-data-id'].push(stageArticleCount);
						} else matchedStage['article-data-id'] = [stageArticleCount];
						break;
					}
				}
				stageArticleCount++;
			}
		},
		filterDashBoard: function (type, filterTag, filterText, filterAtt) {
			if (!type) {
				return;
			}
			eventHandler.components.actionitems.hideRightPanel();
			var project = $('#manuscriptContent #projectVal').text().replace(/\(.*\)/gm, '');
			if (project == "Project" || project == "All" || $('#support').find('.active').length) {
				prjectSelect = '';
			} else {
				prjectSelect = '[data-project="' + project + '"]';
			}
			if ($(type).siblings().children().length) {
				$(type).parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				if ($(type).hasClass('highlightFilter')) {
					$(type).removeClass('highlightFilter');
					$('.filterOptions .filter-list').addClass('active');
					$(type).text($(type).text().replace(/\(.*\//g, '(' + $(type).siblings().find('.filter-list').length + '/'))
					$(type).parent().siblings().each(function (name) {
						$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(' + $(this).children('.filter-drop-content').children('.filter-list').length + '/'))
					})
					// $(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
					$(filterTag + prjectSelect).removeClass('dashboardFilterHidden');
				} else {
					$(type).addClass('highlightFilter');
					$(type).parent().siblings().each(function (name) {
						$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
					})
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
					$('.filterOptions .active').removeClass('active');
					$(type).siblings().children().addClass('active');
					$(type).text($(type).text().replace(/\(.*\//g, '(' + $(type).siblings().find('.filter-list.active').length + '/'))
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
				}
				// } else if ($(type).siblings('.active').length == $(type).siblings().length ) {
				//else if ($(type).parent().children('.active').length == $(type).parent().children().length)
			} else if ($(type).parent().children('.filter-list.active').length == $(type).parent().children('.filter-list').length) {
				$(type).parent().parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				$(type).parent().siblings().addClass('highlightFilter');
				$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				$(type).parent().parent().siblings().each(function (name) {
					$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
				})
				$('.filterOptions .active').removeClass('active');
				$(type).addClass('active');
				$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
				$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
			}
			else {
				// To remove active class with different category.
				if ($(type).siblings('.active').attr('clickvalue') != $(type).attr('clickvalue')) {
					$(type).siblings().removeClass('active');
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				}
				$(type).parent().parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				$(type).parent().siblings().addClass('highlightFilter');
				$(type).closest('.filter-drop').siblings().find('.filter-list.active').removeClass('active');
				$(type).parent().parent().siblings().each(function (name) {
					$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
				})
				if ($(type).siblings('.active').length == 0) {
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				}
				if ($(type).hasClass('active')) {
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').addClass('dashboardFilterHidden');
					$(type).removeClass('active');
					$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length)) + '/'))
				} else {
					$(type).addClass('active');
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
					$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
				}
			}
			if (filterAtt == 'all') {
				$(filterTag + prjectSelect).removeClass('dashboardFilterHidden');
			}
			//if $('#manuscriptsDataContent li') is empty at that time code is breaking
			if($('#manuscriptsDataContent li').length > 0){
				$('#manuscriptsData').scrollTop($('#manuscriptsDataContent li').position().top);
			}
			$('#msCount').text($(filterTag + prjectSelect + ':not(.dashboardFilterHidden)').length);
			$('#sort .fa').remove();
			// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
			var customer = $('#manuscriptContent #customerVal').text();
			if (!customer.match(/customer|first/gi)) {
				var userRole = userData.kuser.kuroles[customer]['role-type'];
				var userAccess = userData.kuser.kuroles[customer]['access-level'];
				if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
					eventHandler.components.actionitems.cardSort($('#sort [sort-option="MSID"]')[0], 'sort-msid', '.msDOI');
				}
			}
		},
		inBetween: function (heystack, needle1, needle2) {

		},
		greaterThanEquals: function (heystack, needle) {},
		lesserThanEquals: function (heystack, needle) {},
		equalsTo: function (heystack, needle) {}
	},
	eventHandler.bulkedit = {
		displayprocess: {
			clickevents: function (targetnode, checkBox) {
				var attr = $(targetnode).attr('data-id');
				if (typeof attr !== typeof undefined && attr !== false) {
					if ($(targetnode).prop('checked')) {
						$(checkBox).removeAttr('checked')
						$(checkBox).prop( "checked", true );
						$(checkBox).attr('checked', 'true')
					} else {
						$(checkBox).removeAttr('checked')
						$(checkBox).prop( "checked", false );
					}
				}
			},
			editOwner: function (targetnode) {
				var cardID = $(targetnode).closest('tr[data-id]').attr('data-id');
				console.log(cardID);
			}
		},
		export: {
			exportExcel: function (param) {
				$($('#chartReportTable').find('.sort-arrow')).remove();
				if (param.tableID) {
					if ($('#' + param.tableID).length > 0) {
						var orgTableData = $('#' + param.tableID)[0];
						var cloneTableData = orgTableData.cloneNode(true);
						if (param.excludeRow) {
							$(cloneTableData).find(param.excludeRow).remove();
						}
						if (param.excludeColumn) {
							$(cloneTableData).find(param.excludeColumn).remove();
						}
						$(cloneTableData).find('.assign-users').each(function () {
							if(this.value != undefined){
								this.replaceWith(this.value.replace(/,.*/g, ''));
							}
						})
						$(cloneTableData).attr('id', 'exportExcelCloneTable');
						$(cloneTableData).attr('class', 'hidden');
						document.body.appendChild(cloneTableData);
						tableToExcel('exportExcelCloneTable', 'Report');
						$('#exportExcelCloneTable').remove();
					}
				}
			},
			managefilter(event) {
				var x = event.which || event.keyCode;
				if (x == 13 || event == 'sort') {
					if ($('.repchkbox').length != $('.repchkbox:checked').length) {
						$("#repchkboxall").prop('checked', false);
					}
					if ($('#chartReportTable').find('tr.reportCheckBox.show').length > 0) {
						//$('#historytbl').append($('#chartReportTable').find('tr.reportCheckBox.show'));
					}
				}
				var count = $('#reportData').find('tr.reportTable:visible').length;
				$('#reportscount').text('Showing 1-' + count + ' of ' + count);
			},
			manageHistory: function (target) {
				/*if($($(target).closest('tr').next()).hasClass('reportCheckBox')){
					$('#historytbl').append($(target).closest('tr').next());
				}else{
					$($('#historytbl').find($(target).attr("data-target"))).insertAfter($(target).closest('tr'));
					$($(target).attr("data-target")).addClass('show');
				}*/
				$('#reportsHistory table tr.stages').remove();
				$('#reportHistoryDoi').text('article');
				//$('#articleHistory').attr('data', '');
				//var msCard = $(targetNode).closest('.msCard');
				var cardID = $(target).closest('tr').attr('data-report-id');
				var parentId = $(target).closest('tr').attr('data-dashboardview');
				var sourceData = chartData[$(target).closest('tr').attr('data-report-id')]._source;
				//$('#articleHistory').attr('data', $(msCard).attr('data').replace(/\'/g, '"'));
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var stages = sourceData.stage;
				var collapseRow = '', hideCompletedRow = '';
				var hideRowId = 0;
				for (var s = 0, sl = $(stages).length; s < sl; s++) {
					var stage = stages[s] ? stages[s] : stages;
					var doi = sourceData.id.replace('.xml', '').replace('\.', '');
					var inprogressDayDiff = Math.floor(dateDistance(stage['sla-start-date'], moment().utc().format('YYYY-MM-DD HH:mm:ss'), daysForCalculation.holidays['default']));
					inprogressDayDiff = (inprogressDayDiff < 0) ? 0 : inprogressDayDiff;
					var stageRow = '';
					$('#reportHistoryDoi').text(sourceData.id.replace('.xml', ''));
					if (stage.status != 'in-progress' && stage.status != 'waiting') {
						var tempSlaStartDate = stage['sla-start-date'] ? moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempSlaEndDate = stage['sla-end-date'] ? moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaStartDate = stage['planned-start-date'] ? moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaEndDate = stage['planned-end-date'] ? moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var stageDuration = ((stage['stageduration-days'] != undefined) ? stage['stageduration-days'] : '');
						var stageAssignedName = (stage.assigned && stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : 'Unassigned';
						if (hideRepeatedStage.default.includes(stage.name)) {
							collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>' + collapseRow;
							continue;
						} else {
							if (stages.length != s + 1 && stages[s + 1].status == 'completed' && hideRepeatedStage.default.includes(stages[s + 1].name)) {
								collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>' + collapseRow;
								continue;
							} else {
								if (collapseRow) {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>'
								} else {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + stageDuration + '</td></tr>'
								}

							}
						}
					}
					if (collapseRow || hideCompletedRow) {
						var hideRow = '';
						if (hideCompletedRow) {
							hideRow += hideCompletedRow;
						}
						if (collapseRow) {
							hideRow += collapseRow;
						}
						stageRow = $(hideRow);
						collapseRow = hideCompletedRow = '';
						$('#reportsHistory table tbody').prepend(stageRow);
					}
					if (stage.status == 'in-progress') {
						stageRow = $('<tr class="stages" data-stage-name="' + stage.name + '"/>');
						usersHTML = "";
						usersArr = {};
						var users;
						var assignUserRole = userData.kuser.kuroles[sourceData.customer]['role-type'];
						var assignUserAccess = userData.kuser.kuroles[sourceData.customer]['access-level'];
						if (userAssignAccessRoleStages && userAssignAccessRoleStages[assignUserRole] && userAssignAccessRoleStages[assignUserRole][assignUserAccess] && !userAssignAccessRoleStages[assignUserRole][assignUserAccess].includes(stage.name)) {
							if (stage.assigned.to) {
								users = $('<span>' + stage.assigned.to + '</span>');
							} else {
								users = $('<span>Unassigned</span>');
							}
						} else {
							users = $('<select class="assign-users" data-doi="' + doi + '" onchange="eventHandler.components.actionitems.assignUser(this,' + cardID + ', \'data-report-id\', false, \'\', \'\', true, \'\', \'\', \'' + parentId + '\')" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"></select>');
							// $(usersDetailsDiv).find('user').each(function (i, v) {
							// 	users.append('<option value="' + $(this).find('first').text() + ', ' + $(this).find('email').text() + '">' + $(this).find('first').text() + '</option>');
							// });
							if (usersList && usersList.users) {
								Object.keys(usersList.users).forEach(function (count, q) {
									var skillLevel = (usersList.users[count].additionalDetails && usersList.users[count].additionalDetails['skill-level']) ? usersList.users[count].additionalDetails['skill-level'] : "";
									users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
									// console.log(usersList.users[a])
								})
							}
							if (users.find('option[value="' + stage.assigned.to + '"]').length > 0) {
								users.find('option[value="' + stage.assigned.to + '"]').attr('selected', true);
								users.append('<option value="' + " " + '">Unassign</option>');
							} else {
								if (stage.assigned.to == null || stage.assigned.to == "" || stage.assigned.to == "Exeter") {
									users.append('<option value="' + " " + '" selected>Un assigned</option>');
								} else {
									users.append('<option value="' + stage.assigned.to + '"  selected>' + stage.assigned.to + '</option>');
									users.append('<option value="' + " " + '">Unassign</option>');
								}
							}
						}
						usersHTML = $(users)[0].outerHTML;
						//update by prasana
						if ($('#reportsHistory table tbody tr').hasClass('hideRow' + hideRowId)) {
							stageRow.html('<td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td><span>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '" id="blkdateEnd' + doi + '">' + moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						} else {
							stageRow.html('<td></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + moment(stage['start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></a></td><td><span>' + moment(stage['end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['sla-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><span>' + moment(stage['planned-start-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + moment(stage['planned-end-date']+ ' UTC').format('MMM DD, YYYY HH:mm:ss') + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date']+ ' UTC', "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						}
						// stageRow.html('<td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'data-report-id\', false,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						//update end by prasana
					}
					hideRowId++;
					// //if (usersArr.users) {
					$('#reportsHistory table tbody').prepend(stageRow);
					//console.log(stages[s])
				}
				$('#reportsHistory table tbody').prepend(' <tr class="stages"><th/><th>Stage Name</th><th>Assigned to</th><th>Start Date</th><th>End Date</th><th>Sla-Start Date</th><th>Sla-End Date</th><th>Planned-Start Date</th><th>Planned-End Date</th><th>Status</th><th>Days</th></tr>')
				$('#reportsHistory').modal()
			},
			exportTable: function (params, customer, fromTotal, toTotal, totalCount, tableType, chartTableType, chartTableTemplateName, movementData, config, clickEvent, fromReportTab, titleName, tableName) {
				$("#repchkboxall").prop('checked', false);
				var terminateConnection;
				if (terminateConnection == undefined) {
					terminateConnection = false;
				}
				//To terminate the connection when modal box is closed
				$("#chartReportCloseButton").click(function () {
					terminateConnection = true;
				});
				if (terminateConnection) {
					return;
				}
				if (!chartTableType || chartTableType == undefined) {
					chartTableType = 'default';
				}
				if (!chartTableTemplateName || chartTableTemplateName == undefined) {
					chartTableTemplateName = 'chartReportTemplate';
				}
				setTimeout(function () {
					var data = {}, assignUserRole = userData.kuser.kuroles[customer]['role-type'], assignUserAccess = userData.kuser.kuroles[customer]['access-level'];
					if (params.url && params) {
						$.ajax({
							type: "POST",
							url: params.url,
							data: params,
							dataType: 'json',
							//async: false,
							success: function (respData) {
								if (terminateConnection) {
									return;
								}
								if (!respData || respData.length == 0) {
									return;
								}
								if (fromTotal == 0) {
									$('#reportData').html('');
									$('#cardReportData #manuscriptsDataContent').html('');
									tableReportData = [];
									$("#chartReportTable tr").html(' ');
									$("#chartReportTable tr").append('<th class="reportCheckBox" col-size="3" data-filter="reportCheckBox"><input data-id="all" class="reportCheckBox" id="repchkboxall" onclick="eventHandler.bulkedit.displayprocess.clickevents(this, \'.repchkbox\')" type="checkbox"/></th>');
									var tablerow = '';
									// To create table with configuration.
									chartTableReport[chartTableType].columns.forEach(function (column) {
										var row = '<th';
										Object.keys(column.attribute).forEach(function (value) {
											row += ' ' + value + '="' + column.attribute[value] + '"';
										})
										row += '>' + column.columnName + '</th>';
										tablerow += row;
									})
									$("#chartReportTable tr").append($(tablerow));
								}
								if (!respData || respData.length == 0) {
									$('.la-container').css('display', 'none');
									return;
								}
								$("#chartReportTable th").show().removeClass('reportCheckBox');
								$("#chartReportTable [data-filter='reportCheckBox'],[data-filter='reportHistory']").addClass('reportCheckBox');
								var pagefn = doT.template(document.getElementById(chartTableTemplateName).innerHTML, undefined, undefined);
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['supportlevel'] = {};
								data.filterObj['others'] = {};
								data.info = respData.hits;
								data.dconfig = dashboardConfig;
								Array.prototype.push.apply(tableReportData, respData.hits);
								chartData = tableReportData;
								data.count = fromTotal;
								data.userDet = JSON.parse($('#userDetails[data]').attr('data'));
								data.userAssignAccessRoleStages = userAssignAccessRoleStages[assignUserRole][assignUserAccess];
								data.usersList = usersList;
								if (movementData && movementData.stageName && movementData.dateType && movementData.date) {
									data.stageName = movementData.stageName;
									data.dateType = movementData.dateType;
									data.date = movementData.date;
								}
								data["info"]['currentStage'] = {}
								data["info"]['currentStage'] = titleName
								$('#reportData').append(pagefn(data));
								data.dashboardview = '#cardReportData';
								data.storeVariable = 'chartData';
								var cardfn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
								$('#cardReportData #manuscriptsDataContent').append(cardfn(data));
								$('#reportColHideShow').addClass('hidden');
								$('#filterExportExcel').addClass('hidden');
								$('#chartReportTable').parent().addClass('hidden');
								$('#cardReportData').removeClass('hidden');
								$('[toggleview="card"]').attr('toggleView','table').removeClass('fa-list').addClass('fa-table').text(' View Table');
								$('.sort-arrow').remove();
								$('#chartReportTable thead').find('.fltrow').remove();
								var count = $('#reportData').find('tr.reportTable').length;
								$('#reportscount').text('Showing 1-' + count + ' of ' + count);
								var dateFilter = function (tf) {
									tf.setFilterValue(8, '>=01-01-1900');
									tf.setFilterValue(9, '>=01-01-1900');
									tf.setFilterValue(10, '>=01-01-1900');
									tf.setFilterValue(11, '>=01-01-1900');
									tf.setFilterValue(12, '>=01-01-1900');
									tf.setFilterValue(13, '>=01-01-1900');
									tf.setFilterValue(14, '>=01-01-1900');
									tf.setFilterValue(15, '>=01-01-1900');
									tf.filter();
								}
								if (chartTableType == 'movementChart') {
									dateFilter = function (tf) {
										tf.setFilterValue(8, '>=01-01-1900');
										tf.setFilterValue(9, '>=01-01-1900');
										tf.setFilterValue(10, '>=01-01-1900');
										tf.setFilterValue(11, '>=01-01-1900');
										tf.setFilterValue(12, '>=01-01-1900');
										tf.setFilterValue(13, '>=01-01-1900');
										tf.filter();
									}
								}
								if (count && count > 1) {
									var tf = new TableFilter('chartReportTable', {
										base_path: './js/libs/tablefilter/',
										auto_filter: true,
										auto_filter: {
											delay: 1000 //milliseconds
										},
										col_types: tableReportFilterConfig[chartTableType].rowtype,
										filters_row_index: 1,
										on_filters_loaded: dateFilter,
										extensions: [{
											name: 'sort'
										}],
									});
									if (tableReportFilterConfig[chartTableType].filter) {
										tableReportFilterConfig[chartTableType].filter.forEach(function (column) {
											tf.cfg[column] = "none";
										})
									}
									tf.init();
									$('.flt').attr('onKeyUp', 'eventHandler.bulkedit.export.managefilter(event)');
								}
								var colHideCheck = '';
								colHideCheck = '<span>';
								for (var checkVal in ShowHideCol[chartTableType]) {
									if (ShowHideCol[chartTableType][checkVal].defaultShow) {
										colHideCheck += '<label class=".checkbox-inline" ><input type="checkbox" index = "' + ShowHideCol[chartTableType][checkVal].index + '" id="' + checkVal + '" checked/>' + ShowHideCol[chartTableType][checkVal].name + '</label>';
										$("#chartReportTable").find('tr :nth-child(' + (ShowHideCol[chartTableType][checkVal].index) + ')').removeClass('reportCheckBox').show();
									} else {
										colHideCheck += '<label class=".checkbox-inline" ><input type="checkbox" index = "' + ShowHideCol[chartTableType][checkVal].index + '" id="' + checkVal + '"/>' + ShowHideCol[chartTableType][checkVal].name + '</label>';
										$("#chartReportTable").find('tr :nth-child(' + (ShowHideCol[chartTableType][checkVal].index) + ')').addClass('reportCheckBox').hide();
									}
								}
								colHideCheck += '</span>';
								$('#reportColHideShow').html(colHideCheck);
								$('#reportsLoadingcount .msCount.loading.hidden').removeClass('hidden');
								if (params.exportTable) {
									//To stop the loading icon
									$('.la-container').css('display', 'none');
									$('#modalReport').modal();
									if (fromTotal == 0) {
										setTimeout(() => {
											if ($('#chartReportTable').attr('table-size') == undefined) {
												var modelwidth = ($(window).width() / 100) * 98 - 62;
												$('#chartReportTable').attr('table-size', modelwidth);
											}
											for (var i = 0; i < $('#chartReportTable th').length; i++) {
												var width = (parseInt($('#chartReportTable').attr('table-size')) / 100) * parseInt($($('#chartReportTable th')[i]).attr('col-size'));
												$('#chartReportTable').find('tr :nth-child(' + (i + 1) + ')th,tr :nth-child(' + (i + 1) + ')td').css('width', width);
											}
										}, 0);
									}
								}
								// To check whether the displaying article count is equal to total article count and to take this in loop
								if (toTotal < totalCount) {
									fromTotal = fromTotal + respData.hits.length;
									toTotal = fromTotal + ((totalCount - toTotal > articleLoopCountInTableReport) ? (articleLoopCountInTableReport) : (totalCount - toTotal));
									$('#reportsLoadingcount #msYtlCount').text(fromTotal);
									$('#reportsLoadingcount #msCount').text(totalCount);
									$('#reportsLoadingcount').removeClass('hidden');
									$('#reportsLoadingcount').show();
									$('#reportscount').hide();
									$('#reportsLoadingcount').siblings().not("#chartReportCloseButton").addClass('hidden');
									if (fromReportTab) {
										eventHandler.tableReportsSummary.articlesReport(fromReportTab.tableId, customer, fromReportTab.xPath, titleName,tableName, fromTotal, toTotal)
									} else {
										eventHandler.highChart.tableReport('', clickEvent, config, customer, tableType, '', '', fromTotal, toTotal);
									}
								} else if (toTotal >= totalCount && params.exportTable != undefined) {
									$('#reportsLoadingcount').hide();
									$('#reportsLoadingcount').siblings().removeClass('hidden');
									$('#reportscount').show();
									$('#filterExportExcel').addClass('hidden');
								} else if (params.exportExcel && toTotal >= totalCount) {
									eventHandler.bulkedit.export.exportExcel({
										'tableID': 'chartReportTable',
										'excludeRow': '.collapse',
										'excludeColumn': '.reportCheckBox'
									})
									//To stop the loading icon
									$('.la-container').css('display', 'none');
								}
							},
							error: function (respData) {
								$('.la-container').css('display', 'none');
								console.log(respData);
							}
						});
					} else if (param.data) {}
				}, 0);

			}
		},
		bulkSave: function (param) {

		}
	}

$('body').on({
	dragenter: function (e) {
		e.preventDefault();
	},
	dragover: function (e) {
		e.preventDefault();
		return false;
	},
	dragleave: function (e) {},
	drop: function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length) {
			var param = {
				'file': e.originalEvent.dataTransfer.files[0]
			}
			tempFileList[0] = e.originalEvent.dataTransfer.files[0];
			$('#uploader .fileList .file-name').remove();
			$('#uploader .fileList').append('<div class="file-name">' + e.originalEvent.dataTransfer.files[0].name + '</div>');
			$('#upload_model .upload_button').removeClass('disabled');
			var dataType = $('#upload_model').attr('data-upload-type');
			if ($('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').length > 0) {
				$('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').removeClass('hidden');
			}
		}
	}
}, '#uploader');
$('body').on('change', '#uploader input', function (e) { // code
	if ($(this)[0].files.length > 0) {
		$('#uploader .fileList .file-name').remove();
		$('#uploader .fileList').append('<div class="file-name">' + $(this)[0].files[0].name + '</div>');
		$('#upload_model .upload_button').removeClass('disabled');
		var dataType = $('#upload_model').attr('data-upload-type');
		if ($('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').length > 0) {
			$('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').removeClass('hidden');
		}
	}
});
$('body').on('click', '#uploader .fileChooser', function () {
	$('#uploader input:file').val('');
	$('#uploader .fileList .file-name').remove();
	$('#uploader .upload_button').addClass('disabled');
	$(this).parent().find('input').trigger('click');
});

$('body').on('click', '.filemanager .folder', function (e) { // code
	if ($(e.target).closest('a[href]').length > 0) {
		var url = $(e.target).closest('a[href]').attr('href');
		var win = window.open(url, '_blank');
		win.focus();
		e.preventDefault();
		e.stopPropagation();
	} else if ($(e.target).closest('.file').length > 0) {
		var url = $(e.target).closest('.file').find('a[href]').attr('href');
		var win = window.open(url, '_blank');
		win.focus();
		e.preventDefault();
		e.stopPropagation();
	} else {
		$(this).parent().find('> li').addClass('non-active');
		$(this).removeClass('non-active').addClass('active');
		$('.filemanager').find('.nav-wrapper .breadcrumb').append('<a data-id="' + $(this).attr('id') + '" href="javascript:;" class="breadcrumb-item">' + $(this).find('> .name').text() + '</a>');
		e.preventDefault();
		e.stopPropagation();
	}
});

$('body').on('click', '.filemanager .nav-wrapper .breadcrumb-item', function () { // code
	$(this).nextAll().remove();
	if ($(this)[0].hasAttribute('data-id')) {
		var dataID = $(this).attr('data-id');
		$('.filemanager li[id="' + dataID + '"]').find('li').removeClass('non-active').removeClass('active');
	} else {
		$('.filemanager li').removeClass('non-active').removeClass('active');
	}
});

$('body').on('click', '.email-content .mail-subject', function () {
	$(this).closest('.email-content').toggleClass('toggle');
});
$('body').on('click', '.note-head', function () {
	$(this).closest('.jsonresp').toggleClass('toggle');
});
$('body').on('click', '.jsonresp:not(:has("div.note-head"))', function () {
	$(this).closest('.jsonresp').toggleClass('toggle');
});

$("#repchkboxall").change(function () { //"select all" change
	if ($(this).prop("checked") == false) {
		$(".repchkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
	} else {
		$(".repchkbox:visible").prop('checked', $(this).prop("checked"));
	}
});
$('.repchkbox').change(function () {
	//uncheck "select all", if one of the listed checkbox item is unchecked
	if (false == $(this).prop("checked")) { //if this item is unchecked
		$("#repchkboxall").prop('checked', false); //change "select all" checked status to false
	}
	//check "select all" if all checkbox items are checked
	if ($('.repchkbox:checked').length == $('.repchkbox').length) {
		$("#repchkboxall").prop('checked', true);
	}
});
//this function is not working for newly added checkbox insted of this i added updateCheckbox() function
/*$(".repchkbox").on('change',function() {
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
        $("#repchkboxall").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.repchkbox:checked').length == $('.repchkbox').length ){
        $("#repchkboxall").prop('checked', true);
    }
});*/
function updateCheckbox(targetNode) {
	if (false == $(targetNode).prop("checked")) { //if this item is unchecked
		$("#repchkboxall").prop('checked', false); //change "select all" checked status to false
	}
	//check "select all" if all checkbox items are checked
	if ($('.repchkbox:checked').length == $('.repchkbox').length) {
		$("#repchkboxall").prop('checked', true);
	}
}
$('.modal-content').on('click', function (target) {
	if (/datepicker/i.test($(target.target).attr('class')) || /datepicker/i.test($(target.target).parent().attr('class'))) {

	} else {
		$('.datepicker').closest('span').hide();
	}
});

function reLoadNotification(doi, header, body) {
	$('#notifyBox').hide();
	$('#reloadconfirm #infoheader').html('Job status for ' + doi + ':' + header);
	$('#reloadconfirm #reloadinfobody').html('Reason: ' + body + ' <br/><br/>Do you want to re-try loading this article?<br/><br/><span class="action"><span class="btn" style="margin-right: 1rem;background-color:#81c784;height: 2rem;" onclick="$(\'#reloadconfirm\').hide();eventHandler.components.actionitems.getftpFilename(\'' + doi + '\')">Yes</span><span class="btn" style="background-color: #feb74d;height: 2rem;" onclick="$(\'#reloadconfirm\').hide()">No</span></span>')
	$('#reloadconfirm').show();
}
function getTimeZone() {
	var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
	return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
}
function showArticleStatus(customer, jobID, notificationID, doi, status) {
	var jobStatus = status.status;
	var log = status.log;
	var logLen = status.log.length;
	var reason = log[logLen - 1];
	if(typeof (reason) == "object"){
		if (reason.log && reason.log.body && reason.log.body.status && reason.log.body.status.message && reason.log.body.status.message.status){
			reason = reason.log.body.status.message.status.message;
		}
		else {
			reason = JSON.stringify(reason);
		}
	}
	reLoadNotification(doi, jobStatus, reason);
}
window.onerror = function (msg, url, line, col, error) {
	// Note that col & error are new to the HTML 5 spec and may not be
	// supported in every browser.  It worked for me in Chrome.
	var extra = !col ? '' : '\ncolumn: ' + col;
	extra += !error ? '' : '\nerror: ' + error;
	//it will give flow of function calling.
	var flow = error.stack ? "\nFlow:"+error.stack : "";
	var browser = '';
	// Chrome 1+
	if(!!window.chrome && !!window.chrome.webstore){
		browser = "Chrome";
	}else if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){// Opera 8.0+
		browser = "Opera";
	}else if(typeof InstallTrigger !== 'undefined'){// Firefox 1.0+
		browser = "Firefox";
	}else if(/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){// Safari 3.0+ "[object HTMLElementConstructor]"
		browser = "Safari";
	}else if(/*@cc_on!@*/false || !!document.documentMode){// Internet Explorer 6-11
		browser = "Internet Explorer";
	}else if(!(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia){// Edge 20+
		browser = "Edge";
	//for blink we need to check like this.
	}else if(((!!window.chrome && !!window.chrome.webstore) || ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)) && !!window.CSS){// Blink engine detection
		browser = "Blink";
	}else{
		browser = "UnKnown";
	}
	//it will give what browser they are using.(if not chrome and mozilla it will print others)
	//var browser = (!!window.chrome && !!window.chrome.webstore)?"chrome":(typeof InstallTrigger !== 'undefined')? "Mozilla":"other";
	// You can view the information in an alert to see things working like this:
	var errorMsg = "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra + "\nUser:" + userData.kuser.kuname.first + "\nEmail:" + userData.kuser.kuemail + "\n" + window.location.href+flow+"\n"+browser;
	console.log(errorMsg);
	// TODO: Report this error via ajax so you can keep track
	//       of what pages have JS issues
	var parameters = {
		'log': errorMsg,
		'sendEmail': true,
		'subject': 'Kriya2: JavaScript Exception in Dashboard',
		'type': 'Kriya2: JavaScript Exception',
		'mailTo': 'crest@exeterpremedia.com'
	};
	eventHandler.common.functions.writeLog('logerrors', parameters, function (res) {
		console.log('Data Saved');
	})
	$('.la-container').css('display', 'none');
	var suppressErrorAlert = true;
	/*PNotify.removeAll();
	var notifyObj = {};
	notifyObj.notify = {};
	notifyObj.notify.notifyTitle = 'Error Message';
	notifyObj.notify.notifyText = "Sorry, we're unable to process your request. The error information sent to support team. Please refresh and try again.";
	notifyObj.notify.notifyType = 'error';
	eventHandler.common.functions.displayNotification(notifyObj);*/
	// If you return true, then error alerts (like in older versions of
	// Internet Explorer) will be suppressed.
	return suppressErrorAlert;
};
$('body').on({
	click: function (evt) {
		var $tbl = $("#chartReportTable");
		var index = parseInt($(this).attr("index"));
		if ($(this).prop("checked")) {
			$tbl.find('tr :nth-child(' + (index) + ')').removeClass('reportCheckBox');
			$tbl.find('tr :nth-child(' + (index) + ')').show();
		} else {
			$tbl.find('tr :nth-child(' + (index) + ')').addClass('reportCheckBox');
			$tbl.find('tr :nth-child(' + (index) + ')').hide();
		}
	},
}, '#reportColHideShow input:checkbox');

$(document).ready(function () {
	$(function () {
		eventHandler.dropdowns.project.reportDateRangePicker();
		// To handle the drag and drop events for book customer
		$('#manuscriptContent #manuscriptsData')
			.on('mouseenter', '.sectionDataChild', function(e){
				// if you do not have another article following the current article then movedown button should not be visible
				if ($(this).next().hasClass('sectionDataChild')){
					//   $('.sectionDataChild').not('.hide').find('.controlIcons.icon-arrow_downward').removeClass('hide');
				}
				else{
					// $(this).find('.controlIcons.icon-arrow_downward').addClass('hide');
				}
			})
			.on('dragenter', '.sectionDataChild', dragEnter)
			.on('dragleave', '.sectionDataChild', dragLeave)
			.on('dragover', '.sectionDataChild', dragOver)
			.on('drop', '.sectionDataChild', drop)
			.on('drop', '.sectionData', drop)
			.on('dragenter', '.sectionData', dragEnter)
			.on('dragleave', '.sectionData', dragLeave)
			.on('dragover', '.sectionData', dragOver)
			.on('dragenter', '.sectionHeader', dragEnter)
			.on('dragleave', '.sectionHeader', dragLeave)
			.on('dragover', '.sectionHeader', dragOver)
		$('#manuscriptContent #sectionContent')
			.on('dragstart', '.uaa', dragStart)
			.on('dragend', '.uaa', dragEnd)

		// To handle the drag and drop events in issue makeup
		$('#issueContent #manuscriptsData')
			.on('mouseenter', '.sectionDataChild', function(e){
				// if you do not have another article following the current article then movedown button should not be visible
				if ($(this).next().hasClass('sectionDataChild')){
					//   $('.sectionDataChild').not('.hide').find('.controlIcons.icon-arrow_downward').removeClass('hide');
				}
				else{
					// $(this).find('.controlIcons.icon-arrow_downward').addClass('hide');
				}
			})
			.on('dragenter', '.sectionDataChild', dragEnter)
			.on('dragleave', '.sectionDataChild', dragLeave)
			.on('dragover', '.sectionDataChild', dragOver)
			.on('drop', '.sectionDataChild', drop)
			.on('drop', '.sectionData', drop)
			.on('dragenter', '.sectionData', dragEnter)
			.on('dragleave', '.sectionData', dragLeave)
			.on('dragover', '.sectionData', dragOver)
			.on('dragenter', '.sectionHeader', dragEnter)
			.on('dragleave', '.sectionHeader', dragLeave)
			.on('dragover', '.sectionHeader', dragOver)
		$('#issueContent #sectionContent')
			.on('dragstart', '.uaa', dragStart)
			.on('dragend', '.uaa', dragEnd)
	});
});

$('body').on('click', function () {
	$('#searchdoi').hide();
})

$('body').on('click','#manuscriptsData .msCard', function () {
	$(this).siblings().removeClass('highlightCard');
	$(this).addClass('highlightCard');
	if ($('#filterCustomer .filter-list.active').attr('data-type') == 'book' || $('.dashboardTabs.active').attr('id') == 'issueContent') {
		if ($('#filterCustomer .filter-list.active').attr('data-type') == 'book') {
			eventHandler.components.actionitems.showRightPanel({ 'target': 'manuscript', 'type': 'book' }, $(this))
		} else {
			eventHandler.components.actionitems.showRightPanel({ 'target': 'manuscript' }, $(this))
		}
		$(currentTab+'#rightPanelContainer').find('[data-component-type="mspdf"]').attr('data-r-uuid', $(this).attr('data-uuid'));
		$(currentTab+'#rightPanelContainer').find('[data-component-type="mspdf"]').attr('data-dc-doi', $(this).attr('data-doi'));
	}
});

// Form validation for adding new books for book customer in dashboard.
$('#addProj input').on('focusin', function () {
	$('#addProj #bookTitle').on('focusout input', function () {
		if ($('#addProj #bookTitle').val().trim() != '') {
			$('#addProj #bookTitle').removeClass('is-invalid');
		} else {
			$('#addProj #bookTitle').addClass('is-invalid');
			return false;
		}
	})
	$('#addProj [data-cloned="true"] [data-name]#corresAuthor,#addProj [data-cloned="true"] [data-name]#corresEditor').on('focusout input', function () {
		if ($(this).val().trim() != '') {
			$(this).removeClass('is-invalid');
		} else {
			$(this).addClass('is-invalid');
		}
	})
})


// Form validation for adding new chapters, backmatter, epub for books in dashboard.
$('#addChapter input').on('focusin', function () {
	$('#addChapter [data-validate="true"]').on('focusout input', function () {
		if ($(this).attr('type') != 'file' && $(this).val().trim() != '') {
			$(this).removeClass('is-invalid');
		} else if ($(this).attr('type') == 'file') {
			if ($(this).attr('accept') == '.zip' &&	$(this)[0].files.length && /zip/.test($(this)[0].files[0].type)) {
				$(this).removeClass('is-invalid');
				return false;
			} else if ($(this).attr('accept') == '.epub' &&	$(this)[0].files.length && $(this)[0].files[0].type == "application/epub+zip") {
				$(this).removeClass('is-invalid');
				return false;
			} else {
				$(this).addClass('is-invalid');
				return false;
			}
		} else {
			$(this).addClass('is-invalid');
			return false;
		}
	})
})
window.addEventListener("resize", function () {
	$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .windowHeight').offset().top) - $('#footerContainer').height()) + 'px');
	if (!$('.dashboardTabs.active #rightPanelContainer').is(":hidden")) {
		$('.dashboardTabs.active #rightPanelContainer .tabContent:not(.hide)').css('max-height', (window.innerHeight - ($('.dashboardTabs.active #rightPanelContainer .tabContent:not(.hide)').offset().top) - $('#footerContainer').height()) + 'px');
		$('.dashboardTabs.active #manuscriptsData #tocContentDiv #sectionGroup').css('max-height', (window.innerHeight - ($('.dashboardTabs.active #manuscriptsData #tocContentDiv #sectionGroup').offset().top) - $('#footerContainer').height()) + 'px');
	}
});
