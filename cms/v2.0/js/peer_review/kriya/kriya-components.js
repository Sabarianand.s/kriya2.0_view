/* kriya loader JS */
/* Loading content */

(function () {

    // 1. PRIVATE INITIALIZER
    function init() {
        kriya.config  = config;
		kriya.actions = actions;

		kriya.config.content.doi = $('#contentContainer').attr('data-doi');
		kriya.config.content.customer = $('#contentContainer').attr('data-customer');
		kriya.config.content.project = $('#contentContainer').attr('data-project');
		kriya.config.content.stage = $('#contentContainer').attr('data-stage');
		kriya.config.content.stageFullName = $('#contentContainer').attr('data-stage-name');
		kriya.config.content.role = $('#contentContainer').attr('data-role') || 'reviewer';
		kriya.classes();
        kriya.components();
		kriya.events();
	}

	//#249 - if middle mouse button is clicked, event.which is undefined - Prabakaran. A (prabakaran.a@focusite.com)
	$('body').mousedown(function(e){
		kriya.pasteHandleEvent = e;
	});
	$('body').keydown(function(e){
		kriya.pasteHandleEvent = e;
	});
	//End of #249

    // 2. CONFIGURATION OVERRIDE
    var config = {
		viewMode: "web", // "web", "print", "pdf"
		comps: {
			editable : false, // based on user, comp
		},
		containerElm        : "#contentDivNode",
		metaElm             : ".front",
		blockElements       : ['p','h1','h2','h3','h4','h5','h6','ul','ol','div'],
		activeElements      : "",
		nonEditableElemets  : "",  //Set the content editable false
		//Selector to prevent the typing and drag and drop elements
		//#295 - All | Components | Components popup | All Browsers - Prabakaran.A (prabakaran.a@focusite.com)
		preventTyping       : ".jrnlArtType,.jrnlSubject,.jrnlAuthors,.jrnlGroupAuthors,.jrnlAffGroup,.jrnlRefText,.jrnlHistory,.jrnlEditedByHead,.jrnlEditorGroup,.jrnlReviewerGroup,.jrnlCorrAff,.jrnlPresAdd, .jrnlEqContrib, .award-group, .jrnlFigure, .jrnlInlineFigure, .jrnlSupplSrc, .jrnlCitation, .jrnlEthicsFN[data-track='del'], .removeNode, .floatHeader, .jrnlEthicsGroup .jrnlFNHead, .jrnlMajorDatasets .jrnlDatasetHead, .jrnlMajorDatasets .jrnlDatasets, .jrnlKwdGroup .jrnlKeywordHead, .jrnlPermission, [data-editable='false'], *[data-track='del'], .jrnlDeleted, .jrnlPubDate, .jrnlENDNotes, .linkCount,.markAs,.linkNowBtn,.jrnlTblFootHead,.jrnlObjectID,.leftHeader,.headerRow, .jrnlTblDefItem, .jrnlRelArtGroup",
		preventTypingErMsg  : {
			'jrnlArtType'      : 'Dear {$role}, please make changes to article type by clicking on the article type.',
			'jrnlSubject'      : 'Dear {$role}, please make changes to subject by clicking on the subject.',
			'jrnlAuthors'      : 'Dear {$role}, please make changes to author by clicking on the author name .',
			'jrnlGroupAuthors' : 'Dear {$role}, please make changes to group author by clicking on the group author name.',
			'jrnlAffGroup'     : 'Dear {$role}, please make changes to affiliation by clicking on the affiliation.',
			'jrnlRefText'        : 'Dear {$role}, please make changes to reference by clicking on the reference.',
			'jrnlHistory'        : 'Dear {$role}, please make changes to history by clicking on the history.',
			'jrnlEditorGroup'    : 'Dear {$role}, please make changes to editor by clicking on the editor name.',
			'jrnlReviewerGroup'  : 'Dear {$role}, please make changes to reviewer by clicking on the reviewer name.',
			'jrnlCorrAff'        : 'Dear {$role}, please make changes to contact details by clicking on the author name in the author list.',
			'jrnlPresAdd'        : 'Dear {$role}, please make changes to present address by clicking on the present address.',
			'award-group'        : 'Dear {$role}, please make changes to funding information by clicking on the funding information.',
			'jrnlMajorDatasets'  : 'Dear {$role}, please make changes to major datasets by clicking on the major datasets.',
			'jrnlPubDate'        : 'Dear {$role}, please make changes to pub date by clicking on the pub date.',
			'jrnlKwdGroup'       : 'Dear {$role}, please make changes to keyword by clicking on the keyword.',
			'jrnlENDNotes'       : 'Dear {$role}, please make changes to end note by clicking on the end note.',
			'jrnlTblDefItem'     : 'Dear {$role}, please make changes to def item by clicking on the def item.',
			'jrnlDefItem'     : 'Dear {$role}, please make changes to def item by clicking on the def item.'
		},
		//End of #295
		invalidSaveNodes    : ".front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,#historyDivNode,#infoDivContent,#navContainer,#pubData,#queryDivNode,#welcomeContainer", //Selectors to prevent from saving when get closest id
		preventSiblingId    : ".jrnlVolume,.jrnlIssue,.jrnleLocation,.jrnlFPage,.jrnlLPage,.jrnlLRH,.jrnlRRH,.jrnlEthicsFN,.jrnlFootNoteGroup,.jrnlPPubDate,.jrnlPubDate", // Prevent adding prev id and next id when save the content
		sameClassSiblingId  : ".jrnlCorrAff",
		activeBlockElement  : [],
		activeChildElements : [],
		saveQueue           : [],
		errorSaveQueue      : [],
		content             : {},
		exportOnsave        : null,
		citationSelector    : ".jrnlFigRef,.jrnlTblRef,.jrnlSupplRef,.jrnlVidRef,.jrnlBoxRef,.jrnlBibRef,.jrnlFootNoteRef,.jrnlSchRef", //Citation selector used to get the citation nodes
		preventTrackComp    : '.jrnlFigRef:not([data-track="del"]),.jrnlSchRef:not([data-track="del"]),.jrnlInlineFigure:not([data-track="del"]),.jrnlTblRef:not([data-track="del"]),.jrnlSupplRef:not([data-track="del"]),.jrnlVidRef:not([data-track="del"]),.jrnlBoxRef:not([data-track="del"]),.jrnlBibRef:not([data-track="del"]),.jrnlFootNoteRef:not([data-track="del"]),.jrnlEqnRef:not([data-track="del"]),.jrnlTblFNRef:not([data-track="del"]), .jrnlKeyword:not([data-track="del"]), .jrnlFigure, .jrnlAuthors, .jrnlGroupAuthors, .jrnlEditors, .jrnlReviewers, .award-group, .jrnlFN, .jrnlFNPara', //Selector to prevent showing the accept/reject popup in specific component
		checkForComp		: '.jrnlAff, .jrnlPresAdd, .jrnlDatasets, .jrnlKeyword, .jrnlEthicsFN, .jrnlDefItem, .kriyaFormula, .jrnlFN', //Selector to prevent showing the accept/reject popup in specific component
		preventMerging      : '.jrnlArtTitle, .jrnlSubDisplay', //Selector to prevent merging blocks in delete and backspace
		preventFormatting   : '[data-editable="false"]', //Selector to prevent formatting for specific nodes
		preventJrnlDelete	: '.jrnlArtTitle', //selector to prevent jrnlDeleted class
		preventCasing       : '.jrnlArtType,.jrnlSubject,.jrnlAuthors,.jrnlGroupAuthors,.jrnlAffGroup,.jrnlHistory' // selector to prevent casing
    };
    // 3. CUT,COPY,PASTE actions
    var actions = {
    	copy : function(){
    		document.execCommand('copy');
    	},
    	cut : function(){
    		if(kriya.selection){
    			var selectedText = kriya.selection.cloneContents();
    			document.execCommand('copy');
    			tracker.deleteContents();

    			var curRange = tracker.getCurrentRange();
    			kriyaEditor.settings.undoStack.push(curRange.startContainer);


			    /*var span = document.createElement("span");
			    span.appendChild(selectedText);
			    kriya.selection.insertNode(span);
			    $(span).kriyaTracker('del');
		    	kriyaEditor.settings.undoStack.push(span);*/

		    	kriyaEditor.init.addUndoLevel('cut-menu');
    		}
    	},
    	paste : function(type){
    		if(type == "html"){

    			var pasteNode = document.createElement('div');
  				pasteNode.setAttribute('class', 'kriya-paste-div');
  				pasteNode.setAttribute('contenteditable', 'true');

  				$(pasteNode).html('&nbsp;');
  				$('body').append(pasteNode);

  				var editorRange = tracker.getCurrentRange();

  				var range = document.createRange();
				range.setStart(pasteNode, 0);
				range.setEnd(pasteNode, 1);
				var winSel = window.getSelection();
				winSel.removeAllRanges();
				winSel.addRange(range);
				//return false;
    			document.execCommand('paste');

    			$(pasteNode).cleanTrackChanges();
    			var pasteHTML = pasteNode.innerHTML;
    			$(pasteNode).remove();
    			pasteHTML = cleanupOnPaste(pasteHTML);
    			// if(citeJS && typeof(citeJS.general.captureCitation) == "function"){
			    //     pasteHTML = citeJS.general.captureCitation(pasteHTML);
			    // }
    			var pasteNodes = $.parseHTML(pasteHTML);
    			$(pasteNodes).each(function(){
    				tracker.insert(this, editorRange);
    			});
    			var saveNode = $(pasteNodes).parents('[id]:first');
    		}else if(window.clipboardData && window.clipboardData.getData){
    			var pasteText = window.clipboardData.getData('Text');
    			var range = tracker.getCurrentRange();
    			// if(citeJS && typeof(citeJS.general.captureCitation) == "function"){
			    //     pasteHTML = citeJS.general.captureCitation(pasteHTML);
			    // }
			    tracker.insert(pasteText, range);
			    var saveNode = $(pasteNodes).parents('[id]:first');
    		}
    		kriyaEditor.settings.undoStack.push(saveNode);
    		kriyaEditor.init.addUndoLevel('paste-menu');
    	},
    	/**
    	 * Function to execute the command
    	 * @param arguments[0] - function name or command to execute
    	 * @param arguments[1] - parameters to the function
    	 */
    	execCommand: function(){
    		var range = rangy.getSelection();
			var targetElement = range.anchorNode;
			targetElement = $(targetElement).closest('[id]');
    		if(arguments[0]){
    			if(arguments[0].match(/cut|copy|paste/) && arguments[0]){
    				if(document.queryCommandEnabled(arguments[0])){
    					this[arguments[0]](arguments[1]);
						return;
    				}else{
    					kriya.notification({
							title : 'ERROR',
							type  : 'error',
							timeout : 8000,
							content : "Your browser doesn't support direct access to the clipboard. Please use the keyboard shortcuts (Ctrl+X/C/V) instead.",
							icon: 'icon-warning2'
						});
    				}
				}
    			if(arguments[0].match(/indent|outdent|justify/)){
    				var saveNode = null; status=false;
					if (targetElement[0].nodeName == "TD" || $(targetElement).closest('td,th').length > 0){
						var textIndent = (!$(targetElement).closest('td,th').attr('text-indent') || $(targetElement).closest('td,th').attr('text-indent')<=0) ? 0 : $(targetElement).closest('td,th').attr('text-indent');
						if (arguments[0] == "indent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html('&emsp;'+$(this).html());
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)+1);
							});
						}else if (arguments[0] == "outdent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html($(this).html().replace(/^\u2003/, ''));
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)-1);
							})
						}
						$("td[text-indent]").each(function() {
						    if($(this).attr("text-indent") <= 0) $(this).removeAttr('text-indent');
						});
						saveNode = $('.wysiwyg-tmp-selected-cell').closest('table');
					}else if (targetElement[0].nodeName == "LI" || $(targetElement).closest('li').length > 0 || $(targetElement).siblings('li').prevObject.length > 0){
						if (arguments[0] == "indent"){
							status=editor.composer.commands.exec('indentList');
						}else if (arguments[0] == "outdent"){



							var paraEl = editor.composer.commands.exec('outdentList');

						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//var parentEle = $(targetElement).closest('ul,ol')[0].parentElement;

                        //On Indent or outdent the next or the previous list style should be applied. - Lakshminarayanan. S(lakshminarayanan.s@focusite.com)
							var optionTexts = [];
							if($(targetElement).closest('ul').length == 0){
								$("#order-list-dropdown li").each(function(key,val) {
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');

									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument)
									});
							} else{

								$("#unorder-list-dropdown li").each(function(key,val) {
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');
									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument)
								});
							}
						if (!status && arguments[0] == "indent"){
							var listStyle = optionTexts[(optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType)+1)%optionTexts.length];
							$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							$(targetElement).closest('ul,ol')[0].type = listStyle;
                            kriyaEditor.settings.undoStack.push($(targetElement).closest('ul,ol').parents('ul,ol')[0]);
						}else {
							//var listStyle = optionTexts[optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.style.listStyleType)-1];
							//$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							//$(targetElement).closest('ul,ol')[0].type = listStyle;
						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType;
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')//[0].parentElement.parentElement.style.listStyleType;
						saveNode = [$(targetElement).closest('ul,ol')[0]];
					}else{
						saveNode = targetElement;
						document.execCommand(arguments[0]);
					}
					if(typeof saveNode !=undefined && saveNode[0]!=undefined){
					//#535-Duplication happens after refresh while indend-Helen.j@focusite.com
					saveNode = $(saveNode).parent().closest('ol,ul[id]').length > 0 ? $(saveNode).parent().closest('ol,ul[id]') : saveNode;
					//End of #535
					kriyaEditor.settings.undoStack.push($(saveNode)[0]);}
					kriyaEditor.settings.undoStack = kriyaEditor.settings.undoStack.concat(paraEl);
					kriyaEditor.init.addUndoLevel('formatting');
					return;
    			}else{
    				//Handling list
	    			if(arguments[0].match(/list/i)){
						var listNode = null;
					if($(range.anchorNode).parents('ul,ol').length > 0){
	    					listNode = $(range.anchorNode).parents('ul,ol').first();
							listNode.attr('type', arguments[1]);
							listNode.css('list-style-type', arguments[1]);
							//rename the elemet if list type is change
                            //#18 - For create tmpRange for change bullets after none by - Vimala.J ( vimala.j@focusite.com)
                            var tmpRange = rangy.saveSelection();
							if(arguments[0].match(/unOrderedList/i)){
								$(listNode).renameElement('ul');
							}else{
								$(listNode).renameElement('ol');
							}
                            //#18 - send tmpRange restoreselection by - Vimala.J ( vimala.j@focusite.com)
							rangy.restoreSelection(tmpRange);
							// End of #18
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}else{
	    					editor.composer.commands.exec(arguments[0], arguments[1]);
	    					listNode = $(targetElement).closest('ul,ol').first();
							listNode.css('list-style-type', arguments[1]);
							if (! $(listNode)[0].hasAttribute('id')){
								$(listNode).attr('id', uuid.v4());
							}
							$(listNode).find('li,p').each(function(){
								if (! $(this)[0].hasAttribute('id')){
									$(this).attr('id', uuid.v4());
								}
								$(this).attr('class','jrnlListPara');
							});
							$(listNode).attr('data-inserted', 'true');

							$(listNode).find('li p').each(function(){
								var clonedPara = $(this).clone(true);
								clonedPara.attr('data-removed', 'true');
								kriyaEditor.settings.undoStack.push(clonedPara[0]);
							});
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}

						kriyaEditor.init.addUndoLevel('formatting');
						createCloneContent();
						return;

	    		}
	    			//editor.composer.commands.exec(arguments[0], arguments[1]);
	    			/*if(!tracker || tracker.length < 1){
    					var formatedNode = $(window.getSelection().getRangeAt(0).startContainer.parentElement);
    					formatedNode.kriyaTracker('sty');
	    			}else if(kriya.selection.text() == kriya.selection.startContainer.parentElement.textContent){
	    				var cid = $(kriya.selection.startContainer.parentElement).attr('data-cid');
						$('#updDivContent'+' [data-rid="' + cid + '"]').find('.reject-changes').trigger('click');
	    			}*/
    			}
    			//kriyaEditor.init.addUndoLevel('formatting', targetElement);
			}
    	},
    	confirmation: function(param,targetNode){
			/*var sampleSettings = {
				'icon'  : '<i class="material-icons">warning</i>',
				'title' : 'Confirmation Needed',
				'text'  : 'Are you sure do you want to delete?',
				'size'  : 'pop-sm',
				'buttons' : {
					'ok' : {
						'text' : 'Yes',
						'class' : 'btn-success',
						'message' : "{'click':{'funcToCall': 'deleteElement','channel':'components','topic':'general'}}"
					},
					'cancel' : {
						'text' : 'No',
						'class' : 'btn-danger',
						'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
					}
				}
 			}*/

 			// If targetNode is not null then hidden the popup and pass the closest popup - rajesh
 			if(targetNode=='equalContrib'){
				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], kriya.config.containerElm);
			 }
			 else if(targetNode){
				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], targetNode.closest('[data-type="popUp"]')[0]);
			 }
			 else{
 				$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
 				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], kriya.config.containerElm);
 			}
    		if(param.size){
    			$('[data-component="confirmation_edit"]').attr('data-pop-size', param.size);
    		}
    		if(param.icon){
    			$('[data-component="confirmation_edit"] .popupHead .popIcon').html(param.icon);
    		}
    		if(param.title){
    			$('[data-component="confirmation_edit"] .popupHead .popTitle').html(param.title);
    		}
    		if(param.text){
				$('[data-component="confirmation_edit"] .popupBody .popUpText').html(param.text);
			}
			if(param.hideElement){
				$('[data-component="confirmation_edit"]').find('.'+param.hideElement).addClass('hidden');
			}
    		$('[data-component="confirmation_edit"] .popupFoot').html('');
    		if(param.buttons && typeof(param.buttons) != "string"){
    			$.each(param.buttons, function(i,val){
    				var newBtn = $('<span class="btn btn-medium" />');
    				if(val.class){
    					newBtn.addClass(val.class);
    				}
    				if(val.message){
    					newBtn.attr('data-message', val.message);
    				}
    				if(val.attr){
    					$.each(val.attr, function(i, val){
    						newBtn.attr(i, val);
    					});
    				}
    				if(val.text){
    					newBtn.html(val.text);
    				}
    				$('[data-component="confirmation_edit"] .popupFoot').append(newBtn);
    			});
    		}
    	},
		removeSelectionFromTableCell: function(){
			var selectedCells = $('.wysiwyg-tmp-selected-cell');
			if (selectedCells.length > 0) {
				for (var i = 0; i < selectedCells.length; i++) {
					wysihtml.dom.removeClass(selectedCells[i], 'wysiwyg-tmp-selected-cell');
				}
			}
		}
    };
    // 4. GLOBAL NAMESPACE
    window.kriya = {
        // allows optional configuration on the object
        classes: function () {
            return {
               // moduleOne: new kriya.ModuleOne(),
               // moduleTwo: new kriya.ModuleOne(config.modTwo)
			   // utilsPack: new kriya.kriyaUtils()
            };
        },
        notification: function(param){
        	var id = uuid.v4();
        	var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
			notice.append('<div class="row kriya-notice-header" />');
			notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');

			if(param.icon){
				notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
			}
			if(param.title){
				notice.find('.kriya-notice-header').append(param.title);
			}

			if(param.closeIcon != false){
				notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="kriya.removeNotify(this)"></i>');
			}


			Materialize.toast(notice[0].outerHTML, param.timeout);
			return id;
        },
        removeNotify: function(target){
        	if(target){
        		$(target).closest('.toast').fadeOut(function(){
			        $(this).remove();
			    });
        	}
        },
        removeAllNotify: function(){
        	$('.toast').fadeOut(function(){
		        $(this).remove();
		    });
        },
        // has private configuration
        components: function () {
			/*$.getJSON( "./js/kriya/components.json", function( data ) {
			  	kriya.componentList = data;
			  	console.log(data);
			});*/
			kriya.componentList = {
				'jrnlArtTitle' : {'type': 'contentEditable'},
				'jrnlAbsPara'  : {'type': 'contentEditable'},
				'jrnlBoxText'  : {'type': 'contentEditable'},
				'jrnlCorrAff'  : {'type': 'contentEditable'},
				'jrnlSecPara'  : {'type': 'contentEditable'},
				'jrnlFigCaption' : {'type': 'contentEditable'},
				'jrnlTblCaption' : {'type': 'contentEditable'},
				'jrnlListPara'   : {'type': 'contentEditable'},
				'jrnlTblFoot'    : {'type': 'contentEditable'},
				'jrnlFigFoot'    : {'type': 'contentEditable'},
				'jrnlHead1'      : {'type': 'contentEditable'},
				'jrnlHead2'      : {'type': 'contentEditable'},
				'jrnlHead3'      : {'type': 'contentEditable'},
				'jrnlHead4'      : {'type': 'contentEditable'},
				'jrnlHead5'      : {'type': 'contentEditable'},
				'jrnlHead6'      : {'type': 'contentEditable'},
				'jrnlPara'       : {'type': 'contentEditable'},
				'jrnlFNHead'	: {'type': 'contentEditable'},
				'jrnlFundingHead'	: {'type': 'contentEditable'},
				'jrnlDatasetHead'	: {'type': 'contentEditable'},
				'jrnlKeywordHead'   : {'type': 'contentEditable'},
				'jrnlAbsTitle'      : {'type': 'contentEditable'},
				'jrnlBoxCaption'    : {'type': 'contentEditable'},
				'floatLabel'        : {'type': 'contentEditable'},
				'jrnlSupplCaption'  : {'type': 'contentEditable'},
				'jrnlVidCaption'    : {'type': 'contentEditable'},
				'jrnlAbsHead'    	: {'type': 'contentEditable'},
				'jrnlRRH'       	: {'type': 'contentEditable'},
				'jrnlBiography'    	: {'type': 'contentEditable'},
				'jrnlUncitedRef'  	: {'type': 'contentEditable'},
				'jrnlPossBibRef' : {
					'type': 'popUp',
					'class': 'autoWidth',
					'buttons': {
						'Add/Cite Reference': {
							'channel': 'menu', 'topic': 'insert', 'event': 'click', 'message': "{'funcToCall': 'newCite', 'param': {'method': 'ADD', 'type': 'QUERY', 'value': ''}}"
						}
					}
				},
				'jrnlRefHead'	: {'type': 'contentEditable'}
			};


			$('.templates *[data-component][data-type]').each(function(){
				kriya.componentList[$(this).attr('data-component')] = [];
				kriya.componentList[$(this).attr('data-component')].type = $(this).attr('data-type');
			});
			//Add buttons in content
			if ($('#contentContainer').attr('data-state') != 'read-only'){
				/*var contentBtns = $('#compDivContent [data-component="contentButtons"] [data-xpath]');
				contentBtns.each(function(){
					var xpath = $(this).attr('data-xpath');
					//var btnContainer = kriya.xpath(xpath);
					var clonedNode = $(this).clone(true);
					if($(btnContainer).length > 0){
						$(btnContainer).append(clonedNode);
					}
				});
				$.each(kriya.config.content.newBtnContainers, function(i, val){
					var topic = 'PopUp';
					if (val.topic) topic = val.topic;
					var eventMessage = 'data-channel="components" data-topic="' + topic + '" data-event="click" data-message="{\'funcToCall\': \''+val.func+'\', \'param\' : {\'component\' : \''+val.component+'\'}}"';
					if(typeof(val.eventMessage) != "undefined"){
						eventMessage = (val.eventMessage != "")?'data-message="' + val.eventMessage + '"':'';
					}
					var className = 'btn btn-small lighten-1 contentBtn';
					if (val.condition && $(val.condition).length > 0) className += ' hidden';
					var btnNode = $('<span class="' + className + '" ' + eventMessage + ' contenteditable="false"></span>');
					if(val.icon){
						btnNode.append('<i class="' +val.icon+ '" />');
					}
					if(val.btnText){
						btnNode.append(val.btnText);
					}
					//append the button if the role is not defined or if the reviewer is defined in the config
					if(!val.role || (val.role && kriya.config.content.role && val.role.split(',').indexOf(kriya.config.content.role) >= 0)){
						$(config.containerElm+' .'+val.container).append(btnNode);
					}
				});*/
			}
			//Add content editable false to nonEditable elements
			if(kriya.config.nonEditableElemets){
				$(kriya.config.nonEditableElemets).attr('contenteditable', 'false');
			}
			$(config.containerElm).css('overflow-y', 'auto');
		},

        // has private configuration
        events: function () {
			/*$('body').on({
				click: function (evt) {
					$('.content-scrollbar .node').remove();
					if ($(this)[0].hasAttribute('id')){
						if ($('#contentDivNode [data-id="' + $(this).attr('id') + '"]').length == 0){
							var id = $(this).attr('id').replace('BLK_', '');
							if ($('#contentDivNode [data-id="' + id + '"]').length > 0){
								$('#contentDivNode [data-id="' + id + '"]')[0].scrollIntoView();
							}
						}else{
							$('#contentDivNode [data-id="' + $(this).attr('id') + '"]')[0].scrollIntoView();
						}
						var citationType = $(this).attr('data-type');
						if (typeof(commonjs.settings.citationClass[citationType]) != 'undefined'){
							var citeClass = commonjs.settings.citationClass[citationType]['floatCitationClass'];
							elementOnScrollBar($('#contentDivNode ' + citeClass + '[data-citation-string*="' + $(this).attr('id').replace('BLK_', '') + ' "]'), 'data-citation-string', citationType);
						}
					}else if ($(this)[0].hasAttribute('data-panel-id') && $('#contentDivNode [data-id="'+ $(this).attr('data-panel-id') +'"]').length > 0){
						$('#contentDivNode [data-id="'+ $(this).attr('data-panel-id') +'"]')[0].scrollIntoView();
						var citationType = $(this).attr('data-type');
						if (typeof(commonjs.settings.citationClass[citationType]) != 'undefined'){
							var citeClass = commonjs.settings.citationClass[citationType]['floatCitationClass'];
							elementOnScrollBar($('#contentDivNode ' + citeClass + '[data-citation-string*="' + $(this).attr('data-panel-id') + ' "]'), 'data-citation-string', citationType);
						}
					}
				}
			}, '#navContainer .resource_reference, #navContainer .jrnlFigBlock, #navContainer .jrnlTblBlock, #navContainer .card');*/


			//reset the go back history added by vijayakumar on 5-10-2018
			history.pushState(null, null, $(location).attr('href'));
			window.addEventListener('popstate', function () {
			history.pushState(null, null, $(location).attr('href'));
			});

			$(config.containerElm).find('divs > * > *').on('focusin', function (evt) {
				//evt.preventDefault();
				var target = evt.target || evt.srcElement;
				kriya.evt = evt;
				kriya.activeBlockElement = [];
				if ($(target).attr('id') != undefined){
					kriya.activeBlockElement[0] = target;
				}else{
					while (config.blockElements.indexOf(target.nodeName.toLocaleLowerCase()) == -1){
						var target = target.parentNode;
					}
					if ($(target).closest('.front').length > 0){
						kriya.activeBlockElement[0] = target;
					}else if ($(target).attr('id') != undefined){
						kriya.activeBlockElement[0] = target;
					}
				}
				kriya.activeBlockElement[1] = $(kriya.activeBlockElement[0]).html();
			});

			$('body').on({
				focusout: function (evt) {
					if($(this).text() != ""){
						var component = $(this).closest('.carousel-item, [data-type="popUp"]');
						var selector = component.find('.dropdown-link-content').attr('data-source-selector');
						var container = kriya.xpath(selector);
						container = $(container).parent();
						$(this).removeAttr('contenteditable');
						var tagName = $(this).closest('li').attr('data-tag');
						tagName = (tagName) ? tagName : 'p';
						var className = $(this).closest('.collection').attr('data-source');
						var newNode = document.createElement(tagName);
						newNode.setAttribute('class', className);
						newNode.innerHTML = $(this).html();
						$(newNode).find('.material-icons').remove();
						var newID = $(this).closest('li').attr('data-rid');
						newNode.setAttribute('id', newID);
						$(newNode).kriyaTracker('ins');
						container.append(newNode);
					}else{
						$(this).closest('li').remove();
					}
				},
			}, '.collections [contenteditable="true"]');//commented by jai

			$('body').on({
				focusin: function (evt) {
					$(".findAction .searchBtn").removeClass('disabled');
				},
			}, '.searchModal .search');//enable search button on find text box focus - priya #119-#123

			$('body').on({
				keydown: function (evt) {
					if(evt.keyCode==13){
						evt.preventDefault();
					}
				},
			}, '.searchModal .search, .findModal .replace');//enable search button on find text box focus - priya #119-#123

			$('body').on({
				paste: function (evt){
					var textData = evt.originalEvent.clipboardData.getData('text');
					if(/<[a-z][\s\S]*>/i.test(textData)){
						$(this).html($(textData).text());
					}else{
						$(this).html(textData);
					}
					evt.preventDefault();
				}
			}, '.searchModal .search, .findModal .replace');

			$('.jrnlTblBlock').scroll(function (evt) {
				var leftPosition = $(evt.target).scrollLeft();
				if($("#clonedContent").find(".highlight[data-tableblock='"+$(evt.target).attr('id')+"']").length>0){
					$("#clonedContent").find(".highlight[data-tableblock='"+$(evt.target).attr('id')+"']").each(function(){
						$(this).css("left",$(this).attr("data-left")-leftPosition+"px");
					});
				}
			});// table horizontal scroll #383 - priya

			$('body').on({
				click: function(evt){
					if($('#specialCharacter.tm-showModal').length>0){
						var range = rangy.getSelection();
						var currentSelection = range.getRangeAt(0);
						if($(currentSelection.startContainer).closest('#contentDivNode , .findSection').length>0){
							$('#specialCharacter').data("currentSelection",currentSelection);
						}
					}
				}
			},'#contentDivNode , .findSection'); //set cursor range in attribute - priya #122

			$('body').on({
				click: function(evt){
					if(!($(evt.target).closest('[data-class]').attr('data-class') == "jrnlProName")){
						$('[data-component="productAuthor_add"]').addClass('hidden');
					}
					if(!($(evt.target).closest('[data-class]').attr('data-class') == "jrnlRelArtAuthor")){
						$('[data-component="RelAuthor_add"]').addClass('hidden');
					}
				}
			},'[data-component="jrnlProduct_edit"] , [data-component="jrnlRelArt_edit"]'); //to hide delete popup - aravind

			$('body').on({
				click: function(evt){
					if($(this).closest('[data-type="popUp"]').find('.collection-item.active').length == 0){
						kriya.config.citeIds = [];
					}
					if($(this).closest('.collection').attr('multiselect') != "true"){
						$(this).closest('.collection').find('.collection-item').removeClass('active');
						kriya.config.citeIds = [];
					}
					var dataId = $(this).attr('data-rid');
					var existIndex = kriya.config.citeIds.indexOf(dataId);
					if($(this).hasClass('active')){
						$(this).removeClass('active');
						kriya.config.citeIds.splice(existIndex, 1);
					}else{
						$(this).addClass('active');
						if(dataId && existIndex < 0){
							kriya.config.citeIds.push(dataId);
						}
					}
				}
			}, '.collection.selectable .collection-item');

			$('body').on({
				drop: function(evt){
					evt.preventDefault();
					evt.stopPropagation();
					var targetNode = evt.target;
					// Notification popup for drop restrict #277 - priya
					if($(targetNode)[0].nodeName.toLowerCase()=="img" || $(targetNode).closest("img").length>0 || $(targetNode).find("img").length>0){
						var message = '<div class="row">Please use <span class="btn btn-small replaceObject" style="padding:8px 5px;margin:0px;background:#0d618b;">Replace</span> on the corresponding objects like Figure, table to upload a new file.</div>';
					}else{
						var message = '<div class="row">Drop Restricted.</div>';
					}
					kriya.notification({
						title : 'Information',
						type  : 'success',
						content : message,
						icon : 'icon-info'
					});
					return;
				}
			}, '#contentDivNode');

			$('body').on({
				click: function(evt){
					var fIdArray = [];
					if(kriya.config.citeIds){
						fIdArray = $.merge(fIdArray, kriya.config.citeIds);
					}
					var popper = $(this).closest('[data-component]');

					var displayBox = popper.find('.selectedCitation');
					if(displayBox.find('option').length > 0){
						displayBox.find('option.indirectCitation').html('Indirect');
						displayBox.find('option.directCitation').html('Direct');


						var refCiteConfig = citeJS.floats.getConfig('R');
						//Get the indirect citation id list
						var indirectIDList = fIdArray;
						if(refCiteConfig && refCiteConfig.sortfor == "indirect" || !refCiteConfig || !refCiteConfig.sortfor){
							indirectIDList = kriya.general.sortMultiCiteIDs(fIdArray);
							var refSortList = kriya.general.sortRefCitationIDs(indirectIDList);
							if(refSortList){
								indirectIDList = refSortList;
							}
						}

						// Get the indirect citation
						var citeHTML = kriya.general.getNewCitation(indirectIDList, kriya.config.containerElm, '');
						if(citeHTML){
							displayBox.find('option.indirectCitation').html(citeHTML);
						}

						//Get the direct citation id list
						var directIDList = fIdArray;
						if(refCiteConfig && refCiteConfig.sortfor == "direct" || !refCiteConfig || !refCiteConfig.sortfor){
							directIDList = kriya.general.sortMultiCiteIDs(fIdArray);
							var refSortList = kriya.general.sortRefCitationIDs(directIDList);
							if(refSortList){
								directIDList = refSortList;
							}
						}

						// get direct reference citation and append in drop dowm
						var citeHTML = kriya.general.getNewCitation(directIDList, kriya.config.containerElm, true, false);
						if(citeHTML){
							displayBox.find('option.directCitation').html(citeHTML);
						}
					}
				}
			}, '.collection.selectable[data-class$="Ref"] .collection-item');

			$('body').on({
				change: function(evt){
					var popper = $(this).closest('[data-component]');
					var refViewNode = $(this).closest('[data-ref-type]:visible');
					var existOnAbsense = $(this).closest('[existOnAbsence]').attr('existOnAbsence');
					var nodeXpath = popper.attr('data-node-xpath');
					var dataNode = kriya.xpath(nodeXpath);
					if(existOnAbsense  && refViewNode.length > 0 && $(dataNode).length > 0){
						existOnAbsense = existOnAbsense.split(' ');
						for(var e=0;e<existOnAbsense.length;e++){
							var className = existOnAbsense[e];
							var absenceNode = refViewNode.find('[data-class="' + className + '"][data-type="htmlComponent"]');
							if($(this).is(':checked')){
								absenceNode.html('');
								absenceNode.closest('.input-field').addClass('hidden');
							}else{
								var absenceNodeXpath = $(absenceNode).attr('data-node-xpath');
								var absenceData = kriya.xpath(absenceNodeXpath, dataNode[0]);
								if($(absenceData).length > 0){
									$(absenceNode).html($(absenceData).html());
								}
								absenceNode.closest('.input-field').removeClass('hidden');
							}
						}
					}
				}
			}, '[data-component="jrnlRefText_edit"] input[data-class="RefInPress"][type="checkbox"]');

			$('body').on({
				click: function(evt){
					if($(this).hasClass('active')){
						$(this).find('input.checkbox[type="radio"]').prop('checked', false);
						$(this).removeClass('active');
					}else{
						$('.refSearchContainer tbody tr').removeClass('active');
						$(this).find('input.checkbox[type="radio"]').prop('checked', true);
						$(this).addClass('active');
					}
				}
			}, '.refSearchContainer tbody tr');

			$('body').on({
				click: function(evt){
					$(this).closest('.dropdown-content').prev().dropdown('close');
				}
			}, '.dropdown-content li');

			//Save a new Element from a pop-up to another list
			$('body').on({
				keydown: function (evt) {
					if (evt.which == 13){
						console.log(evt);
						$('span[contenteditable]:visible').eq( $(this).index('span[contenteditable]:visible') + 1 ).focus();
						evt.preventDefault();
						evt.stopPropagation();
					}
				},
			}, '[data-type="popUp"] span[contenteditable]');

			$('body').on({
				keydown: function (evt) {
					if (evt.which == 13){
						console.log(evt);
						$(evt.target).removeAttr('contenteditable');
						$(evt.target).removeClass('activeElement');
						$('[data-type="popUp"]').addClass('hidden');
						evt.preventDefault();
						evt.stopPropagation();
					}
				},
			}, '.jrnlKeyword.activeElement');

			$('body').on({
				keydown: function (evt) {
					$(this).attr('width',$(this).outerWidth());
				},
			}, 'td:not([width]),th:not([width])');

			$('body').on({ // #161 - priya
				focus: function (evt) {
					var e = jQuery.Event("keyup");
					e.which = 32;
					$(evt.target).val(String.fromCharCode(e.which));
					$(evt.target).trigger(e);
				},
			}, '[data-type="popUp"][data-focus] .kriya-tagsinput');

			$('body').on({
				mousedown: function (evt) {
					//To prevent the selction in editor when click on header
					if($(evt.target).closest('.findSection').length < 1){
        				evt.preventDefault();
					}
				},
			}, '#headerContainer');

			$('body').on({
				click: function (evt) {
					$('.tableClone input:file').val('');
					$(this).parent().find('input:file').trigger('click');
				},
			}, '.tableClone .fileChooser');

			$('body').on( 'change', '.tableClone input:file', function(e){
				var targetNode = kriya.evt.target || kriya.evt.srcElement;
				var popper = $(targetNode).closest('[data-component]');

				var files = e.target.files;
				var f = files[0];
				excel2Table(f, function(tableHTML){
					tableHTML = '<div>' + tableHTML + '</div>';
					if($(tableHTML).find('table').length > 0){
						var tableInnerHtml = $(tableHTML).find('table').html();
						popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
						popper.find('.pasteTableDiv').addClass('hidden');
						popper.find('.resetTableBtn').removeClass('disabled');
					}
				});
			});

			$('body').on({
				click: function(){
					$("#suggesstion-box").addClass('hidden');
				}
			}, '[data-class="award-group"] *[class="text-line"],[data-class="award-group"] [data-input="true"]');

			$('body').on({
				keydown: function (evt) {
					$('[data-component="RefAuthor_add"]').addClass('hidden');
				},
			}, '[data-component="jrnlRefText_edit"] [data-class="RefAuthor"], [data-component="jrnlRefText_edit"] [data-class="RefEditor"]');

			$('body').on({
				click: function (evt) {
					$('[data-component="RefAuthor_add"]').addClass('hidden');
				},
			}, 'div[data-component="jrnlRefText_edit"]');

			$('body').on({
				click: function (evt) {
					//$('#contentDivNode,.leftPanel').toggleClass('editable');
				},
			}, '.ct-ignition__button.ct-ignition__button--edit, .ct-ignition__button.ct-ignition__button--cancel');

			$('body').on({
				click: function (evt) {
					$('.fullHeight.nano,.leftPanel').toggleClass('editable');
				},
			}, '.closeLeftPanel');

			$('body').on({
				mouseleave: function (evt) {
					$(this).css({
						'display' : '',
						'opacity' : '0',
						'left' : '',
						'top' : '',
					});
				}
			}, '[data-component*="_menu"][data-component^="jrnl"]');

			$('body').on({
				change: function (evt) {
					var eleClass = $(this).attr('data-class');
					var updatedValue = $(this).val();
					if($(this).attr('type') == "number" && eleClass.match(/Gap/g) && updatedValue){
						updatedValue = updatedValue+"pt";
					}else if($(this).attr('type') == "checkbox"){
						updatedValue = ($(this).is(':checked'))?1:'';
					}else if(eleClass == "floatStackWith"){
						var nodeXpath = $(this).find(':selected').attr('data-node-xpath');
						var float = kriya.xpath(nodeXpath);
						updatedValue = $(float).closest('[data-id^="BLK_"]').attr('data-id');
					}
					if(updatedValue){
						$(this).closest('[data-component]').find('tr.active .'+eleClass).html(updatedValue);
					}
					else{ // to remove value if set default - aravind
						$(this).closest('[data-component]').find('tr.active .'+eleClass).html('');
					}
				}
			}, '[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]');

			//Change the proof controls value when click on the list
			$('body').on({
				click: function(evt){
					$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatOrientation"], [data-class="floatStackWith"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]').val('');
					$('[data-class="floatContinued"]')[0].checked = false;

					if($(this).hasClass('active')){
						$(this).removeClass('active');
						$('.resetPlaceFloat').addClass('disabled');
						$('.figureControls').addClass('disabled');
						$('.boxType').addClass('disabled');
					}else{
						$(this).closest('table').find('tr').removeClass('active');
						$(this).addClass('active');
						var activeRow = this;
						$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]').each(function(){
							var eleClass = $(this).attr('data-class');
							var eleVal = $(activeRow).find('.'+eleClass).text();
							if($(this).attr('type') == "number"){
								eleVal = eleVal.replace(/[a-z]+/, '');
							}else if($(this).attr('type') == "checkbox"){
								if(eleVal == "1"){
									this.checked = true;
								}else{
									this.checked = false;
								}
							}
							if(eleClass == "floatStackWith"){
								$(this).find('option:not(:first)').removeAttr('value');
								var labelNode = $(kriya.config.containerElm + ' [data-id="' + eleVal + '"] .label');
								if(labelNode.length > 0){
									eleVal = labelNode.html();
								}
							}
							$(this).val(eleVal);
						});
						$('.resetPlaceFloat').removeClass('disabled');
						if($(kriya.xpath($(this).closest('tr').attr('data-node-xpath'))).attr('class')=="jrnlFigBlock"){
							$('.figureControls').removeClass('disabled');
						}else{
							$('.figureControls').addClass('disabled');
						}
						if($(kriya.xpath($(this).closest('tr').attr('data-node-xpath'))).attr('class')=="jrnlBoxBlock"){
							$('.boxType').removeClass('disabled');
						}else{
							$('.boxType').addClass('disabled');
						}
					}
				}
			}, '[data-component="placeFloats_edit"] tbody tr');

			//Reset the float place options
			$('body').on({
				click: function(evt){
					var popper = $(this).closest('[data-component]');
					var activeRow = $(popper).find('tbody tr.active');
					$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"]').each(function(){
							var eleClass = $(this).attr('data-class');
							$(activeRow).find('.'+eleClass).text('');
							if($(this).attr('type') == "checkbox"){
								this.checked = false;
							}
							$(this).val('');
						});
				}
			}, '[data-component="placeFloats_edit"] .resetPlaceFloat');

			$('body').on({
				keyup: function(){
					var topCss       = $(this).offset().top + $(this).height();
					var textVal      = $(this).text();
					textVal = textVal.replace(/^\s+|\s+$/, '');
					var listSelector = $(this).attr('data-list-selector');
					var listNode = kriya.xpath(listSelector);
					if($(listNode).length > 0){
						$(listNode).find('li').addClass('hidden');
						$(listNode).find('li:contains(' + textVal + ')').removeClass('hidden');

						$(listNode).removeClass('hidden').css({
							'display':'block',
							'top'    : topCss,
							'left'   : $(this).offset().left,
							'opacity': '1',
							'z-index': '9999'
						});
						var targetXpath = kriya.getElementTreeXPath(this);
						$(listNode).attr('data-target-selector', targetXpath);
					}
				}
			}, '[data-list-selector]');

			//Update the country when select the list
			$('body').on({
				click: function(){
					var selectedData = $(this).html();
					var listNode = $(this).closest('[data-target-selector]');
					if(listNode.length > 0){
						var targetXpath = listNode.attr('data-target-selector');
						var targetNode  = kriya.xpath(targetXpath);
						if($(targetNode).length > 0){
							$(targetNode).html(selectedData);
							$(targetNode).removeAttr('data-error');

							//to set cursor at enf of the field - jai
							var range = rangy.createRange();
							range.setStart($(targetNode)[0].childNodes[0], $(targetNode)[0].textContent.length);
							range.collapse(true);
							var sel = rangy.getSelection();
							sel.setSingleRange(range);
							listNode.addClass('hidden');
						}
					}
				}
			}, '[data-type="list"] li');

			$('body').on({
				click: function (evt) {
					$('[data-component="partLabel_add"]:visible').addClass('hidden'); //remove the add part label popup
					$('[data-type="list"]').addClass('hidden');
					if ($(evt.target).closest('[contenteditable="true"]').length > 0){
						var range = rangy.getSelection();
						kriya.popupSelection = range.getRangeAt(0);
						$('#specialCharacter').data("currentSelection",range.getRangeAt(0));
					}
				}
			}, '[data-type="popUp"]');

			$('body').on({
				change: function(evt){
					var checkedNode = $('[data-type="popUp"][data-component="jrnlRefText_edit"] [name="refSearch"]:checked');
					var placeHolderVal = checkedNode.attr('data-placeholder-val');
					var validateFUnc = checkedNode.attr('data-field-validate-func');
					var searchField = $('[data-type="popUp"][data-component="jrnlRefText_edit"] .searchField');

					if(validateFUnc){
						searchField.attr('data-validate', validateFUnc);
					}
					$('[data-type="popUp"][data-component="jrnlRefText_edit"] .refSearchContainer').html('');
					$('[data-type="popUp"][data-component="jrnlRefText_edit"] .refSearchContainer').addClass('hidden');
					if(placeHolderVal){
						searchField.attr('data-placeholder', placeHolderVal);
					}else{
						searchField.attr('data-placeholder', 'Enter the search text here');
					}

				}
			}, '[data-type="popUp"][data-component="jrnlRefText_edit"] [name="refSearch"]');

			$('body').on({
				keyup: function (evt) {
					if (evt.which != 13){
						var popper = $(this).closest('#specialCharacter');
						var searchText = $(this).val();
						searchText = searchText.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
						var searchList = popper.find('.advanced-symbols').find('.spl-char[title*="'+searchText+'"], .spl-char[data-html-entity*="'+ $(this).val() +'"]');
						var searchViewList = '';
						if(searchList.length > 0){
							searchList.each(function(i,ele){
								searchViewList += '<li><a href="#"><span data-name="char-name">'+$(this).attr('title')+'</span>&nbsp;&nbsp;&nbsp;&nbsp;<span data-name="char">'+$(this).text()+'</span></a></li>';
								if(i == 25){
									return false;
								}
							});
						}
						$('#special_char_search_list').html(searchViewList);
						$('#special_char_search_list').css({'display':'block','opacity':'1'});
					}
				}
			}, '.special_char_search');

			$('body').on({
				click: function (evt) {
					var popper = $(this).closest('#specialCharacter');
					var charName = $(this).find('[data-name="char-name"]').text();
					$('.special_char_search').val(charName);
					var charList = popper.find('.spl-char[title="'+charName+'"]').closest('.symb-container');
					if(charList.length > 0){
						popper.find('.symb-container').addClass('hidden');
						charList.removeClass('hidden');
					}
					popper.find('.spl-char.active').removeClass('active');
					popper.find('.spl-char[title="'+charName+'"]').addClass('active');
					$('.spl_char_drop_down').val(charList.attr('data-spl-group'));
					$('#special_char_search_list').css({'display':'none','opacity':'0'});
				}
			}, '#special_char_search_list li');

			$('body').on({
				mouseleave: function(evt){
					$('#special_char_search_list').css({'display':'none','opacity':'0'});
				}
			}, '#special_char_search_list');

			// change year in copyright statement
			$('body').on({
				change: function (evt) {
					if($('.jrnlPermission .jrnlCopyrightYear').html()!=undefined){
						var copyRightstmt = $('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightStmt"]').html();
						var regexData = new RegExp("\\b[0-9]{4}\\b","g");
						copyRightstmt = copyRightstmt.replace(regexData, $('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]').val());
						$('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightStmt"]').html(copyRightstmt);
					}
				}
			}, '[data-type="popUp"][data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]');

			//Prevent editing label in the popup
			$('body').on({
				keydown: function (evt) {

          			var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					var selectedNodes = range.getNodes();
					//filter the label node from the selected nodes
					var labelEle = $(selectedNodes).filter('.label');
					if(labelEle.length == 0){
						//If the cursor places in the label node
						labelEle = 	$(range.startContainer).closest('.label');
					}

          			var keycode = event.keyCode;

          			if (
						(keycode > 47 && keycode < 58) || // number keys
						keycode == 32 || keycode == 13 || // spacebar & return key(s) (if you want to allow carriage returns)
						keycode == 46 || keycode == 8 || // spacebar & return key(s) (if you want to allow carriage returns)
						(keycode > 64 && keycode < 91) || // letter keys
						(keycode > 95 && keycode < 112) || // numpad keys
						(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
						(keycode > 218 && keycode < 223) // [\]' (in order)
						){
							if(labelEle.length > 0){

		          				var spanNode = document.createElement('span');
		          				spanNode.setAttribute('class', 'unwrapNode');
		          				$(spanNode).insertAfter(labelEle);
		          				$(spanNode).html('&nbsp;');
		          				var range = document.createRange();
		        				range.setStart(spanNode, 0);
		        				range.setEnd(spanNode, 1);

								var winSel = window.getSelection();
		        				winSel.removeAllRanges();
		        				winSel.addRange(range);
		          			}
						}
				},
				keyup: function(evt){
					//Unwrap the added element in keydown to move next to the label
					if($(this).find('.unwrapNode').length > 0){
						var wrapContent = $(this).find('.unwrapNode').contents().unwrap();
	      				var range = document.createRange();
	    				range.setStartAfter(wrapContent[0]);
	          			range.collapse(true);

						var winSel = window.getSelection();
	    				winSel.removeAllRanges();
	    				winSel.addRange(range);
    				}
				}
			}, '[data-type="popUp"] [data-class*="Caption"]');

			$('body').on({
				paste: function (evt){
					if(evt.originalEvent.clipboardData){
						var targetNode = kriya.evt.target || kriya.evt.srcElement;
						var popper     = $(targetNode).closest('[data-component]');

						var tableHTML = evt.originalEvent.clipboardData.getData('text/html');
						tableHTML     = cleanupOnPaste(tableHTML);
						tableHTML     = '<div>'+tableHTML+'</div>';
						if($(tableHTML).find('table').length == 0){
							evt.stopPropagation();
    						evt.preventDefault();
    						kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table not found in pasted content.' ,icon: 'icon-warning2'});
						}else{

							var tblNode = $(tableHTML).find('table');
							tblNode.find('td,th').each(function(){
								//remove the unwanted space
								var cellHtml = $(this).html();
								cellHtml = cellHtml.replace(/\s\s+/g, ' '); //remove the more than one normal space
								cellHtml = cellHtml.replace(/^\s+$/g, ''); // if cell html is full of normal space then remove every space

								//Wrap the para in table cell
								if($(this).find('p').length > 0){
									$(this).find('p').addClass('jrnlTblBody');
								}else{
									$(this).html('<p class="jrnlTblBody">' + cellHtml + '</p>');
								}
							});

							var tableInnerHtml = tblNode.html();
							popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
							popper.find('table[data-type="floatComponent"]').attr('data-focusout-data', 'paste');
							popper.find('.pasteTableDiv').addClass('hidden');
							popper.find('.resetTableBtn').removeClass('disabled');
						}
					}
					console.log(evt);
				}
			}, '[data-type="popUp"] .pasteTableDiv[contenteditable="true"]');

			$('body').on({
				paste: function (evt){
					//Clean up the pasted content
					var targetNode = $(this);

					//#249 - if middle mouse button is clicked, event.which is undefined - Prabakaran. A (prabakaran.a@focusite.com)
					if(kriya.pasteHandleEvent.which === 2){
						kriya.pasteHandleEvent.preventDefault();
						return false;
					}
					//End of #249

					setTimeout(function(){
						// delete the last child if its <br> to avoid unwanted space in component
						if($(targetNode).children().length>0 && $(targetNode).children().last()[0].nodeName.toLowerCase()=="img"){
							$(targetNode).children().last()[0].remove();
						}
						var targetNodeContent = targetNode.html();
						var classnameParent = '';
						if(targetNode.parentNode!=undefined){
							classnameParent = targetNode.parentNode.className;
						}

                        //#292 - Libre Office - Replaced preceeding and trailing spaces - Mohammed Navaskhan (mohammednavas.a@focusite.com)
						//#154-it is removing the parent node (.tags) in Insert research organism popup-kirankumar
						//clean up the paste content component fields added by vijayakumar on 15-11-2018
						var contenttype = targetNode.attr('data-data-type');
						if(!targetNode.hasClass('kriya-tagsinput')){
							targetNodeContent = cleanupOnPaste(targetNodeContent);
						}
						var cleancontent = "<span>"+targetNodeContent+"</span>";
						if(contenttype == "text"){
							//paste only text
							targetNodeContent = $(cleancontent).text()
						}else if(contenttype == "formatting"){
							//paste with formatting style tags
							$(cleancontent).find('*:not("i,sup,sub,b,strong,em,u,a")').each(function(){
								$(this).contents().unwrap()
							})
							targetNodeContent = $(cleancontent).html();
						}
						targetNodeContent = targetNodeContent.replace(/^\s+|\s+$/gm, '');
                        //End of #292
						targetNodeContent = cleanforInputs(targetNodeContent, classnameParent);
						targetNode.html(targetNodeContent);

						//If refernce html was pasted in popup or in query card
						//unwrap the tag and paste as text
						targetNode.find('.jrnlRefText *').contents().unwrap();
						targetNode.find('.jrnlRefText').removeAllAttr();

					},100);
				}
			}, '[data-type="popUp"] [contenteditable="true"], .query-div.card [contenteditable="true"]');

			//Prevent the right click context menu in editor
			$(kriya.config.containerElm).on("contextmenu",function(e){
		        return false;
		   	 });

			//Select the supplementary type and float dropdown in supplementary popup
			$('body').on({
				click: function (evt) {
					var popper    = $(this).closest('[data-component]');
					var selectedText = $(this).text();
					$(this).parent('.dropdown-content').prev('.dropdown-button').text(selectedText);
					$(this).siblings().removeClass('selected');
					$(this).addClass('selected');

					if(popper.find('[data-class$="Caption"] .label').length == 0){
						popper.find('[data-class$="Caption"]').prepend('<span class="label"></span>');
					}
					var labelNode = popper.find('[data-class$="Caption"] .label');
					var labelId = '';
					if(popper.find('#float-list-dropdown li.selected').length > 0){
						var prefixLabelID = popper.find('#float-list-dropdown li.selected').attr('data-float-id');
						if(prefixLabelID){
							labelNode.attr('data-main-float', prefixLabelID);
						}else{
							labelNode.removeAttr('data-main-float');
						}
					}
					if(popper.find('#supplement-type-dropdown').length > 0){
						labelId = popper.find('#supplement-type-dropdown li.selected').attr('data-id-prefix');
						labelId = (prefixLabelID)?prefixLabelID + '-' + labelId : labelId;
					}else if($(this).attr('data-id-prefix')){
						labelId = $(this).attr('data-id-prefix');
						labelId = (prefixLabelID)?prefixLabelID + '-' + labelId : labelId;
					}

					labelId = labelId.replace(/BLK_/g, '');

					var chapNum = $('#contentDivNode .jrnlChapNumber').text();
					if(chapNum){
						labelId = labelId + chapNum + '_';
					}

					var lastElements = $(kriya.config.containerElm + ' [data-id^="' + labelId + '"]').closest('[data-id^="BLK_"]').last();
					if(lastElements.length > 0){
						var lastElementId = lastElements.attr('data-id');
						if(lastElementId && lastElementId.match(/(\d+)$/g)){
							var lastIdNum = parseInt(lastElementId.match(/(\d+)$/g)[0]);
							labelId = labelId+(lastIdNum+1);
						}
					}else{
						labelId = labelId+1;
					}


					if(labelId){
						var citeConfig = citeJS.floats.getConfig(labelId);
						var newFloatLabelString = citeJS.floats.getCitationHTML([labelId], [], 'renumberFloats');
						var idPrefix = labelId.replace(/(\d+)$/g, '');
						var uncitedCount = $(kriya.config.containerElm + ' .jrnlSupplBlock[data-id^="' + idPrefix + '"]').closest('[data-id^="BLK_"][data-uncited]').length + 1;
						newFloatLabelString =  newFloatLabelString.replace(/(\d+)$/g, uncitedCount);

						var uncitedStr = 'Uncited ';
						if(citeConfig && citeConfig.citationNotNeed == "true"){
							uncitedStr = '';
						}
						newFloatLabelString = uncitedStr + newFloatLabelString;
						labelNode.html(newFloatLabelString);
						labelNode.attr('data-block-id', labelId);
					}
				},
			}, '#float-list-dropdown li, #supplement-type-dropdown li, #box-type-dropdown li');

			$('body').on({
				click: function (evt) {
					$('#specialCharacter .spl-char').removeClass('active');
					$(this).addClass('active')
				},
			}, '#specialCharacter .spl-char');

			$('body').on({
				click: function (evt) {
					var id = $(this).attr('data-dropdown');
					$('.dropdown-content:visible:not(#' + id + ')').fadeOut().css({'opacity': '0'});
					if($('#'+id+':visible').length > 0){
						$('#'+id).fadeOut().css({'opacity': '0'});
					}else{
						$('#'+id).css({'opacity': '1'}).fadeIn();
					}
				}
			}, '.dropdown-button[data-dropdown]');
			$('body').on({
				click: function(e){
					//Highlight validation issue place added by vijayakumar on 9-11-2018
					$('#contentDivNode *[data-validation-error="true"]').removeAttr('data-validation-error', 'true');
					if ($('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]').length > 0){
						$('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]').attr('data-validation-error', 'true');
						$('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]')[0].scrollIntoView();
					}
				},
			}, '#validationDivNode [data-clone-id]');
			$('body').on({
				click: function (evt) {
					if(!$(evt.target).is('.dropdown-button') && $(evt.target).closest('.dropdown-content').length == 0 && $('#trackChangeOptions .dropdown-content:visible')){
						$('.filterOptions').find('.dropdown-content:visible').fadeOut().css({'opacity': '0'});
					}
				}
			});

        },

		isUndefined: function(obj){
			if(typeof(obj)!="undefined"){
				return true;
			}
			return false;
		},
		removeComp: function(obj){
			//kriya[config.activeElements[0]].detach();
		}
	};
	init();
}());
$('.WordSection1').each(function(){
	this.addEventListener('scroll', function(){
		if($('[data-component]:visible').length > 0){
			$('[data-component]:visible').addClass('hidden');
		}
	});
});
