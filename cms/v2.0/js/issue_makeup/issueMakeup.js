var sectionTitle;
var startPage,endPage;
var sortable = [];
var blankCard='<div class="bankedCard row"><div class="row"><span class="col s4 "><span class="cardLabel">Blank Page</span></span></div><div class="blank"><input type="text" placeholder="Blank Page" class="blankAdvert"/></div></div>';
var issueMakeUp = {
	//default config of the issue
	config: {
		'customer': 'bmj',
		'project' : 'bjsports',
		'volume'  : '1',
		'issue'   : '1',
		'fPage' : '1'
	}
};
//function for axpand all collapsible-body
$(document).ready(function() {
	//Initoalize the poste and subscriber
	eventHandler.publishers.add();
	eventHandler.subscribers.add();

	$.ajax({
		type: "GET",
		url: "/api/customers",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (res) {
			if (res){
				var customerObj = {};
				$.each(res.customer, function(i, obj){
					if(obj.name){
						customerObj[obj.name] = ''; 
					}				
				});
				$('[data-class="customer"]').autocomplete({
					data: customerObj,
					limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
					appender: {
				        el: '#compDivContent'
				    }
				});
			}
		},
		error: function(xhr, errorType, exception) {
			 console.log(exception);
		}
	});

	$('body').on( 'click', '#editorial-board .fileChooser', function(){
		$(this).parent().find('input:file').val('');
		$(this).parent().find('input:file').trigger('click');
	});

	$('body').on( 'change', '#editorial-board input:file', function(e){
		var fileObj  = e.target.files[0];
		var fileName = fileObj.name;

		//expand editorial borad
		$(".collapsible-header").addClass("active");
  		$(".collapsible").collapsible({accordion: false});

		if($('#editorial-board').closest('li').find('.sortList').find('.bankedCard').length > 0){
			var cardObj =  $('#editorial-board').closest('li').find('.sortList').find('.bankedCard');
			cardObj.find('.fileName').text(fileName);
		}else{
			var cardId = Math.floor(Math.random()*89999+10000);
			var card =  '<div class="bankedCard row advertCard" id="' + cardId + '">';
			    card += '<div class="truncate row fileName editorialFile">' + fileName + '</div>';
			    card += '</div>';

			var cardObj = $(card);
			$('#editorial-board').closest('li').find('.sortList').append(cardObj);
		}
		
		cardObj.addClass('loading');
		cardObj.append('<div class="loading-status">0%</div>');

		var param = {
			'file': fileObj,
			'block': cardObj,
			success: function(res){
				console.log(res);
				if(res){
					cardObj.find('.fileName').attr('data-path', res);
					cardObj.removeClass('loading');
					cardObj.find('.loading-status').remove();
					var content = cardObj.closest('#sections')[0].outerHTML;
					saveIssue(content);
				}else{
					console.log('Error: response dosn\' have file information');
				}
			},
			error: function(res){
				console.log('Error: uploading failed..');
			}
		};
		eventHandler.menu.upload.uploadFile(param, e.currentTarget);

	});

	$('body').on( 'click', '#bankedAdverts .fileChooser', function(){
		$(this).parent().find('input:file').val('');
		$(this).parent().find('input:file').trigger('click');
	});

	$('body').on( 'change', '#bankedAdverts input:file', function(e){
		var fileObj  = e.target.files[0];
		var fileName = fileObj.name;
		var cardId = Math.floor(Math.random()*89999+10000);
		var card = '<div class="bankedCard row advertCard" id="' + cardId + '" draggable="true" ondragstart="dragStart(event,this)">';
		    card += '<div class="row"><span class="col s4 "><span class="drag-marker"><i></i></span><span class="cardLabel">Advert</span></span>';
		    card += '<span class="col s8 "><span class="articleControls right hide"><i class="fa fa-times fa-lg" aria-hidden="true" style="cursor:pointer" onclick="removeAdvertCard(this)"></i></span></span></div>';
		    card += '<div class="truncate row fileName">' + fileName + '</div>';
		    card += '</div>';

		var cardObj = $(card);
		$(this).closest('#bankedAdverts').append(cardObj);
		cardObj.addClass('loading');
		cardObj.append('<div class="loading-status">0%</div>');

		var param = {
			'file': fileObj,
			'block': cardObj,
			success: function(res){
				console.log(res);
				if(res){
					cardObj.find('.fileName').attr('data-path', res);
					cardObj.removeClass('loading');
					cardObj.find('.articleControls').after($('<span class="articleControls viewPDF right btn amber" data-message="{\'click\':{\'funcToCall\': \'viewPdf\',\'param\': \'' + res + '\',\'channel\':\'menu\',\'topic\':\'edit\'}}" data-href="' + res + '">View Pdf</span>'));
					cardObj.find('.loading-status').remove();
				}else{
					console.log('Error: response dosn\' have file information');
				}
			},
			error: function(res){
				console.log('Error: uploading failed..');
			}
		};
		eventHandler.menu.upload.uploadFile(param, e.currentTarget);
	});

});
function expandAll(){
  $(".collapsible-header").addClass("active");
  $(".collapsible").collapsible({accordion: false});
}

function collapseAll(){
  $(".collapsible-header").removeClass(function(){
    return "active";
  });
  $(".collapsible").collapsible({accordion: true});
  $(".collapsible").collapsible({accordion: false});
}

function removeNode(obj){	
	var content = $(obj).closest('#sections')[0];
	if($('.collapsible').children('li').length>1){
		$(obj).closest('li').remove();
	}		
	saveIssue(content.outerHTML);
	return false;
}

function removeArtiCard(obj){
	var doifil = $(obj).closest('.bankedCard').find('.doi').html();
	$.expr[':'].containsIgnoreCase = function (n, i, m) {
				return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) > 0;
			};
	var articleCard = $('.bankedCard:containsIgnoreCase("'+doifil+'")').first();
	var doi = articleCard.find('.doi').html().split('/');
	var content = '<content id="'+doi[1]+'" data-removed="true"/>';
	saveIssue(content);
	
	$('.bankedCard:containsIgnoreCase("'+doifil+'")').removeClass('hide'); 	
	var ulSortList = $(obj).closest('li').closest('ul');
	var ob = $(obj).closest('li').remove();	
	changeSingPlural(ulSortList);
	updatePageNumber();	
	return false;
}

function removeAdvertCard(obj){
	var cardId = $(obj).closest('.bankedCard').attr('id');
	if(cardId){
		var bCard = addControls(blankCard);
		$(obj).closest('.bankedCard').replaceWith($(bCard));
		$('.bankedCard#'+cardId).removeClass('hide');
		//var content = $(obj).closest('#sections')[0].outerHTML;
		var content = '<content id="'+cardId+'" data-removed="true"/>';
		saveIssue(content);
		//var ob = $(obj).closest('li').remove();
	}
	return false;
}

function editTitle(obj){	
	$(obj).parent('span').parent('span').siblings('.sectionTitle').attr('contenteditable','true');
	$(obj).parent('span').parent('span').siblings('.sectionTitle').focus();
	$(obj).parent('span').siblings('span').addClass('hide');
	sectionTitle = $(obj).parent('span').parent('span').siblings('.sectionTitle').html();
	var tab ='<i class="fa fa-floppy-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="saveSectionTitle(this)"></i><i class="fa fa-times-circle-o" aria-hidden="true" onclick="cancelSectionTitleSave(this)" style="cursor:pointer"></i>';
	$(obj).parent('span').html(tab);	
	return false;
}

function insertNodeAtLast(){
	var newChild = '<li class=""><div class="collapsible-header sectionHead" ondragenter="return dragEnter(event)" ondragover="return dragOver(event)" ondrop="secHeadDrop(event,this)" id=""><span class="sectionTitle" contenteditable="false">New Section</span><span class="right controls"><span class="editSave"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="editTitle(this)"></i></span><span class=""><i class="fa fa-plus fa-lg hide" aria-hidden="true" style="cursor:pointer" onclick="addBlankCard(this)"></i><i class="fa fa-times fa-lg" aria-hidden="true" style="cursor:pointer" onclick="removeNode(this)"></i></span></span></div><div class="collapsible-body sectionBody" id="section1" ondragenter="return dragEnter(event)" ondrop="dragDrop(event,this)" ondragover="return dragOver(event)" style="display: none; padding-top: 12px; margin-top: 0px; padding-bottom: 12px; margin-bottom: 0px;"><ul class="sortList ui-sortable"></ul></div></li>'
	$(newChild).appendTo($('.section1'));
	return false;
}
function saveSectionTitle(obj){	
	$(obj).parent('span').parent('span').siblings('.sectionTitle').attr('contenteditable','false');
	$(obj).parent('span').siblings('span').removeClass('hide');
	var content = $(obj).closest('#sections')[0].outerHTML;
	saveIssue(content);
	
	var tab = '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="editTitle(this)"></i>';
		$(obj).parent('span').html(tab);
}
function saveIssue(content, successCall, errorCall){
	var contentNode = $('<content>' + content + '</content>')
	contentNode.find('input').each(function(){
		$(this).replaceWith('<span class="'+$(this).attr('class')+'"/>');
	});
	content = contentNode.html();

	jQuery.ajax({
		type: "POST",
		url: "/api/save_issue",
		data: {"journal": issueMakeUp.config.project, "volume": issueMakeUp.config.volume, "issue": issueMakeUp.config.issue, "customer": issueMakeUp.config.customer, "update": "true", "content":encodeURIComponent('<issue>'+content+'</issue>')},
		success: function (respData) {
			console.log(respData);
			if(successCall && typeof(successCall) == "function"){
				successCall(respData);
			}
		},
		error: function(xhr, errorType, exception) {
			console.log('unable to edit section title');
			if(errorCall && typeof(errorCall) == "function"){
				errorCall(exception);
			}
			return null;
		}
	});
}
function cancelSectionTitleSave(obj){
	$(obj).parent('span').parent('span').siblings('.sectionTitle').html(sectionTitle);
	$(obj).parent('span').parent('span').siblings('.sectionTitle').attr('contenteditable','false');
	$(obj).parent('span').siblings('span').removeClass('hide');
	var tab = '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="editTitle(this)"></i>';
	$(obj).parent('span').html(tab);
}

function startProcess() {
	$('.collapsibleButtons').removeClass('hide');
// cleanup all temp states
jname = document.getElementById('jrnlName').value;
vol = document.getElementById('volume').value;
iss = document.getElementById('issue').value;
fpage = document.getElementById('fpage').value;
$('.inputs').removeClass('inputError');
if(jname.length<=0)
{
	$('#jrnlName').addClass('inputError');
}else if(vol.length<=0){
	console.log('hari');
	$('#volume').addClass('inputError');
}else if(iss.length<=0){
	$('#issue').addClass('inputError');
}else if(fpage.length<=0){
	$('#fpage').addClass('inputError');
}
else{	
startPage = parseInt(fpage);
	jQuery.ajax({
		type: "GET",
		url: "/api/getIssueData",
		data: {"jrnlName": jname, "volume": vol, "issue": iss, "client": "bmj"},
		success: function (respData) {
			$('#sections').html($($(respData).children('span')[1]).html())
			$('#bankedArticles').disableSelection()
				.html($($(respData).find('#ba')).html())
				.height($(window).height() - $('#bankedArticles').position().top);
			$('.collapsible').collapsible();
			$('.collapsible').disableSelection();
			$('#sectionsContainer, #bankedContainer').height($('#footerContainer').position().top - $('#headerContainer').height());
			$('#bankedArticles').height($('#footerContainer').position().top - $('#bankedArticles').position().top)
			makeSortable();
		},
		error: function(xhr, errorType, exception) {
			console.log('unable to get issue data');
			return null;
		}
	});
}
 
// set inner timer
}

var dragSrcEl=null;
function dragEnter(ev,obj) {		
    ev.preventDefault();
	return true;
}
function dragOver(ev) {
    return false;
}
function dragStart(ev,obj) {	
	dragSrcEl = obj;	
	 ev.dataTransfer.dropEffect = 'move';		 
    ev.dataTransfer.setData("text/html", obj.outerHTML);	
	return true;
}
function makeSortable(){	
	var len = $('.collapsible').find('ul').sortable({
		handle: ".drag-marker",
		stop: function( event, ui ) {
			updatePageNumber();
			var content = this.closest('#sections').outerHTML;
			saveIssue(content);
		}
	});
}
function secHeadDrop(ev, obj){	 
	$('.collapsible-header').removeClass('active');	
	$(obj).addClass('active');
	$('.collapsible').collapsible({accordian:false});
	var hideObj = dragSrcEl;
	$('#bankedContainer').closest('div').find(hideObj).addClass('hide');			
	addToLi(dragSrcEl, $(obj).siblings('.collapsible-body').find('ul'));		
	updatePageNumber();		
	
	var content = $(obj).closest('#sections')[0].outerHTML;
	saveIssue(content);

	ev.stopPropagation();
	return false;
}
function dragDrop(ev,obj) {
	console.log(ev);
	var hideObj = dragSrcEl;
	if($(hideObj).hasClass('advertCard')){
		var currentTarget = $(ev.toElement).closest('.bankedCard');
		var blankCard = (currentTarget.find('.blank').length > 0)?currentTarget:currentTarget.closest('li').next('li').find('.blank').closest('.bankedCard');
		if(blankCard.length > 0){
			$('#bankedContainer').closest('div').find(hideObj).addClass('hide');
			addAdvertInLi(dragSrcEl,blankCard);
		}
	}else{
		$('#bankedContainer').closest('div').find(hideObj).addClass('hide');
		addToLi(dragSrcEl, $(obj).find('ul'));
		updatePageNumber();	
	}
	var content = $(obj).closest('#sections')[0].outerHTML;
	saveIssue(content);
	ev.stopPropagation(); 	
	return false;
	
}
function addAdvertInLi(card, blankCard){
	var newObj = $(addControls(card));
	$(blankCard).replaceWith(newObj);
	$('#sections').find('.hide.articleControls').removeClass('hide');
	$('#sections').height($('#footerContainer').position().top - $('#sections').position().top);
}
function addToLi(inObj, appenTo){
	var newObj = $(addControls(inObj));
	//Don't add check box adverts fm fillers
	if(newObj.find('.fileName').length == 0){
		newObj = addAdvert(newObj);
	}
	var li = $('<li></li>');
	$(newObj).appendTo($(li));
	console.log(li);
	$(li).appendTo($(appenTo));
	changeSingPlural(appenTo);
	$('#sections').find('.hide.articleControls').removeClass('hide');
	$('#sections').height($('#footerContainer').position().top - $('#sections').position().top)
}
function changeSingPlural(ulNode){
	var secTitleNode = $(ulNode).closest('li').find('.sectionTitle');
	var secTitle = secTitleNode.text();
	var singText = secTitleNode.attr('singular');
	var plurText = secTitleNode.attr('plural');
	if ((secTitle == singText) || (secTitle == plurText)){
		if ($(ulNode).find('li').length == 1){
			secTitleNode.text(singText);
		}else{
			secTitleNode.text(plurText);
		}
	}
}
function addAdvert(obj){
	var currID = Math.random().toString(36).slice(2);
	$('<div class="row"><div class="col s4 m4 l4"><input type="checkbox" id="fo-' + currID + '" /><label for="fo-' + currID + '">Is follow-on?</label><input type="checkbox" id="ec-' + currID + '" /><label for="ec-' + currID + '">Is editor\'s choice?</label></div><div class="col s8 m8 l8"><input type=text placeholder="fillers" class="adverts"></input></div>').appendTo($(obj));
	return obj;
}	

function updatePageNumber(){
	var sPage = parseInt(issueMakeUp.config.fpage);
	$('.collapsible-body').find('.blank').parent().parent().remove();
	var bankedCards = $('.collapsible-body').find('.bankedCard:not(:has(.cover,.editorialFile))');
	var len = bankedCards.length;
	for(i=0;i<len;i++){
		var nod = bankedCards[i];
			fpage = $(nod).find('.fpage').html();
			if (fpage == undefined && $(nod).find('.cardLabel').text() == 'Advert'){
				sPage++;
				continue;
			}
			lpage = $(nod).find('.lpage').html(); 		
			lpage = parseInt(lpage)- parseInt(fpage);			
			$(nod).find('.fpage').html(sPage);					
			ePage = sPage + lpage;	
			$(nod).find('.lpage').html(ePage);
			if (parseInt(fpage) != sPage){
				$(nod).find('.generatePdf').removeClass('grey').addClass('red lighten-1').html('Re-generate Pdf');
				eventHandler.menu.general.savePageDetails('', $(nod));
			}
			sPage =	ePage+1;
			if(ePage%2!=0){
				next = bankedCards[i + 1];
				if ($(next).find('.cardLabel').text() == 'Advert'){
					continue;
				}
				//ob=$('<div class="bankedCard row"><input type="text" placeholder="blank/Advert"/></div>');
				ob = addControls(blankCard);
				$(ob).height($(nod).outerHeight());								
				$(ob).find('.bankedCard').width($(nod).outerWidth());
				var li = $('<li></li>');
				$(ob).appendTo($(li));				
				$(li).insertAfter($(nod).closest('li'));	
				$(li).find('.bankedCard .row:first').append($('<span class="col s8 "><span class="articleControls hidden viewPDF right btn amber" data-message="{\'click\':{\'funcToCall\': \'viewPdf\',\'param\': \'http://www.kriya.website/resources/bmj/oem/74_11/blank.pdf\',\'channel\':\'menu\',\'topic\':\'edit\'}}" data-href="http://www.kriya.website/resources/bmj/oem/74_11/blank.pdf">View Pdf</span></span>'))
				len = $('.collapsible-body').find('.bankedCard').length;
				console.log(len);
				sPage++;
				//i++;				
			}			
	}
	//$('.collapsible-body').find('.bankedCard')[len-1].find('.runin').addClass('hide');
}
function addBlankCard(obj){	
	ob = addControls(blankCard);
	var li = $('<li></li>');
	$(ob).appendTo($(li));	
	$(li).insertAfter($(obj).closest('.bankedCard').parent());
}
function saveCoverNotes(obj){	
	var content = $(obj).closest('#sections')[0].outerHTML;
	$(obj).addClass('hide');
	$('<i class="fa fa-spinner fa-spin"></i>').insertAfter(obj);
	saveIssue(content, function(res){
		$(obj).removeClass('hide');
		$(obj).parent().find('.fa-spinner').remove();
	}, function(err){
		$(obj).removeClass('hide');
		$(obj).parent().find('.fa-spinner').remove();
	});
}

function addControls(obj){
	var objId = $(obj).attr('id');
	objId = (objId)?'id="'+objId+'"':'';
	return '<div class="bankedCard row" ' + objId + '>'+$(obj).html()+'</div>';
}
function myFunction() {
	var input, filter, ul, li, a, i;
    filter = $('#search').val();
	
	$('.bankedCard').removeClass('inActive');
    // Declare variables	
	if(filter.length>0){
		$.expr[':'].containsIgnoreCase = function (n, i, m) {
				return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) < 0;
			};
		$('.bankedCard:containsIgnoreCase("'+filter+'")').addClass('inActive');     	
	}	
}

function openPopup(popUp) {
	$(popUp).removeAttr('style').removeClass('hidden').css({
		'top':'30%',
		'left':'40%',
		'width':'0',
		'height':'0',
		'opacity':'1',
		'z-index' : '9999'
	});

	$(popUp).animate({
		top: '0',
		left:'0',
		width:'100%',//($(component).outerWidth()),
		height:'100%',//($(component).outerHeight()),
		opacity:('1')
	},500, function(){
		
	});
}

function closePopUp(popUp){
	$(popUp).animate({
		top: ('40%'),
		left:('40%'),
		width:('0'),
		height:('0'),
		opacity:('0')
	},500, function(){
		$(popUp).addClass('hidden');
		$(popUp).css('z-index', '');
	});	
}

function validateComponent(popUp){
	$(popUp).find('[data-validate]').each(function(){
		var validate = $(this).attr('data-validate');
		if(typeof(window[validate]) == "function"){
			window[validate](this);
		}
	});
	if($(popUp).find('[data-error]').length > 0){
		return false;
	}else{
		return true;
	}
}

function required(component) {
	$(component).removeAttr('data-error');
	if ($(component)[0].nodeName == "INPUT" && ($(component).attr("type") == "radio")){
		var checked = false;
		var radioName = $(component).attr("name");
		$(component).parent().find('input[name="' + radioName + '"]').each(function(){
			if ($(this).prop('checked') == true){
				checked = true;
			}
		})
		if (!checked){
			$(component).parent().attr('data-error', 'Input Required');
		}
	}else if($(component)[0].nodeName == "INPUT"){
		if($(component).val() == ""){
			$(component).attr('data-error', 'Input Required');
		}
		
	}
	else if ($(component).text() == "") {
		$(component).attr('data-error', 'Input Required');
	}
}


    // 4. GLOBAL NAMESPACE
    window.kriya = {
        notification: function(param){
        	var id = uuid.v4();
        	var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
			notice.append('<div class="row kriya-notice-header" />');
			notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');

			if(param.icon){
				notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
			}
			if(param.title){
				notice.find('.kriya-notice-header').append(param.title);
			}
			
			if(param.closeIcon != false){
				notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="kriya.removeNotify(this)"></i>');
			}
			
			
			Materialize.toast(notice[0].outerHTML, param.timeout);
			return id;
        },
        removeNotify: function(target){
        	if(target){
        		$(target).closest('.toast').fadeOut(function(){
			        $(this).remove();
			    });
        	}
        },
        removeAllNotify: function(){
        	$('.toast').fadeOut(function(){
		        $(this).remove();
		    });
        },
        sendAPIRequest: function (funtionName, parameters, onSuccessFunc, onError, onProgress) {
			$.ajax({
				url : '/api/'+funtionName,
				type: 'POST',
				data: parameters,
				xhrFields: {
                    onprogress: function(e) {
                        if(onProgress){
                        	onProgress(e);
                        }
                    }
                },
				error: function(res){
					if(onError){
						onError(res);
					}
				},
				success: function(res){
					if(onSuccessFunc){
						onSuccessFunc(res);
					}
				},
			});
		}
	}