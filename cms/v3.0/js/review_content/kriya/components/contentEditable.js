/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    // 1. CONFIGURATION
    var config = {
        //container: '[data-component="InputBox"]',
		//container: '.jrnlHead1',
		container: '.InputBox',
		editable: true
    };

    // 2. COMPONENT OBJECT
    kriya.contentEditable = {

        init: function (elm, component, evt) {
			this.container = elm;
			this.component = component;
            this.addEditor(component);
        },
		detach: function () {
			
		},
        addEditor: function (component) {
			if ($('#contentContainer').attr('data-state') == 'read-only') return false;
			this.container[0].setAttribute('contenteditable', 'true');
			this.container[0].focus();
			this.container[0].addEventListener('focusout', function(){
				//this.removeAttribute('contenteditable');
				//$('.activeElement').removeClass('activeElement');
			});
		},
    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));