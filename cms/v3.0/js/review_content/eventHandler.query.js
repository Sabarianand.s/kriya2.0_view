(function (eventHandler) {
	eventHandler.query = {
		action: {
			addQuery: function(param, targetNode){			// function for show query at right panel 
				
				if($(targetNode).closest('[data-query-type="predefined"]').length > 0){
					var querySelected = $(targetNode);
				}else if ($('[data-component="QUERY"] [data-query-type]:visible').length == 1){
					var querySelected = $('[data-component="QUERY"] [data-query-type]:visible');
				}else{
					var querySelected = $('[data-component="QUERY"] [data-query-type].selected');
				}
				$(querySelected).each(function(){
					var qid = "";
					try {
						if ($(this).text() == "") return false;
						var queryType = "action";
						var actionType = $(targetNode).attr('data-query-action');

						if ($('#queryPerson').is(':visible')){
							var queryPerson = $('#queryPerson').val();
						}else{
							var queryPerson = 'Publisher';
						}

						//If target node has query person action return queryPerson as action - jagan
						if($(targetNode).attr('data-query-to')){
							var queryPerson = $(targetNode).attr('data-query-to');
						}
						if(param && param == 'comments'){
							var queryPerson = $(targetNode).closest('[data-comment-to]').attr('data-comment-to');
						}
						qid = eventHandler.query.action.createQueryDiv(queryPerson, queryType, actionType);
						var queryContent = $('#' + qid).find('.query-content');
						$(queryContent).html($(this).html());
						$(queryContent).find('.actionIcon').remove();
						
						//if queryType is action and float id is in content then add the attribute data-float-id and remove floatId content in queryContent
						if(queryType == "action" && $(queryContent).find('.floatID').text()){
							$('#' + qid).attr('data-float-id', $(queryContent).find('.floatID').text());
						}
						$(queryContent).find('.floatID').remove();
						
						//Add word break character to links - jagan
						var textChilds = textNodesUnder($(queryContent)[0]);
						$(textChilds).each(function(){
							var thisText = 	this.nodeValue;
							if(thisText.match(/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/g)){
								var matches = thisText.match(/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/g);
								for(var m =0;m < matches.length;m++ ){
									var newText = addLogicalBreaksToLinks(matches[m]);
									thisText = thisText.replace(matches[m], newText);
								}
								this.nodeValue = thisText;
							}
						});

						//add files to query
						if ($('[data-component="QUERY"] div[data-type="file"]').length > 0){
							$('.download-modal').remove();
							$(queryContent).append($('[data-component="QUERY"] div[data-type="file"]'));
						}
						$(queryContent).find('[contenteditable]').contents().unwrap();
						
						$('.queryContent').html('');
						$('[data-component="addQuery_edit"]').addClass('hidden');
						if(param && param == 'comments'){
							$('[data-comment-to] [data-type="text"]').text('');
							$('#queryDivNode .commentsField').addClass('hidden');
							$('#queryCloneContent .commentsField').addClass('hidden');
							kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);
							kriyaEditor.init.addUndoLevel('peer review comments');
							return false;
						}
						
						var selection = rangy.getSelection();
						selection.removeAllRanges();
						var currSelection = kriya.selection.cloneRange();
						selection.addRange(currSelection);
						if (selection.rangeCount > 0) {
							var range = selection.getRangeAt(0);						
							if (range.text() == ""){
								if(kriya.selectedNodes.length > 0 && kriya.selectedNodes[0].nodeType == 3){
									kriya.selectedNodes = [kriya.selectedNodes[0].parentElement];
								}
								if (kriya.selectedNodes.length == 0){
									kriya.selectedNodes = kriya.config.activeElements;
								}
								if (kriya.selectedNodes.length == 0){
									kriya.selectedNodes = [range.startContainer.parentElement];
								}
								var actionNode = "";
								if (kriya.selectedNodes.length > 0){
									// Create ID for an empty footnote to add a query by Tamil selvan on 22-May-19
									if($(kriya.selectedNodes[0]).hasClass("jrnlFN") && !kriya.selectedNodes[0].hasAttribute("id")){
										$(kriya.selectedNodes[0]).attr('id',uuid.v4());
										$(kriya.selectedNodes[0]).attr("data-inserted", "true");
										var selectedNode = $(kriya.selectedNodes[0]);
										if(selectedNode && $('*[data-component="jrnlFN_edit"]').length > 0 && $('*[data-component="jrnlFN_edit"]')[0].hasAttribute('data-callback')){
											var execFn = getStringObj($('*[data-component="jrnlFN_edit"]').attr('data-callback'));
											execFn(selectedNode);
										}
										kriya.popUp.closePopUps($('*[data-component="jrnlFN_edit"]'));
									}
									if (kriya.selectedNodes[0].hasAttribute('id')){
										actionNode = $(kriya.selectedNodes[0]);
									}else{
										actionNode = $(kriya.selectedNodes[0]).closest('*[id]');
									}
								}
								if (actionNode){
									var startNode = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
									$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', queryPerson.toLowerCase()).attr('data-query-from', kriya.config.content.role);
									$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
									$('#' + qid).attr('data-type', 'action');
									$(startNode).attr('data-query-type', 'action');
									$(actionNode).prepend(startNode);
									kriyaEditor.settings.undoStack.push(actionNode[0]);
								}
							}else{
								if(range.commonAncestorContainer.nodeType==3){
									var selectionHTML = range.getHTMLContents();
									//Don't expand word if selection is in table and selection matches special characters. Because range was expanding the selection wrongly if selection has special characters - jagan
									if(!selectionHTML || !($(range.commonAncestorContainer).closest('table').length > 0 && selectionHTML.match(/[\u00C0-\u017F\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF\u03b1-\u03c9]/gi))){
										range.expand('word');
									}
								}
								//code block for add end querynode
								if (selection.rangeCount > 0) {
									var queryLinker = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
									var endNode = queryLinker.clone()[0];
									endNode.setAttribute("data-type","end");
									var startNode = queryLinker.clone()[0];
									$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', queryPerson.toLowerCase()).attr('data-query-from', kriya.config.content.role);
									$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
									range = selection.getRangeAt(0);
									
									var startRangeNode = range.startContainer, startOffset = range.startOffset;
									var endRangeNode   = range.endContainer, endOffset = range.endOffset;
									
									//Issue- Author query is comes inside footnote label
									if($(startRangeNode).closest('span.jrnlFNLabel, ' + kriya.config.citationSelector).length > 0 ){
										tempStartNode = $(startRangeNode).closest('span.jrnlFNLabel, ' + kriya.config.citationSelector)[0].previousSibling;
										while(tempStartNode && ($(tempStartNode).hasClass("del")||$(tempStartNode).hasClass("jrnlFNLabel"))){
											tempStartNode = tempStartNode.previousSibling;
										}
										startRangeNode = tempStartNode;
										range.setStartAfter(startRangeNode);
									}
		
									if($(endRangeNode).closest('span.jrnlFNLabel, ' + kriya.config.citationSelector).length > 0){
										var tempStartNode = $(endRangeNode).closest('span.jrnlFNLabel, ' + kriya.config.citationSelector)[0].nextSibling;
										while(tempStartNode && ($(tempStartNode).hasClass("del")||$(tempStartNode).hasClass("jrnlFNLabel") || $(tempStartNode).closest(kriya.config.citationSelector).length > 0)){
											tempStartNode = tempStartNode.nextSibling;
										}
										endRangeNode = tempStartNode;
										range.setEndBefore(endRangeNode);
									}
									
									var startNodeLength = (startRangeNode.textContent)?startRangeNode.textContent.length:0;
									startOffset = (startNodeLength >= startOffset)?startOffset:0;
									var boundaryRange = range.cloneRange();
									boundaryRange.collapse(false);
									boundaryRange.insertNode(endNode);
									if($(startRangeNode).closest('span.del, *[data-track="del"]').length > 0 && endRangeNode && endOffset > 0){
										boundaryRange.setStart(endRangeNode, 0);
									}else{
										boundaryRange.setStart(startRangeNode, startOffset);
									}
									boundaryRange.collapse(true);
									boundaryRange.insertNode(startNode);
									kriyaEditor.settings.undoStack.push(startNode.parentNode);
									range.setStartAfter(startNode);
									range.setEndBefore(endNode);
									range.collapse(true);
								}
							}
						}		
						if (! $('.notes-icon').hasClass('active')){
							eventHandler.query.action.toggleNotes();
						}
						//move the query based on the ref location
						var nextRef = null;
						$(kriya.config.containerElm + ' .jrnlQueryRef[data-type="start"]').each(function(i){
							if($(this).attr('data-rid') == qid){
								//Get the next ref
								nextRef = $(kriya.config.containerElm + ' .jrnlQueryRef[data-type="start"]:eq(' + (i+1) + ')');
								return false;
							}
						});						
					}catch(e) {
						var parameters = {
							'log': "Add Query Error: " + e.stack
						};
						kriya.general.sendAPIRequest('logerrors',parameters,function(res){
							console.log('Data Saved');
						});
					}

					if(qid){
						$('.query-div#' + qid).attr('data-inserted', 'true');
						eventHandler.query.action.addBadge();
						var poper = $(targetNode).closest("[data-component]");
						kriya.popUp.closePopUps(poper);
						poper.removeAttr('data-display').removeAttr('data-sub-type').removeAttr('style');
						
						$('.query-div#' + qid).find('[data-tooltip]').tooltip({delay: 50});
						$('.query-div#' + qid).trigger('click');

						//Initialize filter option when query adding
						kriya.general.iniFilterOptions($('#queryDivNode .filterOptions .dropdown-content'), $('.query-div#' + qid));
						kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);
					}

					kriyaEditor.init.addUndoLevel('add-query');
				});
			},
			createQueryDiv: function(queryPerson, queryType, actionType){
				var qid = uuid.v4(); 	// generating Query ID 						
				var objDate = new Date();
				var locale = "en-us";
				// Modify 12hours date format -rajesh
				var time = objDate.toLocaleString(locale, { hour: 'numeric',minute:'numeric', hour12: true });
				var month = objDate.toLocaleString(locale, { month: "short" });
				var objDateForm = month + ' ' + objDate.getDate() + ' ' + (objDate.getYear() + 1900) + ' ' + time; 
				var actionTypeAttr = (actionType)?' data-query-action="' + actionType + '" ':'';
				var divNode = $('<div class="query-div card ' + queryPerson.toLowerCase() + '" id="' + qid + '" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'highlightQuery\', \'param\':{\'scroll\':\'true\'}}" data-type="' + queryType + '" ' + actionTypeAttr + '>');
				var userName = kriyaEditor.user.name;
				var queryHolder = $('<div class="query-holder"/>');
				var queryHead = $('<div class="query-head"><span class="query-from">'+ userName +'</span><span class="query-time">' + objDateForm + '</span></div>');
				$(queryHolder).append(queryHead);
				//Add attribute for checking query content is empty or not using function call - rajesh
				var queryContent = $('<div class="query-content" data-time="'+ time + '"/>');
				$(queryContent).attr("data-assigned-by", kriyaEditor.user.name).attr("data-assigned-by-role", kriya.config.content.role).attr("data-assigned-to-role",queryPerson).attr("data-date", objDate.toLocaleDateString());
				$(queryHolder).append(queryContent);
				
				var queryActions = this.getQueryActions(actionType);
				$(queryHolder).append(queryActions);
				
				$(divNode).append(queryHolder);
				$('#queryDivNode').append(divNode);
				return qid;
			},
			getQueryActions: function(actionType){
				var queryActions = '';
				if(actionType == "uncited-citation"){
					queryActions = '<div class="query-action"><span class="query-action-btn" data-channel="menu" data-topic="insert" data-event="click" data-message="{\'funcToCall\': \'newCite\', \'param\': {\'method\': \'ADD\', \'type\': \'CITATION_edit\'}}">Link existing</span><span class="query-action-btn q-e" data-channel="menu" data-topic="insert" data-event="click" data-message="{\'funcToCall\': \'newObject\', \'param\': {\'type\': \'jrnlRefText\'}}">Add new</span><span class="query-action-btn q-d" data-channel="components" data-topic="citation" data-event="click" data-message="{\'funcToCall\': \'unmarkCitation\'}">Not a link</span>';
					if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher" || kriya.config.content.role == "copyeditor"){
						queryActions += '<br /><span class="query-reply" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'createQueryReply\'}"><i class="material-icons">reply</i> Reply</span>';
						if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher"){
							queryActions += '<span class="query-unresolve" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'unableToResolve\'}">Unable to resolve</span>';
						}
					}
					queryActions += '</div>';
				}else if(actionType == "uncited-reference"){
					queryActions = '<div class="query-action"><span class="query-action-btn" data-channel="components" data-topic="citation" data-event="click" data-message="{\'funcToCall\': \'citeNow\'}"><i class="icon-link"/>  Cite Now</span><span class="query-action-btn q-d" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'}}"><i class="icon-link"/> Delete reference</span>';
					if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher" || kriya.config.content.role == "copyeditor"){
						queryActions += '<br /><span class="query-reply" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'createQueryReply\'}"><i class="material-icons">reply</i> Reply</span>';
						if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher"){
							queryActions += '<span class="query-unresolve" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'unableToResolve\'}">Unable to resolve</span>';
						}
					}
					queryActions += '</div>';
				}else if(actionType == "uncited-table"){
					queryActions = '<div class="query-action"><span class="query-action-btn" data-channel="components" data-topic="citation" data-event="click" data-message="{\'funcToCall\': \'citeNow\'}"><i class="icon-link"/>  Cite Now</span><span class="query-action-btn q-d" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'}}"><i class="icon-link"/> Delete table</span>';
					if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher" || kriya.config.content.role == "copyeditor"){
						queryActions += '<br /><span class="query-reply" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'createQueryReply\'}"><i class="material-icons">reply</i> Reply</span>';
						if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher"){
							queryActions += '<span class="query-unresolve" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'unableToResolve\'}">Unable to resolve</span>';
						}
					}
					queryActions += '</div>';
				}else if(actionType == "uncited-figure"){
					queryActions = '<div class="query-action"><span class="query-action-btn" data-channel="components" data-topic="citation" data-event="click" data-message="{\'funcToCall\': \'citeNow\'}"><i class="icon-link"/>  Cite Now</span><span class="query-action-btn q-d" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'}}"><i class="icon-link"/> Delete figure</span>';
					if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher" || kriya.config.content.role == "copyeditor"){
						queryActions += '<br /><span class="query-reply" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'createQueryReply\'}"><i class="material-icons">reply</i> Reply</span>';
						if(kriya.config.content.role == "author" || kriya.config.content.role == "publisher"){
							queryActions += '<span class="query-unresolve" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'unableToResolve\'}">Unable to resolve</span>';
						}
					}
					queryActions += '</div>';
				}else{
					queryActions = '<div class="query-action"><span class="query-delete" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'deleteQuery\'}">Delete</span><span class="query-edit" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'editQuery\'}">Edit</span></div>';
				}
				return queryActions;
			},
			editQuery: function(param, targetNode){
				var target = $(targetNode).closest('.query-div');
				// Get reply content text and store in attribute for cancel - rajesh
				var replyContent = $(targetNode).closest('.query-holder').find('.query-content').html();
				$(targetNode).parent().append('<span class="btn btn-medium green lighten-2 save-edit green" style="font-weight: 600;padding: 6px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'saveEditQuery\'}">Save</span>');
				$(target).find('.query-content').attr('contenteditable', 'true').focus();
				//Add attribute for checking query content is empty or not using function call - rajesh
				$(target).find('.query-content').attr('data-channel', 'query').attr('data-topic', 'action').attr('data-event', 'keyup').attr('data-message', '{\'funcToCall\': \'activateQueryBtn\'}').attr('data-val', replyContent);
				if ($(target).find('.query-content div[data-type="file"]').length > 0){
					$(target).find('.query-content div[data-type="file"]').attr('data-editable', 'true');
					$(target).find('.query-content').after($(target).find('.query-content div[data-type="file"]'));
				}else{
					$(targetNode).parent().append('<input type="file" style="display:none;" data-channel="query" data-topic="action" data-event="change" data-message="{\'funcToCall\': \'uploadFile\'}"/><span class="btn btn-medium blue lighten-2 upload-file pull-right" data-channel="query" data-topic="action" style="font-weight: 600;color: #fff;padding: 6px;" data-event="click" data-message="{\'funcToCall\': \'selectFile\'}">Upload file</span>');
				}
				var assignee = target.find('[data-assigned-to-role]').attr('data-assigned-to-role');
				var selection = $('[data-component="QUERY"]').find('select#queryPerson').clone();
				selection.removeAttr('id');
				selection.find('option[value="' + assignee + '"]').attr('selected','selected');
				$(targetNode).parent().append('<span class="change-assignee">Query to: ' + selection[0].outerHTML + '</span>');
				//Hide roles in query edit for author view
				if(kriya.config.content.role=='author'){
					target.find('.change-assignee').hide();
				}
				$('.change-assignee option[value*="' + kriya.config.content.role + '" i]').remove();
				$(targetNode).parent().find('.query-edit,.query-delete').addClass('hidden');
				var replyid = uuid.v4();
				$(target).find('.query-content').attr('id', replyid);
				//If enabled save button add attribute for edit contents on focus out otherwise put old content enable the buttons - rajesh
				$('body').on('focusout', '#queryCloneContainer #'+replyid, function(){
					if (! $(this).closest('.query-holder').find('.save-edit').hasClass('disabled')){
						$(this).closest('.query-holder').attr('data-save','true');
					}else{
						$(this).text(replyContent);
						$(this).closest('.query-holder').find('.save-edit').removeClass('disabled');
						$(this).closest('.query-holder').find('.upload-file').removeClass('disabled');
						$(this).closest('.query-holder').attr('data-save','true');	
					}
				});
			},
			saveEditQuery: function(param, targetNode){
				var target = $(targetNode).closest('.query-div');
				var qid = $(target).attr('id');
				qid = (qid)?qid:$(target).attr('qid');
				
				if ($(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]').length > 0){
					$('.download-modal').remove();
					$(targetNode).closest('.query-holder').find('.query-content[contenteditable]').append($(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]'));
					$(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]').removeAttr('data-editable');
				}
				var assignee = target.find('[data-assigned-to-role]').attr('data-assigned-to-role');
				if (target.find('.change-assignee select').val() != assignee){
					var newAssignee = target.find('.change-assignee select').val();
					target.find('[data-assigned-to-role]').attr('data-assigned-to-role', newAssignee);
					target.removeClass(assignee.toLowerCase()).addClass(newAssignee.toLowerCase());
					$('span[data-rid="' + qid + '"]').attr('data-query-for', newAssignee.toLowerCase());
				}
				$(targetNode).parent().find('.query-edit,.query-delete').removeClass('hidden');
				$(targetNode).parent().find('.upload-file,input[type="file"],.change-assignee').remove();
				$('.query-content[contenteditable]').removeAttr('data-message');
				$('.query-content[contenteditable]').removeAttr('data-channel');
				$('.query-content[contenteditable]').removeAttr('data-topic');
				$('.query-content[contenteditable]').removeAttr('data-event');
				$('.query-content[contenteditable]').removeAttr('contenteditable');
				$(targetNode).remove();
				eventHandler.query.action.updateRightPanel();
				//Initialize filter option when query adding
				kriya.general.iniFilterOptions($('#queryDivNode .filterOptions .dropdown-content'), $(target));
				kriyaEditor.settings.undoStack.push($('span[data-rid="' + qid + '"]')[0]);
				kriyaEditor.settings.undoStack.push($('#' + qid)[0]);
				kriyaEditor.init.addUndoLevel('save-edit-query');
			},
			editReply: function(param, targetNode){
				var target = $(targetNode).closest('.query-div');
				var qid = $(target).attr('id');
				$(targetNode).closest('.query-holder').find('.reply-content').attr('contenteditable', 'true').focus();
				if($(target).attr('data-replied') == undefined){
					var responseNode = '<span class="queryReplyType response-content"><input type="checkbox" class="filled-in" id="content-changed" data-value="contentChanged" name="query-res">The necessary changes have been made in the editor</span>';
					$(responseNode).insertAfter($(targetNode).closest('.query-holder').find('.reply-content'));
				}

				// Get reply content text and store in attribute for cancel - rajesh
				var replyContent = $(targetNode).closest('.query-holder').find('.reply-content').html();
				$(target).find('.reply-content').attr('data-channel', 'query').attr('data-topic', 'action').attr('data-event', 'keyup').attr('data-message', '{\'funcToCall\': \'activateQueryBtn\'}').attr('data-val',replyContent);
				if ($(targetNode).closest('.query-holder').find('div[data-type="file"]').length > 0){
				// Add css and class for save button styles -rajesh
					$(targetNode).parent().append('<span class="btn btn-medium green lighten-2 save-edit" style="font-weight: 600;padding: 6px 12px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'saveEditReply\'}">Save</span>');
					$(targetNode).closest('.query-holder').find('div[data-type="file"]').attr('data-editable', 'true');
					$(targetNode).closest('.query-holder').find('.reply-content').after($(targetNode).closest('.query-holder').find('div[data-type="file"]'));
				}else{
					$(targetNode).parent().append('<span class="btn btn-medium green lighten-2 save-edit" style="font-weight: 600;padding: 8px 12px;float:left!important" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'saveEditReply\'}">Save</span><span class="btn btn-medium grey lighten-4 cancel-edit" style="margin-left: 3px;color: #505050;font-weight: 600;padding: 8px 12px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'cancelReply\'}">Cancel</span><input type="file" style="display:none;" data-channel="query" data-topic="action" data-event="change" data-message="{\'funcToCall\': \'uploadFile\'}"/><span class="btn btn-medium blue lighten-1 upload-file pull-right" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'selectFile\'}" style="font-weight: 600;color: #fff;padding: 8px 12px;margin-right:5px">Upload file</span>');
				}
				//In edit reply, Change the margin, colors and hide the author name -rajesh
				$(targetNode).closest('.query-holder').css({'background': '#f5f5f5','margin-left': '0px','margin-bottom':'-3px'});
				$(targetNode).closest('.query-holder').find('.reply-content').css({'margin-left': '6px'});
				$(targetNode).closest('.query-holder').find('.query-action').css({'margin-left': '6px'});
				$(targetNode).closest('.query-holder').find('.query-head').hide();
				$(targetNode).parent().find('.query-edit,.query-delete').addClass('hidden');
				var replyid = uuid.v4();
				$(target).find('.reply-content').attr('id', replyid);
				//If enabled save button add attribute for opened reply contents on focus out otherwise trigger cancel button - rajesh
				$('body').on('focusout', '#'+replyid, function(){
					if (! $(this).closest('.query-holder').find('.save-edit').hasClass('disabled')){
						$(this).closest('.query-holder').attr('data-save','true');
					}else{
						$(this).text(replyContent);
						$(this).closest('.query-holder').find('.save-edit').removeClass('disabled');
						$(this).closest('.query-holder').find('.upload-file').removeClass('disabled');
						$(this).closest('.query-holder').attr('data-save','true');				
					}
				});
			},
			saveEditReply: function(param, targetNode){
				var target = $(targetNode).closest('.query-div');
				var targetID = target.attr('id');
				targetID = (targetID)?targetID:target.attr('qid');

				if ($(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]').length > 0){
					$('.download-modal').remove();
					$(targetNode).closest('.query-holder').find('[contenteditable]').append($(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]'));
					$(targetNode).closest('.query-holder').find('div[data-type="file"][data-editable]').removeAttr('data-editable');
				}
				//After save change the styles for edit reply -rajesh
				$(targetNode).closest('.query-holder').removeAttr('style');
				$(targetNode).closest('.query-holder').css({'margin-left': '20px !important','background': '#fff'});
				$(targetNode).closest('.query-holder').find('.reply-content').css({'margin-left': '0px'});
				$(targetNode).closest('.query-holder').find('.query-action').css({'margin-left': '0px'});
				$(targetNode).closest('.query-holder').find('.query-head').show();
				if($(targetNode).closest('.query-div').find('#content-changed:checked').length > 0){
					$(targetNode).closest('.query-div').attr('data-replied', 'true');
					$('[data-rid="'+ targetID +'"]').attr('data-replied', 'true');
					$(target).find('.reply-content').closest('.query-holder').removeClass('data-answered').addClass('data-reply');
					kriyaEditor.settings.undoStack.push($('[data-rid="'+ targetID +'"]')[0]);
				}
				$(targetNode).closest('.query-div').find('.queryReplyType').remove();
				$(targetNode).parent().find('.query-edit,.query-delete').removeClass('hidden');
				$(targetNode).parent().find('.upload-file,input[type="file"],.cancel-edit').remove();

				$('.reply-content[contenteditable]').removeAttr('data-message');
				$('.reply-content[contenteditable]').removeAttr('data-channel');
				$('.reply-content[contenteditable]').removeAttr('data-topic');
				$('.reply-content[contenteditable]').removeAttr('data-event');

				$('.reply-content[contenteditable]').removeAttr('contenteditable');
				$(targetNode).remove();
				eventHandler.query.action.updateRightPanel();
				kriyaEditor.settings.undoStack.push($('.query-div#' + targetID)[0]);
				kriyaEditor.init.addUndoLevel('save-edit-reply');
			},
			highlightQuery: function(param, targetNode){
				$('.class-highlight').addClass('hidden');
				//query highlight no need in structure content stage updated by vijayakumar on 25-07-19
				if (window.location.pathname && window.location.pathname.match(/structure_content/g))return;
				if ($(kriya.evt.target).attr('class') == "queryResponse") return;
				if ($(kriya.evt.target).attr('id') == "content-changed") return;
				if ($(kriya.evt.target).hasClass('query-content') && $(kriya.evt.target).attr('contenteditable') == "true") return;
				if ($(kriya.evt.target).closest('.query-content').length > 0 && $(kriya.evt.target).closest('.query-content').attr('contenteditable') == "true") return;
				if ($(targetNode)[0].hasAttribute('data-rid') && !$(kriya.evt.target)[0].hasAttribute('data-rid')) return;
				if ((!(location.pathname.match(/(proof_review)/)) || !($('.pdfview-icon').hasClass('active'))) && (! $('.notes-icon').hasClass('active'))){
					$('.notes-icon').trigger('click');
				}
				//when we triple click on AuthorQuery sel comming as undefined at that time code is breaking
				//this if condition will return if user click more than one click.
				if(kriya.evt.detail > 1){
					return;
				}
				var range = rangy.getSelection();
				var sel = range.getRangeAt(0);
				if ($(sel.commonAncestorContainer).closest('.query-div').length > 0 && sel.text() != ""){
					return false;
				}
				// to get the hovering query to the right panel, if avaialble
				$('.query-div.disabled').after($('#queryCloneContainer .query-div.hover-inline').clone());
				$('.query-div.disabled').remove();

				var queryID = $('#queryDivNode .query-div.hover-inline').attr('qid');
				$('#queryDivNode .query-div.hover-inline').attr('id', queryID);
				$('#queryDivNode .query-div.hover-inline').removeAttr('qid');

				$('.query-div.hover-inline').removeAttr('style').removeClass('hover-inline').find('.cancelReply').trigger('click');
				$('.query-div').removeClass('top').removeClass('bottom');
				$('.query-div').find('.material-icons.arrow-denoter,.btn-floating.btn-small.com-close').remove();
				$('#queryCloneContainer').html('');
				
				$('.highlight').remove();
				$('.query-div.selected').removeClass('selected');
				
				if ($(targetNode)[0].hasAttribute('data-rid')){
					var target = $(targetNode).attr('data-rid');
				}else if ($(targetNode)[0].hasAttribute('id')){
					var target = $(targetNode).attr('id');
					$(targetNode).addClass('selected');
				}else{
					return false;
				}
				var queryLink = $("span[data-rid='"+target+"']");
				$('style.queryCss').remove();
				if ($('#contentDivNode .jrnlQueryRef[data-rid="' + target + '"]').length > 0){
					var startNode = $('#contentDivNode .jrnlQueryRef[data-type="start"][data-rid="' + target + '"]:first')[0];
					var endNode = $('#contentDivNode .jrnlQueryRef[data-type="end"][data-rid="' + target + '"]:first');
					if (endNode.length > 0){
						endNode = endNode[0];
						sel.setStartAfter(startNode);
						sel.setEndBefore(endNode);
					}else{
						startNode = $(startNode).parent()[0];
						sel.setStart(startNode, 0);
						sel.setEndAfter(startNode);
						/* if (startNode.hasAttribute('id')){
						 	$('body').append($('<style class="queryCss">#' + $(startNode).attr('id') + '{background:transparent !important;}</style>'));
						 }*/
					}
					//#165 - If the closest element is float block then take the block position to highlight query - priya
					if ($(startNode).position().top < 0 || 
					($(startNode).closest('[data-id^="BLK_"]').length>0 && $(startNode).closest('[data-id^="BLK_"]').position().top<0) ||  ($(startNode).position().top + $(startNode).height()) > $('#contentDivNode').height() || ( $(startNode).closest('[data-id^="BLK_"]').length>0 && ($(startNode).closest('[data-id^="BLK_"]').position().top + $(startNode).closest('[data-id^="BLK_"]').height()) > $('#contentDivNode').height())){
						if (! $(targetNode)[0].hasAttribute('data-rid')){
							$(startNode)[0].scrollIntoView();
						}
					}
				}
				if (range){
					if (typeof(window['findReplace']) == 'object'){
						findReplace.general.highlight(range, 'queryText');
					}
					sel.collapse();
					if(param && param.scroll == "true" && startNode){
						//#267, #165 - If the closest element is float block then take the block position to highlight query - priya
						if ($(startNode).position().top < 0 || 
						($(startNode).closest('[data-id^="BLK_"]').length>0 && $(startNode).closest('[data-id^="BLK_"]').position().top<0) ||  ($(startNode).position().top + $(startNode).height()) > $('#contentDivNode').height() || ( $(startNode).closest('[data-id^="BLK_"]').length>0 && ($(startNode).closest('[data-id^="BLK_"]').position().top + $(startNode).closest('[data-id^="BLK_"]').height()) > $('#contentDivNode').height())){
							startNode.scrollIntoView();
						}
					}
				}
				if ($(targetNode)[0].hasAttribute('data-float-id')){
					var floarId = $(targetNode).attr('data-float-id');
					//floarId need to select with data-id not id
					//var floatNode = $(kriya.config.containerElm + ' #' + floarId);
					var floatNode = $(kriya.config.containerElm + ' [data-id="' + floarId+'"]');
					if($(floatNode).length > 0){
						$(floatNode)[0].scrollIntoView();
					}
				}
				if ($(targetNode)[0].hasAttribute('data-rid')){
					//Trigger click edit or reply query button
					if($('#queryDivNode .query-div#'+ target).find('[data-save="true"] .add-reply').length > 0){
						$('#queryDivNode .query-div#'+ target).find('[data-save="true"] .add-reply').trigger('click');
					}else if($('#queryDivNode .query-div#'+ target).find('[data-save="true"] .save-edit').length > 0){
						$('#queryDivNode .query-div#'+ target).find('[data-save="true"] .save-edit').trigger('click');
					}
					

					$('#queryCloneContainer').append($('#queryDivNode .query-div#'+ target).clone());
					$('#queryDivNode .query-div#'+ target).addClass('disabled');
					var queryPopup = $('#queryCloneContainer').find('.query-div#'+ target);
					queryPopup.addClass('hover-inline').attr('qid', target).removeAttr('id').removeClass('hidden');
					// #346 - Dhatshayani . D Jan 09, 2018 - To block reply field for query which has following attributes.
					//some times $('.query-div#'+ target)[0] is undefined at that time code is breaking
					if(queryPopup.length > 0 && (!queryPopup[0].hasAttribute('data-remove-edit') && !queryPopup[0].hasAttribute('data-replied'))){
						queryPopup.find('.query-reply:visible').trigger('click');
					}
					var leftOffset = targetNode.offset().left + 'px';
					var denoterLeft = "0";
					if ($('#contentDivNode').width() < (targetNode.offset().left + $('.hover-inline').width())){
						leftOffset = $('#contentDivNode').width() - $('.hover-inline').width() + 'px';
						denoterLeft = $('.query-div[qid="' + target + '"]').width() - ($('#contentDivNode').width() - targetNode.offset().left + (targetNode.width()));
					}
					$('.query-div[qid="' + target + '"]').append('<a class="btn-floating btn-small com-close" style="position:absolute;top: -1.7rem;right: -1.5rem;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'cancelReply\'}"><i class="close material-icons">close</i></a>');
					var topOffset = targetNode.offset().top + 30;
					if (($('#contentDivNode').offset().top + $('#contentDivNode').height()) < ($('.hover-inline').height() + topOffset) && ($(targetNode).offset().top - $('.hover-inline').outerHeight(true) - 20) > 0){
						topOffset = ($(targetNode).offset().top - $('.hover-inline').outerHeight(true) - 20) + 'px';
						queryPopup.append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
						queryPopup.addClass('bottom');
					}else{
						queryPopup.append('<i class="material-icons arrow-denoter">arrow_drop_up</i>');
						queryPopup.addClass('top');
					}
					queryPopup.find('.material-icons.arrow-denoter').width($('.query-div[qid="' + target + '"]').find('.material-icons.arrow-denoter').css('font-size'))
					queryPopup.find('.material-icons.arrow-denoter').css('left', '0px');
					
					//show all queries and check all options when qyery licon click
					$('#queryDivNode .filterOptions .dropdown-content').each(function(){
						if($(this).find('[data-value="All"] input').length > 0){
							$(this).find('[data-value="All"] input')[0].checked = true;
							eventHandler.menu.navigation.checkAll('', $(this).find('[data-value="All"]'));
						}
					});
					
					$('.hover-inline').css('left', leftOffset).css('top', topOffset)
					$('[data-component]:not(.hidden)').addClass('hidden');
				}
				return false;
			},
			highlightIndex: function(param, targetNode){
				$('.highlight').remove();
				if ($(targetNode)[0].hasAttribute('data-rid')){
					var target = $(targetNode).attr('data-rid');
				}else{
					var target = $(targetNode).attr('id');
				}
				var queryLink = $("span[data-rid='"+target+"']");
				if ($('#contentDivNode .jrnlIndexRef[data-rid="' + target + '"]').length > 0){
					var startNode = $('#contentDivNode .jrnlIndexRef[data-rid="' + target + '"]:first')[0];
					var endNode = $('#contentDivNode .jrnlIndexRef[data-rid="' + target + '"]:last')[0];
					var range = rangy.getSelection();
					var sel = range.getRangeAt(0);
					sel.setStartAfter(startNode);
					sel.setEndBefore(endNode);
					findReplace.general.highlight(range, 'queryText');
					$(startNode)[0].scrollIntoView()
				}
				return false;
			},
			unableToResolve: function(param, targetNode) {
				var queryNode = $(targetNode).closest('.query-div');
				if (queryNode.length > 0){
					var target = $(queryNode).find('.query-holder').first();
					$(queryNode).find('.query-action').remove();
					$(queryNode).attr('data-remove-edit','true');
					eventHandler.query.action.addReply({'undoLevel':'false','content':'<i>Unable to resolve.</i>'}, target);
					$(queryNode).find('.query-holder:last').append('<div class="query-action"><span class="query-delete" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'deleteReply\'}">Delete</span></div>');
					kriyaEditor.init.addUndoLevel('unable-to-resolve');
				}
			},
			acceptNotficationQuery: function(param, targetNode){
				var queryNode = $(targetNode).closest('.query-div');
				if (queryNode.length > 0){
					var target = $(queryNode).find('.query-holder').first();
					$(queryNode).find('.query-action').remove();
					$(queryNode).attr('data-remove-edit','true');
					eventHandler.query.action.addReply({'undoLevel':'false','content':'<i>Query was accepted.</i>'}, target);
					kriyaEditor.init.addUndoLevel('query-accepted');
				}
			},
			deleteQuery: function(param, targetNode){
				var target = $(targetNode).closest('.query-div').attr('id');
				target = (target)?target:$(targetNode).closest('.query-div').attr('qid');

				eventHandler.query.action.updateRightPanel();
				$('#queryCloneContainer .query-div.hover-inline').remove();
				$('#queryDivNode .query-div.disabled').removeClass('disabled');

				var queryDiv = $('.query-div#' + target);
				queryDiv.attr('data-removed', 'true');
				kriyaEditor.settings.undoStack.push(queryDiv[0]);
				var queryRef = $('span.jrnlQueryRef[data-rid=' + target + ']');
				if(queryDiv.attr('data-query-action') == "uncited-citation" && queryRef.closest('.jrnlUncitedRef').length > 0){
					kriyaEditor.settings.undoStack.push(queryRef.closest('.jrnlUncitedRef').parent()[0]);
					queryRef.closest('.jrnlUncitedRef').contents().unwrap();
				}else{
					kriyaEditor.settings.undoStack.push(queryRef.parent()[0]);
				}
				if (queryRef.length > 0){
					queryRef.remove();
				}else if ($('*[data-rid=' + target + ']').length > 0){
					$('*[data-rid=' + target + ']').removeAttr('data-rid').removeAttr('data-query-ref').removeAttr('data-channel').removeAttr('data-topic').removeAttr('data-event').removeAttr('data-message');
				}				
				queryDiv.remove();
				kriyaEditor.init.addUndoLevel('query-delete');
				eventHandler.query.action.addBadge();
				eventHandler.menu.edit.addUndoModal('The query has been deleted.');
				$('.highlight').remove();
				setTimeout(function(){
					$('.material-tooltip[style]').removeAttr('style');
				},300);
			},
			deleteReply: function(param, targetNode){
				var queryDiv = $(targetNode).closest('.query-div');
				var target   = queryDiv.attr('id');
				target = (target)?target:queryDiv.attr('qid');
				
				$(targetNode).closest('.query-holder').remove();
				var queryActionType = queryDiv.attr('data-query-action');
				if(queryActionType){
					var queryAction = this.getQueryActions(queryActionType);
					queryDiv.find('.query-reply:last').closest('.query-action').remove();
					queryDiv.find('.query-holder:last').append(queryAction);
				}else{
					queryDiv.find('.query-reply').last().removeClass('hidden');
				}
				
				queryDiv.find('.collapse-reply').first().remove();
				queryDiv.removeAttr('data-replied');
				$('[data-rid="'+ target +'"]').removeAttr('data-replied');
				//$('#queryDivNode .query-div.disabled').after($(target)[0]);
				//$('#queryDivNode .query-div.disabled').remove();
				eventHandler.query.action.updateRightPanel();

				kriyaEditor.settings.undoStack.push($('.query-div#' + target)[0]);
				eventHandler.query.action.addBadge();
				kriyaEditor.init.addUndoLevel('delete-reply');
				eventHandler.menu.edit.addUndoModal('The reply has been deleted.');
				$('.highlight').remove();
				setTimeout(function(){
					$('.material-tooltip[style]').removeAttr('style');
				},300);
			},
			activateQueryBtn: function(param, targetNode){
				if ($(targetNode).text() == ""){
					if ($(targetNode).hasClass('queryContent')){
						$(targetNode).closest('[data-component]').find('.add-query').addClass('disabled');
						$(targetNode).closest('[data-component]').find('.upload-file').addClass('disabled');
					// Disabled the save and upload button for empty text box -rajesh
					}else if ($(targetNode).hasClass('query-content') || $(targetNode).hasClass('reply-content')){
						$(targetNode).closest('.query-holder').find('.save-edit').addClass('disabled');
						$(targetNode).closest('.query-holder').find('.upload-file').addClass('disabled');
					}else{
						$(targetNode).closest('.response-content').find('.add-reply').addClass('disabled');
						$(targetNode).closest('.response-content').find('.queryReplyType').addClass('hidden');
						$(targetNode).closest('.response-content').find('.upload-file').addClass('disabled');
					}
				}else{
					if ($(targetNode).hasClass('queryContent')){
						$(targetNode).closest('[data-component]').find('.add-query').removeClass('disabled');
						$(targetNode).closest('[data-component]').find('.upload-file').removeClass('disabled');
						// Enabled the save and upload button for empty text box -rajesh
					}else if ($(targetNode).hasClass('query-content') || $(targetNode).hasClass('reply-content')){
						$(targetNode).closest('.query-holder').find('.save-edit').removeClass('disabled');
						$(targetNode).closest('.response-content').find('.queryReplyType').removeClass('hidden');
						$(targetNode).closest('.query-holder').find('.upload-file').removeClass('disabled');
					}else{
						$(targetNode).closest('.response-content').find('.add-reply').removeClass('disabled');
						$(targetNode).closest('.response-content').find('.queryReplyType').removeClass('hidden');
						$(targetNode).closest('.response-content').find('.upload-file').removeClass('disabled');
					}
				}
			},
			selectQuery: function(param, targetNode){
				$(targetNode).toggleClass('selected');
				$(targetNode).removeClass('selected');
				$('[data-component="QUERY"]').find('.query.tab[data-type="freeform"]').trigger('click');
				$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').html($(targetNode).html());
				$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').trigger('keyup');
				$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').removeClass('hide');
				$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('data-class-selector', $(targetNode).attr('data-class-selector'));
				$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').focus();
				if($(targetNode).find('[contenteditable]').length > 0){
					$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('contenteditable', 'false');
					$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').find('[contenteditable]').first().focus();
				}
			},
			markAsResolved: function(param, targetNode){
				//if (param.dataRid != undefined){
					var target = $(targetNode).closest('.query-div').find('.query-holder').first();
					$(targetNode).closest('.query-action').remove();
					eventHandler.query.action.addReply({'content':'<i>Ok</i>'}, target);
				//}
			},
			createQueryReply: function(param, targetNode){		// function for generate and append response container for query
				var queryDiv = $(targetNode).closest('.query-div');
				var target = queryDiv.attr('id');
				target = (target)?target:queryDiv.attr('qid');
				if(!target){
					return false;
				}
				if (queryDiv.find('.queryResponse').length==0){
					//save opened reply contents on clicking another queries reply - jai
					if ($('#queryDivNode .queryResponse').length > 0){
						if ($('#queryDivNode .queryResponse').closest('.response-content').find('.add-reply').hasClass('disabled')){
							$('#queryDivNode .queryResponse').closest('.response-content').find('.cancelReply').trigger('click');
						}else{
							$('#queryDivNode .queryResponse').closest('.response-content').find('.add-reply').trigger('click');
						}
					}
					if ($('#queryCloneContainer .queryResponse').length > 0){
						if ($('#queryCloneContainer .queryResponse').closest('.response-content').find('.add-reply').hasClass('disabled')){
							$('#queryCloneContainer .queryResponse').closest('.response-content').find('.cancelReply').trigger('click');
						}else{
							$('#queryCloneContainer .queryResponse').closest('.response-content').find('.add-reply').trigger('click');
						}
					}
					var replyContent = $('<span class="response-content">');
					var responseNode = $('<span class="queryResponse" placeholder="reply" data-channel="query" data-topic="action" data-event="keyup" data-message="{\'funcToCall\': \'activateQueryBtn\'}">');
					var qid = uuid.v4();
					responseNode.attr('id', qid);
					//If enabled save button Add attribute for opened reply contents on focus out otherwise trigger cancel button - rajesh
					$('body').on('focusout', '#'+qid, function(){
						if (! $(this).closest('.response-content').find('.add-reply').hasClass('disabled')){
							$(this).closest('.response-content').attr('data-save','true');
						}else{
							//if cancelReply button already trigger in scroll event no need to trigger again heare for that this if condition.
							if($('#queryCloneContainer .query-div.hover-inline').find('.cancelReply').length > 0 && $('#queryCloneContainer .query-div.hover-inline').find('.cancelReply').attr('temp-data-removed') == undefined){
								$(this).closest('.response-content').find('.cancelReply').trigger('click');
							}else{
								$('#queryCloneContainer .query-div.hover-inline').find('.cancelReply').removeAttr('temp-data-removed');
							}
						}
					});
					
					$(replyContent).append(responseNode);
					var responseType = $('<span class="queryReplyType hidden"><input type="checkbox" class="filled-in" id="content-changed" data-value="contentChanged" name="query-res">The necessary changes have been made in the editor</span>');
					$(replyContent).append(responseType);
					var replyAction = $('<div class="reply-action"><span class="btn btn-medium green lighten-2 add-reply disabled" style="font-weight: 600;padding: 8px 12px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'addReply\'}">Save</span><span class="btn btn-medium grey lighten-4 cancelReply" style="margin-left: 8px;color: #505050;font-weight: 600;padding: 8px 12px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'cancelReply\'}">Cancel</span><input type="file" style="display:none;" data-channel="query" data-topic="action" data-event="change" data-message="{\'funcToCall\': \'uploadFile\'}"/><span class="btn btn-medium blue lighten-1 upload-file disabled pull-right" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'selectFile\'}" style="font-weight: 600;color: #fff;padding: 8px 12px;">Upload file</span></div>');
					$(replyContent).append(replyAction);
					queryDiv.append(replyContent);
				}
				queryDiv.find('.queryResponse').attr('contenteditable', 'true').focus();
				$(targetNode).addClass('hidden');
			},
			selectFile: function(param, targetNode){
				$(targetNode).parent().find('input[type="file"]').trigger('click');
			},
			removeFile: function(param, targetNode){
				if($(targetNode).closest('[data-component]').length > 0){
					var topOffset = parseInt($('[data-component="QUERY"]').css('top'));
					var heightBefore = $('[data-component="QUERY"]').height()
					var heightAfter = $(targetNode).closest('div[data-type="file"]').height();
					topOffset = topOffset - (heightAfter - heightBefore) + 'px';
					$('[data-component="QUERY"]').animate({top: topOffset}, 600);
				}else if ($(targetNode).closest('.query-holder').find('div[contenteditable]').length > 0){
					$(targetNode).closest('.query-holder').find('.query-action').append('<input type="file" style="display:none;" data-channel="query" data-topic="action" data-event="change" data-message="{\'funcToCall\': \'uploadFile\'}"/><span class="green lighten-1 upload-file pull-right" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'selectFile\'}" style="font-weight: 600;color: #fff;padding: 0px 4px;">Upload file</span>');
				}
				$(targetNode).closest('div[data-type="file"]').animate({ height:'0px' }, 600, function(){$(this).remove()});
			},
			uploadFile: function(param, targetNode){
				if ($(targetNode)[0].files.length == 0) return;
				var param = {
					'file': $(targetNode)[0].files[0],
					'convert': 'false',
					'success': function(res){
						var uploadedFile = '';
						//var sourceData = '../resources/' + kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/' + data;

						var fileDetailsObj = res.fileDetailsObj;
						//var data = fileDetailsObj.dstKey.replace(/\\/g,'/').replace(/.*\//, '');
						var data = fileDetailsObj.name+fileDetailsObj.extn;
						var sourceData = '/resources/' + fileDetailsObj.dstKey;
						

						if (/\.pdf$/.test(data)){
							uploadedFile = '<embed src="' + sourceData + '" width="100%" type="application/pdf" internalinstanceid="80">';
						}else if (/\.(jpe?g|png|gif)$/i.test(data)){
							uploadedFile = '<img src="' + sourceData + '"/>';
						}else{
							uploadedFile = '<p class="note-file" src="'+ sourceData +'">'+data+'</p>';
						}
						uploadedFile = $('<div data-type="file" data-file-name="' + data + '">'+uploadedFile+'</div>');
						//add new query
						if ($(targetNode).closest('[data-component]').length > 0){
							var heightBefore = $('[data-component="QUERY"]').height()
							$(targetNode).closest('[data-component]').find('div[data-type="file"]').remove();
							$(targetNode).closest('[data-component]').find('.queryContent').after(uploadedFile);
							if ($('[data-component="QUERY"]').hasClass('bottom')){
								var topOffset = parseInt($('[data-component="QUERY"]').css('top'));
								var heightAfter = $('[data-component="QUERY"]').height();
								$('[data-component="QUERY"]').css('top', (topOffset - (heightAfter - heightBefore) + 'px'));
							}
						}else if ($(targetNode).closest('.query-holder').find('[contenteditable]').length > 0){
							uploadedFile.attr('data-editable', 'true');
							$(targetNode).closest('.query-holder').find('[contenteditable]').after(uploadedFile);
							$(targetNode).closest('.query-holder').find('.query-action').find('.upload-file,input[type="file"]').remove();
						}else{
							$('.queryResponse').find('div[data-type="file"]').remove();
							$('.queryResponse').after(uploadedFile);
						}

						$('.la-container').fadeOut();
					},
					'error': function(err){
						$('.la-container').fadeOut();
						kriya.notification({
							title: 'Failed',
							type: 'error',
							content: 'Uploading file failed.',
							timeout: 8000,
							icon : 'icon-info'
						});
					}
				}
				$('.la-container').fadeIn();
				eventHandler.menu.upload.uploadFile(param);
			},
			addReply: function(param, targetNode){
				var queryDiv = $(targetNode).closest('.query-div');
				var target   = queryDiv.attr('id');
				target       = (target)?target:queryDiv.attr('qid');

				if (queryDiv.find('.queryResponse').text() != ""){
					if (queryDiv.find('[data-type="file"]').length > 0){
						queryDiv.find('.queryResponse').append(queryDiv.find('[data-type="file"]'));
					}
					var responseHTML = queryDiv.find('.queryResponse').html();
				}else if (param && param.content != ''){
					var responseHTML = param.content;
				}
				var objDate = new Date();
				var time = objDate.getHours() + ":" + ('0'+objDate.getMinutes()).slice(-2);
				var locale = "en-us",
				month = objDate.toLocaleString(locale, { month: "short" });
				var userName = kriyaEditor.user.name;
				if($('#contentContainer').attr('data-offline-workflow') == "true" && $(targetNode).closest('.query-div').find('.query-content').attr('data-assigned-to-role') == "Author"){
					userName = $('#headerContainer .userProfile .username').text() + ' (on behalf of the author)';
				}

				var objDateForm = month + ' ' + objDate.getDate() + ' ' + (objDate.getYear() + 1900) + ' (' + time + ')'; 
				var replyContent = $('<div class="query-holder"/>');
				var replyHead = $('<div class="query-head"><span class="reply-from">'+ userName +'</span><span class="reply-time">' + objDateForm + '</span></div>');
				$(replyContent).append(replyHead);
				var replyText = $('<div class="reply-content">'+ responseHTML + '</div>');
				$(replyText).attr("data-replied-by", kriyaEditor.user.name).attr("data-replied-by-role", kriya.config.content.role).attr("data-date", objDate.toLocaleDateString());
				$(replyContent).append(replyText);
				queryDiv.append(replyContent);
				replyContent.addClass('reply-left');
				queryDiv.find('.query-reply, .query-edit, .query-delete').addClass('hidden');

				
				if (!(param && param.content != '')){
					$(replyContent).append('<div class="query-action"><span class="query-delete" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'deleteReply\'}">Delete</span><span class="query-edit" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'editReply\'}">Edit</span></div>');
				}

				if($(targetNode).closest('.query-div').find('#content-changed:checked').length > 0 || param && param.reply == true){
					$(targetNode).closest('.query-div').attr('data-replied', 'true');
					$('[data-rid="'+ target +'"]').attr('data-replied', 'true');
					$(replyContent).addClass('data-reply');
				}else{
					$(targetNode).closest('.query-div').attr('data-answered', 'true');
					$('[data-rid="'+ target +'"]').attr('data-answered', 'true');
					$(replyContent).addClass('data-answered');
				}

				kriyaEditor.settings.undoStack.push($('[data-rid="'+ target +'"]')[0]);
				$(targetNode).closest('.query-div').find('.collapse-reply').remove();
				var collapseReply = $('<span class="collapse-reply expanded" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'collapseReply\'}">Hide replies <i class="material-icons">arrow_drop_up</i></span>');
				$(targetNode).closest('.query-div').find('.query-action').first().append(collapseReply);
				
				$(targetNode).closest('.query-div').find('.response-content').remove();
				eventHandler.query.action.updateRightPanel();
				
				eventHandler.query.action.addBadge();
				kriyaEditor.settings.undoStack.push($('.query-div#' + target)[0]);
				if (param && param.undoLevel != undefined && param.undoLevel == 'false'){
					//prevent undolevel function call for #346 - priya
				}else{
					kriyaEditor.init.addUndoLevel('add-reply'); 
				}
			},
			cancelReply: function(param, targetNode){
				var queryDiv = $(targetNode).closest('.query-div');
				if($(targetNode).hasClass('cancel-edit')){
					$(queryDiv).find('.queryReplyType').remove();
				}
				if($(targetNode).hasClass('cancelReply')){
					// While click cancel then hide cancel button -rajesh
					if($(queryDiv).find('.data-reply, .data-answered').length == 0){
						$(queryDiv).find('.query-reply:last').removeClass('hidden');
					}
					$(queryDiv).find('.response-content').remove();
					
				}else{

					//After cancel change the styles and show the author name - rajesh
					$(targetNode).closest('.query-holder').removeAttr('style');
					$(targetNode).closest('.query-holder').css({'margin-left': '20px !important','background': '#fff'});
					$(targetNode).closest('.query-holder').find('.reply-content').css({'margin-left': '0px'});
					$(targetNode).closest('.query-holder').find('.query-action').css({'margin-left': '0px'});
					$(targetNode).closest('.query-holder').find('.query-head').show();
					// Replace the previous value for clicking cancel button -rajesh
					var replyContent = $(targetNode).closest('.query-holder').find('.reply-content[contenteditable]').attr('data-val');
					$(targetNode).closest('.query-holder').find('.reply-content[contenteditable]').html(replyContent);
					$(targetNode).closest('.query-holder').find('.reply-content[contenteditable]').removeAttr('data-val');
					$(targetNode).closest('.query-holder').find('.reply-content[contenteditable]').removeAttr('contenteditable');
				}
				// Hide query popup only for clicking the close button -rajesh
				if($(targetNode).hasClass('com-close')){
					if ($('.query-div.hover-inline').length > 0){
						$('.query-div.disabled').after($('#queryCloneContainer .query-div.hover-inline').clone());
						$('.query-div.disabled').remove();

						var queryID = $('#queryDivNode .query-div.hover-inline').attr('qid');
						$('#queryDivNode .query-div.hover-inline').attr('id', queryID);
						$('#queryDivNode .query-div.hover-inline').removeAttr('qid');

						$('#queryCloneContainer .query-div.hover-inline').remove();
						$('.query-div.hover-inline').removeAttr('style').removeClass('hover-inline').removeClass('top').removeClass('bottom');						
					}
					$('.query-div').find('.material-icons.arrow-denoter,.btn-floating.btn-small.com-close').remove();
				}
				// This condition only working for cancel button -rajesh
				if($(targetNode).hasClass('cancel-edit')){
					$('.query-holder').removeAttr('data-save');	
					if ($(queryDiv).find('.save-edit').length > 0){
						$(queryDiv).find('.query-edit,.query-delete').removeClass('hidden');
						$(queryDiv).find('.upload-file,input[type="file"],.save-edit').remove();
						$('.query-content[contenteditable]').removeAttr('contenteditable');
					}
				}
				
				$(queryDiv).find('.cancel-edit').addClass('hidden');
			},
			collapseReply: function(param, targetNode){
				if ($(targetNode).hasClass('expanded')){
					$(targetNode).removeClass('expanded').addClass('collapsed').html('View all ' + $(targetNode).closest('.query-div').find('.query-holder.data-reply').length + ' replies <i class="material-icons">arrow_drop_down</i>');
					$(targetNode).closest('.query-div').find('.query-holder.data-reply').addClass('hidden');
				}else{
					$(targetNode).removeClass('collapsed').addClass('expanded').html('Hide replies <i class="material-icons">arrow_drop_up</i>');
					$(targetNode).closest('.query-div').find('.query-holder.data-reply').removeClass('hidden');
				}
			},
			toggleNotes: function(param, targetNode){
				if (kriya.evt.target && kriya.evt.target.hasAttribute('id') && kriya.evt.target.getAttribute('id') == "queryDivNode") return;
				if (kriya.evt.target && kriya.evt.target.hasAttribute('class') && $(kriya.evt.target).hasClass('addNewNote')) return;
				if ($('.notes-icon').hasClass('active')){
					$('.nav-action-icon').removeClass('active');
					$('.navDivContent > div').removeClass('active');
				}else{
					$('.nav-action-icon').removeClass('active');
					$('.notes-icon').addClass('active');
					$('.navDivContent > div').removeClass('active');
					$('#queryDivNode').addClass('active');
					if($('.notes-icon').length > 0){
						$('.notification-popup').css('right', -(window.innerWidth - ($('.notes-icon').offset().left + $('.notes-icon').outerWidth(true))))
						$('.notification-popup').css('top', ($('.notes-icon').outerHeight()));
					}
				}
			},
			regenerateActionQuery: function(param, targetNode){
				//when editor open as read only mode at that time it will return false-kirankumar
				if ($('#contentContainer').attr('data-state') == "read-only") return false;
				//Add action query for uncited citation
				if($('[data-component="QUERY"] [data-query-type="predefined"] [data-query-action="uncited-citation"]').length > 0){
					$(kriya.config.containerElm).find(kriya.config.citationSelector + ', .jrnlUncitedRef').each(function(i){
						//skip the deleted nodes
						if($(this).closest('.del, [data-track="del"], .jrnlDeleted').length > 0){
							return;
						}
						var citeNode = this;
						var citationClass = $(citeNode).attr('class');
						if(citationClass == "jrnlUncitedRef" && $(citeNode).attr('data-old-class')){
							citationClass = $(citeNode).attr('data-old-class');
						}
						if($(citeNode).find('.jrnlQueryRef[data-query-action="uncited-citation"]:not([data-replied="true"])').length == 0 && $('[data-component="QUERY"] [data-query-type="predefined"] [data-query-action="uncited-citation"][data-class-selector*=" ' + citationClass + ' "]').length > 0){
							var citationString = $(citeNode).attr('data-citation-string');
							citationString = (citationString)?citationString.replace(/^\s+|\s+$/g, '').split(/\s+/):['null'];
							$.each(citationString, function(i, rid){
								if($(kriya.config.containerElm).find('[data-id="' + rid + '"]').length == 0 && $(kriya.config.containerElm).find('[data-citation-string*=" ' + rid + ' "] .jrnlQueryRef[data-query-action="uncited-citation"]').length == 0){
									var queryText = $('<div>' + $('[data-component="QUERY"] [data-query-type="predefined"] [data-query-action="uncited-citation"][data-class-selector*=" ' + citationClass + ' "]').html() + '</div>');
									queryText.find('.material-icons').remove();
									queryText.find('[data-selector]').each(function(i, obj){
										var dataSelector = $(this).attr('data-selector');
										var clonedNode = $(citeNode).clone(true);
										clonedNode.cleanTrackChanges();
										var dataNode     = kriya.xpath(dataSelector, clonedNode[0]);
										if($(dataNode).length > 0){
											var htmlNodes = kriya.general.getHTML(dataNode);
											$.each(htmlNodes, function(i, val){
												$(obj).append(val);
											});
											if($(this).attr('data-start-punctuation')){
												$(obj).prepend($(this).attr('data-start-punctuation'))
											}
											if($(this).attr('data-end-punctuation')){
												$(obj).append($(this).attr('data-end-punctuation'))
											}
										}
									});

									var queryTo = 'Author';
									var qid = eventHandler.query.action.createQueryDiv(queryTo, 'action', 'uncited-citation');
									$('#' + qid).find('.query-from').html('Kriya (' + kriya.config.content.role.toUpperCase() + ')');
									$('#' + qid).attr('data-query-action', 'uncited-citation');
									var queryContent = $('#' + qid).find('.query-content');
									queryContent.attr('data-assigned-by', 'Kriya (' + kriya.config.content.role.toUpperCase() + ')');
									$(queryContent).html($(queryText).html());
									kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);

									var startNode = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
									$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', 'author');
									$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
									
									$(startNode).attr('data-query-action', 'uncited-citation');
									$(citeNode).prepend(startNode);
									kriyaEditor.settings.undoStack.push(citeNode);
								}
							});	

						}
					});
				}
				
				//Add action query for uncited floats
				$(kriya.config.containerElm).find('.jrnlFigBlock, .jrnlTblBlock, .jrnlRefText, .jrnlVidBlock').not('.del,[data-track="del"],[data-inline="true"]').each(function(){
					var floatNode = this;
					if($(floatNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').length > 0){
						floatNode = $(floatNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup')[0];
					}
					var dataId = $(floatNode).attr('data-id');
					dataId = dataId.replace(/BLK_/g, '');
					dataId = dataId.replace(/[a-z]$/g, '');
					var fClass = $(this).attr('class');
					var actionName = (dataId.match(/^V/))?'uncited-video':(dataId.match(/^F/))?'uncited-figure':(dataId.match(/^T/))?'uncited-table':'uncited-reference';
					if($(kriya.config.containerElm).find('[data-citation-string*=" ' + dataId + ' "]:not([data-track="del"])').length == 0 && $(floatNode).find('.jrnlQueryRef[data-query-action="' + actionName + '"]:not([data-replied="true"])').length == 0 && $('[data-component="QUERY"] [data-query-type="predefined"] [data-query-action="' + actionName + '"][data-class-selector*=" ' + fClass + ' "]').length > 0){
						var queryText = $('<div>' + $('[data-component="QUERY"] [data-query-type="predefined"] [data-query-action="' + actionName + '"]').html() + '</div>');
						queryText.find('.material-icons').remove();
						queryText.find('[data-selector]').each(function(i, obj){
							var dataSelector = $(this).attr('data-selector');
							var dataNode     = kriya.xpath(dataSelector, floatNode);
							if($(dataNode).length > 0){
								var htmlNodes = kriya.general.getHTML(dataNode);
								$.each(htmlNodes, function(i, val){
									$(obj).append(val);
								});
								if($(this).attr('data-start-punctuation')){
									$(obj).prepend($(this).attr('data-start-punctuation'))
								}
								if($(this).attr('data-end-punctuation')){
									$(obj).append($(this).attr('data-end-punctuation'))
								}
							}
						});

						var queryTo = 'Author';
						var qid = eventHandler.query.action.createQueryDiv(queryTo, 'action', actionName);
						$('#' + qid).find('.query-from').html('Kriya (' + kriya.config.content.role.toUpperCase() + ')');
						$('#' + qid).attr('data-query-action', actionName);
						$('#' + qid).attr('data-float-id', dataId);
						var queryContent = $('#' + qid).find('.query-content');
						queryContent.attr('data-assigned-by', 'Kriya (' + kriya.config.content.role.toUpperCase() + ')');
						$(queryContent).html($(queryText).html());
						kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);

						var startNode = $('<span class="jrnlQueryRef" data-rid="' + qid + '">');
						$(startNode).attr('data-query-ref', 'true').attr("data-type", "start").attr('data-query-for', 'author');
						$(startNode).attr("data-channel", "query").attr("data-topic", "action").attr("data-event", "click").attr("data-message", "{\'funcToCall\': \'highlightQuery\'}");
						
						$(startNode).attr('data-query-action', actionName);
						if($(floatNode).find('[class*="Caption"][class*="jrnl"]').length > 0){
							$(floatNode).find('[class*="Caption"][class*="jrnl"]:first').prepend(startNode);
						}else{
							$(floatNode).prepend(startNode);
						}
						
						kriyaEditor.settings.undoStack.push(floatNode);
					}
				});
				if(kriyaEditor.settings.undoStack.length > 0){
					eventHandler.query.action.addBadge();
					kriyaEditor.init.addUndoLevel('generate-action-queries');
				}
			},
			addNewNote: function(param, targetNode){
				var popUp = $('[data-component="QUERY"]');
				$(popUp).removeClass('hidden').css({
					'top':kriya.pageYaxis,
					'left':'40%',
					'height':'auto',
					'opacity':'1',
				});
				$(popUp).find('[data-class="jrnlQueryText"]').focus();
			},
			addBadge: function(){
				$('.notes-icon .notify-badge').remove();
				var cnt = 0;

				//#399 -All customers | No. of unanswered queries -Helen.j-focus -helen.j@focusite.com
                $('#queryDivNode .query-div.card[data-type="action"]').each(function(){
					//var qRole = $(this).find('.query-content').attr('data-assigned-by-role');
					//qRole = qRole.toLowerCase();
					//if ($(this).find('.query-holder.data-reply').length == 0 && qRole != kriya.config.content.role){
					//Condition modified by jagan issue ID - #3309
					if ($(this).find('.query-holder.data-reply').length == 0){
						cnt++;
					}
				});
				//End of #399 -All customers | No. of unanswered queries-Helen.j-focus -helen.j@focusite.com
				
				if (cnt > 0){
					$('.notes-icon').append('<span class="notify-badge">'+ cnt + '</span>');
				}
			},
			/**
			 * Function to order the query based on location and check the query duplication
			 */
			orderQuery: function(){
				if ($('#contentContainer').attr('data-state') == "read-only") return false; // to block duplicate query popup for locked article / aravind
				//Display error message if query was duplicated - jagan
				if(kriya.config.content.role == "typesetter" || kriya.config.content.role == "preeditor"){
					$('#queryDivNode .query-div.card').each(function(){
						var qID = $(this).attr('id');
						if($('#queryDivNode .query-div.card#' + qID).length > 1){
							var settings = {
								'icon'  : '<i class="material-icons">warning</i>',
								'title' : ' ERROR',
								'text'  :  'Query was duplicated something went wrong. So, please contact support.',
								'size'  : 'pop-sm',
								'hideElement' : 'com-close',
								'buttons' : {
									'Report Issue' : {
										'text'    : 'Report Issue',
										'class'   : 'btn btn-small action-btn',										
										'message' : "{'click':{'funcToCall': 'reportIssueModal','channel':'welcome','topic':'signoff'}}"
									}
								}	
							};
							kriya.actions.confirmation(settings);
							return false;
						}
					});
				}

				//Add the hide query attribute for action queries - jagan
				if(kriya.config.content.role != "typesetter" || kriya.config.content.role != "preeditor"){
					$('#queryDivNode .query-div.card[data-query-action][data-replied="true"]').each(function(){
						var repliedBy = $(this).find('.reply-content:last').attr('data-replied-by-role').toLowerCase();
						if(repliedBy != kriya.config.content.role){
							$(this).attr('data-hide-query', 'true');
							var qID =  $(this).attr('id');
							$('.jrnlQueryRef[data-rid="' + qID + '"]').attr('data-hide-query', 'true');
						}
					});
				}


				/**
				 * Loop the start query ref node
				 * STEP 1: If first ref and if the first query dive node id not match with ref id then move to first position
				 * STEP 2: Get the previous ref node if previous node of query div node id is not equal to the previous query ref rid 
				 *         then move the queryNode next to the query of previous ref
				 * STEP 3: if the undo stack array has value then trigger undolevel
				 */
				$(kriya.config.containerElm + ' .jrnlQueryRef[data-type="start"]').each(function(i){
					var rid = $(this).attr('data-rid');
					var queryNode = $('.query-div#' + rid);
					if(i == 0 && rid && queryNode.length > 0){
						var firstQueryNode = $('.query-div:first');
						if(firstQueryNode.attr('id') && firstQueryNode.attr('id') != rid){
							queryNode.insertBefore(firstQueryNode);
							//queryNode.attr('data-moved', 'true');
							//kriyaEditor.settings.undoStack.push(queryNode[0]);
							
							//firstQueryNode.attr('data-moved', 'true');
							//kriyaEditor.settings.undoStack.push(firstQueryNode[0]);
						}
					}else if(rid && queryNode.length > 0){
						var prevQRefID = $(kriya.config.containerElm + ' .jrnlQueryRef[data-type="start"]:eq(' + (i-1) + ')').attr('data-rid');
						if(prevQRefID && prevQRefID != queryNode.prev().attr('id') && $('.query-div#' + prevQRefID).length > 0){
							var prevQueryNode = $('.query-div#' + prevQRefID);
							queryNode.insertAfter(prevQueryNode);
							//queryNode.attr('data-moved', 'true');
							//kriyaEditor.settings.undoStack.push(queryNode[0]);

							//prevQueryNode.attr('data-moved', 'true');
							//kriyaEditor.settings.undoStack.push(prevQueryNode[0]);
						}
					}
				});
				/*if(kriyaEditor.settings.undoStack && kriyaEditor.settings.undoStack.length > 0){
					kriyaEditor.init.addUndoLevel('order-query');
				}*/
			},
			/**
			 * Function to update the query in right panel when changes in popup
			 * @author Jagannathan Jothi
			 */
			updateRightPanel: function(){
				//Save the query in right panel when edit query and save -Jagan
				if ($('#queryCloneContainer .query-div.hover-inline').length > 0){
					$('.query-div.disabled').after($('#queryCloneContainer .query-div.hover-inline').clone());
					$('.query-div.disabled').remove();
					var queryID = $('#queryDivNode .query-div.hover-inline').attr('qid');
					$('#queryDivNode .query-div.hover-inline').attr('id', queryID);
					$('#queryDivNode .query-div.hover-inline').removeAttr('qid');
					$('#queryDivNode .query-div.hover-inline').removeAttr('style').removeClass('hover-inline').removeClass('top').removeClass('bottom').addClass('disabled');
					$('#queryDivNode .query-div').find('.material-icons.arrow-denoter,.btn-floating.btn-small.com-close').remove();
				}
			}
		}
	};
	
	return eventHandler;
})(eventHandler || {});
