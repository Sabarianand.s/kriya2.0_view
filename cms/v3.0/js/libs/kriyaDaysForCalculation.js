/**
 * Author: 816-Prasana
 * Internation holidays
 *
 * holidays: These days will be removed for days calculation.
 * TAT: Turn Around Time for each customer.
 */

var daysForCalculation = {
  "holidays": {
    "default": ['*-01-01', '*-12-25']
  },
  "TAT": {
    "default": {
      'addJob': 0, 'File download': 1, 'Convert Files': 1, 'Pre-editing': 2, 'Waiting for CE': 1,
      'Copyediting': 1, 'Copyediting Check': 1, 'Typesetter Check': 1, 'Editor Check': 1, 'Associate Editor Check': 1,
      'Proofreading': 1, 'Publisher Check': 1, 'Author Review': 2, 'Editor Review': 1, 'Typesetter Review': 1, 'Author Revision': 2,
      'Publisher Review': 1, 'Final Deliverables': 1, 'Banked': 0, 'Upload for Online First': 1, 'Archive': 0, 'Resupply': 1,
      'Hold': 1, 'Support': 1, 'Proofreading': 1, 'Issue Makeup': 4, 'Validation Check': 1
    },
    "bmj": {
      'addJob': 0, 'File download': 1, 'Convert Files': 1, 'Pre-editing': 2, 'Waiting for CE': 1,
      'Copyediting': 1, 'Copyediting Check': 1, 'Typesetter Check': 1, 'Editor Check': 1, 'Associate Editor Check': 1,
      'Proofreading': 1, 'Publisher Check': 1, 'Author Review': 2, 'Editor Review': 1, 'Typesetter Review': 1, 'Author Revision': 2,
      'Publisher Review': 1, 'Final Deliverables': 1, 'Banked': 0, 'Upload for Online First': 1, 'Archive': 0, 'Resupply': 1,
      'Hold': 1, 'Support': 1, 'Proofreading': 1, 'Issue Makeup': 4, 'Validation Check': 1
    }
  }
}
this.daysForCalculation = daysForCalculation;