/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

	//var _this;
    kriya.validate = {
        init: function (component) {
			var _this = this;
			this.container = kriya.config.activeElements;
			this.component = component;
			validateContent(component);
		
			function validateContent(component){

				//#428 - Provision for multiple data-validate options - Rajasekar T(rajasekar.t@focusite.com)
				var valTypeStr = $(component).attr('data-validate');
				var valTypeEl = valTypeStr.split(" ");
				var prevRetVal = true;
				$(valTypeEl).each(function(i, valType){
				if (valType == undefined) return false;
				if (typeof(kriya.validate[valType]) != 'undefined' && (i ==0 || prevRetVal)){
					prevRetVal = kriya.validate[valType](_this.container, component);
				}
				});
				//End of #428
			}
        },
		required: function(container, component){
			$(component).removeAttr('data-error');
			if ($(component)[0].nodeName == "INPUT" && ($(component).attr("type") == "radio")){
				var checked = false;
				var radioName = $(component).attr("name");
				
				//$(component).parent().find('input[name="' + radioName + '"]').each(function(){
				$(component).closest('[data-component]').find('input[name="' + radioName + '"]').each(function(){
					if ($(this).prop('checked') == true){
						checked = true;
					}
				})
				if (!checked){
					$(component).parent().attr('data-error', 'Input Required');
					//#428 - checking for previous results - Rajasekar T(rajasekar.t@focusite.com)
					return false;
				}
			}
			else if ($(component).text() == "") {
				$(component).attr('data-error', 'Input Required');
				//#428 - checking for previous results - Rajasekar T(rajasekar.t@focusite.com)
				return false;
			}
			return true;
		},
		predefined: function(container, component){
			$(component).removeAttr('data-error');
			if($(component)[0].hasAttribute('data-list-selector')){
				var validateText     = $(component).text();
				var listSelector = $(component).attr('data-list-selector');
				var listNode = kriya.xpath(listSelector);
				if($(listNode).length > 0){
					var existData = kriya.xpath('//li[. = "' + validateText + '"]', $(listNode)[0]);
					if($(existData).length == 0){
						$(component).attr('data-error', 'Please select data from list');
						return false;
					}
				}
			}
			return true;
		},
		requiredResource: function(container, component){
			$(component).removeAttr('data-error');
			if($(component).attr('data-check-attr')){
				var checkAttr = $(component).attr('data-check-attr');
				checkAttr = checkAttr.split(',');	
				for (var i = 0; i < checkAttr.length; i++) {
					var attrName = checkAttr[i];
					if(!$(component)[0].hasAttribute(attrName)){
						$(component).attr('data-error', 'Input Required');
						//#428 - checking for previous results - Rajasekar T(rajasekar.t@focusite.com)
						return false;
					}
				}
			}
			return true;
		},
		requiredField: function(container, component){
			$(component).removeAttr('data-error');
			if ($(component)[0].nodeName == "INPUT" && ($(component).is(":checked") == false)){
				return;
			}
			if ($(component)[0].nodeName == "IMG" && $(component).attr('src') == undefined){
				return;
			}
			var checkField = $(component).attr('data-check-field');
			if (checkField == undefined) return;
			
			var checkFieldEle = $(component).closest('[data-type="popUp"]').find('[data-class="'+checkField+'"]');
			if (checkFieldEle.length > 0) {
				if (checkFieldEle.text() == ""){
					checkFieldEle.attr('data-error', 'Please fill this field');
					return false;
				}else if(checkFieldEle.find('.kriya-tagsinput').length > 0 && checkFieldEle.find('.kriya-tagsinput').text() == ""){
					checkFieldEle.find('.kriya-tagsinput').attr('data-error', 'Please fill this field');
					return false;
				}else if(checkFieldEle.attr('data-email') == "true"){
					this.email(container, checkFieldEle);
				}
			}
			return true;
		},
		requiredChild: function(container, component){
			$(component).removeAttr('data-error');
			var childClass = $(component).attr('data-source');
			if (childClass == undefined) return;
			if ($(component).find('.'+childClass).length == 0) {
				$(component).attr('data-error', 'Please add required fields');
				return false;
			}
			return true;
		},
		email: function(container, component){
			$(component).removeAttr('data-error');
			if ($(component).text() != "" && !$(component).text().match(/^([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4})(([\;\,]\s?)([\w-\.]+@([\w-]+\.?)+\.[\w-]{2,4}))*$/)) {
				$(component).attr('data-error', 'Please enter the valid email');
				return false;
			}
			return true;
		},
		validateDOI: function(container, component){
			$(component).removeAttr('data-error');
			if ($(component).text() == "" || !$(component).text().match(/^10\.(\d{4})\/(.*)$/)) {
				$(component).attr('data-error', 'Please enter the valid DOI');
				return false;
			}
			return true;
		}
    };
    
    return kriya;

}(window.kriya || {}));