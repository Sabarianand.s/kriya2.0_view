var assignedArticles;
var dateToAssign;

(function (eventHandler) {
    eventHandler.assignmentSummary = {
        showUnassigned: function (param, targetNode, recursive) {
            assignedArticles = param
            dateToAssign = param.fromDate;
            $('.la-container').css('display', '');
            $('.showEmpty').hide();
            $(currentTab + '.userDisplay').addClass('hidden')
            $('#searchAssign').css({'display':'inline','margin-left':'130px'})
            $('#searchBoxAssign').addClass('col-5')
            $(currentTab + 'h3').show()
            $(currentTab + '#sort, '+currentTab +' .assign').show()
            $(currentTab + '#userDataContent').html('');
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
            if (!param.stageName)
                var assignStageName = targetNode.html()
            else
                assignStageName = param.stageName
            var dataToPost = {
                customer: param.customerName,
                fromDate: dateFormat(firstDay, 'yyyy-mm-dd'),
                toDate: dateFormat(lastDay, 'yyyy-mm-dd'),
                stage: assignStageName
            }
            if(assignStageName == 'All')
            assignStageName = 'Typesetter Check|Pre-editing|Validation Check|Typesetter Review|Final Deliverables'
            var param = {
                customer: param.customerName,
                urlToPost: "getStageUnassigned",
                stage: assignStageName,
                excludeUser: ".*com",
                version: "v2.0",
                mainDataXpath: "$..hits",
                time_zone: "+05:30",
                fromDate: param.fromDate,
                toDate: param.toDate,
                storeData: "unAssignData",
                workflowStatus: "in-progress",
                project: param.projectName
            }
            //it should not call directly , needs to call by showManuscript
            // after selecting project, list articles
            $('.save-notice').addClass('hide');
            if (!param.storeData) {
                param.storeData = 'cardData';
            }
            window[param.storeData] = [];
            if (param) {
                if ($(targetNode).parent().attr('id') == 'filterStatus') {
                    $(targetNode).parent().siblings().html($(targetNode).text());
                    $(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
                }
                if ($(targetNode).parent().attr('id') == 'filterCustomer') {
                    $(currentTab + '#filterStatus').children().removeClass('active');
                    $(currentTab + '#filterStatus').children().first().addClass('active');
                }
                if (!param.from || param.from == undefined) {
                    param.from = 0;
                }
                if (!param.size || param.size == undefined) {
                    param.size = 500;
                }
                if (param.excludeStage) {
                    param.excludeStageArticles = excludeStage[param.excludeStage].join(' OR ');
                }
                param.user = 'userName';
                //param.stageName = 'stageName';
                if (!param.customer || param.customer == 'first' || param.customer == 'Customer') {
                    try {
                        param.customer = Object.keys(JSON.parse($('#userDetails').attr('data')).kuser.kuroles)[0];
                    } catch (e) {
                        console.log(e);
                    }
                }
                if (!param.customer || (param.customer == 'selected' && param.custTag)) {
                    try {
                        var cs = [];
                        $(param.custTag + ' .active').each(function (s) {
                            cs.push($(this).text());
                        });
                        param.customer = cs.join(' OR ');
                    } catch (e) {
                        console.log(e);
                    }
                }
                if (param.customer == 'customer' || selectedCustomer != param.customer) {
                    eventHandler.components.actionitems.getUserDetails(param.customer);
                }
                if (!param.project) {
                    param.project = 'project';
                }
                $(targetNode).addClass('active');
                $(targetNode).siblings().removeClass('active');
                if (recursive == undefined) {
                    data = {};
                }
                if (!param.customer.match(/customer/i)) {
                    if (userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['access-level']) {
                        var roleType = userData.kuser.kuroles[param.customer]['role-type'];
                        var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
                        // For Copyeditors login
                        if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].access.indexOf(accessLevel) > -1 && param.urlToPost.match(/getUnassigned/gi)) {
                            var userDetails;
                            $.ajax({
                                type: "GET",
                                url: "/api/getuserdata?customer=" + param.customer,
                                async: false,
                                dataType: 'json',
                                success: function (respData) {
                                    if (respData) {
                                        userDetails = respData.find(function (user) {
                                            return user.email === userData.kuser.kuemail;
                                        })
                                    }
                                },
                                error: function (respData) {
                                    $('.la-container').css('display', 'none');
                                }
                            });
                            var userPageCount = userDetails.roles.find(function (role) {
                                return role['customer-name'] === param.customer
                            })
                            // param['pageCount'] = Number(userData.kuser.kuroles[param.customer]['page-limit']) - Number(userData.kuser.kuroles[param.customer]['assigned-pages']);
                            param['pageCount'] = Number(userPageCount['page-limit']) - Number(userPageCount['assigned-pages']);
                            param['fromWordCount'] = 0;
                        }
                    }
                }
            }
            if (param.customer && param.customer != 'customer') {
                selectedCustomer = param.customer;
            }
            //To raise query for first time and if we select different customer.
            if (manuScriptCardData.length == 0 || manuScriptCardData.length == undefined || param.stage == 'Support' || recursive == 1) {
                $.ajax({
                    type: "POST",
                    url: '/api/getarticlelist',
                    data: param,
                    dataType: 'json',
                    success: function (respData) {
                        window[param.storeData] = respData.hits
                        if (!respData || respData.length == 0) {
                            $('#manuscriptsDataContent').html('');
                            $('#manuscriptContent .filterOptions').hide();
                            $('div.navFilter').empty();
                            if (selectedCustomer) {
                                param = {};
                                param.customer = selectedCustomer;
                                eventHandler.filters.populateNavigater(param);
                            }
                            $(currentTab + '#msYtlCount, ' + currentTab + '#msCount').text('0');
                            $('.la-container').css('display', 'none');
                            $(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
                            return;
                        }
                        var oldTotal = parseInt($(currentTab + '#msYtlCount').text());
                        var currCount = respData.hits.length;
                        if (oldTotal == NaN || oldTotal == undefined || isNaN(oldTotal) || oldTotal == 0) {
                            oldTotal = 0;
                            data = {};
                            data.filterObj = {};
                            data.filterObj['customer'] = {};
                            data.filterObj['project'] = {};
                            data.filterObj['articleType'] = {};
                            data.filterObj['typesetter'] = {};
                            data.filterObj['publisher'] = {};
                            data.filterObj['author'] = {};
                            data.filterObj['editor'] = {};
                            data.filterObj['copyeditor'] = {};
                            data.filterObj['contentloading'] = {};
                            data.filterObj['support'] = {};
                            data.filterObj['supportstage'] = {};
                            data.filterObj['supportlevel'] = {};
                            data.filterObj['others'] = {};
                        }
                        if (param.stage == 'Support') {
                            respData.hits = respData.hits.sort(function (a, b) {
                                a = a._source.stageName.toUpperCase();
                                b = b._source.stageName.toUpperCase();
                                if ((b) < (a)) {
                                    return 1;
                                } else {
                                    return -1;
                                }
                            })
                            //Array.prototype.push.apply(supportData, respData.hits);
                        } else {
                            //Array.prototype.push.apply(manuScriptCardData, respData.hits);
                        }
                        var date = new Date();
                        var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                        var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                        if (param.fromDate) param.fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
                        if (param.toDate) param.toDate = dateFormat(lastDay, 'yyyy-mm-dd');
                        if (!lastMonthList || lastMonthList.length == 0) {
                            var dataToPost = {
                                customer: param.customer,
                                urlToPost: "getDailyArticleReport",
                                stageName: assignStageName,
                                version: "v2.0",
                                mainDataXpath: "$..hits",
                                time_zone: "+05:30",
                                fromDate: param.fromDate,
                                toDate: param.toDate
                            }
                            lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
                        }

                        if (!overAllAverage || overAllAverage == undefined || Object.keys(overAllAverage).length == 0) {
                            //console.log("Overall average calling")
                            overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(lastMonthList.hits, param.stage, dataToPost);
                        }
                        data.dconfig = dashboardConfig;
                        var totalMsCount = respData.total;
                        data.info = respData.hits;
                        data.disableFasttrack = disableFasttrack;
                        data.removeEditIcon = false;
                        if (param.stage != 'Support' && roleType.match(/copyeditor/g) && accessLevel.match(/vendor/g)) {
                            // To disable the action buttons in card for CE vendor
                            data.ceVendor = true;
                            if (param.urlToPost.match(/getUnassigned/g)) {
                                data.removeEditIcon = true;
                            }
                        }
                        data.userDet = JSON.parse($('#userDetails').attr('data'));
                        data.count = oldTotal;
                        data.overAllAverage = overAllAverage;
                        if (param && param.stage != 'Support') {
                            var userRole = userData.kuser.kuroles[param.customer]['role-type'];
                            var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
                            // For Copyeditor -> Vendor In unassigned Bucket the first card should be pickable and other cards are to be in disable mode.
                            if (param && param.urlToPost && param.urlToPost.match(/getUnassigned/g) && cardDisable && cardDisable[userRole] && cardDisable[userRole][userAccess] && cardDisable[userRole][userAccess]['addClassName']) {
                                data.disable = cardDisable[userRole][userAccess]['addClassName'];
                            }
                        }
                        $(currentTab + '#manuscriptsDataContent').html('');
                        manuscriptsData = $(currentTab + '#manuscriptsDataContent');
                        var pagefn = doT.template(document.getElementById('unAssignedTemplate').innerHTML, undefined, undefined);
                        manuscriptsData.append(pagefn(data));
                        // $(currentTab  +'#manuscriptsData').height(window.innerHeight - $(currentTab  +'#manuscriptsData').position().top - $('#footerContainer').height());
                        $(currentTab + '.windowHeight').height(window.innerHeight - ($(currentTab + '.layoutView').offset().top + 50) - $('#footerContainer').height());
                        $(currentTab + '.windowHeight').css('overflow', 'auto');
                        $(currentTab + '.msCount.loading.hidden').removeClass('hidden');
                        if ((currCount + oldTotal) < totalMsCount) {
                            $(currentTab + '.msCount').addClass('loading').removeClass('loaded');
                            $(currentTab + '.la-container').css('display', 'none');
                            $(currentTab + '#msYtlCount').text(data.info.length + oldTotal);
                            $(currentTab + '#msCount').text(totalMsCount);
                            $(currentTab + '#msYtlCount').removeClass('hidden');
                            param.from = currCount + oldTotal;
                            setTimeout(function () {
                                eventHandler.mainMenu.toggle.showUnassigned(param, targetNode, 1);
                            }, 0)
                        } else {
                            $(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
                            $(currentTab + '#msCount').text(totalMsCount);
                            param.filterObj = data.filterObj;
                            if (!param.customer || param.customer == 'first') {
                                //eventHandler.filters.populateNavigater(param);
                            } else {
                                //eventHandler.filters.populateNavigater(param);
                            }
                            //eventHandler.filters.populateFilters(data.filterObj, param.stage);
                            // To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
                            if (!param.customer.match(/customer|first/gi)) {
                                var userRole = userData.kuser.kuroles[param.customer]['role-type'];
                                var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
                                if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
                                    //eventHandler.filters.populateSort();
                                }
                            }
                        }
                        var activeStage = $('#projectStageList').find('.active').html()
				        $('#autoAssignContent #projectStageListVal').html(activeStage);
                        eventHandler.mainMenu.toggle.paging();
                        eventHandler.components.actionitems.showUserList(param);
                        $('.la-container').css('display', 'none');
                    },
                    error: function (respData) {
                        $('.la-container').css('display', 'none');
                    }
                });
            }
        },
        showAssigned: function (targetNode) {
            if (targetNode.value) var selectedUserName = targetNode.value
            else {
                selectedUserName = $(currentTab + '.users .assign-users option')[0].text
            }
            $('.progressassign').removeClass('hidden')
            if($('#userList').find(":selected").text() != 'Select the User '){
            $("#autoAssignContent #manuscriptsDataContent, #autoAssignContent #assignedManuscript").sortable({
                    connectWith: ".connectedSortable",
                    cancel : ".progressassign,#pagination",
                    change: function( event, ui ) {
                        var doi = $(ui.item).attr('id');
                        var customer = $(ui.item).attr('data-customer')
                        var project = $(ui.item).attr('data-project')
                        var stageName = $(ui.item).attr('data-stagename')
                        var ajaxUrl = "/api/assignuser";
                        if($(ui.placeholder).offsetParent().hasClass('unassignedArticles')){
                           var userNameAssign = ""
                        }else{
                            userNameAssign = $('#userList').find(':selected').attr('value');
                        }
                        var firstName = userData.kuser.kuname.first
                        var lastName = userData.kuser.kuname.last
                        var email = userData.kuser.kuemail
                        var assignedBy = firstName + ' ' + lastName + ' ' + email

                        var updateParameter = {
                            "email": "",
                            "customer": customer,
                            "project": project,
                            "assign": (userNameAssign == " ") ? false : true,
                            "doi": doi,
                            "assignedBy": assignedBy,
                            "assignTo": (userNameAssign == " ") ? "" : userNameAssign
                        }
                        var parameters = {
                            "type": 'mergeData',
                            "data[process]": 'update',
                            "data[xpath]": '//workflow/stage[name[.="' + stageName + '"] and status[.="in-progress"]]',
                            "data[content]": '<stage><planned-end-date>'+ dateToAssign  +'</planned-end-date></stage>',
                            "customer": customer,
                            "project": project,
                            "doi": doi
                        }
                        var param = {
                            'notify': {
                                'notifyTitle': 'Assign User',
                                'notifyText': (userNameAssign == "") ? 'User unassigned successfully for {doi}' : 'User assigned successfully for {doi}',
                                'notifyType': 'success'
                            }
                        }
                        if(ui.sender){
                        jQuery.ajax({
                            type: "POST",
                            url: ajaxUrl,
                            data: updateParameter,
                            dataType: "json",
                            success: function (message) {
                                if (message.content || message.status) {
                                    eventHandler.common.functions.sendAPIRequest('updatedata', parameters, "POST")
                                    param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, parameters.doi);
                                    eventHandler.common.functions.displayNotification(param);
                                    $('#pagin').html('')
                                    eventHandler.mainMenu.toggle.paging();
                                }
                            },
                            error: function (e) {
                                if (e && e.responseText != undefined && e.responseText && e.responseText.match(/status|message/g)) {
                                    e.responseText = JSON.parse(e.responseText);
                                    var respText = JSONPath({ json: e.responseText, path: '$..message' });
                                    e.responseText = (respText && respText[1]) ? respText[1] : ' ';
                                }

                                if(e) {
                                    if (name == " " && e && e.responseText != undefined) {
                                        param.notify.notifyText = e.responseText.replace(/assigning/g, 'unassigning');
                                    }
                                    param.notify.notifyText = 'Error While Assigning ' + parameters.doi;
                                    if (e && e.responseText != undefined && !e.responseText.match(/<html>/g)) {
                                        param.notify.notifyText = e.responseText.replace(/{doi}/g, parameters.doi);
                                    }
                                    param.notify.notifyType = 'error';
                                    if (e && e.responseText && typeof (e.responseText) == 'string' && e.responseText.match(/Force log-out/g)) {
                                        var xmlData = '<stage><name>' + stageName + '</name><status>in-progress</status><job-logs><log><status type="system">logged-off</status></log></job-logs></stage>'
                                        param.notify.notifyType = 'warning';
                                        param.notify.doi = parameters.doi;
                                        param.notify.functionToCall = "updateXpath"
                                        param.parameter = {
                                            doi: parameters.doi,
                                            customer: parameters.customer,
                                            project: parameters.project,
                                            functionToCall: "updateXpath",
                                            type: 'mergeData',
                                            from : 'assignment',
                                            stageName : stageName,
                                            userNameAssign : userNameAssign,
                                            data: {
                                                'process': 'update',
                                                'xpath': '//workflow/stage[name[.="' + stageName + '"] and status[.="in-progress"]]',
                                                'content': '<stage><job-logs><log><status type="system">logged-off</status></log></job-logs></stage>'
                                            },
                                            parentId: '#manuscriptsDataContent',
                                            name: (name == " ") ? " " : name,
                                            sourceID: 'data-id'
                                        }
                                        param.notify.confirmNotify = true;
                                    }
                                    eventHandler.common.functions.displayNotification(param);
                                }
                                $('.la-container').css('display', 'none');

                            }
                        })
                    }
                    },
                    stop:function( event, ui ) {
                        $(ui.item).css({'z-index':'','position':'','top':'','left':''})
                    }
            }).disableSelection();
        }

            var dataToPost = {
                customer: assignedArticles.customerName,
                urlToPost: "getAssignedArticles",
                project: assignedArticles.projectName,
                workflowStatus: "in-progress",
                userName: selectedUserName,
                version: "v2.0",
                mainDataXpath: "$..hits",
                from: 0,
                fromDate:dateToAssign,
                toDate:dateToAssign,
                size: 500
            }
            $.ajax({
                type: "POST",
                url: '/api/getarticlelist',
                data: dataToPost,
                dataType: 'json',
                success: function (respData) {
                    var param = assignedArticles
                    if (!respData || respData.length == 0) {
                        $('#assignedManuscript li').remove()
                        $('#manuscriptsData .filterOptions').hide();
                        $('div.navFilter').empty();
                        if (selectedCustomer) {
                            param = {};
                            param.customer = selectedCustomer;
                            eventHandler.filters.populateNavigater(param);
                        }
                        $(currentTab + '#msYtlCount, ' + currentTab + '#msCount').text('0');
                        $('.la-container').css('display', 'none');
                        $(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
                        return;
                    }
                    var oldTotal = parseInt($(currentTab + '#msYtlCount').text());
                    var currCount = respData.hits.length;
                    if (oldTotal == NaN || oldTotal == undefined || isNaN(oldTotal) || oldTotal == 0) {
                        oldTotal = 0;
                        data = {};
                        data.filterObj = {};
                        data.filterObj['customer'] = {};
                        data.filterObj['project'] = {};
                        data.filterObj['articleType'] = {};
                        data.filterObj['typesetter'] = {};
                        data.filterObj['publisher'] = {};
                        data.filterObj['author'] = {};
                        data.filterObj['editor'] = {};
                        data.filterObj['copyeditor'] = {};
                        data.filterObj['contentloading'] = {};
                        data.filterObj['support'] = {};
                        data.filterObj['supportstage'] = {};
                        data.filterObj['supportlevel'] = {};
                        data.filterObj['others'] = {};
                    }

                    if (param.stage == 'Support') {
                        respData.hits = respData.hits.sort(function (a, b) {
                            a = a._source.stageName.toUpperCase();
                            b = b._source.stageName.toUpperCase();
                            if ((b) < (a)) {
                                return 1;
                            } else {
                                return -1;
                            }
                        })
                        //Array.prototype.push.apply(supportData, respData.hits);
                    } else {
                        //Array.prototype.push.apply(manuScriptCardData, respData.hits);
                    }

                    var date = new Date();
                    var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                    var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                    if (!param.fromDate) param.fromDate = dateFormat(firstDay, 'yyyy-mm-dd');
                    if (!param.toDate) param.toDate = dateFormat(lastDay, 'yyyy-mm-dd');
                    if (!lastMonthList || lastMonthList.length == 0) {
                        var dataToPost = {
                            customer: param.customer,
                            urlToPost: "getDailyArticleReport",
                            stageName: assignStageName,
                            version: "v2.0",
                            mainDataXpath: "$..hits",
                            time_zone: "+05:30",
                            fromDate: param.fromDate,
                            toDate: param.toDate
                        }
                        lastMonthList = eventHandler.common.functions.sendSyncAjaxRequest('/api/getarticlelist', 'POST', dataToPost);
                    }
                    if (!overAllAverage || overAllAverage == undefined || Object.keys(overAllAverage).length == 0) {
                        //console.log("Overall average calling")
                        overAllAverage = eventHandler.mainMenu.toggle.getAverageTime(lastMonthList.hits, param.stage, dataToPost);
                    }
                    var countForCompleted =0;
                    var articleLength = respData.hits.length;
                    var util = 0;
                    var completed;
                    var utilization;
                    var finalUtil;
                    if (articleLength > 0) {
                        respData.hits.forEach(function (currData,index) {
						if (currData && currData._source && currData._source.stage && currData._source.workflowStatus) {
							if(currData._source.workflowStatus == 'completed'){
                                countForCompleted++;
                            }
                            else if(currData._source.workflowStatus == 'in-progress'){
                                data.info = respData.hits;
                            }
                            else{

                            }
                        }
                        var articleType = currData._source.articleType
                        var utilUser = currData._source.customer
                        var utilProject = currData._source.project
                        if(overAllAverage && overAllAverage["type"] && overAllAverage["type"][utilProject] && overAllAverage["type"][utilProject][articleType] && overAllAverage["type"][utilProject][articleType].average)
                        utilization = overAllAverage["type"][utilProject][articleType].average
                        else
                        utilization = 60;
                        util = util + utilization ;
                    });
                    if(countForCompleted>0)
                    {
                         completed = Math.floor((countForCompleted/articleLength)*100) + '%';
                    }
                    if(util>0)
                    {
                         finalUtil = Math.floor(util/480*100) + '%'
                    
            
                    }
                }
                else{
                        completed = 0 + '%';
                        finalUtil = 0 + '%';
                }
                if(!completed)
                completed = 0 + '%';
                if(!finalUtil)
                finalUtil = 0 + '%';
                $('.progressassign .completed').css('width',completed)
                $('.progressassign .util strong').html('')
                $('.progressassign .util strong').html(finalUtil)
                $('.progressassign .complete strong').html('')
                $('.progressassign .complete strong').html(completed);
                $('.progressassign .utilization').css('width',finalUtil)
                    data.dconfig = dashboardConfig;
                    var totalMsCount = respData.total;

                    data.disableFasttrack = disableFasttrack;
                    data.removeEditIcon = false;
                    if (userData.kuser.kuroles[param.customerName] && userData.kuser.kuroles[param.customerName]['role-type'] && userData.kuser.kuroles[param.customerName]['access-level']) {
                        var roleType = userData.kuser.kuroles[param.customerName]['role-type'];
                        var accessLevel = userData.kuser.kuroles[param.customerName]['access-level'];
                    }
                    if (param.stage != 'Support' && roleType.match(/copyeditor/g) && accessLevel.match(/vendor/g)) {
                        // To disable the action buttons in card for CE vendor
                        data.ceVendor = true;
                        if (param.urlToPost.match(/getUnassigned/g)) {
                            data.removeEditIcon = true;
                        }
                    }
                    data.userDet = JSON.parse($('#userDetails').attr('data'));
                    data.count = oldTotal;
                    data.overAllAverage = overAllAverage;
                    if (param && param.stage != 'Support') {
                        var userRole = userData.kuser.kuroles[param.customerName]['role-type'];
                        var userAccess = userData.kuser.kuroles[param.customerName]['access-level'];
                        // For Copyeditor -> Vendor In unassigned Bucket the first card should be pickable and other cards are to be in disable mode.
                        if (param && param.urlToPost && param.urlToPost.match(/getUnassigned/g) && cardDisable && cardDisable[userRole] && cardDisable[userRole][userAccess] && cardDisable[userRole][userAccess]['addClassName']) {
                            data.disable = cardDisable[userRole][userAccess]['addClassName'];
                        }
                    }
                    $(currentTab + '#assignedManuscript li').remove()
                    centerPanelContainer = $(currentTab + '#assignedManuscript');
                    var pagefn = doT.template(document.getElementById('AssignedTemplate').innerHTML, undefined, undefined);
                    centerPanelContainer.append(pagefn(data));
                    respData.hits.forEach(function (currData,index) {
						if (currData._source.workflowStatus && currData._source.workflowStatus == 'completed') {
                            $('#assignedManuscript li[id='+currData._id+']').css('display','none')
                        }
                    })
                    // $(currentTab  +'#manuscriptsData').height(window.innerHeight - $(currentTab  +'#manuscriptsData').position().top - $('#footerContainer').height());
                    $(currentTab + '.windowHeight').height(window.innerHeight - ($(currentTab + '.layoutView').offset().top + 50) - $('#footerContainer').height());
                    $(currentTab + '.windowHeight').css('overflow', 'auto');
                    $(currentTab + '.msCount.loading.hidden').removeClass('hidden');
                    if ((currCount + oldTotal) < totalMsCount) {
                        $(currentTab + '.msCount').addClass('loading').removeClass('loaded');
                        $(currentTab + '.la-container').css('display', 'none');
                        $(currentTab + '#msYtlCount').text(data.info.length + oldTotal);
                        $(currentTab + '#msCount').text(totalMsCount);
                        $(currentTab + '#msYtlCount').removeClass('hidden');
                        param.from = currCount + oldTotal;
                        setTimeout(function () {
                            eventHandler.mainMenu.toggle.showUnassigned(param, targetNode, 1);
                        }, 0)
                    } else {
                        $(currentTab + '.msCount.loading').removeClass('loading').addClass('loaded');
                        $(currentTab + '#msCount').text(totalMsCount);
                        param.filterObj = data.filterObj;
                        if (!param.customer || param.customer == 'first') {
                            //eventHandler.filters.populateNavigater(param);
                        } else {
                            //eventHandler.filters.populateNavigater(param);
                        }
                        //eventHandler.filters.populateFilters(data.filterObj, param.stage);
                        // To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
                        if (!param.customerName.match(/customer|first/gi)) {
                            var userRole = userData.kuser.kuroles[param.customerName]['role-type'];
                            var userAccess = userData.kuser.kuroles[param.customerName]['access-level'];
                            if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
                                //eventHandler.filters.populateSort();
                            }
                        }
                    }
                    $('.la-container').css('display', 'none');
                },
                error: function (respData) {
                    $('.la-container').css('display', 'none');
                }
            })


        },
        assignUser:function(param)
        {
            var firstName = userData.kuser.kuname.first
            var lastName = userData.kuser.kuname.last
            var email = userData.kuser.kuemail
            var assignedBy = firstName + ' ' + lastName + ' ' + email
            var updateParameter = {
                "email": "",
                "customer": param.customer,
                "project": param.project,
                "assign": "",
                "doi": param.doi,
                "assignedBy": assignedBy,
                "assignTo": " "
            }
            var parameters = {
                "type": 'mergeData',
                "data[process]": 'update',
                "data[xpath]": '//workflow/stage[name[.="' + param.stageName + '"] and status[.="in-progress"]]',
                "data[content]": '<stage><planned-end-date>'+ dateToAssign  +'</planned-end-date></stage>',
                "customer": param.customer,
                "project": param.project,
                "doi": param.doi
            }
            var param = {
                'notify': {
                    'notifyTitle': 'Assign User',
                    'notifyText': (param.userNameAssign == "") ? 'User unassigned successfully for {doi}' : 'User assigned successfully for {doi}',
                    'notifyType': 'success'
                }
            }
            var ajaxUrl = "/api/assignuser";
            jQuery.ajax({
                type: "POST",
                url: ajaxUrl,
                data: updateParameter,
                dataType: "json",
                success: function (message) {
                    if (message.content || message.status) {
                        eventHandler.common.functions.sendAPIRequest('updatedata', parameters, "POST")
                        param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, parameters.doi);
                        eventHandler.common.functions.displayNotification(param);
                    }
                },
                error: function (e) {
                    if (e && e.responseText != undefined && e.responseText && e.responseText.match(/status|message/g)) {
                        e.responseText = JSON.parse(e.responseText);
                        var respText = JSONPath({ json: e.responseText, path: '$..message' });
                        e.responseText = (respText && respText[1]) ? respText[1] : ' ';
                    }

                    if(e) {
                        if (name == " " && e && e.responseText != undefined) {
                            param.notify.notifyText = e.responseText.replace(/assigning/g, 'unassigning');
                        }
                        param.notify.notifyText = 'Error While Assigning ' + parameters.doi;
                        if (e && e.responseText != undefined && !e.responseText.match(/<html>/g)) {
                            param.notify.notifyText = e.responseText.replace(/{doi}/g, parameters.doi);
                        }
                        param.notify.notifyType = 'error';
                        eventHandler.common.functions.displayNotification(param);
                    }
                    $('.la-container').css('display', 'none');

                }
            })

        },

    }
})(eventHandler || {});
