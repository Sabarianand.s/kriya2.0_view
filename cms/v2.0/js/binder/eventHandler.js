/**
* eventHandler - this javascript holds all the functions required for Reference handling
*					so that the functionalities can be turned on and off by just calling the required functions
*					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['explorer', 'tabs', 'toc', 'pdf'];
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel',
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);

			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}

								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};

    eventHandler.explorer = {
        toggle: {
            panel: function(param, targetNode){
                $('.epToggle').css('transform', 'scale(1)');
                if ($('#tocContainer').hasClass('fullWidth')){
                    $("#tocContainer").animate({
                        width: '73%'
                     }, { duration: 500, queue: false })
                     .toggleClass('fullWidth');

                     $("#explorerContainer").animate({
                        width: 'toggle'
                     }, { duration: 500, queue: false });
                }
                else{
                    $("#tocContainer").animate({ width: '98%' }, { duration: 500, queue: false }).toggleClass('fullWidth');
                    $("#explorerContainer").animate({
                        width: 'toggle'
                     }, { duration: 500, queue: false });
                }
                console.log('toggle explorer clicked');
            }
        },
        customer: {
            toggle: function(param, targetNode){
                if ($(targetNode).hasClass('customer')) {
					//$('#newIssue').addClass('hide');
                    getProjectList($(targetNode).attr('data-customer'))
                    .catch(function(err){
                        console.log('error getProjectList')
                    })
                }
                else if ($(targetNode).hasClass('project')) {
					//addNewIssue($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'))
                    getIssueList($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'))
                    .catch(function(err){
                        console.log('error getIssueList')
                    })
                }
            },
            list: function(param, targetNode){
				//$('#newIssue').addClass('hide');
                getCustomerList()
                .catch(function(err){
                    console.log('error getIssueData')
                })
            },
            load: function(param, targetNode){
                $(targetNode).parent().find('.list').removeClass('active');
                $(targetNode).addClass('active');
                getIssueData($(targetNode).attr('data-customer'), $(targetNode).attr('data-project'), $(targetNode).attr('data-file'))
                .catch(function(err){
                    console.log('error getIssueData')
                })
			},
			reload: function(param, targetNode){
				var fileName = $('.list.folder.active').attr('data-file');
               	getUpdatedAricle(fileName)
				.catch(function(err){
                    console.log('error getIssueData')
                })
			},
			generateIssueForm: function(param, targetNode){
				var form = new PNotify({
				text:'<div id="issueForm" class="issueForm"><div class="issueFormUpload"><div class="upload_input"><input class="upload_file" type="file" name="files[]" id="file"/></div><input class="issue_upload pf-button btn btn-small" type="button" name="issue_Upload" value="Upload"/><input class="issue_cancel pf-button btn btn-small" type="button" name="cancel" value="Cancel"/></div></div>',
					icon: false,
					width: 'auto',
					hide: false,
					buttons: {
						closer: false,
						sticker: false
					},
					addclass: 'stack-modal',
					stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
					insert_brs: false
				});
				form.get().find('.issueFormUpload').on('click', '[name=cancel]', function() {
					form.remove();
				})
				form.get().find('.issueFormUpload').on('click', '[name=issue_Upload]', function() {
				var file = $('.issueFormUpload').find('input[class=upload_file]')
					var fileName = file.name
					var fileContents = file[0].files[0];
					if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();
                        reader.onload = function (e) {
							//var xmlDoc = $.parseXML(e.target.result);
							saveXML('issueXML',e.target.result)
							form.remove();
                        }
                        reader.readAsText(file[0].files[0]);
                    }
				});
			}
        },
        data: {
            filter: function(param, targetNode){
                var input, filter, ul, li, a, i;
				filter = targetNode.val();
				var filterPath = targetNode.attr('data-filter-path');
				$(filterPath).removeClass('hide');
                // Declare variables
                if(filter.length>0){
                    $.expr[':'].containsIgnoreCase = function (n, i, m) {
                            return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) < 0;
                        };
					$(filterPath + ':containsIgnoreCase("' + filter + '")').addClass('hide');
                }
            }
        }
    },
    eventHandler.tabs = {
        toggle: {
            panel: function(param, targetNode){
                $(targetNode).parent().find('.tabHead').removeClass('active');
                var tabID = $(targetNode).addClass('active').attr('data-href');
                $('#' + tabID).parent().find('.tabContent').addClass('hide');
                $('#' + tabID).removeClass('hide');
            }
        }
    },
    eventHandler.toc = {
        toggle: {
            section: function(param, targetNode){
                if ($(targetNode).hasClass('sectionData')){
					$('.sectionDataChild.active').removeClass('active');
					$('.sectionData.active').removeClass('active').find('.openclose').toggleClass('icon-arrow_drop_down').toggleClass('icon-arrow_drop_up');
                    if ($('[data-sec="' + $(targetNode).attr('id') + '"]:visible').length){
						$('[data-sec]').addClass('hide');
                        $('[data-sec="' + $(targetNode).attr('id') + '"]').addClass('hide');
                    }
                    else{
						$(targetNode).addClass('active').find('.openclose').toggleClass('icon-arrow_drop_down').toggleClass('icon-arrow_drop_up');
						$('[data-sec]').addClass('hide');
                        $('[data-sec="' + $(targetNode).attr('id') + '"]').removeClass('hide');
                    }
                }
                else if ($(targetNode).hasClass('icon-fullscreen')){
					$('.sectionData').addClass('active').find('.openclose').addClass('icon-arrow_drop_down').removeClass('icon-arrow_drop_up');
                    $('[data-sec]').removeClass('hide');
                    $(targetNode).removeClass('icon-fullscreen').addClass('icon-fullscreen_exit');
                }
                else if ($(targetNode).hasClass('icon-fullscreen_exit')){
					$('.sectionData').removeClass('active').find('.openclose').addClass('icon-arrow_drop_up').removeClass('icon-arrow_drop_down');
                    $('[data-sec]').addClass('hide');
                    $(targetNode).removeClass('icon-fullscreen_exit').addClass('icon-fullscreen');
                }
            },
            panel: function(param, targetNode){
				$('#metaHistory, #unassignedArticles').addClass('hide');
				$('#manuscriptData').removeClass('hide');
				if ($(targetNode).attr('data-c-id')){
					$('#msDataDiv').attr('data-c-rid', $(targetNode).attr('data-c-id'));
				}
				$('.sectionDataChild.active').removeClass('active');
				$(targetNode).addClass('active');
				var dataType = $(targetNode).attr('data-type');
				if ((dataType == 'advert') || (dataType == 'advert_unpaginated') || (dataType == 'filler')){
					$('#manuscriptData').addClass('hide');
					$('#advertsData').removeClass('hide');
					populateData($('#adDataDiv'), $(targetNode).attr('data-type'), $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')]);
				}
				else{
					populateData($('#msDataDiv'), $(targetNode).attr('data-type'), $('#tocContainer').data('binder')['container-xml'].contents.content[$(targetNode).attr('data-c-id')]);
				}
            }
        }
	}
	eventHandler.upload = {
		uploadFile: function(param, targetNode){
			var thisObj = this;
			var file = param.file;
			$.ajax({
				url: '/getS3UploadCredentials',
				type: 'GET',
				data: {'filename': file.name},
				success: function(response){
					if (response && response.upload_url){
						var formData = new FormData();
						$.each(response.params, function(k, v){
							formData.append(k, v);
						})
						formData.append('file', file, file.name);
						param.response = response;
						param.formData = formData;
						thisObj.uploadToServer(param, targetNode);
					}
				},
				error: function(response){
					if(param && typeof(param.error) == "function"){
						param.error(response);
					}
				}
			});
		},
		uploadToServer: function(param, targetNode){
			var file = param.file;
			var formData = param.formData;
			var response = param.response;
			$.ajax({
				url: response.upload_url,
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if(param && typeof(param.progress) == "function"){
							param.progress(evt);
						}
					}, false);
					xhr.addEventListener("progress", function (evt) {
						if(param && typeof(param.progress) == "function"){
							param.progress(evt);
						}
					}, false);
					return xhr;
				},
				success: function(res){
					if (res && res.hasChildNodes()){
						var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
						if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
							var ext = file.name.replace(/^.*\./, '')
							var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
							var customer = $('#listIssue').attr('data-customer');
							var project = $('#listIssue').attr('data-project');
							var volume = $('#tocContainer').data('binder')['container-xml'].meta.volume._text
							var issue = $('#tocContainer').data('binder')['container-xml'].meta.issue._text
							var params = {
								srcBucket: uploadResult.find('Bucket,bucket').text(),
								srcKey: uploadResult.find('Key,key').text(),
								dstBucket: "kriya-resources-bucket",
								dstKey: customer + '/' + project + '/' + volume + '_' + issue + '/resources/' + fileName,
								convert : 'false'
							}
							$.ajax({
								async: true,
								crossDomain: true,
								url: response.apiURL,
								method: "POST",
								headers: {
									accept: "application/json"
								},
								data: JSON.stringify(params),
								success: function(status){
									console.log(status);
									if(param && typeof(param.success) == "function"){
										param.success(status);
									}
									//var fileDetailsObj = status.fileDetailsObj;
									//$(targetNode).attr('src', '/resources/' + fileDetailsObj.dstKey);
								},
								error: function(err){
									if(param && typeof(param.error) == "function"){
										param.error(err);
									}
								}
							});
						}
					}
				},
				error: function(response){
					if(param && typeof(param.error) == "function"){
						param.error(response);
					}
				}
			});
		}
	}
	
	eventHandler.pdf = {
		export:{
			exportToPDF: function(param, targetNode){
				var metaObj = $('#tocContainer').data('binder')['container-xml'].meta;
				var cid     = $('#manuscriptData #msDataDiv').attr('data-c-rid');
				if(param && param.cid){
					cid = param.cid;
				}
				var articleObj = $('[data-c-id="' + cid + '"]').data('data');

				if(articleObj['_attributes']['type'] == "blank"){
					if(param && param.error && typeof(param.error) == "function"){
						param.error();
					}
					return false;
				}

				var doi      = articleObj['_attributes']['id'];
				var ms_id = articleObj['_attributes']['ms-id'] ? articleObj['_attributes']['ms-id'] : articleObj['_attributes']['id']
				var customer = $('.list.folder.active').attr('data-customer');
				var project  = $('.list.folder.active').attr('data-project');
				var fpage    = articleObj['_attributes']['fpage'];
				var lpage    = articleObj['_attributes']['lpage'];

				//var userName = 'kriyaUser';
				var userName = $('.userinfo:first').attr('data-name');
				var userRole = 'typesetter';

				if(param && param.doi){
					doi = param.doi;
				}
				var proofType = 'print';
				var processType = 'InDesignSetter';
				if (param && param.type) proofType = param.type;
				if (param && param.processType) processType = param.processType;
				parameters = {
					"client"     : customer,
					"project"    : project, 
					"idType"     : "doi", 
					"id"         : doi, 
					"msid"      : ms_id,
					//"processType": "InDesignSetter", 
					"processType": processType, 
					"proof"      : proofType, 
					"volume"     : metaObj.volume._text, 
					"issue"      : metaObj.issue._text, 
					"fpage"      : fpage,
					"lpage"      : lpage,					
					"userName"   : userName, 
					"userRole"   : userRole
				}
				var colorObj  = articleObj['color']
				
				var dataColor = '';
				if(colorObj){
					if (colorObj.constructor.toString().indexOf("Array") == -1) {
						colorObj = [colorObj];
					}
					colorObj.forEach(function (color) {
                    	if(color['_attributes']['color'] == '1'){
                    		dataColor += color['_attributes']['ref-id']+',';
                    	}
                	})
				}

				dataColor = dataColor.replace(/\,$/, '');
				if(dataColor){
					parameters['data-color'] = dataColor;
				}
				
				var title = '';
				if(doi){
					title = 'Proofing ' + doi;
					parameters.issueMakeup = 'true';
				}
				if(articleObj['_attributes']['type'] == 'cover'){
					parameters.pheripherals = 'cover';
					parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
					title = 'Proofing Cover';
					parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_cover';
				}else if(articleObj['_attributes']['type'] == 'table-of-contents'){
					parameters.pheripherals = 'toc';
					parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
					parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_toc';
					title = 'Proofing Toc';
				}
				else if(articleObj['_attributes']['type'] == 'advert'  || articleObj['_attributes']['type'] == 'advert_unpaginated'){
					var currRegion = targetNode.closest('div').parent().attr('data-region')
					parameters.pheripherals = 'advert';
					parameters.advertID = doi;
					parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
					title = 'Proofing Advert_' + doi + '_' + currRegion;
					parameters.region = currRegion;
					parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
				}
				if(processType == 'PDFPreflight'){
					parameters.doi = parameters.doi ? parameters.doi : doi
					title = 'Preflight for '+ parameters.doi;
					parameters.pdfLink = param.pdfLink
					parameters.profile = param.profile;
					var type = articleObj['_attributes']['type']; 
				//if type is manuscript and exist dataColor change profile to cmyk
					if (type == 'manuscript' && dataColor && param.profile == 'bw'){
						parameters.profile = 'cmyk'
					}
					else if (type == 'manuscript' && dataColor && param.profile == 'bw+1'){
						parameters.profile = 'cmyk+1'
					}

				}
				
				var notifyObj = new PNotify({
					title : title,
                    text: '<div class="progress"><div class="determinate" style="width: 0%"></div><span class="progress-text">Sending request..</span></div>',
                    hide: false,
                    type: 'success',
                    buttons: { sticker: false },
                    addclass: 'export-pdf',
                    icon: false
                });
				var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
				var notifyContainer = notifyObj.container[0];
				$(notifyContainer).attr('data-c-nid', cid);
				
				$.ajax({
					type: 'POST',
					url: "/api/pagination",
					data: parameters,
					crossDomain: true,
					success: function(data){
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
							eventHandler.pdf.export.getJobStatus(customer, data.message.jobid, notifyObj, userName, userRole, doi, param);
						}
						else if(data == ''){
							notifyText.innerHTML = 'Failed..';
							if(param && param.error && typeof(param.error) == "function"){
								param.error();
							}
						}else{
							notifyText.innerHTML = data.status.message;
							if(param && param.error && typeof(param.error) == "function"){
								param.error();
							}
						}
					},
					error: function(error){
						notifyText.innerHTML = 'Failed..';
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
					}
				});
			},
			getJobStatus: function(customer, jobID, notifyObj, userName, userRole, doi, param){
				var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
				var progressBar = $(notifyObj.text_container[0]).find('.determinate');
				var notifyTitle = notifyObj.title_container[0];
				var notifyContainer = notifyObj.container[0];

				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.status){
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if(data.status.message.stage.current){
								currStep = data.status.message.stage.current;
							}
							if(data.status.message.progress){
								var barWidth = data.status.message.progress/3;
								if(data.status.message.stage.current == "collectfiles"){
									barWidth = barWidth+'%';
								}else if(data.status.message.stage.current == "proofing"){
									barWidth = (barWidth+33.3)+'%';
								}else if(data.status.message.stage.current == "uploadPDF"){
									barWidth = (barWidth+66.6)+'%';
								}

								progressBar.css('width', barWidth);
							}
							var loglen = data.status.message.log.length;    
							var process = data.status.message.log[loglen-1];
							if (/completed/i.test(status)){
								var loglen = data.status.message.log.length;	
								var process = data.status.message.log[loglen-1];
								var processType = param.processType;
								//notifyText.innerHTML = process;
								if(processType == 'PDFPreflight'){
									var preflightLink = param.pdfLink;
									preflightLink = preflightLink.replace('.pdf','_preflight.pdf')
									
									//display preflight link to user
									notifyTitle.innerHTML = preflightLink;
									notifyObj.remove();
									var nid = $(notifyContainer).attr('data-c-nid');
									
									//set preflight flag as true
									var dataObj = $('[data-c-id="' + nid + '"]').data('data');
										dataObj['_attributes']['data-preflight'] = 'true';
									$('[data-c-id="' + nid + '"]').attr('data-preflight','true');
									
									var fileFlag = false;
									// save preflight pdf path in xml
									//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
									if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated' ) ){
										var currRegion = targetNode.closest('div').parent().attr('data-region')
										currRegion = currRegion.toLowerCase();
										var fileFlag = false;
										var regObj = dataObj['region'];
										if(regObj){
											if (regObj.constructor.toString().indexOf("Array") == -1) {
												regObj = [regObj];
											}
											regObj.forEach(function (region) {
												if(region['_attributes']['type'] == currRegion){
													//region['_attributes']['path'] = pdfLink;
													var fileObj = region['file'];
													if (fileObj.constructor.toString().indexOf("Array") == -1) {
														fileObj = [fileObj];
													}
													fileObj.forEach(function (file) {
														if(file['_attributes'] &&  file['_attributes']['type'] && file['_attributes']['type'] == 'preflightPdf'){
															file['_attributes']['path'] = preflightLink;
															fileFlag = true;
														}
													})
													if(fileFlag == false)	{
														var file = ({"_attributes":{"type": "preflightPdf","path": preflightLink}})
														fileObj.push(file)
														region['file'] = fileObj;
													}
												}
											})
											saveData();
											// save the preflight link in li to download
											targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('target', '_blank');
											targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('href', preflightLink)
										}
									}
									else if(dataObj['file']){
										var fileObj = dataObj['file'];
										if (fileObj.constructor.toString().indexOf("Array") == -1) {
											fileObj = [fileObj];
										}
										fileObj.forEach(function (file) {
											var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
												if(pdfType && pdfType == 'preflightPdf'){
													file['_attributes']['path'] = preflightLink;
													fileFlag = true;
												}
										
											})
											if(fileFlag == false)	{
												var file = ({"_attributes":{"type": "preflightPdf", "path": preflightLink}})
												fileObj.push(file)
												dataObj['file'] = fileObj;
											}
									}
									// save the preflight link in li to download
									$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-preflight="true"] a').attr('target', '_blank');
									$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-preflight="true"] a').attr('href', preflightLink);	
									saveData();
											
								}
								else{
									notifyTitle.innerHTML = 'PDF generation completed.';
									var pdfLink = $(process).attr('href');
									pdfLink =pdfLink.replace('http:','https:');
									// get endPage and PageCount from JobManager
									var endPage,pageCount;
									var datajson = $(process).attr('data-json');
									if(datajson){
										datajson = datajson.replace(/\'|}|{/g,'')
										datajson = datajson.split(',')
										datajson.forEach(function (data){
											data = data.replace(/"|:/g,'')
											if (/LPAGE/gi.test(data)) {
												endPage = data.replace(/lpage/gi,'')
												//console.log(endPage)
												endPage = parseInt(endPage)
											}
											if (/PAGE\_COUNT/gi.test(data)) {
												pageCount = data.replace(/PAGE\_COUNT/gi,'')
												//pageCount = parseInt(pageCount);
												pageCount =  Math.ceil(pageCount);
												//console.log(pageCount)
											}
										})
									}
									var proofType = 'print';
									if (param && param.type) proofType = param.type;
								
									if(pdfLink && processType == 'InDesignSetter'){
										
										// if prooftype is print update the link in object , update download url 
										if(proofType == 'print') {
											$('#manuscriptData #msDataDiv object:visible').attr('data', pdfLink);
											$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-proofType="print"] a').attr('target', '_blank');
											$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-proofType="print"] a').attr('href', pdfLink);
										}
										else{
											//update download url
											$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-proofType="online"] a').attr('target', '_blank');
											$('#manuscriptData #msDataDiv object:visible').prev().find('ul li[data-proofType="online"] a').attr('href', pdfLink);
										}
										notifyObj.remove();
										var nid = $(notifyContainer).attr('data-c-nid');
										if(nid){
											var dataObj = $('[data-c-id="' + nid + '"]').data('data');
											var currentDoi = dataObj._attributes.id;
											//loop through each file and update pdflink in path if proofType matches 
											if(dataObj['file']){
												var fileObj = dataObj['file'];
												if (fileObj.constructor.toString().indexOf("Array") == -1) {
													fileObj = [fileObj];
												}
												fileObj.forEach(function (file) {
													var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
													var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
														if(currProofType == proofType && pdfType != 'preflightPdf'){
															file['_attributes']['path'] = pdfLink;
														}
												
													})
												//if cuurent artcile has run-on articles - update link in all run-on articles
													if (currentDoi && $('[data-c-id][data-runon-with="'+ currentDoi +'"]').length > 0) {
														var followOnAricles = $('[data-c-id][data-runon-with='+ currentDoi +']')
														followOnAricles.each(function(){
															var currArticle = $(this);
															var currArticleData = currArticle.data('data')
															if(currArticleData['file']){
																var fileObj = currArticleData['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																	var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																	if(currProofType == proofType && pdfType != 'preflightPdf'){
																		file['_attributes']['path'] = pdfLink;
																	}
																})
															}
														})
													}	
												//if endpage, update endpage in issue xml,  
												//update page range in UI and update the following article page number if there is change, call updatepage number function
												if(endPage){
													if ((!dataObj['_attributes']['data-run-on']) || dataObj['_attributes']['data-run-on'] != 'true'){
													//check the config and match the prooftype. if the prooftype is mentioned in config update pagenumber
													//if attrib is not available in config, update pagenumber
													//exclude pagenumber update when follow-on is 1
													if ((configData['page']) && (configData['page']._attributes['pageNumberProofType'])){
														var values =  configData['page']._attributes['pageNumberProofType'].toLowerCase()
														values = values.split(',');
														if (values.constructor.toString().indexOf("Array") < 0) {
															values = [values];
														}
														values.forEach(function (value) {
															if(value == proofType){
																dataObj['_attributes']['lpage'] = endPage;
																dataObj['_attributes']['pageExtent'] = pageCount;
																updatePageNumber(nid,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
															}
														})
													}
													else{
														dataObj['_attributes']['lpage'] = endPage;
														dataObj['_attributes']['pageExtent'] = pageCount;
														updatePageNumber(nid,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
													}
													
											 	   }
												}
												//remove data-modified attrib from xml,UI and remove title - pageModified
											if (dataObj._attributes['data-modified'] && (dataObj._attributes['data-modified'].toLowerCase() == 'true')){
												$('[data-c-id="' + nid + '"]').removeAttr('data-modified','true');
												$('[data-c-id="' + nid + '"]').find('.pageRange').removeAttr('title', 'Page is Modified');
												$('[data-c-id="' + nid + '"]').find('.pageRange').removeClass('pageModified');
												delete dataObj._attributes['data-modified'];
											}
											//set preflight flag as false
											dataObj['_attributes']['data-preflight'] = 'false';
											$('[data-c-id="' + nid + '"]').attr('data-preflight','false');
												saveData();
											}
											//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
											if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated')){
												var currRegion = targetNode.closest('div').parent().attr('data-region')
												currRegion = currRegion.toLowerCase();
												var fileFlag = false;
												var regObj = dataObj['region'];
												if(regObj){
													if (regObj.constructor.toString().indexOf("Array") == -1) {
														regObj = [regObj];
													}
													regObj.forEach(function (region) {
														if(region['_attributes']['type'] == currRegion){
															//region['_attributes']['path'] = pdfLink;
															var fileObj = region['file'];
															if (fileObj.constructor.toString().indexOf("Array") == -1) {
																fileObj = [fileObj];
															}
															fileObj.forEach(function (file) {
																if(file['_attributes'] &&  file['_attributes']['proofType'] && file['_attributes']['proofType'] == proofType){
																	file['_attributes']['path'] = pdfLink;
																	fileFlag = true;
																}
															})
															if(fileFlag == false)	{
																var file = ({"_attributes":{"type": "pdf","proofType":proofType, "path": pdfLink}})
																fileObj.push(file)
																region['file'] = fileObj;
															}
														}
													})
													saveData();
													// save the pdf link in li to download
													if(proofType == 'print') {
														targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('target', '_blank');
														targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('href', pdfLink);
													}
													else if(proofType == 'online') {
														targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('target', '_blank');
														targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('href', pdfLink);
													}
												}
											}
										}
										//	when proof type is print and processType is  InDesignSetter, send request to Preflight
										if( proofType == 'print' && param.processType == 'InDesignSetter'){
											param.processType = 'PDFPreflight';
											param.pdfLink = pdfLink;
											//get preflight profile from config
											var type = dataObj['_attributes']['type']; 
											var preflightProfile = '';
											if(configData["binder"][type] && configData["binder"][type]['_attributes'] && configData["binder"][type]['_attributes']['data-preflight-profile']){
												preflightProfile = configData["binder"][type]['_attributes']['data-preflight-profile']
											}
											if(preflightProfile){
												param.profile = preflightProfile;
												eventHandler.pdf.export.exportToPDF(param,targetNode);
											}
										}	
									}
								}
								if(param && param.success && typeof(param.success) == "function"){
									param.success();
									//
									//eventHandler.pdf.export.exportPDf(param,targetNode);
								}
							}else if (/failed/i.test(status) || code == '500'){
								notifyText.innerHTML = process;
								if(param.processType == 'InDesignSetter'){
									notifyTitle.innerHTML = 'PDF generation failed.';
								}
								else{
									notifyTitle.innerHTML = 'Preflight failed.';
								}
								if(param && param.error && typeof(param.error) == "function"){
									param.error();
								}
								//$('#'+notificationID+' .kriya-notice-body').html(process);
								//$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
							}else if (/no job found/i.test(status)){
								notifyText.innerHTML = status;
								//$('#'+notificationID+' .kriya-notice-body').html(status);
							}else{
								if(data.status.message.displayStatus){
									notifyText.innerHTML = data.status.message.displayStatus;
								}
								//$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;	
								if(loglen > 1){
									var process = data.status.message.log[loglen-1];
									if (process != '') notifyText.innerHTML = process;
									///if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								}
								setTimeout(function() {
									eventHandler.pdf.export.getJobStatus(customer, jobID, notifyObj, userName, userRole, doi, param);
									//eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
								}, 1000 );
							}
						}
						else{
							notifyText.innerHTML = 'Failed';
							//$('#'+notificationID+' .kriya-notice-body').html('Failed');
						}
					},
					error: function(error){
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
					}
				});
			},
			exportModified: function(param, index){
				index = (index)?index:0;
				var thisObj = this;
				var cid = $('#tocContainer .bodyDiv [data-modified="true"]:eq(' + index + ')').attr('data-c-id');
				var articleObj = $('[data-c-id="' + cid + '"]').data('data');

				var exportParam = {
					'cid' : cid,
					success: function(){
						$('#tocContainer .bodyDiv [data-modified="true"]:eq(' + index + ')').removeAttr('data-modified', 'true');
						delete articleObj._attributes['data-modified'];
						saveData();

						if($('#tocContainer .bodyDiv [data-modified="true"]:eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv [data-modified="true"]').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					},
					error: function(){
						if($('#tocContainer .bodyDiv [data-modified="true"]:eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv [data-modified="true"]').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					}
				}
				thisObj.exportToPDF(exportParam);
			}
		},
		merge: {
			mergePDF: function(param, targetNode, checkModify){

				if($('#tocContainer .bodyDiv .sectionDataChild .pageRange.incorrectPage').length > 0){
					showMessage({ 'text': 'Incorrect page sequence', 'type': 'error' });
					return false;
				}

				if($('#tocContainer .bodyDiv [data-modified="true"]').length > 0 && checkModify != 'false'){
					(new PNotify({
	                    title: 'Confirmation Needed',
	                    text: 'Some articles was modified do you want to proof?',
	                    hide: false,
	                    confirm: { confirm: true },
	                    buttons: { closer: false, sticker: false },
	                    history: { history: false },
	                    addclass: 'stack-modal',
	                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                	})).get()
                    .on('pnotify.confirm', function () {
                      eventHandler.pdf.export.exportModified({
                      	success: function(){
                      		eventHandler.pdf.merge.mergePDF('', targetNode, 'false');	
                      	}
                      }); 
                    })
                    .on('pnotify.cancel', function () {
                    	eventHandler.pdf.merge.mergePDF('', targetNode, 'false');
                    });
                    return false;
				}

				var pdfLinks = [];
				//$('#tocContainer [data-c-id]').each(function(){
				//epage articles to be excluded while binding 
				$('#tocContainer [data-c-id][data-grouptype !="epage"]').each(function(){	
				var data = $(this).data('data');
					/*if(data['file'] && data['file']['_attributes'] && data['file']['_attributes']['path']){
						var pdfPath = data['file']['_attributes']['path'];
						if(!pdfPath.match(/http(s)?:/)){
							pdfPath = 'http://'+ window.location.host + pdfPath;
						}else{
							pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
						}
						pdfLinks.push(pdfPath);
					}*/
					//loop through each file and update pdflink in path if proofType matches 
					if(data && data['file']){
						var fileObj = data['file'];
						if (fileObj.constructor.toString().indexOf("Array") == -1) {
							fileObj = [fileObj];
						}
						fileObj.forEach(function (file) {
							var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
								if(currProofType == 'print'){
									var pdfPath = file['_attributes']['path'];
									if(!pdfPath.match(/http(s)?:/)){
										pdfPath = 'http://'+ window.location.host + pdfPath;
									}else{
										pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
									}
									pdfLinks.push(pdfPath);
								}
						
							})
						}
						else if(data && data['region']){
							var regObj = data['region']
							if(regObj){
								if (regObj.constructor.toString().indexOf("Array") == -1) {
									regObj = [regObj];
								}
								regObj.forEach(function (region) {
									if(region['file']){
										//region['_attributes']['path'] = pdfLink;
										var fileObj = region['file'];
										if (fileObj.constructor.toString().indexOf("Array") == -1) {
											fileObj = [fileObj];
										}
										fileObj.forEach(function (file) {
											var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
							
											if(currProofType== 'print'){
												var pdfPath = file['_attributes']['path'];
												if(pdfPath){
													if(!pdfPath.match(/http(s)?:/)){
														pdfPath = 'http://'+ window.location.host + pdfPath;
													}
													else{
														pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
													}
													pdfLinks.push(pdfPath);
												}	
											}
										})
										
									}
								})
							}

						}
				});
				
				var customer = $('.list.folder.active').attr('data-customer');
				var project  = $('.list.folder.active').attr('data-project');
				var metaObj = $('#tocContainer').data('binder')['container-xml'].meta;
				$('.la-container').fadeIn();
				$.ajax({
					type: "POST",
					url: "/api/mergepdf",
					data: {
						"customer": customer,
						"project" : project,
						"volume"  : metaObj.volume._text, 
						"issue"   : metaObj.issue._text,
						"pdfLinks": pdfLinks
					},
					success: function (data) {
						if (data && data.status && data.status.code == 200 && data.status.filePath){
							window.open(data.status.filePath, '_blank');
						}
						$('.la-container').fadeOut();
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut();
						showMessage({ 'text': 'Something went wrong when combining pdf. Please try again or contact support', 'type': 'error' });
					  return null;
					}
				});
			}
		}
	}
	return eventHandler;
})(eventHandler || {});