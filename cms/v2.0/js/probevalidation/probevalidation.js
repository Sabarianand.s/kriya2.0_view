var tempFileList = {};
$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/api/customers",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg){
				var customersList = getCustomers(msg);
				$('#customerselect').html(customersList);
				if($('#customerselect option').length == 1) {
					$('#customerselect').trigger('change');
				}
				if($('#projectselect option').length == 1) {
					$('#bucketselect').trigger('change');
				}
				$('#projectselect').html(getJournals(''));
			}else{
				return null;
			}
		},
		error: function(xhr, errorType, exception) {
			  return null;
		}
	});
	$('.la-container').fadeOut()
	$('#customerselect').change(function () {
		$('.la-container').fadeIn()
		$.ajax({
			type: "GET",
			url: "/api/projects?customerName="+$('#customerselect').val(),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (msg) {
				$('.la-container').fadeOut()
				if (msg && !msg.error){
					$('#projectselect').html(getJournals(msg));
					$('.actions-btn').removeClass('hidden')
				}
				else{
					  return null;
				}
			},
			error: function(xhr, errorType, exception) {
				  return null;
			}
		});
	});
	
	$('body').on({
		click: function (evt) {
			$('.fileChooser').parent().find('input').val("");
			$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
			$(this).parent().find('input:file').trigger('click');
		},
	}, '.fileChooser');
	$('body').on('blur keypress','#doi,#stage',function(ele){
		if((ele.type=="keypress") && (ele.which=="13" || ele.which=="9")){
			if(doiToSearch = $('#doi').val().trim()){
				$('.la-container').show();
				$('#proceed').removeClass('hide');
				custProjUpdate(doiToSearch);
			}
			else{
				$('#proceed').addClass('hide');
			}
		}
	});
	
	$('body').bind('paste','#doi',function(ele){
		setTimeout(function () { 
			if(doiToSearch = $('#doi').val().trim()){
				alert('yes');
				$('.la-container').show();
				$('#proceed').removeClass('hide');
				custProjUpdate(doiToSearch);
			}
			else{
				$('#proceed').addClass('hide');
			}
		}, 100);
	});
	
	
	$('#proceed').click(function(){
		$('.la-container').show();
		var doi = $('#doi').val();
		var customerVal = $('#customerselect').val();
		var projectVal  = $('#projectselect').val();
		var role  = $('#role').val();
		var stage  = $('#stage').val();
		var param = {
			'doi'      : doi,
			'customer' : customerVal,
			'project'  : projectVal,
		}
		if(doi&&customerVal&&projectVal){
			$.ajax({
				url:'/api/getxml',
				type:'GET',
				data:param,
				success:function(data){
					var oSerializer = new XMLSerializer();
					var sXML = oSerializer.serializeToString(data);
					$.ajax({
						url: '/api/probevalidation',
						type: 'POST',
						data: {
							'doi'      : doi,
							'customer' : customerVal,
							'project'  : projectVal,
							'role'     : role,
							'stage'    : stage,
							'content'  : sXML
						},
						success: function(res){
							if (res.error == undefined){
								$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
								updateProbeStatus(res)
							}else{
								$('#probeResult #PMC-Validator').append('<p>Could not process</p>');
							}
							$('.la-container').hide();
						},
						error: function(e){
							Console.log(e);
							$('.la-container').hide();
						}
					});
				},
				error:function(e){
					console.log(e);
					$('.la-container').hide();
				}
			});
		}
		else{
			console.log('empty value found');
			$('.la-container').hide();
		}
	});

	$('body').on( 'change', '#uploader input:file', function(e){
		$('#uploader').find('.i-files').remove();
		var customerVal = $('#customerselect').val();
		var projectVal  = $('#projectselect').val();
		var role  = $('#role').val();
		var stage  = $('#stage').val();
		if(!customerVal || !projectVal){
			alert('Please select customer and project.');
			return false;
		}
		$('.la-container').show();
		var file = e.target.files[0];
		var doi = file.name.match(/.+?(?=_)/) ? file.name.match(/.+?(?=_)/)[0] : file.name.replace('.xml','');
		doi = doi.toLowerCase();
		var reader = new FileReader();
		reader.onload = (function(theFile) {
			return function(e) {
				$.ajax({
					url: '/api/convertxml',
					type: 'POST',
					data: {
						'doi'      : doi,
						'customer' : customerVal,
						'project'  : projectVal,
						'role'     : role,
						'stage'     : stage,
						'content'  : e.target.result
					},
					success: function(data){
						if (data.error == undefined){
							$.ajax({
								url: '/api/probevalidation',
								type: 'POST',
								data: {
									'doi'      : doi,
									'customer' : customerVal,
									'project'  : projectVal,
									'role'     : role,
									'stage'     : stage,
									'content'  : data
								},
								success: function(res){
									if (res.error == undefined){
										$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
										updateProbeStatus(res);
										$('.la-container').hide();
									}else{
										$('#probeResult #PMC-Validator').append('<p>Could not process</p>');
										$('.la-container').hide();
									}
								},
								error: function(e){
									Console.log(e);
									$('.la-container').hide();
								}
							});
						}else{
							$('#probeResult #PMC-Validator').append('<p>Could not process</p>');
							$('.la-container').hide();
						}
					},
					error: function(e){
						Console.log(e);
						$('.la-container').hide();
					}
				});
			};
		})(file);
		reader.readAsText(file);
	});
});

function getCustomers(param){
	customerArr = [];
	var customerArray = param.customer;
	var customersLen = 0;
	if(customerArray){
		customersLen = customerArray.length;
		if(!customersLen || customersLen == 0){
			customerArray[0] = param.customer;
			customersLen = 1;
		}
	}

	var myCustomers = '';
	if(customersLen>1) {
		myCustomers += '<option value="">Select</option>';
	}
	for(i=0; i<customersLen; i++) {
		var name = customerArray[i]["name"];
		var fullName = customerArray[i]["fullName"];
		//Trim the customer name
		fullName = fullName.replace(/^([a-zA-Z0-9]+)\s+(.*?)$/,'$1');
		myCustomers += '<option value="' + name +'">' + fullName + '</option>';
		customerArr[i] = name;
	}
	return myCustomers;
}


//Get Journals
function getJournals(param){
	journalArr = {};
	var journalArray = param.project;
	var journalsLen = 0;
	if(journalArray) {
		journalsLen = journalArray.length;
		//in case the length is still zero, an object was returned
		if(!journalsLen || journalsLen == 0){
			journalArray[0] = param.project;
			journalsLen = 1;
		}
	}
	var myJournals = '';
	//if there are more than one journals, display select
	if(journalsLen > 1) {
		myJournals += '<option value="">Select</option>';
	}
	
	for(i=0; i<journalsLen; i++) {
		var name = journalArray[i]["name"];
		var fullName = journalArray[i]["fullName"];
		//default select the first journal
		if(i==0) {
			myJournals += '<option value="' + name+'" selected>' + fullName + '</option>';
		}else{
			myJournals += '<option value="' + name+'">' + fullName + '</option>';
		}
	}
	//close the list
	return myJournals;
}

function custProjUpdate(doiToSearch){
	var param ={
		customer : "customer",
		project : "project",
		doi : doiToSearch,
		from : 0,
		size : 5,
		urlToPost : "getSearchedArticles",
	} 
	$.ajax({
			type: "POST",
			url: '/api/getarticlelist',
			data: param,
			dataType: 'json',
			success: function (artReturn) {
				if(artReturn && (artReturn.hits.length==1)){
					// $('#customerselect').append("<option value="+artReturn.hits[0]._source.customer+">"+artReturn.hits[0]._source.customer+"<option>");
					$('#customerselect').val(artReturn.hits[0]._source.customer);
					$('#projectselect').html('');
					$('#projectselect').append("<option value="+artReturn.hits[0]._source.project+">"+artReturn.hits[0]._source.project+"<option>");
					$('#projectselect').val(artReturn.hits[0]._source.project);
					$('.la-container').hide();
				}
				else{
					$('.la-container').hide();
					$('#customerselect').val("");
					$('#projectselect').html('');
					console.log('error : doi not found');
				}
			},
			error: function(err){
				$('.la-container').hide();
				$('#customerselect').val("");
				$('#projectselect').html('');
				console.log('error : on searching doi');
			}
		});
}

function updateProbeStatus(res){
	if(res){
		res = res.replace(/<column[\s\S]?\/>/g, '<column></column>'); 
		var resNode = $(res);
		if(resNode.find('message:not(:empty)').length > 0){
			resNode.find('message').each(function(){
				
				if($(this).find('remove-display').text() == "true"){
					return;
				}

				var ruleType = $(this).find('rule-type').text();
				var errorId = $(this).find('error-id').text();
				var queryTo = $(this).find('query-role').text();

				var row = $('<div class="row" id="probe-container"/>');
				var left = $('<div class="col-sm-3 small" />');
				var right = $('<div class="col-sm-9" />');
				
				if($(this).find('node-id').text())
				left.append('<p> Node ID: <span class="label label-default">' + $(this).find('node-id').text() + '</span></p>');

				if($(this).find('rule-id').text())
				left.append('<p> Rule ID: <span class="label label-default">' + $(this).find('rule-id').text() + '</span></p>');

				if($(this).find('signoff-allow').text())
				left.append('<p> Signoff allow: <span class="label label-default">' + $(this).find('signoff-allow').text() + '</span></p>');
				
				if(ruleType == "DTD-Validator"){
					left.append('<p> Line: <span class="label label-default">' + $(this).find('line').text() + '</span></p>');
					left.append('<p> Column: <span class="label label-default">' + $(this).find('column').text() + '</span></p>');						
				}else if(ruleType == "Schematron-Validator"){
					left.append('<p> Error ID: <span class="label label-default">' + errorId + '</span></p>');
				}

				row.append(left);
				
				right.append('<p>' + $(this).find('probe-message').text() + '</p>');
				if ($(this).find('text').text() != ""){
					right.append('<p>TEXT: ' + $(this).find('text').text() + '</p>');
				}
				if($(this).find('xpath').text())
				right.append('<pre>' + $(this).find('xpath').text() + '</pre>');
				row.append(right);
				$('#'+ruleType).append(row);
			});
			$('#probeResult').modal('show');
		}
	}
}