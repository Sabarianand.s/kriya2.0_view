var tempFileList = {};
$(document).ready(function() {
	eventHandler.menu.board.getCustomers(msg)
	$.ajax({
		type: "GET",
		url: "/api/customers",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg){
				var customersList = getCustomers(msg);
				$('#customerselect').html(customersList);
				$('#customerselect').material_select();
				if($('#customerselect option').length == 1) {
					$('#customerselect').trigger('change');
				}
				if($('#projectselect option').length == 1) {
					$('#bucketselect').trigger('change');
				}
				$('#projectselect').html(getJournals(''));
			}else{
				return null;
			}
		},
		error: function(xhr, errorType, exception) {
			  return null;
		}
	});
	
	$('#customerselect').change(function () {
		$('.la-container').fadeIn()
		$.ajax({
			type: "GET",
			url: "/api/projects?customerName="+$('#customerselect').val(),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (msg) {
				$('.la-container').fadeOut()
				if (msg && !msg.error){
					$('#projectselect').html(getJournals(msg));
					$('#projectselect').material_select();
					$('.actions-btn').removeClass('hidden')
				}
				else{
					  return null;
				}
			},
			error: function(xhr, errorType, exception) {
				  return null;
			}
		});
	});
	
	$('body').on({
		click: function (evt) {
			$('.tableClone input:file').val('');
			$(this).parent().find('input:file').trigger('click');
		},
	}, '.fileChooser');

	$('body').on( 'change', 'input:file', function(e){
		$('#uploader').find('.i-files').remove();
		var file = e.target.files[0];
		tempFileList = {};
		var cnt = 'fl_' + ($('#uploader').find('.i-files').length + 1);
		tempFileList[cnt] = file;
		var ftype = file.name.replace(/^.*\./, '');
		var fname = file.name;
		console.log(file);
		$('#uploader .fileList').append('<span class="i-files" id="'+cnt+'"><span type="'+ftype+'"></span>'+fname+'</span>');
		$('#uploader').removeClass('active')
	});

	$('body').on( 'click', '.addJobBtn', function(e){
		$('.addjob-panel,.workflow-panel').addClass('hidden');
		$('.addjob-panel').removeClass('hidden');
	});

	$('body').on( 'click', '.getWFBtn', function(e){
		if ($('#projectselect').val() == "") return;
		$('.la-container').fadeIn()
		jQuery.ajax({
			type: "GET",
			url: "/api/getworkflow?customer="+$('#customerselect').val()+"&project="+$('#projectselect').val(),
			success: function (msg) {
				$('.la-container').fadeOut()
				if (msg && !msg.error){
					$('.addjob-panel,.workflow-panel').addClass('hidden')
					$('.workflow-panel').html(msg)
					$('.workflow-panel').find('div[data-stage]').addClass('disabled')
					$('.workflow-panel').find('div[data-stage]').first().removeClass('disabled')
					$('.workflow-panel').removeClass('hidden')
				}
				else{
					  return null;
				}
			},
			error: function(xhr, errorType, exception) {
				$('.la-container').fadeOut()
				  return null;
			}
		});
	});
	
	$('body').on( 'click', 'div[data-stage] .signoff-stage', function(e){
		var currStage = $(this).attr('data-to-stage');
		if ($('.workflow-panel').find('div[data-stage="' + currStage + '"]').length > 0){
			$('.workflow-panel').find('div[data-stage]:not(.hidden)').addClass('disabled');
			$('.workflow-panel').find('div[data-stage="' + currStage + '"]').removeClass('disabled');
		}else if ($('.workflow-panel').find('.wf-triggers trigger[name="' + currStage + '"][action="addStage"]').length > 0){
			var nextStage = $('.workflow-panel').find('.wf-triggers trigger[name="' + currStage + '"][action="addStage"] stage').text().toLowerCase();
			nextStage = nextStage.replace(/ /g, '');
			if ($('.workflow-panel').find('div[data-stage="' + nextStage + '"]').length > 0){
				$('.workflow-panel').find('div[data-stage]:not(.hidden)').addClass('disabled');
				$('.workflow-panel').find('div[data-stage="' + nextStage + '"]').removeClass('disabled');
			}
		}
	});
	
});

function getCustomers(data){
	customerArr = [];
	var customerArray = param.customer;
	var customersLen = 0;
	if(customerArray){
		customersLen = customerArray.length;
		if(!customersLen || customersLen == 0){
			customerArray[0] = param.customer;
			customersLen = 1;
		}
	}

	var myCustomers = '';
	if(customersLen>1) {
		myCustomers += '<option value="">Select</option>';
	}
	for(i=0; i<customersLen; i++) {
		var name = customerArray[i]["name"];
		var fullName = customerArray[i]["fullName"];
		//Trim the customer name
		fullName = fullName.replace(/^([a-zA-Z0-9]+)\s+(.*?)$/,'$1');
		myCustomers += '<option value="' + name +'">' + fullName + '</option>';
		customerArr[i] = name;
	}
	return myCustomers;
}


//Get Journals
function getJournals(data){
	journalArr = {};
	var journalArray = param.project;
	var journalsLen = 0;
	if(journalArray) {
		journalsLen = journalArray.length;
		//in case the length is still zero, an object was returned
		if(!journalsLen || journalsLen == 0){
			journalArray[0] = param.project;
			journalsLen = 1;
		}
	}
	var myJournals = '';
	//if there are more than one journals, display select
	if(journalsLen > 1) {
		myJournals += '<option value="">Select</option>';
	}
	
	for(i=0; i<journalsLen; i++) {
		var name = journalArray[i]["name"];
		var fullName = journalArray[i]["fullName"];
		//default select the first journal
		if(i==0) {
			myJournals += '<option value="' + name+'" selected>' + fullName + '</option>';
		}else{
			myJournals += '<option value="' + name+'">' + fullName + '</option>';
		}
	}
	//close the list
	return myJournals;
}
