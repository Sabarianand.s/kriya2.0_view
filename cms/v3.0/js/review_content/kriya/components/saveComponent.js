/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

	//var _this;
    kriya.saveComponent = {
        config : {
			'htmlComponent'     : 'setHTML',
			'checkBoxComponent' : 'setHTML',
			'getLinkedObjects'  : 'saveLinkedObjects',
			'inputTags'         : 'addLinksToObjects',
			'floatComponent'	: 'setFloat',
		},
        init: function (component, ele) {
			var validated = this.validateComponents(component);
			var returnElement = false;
			if (validated) {
				$(component).find('.com-save').addClass('disabled');
				if($(component).hasClass('manualSave')){
					var ele = kriya.general.convertTemplate('', $(component));
					if(ele){
						ele.attr('data-inserted', 'true');	
					}					
				}
				returnElement = this.triggerSaveFunc(component,ele);
			}
			return returnElement;
        },
        triggerSaveFunc: function(component,ele){
        	var _this = this;
        	this.container = (ele) ? ele : kriya.config.activeElements;
			this.component = component;

			$(component).find('[data-type]').each(function(i){
				var comType  = $(this).attr('data-type');
				var saveType = $(this).attr('data-save-type');
				var saveFunc = false;
				(saveType)?(
					saveFunc = saveType
				):(typeof(_this.config[comType]) != "undefined")?(
					saveFunc = _this.config[comType]
				):(
					saveFunc = false
				);

				if (saveFunc){
					if (typeof(kriya[saveFunc]) != 'undefined'){
						kriya[saveFunc].init(_this.container, component, $(this)[0]);
					}else if (typeof(kriya.general[saveFunc]) != 'undefined'){
						kriya.general[saveFunc]($(this)[0], _this.container, component);
					}
				}
			});
			
			if ($(this.container).find('[data-class-new]').length > 0){
				$(this.container).attr('data-element-added', 'true');
			}
			//this if condition will help for validate the reference when we remove author or contrib from edit ref popup.
			if ($(this.container).find('[data-class-removed]').length > 0){
				$(this.container).attr('data-element-removed', 'true');
			}
			//before this attr is added for validate ref, it is no need for further process
			$('*[data-class-removed]').each(function(){
				$(this).removeAttr('data-class-removed');
			});
			//removing the suffix inserted for newly inserted from its class
			$('*[data-class-new]').each(function(){
				$(this).attr('class', $(this).attr('data-class-new'));
				$(this).removeAttr('data-class-new');
			});
			return this.container;
		},
        validateComponents : function (component){
			var validated = true;
			//In copyright statement popup for radiobuttons group div only have data-error attr if *[data-error][data-validate] this selectore at that time for that div not removing data-error attr-#2690
			$(component).find('*[data-error]').removeAttr('data-error');
			$(component).find('[data-validate]:visible, [data-check-field]:visible').each(function(){
				kriya.validate.init($(this));
			});
			
			if ($(component).find('*[data-error]').length > 0){
				validated = false;
			}
			$(component).find('*ul.tabs a[href*="_tab"]').each(function(){
				$(this).parent().removeAttr('data-error-count');
				var href = $(this).attr('href');
				if ($(component).find(href).find('[data-error]').length > 0){
					$(this).parent().attr('data-error-count', $(component).find(href).find('[data-error]').length);
				}
			});
			return validated;
		},
        addNewElement : function(component){

        	var className = component.attr('data-class');
        	var newElement = kriya.general.convertTemplate(className, component);
        	if(!newElement){
        		return false;
        	}
			newElement.kriyaTracker('ins');
        	var saved = kriya.saveComponent.init(component[0],newElement);
        	if(saved){
        		return newElement;
        	}else{
        		return saved;
        	}
        },
		/**
		*# provoke: basically when a save button is clicked to add new components
		*# function which builds/picks a template from the given component and 
		*# pushes the data from the component to the template and
		*# adds the template to the content at appropriate place
		*# params: component from where the data has to be picked
		**/
		addNewComponent: function (component){
			var className = component.getAttribute('data-class');
			if (className == undefined) return;
			var group = $('.collection[data-source="'+className+'"]');
			//if multiple pop-up's are opened, to save it in right popup
			if ($(component).length > 0 && $(component)[0].hasAttribute('data-component-xpath')){
				var compXpath = $(component).attr('data-component-xpath');
				var parentComponent = $('[data-type="popUp"][data-component="' + compXpath + '"]');
				if (parentComponent.length > 0 &&  parentComponent.find('.collection[data-source="'+className+'"]').length > 0){
					group = parentComponent.find('.collection[data-source="'+className+'"]');
				}
			}
			var separator = group.attr('data-separator');
			separator = (separator) ? separator : ', ';
			if (group.find('[data-template]').length > 0){
				var template = group.find('[data-template]')[0].cloneNode(true);
				template.removeAttribute('data-template');
			}else{
				var template = document.createElement('li');
				template.setAttribute('class', 'collection-item dismissable');
			}
			var subChilds = $(component).find('[data-class][data-type="htmlComponent"]');
			//add it to the cuurent collection
			for (var i = 0, sl = subChilds.length; i < sl; i++){
				subChild = subChilds[i];
				if (subChild.getAttribute('data-class') == undefined || subChild.innerHTML == ''){
					continue;
				} 
				var subNode = document.createElement('span');
				subNode.setAttribute('class', subChild.getAttribute('data-class'));
				subNode.setAttribute('id', uuid.v4());
				if (i != 0){
					if(i == 1 && subChilds[0].innerHTML==''){}
					else{
						var textNode = document.createTextNode(separator);
					}
					if ($(template).find('[data-input]').length > 0){
						$(template).find('[data-input]').append(textNode);
					}else{
						template.appendChild(textNode);
					}
				}
				if ($(template).find('[data-input]').length > 0){
					$(template).find('[data-input]').append(subNode);
				}else{
					template.appendChild(subNode);
				}
				kriya.general['setHTML'](subChild, template);
			}
			$(group).append(template);
			// to directly save affiliation/present address into content on adding new from an author pop-up - Jai 17-08-2017
			if ($(template).closest('.collection[data-type]').attr('data-type') == "getLinkedObjects"){
				var component = $(template).closest('[data-component]');
				if (component.length > 0 && component[0].hasAttribute('data-node-xpath') && !component.hasClass('manualSave')){
					var nodeXpath = '.' + component[0].getAttribute('data-node-xpath').replace(/^\./, '');
					var container = kriya.xpath(nodeXpath, $(kriya.config.containerElm));
					if (container.length > 0 && /Group$|GroupAuthor/.test($(container).attr('class').split(' ')[0])){
						kriyaEditor.settings.undoStack.push(container);
						collection = $(template).closest('.collection[data-type]')[0];
						kriya.general.saveLinkedObjects(collection, container, component);
						kriya.general.reorderLinks(collection.getAttribute('data-class'), collection.getAttribute('data-source'), container[0]);
						kriya.general.getLinkedObjects(container, component, $(template).closest('.collection[data-type]')[0]);

						//Add focus out data in collection
						var objects = $(group).find('> [data-rid]:not([data-template="true"])');
						var focusData = '';
						objects.each(function(){
							focusData += $(this).attr('data-rid') + ',';
						});
						focusData = focusData.replace(/([\,\s]+$)/g, '');
						$(group).attr('data-focusout-data', focusData);

					}
				}
			}
			return true;
		}
    };
    
    return kriya;

}(window.kriya || {}));