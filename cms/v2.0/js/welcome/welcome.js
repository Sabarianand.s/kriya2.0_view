/*
main JS to load scripts related to app.

*/
var md5 = function(value) {
    return CryptoJS.MD5(value).toString();
}

var kriya = {};
function invalidData(){
	jQuery('.error-text').removeClass('hide-element ');
	jQuery('#username').val('');
	jQuery('#password').val('');
	jQuery('#username').removeClass('valid');
	jQuery('#password').removeClass('valid');
	jQuery(".login-form a[href='#']").html('login').removeClass('disabled');
}

function inputIsEmpty(elmArray){
	var count, elmLength = elmArray.length;
	var isEmpty = false;
	for(count=0; count< elmLength; count++){
		var value = elmArray[count].val();
		if(value === ""){
			elmArray[count].addClass('invalid');
			elmArray[count].next().addClass('active');
			isEmpty =true;
		}else{
			elmArray[count].removeClass('invalid');
		}
	}
	return isEmpty;
}

function authenticate(isConfirm, onSuccess, onError){
	if( !inputIsEmpty( [ jQuery('#username'), jQuery('#password') ] ) ){
		var userName    = jQuery('#username').val();
		var passwordVal = jQuery('#password').val();
		//hashUser = md5(userName);
		//hashPass = md5(passwordVal);
		//hashUser = userName;
		//hashPass = passwordVal;
		var param = {
			'user': userName,
			'pass': passwordVal
		}

		if(isConfirm){
			param.ctoken = kriya.ctoken; 
		}
		jQuery.ajax({
			type: "POST",
			url: "/api/authenticate",
			data: param,
			success: function (msg) {
				if (msg && msg.redirectToPage){
					store.remove('kriyaLogoutFlag');
					if(onSuccess && typeof(onSuccess) == "function"){
						onSuccess(msg);
					}

					if(opener && opener.keeplive && opener.keeplive.handleVisibilityChange){
						$(opener.document).find('#lock-screen').hide();
						opener.keeplive.handleVisibilityChange();
						window.close();
					}else{
						window.location = msg.redirectToPage;	
					}
				}else if(msg && msg.status == 2 && msg.token){
					$('.login-form .login-col-2').addClass('hide');
					$('.login-form .confirmationPanel').removeClass('hide');
					kriya.ctoken = msg.token;
				}
				else{
				   invalidData();
				}
			},
			error: function(xhr, errorType, exception) {
				if(onError && typeof(onError) == "function"){
					onError();
				}
			   invalidData();
			}
		});
	}
}
jQuery(function() {
	//jQuery(".login-form a[href='/toc']").click(function (evt) {
	jQuery(".login-form input").keypress(function (evt) {
		if (evt.which == 13) {
			jQuery(".login-form a[href='#']").html('logging in...').addClass('disabled');
			authenticate('', function(){
				jQuery(".login-form a[href='#']").html('login').removeClass('disabled');;
			});
			evt.preventDefault();
		}
	});
	jQuery(".login-form a[href='#']").click(function (evt) {
		jQuery(".login-form a[href='#']").html('logging in...').addClass('disabled');
		authenticate('', function(){
			jQuery(".login-form a[href='#']").html('login').removeClass('disabled');;
		});
		evt.preventDefault();
	});
	$('body').on({
		click: function(evt){
			authenticate(true);	
			evt.preventDefault();
		}
	}, '.confirmationPanel .confirm');

	$('body').on({
		click: function(evt){
			$('.login-form .login-col-2').removeClass('hide');
			$('.login-form .confirmationPanel').addClass('hide');
			$('#username').val('');
			$('#password').val('');
			kriya.ctoken = undefined;	
		}
	}, '.confirmationPanel .confirmCancel');
});

$(window).on('load', function(e) {
	setTimeout(function() {
		$("body").addClass("loaded")
	}, 200)
});
