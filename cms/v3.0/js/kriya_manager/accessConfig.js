var accessConfig = {
    "production": {
        "admin": {
            "production": [
                "developer",
                "support",
                "production",
                "admin",
                "manager"
            ],
            "publisher": [
                "production",
                "manager"
            ],
            "copyeditor": [
                "copyeditor",
                "vendor",
                "production"
            ]
        },
        "manager": {
            "production": [
                "production"
            ],
            "publisher": [
                "production",
                "manager"
            ]
        }
    },
    "copyeditor": {
        "manager": {
            "copyeditor": [
                "manager",
                "vendor",
                "production"
            ]
        }
    }
}
this.accessConfig = accessConfig;