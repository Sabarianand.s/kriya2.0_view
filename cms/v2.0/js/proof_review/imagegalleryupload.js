function initialiseDragDrop(){
    $('body').on({
        dragenter: function(e) {
            e.preventDefault();
			//console.log($(this))
			$(this).addClass('ama-uploader-dragging')
        },
        dragover: function(e) {
			e.preventDefault();
			$(this).addClass('ama-uploader-dragging')
			return false;
		},
        dragleave: function(e) {
			$('.ama-uploader-dragging').removeClass('ama-uploader-dragging')
		},
		drop: function(e){
			e.preventDefault();
			e.stopPropagation();
			if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length){
				if(e.currentTarget.id == "uploader"){
					var convertFile = true;
					var blockNode = $(e.currentTarget).closest('[data-component]');
					if (blockNode.find('[id="uploader"]')[0].hasAttribute('data-convert') && blockNode.find('[id="uploader"]').attr('data-convert') == 'false'){
						var imageNode = blockNode.find('[id="uploader"]').find('[data-class="jrnlFigure"]');
						convertFile = false;
					}else{
						if ($('#uploader img').length == 0){
							$('#uploader').prepend('<img data-type="image"/>');
						}
						$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
						$('#uploader img').addClass('ama-uploader-dragging');
						//replaceImageFile(e.originalEvent.dataTransfer.files[0], e.currentTarget, $(e.currentTarget).closest('[data-component]'), $('#uploader img'));
						var imageNode = $(e.currentTarget).find('img');
					}
					var param = {
						'file': e.originalEvent.dataTransfer.files[0], 
						'block': blockNode,
						'success': function(res){
							/**
							 * Callback function for upload file success
							 * Update the url in image 
							 * Remove the progress bar in block
							 */
							var fileDetailsObj = res.fileDetailsObj;
							var filePath = '/resources/' + fileDetailsObj.dstKey;
							var fileName = fileDetailsObj.name + fileDetailsObj.extn;
							if ($(imageNode)[0].nodeName != 'IMG'){
								$(imageNode).attr('data-href', filePath).text(fileName);
							}else{
								$(imageNode).attr('src', filePath).removeAttr('data-error');
								$(imageNode).attr('data-original-name', fileDetailsObj.name);
								$(imageNode).attr('data-primary-extension', fileDetailsObj.extn.replace(".",""));
								if($(imageNode).attr('type') == "objFile"){
									$(imageNode).attr('data-focusout-data', filePath);
								}
							}
							if ($(blockNode).find('[id="uploader"]')[0].hasAttribute('data-on-success')){
								var callbackFn = $(blockNode).find('[id="uploader"]').attr('data-on-success');
								var execFn = getStringObj(callbackFn);
								execFn(imageNode);
							}
							$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
							setTimeout(function() {
								blockNode.progress('Uploaded', true);
							}, 1000 );
						},
						'error': function(err){
							//Error callback function remove the progress in blocknode
							blockNode.progress('', true);
							//Show the error notification
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Uploading file failed.',
								timeout: 8000,
								icon : 'icon-info'
							});
						}
					}
					if (!convertFile) param.convert = 'false';
					eventHandler.menu.upload.uploadFile(param, imageNode);
				}else if(e.currentTarget.id == "supp_uploader"){
					$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
					$('#supp_uploader [data-class="jrnlSupplSrc"]').addClass('ama-uploader-dragging');
					//uploadSuppFile(e.originalEvent.dataTransfer.files[0], $(e.currentTarget).closest('[data-component]'), $('#supp_uploader [data-class="jrnlSupplSrc"]'));
					var blockNode = $(e.currentTarget).closest('[data-component]');
					if(blockNode.find('#supplement-type-dropdown li.selected').attr('data-block-class') == "jrnlFigBlock"){
						var srcNode = $('#supp_uploader [data-class="jrnlFigure"]');
						$('#supp_uploader [data-class="jrnlSupplSrc"]').addClass('hidden');
					}else{
						var srcNode = $('#supp_uploader [data-class="jrnlSupplSrc"]');
						$('#supp_uploader [data-class="jrnlFigure"]').addClass('hidden');
					}
					srcNode.removeClass('hidden');
					var param = {
						'file': e.originalEvent.dataTransfer.files[0], 
						'block': blockNode,
						'convert': 'false',
						'success': function(res){
							/**
							 * Callback function for upload file success
							 * Update the url in supplementary source
							 * Remove the progress bar in block
							 */
							var fileDetailsObj = res.fileDetailsObj;
							//var fileName = fileDetailsObj.dstKey.replace(/\\/g,'/').replace(/.*\//, '');
							var fileName = fileDetailsObj.name+fileDetailsObj.extn;
							var filePath = '/resources/' + fileDetailsObj.dstKey;

							if(srcNode[0].nodeName == "IMG"){
								$(srcNode).attr('src', filePath);
							}else{
								$(srcNode).attr('href', filePath).text(fileName);
							}
							
							if($(srcNode).attr('type') == "objFile"){
								$(srcNode).attr('data-focusout-data', filePath + '|' + fileName);
							}
							$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
							blockNode.progress('Uploaded', true);
						},
						'error': function(err){
							//Error callback function remove the progress in blocknode
							blockNode.progress('', true);
							//Show the error notification
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Uploading file failed.',
								timeout: 8000,
								icon : 'icon-info'
							});
						}
					}
					eventHandler.menu.upload.uploadFile(param, srcNode);
				}else if($(e.currentTarget).closest('.jrnlSupplBlock').length > 0){
					var blockNode = $(e.currentTarget).closest('.jrnlSupplBlock');

					$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
					if(blockNode.find('.jrnlSupplSrc').length == 0){
						blockNode.append('<p class="jrnlSupplSrcPara"><span class="jrnlSupplSrc" /></p>');
					}
					blockNode.addClass('ama-uploader-dragging');
					var srcNode = blockNode.find('.jrnlSupplSrc');
					var param = {
						'file': e.originalEvent.dataTransfer.files[0], 
						'block': blockNode,
						'convert': 'false',
						'success': function(res){
							/**
							 * Callback function for upload file success
							 * Update the url in supplementary source
							 * Remove the progress bar in block
							 */
							var fileDetailsObj = res.fileDetailsObj;
							var fileName = fileDetailsObj.name+fileDetailsObj.extn;
							var filePath = '/resources/' + fileDetailsObj.dstKey;
							$(srcNode).attr('href', filePath).text(fileName);
							if($(srcNode).attr('type') == "objFile"){
								$(srcNode).attr('data-focusout-data', filePath + '|' + fileName);
							}
							$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
							blockNode.progress('Uploaded', true);
							kriyaEditor.settings.undoStack.push(blockNode);
							kriyaEditor.init.addUndoLevel('upload-file');
						},
						'error': function(err){
							//Error callback function remove the progress in blocknode
							blockNode.progress('', true);
							//Show the error notification
							kriya.notification({
								title: 'Failed',
								type: 'error',
								content: 'Uploading file failed.',
								timeout: 8000,
								icon : 'icon-info'
							});
						}
					}
					eventHandler.menu.upload.uploadFile(param, srcNode);
					//uploadSuppFile(e.originalEvent.dataTransfer.files[0], blockNode, blockNode.find('.jrnlSupplSrc'));
				}else{
					//replaceImageFile(e.originalEvent.dataTransfer.files[0],e.currentTarget, $(e.currentTarget).closest('[data-id*="BLK"]'), $(e.currentTarget).closest('img'));
					var param = {'file': e.originalEvent.dataTransfer.files[0], 'block': $(e.currentTarget).closest('[data-id*="BLK"]')}
					eventHandler.menu.upload.uploadFile(param, e.currentTarget);
				}
				kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;
			}
		}
    }, '#uploader, #supp_uploader, #contentDivNode .jrnlSupplBlock');
	
	$('body').on( 'click', '#uploader .fileChooser', function(){
		$('#uploader input:file').val('');
		$(this).parent().find('input').trigger('click');
	});

	$('body').on( 'click', '#supp_uploader .fileChooser', function(){
		$('#supp_uploader input:file').val('');
		$(this).parent().find('input').trigger('click');
	});

	$('body').on( 'change', '#supp_uploader input:file', function(e){
		$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
		$('#supp_uploader [data-class="jrnlSupplSrc"]').addClass('ama-uploader-dragging');

		var blockNode = $(e.currentTarget).closest('[data-component]');
		if(blockNode.find('#supplement-type-dropdown li.selected').attr('data-block-class') == "jrnlFigBlock"){
			var srcNode = $('#supp_uploader [data-class="jrnlFigure"]');
			$('#supp_uploader [data-class="jrnlSupplSrc"]').addClass('hidden');
		}else{
			var srcNode = $('#supp_uploader [data-class="jrnlSupplSrc"]');
			$('#supp_uploader [data-class="jrnlFigure"]').addClass('hidden');
		}
		srcNode.removeClass('hidden');
		var param = {
			'file': e.target.files[0], 
			'block': blockNode,
			'convert': 'false',
			'success': function(res){
				/**
				 * Callback function for upload file success
				 * Update the url in supplementary source
				 * Remove the progress bar in block
				 */
				var fileDetailsObj = res.fileDetailsObj;
				var fileName = fileDetailsObj.name+fileDetailsObj.extn;
				var filePath = '/resources/' + fileDetailsObj.dstKey;
				
				if(srcNode[0].nodeName == "IMG"){
					$(srcNode).attr('src', filePath);
				}else{
					$(srcNode).attr('href', filePath).text(fileName);
				}
				
				if($(srcNode).attr('type') == "objFile"){
					$(srcNode).attr('data-focusout-data', filePath + '|' + fileName);
				}
				$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
				blockNode.progress('Uploaded', true);
			},
			'error': function(err){
				//Error callback function remove the progress in blocknode
				blockNode.progress('', true);
				//Show the error notification
				kriya.notification({
					title: 'Failed',
					type: 'error',
					content: 'Uploading file failed.',
					timeout: 8000,
					icon : 'icon-info'
				});
			}
		}
		eventHandler.menu.upload.uploadFile(param, srcNode);

		//uploadSuppFile(e.target.files[0], $(e.currentTarget).closest('[data-component]'), $('#supp_uploader [data-class="jrnlSupplSrc"]'));
	});

	$('body').on( 'change', '#uploader input:file', function(e){
		//addFileToHolder(e.target.files[0],e.target);
		var block = $(e.target).closest('[data-component]');
		var convertFile = true;
		if (block.find('[id="uploader"]')[0].hasAttribute('data-convert') && block.find('[id="uploader"]').attr('data-convert') == 'false'){
			var imgNode = block.find('[id="uploader"]').find('[data-class="jrnlFigure"]');
			convertFile = false;
		}else{
			if ($('#uploader img').length == 0){
				$('#uploader').prepend('<img data-type="image"/>');
			}
			$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
			$('#uploader img').addClass('ama-uploader-dragging');
			var imgNode = $(e.target).closest('[data-component]').find('#uploader img');
		}
		var param = {
			'file': e.target.files[0], 
			'block': block,
			'success': function(res){
				/**
				 * Callback function for upload file success
				 * Update the url in image
				 * Remove the progress bar in block
				 */
				var fileDetailsObj = res.fileDetailsObj;
				var filePath = '/resources/' + fileDetailsObj.dstKey;
				var fileName = fileDetailsObj.name + fileDetailsObj.extn;
				if ($(imgNode)[0].nodeName != 'IMG'){
					$(imgNode).attr('data-href', filePath).text(fileName);
				}else{
					$(imgNode).attr('src', filePath).removeAttr('data-error');
					$(imgNode).attr('data-original-name', fileDetailsObj.name);
					$(imgNode).attr('data-primary-extension', fileDetailsObj.extn.replace(".",""));
					$(imgNode).attr('data-fileSize', fileDetailsObj.imageInfo.Filesize).attr('data-colorspace',fileDetailsObj.imageInfo.Colorspace).attr('data-geometry',fileDetailsObj.imageInfo.Geometry);
					if($(imgNode).attr('type') == "objFile"){
						$(imgNode).attr('data-focusout-data', filePath);
					}
				}
				if ($(block).find('[id="uploader"]')[0].hasAttribute('data-on-success')){
					var callbackFn = $(block).find('[id="uploader"]').attr('data-on-success');
					var execFn = getStringObj(callbackFn);
					execFn(imgNode);
				}
				$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
				setTimeout(function() {
					block.progress('Uploaded', true);
				}, 1000 );
			},
			'error': function(err){
				//Error callback function remove the progress in blocknode
				block.progress('', true);
				//Show the error notification
				kriya.notification({
					title: 'Failed',
					type: 'error',
					content: 'Uploading file failed.',
					timeout: 8000,
					icon : 'icon-info'
				});
			}
		}
		if (!convertFile) param.convert = 'false';
		eventHandler.menu.upload.uploadFile(param, imgNode);
		//replaceImageFile(e.target.files[0], e.target, $(e.target).closest('[data-component]'), $('#uploader img'));
	});
}

function replaceImageFile(file, target, block, replaceImage){
	var formData = new FormData();
	formData.append('uploads[]', file, file.name);
	formData.append('customer', kriya.config.content.customer);
	formData.append('project', kriya.config.content.project);
	formData.append('doi', kriya.config.content.doi);
    block.progress('Uploading');
	$.ajax({
		url: '/uploadFiles',
		type: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		success: function(data){
			block.progress('Uploading: 10%');
			if (typeof(data) == "object"){
				getJobStatus(data, block, file.name.split('.').pop(), replaceImage);
			}
		},
		error: function(data){
			block.progress('Failed', true);
			replaceImage.removeClass('ama-uploader-dragging');
			kriya.notification({
				title : 'ERROR',
				type  : 'error',
				timeout : 5000,
				content : 'Failed to upload file',
				icon: 'icon-warning2'
			});
		}
    });
}

function getJobStatus(data, block, ext, replaceImage){
	var jobID = data[Object.keys(data)[0]];
	$.ajax({
		url: '/getJobStatus',
		type: 'POST',
		data: {'jobID': jobID,'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'doi':kriya.config.content.doi},
		success: function(response){
			if (typeof(response) == "object"){
				if (/BLK_/.test(block.attr('data-id'))){
					var clonedImage = $('<img class="jrnlFigure"/>');
					clonedImage.attr('src', '..' + response.src).removeAttr('width').removeAttr('height').attr('data-primary-extension', ext);
					replaceImage.after(clonedImage);
					replaceImage.attr('data-old-src', replaceImage.attr('src')).removeAttr('src').removeAttr('data-alt-src');
					replaceImage.attr('data-track', 'del');
					clonedImage.kriyaTracker('ins');
					clonedImage.closest('[class="jrnlFigBlock"]').find('.floatHeader').find('.image-versions').remove();
					clonedImage.closest('[class="jrnlFigBlock"]').find('.floatHeader').prepend('<span class="btn btn-small image-versions" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'showImageVersions\'}"><i class="material-icons">collections</i> Versions</span>');
					kriya.general.updateRightNavPanel(clonedImage.closest('[class="jrnlFigBlock"]').attr('data-id'), kriya.config.containerElm);
				}else{
					replaceImage.attr('src', '..' + response.src).removeAttr('width').removeAttr('height').attr('data-primary-extension', ext).removeAttr('data-error');
				}
				$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
				block.progress('Uploaded', true);
				if(block.closest('[data-type="popUp"]').length < 1){
					kriyaEditor.settings.undoStack.push(block);
					kriyaEditor.init.addUndoLevel('upload-image');
				}
			}else if (/failed/i.test(response)){
				block.progress('Failed', true);
			}else{
				block.progress(response);
				setTimeout(function(){
					getJobStatus(data, block, ext, replaceImage);
				}, 1000)
			}
		},
		error: function(response){
			block.progress('Failed', true);
		}
	});
}

function uploadSuppFile(file, block, replaceFile){
	var formData = new FormData();
	formData.append('uploads[]', file, file.name);
	formData.append("customer", kriya.config.content.customer);
	formData.append("project", kriya.config.content.project);
	formData.append("doi", kriya.config.content.doi);

	block.progress('Uploading...');
	$.ajax({
      url: '/api/uploadfiles',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      error: function(e){
      	console.log('Error' + e);
      	block.progress('',true);
      },
      success: function(data){
		var sourceData = '../resources/' + kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/' + data;
		replaceFile.attr('href', sourceData).text(data);
		block.progress('',true);
		$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
		if(block.closest('[data-type="popUp"]').length < 1){
			kriyaEditor.settings.undoStack.push(block);
			kriyaEditor.init.addUndoLevel('upload-file');
		}else{
			replaceFile.removeAttr('data-error');
		}
      }
    });
}