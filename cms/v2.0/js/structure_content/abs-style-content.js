var styles = {
    'front': {
        'Session':{
            'Session': 'h1,AbsSession,block',
            'Subject': 'h1,AbsSubj,block',
            'Number': 'h1,AbsNumber,block',
        },
        'ArtTitle': 'h1,AbsTitle,block',
        'Authors': 'p,AbsAuthors,block',
        'Author': 'span,AbsAuthor,inline',
        'Collab': 'span,AbsCollab,inline',
        'Aff': 'p,AbsAff,block',
        'AbsHead': 'h3,AbsHead,block',
        'AbsPara': 'p,AbsPara,block',
        'KeywordPara': 'p,AbsKeywordPara,block',
        'Reference': 'p,AbsRefText,block',
        'DOI': 'p,AbsDOI,block',
        'Delete': 'p,jrnlDeleted,block',
        'Captions':{
            'AbsFigCaption': 'p,AbsFigCaption,block',
            'AbsTblCaption': 'p,AbsTblCaption,block',
            'FigCaption': 'p,jrnlFigCaption,block',
            'FigFoot': 'p,jrnlFigFoot,block',
            'TblCaption': 'p,jrnlTblCaption,block',
            'TblFoot': 'p,jrnlTblFoot,block',
            'BoxCaption': 'p,jrnlBoxCaption,block',
            'BoxPara': 'p,jrnlBoxPara,block',
            'SupplCaption': 'p,jrnlSupplCaption,block',
            'VidCaption': 'p,jrnlVidCaption,block',

        },
    },
};
var refStyles = {
    'AbsRefText': {
        '<u>A</u>U/ED':{
            'sk': 'alt+a',
            '<u>A</u>uthor': 'span,RefAuthor,inline',
            '<u>E</u>ditor':'span,RefEditor,inline',
            '<u>C</u>ollab':'span,RefCollaboration,inline',
            'E<u>t</u>al':'span,RefEtal,inline',
        },
        '<u>J</u>RNL':{
            'sk': 'alt+j',
            '<u>A</u>rticle Title':'span,RefArticleTitle,inline',
            '<u>J</u>ournal Title':'span,RefJournalTitle,inline',
            '<u>E</u>Location':'span,RefELocation,inline',
        },
        '<u>B</u>OOK':{
            'sk': 'alt+b',
            '<u>B</u>ook Title':'span,RefBookTitle,inline',
            '<u>C</u>hapter Title':'span,RefChapterTitle,inline',
            'Publisher <u>N</u>ame':'span,RefPublisherName,inline',
            'Publisher <u>L</u>ocation':'span,RefPublisherLoc,inline',
            '<u>E</u>dition':'span,RefEdition,inline'
        },
        '<u>C</u>ONF':{
            'sk': 'alt+c',
            'Conf <u>N</u>ame':'span,RefConfName,inline',
            'Conf <u>T</u>itle':'span,RefConfArticleTitle,inline',
            'Conf <u>L</u>oc':'span,RefConfLoc,inline',
            'Conf <u>D</u>ate':'span,RefConfDate,inline',
            '<u>S</u>ource':'span,RefSource,inline',
        },
        '<u>S</u>oftware':{
            'sk': 'alt+s',
            '<u>S</u>oft Name':'span,RefSoftName,inline',
            'Soft <u>V</u>er':'span,RefSoftVer,inline',
            'Soft Man<u>L</u>oc':'span,RefSoftManLoc,inline',
            'Soft Man<u>N</u>ame':'span,RefSoftManName,inline',
        },
        'O<u>t</u>her Types':{
            'sk': 'alt+t',
            'Thesis':'span,RefThesis,inline',
            'Patent Title':'span,RefPatentTitle,inline',
            'Patent Num':'span,RefPatentNum,inline',
            'Data Title':'span,RefDataTitle,inline',
            'Data Source':'span,RefDataSource,inline',
            'Periodical Title':'span,RefPeriodicalTitle,inline',
            'Periodical Source':'span,RefPeriodicalSource,inline',
            'PrePrint Title':'span,RefPrePrintTitle,inline',
            'PrePrint Source':'span,RefPrePrintSource,inline',
            'RefPrePrint Link':'span,RefPrePrintLink,inline',
        },
        '<u>O</u>thers':{
            'sk': 'alt+o',
            '<u>Y</u>ear': 'span,RefYear,inline',
            '<u>V</u>olume': 'span,RefVolume,inline',
            '<u>I</u>ssue': 'span,RefIssue,inline',
            '<u>F</u>page': 'span,RefFPage,inline',
            '<u>L</u>page': 'span,RefLPage,inline',
            '<u>E</u>location': 'span,RefELocation,inline',
            '<u>S</u>erial No.': 'span,RefSlNo,inline',
            '<u>W</u>ebSite':'span,RefWebSite,inline',
            '<u>D</u>OI':'span,RefDOI,inline',
            '<u>P</u>MID':'span,RefPMID,inline',
            'P<u>M</u>CID':'span,RefPMCID,inline',
            'L<u>A</u>D': 'span,RefLAD,inline',
        },
        'Co<u>m</u>ments':{
            'sk': 'alt+m',
            '<u>C</u>omment': 'span,RefComments,inline',
            '<u>I</u>nComment': 'span,RefInComment,inline',
            '<u>E</u>ditorEdComment': 'span,RefEditorEdComment,inline',
            '<u>T</u>hesisComment': 'span,RefThesisComment,inline',
            '<u>F</u>PageComment': 'span,RefFPageComment,inline',
        },
        'Delete': 'span,del,inline',
        'Untag': 'p,jrnlUntagged,block'
    }
};
var hoverMenu = $('<div data-type="popUp" data-component="contextMenu" data-selection="true" class="autoWidth hidden bottom z-depth-2">');
	$(hoverMenu).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
	var di = 1;
	$.each(styles, function(index, items) {
		$.each(items, function(key, method) {
			if (typeof(method) == "object"){
				var btnGroup = $('<span class="btn btn-hover" data-type=" ' + index + ' " title="Journal Group">');
				var sk = '';
				if (method['sk'] != undefined){
					sk = ' data-shortcut="' + method['sk'] + '"';
					delete method['sk'];
				}
				//$(btnGroup).append('<span class="btn btn-hover dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"' + sk + '>'+ key + '<span class="caret"></span></span>');
				$(btnGroup).append('<a class="btn dropdown-button" href="#" data-activates="drop-down-' + key.replace(/ /g, '') + di +'"' + sk + '>' + key + '<i class="material-icons">arrow_drop_down</i></a>');
				$(btnGroup).append('<ul class="dropdown-content" id="drop-down-' + key.replace(/ /g, '') + di++ + '">');
				$.each(method, function(k,m){
					if (k == "Author-old"){
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.parseAuthors();">' + k + '</a></li>');
					}else{
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m + '\');">' + k + '</a></li>');
					}
				});
				$(hoverMenu).append(btnGroup);
			}else{
				$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\''+method+'\')">'+key+'</span>');
			}
		});
	});

	$(hoverMenu).append('<span class="btn btn-hover" data-type=" body " data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'markAsCitation\'}" data-selection="true">Mark as Citation</span>');
	$(hoverMenu).append('<span class="btn btn-hover" data-type=" body " data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'unwrapPattern\'}" data-pattern="true">Not a pattern</span>');
$(hoverMenu).append('<span class="btn btn-hover" data-type=" body " data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'setAttribDataTableContinue\'}" data-selection="true">Continue as table</span>');
	$('.templates').append(hoverMenu);

	var hoverMenu = $('<div data-type="popUp" data-component="jrnlRefText" data-selection="true" class="autoWidth hidden z-depth-2 bottom">');
	$(hoverMenu).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
	var referenceTypes = ['Journal', 'Book', 'Conference', 'Thesis', 'Patent', 'Data', 'Software', 'Website', 'Report', 'Newspaper'];
	var referenceSelector = $('<select class="btn btn-hover referenceTypes" data-channel="components" data-topic="general" data-event="change" data-message="{\'funcToCall\': \'changeRefType\'}"></select>');
	$.each(referenceTypes, function(k, v) {
		$(referenceSelector).append('<option value="' + v + '">' + v + '</option>');
	});
	$(hoverMenu).append(referenceSelector)
	$.each(refStyles, function(index, items) {
		$.each(items, function(key, method) {
			if (typeof(method) == "object" && method.constructor !== Array){
				var btnGroup = $('<span class="btn btn-hover" data-type=" ' + index + ' " title="Journal Group">');
				if (method['type'] != undefined){
					btnGroup.attr('data-ref-type', ' ' + method['type'] + ' ');
					delete method['type'];
				}
				var sk = '';
				if (method['sk'] != undefined){
					sk = ' data-shortcut="' + method['sk'] + '"';
					delete method['sk'];
				}
				$(btnGroup).append('<a class="btn dropdown-button" href="#" data-activates="drop-down-' + di +'"' + sk + '>' + key + '<i class="material-icons">arrow_drop_down</i></a>');
				$(btnGroup).append('<ul class="dropdown-content" id="drop-down-' + di++ + '">');
				$.each(method, function(k,m) {
					var sk ='';
					var myRegexp = /<u>([a-z])<\/u>/i;
					if (myRegexp.test(k)){
						var match = myRegexp.exec(k);
						sk = ' data-shortcut="' + match[1].toLocaleLowerCase() + '"';
					}
					if (m.constructor == Array){
						btnGroup.find('ul').append('<li data-ref-type=" ' + m[1] + ' "><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m[0] + '\');"' + sk + '>' + k + '</a></li>');
					}else{
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m + '\');"' + sk + '>' + k + '</a></li>');
					}
				});
				$(hoverMenu).append(btnGroup);
			}else{
				if (method.constructor == Array){
					if (method[1] == undefined){
						$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method[0] + '\')">'+key+'</span>');
					}else{
						$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method[0] + '\')" data-ref-type=" ' + method[1] + ' ">'+key+'</span>');
					}
				}else{
					$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method + '\')">'+key+'</span>');
				}
			}
		});
	});
	$('[data-component="contextMenu"]').append('<span class="btn btn-hover" data-type=" front " title="Journal Group"><a class="btn dropdown-button" href="#" data-activates="drop-down-Authorslist1">Author list<i class="material-icons">arrow_drop_down</i></a><ul class="dropdown-content" id="drop-down-Authorslist1"></ul></span>');
	$('.front .jrnlAuthors .jrnlAuthor').each(function(){
		var authorname = $(this).text()
		$('ul#drop-down-Authorslist1').append('<li><a href="javascript:;" onclick="stylePalette.actions.correspMapping(\''+authorname+'\');">'+authorname+'</a></li>')
	})
	//$(hoverMenu).append('<span class="btn btn-hover" data-type=" jrnlRefText " onclick="stylePalette.actions.clearFormatting()" data-selection="true">Clear format</span>');
	$('[data-component="contextMenu"]').append('<span class="btn btn-hover" data-type=" front " onclick="stylePalette.actions.clearFormatting()" data-selection="true">Clear format</span>');
	//$('.templates').append(hoverMenu);
	$('body').append('<span class="class-highlight hidden" style="position: absolute;bottom: 6px;display: inline-block;height: 25px;width: 200px;right: 10px;background: #eee;border: 1px solid #ddd;font-size: 12px;line-height: 110%;padding:5px;color:#424242;"></span>');
	$('.btn.dropdown-button:contains("Corresp")').parent().attr('data-type', ' hide ');
	if ($('.body[data-edited="true"]').length > 0){
		$('.btn.dropdown-button:contains("Corresp")').parent().attr('data-type', ' front ')
	}