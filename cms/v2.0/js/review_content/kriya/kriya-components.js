/* kriya loader JS */
/* Loading content */

(function () {

    // 1. PRIVATE INITIALIZER
    function init() {
        kriya.config  = config;
		kriya.actions = actions;
		
		kriya.config.content.doi = $('#contentContainer').attr('data-doi');
		kriya.config.content.customer = $('#contentContainer').attr('data-customer');
		kriya.config.content.project = $('#contentContainer').attr('data-project');
		kriya.config.content.stage = $('#contentContainer').attr('data-stage');
		kriya.config.content.stageFullName = $('#contentContainer').attr('data-stage-name');
		kriya.config.content.role = $('#contentContainer').attr('data-role');
		kriya.classes();
        kriya.components();
        kriya.events();
	}

	//#249 - if middle mouse button is clicked, event.which is undefined - Prabakaran. A (prabakaran.a@focusite.com)
	$('body').mousedown(function(e){
		kriya.pasteHandleEvent = e;
	});
	$('body').keydown(function(e){
		kriya.pasteHandleEvent = e;
	});
	//End of #249

    // 2. CONFIGURATION OVERRIDE
    var config = {
		viewMode: "web", // "web", "print", "pdf"
		comps: {
			editable : false, // based on user, comp
		},
		containerElm        : "#contentDivNode",
		metaElm             : ".front",
		blockElements       : ['p','h1','h2','h3','h4','h5','h6','ul','ol','div'],
		activeElements      : "",
		nonEditableElemets  : "",  //Set the content editable false
		//Selector to prevent the typing and drag and drop elements
		//#295 - All | Components | Components popup | All Browsers - Prabakaran.A (prabakaran.a@focusite.com)
		preventTyping       : ".jrnlArtType,.jrnlSubject,.jrnlAuthors,.jrnlGroupAuthors,.jrnlAffGroup, .jrnlHistory,.jrnlEditedByHead,.jrnlEditorGroup,.jrnlReviewerGroup,.jrnlCorrAff,.jrnlPresAdd, .jrnlEqContrib, .award-group, .jrnlFigure, .jrnlInlineFigure, .jrnlSupplSrc, .jrnlCitation, .jrnlEthicsFN[data-track='del'], .removeNode, .floatHeader, .jrnlEthicsGroup .jrnlFNHead, .jrnlMajorDatasets .jrnlDatasetHead, .jrnlMajorDatasets .jrnlDatasets, .jrnlKwdGroup .jrnlKeywordHead, .jrnlPermission, [data-editable='false']:not(.jrnlRefText), *[data-track='del'], .jrnlDeleted, .jrnlPubDate, .jrnlENDNotes, .linkCount,.markAs,.linkNowBtn,.jrnlTblFootHead,.jrnlObjectID,.leftHeader,.headerRow, .jrnlTblDefItem, .jrnlRelArtGroup, .jrnlRRH, .jrnlLRH",
		preventTypingErMsg  : {
			'jrnlArtType'      : 'Dear {$role}, please make changes to article type by clicking on the article type.',
			'jrnlSubject'      : 'Dear {$role}, please make changes to subject by clicking on the subject.',
			'jrnlAuthors'      : 'Dear {$role}, please make changes to author by clicking on the author name .',
			'jrnlGroupAuthors' : 'Dear {$role}, please make changes to group author by clicking on the group author name.',
			'jrnlAffGroup'     : 'Dear {$role}, please make changes to affiliation by clicking on the affiliation.',
			'jrnlRefText'        : 'Dear {$role}, please make changes to reference by clicking on the reference.',
			'jrnlHistory'        : 'Dear {$role}, please make changes to history by clicking on the history.',
			'jrnlEditorGroup'    : 'Dear {$role}, please make changes to editor by clicking on the editor name.',
			'jrnlReviewerGroup'  : 'Dear {$role}, please make changes to reviewer by clicking on the reviewer name.',
			'jrnlCorrAff'        : 'Dear {$role}, please make changes to contact details by clicking on the author name in the author list.',
			'jrnlPresAdd'        : 'Dear {$role}, please make changes to present address by clicking on the present address.',
			'award-group'        : 'Dear {$role}, please make changes to funding information by clicking on the funding information.',
			'jrnlMajorDatasets'  : 'Dear {$role}, please make changes to major datasets by clicking on the major datasets.',
			'jrnlPubDate'        : 'Dear {$role}, please make changes to pub date by clicking on the pub date.',
			'jrnlKwdGroup'       : 'Dear {$role}, please make changes to keyword by clicking on the keyword.',
			'jrnlENDNotes'       : 'Dear {$role}, please make changes to end note by clicking on the end note.',
			'jrnlTblDefItem'     : 'Dear {$role}, please make changes to def item by clicking on the def item.',
			'jrnlDefItem'     : 'Dear {$role}, please make changes to def item by clicking on the def item.'
		},
		//End of #295
		invalidSaveNodes    : ".front,.body,.back,#contentDivNode,#contentContainer,#dataContainer,#historyDivNode,#infoDivContent,#navContainer,#pubData,#queryDivNode,#welcomeContainer,#openQueryDivNode, #resolvedQueryDivNode, #compDivContent, #headerContainer, #pdfviewContainer, #editorContainer", //Selectors to prevent from saving when get closest id
		preventSiblingId    : ".jrnlVolume,.jrnlIssue,.jrnleLocation,.jrnlFPage,.jrnlLPage,.jrnlLRH,.jrnlRRH,.jrnlEthicsFN,.jrnlPPubDate,.jrnlPubDate,.jrnlSplIssue", // Prevent adding prev id and next id when save the content
		sameClassSiblingId  : ".jrnlCorrAff",
		activeBlockElement  : [],
		activeChildElements : [],
		saveQueue           : [],
		errorSaveQueue      : [],
		content             : {},
		exportOnsave        : null,
		citationSelector    : ".jrnlFigRef,.jrnlTblRef,.jrnlSupplRef,.jrnlVidRef,.jrnlBoxRef,.jrnlBibRef,.jrnlFootNoteRef,.jrnlSchRef, .jrnlMapRef", //Citation selector used to get the citation nodes
		preventTrackComp    : '.jrnlFigRef,.jrnlSchRef,.jrnlMapRef,.jrnlInlineFigure,.jrnlTblRef,.jrnlSupplRef,.jrnlVidRef,.jrnlBoxRef,.jrnlBibRef,.jrnlFootNoteRef,.jrnlEqnRef,.jrnlTblFNRef, .jrnlKeyword, .jrnlFigure, .jrnlAuthors, .jrnlGroupAuthors, .jrnlEditors, .jrnlReviewers, .award-group, .jrnlFN, .jrnlFNPara', //Selector to prevent showing the accept/reject popup in specific component
		checkForComp		: '.jrnlAff, .jrnlPresAdd, .jrnlDatasets, .jrnlKeyword, .jrnlEthicsFN, .jrnlDefItem, .kriyaFormula, .jrnlFN', //Selector to prevent showing the accept/reject popup in specific component
		preventMerging      : '.jrnlArtTitle, .jrnlSubDisplay', //Selector to prevent merging blocks in delete and backspace
		preventFormatting   : '[data-editable="false"]:not(.jrnlRefText)', //Selector to prevent formatting for specific nodes
		preventJrnlDelete	: '.jrnlArtTitle', //selector to prevent jrnlDeleted class
		preventCasing       : '.jrnlArtType,.jrnlSubject,.jrnlAuthors,.jrnlGroupAuthors,.jrnlAffGroup,.jrnlHistory', // selector to prevent casing
		preventAddId		: '.jrnlAuthors, .jrnlKeywordPara, .removeNode, .jrnlFNText, .jrnlTblFootHead, .jrnlRelArtHead, .floatHeader, .floatHeader *, .jrnlFundingHead, .jrnlKeywordHead',// to prevent add id in editor js (editor js we are adding id to all child nodes before triggering save)
		cslMappingObj		: {} // to store csl mapping object
    };
    // 3. CUT,COPY,PASTE actions
    var actions = {
    	copy : function(){
    		document.execCommand('copy');
    	},
    	cut : function(){
    		if(kriya.selection){
    			var selectedText = kriya.selection.cloneContents();
    			document.execCommand('copy');
    			tracker.deleteContents();

    			var curRange = tracker.getCurrentRange();
    			kriyaEditor.settings.undoStack.push(curRange.startContainer);
    			

			    /*var span = document.createElement("span");
			    span.appendChild(selectedText);
			    kriya.selection.insertNode(span);
			    $(span).kriyaTracker('del');
		    	kriyaEditor.settings.undoStack.push(span);*/

		    	kriyaEditor.init.addUndoLevel('cut-menu');
    		}
    	},
    	paste : function(type){
    		if(type == "html"){
    			
    			var pasteNode = document.createElement('div');
  				pasteNode.setAttribute('class', 'kriya-paste-div');
  				pasteNode.setAttribute('contenteditable', 'true');

  				$(pasteNode).html('&nbsp;');
  				$('body').append(pasteNode);
  				
  				var editorRange = tracker.getCurrentRange();

  				var range = document.createRange();
				range.setStart(pasteNode, 0);
				range.setEnd(pasteNode, 1);
				var winSel = window.getSelection();
				winSel.removeAllRanges();
				winSel.addRange(range);
				//return false;
    			document.execCommand('paste');
    			
    			$(pasteNode).cleanTrackChanges();
    			var pasteHTML = pasteNode.innerHTML;
    			$(pasteNode).remove();
    			pasteHTML = cleanupOnPaste(pasteHTML);
    			// if(citeJS && typeof(citeJS.general.captureCitation) == "function"){
			    //     pasteHTML = citeJS.general.captureCitation(pasteHTML);
			    // }
    			var pasteNodes = $.parseHTML(pasteHTML);
    			$(pasteNodes).each(function(){
    				tracker.insert(this, editorRange);
    			});
    			var saveNode = $(pasteNodes).parents('[id]:first');
    		}else if(window.clipboardData && window.clipboardData.getData){
    			var pasteText = window.clipboardData.getData('Text');
    			var range = tracker.getCurrentRange();
    			// if(citeJS && typeof(citeJS.general.captureCitation) == "function"){
			    //     pasteHTML = citeJS.general.captureCitation(pasteHTML);
			    // }
			    tracker.insert(pasteText, range);
			    var saveNode = $(pasteNodes).parents('[id]:first');
    		}
    		kriyaEditor.settings.undoStack.push(saveNode);
    		kriyaEditor.init.addUndoLevel('paste-menu');
    	},
    	/**
    	 * Function to execute the command
    	 * @param arguments[0] - function name or command to execute
    	 * @param arguments[1] - parameters to the function
    	 */
    	execCommand: function(){
    		var range = rangy.getSelection();
			var targetElement = range.anchorNode;
			targetElement = $(targetElement).closest('[id]');
    		if(arguments[0]){
    			if(arguments[0].match(/cut|copy|paste/) && arguments[0]){
    				if(document.queryCommandEnabled(arguments[0])){
    					this[arguments[0]](arguments[1]);
						return;	
    				}else{
    					kriya.notification({
							title : 'ERROR',
							type  : 'error',
							timeout : 8000,
							content : "Your browser doesn't support direct access to the clipboard. Please use the keyboard shortcuts (Ctrl+X/C/V) instead.",
							icon: 'icon-warning2'
						});
    				}
				}
    			if(arguments[0].match(/indent|outdent|justify/)){
    				var saveNode = null; status=false;
					if (targetElement[0].nodeName == "TD" || $(targetElement).closest('td,th').length > 0){
						var textIndent = (!$(targetElement).closest('td,th').attr('text-indent') || $(targetElement).closest('td,th').attr('text-indent')<=0) ? 0 : $(targetElement).closest('td,th').attr('text-indent');
						if (arguments[0] == "indent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html('&emsp;'+$(this).html());
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)+1);
							});
						}else if (arguments[0] == "outdent"){
							$('.wysiwyg-tmp-selected-cell').find('p').each(function(){
								$(this).html($(this).html().replace(/^\u2003/, ''));
								$(this).closest('td,th').attr('text-indent',parseInt(textIndent)-1);
							})
						}
						$("td[text-indent]").each(function() {
						    if($(this).attr("text-indent") <= 0) $(this).removeAttr('text-indent');
						});
						saveNode = $('.wysiwyg-tmp-selected-cell').closest('table');
					}else if (targetElement[0].nodeName == "LI" || $(targetElement).closest('li').length > 0 || $(targetElement).siblings('li').prevObject.length > 0){
						if (arguments[0] == "indent"){
							status=editor.composer.commands.exec('indentList');
						}else if (arguments[0] == "outdent"){
							

						
							var paraEl = editor.composer.commands.exec('outdentList');
							
						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//var parentEle = $(targetElement).closest('ul,ol')[0].parentElement;
                        
                        //On Indent or outdent the next or the previous list style should be applied. - Lakshminarayanan. S(lakshminarayanan.s@focusite.com)
							var optionTexts = [];
							if($(targetElement).closest('ul').length == 0){
								$("#order-list-dropdown li").each(function(key,val) { 
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');
									
									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument)
									});
							} else{
					
								$("#unorder-list-dropdown li").each(function(key,val) { 
									var a = $(this).data('message');
									a = a.replace(/\'/g, '\"');
									if(optionTexts.indexOf(JSON.parse(a).click.param.argument) == -1 && JSON.parse(a).click.param.argument != "none")
									optionTexts.push(JSON.parse(a).click.param.argument) 
								});
							}
						if (!status && arguments[0] == "indent"){
							var listStyle = optionTexts[(optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType)+1)%optionTexts.length];
							$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							$(targetElement).closest('ul,ol')[0].type = listStyle;
                            kriyaEditor.settings.undoStack.push($(targetElement).closest('ul,ol').parents('ul,ol')[0]);
						}else {
							//var listStyle = optionTexts[optionTexts.indexOf($(targetElement).closest('ul,ol')[0].parentElement.style.listStyleType)-1];
							//$(targetElement).closest('ul,ol')[0].style.listStyleType = listStyle;
							//$(targetElement).closest('ul,ol')[0].type = listStyle;
						}
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')[0].parentElement.parentElement.style.listStyleType;
						//saveNode = [$(targetElement).closest('ul,ol')[0].parentElement.parentElement];
						//$(targetElement).closest('ul,ol')[0].style.listStyleType = $(targetElement).closest('ul,ol')//[0].parentElement.parentElement.style.listStyleType;
						saveNode = [$(targetElement).closest('ul,ol')[0]];
					}else{
						saveNode = targetElement;
						document.execCommand(arguments[0]);
					}
					if(typeof saveNode !=undefined && saveNode[0]!=undefined){
					//#535-Duplication happens after refresh while indend-Helen.j@focusite.com
					saveNode = $(saveNode).parent().closest('ol,ul[id]').length > 0 ? $(saveNode).parent().closest('ol,ul[id]') : saveNode;
					//End of #535
					kriyaEditor.settings.undoStack.push($(saveNode)[0]);}
					kriyaEditor.settings.undoStack = kriyaEditor.settings.undoStack.concat(paraEl);
					kriyaEditor.init.addUndoLevel('formatting');
					return;
    			}else{
    				//Handling list
	    			if(arguments[0].match(/list/i)){
						var listNode = null;
					if($(range.anchorNode).parents('ul,ol').length > 0){
	    					listNode = $(range.anchorNode).parents('ul,ol').first();
							listNode.attr('type', arguments[1]);
							listNode.css('list-style-type', arguments[1]);
							//rename the elemet if list type is change
                            //#18 - For create tmpRange for change bullets after none by - Vimala.J ( vimala.j@focusite.com)
                            var tmpRange = rangy.saveSelection();
							if(arguments[0].match(/unOrderedList/i)){
								$(listNode).renameElement('ul');
							}else{	
								$(listNode).renameElement('ol');  
							}
                            //#18 - send tmpRange restoreselection by - Vimala.J ( vimala.j@focusite.com)
							rangy.restoreSelection(tmpRange);
							// End of #18 
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}else{
	    					editor.composer.commands.exec(arguments[0], arguments[1]);
	    					listNode = $(targetElement).closest('ul,ol').first();
							listNode.css('list-style-type', arguments[1]);
							if (! $(listNode)[0].hasAttribute('id')){
								$(listNode).attr('id', uuid.v4());
							}
							$(listNode).find('li,p').each(function(){
								if (! $(this)[0].hasAttribute('id')){
									$(this).attr('id', uuid.v4());
								}
								$(this).attr('class','jrnlListPara');
							});
							$(listNode).attr('data-inserted', 'true');
							$(targetElement).filter('h1, h2, h3, h4, h5, h6').each(function(){
								eventHandler.menu.style.renameNode($(this), 'p', 'jrnlListPara');
							});
							$(listNode).find('li p, li h1, li h2, li h3, li h4, li h5, li h6').each(function(){
								var clonedPara = $(this).clone(true);
								clonedPara.attr('data-removed', 'true');
								kriyaEditor.settings.undoStack.push(clonedPara[0]);
							});
							kriyaEditor.settings.undoStack.push($(listNode)[0]);
	    				}
					
						kriyaEditor.init.addUndoLevel('formatting');
						createCloneContent();
						return;
					
	    		}
	    			//editor.composer.commands.exec(arguments[0], arguments[1]);
	    			/*if(!tracker || tracker.length < 1){
    					var formatedNode = $(window.getSelection().getRangeAt(0).startContainer.parentElement);
    					formatedNode.kriyaTracker('sty');
	    			}else if(kriya.selection.text() == kriya.selection.startContainer.parentElement.textContent){
	    				var cid = $(kriya.selection.startContainer.parentElement).attr('data-cid');
						$('#updDivContent'+' [data-rid="' + cid + '"]').find('.reject-changes').trigger('click');
	    			}*/
    			}
    			//kriyaEditor.init.addUndoLevel('formatting', targetElement);
			}
    	},
    	confirmation: function(param,targetNode){
			/*var sampleSettings = {
				'icon'  : '<i class="material-icons">warning</i>',
				'title' : 'Confirmation Needed',
				'text'  : 'Are you sure do you want to delete?',
				'size'  : 'pop-sm',
				'buttons' : {
					'ok' : {
						'text' : 'Yes',
						'class' : 'btn-success',
						'message' : "{'click':{'funcToCall': 'deleteElement','channel':'components','topic':'general'}}"
					},
					'cancel' : {
						'text' : 'No',
						'class' : 'btn-danger',
						'message' : "{'click':{'funcToCall': 'closePopUp','channel':'components','topic':'PopUp'}}"
					}
				}
 			}*/

 			// If targetNode is not null then hidden the popup and pass the closest popup - rajesh
 			if(targetNode=='equalContrib'){
				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], kriya.config.containerElm);
			 }
			 else if(targetNode){
				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], targetNode.closest('[data-type="popUp"]')[0]);
			 }
			 else{
				if(!param.subcomp){
					$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');	
				}
 				kriya.popUp.getSlipPopUp($('[data-component="confirmation_edit"]')[0], kriya.config.containerElm);
 			}
    		if(param.size){
    			$('[data-component="confirmation_edit"]').attr('data-pop-size', param.size);
    		}
    		if(param.icon){
    			$('[data-component="confirmation_edit"] .popupHead .popIcon').html(param.icon);
    		}
    		if(param.title){
    			$('[data-component="confirmation_edit"] .popupHead .popTitle').html(param.title);
    		}
    		if(param.text){
				$('[data-component="confirmation_edit"] .popupBody .popUpText').html(param.text);
			}
			if(param.hideElement){ // to display text in footer of confirmation popup
				$('[data-component="confirmation_edit"]').find('.'+param.hideElement).addClass('hidden');
			}
			if(param.footerMsg){
				$('[data-component="confirmation_edit"] .popupFoot').html('');
				$('[data-component="confirmation_edit"] .popupFoot').append(param.footerMsg);
			}else{
				$('[data-component="confirmation_edit"] .popupFoot').html('');
				if(param.buttons && typeof(param.buttons) != "string"){
					$.each(param.buttons, function(i,val){
						var newBtn = $('<span class="btn btn-medium" />');
						if(val.class){
							newBtn.addClass(val.class);
						}
						if(val.message){
							newBtn.attr('data-message', val.message);
						}
						if(val.attr){
							$.each(val.attr, function(i, val){
								newBtn.attr(i, val);
							});
						}
						if(val.text){
							newBtn.html(val.text);
						}
						$('[data-component="confirmation_edit"] .popupFoot').append(newBtn);
					});
				}
			}
    	},
		removeSelectionFromTableCell: function(){
			var selectedCells = $('.wysiwyg-tmp-selected-cell');
			if (selectedCells.length > 0) {
				//when unselect cells ned to null the start and end cell-kirankumar
				wysihtml.dom.startcell = null;
				wysihtml.dom.endcell = null;
				for (var i = 0; i < selectedCells.length; i++) {
					wysihtml.dom.removeClass(selectedCells[i], 'wysiwyg-tmp-selected-cell');
				}
			}
		}
    };
    // 4. GLOBAL NAMESPACE
    window.kriya = {
        // allows optional configuration on the object
        classes: function () {
            return {
               // moduleOne: new kriya.ModuleOne(),
               // moduleTwo: new kriya.ModuleOne(config.modTwo)
			   // utilsPack: new kriya.kriyaUtils()
            };
        },
        notification: function(param){
        	var id = uuid.v4();
        	var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
			notice.append('<div class="row kriya-notice-header" />');
			notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');

			if(param.icon){
				notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
			}
			if(param.title){
				notice.find('.kriya-notice-header').append(param.title);
			}
			
			if(param.closeIcon != false){
				notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="kriya.removeNotify(this)"></i>');
			}
			
			
			Materialize.toast(notice[0].outerHTML, param.timeout);
			return id;
        },
        removeNotify: function(target){
        	if(target){
        		$(target).closest('.toast').fadeOut(function(){
			        $(this).remove();
			    });
        	}
        },
        removeAllNotify: function(){
        	$('.toast').fadeOut(function(){
		        $(this).remove();
		    });
        },
        // has private configuration
        components: function () {
			/*$.getJSON( "./js/kriya/components.json", function( data ) {
			  	kriya.componentList = data;
			  	console.log(data); 
			});*/
			kriya.componentList = {
				'jrnlArtTitle' : {'type': 'contentEditable'},
				'jrnlAbsPara'  : {'type': 'contentEditable'},
				'jrnlBoxText'  : {'type': 'contentEditable'},
				'jrnlCorrAff'  : {'type': 'contentEditable'},
				'jrnlSecPara'  : {'type': 'contentEditable'},
				'jrnlFigCaption' : {'type': 'contentEditable'},
				'jrnlTblCaption' : {'type': 'contentEditable'},
				'jrnlListPara'   : {'type': 'contentEditable'}, 
				'jrnlTblFoot'    : {'type': 'contentEditable'},
				'jrnlFigFoot'    : {'type': 'contentEditable'},
				'jrnlHead1'      : {'type': 'contentEditable'},
				'jrnlHead2'      : {'type': 'contentEditable'},
				'jrnlHead3'      : {'type': 'contentEditable'},
				'jrnlHead4'      : {'type': 'contentEditable'},
				'jrnlHead5'      : {'type': 'contentEditable'},
				'jrnlHead6'      : {'type': 'contentEditable'},
				'jrnlPara'       : {'type': 'contentEditable'},
				'jrnlFNHead'	: {'type': 'contentEditable'},
				'jrnlFundingHead'	: {'type': 'contentEditable'},
				'jrnlDatasetHead'	: {'type': 'contentEditable'},
				'jrnlKeywordHead'   : {'type': 'contentEditable'},
				'jrnlAbsTitle'      : {'type': 'contentEditable'},
				'jrnlBoxCaption'    : {'type': 'contentEditable'},
				'floatLabel'        : {'type': 'contentEditable'},
				'jrnlSupplCaption'  : {'type': 'contentEditable'},
				'jrnlVidCaption'    : {'type': 'contentEditable'},
				'jrnlAbsHead'    	: {'type': 'contentEditable'},
				'jrnlRRH'       	: {'type': 'contentEditable'},
				'jrnlBiography'    	: {'type': 'contentEditable'},
				'jrnlUncitedRef'  	: {'type': 'contentEditable'},
				'jrnlPossBibRef' : {
					'type': 'popUp',
					'class': 'autoWidth',
					'buttons': {
						'Add/Cite Reference': {
							'channel': 'menu', 'topic': 'insert', 'event': 'click', 'message': "{'funcToCall': 'newCite', 'param': {'method': 'ADD', 'type': 'QUERY', 'value': ''}}"
						}
					}
				},
				'jrnlRefHead'	: {'type': 'contentEditable'}
			};
			

			$('.templates *[data-component][data-type]').each(function(){
				kriya.componentList[$(this).attr('data-component')] = [];
				kriya.componentList[$(this).attr('data-component')].type = $(this).attr('data-type');
			});
			//Add buttons in content
			if ($('#contentContainer').attr('data-state') != 'read-only'){
				/*var contentBtns = $('#compDivContent [data-component="contentButtons"] [data-xpath]');
				contentBtns.each(function(){
					var xpath = $(this).attr('data-xpath');
					//var btnContainer = kriya.xpath(xpath);
					var clonedNode = $(this).clone(true);
					if($(btnContainer).length > 0){
						$(btnContainer).append(clonedNode);
					}
				});
				$.each(kriya.config.content.newBtnContainers, function(i, val){
					var topic = 'PopUp';
					if (val.topic) topic = val.topic;
					var eventMessage = 'data-channel="components" data-topic="' + topic + '" data-event="click" data-message="{\'funcToCall\': \''+val.func+'\', \'param\' : {\'component\' : \''+val.component+'\'}}"';
					if(typeof(val.eventMessage) != "undefined"){
						eventMessage = (val.eventMessage != "")?'data-message="' + val.eventMessage + '"':'';
					}
					var className = 'btn btn-small lighten-1 contentBtn';
					if (val.condition && $(val.condition).length > 0) className += ' hidden';
					var btnNode = $('<span class="' + className + '" ' + eventMessage + ' contenteditable="false"></span>');
					if(val.icon){
						btnNode.append('<i class="' +val.icon+ '" />');
					}
					if(val.btnText){
						btnNode.append(val.btnText);
					}
					//append the button if the role is not defined or if the reviewer is defined in the config
					if(!val.role || (val.role && kriya.config.content.role && val.role.split(',').indexOf(kriya.config.content.role) >= 0)){
						$(config.containerElm+' .'+val.container).append(btnNode);
					}
				});*/
			}
			//Add content editable false to nonEditable elements
			if(kriya.config.nonEditableElemets){
				$(kriya.config.nonEditableElemets).attr('contenteditable', 'false');
			}
			$(config.containerElm).css('overflow-y', 'auto');
		},

        // has private configuration
        events: function () {
			/*$('body').on({
				click: function (evt) {
					$('.content-scrollbar .node').remove();
					if ($(this)[0].hasAttribute('id')){
						if ($('#contentDivNode [data-id="' + $(this).attr('id') + '"]').length == 0){
							var id = $(this).attr('id').replace('BLK_', '');
							if ($('#contentDivNode [data-id="' + id + '"]').length > 0){
								$('#contentDivNode [data-id="' + id + '"]')[0].scrollIntoView();
							}
						}else{
							$('#contentDivNode [data-id="' + $(this).attr('id') + '"]')[0].scrollIntoView();
						}
						var citationType = $(this).attr('data-type');
						if (typeof(commonjs.settings.citationClass[citationType]) != 'undefined'){
							var citeClass = commonjs.settings.citationClass[citationType]['floatCitationClass'];
							elementOnScrollBar($('#contentDivNode ' + citeClass + '[data-citation-string*="' + $(this).attr('id').replace('BLK_', '') + ' "]'), 'data-citation-string', citationType);
						}
					}else if ($(this)[0].hasAttribute('data-panel-id') && $('#contentDivNode [data-id="'+ $(this).attr('data-panel-id') +'"]').length > 0){
						$('#contentDivNode [data-id="'+ $(this).attr('data-panel-id') +'"]')[0].scrollIntoView();
						var citationType = $(this).attr('data-type');
						if (typeof(commonjs.settings.citationClass[citationType]) != 'undefined'){
							var citeClass = commonjs.settings.citationClass[citationType]['floatCitationClass'];
							elementOnScrollBar($('#contentDivNode ' + citeClass + '[data-citation-string*="' + $(this).attr('data-panel-id') + ' "]'), 'data-citation-string', citationType);
						}
					}
				}
			}, '#navContainer .resource_reference, #navContainer .jrnlFigBlock, #navContainer .jrnlTblBlock, #navContainer .card');*/
			

			//reset the go back history added by vijayakumar on 5-10-2018
			history.pushState(null, null, $(location).attr('href'));
			window.addEventListener('popstate', function () {
			history.pushState(null, null, $(location).attr('href'));
			});
			
			$(config.containerElm).find('divs > * > *').on('focusin', function (evt) {
				//evt.preventDefault();
				var target = evt.target || evt.srcElement;
				kriya.evt = evt;
				kriya.activeBlockElement = [];
				if ($(target).attr('id') != undefined){
					kriya.activeBlockElement[0] = target;
				}else{
					while (config.blockElements.indexOf(target.nodeName.toLocaleLowerCase()) == -1){
						var target = target.parentNode;
					}
					if ($(target).closest('.front').length > 0){
						kriya.activeBlockElement[0] = target;
					}else if ($(target).attr('id') != undefined){
						kriya.activeBlockElement[0] = target;
					}
				}
				kriya.activeBlockElement[1] = $(kriya.activeBlockElement[0]).html();
			});

			$('body').on({
				focusout: function (evt) {
					if($(this).text() != ""){
						var component = $(this).closest('.carousel-item, [data-type="popUp"]');
						var selector = component.find('.dropdown-link-content').attr('data-source-selector');
						var container = kriya.xpath(selector);
						container = $(container).parent();
						$(this).removeAttr('contenteditable');
						var tagName = $(this).closest('li').attr('data-tag');
						tagName = (tagName) ? tagName : 'p';
						var className = $(this).closest('.collection').attr('data-source');
						var newNode = document.createElement(tagName);
						newNode.setAttribute('class', className);
						newNode.innerHTML = $(this).html();
						$(newNode).find('.material-icons').remove();
						var newID = $(this).closest('li').attr('data-rid');
						newNode.setAttribute('id', newID);
						$(newNode).kriyaTracker('ins');
						container.append(newNode);
					}else{
						$(this).closest('li').remove();
					}
				},
			}, '.collections [contenteditable="true"]');//commented by jai

			$('body').on({
				focusin: function (evt) {
					$(".findAction .searchBtn").removeClass('disabled');
				},
			}, '.searchModal .search');//enable search button on find text box focus - priya #119-#123

			$('body').on({
				keydown: function (evt) {
					if(evt.keyCode==13){
						evt.preventDefault();
					}		
				},
			}, '.searchModal .search, .findModal .replace');//enable search button on find text box focus - priya #119-#123

			$('body').on({
				paste: function (evt){
					var textData = evt.originalEvent.clipboardData.getData('text');
					if(/<[a-z][\s\S]*>/i.test(textData)){
						$(this).html($(textData).text());
					}else{
						$(this).html(textData);
					}
					evt.preventDefault();	
				}
			}, '.searchModal .search, .findModal .replace');

			$('.jrnlTblBlock').scroll(function (evt) {
				var leftPosition = $(evt.target).scrollLeft();
				if($("#clonedContent").find(".highlight[data-tableblock='"+$(evt.target).attr('id')+"']").length>0){
					$("#clonedContent").find(".highlight[data-tableblock='"+$(evt.target).attr('id')+"']").each(function(){
						$(this).css("left",$(this).attr("data-left")-leftPosition+"px");
					});
				}
			});// table horizontal scroll #383 - priya

			$('body').on({
				click: function(evt){
					if($('#specialCharacter.tm-showModal').length>0){
						var range = rangy.getSelection();
						var currentSelection = range.getRangeAt(0);
						if($(currentSelection.startContainer).closest('#contentDivNode , .findSection').length>0){
							$('#specialCharacter').data("currentSelection",currentSelection);
						}	
					}	
				}
			},'#contentDivNode , .findSection'); //set cursor range in attribute - priya #122

			$('body').on({
				click: function(evt){
					if(!($(evt.target).closest('[data-class]').attr('data-class') == "jrnlProName")){
						$('[data-component="productAuthor_add"]').addClass('hidden');		
					}
					if(!($(evt.target).closest('[data-class]').attr('data-class') == "jrnlRelArtAuthor")){
						$('[data-component="RelAuthor_add"]').addClass('hidden');		
					}
				}
			},'[data-component="jrnlProduct_edit"] , [data-component="jrnlRelArt_edit"]'); //to hide delete popup - aravind

			$('body').on({
				click: function(evt){
					if($(this).closest('[data-type="popUp"]').find('.collection-item.active').length == 0){
						kriya.config.citeIds = [];
					}
					if($(this).closest('.collection').attr('multiselect') != "true"){
						$(this).closest('.collection').find('.collection-item').removeClass('active');
						kriya.config.citeIds = [];
					}
					var dataId = $(this).attr('data-rid'); 
					
					var blockNode = $(kriya.config.containerElm).find('[data-id="'+ dataId +'"]');
				/*	if($(blockNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').length > 0){
						dataId = $(blockNode).closest('.jrnlTblBlockGroup, .jrnlFigBlockGroup').attr('data-id');
						dataId = dataId.replace(/BLK_/g, '');
					}*/

					var existIndex = kriya.config.citeIds.indexOf(dataId);
					if($(this).hasClass('active')){
						$(this).removeClass('active');
						kriya.config.citeIds.splice(existIndex, 1);
					}else{
						$(this).addClass('active');
						if(dataId && existIndex < 0){
							kriya.config.citeIds.push(dataId);
						}
					}
				}
			}, '.collection.selectable .collection-item');


			$('body').on({
				keydown: function (evt) {
					if(window.location.pathname == "/peer_review/"){
						var targetNode = evt.target;
						if(evt.keyCode == 13 && !evt.shiftKey){
							if($(targetNode).closest('.query-div').length > 0){
								$(targetNode).closest('.query-div').find('.save-edit, .add-reply').first().trigger('click');
							}else{
								$(targetNode).closest('.commentsField').find('.saveCommentBox').trigger('click');
							}
						}
					}
				},
			}, '#queryDivNode .jrnlCommnetsBox .commentsField, #queryDivNode .query-div .reply-content, #queryDivNode .query-div .query-content[contenteditable="true"], #queryDivNode .query-div .response-content [contenteditable="true"]');

			$('body').on({
				drop: function(evt){
					evt.preventDefault();
					evt.stopPropagation();
					var targetNode = evt.target;
					// Notification popup for drop restrict #277 - priya
					if($(targetNode)[0].nodeName.toLowerCase()=="img" || $(targetNode).closest("img").length>0 || $(targetNode).find("img").length>0){
						var message = '<div class="row">Please use <span class="btn btn-small replaceObject" style="padding:8px 5px;margin:0px;background:#0d618b;">Replace</span> on the corresponding objects like Figure, table to upload a new file.</div>';
					}else{
						var message = '<div class="row">Drop Restricted.</div>';
					}
					kriya.notification({
						title : 'Information',
						type  : 'success',
						content : message,
						icon : 'icon-info'
					});
					return;
				}
			}, '#contentDivNode');

			$('body').on({
				click: function(evt){
					var fIdArray = [];
					if(kriya.config.citeIds){
						fIdArray = $.merge(fIdArray, kriya.config.citeIds);
					}
					var popper = $(this).closest('[data-component]');
					
					var displayBox = popper.find('.selectedCitation');
					if(displayBox.find('option').length > 0){
						displayBox.find('option.indirectCitation').html('Indirect');
						displayBox.find('option.directCitation').html('Direct');
						
						
						var refCiteConfig = citeJS.floats.getConfig('R');
						//Get the indirect citation id list
						var indirectIDList = fIdArray;
						if(refCiteConfig && refCiteConfig.sortfor == "indirect" || !refCiteConfig || !refCiteConfig.sortfor){
							indirectIDList = kriya.general.sortMultiCiteIDs(fIdArray);
							var refSortList = kriya.general.sortRefCitationIDs(indirectIDList);
							if(refSortList){
								indirectIDList = refSortList;
							}
						}

						// Get the indirect citation 
						var citeHTML = kriya.general.getNewCitation(indirectIDList, kriya.config.containerElm, '');
						if(citeHTML){
							displayBox.find('option.indirectCitation').html(citeHTML);
						}
						
						//Get the direct citation id list
						var directIDList = fIdArray;
						if(refCiteConfig && refCiteConfig.sortfor == "direct" || !refCiteConfig || !refCiteConfig.sortfor){
							directIDList = kriya.general.sortMultiCiteIDs(fIdArray);
							var refSortList = kriya.general.sortRefCitationIDs(directIDList);
							if(refSortList){
								directIDList = refSortList;
							}
						}

						// get direct reference citation and append in drop dowm
						var citeHTML = kriya.general.getNewCitation(directIDList, kriya.config.containerElm, true, false);
						if(citeHTML){
							displayBox.find('option.directCitation').html(citeHTML);
						}
					}
				}
			}, '.collection.selectable[data-class$="Ref"] .collection-item');
			
			$('body').on({
				change: function(evt){
					var popper = $(this).closest('[data-component]');
					var refViewNode = $(this).closest('[data-ref-type]:visible');
					var existOnAbsense = $(this).closest('[existOnAbsence]').attr('existOnAbsence');
					var nodeXpath = popper.attr('data-node-xpath');
					var dataNode = kriya.xpath(nodeXpath);
					if(existOnAbsense  && refViewNode.length > 0 && $(dataNode).length > 0){
						existOnAbsense = existOnAbsense.split(' ');
						for(var e=0;e<existOnAbsense.length;e++){
							var className = existOnAbsense[e];
							var absenceNode = refViewNode.find('[data-class="' + className + '"][data-type="htmlComponent"]');
							if($(this).is(':checked')){
								absenceNode.html('');
								absenceNode.closest('.input-field').addClass('hidden');
							}else{
								var absenceNodeXpath = $(absenceNode).attr('data-node-xpath');
								var absenceData = kriya.xpath(absenceNodeXpath, dataNode[0]);
								if($(absenceData).length > 0){
									$(absenceNode).html($(absenceData).html());
								}
								absenceNode.closest('.input-field').removeClass('hidden');
							}
						}
					}
				}
			}, '[data-component="jrnlRefText_edit"] input[data-class="RefInPress"][type="checkbox"]');

			$('body').on({
				click: function(evt){
					if($(this).hasClass('active')){
						$(this).find('input.checkbox[type="radio"]').prop('checked', false);
						$(this).removeClass('active');
					}else{
						$('.refSearchContainer tbody tr').removeClass('active');
						$(this).find('input.checkbox[type="radio"]').prop('checked', true);
						$(this).addClass('active');
					}
				}
			}, '.refSearchContainer tbody tr');

			$('body').on({
				click: function(evt){
					$(this).closest('.dropdown-content').prev().dropdown('close');
				}
			}, '.dropdown-content li');

			//Save a new Element from a pop-up to another list
			$('body').on({
				keydown: function (evt) {
					if (evt.which == 13){
						console.log(evt);
						$('span[contenteditable]:visible').eq( $(this).index('span[contenteditable]:visible') + 1 ).focus();
						evt.preventDefault();
						evt.stopPropagation();
					}
				},
			}, '[data-type="popUp"] span[contenteditable]');

			$('body').on({
				keydown: function (evt) {
					if (evt.which == 13){
						console.log(evt);
						$(evt.target).removeAttr('contenteditable');
						$(evt.target).removeClass('activeElement');
						$('[data-type="popUp"]').addClass('hidden');
						evt.preventDefault();
						evt.stopPropagation();
					}
				},
			}, '.jrnlKeyword.activeElement');
			
			$('body').on({
				keydown: function (evt) {
					$(this).attr('width',$(this).outerWidth());
				},
			}, 'td:not([width]),th:not([width])');

			$('body').on({ // #161 - priya
				focus: function (evt) {
					var e = jQuery.Event("keyup");
					e.which = 32; 
					$(evt.target).val(String.fromCharCode(e.which));
					$(evt.target).trigger(e);
				},
			}, '[data-type="popUp"][data-focus] .kriya-tagsinput');
			
			$('body').on({
				mousedown: function (evt) {
					//To prevent the selction in editor when click on header
					if($(evt.target).closest('.findSection').length < 1){
        				evt.preventDefault();
					}
				},
			}, '#headerContainer');

			$('body').on({
				click: function (evt) {
					$('.tableClone input:file').val('');
					$(this).parent().find('input:file').trigger('click');
				},
			}, '.tableClone .fileChooser');

			$('body').on( 'change', '.tableClone input:file', function(e){
				var targetNode = kriya.evt.target || kriya.evt.srcElement;
				var popper = $(targetNode).closest('[data-component]');

				var files = e.target.files;
				var f = files[0];
				excel2Table(f, function(tableHTML){
					tableHTML = '<div>' + tableHTML + '</div>';
					if($(tableHTML).find('table').length > 0){
						var tableInnerHtml = $(tableHTML).find('table').html();
						popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
						popper.find('.pasteTableDiv').addClass('hidden');
						popper.find('.resetTableBtn').removeClass('disabled');
					}
				});
			});
			
			$('body').on({
				click: function(){
					$("#suggesstion-box").addClass('hidden');
				}
			}, '[data-class="award-group"] *[class="text-line"],[data-class="award-group"] [data-input="true"]');

			$('body').on({
				keydown: function (evt) {
					$('[data-component="RefAuthor_add"]').addClass('hidden');
				},
			}, '[data-component="jrnlRefText_edit"] [data-class="RefAuthor"], [data-component="jrnlRefText_edit"] [data-class="RefEditor"]');
			
			$('body').on({
				click: function (evt) {
					$('[data-component="RefAuthor_add"]').addClass('hidden');
				},
			}, 'div[data-component="jrnlRefText_edit"]');

			$('body').on({
				click: function (evt) {
					//$('#contentDivNode,.leftPanel').toggleClass('editable');
				},
			}, '.ct-ignition__button.ct-ignition__button--edit, .ct-ignition__button.ct-ignition__button--cancel');

			$('body').on({
				click: function (evt) {
					$('.fullHeight.nano,.leftPanel').toggleClass('editable');
				},
			}, '.closeLeftPanel');

			$('body').on({
				mouseleave: function (evt) {
					$(this).css({
						'display' : '',
						'opacity' : '0',
						'left' : '',
						'top' : '',
					});
				}
			}, '[data-component*="_menu"][data-component^="jrnl"]');
			
			$('body').on({
				change: function (evt) {
					var eleClass = $(this).attr('data-class');
					var updatedValue = $(this).val();
					if($(this).attr('type') == "number" && eleClass.match(/Gap/g) && updatedValue){
						updatedValue = updatedValue+"pt";
					}else if($(this).attr('type') == "checkbox"){
						updatedValue = ($(this).is(':checked'))?1:'';
					}else if(eleClass == "floatStackWith"){
						var nodeXpath = $(this).find(':selected').attr('data-node-xpath');
						var float = kriya.xpath(nodeXpath);
						updatedValue = $(float).closest('[data-id^="BLK_"]').attr('data-id');
					}
					if(updatedValue){
						$(this).closest('[data-component]').find('tr.active .'+eleClass).html(updatedValue);	
					}
					else{ // to remove value if set default - aravind
						$(this).closest('[data-component]').find('tr.active .'+eleClass).html('');
					}										
				}
			}, '[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatColumnStart"], [data-class="floatColumnSpan"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]');

			//Change the proof controls value when click on the list
			$('body').on({
				click: function(evt){
					$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatColumnStart"], [data-class="floatColumnSpan"], [data-class="floatOrientation"], [data-class="floatStackWith"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]').val('');
					$('[data-class="floatContinued"]')[0].checked = false;
					
					if($(this).hasClass('active')){
						$(this).removeClass('active');
						$('.resetPlaceFloat').addClass('disabled');
						$('.figureControls').addClass('disabled');
						$('.boxType').addClass('disabled');
					}else{
						$(this).closest('table').find('tr').removeClass('active');
						$(this).addClass('active');
						var activeRow = this;
						$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatColumnStart"], [data-class="floatColumnSpan"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"], [data-class="boxType"]').each(function(){
							var eleClass = $(this).attr('data-class');
							var eleVal = $(activeRow).find('.'+eleClass).text();
							if($(this).attr('type') == "number"){
								eleVal = eleVal.replace(/[a-z]+/, '');
							}else if($(this).attr('type') == "checkbox"){
								if(eleVal == "1"){
									this.checked = true;
								}else{
									this.checked = false;
								}
							}
							if(eleClass == "floatStackWith"){
								$(this).find('option:not(:first)').removeAttr('value');
								var labelNode = $(kriya.config.containerElm + ' [data-id="' + eleVal + '"] .label');
								if(labelNode.length > 0){
									eleVal = labelNode.html();
								}
							}
							$(this).val(eleVal);
						});
						$('.resetPlaceFloat').removeClass('disabled');
						if($(kriya.xpath($(this).closest('tr').attr('data-node-xpath'))).attr('class')=="jrnlFigBlock"){
							$('.figureControls').removeClass('disabled');
						}else{
							$('.figureControls').addClass('disabled');
						}
						if($(kriya.xpath($(this).closest('tr').attr('data-node-xpath'))).attr('class')=="jrnlBoxBlock"){
							$('.boxType').removeClass('disabled');
						}else{
							$('.boxType').addClass('disabled');
						}
					}
				}
			}, '[data-component="placeFloats_edit"] tbody tr');

			//Reset the float place options
			$('body').on({
				click: function(evt){
					var popper = $(this).closest('[data-component]');
					var activeRow = $(popper).find('tbody tr.active');
					$('[data-class="floatPageNum"], [data-class="floatPosition"], [data-class="floatColumnStart"], [data-class="floatColumnSpan"], [data-class="floatOrientation"], [data-class="topGap"], [data-class="leftGap"], [data-class="rightGap"], [data-class="bottomGap"], [data-class="floatContinued"], [data-class="floatStackWith"], [data-class="captionWidth"], [data-class="captionPosition"], [data-class="figureWidthFactor"]').each(function(){
							var eleClass = $(this).attr('data-class');
							$(activeRow).find('.'+eleClass).text('');
							if($(this).attr('type') == "checkbox"){
								this.checked = false;
							}
							$(this).val('');
						});
				}
			}, '[data-component="placeFloats_edit"] .resetPlaceFloat');

			$('body').on({
				keyup: function(){
					var topCss       = $(this).offset().top + $(this).height();
					var textVal      = $(this).text();
					textVal = textVal.replace(/^\s+|\s+$/, '');
					var listSelector = $(this).attr('data-list-selector');
					var listNode = kriya.xpath(listSelector);
					if($(listNode).length > 0){
						$(listNode).find('li').addClass('hidden');
						//if open or closed parenthasis charecters came in textval variable that time code will break
						//that's why i used escape() function it will escape special charectes in that text val.
						//$(listNode).find('li:contains(' + textVal + ')').removeClass('hidden');
						$(listNode).find('li:contains(' + escape(textVal) + ')').removeClass('hidden');
						$(listNode).removeClass('hidden').css({
							'display':'block',
							'top'    : topCss,
							'left'   : $(this).offset().left,
							'opacity': '1',
							'z-index': '9999'
						});
						var targetXpath = kriya.getElementTreeXPath(this);
						$(listNode).attr('data-target-selector', targetXpath);
					}
				}
			}, '[data-list-selector]');
			
			//Update the country when select the list
			$('body').on({
				click: function(){
					var selectedData = $(this).html();
					var listNode = $(this).closest('[data-target-selector]');
					if(listNode.length > 0){
						var targetXpath = listNode.attr('data-target-selector');
						var targetNode  = kriya.xpath(targetXpath);
						if($(targetNode).length > 0){
							$(targetNode).html(selectedData);
							$(targetNode).removeAttr('data-error');

							//to set cursor at enf of the field - jai
							var range = rangy.createRange();
							range.setStart($(targetNode)[0].childNodes[0], $(targetNode)[0].textContent.length);
							range.collapse(true);
							var sel = rangy.getSelection();
							sel.setSingleRange(range);
							listNode.addClass('hidden');
						}
					}
				}
			}, '[data-type="list"] li');

			$('body').on({
				click: function (evt) {
					$('[data-component="partLabel_add"]:visible').addClass('hidden'); //remove the add part label popup
					$('[data-type="list"]').addClass('hidden');
					if ($(evt.target).closest('[contenteditable="true"]').length > 0){
						var range = rangy.getSelection();
						kriya.popupSelection = range.getRangeAt(0);
						$('#specialCharacter').data("currentSelection",range.getRangeAt(0));
					}
				}
			}, '[data-type="popUp"]');
			
			$('body').on({
				change: function(evt){
					var checkedNode = $('[data-type="popUp"][data-component="jrnlRefText_edit"] [name="refSearch"]:checked');
					var placeHolderVal = checkedNode.attr('data-placeholder-val');
					var validateFUnc = checkedNode.attr('data-field-validate-func');
					var searchField = $('[data-type="popUp"][data-component="jrnlRefText_edit"] .searchField');
					
					if(validateFUnc){
						searchField.attr('data-validate', validateFUnc);
					}
					$('[data-type="popUp"][data-component="jrnlRefText_edit"] .refSearchContainer').html('');
					$('[data-type="popUp"][data-component="jrnlRefText_edit"] .refSearchContainer').addClass('hidden');
					if(placeHolderVal){
						searchField.attr('data-placeholder', placeHolderVal);
					}else{
						searchField.attr('data-placeholder', 'Enter the search text here');
					}
					
				}
			}, '[data-type="popUp"][data-component="jrnlRefText_edit"] [name="refSearch"]');

			$('body').on({
				keyup: function (evt) {
					if (evt.which != 13){
						var popper = $(this).closest('#specialCharacter');
						var searchText = $(this).val();
						searchText = searchText.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
						var searchList = popper.find('.advanced-symbols').find('.spl-char[title*="'+searchText+'"], .spl-char[data-html-entity*="'+ $(this).val() +'"]');
						var searchViewList = '';
						if(searchList.length > 0){
							searchList.each(function(i,ele){
								searchViewList += '<li><a href="#"><span data-name="char-name">'+$(this).attr('title')+'</span>&nbsp;&nbsp;&nbsp;&nbsp;<span data-name="char">'+$(this).text()+'</span></a></li>';
								if(i == 25){
									return false;
								}
							});
						}
						$('#special_char_search_list').html(searchViewList);
						$('#special_char_search_list').css({'display':'block','opacity':'1'});
					}
				}
			}, '.special_char_search');

			$('body').on({
				click: function (evt) {
					var popper = $(this).closest('#specialCharacter');
					var charName = $(this).find('[data-name="char-name"]').text();
					$('.special_char_search').val(charName);
					var charList = popper.find('.spl-char[title="'+charName+'"]').closest('.symb-container');
					if(charList.length > 0){
						popper.find('.symb-container').addClass('hidden');
						charList.removeClass('hidden');
					}
					popper.find('.spl-char.active').removeClass('active');
					popper.find('.spl-char[title="'+charName+'"]').addClass('active');
					$('.spl_char_drop_down').val(charList.attr('data-spl-group'));
					$('#special_char_search_list').css({'display':'none','opacity':'0'});
				}
			}, '#special_char_search_list li');
		
			$('body').on({
				mouseleave: function(evt){
					$('#special_char_search_list').css({'display':'none','opacity':'0'});
				}
			}, '#special_char_search_list');

			// change year in copyright statement
			$('body').on({
				change: function (evt) {
					//before this condition not working for bir,mbs customers because of that that year not updated when we chenge copyright year.
					var copyRightYear = $('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]').val()?$('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]').val():'';
					if(copyRightYear){
					//if($('.jrnlPermission .jrnlCopyrightYear').html()!=undefined){
						var copyRightstmt = $('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightStmt"]').html();
						var regexData = new RegExp("\\b[0-9]{4}\\b","g");
						copyRightstmt = copyRightstmt.replace(regexData, $('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]').val());
						$('[data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightStmt"]').html(copyRightstmt);
					}
				}
			}, '[data-type="popUp"][data-component="jrnlPermission_edit"] [data-class="jrnlCopyrightYear"]');

			//Prevent editing label in the popup
			$('body').on({
				keydown: function (evt) {
					
          			var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					var selectedNodes = range.getNodes();

					var prevSelector = '.del, [data-track="del"]';
					if($(range.startContainer).closest('[data-class*="Caption"]').length > 0){
						prevSelector += ', .label';
					}
					//filter the label node from the selected nodes
					var labelEle = $(selectedNodes).filter(prevSelector);
					if(labelEle.length == 0){
						//If the cursor places in the label node
						labelEle = 	$(range.startContainer).closest(prevSelector);
					}
          			
          			var keycode = event.keyCode;

          			if (
						(keycode > 47 && keycode < 58) || // number keys
						keycode == 32 || keycode == 13 || // spacebar & return key(s) (if you want to allow carriage returns)
						((keycode == 46 || keycode == 8) && labelEle.closest('[data-class*="Caption"]').length > 0) || // spacebar & return key(s) (if you want to allow carriage returns)
						(keycode > 64 && keycode < 91) || // letter keys
						(keycode > 95 && keycode < 112) || // numpad keys
						(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
						(keycode > 218 && keycode < 223) // [\]' (in order)
						){
							if(labelEle.length > 0){

		          				var spanNode = document.createElement('span');
		          				spanNode.setAttribute('class', 'unwrapNode');
		          				$(spanNode).insertAfter(labelEle);
		          				$(spanNode).html('&nbsp;');
		          				var range = document.createRange();
		        				range.setStart(spanNode, 0);
		        				range.setEnd(spanNode, 1);
								
								var winSel = window.getSelection();
		        				winSel.removeAllRanges();
		        				winSel.addRange(range);
		          			}
						}
				},
				keyup: function(evt){
					//Unwrap the added element in keydown to move next to the label
					if($(this).find('.unwrapNode').length > 0){
						var wrapContent = $(this).find('.unwrapNode').contents().unwrap();
	      				var range = document.createRange();
	    				range.setStartAfter(wrapContent[0]);
	          			range.collapse(true);
						
						var winSel = window.getSelection();
	    				winSel.removeAllRanges();
	    				winSel.addRange(range);
    				}
				}
			}, '[data-type="popUp"] [data-type="htmlComponent"]');

			$('body').on({
				paste: function (evt){
					if(evt.originalEvent.clipboardData){
						var targetNode = kriya.evt.target || kriya.evt.srcElement;
						var popper     = $(targetNode).closest('[data-component]');

						var tableHTML = evt.originalEvent.clipboardData.getData('text/html');
						tableHTML     = cleanupOnPaste(tableHTML);
						tableHTML     = '<div>'+tableHTML+'</div>';
						if($(tableHTML).find('table').length == 0){
							evt.stopPropagation();
    						evt.preventDefault();
    						kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table not found in pasted content.' ,icon: 'icon-warning2'});
						}else{
							
							var tblNode = $(tableHTML).find('table');
							$(tblNode).css('table-layout','');
							tblNode.find('td,th').each(function(){
								//remove the unwanted space
								var cellHtml = $(this).html();
								cellHtml = cellHtml.replace(/\s\s+/g, ' '); //remove the more than one normal space
								cellHtml = cellHtml.replace(/^\s+$/g, ''); // if cell html is full of normal space then remove every space

								//Wrap the para in table cell
								if($(this).find('p').length > 0){
									$(this).find('p').addClass('jrnlTblBody');
								}else{
									$(this).html('<p class="jrnlTblBody">' + cellHtml + '</p>');
								}
							});
							$('.la-container').fadeIn();
							var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.project+'.captions', 'fileData': '<div class="body">'+tblNode[0].outerHTML+'</div>'};
							kriya.general.sendAPIRequest('structurecontent', parameters, function(res){
								if(res.error){
									$('.la-container').fadeOut();
									kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
								}else if(res){
									data = res.replace(/&apos;/g, "\'");
									var response = $('<r>'+ data + '</r>');
									if ($(response).find('response').length > 0 && $(response).find('response content').length > 0){
										var parameters = {'customerName' : kriya.config.content.customer, 'projectName' : kriya.config.content.project, 'articleID' : kriya.config.content.project+'.captions', 'content': $(response).find('response content').html()};
										kriya.general.sendAPIRequest('applyHouseRules', parameters, function(applyres){
										if(applyres.error){
											$('.la-container').fadeOut();
											kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table setter not update. Please contect Support.' ,icon: 'icon-warning2'});
										}else if(applyres){
											var response = $('<r>'+ applyres + '</r>');
											var table = $(response).find('table')
											var tableInnerHtml = table[0].outerHTML;
											$(table).each(function(){
												$.each(this.attributes,function(i,a){
													popper.find('table[data-type="floatComponent"]').attr(a.name,a.value)
												})
											})
											popper.find('table[data-type="floatComponent"]').html(tableInnerHtml).removeClass('hidden');
											popper.find('table[data-type="floatComponent"]').attr('data-focusout-data', 'paste');
											popper.find('.pasteTableDiv').addClass('hidden').html('');
											popper.find('.resetTableBtn').removeClass('disabled');
											$('.la-container').fadeOut();
										}
										}, function(err){
											$('.la-container').fadeOut();
											kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table setter not update. Please contect Support' ,icon: 'icon-warning2'});
										});
									}							
								}
							}, function(err){
								$('.la-container').fadeOut();
								kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table Rule set not updated. Please contect Support.' ,icon: 'icon-warning2'});
							});
						}
					}
				}
			}, '[data-type="popUp"] .pasteTableDiv[contenteditable="true"]');

			$('body').on({
				paste: function (evt){
					//Clean up the pasted content
					var targetNode = $(this);

					//#249 - if middle mouse button is clicked, event.which is undefined - Prabakaran. A (prabakaran.a@focusite.com)
					if(kriya.pasteHandleEvent.which === 2){
						kriya.pasteHandleEvent.preventDefault();
						return false;
					}
					//End of #249

					setTimeout(function(){
						// delete the last child if its <br> to avoid unwanted space in component
						if($(targetNode).children().length>0 && $(targetNode).children().last()[0].nodeName.toLowerCase()=="img"){
							$(targetNode).children().last()[0].remove();
						}
						var targetNodeContent = targetNode.html();
						var classnameParent = '';
						if(targetNode.parentNode!=undefined){
							classnameParent = targetNode.parentNode.className;
						}

                        //#292 - Libre Office - Replaced preceeding and trailing spaces - Mohammed Navaskhan (mohammednavas.a@focusite.com)
						//#154-it is removing the parent node (.tags) in Insert research organism popup-kirankumar
						//clean up the paste content component fields added by vijayakumar on 15-11-2018
						var contenttype = targetNode.attr('data-data-type');
						if(!targetNode.hasClass('kriya-tagsinput') && targetNode.attr('data-paste-clean') != "false"){
							targetNodeContent = cleanupOnPaste(targetNodeContent);
						}
						var cleancontent = "<span>"+targetNodeContent+"</span>";
						if(contenttype == "text"){
							//paste only text
							targetNodeContent = $(cleancontent).text()
							targetNodeContent = targetNodeContent.replace(/\</g,'&lt;').replace(/\>/g,'&gt;'); // to paste the same content if copied with xml tags
						}else if(contenttype == "formatting"){
							//paste with formatting style tags
							$(cleancontent).find('*:not("i,sup,sub,b,strong,em,u,a")').each(function(){
								$(this).contents().unwrap()
							})
							targetNodeContent = $(cleancontent).html();
						}
						targetNodeContent = targetNodeContent.replace(/^\s+|\s+$/gm, '');
                        //End of #292
						targetNodeContent = cleanforInputs(targetNodeContent, classnameParent);
						targetNode.html(targetNodeContent);

						//If refernce html was pasted in popup or in query card
						//unwrap the tag and paste as text
						targetNode.find('.jrnlRefText *').contents().unwrap();
						targetNode.find('.jrnlRefText').removeAllAttr();

					},100);
				}
			}, '[data-type="popUp"] [contenteditable="true"], .query-div.card [contenteditable="true"]');

			//Prevent the right click context menu in editor
			$(kriya.config.containerElm).on("contextmenu",function(e){
		        return false;
		   	 });

			//Select the supplementary type and float dropdown in supplementary popup
			$('body').on({
				click: function (evt) {
					var popper    = $(this).closest('[data-component]');
					var selectedText = $(this).text();
					$(this).parent('.dropdown-content').prev('.dropdown-button').text(selectedText);
					$(this).siblings().removeClass('selected');
					$(this).addClass('selected');

					if(popper.find('[data-class$="Caption"] .label').length == 0){
						popper.find('[data-class$="Caption"]').prepend('<span class="label"></span>');
					}
					var labelNode = popper.find('[data-class$="Caption"] .label');
					var labelId = '';
					if(popper.find('#float-list-dropdown li.selected').length > 0){
						var prefixLabelID = popper.find('#float-list-dropdown li.selected').attr('data-float-id');
						if(prefixLabelID){
							labelNode.attr('data-main-float', prefixLabelID);
						}else{
							labelNode.removeAttr('data-main-float');
						}
					}
					if(popper.find('#supplement-type-dropdown').length > 0){
						labelId = popper.find('#supplement-type-dropdown li.selected').attr('data-id-prefix');
						labelId = (prefixLabelID)?prefixLabelID + '-' + labelId : labelId;
					}else if($(this).attr('data-id-prefix')){
						labelId = $(this).attr('data-id-prefix');
						labelId = (prefixLabelID)?prefixLabelID + '-' + labelId : labelId;
					}

					labelId = labelId.replace(/BLK_/g, '');
					
					var citeConfig = citeJS.floats.getConfig(labelId);
					var chapNum = $('#contentDivNode .jrnlChapNumber').text();
					if(chapNum && citeConfig && citeConfig['chapter-as-prefix']){
						labelId = labelId + chapNum + '_';
					}

					var lastElements = $(kriya.config.containerElm + ' [data-id^="' + labelId + '"]').closest('[data-id^="BLK_"]:not([data-inline="true"])').last();
					if(lastElements.length > 0){
						var lastElementId = lastElements.attr('data-id');
						if(lastElementId && lastElementId.match(/(\d+)$/g)){
							var lastIdNum = parseInt(lastElementId.match(/(\d+)$/g)[0]);
							labelId = labelId+(lastIdNum+1);
						}
					}else{
						labelId = labelId+1;
					}


					if(labelId){
						var newFloatLabelString = citeJS.floats.getCitationHTML([labelId], [], 'renumberFloats');
						var idPrefix = labelId.replace(/(\d+)$/g, '');
						var uncitedCount = $(kriya.config.containerElm + ' .jrnlSupplBlock[data-id^="' + idPrefix + '"]').closest('[data-id^="BLK_"][data-uncited]').length + 1;
						newFloatLabelString =  newFloatLabelString.replace(/(\d+)$/g, uncitedCount);

						var uncitedStr = 'Uncited ';
						if(citeConfig && citeConfig.citationNotNeed == "true"){
							uncitedStr = '';
						}
						newFloatLabelString = uncitedStr + newFloatLabelString;
						labelNode.html(newFloatLabelString);
						labelNode.attr('data-block-id', labelId);
					}
				},
			}, '#float-list-dropdown li, #supplement-type-dropdown li, #box-type-dropdown li');
			
			$('body').on({
				click: function (evt) {
					$('#specialCharacter .spl-char').removeClass('active');
					$(this).addClass('active')
				},
			}, '#specialCharacter .spl-char');

			$('body').on({
				click: function (evt) {
					var id = $(this).attr('data-dropdown');
					$('.dropdown-content:visible:not(#' + id + ')').fadeOut().css({'opacity': '0'});
					if($('#'+id+':visible').length > 0){
						$('#'+id).fadeOut().css({'opacity': '0'});
					}else{
						$('#'+id).css({'opacity': '1'}).fadeIn();
					}
				}
			}, '.dropdown-button[data-dropdown]');	
			$('body').on({
				click: function(e){
					//Highlight validation issue place added by vijayakumar on 9-11-2018
					$('#contentDivNode *[data-validation-error="true"]').removeAttr('data-validation-error', 'true');
					if ($('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]').length > 0){
						$('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]').attr('data-validation-error', 'true');
						$('#contentDivNode [id="' + $(this).attr('data-clone-id') + '"]')[0].scrollIntoView();
					}
				},
			}, '#validationDivNode [data-clone-id]');
			$('body').on({
				click: function (evt) {
					if(!$(evt.target).is('.dropdown-button') && $(evt.target).closest('.dropdown-content').length == 0 && $('#trackChangeOptions .dropdown-content:visible')){
						$('.filterOptions').find('.dropdown-content:visible').fadeOut().css({'opacity': '0'});
					}
				}
			});

        },

		isUndefined: function(obj){
			if(typeof(obj)!="undefined"){
				return true;
			}
			return false;
		},
		removeComp: function(obj){
			//kriya[config.activeElements[0]].detach();
		}
	};


	init();

}());


$(document).ready(function(){

	//to enable save option in resolve issue popup - aravind
	$('[data-component="resolveIssue_edit"] select[data-class="category"]').on('change', function(){
		var prevCategory = $(this).attr('data-focusin');
		if($(this).val() == ''){
			$('[data-component="resolveIssue_edit"]').find('[data-savecategory="true"]').addClass('disabled');
		}else if($(this).val() == prevCategory){
			$('[data-component="resolveIssue_edit"]').find('[data-savecategory="true"]').addClass('disabled');
		}else{
			$('[data-component="resolveIssue_edit"]').find('[data-savecategory="true"]').removeClass('disabled');
		}
	});

	$('[data-component="QUERY"] .tab').on('click',function(){
		var queryType = $(this).attr('data-type');
		$('*[data-component="QUERY"] .tab.active').removeClass('active');
		$('[data-component="QUERY"] [data-query-type]').addClass('hidden');
		$(this).addClass('active');
		if (queryType == "freeform"){
			$('[data-component="QUERY"] [data-query-type="' + queryType + '"]').removeClass('hidden');
			$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('contenteditable', 'true').html('').focus();
			$('[data-component="QUERY"]').find('[data-class="jrnlQueryText"]').attr('data-class-selector', 'default');
		}else{
			$('[data-component="QUERY"] [data-query-type="' + queryType + '"]').removeClass('hidden');
		}
	});
	
	$('[data-class="jrnlKeyword"]').on('keydown',function(evt){
		if(evt.keyCode==13 && $('[data-class="jrnlKeyword"]').html().length>0){
			evt.stopPropagation();
			evt.preventDefault();
			eventHandler.components.general.saveKeyword('',$('[data-class="jrnlKeyword"]'));
		}		
	});

	$('#actionQuery').on('change',function(){
		$('.ManufacturerQuery').css('display','none');
		$('.CitationQuery').css('display','none');
		var query = $(this).val();
		$('.actiontext.queryContent').html('');
		if ($('[data-query-type="'+query+'"]').length > 0){
			$('.actiontext.queryContent').html($('[data-query-type="'+query+'"]').html());
		}
		console.log(query+"Query");
		$('.'+query+"Query").css('display','block');
	});		

	/*$('body').on({
		mouseover: function (evt){
			if ($('.image-hover').length == 0 && $(this).closest('[data-track="del"]').length == 0){
				$('.image-hover').remove();
				var version = '';
				if ($(this).closest('.jrnlFigBlock').find('img.jrnlFigure').length > 1){
					version = '<span class="image-version"  data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'showImageVersions\', \'param\': {\'id\': \'' + $(this).attr('id') + '\'}}">Versions</span>';
				}
				var imageHover = $('<div class="image-hover"><span class="replace-file" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'editPopUp\', \'param\': {\'component\': \'FIGURE_edit\', \'id\': \'' + $(this).attr('id') + '\'}}">Replace</span>' + version + '</div>');
				$('body').append(imageHover);
				imageHover.css('left', (($(this).offset().left + $(this).width()) - imageHover.width() - 20));
				var divHeight = $(this).offset().top;
				//var divHeight = ($('#contentDivNode').scrollTop() + $(this).parent().offset().top) - $('#contentDivNode').offset().top + $(this).position().top;
				imageHover.css('top', divHeight + 'px');
			}
		},
	}, '#contentDivNode img.jrnlFigure:not([data-track="del"]), #contentDivNode img.jrnlInlineFigure:not([data-track="del"])');
	$('body').on({
		mousemove: function (evt){
			if ($('.image-hover').length == 0 && $(this).closest('[data-track="del"]').length == 0){
				$('.image-hover').remove();
				var version = '';
				if ($(this).closest('.jrnlFigBlock').find('img.jrnlFigure').length > 1){
					version = '<span class="image-version"  data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'showImageVersions\', \'param\': {\'id\': \'' + $(this).attr('id') + '\'}}">Versions</span>';
				}
				var imageHover = $('<div class="image-hover"><span class="replace-file" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'editPopUp\', \'param\': {\'component\': \'FIGURE_edit\', \'id\': \'' + $(this).attr('id') + '\'}}">Replace</span>' + version + '</div>');
				$('body').append(imageHover);
				imageHover.css('left', (($(this).offset().left + $(this).width()) - imageHover.width() - 20));
				var divHeight = $(this).offset().top;
				//var divHeight = ($('#contentDivNode').scrollTop() + $(this).parent().offset().top) - $('#contentDivNode').offset().top + $(this).position().top;
				imageHover.css('top', divHeight + 'px');
			}
		},
	}, '#contentDivNode img.jrnlFigure:not([data-track="del"])');
	$('body').on({
		mouseleave: function (event) {
			var elements = elementsFromPoint(event.pageX, event.pageY);
			var remove = true;
			$(elements).each(function(){
				if ($(this).hasClass('image-hover')){
					remove = false;
				}
			});
			if (remove){
				$('.image-hover').remove();
			}
		},
	}, '#contentDivNode img.jrnlFigure:not([data-track="del"])');*/
	
	$('body').on({
		mouseenter: function (evt) {
			if ($(this).find('.download-modal').length == 0){
				if ($(this).closest('[data-component="QUERY"]').length > 0){
					$(this).append('<div class="download-modal">'+$(this).attr('data-file-name') + ' <span class="download-file"><i class="material-icons" style="font-size:20px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'removeFile\'}">delete</i></span></div>');
				}else if ($(this)[0].hasAttribute('data-editable')){
					$(this).append('<div class="download-modal">'+$(this).attr('data-file-name') + ' <span class="download-file"><i class="material-icons" style="font-size:20px;" data-channel="query" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'removeFile\'}">delete</i></span></div>');
				}else{
					$(this).append('<div class="download-modal">'+$(this).attr('data-file-name') + ' <span class="download-file" onclick="window.open(\''+$(this).find('[src]').attr('src')+'\')"><i class="material-icons">file_download</i></span></div>');
				}
			}
		},
	}, '#queryDivNode div[data-type="file"], [data-component="QUERY"] div[data-type="file"]');
	$('body').on({
		mouseleave: function (evt) {
			$(this).find('.download-modal').remove();
		},
	}, '#queryDivNode div[data-type="file"]');

	$('body').on({
		mouseenter: function (evt) {
			if ($('.kriyaMenuControl.active').length > 0){
				if (! $(this).find('.kriyaMenuBtn').hasClass('disabled')){
					$('.kriyaMenuControl.active').removeClass('active');
					$(this).addClass('active');
				}
			}
		},
	}, '.kriyaMenuControl');

	$('body').on({
		click: function (evt) {
			var href = $(this).attr('href');
			if (! /javascript|\#/.test(href)){
				window.open(href);
				evt.stopPropagation();
				evt.preventDefault();
			}
		},
	}, 'a[href]');

	$('body').on({
		change: function (evt) {
			if ($(this).is(':checked') && kriya.config.content.role.match(/preeditor|typesetter|publisher/)){
				var reviewer = $(this).attr('data-reviewer');
				$(this).closest('[data-wrapper]').find('.email-message').remove();
				if ($('trigger[name="' + reviewer + '"]').length > 0){
					var toAddress = $('trigger[name="' + reviewer + '"]').find('to').first().text();
					if ($(this).parent().find('select.recipient').length > 0){
						toAddress = $(this).parent().find('select.recipient').val();
					}
					if (toAddress != "" && toAddress != "---"){
						$(this).closest('[data-wrapper]').append('<p class="email-message" style="background: #f1f1f1;padding: 5px;border-radius: 5px;">This will be signed-off to: <span style="font-weight: 600;">' + toAddress + '</span></p>');
					}
				}
			}
			$(this).closest('[data-wrapper]').find('.email-component').remove();
			if ($(this).closest('[data-email-editable]').length > 0){
				var reviewer = $(this).attr('data-reviewer');
				if ($('trigger[name="' + reviewer + '"]').length > 0){
					var emailComp = $('<div class="email-component card" style="padding: 0px 30px 10px;margin: 5px 10px;"><p style="margin: 0px -30px;padding: 10px 10px;background: #3c3c3c;color: #fff;font-weight: bold;box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);border-top-left-radius: 3px;border-top-right-radius: 3px;">Email Message</p></div>');
					var trigger = $('trigger[name="' + reviewer + '"]:first');
					var userEmail = $('.userProfile .username').attr('data-user-email');
					var userName = $('.userProfile .username').text();
					$(trigger).find('> *').each(function(){
						if ($(this)[0].nodeName != 'DIV'){
							var currEmailComp = $('<p style="padding: 0px 5px;border-bottom: 1px solid #848484;" contenteditable="true" class="email-' + $(this)[0].nodeName.toLocaleLowerCase() + '">');
						}else{
							var currEmailComp = $('<div contenteditable="true" class="' + $(this).attr('class') + '">');
						}

						var compData = $(this).html();
						compData = compData.replace(/{userMailID}/g, userEmail);
						compData = compData.replace(/{userName}/g, userName);
						currEmailComp.html(compData);
						$(emailComp).append(currEmailComp)
					});

					if(window.location.pathname == "/peer_review/"){
						var currEmailComp = $('<p style="padding: 0px 5px;border-bottom: 1px solid #848484;" contenteditable="true" class="email-replyTo">');
						$(emailComp).find('.email-subject').before(currEmailComp);
						
						var currEmailComp = $('<div class="email-attachement row"><span class="btn btn-medium amber"><input type="file" name="files[]" multiple="" style="display:none;" data-channel="components" data-topic="PopUp" data-event="change" data-message="{\'funcToCall\': \'changeAttachFile\'}"/><span class="fileChooser" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'attachFileTrigger\'}"><i class="material-icons">attach_file</i>Please click here to attach</span></span></div><div class="row attachementContainer"/>');
						$(emailComp).append(currEmailComp);
					}
					
					$(this).closest('[data-wrapper]').append(emailComp);
				}
			}
			//If mail body has comments div then append the comments in mail body - jagan
			if($('[data-component="signoff_edit"] [data-wrapper] .email-component .comments-list').length > 0){
				var commentsRow = $('<div class="row" style="margin-left: 25px !important;"/>');
				var r = 0;
				$('#queryDivNode #openQueryDivNode [data-current-version="true"] .kriya-collapsible li').each(function(){
					if($(this).find('.query-div.card.author').length > 0){
						if($(this).attr('data-role') == "Reviewer"){
							r++;
							var reviewerName = "Reviewer " + r;
						}else{
							var reviewerName = $(this).find('.reviewer-name').clone(true);
							reviewerName.find('.userLabel').remove();
							reviewerName = reviewerName.text();
						}
						
						var commentsList = $('<div><p style="font-size: 13px;margin: 0px;font-weight: 600;">' + reviewerName + '</p><ul /></div>');
						$(this).find('.query-div.card.author').each(function(){
							commentsList.find('ul').append('<li>' + $(this).find('.query-content').html() + '</li>');
						});
						commentsRow.append(commentsList);
					}
				});
				$('[data-component="signoff_edit"] [data-wrapper] .email-component .comments-list').append(commentsRow);
			}
			//$(this).closest('[data-wrapper]').css('height', 'auto')
			//$(this).closest('[data-wrapper]').css('height', ($(this).closest('[data-wrapper]').height()+100))
		},
	}, '*[data-component="signoff_edit"] [data-wrapper] input[type="radio"][data-reviewer]');

	$('body').on({
		change: function (evt) {
			var input = $(this).parent().find('input[data-reviewer]');
			if ($(input).is(':checked') && kriya.config.content.role.match(/preeditor|typesetter|publisher/)){
				var reviewer = $(input).attr('data-reviewer');
				$(this).closest('[data-wrapper]').find('.email-message').remove();
				if ($('trigger[name="' + reviewer + '"]').length > 0){
					var toAddress = $(this).val();
					if (toAddress != "---"){
						$(this).closest('[data-wrapper]').append('<p class="email-message" style="background: #f1f1f1;padding: 5px;border-radius: 5px;">This will be signed-off to: <span style="font-weight: 600;">' + toAddress + '</span></p>');
					}
				}
			}
		},
	}, '*[data-component="signoff_edit"] [data-wrapper] select.recipient');

	$('body').on({
		click: function (evt) {
			$('.kriyaMenuControl.active').removeClass('active');
		},
	}, '.kriyaMenuControl .kriyaMenuItem');

	$('.body').on('click',function(e){	
		$('.highlight:not(".grammar,.spelling,.suggestion,.findOverlay")').remove();
		$('.query-div.selected').removeClass('selected');
	});

	//#533 - Error: ReferenceError: event is not defined - Mariyammal(mariyammal.m@praniontech.com)
	$('body').on('click',function(event){	
		if ($('.kriyaMenuControl.active').length > 0 && $(event.target).closest('.kriyaMenuControl').length == 0){
			$('.kriyaMenuControl.active').removeClass('active');
		}
		//To trigger save button while click anywhere(except upload, cancel, author button) after entering the query - rajesh  
		if(event && $(event.target) && !$(event.target).hasClass('upload-file') && !$(event.target).hasClass('change-assignee') && !$(event.target).hasClass('cancelReply') && !$(event.target).hasClass('cancel-edit') && $(event.target).closest('input[type="file"]').length == 0 && $(event.target).closest('.queryReplyType').length == 0 && $(event.target).closest('.confidential-comments').length == 0){
			$('div[data-save="true"]').find('.save-edit').trigger('click');
			$('span[data-save="true"]').find('.add-reply').trigger('click');
			$('.query-holder').removeAttr('data-save');
		}
	});

	$('body').on( 'click', '#helpDivNode ul.helpList > li', function(){
		if ($(this).hasClass('active')){
			$(this).find('.arrow-denoter').html('arrow_drop_down');
		}else{
			$(this).find('.arrow-denoter').html('arrow_drop_up');
		}
		$(this).toggleClass('active');
	});
	$('body').on({// Issue Id - #676 - Dhatshayani . D Feb 26, 2018 - Scroll to element which has validation error
		click: function(event){
			if(this.hasAttribute('data-validation-id') && this.getAttribute('data-validation-id') != "" && $('#contentDivNode').find('*[data-validation-rid~="'+this.getAttribute('data-validation-id')+'"]').length > 0){
				$('#contentDivNode').find('*[data-validation-rid~="'+this.getAttribute('data-validation-id')+'"]')[0].scrollIntoView();
			}
		}
	}, '#validationDivNode .jrnlValMesg');
	$('body').on({
	    dragstart: function(event){
		event.preventDefault();
		event.stopPropagation();
	    },
	    drop: function(event){
		if(event.originalEvent.dataTransfer && event.originalEvent.dataTransfer.files && event.originalEvent.dataTransfer.files.length == 0	){
		     event.preventDefault();
		     event.stopPropagation();
	        }
	    }	
	}, '#contentDivNode');

	$('[data-component]').on({
		'focusin': function(event){
			return;
			var focusedNode = event.target;
			var typeValue = $(focusedNode).attr('type');
			switch (typeValue){
				case 'text':
					$(focusedNode).attr('data-focusin-data', $(focusedNode).text());
					$(focusedNode).removeAttr('data-focusout-data');
					break;
				case 'checkbox':
					$(focusedNode).attr('data-focusin-data', $(focusedNode).is(':checked'));
					$(focusedNode).removeAttr('data-focusout-data');
					break;
				case 'radio':
					$(focusedNode).attr('data-focusin-data', $(focusedNode).is(':checked'));
					$(focusedNode).removeAttr('data-focusout-data');
					break;
				case 'file':
					$(focusedNode).attr('data-focusin-data', $(focusedNode).val());
					$(focusedNode).removeAttr('data-focusout-data');
					break;
				case 'number':
					$(focusedNode).attr('data-focusin-data', $(focusedNode).val());
					$(focusedNode).removeAttr('data-focusout-data');
					break;
				default:
					break;
			}
			console.log('focus in');
		},
		'focusout': function(event){
			console.log('focus out');
			var focusedNode = event.target;
			focusedNode = $(focusedNode).closest('[type]');
			var typeValue = $(focusedNode).attr('type');
			switch (typeValue){
				case 'text':
					//clean the track changes and get text
					$(focusedNode).attr('data-focusout-data', $(focusedNode).clone(true).excludeRemoveNode().cleanTrackChanges().text());
					break;
				case 'checkbox':
					$(focusedNode).attr('data-focusout-data', $(focusedNode).is(':checked'));
					break;
				case 'radio':
					if($(focusedNode).is(':checked') == true){
						var radioName = $(focusedNode).attr('name');
						$(focusedNode).closest('[data-component]').find('[name="' + radioName + '"]').removeAttr('data-focusout-data');
					}
					$(focusedNode).attr('data-focusout-data', $(focusedNode).is(':checked'));
					break;
				case 'file':
					$(focusedNode).attr('data-focusout-data', $(focusedNode).val());
					break;
				case 'number':
					$(focusedNode).attr('data-focusout-data', $(focusedNode).val());
					break;
				case 'inputTags':
					var tags = $(focusedNode).find('.kriya-chips .tags');
					var focusData = '';
					tags.each(function(){
						focusData += $(this).text() + ', ';
					});
					focusData = focusData.replace(/(^[\s]+|[\,\s]+$)/g, '');
					$(focusedNode).attr('data-focusout-data', focusData);

					break;	
				default:
					break;
			}
		},
		'change': function(event){
			console.log('change');
			console.log(event.target);
		}
	}, '[data-selector]');
  
  	//if($('#contentContainer [data-id^="BLK_"][data-current-page]').length<=0 || $('#contentDivNode').data("pdf")=="")){
	if($('#contentContainer [data-id^="BLK_"]').length<=0 || $('#contentDivNode').data("pdf")==""){
		$("#blockelemenu span").addClass("disabled");
	}
	
	$(".jrnlCitation").find('*').each(function(){
		$(this).cleanTrackChanges();
		$(this).cleanTrackAttributes();
	});
});


function elementOnScrollBar(elementToScroll, dataAttr, citationType){//add elements position on scroll bar
	$('.content-scrollbar .node').remove();
	var contentHeight = 0;
	$('#contentDivNode > *').each(function(){contentHeight += $(this).height()});
	$(elementToScroll).each(function(){
		if ($(this).attr('data-track') == "del") return true;
		var rid = $(this).attr(dataAttr);
		var nodeXpath = kriya.getElementXPath($(this)[0]);
		//var factor =($('.content-scrollbar > div').position().top/$('.content-scrollbar').height())*100;
		//var factor = (contentHeight/$('.content-scrollbar').height());
		var topPos = ($('#contentDivNode').scrollTop() + $(this).position().top);// - $('#contentDivNode').offset().top;
		topPos = ($('#contentDivNode').height() * ((topPos/contentHeight)*100))/100;
		//topPos = topPos - 5;
		//var thisPos = heightLost + topPos;
		//var factor = (topPos/contentHeight)*100
		//var topPos = (topPos/factor);
		//topPos = ($('.content-scrollbar').height() * (factor/100));//- ($('.content-scrollbar > div').height()/2);
		var node = $('<div data-rid="' + rid + '" class="node highlighted citations_' + citationType + '" style="top:' + topPos + 'px;" data-channel="menu" data-topic="navigation" data-event="click" data-message="{\'funcToCall\': \'scrollElementOnContent\'}"/>')
		$(node).attr('data-node-xpath', nodeXpath);
		$('.content-scrollbar').append(node);
	});
}

/**
 * Function to change the attribute name 
 * http://wowmotty.blogspot.in/2011/10/jquery-rename-attribute-renameattr.html
 */

$.fn.extend({
  renameAttr: function( name, newName, removeData ) {
    var val;
    return this.each(function() {
      val = $.attr( this, name );
      $.attr( this, newName, val );
      $.removeAttr( this, name );
      // remove original data
      if (removeData !== false){
        $.removeData( this, name.replace('data-','') );
      }
    });
  }
});

/**
 * Functio to change the element name
 */
$.fn.renameElement = function(newTag, renameUsingData){
	var newEle;
	this.each(function(i, el){
		var $el = $(el);
		//rename using the data-tag attribute
		if(renameUsingData){
			newTag = $(el).attr('data-tag');
			$(el).removeAttr('data-tag');
		}
		//continue if the newTag is null
		if(!newTag){
			return;
		}
		$newTag = $("<" + newTag + ">");

		// attributes
		$.each(el.attributes, function(i, attribute){
			$newTag.attr(attribute.nodeName, attribute.nodeValue);
		});
		
		$newTag.html($el.html());
		$el.replaceWith($newTag[0]);
		newEle = $newTag;
	});
	return newEle;
};

/**
* Function to track the changes in the content
* @param action : Action thet performed in content (Insret, Deleted, Formated)  
* @param actionCallback : Call the function after accept reject triggered
*/
$.fn.extend({
	kriyaTracker: function(action, actionCallback, cid, trackDetail) {
		return this.each(function() {
			var node = this;

			//Remove the inserted element if action is delete and the node inserted by the same user
			if($(node).closest('.ins, [data-track="ins"]').length > 0 && action == "del" && $(node).closest('.ins, [data-track="ins"]').attr('data-userid') == kriyaEditor.user.id){
				kriya.lastRemoveNodeSibling = node.nextSibling;
				$(node).remove();
				return;
			}else if($(node).closest('.del, [data-track="del"]').length > 0 && action == "del"){
				//If the element is alerdy deleted and action is delte then return false
				return;
			}

			if(node.nodeType == 3){
				var trackNode   = $('<span />');
				trackNode.attr('id', uuid.v4());
				trackNode.addClass(action);
				trackNode.addClass('cts-1');
			}else{
				trackNode = $(node);
				//Remove the existing track class
				trackNode.removeClass('ins').removeClass('del').removeClass('sty').removeClass('styrm');
				trackNode.attr('data-track', action);
			}
			
			var currDate    = new Date();
			var time        = currDate.getTime();
			//var timeString  = currDate.toLocaleDateString("en-au", {year: "numeric", month: "numeric",day: "numeric",hour: "2-digit",minute: "2-digit"});
			// Modify the date format - rajesh
			var timeString = ice.dom.date('M d Y h:ia', time);
			cid         = (!cid)? Date.now():cid;
			
			trackNode.attr('data-cid', cid);
			trackNode.attr('data-username', kriyaEditor.user.name);
			trackNode.attr('data-userid', kriya.config.content.role + ' ' +kriyaEditor.user.id);
			trackNode.attr('data-time', time);
			
			if(actionCallback){
				trackNode.attr('data-callback', actionCallback);
			}
			
			if(trackDetail){
				trackNode.attr('data-track-detail', trackDetail);
			}
			
			if (action == "del"){
				trackNode.attr('title', 'Deleted by ' + kriyaEditor.user.name + ' - '+timeString);
			}else if(action == "ins"){
				trackNode.attr('title', 'Inserted by ' + kriyaEditor.user.name + ' - '+timeString);
			}else if(action == "sty"){
				trackNode.attr('title', 'Formatted by ' + kriyaEditor.user.name + ' - '+timeString);
			}

			//It should be change

			//To wrap inner html instead of entire node
			/*trackNode.html($(node).html());
			$(node).html(trackNode);*/
			if(node.nodeType == 3){
				$(node).wrapAll(trackNode);
				trackNode = $(node).parent();
			}
			//not adding front matter elements in changes tab - jai 21-08-2017
			if ($(node).closest('#contentDivNode').length > 0 && $(node).closest('.front').length == 0){
				eventHandler.menu.observer.addTrackChanges($(trackNode)[0], 'added');
				//eventHandler.menu.observer.nodeChange({'node':$(node)});
			}
		});
	},
	/**
	 * Function to remove the track changes in the element
	 */
	cleanTrackChanges: function(){
		return this.each(function() {
			$(this).find('.del,[data-track="del"]').remove();
			$(this).find('.ins').contents().unwrap();
			if($(this).attr('data-track') == "ins"){
				$(this).cleanTrackAttributes();
			}else if($(this).attr('data-track') == "del"){
				$(this).remove();
			}
			$(this).find('[data-track="ins"]').each(function(){
				$(this).cleanTrackAttributes();
			});
		});
	},
	/**
	 * Function to clean track attributes
	 */
	 cleanTrackAttributes: function(){
		var trackAttr = ['data-track', 'data-cid', 'data-userid', 'data-username', 'data-time', 'data-callback', 'data-track-detail', 'data-track-hint', 'title'];
		return this.each(function() {
			for (var i = 0; i < trackAttr.length; i++) {
				$(this).removeAttr(trackAttr[i]);
			}
			$(this).removeClass('ins del sty styrm cts-1'); //remove track class
		});
	},
	/**
	 * Function to remove the "removeNode" in the element
	 */
	excludeRemoveNode : function(){
		return this.each(function() {
			$(this).find('.removeNode').remove();
		});
	}
});

/**
 * Functio to progress on the element
 * This progress function now only handled for the popup component
 */
$.fn.progress = function(label,remove){
	label = (label) ? label : 'Loading';
	var loadingEle = $('<div class="loadingProgress"><span data-label="progress">' + label + '...</span><div class="progress"><div class="indeterminate"/></div></div>');
	this.each(function(i, el){
		var comp = $(this).find('[data-input-editable]:visible');
		comp = (comp.length > 0) ? comp : $(this);
		$(this).find('.loadingProgress').remove();
		$(this).find('.loading-wraper').contents().unwrap();
		if(remove){
			return;
		}
		//comp.children().wrap("<div class='loading-wraper'></div>");
		var loadingWraper = $("<div class='loading-wraper' />");
		loadingWraper.append(comp[0].childNodes);
		comp.prepend(loadingWraper);
		//comp.html("<div class='loading-wraper'>" + comp.html() + "</div>");
		
		$(this).append(loadingEle);
		loadingEle.css('width',comp.css('width'));
		//loadingEle.css('top','-'+comp.css('top'));
		loadingEle.css('display','block');
		
		var compoHeght = parseFloat(comp.height());
		loadingEle.css('bottom', compoHeght/2);
	});
};

/**
 * Function to get the total number fo column
 */
$.fn.getTotalColumn = function(){
	var node = this[0];
	var colLen = 0;
	$(node).find('tbody tr:first td,tbody tr:first th ').each(function(i){
		if($(this).attr('colspan')){
			colspan = parseInt($(this).attr('colspan'));
			colLen  = colspan+colLen;
		}else{
			colLen++;
		}
	});
	return colLen;
};

/**
 * Function to get the total number fo column
 */
$.fn.getRowColumn = function(){
	var node = this[0];
	var colLen = 0;
	$(node).find('td,th ').each(function(i){
		if($(this).attr('colspan')){
			colspan = parseInt($(this).attr('colspan'));
			colLen  = colspan+colLen;
		}else{
			colLen++;
		}
	});
	return colLen;
};

/**
* Function to remove the all attributes
* @param skipAttr - Arrar of attributes
*/
$.fn.removeAllAttr = function(skipAttr){

	return this.each(function(){
		var that = $(this);
		var attributes = $.map(this.attributes, function(item) {
			if($.inArray(item.name, skipAttr) < 0){
				return item.name;
			}
    	});
		$.each(attributes, function(i, item) {
    		that.removeAttr(item);
    	});
	});
};

/**
 * Object.values function will not support in chrome version less than 54
 * and in internet explorer so, if Object.values type is not a function than 
 * the below function will execute
 */
if(typeof(Object.values) != "function"){
	Object.values = function(obj){
		var returnVal = [];
		if(typeof(obj) == "object"){
			for(var v in obj){
				returnVal.push(obj[v]);
			}
		}
		return returnVal;
	}
}

function moveCursor(ele, pos){
    var range,selection;
    if(document.createRange && ele){
        range = document.createRange();
        range.setStart(ele, pos);
        //range.setEnd(ele, pos);
        //range.selectNodeContents(contentEditableElement);
        range.collapse(true);
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

/**
 * Function to get the cursor position in the element 
 * Function get from stackoverflow.com
 * Added by jagan 
 */
function getCaretCharacterOffsetWithin(element) {
	if(!element || $(element).length == 0){
		return false;
	}
    var caretOffset = 0;
    var doc = element.ownerDocument || element.document;
    var win = doc.defaultView || doc.parentWindow;
    var sel;
    if (typeof win.getSelection != "undefined") {
        sel = win.getSelection();
        if (sel.rangeCount > 0) {
            var range = win.getSelection().getRangeAt(0);
            var preCaretRange = range.cloneRange();
            preCaretRange.selectNodeContents(element);
            preCaretRange.setEnd(range.endContainer, range.endOffset);
            caretOffset = preCaretRange.toString().length;
        }
    } else if ( (sel = doc.selection) && sel.type != "Control") {
        var textRange = sel.createRange();
        var preCaretTextRange = doc.body.createTextRange();
        preCaretTextRange.moveToElementText(element);
        preCaretTextRange.setEndPoint("EndToEnd", textRange);
        caretOffset = preCaretTextRange.text.length;
    }
    return caretOffset;
}

/**
 * Function to set the cursor position in the element
 * @author jagan
 */
function setCursorPosition($element, position) {
	var textChilds = textNodesUnder($element);
	var tLength = 0;
	$(textChilds).each(function(){
		tLength = tLength + this.nodeValue.length;
		if(tLength >= position){
			var remaingLength = tLength-position;
			var thisPos = Math.abs(this.nodeValue.length-remaingLength);
			moveCursor(this, thisPos);
			return false;
		}
	});
}

/**
 * Function to get the all text node from element
 * https://stackoverflow.com/questions/10730309/find-all-text-nodes-in-html-page
 */
function textNodesUnder(el){
	var n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
	while(n=walk.nextNode()) a.push(n);
	// if the element is already a text node and no child text node is available, push text element in to array - JAI
	if (a.length == 0 && el.nodeType == 3){
		a.push(el)
	}
	return a;
}
/**
 * Function to get all comment nodes under the given node
 * https://stackoverflow.com/questions/31966118/get-comment-nodes-in-dom-deep-level
 */
function commentNodesUnder(el){
	var treeWalker = document.createTreeWalker(el,NodeFilter.SHOW_ALL,null,false);
	var commentList = [];
	while (treeWalker.nextNode()){
		// keep only comments
		if (treeWalker.currentNode.nodeType === 8) 
		commentList.push(treeWalker.currentNode);
	}
	return commentList;
}

/**
*Function to get the string as object
*/
function getStringObj(fnString){
    var arr = fnString.split('.');
    var fn = window[ arr[0] ];
    
    for (var i = 1; i < arr.length; i++)
    { fn = fn[ arr[i] ]; }
    return fn;
}

function cleanforInputs(text, className){
	var parent = "kriya-chips";
    //#292 - Libre Office - only when condition checks - Mohammed Navaskhan (mohammednavas.a@focusite.com)
    try{
	if(className == parent){
		text = text.innerHTML();
	}
        }catch(e){
            
        }

	return text;
        
}

function getStyle(style,attr){
    if(!style) return false;
    matchStr = new RegExp('\\s?'+attr+'[\\-\\w+]*\\s?\\:\\s?[^\\;|$]*','gmi');
    var styles='';
    if(match = style.match(matchStr)){
        if(match.length > 0){
            match.forEach(function(styleAttr){
                styles+= styleAttr;
            });
        }
    }
	return styles ? styles : false;
}

function removeTralingSpaces(node, leading, trailing){
	var textNodes = textNodesUnder(node);
	var tnLength = textNodes.length;
	if (leading){
		for(var t=0;t<tnLength;t++){
			var tNode = textNodes[t];
			if ($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef').length == 0){
				var firstNodeText = tNode.nodeValue;
				if (firstNodeText.match(/^([\u0020]+)/g)){
					tNode.nodeValue = firstNodeText.replace(/^([\u0020]+)/, '');
					break;
				}else{
					break;
				}
			}
		}
	}
	if (trailing){
		for(var t=tnLength-1;t>=0;t--){
			var tNode = textNodes[t];
			if($(tNode).closest('.del, [data-track="del"], .jrnlDeleted, .forceJustify, .jrnlQueryRef').length == 0){
				var lastNodeText = tNode.nodeValue;
				if(lastNodeText.match(/([\u0020]+)$/g)){
					tNode.nodeValue = lastNodeText.replace(/([\u0020]+)$/, '');
					break;
				}else{
					break;
				}
			}
		}
	}
	return node
}

function cleanupOnPaste(text){
	text = text.replace(/<!--[\s\S]+?-->/g, '');
	text = text.replace(/[\n\r]+/g, ''); // Remove only the line break rather than the tab character to restore spaces - hamza
	var $result = $('<div></div>').html(text);
	
	//Remove the start end spaces in the para - jagan
	$result.find('p').each(function(){
		removeTralingSpaces(this, true, true);
	});

	//Remove the comment node - jagan
	var cNodes = commentNodesUnder($result[0]);
	$(cNodes).each(function(){
		$(this).remove();
	});
	var dataOrg = ['border','margin','padding','background'];

	$result.find('td,tr,th').each(function(i,e){
		if(style = e.getAttribute('style')){
			dataOrg.forEach(function(ele){
				if(attr = getStyle(style,ele)){
					if(ele=='background'){
						e.setAttribute('data-table-background-color',attr.replace('background:',''));
					}else{
						e.setAttribute('data-original-'+ele,attr);
					}
				};
			});
		}
	
		if(bgcolor = e.getAttribute('bgcolor')){
			e.setAttribute('data-table-background-color', bgcolor);
			e.removeAttribute('bgcolor');e.removeAttribute('height');
		}
	});
	
    $result.children('style,title,meta,link,script').remove();
	$result.find(':empty:not("td,tr"), colgroup').remove();
     // replace all styles except bold and italic
    $.each($result.find("*"), function(idx, val) {
		var $item = $(val);
		//Set the new id for the element 
		$item.attr('id', uuid.v4());
         cleanRoutine($item);
         
         function cleanRoutine(rawHTML){
            if ($item.length > 0){
                if (($item.css('font-weight') > 400) ||($item.css('font-weight') == 'bold')){
                     $item.wrap('<strong>');
                }
                if (($item.css('font-style') != 'normal') && ($item.css('font-style') != '')){
                    $item.wrap('<em>');
                }
                if (($item.css('text-decoration') == 'underline') && ($item.css('font-style') != '')){
                    $item.wrap('<U>');
                }
				if(!/^(TABLE|TD|TH|TBODY|TR|THEAD)$/i.test($item[0].nodeName)){
					$item.removeAttr('style');
				}
                if (!/^(I|SUP|SUB|B|STRONG|EM|U|A|OL|UL|LI|TABLE|TD|TH|TBODY|TR|THEAD)$/i.test($item[0].nodeName)){
                    if (!(($item[0].attributes.class) && ($item[0].attributes.class.textContent.match(/^(del|ins|jrnl|Ref|label)/gi)))){
						$item.contents().unwrap();
                    }
                }
                return rawHTML;
            }
        }
    });
    $result.find('strong,b').find('b,strong').contents().unwrap();
    $result.find('em,i').find('i,em').contents().unwrap();
	$result.find('span').each(function(){
		if($(this).attr('class') && $(this).attr('class').match(/^jrnl/)){
			$(this).contents().unwrap();
		}
	});

    return $result.html();
}

function excel2Table(file, callback){
	if(file){
		var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
		var reader = new FileReader();
		reader.onload = function(e) {
			var data = e.target.result;
			var returnVal = false;
			try {
				var arr = rABS ? data : btoa(fixdata(data));
				var wb = XLSX.read(arr, {type: rABS ? 'binary' : 'base64'});
				returnVal = XLSX.utils.sheet_to_html(wb.Sheets[wb.SheetNames[0]], {editable:true});
			} catch(e) {
				console.log(e);
			}
			if(callback && typeof(callback) == 'function'){
				callback(returnVal);
			}
		};

		if(rABS) reader.readAsBinaryString(file);
		else reader.readAsArrayBuffer(file);
	}else{
		if(callback && typeof(callback) == 'function'){
			callback(false);	
		}
	}
}

function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
	return o;
}

/**
 * https://stackoverflow.com/questions/10841773/javascript-format-number-to-day-with-always-3-digits
 * @param value 
 * @param padding 
 */
function lpad(value, padding) {
    var zeroes = new Array(padding+1).join("0");
    return (zeroes + value).slice(-padding);
}

/**
 * Synchronous function
 * @author - jagan
 * @param func
 */
function syncFunction(func){
	if(typeof(func) == "function"){
		setTimeout(function(){
			func();
		}, 0);
	}
}


/**
 * Function to check is the cursor is in that statrting of sentense
 */
function caretIsFirstInSentence(){
	if(kriya.selection){
		var focusedNode = kriya.selection.startContainer;
		var focusOffset = kriya.selection.startOffset;
		//If the cursor is in the starting of the block node
		if(editor.composer.selection.caretIsFirstInSelection()){
			return true;
		}

		//if the focused element is text and previous two character from cursor is sentese ending puntuation then return true
		//else if focusOffset is zero and parent element is block node (if the cursor is in starting of block node)
		if(focusedNode && focusedNode.nodeType == 3 && focusOffset > 1){
			var nodeValue = focusedNode.nodeValue;
			var prevChar = nodeValue.substring(focusOffset-2, focusOffset);
			if(prevChar.match(/^((\.\s)|(\;\s)|(\!\s)|(\?\s))$/g)){
				return true;
			}
		}else if(focusOffset == 0 && focusedNode.parentElement){
			var blockNodeRegex = /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i;
			if(blockNodeRegex.test(focusedNode.parentElement.nodeName)){
				return true;
			}
		}
	}
	return false;
}

function addLogicalBreaksToLinks(currLink){
	var linkStr = currLink;
	//var linkStr = 'http://dx.doi.org/10.1136/openhrt-2016-000417';
	//linkStr = 'http://static.www.bmj.com/sites/default/files/responseattachments/2015/10/Figure%20271015.docx';
	var lineBreakChar = "\u200B";//String.fromCodePoint(0x200B);
	// split the given link into protocol and remaining part
	var linkPartsArr = /^(https?:\/\/(?:www\.)?|www\.)(.*)$/g.exec(linkStr);
	if (linkPartsArr == null){
		linkPartsArr = ['', '', linkStr];
	}
	var protocolStr = linkPartsArr[1];
	// split the remaining part of the link into array
	var temp = linkPartsArr[2];
	var processedLinkStrArr = [protocolStr + lineBreakChar];
	var index = 0;
	// if the temp string begins with alphanumeric characters and has a punctuation then take that part alone for processing 
	while ((/^.*?[^a-z0-9]/i.test(temp)) || (index++ < 25)){
		var tempArr = /(.*?)([^a-z0-9])/i.exec(temp);
		temp = temp.replace(/^.*?[^a-z0-9]/i, '');
		if (tempArr == null){
			linkStrPart = temp;
			var linkStrPunc = '';
			index = 100; // push the index to higher value so the loop will break
		}
		else{
			linkStrPart = tempArr[1];
			// updated by PM, May 5, 2017
			// if length of string before the punctuation is one, then its not okay to split the string at that point
			// so skip adding the link break character for that punctuation
			if (linkStrPart.length == 1){
				var linkStrPunc = tempArr[2];
			}
			else{
				var linkStrPunc = tempArr[2] + lineBreakChar;
			}
		}
		// as a design assumption we try to split the character only if we have a minimum of 8 characters
		// we split at 4 characters
		if (linkStrPart.length > 20){
			var currStrArr = linkStrPart.match(/.{1,4}/g)
			// merge the last element of the array with its previous element and remove the last element
			// this is done to avoid leaving those characters on a separate line on the proof
			var len = currStrArr.length;
			if (len > 1){
				currStrArr[len - 2] = currStrArr[len - 2] + currStrArr[len - 1]
				currStrArr.pop();
			}
			processedLinkStrArr = processedLinkStrArr.concat(currStrArr.join(lineBreakChar));
		}
		if ((linkStrPart + linkStrPunc) != ''){
			processedLinkStrArr.push(linkStrPart + linkStrPunc);
		}
	}
	// if the last character is a punctuation then the lineBreakChar would have been added before it. please remove.
	processedLinkStr = processedLinkStrArr.join('');
	return processedLinkStr;
}

function nodeIsFirstInSentence(ele){
	if(ele){
		var parentNode = ele.parentElement;
		var prevEle = ele.previousSibling;
		var blockNodeRegex = /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i;
		while(!prevEle && !blockNodeRegex.test(parentNode.nodeName)){
			parentNode = parentNode.parentElement;
			prevEle = parentNode.previousSibling;
		}
		/**
		 * If the node is first child in the block node then return true
		 */
		if(blockNodeRegex.test(parentNode.nodeName) && parentNode.firstChild == ele){
			return true;
		}
		// if prevEle not there code will break some times it will come as undefined
		//if(prevEle.nodeType != 3){
		if(prevEle && prevEle.nodeType != 3){
			var prevTextNodes = textNodesUnder(prevEle);
			if(prevTextNodes.length > 0){
				prevEle = prevTextNodes[prevTextNodes.length-1];
			}
		}
		//if(prevEle.nodeType == 3){
		if(prevEle && prevEle.nodeType == 3){
			var prevNodeVal = prevEle.nodeValue;
			if(prevNodeVal && prevNodeVal.match(/^((\.\s)|(\;\s)|(\!\s)|(\?\s))$/g)){
				return true;
			}
		}
	}
	return false;
}


function setTableIndex(table){
	table = $(table);
	if(!table || table.length == 0) return;
	table.find('.headerRow, .leftHeader').remove();
	
	//$('#content table .headerRow,#content table .leftHeader,#content table colgroup').remove();
	//$('#content table').removeClass('ecsTable');
	/*table.addClass('ecsTable');
	//create column group
	$(table).find('th,td').each(function(){
		getCellLocation($(this));
	});*/
	
	if($(table).find('thead').length > 0){
		var firstRow = $(table).find('thead tr:first');
	}else{
		var firstRow = $(table).find('tr:first');
	}
	
	//insert header row/column
	//insert new td for every first td of a row
	var firstTds = $(table).find('tr').find('td:first,th:first');
	firstTds.each(function(i, e){
		$(this).before('<td class="leftHeader" width="2%"><div class="leftHeaderText">'+(i+1)+'</div></td>');
	});
	
	var numExistTds = firstRow.children().length;
	var rowSpan = firstRow.find('td[colspan], th[colspan]');
	var rowSpansVal = 0;
	rowSpan.each(function(){
		rowSpansVal = rowSpansVal + parseInt($(this).attr('colspan'));
	});
	// total td to be inserted = (total td - no of td with colspan) + total colspan value ;
	//since we insert another column at first add another td
	var numNewTds =  ( numExistTds -  rowSpan.length ) + rowSpansVal;
	var alphabet  = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var newRow    = '<tr class="headerRow"><td class="dummyHeader">&nbsp;</td>';
	for(var i=1; i < numNewTds; i++){
		newRow += '<td class="topHeader" data-col-pos="col-'+(i-1)+'">'+alphabet.charAt(i)+'</td>';
	}
	newRow += '</tr>';
	firstRow.before(newRow);
	return table;
}

	//#535 - Convert Greek, Alphabet, roman into numbers
	function fromRoman(str) { 
		str = str.toLowerCase();
	 	var result = 0;
	  	// the result is now a number, not a string
	  	var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
		var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
		for (var i = 0;i<=decimal.length;i++) {
			while (str.indexOf(roman[i]) === 0){
			result += decimal[i];
			str = str.replace(roman[i],'');
			}
		}
		return result;
	}
	
	function str_split(string, split_length) {
		if (split_length == null) {
			split_length = 1;
		}
		if (string == null || split_length < 1) {
			return false;
		}
		string += '';
		var chunks = [],
			pos = 0,
			len = string.length;
		while (pos < len) {
			chunks.push(string.slice(pos, pos += split_length));
		}
		return chunks;
	}
	
	function fromAlpha(string){
		string = string.toLowerCase();
		var alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
		var splitted_string = str_split(string);
		var count = 0;
		for (i = 0; i < splitted_string.length; i++) { 
			var letterPosition = alphabet.indexOf(splitted_string[i])+1;
			count = (count*26)+letterPosition;
		}
		return count;
	}
	
	function fromGreek(string){
		string = string.toLowerCase();
		var greekAlphabet = ["α","β","γ","δ","ε","ζ","η","θ","ι","κ","λ","μ","ν","ξ","ο","π","ρ","σ","τ","υ","φ","χ","ψ","ω"];
		var splitted_string = str_split(string);
		var count = 0;
		for (i = 0; i < splitted_string.length; i++) { 
			var letterPosition = greekAlphabet.indexOf(splitted_string[i])+1;
			count = (count*24)+letterPosition;
		}
		return count;
	}
	//End of #535	
