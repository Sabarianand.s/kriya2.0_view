/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    kriya.inlineEditing = {

        init: function (elm, component, evt) {
			this.container = elm;
			this.component = component;
            addEditor(component);
			
			function addEditor (component) {
				if ($('[data-type="'+component.type+'"][data-component="'+component.name+'"]').length > 0){
					var comp = $('[data-type="'+component.type+'"][data-component="'+component.name+'"]');
					comp.find('[data-type="inlineEditing"]').html(kriya.inlineEditing.container.html());
					comp.find('[data-type="inlineEditing"]')[0].focus();
				}
			}
        },
		
		detach: function () {
			
		},

    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));