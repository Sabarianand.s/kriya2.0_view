/* jQuery Case Change plugin: Performing case changing for a given text
 * with many options for case changing
 * Copyright (C) 2013 Word Count Tool - http://wordcounttools.com
 * Licensed under the MIT (MIT-license.txt)
 */

//Utilities, deal with old browers that do not support Object.create function
if (typeof Object.create !=='function') {
  Object.create=function(obj){
    function F(){};
    F.prototype=obj;
    return new F();
  }
};

//Beginning plugin
(function($,window,document,undefined){
  //CaseChanger object, the main logic of the plugin
  var CaseChanger={
    init:function(options,elem){    
      //Overind default options
      this.options=$.extend({},$.fn.caseChange.options,options);

      //If performing on a DOM element
      if(typeof elem ==='object'){
        this.elem=elem;
        this.$elem=$(elem);
        //this.textToChangeCase=this.$elem.val();
        //MODIFY BY - jagan Get the value if the element is input or text area else get the html
        this.textToChangeCase = (this.$elem[0].nodeName == "INPUT" || this.$elem[0].nodeName == "TEXTAREA")?this.$elem.val():this.$elem.html();
      }
      else {   //typeof elem ==='function' - Performing directly from jQuery function/ object
        this.textToChangeCase=options.textToChangeCase;        
      }
      
      return this.performCaseChange();
    },

    performCaseChange:function(){
      switch(this.options.toCase){
        case "upperCase":
          this.changedCaseText=this.textToChangeCase.toUpperCase();
          break;
        case "lowerCase":
          this.changedCaseText=this.textToChangeCase.toLowerCase();
          break;
        case "sentenceCase":
          var text=this.textToChangeCase+'.'
          text=text.replace(/\w[^.?!:\n]*[.?!:\n$]+/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
          this.changedCaseText=text.substr(0,text.length-1);
          break;
        case "titleCase":
          this.changedCaseText=this.toTitleCase();    
        break;
        case "toggleCase":
          this.changedCaseText=this.textToChangeCase.replace(/[\w]*/g, function(txt){return txt.charAt(0).toLowerCase() + txt.substr(1).toUpperCase();});
          console.log(this.textToChangeCase);
          break;
      }
      return this.display();
    },

    display:function(){
      if(this.elem){
        var outputElem=this.$elem;
        if(this.options.toElement){
          outputElem=this.options.toElement;
        }
        //MODIFY BY - jagan update the value if the element is input or text area else update the html
        if(outputElem[0].nodeName == "INPUT" || outputElem[0].nodeName == "TEXTAREA"){
          outputElem.val(this.changedCaseText);
        }else{
          outputElem.html(this.changedCaseText);
        }
        return;   
      }
      else
        return this.changedCaseText;
    },
    toTitleCase: function() {
      var i, str, lowers, uppers;
      str = this.textToChangeCase.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });

      // Certain minor words should be left lowercase unless 
      // they are the first or last words in the string
      lowers = this.options.alwaysLowerCase;
      for (i = 0; i < lowers.length; i++)
          str = str.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'), 
              function(txt) {
                  return txt.toLowerCase();
              });

      // Certain words such as initialisms or acronyms should be left uppercase
      uppers = this.options.alwaysUpperCase;
      for (i = 0; i < uppers.length; i++)
          str = str.replace(new RegExp('\\b' + uppers[i] + '\\b', 'g'), 
              uppers[i].toUpperCase());

      return str;
    }
  }

  caseChangingFunction=function(options){
    var caseChanger=Object.create(CaseChanger);
    return caseChanger.init(options,this);
  }

  //If a DOM element calls this function
  $.fn.caseChange=function(options){
    return this.each(function(){
      //var caseChanger=Object.create(CaseChanger);
      //caseChanger.init(options,this);
      return caseChangingFunction.call(this,options);
    });
  }

  //Extend utility function for jQuery
  $.extend({caseChange:function(options){
    //var caseChanger=Object.create(CaseChanger);
    //return caseChanger.init(options,this);
    return caseChangingFunction.call(this,options);
  }});


  $.fn.caseChange.options={
      textToChangeCase:null,  //If performing case changing from a DOM element, this property is ignored
      toCase:'lowerCase',
      toElement:null,
      alwaysLowerCase:['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At', 
      'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'To', 'With'],
      alwaysUpperCase:['Id', 'Tv']
  }
})(jQuery,window,document);
