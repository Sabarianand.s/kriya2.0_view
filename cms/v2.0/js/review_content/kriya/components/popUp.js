/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    // 1. CONFIGURATION
    var config = {
    };

    // 2. COMPONENT OBJECT
    kriya.popUp = {

        init: function (elm, component) {
			this.container = elm;
			this.evt = kriya.evt;
			this.component = component;
            this.getTemplate(component, elm);
        },
		detach: function () {
			
		},

        /**
		*# Function which iterates all '[data-type][data-class]' elements inside the current modal and
		*# get the data-type from it and decides to which function the element has to be sent
		**/
		getTemplate: function (component, containerElement) {
			if ($('[data-type="popUp"][data-component="'+component.name+'"]').length > 0){
				var popper = $('[data-type="popUp"][data-component="'+component.name+'"]');
				
				if (popper.attr('data-sub-type') == undefined && containerElement.attr('data-sub-type') == undefined && popper.attr('data-component') != "tableMenu"){
					$('[data-type="popUp"]').addClass('hidden').removeClass('manualSave');
				}
				var nodeXpath = kriya.getElementXPath(this.container);
				$('[data-element-added]').removeAttr('data-element-added');
				popper.find('[data-node-xpath]').removeAttr('data-node-xpath');
				popper.attr('data-node-xpath', nodeXpath)
				this.cleanComponents(popper);
				$(popper).find('*[data-if-selector]').addClass('hidden');
				$(popper).find('*[data-error]').removeAttr('data-error');
				$(popper).find('*[data-error-count]').removeAttr('dara-error-count');
				$(popper).find('*[add-class]').each(function(){
					$(this).addClass($(this).attr('add-class'));
				});
				$(popper).find('*[remove-class]').each(function(){
					$(this).removeClass($(this).attr('remove-class'));
				});
				if ($(kriya.popUp.container).closest(kriya.config.containerElm).length == 0){
					var selectedNodes = kriya.selectedNodes;
					// Check selectedNodes is an undefined value.
					if (selectedNodes && selectedNodes.length == 0){
						selectedNodes[0] = kriya.selection.startContainer;
					}
					if (selectedNodes && selectedNodes.length == 1 && selectedNodes[0].nodeType == 3){
						selectedNodes[0] = selectedNodes[0].parentElement;
						//we are not using this if condition.
						/*if (kriya.config.blockElements.indexOf(selectedNodes[0].nodeName.toLocaleLowerCase()) == -1){
							//selectedNodes[0] = selectedNodes[0].parentElement;
						}*/
					}
					console.log(selectedNodes);
					if(selectedNodes && $(kriya.selection.startContainer).closest('*[id]').not(kriya.config.invalidSaveNodes).length > 0){
						kriya.popUp.container = $(kriya.selection.startContainer).closest('*[id]').not(kriya.config.invalidSaveNodes);
					}
				}
				if ($(popper).find('*[data-class-selector]').length > 0){
					$(popper).find('*[data-class-selector]').each(function(){
						var selectorDiv = $(this);
						var ifSelector = $(this).attr('data-class-selector');
						$(this).addClass('hidden');
						if ($(kriya.popUp.container)[0].hasAttribute('class') && (ifSelector).match(' ' + $(kriya.popUp.container).attr('class').split(' ')[0] + ' ')){
							if ($(this)[0].hasAttribute('data-if-selector')){	
								var xpathSelector = $(this).attr('data-if-selector');
								var xpathSelector = kriya.validateXpath($(this), $(kriya.popUp.container));
								var condition = kriya.xpath(xpathSelector, kriya.popUp.container);
								if (condition.length > 0){
									selectorDiv.removeClass('hidden');
								}
							}else{
									selectorDiv.removeClass('hidden');
							}
						}
						$(selectedNodes).each(function(){
							if ($(this)[0].nodeType == 1 && $(this).attr('class') != undefined && ((ifSelector).match(' ' + $(this).attr('class').split(' ')[0] + ' '))){
								kriya.popUp.container = $(this);
								 selectorDiv.removeClass('hidden');
							}else if ($(this)[0].nodeType == 1 && $(this).parent().attr('class') != undefined && ((ifSelector).match(' ' + $(this).parent().attr('class').split(' ')[0] + ' '))){
								kriya.popUp.container = $(this).parent();
								 selectorDiv.removeClass('hidden');
							}
						})
					});
					if ($(popper).find('*[data-class-selector]').length == $(popper).find('*[data-class-selector].hidden').length){
						$(popper).find('*[data-class-selector*="default"]').removeClass('hidden');
					}
				}else{
					$(popper).find('*[data-if-selector]').each(function(){
						var ifSelector = $(this).attr('data-if-selector').split('|');
						var ifIdentified = true;
						for (var x = 0, xl = ifSelector.length; x < xl; x++){
							var noflag = false;
							if (/^!/.test(ifSelector[x])){
								ifSelector[x] = ifSelector[x].replace('!', '');
								noflag = true;
							}
							
							//If data-if-selector-global attribute is true thenn evaluate the xpath globally else evaluate with in the container - jagan
							if($(this).attr('data-if-selector-global') == "true"){
								var condition = kriya.xpath(ifSelector[x]);
							}else{
								var condition = kriya.xpath(ifSelector[x], kriya.popUp.container);	
							}

							if (condition.length > 0 && noflag){
								ifIdentified = false;
							}else if (condition.length == 0 && !noflag){
								ifIdentified = false;
							}else if (condition.length > 0 && $(this)[0].hasAttribute('data-if-value') && condition[0].nodeType == 2){
								if (condition[0].nodeValue != $(this).attr('data-if-value')){
									ifIdentified = false;
								}
							}
						}
						if (ifIdentified){
							$(this).removeClass('hidden');
							if ($(this)[0].hasAttribute('data-ref-type')){
								$(popper).find('.input-type select').val($(this).attr('data-ref-type'));
							}
						}
					});
					if ($(popper).find('*[data-if-selector]').length > 0){
						if ($(popper).find('*[data-if-selector]').length == $(popper).find('*[data-if-selector].hidden').length){
							$(popper).find('*[data-if-selector]').first().removeClass('hidden');
						}
					}
				}
				popper.find('*[data-visible-selector]').addClass('hidden');
				popper.find('*[data-visible-selector]').each(function(){
					var visSelector = $(this).attr('data-visible-selector');
					if(visSelector){
						var condition = kriya.xpath(visSelector, kriya.popUp.container);
						if($(condition).length > 0){
							$(this).removeClass('hidden');
						}
					}
				});

				popper.find('*[data-clone]').remove();
				popper.find('.historyTab .historyCard').remove();
				popper.find('.material-icons.fieldChange').remove();
				var containerId = $(containerElement).attr('id');
				if($(containerElement).closest('[data-component]').length > 0 && $(containerElement).attr('data-node-xpath')){
					var cSeletor = $(containerElement).attr('data-node-xpath');
					var cDataNode = kriya.xpath(cSeletor);
					containerId = $(cDataNode).attr('id');
				}
				var cards = $('#historyDivNode .historyCard[data-content-id="' + containerId + '"]').clone(true);
				popper.find('.historyTab').append(cards);
				kriya.config.subPopUpCards = undefined;

				var dataChilds = popper.find('*[data-type]');
				// iterating thru childs
				for (var i = 0, dl = dataChilds.length; i < dl; i++) {
					var dataChild = dataChilds[i];
					if (dataChild.parentElement.hasAttribute('data-type') == true && (dataChild.parentElement.getAttribute('data-type') == dataChild.getAttribute('data-type'))) continue;
					var eleType = dataChild.getAttribute('data-type');
					//checks for whether the method exists and then acts upon
					if (typeof(this[eleType]) != 'undefined'){
						this[eleType](dataChild, containerElement);
					}else if (typeof(kriya[eleType]) != 'undefined'){
						kriya[eleType].init(this.container, component, dataChild);
					}else if (typeof(kriya.general[eleType]) != 'undefined'){
						kriya.general[eleType](this.container, component, dataChild);
					}
				}
				
				// just to popup the content as a view-only mode
				if ((/_edit/.test(component.name)) || (popper.attr('data-display') == "modal")){
					if(containerElement.attr('data-sub-type') != undefined){
						popper.find('.authorAffLinks').remove();
					}
					//for edit-mode sends to open it with effects
					this.openPopUps(popper, containerElement);
				}else{
					popper.removeClass('hidden');
					var topCss = this.container.offset().top - popper.outerHeight() - 10;//(this.container.height()*2);
					popper.css('top', topCss);
					if (popper.hasClass('autoWidth')){
						topCss = kriya.pageYaxis - popper.outerHeight() - 20;//(this.container.height()*2);
						if (isNaN(topCss) && kriya.selection){
							topCss = kriya.selection.getBoundingClientRect().top - popper.outerHeight() - 10;
						}
					}
					if (popper.hasClass('hoverInline')){
						if (kriya.selection && kriya.selection.getBoundingClientRect().top > kriya.pageYaxis){
							topCss = kriya.selection.getBoundingClientRect().top - popper.outerHeight() - 20;
						}else{
							topCss = kriya.pageYaxis - popper.outerHeight() - 20;//(this.container.height()*2);
						}
						popper.css('width', 'auto');
						popper.css('height', 'auto');
					}
					if (popper.find('.material-icons.arrow-denoter').length > 0){
						//topCss = topCss - 5;
					}
					//var topCss = this.evt.pageY - popper.outerHeight() - (this.container.height()*2);
					//topCss = topCss - $(window).scrollTop();
					popper.addClass('bottom').removeClass('top').find('.material-icons.arrow-denoter').html('arrow_drop_down');
					if ((topCss - $(window).scrollTop()) < $(kriya.config.containerElm).offset().top) {
						//topCss = parseInt(this.container.css('font-size').replace('px', '')) + this.container.offset().top;
						topCss = this.container.offset().top + this.container.height() + 15;
						if (popper.hasClass('autoWidth')){
							if (kriya.selection && kriya.selection.getBoundingClientRect().bottom > kriya.pageYaxis){
								topCss = kriya.selection.getBoundingClientRect().bottom;
							}else{
								topCss = kriya.pageYaxis + 20;
							}
						}
						if (popper.hasClass('hoverInline')){
							if (kriya.selection && kriya.selection.getBoundingClientRect().bottom > kriya.pageYaxis){
								topCss = kriya.selection.getBoundingClientRect().bottom;
							}else{
								topCss = kriya.pageYaxis + 20;
							}
						}
						popper.addClass('top').removeClass('bottom').find('.material-icons.arrow-denoter').html('arrow_drop_up');
					}
					if(popper.attr('data-showbelow') == "true"){
						if (rangy && rangy.getSelection() && rangy.getSelection().getBoundingClientRect().bottom){
							var selectionPos = rangy.getSelection().getBoundingClientRect().bottom;
						 	if(	selectionPos > kriya.pageYaxis){
								topCss = rangy.getSelection().getBoundingClientRect().bottom;
								popper.addClass('top').removeClass('bottom').find('.material-icons.arrow-denoter').html('arrow_drop_up');
							}
							var popperBottom = topCss + $(popper).height();
							var containerHeight = $(kriya.config.containerElm).offset().top + $(kriya.config.containerElm).height();
							if(popperBottom > containerHeight){
								topCss = rangy.getSelection().getBoundingClientRect().top - popper.outerHeight();
								popper.addClass('bottom').removeClass('top').find('.material-icons.arrow-denoter').html('arrow_drop_down');
							}
						}
					}
					//var cp = this.container.offset().left + (this.container.width()/2) - (popper.outerWidth()/2);
					var cp = kriya.pageXaxis - (popper.outerWidth()/2);
					if (cp < $(kriya.config.containerElm).offset().left) cp = $(kriya.config.containerElm).offset().left;
					if ((cp + popper.width()) > ($(kriya.config.containerElm).offset().left + $(kriya.config.containerElm).width())) {
						cp = ($(kriya.config.containerElm).offset().left + $(kriya.config.containerElm).width()) - popper.width();
					}
					if (popper.width() > $(kriya.config.containerElm).width()) popper.width($(kriya.config.containerElm).width());
					

					//If the pop up has class pop-fixed then the container element top is hidden in the screen (offset top is less than zero) then show the pop up in the bottom of the 
					if(popper.hasClass('pop-fixed')){
						var containetDetails  = this.container[0].getBoundingClientRect();
						var scrollDivHeight   = $(kriya.config.containerElm).height();
						var offsetTop         = containetDetails.top-$('#headerContainer').outerHeight();

						if(offsetTop < 0 && (scrollDivHeight-30) < containetDetails.bottom){
							topCss = $(window).height()-popper.outerHeight();
						}else if(offsetTop < 0 && (scrollDivHeight-30) > containetDetails.bottom){
							topCss = containetDetails.bottom;
						}
					}

					popper.css('left', cp);
					popper.css('top', topCss);
					popper.css('opacity', '1');
					
					//attching events to the popper to hide it on scroll or click on document
					setTimeout(function(){
						popper[0].onclick = function (e) {
							if (e.target == this){
								e = e || window.event;
								e.cancelBubble = true;
								if (e.stopPropagation) e.stopPropagation();
							}
						}
						// click everywhere else closes the box
						document.onclick = function (e){
							if (e.target == popper){
								if (popper) {
									popper.addClass('hidden');
								}
								$('.activeElement').removeClass('activeElement');
							}
						}
						$(kriya.config.containerElm).scroll(function (){
							//if element not there in side of array code breaking
							//var containetDetails = kriya.popUp.container[0].getBoundingClientRect();
							//when we scroll in editor some times kriya.popUp.container element comming as undefined because of that code is breaking for that i added if condition for checking undefined or not.
							if($(kriya.popUp.container).length > 0){
								var containetDetails = $(kriya.popUp.container)[0].getBoundingClientRect();
								var scrollDivHeight = $(kriya.config.containerElm).height();
								var offsetTop = containetDetails.top-$('#headerContainer').outerHeight();
								if (popper && ! popper.hasClass('hidden') && !popper.hasClass('pop-fixed')){
									$('.activeElement').removeClass('activeElement');
									popper.addClass('hidden');
								}else if(popper.hasClass('pop-fixed') && offsetTop < 0 && scrollDivHeight > containetDetails.bottom){
									$('.activeElement').removeClass('activeElement');
									popper.addClass('hidden');
								}else if(popper.hasClass('pop-fixed') && offsetTop > 0 && scrollDivHeight > containetDetails.top){
									$('.activeElement').removeClass('activeElement');
									popper.addClass('hidden');
								}
							}
						})
					}, 500);
				}
				
				// to display all the elements which are not configured in the  pop-up
				// Done specifically to display left elements of reference while validating - JAI 11-JAN-2019
				if (popper.attr('data-display-elements') == "all"){
					var allChilds = $($(containerElement)[0]).find('> *');
					var editableElements = popper.attr('data-editable-elements');
					for (var a = 0, al = allChilds.length; a < al; a++){
						var currChild = allChilds[a];
						var eleClass = $(currChild).attr('class');
						if (popper.find('[data-class="' + eleClass + '"][data-type]:not([data-extra-element])').length == 0){
							if (a == 0){
								// don't add the first element, assuming the first element will be Sl. No. or Author (which will be configured)
							}else{
								var prevChild = allChilds[a-1];
								var prevChildClass = $(prevChild).attr('class');
								if (popper.find('[data-class="' + prevChildClass + '"][data-type]:visible').length > 0){
									var prevRow = popper.find('[data-class="' + prevChildClass + '"][data-type]:visible').closest('.row')
									var currRow = $('<div data-clone="true" class="row input-field" row-index="5"><div class="col s2"><label data-label="' + eleClass + '" contenteditable="false">' + eleClass.replace(/^Ref/, '') + '</label></div><div class="col s10"><p data-extra-element="true" data-sethtml="false" data-type="htmlComponent" type="text" data-class="' + eleClass + '">' + $(currChild).html() + '</p></div></div>');
									var re = new RegExp(' ' + eleClass + ' ');
									if (re.test(editableElements)){
										$(currRow).find('[data-type="htmlComponent"]').attr('contenteditable', "true").attr('class', "text-line");
										$(currRow).find('[data-type="htmlComponent"]').attr('data-selector', ".//*[@class='" + eleClass + "']");
										$(currRow).find('[data-type="htmlComponent"]').attr('data-node-xpath', kriya.getElementXPath(currChild));
									}else{
										$(currRow).find('[data-type="htmlComponent"]').attr('contenteditable', "false");
									}
									if (currChild.previousSibling && currChild.previousSibling.nodeType == 3){
										currRow.attr('data-previous-text', currChild.previousSibling.textContent);
									}
									prevRow.after(currRow);
								}
							}
						}
					}
				}
				
				popper.find('[data-wrapper]').scrollTop(0)
				if (popper.find('.material-icons.arrow-denoter').length > 0){
					popper.find('.material-icons.arrow-denoter').width(popper.find('.material-icons.arrow-denoter').css('font-size'))
					var acp = kriya.pageXaxis - cp - (popper.find('.material-icons.arrow-denoter').width()/2);
					popper.find('.material-icons.arrow-denoter').css('left', acp);
				}
				if (popper.find('*[add-focus]').length > 0){
					popper.find('*[add-focus]:first').focus();
				}
				if (popper.find('input[id*="tab"]').length > 0){
					popper.find('input[id*="tab"]').first().trigger('click');
				}
				setTimeout(function(){
					if (popper.find('.carousel.flip-card').length > 0){
						$('.carousel.flip-card').carousel({full_width: true, no_wrap: true});
					}
					if (popper.find('ul.tabs').length > 0){
						 popper.find('ul.tabs').tabs('select_tab', 'det_tab');
					}
					if (popper.find('ul.dropdown-link-content').length > 0){
						popper.find('ul.dropdown-link-content').addClass('hide');
					}
					//popper.find('*[contenteditable],input,textarea').first().focus()
					// for the elements that have the 'data-selector' get their current values in the attribute 'data-focusin' to compare later
					$(popper).find('[data-selector]').each(function(){
						focusedNode = $(this);
						var typeValue = $(focusedNode).attr('type');
						switch (typeValue){
							case 'text':
								var clonedNode = $(focusedNode).clone(true).excludeRemoveNode().cleanTrackChanges();
								clonedNode.find('.deleteLink').remove();
								$(focusedNode).attr('data-focusin-data', clonedNode.text());
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'checkbox':
								$(focusedNode).attr('data-focusin-data', $(focusedNode).is(':checked'));
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'radio':
								$(focusedNode).attr('data-focusin-data', $(focusedNode).is(':checked'));
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'file':
								$(focusedNode).attr('data-focusin-data', $(focusedNode).val());
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'number':
								$(focusedNode).attr('data-focusin-data', $(focusedNode).val());
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'inputTags':
								var tags = $(focusedNode).find('.kriya-chips .tags');
								var focusData = '';
								tags.each(function(){
									focusData += $(this).text() + ', ';
								});
								focusData = focusData.replace(/(^[\s]+|[\,\s]+$)/g, '');
								$(focusedNode).attr('data-focusin-data', focusData);
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'linkedObjects':
								var objects = $(focusedNode).find('> [data-rid]:not([data-template="true"])');
								var focusData = '';
								objects.each(function(){
									focusData += $(this).attr('data-rid') + ',';
								});
								focusData = focusData.replace(/([\,\s]+$)/g, '');
								$(focusedNode).attr('data-focusin-data', focusData);
								$(focusedNode).removeAttr('data-focusout-data');
								break;	
							case 'objFile':
								var filePath = '';
								var nodeXpath = $(focusedNode).attr('data-node-xpath');
								var dataNode = kriya.xpath(nodeXpath);
								if($(focusedNode)[0].nodeName == "IMG" && $(dataNode).attr('src')){
									filePath = $(dataNode).attr('src');
								}else if($(dataNode).attr('href')){
									filePath = $(dataNode).attr('href');
									filePath = filePath + '|' + $(dataNode).text();
								}

								$(focusedNode).attr('data-focusin-data', filePath);
								$(focusedNode).removeAttr('data-focusout-data');
								break;
							case 'table':
								if($(focusedNode).html() != ""){
									$(focusedNode).attr('data-focusin-data', 'true');	
								}
								break;
							default:
								break;
						}

					})
				}, 500);
			}
		},
		
		cleanComponents: function(component){
			$(component).find('[data-type="htmlComponent"]').each(function(){
				if ($(this).find('[data-type="htmlComponent"]').length == 0){
					if(this.nodeName == "INPUT" || this.nodeName == "SELECT" || this.nodeName == "TEXTAREA"){
						$(this).val('');
					}else{
						$(this).html('');
					}
				}
				if($(this).attr('data-freez-text')){
					var freezText  = $(this).attr('data-freez-text');
					$(this).html('<span class="freezText">' + freezText + '</span><span contenteditable="true"></span>');
				}
			});
			$(component).find('*[data-input],*[contenteditable="true"]').html('');
			$(component).find('*[data-for-edit]').removeAttr('data-for-edit');
			$(component).find('*[data-node-xpath]').removeAttr('data-node-xpath');
			$(component).find('.collection[data-selector]').children('*:not("[data-template],.collection-header")').remove();
		},
		
		getSlipPopUp: function(popUp, component){
			this.cleanComponents(popUp);
			$(popUp).addClass('manualSave');
			this.openPopUps(popUp, component);
		},
		
		/**
		*# Helps to open a popUp window with an animation effect
		*# params: popUp, which has to be opened
		*# params: component, from where the details has to refer
		**/
		openPopUps: function(popUp, component){
			//if more than one popup is open then update the z-index
			var curentIndex = $('#compDivContent [data-type="popUp"]:visible:last').css('z-index');
			curentIndex = (curentIndex)?(parseInt(curentIndex)+1):'';
			kriya.popupSelection = false;
			$(popUp).find('.com-save').removeClass('disabled');

			$(popUp).find('[data-validate-for]').each(function(){
				var validateFor      = $(this).attr('data-validate-for');
				var validateSelector = validateFor.split('==');
				var validateNode     = kriya.xpath(validateSelector[0], $(component)[0]);
				if(validateNode.length > 0){
					$(this).attr('data-validate', validateSelector[1]);
				}else{
					$(this).removeAttr('data-validate');
				}
			});
			
			$(popUp).removeAttr('style').removeClass('hidden').css({
				'top':'30%',
				'left':'40%',
				'width':'0',
				'height':'0',
				'opacity':'1',
				'z-index' : curentIndex
			});
			$(popUp).animate({
				top: '0',
				left:'0',
				width:'100%',//($(component).outerWidth()),
				height:'100%',//($(component).outerHeight()),
				opacity:('1')
			},500, function(){
				if ($(popUp).find('[data-wrapper]').length > 0){
					 //$(popUp).find('[data-wrapper]').css('height', $(popUp).find('[data-input-editable]').height());
					 /*if (typeof(popUpTracker) != "undefined"){
						 for (var key in popUpTracker){
							 popUpTracker[key].stopTracking();
						 }
					 }*/
					popUpTracker = {};
					/*$(popUp).find('[data-wrapper]').find('p[contenteditable]').each(function(i){
						popUpTracker[i] = new ice.InlineChangeEditor({
						   element: $(this).parent()[0],
						   handleEvents: true,
						   currentUser: { id: kriya.config.content.role + ' ' + kriyaEditor.user.id, name: kriyaEditor.user.name },
						   plugins: [
							 'IceAddTitlePlugin',
							 'IceEmdashPlugin',
							 {
							   name: 'IceCopyPastePlugin',
							   settings: {
								 pasteType : 'formattedClean',
								 preserve: 'strong'
							   }
							 }
						   ]
						}).startTracking();
						$(this).parent().removeAttr('contenteditable');
					});*/
					$(popUp).find('label').attr('contenteditable', 'false');
				}
			});
			//http://forum.webflow.com/t/how-to-prevent-scroll-behind-pop-up/7934/5
			//$(kriya.config.containerElm).css('overflow-y', 'hidden');
		},
		
		openSubPopUps: function(popUp, component){
			kriya.popupSelection = false;
			var topCss = $(component).offset().top - popUp.outerHeight() - 10;//(this.container.height()*2);
			if (popUp.hasClass('autoWidth')){
				topCss = kriya.pageYaxis - popUp.outerHeight() - 20;//(this.container.height()*2);
			}
			if (popUp.hasClass('hoverInline')){
				topCss = kriya.pageYaxis - popUp.outerHeight() - 20;//(this.container.height()*2);
			}
			$(popUp).removeClass('hidden').css({
				'top':topCss,
				'left':$(component).offset().left,
			})
		},
		
		/**
		*# Helps to close a popUp window with an animation effect
		**/
		closePopUps: function(popUp){
			kriya.popupSelection = false;
			popUp.animate({
				top: ('40%'),
				left:('40%'),
				width:('0'),
				height:('0'),
				opacity:('0')
			},500, function(){
				if (popUp.find('ul.tabs').length > 0){
					 popUp.find('ul.tabs').tabs('select_tab', '#det_tab');
				}
				popUp.addClass('hidden');
				popUp.css('z-index', '');
			});
			var compoXpath = popUp.attr('data-component-xpath');
			$('[data-type="popUp"][data-component="' + compoXpath + '"] .saveComponent.com-save').removeClass('disabled');
			popUp.removeAttr('data-component-xpath').removeAttr('data-component-type');
			popUp.removeAttr('data-node-xpath').find('*[data-node-xpath]').removeAttr('data-node-xpath');
			$(kriya.config.containerElm).css('overflow-y', 'auto');
			$('.kriya-tags-suggestions').addClass("hidden");
		},
		
		/**
		*# Function which will generate the html to the element
		*# params: htmlElement, xpath in it will get the element from where the html has to be picked
		*# if the xpath return multiple element, then this function duplicated the current htmlElements
		*# and populated the data with also adding an unique attribute to it
		**/
		htmlComponent: function(htmlElement, containerElement){
			if (typeof(containerElement) == "undefined") containerElement = this.container;
			var popper = $(htmlElement).closest('[data-component]');
			if (htmlElement.getAttribute('data-selector') != undefined || htmlElement.getAttribute('data-jq-selector') != undefined){

				if(htmlElement.getAttribute('data-jq-selector') != undefined){
					var selector = htmlElement.getAttribute('data-jq-selector');
					selector = $(containerElement).find(selector);
				}else{
					var selector = kriya.validateXpath($(htmlElement), containerElement);
					selector = kriya.xpath(selector, containerElement[0]);
				}			
				// to handle deceased author
				if((selector.length==0) && ($(htmlElement).attr('data-save-type') == "setDeceasedAuthor")){
					selector = kriya.xpath(htmlElement.getAttribute('data-selector'));
				}
				
				
				var compId = $(htmlElement).attr('id');
				var labelNode = $('label[for="' + compId + '"]');
				if(labelNode.length == 0){
					labelNode = ( $(htmlElement).next('label').length > 0 )?$(htmlElement).next('label'):( $(htmlElement).prev('label').length > 0 )?$(htmlElement).prev('label'):'';
				}

				/* For Pulling Authors from Reference, where it may contain multiple Author nodes */
				for (var i = 0, dl = selector.length; i < dl; i++) {
					var htmlNode = $(selector[i]);
					var nodeClass = htmlNode.attr('class');

					if(htmlNode.attr('id') &&  popper.find('.historyTab .historyCard[data-h-id="' + htmlNode.attr('id') + '"]').length > 0 && $(labelNode).length > 0){
						labelNode.append('<i class="material-icons fieldChange" data-message="{\'click\':{\'funcToCall\': \'filterHistoryChange\',\'channel\':\'components\',\'topic\':\'PopUp\'}}">history</i>');
					}

					if (i == 0 && htmlElement.getAttribute('data-template') == undefined){
						var newNode = htmlElement;
					}else{
						var newNode = htmlElement.cloneNode(true);
						if(htmlElement.getAttribute('data-class-tmp') == "true"){
							var tmpNode = $(htmlElement).parent().find('[data-class="' + nodeClass + '"][data-template]').clone(true);;
							newNode = tmpNode[0];
						}

						newNode.removeAttribute('data-template');
						newNode.removeAttribute('data-validate');
						newNode.setAttribute('data-clone', 'true');
						//for adding special attributes from node (specifically done for author roles) - jai 02-02-2018
						if (htmlElement.hasAttribute('data-set-attribute')){
							var attr = htmlElement.getAttribute('data-set-attribute');
							if ($(htmlNode)[0].hasAttribute(attr)){
								newNode.setAttribute(attr, $(htmlNode)[0].getAttribute(attr));
							}
						}
						//htmlElement.parentNode.insertBefore(newNode, htmlElement);
						htmlElement.parentNode.appendChild(newNode);
					}
					if ($(newNode).find('[data-selector]').length > 0){
						$(newNode).find('[data-selector]').each(function(){
							kriya.popUp.htmlComponent($(this)[0], htmlNode);
						});
						var nodeXpath = kriya.getElementXPath(htmlNode[0]);
						if(containerElement.attr('data-for-edit') != undefined){
							var nodeXpath = kriya.getElementXPath(containerElement[0])+kriya.getElementXPath(htmlNode[0]);
							newNode.setAttribute('data-for-edit', 'true');
						}
						if (! (/^\/\//).test(nodeXpath)) nodeXpath = "/"+nodeXpath;
						newNode.setAttribute('data-node-xpath', nodeXpath);
					}else{
						var htmlNodes = kriya.general.getHTML(htmlNode);
						if ($(htmlElement)[0].nodeName == "INPUT" || $(htmlElement)[0].nodeName == "SELECT" || $(htmlElement)[0].nodeName == "TEXTAREA"){
							$(newNode).val(htmlNodes[0]);
						}else{
							$(newNode).html(htmlNodes[0]);
						}

						if ($(htmlElement)[0].nodeName == "TEXTAREA"){
							$(htmlElement).css('height', '22px');
							$(htmlElement).css('height', ($(htmlElement).outerHeight() + 5));
						}
						if ($(newNode).parent().find('label').length > 0 && $(newNode).val() != ""){
							$(newNode).parent().find('label').addClass('active');
						}
						var nodeXpath = kriya.getElementXPath(htmlNode[0]);
						if(containerElement.attr('data-for-edit') != undefined){
							//var nodeXpath = kriya.getElementXPath(containerElement[0])+kriya.getElementXPath(htmlNode[0]);
							if(htmlNode[0].getAttribute('id')!=undefined)
							var nodeXpath = kriya.getElementXPath(containerElement[0])+kriya.getElementXPath(htmlNode[0]);
							newNode.setAttribute('data-for-edit', 'true');
						}
						
						if (! (/^\/\//).test(nodeXpath)) nodeXpath = "/"+nodeXpath;
						newNode.setAttribute('data-node-xpath', nodeXpath);
						//for adding delete link button (specifically done for author roles) - jai 02-02-2018
						if ($(htmlElement).attr('data-append-button') == 'deleteLink'){
							$(newNode).append($('<i class="material-icons deleteLink" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'deleteConfirmation\'}">remove_circle_outline</i>'));
						}
					}
					//newNode.parentNode.insertBefore(htmlElement, newNode)
				}
				if($(htmlElement).text() == "" && $(htmlElement).attr('data-add-label')){
					var className = $(htmlElement).attr('data-class');
					var templateNode = $('[data-element-template="true"] [tmp-class="' + className + '"]');
					if(templateNode.length > 0 && templateNode.attr('tmp-data-id')){
						var labelId = templateNode.attr('tmp-data-id');
						if(popper.find('#float-list-dropdown').length > 0){
							var prefixLabelID = popper.find('#float-list-dropdown li.selected').attr('data-float-id');
						}
						if(popper.find('#supplement-type-dropdown, #box-type-dropdown').length > 0){
							var dropdown = popper.find('#supplement-type-dropdown, #box-type-dropdown');
							var dropdownID = dropdown.attr('id');
							var selectedTypeNode = dropdown.find('li.selected');
							labelId = selectedTypeNode.attr('data-id-prefix');
							popper.find('.dropdown-button[data-activates="' + dropdownID + '"]').html(selectedTypeNode.text());
							labelId = (prefixLabelID) ? prefixLabelID + '-' + labelId : labelId;
						}

						labelId = labelId.replace(/\$position/g, '');
						labelId = labelId.replace(/BLK_/g, '');
						var labelPrefix = labelId;
						var uncitedCount = $(kriya.config.containerElm + ' .' + className + '[data-id^="' + labelId + '"]').closest('[data-id^="BLK_"][data-uncited]').length + 1;
						//commented by kirankumar:-If I delete tables and then add new ones,it doesn't update the 'tables' section/menu on the right
						//start
						//var lastElements = $(kriya.config.containerElm + ' .'+className+'[data-id^="' + labelId + '"]').closest('[data-id^="BLK_"]').last();
						var chapNum = $('#contentDivNode .jrnlChapNumber').text();
						var citeConfig = citeJS.floats.getConfig(labelId);
						if(chapNum && citeConfig && citeConfig['chapter-as-prefix']){
							labelId = labelId + chapNum.replace(/([a-zA-Z0-9])/g,'$1') + '_';
						}

						var elements = $(kriya.config.containerElm + ' .'+className+'[data-id^="' + labelId + '"]').closest('[data-id^="BLK_"]:not([data-inline="true"])');
						if(elements.length > 0){
							//var lastElementId = lastElements.attr('data-id');
							//if(lastElementId && lastElementId.match(/(\d+)$/g)){
								//var lastIdNum = parseInt(lastElementId.match(/(\d+)$/g)[0]);
								//labelId = labelId + (lastIdNum + 1);
							//}
							labelId = labelId + (elements.length + 1);
							//End
						}else{
							labelId = labelId + 1;	
						}
						
						//var newFloatLabelString = citeJS.floats.getCitationHTML([labelId], [], 'renumberFloats');
						//to display the label as uncited 'figure/table' - jai 06-09-2017
						if(labelId){
							var newFloatLabelString = citeJS.floats.getCitationHTML([labelId], [], 'renumberFloats');
							uncitedCount = (citeJS.settings[labelPrefix].skipNumber)?'':uncitedCount;						
							newFloatLabelString =  newFloatLabelString.replace(/(\d+)$/g, uncitedCount);
	
							var uncitedStr = 'Uncited ';
							if(citeConfig && citeConfig.citationNotNeed == "true"){
								uncitedStr = '';
							}
							newFloatLabelString = uncitedStr + newFloatLabelString;
							$(htmlElement).append('<span class="label" data-block-id="' + labelId + '">' + newFloatLabelString + '</span>');							
							//labelNode.html(newFloatLabelString);
							//labelNode.attr('data-block-id', labelId);
						}
						
						/*var numberPrefix = (citeJS.settings[labelPrefix] && citeJS.settings[labelPrefix].numberPrefix)?citeJS.settings[labelPrefix].numberPrefix:'';						
						var newFloatLabelString = uncitedStr + citeJS.settings[labelPrefix].singularString + ' ' + numberPrefix ;
						if(chapNum && citeJS.settings[labelPrefix] && citeJS.settings[labelPrefix]['chapter-as-prefix']){
							newFloatLabelString = newFloatLabelString + chapNum + ((citeJS.settings[labelPrefix].chapNumSuffix)?citeJS.settings[labelPrefix].chapNumSuffix : '');
						}
						newFloatLabelString = newFloatLabelString + uncitedCount;
						$(htmlElement).append('<span class="label" data-block-id="' + labelId + '">' + newFloatLabelString + '</span>');*/
					}
				}
				//Freez the some content in html element EX: ORCID link
				if($(htmlElement).attr('data-freez-text')){
					if($(selector).length == 0){
						$(htmlElement).html('');
					}
					var freezText  = $(htmlElement).attr('data-freez-text');
					var newNodeVal = $(htmlElement).html();
					var re = new RegExp(freezText, 'g');
					newNodeVal = newNodeVal.replace(re, '');
					$(htmlElement).html('<span class="freezText">' + freezText + '</span><span contenteditable="true">' + newNodeVal + '</span>');
				}
				if($(htmlElement).attr('data-set-link')){
					$(htmlElement).attr('href', $(htmlElement).html());
				}
			}
		},
		htmlComponents: function(htmlElement){
			$(htmlElement).html($(this.container).find('.' + htmlElement.getAttribute('data-class')).html());
			if ($(htmlElement)[0].nodeName == "INPUT"){
				$(htmlElement).val($(this.container).find('.' + htmlElement.getAttribute('data-class')).html());
				if ($(htmlElement).parent().find('label').length > 0 && $(htmlElement).val() != ""){
					$(htmlElement).parent().find('label').addClass('active');
				}
			}
		},
		/**
		 * Function to set the float componenet like img and table
		 */
		floatComponent: function(htmlElement, containerElement){
			if (typeof(containerElement) == "undefined") containerElement = this.container;
			if (htmlElement.getAttribute('data-selector') != undefined){
				var popper = $(htmlElement).closest('[data-component]');
				var selector = kriya.validateXpath($(htmlElement), containerElement);
				var selector = kriya.xpath(selector, containerElement[0]);
				if (selector == 0 && containerElement[0].nodeName == "IMG"){
					selector = kriya.xpath('.', containerElement[0]);
				}
				if($(selector).length > 0){
					var nodeXpath = kriya.getElementXPath($(selector)[0]);
					$(htmlElement).attr('data-node-xpath', nodeXpath);
					//commented by kirankumar:-unwanted attributes there in figure repalace popup
					/*$.each($(selector)[0].attributes,function(i,attr){
						var attrVal = $(selector).attr(attr.nodeName);
						if(attr.nodeName != "id" && attr.nodeName != "class" && attr.nodeName != "src"){
							$(htmlElement).attr(attr.nodeName, attrVal);	
						}
					});*/
					$(htmlElement).html($(selector)[0].innerHTML);

					if($(htmlElement)[0].nodeName == "TABLE"){
						popper.find('table[data-type="floatComponent"]').addClass('hidden');
						popper.find('.pasteTableDiv').removeClass('hidden');
						popper.find('.pasteBtn').addClass('disabled');
					}

				}else{
					$(htmlElement).text('');
					if($(htmlElement)[0].nodeName == "TABLE"){
						popper.find('.pasteTableDiv').removeClass('hidden');
						popper.find('.pasteBtn').addClass('disabled');
					}
				}

				//Remove the image attributes when open popup
				$(htmlElement).removeAttr('src');
				$(htmlElement).removeAttr('alt');
				$(htmlElement).removeAttr('href');
				$(htmlElement).removeAttr('data-primary-extension');
				
				if($(htmlElement).closest('#supp_uploader').length > 0){
					var blockNode = $(selector).closest('[class^="jrnl"][class$="Block"]');
					if(blockNode.length > 0){
						if($(selector).closest('.jrnlFigBlock').length > 0 && $(htmlElement)[0].nodeName == "IMG"){
							$(htmlElement).removeClass('hidden');
							$(htmlElement).closest('#supp_uploader').find('[data-class="jrnlSupplSrc"]').addClass('hidden');
						}else if($(selector).closest('.jrnlSupplBlock').length > 0 && $(htmlElement).attr('data-class') == "jrnlSupplSrc"){
							$(htmlElement).removeClass('hidden');
							$(htmlElement).closest('#supp_uploader').find('[data-class="jrnlFigure"]').addClass('hidden');
						}
						if(blockNode.attr('data-id')){
							var blockId = blockNode.attr('data-id');
							if(/\-/.test(blockId)){
								var suppID = blockId.split(/\-/);
								suppID = suppID[suppID.length-1];
							}else{
								var suppID = blockId.replace(/BLK_/,'');
							}
							var supplPrefix = suppID.replace(/\d+$/g, '');
							$('#supplement-type-dropdown li').removeClass('selected');
							var selectedType = $('#supplement-type-dropdown li[data-id-prefix="BLK_' + supplPrefix + '"]');
							selectedType.addClass('selected');
							$('[data-activates="supplement-type-dropdown"]').text(selectedType.text());
						}
					}
				}

				//Reset the button status and paste div when popup open
				if($(htmlElement)[0].nodeName == "TABLE"){
					/*popper.find('table[data-type="floatComponent"]').removeClass('hidden');
					popper.find('.pasteTableDiv').addClass('hidden');
					popper.find('.resetTableBtn').addClass('disabled');
					popper.find('.pasteBtn').removeClass('disabled');*/
				}

			}
		},
		floatOfList: function(htmlElement, containerElement){
			$(htmlElement).find('option[value!=""]').remove();
			if($(kriya.config.containerElm).find('.jrnlAppBlock, .sub-article[data-article-type="reply"]').length > 0){
				$(kriya.config.containerElm).find('.jrnlAppBlock, .sub-article[data-article-type="reply"]').each(function(){
					var dataId = $(this).attr('data-id');
					if(dataId){
						var labelString = kriya.general.getNewCitation([dataId], '', '', false);
						if(labelString){
							labelString = $(labelString).text();
							$(htmlElement).append('<option value="' + dataId + '">' + labelString + '</option>');
						}
					}
				});
			}
		},
		floatList: function(htmlElement, containerElement){
			var list = $(htmlElement).find('#float-list-dropdown');
			$(htmlElement).find('[data-activates="float-list-dropdown"]').html('None');
			list.html('<li class="selected" data-block="jrnlSupplBlock">None</li>');
			$(kriya.config.containerElm+' [class*="Block"]').each(function(){
				var label = $(this).find('.label:first').clone(true);
				label = label.cleanTrackChanges().text();
				label = label.replace(/[\|\,\.\s]+$/,'');
				var blockClass = $(this).attr('class');
				blockClass = blockClass.split(' ')[0];

				if(label){
					var liChild = $('<li>' + label + '</li>')
					if(blockClass){
						liChild.attr('data-block', blockClass);
					}
					liChild.attr('data-float-id', $(this).attr('data-id'));
					list.append(liChild);
				}
			});
		},
		// to populate author bio in popup / aravind 6/7/18
		AuthorBioImage: function(htmlElement, containerElement){
			$(htmlElement).find('img').remove();
			if (typeof(containerElement) == "undefined") containerElement = this.container;		
			var imagePath = $($(containerElement).find("img")).clone(true);			
			$(htmlElement).html(imagePath);
		},
		showEmptyPopUp : function(component,ele){

			kriya.popUp.getSlipPopUp(component[0], kriya.config.containerElm);
			
			var dataChilds = component.find('*[data-type]');
			$(component).find('*[data-clone]').remove();
			$(component).find('.historyTab .historyCard, .material-icons.fieldChange').remove();
			
			$(component).find('[data-focusout-data]').removeAttr('data-focusout-data');
			$(component).find('[data-focusin-data]').removeAttr('data-focusin-data');
			kriya.config.subPopUpCards = undefined;
			
			// iterating thru childs
			for (var i = 0, dl = dataChilds.length; i < dl; i++) {
				var dataChild = dataChilds[i];
				if (dataChild.parentElement && dataChild.parentElement.hasAttribute('data-type') == true && (dataChild.parentElement.getAttribute('data-type') == dataChild.getAttribute('data-type'))) continue;
				var eleType = dataChild.getAttribute('data-type');
				//checks for whether the method exists and then acts upon
				if (typeof(kriya[eleType]) != 'undefined'){
					kriya[eleType].init(ele, '', dataChild);
				}
			}
		}

    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));