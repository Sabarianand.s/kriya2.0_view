var usersArr = {};
var cardData = {};
var chartData = {};
var manuScriptCardData = {};
var supportData = {};
var projectLists = {};
var usersList = {};
var usersLists = {};
var userData = JSON.parse($('#userDetails').attr('data'));
var currentCustomers = [];
var key = 0;
var chartProject = [];
var selectedCustomer;
var usersDetailsDiv;
var assign;
var blkSuccessDoi, blkFailureDoi, lastCount, successCount, failureCount, openedArticles = 0;
var searchedDoiList = [];
var tableReportData = [];
/**
 * Added by Anuraja on 2018-08-01
 * Function to create Excel from Table data
 * Link: http://jsfiddle.net/cmewv/5623/
 */
var tableToExcel = (function () {
	var uri = 'data:application/vnd.ms-excel;base64,',
		template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
	base64 = function (s) {
			return window.btoa(unescape(encodeURIComponent(s)))
		},
		format = function (s, c) {
			return s.replace(/{(\w+)}/g, function (m, p) {
				return c[p];
			})
		}
	return function (table, name) {
		if (!table.nodeType)
			table = document.getElementById(table)

		var ctx = {
			worksheet: name || 'Worksheet',
			table: table.innerHTML
		}
		// window.location.href.download = 'ad.xls'
		var blob = new Blob([format(template, ctx)]);
		var a = window.document.createElement('a');
		a.href = window.URL.createObjectURL(blob);
		a.download = name + '.xls';
		// Append anchor to body.
		document.body.appendChild(a);
		a.click();
		// Remove anchor from body
		document.body.removeChild(a);
		return false;
	}

})()

/**
 * eventHandler - this javascript holds all the functions required for Reference handling
 *					so that the functionalities can be turned on and off by just calling the required functions
 *					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
 */
var filterData = {};
var eventHandler = function () {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['mainMenu', 'components', 'dropdowns', 'bulkedit', 'common', 'flatplan'];
	//add or override the initial setting
	var initialize = function (params) {
		function extend(obj, ext) {
			if (obj === undefined) {
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function () {
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function () {
			/**
			 * attach a click event listner to the body tag
			 * if the target node has the special attribute 'data-channel',
			 * then we need to publish some data using the data in the attributes
			 */
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function () {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function (event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);
			//Hide more action dropdowns
			$('.moreActionsBtns:not(.hidden)').addClass('hidden');

			$(elements).each(function () {
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length) {
					targetNode = $(targetNodes[0]);
					if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
						return;
					}
					var message = eval('(' + targetNode.attr('data-message') + ')');
					var eveDetails = message[event.type];
					if (!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type) {
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic = targetNode.attr('data-topic');
					} else if (typeof (eveDetails) == "string" && typeof (message[eveDetails]) == "object") {
						eveDetails = message[eveDetails];
					} else if (!eveDetails || typeof (eveDetails) == "string") {
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic: eveDetails.topic + '.' + event.type,
						data: {
							message: eveDetails,
							event: event.type,
							target: targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function () {
			var subscribers = eventHandler.settings.subscribers;
			for (var i = 0; i < subscribers.length; i++) {
				initSubscribe(subscribers[i], subscribers[i] + '_subscription');
			}

			function initSubscribe(subscribeChannel, subscribeName) {
				if (!eventHandler.settings.subscriptions[subscribeName]) {
					eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
						channel: subscribeChannel,
						topic: "*.*",
						callback: function (data, envelope) {
							//console.log(data, envelope);
							if (data.message != '') {
								/*http://stackoverflow.com/a/3473699/3545167*/
								if (typeof (data.message) == "object") {
									var array = data.message;
								} else {
									var array = eval('(' + data.message + ')');
								}
								var functionName = array.funcToCall;
								var param = array.param;
								var topic = envelope.topic.split('.');
								//var fn = 'eventHandler.menu.edit.'+functionName;
								if (typeof (window[functionName]) == "function") {
									window[functionName](param, data.target);
								} else if (typeof (window['eventHandler'][envelope.channel][topic[0]]) == "object") {
									if (typeof (window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function') {
										window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
									}
								} else {
									console.log(functionName + ' is not defined in ' + topic[0]);
								}
							} else {
								console.log('Message is empty');
							}

							// `data` is the data published by the publisher.
							// `envelope` is a wrapper around the data & contains
							// metadata about the message like the channel, topic,
							// timestamp and any other data which might have been
							// added by the sender.
						}
					});
				}
			}
		},
		remove: function () {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};

	return eventHandler;
})(eventHandler || {});

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.mainMenu = {
		toggle: {
			toggleContainer: function (param, targetNode) {
				if (targetNode != undefined) {
					$('.dashboardTabs').removeClass('active');
					$('.sidebar .nav-item .nav-link').removeClass('active');
					var contNode = $('#' + $(targetNode).attr('data-href'));
					// targetNode.addClass('active');
					contNode.addClass('active');
				}
			},
			// To display the Reports Page in the dashboard
			showReport: function (param, targetNode) {
				if (!$(targetNode).hasClass('active')) {
					$('#search').hide();
					$('.showEmpty').addClass('hidden');
					if (!$('#reportContent table').length) {
						$('.showEmpty').removeClass('hidden');
						$('.showEmpty .watermark-text').html('Please select customer <br/>to display Report');
					}
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
					var customerName = '';
					var cardCustomer = $('#manuscriptContent #customerVal').text();
					var chartCustomer = $('#chartSelection #customerVal').text();
					if (!cardCustomer.match(/Customer|customer/g)) {
						customerName = cardCustomer;
					} else if (!chartCustomer.match(/Customer|customer/g)) {
						customerName = chartCustomer;
					}
					eventHandler.dropdowns.project.reportCustomerProject(customerName);
				}
			},
			showDashboard: function (param, targetNode) {
				$('#search').hide();
				$('.showEmpty').removeClass('hidden');
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display charts');
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				$('#manuscriptContent #filterCustomer').find('.active')
				var cardCustCount = $('#manuscriptContent #filterCustomer').find('.active').length;
				var reportCustCount = $('#reportContent #reportCustomer').find('.active').length;
				var cardCust = $('#manuscriptContent #customerVal').text();
				var chartCust = $('#chartSelection #customerVal').text();
				var reportCust = $('#reportSelection #reportCustomerVal').text();
				var customer = '';
				if (!cardCust.match(/Customer|customer/g)) {
					customer = cardCust;
					// $('#chartSelection .showBreadcrumb').css('display','inline-block');
				} else if (!reportCust.match(/Customer|customer/g)) {
					customer = reportCust;
					// $('#chartSelection .showBreadcrumb').css('display','inline-block');
				}
				if (cardCustCount > 0 || reportCustCount > 0) {
					if (chartCust != customer) {
						$('#customerVal').text(customer);
						$('#chartSelection .showBreadcrumb').css('display', 'inline-block');
						$('#chartCustomer [value="' + customer + '"]').addClass('active');
						$('#chartCustomer [value="' + customer + '"]').siblings().removeClass('active');
						$('#chartSelection .showBreadcrumb').css('display', 'inline-block;');
						eventHandler.dropdowns.project.chartProject(customer);
						$('#chartSelection #projectVal').text('Project');
						$('#chartProject').children().addClass('active');
						eventHandler.dropdowns.project.updateChart({
							'onsuccess': 'makeCharts'
						}, 'showManuscript');
					}
				}
				$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .newStatsRow').offset().top + 50) - $('#footerContainer').height()) + 'px');
				$('.la-container').css('display', 'none');
				// eventHandler.components.actionitems.initializeAddJob(param);
			},
			showManuscript: function (param, targetNode, from) {
				$('#searchBox').val('');
				$('.showEmpty').removeClass('hidden');
				$('.showEmpty .watermark-text').html('Please select customer <br/>to display cards');
				$('#manuscriptContent > .layoutView').removeClass('hidden');
				$('.ulheight').height(window.innerHeight - $('.ulheight').position().top - $('footer').height())
				$('#manuscriptsData').removeData();
				$('.progress-container').html('');
				eventHandler.components.actionitems.hideRightPanel();
				if(param) $('#manuscriptContent #customerVal').text(param.customer);
				var displayBookFlow = false;
				if ($('#filterCustomer .filter-list').length == 0 && $("#chartCustomer").children('.filter-list').length == 1){
					var customerNode = $("#chartCustomer").children('.filter-list');
					if (customerNode.attr('data-type') == 'book'){
						displayBookFlow = true;
					}
				}
				if (($(targetNode) && ($(targetNode).attr('data-type') == 'book' || $('#manuscriptContent #filterCustomer [value="'+ $('#manuscriptContent #customerVal').text() +'"]').attr('data-type') == 'book')) || displayBookFlow) {
					$('.la-container').css('display', '');
					if ($(targetNode).attr('data-type') == undefined && $('#manuscriptContent #filterCustomer [value="'+ $('#manuscriptContent #customerVal').text() +'"]').attr('data-type') == 'book') {
						var param = {
							'workflowStatus': 'in-progress',
							'urlToPost': 'getAssigned',
							'customer': $('#manuscriptContent #customerVal').text(),
							'custTag': '#filterCustomer',
							'prjTag': '#filterProject',
							'user': 'user',
							'optionforassigning': true,
							'version': 'v2.0'
						}
					}
					$('header .nav-items-journals').addClass('hidden');
					$('header .nav-items-books').removeClass('hidden');
					$('#manuscriptContent .showBreadcrumb').css('display', '');
					$('#manuscriptContent > .layoutView').addClass('hidden');
					$('.ulheight').height(window.innerHeight - $('.ulheight').position().top - $('footer').height())
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .newStatsRow').css('padding-bottom', '7px');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					$('#manuscriptContent .filterOptions').hide();
					$('#manuscriptContent #customerVal').text(param.customer);
					$('#filterCustomer').children().removeClass('active');
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					manuScriptCardData = [];
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
					eventHandler.flatplan.action.getBooks(param, targetNode);
					eventHandler.components.actionitems.initializeAddJob(param);
					$('.showEmpty').hide();
				}else if (param) {
					// if(param.customer && )
					// eventHandler.components.actionitems.getUserDetails(param.customer);
					$('header .nav-items-books').addClass('hidden');
					$('header .nav-items-journals').removeClass('hidden');
					var startTime = new Date().getTime();
					$('.la-container').css('display', '');
					$('.showBreadcrumb').css('display', 'inline-block');
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .newStatsRow').css('padding-bottom', '7px');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					$('#manuscriptContent .filterOptions').show();
					manuScriptCardData = [];
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					setTimeout(function () {
						eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
						$('.assignAccess').hide();
						if (param.optionforassigning) {
							if (!$(targetNode).hasClass('assignAccess')) {
								if (!targetNode) {
									$('#manuscriptContent #statusVal').text('WIP');
									$('#manuscriptContent #filterStatus').children().removeClass('active');
									$('#manuscriptContent #filterStatus').children().first().addClass('active');
								}
								$('#manuscriptContent #assignVal').text('Assigned to me');
								$('#manuscriptContent #assignVal').siblings().children().removeClass('active');
								$('#manuscriptContent #assignVal').siblings().children().first().addClass('active');
							}
							$('.assignAccess').show();
						}
						var contNode = $('#' + $(targetNode).attr('data-href'));
						eventHandler.dropdowns.article.showArticles(param, targetNode);
						eventHandler.components.actionitems.initializeAddJob(param);
						$('.showEmpty').hide();
					}, 0)
				} else {
					$('header .nav-items-books').addClass('hidden');
					$('header .nav-items-journals').removeClass('hidden');
					if (!$('#dashboardContent').hasClass('active') && targetNode.hasClass('active')) {
						$('.la-container').css('display', 'none');
						return;
					}
					$('.la-container').css('display', '');
					$(targetNode).parent().siblings().find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#search').show();
					$('#sort').show();
					$('#searchResult').hide();
					$('#searchBar').siblings().val('');
					$('#manuscriptContent .supportcard').show();
					$('#manuscriptContent .filterOptions').show();
					$('.dashboardTabs').removeClass('active');
					$('#manuscriptsDataContent').html('');
					$('div.navFilter').empty();
					$('.msCount.loaded').removeClass('loaded').addClass('loading');
					$('#msYtlCount, #msCount').text('0');
					setTimeout(function () {
						var contNode = $('#' + $(targetNode).attr('data-href'));
						targetNode.addClass('active');
						eventHandler.dropdowns.article.showArticles('', targetNode);
						eventHandler.components.actionitems.initializeAddJob({
							'customer': $('#filterCustomer > .filter-list.active').text()
						});
						$('#manuscriptContent').addClass('active');
						$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
						$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .layoutView').offset().top + 30) - $('#footerContainer').height()) + 'px');
					}, 0)

					//$('#sort .fa').remove();
					//$('#sort [sort-option="Doi"]')[0].addClass('active')
					//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
				}
				setTimeout(function () {
					// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
					var customer = $('#manuscriptContent #customerVal').text();
					if (!customer.match(/customer|selected/gi)) {
						var userRole = userData.kuser.kuroles[customer]['role-type'];
						var userAccess = userData.kuser.kuroles[customer]['access-level'];
						if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
							eventHandler.filters.populateSort();
						}
					}
				}, 0)
				//$('#sort .fa').remove();
				//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
			},
			// Shows support articles.
			showSupport: function (param, targetNode) {
				$('#searchBox').val('');
				eventHandler.components.actionitems.hideRightPanel();
				$('.showEmpty').addClass('hidden');
				if (!param) {
					return;
				}
				$('.la-container').css('display', '');
				$('#assignUser').show();
				$(targetNode).parent().siblings().find('.active').removeClass('active');
				$(targetNode).addClass('active');
				$('#search').show();
				$('#searchResult').hide();
				$('#sort').show();
				$('#searchBar').siblings().val('');
				$('#manuscriptContent .supportcard').hide();
				$('#manuscriptContent .filterOptions').show();
				$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
				$('#manuscriptsDataContent').html('');
				$('div.navFilter').empty();
				supportData = [];
				$('.msCount.loaded').removeClass('loaded').addClass('loading');
				$('#msYtlCount, #msCount').text('0');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode);
				eventHandler.dropdowns.article.showArticles(param, targetNode);
				eventHandler.components.actionitems.initializeAddJob(param);
				$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .layoutView').offset().top + 30) - $('#footerContainer').height()) + 'px');
				//$('#sort .fa').remove();
				//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
			},
			//To display the customer articles when refresh is clicked.
			showRefreshed: function (targetNode) {
				eventHandler.components.actionitems.hideRightPanel();
				if (!targetNode) {
					return;
				}
				if ($('#manuscriptContent #customerVal').text() == "Customer" || $('#manuscriptContent #customerVal').text() == "customer") {
					return;
				}
				$('.la-container').css('display', '');
				var param = {};
				if ($('.assignAccess').css('display') == 'none') {
					var assignId = '#statusVal';
				} else {
					var assignId = '#assignVal';
					$('#manuscriptContent .supportcard').show();
				}
				param = JSON.parse(JSON.stringify(refreshButtonConfig[$(targetNode.targetNode).siblings().find(assignId).text().trim()]))
				param.from = 0;
				param.customer = $('#manuscriptContent #customerVal').text();
				if (!(/^(All|Project)$/.test($('#manuscriptContent #projectVal').text()))) {
					param.refreshedProject = $('#manuscriptContent #projectVal').text();
				}

				$('#manuscriptContent .newStatsRow').css('padding-bottom', '10px');
				$('#manuscriptsDataContent').html('');
				$('div.navFilter').empty();
				manuScriptCardData = [];
				$('.msCount.loaded').removeClass('loaded').addClass('loading');
				$('#msYtlCount, #msCount').text('0');
				eventHandler.mainMenu.toggle.toggleContainer(param, targetNode.targetNode);
				eventHandler.dropdowns.article.showArticles(param, targetNode.targetNode);
				param.project = param.refreshedProject;
				eventHandler.filters.updateFilters(param);
				//$('#sort .fa').remove();
				//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');
			},
			showSearched: function (targetNode, action, event) {
				if (targetNode.value.length <= 3 && event && event.inputType && event.inputType.match(/deleteContentBackward|deleteByCut/g)) {
					$('#searchdoi').hide();
					return;
				}
				if (action == 'search' || (event.keyCode == 13 || event.which == 13)) {
					eventHandler.components.actionitems.hideRightPanel();
					if (!targetNode) {
						return;
					}
					if (!targetNode.value) {
						$(targetNode).closest('#search').siblings().find('.active').removeClass('active');
						$(targetNode).closest('#search').siblings().find('#cardView').addClass('active');
						eventHandler.mainMenu.toggle.showManuscript('', $(targetNode).parent().siblings().find('#cardView'));
						return;
					}
					eventHandler.dropdowns.article.getSearchedArticles({
						'doi': (targetNode.value).trim(),
						'customer': 'customer',
						'project': 'project',
						'urlToPost': 'getSearchedArticles'
					});
				} else if (action.match(/oninput/g)) {
					$('#searchdoi').html('');
					if (targetNode.value.length >= 4) {
						$('#searchdoi').show();
						$('#searchdoi').html('<a>Searching...</a>');
						var param = {
							'customer': 'customer',
							'project': '*',
							'searchedWord': targetNode.value,
							'urlToPost': 'getSearchedArticlesDropDown'
						}
						$.ajax({
							type: "POST",
							url: '/api/getarticlelist',
							data: param,
							dataType: 'json',
							success: function (respData) {
								searchedDoiList = [];
								if (respData) {
									var searchDoi = JSONPath({
										json: respData,
										path: '$..doi.buckets'
									})[0];
									for (var doi of searchDoi) {
										searchedDoiList.push(doi.key)
									}
									searchedDoiList.sort();
									addDoi();
								} else {
									addDoi();
								}
							}
						})
					}
				}
				function addDoi() {
					var string = '';
					var count = 0;
					searchedDoiList.filter(function (num) {
						if ((num).match(new RegExp(targetNode.value + '.*', 'g')) && count < 10) {
							string += '<a onclick="eventHandler.dropdowns.article.getSearchedArticles({\'doi\':\'' + num + '\',\'customer\': \'customer\',\'project\': \'project\',\'urlToPost\': \'getSearchedArticles\'});">' + num + '</a>'
							count++;
						}
					});
					(string == '') ? string = '<a>No matches found</a>' : string = string;
					$('#searchdoi').html(string);
					$('#searchdoi').show();
				}
			},
			toggleReportView: function (targetNode) {
      //To toggle display card view and table view in chart
				console.log($(targetNode).attr('toggleView'));
				var toggleView = 'chart';
				toggleView = $(targetNode).attr('toggleView');
				if(toggleView ==  'table'){
					$('#reportColHideShow').removeClass('hidden');
					$('#chartReportTable').parent().removeClass('hidden');
					$('#cardReportData').addClass('hidden');
					$('#filterExportExcel').removeClass('hidden');
					$(targetNode).attr('toggleView','card').removeClass('fa-table').addClass('fa-list');
					$(targetNode).text(' View Card');
				}else if(toggleView ==  'card'){
					$('#filterExportExcel').addClass('hidden');
					$('#reportColHideShow').addClass('hidden');
					$('#chartReportTable').parent().addClass('hidden');
					$('#cardReportData').removeClass('hidden');
					$(targetNode).attr('toggleView','table').removeClass('fa-list').addClass('fa-table');
					$(targetNode).text(' View Table');
				}
			}
		}
	}
	return eventHandler;
})(eventHandler || {});
eventHandler.common = {
		functions: {
			sendAPIRequest: function (funtionName, parameters, requestType, onSuccessFunc, onError, onProgress, blkUpdateCount, name) {
			if (!funtionName || !requestType) {
				console.log('Error finding Name');
				return;
			}
			$.ajax({
				url: '/api/' + funtionName,
				type: requestType,
				data: parameters,
				xhrFields: {
					onprogress: function (e) {
						if (onProgress) {
							onProgress(e);
						}
					}
				},
				error: function (res) {
					lastCount++;
					if (onError) {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkFailureDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'error';
								parameters.notify.notifyText = parameters.notify.errorText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
						onError(parameters);
					}
				},
				success: function (res) {
					lastCount++;
					parameters.response = res;
					if (typeof (res) == 'string' && res.match('<error>')) {
						if (onError) {
							if (parameters.notify && parameters.notify != undefined) {
								parameters.notify.notifyType = 'error';
								parameters.notify.notifyText = parameters.notify.errorText;
								// eventHandler.common.functions.displayNotification(parameters);
							}
							onError(parameters);
						}
					} else if (typeof (res) == 'object' && res.status != undefined) {
						if (res.status.code != '200') {
							if (onError) {
								if (parameters.notify && parameters.notify != undefined) {
									if (onError == 'conform') {
										blkFailureDoi.push(parameters.doi)
									} else {
										parameters.notify.notifyType = 'error';
										parameters.notify.notifyText = parameters.notify.errorText;
										eventHandler.common.functions.displayNotification(parameters);
									}
								}
								onError(parameters);
							}
						} else {
							if (parameters.notify && parameters.notify != undefined) {
								if (onError == 'conform') {
									blkSuccessDoi.push(parameters.doi)
								} else {
									parameters.notify.notifyType = 'success';
									parameters.notify.notifyText = parameters.notify.successText;
									eventHandler.common.functions.displayNotification(parameters);
								}
							}
							onSuccessFunc(parameters);
						}
					} else if (onSuccessFunc) {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkSuccessDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'success';
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
						onSuccessFunc(parameters);
					} else {
						if (parameters.notify && parameters.notify != undefined) {
							if (onError == 'conform') {
								blkSuccessDoi.push(parameters.doi)
							} else {
								parameters.notify.notifyType = 'success';
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
							}
						}
					}
					// For bulk updation to display the all articles in single notification.
					if (onError == 'conform' && blkUpdateCount == lastCount) {
						if (blkSuccessDoi.length) {
							parameters.notify.notifyType = 'success';
							parameters.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText.replace(/{doi}/g, blkSuccessDoi.join(', '));
							if(name == " "){
								parameters.notify.notifyText = parameters.notify.notifyText.replace(/assigned/g, 'unassigned');
							}
							eventHandler.common.functions.displayNotification(parameters);
						}
						if (blkFailureDoi.length) {
							parameters.notify.notifyType = 'error';
							parameters.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText.replace(/{doi}/g, blkFailureDoi.join(', '));
							eventHandler.common.functions.displayNotification(parameters);
						}
						$('.la-container').css('display', 'none');
					}
				},
			});
		},
			writeLog: function (funtionName, parameters, onSuccessFunc, onError, onProgress) {
				$.ajax({
					url: '/api/' + funtionName,
					type: 'POST',
					data: parameters,
					xhrFields: {
						onprogress: function (e) {
							if (onProgress) {
								onProgress(e);
							}
						}
					},
					error: function (res) {
						if (onError) {
							onError(res);
						}
					},
					success: function (res) {
						if (onSuccessFunc) {
							onSuccessFunc(res);
						}
					},
				});
			},
			displayNotification: function (param) {
			if (param.notify) {
				if (param.notify.confirmNotify) {
					new PNotify({
						title: param.notify.notifyTitle,
						text: param.notify.notifyText,
						type: param.notify.notifyType,
						functionToCall: forceLogOutFunction[param.notify.functionToCall],
						hide: false,
						parameters: param.parameter,
						icon: 'fa fa-question-circle',
						confirm: {
							confirm: true,
							buttons: [{
								text: 'Yes',
								click: function (param) {
									eval(param.options.functionToCall)(param.options.parameters);
									PNotify.removeAll();
								}
							},
							{
								text: 'No',
								click: function () {
									PNotify.removeAll();
								}
							}]
						}
					});
				} else {
					new PNotify({
						title: param.notify.notifyTitle,
						text: param.notify.notifyText,
						type: param.notify.notifyType,
						hide: false,
					});
				}
			} else {
				new PNotify({
					title: 'Error',
					text: 'Unable to process',
					type: 'error',
					hide: false
				});
			}
		},
			getUserData: function (customer) {
				usersArr = {};
				jQuery.ajax({
					type: "GET",
					url: "/api/getuserdata?customer=" + customer,
					success: function (msg) {
						var usersDiv = $(msg);
						var users = $('<select class="assign-users" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"/>');
						$(usersDiv).find('user').each(function (i, v) {
							users.append('<option value="' + $(this).find('first').text() + '">' + $(this).find('first').text() + '</option>');
						});
						usersArr.users = users;
					}
				})
			},
			getUserList: function () {
				//Added by Anuraja to get all users list
				jQuery.ajax({
					type: "GET",
					url: '/api/getuserlist',
					success: function (respData) {
						if (!respData || respData.length == 0) {
							$('.la-container').css('display', 'none');
							return;
						}
						var usersDiv = $(respData);
						$(usersDiv).find('user').each(function (i, v) {
							var userInfo = {};
							userInfo.name = $(this).find('first').text();
							$(this).find('role').each(function (i, v) {
								if (!usersLists[$(this).attr('customer-name')]) {
									usersLists[$(this).attr('customer-name')] = [];
								}
								userInfo.role = $(this).attr('role-type');
								usersLists[$(this).attr('customer-name')].push(userInfo);
							})
						});
						// console.log(usersList);
						$('.la-container').css('display', 'none');
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				});
			},
			pickDateNew: function (id, type, cardID, source, replaceCard, event) {
				$('.datepicker-inline').parent().hide();
				if (!source) {
					source = 'data-id';
				}
				if ($("[id='blkdate" + type + id + "']").length > 0) {
					$("[id='blkdate" + type + id + "']").attr('data-old-value', $("[id='blkdate" + type + id + "']").html());
				} else {
					$("[id='date" + type + id + "']").attr('data-old-value', $("[id='date" + type + id + "']").html());
				}
				var timeToSend = {
					time: '',
					date: ''
				}
				var timeToSet;

				var datePicker = $("[id='blkpickDate" + type + id + "']").datepicker({
					timepicker: true,
					todayButton: true,
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						today: 'Today',
						clear: 'Clear',
						dateFormat: 'dd.mm.yyyy',
						timeFormat: 'hh:ii',
						firstDay: 1
					},
					onShow: function (dp, animationCompleted) {
						if (!animationCompleted) {

						} else {

						}
					},
					onHide: function (dp, animationCompleted) {
						if (!animationCompleted) {

						} else {}
					},
					onSelect: function (formattedDate, date, inst) {
						timeToSend.date = formattedDate.slice(0, 10);
						timeToSend.time = formattedDate.slice(11, 16);
						timeToSet = date.toString().slice(4, 15);
					}
				})
				datePicker.show();
				if ($("[id='blkdate" + type + id + "']").length > 0) {
					var inputDate = new Date($("[id='blkdate" + type + id + "']").html());
				} else {
					var inputDate = new Date($("[id='date" + type + id + "']").html());
				}
				if (inputDate == 'Invalid Date') {
					var currentDate = new Date();
					datePicker.data('datepicker').selectDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()))
				} else {
					datePicker.data('datepicker').selectDate(inputDate)
				}
				$('.datepicker--button').text('CONFIRM');
				$('.datepicker--button').off('click');
				$('.datepicker--button').on('click', function () {
					datePicker.hide();
					if ($(this).closest('[data-on-success]').length > 0) {
						var onSuccess = $(this).closest('[data-on-success]').attr('data-on-success');
						if (typeof (window[onSuccess]) == "function") {
							if (datePicker.data('datepicker').selectedDates.length > 0) {
								window[onSuccess](datePicker.attr('id'), 'yes', cardID, source, replaceCard, '', id);
							} else {
								window[onSuccess](datePicker.attr('id'), 'delete', cardID, source, replaceCard, '', id);
							}
						} else if (typeof (eval(onSuccess)) == "function") {
							eval(onSuccess)(datePicker.attr('id'), 'yes', cardID, source, replaceCard, '', id);
						}
					}
				});
				event.preventDefault();
				event.stopPropagation();
			},
			makeChart: function (param) {
				var val = $(param.val).val();
				data = {};
				data.project = val;
				eventHandler.highChart[param.functionName](data);
				//Call Highchart to create chart
			},
			getOverDue: function (param) {}
		}
	},
	eventHandler.components = {
		actionitems: {
			undo: function (param, targetNode) {
				console.log(param)
			},
			moreActions: function (param, targetNode) {
				$(targetNode).parent().parent().find('.moreActionsBtns').toggleClass('hidden');
			},
			showAuthors: function (param, targetNode) {
				$('#authorList').modal();
				console.log(param)
			},
			toggleManuscriptLayout: function (param) {
				var data = {};
				data.filterObj = {};
				data.filterObj['customer'] = {};
				data.filterObj['project'] = {};
				data.filterObj['articleType'] = {};
				data.filterObj['typesetter'] = {};
				data.filterObj['publisher'] = {};
				data.filterObj['author'] = {};
				data.filterObj['copyeditor'] = {};
				data.filterObj['editor'] = {};
				data.filterObj['others'] = {};
				data.dconfig = dashboardConfig;
				data.info = cardData;
				data.disableFasttrack = disableFasttrack;
				data.count = 0;
				data.userDet = JSON.parse($('#userDetails[data]').attr('data'));
				manuscriptsData = $('#manuscriptsDataContent');
				var pagefn = doT.template(document.getElementById(param.type + 'Template').innerHTML, undefined, undefined);
				manuscriptsData.html(pagefn(data));
				$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
			},
			showNotes: function (param, targetNode) {
				var customer = 'elife',
					project = 'elife_1.0',
					doi = 'eLife.21738';
				var url = "/api/articlenode?doi=#DOI#&projectName=#PROJECT#&customerName=#CUSTOMER#&xpath=//notes";
				var modalDiv = $(".comments-modal");
				if (modalDiv.length === 0) {
					$("body").append('<div class="modal comments-modal" style="display:none;"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h3 class="holdHeader">Production Notes for <span class="doiText"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field maincontent"><div class="textarea-wrapper"><div class="textarea-content" data-placeholder="Add a comment" contenteditable="true" style="height: 3.65em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="postComments(this)"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="openFileUpload(this);" data-upload-type="Files" data-success="attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
					modalDiv = $(".comments-modal");
				}
				$(modalDiv).find('.data-container').find('#comment-list').remove()
				$(modalDiv).find('.data-container').append('<ul id="comment-list" class="main"/>');
				$('.modal.comments-modal .holdHeader .doiText').text(doi);
				$('.modal.comments-modal').attr('data-customer', customer).attr('data-project', project).attr('data-doiid', doi);
				var notesURL = url.replace("#DOI#", doi).replace("#CUSTOMER#", customer).replace("#PROJECT#", project);
				notesURL = notesURL + '&bucketName=AIP'; // + $('#bucketselect').val();
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (msg) {
						if (msg && !msg.error) {
							var commentObj = $(msg);
							$(commentObj).find('note').each(function () {
								var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + $(this).find('created').html() + '">' + $(this).find('created').html() + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
								commentDiv.find('*[data-class]').each(function () {
									$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
								});
								//it the current on is reply for some other, the add it next to it
								if ($(this).find('parent').length > 0 && $('li.comment[data-id="' + $(this).find('parent').html() + '"]').length > 0) {
									var parentID = $(this).find('parent').html();
									commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
									//add before next comment
									if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
									} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
										//add after last reply
										$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
									} else { //or aff next to parent
										$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
									}
								} else { //append to comment list
									$(modalDiv).find('#comment-list').append(commentDiv);
								}
							});
							modalDiv.modal();
						} else {
							modalDiv.modal();
						}
					},
					error: function (xhr, errorType, exception) {
						modalDiv.modal();
					}
				});
			},
			openPDF: function (param, targetNode) {
				console.log(param)
			},
			changeFileName: function(param, targetNode){
				if ($(targetNode).length > 0 && $(targetNode)[0].files && $(targetNode)[0].files.length > 0){
					var fileName = $(targetNode)[0].files[0].name;
					$(targetNode).parent().find('span').text(fileName);
				}
			},
			removeFileFromList: function(param, targetNode){
				/* Remove file option */
				var fileHolder = $(targetNode).closest('.file-holder');
				$(targetNode).closest('.row.file-list').remove();
				fileHolder.find('.row.file-list').last().find('.addFile').removeClass('disabled');
			},
			addNewFileList: function(param, targetNode){
				/* Add another file option */
				var fileHolder = $(targetNode).closest('.file-holder');
				var newFileList = $(targetNode).closest('.row.file-list').clone(true);
				newFileList.find('input')[0].value = "";
				newFileList.find('input').parent().find('span').text('Browse');
				newFileList.find('.removeFile').removeClass('disabled');
				newFileList.find('.is-invalid').removeClass('is-invalid');
				targetNode.addClass('disabled');
				fileHolder.find('.row.file-list').last().after(newFileList);
			},
			initializeAddJob: function (param) {
				if (!param || !param.customer) return false;
				$('#addJobBtn,#submitJobBtn,#addBookBtn,#exportEpubBtn').addClass('hidden');
				$('#submitJobBtn').addClass('hidden');
				//var customers = '^(' + addJobConfig.customer.join('|') + ')$';
				//var matchCustomer = new RegExp(customers, 'i');
				var customers = addJobConfig.customer[param.customer];
				var roleType = userData.kuser.kuroles[param.customer]['role-type'];
				var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
				var matchCustomerRole = false;
				if (customers){
					matchCustomerRole = new RegExp(customers, 'i');
				}
				var addJobButton = false;
				if (param && param.customer && customers && customers == 'all'){
					$('#addJobBtn').removeClass('hidden');
				}else if (param && param.customer && customers && userData && userData.kuser && userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['role-type'].match(matchCustomerRole)){
					$('#addJobBtn').removeClass('hidden');
				}
				var customers = addBookConfig.customer[param.customer];
				var matchCustomerRole = false;
				if (customers){
					matchCustomerRole = new RegExp(customers, 'i');
				}
				var addJobButton = false;
				if (param && param.customer && customers && customers == 'all'){
					$('#addBookBtn').removeClass('hidden');
				}else if (param && param.customer && customers && userData && userData.kuser && userData.kuser.kuroles && userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['role-type'].match(matchCustomerRole)){
					$('#addBookBtn').removeClass('hidden');
				}
				/* var sjCustomers = '^(' + submitJobConfig.customer.join('|') + ')$';
				var matchSJCustomer = new RegExp(sjCustomers, 'i'); */
				/*if ($('#cardView').is('.active') && param && param.customer && addJobConfig && addJobConfig.customer && param.customer.match(matchCustomer)) {
					$('#addJobBtn').removeClass('hidden');
				} else */
				if ($('#cardView').is('.active') && param && param.customer && submitJobConfig && submitJobConfig[param.customer] && submitJobConfig[param.customer][roleType] && submitJobConfig[param.customer][roleType].indexOf(accessLevel) == -1) {
					$('#submitJobBtn').removeClass('hidden');
				} else {
					return;
				}
			},
			openSubmitJob: function (param) {
				/* Open submit job modal */
				var clientList = '';
				if (!param) var param = {};
				if (!param.customer) param.customer = $('#filterCustomer > .filter-list.active').text();
				clientList += '<option data-value="' + param.customer + '" value="' + param.customer + '">' + param.customer.toLocaleUpperCase() + '</option>';
				$('#submitJob').modal();
				$('#submitJob select[id="ajcustomer"]').html(clientList);
				$('#submitJob #multipleUpload').prop("checked", false);
				$('#submitJob #reupload').prop("checked", false);
				$('#submitJob [data-type="single"]').removeClass('hidden');
				$('#submitJob select[id="ajcustomer"]').trigger('click');
				$('#submitJob').find('.file-list').not(":first").remove();
				$('#submitJob input').val('');
				$('#submitJob input').parent().find('span').text('Browse');
				$('#submitJob').find('.file-list').find('[data-mandatory]:not(:first)').each(function(){
					var mandatoryVal = $(this).val();
					var row = $(this).closest('.file-list');
					var clone = $(row).clone();
					$(clone).find('.removeFile').removeClass('disabled');
					$(clone).find('select').val(mandatoryVal);
					$('#submitJob .file-holder .file-list:last').after(clone);
				});
				$('#submitJob').find('.row.file-list').find('.addFile').addClass('disabled');
				$('#submitJob').find('.row.file-list').last().find('.addFile').removeClass('disabled');
			},
			openAddJob: function (param, targetNode) {
				//var userObj = JSON.parse($('#userDetails[data]').attr('data'));
				var clientList = '';
				if (!param) var param = {};
				if (!param.customer) param.customer = $('#filterCustomer > .filter-list.active').text();
				var clientType = $('#filterCustomer > .filter-list.active').attr('data-type');
				if (clientType == 'book'){
					modal = 'addChapter';
				}else{
					modal = 'addJob';
				}
				clientList += '<option data-value="' + param.customer + '" value="' + param.customer + '">' + param.customer.toLocaleUpperCase() + '</option>';
				$('#' + modal + ' [data-customer]').addClass('hidden');
				$('#' + modal + ' [data-skip-customer]').removeClass('hidden');
				$('#' + modal + ' [data-customer*=" ' + param.customer + ' "]').removeClass('hidden');
				$('#' + modal + ' [data-skip-customer*=" ' + param.customer + ' "]').addClass('hidden');
				/*Object.keys(userObj.kuser.kuroles).forEach(function (key, index) {
					clientList += '<option data-value="' + key + '" value="' + key + '">' + key.toLocaleUpperCase() + '</option>';
				});*/
				$('#' + modal + ' select[id="ajcustomer"]').html(clientList);
				$('#' + modal + ' #multipleUpload').prop("checked", false);
				$('#' + modal + ' #reupload').prop("checked", false);
				$('#' + modal + ' [data-type="single"]').removeClass('hidden');
				$('#' + modal + ' select[id="ajcustomer"]').trigger('click');
				$('#' + modal + ' input').val('');
				$('#' + modal + ' input').removeClass('is-invalid');
				$('#' + modal + '').modal();
			},
			addJobMultipleUpload: function (param, targetNode) {
				if (targetNode.is(":checked")) {
					$('#addJob [data-type="single"]').addClass('hidden');
				} else {
					$('#addJob [data-type="single"]').removeClass('hidden');
				}
			},
			addJob: function (param, targetNode) {
				var modal = '#' + param.modal;
				var validInput = true;
				$(modal + " [data-required]").each(function(){
					if ($(this).closest('.hidden').length == 0){
						$(this).removeClass('is-invalid');
						if ($(this).val() == "") {
							$(this).addClass('is-invalid');
							validInput = false;
						}
						if ($(this).attr('data-pattern')){
							var regexp = new RegExp($(this).attr('data-pattern'));
							if (! $(this).val().match(regexp)){
								$(this).addClass('is-invalid');
								validInput = false;
							}
						}
					}
				});
				if (! validInput){
					return false;
				}
				$('.la-container').css('display', '');
				var parameters = new FormData();
				parameters.append('customer', $(modal + " [id='ajcustomer']").val());
				parameters.append('project', $(modal + " [id='ajproject']").val());
				if ($('#addJob #reupload').is(":checked")) {
					parameters.append('forceLoad', 'true');
				}
				if ($('#addJob #multipleUpload').is(":checked")) {
					parameters.append('multiple', 'true');
				} else {
					if ($(modal + " input#ajdoi").val() == "") {
						$('.la-container').css('display', 'none');
						return false;
					}
					parameters.append('doi', $(modal + " input#ajdoi").val());
					parameters.append('articleTitle', $(modal + " input[id='ajtitle']").val());
					parameters.append('articleType', $(modal + " *[id='ajtype']").val());
				}
				$(modal + " input#manuscriptFile").removeClass('is-invalid');
				if ($(modal + " input#manuscriptFile")[0] && $(modal + " input#manuscriptFile")[0].files && $(modal + " input#manuscriptFile")[0].files.length > 0) {
					parameters.append('manuscript', $(modal + " input#manuscriptFile")[0].files[0]);
				}else if ($(modal + " input#manuscriptFile")[0] && $(modal + " input#manuscriptFile")[0].files && $(modal + " input#manuscriptFile")[0].files.length == 0) {
					$(modal + " input#manuscriptFile").addClass('is-invalid');
					$('.la-container').css('display', 'none');
					return false;
				}
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						$('#addJob').modal('hide');
						parameters.notify.notifyText = parameters.notify.successText;
						eventHandler.common.functions.displayNotification(parameters);
						$('.la-container').css('display', 'none');
					},
					error: function () {
						$('#addJob').modal('hide');
						parameters.notify.notifyText = parameters.notify.errorText;
						eventHandler.common.functions.displayNotification(parameters);
						$('.la-container').css('display', 'none');
					}
				})
				console.log(parameters);
			},
			submitJob: function (param, targetNode) {
				var modal = '#' + param.modal;
				var parameters = new FormData();
				parameters.append('customer', $(modal + " [id='ajcustomer']").val());
				parameters.append('project', $(modal + " [id='ajproject']").val());
				var validInput = true;
				$(modal + " [data-required]").each(function(){
					$(this).removeClass('is-invalid');
					if ($(this).val() == "") {
						$(this).addClass('is-invalid');
						validInput = false;
					}
					if ($(this).attr('data-pattern')){
						var regexp = new RegExp($(this).attr('data-pattern'));
						if (! $(this).val().match(regexp)){
							$(this).addClass('is-invalid');
							validInput = false;
						}
					}
				});
				if (! validInput){
					return false;
				}
				parameters.append('doi', $(modal + " input[id='ajdoi']").val());
				parameters.append('articleTitle', $(modal + " input[id='ajtitle']").val());
				parameters.append('addTitle', 'true');
				parameters.append('articleType', $(modal + " *[id='ajtype']").val());
				parameters.append('authorName', $(modal + " input[id='cauthor']").val());
				parameters.append('authorEmail', $(modal + " input[id='cemail']").val());
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify){
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				/* ZIP all uploaded files into a zip along with a manifest file */
				var zip = new JSZip();
				var files = $('.file-holder .file-list input');
				var fileList = '<files>';
				var manuscriptCount = 0;
				var validInput = true;
				var mustHaveFiles = [];
				$(modal + ' .file-holder .file-designation:first').find('option').each(function(){
					if ($(this)[0].hasAttribute('data-mandatory')){
						mustHaveFiles.push($(this).text());
					}
				});
				for (var f = 0, fl = files.length; f < fl; f++){
					var currFile = files[f];
					$(files[f]).removeClass('is-invalid');
					if (currFile.files.length == 0){
						$(files[f]).addClass('is-invalid');
						validInput = false;
					}
					var fileType = $(currFile).closest('.row').find('.file-designation').val();
					var fileTypeValue = $(currFile).closest('.row').find('.file-designation option:selected').text()
					var mindex = mustHaveFiles.indexOf(fileTypeValue);
					if (mindex > -1) {
						mustHaveFiles.splice(fileTypeValue, 1);
					}
					$(currFile).closest('.row').find('.file-designation').removeClass('is-invalid');
					if (fileType == 'manuscript'){
						if (manuscriptCount > 0){
							$(currFile).closest('.row').find('.file-designation').addClass('is-invalid');
							validInput = false;
						}
						manuscriptCount++;
					}
					if (currFile.files.length > 0){
						fileList += '<file file_name="' + currFile.files[0].name + '" file_size="' + currFile.files[0].size + '" file_designation="' + fileType + '"/>'
						zip.file(currFile.files[0].name, currFile.files[0]);
					}
				}
				fileList += '</files>';
				if (! validInput){
					return false;
				}
				if (mustHaveFiles.length > 0){
					$.each(mustHaveFiles, function (k, v) {
						
					});
					var notifyObj = {}
					notifyObj.notify = {};
					notifyObj.notify.notifyTitle = 'Files missing';
					notifyObj.notify.notifyText = 'Please upload the following file type : ' + mustHaveFiles.join(', ');
					notifyObj.notify.notifyType = 'info';
					eventHandler.common.functions.displayNotification(notifyObj);
					return false;
				}

				zip.file('kriya_job_manifest.xml', fileList);
				$('.la-container').css('display', '');
				zip.generateAsync({type:"blob"})
					.then(function(content) {
						var fileName = new Date().getTime() + '.zip';
						parameters.append('manuscript', content, fileName);
						$.ajax({
							type: 'POST',
							url: '/api/addjob',
							data: parameters,
							contentType: false,
							processData: false,
							success: function (response) {
								$('#submitJob').modal('hide');
								parameters.notify.notifyText = parameters.notify.successText;
								eventHandler.common.functions.displayNotification(parameters);
								$('.la-container').css('display', 'none');
							},
							error: function () {
								$('#submitJob').modal('hide');
								parameters.notify.notifyText = parameters.notify.errorText;
								eventHandler.common.functions.displayNotification(parameters);
								$('.la-container').css('display', 'none');
							}
						})
					})
					.catch(function(err){
						console.log(err)
					})
			},
			/**
			 * to get resources from cloud and display as files and folder
			 **/
			getResource: function (param, targetNode) {
				if (!param.doi || !param.customer || !param.project) {
					return;
				}
				var parameters = 'customer=' + param.customer + '&project=' + param.project + '&doi=' + param.doi + '&process=new';
				$.ajax({
					type: "GET",
					url: "/api/listbucket/?" + parameters,
					success: function (data) {
						$('.filemanager').find('.data').remove();
						$('.filemanager').find('.nav-wrapper').html('<div class="breadcrumb"><a href="javascript:;" class="breadcrumb-item">' + param.doi + '</a></div>');
						$('.filemanager').append(data);
						$('#fileBrowser').modal();
					},
					error: function (err) {
						console.log(err);
					}
				});
			},
			showHistory: function (param, targetNode) {
				$('#articleHistory table tr.stages').remove();
				$('#historyDoi').text('article');
				//$('#articleHistory').attr('data', '');
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(msCard).attr('data-id');
				var storeData = window[$(msCard).attr('data-store-variable')];
				var parentId = $(msCard).attr('data-dashboardview');
				var dataID = 'data-id';
				if(parentId=='#cardReportData'){
					dataID = 'data-report-id';
				}
				var sourceData ;
				if(storeData && storeData[$(msCard).attr('data-id')] && storeData[$(msCard).attr('data-id')]._source){
					sourceData = storeData[$(msCard).attr('data-id')]._source;
				}
				//$('#articleHistory').attr('data', $(msCard).attr('data').replace(/\'/g, '"'));
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var stages = sourceData.stage;
				var collapseRow = '', hideCompletedRow = '';
				var hideRowId = 0;
				for (var s = 0, sl = $(stages).length; s < sl; s++) {
					var stage = stages[s] ? stages[s] : stages;
					var doi = sourceData.id.replace('.xml', '').replace('\.', '');
					var startDate = new Date(stage['start-date']);
					var endDate = new Date(stage['end-date']);
					var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					var slaStartDate = new Date(stage['sla-start-date']);
					var inprogTimeDiff = Math.abs(new Date().getTime() - slaStartDate.getTime());
					var inprogressDayDiff = Math.ceil(inprogTimeDiff / (1000 * 3600 * 24));
					var stageRow = '';
					$('#historyDoi').text(sourceData.id.replace('.xml', ''));
					if (stage.status != 'in-progress' && stage.status != 'waiting') {
						var tempSlaStartDate = stage['sla-start-date'] ? dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempSlaEndDate = stage['sla-end-date'] ? dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempPlaStartDate = stage['planned-start-date'] ? dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempPlaEndDate = stage['planned-end-date'] ? dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") : '';
						if (hideRepeatedStage.default.includes(stage.name)) {
							collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>' + collapseRow;
							continue;
						} else {
							if (stages.length != s + 1 && stages[s + 1].status == 'completed' && hideRepeatedStage.default.includes(stages[s + 1].name)) {
								collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>' + collapseRow;
								continue;
							} else {
								if (collapseRow) {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned) ? ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) : '-') + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>'
								} else {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned) ? ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) : '-') + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>'
								}
							}
						}
					}
					if (collapseRow || hideCompletedRow) {
						var hideRow = '';

						if (hideCompletedRow) {
							hideRow += hideCompletedRow;
						}
						if (collapseRow) {
							hideRow += collapseRow;
						}
						stageRow = $(hideRow);
						collapseRow = hideCompletedRow = '';
						$('#articleHistory table tbody').prepend(stageRow);
					}
					if (stage.status == 'in-progress') {
						stageRow = $('<tr class="stages" data-stage-name="' + stage.name + '"/>');
						usersHTML = "";
						usersArr = {};
						var users;
						var assignUserRole = userData.kuser.kuroles[sourceData.customer]['role-type'];
						var assignUserAccess = userData.kuser.kuroles[sourceData.customer]['access-level'];
						if (userAssignAccessRoleStages && userAssignAccessRoleStages[assignUserRole] && userAssignAccessRoleStages[assignUserRole][assignUserAccess] && !userAssignAccessRoleStages[assignUserRole][assignUserAccess].includes(stage.name)) {
							if (stage.assigned.to) {
								users = $('<span>' + stage.assigned.to + '</span>');
							} else {
								users = $('<span>Unassigned</span>');
							}
						} else {
							users = $('<select class="assign-users" onchange="eventHandler.components.actionitems.assignUser(this,' + cardID + ', \''+dataID+'\', true, \'\', \'\', true, \'\',\'\',\''+parentId+'\')" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"/>');
							// $(usersDetailsDiv).find('user').each(function (i, v) {
							// 	users.append('<option value="' + $(this).find('first').text() + ', ' + $(this).find('email').text() + '">' + $(this).find('first').text() + '</option>');
							// });
							if (usersList && usersList.users) {
								Object.keys(usersList.users).forEach(function (count, q) {
									var skillLevel = (usersList.users[count].additionalDetails && usersList.users[count].additionalDetails['skill-level']) ? usersList.users[count].additionalDetails['skill-level'] : "";
									// To show only particular users for assigning the articles for particular stage based on user role-type and access-level
									if (showAssignUserName[assignUserRole] && showAssignUserName[assignUserRole][assignUserAccess]) {
										var role = usersList.users[count].additionalDetails['role-type'];
										var access = usersList.users[count].additionalDetails['access-level'];
										if (showAssignUserName[assignUserRole][assignUserAccess][stage.name] && showAssignUserName[assignUserRole][assignUserAccess][stage.name].roleType.indexOf(role) > -1 && showAssignUserName[assignUserRole][assignUserAccess][stage.name].accessLevel.indexOf(access) > -1) {
											users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
										}
									} else {
										users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
									}
								})
							}
							if (users.find('option[value="' + stage.assigned.to + '"]').length > 0) {
								users.find('option[value="' + stage.assigned.to + '"]').attr('selected', true);
								users.append('<option value="' + " " + '">Unassign</option>');
							} else {
								if (stage.assigned.to == null) {
									users.append('<option value="' + " " + '" selected>Unassigned</option>');
								} else {
									users.append('<option value="' + stage.assigned.to + '"  selected>' + stage.assigned.to + '</option>');
									users.append('<option value="' + " " + '">Unassign</option>');
								}
							}
						}
						usersHTML = $(users)[0].outerHTML;
						//update by prasana
						if ($('#articleHistory table tbody tr').hasClass('hideRow' + hideRowId)) {
							stageRow.html('<td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						} else {
							stageRow.html('<td></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'\', true,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						}
						//update end by prasana
					}
					hideRowId++;
					// //if (usersArr.users) {
					$('#articleHistory table tbody').prepend(stageRow);
					//console.log(stages[s])
				}
				$('#articleHistory table tbody').prepend('<tr class="stages"><th/><th>Stage Name</th><th>Assigned to</th><th>Start Date</th><th>End Date</th><th>Sla-Start Date</th><th>Sla-End Date</th><th>Planned-Start Date</th><th>Planned-End Date</th><th>Status</th><th>Days</th></tr>')
				$('#articleHistory').modal()
			},
			articleInfo: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(msCard).attr('data-id');
				var storeData = window[$(msCard).attr('data-store-variable')];
				var sourceData;
				if (storeData && storeData[cardID] && storeData[cardID]._source) {
					sourceData = storeData[cardID]._source;
				}
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var infoTable = '';
				$('#articleInfo .header-tag span').html(sourceData.doi);
				$('#articleInfo tbody').html('');
				for (var info of articleInfoDetails) {
					if (sourceData[info.attribute] == null) {
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>0</td></tr>'
					} else {
						var infoValue = '';
						if (info.name == 'Authors') {
							if (sourceData[info.attribute].name.length) {
								for (var author of sourceData[info.attribute].name) {
									infoValue += author['given-names'] + ' ' + author.surname + ', '
								}
								infoValue = infoValue.slice(0, -2);
							} else {
								infoValue = sourceData[info.attribute].name['given-names'] + ' ' + sourceData[info.attribute].name.surname
							}
						} else {
							infoValue = sourceData[info.attribute]
						}
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>' + infoValue + '</td></tr>'
					}
				}
				$('#articleInfo tbody').html(infoTable);
				$('#articleInfo').modal();
				$('#articleInfo').on('hide.bs.modal', function (event) {
					$('#articleInfo .modal-body').scrollTop(0);
				});
			},
			collapseHistoryRow: function (node, buttonClass) {
				$('.' + buttonClass).slideToggle(0);
				if ($(node).hasClass("fa-plus-circle")) {
					$(node).addClass("fa-minus-circle").removeClass('fa-plus-circle')
				} else {
					$(node).addClass("fa-plus-circle").removeClass('fa-minus-circle')
				}
			},
			assignUserForceLogOff: function (param) {
				if (!param) {
					return;
				}
				$.ajax({
					url: '/api/updatedatausingxpath',
					type: 'POST',
					data: param,
					async: false,
					success: function (res) {
						if (res) {
							eventHandler.components.actionitems.assignUser(param.name, param.cardID, param.sourceID, true, '', '', true, '','', param.parentId);
						}
					},
					error: function (res) {
					}
				});
			},
			getUserDetails: function (customer) {
				jQuery.ajax({
					type: "GET",
					url: "/api/getassignuserdata?customer=" + customer,
					success: function (msg) {
						usersDetailsDiv = $(msg);
						usersList.users = [];
						if ($(usersDetailsDiv).find('user').length <= 1 && $(usersDetailsDiv).find('first').text() != userData.kuser.kuname.first) {
							var userInfo = {};
							userInfo.name = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last;
							userInfo.email = userData.kuser.kuemail;
							userInfo.additionalDetails = Object.entries(userData.kuser.kuroles)[0][1];
							usersList.users.push(userInfo);
						}
						$(usersDetailsDiv).find('user').each(function (i, v) {
							var userInfo = {};
							userInfo.name = $(this).find('first').text() + ' ' + $(this).find('last').text();
							userInfo.email = $(this).find('email').text();
							userInfo.additionalDetails = {};
							$(this).find('role').each(function () {
								$.each(this.attributes, function () {
									if (this.specified) {
										userInfo.additionalDetails[this.name] = this.value;
									}
								});
							});
							usersList.users.push(userInfo);
						});
					}
				})
			},
			/**
			 * To get user details based on our role and access level from elastic
			 */
			getUserDetails_new: function (customer) {
				jQuery.ajax({
					type: "GET",
					url: "/api/getassignuserdata?customer=" + customer,
					success: function (msg) {
						usersList.users = msg;
					}
				})
			},
			holdArticle: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(targetNode).closest('.msCard').attr('data-id');
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				if ($(""+parentId+"[data-id='" + cardID + "']").find('.msStageName').text() == 'Hold') {
					$('#modalUnHold').find('div.commentArea').text('');
					$('#modalUnHold #unholdid').text($(""+parentId+"[data-id='" + cardID + "']").attr('data-doi'));
					$('#modalUnHold .saveHold').attr('data-message', "{'funcToCall': 'saveHold', 'param':{'holdtype':'release', 'cardid':'" + cardID + "', 'parentid': '"+parentId+"', 'modal':'#modalUnHold'}}");
					$('#modalUnHold').modal();
				} else {
					$('#modalHold').find('div.commentArea').text('');
					$('#modalHold').find('select').val();
					$('#modalHold #holdid').text($(""+parentId+"[data-id='" + cardID + "']").attr('data-doi'));
					$('#modalHold .saveHold').attr('data-message', "{'funcToCall': 'saveHold', 'param':{'holdtype':'hold', 'cardid':'" + cardID + "', 'parentid': '"+parentId+"', 'modal':'#modalHold'}}");
					$('#modalHold').modal();
				}
			},
			fastTrack: function (param, targetNode) {
				if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
					return;
				}
				var msCard = $(targetNode).closest('.msCard');
				var cardID = $(targetNode).closest('.msCard').attr('data-id');
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				var modal = '#modalFastTrack';
				if(param.fastTrack == 'true'){
					$(modal).find('.FastTrackHeader').text('Remove Fast Track');
				} else {
					$(modal).find('.FastTrackHeader').text('Add Fast Track');
				}
				//var param = {};
				//param.cardID = $(targetNode).closest('.msCard').attr('data-id');
				param.modal = modal;
				param.parentId = parentId;
				$(modal).find('select').prop('selectedIndex', 0);
				$(modal).find('div .commentArea').text('');
				var buttonInfo = {
					'funcToCall': 'saveComment',
					'param': {
						'funcToPost': 'eventHandler.components.actionitems.saveFastTrack',
						'currentNode': param
					}
				};
				$(modal + ' .saveFastTrack').attr("data-message", JSON.stringify(buttonInfo));
				$(modal).modal();
			},
			conformationModal: function (type, targetNode) {
				if($(targetNode).parentsUntil('li').find('.actionBtns .fa.fa-flag').length){
					$('#blkupdate').find('.text').text('Article is in Fast-Track. Are you sure you want to put it on Hold?');
					$('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.openmodal(\''+type+'\',\''+ $(targetNode).attr('test') +'\')');
					$('#blkupdate').modal();
					return;
				} else {
					eventHandler.components.actionitems.openmodal(type, targetNode);
				}
			},
			openmodal: function (type, targetNode) {
				console.log(targetNode);
				if(typeof (targetNode) == 'string'){
					targetNode = $('#manuscriptsDataContent').find('[test="'+ targetNode +'"][title="Hold"]');
					$('#blkupdate').modal('hide');
				} else {
					if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
						return;
					}
				}
				var modal = '#commonmodal';
				$(modal + ' .commonModalHeader').text();
				$(modal + ' .save').removeAttr('data-message');
				$(modal + ' .commentbox').addClass('hidden');
				$(modal + ' .dropdown').addClass('hidden');
				var msCard = $(targetNode).closest('.msCard');
				var param = {};
				param.cardID = $(targetNode).closest('.msCard').attr('data-id');
				param.parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				param.modal = modal;
				console.log(param.cardID);
				var doi = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-doi');
				var header = '';
				if (popups[type].title != undefined) {
					header = popups[type].title;
				}
				var descriptiontext = popups[type].descriptiontext;
				if (popups[type].function != undefined) {
					$(modal + ' .save').attr("data-message", "{'funcToCall': 'saveComment', 'param':{ 'funcToPost':'" + popups[type].function+"',  'currentNode':'" + JSON.stringify(param) + "'}}");
				}
				if (header.match('{doi}')) {
					header = header.replace('{doi}', doi);
				}
				if (popups[type].commentbox) {
					$(modal + ' .commentbox').removeClass('hidden');
				}
				if (popups[type].dropdown) {
					$(modal + ' .dropdown').removeClass('hidden');
					options = "<option value='' disabled='disabled' selected='selected'>CHOOSE OPTION</option>";
					popups[type].options.forEach(function (element) {
						console.log(element);
						options += "<option value='" + element + "' >" + element.toUpperCase() + "</option>";
					});
					$('#commonmodal .dropdown').find('select').html(options);
				}
				if (popups[type].confirmation) {}
				$(modal + ' .commonModalHeader').text(header);
				$(modal).modal();
			},
			saveHold: function (param) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var hold = {};
				hold.type = $(modal).find('select').val();
				if ((hold.type == "" || hold.type == null || hold.type == undefined) && !$(modal).find('.dropdown').hasClass('hidden')) {
					$(modal).find('#modalError').html("Please select an option.");
					return;
				} else {
					var holdIcontooltip = "";
					if (hold.comment != null && hold.comment != "") {
						holdIcontooltip = 'data-tooltip="' + hold.comment + '"';
					}
					var parameters = {};
					var notify = {};
					// parameters.holdComment = $(modal).find('div.commentArea').text();
					if (hold.type != null && hold.type != undefined) {
						// parameters.holdComment = hold.type + ': ' + parameters.holdComment;
					}
					parameters.cardID = cardID;
					parameters.parentId = parentId;
					parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
					parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
					parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
					var currStageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
					var d = new Date();
					var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
					var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
					if (currStageName == 'Hold') {
						parameters.releaseHold = 'true';
						notify.notifyTitle = "Remove Hold";
						notify.successText = parameters.doi + " has been removed from hold";
						notify.errorText = "Error while remove hold for " + parameters.doi;
					} else {
						parameters.stageName = 'Hold';
						notify.notifyTitle = "Add Hold";
						notify.successText = parameters.doi + " has been put on hold";
						notify.errorText = "Error while hold article " + parameters.doi;
					}
					parameters.notify = notify;
					console.log(parameters);
					$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
					$(modal).find('#modalError').html("");
					$(modal).find('div .commentArea').text('');
					eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
					$(modal).modal('hide');
				}
			},
			revokeArticle: function (param, targetNode) {
				var modal = $(param.modal).closest('.modal');
				var parameters = {};
				var msCard = $(targetNode).closest('.msCard');
				parameters.cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
					return;
				}
				(new PNotify({
					title: '<b>Revoke access for Author</b>',
					text: 'You are trying to revoke the access from the Author. If you continue, Author will no longer be able to access the article using the link sent to him.<br/><br/>Click <b>OK</b> to continue !',
					icon: 'glyphicon glyphicon-question-sign',
					hide: false,
					confirm: {
						confirm: true
					},
					buttons: {
						closer: false,
						sticker: false
					},
					history: {
						history: false
					}
				})).get().on('pnotify.confirm', function () {
					eventHandler.components.actionitems.sendRevokeArticle(parameters);
				}).on('pnotify.cancel', function () {
					console.log('close');
					$(modal).modal('hide');
				});
			},
			sendRevokeArticle: function (param) {
				console.log(param);
				var cardID = param.cardID;
				var parentId = param.parentId;
				var parameters = {};
				var notify = {};
				parameters.cardID = param.cardID;
				parameters.parentId = param.parentId;
				parameters.customerName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.projectName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-stage-name');
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				eventHandler.common.functions.sendAPIRequest('revokearticle', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
			},
			saveFastTrack: function (param, commentText) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var fastTrack = {};
				fastTrack.type = param.fastTrack;
				// fastTrack.comment = $(modal).find('div .commentArea').text()
				fastTrack.comment = commentText;
				if ((fastTrack.type == "" || fastTrack.type == null || fastTrack.type == undefined) && !$(modal).find('.dropdown').hasClass('hidden')) {
					return;
				} else {
					var parameters = {};
					var notify = {};
					parameters.cardID = cardID;
					parameters.parentId = parentId;
					// parameters.fastTrackComment = $(modal).find('div .commentArea').text();
					parameters.fastTrackComment = commentText;
					parameters.customerName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
					parameters.projectName = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
					parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
					var currStageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
					var fastTrackType = $(""+parentId+" [data-id='" + cardID + "']").attr('data-flag');
					var d = new Date();
					var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
					var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
					//parameters.data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><status>completed</status><end-date>' + currDate + '</end-date></stage>';
					if (fastTrack.type == 'false') {
						fastTrack.type == 'fastTrack'
						var priority = 'on';
					} else if (fastTrack.type == 'true') {
						fastTrack.type == 'remove'
						var priority = 'off';
					}
					parameters.data = '<priority requested-date="'+ currDate + ' ' + currTime + '">' + priority + '</priority>';
					parameters.updateOn = 'workflow';
					if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
						parameters.notify = notifyConfig[arguments.callee.name].notify;
						parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
					}
					console.log(parameters);
					$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
					$(modal).find('#holdError').html("");
					$(modal).find('div.commentArea').text('');
					eventHandler.common.functions.sendAPIRequest('updatedatausingxpath', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
					$(modal).modal('hide');
				}
			},
			saveComment: function (param, source) {
				if (!param.currentNode) {
					return;
				}
				$(source).css("pointer-events", "none");
				var commentText = '';
				if (typeof (param.currentNode) == 'string') {
					var currentNode = JSON.parse(param.currentNode);
				} else {
					var currentNode = param.currentNode;
				}
				if (!param.commentText) {
					var modal = $(currentNode.modal).closest('.modal');
					if ($(modal).find('select').val() != null) {
						commentText += $(modal).find('select').val() + ': ';
					} else {
						commentText += $(modal).find('.modal-header').children().first().text() + ' : ';
					}
					if ($(modal).find('div .commentArea').text() != '') {
						commentText += $(modal).find('div .commentArea').text();
					}
				} else {
					commentText = param.commentText;
				}
				if (commentText != "") {
					eventHandler.components.actionitems.postComments(commentText, currentNode.cardID, ' ', source, currentNode.parentId);
				}
				if (param.funcToPost) {
					eval(param.funcToPost)(currentNode, commentText);
				}
				$(source).css("pointer-events", "auto");
			},
			resupplyArticle: function (param) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var resupply = {};
				resupply.comment = $(modal).find('div .commentArea').text()
				var parameters = {};
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				parameters.fastTrackComment = $(modal).find('div .commentArea').text();
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.journalID = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.articleID = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.currStage = 'papresupply';
				//Added by anuraja for git-1714 on 01-11-2018 - To add comment in previous stage
				parameters.addStageComment = 'true';
				// parameters.status = {'code':'200'};
				parameters.resupply = 'true';
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.articleID));
				}
				console.log(parameters);
				$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
				$(modal).find('div.commentArea').text('');
				eventHandler.common.functions.sendAPIRequest('signoff', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
				$(modal).modal('hide');
			},
			packageCreation: function (btnNode, target) {
				if (!btnNode) return true;
				var cardID = $(target).closest('.msCard').attr('data-id');
				var storeData = $(target).closest('.msCard').attr('data-store-variable');
				var parentId = $(target).closest('.msCard').attr('data-dashboardview');
				var currentCard = window[storeData][cardID]._source;
				var customer = $(target).closest('.msCard').attr('data-customer')
				if (packageCreationParameter[customer]) {
					var parameter = {};
					parameter = JSON.parse(JSON.stringify(packageCreationParameter[customer]));
					var parameterString = '';
					Object.keys(parameter).forEach(function (value, key) {
						if (currentCard[parameter[value]]) {
							if (value == 'issueFileName') {
								parameter[value] = currentCard[parameter[value]] + '.xml'
							} else {
								parameter[value] = currentCard[parameter[value]]
							}
						}
						parameterString += value + '=' + parameter[value]
						if (Object.keys(parameter).length - 1 > key) parameterString += '&'
					})
				}
				$('#notifyBox .notifyHeaderContent').text("Publishing file... Please wait.");
				$('#notifyBox #reloadinfobody').text('');
				$('#notifyBox').show();
				$.ajax({
					type: 'GET',
					url: "/api/publisharticle?" + parameterString,
					success: function (data) {
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))) {
							eventHandler.components.actionitems.getJobStatus(parameter.client, data.message.jobid, '', '', '', parameter.id);
						}
						else if (data == '') {
							$('#notifyBox #reloadinfobody').text('Failed');
						} else {
							$('#notifyBox #reloadinfobody').text(data.status.message);
							if (data.status.message == "job already exist") {
									eventHandler.components.actionitems.getJobStatus(parameter.client, data.message.jobid, '', '', '', parameter.id);
							}
						}
					},
					error: function (error) {
						$('#notifyBox #reloadinfobody').text('Failed');
					}
				});
			},
			probeValidation: function (param, target) {
				if (!target || $(target).length == 0) {
					return false;
				}
				var parameter = {
					'doi': $(target).closest('.msCard').attr('data-doi'),
					'customer': $(target).closest('.msCard').attr('data-customer'),
					'project': $(target).closest('.msCard').attr('data-project'),
					'stage': $(target).closest('.msCard').attr('data-stage-name'),
					'role': 'typesetter'
				}
				$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
				$('.la-container').fadeIn();
				$.ajax({
					type: "POST",
					url: "/api/probevalidation",
					data: parameter,
					success: function (res) {
						$('.la-container').fadeOut();
						$('#probeResult #PMC-Validator,#probeResult #DTD-Validator,#probeResult #Schematron-Validator,#probeResult #Probe-Validator').html('');
						$('#probeResult h3 span').text(parameter.doi);
						eventHandler.components.actionitems.updateProbeStatus(res);
					},
					error: function (response) {
						$('.la-container').fadeOut();
						var errorParam = {
							'notify': {
								notifyTitle: 'ERROR',
								notifyType: 'error',
								notifyText: 'Probe validation failed.'
							}
						}
						eventHandler.common.functions.displayNotification(errorParam);
					}
				});
			},
			updateProbeStatus: function (res) {
				if (res && typeof(res) == 'string') {
					res = res.replace(/<column[\s\S]?\/>/g, '<column></column>');
					var resNode = $(res);
					if (resNode.find('message:not(:empty)').length > 0) {
						var old_ruleType = '';
						$('#probeResult ul,#probeResult .tab-content').html('');
						resNode.find('message').each(function (count) {
							if ($(this).find('remove-display').text() == "true") {
								return;
							}
							var ruleType = $(this).find('rule-type').text();
							if (old_ruleType == '' || old_ruleType != ruleType) {
								if (count == 0) {
									$('#probeResult ul').append('<li class="btn btn-success" onclick="eventHandler.components.actionitems.probeType(this,\'' + ruleType + '\');"><a>' + ruleType + '</a></li>');
									$('#probeResult .tab-content').append('<div id="' + ruleType + '" class="tab-pane active in"></div>');
								} else {
									$('#probeResult ul').append('<li class="btn" onclick="eventHandler.components.actionitems.probeType(this,\'' + ruleType + '\');"><a>' + ruleType + '</a></li>');
									$('#probeResult .tab-content').append('<div id="' + ruleType + '" class="tab-pane"></div>');
								}
							}
							old_ruleType = ruleType;
							var errorId = $(this).find('error-id').text();
							var queryTo = $(this).find('query-role').text();
							var row = $('<div class="row" id="probe-container"/>');
							var left = $('<div class="col-sm-3 small" />');
							var right = $('<div class="col-sm-9" />');
							if ($(this).find('node-id').text())
								left.append('<p> Node ID: <span class="label label-default">' + $(this).find('node-id').text() + '</span></p>');

							if ($(this).find('rule-id').text())
								left.append('<p> Rule ID: <span class="label label-default">' + $(this).find('rule-id').text() + '</span></p>');

							if ($(this).find('signoff-allow').text())
								left.append('<p> Signoff allow: <span class="label label-default">' + $(this).find('signoff-allow').text() + '</span></p>');

							if (ruleType == "DTD-Validator") {
								left.append('<p> Line: <span class="label label-default">' + $(this).find('line').text() + '</span></p>');
								left.append('<p> Column: <span class="label label-default">' + $(this).find('column').text() + '</span></p>');
							} else if (ruleType == "Schematron-Validator") {
								left.append('<p> Error ID: <span class="label label-default">' + errorId + '</span></p>');
							}
							row.append(left);
							right.append('<p>' + $(this).find('probe-message').text() + '</p>');
							if ($(this).find('text').text() != "") {
								right.append('<p>TEXT: ' + $(this).find('text').text() + '</p>');
							}
							if ($(this).find('xpath').text())
								right.append('<pre>' + $(this).find('xpath').text() + '</pre>');
							row.append(right);
							$('#' + ruleType).append(row);
						});
						$('#probeCount').text($('#probeResult .tab-pane.active').children().length)
						$('#probeResult #countTitle, #probeResult .modal-body ul').show();
						$('#probeResult .noProbeError').hide();
					} else {
						$('#probeResult .noProbeError').show();
						$('#probeResult #countTitle, #probeResult .modal-body ul').hide();
					}
					$('#probeResult').modal('show');
				}
			},
			probeType: function (node, probeType) {
				$(node).siblings().removeClass('btn-success');
				$(node).addClass('btn-success');
				$('#probeResult .tab-content').children().removeClass('active');
				$('#probeResult .tab-content #' + probeType).addClass('active');
				$('#probeCount').text($('#probeResult .tab-pane.active').children().length)
			},
			withdrawArticle: function (param) {
				var modal = $(param.modal).closest('.modal');
				var cardID = param.cardID;
				var parentId = param.parentId;
				var resupply = {};
				resupply.comment = $(modal).find('div .commentArea').text()
				var parameters = {};
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				parameters.fastTrackComment = $(modal).find('div .commentArea').text();
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = 'Withdrawn';
				parameters.addStageComment = 'true';
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				console.log(parameters);
				$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
				$(modal).find('div.commentArea').text('');
				eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
				$(modal).modal('hide');
			},
			markUrgent: function (param, targetNode) {
				$('#modalUrgent').modal();
			},
			//save workflow's stage due dates
			saveWFDate: function (datePicker, type, cardID, source, replaceCard, from, id, stageName, parentId) {
				if (cardID == undefined) {
					//var param = JSON.parse($('#articleHistory').attr('data').replace(/\'/g, '"'));
				}
				var parameters = {};
				var dueDateNode = $('[id="' + datePicker + '"]').parent().find('[data-date-type]');
				var dateType = dueDateNode.attr('data-date-type');
				if ($('[id="' + datePicker + '"]').data('datepicker').selectedDates.length == 0) return false;
				var dueDate = $('[id="' + datePicker + '"]').data('datepicker').selectedDates[0].toString().slice(4, 24);
				if ($('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == false && from != "conform") {
					$('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.blkSaveWFDate("' + datePicker + '","' + type + '","' + cardID + '","' + source + '",' + replaceCard + ',"conform","' + id + ', ' + parentId + '")')
					$('#blkupdate').modal();
					return;
				}

				//dueDateNode.html(dateFormat(new Date(dueDate), 'mmm dd yyyy HH:MM:ss'));
				// var formatDueDate = dateFormat(new Date(dueDate), 'yyyy-mm-dd');
				var formatDueDate = dateFormat(new Date(dueDate), 'yyyy-mm-dd HH:MM:ss');
				/*if($('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == "false"){
							$('#blkupdate').modal('hide');
						$('[type="checkbox"]:checked').closest('tr.reportTable').each(function(){
							if($("[id='dateRepEnd"+$(this).attr('data-doi')+"']").length > 0){
								dueDateNode = $("[id='dateRepEnd"+$(this).attr('data-doi')+"']");
							}else{
								dueDateNode =$("[id='blkdateRepEnd"+$(this).attr('data-doi')+"']")
							}
							$('[data-doi="'+$(this).attr('data-doi')+'"].Pickdate').html(dateFormat(new Date(dueDate), 'mmm dd, yyyy HH:MM:ss'));
						//dueDateNode.html(dateFormat(new Date(dueDate), 'mmm dd yyyy HH:MM:ss'));
							var currStageName = $(this).attr('data-stage-name');
							cardID=	$(this).attr('data-report-id');
							parameters.data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><' + dateType + '>' + formatDueDate + '</' + dateType + '></stage>';
							parameters.customerName = $("["+ source +"='" + cardID + "']").attr('data-customer');
							parameters.projectName = $("["+ source +"='" + cardID + "']").attr('data-project');
							parameters.doi = $("["+ source +"='" + cardID + "']").attr('data-doi');
							parameters.cardID = cardID;
						//$("[" + source + "='" + cardID + "']").find('[data-filter="Stage Due"]').text(dueDate);
							if (notifyConfig["saveWFDate"] && notifyConfig["saveWFDate"].notify) {
								parameters.notify = notifyConfig["saveWFDate"].notify;
								parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
							}
						eventHandler.common.functions.sendAPIRequest('updatedatausingxpath', parameters, "POST",function(params){
							//$('[data-report-id="'+params.cardID+'"] [data-filter="Stage Due"]').text($($('[data-report-id="'+params.cardID+'"]').closest('tbody').find('.Pickdate')[0]).text())
						});
						});
					}else{*/
				$('[data-doi="' + id + '"].Pickdate').html(dateFormat(new Date(dueDate), 'mmm dd, yyyy HH:MM:ss'));
				if (stageName == undefined) {
					var currStageName = $('[id="' + datePicker + '"]').closest('tr').attr('data-stage-name');
				} else {
					var currStageName = stageName;
				}
				//var currStageName = $('#' + datePicker).closest('tr').find('td.stage-name').text();
				parameters.data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><' + dateType + '>' + formatDueDate + '</' + dateType + '></stage>';
				parameters.customerName = $("[" + source + "='" + cardID + "']").attr('data-customer');
				parameters.projectName = $("[" + source + "='" + cardID + "']").attr('data-project');
				parameters.doi = $("[" + source + "='" + cardID + "']").attr('data-doi');
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				if (notifyConfig[arguments.callee.name] && notifyConfig[arguments.callee.name].notify) {
					parameters.notify = notifyConfig[arguments.callee.name].notify;
					parameters.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi));
				}
				if (replaceCard) {
					eventHandler.common.functions.sendAPIRequest('updatedatausingxpath', parameters, "POST", eventHandler.dropdowns.article.getArticles)
				} else {
					eventHandler.common.functions.sendAPIRequest('updatedatausingxpath', parameters, "POST", function (params) {
						for (var stage = 0; stage < chartData[cardID]._source.stage.length; stage++) {
							if (chartData[cardID]._source.stage[stage].name == currStageName) {
								chartData[cardID]._source.stage[stage][dateType] = formatDueDate;

							}
						}
						//$('[data-report-id="'+params.cardID+'"] [data-filter="Stage Due"]').text($($('[data-report-id="'+params.cardID+'"]').closest('tbody').find('.Pickdate')[0]).text());
					});
				}
				// jQuery.ajax({
				// 	type: "POST",
				// 	url: "/api/updatedatausingxpath",
				// 	data: {
				// 		'customerName': $('#customerselect').val(),
				// 		'projectName': $('#projectselect').val(),
				// 		'doi': dueDateNode.closest('.jobCards[data-doiid]').attr('data-doiid'),
				// 		"data": data
				// 	},
				// 	success: function (response) {
				// 		console.log(response)
				//}
				// });
			},
			assignUser: function (name, cardID, source, replaceCard, from, id, bulkUpdate, blkUpdateCount, bulkClickEvent, parentId) {
				if (cardID == undefined) {
				//var param = JSON.parse($('#articleHistory').attr('data').replace(/\'/g, '"'));
				}
				var currentClickEvent;
				if (!source || source == undefined) {
					source = 'data-id'
				}
				if(parentId=='#cardReportData'){
					sourceAtt = 'data-id';
				}else{
					sourceAtt = source;
				}
				if (bulkUpdate == undefined || bulkUpdate == undefined ) {
				    bulkUpdate = true;
				}
				var customer = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-customer')
				var roleType = userData.kuser.kuroles[customer]['role-type'];
				var accessLevel = userData.kuser.kuroles[customer]['access-level'];
				if (typeof (name) == 'object' && $(name).parent().parent().find('[type="checkbox"]').prop("checked") == true && id == '#modalReport' && $('tr.reportTable [type="checkbox"]:checked').length > 1 && replaceCard == false && from != "conform") {
				    blkSuccessDoi = [], blkFailureDoi = [], openedArticles = [];
				    successCount = 0;
				    failureCount = 0;
				    $('#blkupdate').find('.text').text('Are you sure? you are trying to do bulk updation.');
				    $('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.blkUpdate("' + name.value + '","' + source + '",' + replaceCard + ', \''+parentId+'\')');
				    $('#blkupdate').find('.cancelBulkUpdate').attr('onclick', 'eventHandler.components.actionitems.cancelBlkUpdate("' + $(name).attr('data-doi') + '")');
				    $('#blkupdate').find('.cancelBulkUpdate').attr('onclick', 'eventHandler.components.actionitems.cancelBlkUpdate("' + $(name).attr('data-doi') + '")');
				    $('#blkupdate').modal();
				    return;
				}
				var userEmail = userData.kuser.kuemail;
				var userName = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last + ', ' + userEmail;
				if (typeof (name) == 'object') {
					currentClickEvent = name;
					if (name.className == 'msAssignToME') {
						name.value = userName;
						var skillLevel = userData.kuser.kuroles[$("" + parentId + " [" + sourceAtt + "='" + cardID + "']").attr('data-customer')]['skill-level']
					} else {
						var skillLevel = $(name).find('[value="' + name.value + '"]').attr('skill-level');
						name = name.value;
					}
				}
				var parameters = {};
				var d = new Date();
				var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
				var currStageName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-stage-name');
				parameters.customerName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-customer');
				parameters.projectName = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [" + sourceAtt + "='" + cardID + "']").attr('data-doi');
				if (name.value == " ") {
					email = $(name.offsetParent).find('[selected="selected"]').val().replace(/.*, /g, '')
				}
				if (typeof name != "string") {
				    name = name.value;
				}
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				var email = '';
				if (name.split(',')[1]) {
				    email = (name.split(',')[1]).replace(' ', '');
				}
				var updateParameter = {
				    "email": email,
				    "customerName": customer,
				    "projectName": parameters.projectName,
				    "assign": (name == " ") ? false : true,
				    "doi": parameters.doi,
				    "assignedBy": userName,
				    "assignTo": (name == " ") ? "" : name
				}
				var ajaxUrl = "/api/assignuser";
				if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].stage.indexOf(currStageName) > -1) {
					updateParameter = {};
					updateParameter = {
						"customer": customer,
						"journalID": parameters.projectName,
						"articleID": parameters.doi,
						"pageCount": Number($("" + parentId + " [" + sourceAtt + "='" + cardID + "']").attr('data-page-count')),
						"wordCountWithoutRef": Number($("" + parentId + " [" + sourceAtt + "='" + cardID + "']").attr('data-wordCountWithoutRef')),
						"assign": (name == " ") ? false : true,
						"assignTo": (name == " ") ? "" : name,
						"email": email,
						"assignedBy": userName,
						"assignType": (name == " ") ? "Unassigned" : "Assigned",
						"skillLevel": (name == " ") ? "" : skillLevel,
						"to": (accessLevel == 'manager') ? email : "",
						"status": {
							"code": 200,
							"message": "Added successfully"
						}
					}
					if (accessLevel == 'manager') {
						ajaxUrl = '/workflow?currStage=copyeditingManager';
					} else {
						ajaxUrl = '/workflow?currStage=copyeditingVendor';
					}
				}
				var param = {
				    'notify': {
					'notifyTitle': 'Assign User',
					'notifyText': (name == " ") ? 'User unassigned successfully for {doi}' : 'User assigned successfully for {doi}',
					'notifyType': 'success'
				    }
				}
				jQuery.ajax({
					type: "POST",
					url: ajaxUrl,
					data: updateParameter,
					dataType: "json",
					success: function (message) {
						if (message.content || message.status) {
							successCount++;
							(name == "") ? name = " " : name;
							var jQueryID = '', jQueryIDVal = '';
							if ($('#articleHistory.show').length || $('#reportsHistory.show').length) {
								if (sourceAtt == 'data-report-id') {
									jQueryID = '#reportsHistory .assign-users,#chartReportTable [data-doi="' + parameters.doi + '"].assign-users'
									jQueryIDVal = '#reportsHistory .assign-users [value="' + name + '"], #chartReportTable [data-doi="' + parameters.doi + '"].assign-users [value="' + name + '"]'
								} else {
									jQueryID = '#articleHistory .assign-users';
									jQueryIDVal = '#articleHistory .assign-users [value="' + name + '"]'
								}
							} else {
								jQueryID = '#chartReportTable [data-doi="' + parameters.doi + '"].assign-users'
								jQueryIDVal = '#chartReportTable [data-doi="' + parameters.doi + '"].assign-users [value="' + name + '"]'
							}
							$(jQueryID).find('[selected]').removeAttr('selected');
							$(jQueryIDVal).attr("selected", "true");
							$(jQueryID).val($(jQueryIDVal).val());
							if (!bulkUpdate) {
								changeUserNameInHistory(cardID);
								blkSuccessDoi.push(parameters.doi);
							} else {
								if (sourceAtt == 'data-report-id') {
									changeUserNameInHistory(cardID);
								}
								param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, parameters.doi);
								eventHandler.common.functions.displayNotification(param);
								if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access) {
									if (name == " ") {
										userData.kuser.kuroles[customer]['assigned-pages'] = Number(userData.kuser.kuroles[customer]['assigned-pages']) - updateParameter['pageCount'];
									} else {
										userData.kuser.kuroles[customer]['assigned-pages'] = Number(userData.kuser.kuroles[customer]['assigned-pages']) + updateParameter['pageCount'];
									}
								}
							}
							if (successCount == blkUpdateCount) {
								bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi);
							}
						}
						parameters.unassign = (name == " ") ? true : false;
						if ($('#articleHistory.show').length || $('#manuscriptContent').hasClass('active')) eventHandler.dropdowns.article.getArticles(parameters);
						$('.la-container').css('display', 'none');
					},
					error: function (e) {
						if (e && e.responseText != undefined && e.responseText && e.responseText.match(/status|message/g)) {
							e.responseText = JSON.parse(e.responseText);
							var respText = JSONPath({ json: e.responseText, path: '$..message' });
							e.responseText = (respText && respText[1]) ? respText[1]:' ';
						}
						successCount++;
						if (!bulkUpdate) {
							if (e && e.responseText && e.responseText.match('opened')) {
								openedArticles.push({ 'name': (/(by )(.*)/g).exec(e.responseText)[2], 'doi': parameters.doi });
							} else {
								blkFailureDoi.push(parameters.doi);
							}
							$('[data-doi="' + parameters.doi + '"].assign-users').val($(bulkClickEvent).find('[selected]').val());
						} else {
							if (name == " " && e && e.responseText != undefined) {
								param.notify.notifyText = e.responseText.replace(/assigning/g, 'unassigning');
							}
							param.notify.notifyText = 'Error While Assigning ' + parameters.doi;
							if (e && e.responseText != undefined && !e.responseText.match(/<html>/g)) {
								param.notify.notifyText = e.responseText.replace(/{doi}/g, parameters.doi);
							}
							param.notify.notifyType = 'error';
							if ($('#articleHistory.show').length || $('#reportsHistory.show').length) {
								if (source == 'data-report-id') {
									$('#reportsHistory .assign-users').val($(currentClickEvent).find('[selected]').val());
								} else {
									$('#articleHistory .assign-users').val($(currentClickEvent).find('[selected]').val());
								}
							} else {
								$('[data-doi="' + parameters.doi + '"].assign-users').val($(currentClickEvent).find('[selected]').val());
							}
							if (e && e.responseText && typeof(e.responseText) == 'string' && e.responseText.match(/Force log-out/g)) {
								var xmlData = '<stage><name>' + currStageName + '</name><status>in-progress</status><job-logs><log><status type="system">logged-off</status></log></job-logs></stage>'
								param.notify.notifyType = 'warning';
								param.notify.doi = parameters.doi;
								param.notify.functionToCall = "updateXpath"
								param.parameter = {
									doi: parameters.doi,
									customerName: parameters.customerName,
									projectName: parameters.projectName,
									functionToCall: "updateXpath",
									data: xmlData,
									cardID: cardID,
									parentId: parentId,
									name: (name == " ") ? " " : name,
									sourceID: source
								}
								param.notify.confirmNotify = true;
							}
							eventHandler.common.functions.displayNotification(param);
						}
						if (successCount == blkUpdateCount) {
							bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi);
						}
						$('.la-container').css('display', 'none');
					}
				})
				function bulkNotification(openedArticles, blkSuccessDoi, blkFailureDoi) {
					if (openedArticles.length) {
						var openNotification = '';
						openedArticles.forEach(function (openDetail) {
							openNotification += openDetail.doi + ' is opened by ' + openDetail.name + '\n';
						})
						var param = notifyConfig.blkUpdate;
						// param.notity = JSON.parse(JSON.stringify(parameters.notify).replace(/{doi}/g, parameters.doi))
						param['notify']['notifyTitle'] = 'Assign User',
							param['notify']['notifyText'] = openNotification;
						param['notify']['notifyType'] = 'error';
						eventHandler.common.functions.displayNotification(param);
					}
					if (blkSuccessDoi.length) {
						param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText;
						if (name == " ") {
							param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.successText.replace(/assigned/g, 'unassigned');
						}
						param.notify.notifyType = 'success';
						param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, blkSuccessDoi.join(', '));
						eventHandler.common.functions.displayNotification(param);
					}
					if (blkFailureDoi.length) {
						param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText;
						if (name == " ") {
							param.notify.notifyText = notifyConfig.assignUser.bulkUpdate.errorText.replace(/assigning/g, 'unassigning');
						}
						param.notify.notifyText = param.notify.notifyText.replace(/{doi}/g, blkFailureDoi.join(', '));
						param.notify.notifyType = 'error';
						eventHandler.common.functions.displayNotification(param);
					}
				}
				function changeUserNameInHistory(cardID) {
					for (var stage = 0; stage < chartData[cardID]._source.stage.length; stage++) {
						if (chartData[cardID]._source.stage[stage].name == currStageName && chartData[cardID]._source.stage[stage].status == 'in-progress') {
							chartData[cardID]._source.stage[stage].assigned = {
								'to': (name == " ") ? "" : name,
								'on': currDate + ' ' + currTime,
								'by': userName + ', ' + userEmail,
								'reason': null
							};
						}
					}
				}
			},
			// To get the user name who opened the article.
			getOpenArticleUserName: function (parameter, parameters, bulkUpdate) {
				var openedUserName = '';
				openedUserName = eventHandler.components.actionitems.getCurrentArticleStatus(parameter);
				if (bulkUpdate && openedUserName && openedUserName != undefined && parameters.notify && parameters.notify.openedArticle) {
					var param = {};
					param.notify = JSON.parse(JSON.stringify(parameters.notify).replace(/{name}/g, openedUserName));
					param.notify['notifyText'] = param.notify.openedArticle;
					eventHandler.common.functions.displayNotification(param);
				}
				return openedUserName;
			},
			blkSaveWFDate: function (datePicker, type, cardID, source, replaceCard, from, id, parentId) {
				$('#blkupdate').modal('hide');
				$('[type="checkbox"]:checked').closest('tr.reportTable').each(function () {
					eventHandler.components.actionitems.saveWFDate(datePicker, type, $(this).attr('data-report-id'), source, replaceCard, from, $($(this).find('.Pickdate')).attr('data-doi'), $(this).attr('data-stage-name'), parentId);
				});
			},
			// To update bulk assign user in table report.
			blkUpdate: function (value, source, replaceCard) {
                $('#blkupdate').modal('hide');
                $('.la-container').css('display', '');
                var doiCount = $('[type="checkbox"]:checked').closest('tr.reportTable').length;
                lastCount = 0
                var openedArticle = [],
                    bulkArticles = [],
                    unAccessibleArticles = [],
                    currentName;
                setTimeout(function () {
                    $('[type="checkbox"]:checked').closest('tr.reportTable').each(function () {
                        var cardID = $(this).attr(source);
                        currentName = $(this);
                        var checkOpenArticleParameter = {
                            'customer': $("[" + source + "='" + cardID + "']").attr('data-customer'),
                            'project': $("[" + source + "='" + cardID + "']").attr('data-project'),
                            'doi': $("[" + source + "='" + cardID + "']").attr('data-doi'),
                            'xpath': xpath.blkUpdate
                        }
                        if (!$("[" + source + "='" + cardID + "'] [data-filter='assignUser'] select").length) {
                            unAccessibleArticles.push($("[" + source + "='" + cardID + "']").attr('data-doi'));
                        } else {
                            bulkArticles.push({ 'value': value, 'cardID': cardID, 'source': source, 'replaceCard': replaceCard, 'conform': 'conform', 'id': '#modalReport', 'clickEvent': currentName })
                        }
                    });
                    if (unAccessibleArticles.length > 0) {
                        var param = notifyConfig.blkUpdate;
                        param['notify']['notifyTitle'] = 'Access Denied',
                            param['notify']['notifyText'] = notifyConfig.blkUpdate.notify.unAccessible.replace(/{doi}/g, unAccessibleArticles.join(', '));
                        eventHandler.common.functions.displayNotification(param);
                    }
                    if (bulkArticles.length > 0) {
                        bulkArticles.forEach(function (articleDetails) {
                            eventHandler.components.actionitems.assignUser(articleDetails.value, articleDetails.cardID, articleDetails.source, articleDetails.replaceCard, articleDetails.conform, articleDetails.id, false, bulkArticles.length, articleDetails.clickEvent, articleDetails.parentId);
                        })
                    }
                }, 1);
            },
			// To reset the value in dropdown when we cancel the bulk update while assigning user
			cancelBlkUpdate: function (currentDoi) {
                var value = $('#modalReport').find('select[data-doi="' + currentDoi + '"] [selected]').val();
                if (value == "Un assigned") {
                    $('#modalReport').find('select[data-doi="' + currentDoi + '"] [selected]').val("Un assigned");
                }
                $('#modalReport').find('select[data-doi="' + currentDoi + '"].assign-users').val(value)
                $('#blkupdate').modal('hide');
            },
			getArticleStatus: function (btnNode, target) {
				if (!btnNode) return true;
				var jobCard = $(target).closest('.msCard');
				var doiString = $(target).closest('.msCard').attr('data-doi');
				var doiCustomer = $(target).closest('.msCard').attr('data-customer');
				var message = "";
				jQuery.ajax({
					type: "GET",
					url: "/api/articlenode?customerName=" + $(target).closest('.msCard').attr('data-customer') + "&projectName=" + $(target).closest('.msCard').attr('data-project') + "&xpath=//workflow&doi=" + doiString,
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if ($(data).find('workflow').attr('data-job-id') == undefined) {
							//cannot fetch status, so directly retry
							//reLoadNotification(doiString,' unknown','Cannot find current status');
							showArticleStatus('', '', '', doiString, {
								'status': 'unknown',
								'log': ['Cannot find current status']
							});
						} else {
							$('#notifyBox').show();
							$('#notifyBox .notifyHeaderContent').html('Getting job status please wait.');
							$('#notifyBox #reloadinfobody').html('');
							eventHandler.components.actionitems.getJobStatus(doiCustomer, $(data).find('workflow').attr('data-job-id'), '', '', '', doiString, {
								'process': 'showArticleStatus',
								'onfailure': 'showArticleStatus',
								'onsuccess': 'showArticleStatus'
							});
						}
					}
				})
			},
			getftpFilename: function (doi) {
				$('#notifyBox').show();
				$('#notifyBox .notifyHeaderContent').html('Reloading article...');
				$('#notifyBox #reloadinfobody').html('');
				$('#commonmodal').modal('hide');
				var jobCard = $('[data-doi="' + doi + '"].msCard')
				var parameters = {
					'customer': jobCard.attr('data-customer'),
					'project': jobCard.attr('data-project'),
					'doi': jobCard.attr('data-doi'),
					'xpath': 'data(//workflow/@ftp-file-name)'
				};
				eventHandler.common.functions.sendAPIRequest('getdatausingxpath', parameters, 'POST', function (res) {


					//$('#artRetry').find('#retryConform').attr('onclick','eventHandler.components.actionitems.reloadArticle("'+doiString+'","'+res.response+'")')
					//$('#artRetry').modal();
					eventHandler.components.actionitems.reloadArticle(doi, res.response);
				}, function (res) {
					$('#notifyBox').show();
					$('#notifyBox .notifyHeaderContent').html('Reload article failed.');
					$('#notifyBox #reloadinfobody').html('');
					//notify("Error getting workflow","error");
					console.log('Error getting workflow');
				});
			},
			reloadArticle: function (doiString, ftpFileName) {

				if (!doiString) return false;
				var jobCard = $('[data-doi="' + doiString + '"]');
				if (jobCard.length == 0) return false;
				//if (jobCard.find('[data-ftp-file-name]').length == 0) return false;
				//var ftpFileName = jobCard.find('[data-ftp-file-name]').attr('data-ftp-file-name');
				var articleType = jobCard.attr('data-article-type');
				var articleTitle = jobCard.find('[data-filter="Title"]').text();
				var params = {
					customer: jobCard.attr('data-customer'),
					journalID: jobCard.attr('data-project'),
					articleID: doiString,
					ftpFileName: ftpFileName,
					articleTitle: articleTitle,
					articleType: articleType,
					subject: 'field',
					specialInstructions: 'specialInstructions',
					priority: 'priority',
					mandatoryFields: 'mandatoryFields',
					hasMandatoryData: 'hasMandatoryData',
					forceLoad: 'true',
					status: {
						code: 200
					}
				}
				
				//if download type is aws status code should 202 added by vijayakumar on 18-03-2019
				if($('li[data-doi="' + doiString + '"]').length > 0){
					var downloadtye = $('li[data-doi="' + doiString + '"]').attr('data-download-type');
					if(downloadtye == "aws"){
						params['status']['code'] = "202"
					}
				}
				//$(btnNode).addClass('disabled');
				jQuery.ajax({
					type: "POST",
					url: "/workflow?currStage=addJob",
					data: params,
					success: function (data) {
						if (data && data.status && data.status.code && data.status.code == 200) {
							$('#artRetry').modal('hide');
							//on success, get the job id from workflow for getting the status
							setTimeout(function () {
								eventHandler.components.actionitems.getWFStatus(doiString);
							}, 200);
						}
					},
					error: function (err) {
						$('#notifyBox').show();
						$('#notifyBox .notifyHeaderContent').html('Reload article failed.');
						$('#notifyBox #reloadinfobody').html('');

						console.log(err);
					}
				});
			},
			getWFStatus: function (doiString) {
				var message = "";
				var jobCard = $('[data-doi="' + doiString + '"]');
				jQuery.ajax({
					type: "GET",
					url: "/api/articlenode?customerName=" + jobCard.attr('data-customer') + "&projectName=" + jobCard.find('.msProject').text() + "&xpath=//workflow&doi=" + doiString,
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if ($(data).find('workflow').attr('data-job-id') == undefined) {
							reLoadNotification(doiString, ' unknown', 'There is no job id for this article');
							/*setTimeout(function () {
								eventHandler.components.actionitems.getWFStatus(doiString);
							}, 200);*/
						} else {
							$('#notifyBox').show();
							$('#notifyBox .notifyHeaderContent').html('Downloading in progress... Please wait.');
							$('#notifyBox #reloadinfobody').html('');
							//$('#reloadinfobody').text("Downloading in progress... Please wait.");
							eventHandler.components.actionitems.getJobStatus($('[data-doi="' + doiString + '"]').attr('data-customer'), $(data).find('workflow').attr('data-job-id'), '', '', '', doiString);
						}
					}
				})
			},
			getJobStatus: function (customer, jobID, notificationID, userName, userRole, doi, callback) {
				//console.log('Job ID '+jobID);
				if (callback == undefined) callback = false;
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {
						"id": jobID,
						"userName": userName,
						"userRole": userRole,
						"doi": doi,
						"customer": customer
					},
					crossDomain: true,
					success: function (data) {
						if (data && data.status && data.status.code && data.status.message && data.status.message.status) {
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if (data.status.message.stage.current) {
								currStep = data.status.message.stage.current;
							}
							var loglen = data.status.message.log.length;
							var process = data.status.message.log[loglen - 1];
							$('#notifyBox').show();
							if(data.status.message.input && data.status.message.input.downloadType){
								var downtype = data.status.message.input.downloadType;
								var jobCard = $('li[data-doi="' + doi + '"]');
								if($(jobCard).length > 0){
									jobCard.attr('data-download-Type', downtype);
								}
							}
							if (/completed/i.test(status)) {
								currStep = data.status.message.stage.current;
								$('#notifyBox .notifyHeaderContent').text("Process completed");
								$('#notifyBox #reloadinfobody').html(process);
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//$('#upload_model').closeModal();
								/*if(callback && callback.onsuccess && typeof(window[callback.onsuccess]) == "function"){
									window[callback.onsuccess](customer, jobID, notificationID, doi, data.status.message);
								}*/
							} else if (/failed/i.test(status) || code != '200') {
								$('#notifyBox .notifyHeaderContent').html(currStep);
								$('#notifyBox #reloadinfobody').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								if (callback && callback.onfailure && typeof (window[callback.onfailure]) == "function") {
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							} else if (/no job found/i.test(status)) {
								$('#notifyBox .notifyHeaderContent').html(status);
								$('#notifyBox #reloadinfobody').html(process);
								//$.notify(process,{className: 'success',autoHide: false});
								//if (notificationID != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								if (callback && callback.onfailure && typeof (window[callback.onfailure]) == "function") {
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							} else {
								$('#notifyBox .notifyHeaderContent').html(data.status.message.displayStatus);
								//$('.notify').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;
								if (loglen > 1) {
									var process = data.status.message.log[loglen - 1];
									if (process != '') $('#notifyBox #reloadinfobody').html(process);
								}
								if (callback && callback.process && typeof (window[callback.process]) == "function") {
									window[callback.process](customer, jobID, notificationID, doi, data.status.message);
								} else {
									setTimeout(function () {
										eventHandler.components.actionitems.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback);
									}, 1000);
								}
							}
						} else {
							reLoadNotification(doi, ' unknown', 'There is no job status for this article');

						}
					},
					error: function (error) {
						reLoadNotification(doi, ' unknown', 'There is no job status for this article');

					}
				});
			},
			cardSort: function (ele, sortwith, highlightAttr) {
				$('.la-container').show();
				setTimeout(function () {
					if ($('#manuscriptsDataContent li:visible').length < 2) {
						$('.la-container').hide();
						$('#sort span').removeClass('active');
						$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$('#sort').hide();
						return;
					} else {
						$('#sort').show();
					}
					if ($(ele).find('.fa-caret-up').length > 0) {
						$($(ele).children('.fa-caret-up')).remove();
						$(ele).append('<i class="fa fa-caret-down fa-fw"></i>');
					} else {
						$($(ele).children('.fa-caret-down')).remove();
						$(ele).append('<i class="fa fa-caret-up fa-fw"></i>');
					}
					// Based on http://jsfiddle.net/fyh0nmdh/
					$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
					$('#manuscriptsDataContent li:visible').sort(sort_li).appendTo('#manuscriptsDataContent');
					function sort_li(a, b) {
						$(a).find(highlightAttr).addClass('highlightSort');
						$(b).find(highlightAttr).addClass('highlightSort');
						var a = $(a).data(sortwith);
						var b = $(b).data(sortwith);
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.string && sortmapping.sortType.string.indexOf(sortwith) > -1) {
							if (a == undefined) a = " ";
							if (b == undefined) b = " ";
							a = a.toString().toUpperCase();
							b = b.toString().toUpperCase();
						}
						if (sortmapping && sortmapping.sortType && sortmapping.sortType.number && sortmapping.sortType.number.indexOf(sortwith) > -1) {
							if (a == " " || a == undefined) a = 0;
							if (b == " " || b == undefined) b = 0;
						}
						if ($(ele).children().hasClass('fa-caret-up')) {
							return (b) < (a) ? 1 : -1;
						} else {
							return (b) > (a) ? 1 : -1;
						}
					}
					$($(ele).siblings().find('.fa')).remove();
					$('#sort span').removeClass('active');
					eventHandler.components.actionitems.hideRightPanel();
					$(ele).addClass('active');
					$('.la-container').hide();
				}, 0);
			},
			showRightPanel: function (param, targetNode) {
				//PNotify.removeAll();
				$('#manuscriptsData').removeClass('col-12');
				$('#manuscriptsData').addClass('col-8');
				$('#rightPanelContainer').addClass('col-4');
				$('#rightPanelContainer').find('[data-type]').addClass('hidden');
				if (param && param.target == 'pdfviewer'){
					$('#rightPanelContainer').find('[data-type*=" pdfviewer "]').removeClass('hidden');
					if (param && param.link){
						$('#rightPanelContainer').find('*[data-type=" pdfviewer "] object').attr('data', param.link);
						$('#pdfins').hide();
						$('#pdfview').show();
					}
				}else if (param && param.target){
					$('#rightPanelContainer').find('[data-type*=" ' + param.target + ' "]').removeClass('hidden');
					$('#rightPanelContainer').attr('data-c-rid', $(targetNode).attr('data-c-id')).attr('data-dc-doi', $(targetNode).attr('data-doi')).attr('data-r-uuid', $(targetNode).attr('data-uuid'));
					if (param.target.match(/manuscript|sectionCard/g)) {
						$('[data-type=" ' + param.target + ' "] object').attr('data', '').html('');
						var mainCardContent = $('#manuscriptsData *[data-c-id="' + $(targetNode).attr('data-c-id') + '"]').data('data');
						if (!mainCardContent || mainCardContent == undefined || !mainCardContent.file) {
							$('[data-type=" ' + param.target + ' "] object').hide();
							$('[data-type=" ' + param.target + ' "] .noPDF').show();
							return false;
						}
						var content = mainCardContent.file;
						if(!content || content == undefined){
							$('[data-type=" ' + param.target + ' "] object').hide();
							$('[data-type=" ' + param.target + ' "] .noPDF').show();
							return false;
						}
						var noPDF = false;
						if (content.constructor.toString().indexOf("Array") < 0) {
							content = [content];
						}
						for (path in content) {
							if (content[path]._attributes && content[path]._attributes.proofType == 'print') {
								if (content[path]._attributes.path != '') {
									$('[data-type=" ' + param.target + ' "] object').show();
									$('[data-type=" ' + param.target + ' "] object').attr('data', content[path]._attributes.path);
									$('[data-type=" ' + param.target + ' "] .noPDF').hide();
									$('[data-type=" ' + param.target + ' "] object').css('height', window.innerHeight - $('[data-type=" ' + param.target + ' "] object').offset().top - $('#footerContainer').height() - 15 + 'px');
									noPDF = false;
								} else {
									noPDF = true;
								}
								break;
							} else {
								noPDF = true;
							}
						}
						if (noPDF) {
							$('[data-type=" ' + param.target + ' "] object').hide();
							$('[data-type=" ' + param.target + ' "] .noPDF').show();
						}
					}
				}
				$('#sectionContent .innerCards').css('height', window.innerHeight - $('#sectionContent .innerCards').offset().top - $('#footerContainer').height() - 15 + 'px');
				$('#rightPanelContainer').show();
				if (param && param.doi){
					$('#pdfDoi').text(param.doi);
				}
			},
			disabledNotify: function (param) {
				eventHandler.components.actionitems.hideRightPanel();
				PNotify.removeAll();
				var notifyObj = {}
				notifyObj.notify = {};
				notifyObj.notify.notifyTitle = param.notifyHead;
				notifyObj.notify.notifyText = param.notifyText + ' (' + param.doi + ') ';
				notifyObj.notify.notifyType = 'info';
				eventHandler.common.functions.displayNotification(notifyObj);
			},
			hideRightPanel: function () {
				$('#manuscriptsData').removeClass('col-8');
				$('#manuscriptsData').addClass('col-12');
				$('#rightPanelContainer').hide();
				$('#pdfview').hide();
				$('#pdfins').show();
				$('#pdfDoi').text('');
				//$('#sort span').removeClass('active');
			},
			showProductionNotes: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var parameters = {};
				parameters.cardID = cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.cardID = param.cardID;
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				parameters.parentId = parentId;
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
				var url = "/api/articlenode?doi=#DOI#&projectName=#PROJECT#&customerName=#CUSTOMER#&xpath=//article/production-notes";
				var modalDiv = $(".comments-modal");
				if (modalDiv.length === 0) {
					$("body").append('<div class="modal" id="commentsmodal" style="display:none;"><div class="modal-dialog modal-dialog-centered comments-modal" role="document" style="width:400px;"><div class="modal-header"><h3 class="holdHeader">Production Notes for <span class="doiText"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-content"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field maincontent"><div class="textarea-wrapper"><div class="textarea-content" data-placeholder="Add a comment" contenteditable="true" style="height: 6.5em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="eventHandler.components.actionitems.postComments(this, \'\', \'\' )"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="eventHandler.components.actionitems.openFileUpload(this);" data-upload-type="Files" data-success="eventHandler.components.actionitems.attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
					modalDiv = $("#comments-modal");
				}
				$(modalDiv).find('.data-container').find('#comment-list').remove()
				$(modalDiv).find('.data-container').append('<ul id="comment-list" class="main"/>');
				var buttonInfo = "eventHandler.components.actionitems.postComments(''," + cardID + ",this, '', '"+parentId+"')";
				$(modalDiv).find('.sendNotes').attr("onclick", buttonInfo);
				$('.modal.comments-modal .notesHeader .doiText').text(parameters.doi);
				$('.modal.comments-modal').attr('data-customer', parameters.customer).attr('data-project', parameters.project).attr('data-doiid', parameters.doi).attr('data-role-type', userData.kuser.kuroles[parameters.customer]['role-type']).attr('data-access-level', userData.kuser.kuroles[parameters.customer]['access-level']);
				var notesURL = url.replace("#DOI#", parameters.doi).replace("#CUSTOMER#", parameters.customer).replace("#PROJECT#", parameters.project);
				notesURL = notesURL + '&bucketName=AIP';
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						$(modalDiv).find('.data-container').find('#comment-list').html('');
						if (msg && !msg.error) {
							var commentObj = $(msg);
							var notesDate = '', lastDate = '';
							var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
							var nl = $(commentObj).find('note').length - 1;
							$(commentObj).find('note').each(function (i, v) {
								var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="copy-note-clipboard  pull-right" data-channel="components" title="Copy" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'copyNoteClipboard\', \'param\':this}">&nbsp;<i class="fa fa-files-o" aria-hidden="true">&nbsp;</i></span><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + $(this).find('created').html() + '">' + $(this).find('created').html() + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
								commentDiv.find('*[data-class]').each(function () {
									$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
								});
								commentDiv.find('.jsonresp').each(function () {
									$(this).addClass('toggle');
								})
								// to change email-content to a html structure
								commentDiv.find('email-content').each(function () {
									$(this).find('> *').each(function () {
										var div = $('<div>');
										$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase());
										$(div).html($(this).html());
										$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
									});
									$(this).find(".mail-body p:contains('review_content/?key=')").each(function () {
										$(this).html($(this).html().replace(/(https?:\/\/[a-z\.]+\/review_content\/\?key=[a-z0-9]+)/, '<span class="email-link" data-href="$1">KRIYA-LINK</span>'))
									})
									var div = $('<div>');
									$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase()).addClass('toggle');
									$(div).html($(this).html());
									$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
								})
								if ($(this).find('fullname').html() == 'Automation') {
									$(this).attr('type', 'automation');
								}
								if ($(this)[0].hasAttribute('type')) {
									commentDiv.attr('data-type', $(this).attr('type'));
									commentDiv.find('.comment-reply').remove();
									$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="' + $(this).attr('type').toLocaleLowerCase() + '">' + $(this).attr('type').toLocaleUpperCase() + '</span>');
								}
								//it the current on is reply for some other, the add it next to it
								if ($(this).find('parent').length > 0 && $('li.comment[data-id="' + $(this).find('parent').html() + '"]').length > 0) {
									var parentID = $(this).find('parent').html();
									commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
									if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
									} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
									} else { //or aff next to parent
										$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
									}
								} else { //append to comment list
									notesDate = $(this).find('created').html().replace(/\s.*$/, '');
									if (lastDate == ''){
										lastDate = notesDate
									}
									if (notesDate != lastDate){
										var n = new Date(lastDate);
										var divider = '<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>';
										$(modalDiv).find('#comment-list').prepend(divider);
									}
									lastDate = notesDate;
									$(modalDiv).find('#comment-list').prepend(commentDiv);
								}
								if (i == nl){
									var n = new Date(notesDate);
									var divider = '<p style="text-align: center;margin: 2px 0px;color: #888;line-height:20px;"><span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span style="width: 40px;border-bottom: 1px solid #eee;display: inline-block;height: 12px;margin: 0px 5px;">&nbsp;</span></p>';
									$(modalDiv).find('#comment-list').prepend(divider);
								}
							});
							if(modalDiv){
								modalDiv.modal();
							}
						} else {
							if(modalDiv){
								modalDiv.modal();
							}
						}
						eventHandler.components.actionitems.showSplInstructions(param, targetNode);
					},
					error: function (xhr, errorType, exception) {
						if(modalDiv){
							modalDiv.modal();
						}
						eventHandler.components.actionitems.showSplInstructions(param, targetNode);
					}
				});
			},
			showSplInstructions: function (param, targetNode) {
				var msCard = $(targetNode).closest('.msCard');
				var parameters = {};
				parameters.cardID = cardID = $(targetNode).closest('.msCard').attr('data-id');
				parameters.cardID = param.cardID;
				var parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
				parameters.customer = $(""+parentId+" [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $(""+parentId+" [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $(""+parentId+" [data-id='" + cardID + "']").attr('data-doi');
				parameters.stageName = $(""+parentId+" [data-id='" + cardID + "']").find('.msStageName').text();
				var url = "/api/articlenode?doi=#DOI#&projectName=#PROJECT#&customerName=#CUSTOMER#&xpath=//article//article-meta/custom-meta-group/custom-meta[./meta-name[.='Special Instructions']]/meta-value";
				var modalDiv = $(".comments-modal");
				if (modalDiv.length === 0) {
					$("body").append('<div class="modal" id="commentsmodal" style="display:none;"><div class="modal-dialog modal-dialog-centered comments-modal" role="document" style="width:400px;"><div class="modal-header"><h3 class="holdHeader">Production Notes for <span class="doiText"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-content"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field maincontent"><div class="textarea-wrapper"><div class="textarea-content" data-placeholder="Add a comment" contenteditable="true" style="height: 6.5em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="eventHandler.components.actionitems.postComments(this)"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="eventHandler.components.actionitems.openFileUpload(this);" data-upload-type="Files" data-success="eventHandler.components.actionitems.attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
					modalDiv = $("#comments-modal");
				}
				var notesURL = url.replace("#DOI#", parameters.doi).replace("#CUSTOMER#", parameters.customer).replace("#PROJECT#", parameters.project);
				notesURL = notesURL + '&bucketName=AIP';
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
						if (msg && !msg.error) {
							var commentObj = $(msg);
							$(commentObj).each(function (key, comment) {
								var commentDiv = $('<li data-id="' + $(comment).html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">Special Instructions</span><span class="comment-time" data-original=""></span><span class="comment-label">Special Instructions</span></div><div class="wrapper"><div class="content"><pre>' + $(comment).html() + '</pre></div></div></div></li>');
								$(modalDiv).find('#comment-list').append(commentDiv);
							});
						}
					},
					error: function (xhr, errorType, exception) {}
				});
			},
			attachFileToNotes: function (status) {
				console.log(status)
				if (typeof (status.fileDetailsObj.dstKey) == "undefined") return false;
				var fileContent = '<span class="comment-file" contenteditable="false"><a href="/resources/' + status.fileDetailsObj.dstKey + '" target="_blank">' + status.fileDetailsObj.name + status.fileDetailsObj.extn + '</a></span>&nbsp;';
				$('.commenting-field').find('.textarea-content').append(fileContent);
				$('#upload_model').modal('hide');
			},
			postComments: function (comment, cardID, targetNode, source, parentId) {
				if (comment == '' && targetNode) {
					comment = $(targetNode).closest('.modal').find('.textarea-content').html();
				}
				// var contentDiv = $(saveBtn).closest('.modal').find('.textarea-content');
				if (comment == '') return false;
				//var d = new Date();
				//var currTime = d.toLocaleDateString() + " " + d.toLocaleTimeString();
				var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
				var commentJSON = {
					"content": comment,
					"fullname": $('.username').html(),
					"created": currTime
				}
				if ($('li.comment.replying').length > 0) {
					commentJSON.parent = $('li.comment.replying').attr('data-id');
				}
				if (!source || typeof(source) == 'object' || source == undefined) {
					source = 'data-id';
				}
				var noteType = $(targetNode).hasClass('sendNotes');
				$.ajax({
					type: 'POST',
					url: '/api/save_note_new',
					processData: false,
					contentType: 'application/json; charset=utf-8',
					data: JSON.stringify({
						"content": commentJSON,
						"doi": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-doi'),
						"customer": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-customer'),
						"journal": $(""+parentId+" [" + source + "='" + cardID + "']").attr('data-project'),
						"noteType": (noteType) ? '' : 'workflow',
						"notesCount": comment.length
					}),
					success: function (comment) {
						//console.log(comment);
						if(comment && comment.message){
							comment = comment.message;
							if(typeof(comment)==='object' && comment.length>0) comment = comment[0];
						}
						var commentDiv = $('<li data-id="' + comment.id + '" class="comment pulse"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + comment.fullname + '</span><span class="comment-time" data-original="' + comment.created + '">' + comment.created + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + $('#productioncommands').html() + '</div></div></div></li>');
						if (comment.parent && $('li.comment[data-id="' + comment.parent + '"]').length > 0) {
							var parentID = comment.parent;
							commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
							if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
								$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
							} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
								$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
							} else {
								$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
							}
							$('li.comment').removeClass('replying');
						} else {
							$('#comment-list').prepend(commentDiv);
						}
						setTimeout(function(){$(commentDiv).removeClass('pulse')},1000);
						$(commentDiv)[0].scrollIntoView()
						var jobCard = $('.jobCards[data-doiid="' + $('.modal.comments-modal').attr('data-doiid') + '"]');
						if (jobCard.find('.notes-notify').length > 0) {
							var notesNotify = 0;
							$('.comment:not(.reply)').each(function () {
								var parentID = $(this).attr('data-id');
								if ($('.comment.reply[data-parent-id="' + parentID + '"]').length == 0) {
									notesNotify++;
								}
							});
							if (notesNotify == 0) {
								jobCard.find('.notes-notify').text("");
							} else {
								jobCard.find('.notes-notify').text(notesNotify);
							}
						}
						$('.commenting-field.maincontent').find('.textarea-content').html('');
						// To replace the card for notification 
						if (noteType) {
							var param = {
								"doi": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-doi'),
								"customer": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-customer'),
								"journal": $("" + parentId + " [" + source + "='" + cardID + "']").attr('data-project'),
								"parentId": parentId,
								"cardID": cardID
							}
							eventHandler.dropdowns.article.getArticles(param);
						}
					}
				});
			},
			replyComment: function (replyBtn) {
				var commentBox = $(replyBtn).closest('li.comment');
				if (commentBox.hasClass('replying')) {
					commentBox.removeClass('replying')
				} else {
					$('li.comment').removeClass('replying');
					commentBox.addClass('replying');
					$('.commenting-field.maincontent').find('.textarea-content').focus();
				}
			},
			copyNoteClipboard: function (clipButton) {
				//Added by Anuraja for Notes-copy to clipboard function based on - http://jsfiddle.net/jdhenckel/km7prgv4/3
				var $temp = $("<div id='clipBoardHtml' style='display:none'>");
				$temp.html($(clipButton).closest('li.comment .comment-wrapper').find('.wrapper .content').html());
				$("body").append($temp);
				var str = document.getElementById('clipBoardHtml').innerHTML;
				function listener(e) {
					e.clipboardData.setData("text/html", str);
					e.clipboardData.setData("text/plain", str);
					e.preventDefault();
				}
				document.addEventListener("copy", listener);
				document.execCommand("copy");
				document.removeEventListener("copy", listener);
				$temp.remove();
				var noticationInfo = {};
				noticationInfo.notify = {};
				noticationInfo.notify.notifyTitle = 'Copy to clipboard';
				noticationInfo.notify.notifyText = 'Copied to clipboard';
				noticationInfo.notify.notifyType = 'info';
				PNotify.removeAll();
				eventHandler.common.functions.displayNotification(noticationInfo);
			},
			openFileUpload: function (e) {
				$('#upload_model').remove();
				var customer = $(e).closest('*[data-doiid]').attr('data-customer');
				var project = $(e).closest('*[data-doiid]').attr('data-project');
				var doi = $(e).closest('*[data-doiid]').attr('data-doiid');
				var uploadType = $(e).closest('span[data-message]').attr('data-upload-type');
				var successFunction = $(e).closest('span[data-message]').attr('data-success');
				if (typeof (doi) == "undefined") return false;
				$("body").append('<div id="upload_model" id="upload_modal" role="document" data-customer="' + project + '" data-project="' + project + '" data-doi="' + doi + '" data-upload-type="' + uploadType + '" data-success="' + successFunction + '" class="modal comments-modal-upload"><div class="modal-dialog modal-dialog-centered modal-upload"><div class="modal-content"><div class="modal-header"><h3 class="uploadHeader">Upload ' + uploadType + ' for ' + doi + '<span style="margin-left: 20px;font-size: 12px;background: #fafafa;padding: 3px 10px;color: #313131;border-radius: 6px;display:none;" class="notify"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div id="uploader" class="fileHolder"><div class="fileList" style="margin: 10px;text-align:center;"></div><div class="i-note"><p style="opacity: 0.6;">Drag and drop Files here to Upload</p><p><span class="btn btn-small btn-info" style="padding: 0px !important;"><input type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser" style="padding:0.375rem 0.75rem;">Or Select Files to Upload</span></span></p></div></div><div class="modal-caption hidden" data-type="Striking Image" contenteditable="true">metacontent</div><div class="modal-footer"><span class="btn btn-small btn-success upload_button save disabled highlight-background" data-channel="components" data-topic="actionitems" data-event="click" data-success="eventHandler.components.actionitems.attachFileToNotes" data-upload-type="Files" data-message="{\'funcToCall\': \'uploadFile\', \'param\':this}"><i class="fa fa-upload"> upload</i></span></div></div></div></div>');
				$('#upload_model').modal();
				tempFileList = {};
			},
			//upload file : gets upload path from server
			uploadFile: function (param, targetNode) {
				$(targetNode).addClass('disabled');
				var fileHolder = $('#uploader input')[0];
				var file = false;
				if (param && param.file != undefined) {
					file = param.file;
				}
				if (fileHolder.files.length > 0) {
					file = fileHolder.files[0];
				}
				if (tempFileList && Object.keys(tempFileList).length > 0) {
					file = tempFileList[0];
				}
				if (!file) return;
				$('#upload_model').attr('data-file-size', file.size);
				$('.notify').show();
				$('.notify').html('Uploading to server');
				var param = {}
				param.file = file;
				param.service = $('#upload_model').attr('data-upload-type').replace(/\s/g, '').toLocaleLowerCase();
				param.success = $('#upload_model').attr('data-success');
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {
						'filename': file.name
					},
					success: function (response) {
						if (response && response.upload_url) {
							var formData = new FormData();
							$.each(response.params, function (k, v) {
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.components.actionitems.uploadToServer(param);
						}
					},
					error: function (err) {
						if (param.error && typeof (param.error) == "function") {
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function (param, targetNode) {
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								$('.notify').html('Uploading to server ' + percentComplete);
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								$('.notify').html('Uploading to server ' + percentComplete);
							}
						}, false);
						return xhr;
					},
					success: function (res) {
						$('.notify').html('Converting for web view');
						kriya = {};
						kriya.config = {};
						kriya.config.content = {};
						kriya.config.content.customer = $('#upload_model').attr('data-customer');
						kriya.config.content.project = $('#upload_model').attr('data-project');
						kriya.config.content.doi = $('#upload_model').attr('data-doi');
						if (res && res.hasChildNodes()) {
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0) {
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
									srcBucket: uploadResult.find('Bucket,bucket').text(),
									srcKey: uploadResult.find('Key,key').text(),
									dstBucket: "kriya-resources-bucket",
									dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
									convert: 'false'
								}
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function (status) {
										if (param.success && typeof (window[param.success]) == "function") {
											window[param.success](status);
										} else if (typeof (eval(param.success)) == "function") {
											eval(param.success)(status);
										}
										//uploadDigest(status)
									},
									error: function (err) {
										if (param.error && typeof (param.error) == "function") {
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			}
		}
	},
	eventHandler.dropdowns = {
		customer: {
			getCustomer: function (param, targetNode) {
				$('.la-container').css('display', 'none');
				$('div.bucket-filter').hide();
				jQuery.ajax({
					type: "GET",
					url: "/api/customers",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						if (msg) {
							var customerHTML = ``;
							if (msg.customer.length == 1) $('#customerselect ul.dropdown-list').attr('data-multiple-customer', 'false');
							else $('#customerselect ul.dropdown-list').attr('data-multiple-customer', 'true');
							for (const customer of msg.customer) {
								if (customer.status == "live") {
									customerHTML += `<li class="nav-link" data-channel="dropdowns" data-topic="project" id="currentProject" data-event="click" data-message="{'funcToCall': 'getProject','param': {'customer': '` + customer.name + `'}}"    data-value="` + customer.name + `">` + customer.fullName + `</li>`
								}
							}
							$('#customerselect ul.dropdown-list').empty();
							$('#customerselect ul.dropdown-list').append(customerHTML);
						} else {
							return null;
						}
					},
					error: function (xhr, errorType, exception) {
						return null;
					}
				});

			},
			showCustomer: function (param, targetNode) {
				// toggle customers dropdown
				if ($('#customerselect ul.dropdown-list').hasClass('hide-dropdown')) {
					$('#customerselect ul.dropdown-list').removeClass('hide-dropdown');
				} else {
					$('#customerselect ul.dropdown-list').addClass('hide-dropdown');
				}
			},
			select: function (param, targetNode) {
				console.log(param)
			},
		},
		project: {
			getProject: function (param, targetNode) {
				// after selecting customer, list projects
				var customer = $(targetNode).val();
				if (customer == undefined || customer == '') {
					return;
				}
				jQuery.ajax({
					type: "GET",
					url: "/api/projects?customerName=" + customer,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						if (msg) {
							var projectHTML = ``;
							var projects;
							if (msg.project.constructor.name == 'Array') {
								projects = msg.project;
							} else {
								projects = [msg.project];
							}
							for (const project of projects) {
								projectHTML += '<option id="ajprojectlist" value="' + project.name + '">' + project.fullName + '</option>';
							}
							$('[id="ajproject"]').html('');
							$('[id="ajproject"]').append(projectHTML);
						} else {
							$('[id="ajproject"]').html('');
							return null;
						}
					},
					error: function (xhr, errorType, exception) {
						$('[id="ajproject"]').html('');
						return null;
					}
				});
			},
			listProject: function (param, targetNode) {
				$('.fs-wrap').removeClass('fs-open');
				$('.fs-dropdown').addClass('hidden');
				window.fSelect.active_el = null;
				window.fSelect.active_id = null;
				window.fSelect.last_choice = null;
				if (param.val) {
					var val = $(param.val).val();
					param.customer = val;
				}
				var customer = param.customer;
				if (customer == undefined || customer == '') {
					return;
				}
				if (typeof customer == 'string') {
					customer = customer.split(',');
				}
				var projectHTML = ``;
				var projectList = [];
				currentCustomers = [];
				customer.forEach(function (customerName) {
					currentCustomers.push(customerName);
					jQuery.ajax({
						type: "GET",
						url: "/api/projects?customerName=" + customerName,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (msg) {
							if (msg) {
								var projects;
								if (msg.project.constructor.name == 'Array') {
									projects = msg.project;
								} else {
									projects = [msg.project];
								}
								projectLists[customer] = [];
								for (const project of projects) {
									projectList.push(project);
									projectLists[customer].push(project.name);
									projectHTML += '<option id="dsprojectlist" value="' + project.name + '">' + project.fullName + '</option>';
								}
							} else {
								$('#ajproject').html('');
								return null;
							}
						},
						error: function (xhr, errorType, exception) {
							$('#ajproject').html('');
							return null;
						}
					});
				})
				$('.projectList').html('');
				$('.projectList').append(projectHTML);
				$('.projectList').fSelect();
				if (param.onsuccess) {
					eventHandler.highChart[param.onsuccess]({
						'customer': customer,
						'project': projectList
					});
				}
			},
			updateChart: function (param, from) {
				$('.showEmpty').hide();
				$('[data-type="chart"]').closest('.animated').show();
				$('#chartSelection .showBreadcrumb').css('display','inline-block');
				// if ($('#customerVal').text() == "Customer") {
				// 	return;
				// }
				$('[data-type="chart"]').css('background-color', 'white');
				$('[data-type="chart"]').html('<div class="chartLoader"></div>');
				$('#chartCustomer').hide();
				setTimeout(function () {
					$('#notifyprojectlist').find('[data-last-sele]').removeAttr('data-last-sele');
					$('#notifyprojectlist').hide();
					if (param.node) {
						$(param.node).closest('#chartSelection').find('#projectVal').html('Project');
					}
					var customer = [],
						project = [];
					$("#chartCustomer").children('.active').each(function (name) {
						customer.push($(this).text())
					});
					$("#chartProject").children('.active').each(function (name) {
						if ($(this).text() != 'All') {
							project.push($(this).attr('data-project'));
						}
					});
					if (param.onsuccess) {
						if (from == 'showManuscript' || $(from).text() == 'Yes' || $(from).closest('#dashboardContent').hasClass('active')) {
							eventHandler.highChart[param.onsuccess]({
								'customer': customer,
								'project': project
							});
							$('#dashboardContent #customerVal').text(customer[0]);
						}
						if (Object.keys(usersList).length == 0) {
							eventHandler.components.actionitems.getUserDetails(customer);
						}
						if (from != 'showManuscript') {
							$('.showBreadcrumb').css('display', 'inline-block');
							var roleAccess = (/^(production|copyeditor)$/.test(userData.kuser.kuroles[customer[0]]['role-type']));
							if (roleAccess) {
								eventHandler.mainMenu.toggle.showManuscript({
									'workflowStatus': 'in-progress',
									'urlToPost': 'getAssigned',
									'customer': customer[0],
									'custTag': '#filterCustomer',
									'project': 'project',
									'prjTag': '#filterProject',
									'user': 'user',
									'optionforassigning': true,
									'version': 'v2.0'
								}, undefined, 'updateChart');
							} else {
								eventHandler.mainMenu.toggle.showManuscript({
									'workflowStatus': 'in-progress OR completed',
									'project': 'project',
									'customer': customer[0],
									'excludeStage': 'inCardView',
									'optionforassigning': true,
									'version': 'v2.0'
								}, undefined, 'updateChart');
							}
						}
					}
					$('#chartCustomer').css('display', '');
					$('.la-container').css('display', 'none');
				}, 0);
			},
			listCustomer: function (param, targetNode) {
				var userObj = JSON.parse($("#userDetails").attr('data'));
				var clients = [],
					clientHTML = '',
					chartClient = '',
					projectList = '';
				Object.keys(userObj.kuser.kuroles).forEach(function (key, index) {
					clients.push(key);
				});
				if(!userData || !userData.kuser || !userData.kuser.kuemail){
					return null;
				}
				jQuery.ajax({
					type: "GET",
					url: "/api/userprojects?username=" + userObj.kuser.kuname.first + "&email=" + userData.kuser.kuemail,
					contentType: "application/xml; charset=utf-8",
					//async: false,
					dataType: "xml",
					success: function (msg) {
						if (msg) {
							$(msg).find('customer').each(function (customername) {
								var custName = $(this).attr('name');
								projectLists[custName] = {};
								projectLists[custName]['name'] = [];
								projectLists[custName]['value'] = [];
								projectLists[custName]['type'] = $(this).attr('type');
								$(this).find('name').each(function (projectname) {
									projectLists[custName]['name'].push($(this).text());
									projectLists[custName]['value'].push($(this).attr('fullName'));
								})
							});
							
							var i = 0;
							for (const client of clients) {
								var clientType = projectLists[client].type ? projectLists[client].type : 'journal';
								if (i == 0) {
									clientHTML += '<option selected="true" value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
									if (clients.length > 1) {
										chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
									} else {
										chartClient += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
										$('[data-type="chart"]').css('background-color', 'white');
										eventHandler.dropdowns.project.chartProject(client);
									}
								} else {
									clientHTML += '<option value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
									chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,   \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
								}
								i++;
							}
							$('.customerList').html('');
							$('#chartCustomer').html('');
							$('.customerList').append(clientHTML);
							$('#chartCustomer').append(chartClient);
							// $('#chartCustomer').fSelect({
							// 	placeholder: clients[0],
							// 	numDisplayed: 5,

							// 	searchText: 'Search',
							// 	showSearch: true
							// });
							if (clients.length == 1) {
								$('#sort [sort-option="Customer"]').remove();
								eventHandler.dropdowns.project.updateChart({
									'onsuccess': 'makeCharts'
								})
							} else {
								$('.showEmpty').show();
								$('[data-type="chart"]').closest('.animated').hide();
								var customer = {
									"customer": "Customer"
								};
								eventHandler.filters.populateNavigater(customer);
							}

						}
					},
					error: function (xhr, errorType, exception) {
						$('#ajproject').html('');
						return null;
					}
				});
			},
			// To create customer list for report tab
			listCustomer_new: function (param, targetNode) {
				var userObj = JSON.parse($("#userDetails").attr('data'));
				var clients = [],
					clientHTML = '',
					chartClient = '',
					projectList = '';
				Object.keys(userObj.kuser.kuroles).forEach(function (key, index) {
					clients.push(key);
				});
				if (!userData || !userData.kuser || !userData.kuser.kuemail) {
					return null;
				}
				jQuery.ajax({
					type: "GET",
					url: "/api/userprojects?customers=" + clients,
					contentType: "application/xml; charset=utf-8",
					//async: false,
					dataType: "json",
					success: function (msg) {
						if (Object.keys(msg).length) {
							projectLists = msg;
							var i = 0;
							for (const client of clients) {
								var clientType = projectLists[client].type ? projectLists[client].type : 'journal';
								if (i == 0) {
									clientHTML += '<option selected="true" value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
									if (clients.length > 1) {
										chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
									} else {
										chartClient += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,  \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
										$('[data-type="chart"]').css('background-color', 'white');
										eventHandler.dropdowns.project.chartProject(client);
									}
								} else {
									clientHTML += '<option value="' + client + '" data-type="' + clientType + '">' + client + '</option>';
									chartClient += '<div class="filter-list" onclick="eventHandler.dropdowns.project.chartProject(\'' + client + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" data-message="{\'funcToCall\': \'updateChart\', \'param\': {\'val\':\'\.customerList\' ,\'node\':this,   \'data\':\'val\', \'onsuccess\': \'makeCharts\',\'optionforassigning\':true}}" value=' + client + ' data-type="' + clientType + '">' + client + '</div>'
								}
								i++;
							}
							$('.customerList').html('');
							$('#chartCustomer').html('');
							$('.customerList').append(clientHTML);
							$('#chartCustomer').append(chartClient);
							// $('#chartCustomer').fSelect({
							// 	placeholder: clients[0],
							// 	numDisplayed: 5,

							// 	searchText: 'Search',
							// 	showSearch: true
							// });
							if (clients.length == 1) {
								$('#sort [sort-option="Customer"]').remove();
								eventHandler.dropdowns.project.updateChart({
									'onsuccess': 'makeCharts'
								})
							} else {
								$('.showEmpty').show();
								$('[data-type="chart"]').closest('.animated').hide();
								var customer = {
									"customer": "Customer"
								};
								eventHandler.filters.populateNavigater(customer);
							}

						}
					},
					error: function (xhr, errorType, exception) {
						$('#ajproject').html('');
						return null;
					}
				});
			},
			reportCustomerProject: function (customerName) {
				var customer = '';
				var activeCustomer = '';
				$('#reportSelection #reportCustomer').html('');
				if (projectLists) {
					Object.keys(userData.kuser.kuroles).forEach(function (custm) {
						if (customerName == custm) {
							customer += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
							activeCustomer = '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
							$('#reportCustomerVal').text(customerName);
						} else {
							customer += '<div class="filter-list" onclick="eventHandler.dropdowns.project.reportSelection(\'' + custm + '\',\'\', this)" value=' + custm + '>' + custm + '</div>'
						}
					})
					$('#reportSelection #reportCustomer').html(customer);
				}
				if (activeCustomer) {
					eventHandler.dropdowns.project.reportSelection(customerName, '', activeCustomer);
					$('#reportSelection').find('.reportshowBreadcrumb').css('display', 'inline-block');
				}

			},
			reportSelection: function (customer, projectName, targetNode, fromDate, toDate) {
				var project = '';
				if (targetNode) {
					$('#reportContent #date').show();
					if (!projectName) {
						$('#reportSelection #reportProject').html('');
						$(targetNode).siblings().removeClass('active');
						$(targetNode).parents('#reportSelection').find('#reportProjectVal').text('Project');
						$(targetNode).parent().siblings('#reportCustomerVal').text(customer);
						$(targetNode).addClass('active');
						$(targetNode).closest('#reportSelection').find('.reportshowBreadcrumb').css('display', 'inline-block');
						if (customer && projectLists && projectLists[customer] && projectLists[customer].name && projectLists[customer].value) {
							project += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + customer + '\',\'All\', this)" data-channel="dropdowns" data-topic="project" data-event="click" value=\'All\'>All</div>'
							projectLists[customer].value.forEach(function (proj, index) {
								project += '<div class="filter-list active" onclick="eventHandler.dropdowns.project.reportSelection(\'' + customer + '\',\'' + projectLists[customer].value[index] + '\', this)" data-channel="dropdowns" data-topic="project" data-event="click" value=' + projectLists[customer].value[index] + '>' + projectLists[customer].name[index] + '</div>'
							})
						}
						$('#reportSelection #reportProject').html(project);
					} else {
						if (projectName == 'All') {
							$(targetNode).siblings().addClass('active');
							projectName = '*';
							$(targetNode).parent().siblings('#reportProjectVal').text('All');
						} else {
							$(targetNode).siblings().removeClass('active');
							$(targetNode).parent().siblings('#reportProjectVal').text(projectName);
						}
						$(targetNode).addClass('active');
					}
				}
				$('.showEmpty').addClass('hidden');
				if (fromDate == undefined || fromDate == '') {
					fromDate = moment().format('YYYY-MM-DD');
					$("#fromDatePicker").val(moment(fromDate).format('D MMM, YY'));
					$("#fromDatePicker").data('datepicker').selectDate(new Date());
				}
				if (toDate == undefined || toDate == '') {
					toDate = moment().format('YYYY-MM-DD');
					$("#toDatePicker").val(moment(toDate).format('D MMM, YY'));
					$("#toDatePicker").data('datepicker').selectDate(new Date());
				}
				    eventHandler.tableReportsSummary.getCustomerData({ 'customer': customer, 'project': projectName, 'roleType': userData.kuser.kuroles[customer]['role-type'], 'accessLevel': userData.kuser.kuroles[customer]['access-level'], 'fromDate': fromDate, 'toDate': toDate });
			},
			// To show date range picker in report tab
			reportDateRangePicker: function (targetNode) {
				var sDate, eDate;
				$("#fromDatePicker,#toDatePicker").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					dateFormat: 'd M, yy',
					language: {
						days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
						daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						daysMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
						months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
						monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						dateFormat: 'dd.mm.yyyy'
					},
				})
				if (targetNode) {
					sDate = $(targetNode).siblings('input#fromDatePicker').val();
					eDate = $(targetNode).siblings('input#toDatePicker').val();
					sDate = moment(sDate).format('YYYY-MM-DD');
					eDate = moment(eDate).format('YYYY-MM-DD');
					if (sDate > eDate || sDate == undefined || eDate == undefined || sDate == 'Invalid date' || eDate == 'Invalid date') {
						var notifyParam = {
							'notify': {
								'notifyTitle': 'Invalid Date Range',
								'notifyText': 'Please Select Valid Date Range',
								'notifyType': 'error'
							}
						}
						eventHandler.common.functions.displayNotification(notifyParam);
					} else {
						var customer = $('#reportCustomerVal').text();
						var project = ''
						if ($('#reportProjectVal').text().match(/Project|All/g)) {
							project = '*';
						} else {
							project = $('#reportProjectVal').text();
						}
						eventHandler.dropdowns.project.reportSelection(customer, project, '', moment(sDate).format('YYYY-MM-DD'), moment(eDate).format('YYYY-MM-DD'))
					}
				} else {
                    $("#fromDatePicker").data('datepicker').selectDate(new Date())
                    $("#toDatePicker").data('datepicker').selectDate(new Date())
                }
			},
			chartProject: function (param, targetNode) {
				//$('#dashboardContent #customerVal').text(param);
				if (targetNode) {
					$(targetNode).addClass('active');
					$($(targetNode).siblings()).removeClass('active');
				}
				var projectList = '';
				chartProject = [];
				chartProject = projectLists[param];
				$('#chartProject').html('');
				projectList += '<div class="filter-list active" id="allproject" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this, \'attributeToCheck\':\'data-project\'})">All</div>';
				// for (var proj in chartProject) {
				// 	projectList += '<div class="filter-list active" data-project="' + chartProject[proj] + '" onclick="eventHandler.filters.projectFilters({\'project\':\'' + chartProject[proj] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})">' + chartProject[proj] + '</div>';
				// }
				if(chartProject && chartProject['name'] && chartProject['value']){
					chartProject['name'].forEach( function(project, count){
						projectList += '<div class="filter-list active" data-project="' + chartProject['value'][count] + '" onclick="eventHandler.filters.projectFilters({\'project\':\'' + chartProject['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})">' + project + '</div>';
					})
				}
				$('#chartProject').append(projectList);
			},
			toggleProject: function (param, targetNode) {
				// toggle project dropdown
				if ($('#projectselect ul.dropdown-list').hasClass('hide-dropdown')) {
					$('#projectselect ul.dropdown-list').removeClass('hide-dropdown');
				} else {
					$('#projectselect ul.dropdown-list').addClass('hide-dropdown');
				}
			},
			select: function (param, targetNode) {
				console.log(param)
			}
		},
		stage: {
			toggleStageFilter: function (param, targetNode) {
				// toggle filter dropdown - stageowner
				var ulNode = $(targetNode).siblings('div.dropdown').find('ul');
				if (ulNode.hasClass('hide-dropdown')) {
					ulNode.removeClass('hide-dropdown');
				} else {
					ulNode.addClass('hide-dropdown');
				}
			},
			plotNavFilter: function (param, targetNode) {
				var navHTML = ``;

				// code block to add stage owners article count dropdown
				for (const stageOwner in stageOwners) {
					if (stageOwners.hasOwnProperty(stageOwner)) {
						var navListTotal = 0;
						var navListHTML = ``;

						for (const owner in stageOwners[stageOwner]) {
							if (stageOwners[stageOwner].hasOwnProperty(owner) && stageOwners[stageOwner][owner]['article-count']) {
								navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${owner}','filterBy':'stageOwners'}}"    data-value="${owner}">${owner}:${stageOwners[stageOwner][owner]['article-count']}</li>`;
								navListTotal += stageOwners[stageOwner][owner]['article-count'];
							}
						}

						navHTML += `<span class="stage-filter">
							<span data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >${stageOwner} (${navListTotal}) <i class="fa fa-caret-down" aria-hidden="true"></i>
							</span>
							<div class="dropdown">
								<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
							</div>
						</span>`;

					}
				}


				// code block to add article type count dropdown
				{
					var navListTotal = 0;
					var navListHTML = ``;
					for (const articleType in articleTypesCount['article-type']) {
						if (articleTypesCount['article-type'].hasOwnProperty(articleType)) {
							navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="articleType" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${articleType}','filterBy':'articleTypesCount'}}"  >${articleType}:${articleTypesCount['article-type'][articleType]}</li>`;
							navListTotal += articleTypesCount['article-type'][articleType];
						}
					}

					navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Article Type (${navListTotal}) <i class="fa fa-caret-down" aria-hidden="true"></i>
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
						</div>
					</span>`;
				}

				$('div.navFilter').empty();
				$('div.navFilter').html(navHTML);
			},
			filterArticle: function (param, targetNode) {
				// filter article based on stage owners and article type
				var filteredArticleIDs = filterData[param.filterOwner][param.filter]['article-data-id'];
				$('div.msCard.card[data-id]').hide();
				$('[data-id=card-' + filteredArticleIDs.join('],[data-id=card-') + ']').show();
				// console.log();
			},
			smartFilterArticle: function (param, targetNode) {
				// filter article based on stage owners and article type
				var filteredArticleIDs = filterData[param.filterOwner][param.filter]['article-data-id'];
				$('div.msCard.card[data-id]').hide();
				$('[data-id=card-' + filteredArticleIDs.join('],[data-id=card-') + ']').show();
				// console.log();
			}
		},
		article: {
			showArticles: function (param, targetNode, recursive) {
				//it should not call directly , needs to call by showManuscript
				// after selecting project, list articles
				$('.save-notice').addClass('hide');
				cardData = [];
				if (param) {
					if ($(targetNode).parent().attr('id') == 'filterStatus') {
						$(targetNode).parent().siblings().html($(targetNode).text());
						$(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					} else if ($(targetNode).attr('id') == 'refresh') {
						// $(targetNode).closest('.newStatsRow').find('#statusVal').html($(targetNode).closest('.newStatsRow').find('#filterStatus .active').text())
						// $(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					} else {
						// $(targetNode).closest('.newStatsRow').find('#statusVal').html($(targetNode).closest('.newStatsRow').find('#filterStatus .active').text())
						$(targetNode).closest('.newStatsRow').find('#statusVal').html('WIP')
						$(targetNode).closest('.newStatsRow').find('#projectVal').html('Project');
					}
					if ($(targetNode).parent().attr('id') == 'filterCustomer') {
						$('#filterStatus').children().removeClass('active');
						$('#filterStatus').children().first().addClass('active');
					}
					if (!param.from || param.from == undefined) {
						param.from = 0;
					}
					if (!param.size || param.size == undefined) {
						param.size = 500;
					}
					if (param.excludeStage) {
						param.excludeStageArticles = excludeStage[param.excludeStage].join(' OR ');
					}
					param.user = 'userName';
					param.stageName = 'stageName';
					if (!param.customer || param.customer == 'first' || param.customer == 'Customer') {
						try {
							param.customer = Object.keys(JSON.parse($('#userDetails').attr('data')).kuser.kuroles)[0];
						} catch (e) {
							console.log(e);
						}
					}
					if (!param.customer || (param.customer == 'selected' && param.custTag)) {
						try {
							var cs = [];
							$(param.custTag + ' .active').each(function (s) {
								cs.push($(this).text());
							});
							param.customer = cs.join(' OR ');
						} catch (e) {
							console.log(e);
						}
					}
					if (param.customer == 'customer' || selectedCustomer != param.customer) {
						eventHandler.components.actionitems.getUserDetails(param.customer);
					}
					if (param.optionforassigning) {
						eventHandler.dropdowns.article.showAssignOptions(param, targetNode);
					}
					if (!param.project) {
						param.project = 'project';
					}
					$(targetNode).addClass('active');
					$(targetNode).siblings().removeClass('active');
					eventHandler.dropdowns.project.toggleProject(param, targetNode);
					if (recursive == undefined) {
						data = {};
					}
					if (!param.customer.match(/customer/i)) {
						if (userData.kuser.kuroles[param.customer] && userData.kuser.kuroles[param.customer]['role-type'] && userData.kuser.kuroles[param.customer]['access-level']) {
							var roleType = userData.kuser.kuroles[param.customer]['role-type'];
							var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
							// For Copyeditors login
							if (checkPageCountWhileAssigning && checkPageCountWhileAssigning[roleType] && checkPageCountWhileAssigning[roleType].access && checkPageCountWhileAssigning[roleType].access.indexOf(accessLevel) > -1) {
								param['toPageCount'] = userData.kuser.kuroles[param.customer]['page-limit'] - userData.kuser.kuroles[param.customer]['assigned-pages'];
								param['fromPageCount'] = 0;
							}
						}
					}
					$('#projectselect span[data-label="true"]>span').text($(targetNode).text());
					$('div.bucket-filter').show();
				}
				if (param.customer && param.customer != 'customer') {
					selectedCustomer = param.customer;
				}
				//To raise query for first time and if we select different customer.
				if (manuScriptCardData.length == 0 || manuScriptCardData.length == undefined || param.stage == 'Support' || recursive == 1) {
					$.ajax({
						type: "POST",
						url: '/api/getarticlelist',
						data: param,
						dataType: 'json',
						//async: false,
						success: function (respData) {
							if (!respData || respData.length == 0) {
								$('#manuscriptsDataContent').html('');
								$('#manuscriptContent .filterOptions').hide();
								$('div.navFilter').empty();
								if (selectedCustomer) {
									param = {};
									param.customer = selectedCustomer;
									eventHandler.filters.populateNavigater(param);
								}
								$('#msYtlCount, #msCount').text('0');
								$('.la-container').css('display', 'none');
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								return;
							}
							var oldTotal = parseInt($('#msYtlCount').text());
							var currCount = respData.hits.length;
							if (oldTotal == NaN || oldTotal == undefined || isNaN(oldTotal) || oldTotal == 0) {
								oldTotal = 0;
								data = {};
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['others'] = {};
							}
							if (param.stage == 'Support') {
								respData.hits = respData.hits.sort(function (a, b) {
									a = a._source.stageName.toUpperCase();
									b = b._source.stageName.toUpperCase();
									if ((b) < (a)) {
										return 1;
									} else {
										return -1;
									}
								})
								Array.prototype.push.apply(supportData, respData.hits);
							} else {
								Array.prototype.push.apply(manuScriptCardData, respData.hits);
							}
							data.dconfig = dashboardConfig;
							var totalMsCount = respData.total;
							data.info = respData.hits;
							data.disableFasttrack = disableFasttrack;
							data.removeEditIcon = false;
							if (param.stage != 'Support' && roleType.match(/copyeditor/g) && accessLevel.match(/vendor/g)) {
								// To disable the action buttons in card for CE vendor
								data.ceVendor = true;
								if (param.urlToPost.match(/getUnassigned/g)) {
									data.removeEditIcon = true;
								}
							}
							data.userDet = JSON.parse($('#userDetails').attr('data'));
							data.count = oldTotal;
							if (param && param.stage != 'Support') {
								var userRole = userData.kuser.kuroles[param.customer]['role-type'];
								var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
								// For Copyeditor -> Vendor In unassigned Bucket the first card should be pickable and other cards are to be in disable mode.
								if (param && param.urlToPost && param.urlToPost.match(/getUnassigned/g) && cardDisable && cardDisable[userRole] && cardDisable[userRole][userAccess] && cardDisable[userRole][userAccess]['addClassName']) {
									data.disable = cardDisable[userRole][userAccess]['addClassName'];
								}
							}
							manuscriptsData = $('#manuscriptsDataContent');
							var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
							manuscriptsData.append(pagefn(data));
							$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
							$('.msCount.loading.hidden').removeClass('hidden');
							console.log(currCount, oldTotal, totalMsCount);
							if ((currCount + oldTotal) < totalMsCount) {
								$('.la-container').css('display', 'none');
								$('#msYtlCount').text(data.info.length + oldTotal);
								$('#msCount').text(totalMsCount);
								$('#msYtlCount').removeClass('hidden');
								param.from = currCount + oldTotal;
								setTimeout(function () {
									eventHandler.dropdowns.article.showArticles(param, targetNode, 1);
								}, 0)
							} else {
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								$('#msCount').text(totalMsCount);
								param.filterObj = data.filterObj;
								if (!param.customer || param.customer == 'first') {
									eventHandler.filters.populateNavigater(param);
								} else {
									eventHandler.filters.populateNavigater(param);
								}
								eventHandler.filters.populateFilters(data.filterObj, param.stage);
								// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
								if (!param.customer.match(/customer|first/gi)) {
									var userRole = userData.kuser.kuroles[param.customer]['role-type'];
									var userAccess = userData.kuser.kuroles[param.customer]['access-level'];
									if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
										eventHandler.filters.populateSort();
									}
								}
								if (param.refreshedProject) {
									eventHandler.filters.projectFilters({
										'project': $(targetNode).closest('#manuscriptContent').find('#projectVal').text(),
										'node': targetNode,
										'attributeToCheck': 'data-project'
									})
									$('#msCount').text(param.filterObj.project[param.refreshedProject.replace(/[([0-9)]/g, '')]);
								}
							}
							$('.la-container').css('display', 'none');
							console.log(filterData);
						},
						error: function (respData) {
							$('.la-container').css('display', 'none');
							console.log(respData);
						}
					});
				} else {
					//Do not reload the card view for second time
					if (param.stage != 'Support') {
						cardData = manuScriptCardData;
					}
					param = {};
					$('#manuscriptContent #projectVal').text('Project');
					var totalMsCount = cardData.length;
					data.info = cardData;
					data.disableFasttrack = disableFasttrack;
					data.filterObj = {};
					data.filterObj['customer'] = {};
					data.filterObj['project'] = {};
					data.filterObj['articleType'] = {};
					data.filterObj['typesetter'] = {};
					data.filterObj['publisher'] = {};
					data.filterObj['author'] = {};
					data.filterObj['editor'] = {};
					data.filterObj['copyeditor'] = {};
					data.filterObj['contentloading'] = {};
					data.filterObj['support'] = {};
					data.filterObj['supportstage'] = {};
					data.filterObj['others'] = {};
					manuscriptsData = $('#manuscriptsDataContent');
					var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
					manuscriptsData.append(pagefn(data));
					$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
					$('.msCount.loading.hidden').removeClass('hidden');
					$('.msCount.loading').removeClass('loading').addClass('loaded');
					$('#msCount').text(totalMsCount);
					param.filterObj = data.filterObj;
					if (selectedCustomer) {
						eventHandler.components.actionitems.getUserDetails(selectedCustomer);
						param.customer = selectedCustomer;
					}
					if (!param.customer || param.customer == 'first') {
						eventHandler.filters.populateNavigater(param);
					} else {
						eventHandler.filters.populateNavigater(param);
					}
					eventHandler.filters.populateFilters(data.filterObj);
					//eventHandler.filters.populateSort();
					var contNode = $('#' + $(targetNode).attr('data-href'));
					contNode.addClass('active');
					$('.la-container').css('display', 'none');
				}
				if (param.stage == 'Support') {
					cardData = supportData;
				} else {
					cardData = manuScriptCardData;
				}
			},
			showSupportArticles: function () {
				Object.keys(userData.kuser.kuroles).forEach(function (role) {
					var supportRole = userData.kuser.kuroles[role]['role-type'];
					var supportAccess = userData.kuser.kuroles[role]['access-level'];
					if (showSupportTab[supportRole] && showSupportTab[supportRole].access && (showSupportTab[supportRole].access.indexOf(supportAccess) > -1 || showSupportTab[supportRole].access == 'All')) {
						$("#support").html('<a class="nav-link" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showSupport\', \'param\': {\'workflowStatus\':\'in-progress\' ,\'project\':\'project\', \'customer\': \'customer\',\'urlToPost\':\'getSupportStage\', \'stage\': \'Support\',\'showStages\':\'(Support) OR (addJob) OR (File download) OR (Convert Files)\', \'version\':\'v2.0\'}}" data-href="manuscriptContent"><i class="fa fa-ticket"/> Support</a>');
					}
				})
			},
			showAssignOptions: function (param, targetnode) {
				if (param.customer) {
					var roleType = userData.kuser.kuroles[param.customer]['role-type'];
					var accessLevel = userData.kuser.kuroles[param.customer]['access-level'];
					if (userAuthorization && userAuthorization[roleType] && userAuthorization[roleType][accessLevel] && (userAuthorization[roleType][accessLevel]['toShow'].length || userAuthorization[roleType][accessLevel]['toHide'].length)) {
						if (targetnode) {
							$(targetnode).closest('#customer').parent().siblings().find('#statusVal').siblings('#filterStatus').children().first().attr('data-message', "{'funcToCall': 'showManuscript', 'param' : { 'workflowStatus':'in-progress', 'urlToPost':'getAssigned', 'customer':'selected','custTag': '#filterCustomer', 'prjTag':'#filterProject', 'user':'user','optionforassigning':true ,'version': 'v2.0'}}")
						} else {
							$('#manuscriptContent #filterStatus').children().first().attr('data-message', "{'funcToCall': 'showManuscript', 'param' : { 'workflowStatus':'in-progress', 'urlToPost':'getAssigned', 'customer':'selected','custTag': '#filterCustomer', 'prjTag':'#filterProject', 'user':'user','optionforassigning':true, 'version': 'v2.0' }}")
						}
						if ($('#manuscriptContent #statusVal').text().trim() == "WIP") {
							$('.assignAccess').show();
						} else {
							$('.assignAccess').hide();
						}
						userAuthorization[roleType][accessLevel]['toShow'].forEach(function (id) {
							$(id).show();
						})
						userAuthorization[roleType][accessLevel]['toHide'].forEach(function (id) {
							$(id).hide();
						})
					} else {
						if (targetnode) {
							$(targetnode).closest('#customer').parent().siblings().find('#statusVal').siblings('#filterStatus').children().first().attr('data-message', '{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'selected\', \'project\': \'project\', \'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'excludeStage\': \'inCardView\', \'version\': \'v2.0\'}}')
						} else {
							$('#manuscriptContent #filterStatus').children().first().attr('data-message', '{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'selected\', \'project\': \'project\', \'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'excludeStage\': \'inCardView\', \'version\': \'v2.0\'}}')
						}
						if ($('#manuscriptContent #statusVal').text().trim() == "WIP") {
							$('.assignAccess').hide();
						} else {
							$('.assignAccess').show();
						}

					}
				}
			},
			showUserName: function () {
				$('#userName').attr('title', userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last);
				$('#userName').html(userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last);
			},
			/**
			 * To get searched articles in search bar.
			 */
			getSearchedArticles: function (param) {
				if (param == undefined || param.doi == undefined) {
					console.log('Empty parameters');
					return
				}
				$('#searchBox').val(param.doi);
				$('#searchdoi').hide();
				$('.la-container').css('display', '');
				$('#manuscriptContent .supportcard').hide();
				$('#manuscriptContent .filterOptions').hide();
				//To remove active class from header (Card view, Support)
				$('header .nav-items-journals').children().find('.active').removeClass('active');
				$('#sort').hide();
				$('#searchResult').show();
				if (!param.from) {
					param.from = 0;
				}
				if (!param.size) {
					param.size = 5;
				}
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						if (!respData || respData.hits.length == 0) {
							$('.la-container').css('display', 'none');
							$('#manuscriptsDataContent').html('NO RESULTS FOUND');
							$('#msCount').text('0');
							return;
						}
						$('.showEmpty').addClass('hidden');
						var data = {};
						data.dconfig = dashboardConfig;
						// data.info = [];
						data.filterObj = {};
						data.filterObj['customer'] = {};
						data.filterObj['project'] = {};
						data.filterObj['articleType'] = {};
						data.filterObj['typesetter'] = {};
						data.filterObj['publisher'] = {};
						data.filterObj['author'] = {};
						data.filterObj['editor'] = {};
						data.filterObj['copyeditor'] = {};
						data.filterObj['support'] = {};
						data.filterObj['supportstage'] = {};
						data.filterObj['contentloading'] = {};
						data.filterObj['others'] = {};
						data.disableFasttrack = disableFasttrack;
						data.userDet = JSON.parse($('#userDetails').attr('data'));
						$('#manuscriptsDataContent').html('');
						data.count = 0;
						cardData = [];
						cardData = respData.hits;
						data.info = cardData;
						manuscriptsData = $('#manuscriptsDataContent');
						var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
						manuscriptsData.append(pagefn(data));
						$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
						if(respData.hits && respData.hits[0] && respData.hits[0]._source && respData.hits[0]._source.customer){
							eventHandler.components.actionitems.getUserDetails(respData.hits[0]._source.customer);
						}
						$('.msCount.loading.hidden').removeClass('hidden');
						$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
						$('.msCount.loading').removeClass('loading').addClass('loaded');
						$('#msCount').text(respData.hits.length);
						$('.la-container').css('display', 'none');
					},
					error: function (respData) {
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				})
			},
			getArticles: function (param) {
				if (param == undefined || param.doi == undefined || param.cardID == undefined || param.parentId==undefined) {
					console.log('Empty parameters');
					return
				}
				if (param.index == undefined) {
					param.index = 0;
				}
				$.ajax({
					type: "GET",
					url: '/api/getarticle',
					data: {
						'doi': param.doi
					},
					dataType: 'json',
					success: function (respData) {
						var data = {};
						data.dconfig = dashboardConfig;
						data.info = [];
						data.filterObj = {};
						data.filterObj['customer'] = {};
						data.filterObj['project'] = {};
						data.filterObj['articleType'] = {};
						data.filterObj['typesetter'] = {};
						data.filterObj['publisher'] = {};
						data.filterObj['author'] = {};
						data.filterObj['editor'] = {};
						data.filterObj['copyeditor'] = {};
						data.filterObj['support'] = {};
						data.filterObj['supportstage'] = {};
						data.filterObj['contentloading'] = {};
						data.filterObj['others'] = {};
						data.userDet = JSON.parse($('#userDetails').attr('data'));
						data.count = parseInt(param.cardID);
						data.dashboardview = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-dashboardview');
						data.storeVariable = $(""+param.parentId+" [data-id='" + param.cardID + "']").attr('data-store-variable');
						data.disableFasttrack = disableFasttrack;
						var customer = $("" + param.parentId + " [data-id='" + param.cardID + "']").attr('data-customer');
						var userRole = userData.kuser.kuroles[customer]['role-type'];
						var userAccess = userData.kuser.kuroles[customer]['access-level'];
						// To disable the action buttons in card for CE vendor
						if (userRole.match(/copyeditor/g) && userAccess.match(/vendor/g)) {
							data.ceVendor = true;
							if (param.unassign) {
								data.removeEditIcon = true;
							}
						}
						data.customerType = projectLists[customer].type;
						data.info.push(respData.body);
						if (window[data.storeVariable] && window[data.storeVariable][param.cardID] && window[data.storeVariable][param.cardID]._source && respData.body._source && (JSON.stringify(window[data.storeVariable][param.cardID]._source) === JSON.stringify(respData.body._source)) && param.index != 6) {
							param.index++;
							setTimeout(function () {
								eventHandler.dropdowns.article.getArticles(param);
							}, 500);
							console.log(param.index);
						} else {
							window[data.storeVariable][param.cardID] = respData.body;
							if (data.customerType == 'book') {
								var bookChapter = $("" + param.parentId + " [data-id='" + param.cardID + "']");
								var bookChapterMetaData = bookChapter.data();
							}
							var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
							var content = pagefn(data);
							$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
							$("" + param.parentId + " [data-id='" + param.cardID + "']").replaceWith(content);
							if (data.customerType == 'book') {
								var bookChapterData = bookChapterMetaData.data._attributes;
								bookChapter = $("" + param.parentId + " [data-id='" + param.cardID + "']");
								bookChapter.find('.pageRange').attr('data-fpage', bookChapterData.fpage)
								bookChapter.find('.pageRange .rangeStart').text(bookChapterData.fpage)
								bookChapter.find('.pageRange').attr('data-lpage', bookChapterData.lpage)
								bookChapter.find('.pageRange .rangeEnd').text(bookChapterData.lpage)
								bookChapter.find('.msChNo').text(bookChapterData.chapterNumber)
								bookChapter.attr({
									'data-grouptype': bookChapterMetaData.grouptype,
									'data-uuid': bookChapterMetaData.uuid,
									'data-c-id': bookChapterMetaData.cId,
									'data-on-reorder': bookChapterMetaData.onReorder
								})
								bookChapter.data('data', bookChapterMetaData.data);
							}
						}
					},
					error: function (respData) {
						// alert('something went wrong while fetching data');
						$('.la-container').css('display', 'none');
						console.log(respData);
					}
				});
			}
		}
	},
	eventHandler.filters = {
		populateSort: function () {
			var tempObj = {
				'MSID': 'MS ID',
				'Doi': 'Doi',
				'AcceptDate': 'Accept Date',
				'Article': 'Article Type',
				'Customer': 'Customer',
				'DaysInProd': 'Days In Prod',
				'DueDate': 'Due Date',
				'Owner': 'Owner',
				'Stage': 'Stage'
			}
			var sortopt = '<i>Sort cards by: </i>';
			for (var obj in tempObj) {
				if (obj == "MSID") {
					sortopt += '<span class="sort-item active" sort-option="' + obj + '" onclick="eventHandler.components.actionitems.cardSort(this,\'' + sortmapping[obj].sortAttr + '\',\'' + sortmapping[obj].highLightAttr + '\');" data-href="#">' + tempObj[obj] + '<i class="fa fa-caret-up fa-fw"></i></span>'
				} else {
					sortopt += '<span class="sort-item" sort-option="' + obj + '" onclick="eventHandler.components.actionitems.cardSort(this,\'' + sortmapping[obj].sortAttr + '\',\'' + sortmapping[obj].highLightAttr + '\');" data-href="#">' + tempObj[obj] + '</span>'
				}
			}
			var cards = $('#manuscriptsDataContent li:visible');
			var vals = [];
			cards.each(function () {
				vals.push($(this).attr('data-customer'));
			});
			vals = [...new Set(vals)];
			$('#sort').html(sortopt);
			if (vals.length <= 1) {
				$('#sort [sort-option="Customer"]').remove();
			}
			if (cards.length <= 1) {
				$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
				$('#sort').hide();
			} else {
				$('#sort').show();
			}
			//eventHandler.components.actionitems.cardSort($('#sort [sort-option="Doi"]')[0],'data-sort-doi','string');

		},
		populateFilters: function (filterObj) {
			var tempObj = {
				'articleType': 'Article Type',
				'typesetter': 'Typesetter',
				'publisher': 'Publisher',
				'author': 'Author',
				'copyeditor': 'Copyeditor',
				'editor': 'Editor',
				'support': 'Support',
				'supportstage': 'Support Stages',
				'customer': 'Customer',
				'contentloading': 'Content Loading',
				'others': 'Timeline'
			}
			$('div.filterOptions').empty();
			var navFilterHTML = '';
			var tableFilterHtml = '<table class="table table-bordered table-striped table-sm"><thead><tr  data-toggle="collapse" data-target="#dispFilter">';
			var tbodyHtml = '<tbody><tr  id="dispFilter" class="collapse out">';
			var tempHtml = '<table class="table table-bordered table-striped table-sm"><tr>';
			for (var obj in tempObj) {
				if (Object.keys(filterObj[obj]).length > 0) {
					tempNavFilterHTML = '';
					tempHtml += '<td><div class="container btn-group"><select name="multicheckbox[]" multiple="multiple" class="4col formcls">';
					tbodyHtml += '<td>';
					tableFilterHtml += "<th>" + tempObj[obj] + "</th>";
					var mainFiltAttr = 'no';
					var subFiltAtt = 'data-stage-name';
					if (filterMapping && filterMapping[obj]) {
						mainFiltAttr = filterMapping[obj];
					} else if (filterMapping && filterMapping['default']) {
						mainFiltAttr = filterMapping['default'];
					}
					/*if (Object.keys.length > 0) {
						tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + Object.keys(filterObj[obj]).length + '/' + Object.keys(filterObj[obj]).length + ')';
						tempNavFilterHTML += '</span>';
						tempNavFilterHTML += '<div class="filter-drop-content">';
					}*/
					var tempValue = 0;
					if (obj == "others") {
						if (Object.keys.length > 0) {
							tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + Object.keys(filterMapping["others-sub"]).length + '/' + Object.keys(filterMapping["others-sub"]).length + ')';
							tempNavFilterHTML += '</span>';
							tempNavFilterHTML += '<div class="filter-drop-content">';
						}
						for (var subObj in filterMapping["others-sub"]) {
							if (filterMapping["others-sub"][subObj] != undefined && filterMapping["others-sub"][subObj] != '') {
								tempHtml += ' <option value="' + subObj + '" main="' + obj + '">' + subObj + '</option>';
								tbodyHtml += '<label style=font-size:12px;"><input type="checkbox" name="fl-' + obj + '" value="' + subObj + '" id="' + subObj + '" />' + subObj + '</label>';
								var count = filterObj[obj][subObj] == undefined ? 0 : filterObj[obj][subObj];
								tempValue += count;
								tempNavFilterHTML += '<div class="filter-list active" data-filter-val="' + filterMapping["others-sub"][subObj]["val"] + '" data-filter-attr="' + filterMapping["others-sub"][subObj]["attr"] + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + filterMapping["others-sub"][subObj]["val"] + '\', \'' + filterMapping["others-sub"][subObj]["attr"] + '\' );" data-href="#">' + filterMapping["others-sub"][subObj]["name"] + '&nbsp;(' + count + ')</div>';
							}
						}
					} else {
						if (Object.keys.length > 0) {
							var filterLength = Object.keys(filterObj[obj]).length;
							if (cardViewFilterSeperator[obj] && cardViewFilterSeperator[obj].SecondaryFilterNames){
								cardViewFilterSeperator[obj].SecondaryFilterNames.forEach(append => {
									filterLength += Object.keys(filterObj[append]).length;
								})
							}
							tempNavFilterHTML += '<div class="btn-group filter-drop"><span style="color: black;" data-filter-root="' + obj + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + obj + '\', \'' + mainFiltAttr + '\');">' + tempObj[obj] + '&nbsp;(' + filterLength + '/' + filterLength + ')';
							tempNavFilterHTML += '</span>';
							tempNavFilterHTML += '<div class="filter-drop-content">';
						}
						if (cardViewFilterSeperator[obj]) {
							tempNavFilterHTML += '<div class="dropdown-header">' + tempObj[obj] + '</div><div class="divider"></div>'
						}
						for (var subObj in filterObj[obj]) {
							if (filterMapping && filterMapping[obj + '-sub']) {
								subFiltAttr = filterMapping[obj + '-sub'];
							} else if (filterMapping && filterMapping[obj]) {
								subFiltAttr = filterMapping[obj];
							} else if (filterMapping && filterMapping[subObj]) {
								subFiltAttr = filterMapping[subObj];
							} else if (filterMapping && filterMapping['default']) {
								subFiltAttr = filterMapping['default'];
							}
							tempHtml += ' <option value="' + subObj + '" main="' + obj + '">' + subObj + '</option>';
							tbodyHtml += '<label style=font-size:12px;"><input type="checkbox" name="fl-' + obj + '" value="' + subObj + '" id="' + subObj + '" />' + subObj + '</label>';
							tempValue += filterObj[obj][subObj];
							tempNavFilterHTML += '<div class="filter-list active" clickValue="' + obj + '" data-filter-val="' + subObj + '" data-filter-attr="' + subFiltAttr + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + subObj + '\', \'' + subFiltAttr + '\' );" data-href="#">' + subObj + '&nbsp;(' + filterObj[obj][subObj] + ')</div>';
						}
						if (cardViewFilterSeperator[obj] && cardViewFilterSeperator[obj].SecondaryFilterNames) {
							cardViewFilterSeperator[obj].SecondaryFilterNames.forEach(append => {
								if (Object.keys(filterObj[append]).length > 0) {
								tempNavFilterHTML += '<div class="dropdown-header">' + tempObj[append] + '</div><div class="divider"></div>'
								Object.keys(filterObj[append]).forEach(subObj => {
									tempNavFilterHTML += '<div class="filter-list active" clickValue="' + append + '" data-filter-val="' + subObj + '" data-filter-attr="' + filterMapping[append + '-sub'] + '" onclick="eventHandler.filters.filterDashBoard(this, \'#manuscriptContent li\',\'' + subObj + '\', \'' + filterMapping[append + '-sub'] + '\' );" data-href="#">' + subObj + '&nbsp;(' + filterObj[append][subObj] + ')</div>';
								})
								delete filterObj[append];
								delete tempObj[append];
							     }
							})
						}
					}
					tempHtml += '</td>';
					tbodyHtml += '</td>';
					if (Object.keys.length > 0) {
						tempNavFilterHTML += '</div></div>';
					}
					// tempNavFilterHTML = tempNavFilterHTML.replace('{count}', Object.keys(filterObj[obj]).length);
					if (tempValue > 0) {
						// navFilterHTML += tempNavFilterHTML.replace('{value}', tempValue);
						navFilterHTML += tempNavFilterHTML;
					}
				}
			}

			var tableContent = tableFilterHtml + '</thead>' + tbodyHtml + '</tbody></table>'
			$('div.filterOptions').html(navFilterHTML);
			var cards = $('#manuscriptsDataContent li:visible');
			var vals = [];
			cards.each(function () {
				vals.push($(this).attr('data-customer'));
			});
			vals = [...new Set(vals)];
			if (vals.length <= 1) {
				$('div.filterOptions [data-filter-root="customer"]').closest('div').remove();
			}
			return true;
			var navHTML = ``;
			for (const stageOwner in stageOwners) {
				if (stageOwners.hasOwnProperty(stageOwner) && (stageOwner != 'article-type' || stageOwner != 'total')) {
					var navListTotal = 0;
					var navListHTML = ``;
					for (const owner in stageOwners[stageOwner]) {
						if (stageOwners[stageOwner].hasOwnProperty(owner) && stageOwners[stageOwner][owner]['article-count']) {
							navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${owner}','filterBy':'stageOwners','filterOwner':'${stageOwner}'}}"    data-value="${owner}">${stageOwners[stageOwner][owner]['article-count']} : ${owner}</li>`;
							navListTotal += stageOwners[stageOwner][owner]['article-count'];
						}
					}
					navHTML += `<span class="stage-filter">
							<span data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >${stageOwner} (${navListTotal})
							</span>
							<div class="dropdown">
								<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
							</div>
						</span>`;
				}
			} {
				var navListTotal = 0;
				var navListHTML = ``;
				for (const articleType in stageOwners['article-type']) {
					if (stageOwners['article-type'].hasOwnProperty(articleType)) {
						navListHTML += `<li class="owner-filter" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'filterArticle','param': {'filter': '${articleType}','filterBy':'stageOwners', 'filterOwner':'article-type'}}"  >${stageOwners['article-type'][articleType]['article-count']} : ${articleType}</li>`;
						navListTotal += stageOwners['article-type'][articleType]['article-count'];
					}
				}
				navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Article Type (${navListTotal})
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown">${navListHTML}</ul>
						</div>
					</span>`;
				navHTML += `<span class="article-type-filter">
						<span data-label="true" data-label="true" data-channel="dropdowns" data-topic="stage" data-event="click" data-message="{'funcToCall': 'toggleStageFilter'}" >Total (5)
						</span>
						<div class="dropdown">
							<ul class="dropdown-list hide-dropdown"></ul>
						</div>
					</span>`;
			}
			$('div.navFilter').empty();
			$('div.navFilter').html(navHTML);
		},
		populateNavigater: function (param) {
			var navigatePanel = '';
			var projectList = '';
			var projectArray;
			var cardAttribute = {
				'articleType': 'data-article-type',
				'typesetter': 'data-stage-name',
				'publisher': 'data-stage-name',
				'author': 'data-stage-name',
				'editor': 'data-stage-name',
				'copyeditor': 'data-stage-name',
			};
			$('#manuscriptContent #customerVal').text(param.customer);
			if (param) {
				if (param.refreshedProject && param.refreshedProject != undefined && (/(.*)\(/g).exec(param.refreshedProject) && (/(.*)\(/g).exec(param.refreshedProject)[1]) {
					param.project = (/(.*)\(/g).exec(param.refreshedProject)[1];
				}
				Object.keys(userData.kuser.kuroles).forEach(function (customer, index) {
					if (projectLists && projectLists[customer]) {
						var roleAccess = (/^(production|copyeditor)$/.test(userData.kuser.kuroles[customer]['role-type']));
						var clientType = projectLists[customer].type ? projectLists[customer].type : 'journal'
						if (index == 0 && !param.customer) {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true , \'version\': \'v2.0\'}}" class="filter-list active" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list active" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							}
							if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
								projectLists[customer]['name'].forEach(function (project, count) {
									projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
								});
							}
						} else if ((param.customer) && (param.customer == customer)) {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true ,\'version\': \'v2.0\'}}" class="filter-list active" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list active" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							}
							if (param.project && param.project != undefined && param.project != 'project') {
								projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this})">All</div>';
								if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
									projectLists[param.customer]['name'].forEach(function (project, count) {
										if (param.project == projectLists[customer]['value'][count]) {
											if (param.filterObj.project[projectLists[customer]['value'][count]] == undefined) {
												projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
											} else {
												projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
											}
										} else if (param.filterObj && param.filterObj.project[project]) {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
										} else {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
										}
									});
								}
							} else {
								projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'All\', \'node\':this})">All</div>';
								if (projectLists[param.customer] && projectLists[param.customer]['name'] && projectLists[param.customer]['value']) {
									projectLists[customer]['name'].forEach(function (project, count) {
										if (param.filterObj && param.filterObj.project[projectLists[customer]['value'][count]]) {
											projectList += '<div class="filter-list active" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '(' + param.filterObj.project[projectLists[customer]['value'][count]] + ')</div>';
										} else {
											projectList += '<div class="filter-list" onclick="eventHandler.filters.projectFilters({\'project\':\'' + projectLists[customer]['value'][count] + '\', \'node\':this, \'attributeToCheck\':\'data-project\'})" value=' + projectLists[customer]['value'][count] + '>' + project + '</div>';
										}
									});
								}
							}
						} else {
							if (roleAccess) {
								navigatePanel += '<div style="color: black;" data-channel="mainMenu" data-topic="toggle" data-event="click" data-href="manuscriptContent" data-message="{\'funcToCall\': \'showManuscript\', \'param\' : { \'workflowStatus\':\'in-progress\', \'urlToPost\':\'getAssigned\', \'customer\':\'' + customer + '\',\'custTag\': \'#filterCustomer\', \'prjTag\':\'#filterProject\', \'user\':\'user\',\'optionforassigning\':true ,\'version\': \'v2.0\'}}" class="filter-list" value="' + customer + '" data-type="' + clientType + '">' + customer + '</div>';
							} else {
								navigatePanel += '<div class="filter-list" data-channel="mainMenu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'showManuscript\', \'param\': {\'workflowStatus\':\'in-progress OR completed\' , \'customer\':\'' + customer + '\',\'excludeStage\': \'inCardView\',\'optionforassigning\':true, \'version\': \'v2.0\'}}" data-href="manuscriptContent" value="' + customer + '" data-type="' + clientType + '"> ' + customer + '</div>';
							}

						}
					}
				});
				param.project = 'project'
				$('#filterCustomer').html(navigatePanel);
				$('#filterProject').html(projectList);
				$('.la-container').css('display', 'none');
			}
		},
		notifyNo: function (target) {
			if ($(target).attr('data-last-sele') == "All") {
				$('#allproject').addClass('active');
				$($("#allproject").siblings()).addClass('active');
			} else {
				$('#chartProject').find('.active').removeClass('active');
				$('#chartProject').find('[data-project="' + $(target).attr('data-last-sele') + '"]').addClass('active');
			}
			$('#notifyprojectlist').hide();
		},
		updateFilters: function (param) {
			if (param.project != "Project" && param.project != undefined) {
				$('[data-filter-attr]').each(function () {
					var attrval = [];
					var attr = $(this).attr('data-filter-attr');
					var attrval = $(this).attr('data-filter-val');
					param.project = param.project.replace(/\(.*\)/gm, '')
					if (param.project != "All") {
						var count = $('#manuscriptsDataContent li[data-project="' + param.project + '"][' + attr + '="' + attrval + '"]:visible').length;
					} else {
						var count = $('#manuscriptsDataContent li[' + attr + '="' + attrval + '"]:visible').length;
					}
					$(this).text($(this).text().replace(/\(.*\)/gm, '(' + count + ')'));
				});
			}
		},
		projectFilters: function (param) {
			eventHandler.components.actionitems.hideRightPanel();
			if ($(param.node).attr('id') == 'refresh') {
				// param.node = $(param.node).siblings().find('#filterProject .active');
				param.node = $(param.node).closest('#manuscriptContent').find('#filterProject .active');
			}
			$(param.node).parent().siblings().html($(param.node).text());
			if (param.project == "All") {
				if ($(param.node).hasClass('active')) {
					$(param.node).removeClass('active');
					$($(param.node).siblings()).removeClass('active');
					$('#manuscriptContent li').addClass('dashboardFilterHidden');
				} else {
					$(param.node).addClass('active');
					$($(param.node).siblings()).addClass('active');
					$('#manuscriptContent li').removeClass('dashboardFilterHidden');
				}
			} else {
				$(param.node).addClass('active');
				$(param.node).siblings('#allproject').removeClass('active');
				$($(param.node).siblings()).removeClass('active');
			}
			if (param.attributeToCheck) {
				var filterTag = '#manuscriptContent li';
				var filterAtt = param.attributeToCheck;
				// var filterAtt = 'data-category';
				if ($(param.node).hasClass('active')) {
					//$(param.node).removeClass('active');
					var filterText = $(param.node).attr('value');
					if ($(filterTag + '[' + filterAtt + '="' + filterText + '"]').length > 0) {
						$($(filterTag + '[' + filterAtt + '="' + filterText + '"]').siblings()).addClass('dashboardFilterHidden');
						$(filterTag + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
					} else {
						$(filterTag).addClass('dashboardFilterHidden');
					}
				} else {
					var filterText = $(param.node).attr('value');
					$(filterTag + '[' + filterAtt + '="' + filterText + '"]').addClass('dashboardFilterHidden');
				}

			}
			$('#msCount').text($('#manuscriptsDataContent').children().not('.dashboardFilterHidden').length);
			if ($(param.node).closest("#manuscriptContent").length == 0) {
				if ($('#chartProject').find('.active').length > 0) {
					$('#notifyprojectlist').show();
				} else {
					$('#notifyprojectlist').hide();
				}
			}
			//when loading at first time if cards not there in card view code is breaking
			if ($('#manuscriptsDataContent li:visible').length > 0) {
				$('#manuscriptsData').scrollTop($('#manuscriptsDataContent li').position().top);
			}
			eventHandler.filters.updateFilters(param);
			$('#sort .fa').remove();
			// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
			var customer = $('#manuscriptContent #customerVal').text();
			if (!customer.match(/customer|first/gi)) {
				var userRole = userData.kuser.kuroles[customer]['role-type'];
				var userAccess = userData.kuser.kuroles[customer]['access-level'];
				if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
					eventHandler.components.actionitems.cardSort($('#sort [sort-option="MSID"]')[0], 'sort-msid', '.msDOI');
				}
			}
		},
		customerFilter: function (type, send) {
			if (!type) {
				return;
			}
			var customer;
			$(type).data('clicked', true);
			if ($(type).hasClass('active')) {
				$(type).removeClass('active');
			} else {
				$(type).addClass('active')
			}
			if (send) {
				var customer = [];
				$("#chartfilterCustomer .active").each(function () {
					customer.push($(this).text());
				});
				$('.la-container').css('display', 'none');
				eventHandler.highChart.makeCharts({
					'customer': customer,
					'project': []
				})
			}
		},
		stageBasedArticles: function (articles) {
			var stageOwners = {
				'exeter': {
					'addJob': {},
					'pre-editing': {},
					'typesetter': {},
					'copyediting': {}
				},
				'publisher': {
					'publisher-check': {},
					'publisher-review': {},
				},
				'author': {
					'author-check': {},
					'review': {},
				},
				'article-type': {}
			};
			var addFilterData = {
				'accepted': [],
				'stage-due': [],
				'ce-level': [],
				'days-in-production': []
			};
			var stageArticleCount = 0;
			var articleTypeCount = 0;
			for (const article of articles) {
				for (const stageOwner in stageOwners) {
					var matchedStage = stageOwners[stageOwner][article._source.stageName.toLowerCase()];
					if (matchedStage) {
						if (matchedStage['article-count']) {
							matchedStage['article-count']++;
						} else matchedStage['article-count'] = 1;
						if (matchedStage['article-data-id']) {
							matchedStage['article-data-id'].push(stageArticleCount);
						} else matchedStage['article-data-id'] = [stageArticleCount];
						break;
					}
				}
				addFilterData['accepted'].push(article._source.acceptedDate);
				addFilterData['stage-due'].push(article._source.stageDueDate);
				addFilterData['ce-level'].push(article._source.CELevel);
				addFilterData['days-in-production'].push(article._source.daysInProduction);
				stageArticleCount++;
			}
			for (const article of articles) {
				var matchedArticleType = stageOwners['article-type'][article._source.articleType];
				if (matchedArticleType) {
					stageOwners['article-type'][article._source.articleType]['article-count']++;
					stageOwners['article-type'][article._source.articleType]['article-data-id'].push(articleTypeCount);
				} else {
					stageOwners['article-type'][article._source.articleType] = {};
					stageOwners['article-type'][article._source.articleType]['article-count'] = 1;
					stageOwners['article-type'][article._source.articleType]['article-data-id'] = [articleTypeCount];
				}
				articleTypeCount++;
			}
			return [stageOwners, filterData];
		},
		articleFilter: function (articles) {
			var stageArticleCount = 0;
			var articleTypeCount = 0;
			for (const article of articles) {
				for (const stageOwner in stageOwners) {
					var matchedStage = stageOwners[stageOwner][article._source.stageName.toLowerCase()];
					if (matchedStage) {
						if (matchedStage['article-count']) {
							matchedStage['article-count']++;
						} else matchedStage['article-count'] = 1;
						if (matchedStage['article-data-id']) {
							matchedStage['article-data-id'].push(stageArticleCount);
						} else matchedStage['article-data-id'] = [stageArticleCount];
						break;
					}
				}
				stageArticleCount++;
			}
		},
		filterDashBoard: function (type, filterTag, filterText, filterAtt) {
			if (!type) {
				return;
			}
			eventHandler.components.actionitems.hideRightPanel();
			var project = $('#manuscriptContent #projectVal').text().replace(/\(.*\)/gm, '');
			if (project == "Project" || project == "All" || $('#support').find('.active').length) {
				prjectSelect = '';
			} else {
				prjectSelect = '[data-project="' + project + '"]';
			}
			if ($(type).siblings().children().length) {
				$(type).parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				if ($(type).hasClass('highlightFilter')) {
					$(type).removeClass('highlightFilter');
					$('.filterOptions .filter-list').addClass('active');
					$(type).text($(type).text().replace(/\(.*\//g, '(' + $(type).siblings().find('.filter-list').length + '/'))
					$(type).parent().siblings().each(function (name) {
						$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(' + $(this).children('.filter-drop-content').children('.filter-list').length + '/'))
					})
					// $(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
					$(filterTag + prjectSelect).removeClass('dashboardFilterHidden');
				} else {
					$(type).addClass('highlightFilter');
					$(type).parent().siblings().each(function (name) {
						$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
					})
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
					$('.filterOptions .active').removeClass('active');
					$(type).siblings().children().addClass('active');
					$(type).text($(type).text().replace(/\(.*\//g, '(' + $(type).siblings().find('.filter-list.active').length + '/'))
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
				}
				// } else if ($(type).siblings('.active').length == $(type).siblings().length ) {
				//else if ($(type).parent().children('.active').length == $(type).parent().children().length)
			} else if ($(type).parent().children('.filter-list.active').length == $(type).parent().children('.filter-list').length) {
				$(type).parent().parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				$(type).parent().siblings().addClass('highlightFilter');
				$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				$(type).parent().parent().siblings().each(function (name) {
					$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
				})
				$('.filterOptions .active').removeClass('active');
				$(type).addClass('active');
				$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
				$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
			}
			else {
				// To remove active class with different category.
				if ($(type).siblings('.active').attr('clickvalue') != $(type).attr('clickvalue')) {
					$(type).siblings().removeClass('active');
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				}
				$(type).parent().parent().siblings().find('.highlightFilter').removeClass('highlightFilter');
				$(type).parent().siblings().addClass('highlightFilter');
				$(type).closest('.filter-drop').siblings().find('.filter-list.active').removeClass('active');
				$(type).parent().parent().siblings().each(function (name) {
					$(this).children().first().text($(this).children().first().text().replace(/\(.*\//g, '(0/'))
				})
				if ($(type).siblings('.active').length == 0) {
					$(filterTag + prjectSelect).addClass('dashboardFilterHidden');
				}
				if ($(type).hasClass('active')) {
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').addClass('dashboardFilterHidden');
					$(type).removeClass('active');
					$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length)) + '/'))
				} else {
					$(type).addClass('active');
					$(filterTag + prjectSelect + '[' + filterAtt + '="' + filterText + '"]').removeClass('dashboardFilterHidden');
					$(type).parent().siblings().text($(type).parent().siblings().text().replace(/\(.*\//g, '(' + (Number($(type).siblings('.filter-list.active').length) + 1) + '/'))
				}
			}
			if (filterAtt == 'all') {
				$(filterTag + prjectSelect).removeClass('dashboardFilterHidden');
			}
			//if $('#manuscriptsDataContent li') is empty at that time code is breaking
			if($('#manuscriptsDataContent li').length > 0){
				$('#manuscriptsData').scrollTop($('#manuscriptsDataContent li').position().top);
			}
			$('#msCount').text($(filterTag + prjectSelect + ':not(.dashboardFilterHidden)').length);
			$('#sort .fa').remove();
			// To show sort option for particular user role and access level mentioned in variable in sortAuthorization in file default.js
			var customer = $('#manuscriptContent #customerVal').text();
			if (!customer.match(/customer|first/gi)) {
				var userRole = userData.kuser.kuroles[customer]['role-type'];
				var userAccess = userData.kuser.kuroles[customer]['access-level'];
				if (sortAuthorization && sortAuthorization[userRole] && sortAuthorization[userRole].indexOf(userAccess) > -1) {
					eventHandler.components.actionitems.cardSort($('#sort [sort-option="MSID"]')[0], 'sort-msid', '.msDOI');
				}
			}
		},
		inBetween: function (heystack, needle1, needle2) {

		},
		greaterThanEquals: function (heystack, needle) {},
		lesserThanEquals: function (heystack, needle) {},
		equalsTo: function (heystack, needle) {}
	},
	eventHandler.bulkedit = {
		displayprocess: {
			clickevents: function (targetnode, checkBox) {
				var attr = $(targetnode).attr('data-id');
				if (typeof attr !== typeof undefined && attr !== false) {
					if ($(targetnode).prop('checked')) {
						$(checkBox).removeAttr('checked')
						$(checkBox).prop( "checked", true );
						$(checkBox).attr('checked', 'true')
					} else {
						$(checkBox).removeAttr('checked')
						$(checkBox).prop( "checked", false );
					}
				}
			},
			editOwner: function (targetnode) {
				var cardID = $(targetnode).closest('tr[data-id]').attr('data-id');
				console.log(cardID);
			}
		},
		export: {
			exportExcel: function (param) {
				$($('#chartReportTable').find('.sort-arrow')).remove();
				if (param.tableID) {
					if ($('#' + param.tableID).length > 0) {
						var orgTableData = $('#' + param.tableID)[0];
						var cloneTableData = orgTableData.cloneNode(true);
						if (param.excludeRow) {
							$(cloneTableData).find(param.excludeRow).remove();
						}
						if (param.excludeColumn) {
							$(cloneTableData).find(param.excludeColumn).remove();
						}
						$(cloneTableData).find('.assign-users').each(function () {
							if(this.value != undefined){
								this.replaceWith(this.value.replace(/,.*/g, ''));
							}
						})
						$(cloneTableData).attr('id', 'exportExcelCloneTable');
						$(cloneTableData).attr('class', 'hidden');
						document.body.appendChild(cloneTableData);
						tableToExcel('exportExcelCloneTable', 'Report');
						$('#exportExcelCloneTable').remove();
					}
				}
			},
			managefilter(event) {
				var x = event.which || event.keyCode;
				if (x == 13 || event == 'sort') {
					if ($('.repchkbox').length != $('.repchkbox:checked').length) {
						$("#repchkboxall").prop('checked', false);
					}
					if ($('#chartReportTable').find('tr.reportCheckBox.show').length > 0) {
						//$('#historytbl').append($('#chartReportTable').find('tr.reportCheckBox.show'));
					}
				}
				var count = $('#reportData').find('tr.reportTable:visible').length;
				$('#reportscount').text('Showing 1-' + count + ' of ' + count);
			},
			manageHistory: function (target) {
				/*if($($(target).closest('tr').next()).hasClass('reportCheckBox')){
					$('#historytbl').append($(target).closest('tr').next());
				}else{
					$($('#historytbl').find($(target).attr("data-target"))).insertAfter($(target).closest('tr'));
					$($(target).attr("data-target")).addClass('show');
				}*/
				$('#reportsHistory table tr.stages').remove();
				$('#reportHistoryDoi').text('article');
				//$('#articleHistory').attr('data', '');
				//var msCard = $(targetNode).closest('.msCard');
				var cardID = $(target).closest('tr').attr('data-report-id');
				var parentId = $(target).closest('tr').attr('data-dashboardview');
				var sourceData = chartData[$(target).closest('tr').attr('data-report-id')]._source;
				//$('#articleHistory').attr('data', $(msCard).attr('data').replace(/\'/g, '"'));
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var stages = sourceData.stage;
				var collapseRow = '', hideCompletedRow = '';
				var hideRowId = 0;
				for (var s = 0, sl = $(stages).length; s < sl; s++) {
					var stage = stages[s] ? stages[s] : stages;
					var doi = sourceData.id.replace('.xml', '').replace('\.', '');
					var startDate = new Date(stage['start-date']);
					var endDate = new Date(stage['end-date']);
					var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					var slaStartDate = new Date(stage['sla-start-date']);
					var inprogTimeDiff = Math.abs(new Date().getTime() - slaStartDate.getTime());
					var inprogressDayDiff = Math.ceil(inprogTimeDiff / (1000 * 3600 * 24));
					// var stageRow = $('<tr class="stages" data-stage-name="' + stage.name + '"/>');
					var stageRow = '';
					$('#reportHistoryDoi').text(sourceData.id.replace('.xml', ''));
					if (stage.status != 'in-progress' && stage.status != 'waiting') {
						var tempSlaStartDate = stage['sla-start-date'] ? dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempSlaEndDate = stage['sla-end-date'] ? dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempPlaStartDate = stage['planned-start-date'] ? dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") : '';
						var tempPlaEndDate = stage['planned-end-date'] ? dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") : '';
						// stageRow.html('<td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td>');
						if (hideRepeatedStage.default.includes(stage.name)) {
							collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>' + collapseRow;
							continue;
						} else {
							if (stages.length != s + 1 && stages[s + 1].status == 'completed' && hideRepeatedStage.default.includes(stages[s + 1].name)) {
								collapseRow = '<tr class="hideRow' + hideRowId + ' stages" style="display:none;background-color:#E0FFFF;"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>' + collapseRow;
								continue;
							} else {
								if (collapseRow) {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned) ? ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) : '-') + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>'
								} else {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td></td><td class="stage-name">' + stage.name + '</td><td>' + ((stage.assigned) ? ((stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : stage.assigned.to) : '-') + '</td><td>' + dateFormat(stage['start-date'], "mediumDate") + '</td><td>' + dateFormat(stage['end-date'], "mediumDate") + '</td><td>' + tempSlaStartDate + '</td><td>' + tempSlaEndDate + '</td><td>' + tempPlaStartDate + '</td><td>' + tempPlaEndDate + '</td><td>' + stage.status + '</td><td>' + diffDays + '</td></tr>'
								}

							}
						}
					}
					if (collapseRow || hideCompletedRow) {
						var hideRow = '';
						if (hideCompletedRow) {
							hideRow += hideCompletedRow;
						}
						if (collapseRow) {
							hideRow += collapseRow;
						}
						stageRow = $(hideRow);
						collapseRow = hideCompletedRow = '';
						$('#reportsHistory table tbody').prepend(stageRow);
					}
					if (stage.status == 'in-progress') {
						stageRow = $('<tr class="stages" data-stage-name="' + stage.name + '"/>');
						usersHTML = "";
						usersArr = {};
						var users;
						var assignUserRole = userData.kuser.kuroles[sourceData.customer]['role-type'];
						var assignUserAccess = userData.kuser.kuroles[sourceData.customer]['access-level'];
						if (userAssignAccessRoleStages && userAssignAccessRoleStages[assignUserRole] && userAssignAccessRoleStages[assignUserRole][assignUserAccess] && !userAssignAccessRoleStages[assignUserRole][assignUserAccess].includes(stage.name)) {
							if (stage.assigned.to) {
								users = $('<span>' + stage.assigned.to + '</span>');
							} else {
								users = $('<span>Unassigned</span>');
							}
						} else {
							users = $('<select class="assign-users" data-doi="' + doi + '" onchange="eventHandler.components.actionitems.assignUser(this,' + cardID + ', \'data-report-id\', false, \'\', \'\', true, \'\', \'\', \''+parentId+'\')" style="display:inline;height:auto;padding: 2px 0px;background: #0089ec;color: #fff;"></select>');
							// $(usersDetailsDiv).find('user').each(function (i, v) {
							// 	users.append('<option value="' + $(this).find('first').text() + ', ' + $(this).find('email').text() + '">' + $(this).find('first').text() + '</option>');
							// });
							if (usersList && usersList.users) {
								Object.keys(usersList.users).forEach(function (count, q) {
									var skillLevel = (usersList.users[count].additionalDetails && usersList.users[count].additionalDetails['skill-level']) ? usersList.users[count].additionalDetails['skill-level'] : "";
									users.append('<option skill-level="' + skillLevel + '" value="' + usersList.users[count].name + ', ' + usersList.users[count].email + '">' + usersList.users[count].name + '</option>');
									// console.log(usersList.users[a])
								})
							}
							if (users.find('option[value="' + stage.assigned.to + '"]').length > 0) {
								users.find('option[value="' + stage.assigned.to + '"]').attr('selected', true);
								users.append('<option value="' + " " + '">Unassign</option>');
							} else {
								if (stage.assigned.to == null || stage.assigned.to == "" || stage.assigned.to == "Exeter") {
									users.append('<option value="' + " " + '" selected>Un assigned</option>');
								} else {
									users.append('<option value="' + stage.assigned.to + '"  selected>' + stage.assigned.to + '</option>');
									users.append('<option value="' + " " + '">Unassign</option>');
								}
							}
						}
						usersHTML = $(users)[0].outerHTML;
						//update by prasana
						if ($('#reportsHistory table tbody tr').hasClass('hideRow' + hideRowId)) {
							stageRow.html('<td><i class="fa fa-plus-circle" style="cursor:pointer;" data-toggle="collapse" onclick = "eventHandler.components.actionitems.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'data-report-id\', false,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						} else {
							stageRow.html('<td></td><td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'data-report-id\', false,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						}
						// stageRow.html('<td class="stage-name">' + stage.name + '</td><td id="editingName" class="assigned-to">' + usersHTML + '</td><td><a class="kriyaDatePic"><span style="position: absolute; z-index: 111;" id="pickDateStart' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span>' + dateFormat(stage['start-date'], "mediumDate") + '</span></a></td><td><span>' + dateFormat(stage['end-date'], "mediumDate") + '</span></td><td><span>' + dateFormat(stage['sla-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['sla-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><span>' + dateFormat(stage['planned-start-date'], "mmm dd, yyyy HH:MM:ss") + '</span></td><td><a class="kriyaDatePic"><span id="blkpickDateEnd' + doi + '" data-on-success="eventHandler.components.actionitems.saveWFDate"></span><span class="Pickdate" data-doi="' + doi + '" onclick="eventHandler.common.functions.pickDateNew(\'' + doi + '\', \'End\', ' + cardID + ', \'data-report-id\', false,event)" data-date-type="planned-end-date" end-date="' + dateFormat(stage['planned-end-date'], "mmm dd yyyy HH:MM:ss") + '" id="blkdateEnd' + doi + '">' + dateFormat(stage['planned-end-date'], "mmm dd, yyyy HH:MM:ss") + '</span></a></td><td>' + stage.status + '</td><td style="color:red">' + inprogressDayDiff + '+</td>');
						//update end by prasana
					}
					hideRowId++;
					// //if (usersArr.users) {
					$('#reportsHistory table tbody').prepend(stageRow);
					//console.log(stages[s])
				}
				$('#reportsHistory table tbody').prepend(' <tr class="stages"><th/><th>Stage Name</th><th>Assigned to</th><th>Start Date</th><th>End Date</th><th>Sla-Start Date</th><th>Sla-End Date</th><th>Planned-Start Date</th><th>Planned-End Date</th><th>Status</th><th>Days</th></tr>')
				$('#reportsHistory').modal()
			},
			exportTable: function (params, customer,fromTotal, toTotal, totalCount, tableType, chartTableType, chartTableTemplateName, movementData, config, clickEvent, fromReportTab, titleName, tableName) {
				$("#repchkboxall").prop('checked', false);
				var terminateConnection;
				if (terminateConnection == undefined) {
					terminateConnection = false;
				}
				//To terminate the connection when modal box is closed
				$("#chartReportCloseButton").click(function () {
					terminateConnection = true;
				});
				if (terminateConnection) {
					return;
				}
				if (!chartTableType || chartTableType == undefined) {
					chartTableType = 'default';
				}
				if (!chartTableTemplateName || chartTableTemplateName == undefined) {
					chartTableTemplateName = 'chartReportTemplate';
				}
				setTimeout(function () {
					var data = {}, assignUserRole = userData.kuser.kuroles[customer]['role-type'], assignUserAccess = userData.kuser.kuroles[customer]['access-level'];
					if (params.url && params) {
						$.ajax({
							type: "POST",
							url: params.url,
							data: params,
							dataType: 'json',
							//async: false,
							success: function (respData) {
								if (terminateConnection) {
									return;
								}
								if (!respData || respData.length == 0) {
									return;
								}
								if (fromTotal == 0) {
									$('#reportData').html('');
									$('#cardReportData #manuscriptsDataContent').html('');
									tableReportData = [];
									$("#chartReportTable tr").html(' ');
									$("#chartReportTable tr").append('<th class="reportCheckBox" col-size="3" data-filter="reportCheckBox"><input data-id="all" class="reportCheckBox" id="repchkboxall" onclick="eventHandler.bulkedit.displayprocess.clickevents(this, \'.repchkbox\')" type="checkbox"/></th>');
									var tablerow = '';
									// To create table with configuration.
									chartTableReport[chartTableType].columns.forEach(function (column) {
										var row = '<th';
										Object.keys(column.attribute).forEach(function (value) {
											row += ' ' + value + '="' + column.attribute[value] + '"';
										})
										row += '>' + column.columnName + '</th>';
										tablerow += row;
									})
									$("#chartReportTable tr").append($(tablerow));
								}
								if (!respData || respData.length == 0) {
									$('.la-container').css('display', 'none');
									return;
								}
								$("#chartReportTable th").show().removeClass('reportCheckBox');
								$("#chartReportTable [data-filter='reportCheckBox'],[data-filter='reportHistory']").addClass('reportCheckBox');
								var pagefn = doT.template(document.getElementById(chartTableTemplateName).innerHTML, undefined, undefined);
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['others'] = {};
								data.info = respData.hits;
								data.dconfig = dashboardConfig;
								Array.prototype.push.apply(tableReportData, respData.hits);
								chartData = tableReportData;
								data.count = fromTotal;
								data.userDet = JSON.parse($('#userDetails[data]').attr('data'));
								data.userAssignAccessRoleStages = userAssignAccessRoleStages[assignUserRole][assignUserAccess];
								data.usersList = usersList;
								if (movementData && movementData.stageName && movementData.dateType && movementData.date) {
									data.stageName = movementData.stageName;
									data.dateType = movementData.dateType;
									data.date = movementData.date;
								}
								$('#reportData').append(pagefn(data));
								data.dashboardview = '#cardReportData';
								data.storeVariable = 'chartData';
								var cardfn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
								$('#cardReportData #manuscriptsDataContent').append(cardfn(data));
								$('#reportColHideShow').addClass('hidden');
								$('#filterExportExcel').addClass('hidden');
								$('#chartReportTable').parent().addClass('hidden');
								$('#cardReportData').removeClass('hidden');
								$('[toggleview="card"]').attr('toggleView','table').removeClass('fa-list').addClass('fa-table').text(' View Table');
								$('.sort-arrow').remove();
								$('#chartReportTable thead').find('.fltrow').remove();
								var count = $('#reportData').find('tr.reportTable').length;
								$('#reportscount').text('Showing 1-' + count + ' of ' + count);
								var dateFilter = function (tf) {
									tf.setFilterValue(8, '>=01-01-1900');
									tf.setFilterValue(9, '>=01-01-1900');
									tf.setFilterValue(10, '>=01-01-1900');
									tf.setFilterValue(11, '>=01-01-1900');
									tf.setFilterValue(12, '>=01-01-1900');
									tf.setFilterValue(13, '>=01-01-1900');
									tf.setFilterValue(14, '>=01-01-1900');
									tf.setFilterValue(15, '>=01-01-1900');
									tf.filter();
								}
								if (chartTableType == 'movementChart') {
									dateFilter = function (tf) {
										tf.setFilterValue(8, '>=01-01-1900');
										tf.setFilterValue(9, '>=01-01-1900');
										tf.setFilterValue(10, '>=01-01-1900');
										tf.setFilterValue(11, '>=01-01-1900');
										tf.setFilterValue(12, '>=01-01-1900');
										tf.setFilterValue(13, '>=01-01-1900');
										tf.filter();
									}
								}
								if (count && count > 1) {
									var tf = new TableFilter('chartReportTable', {
										base_path: './js/libs/tablefilter/',
										auto_filter: true,
										auto_filter: {
											delay: 1000 //milliseconds
										},
										col_types: tableReportFilterConfig[chartTableType].rowtype,
										filters_row_index: 1,
										on_filters_loaded: dateFilter,
										extensions: [{
											name: 'sort'
										}],
									});
									if (tableReportFilterConfig[chartTableType].filter) {
										tableReportFilterConfig[chartTableType].filter.forEach(function (column) {
											tf.cfg[column] = "none";
										})
									}
									tf.init();
									$('.flt').attr('onKeyUp', 'eventHandler.bulkedit.export.managefilter(event)');
								}
								var colHideCheck = '';
								colHideCheck = '<span>';
								for (var checkVal in ShowHideCol[chartTableType]) {
									if (ShowHideCol[chartTableType][checkVal].defaultShow) {
										colHideCheck += '<label class=".checkbox-inline" ><input type="checkbox" index = "' + ShowHideCol[chartTableType][checkVal].index + '" id="' + checkVal + '" checked/>' + ShowHideCol[chartTableType][checkVal].name + '</label>';
										$("#chartReportTable").find('tr :nth-child(' + (ShowHideCol[chartTableType][checkVal].index) + ')').removeClass('reportCheckBox').show();
									} else {
										colHideCheck += '<label class=".checkbox-inline" ><input type="checkbox" index = "' + ShowHideCol[chartTableType][checkVal].index + '" id="' + checkVal + '"/>' + ShowHideCol[chartTableType][checkVal].name + '</label>';
										$("#chartReportTable").find('tr :nth-child(' + (ShowHideCol[chartTableType][checkVal].index) + ')').addClass('reportCheckBox').hide();
									}
								}
								colHideCheck += '</span>';
								$('#reportColHideShow').html(colHideCheck);
								$('#reportsLoadingcount .msCount.loading.hidden').removeClass('hidden');
								if (params.exportTable) {
									//To stop the loading icon
									$('.la-container').css('display', 'none');
									$('#modalReport').modal();
									if (fromTotal == 0) {
										setTimeout(() => {
											if ($('#chartReportTable').attr('table-size') == undefined) {
												var modelwidth = ($(window).width() / 100) * 98 - 62;
												$('#chartReportTable').attr('table-size', modelwidth);
											}
											for (var i = 0; i < $('#chartReportTable th').length; i++) {
												var width = (parseInt($('#chartReportTable').attr('table-size')) / 100) * parseInt($($('#chartReportTable th')[i]).attr('col-size'));
												$('#chartReportTable').find('tr :nth-child(' + (i + 1) + ')th,tr :nth-child(' + (i + 1) + ')td').css('width', width);
											}
										}, 0);
									}
								}
								// To check whether the displaying article count is equal to total article count and to take this in loop
								if (toTotal < totalCount) {
									fromTotal = fromTotal + respData.hits.length;
									toTotal = fromTotal + ((totalCount - toTotal > articleLoopCountInTableReport) ? (articleLoopCountInTableReport) : (totalCount - toTotal));
									$('#reportsLoadingcount #msYtlCount').text(fromTotal);
									$('#reportsLoadingcount #msCount').text(totalCount);
									$('#reportsLoadingcount').removeClass('hidden');
									$('#reportsLoadingcount').show();
									$('#reportscount').hide();
									$('#reportsLoadingcount').siblings().not("#chartReportCloseButton").addClass('hidden');
									if (fromReportTab) {
										eventHandler.tableReportsSummary.articlesReport(fromReportTab.tableId, customer, fromReportTab.xPath, titleName,tableName, fromTotal, toTotal)
									} else {
										eventHandler.highChart.tableReport('', clickEvent, config, customer, tableType, '', '', fromTotal, toTotal);
									}
								} else if (toTotal >= totalCount && params.exportTable != undefined) {
									$('#reportsLoadingcount').hide();
									$('#reportsLoadingcount').siblings().removeClass('hidden');
									$('#reportscount').show();
									$('#filterExportExcel').addClass('hidden');
								} else if (params.exportExcel && toTotal >= totalCount) {
									eventHandler.bulkedit.export.exportExcel({
										'tableID': 'chartReportTable',
										'excludeRow': '.collapse',
										'excludeColumn': '.reportCheckBox'
									})
									//To stop the loading icon
									$('.la-container').css('display', 'none');
								}
							},
							error: function (respData) {
								$('.la-container').css('display', 'none');
								console.log(respData);
							}
						});
					} else if (param.data) {}
				}, 0);

			}
		},
		bulkSave: function (param) {

		}
	}

$('body').on({
	dragenter: function (e) {
		e.preventDefault();
	},
	dragover: function (e) {
		e.preventDefault();
		return false;
	},
	dragleave: function (e) {},
	drop: function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length) {
			var param = {
				'file': e.originalEvent.dataTransfer.files[0]
			}
			tempFileList[0] = e.originalEvent.dataTransfer.files[0];
			$('#uploader .fileList .file-name').remove();
			$('#uploader .fileList').append('<div class="file-name">' + e.originalEvent.dataTransfer.files[0].name + '</div>');
			$('#upload_model .upload_button').removeClass('disabled');
			var dataType = $('#upload_model').attr('data-upload-type');
			if ($('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').length > 0) {
				$('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').removeClass('hidden');
			}
		}
	}
}, '#uploader');
$('body').on('change', '#uploader input', function (e) { // code
	if ($(this)[0].files.length > 0) {
		$('#uploader .fileList .file-name').remove();
		$('#uploader .fileList').append('<div class="file-name">' + $(this)[0].files[0].name + '</div>');
		$('#upload_model .upload_button').removeClass('disabled');
		var dataType = $('#upload_model').attr('data-upload-type');
		if ($('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').length > 0) {
			$('#upload_model').find('.modal-caption[data-type="' + dataType + '"]').removeClass('hidden');
		}
	}
});
$('body').on('click', '#uploader .fileChooser', function () {
	$('#uploader input:file').val('');
	$('#uploader .fileList .file-name').remove();
	$('#uploader .upload_button').addClass('disabled');
	$(this).parent().find('input').trigger('click');
});

$('body').on('click', '.filemanager .folder', function (e) { // code
	if ($(e.target).closest('a[href]').length > 0) {
		var url = $(e.target).closest('a[href]').attr('href');
		var win = window.open(url, '_blank');
		win.focus();
		e.preventDefault();
		e.stopPropagation();
	} else if ($(e.target).closest('.file').length > 0) {
		var url = $(e.target).closest('.file').find('a[href]').attr('href');
		var win = window.open(url, '_blank');
		win.focus();
		e.preventDefault();
		e.stopPropagation();
	} else {
		$(this).parent().find('> li').addClass('non-active');
		$(this).removeClass('non-active').addClass('active');
		$('.filemanager').find('.nav-wrapper .breadcrumb').append('<a data-id="' + $(this).attr('id') + '" href="javascript:;" class="breadcrumb-item">' + $(this).find('> .name').text() + '</a>');
		e.preventDefault();
		e.stopPropagation();
	}
});

$('body').on('click', '.filemanager .nav-wrapper .breadcrumb-item', function () { // code
	$(this).nextAll().remove();
	if ($(this)[0].hasAttribute('data-id')) {
		var dataID = $(this).attr('data-id');
		$('.filemanager li[id="' + dataID + '"]').find('li').removeClass('non-active').removeClass('active');
	} else {
		$('.filemanager li').removeClass('non-active').removeClass('active');
	}
});

$('body').on('click', '.email-content .mail-subject', function () {
	$(this).closest('.email-content').toggleClass('toggle');
});
$('body').on('click', '.note-head', function () {
	$(this).closest('.jsonresp').toggleClass('toggle');
});
$('body').on('click', '.jsonresp:not(:has("div.note-head"))', function () {
	$(this).closest('.jsonresp').toggleClass('toggle');
});

$("#repchkboxall").change(function () { //"select all" change
	if ($(this).prop("checked") == false) {
		$(".repchkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
	} else {
		$(".repchkbox:visible").prop('checked', $(this).prop("checked"));
	}
});
$('.repchkbox').change(function () {
	//uncheck "select all", if one of the listed checkbox item is unchecked
	if (false == $(this).prop("checked")) { //if this item is unchecked
		$("#repchkboxall").prop('checked', false); //change "select all" checked status to false
	}
	//check "select all" if all checkbox items are checked
	if ($('.repchkbox:checked').length == $('.repchkbox').length) {
		$("#repchkboxall").prop('checked', true);
	}
});
//this function is not working for newly added checkbox insted of this i added updateCheckbox() function
/*$(".repchkbox").on('change',function() {
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
        $("#repchkboxall").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.repchkbox:checked').length == $('.repchkbox').length ){
        $("#repchkboxall").prop('checked', true);
    }
});*/
function updateCheckbox(targetNode) {
	if (false == $(targetNode).prop("checked")) { //if this item is unchecked
		$("#repchkboxall").prop('checked', false); //change "select all" checked status to false
	}
	//check "select all" if all checkbox items are checked
	if ($('.repchkbox:checked').length == $('.repchkbox').length) {
		$("#repchkboxall").prop('checked', true);
	}
}
$('.modal-content').on('click', function (target) {
	if (/datepicker/i.test($(target.target).attr('class')) || /datepicker/i.test($(target.target).parent().attr('class'))) {

	} else {
		$('.datepicker').closest('span').hide();
	}
});

function reLoadNotification(doi, header, body) {
	$('#notifyBox').hide();
	$('#reloadconfirm #infoheader').html('Job status for ' + doi + ':' + header);
	$('#reloadconfirm #reloadinfobody').html('Reason: ' + body + ' <br/><br/>Do you want to re-try loading this article?<br/><br/><span class="action"><span class="btn" style="margin-right: 1rem;background-color:#81c784;height: 2rem;" onclick="$(\'#reloadconfirm\').hide();eventHandler.components.actionitems.getftpFilename(\'' + doi + '\')">Yes</span><span class="btn" style="background-color: #feb74d;height: 2rem;" onclick="$(\'#reloadconfirm\').hide()">No</span></span>')
	$('#reloadconfirm').show();
}

function showArticleStatus(customer, jobID, notificationID, doi, status) {
	var jobStatus = status.status;
	var log = status.log;
	var logLen = status.log.length;
	var reason = log[logLen - 1];
	if (typeof (reason) == "object") reason = reason.log.body.status.message.status.message;
	reLoadNotification(doi, jobStatus, reason);
}
window.onerror = function (msg, url, line, col, error) {
	// Note that col & error are new to the HTML 5 spec and may not be
	// supported in every browser.  It worked for me in Chrome.
	var extra = !col ? '' : '\ncolumn: ' + col;
	extra += !error ? '' : '\nerror: ' + error;
	//it will give flow of function calling.
	var flow = error.stack ? "\nFlow:"+error.stack : "";
	var browser = '';
	// Chrome 1+
	if(!!window.chrome && !!window.chrome.webstore){
		browser = "Chrome";
	}else if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){// Opera 8.0+
		browser = "Opera";
	}else if(typeof InstallTrigger !== 'undefined'){// Firefox 1.0+
		browser = "Firefox";
	}else if(/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){// Safari 3.0+ "[object HTMLElementConstructor]"
		browser = "Safari";
	}else if(/*@cc_on!@*/false || !!document.documentMode){// Internet Explorer 6-11
		browser = "Internet Explorer";
	}else if(!(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia){// Edge 20+
		browser = "Edge";
	//for blink we need to check like this.
	}else if(((!!window.chrome && !!window.chrome.webstore) || ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)) && !!window.CSS){// Blink engine detection
		browser = "Blink";
	}else{
		browser = "UnKnown";
	}
	//it will give what browser they are using.(if not chrome and mozilla it will print others)
	//var browser = (!!window.chrome && !!window.chrome.webstore)?"chrome":(typeof InstallTrigger !== 'undefined')? "Mozilla":"other";
	// You can view the information in an alert to see things working like this:
	var errorMsg = "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra + "\nUser:" + userData.kuser.kuname.first + "\nEmail:" + userData.kuser.kuemail + "\n" + window.location.href+flow+"\n"+browser;
	console.log(errorMsg);
	// TODO: Report this error via ajax so you can keep track
	//       of what pages have JS issues
	var parameters = {
		'log': errorMsg,
		'sendEmail': true,
		'subject': 'Kriya2: JavaScript Exception in Dashboard',
		'type': 'Kriya2: JavaScript Exception',
		'mailTo': 'crest@exeterpremedia.com'
	};
	eventHandler.common.functions.writeLog('logerrors', parameters, function (res) {
		console.log('Data Saved');
	})
	$('.la-container').css('display', 'none');
	var suppressErrorAlert = true;
	/*PNotify.removeAll();
	var notifyObj = {};
	notifyObj.notify = {};
	notifyObj.notify.notifyTitle = 'Error Message';
	notifyObj.notify.notifyText = "Sorry, we're unable to process your request. The error information sent to support team. Please refresh and try again.";
	notifyObj.notify.notifyType = 'error';
	eventHandler.common.functions.displayNotification(notifyObj);*/
	// If you return true, then error alerts (like in older versions of
	// Internet Explorer) will be suppressed.
	return suppressErrorAlert;
};
$('body').on({
	click: function (evt) {
		var $tbl = $("#chartReportTable");
		var index = parseInt($(this).attr("index"));
		if ($(this).prop("checked")) {
			$tbl.find('tr :nth-child(' + (index) + ')').removeClass('reportCheckBox');
			$tbl.find('tr :nth-child(' + (index) + ')').show();
		} else {
			$tbl.find('tr :nth-child(' + (index) + ')').addClass('reportCheckBox');
			$tbl.find('tr :nth-child(' + (index) + ')').hide();
		}
	},
}, '#reportColHideShow input:checkbox');

$(document).ready(function () {
	$(function () {
		eventHandler.dropdowns.project.reportDateRangePicker();
		$('#manuscriptsData')
        .on('mouseenter', '.sectionDataChild', function(e){
            // if you do not have another article following the current article then movedown button should not be visible
            if ($(this).next().hasClass('sectionDataChild')){
             //   $('.sectionDataChild').not('.hide').find('.controlIcons.icon-arrow_downward').removeClass('hide');
            }
            else{
               // $(this).find('.controlIcons.icon-arrow_downward').addClass('hide');
            }
        })
        .on('dragenter', '.sectionDataChild', dragEnter)
        .on('dragleave', '.sectionDataChild', dragLeave)
        .on('dragover', '.sectionDataChild', dragOver)
        .on('drop', '.sectionDataChild', drop)
	.on('drop', '.sectionData', drop)
        .on('dragenter', '.sectionData', dragEnter)
        .on('dragleave', '.sectionData', dragLeave)
        .on('dragover', '.sectionData', dragOver)
        .on('dragenter', '.sectionHeader', dragEnter)
        .on('dragleave', '.sectionHeader', dragLeave)
        .on('dragover', '.sectionHeader', dragOver)
	$('#sectionContent')
        .on('dragstart', '.uaa', dragStart)
        .on('dragend', '.uaa', dragEnd)
	});
});

$('body').on('click', function () {
	$('#searchdoi').hide();
})

$('body').on('click','#manuscriptsData .msCard', function () {
	$(this).siblings().removeClass('highlightCard');
	$(this).addClass('highlightCard');
	if ($('#filterCustomer .filter-list.active').attr('data-type') == 'book'){
		eventHandler.components.actionitems.showRightPanel({'target': 'manuscript'}, $(this))
		$('#rightPanelContainer').find('[data-component-type="mspdf"]').attr('data-r-uuid', $(this).attr('data-uuid'));
		$('#rightPanelContainer').find('[data-component-type="mspdf"]').attr('data-dc-doi', $(this).attr('data-doi'));
	}
});

// Form validation for adding new books for book customer in dashboard.
$('#addProj input').on('focusin', function () {
	$('#addProj #bookTitle').on('focusout input', function () {
		if ($('#addProj #bookTitle').val().trim() != '') {
			$('#addProj #bookTitle').removeClass('is-invalid');
		} else {
			$('#addProj #bookTitle').addClass('is-invalid');
			return false;
		}
	})
	$('#addProj [data-cloned="true"] [data-name]#corresAuthor,#addProj [data-cloned="true"] [data-name]#corresEditor').on('focusout input', function () {
		if ($(this).val().trim() != '') {
			$(this).removeClass('is-invalid');
		} else {
			$(this).addClass('is-invalid');
		}
	})
})


// Form validation for adding new chapters, backmatter, epub for books in dashboard.
$('#addChapter input').on('focusin', function () {
	$('#addChapter [data-validate="true"]').on('focusout input', function () {
		if ($(this).attr('type') != 'file' && $(this).val().trim() != '') {
			$(this).removeClass('is-invalid');
		} else if ($(this).attr('type') == 'file') {
			if ($(this).attr('accept') == '.zip' &&	$(this)[0].files.length && /zip/.test($(this)[0].files[0].type)) {
				$(this).removeClass('is-invalid');
				return false;
			} else if ($(this).attr('accept') == '.epub' &&	$(this)[0].files.length && $(this)[0].files[0].type == "application/epub+zip") {
				$(this).removeClass('is-invalid');
				return false;
			} else {
				$(this).addClass('is-invalid');
				return false;
			}
		} else {
			$(this).addClass('is-invalid');
			return false;
		}
	})
})
window.addEventListener("resize", function () {
	$('.dashboardTabs.active .windowHeight').css('height', (window.innerHeight - ($('.dashboardTabs.active .windowHeight').offset().top + 30) - $('#footerContainer').height()) + 'px');
});