﻿var config = {
    "defaultUnits":"pt",
    "bookmarks":4,
    "baseLeading":11.5,
    "preferredPlaceColumn":0,
    "baseAlignmentItterationLimit":3,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":false,
    "landscapeFloatOnFirstPage":false,
	"articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY,KEY_BACK",
    "minStackSpace":18,
    "placeStyle":"Sandwich",
	"figCaptionMinLines": 0,
    "minNoLinesOnPag":4,
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.5,
    "applyTableBorderColor":'Black',
    "pageSize":{
        "width":595.276,
        "height":782.362
    },
	"exportPDFpreset":{
		"print":{
            "crop_marks":true, 
            "bleed_marks":true, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":8.504
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            },
        "cover":{
            "print":{
                "crop_marks":true,
                "bleed_marks":false,
                "registration_marks":true,
                "colour_bars":false,
                "page_information":false,
                "offset":8.504
                },
            "online":{
                "crop_marks":false, 
                "bleed_marks":false, 
                "registration_marks":false, 
                "colour_bars":false, 
                "page_information":false,
                "offset":0
                }
            },
        "toc":{
            "print":{
                "crop_marks":true,
                "bleed_marks":false,
                "registration_marks":true,
                "colour_bars":false,
                "page_information":false,
                "offset":8.504
                },
            "online":{
                "crop_marks":false, 
                "bleed_marks":false, 
                "registration_marks":false, 
                "colour_bars":false, 
                "page_information":false,
                "offset":0
                }
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":18,
      "right":18,
      "gutter":25.512
    },
	"geoBoundsVerso":[45.354,68.03,504.567,663.307
    ],
    "geoBoundsRecto":[45.354,68.03,504.567,663.307
    ],
	"articleTypeDetails":{
    "undefined":"LAYOUT1",
    "jgv":"LAYOUT1",
    "acmi":"LAYOUT1",
    "ijsm":"LAYOUT2",
    "jmm":"LAYOUT3",
    "jmmcr":"LAYOUT4",
    "mgen":"LAYOUT5",
    "mic":"LAYOUT6"
    },
"pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
        "LAYOUT2":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
        "LAYOUT3":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
        "LAYOUT4":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
        "LAYOUT5":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            },
        "LAYOUT6":{        
            "openerPageColumnDetails":[
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":627.874,"floatsCited":false,"gutter":25.512}
                ],
                "columnDetails":[
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":0},
				{"width":239.528, "height":663.307,"floatsCited":false,"gutter":25.512}
                  ],
                "openerPageMargin":{
                "top":103.465,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                },
            "otherPageMargin":{
				"top":68.031,
				"bottom":51.024,
				"inside":45.354,
				"outside":45.354
                }
            }
    },
	"floatRuleSeparator":{//this object will be considered when a rule separator is required 
        "jrnlFigBlock":{
            "fromBottom":{
                "weight":0.5,
                "offset":12,
                "color":"Black",
                "frameIndent":6
                },
            },
        "jrnlTblBlock":{
            "fromBottom":{
                "weight":0.5,
                "offset":12,
                "color":"Black",
                "frameIndent":6
                },
            }
        },
	"jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":336.378, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "textAndFloatSplitRule":{
      "jrnlFigBlock":{
        "top":{
            },
        "bottom":{
            },
        "stack":{
            }
      }
    },
    
    "adjustFigCaptionForSingleColumn":{
        "reduceIndentTo":0
    },
    
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
	"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":[40,45,50],
            "maximum":[60,70,80]
            }
    },
	"detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jrnlArtTitle,jrnlArtType,jrnlAbsHead,jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlHead3",
        "limitationPercentage":[6,9,12]
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":[30,35,40],
            "maximum":[60,70,80]
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "firstPageFrameOnBottomXpath": "//div[@class='jrnlStubBlock']",
    "relinkFigures":['LOGO_1.eps'],
    "replaceColors":['BASECOLOR'],
	"nonDefaultLayers":["PRIM_SUR"],
	"assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRHAG']", "frame-name":"RRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRHAG']", "frame-name":"LRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCRRH']", "frame-name":"CRRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCLRH']", "frame-name":"CLRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlMetaInfo']", "frame-name":"METAINFO", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlRelatedInfo']", "frame-name":"RELATED_ARTICLE_INFO", "action":"move", "styleOverride":null}
       ]       
     },
	  "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,8,40"
                    }
                }            
            },
        "jgv":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"159,198,59"
                    }
                }            
            },
        "ijsm":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"248,172,39"
                    }
                }            
            },
        "jmm":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"209,8,40"
                    }
                }            
            },
        "jmmcr":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"133,82,156"
                    }
                }            
            },
        "mgen":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,161,116"
                    }
                }            
            },
        "mic":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"124,206,243"
                    }
                }            
            },
        "acmi":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"219,62,177"
                    }
                }            
            }
        },
	      "tableSetter" :{
            "thead": {
                "css" : {
                    "font-family" : "'MinionPro-Bold', Times New Roman",
                    "font-size" : "7.5px",
                    "line-height" : "9.5px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionProBold.otf",
                "align" : "left",
                "valign": "top"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'MinionPro-Regular', Times New Roman",
                    "font-size" : "7.5px",
                    "line-height" : "9.5px",
                    "padding" : "0px 6px 0px 6px",
                },
                 "fontPath"  : "MinionPro/MinionProRegular.otf",
                "align" : "left",
                "valign": "top"
            }
        },
      "equation" : {
          "default" : {
              "page":{
                  "size" : "595.276"
              },
              "font" : {
                  "size"    : "9pt",
                  "ledding" : "12pt",
                  "path"    : "/MinionPro/",
                  "ext"     : "otf",
                  "bold"    : "MinionProBold",
                  "italic"  : "MinionProItalic",
                  "bold_italic":"MinionProBoldItalic",
                  "main"    : "MinionProRegular"
              }
           },
        },
    "stubColObj":{
        "top": {
            "STUB_COLUMN": false,
            "STUB_STMT": false
        }	
    },
   "tocDetails":{
       "tocAdjustParagraphSpaceStyleList":{
            "increase":{
                "before":"jrnlArticleType",
                "after":"jrnlAuthors"
            },
            "softBreak":["jrnlArtTitle"],                
            "limitationPercentage":{
                "minimum":40,
                "maximum":60
            },
            "pointSizeModify":{                
                "maximum":0.5
            }
        },
       "fontFamily":{
            "DINNextLTPro":{
                "Medium":{  
                "roman":"Medium",
                "bold":"Bold",
                "italic":"Medium Italic",
                "bolditalic":"Bold Italic"
                },
                "LightItalic":{  
                "roman":"Light Italic",
                "bold":"Medium Italic",
                "italic":"Light Italic",
                "bolditalic":"Medium Italic"
                },
                "Light":{
                "roman":"Light",
                "bold":"Medium",
                "italic":"Light Italic",
                "bolditalic":"Medium Italic"
                },
                "Regular":{  
                "roman":"Roman",
                "bold":"Bold",
                "italic":"Italic",
                "bolditalic":"Bold Italic"
                },
                "Italic":{
                "roman":"Roman",
                "bold":"Bold Italic",
                "italic":"Bold Italic",
                "bolditalic":"Bold Italic" 
                },
                "Bold":{
                "roman":"Roman",
                "bold":"Bold Italic",
                "italic":"Bold Italic",
                "bolditalic":"Bold Italic" 
                }  
            }
        }
    },
    "coverDetails":{
        "spineCrop":60
        }
}
this.config = config;