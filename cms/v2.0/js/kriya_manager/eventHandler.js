
/**
* eventHandler - this javascript holds all the functions required for Reference handling
*				 so that the functionalities can be turned on and off by just calling the required functions
*				 Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	
	settings.subscriptions = {};
	settings.subscribers = ['menu','query','components','welcome'];
	settings.tempFileList = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel', 
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);
			
			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			
			elements = $(event.target);
			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}
								
								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.notification = function(param){
		var notice = $('<div class="kriya-notice ' + param.type + '" />');
		notice.append('<div class="row kriya-notice-header" />');
		notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');
		if(param.icon){
			notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
		}
		if(param.title){
			notice.find('.kriya-notice-header').append(param.title);
		}
		if(param.closeIcon != false){
			notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="eventHandler.removeNotify(this)"></i>');
		}
		Materialize.toast(notice[0].outerHTML, param.timeout);
	},
	eventHandler.removeNotify = function(target){
		if(target){
			$(target).closest('.toast').fadeOut(function(){
				$(this).remove();
			});
		}
	},
	eventHandler.menu = {
		board: {
			onload: function(param, targetNode){
				$.ajax({
					type: "GET",
					url: "/api/customers",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						if (msg){
							var customersList = eventHandler.menu.board.getCustomers(msg);
						}else{
							return null;
						}
					},
					error: function(xhr, errorType, exception) {
						  return null;
					}
				});
				$('#dataContainer').css('height', (window.innerHeight - $('#dataContainer').offset().top)+ 'px');
				$('#jobContainer').css('height', (window.innerHeight - $('#jobContainer').offset().top)+ 'px');
				$('body').on('click', '.workflow-panel svg rect', function(){
					var stageName = $(this).attr('data-stage');
					$('.workflow-panel svg g[data-path]').css('opacity', '0.3');
					$('.workflow-panel svg g[data-return-path]').css('opacity', '0.3');
					$('.workflow-panel').find('.wf-opt,.trigger-opt').remove();
					$('.workflow-panel svg rect').removeAttr('data-selected').removeAttr('data-child-selected');
					if (!$(this)[0].hasAttribute('data-selected')){
						$(this).attr('data-selected', 'true');
						$('.workflow-panel svg rect[data-parent-stage*=" ' + stageName + ' "]').each(function(){
							$(this).attr('data-child-selected', 'true');
							var moreOpt = $('<span data-channel="menu" data-topic="components" data-event="click" data-message="{\'funcToCall\': \'openWFTrigger\'}" data-stage-name="' + $(this).attr('data-stage') + '" class="trigger-opt" style="top: ' + (parseInt($(this).attr('y'))+1) + 'px;left: ' + (parseInt($(this).attr('x')) + parseInt($(this).attr('width')) - 24) + 'px;"><i class="material-icons">settings_applications</i></span>');
							$('.workflow-panel .wf-modal').append(moreOpt);
						})
						$(this).parent().find('g[data-path^="' + stageName + ' to "]').css('opacity', '1');
						$(this).parent().find('g[data-return-path^="' + stageName + ' to "]').css('opacity', '1');
						var moreOpt = $('<span data-stage-name="' + stageName + '" class="wf-opt" style="top: ' + (parseInt($(this).attr('y'))+1) + 'px;left: ' + (parseInt($(this).attr('x')) + parseInt($(this).attr('width')) - 24) + 'px;"><i class="material-icons">more_vert</i></span>');
						$('.workflow-panel .wf-modal').append(moreOpt)
					}
				})
				/*$('body').on('mouseup', '.workflow-panel svg rect', function(e) {
					console.log('released');
					$('rect').removeAttr('data-drag');
				});
				$('body').on('mousedown', '.workflow-panel svg rect', function(e) {
					$(this).attr('data-drag', 'true');
					console.log('clicked')
				});
				$('body').on('mousemove', function(e) {
					if ($('rect[data-drag]').length > 0){
						var top = e.pageY - $('rect[data-drag]').parent().offset().top;
						var left = e.pageX - $('rect[data-drag]').parent().offset().left;
						$('rect[data-drag]').attr('x', left).attr('y', top);
						$('rect[data-drag]').next('svg').attr('x', left).attr('y', top);
					}
				});
				$('body').on('mouseup', function(e) {
					if ($('rect[data-drag]').length > 0){
						console.log('released');
						var top = e.pageY - $('rect[data-drag]').parent().offset().top;
						var left = e.pageX - $('rect[data-drag]').parent().offset().left;
						$('rect[data-drag]').attr('x', left).attr('y', top);
						$('rect[data-drag]').next('svg').attr('x', left).attr('y', top);
						$('rect').removeAttr('data-drag');
					}
				});*/
				/*$('body').on('mouseleave', 'svg rect', function(){
					var stageName = $(this).attr('data-stage');
					$(this).parent().find('line[data-path^="' + stageName + ' to "]').css('opacity', '0.1')
					$(this).parent().find('line[data-return-path^="' + stageName + ' to "]').css('opacity', '0.1')
				})*/
			},
			removeCustomer: function(param, targetNode){
				var popper = $(targetNode).closest('.userInfo.active');
				var currCus = $(targetNode).closest('tr').find('input[class="customer-name"]').val();
				var currUserFirstName = $(popper).find('input[id="first"]').val();
				var currUserLastName = $(popper).find('input[id="last"]').val();
				var currMailId = $(popper).find('input[id="email"]').val();
				//values to be posted to api to remove current customer
			},
			changeCustomer: function(param, targetNode, selCustomerName){
				if(selCustomerName == undefined){
					$('.la-container').fadeIn();
					$('#filterCustomer').find('.active').removeClass('active');
					$(targetNode).addClass('active');
					$('#customerVal').text($('#filterCustomer').find('.active').text());
					$('#projectVal').text('Project');
					var customer = $(targetNode).attr('data-customer');
				}else{
					var customer = selCustomerName;
				}
				$.ajax({
					type: "GET",
					url: "/api/projects?customerName="+customer,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						$('.la-container').fadeOut()
						if (msg && !msg.error){
							eventHandler.menu.board.getJournals(msg, $(targetNode));
							if(selCustomerName != undefined){
								eventHandler.menu.board.getUsers();
							}
							$('.actions-btn').removeClass('hidden')
						}else{
							  return null;
						}
					},
					error: function(xhr, errorType, exception) {
						  return null;
					}
				});
			},
			changeProject: function(param, targetNode){
				$('#filterProject').find('.active').removeClass('active');
				$(targetNode).addClass('active');
				$('#projectVal').text($('#filterProject').find('.active').text());
			},
			getCustomers: function(param, targetNode){
				customerArr = [];
				var customerArray = param.customer;
				var customersLen = 0;
				if(customerArray){
					customersLen = customerArray.length;
					if(!customersLen || customersLen == 0){
						customerArray[0] = param.customer;
						customersLen = 1;
					}
				}
				if (customersLen > 0) $('#filterCustomer').html('');
				for (var cIndex = 0; cIndex < customersLen; cIndex++) {
					customerType = 'journal';
					if (customerArray[cIndex].type) {
						customerType = customerArray[cIndex].type;
					}
					var customerData = $('<div class="filter-list customer" data-customer="' + customerArray[cIndex].name + '" data-type="' + customerType + '" data-channel="menu" data-topic="board" data-event="click" data-message="{\'funcToCall\': \'changeCustomer\'}">' + customerArray[cIndex].name + '</div>');
					$('#filterCustomer').append(customerData);
				}
				if (customersLen == 1){
					$('#filterCustomer').find('.filter-list.customer').addClass('active');
					$('#customerVal').text($('#filterCustomer').find('.active').text())
					$('#filterCustomer').find('.active').trigger('click');
				}
				return customersLen;
			},
			getJournals: function(param, targetNode){
				journalArr = {};
				var journalArray = param.project;
				var journalsLen = 0;
				if(journalArray) {
					journalsLen = journalArray.length;
					//in case the length is still zero, an object was returned
					if(!journalsLen || journalsLen == 0){
						journalArray[0] = param.project;
						journalsLen = 1;
					}
				}
				if (journalsLen > 0){
					$('#filterProject').html('');
				}
				var pHTML = '';
				for (var pIndex = 0; pIndex < journalsLen; pIndex++) {
					var projectData = $('<div class="filter-list project" data-project-type="journal" data-customer="' + $(targetNode).attr('data-customer') + '" data-project="' + journalArray[pIndex].name + '" data-channel="menu" data-topic="board" data-event="click" data-message="{\'funcToCall\': \'changeProject\'}">' + journalArray[pIndex].name + '</div>');
					$('#filterProject').append(projectData);
				}
				$('#filterProject .filter-list:first').trigger('click');
				//close the list
				return journalsLen;
			},
			getJobTemplate: function(param, targetNode){
				$('.addjob-panel,.workflow-panel').addClass('hidden');
				$('.addjob-panel').removeClass('hidden');
			},
			getWFTemplateOld: function(param, targetNode){
				if ($('#filterProject .active').length == 0) return;
				$('.la-container').fadeIn();
				jQuery.ajax({
					type: "GET",
					url: "/api/getworkflow?customer="+$('#filterCustomer .active').attr('data-customer')+"&project="+$('#filterProject .active').attr('data-project'),
					success: function (msg) {
						$('.la-container').fadeOut()
						if (msg && !msg.error){
							$('#jobContainer [data-panel="true"]').addClass('hidden');
							var wfModal = $(msg);
							$('.workflow-panel').html(msg)
							$('.workflow-panel').removeClass('hidden');
							var wfWidth = $('.workflow-panel').width();
							var svg = $('<svg width="' + wfWidth + '" height="auto"></svg>');
							var y = 20, width = 150, height = 50;
							var x = (wfWidth/2) - 75;
							var l = $(wfModal).find('div[data-stage]').length - 1;
							$(wfModal).find('div[data-stage]').each(function(i,v){
								var exist = false;
								if ($(svg).find('[data-stage="' + $(this).attr('data-stage') + '"]').length > 0) {
									exist = true;
								}
								var parentStageName = $(this).attr('data-stage').replace(/[\s\-]+/g, '').toLowerCase()
								console.log($(this).attr('data-stage'))
								// create a rect-box for current option
								if (! exist){
									var rect = $('<rect x="' + x + '" y="' + y + '" width="' + width + '" height="' + height + '" style="fill:#1565C0;stroke-opacity:0.9" data-stage="' + parentStageName + '"/>');
									$(svg).append(rect);
									var text = $('<svg X="' + x + '" Y="' + y + '" width="' + width + 'PX" height="' + height + 'px"><text x="50%" y="50%" alignment-baseline="middle" fill="white" text-anchor="middle">' + $(this).attr('data-stage') + '</text></svg>');
									$(svg).append(text);
								}
								//create rec-box for the triggers inside option
								var subStages = $(this).find('.signoff-stage').length;
								var newX = (wfWidth - ((width * subStages) + (50 * (subStages-1))))/2;
								var increaseHeight = true;
								$(this).find('.signoff-stage').each(function(i,v){
									var stageName = $(this).text().replace(/[\s\-]+/g, '').toLowerCase();
									if ($(svg).find('[data-stage="' + stageName + '"]').length > 0) {
										if ($(svg).find('[data-stage="' + stageName + '"]')[0].hasAttribute('data-parent-stage')){
											var ps = $(svg).find('[data-stage="' + stageName + '"]').attr('data-parent-stage')
											$(svg).find('[data-stage="' + stageName + '"]').attr('data-parent-stage', ps + parentStageName + ' ')
										}else{
											$(svg).find('[data-stage="' + stageName + '"]').attr('data-parent-stage', ' ' + parentStageName + ' ')
										}
										return true;
									}
									console.log('Sub', stageName)
									if (increaseHeight){
										y = y + height + height;
										increaseHeight = false;
									}
									if (i > 0) newX = newX + width + 50
									var rect = $('<rect x="' + newX + '" y="' + y + '" width="' + width + '" height="' + height + '" style="fill:#1565C0;stroke-opacity:0.9" data-stage="' + stageName + '" data-parent-stage=" ' + parentStageName + ' "/>');
									$(svg).append(rect);
									var text = $('<svg X="' + newX + '" Y="' + y + '" width="' + width + 'PX" height="' + height + 'px"><text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" fill="white" data-stage="' + stageName + '">' + $(this).text() + '</text></svg>');
									$(svg).append(text);
								});
							});
							//<g><path fill="none" stroke="rgb(0,0,0)" d="M 330,140 L 510,140 Q 520,140 520,150 L 520,248" stroke-opacity="1" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"></path><use href="#ygc1_0" transform="matrix(0 -1 1 0 520 253)" style="pointer-events: none;"><path fill="rgb(0,0,0)" stroke="rgb(0,0,0)" d="M 0,0 L 10,-5 L 5,0 L 10,5 Z" fill-opacity="1" stroke-opacity="1" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" id="ygc1_0"></path></use></g>
							$(svg).find('rect[data-stage]').each(function(){
								var parentStage = $(this);
								var parentStageName = $(this).attr('data-stage');
								console.log('parent : ', parentStageName)
								var childStage = $(svg).find('rect[data-parent-stage*=" ' + parentStageName + ' "]');
								$(childStage).each(function(){
									var path = $(this).attr('data-stage') + ' to ' + parentStageName;
									if ($(svg).find('line[data-path="' + path + '"]').length > 0){
										var subPath = parentStageName + ' to ' + $(this).attr('data-stage');
										$(svg).find('line[data-path="' + path + '"]').attr('data-return-path', subPath);
									}else{
										console.log('childs : ', $(this).attr('data-stage'))
										var x1 = parseInt(parentStage.attr('x')) + parseInt(parentStage.attr('width')/2);
										var y1 = parseInt(parentStage.attr('y')) + parseInt(parentStage.attr('height'));
										var x2 = parseInt($(this).attr('x')) + parseInt(parentStage.attr('width')/2);
										var y2 = parseInt($(this).attr('y'));
										if (y1 > y2 && parentStage.attr('y') != $(this).attr('y')){
											y1 = parseInt(parentStage.attr('y'));
											y2 = parseInt($(this).attr('y')) + parseInt(parentStage.attr('height'));
										}
										var path = parentStageName + ' to ' + $(this).attr('data-stage');
										var line = $('<line x1="' + x1 + '" y1="' + y1 + '" x2="' + x2 + '" y2="' + y2 + '" data-path="' + path + '" style="stroke:rgb(255,0,0);opacity:0.2;stroke-width:2"/>');
										$(svg).append(line);
										$(svg).append('<circle cx="' + x2 + '" cy="' + y2 + '" r="2" stroke="none" fill="#f00"/>');
									}
								})
							})
							$(svg).attr('height', (y+height+height));
							$('.workflow-panel').append('<div class="wf-modal">' + $(svg)[0].outerHTML + '</div>');
						}
						else{
							  return null;
						}
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			getWFTemplate: function(param, targetNode){
				if ($('#filterProject .active').length == 0) return;
				$('.la-container').fadeIn();
				$('#leftContainer .side-navbar li').removeClass('active');
				$(targetNode).addClass('active');
				jQuery.ajax({
					type: "GET",
					url: "/api/getworkflow?customer="+$('#filterCustomer .active').attr('data-customer')+"&project="+$('#filterProject .active').attr('data-project'),
					success: function (msg) {
						$('.la-container').fadeOut()
						if (msg && !msg.error){
							$('#jobContainer [data-panel="true"]').addClass('hidden');
							var wfModal = $(msg);
							//$('.workflow-panel').html(msg)
							$('.workflow-panel').find('.wf-modal').remove();
							$('.workflow-panel').removeClass('hidden');
							var svgObj = {'y': 20, 'width': 150, 'height': 50};
							svgObj.wfWidth = $('.workflow-panel').width();
							svgObj.x = (svgObj.wfWidth/2) - 75;
							var svg = $('<svg width="' + svgObj.wfWidth + '" height="auto"></svg>');
							var l = $(wfModal).find('flow > options[name]').length - 1;
							var param = {'wfModal':wfModal, 'svg':svg, 'svgObj':svgObj}
							$(wfModal).find('flow > options[name]').each(function(i,v){
								param.stage = $(this);
								param = eventHandler.menu.board.constructWF(param);
							});
							svg = param.svg;
							svgObj = param.svgObj;
							var colors = ['9C27B0', '3F51B5', 'FFC107', 'F44336', '4CAF50', '795548', '607D8B', '009688', '03A9F4']
							$(svg).find('rect[data-stage]').each(function(w,f){
								/*if (w < colors.length){
									var color = colors[w];
								}else{
									var mod = w%colors.length;
									var color = colors[mod];
								}*/
								var color = 'F44336';
								var parentStage = $(this);
								var parentStageName = $(this).attr('data-stage');
								console.log('parent : ', parentStageName)
								var childStage = $(svg).find('rect[data-parent-stage*=" ' + parentStageName + ' "]');
								var l1 =2, l2 = 2;
								$(childStage).each(function(t,b){
									var path = $(this).attr('data-stage') + ' to ' + parentStageName;
									if ($(svg).find('g[data-path="' + path + '"]').length > 0){
										var subPath = parentStageName + ' to ' + $(this).attr('data-stage');
										$(svg).find('g[data-path="' + path + '"]').attr('data-return-path', subPath);
									}else{
										//console.log('childs : ', $(this).attr('data-stage'))
										var x1 = parseInt(parentStage.attr('x'));
										var y1 = parseInt(parentStage.attr('y'));
										var x2 = parseInt($(this).attr('x'));
										var y2 = parseInt($(this).attr('y'));
										var path = parentStageName + ' to ' + $(this).attr('data-stage');
										if (x1 == x2){
											var s1 = parseInt(parentStage.attr('x')) + parseInt(parentStage.attr('width')/2);
											var s2 = parseInt(parentStage.attr('y')) + parseInt(parentStage.attr('height'));
											var s3 = parseInt($(this).attr('y'));
											var line = '<g data-path="' + path + '"><path fill="none" stroke="#'+color+'" d="M '+s1+','+s2+' L'+s1+', '+s3+'" stroke-opacity="1" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"></path><use href="#ygc1_0" transform="matrix(0 -1 1 0 '+s1+' '+(s3+5)+')" style="pointer-events: none;"><path fill="rgb(0,0,0)" stroke="#'+color+'" d="M 0,0 L 10,-5 L 5,0 L 10,5 Z" fill-opacity="1" stroke-opacity="1" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" id="ygc1_0"></path></use></g>'
										}else {
											if (x1 > x2){
												var s1 = parseInt(parentStage.attr('x'));
												var s2 = parseInt(parentStage.attr('y')) + parseInt(parentStage.attr('height')/2);
												var s3 = parseInt($(this).attr('x')) + parseInt(parentStage.attr('width')/2);
												l1--;
												s2 = s2 - (5*l1)
											}else if (x2 > x1){
												var s1 = parseInt(parentStage.attr('x')) + parseInt(parentStage.attr('width'));
												var s2 = parseInt(parentStage.attr('y')) + parseInt(parentStage.attr('height')/2);
												var s3 = parseInt($(this).attr('x')) + parseInt(parentStage.attr('width')/2);
												l2--;
												s2 = s2 - (5*l1)
											}
											var s4 = y2 + 5;
											var s5 = "-1";
											if (y1 > y2){
												y2 = y2 + parseInt($(this).attr('height'))
												var s4 = y2 - 5;
												var s5 = "1"
											}
											if ($(svg).find('g > path[data-path$=" to ' + $(this).attr('data-stage') + '"]').length == 1){
												s3 = s3+15
											}else if ($(svg).find('g > path[data-path$=" to ' + $(this).attr('data-stage') + '"]').length == 2){
												s3 = s3-15
											}
											//Q '+(s1+10)+','+y1+' +'+(s1+10)+','+(y1+10)+' 
											var line = '<g data-path="' + path + '"><path fill="none" stroke="#'+color+'" d="M '+s1+','+s2+' L '+s3+','+s2+' L '+s3+','+y2+'" stroke-opacity="1" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"></path><use href="#ygc1_0" transform="matrix(0 '+s5+' 1 0 '+s3+' '+s4+')" style="pointer-events: none;"><path fill="rgb(0,0,0)" stroke="#'+color+'" d="M 0,0 L 10,-5 L 5,0 L 10,5 Z" fill-opacity="1" stroke-opacity="1" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" id="ygc1_0"></path></use></g>'
										}
										$(svg).append(line);
										//$(svg).append('<circle cx="' + x2 + '" cy="' + y2 + '" r="2" stroke="none" fill="#f00"/>');
									}
									//return false;
								})
								//return false;
							})
							$(svg).attr('height', (svgObj.y + svgObj.height + svgObj.height));
							$('.workflow-panel').append('<div class="wf-modal" style="position:relative;">' + $(svg)[0].outerHTML + '</div>');
							$(wfModal).find('flow > trigger').each(function(){
								$('.workflow-panel .wf-modal').append($(this));
							})
						}
						else{
							  return null;
						}
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			constructWF: function(param){
				var stageName = $(param.stage).attr('name');
				// create a rect-box for current option
				if ($(param.svg).find('rect[data-stage="' + stageName + '"]').length == 0) {
					if ($(param.svg).find('rect[y="' + param.svgObj.y + '"]').length > 0){
						param.svgObj.y = param.svgObj.y + param.svgObj.height + param.svgObj.height;
					}
					var rect = $('<rect x="' + param.svgObj.x + '" y="' + param.svgObj.y + '" rx="3" ry="3" width="' + param.svgObj.width + '" height="' + param.svgObj.height + '" style="fill:#1565C0;stroke-opacity:0.9" data-stage="' + stageName + '"/>');
					$(param.svg).append(rect);
					var text = $('<svg X="' + param.svgObj.x + '" Y="' + param.svgObj.y + '" width="' + param.svgObj.width + 'PX" height="' + param.svgObj.height + 'px"><text x="50%" y="50%" alignment-baseline="middle" fill="white" text-anchor="middle">' + $(param.stage).attr('name') + '</text></svg>');
					$(param.svg).append(text);
				}
				var svgStage = $(param.svg).find('rect[data-stage="' + stageName + '"]');
				param.svgObj.x = svgStage.attr('x');
				var subStages = $(param.stage).find('p input[data-reviewer]');
				var subStageArray = [];
				var subStagesLen = 0;
				$(subStages).each(function(i,v){
					var childStage = $(this).attr('data-reviewer');
					if ($(param.wfModal).find('flow > trigger[name="' + childStage + '"]').length > 0){
						var subStageFullName = $(param.wfModal).find('flow > trigger[name="' + childStage + '"]').find('> stage').text();
						var subStageName = subStageFullName.replace(/[\s\-]+/g, '').toLowerCase()
						if ($(param.svg).find('[data-stage="' + subStageName + '"]').length > 0) {
							if ($(param.svg).find('[data-stage="' + subStageName + '"]')[0].hasAttribute('data-parent-stage')){
								var ps = $(param.svg).find('[data-stage="' + subStageName + '"]').attr('data-parent-stage')
								$(param.svg).find('[data-stage="' + subStageName + '"]').attr('data-parent-stage', ps + stageName + ' ')
							}else{
								$(param.svg).find('[data-stage="' + subStageName + '"]').attr('data-parent-stage', ' ' + stageName + ' ')
							}
						}else{
							subStageArray.push($(this));
							subStagesLen++;
						}
					}
				})
				var newX = (((parseInt(svgStage.attr('x')) * 2) + param.svgObj.width) - ((param.svgObj.width * subStagesLen) + (50 * (subStagesLen - 1))))/2;
				if (newX < 0){
					var newX = 10;//(param.svgObj.wfWidth - ((param.svgObj.width * subStagesLen) + (50 * (subStagesLen - 1))))/2;
				}
				var increaseHeight = true;
				//create rect box for the options
				$(subStageArray).each(function(i,v){
					var childStage = $(this).attr('data-reviewer');
					if ($(param.wfModal).find('flow > trigger[name="' + childStage + '"]').length > 0){
						var subStageFullName = $(param.wfModal).find('flow > trigger[name="' + childStage + '"]').find('> stage').text();
						var subStageName = subStageFullName.replace(/[\s\-]+/g, '').toLowerCase()
						if (increaseHeight){
							param.svgObj.y = param.svgObj.y + param.svgObj.height + param.svgObj.height;
							increaseHeight = false;
						}
						if (i > 0) newX = newX + param.svgObj.width + 50
						console.log(subStageName)
						var rect = $('<rect x="' + newX + '" y="' + param.svgObj.y + '" rx="3" ry="3" width="' + param.svgObj.width + '" height="' + param.svgObj.height + '" style="fill:#1565C0;stroke-opacity:0.9" data-stage="' + subStageName + '" data-parent-stage=" ' + stageName + ' "/>');
						$(param.svg).append(rect);
						var text = $('<svg X="' + newX + '" Y="' + param.svgObj.y + '" width="' + param.svgObj.width + 'PX" height="' + param.svgObj.height + 'px"><text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" fill="white" data-stage="' + subStageName + '">' + subStageFullName + '</text></svg>');
						$(param.svg).append(text);
						if ($(param.wfModal).find('flow > options[name="' + subStageName + '"]').length > 0){
							//param.stage = $(this);
							//param = eventHandler.menu.board.constructWF(param);
						}
					}
				});
				return param;
			},
			getUsers: function(param, targetNode){
				if(targetNode != undefined){
					if ($('#filterCustomer').find('.active').length == 0) return;
					$('.la-container').fadeIn();
					$('.users-panel .userInfo.active').remove();
					$('.users-panel > *').removeClass('hidden');
					$('#leftContainer .side-navbar li').removeClass('active');
					$(targetNode).addClass('active');
				}
				$('.users-panel .userList div[data-user-data]').remove();
				var currCustomerName;
				if($('#filterCustomer').find('.active').attr('data-customer') == undefined){
					var userDetails = JSON.parse($('#userDetails').attr('data'));
					var authRoles = userDetails.kuser.kuroles;
					for(var cus in authRoles){
						currCustomerName.push(cus);
					}
					currCustomerName = currCustomerName.toString();
					// this case not yet handled in api
					// as we have planned to move user xml from exist db to elastic search
				}else{
					currCustomerName = $('#filterCustomer').find('.active').attr('data-customer');
				}
				jQuery.ajax({
					type: "GET",
					url: "/api/getuserdata?customer="+currCustomerName+ "&role=production",
					success: function (msg) {
						if(targetNode != undefined){
							$('.la-container').fadeOut();
						}
						var userData = JSON.parse(msg).user;
						var ul = userData.user.length;
						var usersList = [];
						$('#jobContainer [data-panel="true"]').addClass('hidden');
						$('.users-panel').removeClass('hidden');
						$('.users-panel').find('.user:not(.template)').remove();
						for (var u = 0; u < ul; u++){
							var user = userData.user[u];
							var accessLevel, pageLimit, assignedPages;
							for(var r in user.roles.role){
								if(r == '_attributes'){
									if(user.roles.role._attributes['customer-name'] == currCustomerName){
										accessLevel = user.roles.role._attributes['access-level'];
										pageLimit = user.roles.role._attributes['page-limit'];
										assignedPages = user.roles.role._attributes['assigned-pages'];
										break;
									}
								}else if(user.roles.role[r]._attributes['customer-name'] == currCustomerName){
									accessLevel = user.roles.role[r]._attributes['access-level'];
									pageLimit = user.roles.role[r]._attributes['page-limit'];
									assignedPages = user.roles.role[r]._attributes['assigned-pages'];
									break;
								}
							}
							usersList.push({
								'userobj':encodeURIComponent(JSON.stringify(user)),
								'userName':user.name.first._text,
								'emailid':user.email._text,
								'accesslevel':accessLevel,
								'pagelimit':pageLimit,
								'assignedpages':assignedPages
							});
						}
						var pagefn = doT.template(document.getElementById('userslist').innerHTML, undefined, undefined);
						$('.users-panel .userList > div').remove();
						$('.users-panel .userList').append(pagefn(usersList));
						$('.user label').addClass('active');
						eventHandler.menu.components.userCustomerOptions();
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			getRulesets: function(param, targetNode){
				if ($('#filterProject .active').length == 0) return;
				$('.la-container').fadeIn();
				$('#leftContainer .side-navbar li').removeClass('active');
				$(targetNode).addClass('active');
				jQuery.ajax({
					type: "GET",
					url: "/api/getrulesets",
					success: function (msg) {
						//$('.la-container').fadeOut();
						$('#jobContainer [data-panel="true"]').addClass('hidden');
						$('.rulesets-panel').find('.ruleset').remove();
						$(msg).find('stylename').each(function(){
							var ruleName = $(this).attr('name');
							if ($('.rulesets-panel').find('.ruleset[data-name="' + ruleName + '"]').length > 0){
								var ruleset = $('.rulesets-panel').find('.ruleset[data-name="' + ruleName + '"]')
							}else{
								var ruleset = $('<div class="row ruleset card" data-name="' + ruleName + '"/>');
								$(ruleset).append('<p class="rule-name">' + ruleName + '</p>');
								$('.rulesets-panel').append(ruleset)
							}
							var ruleNode = $('<div class="rule"><i class="material-icons toggleRuleSelect" data-channel="menu" data-topic="board" data-event="click" data-message="{\'funcToCall\': \'toggleRuleSelect\'}">crop_din</i> </div>');
							var attributes = $(this).prop("attributes");
							$.each(attributes, function() {
								$(ruleNode).attr(this.name, this.value);
							});
							$(ruleset).append(ruleNode);
							$(this).find('function').each(function(i,v){
								if (i == 0){
									$(ruleNode).append('<p class="function-name"></p>');
								}
								var fName = $(this).attr('name');
								if ($('#componentDiv .function[name="' + fName + '"]').length > 0){
									var func = $('#componentDiv .function[name="' + fName + '"]').clone();
									if ($(this)[0].hasAttribute('data-content-before')){
										$(func).prepend($(this).attr('data-content-before'));
									}
									$(ruleNode).find('.function-name').append($(func));
								}else{
									var func = $('<div class="function">' + $(this).html() + '</div>');
									var attributes = $(this).prop("attributes");
									$.each(attributes, function() {
										$(func).attr(this.name, this.value);
									});
									$(ruleNode).find('.function-name').append(func);
								}
							});
							var condCount = $(this).find('replace-patterns > *').length;
							$(this).find('example').each(function(i,v){
								if (i == 0){
									$(ruleNode).append('<div class="example-row row"><div class="example-container row"><h5>Example text</h5></div><div class="description-container row"><h5>Replacement options</h5></div></div>');
									$(this).find('.jrnlPatterns').each(function(){
										var patternName = $(this).attr('data-tag-type');
										if ($(this).closest('styleName').find('replace-patterns ' + patternName).length > 0){
											var replacement = $(this).closest('styleName').find('replace-patterns ' + patternName);
											var pattern = $('<div class="row col s4 pattern" data-type="' + patternName + '"></div>');
											$(pattern).append('<p>' + $(this).attr('data-name') + '</p>');
											var patternTemplate = $('#jobContainer > .pattern-template').clone()
											patternTemplate.removeClass('hidden')
											$(pattern).append(patternTemplate);
											if ($(replacement).find('conditions select').length > 0){
												$(patternTemplate).find('.condition-options').append($(replacement).find('conditions').html());
											}else{
												$(patternTemplate).find('.row.condition .add-condition').html('')
											}
											$(patternTemplate).find('.replace-options').append($(replacement).find('options').html());
											$(ruleNode).find('.description-container').append(pattern);
										}
									})
									/*$(this).find('.jrnlPatterns').each(function(){
										var pattern = $('<div class="row col s6 pattern"></div>');
										$(pattern).append('<p>' + $(this).attr('data-name') + '</p>');
										var patternTemplate = $('.pattern-template').clone()
										patternTemplate.removeClass('hidden')
										$(pattern).append(patternTemplate);
										$(ruleNode).find('.description-container').append(pattern);
									})*/
								}
								$(ruleNode).find('.example-container').append('<p class="example">' + $(this).html() + '</p>');
							})
						})
						$('.rulesets-panel').removeClass('hidden');
						eventHandler.menu.board.getCustomerRules();
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			getCustomerRules: function(){
				if ($('#filterProject .active').length == 0) return;
				$('.la-container').fadeIn();
				$('#leftContainer .side-navbar li').removeClass('active');
				$(targetNode).addClass('active');
				jQuery.ajax({
					type: "GET",
					url: "/api/getrulesets?customer=" + $('#filterCustomer .active').attr('data-customer'),
					success: function (msg) {
						$('.la-container').fadeOut();
						$(msg).find('stylename').each(function(){
							var ruleID = $(this).attr('id');
							var rule = $('.rulesets-panel .rule[id="' + ruleID + '"]');
							rule.find('.toggleRuleSelect').trigger('click');
							$(this).find('function').each(function(){
								var funcName = $(this).attr('name');
								var funcOption = $(this).attr('option');
								if (rule.find('.function-name .function[name="' + funcName + '"]').find('select').length > 0){
									rule.find('.function-name .function[name="' + funcName + '"]').find('select').val(funcOption)
								}else if (rule.find('.function-name .function[name="' + funcName + '"]').find('input').length > 0){
									rule.find('.function-name .function[name="' + funcName + '"]').find('input').val(funcOption)
								}
							});
							$(this).find('replace-patterns > *').each(function(){
								var patternName = $(this)[0].nodeName;
								var pattern = rule.find('.description-container .pattern[data-type="' + patternName + '"]');
								if (pattern.length > 0){
									$(this).find('> options').each(function(){
										if ($(this).attr('condition') == 'default'){
											pattern.find('.row.condition:not([data-type]) .replace-options select').val($(this).text())
										}else{
											pattern.find('.add-condition:first i').trigger('click');
											pattern.find('.row.condition:first .condition-options select').val($(this).attr('condition'))
											pattern.find('.row.condition:first .replace-options select').val($(this).text())
										}
									})
								}
							})
						})
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			saveRulesets: function(param, targetNode){
				var rulesetXML = '<styles>';
				var rulesAdded = false;
				$('.rulesets-panel .ruleset .rule').each(function(){
					if ($(this).find('.toggleRuleSelect[data-type="selected"]').length == 0){
						return true;
					}
					rulesAdded = true;
					rulesetXML += '<styleName';
					var attributes = $(this).prop("attributes");
					$.each(attributes, function() {
						if (! /^(class)$/.test(this.name)){
							rulesetXML += ' ' + this.name + '="' + this.value + '"';
						}
					});
					rulesetXML += '>';
					$(this).find('.function-name .function').each(function(){
						rulesetXML += '<function name="' + $(this).attr('name') + '"';
						if ($(this).find('select').length > 0){
							rulesetXML += ' option="' + $(this).find('select').val() + '"';
						}
						if ($(this).find('input').length > 0){
							rulesetXML += ' option="' + $(this).find('input').val() + '"';
						}
						rulesetXML += '/>';
					});
					if ($(this).find('.description-container .pattern').length > 0){
						rulesetXML += '<replace-patterns>';
					}
					$(this).find('.description-container .pattern').each(function(){
						rulesetXML += '<' + $(this).attr('data-type') + '>';
						$(this).find('.pattern-template .condition').each(function(){
							if ($(this).find('.condition-options').length > 0){
								var ruleCond = $(this).find('.condition-options select').val();
							}else{
								var ruleCond = 'default';
							}
							rulesetXML += '<options condition="' + ruleCond + '">' + $(this).find('.replace-options select').val() + '</options>';
						})
						rulesetXML += '</' + $(this).attr('data-type') + '>';
					});
					if ($(this).find('.description-container .pattern').length > 0){
						rulesetXML += '</replace-patterns>';
					}
					rulesetXML += '</styleName>';
				});
				rulesetXML += '</styles>';
				if (! rulesAdded){
					return;
				}
				$('.la-container').fadeIn();
				var param = {'customer': $('#filterCustomer .active').attr('data-customer'), 'project': $('#filterProject .active').attr('data-project'), 'rulesetXML': rulesetXML}
				$.ajax({
					type: "POST",
					url: "/api/saverulesets",
					data: param,
					success: function (msg) {
						$('.la-container').fadeOut()
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			},
			addCondition: function(param, targetNode){
				var field = $(targetNode).closest('.field');
				var condition = $(field).find('.condition-template').clone(true);
				$(condition).removeClass('condition-template').removeClass('hidden').addClass('condition').attr('data-type', 'condition');
				$(targetNode).closest('.row').before(condition);
			},
			removeCondition: function(param, targetNode){
				$(targetNode).closest('.row').remove();
			},
			toggleRuleSelect: function(param, targetNode){
				if ($(targetNode).attr('data-type') == 'selected'){
					$(targetNode).removeAttr('data-type');
					$(targetNode).text('crop_din');
					$(targetNode).parent().find('select').attr('disabled', 'true');
				}else{
					$(targetNode).attr('data-type', 'selected')
					$(targetNode).text('check_box');
					$(targetNode).parent().find('select').removeAttr('disabled');
				}
			},
			toggleRoles: function(param, targetNode){
				$(targetNode).closest('.user').find('.role-details').toggleClass('hidden')
			},
			openFileExplorer: function(param, targetNode){
				$('.tableClone input:file').val('');
				$(targetNode).parent().find('input:file').trigger('click');
			},
			onFileUpload: function(param, targetNode){
				$('#uploader').find('.i-files').remove();
				var file = targetNode[0].files[0];
				eventHandler.settings.tempFileList = {};
				var cnt = $('#uploader').find('.i-files').length + 1;
				eventHandler.settings.tempFileList[cnt] = file;
				var ftype = file.name.replace(/^.*\./, '');
				var fname = file.name;
				console.log(file);
				$('#uploader .fileList').append('<span class="i-files" id="'+cnt+'"><span type="'+ftype+'"></span>'+fname+'</span>');
				$('#uploader').removeClass('active')
				$('.uploadJob').removeClass('disabled')
			},
			addNewJob: function(param, targetNode){
				eventHandler.menu.upload.uploadFile({'file': eventHandler.settings.tempFileList[1], 'success': eventHandler.menu.board.uploadDigest}, targetNode);
			},
			uploadDigest: function(status){
				if (typeof(status.fileDetailsObj.dstKey) == "undefined") return false;
				var param = {'customer': 'elife', 'project': 'elife', 'doi': 'test.00001', 'key': status.fileDetailsObj.dstKey}
				$.ajax({
					type: "POST",
					url: "/api/uploaddigest",
					data: param,
					success: function (msg) {
						$('.la-container').fadeOut()
						if (msg && !msg.error){
							$('.addjob-panel,.workflow-panel').addClass('hidden')
							$('.workflow-panel').html(msg)
							$('.workflow-panel').find('div[data-stage]').addClass('disabled')
							$('.workflow-panel').find('div[data-stage]').first().removeClass('disabled')
							$('.workflow-panel').removeClass('hidden')
						}
						else{
							  return null;
						}
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut()
						  return null;
					}
				});
			}
		},
		upload: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				//var block = param.block;
				//block.progress('Uploading to server');
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {'filename': file.name},
					success: function(response){
						if (response && response.upload_url){
							var formData = new FormData();
							$.each(response.params, function(k, v){
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.menu.upload.uploadToServer(param, targetNode);
						}
					},
					error: function(err){
						if(param.error && typeof(param.error) == "function"){
							param.error(err);
						}
					}
				});
			},
			uploadToServer: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								console.log('Uploading to server ' + percentComplete);
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								console.log('Uploading to server ' + percentComplete);
							}
						}, false);
						return xhr;
					},
					success: function(res){
						console.log('Converting for web view');
						kriya = {}; kriya.config = {}; kriya.config.content = {};
						kriya.config.content.customer = 'elife';
						kriya.config.content.project = 'elife';
						kriya.config.content.doi = 'test.00001';
				
						if (res && res.hasChildNodes()){
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
										srcBucket: uploadResult.find('Bucket,bucket').text(),
										srcKey: uploadResult.find('Key,key').text(),
										dstBucket: "kriya-resources-bucket",
										dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
										convert: 'false'
									}
								/*if(param.convert){
									params.convert = param.convert;
								}*/
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function(status){
										if(param.success && typeof(param.success) == "function"){
											param.success(status);
										}
									},
									error: function(err){
										if(param.error && typeof(param.error) == "function"){
											param.error(err);
										}
										console.log(err)
									}
								});
							}
						}
					}
				});
			}
		},
		components: {
			saveComponent: function(param, targetNode){
				$('.workflow-trigger').removeClass('selected');
			},
			closePopUp: function(param, targetNode){
				$('.workflow-trigger').removeClass('selected');
			},
			openWFTrigger: function(param, targetNode){
				var stageName = $(targetNode).attr('data-stage-name');
				var trigger = $('.wf-modal trigger[name="' + stageName + '"]');
				$('#componentDiv .workflow-trigger').addClass('selected');
				$('#componentDiv .workflow-trigger .right-tab div[data-type="email-content"][data-cloned]').remove();
				$(trigger).find('trigger[action="sendMail"]').each(function(){
					var clonedEmail = $(this).clone();
					var cloneTemplate = $('#componentDiv .workflow-trigger .right-tab div[data-type="email-content"][data-template]').clone();
					cloneTemplate.removeAttr('data-template').attr('data-cloned', 'true');
					clonedEmail.find('> *').each(function(){
						var nodeName = $(this)[0].nodeName.toLocaleLowerCase();
						if (cloneTemplate.find('[data-type="' + nodeName + '"]').length > 0){
							cloneTemplate.find('[data-type="' + nodeName + '"]').html($(this).html());
						}
					});
					$('#componentDiv .workflow-trigger .right-tab').append(cloneTemplate);
				})
			},
			editUserInfo: function(param, targetNode){
				$('.la-container').fadeIn();
				$('.userInfo.active').remove();
				$('.userList .selected').removeClass('selected');
				$(targetNode).addClass('selected');
				var userInfo = JSON.parse(decodeURIComponent($(targetNode).attr('data-user-data')));
				var userInfoDiv = $('.users-panel .userInfo').clone();
				for (var k in userInfo){
					if (Object.keys(userInfo[k]).length == 1 && Object.keys(userInfo[k]) != 'role'){
						$(userInfoDiv).find('input#' + k).val(userInfo[k]._text);
						$(userInfoDiv).find('input#' + k).parent().find('label').addClass('active');
					}else if (Object.keys(userInfo[k]).length == 1 && Object.keys(userInfo[k]) == 'role'){
						for (var v in userInfo[k]['role']){
							var role;
							if( v != '_attributes'){
								var role = userInfo[k]['role'][v];
							}else{
								var role = userInfo[k]['role'];
							}
							if (role._attributes && role._attributes['customer-name']){
								var roleRow = userInfoDiv.find('.resource-details tbody tr[data-template="true"]').clone();
								userInfoDiv.find('.resource-details tbody').append(roleRow);
								if((role._attributes['customer-name'] != $('#customerVal').text())&&(role._attributes['customer-name'] != 'all')){
									$(roleRow).addClass('hidden');
								}
								roleRow.removeAttr('data-template')
								for (var a in role._attributes){
									if (roleRow.find('.' + a).length > 0){
										if(a == 'role-type' || a == 'access-level'){
											roleRow.find('.' + a).append('<option value="'+role._attributes[a]+'">'+role._attributes[a]+'</option>');
											roleRow.find('.' + a).val(role._attributes[a]);
											roleRow.find('.' + a).attr('disabled','true');
										}else{
											roleRow.find('.' + a).val(role._attributes[a]);
										}
									}
								}
							}
						}
					}else{
						for (var v in userInfo[k]){
							if (Object.keys(userInfo[k][v]).length == 1){
								$(userInfoDiv).find('input#' + v).val(userInfo[k][v]._text).focus();
								$(userInfoDiv).find('input#' + v).parent().find('label').addClass('active');
							}
						}
					}
				}
				userInfoDiv.find('.resource-details tr[data-template="true"]').remove();
				$(userInfoDiv).find('.saveComponent').attr('data-message',"{'funcToCall': 'saveUserInfo', 'param': 'modifiedData'}");
				$(userInfoDiv).addClass('active').removeClass('hidden');
				//$('.users-panel > *').addClass('hidden');
				$('.users-panel').append($(userInfoDiv));
				$('.la-container').fadeOut();
			},
			deactivateUser: function(param, targetNode){
				var popper = $(targetNode).closest('.userInfo.active');
				var userMailId = $(popper).find('[id="email"]').val();
				var firstName = $(popper).find('[id="first"]').val();
				var lastName = $(popper).find('[id="last"]').val();
				var param = {
					'firstName':firstName,
					'lastName':lastName,
					'email':userMailId,
					'dormantuser':'dormant',
					'modifiedData':true
				}
				$.ajax({
					type: "POST",
					url: "/api/updateuserdata",
					data: param,
					success: function (result) {
						$('.la-container').fadeOut();
						$('.users-panel .userInfo.active').remove();
					},
					error: function(err) {
						$('.la-container').fadeOut();
						eventHandler.notification({
							title : 'ERROR',
							type  : 'error',
							content : 'Changes Not Saved',
							icon : 'icon-warning2'
						});
					}
				});
			},
			addNewUser: function(param, targetNode, cutomerName){
				$('.la-container').fadeIn();
				var config = accessConfig;
				var userDetails = $('#userDetails').attr('data');
				if(userDetails != undefined){
					userDetails = JSON.parse(userDetails);
					if(cutomerName != undefined){
						userDetails = userDetails.kuser.kuroles[cutomerName];
					}else{
						userDetails = userDetails.kuser.kuroles[$('#customerVal').text()];
					}
				}else{
					return;
				}
				var userObj = config[userDetails['role-type']];
				var userRoleObj = userObj[userDetails['access-level']];
				if(userRoleObj == undefined){
					$('.la-container').fadeOut();
					eventHandler.notification({
						title : 'ERROR',
						type  : 'error',
						content : "You can't add user",
						icon : 'icon-warning2'
					});
					return false;
				}
				if(cutomerName != undefined){
					$('.userInfo.active .resource-details').find('td [class="customer-name"]').val(cutomerName);
				}else{
					$('.users-panel .userInfo.active').remove();
					var userInfoDiv = $('.users-panel .userInfo').clone();
					$(userInfoDiv).find('h5.first').text('Add User');
					$(userInfoDiv).addClass('active').removeClass('hidden');
					$(userInfoDiv).find('.deactivateUser').addClass('hidden');
					$('.users-panel').append($(userInfoDiv));
					$('.userInfo.active .resource-details').find('td [class="customer-name"]').val($('#customerVal').text());
				}
				$('.userInfo.active tbody tr:not(.hidden) [class="role-type"]').attr('data-access-config',encodeURIComponent(JSON.stringify(userRoleObj)));
				var userRoleObjKey = Object.keys(userRoleObj);
				$(userRoleObjKey).each(function(){
					if($('.userInfo.active tbody tr:not(.hidden) [class="role-type"]').length > 0){
						$('.userInfo.active tbody tr:not(.hidden) [class="role-type"]').append('<option value="'+this+'">'+this+'</option>');
					}
				});
				$('.la-container').fadeOut();
			},
			saveUserInfo: function(param, targetNode){
				var usersNewInfo = {};
				var selectedUser = false;
				$('.userInfo.active .user-details input[id]').each(function(){
					var keyName = $(this).attr('id')
					if ($(this).val() != ''){
						$(this).removeAttr('data-error');
						usersNewInfo[keyName] = $(this).val();
					}else{
						if($(this).attr('data-required')){
							$(this).attr('data-error','true');
						}
					}
				});
				$('.userInfo.active .resource-details tbody tr:not(.hidden)').each(function(){
					$(this).find('td>input, td>select').each(function(){
						var className = $(this).attr('class');
						if ($(this).val() != ''){
							$(this).removeAttr('data-error');
							usersNewInfo[className] = $(this).val();
						}else{
							if($(this).attr('data-required')){
								$(this).attr('data-error','true');
							}
						}
					});
				});
				if ($('.userList .selected').length > 0){
					selectedUser = $('.userList .selected');
				}
				// if any of the required field is empty, then returning
				if($('.userInfo.active .resource-details tbody tr, .userInfo.active .user-details input[id]').find('[data-error]').length > 0){
					return false;
				}
				if(usersNewInfo['page-limit'] != undefined){
					usersNewInfo['assigned-pages'] = 0;
				}
				if(param){
					usersNewInfo['modifiedData'] = 'true';
					eventHandler.menu.components.updateUserData(usersNewInfo,'',true);
				}else{
					$.ajax({
						type: "GET",
						url: "/api/getuserdata?searchUserMail="+usersNewInfo['email'],
						success: function (result) {
							if(result){
								result = JSON.parse(result);
								if($.isEmptyObject(result.user)){
									eventHandler.menu.components.updateUserData(usersNewInfo,true,true);
								}else{
									eventHandler.notification({
										title : 'ERROR',
										type  : 'error',
										content : 'An user already exist with this <b>'+usersNewInfo['email']+'</b> mail id',
										icon : 'icon-warning2'
									});
									return false;
								}
							}
						}
					});
				}
			},
			updateUserData: function(usersNewInfo,newUser,verifyUser){
				var param = {
					'customerName' : usersNewInfo['customer-name'],
					'firstName' : usersNewInfo['first'],
					'lastName'  : usersNewInfo['last'],
					'email' : usersNewInfo['email'],
					'roleType' : usersNewInfo['role-type'],
					'accessLevel' : usersNewInfo['access-level'],
					'skillLevel': usersNewInfo['skill-level'],
					'pageLimit': usersNewInfo['page-limit'],
					'assignedPages': usersNewInfo['assigned-pages'],
					'modifiedData': usersNewInfo['modifiedData']
				};
				if(verifyUser){
					param['verifyUser'] = 'true';
				}
				if(newUser != undefined){
					param['password'] = 'Happy_Authors';
				}
				$.ajax({
					type: "POST",
					url: "/api/updateuserdata",
					data: param,
					success: function (result) {
						$('.la-container').fadeOut();
						$('.users-panel .userInfo.active').remove();
						eventHandler.menu.board.changeCustomer('','',param.customerName);
					},
					error: function(err) {
						$('.la-container').fadeOut();
						if(err && err.status == 400 && err.responseText == 'Not Authorized'){
							eventHandler.notification({
								title : 'ERROR',
								type  : 'error',
								content : 'Authorization Failed',
								icon : 'icon-warning2'
							});
						}else{
							$('.users-panel .userInfo.active').remove();
						}
					}
				});
			},
			searchUserData: function(param, targetNode){
				var searchMailId = $('#searchMail').text();
				var displayedUsers = $('.users-panel .userList [data-user-data]');
				$('.users-panel .userList .highlight').removeClass('highlight');
				$(displayedUsers).each(function(){
					var currUserInfo = JSON.parse(decodeURIComponent($(this).attr('data-user-data')));
					if(currUserInfo.email._text == searchMailId){
						$(this).addClass('highlight');
						return false;
					}
				});
				if($('.users-panel .userList .highlight').length > 0){
					return false;
				}
				$.ajax({
					type: "GET",
					url: "/api/getuserdata?searchUserMail="+searchMailId,
					success: function (result) {
						if(result){
							result = JSON.parse(decodeURIComponent(result));
							if($.isEmptyObject(result.user)){
								eventHandler.notification({
									title : 'ERROR',
									type  : 'error',
									content : 'No user registred with this <b>'+searchMailId+'</b> mail id',
									icon : 'icon-warning2'
								});
								return false;
							}else{
								var user = result.user.user;
								var userName = user.name.first._text;
								if (user.name.last._text) userName += ' ' + user.name.last._text;
								var userDiv = '<div class="row highlight" data-user-data="' + encodeURIComponent(JSON.stringify(user)) + '" data-channel="menu" data-topic="components" data-event="click" data-message="{\'funcToCall\': \'editUserInfo\'}">';
								userDiv += '<div class="col s3">' + userName + '</div>';
								userDiv += '<div class="col s3">'+ user.email._text + '</div>';
								var customerName = $('#filterCustomer').find('.active').attr('data-customer');
								var customerRole = {}
								if (user.roles && user.roles.role){
									for (var r = 0, rl = user.roles.role.length; r < rl; r++){
										var role = user.roles.role[r];
										if (role._attributes && role._attributes['customer-name'] && role._attributes['customer-name'] == customerName){
											customerRole = role._attributes;
											r = rl;
										}
									}
								}
								userDiv += '<div class="col s2">'
								if (customerRole && customerRole['access-level']){
									userDiv += customerRole['access-level'];
								}
								userDiv += '</div>';
								userDiv += '<div class="col s1">'
								if (customerRole && customerRole['page-limit']){
									userDiv += customerRole['page-limit'];
								}
								userDiv += '</div>';
								userDiv += '<div class="col s1">'
								if (customerRole && customerRole['assigned-pages']){
									userDiv += customerRole['assigned-pages'];
								}
								userDiv += '</div>';
								userDiv += '</div>';
								$('.users-panel .userList').append(userDiv);
							}
						}
					}
				});
			},
			closeUserPopUp: function(param, targetNode){
				//$('.userinfo-btn').trigger('click');
				$('.users-panel .userInfo.active').remove();
			},
			changeAcessLevel: function(param, targetNode){
				if($('.userInfo.active tbody tr:not(.hidden) [class="access-level"] option:not([value=""])').length > 0){
					$('.userInfo.active tbody tr:not(.hidden) [class="access-level"] option:not([value=""])').remove();
				}
				var accessObj = $('.userInfo.active tbody tr:not(.hidden) [class="role-type"]').attr('data-access-config');
				accessObj = JSON.parse(decodeURIComponent(accessObj));
				var selRole = $('.userInfo.active tbody tr:not(.hidden) [class="role-type"]').val();
				var selAccess = accessObj[selRole];
				$(selAccess).each(function(){
					if($('.userInfo.active tbody tr:not(.hidden) [class="access-level"]').length > 0){
						$('.userInfo.active tbody tr:not(.hidden) [class="access-level"]').append('<option value="'+this+'">'+this+'</option>');
					}
				});
			},
			userCustomerOptions: function(){
				var currUserData = JSON.parse($('#userDetails').attr('data'));
				var cusList = [];
				var userRolesAndAccess = [];
				var currRoles = [];
				var currAccess = [];
				for(var cus in currUserData.kuser.kuroles){
					cusList.push(cus);
					var roleType = currUserData.kuser.kuroles[cus]['role-type'];
					var accessLevel = currUserData.kuser.kuroles[cus]['access-level'];
					if(userRolesAndAccess.indexOf(roleType+','+accessLevel) < 0){
						userRolesAndAccess.push(roleType+','+accessLevel);
						for(var role in accessConfig[roleType][accessLevel]){
							if(currRoles.indexOf(role) < 0){
								currRoles.push(role);
							}
							for(var access in accessConfig[roleType][accessLevel][role]){
								if(currAccess.indexOf(accessConfig[roleType][accessLevel][role][access]) < 0){
									currAccess.push(accessConfig[roleType][accessLevel][role][access]);
								}
							}
						}
					}
				}
				var pagefn = doT.template(document.getElementById('options').innerHTML, undefined, undefined);
				$('#usersCustomerList, #usersRoleList, #usersAccessLevelList').find('input').remove();
				$('#usersCustomerList, #usersRoleList, #usersAccessLevelList').append('<option value="">none</option>');
				cusList.sort();
				$('#usersCustomerList').append(pagefn(cusList));
				currRoles.sort();
				$('#usersRoleList').append(pagefn(currRoles));
				currAccess.sort();
				$('#usersAccessLevelList').append(pagefn(currAccess));
			},
			addNewCustomer: function(param, targetNode){
				var newInputs = $('.users-panel .userInfo:not(.active) tbody tr[data-template="true"]').clone();
				$('.users-panel .userInfo.active tbody tr').addClass('hidden');
				$('.users-panel .userInfo.active tbody').append(newInputs);
				var selectOptions = $(newInputs).find('select[data-event="change"]').clone();
				$(selectOptions).removeAttr('class');
				$(selectOptions).addClass('customer-name');
				$(selectOptions).attr("data-message","{'funcToCall': 'changeRole'}");
				$(newInputs).find('.customer-name').closest('td').append(selectOptions);
				$(newInputs).find('input.customer-name').remove();
				var userConfig = JSON.parse($("#userDetails").attr('data'));
				var objKeys = Object.keys(userConfig.kuser.kuroles);
				$(objKeys).each(function(){
					if($('.userInfo.active tbody tr:not(.hidden) [class="customer-name"]').length > 0){
						$('.userInfo.active tbody tr:not(.hidden) [class="customer-name"]').append('<option value="'+this+'">'+this+'</option>');
					}
				});

			},
			changeRole: function(param, targetNode){
				var cusName = $('.userInfo.active tr:not(.hidden) .customer-name').val();
				eventHandler.menu.components.addNewUser('','',cusName);
			}
		}
	};
	return eventHandler;

})(eventHandler || {});
