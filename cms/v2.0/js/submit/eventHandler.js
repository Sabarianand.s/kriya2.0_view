/**
* eventHandler - this javascript holds all the functions required for Reference handling
*					so that the functionalities can be turned on and off by just calling the required functions
*					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['form'];
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel',
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);

			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};

    eventHandler.form = {
        action: {
            addNewFileList: function(param, targetNode){
				var actionRow = $(targetNode).closest('.file-list');
				var newRow = actionRow.clone(true);
				newRow.find('.removeFile').removeClass('disabled');
				newRow.find('.fileChooser').html('Browse');
				newRow.find('[type="file"]').val('');
				actionRow.after(newRow);
				$(targetNode).addClass('disabled');
				$('#dataContainer').scrollTop($('#dataContainer').prop("scrollHeight"));
			},
			removeFileFromList: function(param, targetNode){
				var actionRow = $(targetNode).closest('.file-list');
				actionRow.remove();
				if($('.file-list .addFile:not(.disabled)').length == 0){
					$('.file-list:last').find('.addFile').removeClass('disabled');
				}
				$('#dataContainer').scrollTop($('#dataContainer').prop("scrollHeight"));
			},
			notification: function(param){
				var id = uuid.v4();
				var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
				notice.append('<div class="row kriya-notice-header" />');
				notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');
	
				if(param.icon){
					notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
				}
				if(param.title){
					notice.find('.kriya-notice-header').append(param.title);
				}
				
				if(param.closeIcon != false){
					notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="eventHandler.form.action.removeNotify(this)"></i>');
				}
				
				Materialize.toast(notice[0].outerHTML, param.timeout);
				return id;
			},
			removeNotify: function(targetNode){
				if(targetNode){
					$(targetNode).closest('.toast').fadeOut(function(){
						$(this).remove();
					});
				}
			},
			toggleNavTitle: function(param, targetNode){
				if($(targetNode).find('svg').hasClass('active')){
					$(targetNode).find('svg').removeClass('active');
					$(targetNode).find('ul').addClass('hidden');
				}else{
					$(targetNode).find('svg').addClass('active');
					$(targetNode).find('ul').removeClass('hidden');
				}
			},
			emailValidate: function(param, targetNode){
				if ($(targetNode).attr('data-pattern') && $(targetNode).val() != ""){
					var regexp = new RegExp($(targetNode).attr('data-pattern'));
					if (! $(targetNode).val().match(regexp)){
						$(targetNode).parent().find('label').addClass('email-invalid').removeClass('email-valid');
					}else{
						$(targetNode).parent().find('label').addClass('email-valid').removeClass('email-invalid');
					}
				}else{
					$(targetNode).parent().find('label').removeClass('email-valid').removeClass('email-invalid');
				}
			},
			addJobAction: function (param, targetNode) {
				var parameters = new FormData();
				if (typeof (grecaptcha) != 'undefined') {  
					var response = grecaptcha.getResponse();
					if(response.length === 0){
						alert('Please verify you are not robot');
						return false;
					}else{
						parameters.append('g-recaptcha-response', response);
					}
				} 
				
				parameters.append('customer', $('.formContainer').attr('data-customer'));
				parameters.append('project', $('.formContainer').attr('data-project'));

				var validInput = true;
				$(".formContainer [data-required='true']").each(function(){
					$(this).removeClass('is-invalid');
					if ($(this).val() == "") {
						$(this).addClass('is-invalid');
						validInput = false;
					}
					if ($(this).attr('data-pattern')){
						var regexp = new RegExp($(this).attr('data-pattern'));
						if (! $(this).val().match(regexp)){
							$(this).addClass('is-invalid');
							validInput = false;
						}
					}
				});
				if (! validInput){
					return false;
				}
				//parameters.append('doi', $("#article-id").val());
				parameters.append('generateDOI', "true");
				parameters.append('articleTitle', $("#article-title").val());
				parameters.append('addTitle', 'true');
				parameters.append('articleType', $("#article-type").val());
				parameters.append('authorName', $("#corresp-author").val());
				parameters.append('authorEmail', $("#corresp-email").val());
				
				/* ZIP all uploaded files into a zip along with a manifest file */
				var zip = new JSZip();
				var files = $('.file-list .fileContainer input[type="file"]');
				var fileList = '<files>';
				
				var manuscriptCount = 0;
				var validInput = true;
				var mustHaveFiles = [];
				$('.file-list .file-designation:first').find('option').each(function(){
					if ($(this)[0].hasAttribute('data-mandatory')){
						mustHaveFiles.push($(this).text());
					}
				});
				for (var f = 0, fl = files.length; f < fl; f++){
					var currFile = files[f];
					$(files[f]).removeClass('is-invalid');
					if (currFile.files.length == 0){
						$(files[f]).addClass('is-invalid');
						validInput = false;
					}
					var fileType = $(currFile).closest('.row').find('.file-designation').val();
					var fileTypeValue = $(currFile).closest('.row').find('.file-designation option:selected').text()
					var mindex = mustHaveFiles.indexOf(fileTypeValue);
					if (mindex > -1) {
						mustHaveFiles.splice(mindex, 1);
					}
					$(currFile).closest('.row').find('.file-designation').removeClass('is-invalid');
					if (fileType == 'manuscript'){
						if (manuscriptCount > 0){
							$(currFile).closest('.row').find('.file-designation').addClass('is-invalid');
							validInput = false;
						}
						manuscriptCount++;
					}
					if (currFile.files.length > 0){
						fileList += '<file file_name="' + currFile.files[0].name + '" file_size="' + currFile.files[0].size + '" file_designation="' + fileType + '"/>'
						zip.file(currFile.files[0].name, currFile.files[0]);
					}
				}
				fileList += '</files>';
				if (! validInput){
					if (manuscriptCount > 1){
						eventHandler.form.action.notification({
							title : 'ERROR',
							type  : 'error',
							content : 'More than one manuscript was selected.',
							icon : 'icon-warning2'
						});
					}
					return false;
				}
				if (mustHaveFiles.length > 0){
					eventHandler.form.action.notification({
						title : 'Files missing',
						type  : 'success',
						content : 'Please upload the following file type : ' + mustHaveFiles.join(', '),
						icon : 'icon-info'
					});
					return false;
				}

				zip.file('kriya_job_manifest.xml', fileList);
				
				$('.la-container').fadeIn();
				zip.generateAsync({type:"blob"})
					.then(function(content) {
						var fileName = new Date().getTime() + '.zip';
						parameters.append('manuscript', content, fileName);
						$.ajax({
							type: 'POST',
							url: '/api/addjob',
							data: parameters,
							contentType: false,
							processData: false,
							success: function (response) {
								var parameters = {
									'customer'  : $('.formContainer').attr('data-customer'),
									'journalID' : $('.formContainer').attr('data-project'),
									'articleID' : (response && response.doi)?response.doi:'',
									'role'      : 'author',
									'currStage' : 'submit-manuscript',
									'editedTo'        : $("#corresp-email").val()
								};
								$.ajax({
									type : "POST",
									url  : "/api/signoff",
									data : parameters,
									success: function (res) {
										$('.formContainer input').val('');
										$('.formContainer .file-list .fileChooser').html('Browse');
										$('.formContainer .file-list:not(:first)').remove();
										$('.formContainer .file-list .addFile').removeClass('disabled');
										
										$.ajax({
											type: 'GET',
											url: '/logout?key=true',
											success: function (res) {
												$('.la-container').fadeOut();
												$('body .messageDiv .message').html('You have successfully submitted job.<br> Please close the browser to exit the system.');
												$('body .messageDiv').removeClass('hidden');
											},
											error: function(err){
												$('.la-container').fadeOut();
												$('body .messageDiv .message').html('You have successfully submitted job.<br> Please close the browser to exit the system.');
												$('body .messageDiv').removeClass('hidden');
											}
										});
									},
									error: function(err){
										$('.la-container').fadeOut();
										$('body .messageDiv .message').html('You have successfully submitted job.<br> Please close the browser to exit the system.');
										$('body .messageDiv').removeClass('hidden');
									}
								});
							},
							error: function () {
								eventHandler.form.action.notification({
									title : 'ERROR',
									type  : 'error',
									content : 'Failed to create job.',
									icon : 'icon-warning2'
								});
								$('.la-container').fadeOut()
							}
						})
					})
					.catch(function(err){
						eventHandler.form.action.notification({
							title : 'ERROR',
							type  : 'error',
							content : 'Failed to create job.',
							icon : 'icon-warning2'
						});
					})
			}
        }
    }
	return eventHandler;
})(eventHandler || {});

$(document).ready(function(){
	//Initialize the subscriber and publisher
	eventHandler.subscribers.add();
	eventHandler.publishers.add();
	//Set the height to data container
	$('#dataContainer').css('height', (window.innerHeight - $('#dataContainer').offset().top) + 'px');

	window.addEventListener("resize", function(){
		$('#dataContainer').css('height', (window.innerHeight - $('#dataContainer').offset().top) + 'px');
	});

	$('body').on({
		click: function (evt){
			$(this).parent().find('input[type="file"]').trigger('click');
		},
	}, '.formContainer .fileContainer .fileChooser');

	$('body').on({
		change: function (evt){
			var fileObj = evt.target.files[0];
			if(fileObj && fileObj.name){
				$(this).parent().find('.fileChooser').html(fileObj.name);
			}
		},
	}, '.formContainer .fileContainer input[type="file"]');

});

