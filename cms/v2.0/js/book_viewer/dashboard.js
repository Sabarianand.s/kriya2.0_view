// Global Variables
var customer; // Edited the 1st line after 2 commits - Branch 1 - sabari
var customerArr = [];
var journal;
var journalArr = {};
var journalPMArr = {};
var journalPEArr = {};
var bucket;
var bucketArr = [];
var myData;
//Two lines added - this is 1st commit
// Added by sabari
var totalArray = [];
//Another two lines added - This is 2nd commit another branch
//Added by dev-branch 2 - sabari
var totalTypeArray = {};
var totalStageOwnerArray = {};
var totalOwnerStageArray = {};
var totalDOIArray = [];
var totalHoldArray = [];
var totalUrgentArray = [];
var totalPublishedArray = [];
var totalPublishedLastMonthArray = [];
var totalPublishedThisMonthArray = [];
var totalStageArray = {};
var totalRoleArray = {};
var uniqueArray = {};
var stageArray = {};
var roleArray = {};
var holdArray = {};
var urgentArray = {};
var urgentArrayByStage = {};
var publishedArray = {};
var publishedThisMonthArray = {};
var publishedLastMonthArray = {};
var overdueArray = {};
var overdueArrayByStage = {};
var completedArray = {};

var tbCompletedArray = {};
var newArray = {};

//Aging array
var agingArray = {};

//Days array
var daysArray = {};
var chaptersArray = [{url:"read_only/?doi=oemed-2016-104160.rajasekar&customer=bmj&project=oem", title:"Chapter 1"}, {url:"", title:"Chapter 2"}, {url:"", title:"Chapter 3"}, {url:"", title:"Chapter 4"}, {url:"", title:"Chapter 5"}, {url:"", title:"Chapter 6"}];



// Build the stage name mapping to role
var stageNameRoleMap = [];
var custStageCols = [];
var custStageTimes = [];

var roleName;
var typeStageNameArr;
var authStageNameArr;
var pubStageNameArr;
var copyStageNameArr;
var stageNameLen = 0;

var roleTableBar = '';

var t = new Date();
var currDate = (t.getYear() + 1900) + '-' + (t.getMonth() + 1) + '-' + t.getDate()
//Typesetter role and stages
roleName = "Typesetter";
typeStageNameArr = ["Add Job", "File Download", "Convert Files", "Corrections", "Post author Validation", "Pre-editing", "Preediting QA", "Typesetter Check", "Typesetter QA", "Typesetter Review", "Upload for Online First", "Moved to Archive", "Validation Check", "PAP Resupply", "Content-Loading", "Hold"];

stageNameLen = typeStageNameArr.length;
for (i = 0; i < stageNameLen; i++) {
	stageNameRoleMap[typeStageNameArr[i].toLowerCase()] = roleName;
}
//Copyeditor role and stages
roleName = "Copyeditor";
copyStageNameArr = ["Await CE", "Waiting for CE", "Copyediting", "Copyediting QC", "Copyediting QA", "Copyediting Check", "Proofreading"];
stageNameLen = copyStageNameArr.length;
for (i = 0; i < stageNameLen; i++) {
	stageNameRoleMap[copyStageNameArr[i].toLowerCase()] = roleName;
}
//Author role and stages
roleName = "Author";
authStageNameArr = ["Author proof", "Author Review", "Author Revision", "Revised proof to author"];
stageNameLen = authStageNameArr.length;
for (i = 0; i < stageNameLen; i++) {
	stageNameRoleMap[authStageNameArr[i].toLowerCase()] = roleName;
}
//Publisher role and stages
roleName = "Publisher";
pubStageNameArr = ["Deliver digest", "Editor Review", "Feature Review", "Final Deliverables", "New Version", "Production QC", "Publisher Check", "Publisher Review", "Ready for Publication", "Revises", "Silent correction", "Silent Resupply", "Official correction","Ready for Online","Resupply Online"];
stageNameLen = pubStageNameArr.length;
for (i = 0; i < stageNameLen; i++) {
	stageNameRoleMap[pubStageNameArr[i].toLowerCase()] = roleName;
}

//var stageCols = pubStageNameArr.concat(authStageNameArr,copyStageNameArr,typeStageNameArr);
var roleCols = ["Typesetter","Copyeditor","Author","Publisher"];


//Array of stages and SLAs for each stage by customer
// IMPORTANT - This is a bad hardcode and is to be taken out once the workflow code is available
/*
custStageCols["bir"] = ["Add Job","Content-Loading", "Pre-editing","Copyediting", "Typesetter Check","Publisher Check","Deliver digest","Author Review","Post Author Validation","Feature Review","Publisher Review","Typesetter Review","Validation Check","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New Version", "Official correction"];
custStageTimes["bir"] = [0,0,1,0,2,0,1,3,3,3,2,1,1,1,1,1,1,1,1,1,1];
custStageCols["bmj"] = ["Content-Loading", "Pre-editing","Waiting for CE","Copyediting", "Copyediting QC","Typesetter QA","Publisher check","Author proof","Editor Review","Production QC","Corrections","Revises","Author Revision","Validation Check", "Ready for Online", "Upload for Online First", "Moved to Archive", "Sent to typesetter for correction", "PAP Resupply", "Reuploaded for Online First", "Article Bank / Published", "Bank", "Revised proof to author"];
custStageTimes["bmj"] = [0,1,0,1,1,1,2,2,2,2,1,1,2,1,1,0,1,1,1,1,1,1,2];
custStageCols["elife"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Deliver digest","Author Review","Post Author Validation","Feature Review","Publisher Review","Typesetter Review","Validation Check","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New Version", "Official correction"];
custStageTimes["elife"] = [0,1,1,1,2,2,3,1,3,2,1,1,1,1,1,1,1,1,1];
custStageCols["rcs"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Deliver digest","Author Review","Post Author Validation","Feature Review","Publisher Review","Typesetter Review","Validation Check","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New Version", "Official correction"];
custStageTimes["rcs"] = [1,1,0,2,0,1,3,3,3,2,1,1,1,1,1,1,1,1,1,1];
custStageCols["frontiers"] = ["addJob", "Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["frontiers"] = [0,1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["kriya"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["kriya"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["bir"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter Check","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["bir"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["cabi"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["cabi"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["mbs"] = ["Content-Loading", "Pre-editing","Preediting QA","Waiting for CE","Copyediting", "Copyediting QA", "Typesetter QA","Publisher Check","Proofreader","Author Review","Publisher Review","Typesetter Review","Validation Check", "Final Deliverables"];
custStageTimes["mbs"] = [0,1,0,0,2,0,1,3,3,3,2,1,1,1];
custStageCols["aiaa"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["aiaa"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["springer"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["springer"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["scrivener"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["scrivener"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["sense"] = ["Content-Loading", "Pre-editing","Copyediting", "Typesetter QA","Publisher Check","Author Review","Publisher Review","Typesetter Review","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New version", "Official correction"];
custStageTimes["sense"] = [1,1,2,1,3,3,1,1,1,1,1,1,1,1,1];
custStageCols["jb"] = ["addJob","Content-Loading", "Pre-editing","Copyediting", "Typesetter Check","Publisher Check","Deliver digest","Author Review","Post Author Validation","Feature Review","Publisher Review","Typesetter Review","Validation Check","Ready for Publication", "Final Deliverables","Silent correction","Silent Resupply","Resupply Online","New Version", "Official correction"];
custStageTimes["jb"] = [0,0,1,0,2,0,1,3,3,3,2,1,1,1,1,1,1,1,1,1,1];
*/


//Sorting a string
var sortstring = function (a, b)  {
	a = a.toLowerCase();
	b = b.toLowerCase();
	if (a < b) return -1;
	if (a > b) return 1;
	return 0;
}

//Hiding a row
var rowHiding=function(obj) {
	var currentTh = ($(obj).index());
	$('.headerTable > tbody > tr').show();
	$('.projectName').removeClass('focuss');
	$('.gradientBoxesWithOuterShadows').removeClass('focuss');
	$(obj).addClass('focuss');
	$('.headerTable > tbody > tr').find('td:nth-child('+(currentTh+1)+')').has('div:empty').parent('tr').hide();
}

setChapters=function(){

	var chapters = $('#chapters');
	//console.log(chapters)
	var url_string = window.location.href;
	var url = new URL(url_string);
	customer = url.searchParams.get("customer");
	project = url.searchParams.get("project");

	var element = '<div class="chapterBlock card"><div class="chapters"><span class="titl">Chapter 1</span><span class="currStage"></span></div><span class="chapterTitle" onClick="viewChapters(0)">Peripheral Isolates as Sources of Adaptive Diversity Under Climate Change</span></div>';
	
	

	$('.la-container').fadeIn();
	//chapters.append(chaptersArray);
	jQuery.ajax({
		type: "GET",
		url: "/api/articles?customerName="+customer+"&projectName="+project+"&articleStatus=in-progress",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			$.each(data['articles'],function(i,val){
				//console.log(val);
				var chapter = $(element);
				var doi = val["doi"];
				if (doi && typeof(doi) != 'undefined') {
					doi = doi.replace(/^[0-9\.\/]+/,"");
				}
				var currStage = val.workflow['current-stage'];
				chapter.find('.titl')[0].innerText = val.chapter;
				chapter.find('.chapterTitle')[0].innerText = val.title;
				chapter.find('.chapterTitle')[0].setAttribute('onClick', 'viewChapters(this, '+i+')')
				chapter.find('.currStage').text(currStage);
				chapter.find('.chapterTitle').attr('doi', doi).attr('customer', customer).attr('project', project);
				if (val.proofLink != null && val.proofLink.path != undefined){
					chapter.find('.chapterTitle').attr('pdf-link', val.proofLink.path);
				}
				var href = "/review_content/?customer=" + customer + "&project=" + project + "&doi=" + doi;
				chapter.find('.chapterTitle').attr('chapter-link', href);
				//chapter.find('.chapters a').attr('href', href);
				//chapter.innerHTML = val.title + "<i class=\"material-icons pull-right\">mode_edit</i>";
				//chapter.setAttribute('onClick', "viewChapters("+i+")");
				chapters[0].appendChild(chapter[0]);
			});
			$('.la-container').fadeOut()
		}
	});


	
	// $.each(chaptersArray, function(i, val){
	// 	var chapter = $(element);
	// 	chapter.find('.chapterTitle').innerText = val.title;
	// 	chapter.find('.chapterTitle').setAttribute('onClick', 'viewChapters('+i+')')
	// 	//chapter.innerHTML = val.title + "<i class=\"material-icons pull-right\">mode_edit</i>";
	// 	//chapter.setAttribute('onClick', "viewChapters("+i+")");
	// 	chapters[0].appendChild(chapter);

	// });
}

viewChapters = function(target, index){
	var url = '/review_content/?customer=' + $(target).attr('customer') + '&project=' + $(target).attr('project') + '&doi=' + $(target).attr('doi');
	$('.editChapter a').attr('href', $(target).attr('chapter-link'));
	$('.viewPDF').find('a').attr('href', $(target).attr('pdf-link'));
	$('.chapter-action-btn').removeAttr('style');
	$('.chapterBlock.selected').removeClass('selected');
	$(target).closest('.chapterBlock').addClass('selected');
	$('.la-container').fadeIn();
		//Make a call to get the Stages for the customer
		jQuery.ajax({
			type: "GET",
			url: url,
			success: function (data) {
				$('#temp').remove();
				var temp = $('<div id="temp"/>');
				$(temp).html(data);

				var linkTags = $(temp).find('link');
				var linkHref = [];
				
				if ($('head link[data-name-="kriya-css"]').length == 0){
					$(temp).find('link[rel="stylesheet"]').each(function(i,val){
						linkHref.push(val.href);
						$('head').append('<link data-name-="kriya-css" href="'+val.href+'" rel="stylesheet" page="review_content"/>')
					});
				}
				if ($('head script[data-name-="kriya-css"]').length == 0){
					$(temp).find('script[src]').each(function(i,val){
						var href = $(this).attr('src');
						if (/(indesignAutoPageConfig.js|jquery-3.1.0.min.js|ice.js|wysihtml.js|wysihtml.all-commands.js|wysihtml.toolbar.js|wysihtml.table_editing.js|advanced_and_extended.js|mutation-summary.js|undo.js|materialize.min.js|tinysort.min.js|tinysort.charorder.min.js|stylePalette.js|tomloprodModal-1.0.2.min.js|findreplace.js|cite.js|autocite.plugins.js|imagegalleryupload.js|ecs_commonjs.js|uuid.js|jquery.casechange.js|enjoyhint.min.js|xlsx.full.min.js)$/.test(href)){
							return true;
						}
						console.log(href)
						$('head').append('<script data-name-="kriya-css" src="' + $(this).attr('src') + '" type="text/javascript"/>')
					});
				}
				//$(temp).find('#contentContainer script[src]').remove()
				$(temp).find('#contentContainer').find('[data-track="del"],.del').remove();
				$('#contentContainer').html($(temp).find('#contentContainer').html())
				$('#compDivContent').html($(temp).find('#compDivContent').html())
				$('#compDivContent').find('.com-add,.btn.btn-medium').remove();
				$('#infoDivContent #infoDivNode').html($(temp).find('#infoDivNode').html())
				$('#infoDivContent #indexPanel').html($(temp).find('#indexPanel').html());
				//$('#temp').html("");
				setTimeout(function(){
					$('#contentContainer').height($(window).height() - $('#contentContainer').position().top);
					$('#infoDivContent').height($(window).height() - $('#infoDivContent').position().top);
					$('#navContainer .navDivContent > div:first').addClass('active');
					if ($('#indexPanelContent .jstree-container-ul').length > 0){
						$('#indexPanelContent .jstree-container-ul li').removeAttr('class');
						tree = $("#indexPanelContent").jstree({
							core: {check_callback: true}, 
							plugins: ["dnd"]
						});
						tree.jstree("deselect_all").jstree('open_all');
					}
					$('.templates *[data-component][data-type]').each(function(){
						kriya.componentList[$(this).attr('data-component')] = [];
						kriya.componentList[$(this).attr('data-component')].type = $(this).attr('data-type');
					});
					$('.la-container').fadeOut();
				},200)
			}
		});
	//$('#reviewContent').load(chaptersArray[index].url);

}

//set the stages for the customer
setCustomerProjectStages=function(customerName){
	//Make a call to get the Stages for the customer
	jQuery.ajax({
		type: "GET",
		url: "/api/getProjectStages?customerName="+customerName,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			if (data){
				custStageCols[customerName] = [];
				custStageTimes[customerName] = [];
				//get the stages
				var stagesData = data.stage;
				//if the stagesData is not empty
				if(stagesData){
					var dataLen = stagesData.length;
					for(var i=0;i<dataLen;i++){
						//get the stage name
						var name = stagesData[i]["name"];
						var dispName = stagesData[i]["customer-stage-name"];
						var startDate = stagesData[i]["start-date"];
						var endDate = stagesData[i]["end-date"];
						custStageCols[customerName][i] = name;
						custStageTimes[customerName][i] = endDate-startDate;
					}
				}
			}
		}
	});
}

//get the articles for the client and project
getCustomerProjectArticles=function(customerName,projectName,rowCount){
	//get the stages for the selected customer
	var stageCols = custStageCols[customerName];
	var stageColsLen = stageCols.length;

	var roleColsLen = roleCols.length;
	//Make a call to get the articles for the customer and project
	jQuery.ajax({
		type: "GET",
		url: "/api/articles?customerName="+customerName+"&projectName="+projectName+"&articleStatus=in-progress",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			if (data){
				roleArray[projectName] = {};
				overdueArray[projectName] = {};
				urgentArray[projectName] = {};
				completedArray[projectName] = {};
				tbCompletedArray[projectName] = {};
				newArray[projectName] = {};
				totalTypeArray = {};
				totalStageOwnerArray = {};
				totalOwnerStageArray = {};

				//Loop through the stages and initialize the arrays
				for(l=0;l<stageColsLen;l++){
					currCol = stageCols[l].toLowerCase();
					overdueArray[projectName][currCol] = [];
					urgentArray[projectName][currCol] = [];
					completedArray[projectName][currCol] = {};
					completedArray[projectName][currCol]["Today"] = [];


					completedArray[projectName][currCol]["Yesterday"] = [];
					completedArray[projectName][currCol]["Week"] = [];
					completedArray[projectName][currCol]["All"] = [];

					tbCompletedArray[projectName][currCol] = {};
					tbCompletedArray[projectName][currCol]["Today"] = [];


					tbCompletedArray[projectName][currCol]["Yesterday"] = [];
					tbCompletedArray[projectName][currCol]["Week"] = [];
					tbCompletedArray[projectName][currCol]["All"] = [];

					newArray[projectName][currCol] = {};
					newArray[projectName][currCol]["Today"] = [];


					newArray[projectName][currCol]["Yesterday"] = [];
					newArray[projectName][currCol]["Week"] = [];
					newArray[projectName][currCol]["All"] = [];


				}
				//loop through the roles
				for(l=0;l<roleColsLen;l++){
					currCol = roleCols[l];
					roleArray[projectName][currCol] = [];
				}

				var articlesData = data.articles;
			 var dataLen = articlesData.length;

				//if the articlesData is not empty
			 if(articlesData){
					//set the uniqueArray which is the list of articles by project
					uniqueArray[projectName] = articlesData;
					holdArray[projectName] = [];
					publishedArray[projectName] = [];
					
					publishedThisMonthArray[projectName] = [];
					publishedLastMonthArray[projectName] = [];
					//in case the length is still zero, an object was returned
					if(!dataLen || dataLen == 0){
				  	articlesData[0] = data.articles;
				  	dataLen = 1;
						}
						var today = new Date();
						//Loop through the articles and build the data arrays
						for(var i=0;i<dataLen;i++){
							//add to the total array which is the list of all articles
							totalArray.push(articlesData[i]);
							//get the doi
				  	var doi = articlesData[i]["doi"];
							if (doi && typeof(doi) != 'undefined') {
								doi = doi.replace(/^[0-9\.\/]+/,"");
				  	}
							//if the doi is not already added, add to the totalDOIArray
							if(totalDOIArray.indexOf(doi)<0){
								totalDOIArray.push(doi);
							}

							daysArray[doi]= {};
							var dataStagesLen = 0;
							var urgentState = "false";
				  	if(articlesData[i]["workflow"] && articlesData[i]["workflow"]["stage"]){
								dataStagesLen = articlesData[i]["workflow"]["stage"].length;
								if ((typeof(dataStagesLen) === 'undefined') && (/Not yet loaded/i.test(articlesData[i].title))){
									articlesData[i]["workflow"]["stage"] = [articlesData[i]["workflow"]["stage"]];
									dataStagesLen = articlesData[i]["workflow"]["stage"].length;
								}
								if(articlesData[i]["workflow"]["priority"] && articlesData[i]["workflow"]["priority"]["type"]=="urgent") urgentState = "true";

				  	}
				  	else{
								console.log("No stages data for project name"+projectName+" doi "+doi+" data len "+dataLen);
				  	}
							var completed = true;



							//Loop through the stages and build the arrays
				  	for(var j=0;j<dataStagesLen;j++){
						  		var days = 0;
								var status = articlesData[i]["workflow"]["stage"][j]["status"];
								if(typeof(articlesData[i]["workflow"]["stage"][j]["name"])!='object')
									var stageName = articlesData[i]["workflow"]["stage"][j]["name"].toLowerCase();
								else
									var stageName = articlesData[i]["workflow"]["stage"][j]["name"]["#text"].toLowerCase();
								var roleName = stageNameRoleMap[stageName];
								var holdState = articlesData[i]["workflow"]["stage"][j]["hold"];
								var startDate = articlesData[i]["workflow"]["stage"][j]["start-date"];
								var dueDate = articlesData[i]["workflow"]["stage"][j]["end-date"];
								if(startDate == "--") startDate = "";
								if(dueDate == "--") dueDate = "";
								var startDateObj;
								if(startDate) startDateObj = new Date(startDate);
								var dueDateObj;
								if (dueDate){
									dueDateObj = new Date(dueDate);
								}
										
								var ownerName = articlesData[i]["workflow"]["stage"][j]["assigned"]["to"];

								if (!startDate){
									startDate = currDate;
									articlesData[i]["workflow"]["stage"][j]["start-date"] = startDate;
								}

								if (!dueDate){
									dueDate = currDate;
									articlesData[i]["workflow"]["stage"][j]["end-date"] = dueDate;
								}

								//set the start date and production time for the article
								if(j==0) {
									articlesData[i]["start-date"] = startDateObj;
									//days in production
									days = workingDaysBetweenDates(articlesData[i]["start-date"],today);
									articlesData[i]["days"] = days;

								}
								//if the stagename is invalid
								if(stageCols.customIndexOf(stageName)==-1){
									console.log("Invalid stage "+stageName+" for doi "+doi);
									var oldStageName = stageName;
									stageName = stageCols[0].toLowerCase();
									//console.log("Replacing invalid stage "+oldStageName+ " with stage "+stageName+" for doi "+doi);
								}

								//set the due time to be 5PM GMT time - IMPORTANT - This is a hard code need to remove
								//var dueTime = articlesData[i]["workflow"]["stage"][j]["end-time"];
								//if(!dueTime) dueTime = dueDate.concat('T17:00:00');
								var dueTime = dueDate.concat('T17:00:00');
								dueDateObj = new Date(dueTime);
								var diffDays = 0;

								//add the job to the appropriate array based on start date
								if(status == "completed" || status == "in-progress"){
									diffDays = daysBetweenDates(startDateObj,today);
									if(diffDays == 0){
										if(newArray[projectName][stageName]["Today"].indexOf(doi)<0) newArray[projectName][stageName]["Today"].push(doi);
									}
									else if(diffDays == 1){
										if(newArray[projectName][stageName]["Yesterday"].indexOf(doi)<0) newArray[projectName][stageName]["Yesterday"].push(doi);
									}
									if(diffDays <= 7){
										if(newArray[projectName][stageName]["Week"].indexOf(doi)<0) newArray[projectName][stageName]["Week"].push(doi);
									}
									if(newArray[projectName][stageName]["All"].indexOf(doi)<0) newArray[projectName][stageName]["All"].push(doi);
								
								}

								//if the status is completed, add to the completed arrays
								if(status == "completed"){

									//Fill in the daysArray for each stage
									diffDays = workingDaysBetweenDates(startDateObj,dueDateObj);
									if(diffDays) daysArray[doi][stageName] = diffDays;
									else daysArray[doi][stageName] = 0;

									diffDays = daysBetweenDates(dueDateObj,today);
	
									if(diffDays <= 0){
										if(completedArray[projectName][stageName]["Today"].indexOf(doi)<0) completedArray[projectName][stageName]["Today"].push(doi);
										if(tbCompletedArray[projectName][stageName]["Today"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Today"].push(doi);
									}
									else if(diffDays <= 1){
										if(completedArray[projectName][stageName]["Yesterday"].indexOf(doi)<0) completedArray[projectName][stageName]["Yesterday"].push(doi);
										if(tbCompletedArray[projectName][stageName]["Yesterday"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Yesterday"].push(doi);
									}
									if(diffDays <= 7){
										if(completedArray[projectName][stageName]["Week"].indexOf(doi)<0) completedArray[projectName][stageName]["Week"].push(doi);
										if(tbCompletedArray[projectName][stageName]["Week"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Week"].push(doi);
									}
									if(completedArray[projectName][stageName]["All"].indexOf(doi)<0) completedArray[projectName][stageName]["All"].push(doi);
									if(tbCompletedArray[projectName][stageName]["All"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["All"].push(doi);

									//Reset days in production
									days = workingDaysBetweenDates(articlesData[i]["start-date"],dueDateObj);
									articlesData[i]["days"] = days;
								}
								//if status is in progress 
								if(status == "in-progress") {
									completed = false;

									//Fill in the daysArray for each stage
									diffDays = workingDaysBetweenDates(startDateObj,today);
									if(diffDays) daysArray[doi][stageName] = diffDays;
									else daysArray[doi][stageName] = 0;

									//if hold status is true, push into hold array
									if(holdState=="true" && totalHoldArray.indexOf(doi)<0){
										holdArray[projectName].push(doi);
										totalHoldArray.push(doi);
									}
									//else add to the stage arrays and role arrays
									else{
										//add to the tbCompletedArray
										diffDays = daysBetweenDates(dueDateObj,today);
										if(diffDays<=0){
											if(tbCompletedArray[projectName][stageName]["Today"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Today"].push(doi);
										}
										else if(diffDays<=1){
											if(tbCompletedArray[projectName][stageName]["Yesterday"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Yesterday"].push(doi);	
										}
										if(diffDays<=7){
											if(tbCompletedArray[projectName][stageName]["Week"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Week"].push(doi);
										}
										if(tbCompletedArray[projectName][stageName]["All"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["All"].push(doi);
										if(tbCompletedArray[projectName][stageName]["Today"].indexOf(doi)<0) tbCompletedArray[projectName][stageName]["Today"].push(doi);

										//check if the article is already in the completed array at that stage, cases where a stage is repeated for some reason and pop it out
										if(completedArray[projectName][stageName]["Today"].indexOf(doi)>=0) {
											completedArray[projectName][stageName]["Today"].pop(doi);
										}
										if(completedArray[projectName][stageName]["Yesterday"].indexOf(doi)>=0) {
											completedArray[projectName][stageName]["Yesterday"].pop(doi);
										}
										if(completedArray[projectName][stageName]["Week"].indexOf(doi)>=0) {
											completedArray[projectName][stageName]["Week"].pop(doi);
										}
										if(completedArray[projectName][stageName]["All"].indexOf(doi)>=0) {
											completedArray[projectName][stageName]["All"].pop(doi);
										}

										//Reset days in production
										days = workingDaysBetweenDates(articlesData[i]["start-date"],today);
										articlesData[i]["days"] = days;
										
										//Fill out the aging array
										if(days>30){if(agingArray[">30d"][stageName].indexOf(doi)<0) agingArray[">30d"][stageName].push(doi);}
										//if(days>25){if(agingArray[">25d"][stageName].indexOf(doi)<0) agingArray[">25d"][stageName].push(doi);}
										if(days>20){if(agingArray[">15d"][stageName].indexOf(doi)<0) agingArray[">20d"][stageName].push(doi);}
										if(days>15){if(agingArray[">15d"][stageName].indexOf(doi)<0) agingArray[">15d"][stageName].push(doi);}
										//if(days>12){if(agingArray[">12d"][stageName].indexOf(doi)<0) agingArray[">12d"][stageName].push(doi);}
										if(days>10){if(agingArray[">10d"][stageName].indexOf(doi)<0) agingArray[">10d"][stageName].push(doi);}
										//if(days>7){if(agingArray[">7d"][stageName].indexOf(doi)<0) agingArray[">7d"][stageName].push(doi);}
										if(days>4){if(agingArray[">4d"][stageName].indexOf(doi)<0) agingArray[">4d"][stageName].push(doi);}
										//if(days>2){if(agingArray[">2d"][stageName].indexOf(doi)<0) agingArray[">2d"][stageName].push(doi);}
										if(days>1){if(agingArray[">1d"][stageName].indexOf(doi)<0) agingArray[">1d"][stageName].push(doi);}
										if(agingArray["All"][stageName].indexOf(doi)<0) agingArray["All"][stageName].push(doi);
										
										//if urgent status is true, push into urgent array
										if(urgentState=="true" && totalUrgentArray.indexOf(doi)<0){
											urgentArray[projectName][stageName].push(doi);
                                            urgentArrayByStage[stageName].push(doi);
											totalUrgentArray.push(doi);
										}
										//get the article type
										var articleType = articlesData[i]["type"];
										//push into the articletype array
										if(totalTypeArray[articleType]){
											totalTypeArray[articleType].push(doi);
										}
										else{
											totalTypeArray[articleType] = [];
											totalTypeArray[articleType].push(doi);
										}

										if(projectName && roleName) {
											stageArray[projectName][stageName].push(doi);
											roleArray[projectName][roleName].push(doi);
											totalStageArray[stageName].push(doi);
											totalRoleArray[roleName].push(doi);
											//if the dueDate has passed
											if(Date.parse(dueDateObj)<Date.now()){
												overdueArray[projectName][stageName].push(doi);
												overdueArrayByStage[stageName].push(doi);
											}

											//push the article into the stageowner/ownerstage arrays
 										 	var own = ownerName;
 										 	var stg = stageCols[stageCols.customIndexOf(stageName)];
 										 	var ownStg = own + "_" + stg;
 										 	var stgOwn = stg + "_" + own;

 										 	if(totalStageOwnerArray[stg]){
 											 	totalStageOwnerArray[stg].push(doi);
 										 	}
 										 	else{
 											 	totalStageOwnerArray[stg] = [];
 											 	totalStageOwnerArray[stg].push(doi);
 										 	}
 										 	if(totalStageOwnerArray[stgOwn]){
 											 	totalStageOwnerArray[stgOwn].push(doi);
 										 	}
 										 	else{
 											 	totalStageOwnerArray[stgOwn] = [];
 											 		totalStageOwnerArray[stgOwn].push(doi);
 										 	}

 										 	if(totalOwnerStageArray[own]){
 											 	totalOwnerStageArray[own].push(doi);
 										 	}
 										 	else{
 											 	totalOwnerStageArray[own] = [];
 											 	totalOwnerStageArray[own].push(doi);
 										 	}
 										 	if(totalOwnerStageArray[ownStg]){
 											 	totalOwnerStageArray[ownStg].push(doi);
 										 	}
 										 	else{
 											 	totalOwnerStageArray[ownStg] = [];
 											 	totalOwnerStageArray[ownStg].push(doi);
 										 	}
										}
										else {
											//console.log("improper call for project "+projectName+" and role "+roleName+" and stage "+stageName);
										}
									}
								}
				 		}
							//if the completed flag is true, add to the totalPublishedArray
							if(completed) {
								publishedArray[projectName].push(doi);
								//add to this month and last month arrays
								if(dueDateObj && (dueDateObj.getYear() == today.getYear()) && (dueDateObj.getMonth() == today.getMonth())){
									totalPublishedThisMonthArray.push(doi);
									publishedThisMonthArray[projectName].push(doi);
									//Push the publication days into an array
									daysArray[doi]["Time to Publication"] = articlesData[i]["days"];
									//push the proof pages into an array
									daysArray[doi]["Proof Count"] = articlesData[i]["workflow"]["proof-count"];
									daysArray[doi]["Word Count"] = articlesData[i]["workflow"]["word-count"];
								}
								else if(dueDateObj && (dueDateObj.getYear() == today.getYear()) && (dueDateObj.getMonth()  == today.getMonth()-1)) {
									totalPublishedLastMonthArray.push(doi);
									publishedLastMonthArray[projectName].push(doi);
									//Push the publication days into an array
									daysArray[doi]["Time to Publication"] = articlesData[i]["days"];
									//push the proof pages into an array
									daysArray[doi]["Proof Count"] = articlesData[i]["workflow"]["proof-count"];
									daysArray[doi]["Word Count"] = articlesData[i]["workflow"]["word-count"];
								}
								//if the doi is not already added, add to the totalDOIArray
								if(totalPublishedArray.indexOf(doi)<0){
									totalPublishedArray.push(doi);
								}
							}
						}
						//generate the roletable
						var uniqueArrayLen = Object.keys(uniqueArray).length
						if(uniqueArrayLen == rowCount){
							generateFilters(customerName);
						}
			  }
			  else{
						uniqueArray[projectName] = '';
						var uniqueArrayLen = Object.keys(uniqueArray).length
						if(uniqueArrayLen == rowCount){
							generateFilters(customerName);
						}
			  }
			}
			else{
			  uniqueArray[projectName] = '';
			  var uniqueArrayLen = Object.keys(uniqueArray).length
			  if(uniqueArrayLen == rowCount){
					 generateFilters(customerName);
			  }
			}
		},
		error: function(xhr, errorType, exception) {
		  return null;
		}
	});
}

//get the articles for the customer and project
getProjectStages=function(customerName,projectName,bucketName){
	var roleColsLen = roleCols.length;
	jQuery.ajax({
		type: "GET",
		url: "/api/articles?customerName="+customerName+"&projectName="+projectName+"&bucketName="+bucketName+"&articleStatus=in-progress",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			if (data){
				if(data.error){
					return null;
				}
				else{
					//reset the unique arrays
					uniqueArray = {};
					urgentArray[projectName] = {};
					//get the list of stages for the customer
					var stageCols = custStageCols[customerName];
					var stageColsLen = stageCols.length;
					//loop through the stages list and initialize the arrays
					for (i = 0; i < stageColsLen; i++) {
						var stageName = stageCols[i].toLowerCase();
						totalArray = [];
						uniqueArray[stageName] = [];
						roleArray[stageName] = {};
						urgentArray[projectName][stageName] = [];
						totalStageArray[stageName] = [];

						totalTypeArray = {};
						totalStageOwnerArray = {};
						totalOwnerStageArray = {};

						for(l=0;l<roleColsLen;l++){
							currCol = roleCols[l];
							roleArray[stageName][currCol] = [];
							totalRoleArray[currCol] = [];
						}
					}

				 var articlesData = data.articles;
				 var dataLen = articlesData.length;
				 //in case the length is still zero, an object was returned
			  if(articlesData){
					  holdArray[projectName] = [];
					  
					  if(!dataLen || dataLen == 0){
								articlesData[0] = data.articles;
								dataLen = 1;
					  }
					  var today = new Date();
							//loop through the data
					  for(var i=0;i<dataLen;i++){
								var dataStagesLen = articlesData[i]["workflow"]["stage"].length;
								if(!dataStagesLen || dataStagesLen == 0){
									articlesData[i]["workflow"]["stage"][0] = articlesData[i]["workflow"]["stage"];
									dataStagesLen = 1;
					  			}
								var doi = articlesData[i]["doi"]
								if (doi && typeof(doi) != 'undefined') {
									doi = doi.replace(/^[0-9\.\/]+/,"");
								}

								//push into the total array
								totalArray.push(articlesData[i]);
								//if the doi is not already added, push into the doiarray
								if(totalDOIArray.indexOf(doi)<0){
									totalDOIArray.push(doi);
								}

								//urgent
								var urgentState = "false";
								if(articlesData[i]["workflow"]["priority"] && articlesData[i]["workflow"]["priority"]["type"]=="urgent") urgentState = "true";

								for(var j=0;j<dataStagesLen;j++){
						  			var status = articlesData[i]["workflow"]["stage"][j]["status"];
									if(typeof(articlesData[i]["workflow"]["stage"][j]["name"])!='object')
										var currStageName = articlesData[i]["workflow"]["stage"][j]["name"].toLowerCase();
									else
										var currStageName = articlesData[i]["workflow"]["stage"][j]["name"]["#text"].toLowerCase();
						  			
						  			var holdState = articlesData[i]["workflow"]["stage"][j]["hold"];
									var ownerName = articlesData[i]["workflow"]["stage"][j]["assigned"]["to"];
									var startDate = articlesData[i]["workflow"]["stage"][j]["start-date"];
									var dueDate = articlesData[i]["workflow"]["stage"][j]["end-date"];
									if(startDate == "--") startDate = "";
									if(dueDate == "--") dueDate = "";
									var startDateObj;
									if(startDate) startDateObj = new Date(startDate);
									var dueDateObj;
									if (dueDate){
										dueDateObj = new Date(dueDate);
									}

									//set the start date and production time for the article based on the first stage
									if(j==0) {
										articlesData[i]["start-date"] = startDateObj;
										//days in production
										var days = workingDaysBetweenDates(articlesData[i]["start-date"],today);
										articlesData[i]["days"] = days;
									}
									//if the stage is invalid
									if(stageCols.customIndexOf(currStageName)==-1){
										console.log("Invalid stage "+currStageName+" for doi "+doi);
										var oldStageName = currStageName;
										currStageName = stageCols[0].toLowerCase();
										//console.log("Replacing invalid stage "+oldStageName+ " with stage "+currStageName+" for doi "+doi);
									}

						   			if(status == "in-progress") {
									 uniqueArray[currStageName].push(articlesData[i]);

									 //get the article type
									 var articleType = articlesData[i]["type"];
									 //push into the articletype array
									 if(totalTypeArray[articleType]){
										 totalTypeArray[articleType].push(doi);
									 }
									 else{
										 totalTypeArray[articleType] = [];
										 totalTypeArray[articleType].push(doi);
									 }

									
									 //if hold status is true, push into hold array
									 if(holdState=="true" && totalHoldArray.indexOf(doi)<0){
										 holdArray[projectName].push(doi);
										 totalHoldArray.push(doi);
									 }
									 else{
										//if urgent status is true, push into urgent array
										if(urgentState=="true" && totalUrgentArray.indexOf(doi)<0){
											urgentArray[projectName][currStageName].push(doi)
											urgentArrayByStage[currStageName].push(doi);
											totalUrgentArray.push(doi);
										}

										 var roleName = stageNameRoleMap[currStageName];
										 roleArray[currStageName][roleName].push(doi);
										 totalStageArray[currStageName].push(doi);
										 totalRoleArray[roleName].push(doi);
										 //push the article into the stageowner/ownerstage arrays
										 var own = ownerName;
										 var stg = stageCols[stageCols.customIndexOf(currStageName)];
										 var ownStg = own + "_" + stg;
										 var stgOwn = stg + "_" + own;

										 if(totalStageOwnerArray[stg]){
											 totalStageOwnerArray[stg].push(doi);
										 }
										 else{
											 totalStageOwnerArray[stg] = [];
											 totalStageOwnerArray[stg].push(doi);
										 }
										 if(totalStageOwnerArray[stgOwn]){
											 totalStageOwnerArray[stgOwn].push(doi);
										 }
										 else{
											 totalStageOwnerArray[stgOwn] = [];
											 totalStageOwnerArray[stgOwn].push(doi);
										 }

										 if(totalOwnerStageArray[own]){
											 totalOwnerStageArray[own].push(doi);
										 }
										 else{
											 totalOwnerStageArray[own] = [];
											 totalOwnerStageArray[own].push(doi);
										 }
										 if(totalOwnerStageArray[ownStg]){
											 totalOwnerStageArray[ownStg].push(doi);
										 }
										 else{
											 totalOwnerStageArray[ownStg] = [];
											 totalOwnerStageArray[ownStg].push(doi);
										 }
									 }
						  }
							}
						}
					}
					else{
						return null;
					}
				}
				//generate the table
				generateFilters(customerName);
			}
			else{
				return null;
			}
		},
		error: function(xhr, errorType, exception) {
		 	return;
		}
	});
}

generateFilters=function(customerName){
	generateTypeFilters(customerName);
	generateStageFilters(customerName);
	generateOwnerFilters(customerName);
	var docName = window.document.title;
	//if this is the dashboard, open the nav
	if(docName.includes("Dashboard")){
		displayCards("All");
	}
	else if(docName.includes("Report")){
		reportName = $('body').attr('data-report')?$('body').attr('data-report'):'';
		if(reportName) this[reportName]();
		else journalReport();
	}
}

//generate type filters
generateTypeFilters=function(customerName){
	var collapse='<li>';
	var collapsebody='<div class="collapsible-header">Articles by type <img src="images/unfold.png" alt="sorry" id="updownbtn"></img></div><div class="collapsible-body" data-typefilter="" data-typelist="">';

	var typeArrayKeys = Object.keys(totalTypeArray);
	typeArrayKeys.sort();
	var arrayLen = typeArrayKeys.length;
	//loop through the data
	for(i=0; i<arrayLen; i++) {
		var filterName = typeArrayKeys[i];
		var displayName = filterName;
		var colData = totalTypeArray[filterName];
		if(colData) colDataLen = colData.length;
		//if there are articles at this stage,
		if(colDataLen > 0){
			collapsebody += '<label for="'+i+1+'"><p class="stagestyle type_main" data-filter="'+filterName+'" data-type="Type" data-articlelist="'+totalTypeArray[filterName]+'"><input type="checkbox" class="article_filters" id="'+i+1+'" style="position:static;opacity:1;">&nbsp;'+filterName+' ('+colDataLen+')</p></label>';
		}
	}

    collapse+=collapsebody+'</div></li>';
	if(arrayLen>0) {
		$('#colpara').html(collapse);
	}
	else{
		$('#colpara').html('');
	}

}

//generate stage filters
generateStageFilters=function(customerName){
	var collapse='<li>';
	var ulFlag = false;
	var parent = 0;
	var collapsebody='<div class="collapsible-header">Articles by stage <img src="images/unfold.png" alt="sorry" id="updownbtn"></img></div><div class="collapsible-body" data-stagefilter="" data-stagelist=""><ul>';
		var arrayKeys = Object.keys(totalStageOwnerArray);
	arrayKeys.sort();
	var arrayLen = arrayKeys.length;
	//loop through the data
	for(i=0; i<arrayLen; i++) {
		var filterName = arrayKeys[i];
		var displayName = filterName;
		var colData = totalStageOwnerArray[filterName];
		if(colData) colDataLen = colData.length;

		if(filterName.indexOf('_')>-1){
			var splitStr = filterName.split('_');
			displayName = splitStr[1];
			//sub entries
			if(colDataLen > 0){
				collapsebody += '<li class="stage_sub" childOf="'+parent+'" data-filter="'+filterName+'" data-type="Stage" data-articlelist="'+totalStageOwnerArray[filterName]+'"><label for="SS'+i+1+'" ><p class="stagestyle"><input type="checkbox" id="SS'+i+1+'" class="article_filters" style="position:static;opacity:1;">&nbsp;'+displayName+' ('+colDataLen+')</p></label></li>';
			}
		}
		else{
			//main entry
			if(colDataLen > 0){
				parent = i+1;
				collapsebody += '<li class="stage_main" parent-id="'+parent+'" data-filter="'+filterName+'" data-type="Stage" data-articlelist="'+totalStageOwnerArray[filterName]+'"><label for="SM'+parent+'"><p class="stagestyle"><input id="SM'+parent+'" type="checkbox" class="article_filters" style="position:static;opacity:1;">&nbsp;'+displayName+' ('+colDataLen+')</p></label></li>';
			}
		}
	}

    collapse+=collapsebody+'</ul></div></li>';
	if(arrayLen>0) {
		$('#colpara1').html(collapse);
	}
	else{
		$('#colpara1').html('');
	}
}

//generate owner filters
generateOwnerFilters=function(customerName){
	var collapse='<li>';
	var ulFlag = false;
	var parent = 0;
	var collapsebody='<div class="collapsible-header">Articles by owner <img src="images/unfold.png" alt="sorry" id="updownbtn"></img></div><div class="collapsible-body" data-ownerfilter="" data-ownerlist=""><ul>';
		var arrayKeys = Object.keys(totalOwnerStageArray);
	arrayKeys.sort();
	var arrayLen = arrayKeys.length;
	//loop through the data
	for(i=0; i<arrayLen; i++) {
		var filterName = arrayKeys[i];
		var displayName = filterName;
		var colData = totalOwnerStageArray[filterName];
		if(colData) colDataLen = colData.length;

		if(filterName.indexOf('_')>-1){
			var splitStr = filterName.split('_');
			displayName = splitStr[1];
			//sub entries
			if(colDataLen > 0){
				collapsebody += '<li class="owner_sub" childOf="'+parent+'" data-filter="'+filterName+'" data-type="Owner" data-articlelist="'+totalOwnerStageArray[filterName]+'"><label for="OS'+i+1+'" ><p class="stagestyle"><input type="checkbox" id="OS'+i+1+'" class="article_filters" style="position:static;opacity:1;">&nbsp;'+displayName+' ('+colDataLen+')</p></label></li>';
			}
		}
		else{
			//main entry
			if(colDataLen > 0){
				parent = i+1;
				collapsebody += '<li class="owner_main" parent-id="'+parent+'" data-filter="'+filterName+'" data-type="Owner" data-articlelist="'+totalOwnerStageArray[filterName]+'"><label for="OM'+parent+'"><p class="stagestyle"><input type="checkbox" id="OM'+parent+'" class="article_filters" style="position:static;opacity:1;">&nbsp;'+displayName+' ('+colDataLen+')</p></label></li>';
			}
		}
	}

    collapse+=collapsebody+'</ul></div></li>';
	if(arrayLen>0) {
		$('#colpara2').html(collapse);
	}
	else{
		$('#colpara2').html('');
	}
}


//generate journal stage table
generateJournalStageTable=function(customerName){
	var rowTotal = [];
	var columnTotal = [];
	var delayTotal = [];
	var urgentTotal = [];
	var skipStageArray = [];
	var stageName = "";
 	stageCols = custStageCols[customerName];

	var stageColsLen = stageCols.length;
	var indexFilterName ="";

	//Initialize value for columnwise total variable
	for(i=0; i<stageColsLen+10; i++) {
		columnTotal[i] = 0;
		delayTotal[i] = 0;
		urgentTotal[i] = 0;
	}

	var displayString = "Journal";
	var peDisplayString = "Production Editor";
	var pmDisplayString = "Project Manager";

	var tableHeader = '';
 	tableHeader+='<a href="javascript:export_report()">Export to XLSX</a>';
	tableHeader +='<table id="stageTable" align="center" style="font-size: small;"><caption/><thead><tr><th>'+displayString+'</th><th>'+peDisplayString+'</th><th>'+pmDisplayString+'</th><th onClick="rowHiding(this)">No. of WIP articles</th><th onClick="rowHiding(this)">On hold articles</th><th onClick="rowHiding(this)">Urgent articles</th>';

	//loop through the stages and get data at each stage
	for(i=0; i<stageColsLen; i++){
		var stageName = stageCols[i].toLowerCase();
		var colData = totalStageArray[stageName];
		if(colData) colDataLen = colData.length;
		//if there are no articles at this stage, add to skip stage array and skip
		if(colDataLen == 0) {
			skipStageArray.push(stageName);
			continue;
		}
		tableHeader += '<th onClick="rowHiding(this)">'+stageCols[i]+'</th>';
	}

	tableHeader += '<th class="gradientBoxesWithOuterShadows">Articles published last month</th><th class="gradientBoxesWithOuterShadows">Articles published this month</th><th class="gradientBoxesWithOuterShadows">Total published articles</th><th class="gradientBoxesWithOuterShadows">Total</th><tr>';

	var tableBody = '<tbody>';

	var uniqueArrayKeys = Object.keys(uniqueArray);
	uniqueArrayKeys.sort();
	var uniqueArrayLen = uniqueArrayKeys.length;
	//loop through the data
	for(k=0; k<uniqueArrayLen; k++) {
		var filterName = uniqueArrayKeys[k];
		var rowArray = uniqueArray[filterName];
		var holdData = {};
		var holdDataLen = 0;
		var urgentData = {};
		var urgentDataLen = 0;
		var cellVal = 0;
		var rowTotalLen = 0;
		holdData = holdArray[filterName];
		urgentData = urgentArray[filterName];
		//if the article was held, add to the array
		if(holdData) holdDataLen = holdData.length;
		//if the article is urgent, add to the array
		if(urgentData) {
			var urgentDataKeys = Object.keys(urgentData);
			var urgentDataKeysLen = urgentDataKeys.length;
			for(l=0;l<urgentDataKeysLen;l++){
				urgentDataLen = urgentDataLen + urgentArray[filterName][urgentDataKeys[l]].length;
			}
		}

		var publishedData = {};
		var publishedDataLen = 0;
		publishedData = publishedArray[filterName];
		if(publishedData) publishedDataLen = publishedData.length;

		//Articles published this month
		var publishedThisMonthData = {};
		var publishedThisMonthDataLen = 0;
		publishedThisMonthData = publishedThisMonthArray[filterName];
		if(publishedThisMonthData) publishedThisMonthDataLen = publishedThisMonthData.length;

		//Articles published last month
		var publishedLastMonthData = {};
		var publishedLastMonthDataLen = 0;
		publishedLastMonthData = publishedLastMonthArray[filterName];
		if(publishedLastMonthData) publishedLastMonthDataLen = publishedLastMonthData.length;


		if(rowArray){
	 		rowTotalLen = rowArray.length;
		 	if(!rowTotalLen || rowTotalLen == 0){
				rowTotalLen = 1;
			}
		}
		 //if(rowTotalLen>0){
				if(indexFilterName == "") indexFilterName = filterName;
				tableBody += '<tr>';
				for(i=0; i<stageColsLen+10; i++){
					var currCol;
					if(i==0) {
						var dispName = journalArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else if(i==1){
						var dispName = journalPEArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else if(i==2){
						var dispName = journalPMArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else if(i==3){
						cellVal = rowTotalLen-holdDataLen-publishedDataLen;
						tableBody+='<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else if(i==4){
						cellVal = holdDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else if(i==5){
						cellVal = urgentDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else if(i==(stageColsLen+6)){
						cellVal = publishedLastMonthDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else if(i==(stageColsLen+7)){
						cellVal = publishedThisMonthDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else if(i==(stageColsLen+8)){
						cellVal = publishedDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}

					else if(i==(stageColsLen+9)){
						cellVal = rowTotalLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += cellVal;
					}
					else{
						var colData = {};
						var colDataLen = 0;
						var totalColDataLen = 0;
						var overdueDataLen = 0;
						var urgentDataLen = 0;
						currCol = stageCols[i-6].toLowerCase();
						colData = stageArray[filterName][currCol];
						if(colData) colDataLen = colData.length;
						//if there are no articles at this stage, skip
						if(skipStageArray.indexOf(currCol)>=0) continue;
						var overdueData = "";
						if(overdueArray[filterName]){
							overdueData = overdueArray[filterName][currCol];
						} 
						
						if(overdueData) overdueDataLen = overdueData.length;
						else overdueDataLen = 0;

						var urgentData = "";
						if(urgentArray[filterName]){
							urgentData = urgentArray[filterName][currCol];
						} 
						
						if(urgentData) urgentDataLen = urgentData.length;
						else urgentDataLen = 0;

						cellVal = colDataLen;
						tableBody += '<td>'+cellVal;
						if(overdueDataLen>0){
							tableBody +=  ' (<font color="red">'+overdueDataLen+'</font>)';
						}
						if(urgentDataLen>0){
							tableBody +=  ' (<font color="green">'+urgentDataLen+'</font>)';
						}

						tableBody += '</td>';
						
						columnTotal[i] += cellVal;
						delayTotal[i] += overdueDataLen;
						urgentTotal[i] += urgentDataLen;
					}
				}
				tableBody += '</tr>';
			//}
		//}
	}

	tableBody += '</tbody>';
	tableHeader +='</tr>';

	var tableFooter = '';
	tableFooter +='<tfoot><tr>';
	for(i=0; i<stageColsLen+10; i++){
		if(i==0){
			tableBody+='<td><b>Total</b></td>';
		}
		else if(i==1){
			tableBody+='<td>&nbsp;</td>';
		}
		else if(i==2){
			tableBody+='<td>&nbsp;</td>';
		}
		else{
			j = i-6;
			if(j>=0 && j<stageColsLen){
				currCol = stageCols[j].toLowerCase();
				//if there are no articles at this stage, skip
				if(skipStageArray.indexOf(currCol)>=0) continue;
			}

			tableBody += '<td>'+columnTotal[i];
			if(delayTotal[i]>0){
				tableBody+=' (<font color="red">'+delayTotal[i]+'</font>)';
			}
			if(urgentTotal[i]>0){
				tableBody+=' (<font color="green">'+urgentTotal[i]+'</font>)';
			}
			tableBody+='</td>';
			
		}
	}
	tableFooter += '</tr></tfoot>';
	tableHeader+=tableFooter+tableBody+'</table>';

	return tableHeader;
}

//generate journal movement table
generateJournalMovementTable=function(customerName,timeFrame){
	var rowTotal = [];
	var columnTotal = [];
	var fullColumnTotal = [];
	var inHandColumnTotal = [];
	var newColumnTotal = [];
	var movementColumnTotal = [];
	var skipStageArray = [];
	var delayTotal = [];
	var stageName = "";
    stageCols = custStageCols[customerName];

	var stageColsLen = stageCols.length;
	var indexFilterName ="";

	//Initialize value for columnwise total variable
	for(i=0; i<stageColsLen+6; i++) {
		columnTotal[i] = 0;
		delayTotal[i] = 0;
		fullColumnTotal[i] = 0;
		inHandColumnTotal[i] = 0;
		newColumnTotal[i] = 0;
		movementColumnTotal[i] = 0;
	}

	var displayString = "Journal";
	var peDisplayString = "Production Editor";
	var pmDisplayString = "Project Manager";

	var tableHeader = '';
    tableHeader +='<a href="javascript:export_report()">Export to XLSX</a>';
	tableHeader +='<table id="stageTable" align="center" style="font-size: small;"><caption/><thead><tr><th>'+displayString+'</th><th>'+peDisplayString+'</th><th>'+pmDisplayString+'</th>';

	for(i=0; i<stageColsLen; i++){
		var stageName = stageCols[i].toLowerCase();
		var colData = totalStageArray[stageName];
		if(colData) colDataLen = colData.length;
		//if there are no articles at this stage, add to skip stage array and skip only for today's plan
		//if(timeFrame=="Today" && colDataLen == 0) {
			//skipStageArray.push(stageName);
			//continue;
		//}
		tableHeader += '<th onClick="rowHiding(this)">'+stageCols[i]+'</th>';
	}

	tableHeader +='</tr>';

	var tableBody = '<tbody>';

	var uniqueArrayKeys = Object.keys(uniqueArray);
	uniqueArrayKeys.sort();
	var uniqueArrayLen = uniqueArrayKeys.length;
	//loop through the data
	for(k=0; k<uniqueArrayLen; k++) {
		var filterName = uniqueArrayKeys[k];
		var rowArray = uniqueArray[filterName];
		var cellVal = 0;

		if(rowArray){
	  rowTotalLen = rowArray.length;
		 if(!rowTotalLen || rowTotalLen == 0){
				rowTotalLen = 1;
			}

		 if(rowTotalLen>0){
				if(indexFilterName == "") indexFilterName = filterName;
				tableBody += '<tr>';
				for(i=0; i<stageColsLen+3; i++){
					var currCol;
					if(i==0) {
						var dispName = journalArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else if(i==1){
						var dispName = journalPEArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else if(i==2){
						var dispName = journalPMArr[filterName];
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
					else{
						var completeDataLen = 0;
						var plannedDataLen = 0;
						var colData = {};
						var colDataLen = 0;
						var newData = {};
						var newDataLen = 0;
						currCol = stageCols[i-3].toLowerCase();
						//if there are no articles at this stage, skip
						if(skipStageArray.indexOf(currCol)>=0) continue;
						var completeData = completedArray[filterName][currCol][timeFrame];
						if(completeData) completeDataLen = completeData.length;
						var plannedData = tbCompletedArray[filterName][currCol][timeFrame];
						if(plannedData) plannedDataLen = plannedData.length;
						var newData = newArray[filterName][currCol][timeFrame];
						if(newData) newDataLen = newData.length;
						colData = stageArray[filterName][currCol];
						if(colData) colDataLen = colData.length;

						cellVal = completeDataLen+'/'+plannedDataLen;
						//cellVal = completeDataLen;
						tableBody += '<td>'+cellVal+'</td>';
						columnTotal[i] += completeDataLen;
						fullColumnTotal[i] += plannedDataLen;
						inHandColumnTotal[i] += colDataLen;
						newColumnTotal[i] += newDataLen;
						movementColumnTotal[i] += completeDataLen - newDataLen;
					}
				}//end of for
				tableBody += '</tr>';
			}
		}
	}

	tableBody += '</tbody>';

	var tableFooter = '';
	tableFooter +='<tfoot>';

 if (timeFrame=="Week") timeFrame = "in the last 7 days";


 //if the timeFrame is Today, print the target row
	if(timeFrame == "Today"){
		//target row
		for(i=0; i<stageColsLen+3; i++){
			if(i==0) tableFooter+="<tr><td><b>In Hand "+timeFrame+"</b></td>";
			else if(i==1) tableFooter+="<td>&nbsp;</td>";
			else if(i==2) tableFooter+="<td>&nbsp;</td>";
			else{
					j = i-3;
					if(j>=0 && j<stageColsLen){
						currCol = stageCols[j].toLowerCase();
						//if there are no articles at this stage, skip
						if(skipStageArray.indexOf(currCol)>=0) continue;
					}
					tableFooter+='<td><b>'+fullColumnTotal[i]+'</b></td>';
			}
		}
		tableFooter+="</tr>";
	}


	//achieved row
	for(i=0; i<stageColsLen+3; i++){
		if(i==0) tableFooter+="<tr><td><b>Achieved "+timeFrame+"</b></td>";
		else if(i==1) tableFooter+="<td>&nbsp;</td>";
		else if(i==2) tableFooter+="<td>&nbsp;</td>";
		else{
			j = i-3;
			if(j>=0 && j<stageColsLen){
				currCol = stageCols[j].toLowerCase();
				//if there are no articles at this stage, skip
				if(skipStageArray.indexOf(currCol)>=0) continue;
			}
			tableFooter+='<td><b>'+columnTotal[i]+'</b></td>';
		}
	}
	tableFooter+="</tr>";

	//if the timeFrame is Today, print the remaining in hand
	if(timeFrame == "Today"){
		//remaining at stage row
		for(i=0; i<stageColsLen+3; i++){
			if(i==0) tableFooter+="<tr><td><b>Remaining at stage</b></td>";
			else if(i==1) tableFooter+="<td>&nbsp;</td>";
			else if(i==2) tableFooter+="<td>&nbsp;</td>";
			else{
				j = i-3;
				if(j>=0 && j<stageColsLen){
					currCol = stageCols[j].toLowerCase();
					//if there are no articles at this stage, skip
					if(skipStageArray.indexOf(currCol)>=0) continue;
				}
				// change made by Hamza on Sep 9,2017 , to list urgent and overdue
				tableFooter+=`<td><b>${inHandColumnTotal[i]}</b>
				${overdueArrayByStage[currCol].length!=0?`(<font color="red">${overdueArrayByStage[currCol].length}</font>)`:``}
				${urgentArrayByStage[currCol].length!=0?`(<font color="green">${urgentArrayByStage[currCol].length}</font>)`:``}
				</td>`;
				// end of change made by Hamza
			}
		}
		tableFooter+="</tr>";
	}
	
	//New articles
	for(i=0; i<stageColsLen+3; i++){
		if(i==0) tableFooter+="<tr><td><b>New additions</b></td>";
		else if(i==1) tableFooter+="<td>&nbsp;</td>";
		else if(i==2) tableFooter+="<td>&nbsp;</td>";
		else{
			j = i-3;
			if(j>=0 && j<stageColsLen){
				currCol = stageCols[j].toLowerCase();
				//if there are no articles at this stage, skip
				if(skipStageArray.indexOf(currCol)>=0) continue;
			}
			tableFooter+='<td><b>'+newColumnTotal[i]+'</b></td>';
		}
	}
	tableFooter+="</tr>";

	//Net movement
	for(i=0; i<stageColsLen+3; i++){
		if(i==0) tableFooter+="<tr><td><b>Net movement</b></td>";
		else if(i==1) tableFooter+="<td>&nbsp;</td>";
		else if(i==2) tableFooter+="<td>&nbsp;</td>";
		else{
			j = i-3;
			if(j>=0 && j<stageColsLen){
				currCol = stageCols[j].toLowerCase();
				//if there are no articles at this stage, skip
				if(skipStageArray.indexOf(currCol)>=0) continue;
			}
			if(movementColumnTotal[i]<0) tableFooter+='<td><font color="red">'+movementColumnTotal[i]+'</font></td>';
			else tableFooter+='<td><b>'+movementColumnTotal[i]+'</b></td>';
		}
	}
	tableFooter+="</tr>";

	tableFooter +='</tfoot>';

	tableHeader+=tableFooter+tableBody+'</table>';

	return tableHeader;
}

//generate article stage table
generateArticleStageTable=function(customerName,displayType){
	var rowTotal = [];
	var columnTotal = [];
	var delayTotal = [];
	var skipStageArray = [];
	var stageName = "";
    stageCols = custStageCols[customerName];

	var stageColsLen = stageCols.length;
	var indexFilterName ="";

	//Initialize value for columnwise total variable
	for(i=0; i<stageColsLen+3; i++) {
		columnTotal[i] = 0;
		delayTotal[i] = 0;
	}

	var displayString = "Article";

	var tableHeader = '';
    tableHeader+='<a href="javascript:export_report()">Export to XLSX</a>';
	tableHeader +='<table id="stageTable" align="center" style="font-size: small;"><caption/><thead><tr><th style="text-align:left;width:110px">'+displayString+'</th><th style="text-align:left;width:110px">Proof page count</th><th style="text-align:left;width:110px">Invoice page count</th><th style="text-align:left;width:110px">Word count</th><th style="text-align:left;width:110px">Days in production</th><th style="text-align:left;width:110px">Current stage</th><th style="text-align:left;width:110px">Article type</th><th style="text-align:left;width:110px">Assignee</th><th style="text-align:left;width:110px">Accepted Date</th><th style="text-align:left;width:110px">Exported Date</th>';

	for(i=0; i<stageColsLen; i++){
		var stageName = stageCols[i].toLowerCase();
		var colData = totalStageArray[stageName];
		if(colData) colDataLen = colData.length;
		//if there are no articles at this stage, add to skip stage array and skip
		if(colDataLen == 0) {
			skipStageArray.push(stageName);
			continue;
		}
		tableHeader += '<th onClick="rowHiding(this)">'+stageCols[i]+'</th>';
	}

	var tableBody = '<tbody>';

	var articleArray = [];
	//sort the data based on acceptance date
	totalArray.sort(function (a, b) {
		var aDate = a["days"];
		var bDate = b["days"];
		return bDate-aDate;
	});

	var totalArrayLen = totalArray.length;
	for(j=0; j<totalArrayLen; j++) {
		var rowArray = totalArray[j];
		var cellVal = 0;
		var status = "";
		var name = "";
		var owner = "";

		var doi = rowArray["doi"];
		if (doi && typeof(doi) != 'undefined') {
			doi = doi.replace(/^[0-9\.\/]+/,"");
		}

		//check the display type and only display relevant articles
		if(displayType == "Published"){
			if(totalPublishedArray.indexOf(doi)<0) continue;
		}
		else if(displayType == "WIP"){
			if(totalPublishedArray.indexOf(doi)>-1) continue;
		}

		var pubID = rowArray["pubID"];

		var id = rowArray["id"];

  		var currCol;
		if(rowArray){
			for(i=0; i<stageColsLen; i++){
				currCol = stageCols[i].toLowerCase();
				rowArray["workflow"][currCol] = {};
				rowArray["workflow"][currCol]["date"] = "-";
				rowArray["workflow"][currCol]["count"] = 0;
			}
			var totalStages = rowArray["workflow"]["stage"];
			var totalStagesLen = 0;
			var endDate = ""; 
			if(totalStages) totalStagesLen = totalStages.length;
			for(k=0; k<totalStagesLen; k++) {
				status = rowArray["workflow"]["stage"][k]["status"];
				name = rowArray["workflow"]["stage"][k]["name"].toLowerCase();
				owner = rowArray["workflow"]["stage"][k]["assigned"]["to"];
				var startDate = rowArray["workflow"]["stage"][k]["start-date"];
				var dueDate = rowArray["workflow"]["stage"][k]["end-date"];
				if(startDate == "--") startDate = "";
				if(dueDate == "--") dueDate = "";
				var startDateObj;
				if(startDate) startDateObj = new Date(startDate);
				var dueDateObj;
				if (dueDate){
					dueDateObj = new Date(dueDate);
				}

				if(stageCols.customIndexOf(name)==-1){
					console.log("Invalid stage "+name+" for doi "+doi);
					var oldStageName = name;
					name = stageCols[0].toLowerCase();
					//console.log("Replacing invalid stage "+oldStageName+ " with stage "+name+" for doi "+doi);
				}

				if(status == "completed"){
					rowArray["workflow"][name]["count"] = rowArray["workflow"][name]["count"]+1;
					rowArray["workflow"][name]["date"] = "--";
					if(dueDateObj) rowArray["workflow"][name]["date"] =dateFormat(dueDateObj, "mediumDate");
				}
				else if(status == "in-progress"){
					rowArray["currentStage"] = name;
					rowArray["owner"] = owner;
					rowArray["workflow"][name]["count"] = rowArray["workflow"][name]["count"]+1;
					rowArray["workflow"][name]["date"] = "--";
					if(dueDateObj) rowArray["workflow"][name]["date"] =dateFormat(dueDateObj, "mediumDate");
				}
			}

			//if article is being held
			if(totalHoldArray.indexOf(doi)>-1){
				rowArray["currentStage"] = "On Hold";
			}

			//if the article has been published
			if(totalPublishedArray.indexOf(doi)>-1){
				rowArray["currentStage"] = "Published";
			}

			tableBody += '<tr>';
			var invoiceCount = 0;
			for(i=0; i<stageColsLen+10; i++){
				var currCol;
				if(i==0) {
					var dispName = doi;
					//if pub id is there, use that instead
					if(pubID && typeof(doi) != 'undefined'){
						dispName = pubID;
					}
					tableBody+='<td class="projectName" title="'+id+'">'+dispName+'</td>';
				}
				else if(i==1){
					var dispName = 0;
					if(rowArray["workflow"]["proof-count"]) dispName = rowArray["workflow"]["proof-count"];
					invoiceCount = Math.ceil(dispName);
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==2){
					tableBody+='<td class="projectName">'+invoiceCount+'</td>';
				}
				else if(i==3){
					var dispName = 0;
					if(rowArray["workflow"]["word-count"]) dispName = rowArray["workflow"]["word-count"];
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==4){
					var dispName = rowArray["days"];
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==5){
					if(rowArray["currentStage"] == "On Hold" || rowArray["currentStage"] == "Published") dispName = rowArray["currentStage"];
					else dispName = stageCols[stageCols.customIndexOf(rowArray["currentStage"])];
					//if article is marked as urgent
					if(totalUrgentArray.indexOf(doi)>-1){
						dispName = dispName+' (Urgent)';
					}
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==6){
					var dispName = rowArray["type"];
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==7){
					var dispName = '--';
					if(rowArray["owner"]) dispName = rowArray["owner"];
					tableBody+='<td class="projectName">'+dispName+'</td>';
				}
				else if(i==8){
					if (rowArray["acceptedDate"] == '--'){
						tableBody+='<td class="projectName">--</td>';
					}
					else{
						var dispName = dateFormat(new Date(rowArray["acceptedDate"]), "mediumDate");
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
				}
				else if(i==9){
					if (rowArray["exportedDate"] == '--'){
						tableBody+='<td class="projectName">--</td>';
					}
					else{
						var startDate = rowArray["start-date"];
						var dispName = '--';
						if(startDate && startDate != '--') dispName = dateFormat(new Date(), "mediumDate");
						tableBody+='<td class="projectName">'+dispName+'</td>';
					}
				}
				else{
					var colData = {};
					var colDataLen = 0;
					var overdueDataLen = 0;
					currCol = stageCols[i-10].toLowerCase();
					//if there are no articles at this stage, skip
					if(skipStageArray.indexOf(currCol)>=0) continue;
					colData = rowArray["workflow"][currCol]["date"]
					if(colData) {
						if(rowArray["workflow"][currCol]["count"]>1) cellVal = colData + '('+rowArray["workflow"][currCol]["count"] + ')';
						else cellVal = colData;
					}

					if(currCol == rowArray["currentStage"]) {
						cellVal = "In progress";
						colData = rowArray["workflow"][currCol]["date"];
						cellVal += ' ('+colData+')';
						tableBody += '<td style="color: red; text-align: center;">'+cellVal+'</td>';
					}
					else tableBody += '<td>'+cellVal+'</td>';

				}
			}
			tableBody += '</tr>';
		}
	}

	tableHeader +='</tr>';
	tableHeader+=tableBody+'</tr></tbody></table>';

	return tableHeader;


}

//generate article summary table
generateArticleSummaryTable=function(customerName,displayType){
	var rowTotal = [];
	var columnTotal = [];
	var delayTotal = [];
	var stageName = "";
    stageCols = custStageCols[customerName];
				stageTimes = custStageTimes[customerName];
	var stageColsLen = stageCols.length;
	var indexFilterName ="";

	//Initialize value for columnwise total variable
	for(i=0; i<stageColsLen+3; i++) {
		columnTotal[i] = 0;
		delayTotal[i] = 0;
	}

	var displayString = "Article";

	var tableHeader = '';
    tableHeader+='<a href="javascript:export_report()">Export to XLSX</a>';
	tableHeader +='<table id="stageTable" align="center" style="font-size: small;"><caption/><thead><tr><th style="text-align:left;width:110px">Journal</th><th style="text-align:left;width:110px">Article</th><th style="text-align:left;width:110px">Article type</th><th style="text-align:left;width:110px">Current stage</th><th style="text-align:left;width:110px">Current owner</th><th style="text-align:left;width:110px">Received Date</th><th style="text-align:left;width:110px">Due Date</th><th style="text-align:left;width:110px">Completed Date</th><th style="text-align:left;width:110px">Delay</th>'; //<th style="text-align:left;width:110px">Notes</th>

	var tableBody = '<tbody>';

	//sort the data based on acceptance date
	totalArray.sort(function (a, b) {
		var aDate = a["days"];
		var bDate = b["days"];
		return bDate-aDate;
	});

	var totalArrayLen = totalArray.length;

	for(j=0; j<totalArrayLen; j++) {
		var rowArray = totalArray[j];
		var cellVal = 0;
		var status = "";
		var name = "";
		var owner = "";
		var jrnl = "";

		var doi = rowArray["doi"]
		if (doi && typeof(doi) != 'undefined') {
			doi = doi.replace(/^[0-9\.\/]+/,"");
			jrnl = doi.replace(/[0-9\.\/\-]*/g,"");
		}

		//check the display type and only display relevant articles
		if(displayType == "Published"){
			if(totalPublishedArray.indexOf(doi)<0) continue;
		}
		else if(displayType == "WIP"){
			if(totalPublishedArray.indexOf(doi)>-1) continue;
		}

		var pubID = rowArray["pubID"];

		var id = rowArray["id"];

		var type = rowArray["type"];

  var currCol = "";
		if(rowArray){
			var currCol="";
			var displName = "";
			var days = 0;
			var delay = 0;
			var totalStages = rowArray["workflow"]["stage"];
			var totalStageLen = 0;
			if(totalStages) totalStagesLen = totalStages.length;
			for(k=0; k<stageColsLen; k++){
				currCol = stageCols[k].toLowerCase();
				rowArray["workflow"][currCol] = {};
				rowArray["workflow"][currCol]["date"] = "-";
				rowArray["workflow"][currCol]["count"] = 0
				rowArray["workflow"][currCol]["printed"] = false;
				rowArray["workflow"][currCol]["notes"] = "-";
				rowArray["workflow"][currCol]["delay"] = 0;
				rowArray["workflow"][currCol]["printed"] = false;
			}

			for(k=0; k<totalStagesLen; k++) {
				status = rowArray["workflow"]["stage"][k]["status"];
				name = rowArray["workflow"]["stage"][k]["name"].toLowerCase();
				owner = rowArray["workflow"]["stage"][k]["assigned"]["to"];
				days = 0;

				if(stageCols.customIndexOf(name)==-1){
					console.log("Invalid stage "+name+" for doi "+doi);
					var oldStageName = name;
					name = stageCols[0].toLowerCase();
					//console.log("Replacing invalid stage "+oldStageName+ " with stage "+name+" for doi "+doi);
				}

				if(stageTimes[stageCols.customIndexOf(name)]){
					days = stageTimes[stageCols.customIndexOf(name)];
				}

				var startDate;
				var dueDate;
				var startDateObj;
				var dueDateObj;
				if(status == "in-progress" || status == "completed"){
					rowArray["workflow"][name]["count"] = rowArray["workflow"][name]["count"]+1;
					startDateObj = new Date(rowArray["workflow"]["stage"][k]["start-date"]);
					//set the start date only for the first occurence
					if(rowArray["workflow"][name]["count"]==1){
						rowArray["workflow"][name]["start-date"] = dateFormat(startDateObj, "mediumDate");
						//console.log("start date "+startDate);
						dueDateObj = startDateObj.addDays(days);
						//console.log("days "+days+"due date "+dueDate);
						rowArray["workflow"][name]["due-date"] = dateFormat(dueDateObj, "mediumDate");
					}

					var endDate = rowArray["workflow"]["stage"][k]["end-date"];
					var endDateObj;
					if(endDate == "--") endDateObj = new Date(); 
					else endDateObj = new Date(endDate);
					rowArray["workflow"][name]["end-date"] = dateFormat(endDateObj, "mediumDate");

					//rowArray["workflow"][name]["notes"] = rowArray["workflow"]["notes"];
				}

				if(status == "in-progress"){
					rowArray["currentStage"] = name;

					var today = new Date();
					delay = workingDaysBetweenDates(dueDateObj,today);
					if(delay<0) delay = 0;
					rowArray["workflow"][name]["delay"] = delay;
				}
				else if(status == "completed") {
					delay = workingDaysBetweenDates(dueDateObj,endDateObj);
					if(delay<0) delay = 0;
					rowArray["workflow"][name]["delay"] = delay;
				}

			}

			//if article is being held
			if(totalHoldArray.indexOf(doi)>-1){
				rowArray["currentStage"] = "On Hold";
			}

			for(k=0; k<totalStagesLen; k++) {
				currCol = rowArray["workflow"]["stage"][k]["name"].toLowerCase();
				owner = rowArray["workflow"]["stage"][k]["assigned"]["to"];
				status = rowArray["workflow"]["stage"][k]["status"];
				if(status == "in-progress" || status == "completed"){
					//TEMPORARY ADD BY RAVI to handle BMJ
					if((customerName == "bmj")&&(currCol == "author review" || currCol == "publisher review" || currCol == "typesetting")) continue;
					//console.log("current column "+currCol);
					//console.log("Row ID "+rowArray['id']);
					//if the curr col stage has already been printed, skip
					if(rowArray["workflow"][currCol]["printed"]) continue;
					tableBody += '<tr>';

					dispName = jrnl;
					tableBody+='<td class="projectName" id="Journal">'+dispName+'</td>';

					dispName = doi;
					//if pub id is there, use that instead
					if(pubID && typeof(doi) != 'undefined'){
						dispName = pubID;
					}
					tableBody+='<td class="projectName" id="Article" title="'+id+'">'+dispName+'</td>';

					dispName = type;
					tableBody+='<td class="projectName">'+dispName+'</td>';

					dispName = stageCols[stageCols.customIndexOf(currCol)];

					//if article is marked as urgent
					if(status == "in-progress" && totalUrgentArray.indexOf(doi)>-1){
						dispName = dispName+' (Urgent)';
					}
					

					if(rowArray["workflow"][currCol]["count"]>1) dispName += '('+rowArray["workflow"][currCol]["count"] + ')';
					tableBody+='<td class="projectName" id="Stage">'+dispName+'</td>';

					dispName = owner;
					tableBody+='<td class="projectName" id="Article">'+dispName+'</td>';

					cellVal = '';
					colData = rowArray["workflow"][currCol]["start-date"];
					if(colData) {
							cellVal = colData;
							tableBody += '<td id="Start Date">'+cellVal+'</td>';
					}

					cellVal = '';
					colData = rowArray["workflow"][currCol]["due-date"]
					if(colData) {
						cellVal = colData;
						tableBody += '<td id="Due Date">'+cellVal+'</td>';
					}

					cellVal = '';
					colData = rowArray["workflow"][currCol]["end-date"];
					if(currCol == rowArray["currentStage"]) {
						//tableBody += '<td style="color: red; text-align: center;">'+cellVal+'</td>';
						cellVal = "In progress";
						cellVal += ' ('+colData+')';
						tableBody += '<td style="color: red; text-align: center;" id="Completed Date">'+cellVal+'</td>';
					}
					else tableBody += '<td id="Completed Date">'+colData+'</td>';

					cellVal = '';
					colData = rowArray["workflow"][currCol]["delay"]
					if(colData>=0) {
						cellVal = colData;
						tableBody += '<td id="Delay">'+cellVal+'</td>';
					}

/*
						cellVal = '';
						colData = rowArray["workflow"][currCol]["notes"]
						if(colData) {
							cellVal = colData;
							tableBody += '<td id="Notes">'+cellVal+'</td>';
						}
*/

					tableBody += '</tr>';
					rowArray["workflow"][currCol]["printed"] = true;

					//do not print the next stage if the article is in progress at a certain stage
					if(status == "in-progress"){
						break;
					}
				}
			}
		}
	}

	tableHeader +='</tr>';
	tableHeader+=tableBody+'</tr></tbody></table>';

	return tableHeader;


}

//generate article stage table
generateArticlePerformanceTable=function(customerName){
	var rowTotal = [];
	var columnTotal = [];
	var skipStageArray = [];
	var stageName = "";
    var stageCols = custStageCols[customerName];
	var stageTimes = custStageTimes[customerName];

	var stageColsLen = stageCols.length;

	var displayString = "Aging period";

	//Change made by Hamza on Sep 9, 2017 - Add the overdue and urgent rows
	agingArray['Overdue Articles'] = overdueArrayByStage;
	agingArray['Urgent Articles'] = urgentArrayByStage;
	//End of Change made by Hamza on Sep 9, 2017

	var agingArrayKeys = Object.keys(agingArray);
	var agingArrayLen = agingArrayKeys.length;

	var tableHeader = '';
    tableHeader+='<a href="javascript:export_report()">Export to XLSX</a>';
	tableHeader +='<table id="stageTable" align="center" style="font-size: small;"><caption/><thead><tr><th style="text-align:left;width:110px">'+displayString+'</th>';
	
	for(i=0; i<stageColsLen; i++){
		var stageName = stageCols[i].toLowerCase();
		var colData = agingArray["All"][stageName];
		if(colData) colDataLen = colData.length;
		//if there are no articles at this stage, add to skip stage array and skip
		if(colDataLen == 0) {
			skipStageArray.push(stageName);
			continue;
		}
		tableHeader += '<th onClick="rowHiding(this)">'+stageCols[i]+'</th>';
	}
	
	tableHeader += '<th>Total</th>'; 
	var tableBody = '<tbody>';

	for(j=0; j<agingArrayLen; j++) {
  		var currCol;
		var rowTotal = 0;
		for(i=0; i<stageColsLen+2; i++){
			var currCol;
			if(i==0) {
				var dispName = agingArrayKeys[j];
				tableBody+='<tr><td class="projectName">'+dispName+'</td>';
			}
			else if(i>stageColsLen){
				tableBody+='<td>'+rowTotal+'</td></tr>';
			}
			else{
				var colData;
				var colDataLen = 0;
				var cellVal = 0;
				currCol = stageCols[i-1].toLowerCase();
				//if there are no articles at this stage, skip
				if(skipStageArray.indexOf(currCol)>=0) continue;

				colData = agingArray[dispName][currCol];
				if(colData) {
					colDataLen = colData.length;
					cellVal = colDataLen;
					rowTotal = rowTotal + cellVal;
				}
				tableBody += '<td>'+cellVal+'</td>';
			}
		}
	}

	tableBody +='</tr>';

	var daysArrayKeys = Object.keys(daysArray);
	var daysArrayLen = daysArrayKeys.length;

	// changes made by Hamza on 12 Sep.., replot average days at stage block

	//Median and Average days at stage
	//Remove the published articles from the daysArray to aid in calculation
	var unpublishedArray=deleteKeys(daysArray,totalPublishedArray);
	var unpublishedKeys=Object.keys(unpublishedArray);
	var unpublishedKeysLen=unpublishedKeys.length;

	//Create days arrays to store the values
	var daysData = {};
	var daysDataTotal = [];
	var daysVal = 0;
	//loop though the unpublishedArray
	for(i=0; i<unpublishedKeysLen; i++){
		var currCol;
		for (var stages in unpublishedArray[unpublishedKeys[i]]) {
			if (daysData[stages] || daysData[stages]==0) {
				daysVal = unpublishedArray[unpublishedKeys[i]][stages];
				daysData[stages].push(daysVal);
				daysDataTotal[stages] =  daysDataTotal[stages] + daysVal;
			}
			else{
				daysData[stages]=[];
				daysDataTotal[stages] = 0;
				daysVal = unpublishedArray[unpublishedKeys[i]][stages];
				daysData[stages].push(daysVal);
				daysDataTotal[stages] =  daysDataTotal[stages] + daysVal;
			}
		}
	}

	var medianString = '';
	var averageString = '';
	var modeString = '';

	for(i=0; i<stageColsLen+1; i++){
		var currCol;
		if(i==0) {
			medianString+='<tr><td class="projectName">Median days</td>';
			averageString+='<tr><td class="projectName">Mean days</td>';
			modeString+='<tr><td class="projectName">Mode days</td>';
		}
		else{
			var colData;
			var colDataLen = 0;
			var medianVal = 0;
			var averageVal = 0;
			
			currCol = stageCols[i-1].toLowerCase();
			currTime = stageTimes[i-1];
			//if there are no articles at this stage, skip
			if(skipStageArray.indexOf(currCol)>=0) continue;

			if (currCol in daysData) {
				medianVal = medianValue(daysData[currCol]);
				averageVal = (daysDataTotal[currCol]/daysData[currCol].length).toFixed(2);
				modeVal = modeValue(daysData[currCol]);
			}
			else {
				medianVal = 0;
				averageVal = 0;
				modeVal = 0;
			}
			medianString += '<td>'+medianVal+' ('+currTime+')</td>';
			averageString += '<td>'+averageVal+' ('+currTime+')</td>';
			modeString += '<td>'+modeVal+' ('+currTime+')</td>';
		}
	}

	medianString += '</tr>';
	averageString += '</tr>';
	modeString += '</tr>';

	tableBody += medianString + averageString + modeString;
	// end of changes made by Hamza

	//Average publication time
	var timeString = "Time to Publication";
	var proofString = "Proof Count";
	var wordString = "Word Count";
	
	var daysTotalLastMonth = 0;
	var pageTotalLastMonth = 0;
	var wordTotalLastMonth = 0;
	var articleTotalLastMonth = 0;
	var daysTotalThisMonth = 0;
	var pageTotalThisMonth = 0;
	var wordTotalThisMonth = 0;
	var articleTotalThisMonth = 0;

	var medianThisMonth=[];
	var medianLastMonth=[];

	for(var j=0;j<daysArrayLen;j++){
		var doi = daysArrayKeys[j];
		//skip all the unpublished articles
		if(totalPublishedArray.indexOf(doi)<0) continue;
		
		//last month
		if(totalPublishedLastMonthArray.indexOf(doi)>=0){
			//time total
			colData = daysArray[doi][timeString];
			if(colData) {
				daysTotalLastMonth = daysTotalLastMonth + colData;
				medianLastMonth.push(colData);
			}
			//proof total
			colData = daysArray[doi][proofString];
			if(colData) pageTotalLastMonth = pageTotalLastMonth + Math.ceil(colData);
			//word total
			colData = daysArray[doi][wordString];
			if(colData) wordTotalLastMonth = wordTotalLastMonth + Math.ceil(colData);
			articleTotalLastMonth++;
		}

		//this month
		if(totalPublishedThisMonthArray.indexOf(doi)>=0){
			//time total
			colData = daysArray[doi][timeString];
			if(colData) {
				daysTotalThisMonth = daysTotalThisMonth + colData;
				medianThisMonth.push(colData);
			}				
			//proof total
			colData = daysArray[doi][proofString];
			if(colData) pageTotalThisMonth = pageTotalThisMonth + Math.ceil(colData);
			//word total
			colData = daysArray[doi][wordString];
			if(colData) wordTotalThisMonth = wordTotalThisMonth + Math.ceil(colData);
			articleTotalThisMonth++;
		}
	}

	
	//print this month's data
	cellVal = (daysTotalThisMonth/articleTotalThisMonth).toFixed(2);
	if(cellVal>0){
		tableBody+='<tr><td class="projectName">Mean Time to Publication (this month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = medianValue(medianThisMonth);
	if(cellVal>0){
		tableBody+='<td class="projectName">Median Time to Publication (this month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = articleTotalThisMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Articles published (this month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = pageTotalThisMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Pages published (this month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = wordTotalThisMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Words published (this month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	tableBody += '</tr>';

	//Print last month's data
	cellVal = (daysTotalLastMonth/articleTotalLastMonth).toFixed(2);
	if(cellVal>0){
		tableBody+='<tr><td class="projectName">Average Time to Publication (last month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = medianValue(medianLastMonth);
	if(cellVal>0){
		tableBody+='<td class="projectName">Median Time to Publication (last month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = articleTotalLastMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Articles published (last month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = pageTotalLastMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Pages published (last month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	cellVal = wordTotalLastMonth;
	if(cellVal>0){
		tableBody+='<td class="projectName">Words published (last month)</td>';		
		tableBody += '<td>'+cellVal+'</td>';
	}
	tableBody += '</tr>';
	tableHeader+=tableBody+'</tbody></table>';


	return tableHeader;

}


function parseDate(s) {
	 var b = s.split(/\D/);
	 return new Date(b[0], --b[1], b[2]);
}

/******************multiple filter start*******************/
// Display Article Cards
displayCards=function(filterName,filterType,articleList,removeData){
	var customerName = $('#customerselect').val();
	var stageCols = custStageCols[customerName];
	var stageColsLen = 0;
	if(stageCols) stageColsLen = stageCols.length;
	var card = "";
	var cards = "";
	var data = [];
	
	if(filterName == "All"){
		//get the unique array keys and sort
		var uniqueArrayKeys = Object.keys(uniqueArray);
		var uniqueArrayLen = uniqueArrayKeys.length;
		for(k=0; k<uniqueArrayLen; k++) {
			if(uniqueArray[uniqueArrayKeys[k]]){
				data = Array.prototype.concat.apply(data,uniqueArray[uniqueArrayKeys[k]]);
			}
		}

		var cardLen = data.length;

		//sort the data based on acceptance date
		if(cardLen>1) {
			data.sort(function (a, b) {
				var aDate = a["days"];
				var bDate = b["days"];
				return bDate-aDate;
			});
		}

		for(i=0;i<cardLen;i++) {
			var stageDetails = "";
			var stageName="";
			var stageOwner="";
			var stageDueDate="";
			var digestStatus="";
			var acceptedDate="";

			//Author Details
			var authors = "";
			var totalAuthors = "";
			if(data[i]["authors"]) totalAuthors = data[i]["authors"].name;
			var totalAuthorsLen = 0;
			if(totalAuthors) {
				totalAuthorsLen = totalAuthors.length;
				if(!totalAuthorsLen || totalAuthorsLen==0){
					totalAuthors[0] = data[i]["authors"].name;
					totalAuthorsLen = 1;
				}
			}

			//accepted date
			if (data[i]["acceptedDate"] && /([0-9a-z]+)\-([a-z0-9]+)/i.test(data[i]["acceptedDate"])){
				data[i]["acceptedDate"] = data[i]["acceptedDate"].replace(/\-/g, ' ');
			}
			if(data[i]["acceptedDate"] && data[i]["acceptedDate"]!='' && data[i]["acceptedDate"]!='--') data[i]["acceptedDate"] = dateFormat(new Date(data[i]["acceptedDate"]), "mediumDate");

			var totalStages = data[i]["workflow"]["stage"];
			var totalStagesLen = 0;
			if(totalStages) {
				totalStagesLen = totalStages.length;

				if(!totalStagesLen || totalStagesLen == 0){
					data[i]["workflow"]["stage"] = [];
					data[i]["workflow"]["stage"][0] = totalStages;
					totalStagesLen = 1;
				}
				//Initialize workflow data
				data[i]["workflow"]["stage"]["owner"] = "";
				data[i]["workflow"]["stage"]["name"] = "";
				data[i]["workflow"]["stage"]["dueDate"] = "";
				data[i]["workflow"]["stage"]["stageNo"] = "";
				data[i]["workflow"]["stage"]["priority"] = "";
				data[i]["workflow"]["stage"]["log"] = "";
			}

			for(j=0; j<totalAuthorsLen; j++) {
				var givenName = totalAuthors[j]["given-names"];
				if(typeof(givenName) === "object" && totalAuthors[j]["given-names"]["#text"]!=undefined && totalAuthors[j]["given-names"]["#text"]!=null){
					if (typeof(totalAuthors[j]["given-names"]["#text"]) == "object"){
						givenName = totalAuthors[j]["given-names"]["#text"].join('');
					}else if (totalAuthors[j]["given-names"]["#text"].trim()!=""){
						givenName = totalAuthors[j]["given-names"]["#text"];
					}
				} 
				var surname = totalAuthors[j]["surname"];
				if(typeof(surname) === "object" && totalAuthors[j]["surname"]["#text"] != undefined && totalAuthors[j]["surname"]["#text"]!= null) {
					if (typeof(totalAuthors[j]["surname"]["#text"]) == "object"){
						surname = totalAuthors[j]["surname"]["#text"].join('');
					}else if (totalAuthors[j]["surname"]["#text"].trim() != ""){
						surname = totalAuthors[j]["surname"]["#text"];
					}
				}

				if(j>0 && (typeof(givenName)!="object" && typeof(surname)!="object" && givenName!=undefined && surname!=undefined)) authors = authors + ", " + givenName + " " + surname;
				else if(typeof(givenName)!="object" && typeof(surname)!="object" && givenName!=undefined && surname!=undefined) authors = givenName + " " + surname;
			}

			if(authors) data[i]["authorString"] = authors;
			else data[i]["authorString"] = '';

			if(data[i]["authors"]){
				var emailLen  = 0;
				if(data[i]["authors"]["email"]!=undefined)
					emailLen = data[i]["authors"]["email"].length;
				var emailStr = "";
				for(j=0;j<emailLen;j++){
					if(emailStr==""){
						emailStr = data[i]["authors"]["email"][j]["#text"];
					}else{
						emailStr += ","+data[i]["authors"]["email"][j]["#text"];
					}
				}
			}
			
			if(emailStr) data[i]["emailString"] = emailStr;
			else data[i]["emailString"] = '';
			
			if(data[i]["doi"] && typeof(data[i]["doi"]) != 'undefined') data[i]["doi"] = data[i]["doi"].replace(/^[0-9\.\/]+/,"");
			else data[i]["doi"] = data[i]["id"];

			var articleTitle = data[i]["title"];
			var titleLength =0;
			if(articleTitle) titleLength = articleTitle.length;
			else data[i]["title"] = "Error in file "+data[i]["id"];

			if (titleLength > 120) {
				articleTitle = articleTitle.substring(0, 120) + " ...";
				data[i]["title"] = articleTitle;
			}

			//Related articles
			var relArticles = data[i]["relatedArticles"];
			var relArticlesLen = 0;
			var relArticleHTML = "";
			if(relArticles) {
				relArticles = data[i]["relatedArticles"]["related-article"];
				relArticlesLen = relArticles.length;
				if(!relArticlesLen || relArticlesLen==0) {
					relArticles[0] = data[i]["relatedArticles"]["related-article"];
					relArticles[0]["xlink:href"] = data[i]["relatedArticles"]["related-article"]["xlink:href"];
					relArticlesLen = 1;
				}
				if(relArticlesLen>1){
					//console.log("More than one related article '+relArticlesLen+' DOI "+data[i]["doi"]);
				}
			}
			for(j=0; j<relArticlesLen; j++) {
				if(j>0) relArticleHTML += '<span>&nbsp;</span><span class="articleNumber">'+relArticles[j]["xlink:href"].replace(/[0-9\.\/]+/,"")+'</span>';
				else relArticleHTML = '<span class="articleNumber">'+relArticles[j]["xlink:href"].replace(/[0-9\.\/]+/,"")+'</span>';
			}

			if(relArticleHTML) data[i]["relArticleHTML"] = relArticleHTML;
			else data[i]["relArticleHTML"] = '';


			//CE Level - set it to 1 if not set
			if(!data[i]["CELevel"] || data[i]["CELevel"] == "null"){
				data[i]["CELevel"] = "1";
			}

			var stageArticleHTML = '';
			//current date
			var todayDateObj = new Date();

			for(j=0; j<totalStagesLen; j++) {
				var status = data[i]["workflow"]["stage"][j]["status"];
				var owner = data[i]["workflow"]["stage"][j]["assigned"]["to"];
				var name = data[i]["workflow"]["stage"][j]["name"];
				var startDate = data[i]["workflow"]["stage"][j]["start-date"];
				var dueDate = data[i]["workflow"]["stage"][j]["end-date"];
				var stagePriority = data[i]["workflow"]["stage"][j]["priority"];
				var holdComm = data[i]["workflow"]["stage"][j]["comments"];
				if(startDate == "--") startDate = "";
				if(dueDate == "--") dueDate = "";
				var startDateObj;
				if(startDate) startDateObj = new Date(startDate);
				var dueDateObj;
				if (dueDate){
					dueDateObj = new Date(dueDate);
				}
				var days = workingDaysBetweenDates(startDateObj,dueDateObj);
				var version = 'v'+(j+1);
				if(status == "in-progress"){
					data[i]["workflow"]["stage"]["owner"] = owner;
					data[i]["workflow"]["stage"]["name"] = name;
					data[i]["workflow"]["stage"]["dueDate"] = "--";
					if(dueDateObj) data[i]["workflow"]["stage"]["dueDate"] = dateFormat(dueDateObj, "mediumDate");
					data[i]["workflow"]["stage"]["stageNo"] = j;
					if(stagePriority!=null && stagePriority!="" && stagePriority!=undefined && stagePriority=="on" && data[i]["workflow"]["stage"][j]["job-logs"]!=null && data[i]["workflow"]["stage"][j]["job-logs"]!=undefined){
						data[i]["workflow"]["stage"]["priority"] = stagePriority;
						data[i]["workflow"]["stage"]["log"] = data[i]["workflow"]["stage"][j]["job-logs"]['log'];
					}else if(data[i]["workflow"]["priority"]!=null && data[i]["workflow"]["priority"]!="" && data[i]["workflow"]["priority"]!=undefined && data[i]["workflow"]["priority"]=="on" && data[i]["workflow"]["stage"][j]["job-logs"]!=null && data[i]["workflow"]["stage"][j]["job-logs"]!=undefined){
						data[i]["workflow"]["stage"]["log"] = data[i]["workflow"]["stage"][j]["job-logs"]['log'];
					}else if(name=="Hold"){
						data[i]["workflow"]["stage"]["comments"]=holdComm;
					}
					//Set the color for the card based on the due date status
					var diffDays =  daysBetweenDates(dueDateObj,todayDateObj);
					if (diffDays > 0) data[i]["cardColor"] = "redCard";
					else if(diffDays == 0) data[i]["cardColor"] = "yellowCard";
					else if(diffDays < 0) data[i]["cardColor"] = "greenCard";

				}
				var tooltipComm = "";
				if(name=="Hold" && holdComm!=null && holdComm!=undefined && holdComm!=""){
					tooltipComm = 'data-tooltip="'+holdComm+'"';
				}
				var medStartDate = '';
				if (startDateObj) medStartDate = dateFormat(startDateObj, "mediumDate");
				var medEbdDate = '';
				if (dueDateObj) medEbdDate = dateFormat(dueDateObj, "mediumDate");
				if(status!='waiting' && status != "in-progress" ) stageArticleHTML += '<tr><td>'+name+'</td><td>'+owner+'</td><td>'+dateFormat(startDate, "mediumDate")+ '</td><td>'+dateFormat(dueDate, "mediumDate")+'</td><td '+tooltipComm+'>'+status+'</td><td>'+days+'</td><td>'+version;
				if(status == "in-progress")
				stageArticleHTML += '<tr><td>'+name+'</td><td id="editingName">'+'<div class="input-field col s6" id="AuthorEdit"><input id="edit_name" type="text" class="validate editing"></div>'+'</td><td>'+'<a class="kriyaDatePic"><span id="pickDateStart'+i+'"></span><span class="Pickdate" onclick="pickDateNew('+i+', \'Start\')" start-date="'+startDate+'" id="dateStart'+i+'">pick date</span></a>'+ '</td><td>'+'<a class="kriyaDatePic"><span id="pickDateEnd'+i+'"></span><span class="Pickdate" onclick="pickDateNew('+i+', \'End\')" end-date="'+dueDate+'" id="dateEnd'+i+'">pick date</span></a>'+'</td><td style="cursor:default" '+tooltipComm+'>'+status+'</td><td>'+days+'</td><td>'+version;
			}

			if(stageArticleHTML) data[i]["stageArticleHTML"] = stageArticleHTML;
			else data[i]["stageArticleHTML"] = '';

		}

		var customerName = $('#customerselect').val();
		var projectName = $('#projectselect').val();
		//cardLen = 100;
		for(i=0;i<cardLen;i++) {
			var middleRow = '';
			var articleRow = '';
			var cardColor = '';
			if(customerName == 'elife') middleRow = '<!-- Third Row --><div class="row bottomRow">'+
					'<div class="col custom-col-20 padding0" id="datecol"><div id="datepickerfont">Home:</div></div>'+
					'<div class="col custom-col-20 padding0" id="datecolpress"><div id="datepickerfont">Press:</div></div>'+
					'<div class="col custom-col-20 padding0"><div id="triangle-topleft2"><select class="selectjournal browser-default selectjournalsub"  id="mySelectstr" onchange="myFunctionstr()">Striking Image</select></div></div>'+
					'<div class="col custom-col-20 padding0"><div id="triangle-topleft"><select class="selectjournal browser-default selectjournalsub"  id="mySelect" onchange="myFunction()" >Digest</select></div></div>'+
					'<div class="col custom-col-20 padding0"><div id="triangle-topleft1"><select class="selectjournal browser-default selectjournalsub"   id="mySelectlet" onchange="myFunctionlet()">Decision Letter</select></div></div>'+
					'</div>';
			if(data[i]["relArticleHTML"] != ''){
				articleRow = '<span class="linkedarticle">'+data[i]["relArticleHTML"]+'</span><br>';
			}
			//set the links
			//var link = data[i]["use"]=="ready"?'/review_content/?role=publisher&doi=' + data[i]['doi'] +'&customer=' + customerName:"/not_ready";
			var dashboardLink = "";
			var dashboardURL = "";
			var link = '/review_content/?doi=' + data[i]['doi'] +'&customer=' + customerName +'&project=' + projectName;
			var stageName = data[i]["workflow"]["stage"]["name"];
			if(stageName.includes("Author")) link = "/with_author";
			if(data[i]["workflow"]["dashboard-url"]) {
				dashboardURL = data[i]["workflow"]["dashboard-url"];
				dashboardLink = '<span style="color: inherit !important;"></span>';
				//if the article-url is set, set the link to go to kriya 1.0
				if(data[i]["workflow"]["article-url"]) link = data[i]["workflow"]["article-url"];
			}
			// for pdf link
			if(data[i]['proofLink'] != null) {
				pdfHref =  '" target="_blank" href="' + data[i]['proofLink'].path + '"';
			}else{
				pdfHref =  ' disabled" href="#modalno5"';
			}
			
			var stageClass = stageName.toLowerCase().replace(/[\s\-]/,'');
			var prevStage = '';
			var onholdicon = holdStyle = iconColor = priorIcon = priorStyle = priorIconColor = holdIconTooltip = fastIconTooltip = '';
			var holdCheck = "false";
			var priorityCheck = "none";
			var publishButton = '';
			var reloadButton = '';
			var authorbtn = authorattr =  '';
			
			if(stageClass == 'hold'){
				holdCheck = "true";
				stageClass += " holdCard";
				prevStage = data[i]["workflow"]["stage"][data[i]["workflow"]["stage"].stageNo]['held-stage'];
				link = 'data-href="/review_content/?doi=' + data[i]['doi'] +'&customer=' + customerName +'&project=' + projectName +'"';
				if(data[i]["workflow"]["stage"]["comments"]!=null && data[i]["workflow"]["stage"]["name"]!="" && data[i]["workflow"]["stage"]["name"]!=undefined)
					holdIconTooltip = 'data-tooltip="'+data[i]["workflow"]["stage"]["comments"]+'"';
				onholdicon = '<span style="cursor:default" class="" '+holdIconTooltip+'><span>ON HOLD</span><i class="material-icons">lock_outline</i></span>';
				holdStyle = 'style="background-color:#4492e1;"';
				iconColor = 'style="color:#fff;"';
				priorStyle = 'style="background-color:#DFDFDF;" disabled="disabled"';				
			}else{
				stageClass ="";				
			}
			
			if(data[i]["workflow"]["priority"]!=null && data[i]["workflow"]["priority"]!="" && data[i]["workflow"]["priority"]!=undefined && data[i]["workflow"]["priority"]=="on"){
				stageClass = "fastTrackAll";
				if(data[i]["workflow"]["stage"]["log"]!=""){
					//var logLens = Object.keys(data[i]["workflow"]["stage"]["log"]).length;
					var finalCmts  = data[i]["workflow"]["stage"]["log"]["comments"];
					if(finalCmts!="" && finalCmts!=null && finalCmts!=undefined){
						fastIconTooltip = 'data-tooltip="'+finalCmts+'"';
					}				
				}
				priorIcon = '<span style="cursor:default" '+fastIconTooltip+' class="onUrgentAll"><i class="material-icons">directions_walk</i></span>';
				priorityCheck = "track-all";
				priorStyle = 'style="background-color:#FF9000;"';
				priorIconColor = 'style="color:#fff;"';
				holdStyle = 'style="background-color:#DFDFDF;" disabled="disabled"';
			}else if(data[i]["workflow"]["stage"]["priority"]!=null && data[i]["workflow"]["stage"]["priority"]!="" && data[i]["workflow"]["stage"]["priority"]!=undefined && data[i]["workflow"]["stage"]["priority"]=="on"){
				stageClass = "fastTrackCurrent";
				if(data[i]["workflow"]["stage"]["log"]!=""){
					//var logLens = Object.keys(data[i]["workflow"]["stage"]["log"]).length;
					var finalCmts  = data[i]["workflow"]["stage"]["log"]["comments"];
					if(finalCmts!="" && finalCmts!=null && finalCmts!=undefined){
						fastIconTooltip = 'data-tooltip="'+finalCmts+'"';
					}				
				}
				priorIcon = '<span style="cursor:default" '+fastIconTooltip+' class="onUrgentCurrent"><i class="material-icons">directions_run</i></span>';
				priorityCheck = "track-current";
				priorStyle = 'style="background-color:#ea3684;"';
				priorIconColor = 'style="color:#fff;"';
				holdStyle = 'style="background-color:#DFDFDF;" disabled="disabled"';
			}
			
			authorattr = 'onclick="authors(\''+data[i]["authorString"]+'\','+i+',\''+data[i]["emailString"]+'\')"';
			if(data[i]["authorString"]=="" || data[i]["authorString"]==null || data[i]["authorString"]==undefined){
				authorbtn = 'disabled';
				authorattr ="";
			}

			if (($('#customerselect').val() == 'frontiers') && (data[i]["workflow"]["stage"]["name"] == 'Final Deliverables')){
				publishButton = '<div><button class="btn btnstyle" onclick="publishFile($(this));" data-tooltip="Publish Files"><i class="material-icons whiteImages">cloud_upload</i></button></div>';
			}

			// to add re-load button for articles which are in File Download stage
			if (data[i]["workflow"] && data[i]["workflow"]["stage"] && data[i]["workflow"]["stage"]["name"] == 'File download'){
				if (data[i]["workflow"]["ftp-file-name"]){
					reloadButton = '<div><button class="btn btnstyle" data-ftp-file-name="' + data[i]["workflow"]["ftp-file-name"] + '" onclick="reloadArticle($(this));" data-tooltip="Re-try to load article"><span style="color: #858585;font-weight: 600;float: right;padding-right: 5px;">Re-try</span><i class="material-icons whiteImages">refresh</i></button></div>';
				}
			}
			
			// retrieve press date in dashboard
			if (data[i]["workflow"] && data[i]["workflow"]["data-press-date"]){
				var pressDate = data[i]["workflow"]["data-press-date"];
			}else{
				var pressDate = 'Press';
			}

			if (data[i]['digest'] == 2){
				var digestOptions = '<option value="Has">Has Digest</option>';
			}else if (data[i]['digest'] == 1){
				var digestOptions = '<option value="Awaiting">Awaiting Digest</option><option value="No">No Digest</option>';
			}else{
				var digestOptions = '<option value="No">No Digest</option><option value="Awaiting">Awaiting Digest</option>';
			}

			//Add the card to the list
			if (data[i]['decisionLetter'] == 2){
				var decisionOptions = '<option value="Has">Has Letter</option>';
			}else if (data[i]['decisionLetter'] == 1){
				var decisionOptions = '<option value="Awaiting">Awaiting Letter</option><option value="No">No Letter</option>';
			}else{
				var decisionOptions = '<option value="No">No Letter</option><option value="Awaiting">Awaiting Letter</option>';
			}

			//Add the card to the list
			card =	'<div class="jobCards '+stageClass+'" data-doiId="'+data[i]["doi"]+'"><div class="cardColor '+data[i]["cardColor"]+'"></div>'+'<div class="row topRow"><div id="addFastrackIcon'+i+'" class="col custom-col-20 stageName Main">'+data[i]["workflow"]["stage"]["name"]+' '+priorIcon+''+onholdicon+'</div><div class="col custom-col-20 stageName"><span style="color: inherit !important;"></span></div><div class="col custom-col-40 padding0" id="btnfloat">' + publishButton + reloadButton + '<div><button class="btn btnstyle disabled" alt="Missing Image"><i class="material-icons  whiteImages">replay</i></button></div>'+
					'<div><button class="btn listbtn btnstyle '+authorbtn+'" '+authorattr+' data-tooltip="Author list"><i class="material-icons  whiteImages">list</i></button></div>'+	
					'<div><button class="btn listbtn btnstyle" data-tooltip="Production notes" onclick="javascript:showComments(\''+customerName+'\',\''+projectName+'\',\''+data[i]["doi"]+'\')"><i class="material-icons  whiteImages">comment</i></button></div>'+			
					'<div><a class="btn fileup btnstyle' + pdfHref + ' id="myBtn'+i+'"><i class="material-icons  whiteImages">picture_as_pdf</i></a></div>'+
					'<div><button class="btn cloud" onclick="getBucketList(\'customer='+customerName+'&project='+projectName+'&doi='+data[i]["doi"]+'\',\'' + data[i]["doi"] + '\')" data-tooltip="Resources"><i class="material-icons  whiteImages">cloud</i></button></div>'+
					' <div id="myBtnschedule"><button class="btn btnstyle" data-tooltip="Schedule" onClick="toggleDiv(stage'+i+')"><i class="material-icons  whiteImages" >query_builder</i></button></div>'+
					'<div><button '+holdStyle+' class="btn pause '+stageClass+'" holded-card="'+holdCheck+'" customerName="'+customerName+'" projectName="'+projectName+'" doi="'+data[i]["doi"]+'" currStageName="'+stageName+'" prevStage="'+prevStage+'" onclick="pause(this,'+i+')" id="pausebtn'+i+'" data-tooltip="Hold"><i class="material-icons whiteImages"  '+iconColor+' id="pausecol'+i+'">pause</i></button></div>'+
					' <div><button '+priorStyle+' class="btn flag" urgent-card="'+priorityCheck+'" onclick="flag('+i+')" id="flagbtn'+i+'" data-tooltip="Fast Track"><i class="material-icons  whiteImages" '+priorIconColor+' id="flagcol'+i+'">flag</i></button></div></div></div>'+
					''+
					'<div class="row middleRow" id="carddetails"><div class="col custom-col-20">'+
					'<span class="ArticleIddata">'+data[i]["doi"]+'</span><br><span class="articleType">'+data[i]["type"]+'</span></div>'+
					'<div class="col custom-col-40" id="cardpadding2"><span class="articleTitle"><a href="'+link+'" style="color: inherit !important;" target="_blank">'+data[i]["title"].replace(/<[^>]+>/g, '')+'</a></span></div>'+
					'<div class="col custom-col-20" id="cardpadding3"><span class="acceptedDate">'+data[i]["acceptedDate"]+'</span><br><span class="Stageowner">'+data[i]["workflow"]["stage"]["owner"]+'</span><br><span class="daysInProd">'+data[i]["days"]+'</span></div>'+
					'<div class="col custom-col-20" id="cardpadding4">'+articleRow+
					'<span class="CElevel">'+data[i]["CELevel"]+'</span><br><span class="stageDue">'+data[i]["workflow"]["stage"]["dueDate"]+'</span></div>'+'</div>'+
					'<div id="stage'+i+'" class="row stageRow" hidden="true"><!-- col s12 --><div class=""><table class="bordered" style="margin-bottom:2%;"><tr><th>Stage Name</th><th>Owner</th><th>Start Date</th><th>End Date</th><th>Status</th><th>Days</th><th>Version</th></tr>'+
					data[i]["stageArticleHTML"]+
					'</table></div></div>'+
					'<!--bottomRow-->'+
					'<div class="row bottomRow"  style="display:none;">'+
					'<div class="col custom-col-20 padding0"><div class="triangle-topleft" id="triangle-topleft'+i+'"></div>'+
					'<select class="selectjournal browser-default selectjournalsub"  id="mySelectBox'+i+'" onchange="myFunction(this,'+i+')" >'+
					digestOptions+
					'<!--<option value="Has">Has Digest</option>-->'+
					'</select><button data-tooltip="Awaiting Digest" class="attachmentBox" id="mySelectUploadBox'+i+'"  onclick="openFileUpload(this);" data-upload-type="Digest" data-success="uploadDigest"><i class="fa fa-upload"></i></button></div>'+
					'<div class="col custom-col-20 padding0"><div class="triangle-topleft1" id="triangle-topleft1'+i+'"></div>'+
					'<select class="selectjournal browser-default selectjournalsub"   id="mySelectletBox'+i+'" onchange="myFunctionlet(this,'+i+')">'+
					decisionOptions+
					'<!--<option value="Has">Has Letter</option>-->'+
					'</select><button data-tooltip="Awaiting Letter" class="attachmentBox" id="mySelectletUploadBox'+i+'" onclick="openFileUpload(this);" data-upload-type="Decision Letter" data-success="uploadDigest"><i class="fa fa-upload"></i></button></div>'+
					'<div class="col custom-col-20 padding0"><div class="triangle-topleft2" id="triangle-topleft2'+i+'"></div>'+
					'<select class="selectjournal browser-default selectjournalsub"  id="mySelectstrBox'+i+'" onchange="myFunctionstr(this,'+i+')">'+
					'<option value="Awaiting">Awaiting Striking Image</option>'+
					'<option value="No">No Striking Image</option>'+
					'<!--<option value="Has">Has Striking Image</option>-->'+
					'</select><button  data-tooltip="Awaiting Striking Image" class="attachmentBox" id="mySelectstrUploadBox'+i+'" onclick="openFileUpload(this);" data-upload-type="Striking Image"><i class="fa fa-upload"></i></button></div>'+
					'<div class="col custom-col-20 padding0 firstRBorder" id="datecol1">'+
					'<span id="pickDateSchedule'+i+'"></span><span class="datepickerBox hand" id="homesty" onclick="pickDateNew('+i+', \'Schedule\')"><span class="Pickdate" id="dateSchedule'+i+'">Home</span> &nbsp; <i class="fa fa-calendar"></i></span></span></div>'+
					'<div class="col custom-col-20 padding0 pressDate" id="datecol1">'+
					'<span id="pickDateSchedulenext'+i+'" data-on-close="confirmPressDate"></span><span class="datepickerBox hand" id="homesty" onclick="pickDateNew('+i+', \'Schedulenext\')"><span class="Pickdate" id="dateSchedulenext'+i+'">' + pressDate + '</span> &nbsp; <i class="fa fa-calendar"></i></span></span></div>'+
					'</div>'+''+
					'</div>';
					cards += card;
					//$('#articleCards').append(card);

		}
		var dispName = filterName;
		if(dispName == "All") {
			filterName = "";
		}
		else if(journalArr[filterName]){
			dispName = journalArr[filterName];
		}
		else if(filterType == "Stage"){
			//if the dispName contains the owner as well
			if(dispName.indexOf('_')>-1){
				var splitStr = dispName.split('_');
				dispName = "Stage - "+splitStr[0]+", Owner - "+splitStr[1];
			}
			else{
				dispName = "Stage - "+dispName;
			}
		}
		else if(filterType == "Owner"){
			//if the dispName contains the stage as well
			if(dispName.indexOf('_')>-1){
				var splitStr = dispName.split('_');
				dispName = "Owner - "+splitStr[0]+", Stage - "+splitStr[1];
			}
			else{
				dispName = "Owner - "+dispName;
			}
		}
		else if(filterType == "Type"){
			dispName = "Article type - "+filterName;
		}

		var displayString = '';
		if(filterName) displayString += 'Filter: '+ dispName + ' | ';

		cards = '<div class="jobCard"><div class="articleID">'+ displayString + 'Total number of articles: ' + cardLen + '</div></div>' + cards + '</div></div>';
		$('#articleCards').html(cards);
		$('#articleCount').html(cardLen);
		$('#cardsColumn').nanoScroller({ alwaysVisible: true, scroll: 'top' });
		//id='#'+id;
		//$(id).html(card);

		//select only elife
		if($('#customerselect').val() == 'elife')
		{
		$('.bottomRow').removeAttr('style')
		}

		$('[data-tooltip]').tooltip({delay: 30, effect: 'toggle'});
	}
	else {
		var filterTypeChange = filterType.toLowerCase();
		var getFilterVal = getListval = updateFilterVal = updateListval = "";			
		getFilterVal = $('[data-'+filterTypeChange+'filter]')[0].getAttributeNode("data-"+filterTypeChange+"filter").value;
		getListval = $('[data-'+filterTypeChange+'list]')[0].getAttributeNode("data-"+filterTypeChange+"list").value;
		var filterExist = 0;
		if(removeData=="remove" && getFilterVal!=undefined && getListval!=undefined){
			var regex = "";
			if(articleList.indexOf(",")>=0){
				var artListVal = articleList.split(",");
				var artFiltrVal = filterName.split(",");
				var artListValLen = artListVal.length;
				var artFiltValLen = artFiltrVal.length;
				for(i=0;i<artListValLen;i++){
					regex = new RegExp("\\b"+artListVal[i]+"\\b");
					getFilterVal = getFilterVal.replace(regex,'');
				}
				for(i=0;i<artFiltValLen;i++){
					regex = new RegExp("\\b"+artFiltrVal[i]+"\\b");
					getListval = getListval.replace(regex,'');
				}
				updateFilterVal = getFilterVal;
				updateListval = getListval;
			}else{
				var regex1 = new RegExp("\\b"+articleList+"\\b");
				updateFilterVal = getFilterVal.replace(regex1,'');
				var regex2 = new RegExp("\\b"+filterName+"\\b");
				updateListval = getListval.replace(regex2,'');
			}		
		}else {
			// get and update the attribute value			
			if(getListval!="" && getListval!=undefined){
				var FiltersArr = getListval.split(",");
				if(FiltersArr.indexOf(filterName)>=0){
					filterExist = 1;
				}				
			}
			
			if(filterExist==0){
				var uniqueArrayKeys = new Array();		
				if(getFilterVal!=null && getFilterVal!="" && getFilterVal!=undefined && getListval!=null && getListval!="" && getListval!=undefined){
					updateFilterVal = getFilterVal+","+articleList;
					updateListval = getListval+","+filterName;
				}else{
					updateFilterVal = articleList;
					updateListval = filterName;
				}
			}
		}
		
		if(filterExist==0){		
			$('[data-'+filterTypeChange+'filter]').attr("data-"+filterTypeChange+"filter",updateFilterVal.replace(/,+/g,',').replace(/(^,)|(,$)/g, ""));
			$('[data-'+filterTypeChange+'list]').attr("data-"+filterTypeChange+"list",updateListval.replace(/,+/g,',').replace(/(^,)|(,$)/g, ""));
			var resSet = [];
			var typeFilterVal = typeListVal = stageFilterVal = stageListVal = ownerFilterVal = ownerListVal = "";
			
			typeFilterVal = $('[data-typefilter]')[0].getAttributeNode("data-typefilter").value;
			stageFilterVal = $('[data-stagefilter]')[0].getAttributeNode("data-stagefilter").value;
			ownerFilterVal = $('[data-ownerfilter]')[0].getAttributeNode("data-ownerfilter").value;
			
			if(typeFilterVal!="" && typeFilterVal!=null && typeFilterVal!=undefined){
				resSet = typeFilterVal.split(',');
			}
			if(stageFilterVal!="" && stageFilterVal!=null && stageFilterVal!=undefined){
				if(resSet.length>0){
					resSet = intersection_destructive(resSet,stageFilterVal.split(','));
				}
				else{
					resSet = stageFilterVal.split(',');
				}
			}
			if(ownerFilterVal!="" && ownerFilterVal!=null && ownerFilterVal!=undefined){
				if(resSet.length>0){
					resSet = intersection_destructive(resSet,ownerFilterVal.split(','));
				}
				else{
					resSet = ownerFilterVal.split(',');
				}
			}
			
			//get the unique array keys and sort
			// if(updateListval.indexOf(',')>-1) uniqueArrayKeys = updateListval.split(',');
			// else uniqueArrayKeys.push(updateListval);
			var uniqueArrayLen = resSet.length;
			var articleData = $("#articleCards .jobCards");
			articleData.attr('style', 'display:none');
			for(k=0; k<uniqueArrayLen; k++){
				var dataDoiId = resSet[k]; 
				if($('[data-doiid="'+dataDoiId+'"]').length>0){
					$('[data-doiid="'+dataDoiId+'"]').attr('style', 'display:block');
				}
			}			
			$(".articleID").html('Total number of articles: ' + unique(resSet).length);
			
			if($(".article_filters:checked").length==0){
				displayCards("All");
			}
		}
	}
}

function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

function intersection_destructive(arr1, arr2)
{
	var r = [], o = {}, l = arr2.length, i, v;
    for (i = 0; i < l; i++) {
        o[arr2[i]] = true;
    }
    l = arr1.length;
    for (i = 0; i < l; i++) {
        v = arr1[i];
        if (v in o) {
            r.push(v);
        }
    }
    return r;
}
/******************multiple filter end*******************/
//Get Customers
function getCustomers(data){
	customerArr = [];
	var customerArray = data.customer;
	var customersLen = 0;
	if(customerArray){
		customersLen = customerArray.length;
		if(!customersLen || customersLen == 0){
			customerArray[0] = data.customer;
			customersLen = 1;
		}
	}

	var myCustomers = '';
	if(customersLen>1) {
		myCustomers += '<option value="">Select</option>';
	}
	for(i=0; i<customersLen; i++) {
		var name = customerArray[i]["name"];
		var fullName = customerArray[i]["fullName"];
		//Trim the customer name
		fullName = fullName.replace(/^([a-zA-Z0-9]+)\s+(.*?)$/,'$1');
		myCustomers += '<option value="' + name +'">' + fullName + '</option>';
		customerArr[i] = name;
		//set the customer project stages
		setCustomerProjectStages(name);
	}
	return myCustomers;
}

//Get Journals
function getJournals(data){
	journalArr = {};
	journalPMArr = {};
	journalPEArr = {};
	var journalArray = data.project;
	var journalsLen = 0;
	var customerName = $('#customerselect').val();
	var docName = window.document.title;

  if(journalArray) {
	  journalsLen = journalArray.length;
	  //in case the length is still zero, an object was returned
	  if(!journalsLen || journalsLen == 0){
			journalArray[0] = data.project;
			journalsLen = 1;
	  }
	}
	var myJournals = '';

	//if there are more than one journals, display select
	if(journalsLen>1) {
		myJournals += '<option value="">Select</option>';
	}
	uniqueArray = {};
	totalTypeArray = {};
	totalStageOwnerArray = {};
	totalOwnerStageArray = {};
	totalArray = [];
	totalDOIArray = [];
	totalHoldArray = [];
	totalUrgentArray = [];
	totalPublishedArray = [];
	totalRoleArray = {};
	totalStageArray = {};
	stageArray = {};
	daysArray = {};

	var roleColsLen = roleCols.length;
	for(l=0;l<roleColsLen;l++){
		var currCol = roleCols[l]; 
		totalRoleArray[currCol] = [];
	}


	//initialize the customer stage cols
	var stageCols = custStageCols[customerName];
	var stageColsLen = 0;
	if(stageCols) stageColsLen = stageCols.length;

	agingArray["All"] = {};
	agingArray[">1d"] = {};
	//agingArray[">2d"] = {};
	agingArray[">4d"] = {};
	//agingArray[">7d"] = {};
	agingArray[">10d"] = {};
	//agingArray[">12d"] = {};
	agingArray[">15d"] = {};
	agingArray[">20d"] = {};
	//agingArray[">25d"] = {};
	agingArray[">30d"] = {};

	for(l=0;l<stageColsLen;l++){
		currCol = stageCols[l].toLowerCase();
		
		agingArray["All"][currCol] = [];
		agingArray[">1d"][currCol] = [];
		//agingArray[">2d"][currCol] = [];
		agingArray[">4d"][currCol] = [];
		//agingArray[">7d"][currCol] = [];
		agingArray[">10d"][currCol] = [];
		//agingArray[">12d"][currCol] = [];
		agingArray[">15d"][currCol] = [];
		agingArray[">20d"][currCol] = [];
		//agingArray[">25d"][currCol] = [];
		agingArray[">30d"][currCol] = [];

		//Change made by Hamza on Sep 9, 2017 - Intiialize the overdue and urgent rows
		overdueArrayByStage[currCol] = [];
		urgentArrayByStage[currCol] = [];
		//End of Change made by Hamza on Sep 9, 2017
	}

	for(i=0; i<journalsLen; i++) {
		var name = journalArray[i]["name"];
		var fullName = journalArray[i]["fullName"];
		var pm = journalArray[i]["pm"];
		var pe = journalArray[i]["pe"];
		//default select the first journal
		if(i==0) {
			myJournals += '<option value="' + name+'" selected>' + fullName + '</option>';
		}
		else{
			myJournals += '<option value="' + name+'">' + fullName + '</option>';
		}

		journalArr[name] = fullName;
		//set the pm name
		if(pm) journalPMArr[name] = pm;
		else journalPMArr[name] = '';
		//set the pe name
		if(pe) journalPEArr[name] = pe;
		else journalPEArr[name] = '';

		stageArray[name] = {};
		for(l=0;l<stageColsLen;l++){
			var currCol = stageCols[l].toLowerCase();
			stageArray[name][currCol] = [];
			totalStageArray[currCol] = [];
		}
		//if this is the report page
		if((docName.includes("Report"))) {
			getCustomerProjectArticles(customerName,name,journalsLen);
		}
	}
	//close the list
	return myJournals;
}


//Get Buckets
var getBuckets = function(data){
	bucketArr = [];
	var bucketArray = [];
	var bucketsLen = 0;
	if(data) {
		bucketArray = data.bucket;
	    bucketsLen = bucketArray.length;
	  	//in case the length is still zero, an object was returned
	    if(!bucketsLen || bucketsLen == 0){
		bucketArray[0] = data.bucket;
		bucketsLen = 1;
	  }
	}
	var myBuckets = '';
	//if there are more than one buckets, display select
	if(bucketsLen>1) {
		myBuckets += '<option value="">Select</option>';
	}

	for(i=0; i<bucketsLen; i++) {
	    var name = bucketArray[i]["name"];
	    var fullName = bucketArray[i]["fullName"];
		myBuckets += '<option value="' + name+'">' + fullName + '</option>';
		bucketArr[i] = name;
	}
	//close the list
	return myBuckets;
}


function updateWorkflowField(customerName,projectName,bucketName,doi,xpath,newXMLFrag){
	var message = "";
	jQuery.ajax({
		type: "GET",
		url: "/api/articlenode?customerName="+customerName+"&projectName="+projectName+"&bucketName="+bucketName+'&xpath='+xpath+'&doi=' + doi,
		contentType: "application/xml; charset=utf-8",
		dataType: "xml",
		success: function (data) {
			if (data){
				oldXMLFrag = data.documentElement;
				jQuery.ajax({
					type: "POST",
					url: "/api/updatearticle",
					data: '{"customerName": "' + customerName + '", "projectName": "' + projectName + '" , "customerName": "' + customerName + '", "bucketName": "' + bucketName + '" , "xpath": "' + xpath + '", "doi": "' + doi + '", "oldXMLFrag": "' + oldXMLFrag + '" , "newXMLFrag": "' + newXMLFrag + '"}',
					contentType: "application/xml; charset=utf-8",
					dataType: "xml",
					success: function (data) {
						if (data){
							 message = "The article with doi "+doi+" was successfully updated for field "+xpath;
							 return message;
						}
						else{
							message = "The article with doi "+doi+" could not be updated for field "+xpath;
							return message;
						}
					},
					error: function(xhr, errorType, exception) {
					  return null;
					}
				});
			}
			else{
			  return "Field not found";
			}
		},
		error: function(xhr, errorType, exception) {
		  return null;
		}
	});
}


function getStages(customerName,projectName,bucketName){
	jQuery.ajax({
		type: "GET",
		url: "/api/articlestages?customerName="+customerName+"&projectName="+projectName+"&articleStatus=in-progress",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			if (data){
				uniqueArray = {};
				totalArray = [];
				totalTypeArray = {};
				totalStageOwnerArray = {};
				totalOwnerStageArray = {};
				getProjectStages(customerName,projectName,bucketName);
			}
			else{
			  uniqueArray = {};
			  generateFilters();
			}
		},
		error: function(xhr, errorType, exception) {
		  return null;
		}
	});
}


$(document).ajaxError(function(event, jqxhr, settings, exception) {
	console.log('jqxhr.status: ' + jqxhr.status);
  if (jqxhr.status != 200) {
	  console.log (event, jqxhr, settings, exception);
   //handle status code 412 here
  }
});

$(document).ajaxSuccess(function(event, jqxhr, settings, exception) {
	console.log('jqxhr.status: ' + jqxhr.status);
  if (jqxhr.status != 200) {
	  console.log (event, jqxhr, settings, exception);
   //handle status code 412 here
  }
});

$(document).ready(function () {
	/*$(document).ajaxComplete(function(event, jqxhr, settings, exception) {
		console.log('complete jqxhr.status: ' + jqxhr.status);
	});*/
	$( document ).ajaxComplete(function() {
		$( ".log" ).text( "Triggered ajaxComplete handler." );
	});
	$.ajaxSetup({
		statusCode: {
			302: function(jqxhr, textStatus, errorThrown) {
				alert(textStatus + " " + errorThrown)
			}
		}
	})
	$('.la-container').fadeIn();
	setChapters();
	$('.la-container').fadeIn();
	//$('#dataContainer').height($(window).height() - $('.navBar').height() - $('#footerContainer').height());
	/*jQuery.ajax({
		type: "GET",
		url: "/api/customers",
		//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg){
				$('#customerselect').html(getCustomers(msg));
				if($('#customerselect option').length == 1) {
					$('#customerselect').trigger('change');
				}

				if($('#projectselect option').length == 1) {
					$('#bucketselect').trigger('change');
				}
				$('#projectselect').html(getJournals(''));
				$('#bucketselect').html(getBuckets(''));
				//$('#dataContainer').height($(window).height() - $('.navBar').height() - $('#footerContainer').height());
			}
			else{
				  return null;
			}
		},
		error: function(xhr, errorType, exception) {
			  return null;
		}
	});*/
});

$('#customerselect').change(function () {
	$('.preloader-wrapper').addClass('active');
	pmName = $('body').attr('data-pm')?$('body').attr('data-pm'):'';
	jQuery.ajax({
		type: "GET",
		url: "/api/projects?customerName="+$('#customerselect').val()+"&pmName="+pmName,
		//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg && !msg.error){
				$('#projectselect').html(getJournals(msg));
				var docName = window.document.title;
				//if this is the dashboard, default the first journal and get the stages
				//if((docName.includes("Dashboard"))&&($('#projectselect option').length == 1)) {
				if((docName.includes("Dashboard"))) {
					$('#projectselect').trigger('change');
				}
				//$('#dataContainer').height($(window).height() - $('.navBar').height() - $('#footerContainer').height());

			}
			else{
				  return null;
			}
		},
		error: function(xhr, errorType, exception) {
			  return null;
		}
	});
});

$('#projectselect').change(function () {
	jQuery.ajax({
		type: "GET",
		url: "/api/buckets?customerName="+$('#customerselect').val()+"&projectName="+$('#projectselect').val(),
		//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg && !msg.error){
				$('#bucketselect').html(getBuckets(msg));
				uniqueArray = {};
				totalArray = [];
				totalTypeArray = {};
				totalStageOwnerArray = {};
				totalOwnerStageArray = {};
				getStages($('#customerselect').val(),$('#projectselect').val(),$('#bucketselect').val());
				//$('#dataContainer').height($(window).height() - $('.navBar').height() - $('#footerContainer').height());
			}
			else{
				var jrnlName = $('#projectselect').val();
				//if the error was because the journal name was empty, get the journals for the customer.
				if(jrnlName == ""){
					$('#customerselect').trigger('change');
				}
				else return null;
			}
		},
		error: function(xhr, errorType, exception) {
			  return null;
		}
	});
});
function viewReports(){
	var my_window = window.open('/report','articleReport');
}

function journalReport() {
	var now = new Date();
	var html = '';
	var customerName = $('#customerselect').val();
	if(customerName == ''){
		$('#articleReports').html('<b>Choose Customer to generate report</b>');
	}
	else{
		$('.preloader-wrapper').addClass('active');
		setTimeout(function(){
			html = generateJournalStageTable(customerName);
			$('#articleReports').html(html);
			$('caption').html('<b>'+customerName+' - Journal Summary Report (Generated on '+now+')</b>');
			initReportTable();
		},500);
	}
}

function movementReport(timeFrame) {
	var now = new Date();
	var html = '';
	var customerName = $('#customerselect').val();
	if(!timeFrame || timeFrame == "") timeFrame = "Today";

	if(customerName == ''){
		$('#articleReports').html('<b>Choose Customer to generate report</b>');
	}
	else{
		$('.preloader-wrapper').addClass('active');
		setTimeout(function(){
			html = generateJournalMovementTable(customerName,timeFrame);
			$('#articleReports').html(html);
			$('caption').html('<b>'+customerName+' - Journal Movement Report for '+timeFrame+ ' (Generated on '+now+')</b>');
			initReportTable();
		},500);
	}
}

function articleReport(displayType) {
	var now = new Date();
	var html = '';
	if(!displayType || displayType == "") displayType = "WIP";
	var customerName = $('#customerselect').val();

	if(customerName == ''){
		$('#articleReports').html('<b>Choose Customer to generate report</b>');
	}
	else{
		$('.preloader-wrapper').addClass('active');
		setTimeout(function(){
			html = generateArticleStageTable(customerName,displayType);
			$('#articleReports').html(html);
			$('caption').html('<b>'+customerName+' - Article Summary Report (Generated on '+now+'</b>');
			initReportTable();
		},500);
	}
}

function articleSummaryReport(displayType) {
	var now = new Date();
	var html = '';
	if(!displayType || displayType == "") displayType = "WIP";
	var customerName = $('#customerselect').val();

	if(customerName == ''){
		$('#articleReports').html('<b>Choose Customer to generate report</b>');
	}
	else{
		$('.preloader-wrapper').addClass('active');
		setTimeout(function(){
			html = generateArticleSummaryTable(customerName,displayType);
			$('#articleReports').html(html);
			$('caption').html('<b>'+customerName+' - Article Stage History Report (Generated on '+now+'</b>');
			initReportTable();
		},500);
	}
}

function articlePerformanceReport() {
	var now = new Date();
	var html = '';
	var customerName = $('#customerselect').val();

	if(customerName == ''){
		$('#articleReports').html('<b>Choose Customer to generate report</b>');
	}
	else{
		$('.preloader-wrapper').addClass('active');
		setTimeout(function(){
			html = generateArticlePerformanceTable(customerName);
			$('#articleReports').html(html);
			$('caption').html('<b>'+customerName+' - Performance Metrics Report (Generated on '+now+'</b>');
			initReportTable();
		},500);
	}
}

function xml2json(xml) {
  try {
    var obj = {};
    if (xml.children.length > 0) {
      for (var i = 0; i < xml.children.length; i++) {
        var item = xml.children.item(i);
        var nodeName = item.nodeName;

        if (typeof (obj[nodeName]) == "undefined") {
          obj[nodeName] = xml2json(item);
        } else {
          if (typeof (obj[nodeName].push) == "undefined") {
            var old = obj[nodeName];

            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(xml2json(item));
        }
      }
    } else {
      obj = xml.textContent;
    }
    return obj;
  } catch (e) {
      console.log(e.message);
  }
};

function showComments(customer,project,doi){
	var url = "/api/articlenode?doi=#DOI#&projectName=#PROJECT#&customerName=#CUSTOMER#&xpath=//notes";
	var modalDiv = $(".comments-modal");
	if (modalDiv.length === 0){
		$("body").append('<div class="modal comments-modal" style="display:none;"><div class="modal-wrapper"><div class="modal-container"><div class="modal-header"><h3 class="holdHeader">Production Notes</h3><button type="button" class="modal-action modal-close close"><span>×</span></button></div><div class="modal-content"><div class="comments-area jquery-comments"><div class="data-container" data-container="comments"></div><div class="commenting-field main"><div class="textarea-wrapper"><div class="textarea" data-placeholder="Add a comment" contenteditable="true" style="height: 3.65em;"></div><div class="control-row" style=""><span class="send save enabled highlight-background" onclick="postComments(this)"><i class="material-icons">send</i>&nbsp;Send</span><span class="enabled upload" data-tooltip="Upload Files" onclick="openFileUpload(this);" data-upload-type="Files" data-success="attachFileToNotes"><i class="fa fa-upload"></i>&nbsp;upload</span></div></div></div></div></div></div></div></div>');
		modalDiv = $(".comments-modal");
	}

	/*$('.comments-area').comments({
		enableNavigation : false,
		enableUpvoting : false,
		enableAttachments : true,
		fieldMappings : {
			"fileURL" : "fileurl"
		},
		timeFormatter: function(time) {
			var d = new Date(time);
			return d.toLocaleDateString() +" " + d.toLocaleTimeString();
    	},
    	postComment: function(commentJSON, success, error) {
	        $.ajax({
	            type: 'POST',
	            url: '/api/save_note',
	            processData: false,
	            contentType: 'application/json; charset=utf-8',
	            data: JSON.stringify({
	            	"content" : commentJSON,
	            	"doi" : doi,
	            	"customer" : customer,
	            	"journal" : project
	            }),
	            success: function(comment) {
	                success(comment);
	            },
	            error: error
	        });
	    },
    	uploadAttachments : function(commentArray,success,error){
    		var responses = 0;
	        var successfulUploads = [];

	        var serverResponded = function() {
	            responses++;
	            if(responses == commentArray.length) {
	                // Case: all failed
	                if(successfulUploads.length == 0) {
	                    error();
	                } else {
	                	//some succeeded
	                    success(successfulUploads)
	                }
	            }
	        }

    		$(commentArray).each(function(index, commentJSON) {
				var file = commentJSON.file;
				var formData = new FormData();
				formData.append('uploads[]', file, file.name);
				formData.append("customer", customer);
				formData.append("project", project);
				formData.append("doi", doi);

				$.ajax({
					url: '/api/uploadfiles',
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(data){
						var sourceData = '../resources/' + customer + '/' + project + '/' + doi + '/' + data;
						commentJSON.fileurl = sourceData;
						//debugger;
						$.ajax({
				            type: 'POST',
				            url: '/api/save_note',
				            processData: false,
				            contentType: 'application/json; charset=utf-8',
				            data: JSON.stringify({
				            	"content" : commentJSON,
				            	"doi" : doi,
				            	"customer" : customer,
				            	"journal" : project
				            }),
				            success: function(comment) {
				                successfulUploads.push(comment);
				                serverResponded();
				            },
				            error: error
				        });
                    	
					},
					error: function(e){
						console.log("Upload failed with error = " + e);
						serverResponded();
					},
				});
    		});
    	},
	    getComments: function(success, error) {
	    	var notesURL = url.replace("#DOI#",doi).replace("#CUSTOMER#",customer).replace("#PROJECT#",project);
	    	jQuery.ajax({
				type: "GET",
				url: notesURL,
				//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
				contentType: "application/xml; charset=utf-8",
				dataType: "xml",
				success: function (msg) {
					//alert(msg);
					if (msg && !msg.error){
						var jsonObj = xml2json(msg);
						var commentsArray = [];
						if (jsonObj.notes && jsonObj.notes.note){
							commentsArray = jsonObj.notes.note;
							if (!Array.isArray(commentsArray)){
								commentsArray = [commentsArray];
							}
						}
				        success(commentsArray);
				        modalDiv.openModal('open');
					}
					else{
						success([]);
					  	modalDiv.openModal('open');
					}
				},
				error: function(xhr, errorType, exception) {
					  success([]);
					  modalDiv.openModal('open');
				}
			});
	    }
	});*/
	$(modalDiv).find('.data-container').find('#comment-list').remove()
	$(modalDiv).find('.data-container').append('<ul id="comment-list" class="main"/>');
	$('.modal.comments-modal').attr('data-customer', customer).attr('data-project', project).attr('data-doiid', doi);
	var notesURL = url.replace("#DOI#",doi).replace("#CUSTOMER#",customer).replace("#PROJECT#",project);
	jQuery.ajax({
		type: "GET",
		url: notesURL,
		//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
		contentType: "application/xml; charset=utf-8",
		dataType: "xml",
		success: function (msg) {
			//alert(msg);
			if (msg && !msg.error){
				var commentObj = $(msg);
				$(commentObj).find('note').each(function(){
					var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + $(this).find('created').html() + '">' + $(this).find('created').html() + '</span><span class="comment-reply" onclick="replyComment(this)"><i class="material-icons">reply</i></span></div><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
					commentDiv.find('*[data-class]').each(function(){
						$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
					});
					//it the current on is reply for some other, the add it next to it
					if ($(this).find('parent').length > 0 && $('li.comment[data-id="' + $(this).find('parent').html() + '"]').length > 0){
						var parentID = $(this).find('parent').html();
						commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
						//add before next comment
						if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0){
							$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
						}else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0){
						//add after last reply
							$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
						}else{//or aff next to parent
							$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
						}
					}else{//append to comment list
						$(modalDiv).find('#comment-list').append(commentDiv);
					}
				});
				modalDiv.openModal('open');
			}
			else{
				//success([]);
				modalDiv.openModal('open');
			}
		},
		error: function(xhr, errorType, exception) {
			  //success([]);
			  modalDiv.openModal('open');
		}
	});	
}
//after uploading file to aws, add the file path as html content to comment
function attachFileToNotes(status){
	console.log(status)
	if (typeof(status.fileDetailsObj.dstKey) == "undefined") return false;
	var fileContent = '<span class="comment-file"><a href="/resources/' + status.fileDetailsObj.dstKey + '" target="_blank">' +  status.fileDetailsObj.name + status.fileDetailsObj.extn + '</a></span>';
	$('.commenting-field.main').find('.textarea').append(fileContent);
	$('#upload_model').closeModal();
	//postComments($('.commenting-field.main').find('.textarea'));
}
//on click send - save notes to xml
function postComments(saveBtn){
	var contentDiv = $(saveBtn).closest('.commenting-field.main').find('.textarea');
	var d = new Date();
	var currTime = d.toLocaleDateString() +" " + d.toLocaleTimeString();
	var commentJSON = {
						"content": contentDiv.html(), 
						"fullname": $('.username').html(), 
						"created": currTime
					}
	// for replying to particular comment, send its parent id
	if ($('li.comment.replying').length > 0){
		commentJSON.parent = $('li.comment.replying').attr('data-id');
	}
	$.ajax({
		type: 'POST',
		url: '/api/save_note',
		processData: false,
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			"content" : commentJSON,
			"doi" : $('.modal.comments-modal').attr('data-doiid'),
			"customer" : $('.modal.comments-modal').attr('data-customer'),
			"journal" : $('.modal.comments-modal').attr('data-project')
		}),
		success: function(comment) {
			var commentDiv = $('<li data-id="' + comment.id + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="name">' + comment.fullname + '</span><span class="comment-time" data-original="' + comment.created + '">' + comment.created + '</span><span class="comment-reply" onclick="replyComment(this)"><i class="material-icons">reply</i></span></div><div class="wrapper"><div class="content">' + comment.content + '</div></div></div></li>');
			if (comment.parent && $('li.comment[data-id="' + comment.parent + '"]').length > 0){
				var parentID = comment.parent;
				commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
				if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0){
					$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
				}else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0){
					$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
				}else{
					$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
				}
				$('li.comment').removeClass('replying');
			}else{
				$('#comment-list').append(commentDiv);
			}
			$('.commenting-field.main').find('.textarea').html('');
		}
	});
}

function replyComment(replyBtn){
	var commentBox = $(replyBtn).closest('li.comment');
	if (commentBox.hasClass('replying')){
		commentBox.removeClass('replying')
	}else{
		$('li.comment').removeClass('replying');
		commentBox.addClass('replying');
		$('.commenting-field.main').find('.textarea').focus();
	}
}
 
function toggleDiv(divID) { 
  $(divID).toggle();
}

//assumes that we do not work on the weekend
function workingDaysBetweenDates(startDate, endDate) {

  // Validate input
  //if (endDate < startDate) return 0;

  // Calculate days between dates
  var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
  //startDate.setHours(0,0,0,1); // Start just after midnight
  startDate.setHours(23,59,59,999); // Start just before midnight
  endDate.setHours(23,59,59,999); // End just before midnight
  var diff = endDate - startDate; // Milliseconds between datetime objects
  var days = Math.ceil(diff / millisecondsPerDay);

  // Subtract two weekend days for every week in between
  var weeks = Math.floor(days / 7);
  days = days - (weeks * 2);

  // Handle special cases
  var startDay = startDate.getDay();
  var endDay = endDate.getDay();

  // Remove weekend not previously removed.
  if (startDay - endDay > 1)
    days = days - 2;

  // Remove start day if span starts on Sunday but ends before Saturday
  if (startDay == 0 && endDay != 6)
    days = days - 1

  // Remove end day if span ends on Saturday but starts after Sunday
  if (endDay == 6 && startDay != 0)
    days = days - 1

  return days;
}

//assumes that Saturday is a working day
function exeterWorkingDaysBetweenDates(startDate, endDate) {

  // Validate input
  //if (endDate < startDate) return 0;

  // Calculate days between dates
  var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
  //startDate.setHours(0,0,0,1); // Start just after midnight
  startDate.setHours(23,59,59,999); // Start just before midnight
  endDate.setHours(23,59,59,999); // End just before midnight
  var diff = endDate - startDate; // Milliseconds between datetime objects
  var days = Math.ceil(diff / millisecondsPerDay);

  // Subtract one weekend days for every week in between
  var weeks = Math.floor(days / 7);
  days = days - (weeks * 1);

  // Handle special cases
  var startDay = startDate.getDay();
  var endDay = endDate.getDay();

  // Remove weekend not previously removed.
  if (startDay - endDay > 1)
    days = days - 1;

  return days;
}

//calendar days between dates
function daysBetweenDates(startDate, endDate) {

  // Validate input
  //if (endDate < startDate) return 0;

  // Calculate days between dates
  var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
  //startDate.setHours(0,0,0,1); // Start just after midnight
  startDate.setHours(23,59,59,999); // Start just before midnight
  endDate.setHours(23,59,59,999); // End just before midnight
  var diff = endDate - startDate; // Milliseconds between datetime objects
  var days = Math.ceil(diff / millisecondsPerDay);

  return days;
}

//To get the an object based on property
function arrayObjectIndexOf(myArray, searchTerm, property) {
  for(var i = 0, len = myArray.length; i < len; i++) {
    if (myArray[i][property] === searchTerm) return i;
  }
  return -1;
}

//To add days to Date
Date.prototype.addDays = function(days) {
  this.setDate(this.getDate() + parseInt(days));
  return this;
};

//case insensitive index of
Array.prototype.customIndexOf = function (searchElement, fromIndex) {
  return this.map(function (value) {
    return value.toLowerCase();
  }).indexOf(searchElement.toLowerCase(), fromIndex);
};

// changes made by Hamza on 12 Sep, 2017
//delete the specified keys from the original array
function deleteKeys(original,keysArr){
	var objArr=JSON.parse(JSON.stringify(original));
	for(var ar of keysArr){
		delete objArr[ar];
	}
	return objArr;
}

//calculate median value of an array
function medianValue(values) {
	values.sort( function(a,b) {return a - b;} );
	var half = Math.floor(values.length/2);
	if(values.length % 2)
		return values[half];
	else
		return ((values[half-1] + values[half]) / 2.0).toFixed(2);
}

// calculate mode value of an array
function modeValue(values)
{
    var modeMap = {},
        maxCount = 1, 
        modes = [values[0]],
        valueLen=values.length;

    for(var i = 0; i < valueLen; i++)
    {
        var el = values[i];
        if (modeMap[el] == null)
            modeMap[el] = 1;
        else
            modeMap[el]++;
        if (modeMap[el] > maxCount)
        {
            modes = [el];
            maxCount = modeMap[el];
        }
        else if (modeMap[el] == maxCount)
        {
            modes.push(el);
            maxCount = modeMap[el];
        }
    }
    return modes;
}
// end of changes made by Hamza on 12 Sep, 2017

function updateScrollbar(){
	$(".nano").nanoScroller({  alwaysVisible: true, sliderMinHeight: 40 });
//	$('.reportTable .nano').height($('#footerContainer').position().top - $('.reportTable .nano').position().top);
}
//kriya2.0
//jagan
//side nav
var openNavi=true;
function openNav() {
	if(openNavi){
	//document.getElementById("mysidenavrocket").style.width = "0";
    //document.getElementById("mainrocket").style.marginRight = "0";
    document.getElementById("mySidenav").style.width = "25%";
    document.getElementById("main").style.marginLeft = "25%";
    document.getElementById("arrowRight").innerHTML = "keyboard_arrow_left";
	//document.getElementById("wholealert").style.width="32%";
	openNavi=false;

}
else{
	 document.getElementById("mySidenav").style.width = "0";
	 document.getElementById("main").style.marginLeft = "0";
	 document.getElementById("arrowRight").innerHTML = "keyboard_arrow_right";
	 //document.getElementById("wholealert").style.width="23%";
    openNavi=true;
}}
//rocket
 var openNaviroc=true;
function openNavrocket() {
    if(openNaviroc){
		document.getElementById("mySidenav").style.width = "0";
		document.getElementById("main").style.marginLeft = "0";
		document.getElementById("mysidenavrocket").style.width = "25%";
		document.getElementById("mainrocket").style.marginRight = "25%";
		document.getElementById("arrowRightroc").innerHTML = "keyboard_arrow_right";
		document.getElementById("wholealert").style.width="32%";


		openNaviroc=false;

	}
	else{
		 document.getElementById("mysidenavrocket").style.width = "0";
		 document.getElementById("mainrocket").style.marginRight = "0";
		 document.getElementById("arrowRightroc").innerHTML = "send";
		 document.getElementById("wholealert").style.width="23%";
		openNaviroc=true;
	}
}
function myFunction(mySelect,parentVal) {
    var x =mySelect.value;
	var thisIs=mySelect.id;
	var mytext = $("#"+thisIs+" option:selected").text();

    if(x=="No"){
		document.getElementById("triangle-topleft"+parentVal).style.color = "#0089ec";
		$("#mySelectUploadBox"+parentVal).attr("disabled", "disabled");
		$("#mySelectUploadBox"+parentVal+" i").css("color", "#ccc");
		$("#mySelectUploadBox"+parentVal).css("cursor", "default");
		$("#mySelectUploadBox"+parentVal).attr("data-tooltip", mytext);
	}
    else if (x=="Has"){
		document.getElementById("triangle-topleft"+parentVal).style.color = "#4caf50";
		$("#mySelectUploadBox"+parentVal).removeAttr('disabled');
		$("#mySelectUploadBox"+parentVal+" i").css("color", "#333");
		$("#mySelectUploadBox"+parentVal).css("cursor", "pointer");
		$("#mySelectUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
    else{
		document.getElementById("triangle-topleft"+parentVal).style.color = "#ecda00";
		$("#mySelectUploadBox"+parentVal).removeAttr('disabled');
		$("#mySelectUploadBox"+parentVal+" i").css("color", "#333");
		$("#mySelectUploadBox"+parentVal).css("cursor", "pointer");
		$("#mySelectUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
	$('[data-tooltip]').tooltip({delay: 30, effect: 'toggle'});
};
function myFunctionlet(mySelectlet,parentVal) {
	
    var y = mySelectlet.value;
	var thisIs=mySelectlet.id;
	var mytext = $("#"+thisIs+" option:selected").text();
    if(y=="No") 
	{
    document.getElementById("triangle-topleft1"+parentVal).style.color = "#0089ec";
	$("#mySelectletUploadBox"+parentVal).attr("disabled", "disabled");
	$("#mySelectletUploadBox"+parentVal+" i").css("color", "#ccc");
	$("#mySelectletUploadBox"+parentVal).css("cursor", "default");
	$("#mySelectletUploadBox"+parentVal).attr("data-tooltip", mytext);
	}
    else if (y=="Has") {
    document.getElementById("triangle-topleft1"+parentVal).style.color = "#4caf50";
	$("#mySelectletUploadBox"+parentVal).removeAttr('disabled');
	$("#mySelectletUploadBox"+parentVal+" i").css("color", "#333");
	$("#mySelectletUploadBox"+parentVal).css("cursor", "pointer");
	$("#mySelectletUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
    else {
      document.getElementById("triangle-topleft1"+parentVal).style.color = "#ecda00";
	 $("#mySelectletUploadBox"+parentVal).removeAttr('disabled');
	 $("#mySelectletUploadBox"+parentVal+" i").css("color", "#333");
	 $("#mySelectletUploadBox"+parentVal).css("cursor", "pointer");
	 $("#mySelectletUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
	$('[data-tooltip]').tooltip({delay: 30, effect: 'toggle'});
};
function myFunctionstr(mySelectstr,parentVal) {
	
    var z = mySelectstr.value;
	var thisIs=mySelectstr.id;
	var mytext = $("#"+thisIs+" option:selected").text();
    if(z=="No")
	{
    document.getElementById("triangle-topleft2"+parentVal).style.color = "#0089ec";
	$("#mySelectstrUploadBox"+parentVal).attr("disabled", "disabled");
	$("#mySelectstrUploadBox"+parentVal+" i").css("color", "#ccc");
	$("#mySelectstrUploadBox"+parentVal).css("cursor", "default");
	$("#mySelectstrUploadBox"+parentVal).attr("data-tooltip", mytext);
	}
    else if (z=="Has") {
    document.getElementById("triangle-topleft2"+parentVal).style.color = "#4caf50";
	$("#mySelectstrUploadBox"+parentVal).removeAttr('disabled');
	$("#mySelectstrUploadBox"+parentVal+" i").css("color", "#333");
	$("#mySelectstrUploadBox"+parentVal).css("cursor", "pointer");
	$("#mySelectstrUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
    else {
      document.getElementById("triangle-topleft2"+parentVal).style.color = "#ecda00";
	$("#mySelectstrUploadBox"+parentVal).removeAttr('disabled');
	$("#mySelectstrUploadBox"+parentVal+" i").css("color", "#333");
	$("#mySelectstrUploadBox"+parentVal).css("cursor", "pointer");
	$("#mySelectstrUploadBox"+parentVal).attr("data-tooltip", mytext);
    }
	$('[data-tooltip]').tooltip({delay: 30, effect: 'toggle'});
};
function typ(){
$(".articleType").each(function(i){
	var array=[];
var art=($(this).text())
array.push(art);
$("#articleTypes,form").append('<p><input type="checkbox" class="filled-in" id="'+i+'">'+'<label for="'+i+'" class="fontcolor">'+art+'</label></p><hr>');
	});
}
function owner(){
$(".Stageowner").each(function(i){
var arrayown=[];
var own=($(this).text())
arrayown.push(own);
//array
$("#typeget").append('<p><input type="checkbox" class="filled-in" id="'+da+'">'+'<label for="'+da+'" class="fontcolor">'+own+'</label></p><hr>');
	});
}
function stagenam(){
$(".row topRow,#stageName").each(function(j){
var arraystagenam=[];
var stag=($(this).text())
arraystagenam.push(stag);
$("#stName").append('<p><input type="checkbox" class="filled-in" id="'+j+'">'+'<label for="'+j+'" class="fontcolor">'+stag+'</label></p><hr>');
	});
}
/*
//pause
var holdbtn=true;
function pause(id){
  if(holdbtn)
  {
    document.getElementById("pausebtn"+id).style.backgroundColor="4492e1";
    document.getElementById("pausecol"+id).style.color="#fff";
  holdbtn=false;
}
else{
     document.getElementById("pausebtn"+id).style.backgroundColor="#fff";
     document.getElementById("pausecol"+id).style.color="#919191";
  holdbtn=true;
}
}
//flag
var flagbtn=true;
function flag(id){
  if(flagbtn)
  {
  document.getElementById("flagbtn"+id).style.backgroundColor="ea3684";
  document.getElementById("flagcol"+id).style.color="#fff";
  flagbtn=false;
}
else{
     document.getElementById("flagbtn"+id).style.backgroundColor="#fff";
     document.getElementById("flagcol"+id).style.color="#919191";
  flagbtn=true;
}
}*/
// get unique type in card
function typetest(){
var r = [];
$('.articleType').each(function(){
r.push($(this).html());
})
var unique = r.filter(function(itm, i, a) {
return i == r.indexOf(itm);
});
	var uniquelin=unique.length;
	for(i=0;i<uniquelin;i++)
	{
	$(".typeownertest").append('<p onclick="openrelatedtype()">'+unique[i]+'</p><br>');
	}
function openrelatedtype(){
	var data=unique[i];

}
    //console.log(unique);
	}
// get unique ownername in card
function teststageowner(){
var a = [];
$('.Stageowner').each(function(){
//console.log($(this).html())
a.push($(this).html());
})
var uniqueo = a.filter(function(itm, i, a) {
return i == a.indexOf(itm);
});
//console.log(uniqueo);
	var uniqueolin=uniqueo.length;
	for(j=0;j<uniqueolin;j++)
	{
  $(".typetest").append('<p>'+uniqueo[j]+'</p><br>');
}
	//console.log(uniqueo);
}
//doi
$(document).ready(function () {
	/******************multiple filter start*******************/
	$('body').on('change', '.owner_main, .stage_main, .type_main, .stage_sub, .owner_sub', function(e) { // code
	    e.preventDefault();
		var parentID = $(this).attr('parent-id');
		var childID = $(this).attr('childOf');
		var artDataType = $(this)[0].getAttributeNode('data-type').value;
		var artDataFilt = $(this)[0].getAttributeNode('data-filter').value;
		var artList = $(this)[0].getAttributeNode('data-articlelist').value;
		var removedata = "";
		if(parentID!=undefined){
			if (!$(this).find('.article_filters').is(':checked')){
				$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').hide();
				$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').find("input").prop("checked",false);		
				var childLen = $('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').length; 
				for(var i=0;i<childLen;i++){
					artDataFilt = artDataFilt+","+$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']')[i].getAttributeNode('data-filter').value;
					artList = artList+","+$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']')[i].getAttributeNode('data-articlelist').value;
				}
				removedata = "remove";
			}else{
				$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').show();
				$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').find("input").prop("checked",true);
				var childLen = $('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']').length;
				for(var i=0;i<childLen;i++){
					artDataFilt = artDataFilt+","+$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']')[i].getAttributeNode('data-filter').value;
					artList = artList+","+$('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + parentID + ']')[i].getAttributeNode('data-articlelist').value;
				}
			}
		}else if(childID!=undefined){
			if (!$(this).find('.article_filters').is(':checked')){
				if($('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']').find("input:checked").length>0){
					$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']').find("input").prop("checked",false);	
					artDataFilt = artDataFilt+","+$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']')[0].getAttributeNode('data-filter').value;
					artList = artList+","+$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']')[0].getAttributeNode('data-articlelist').value;			
				}
				removedata = "remove";
			}else{				
				if($('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + childID + ']').length == $('[class="'+artDataType.toLowerCase()+'_sub"][childOf=' + childID + ']').find('.article_filters:checked').length){
					$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']').find("input").prop("checked",true);
					artDataFilt = artDataFilt+","+$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']')[0].getAttributeNode('data-filter').value;
					artList = artList+","+$('[class="'+artDataType.toLowerCase()+'_main"][parent-id=' + childID + ']')[0].getAttributeNode('data-articlelist').value;
				}
			}
		}else{
			if (!$(this).find('.article_filters').is(':checked')){				
				removedata = "remove";
			}
		}
		displayCards(artDataFilt,artDataType,artList,removedata);
	});
	/******************multiple filter end*******************/
	var docName = window.document.title;
	//if this is the dashboard, open the nav
	//if(docName.includes("Dashboard")){
		openNav();
	//}
	if($('body[data-customer]').length>0){
		setTimeout(function(){
		customerName = $('body').attr('data-customer');
		$('#customerselect').val(customerName).change();
		},1000);
	}
	
	$('body').on('click', '.filemanager .folder', function(e) { // code
		if ($(e.target).closest('.file').length > 0){
			var url = $(e.target).closest('.file').find('a[href]').attr('href');
			var win = window.open(url, '_blank');
			win.focus();
			e.preventDefault();
			e.stopPropagation();
		}else{
			$(this).parent().find('> li').addClass('non-active');
			$(this).removeClass('non-active').addClass('active');
			$('.filemanager').find('.nav-wrapper').append('<a data-id="' + $(this).attr('id') + '" href="javascript:;" class="breadcrumb">' + $	(this).find('> .name').text() + '</a>');
			e.preventDefault();
			e.stopPropagation();
		}
	});
	$('body').on('click', '.filemanager .nav-wrapper .breadcrumb', function() { // code
		$(this).nextAll().remove();
		if ($(this)[0].hasAttribute('data-id')){
			var dataID = $(this).attr('data-id');
			$('.filemanager li[id="' + dataID + '"]').find('li').removeClass('non-active').removeClass('active');
		}else{
			$('.filemanager li').removeClass('non-active').removeClass('active');
		}
	});
	$('body').on('change', '#upload_model input', function(e) { // code
		if ($(this)[0].files.length > 0){
			$('#upload_model .upload-area .file-name').remove();
			$('#upload_model .upload-area').append('<div class="file-name">' + $(this)[0].files[0].name + '</div>');
			$('#upload_model .upload_button').removeClass('disabled')
		}
	});
	$('body').on('click', function(e) { // code
		if ($(e.target).closest('.datepicker-inline').length > 0 || /datepicker/i.test($(e.target).attr('class')) || /datepicker/i.test($(e.target).parent().attr('class'))){
			//do nothing
		}else if ($('.datepicker-inline:visible').length > 0){
			if ($('.datepicker-inline:visible').parent()[0].hasAttribute('data-on-close')){
				var onClose = $('.datepicker-inline:visible').parent().attr('data-on-close');
				if(typeof(window[onClose]) == "function"){
					window[onClose]($('.datepicker-inline:visible').parent());
				}else{
					$('.datepicker-inline:visible').parent().hide();
				}
			}else{
				$('.datepicker-inline:visible').parent().hide();
			}
		}
	});
	$('body').on('click', '.jrnlBibRef', function(e) { // code
		kriya.evt = e;
		if (typeof(e.pageX) != "undefined"){
			kriya.pageXaxis = e.pageX;
			kriya.pageYaxis = e.pageY;
		}
		kriya['styles'].init($(this), 'jrnlBibRef');
	});
	$('body').on('click', '.jrnlFigRef', function(e) { // code
		kriya.evt = e;
		if (typeof(e.pageX) != "undefined"){
			kriya.pageXaxis = e.pageX;
			kriya.pageYaxis = e.pageY;
		}
		kriya['styles'].init($(this), 'jrnlFigRef');
	});
	$('body').on('click', '.jrnlTblRef', function(e) { // code
		kriya.evt = e;
		if (typeof(e.pageX) != "undefined"){
			kriya.pageXaxis = e.pageX;
			kriya.pageYaxis = e.pageY;
		}
		kriya['styles'].init($(this), 'jrnlTblRef');
	});
	$('body').on('click', '.com-close', function(e) { // code
		$(this).closest('[data-type="popUp"]').addClass('hidden');
	});
	$('body').on('click', '.right-nav-row span[data-name]', function(e) { // code
		$('.right-nav-row span[data-name].active').removeClass('active');
		$(this).addClass('active');
		var dataID = $(this).attr('data-id');
		$('#navContainer .navDivContent > div').removeClass('active')
		$('#navContainer .navDivContent > div#' + dataID).addClass('active')
	});
});


function getBucketList(param, doi){
	$.ajax({
		type: "GET",
		url: "/api/listbucket/?"+param,
		success: function (data) {
			$('.filemanager').find('.data').remove();
			$('.filemanager').find('.nav-wrapper').html('<a href="javascript:;" class="breadcrumb">'+doi+'</a>');
			$('.filemanager').append(data);
		   // $(".folder").append('<i class="material-icons align-arrow-icon">keyboard_arrow_right</i>');
			$('#fileBrowser').openModal();
		}
	});

}

// MODAL HANDLER

function modalHandler(modal, id, type){
    if(type == 'CLOSE-HOLD'){
        $('#pausebtn'+id).parent().parent()
            .find('div button')
            .each(function (i, obj) {
                $(obj).removeAttr("disabled")
            });
        document.getElementById("pausebtn"+id).style.backgroundColor="#fff";
        document.getElementById("pausecol"+id).style.color="#919191";
		document.getElementById("modalHold").style.display = "none";
		$(modal).find('textarea').val("");
		$(modal+' select').prop('selectedIndex', 0); //Sets the first option as selected
		$(modal+' select').material_select();
        $(modal).closeModal();
        //$(modal).remove();
    }
    if(type == "SAVE-HOLD"){
		let hold = {};
			hold.type = $(modal)
				.find('li.active.selected')
				.find('span').text();
			hold.comment = $(modal).find('textarea').val();
		if(hold.type=="" || hold.type==null || hold.type==undefined){
			$(modal).find('#holdError').html("Please select an option.");
			return;
		}else{
			$(modal).find('textarea').val("");
			$(modal+' select').prop('selectedIndex', 0); //Sets the first option as selected
			$(modal+' select').material_select();
			$(modal).find('#holdError').html("");	
			var holdIcontooltip="";
			if(hold.comment!=null && hold.comment!=""){
				holdIcontooltip = 'data-tooltip="'+hold.comment+'"';
			}
			$("#flagbtn"+id).attr("disabled","disabled");
			document.getElementById("flagbtn"+id).style.backgroundColor="#DFDFDF";
			document.getElementById("flagcol"+id).style.color="#858585";
			var eleId = $(modal).find(".saveHold").closest('#holdOption').attr('nodeid');
			var customerName = $(modal).find(".saveHold").closest('#holdOption').attr('customerName');
			var projectName = $(modal).find(".saveHold").closest('#holdOption').attr('projectName');
			var doi = $(modal).find(".saveHold").closest('#holdOption').attr('doi');
			var currStageName = $(modal).find(".saveHold").closest('#holdOption').attr('currStageName');
			var d = new Date();
			var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
			var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);			
			$("#stage"+id+" td:contains('in-progress')").html("held");
			$('#stage'+id+' tr:last-child td')[1].textContent=$('.username').html();

			var days = workingDaysBetweenDates(new Date($("#stage"+id+" #dateStart"+id)[0].getAttributeNode("start-date").value),new Date(currDate));
			if(!$.isNumeric(days) || days<0 )
				days =0;
			$('#stage'+id+' tr:last-child td')[2].textContent = dateFormat($("#stage"+id+" #dateStart"+id)[0].getAttributeNode("start-date").value, "mediumDate");
			$('#stage'+id+' tr:last-child td')[3].textContent = dateFormat(currDate, "mediumDate");
			$('#stage'+id+' tr:last-child td')[5].textContent = days;
			var addversion = $('#stage'+id+' tr:last-child td')[6].textContent;
			addversion = 'v'+(parseInt(($.isNumeric(addversion.replace(/\D/g,'')))?addversion.replace(/\D/g,''):0)+1);
			
			$("#stage"+id+" tbody").append('<tr><td>Hold</td><td id="editingName">'+'<div class="input-field col s6" id="AuthorEdit"><input id="edit_name" type="text" class="validate editing"></div>'+'</td><td>'+'<a class="kriyaDatePic"><span id="pickDateStart'+id+'"></span><span class="Pickdate" onclick="pickDateNew('+id+', \'Start\')" start-date="'+currDate+'" id="dateStart'+id+'">pick date</span></a>'+ '</td><td>'+'<a class="kriyaDatePic"><span id="pickDateEnd'+id+'"></span><span class="Pickdate" onclick="pickDateNew('+id+', \'End\')" end-date="'+currDate+'" id="dateEnd'+id+'">pick date</span></a>'+'</td><td style="cursor:default" '+holdIcontooltip+'>in-progress</td><td>'+0+'</td><td>'+addversion+'</tr>');
			
			data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><status>held</status><end-date>'+currDate+'</end-date></stage>';
			jQuery.ajax({
				type: "POST",
				url: "/api/updatedatausingxpath",
				data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : data},
				success: function (response) {
					jobLog = '<log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>hold-on</status></log>';
					//console.log(response);
					jQuery.ajax({
						type: "POST",
						url: "/api/addhold",
						data: {"customer" : customerName, "journalID" : projectName, "articleID" : doi, "stagename" : "Hold", "heldStage" : currStageName, "holdState": hold.type, "holdComment": hold.comment, "joblog":jobLog },
						success: function (data) {
							if (data){
								$('#'+eleId).closest('.jobCards').addClass('hold'); //stageName
								//$('#'+eleId).closest('.jobCards').find('.stageName').html('Hold'); //stageName
							//	$('#'+eleId).closest('.jobCards').attr('style','border-left-color:#504e4e !important');
								$('#'+eleId).addClass('hold');
								//$('#'+eleId).attr('currStageName','Hold');
								$('#'+eleId).attr('prevStage',currStageName);
								var artTitleNode = $('#'+eleId).closest('.jobCards').find('.articleTitle a');
								artTitleNode.attr('data-href', artTitleNode.attr('href'));
								artTitleNode.removeAttr('href');
								//$('#holdOption .com-close').click()
								return true;
							}
							else{
							  return "Field not found";
							}
						},
						error: function(xhr, errorType, exception) {
						  return null;
						}
					});
				},
				error: function(xhr, errorType, exception) {
				  return null;
				}
			});
			
			//console.log('Hold Added on 3125 DashboardJS'+JSON.stringify(hold))
			
			let holdCard = '<div '+holdIcontooltip+' class="onHold"><span>ON HOLD</span><i class="material-icons">lock_outline</i></div>'

			$('#pausebtn'+id).parent().parent().parent().parent()
				.addClass('holdCard')
				.append(holdCard)

			$('#pausebtn'+id).attr('holded-card', true)
			document.getElementById("modalHold").style.display = "none";
			$(modal).closeModal();
			}       
       // $(modal).remove();
    }
    if(type == 'CLOSE-UNHOLD'){
		document.getElementById("modalUnhold").style.display = "none";
		$(modal).find('textarea').val("");
        $(modal).closeModal();
        //$(modal).remove();
    }
    if(type == 'SAVE-UNHOLD'){
        let unhold = {};
        unhold.comment = $(modal).find('textarea').val();
		$(modal).find('textarea').val("");
       // console.log('UNHold Added on 3125 DashboardJS'+JSON.stringify(unhold))

        $('#pausebtn'+id).parent().parent().parent().parent()
            .removeClass('holdCard')
            .find('.onHold')
            .remove();

        $('#pausebtn'+id).parent().parent()
            .find('div')
            .each(function (i, obj) {
                $(obj).find('button').removeAttr("disabled")
            });
        document.getElementById("pausebtn"+id).style.backgroundColor="#fff";
        document.getElementById("pausecol"+id).style.color="#919191";
        $('#pausebtn'+id).removeAttr('holded-card')
		document.getElementById("modalUnhold").style.display = "none";
        $(modal).closeModal();
        //$(modal).remove();
		document.getElementById("flagbtn"+id).style.backgroundColor="#fff";
        document.getElementById("flagcol"+id).style.color="#858585";
		$("#flagbtn"+id).removeAttr("disabled");
        $('#pausebtn'+id).attr('holded-card', false)
		var currStageName = $('#pausebtn'+id).attr('currStageName');
		var customerName = $('#pausebtn'+id).attr('customerName');
		var projectName = $('#pausebtn'+id).attr('projectName');
		var doi = $('#pausebtn'+id).attr('doi');
		var prevStage = $('#pausebtn'+id).attr('prevStage');
		var elementId = $('#pausebtn'+id).attr('id');
		$("#stage"+id+" td:contains('held')").html("released");
		$("#stage"+id+" td:contains('in-progress')").html("completed");
		$('#stage'+id+' tr:last-child td')[1].textContent=$('.username').html();
		// need to change the date
		var d = new Date();
		var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
		var days = workingDaysBetweenDates(new Date($("#stage"+id+" #dateStart"+id)[0].getAttributeNode("start-date").value),new Date(currDate));
		if(!$.isNumeric(days) || days<0 )
			days =0;
		$('#stage'+id+' tr:last-child td')[2].textContent = dateFormat($("#stage"+id+" #dateStart"+id)[0].getAttributeNode("start-date").value, "mediumDate");
		$('#stage'+id+' tr:last-child td')[3].textContent = dateFormat(currDate, "mediumDate");
		$('#stage'+id+' tr:last-child td')[5].textContent = days;
		
		var addversion = $('#stage'+id+' tr:last-child td')[6].textContent;
		addversion = 'v'+(parseInt(($.isNumeric(addversion.replace(/\D/g,'')))?addversion.replace(/\D/g,''):0)+1);
		$("#stage"+id+" tbody").append('<tr><td>'+prevStage+'</td><td id="editingName">'+'<div class="input-field col s6" id="AuthorEdit"><input id="edit_name" type="text" class="validate editing"></div>'+'</td><td>'+'<a class="kriyaDatePic"><span id="pickDateStart'+id+'"></span><span class="Pickdate" onclick="pickDateNew('+id+', \'Start\')" start-date="'+currDate+'" id="dateStart'+id+'">pick date</span></a>'+ '</td><td>'+'<a class="kriyaDatePic"><span id="pickDateEnd'+id+'"></span><span class="Pickdate" onclick="pickDateNew('+id+', \'End\')" end-date="'+currDate+'" id="dateEnd'+id+'">pick date</span></a>'+'</td><td style="cursor:default" >in-progress</td><td>'+0+'</td><td>'+addversion+'</tr>');
		data = '<stage><name>' + prevStage + '</name><currentstatus>held</currentstatus><status>released</status></stage>';
		jQuery.ajax({
			type: "POST",
			url: "/api/updatedatausingxpath",
			data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : data},
			success: function (response) {			
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				jobLog = '<log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>hold-off</status></log>';
				data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><status>completed</status><end-date>'+currDate+'</end-date><job-logs>'+jobLog+'</job-logs></stage>';
				jQuery.ajax({
					type: "POST",
					url: "/api/updatedatausingxpath",
					data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : data},
					success: function (response) {
						jQuery.ajax({
							type: "POST",
							url: "/api/addhold",
							data: {"customer" : customerName, "journalID" : projectName, "articleID" : doi, "stagename" : prevStage},
							success: function (data) {
								if (data){
									$('#pausebtn'+id).closest('.jobCards').removeClass('hold');
									$('#pausebtn'+id).closest('.jobCards').find('.stageName.Main').html(prevStage);
									$('#pausebtn'+id).closest('.jobCards').find('.stageName.Main').next('.stageName').html("<span style='color: inherit !important;'></span>");
									//$(currentNode).closest('.jobCards').attr('style','border-left-color:#71ba51 !important');
									$('#pausebtn'+id).removeClass('hold');
									$('#pausebtn'+id).attr('currStageName', prevStage);
									$('#pausebtn'+id).attr('prevStage','');
									return true;
								}
								else{
								  return "Field not found";
								}
							},
							error: function(xhr, errorType, exception) {
							  return null;
							}
						});
					},
					error: function(xhr, errorType, exception) {
					  return null;
					}
				});
			},
			error: function(xhr, errorType, exception) {
			  return null;
			}
		});
    }
    if(type == 'CLOSE-URGENT'){
		$(modal+' select').prop('selectedIndex', 0); //Sets the first option as selected
		$(modal+' select').material_select();
		$(modal).find('textarea').val("");
		$(modal).find('#urgentError').html("");	
        $(modal).closeModal();
       // $(modal).remove();
    }
    if(type == 'SAVE-URGENT'){
        let urgent = {};
        urgent.type = $(modal).find('li.active.selected').find('span').text();
        urgent.comment = $(modal).find('textarea').val();
		
       // console.log('URGENT Added on 3125 DashboardJS'+JSON.stringify(urgent))
	
		$(modal).find('#urgentError').html("");	
			
		if(urgent.type=="" || urgent.type==null || urgent.type==undefined){
			$(modal).find('#urgentError').html("Please select an option.");
			return;
		}else{
			//var filename = "updatedatausingxpath";
			$(modal).find('textarea').val("");
			$(modal+' select').prop('selectedIndex', 0); //Sets the first option as selected
			$(modal+' select').material_select();
			
			var urgentIcontooltip="";
			if(urgent.comment!=null && urgent.comment!=""){
				urgentIcontooltip = 'data-tooltip="'+urgent.comment+'"';
			}
			
			var currStageName = $('#pausebtn'+id).attr('currStageName');
			var customerName = $('#pausebtn'+id).attr('customerName');
			var projectName = $('#pausebtn'+id).attr('projectName');
			var doi = $('#pausebtn'+id).attr('doi');
			var prevStage = $('#pausebtn'+id).attr('prevStage');
			var d = new Date();
			var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
			var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
			var commentsAdded = "";
			if(urgent.comment!="" && urgent.comment!=null && urgent.comment!=undefined){
				commentsAdded = "<comments>"+urgent.comment+"</comments>";
			}
			var currTrackChange = "";
			if($('#flagbtn'+id).closest('.jobCards').hasClass("fastTrackAll")){
				currTrackChange = "article";
			}else if($('#flagbtn'+id).closest('.jobCards').hasClass("fastTrackCurrent")){
				currTrackChange = "stage";
			}
			
			if(urgent.type == 'Fast Track Current Stage' && currTrackChange=="stage"){
				$(modal).find('#urgentError').html("Already priority has set.");
				return;
			}else if(urgent.type == 'Fast Track All Stages' && currTrackChange=="article"){
				$(modal).find('#urgentError').html("Already priority has set.");
				return;
			}else if(urgent.type == 'No Fast Track' && currTrackChange==""){
				$(modal).find('#urgentError').html("No priority has set till now.");
				return;
			}
			var data = addData = "";
			if(urgent.type == 'Fast Track Current Stage'){
				document.getElementById("flagbtn"+id).style.backgroundColor="ea3684";
				document.getElementById("flagcol"+id).style.color="#fff";
				let urgentCard ='<span style="cursor:default" '+urgentIcontooltip+' class="onUrgentCurrent"><i class="material-icons">directions_run</i></span>'

				$('#flagbtn'+id).parent().parent().parent().parent()
					.removeClass('fastTrackAll')
					.addClass('fastTrackCurrent')
					//.append(urgentCard)
					.find('.onUrgentAll').remove()
					
					// added for positioning Fasttrack icon
				$('#addFastrackIcon'+id)
					.append(urgentCard)
					
				$('#flagbtn'+id).attr('urgent-card', 'track-current');
				if(currTrackChange=="article"){
					jQuery.ajax({
						type: "POST",
						url: "/api/updatedatausingxpath",
						data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : '<priority>off</priority>',"updateOn":"workflow"},			
						success: function (response) {
							data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>article-priority-off</status>'+commentsAdded+'</log></job-logs></stage>';
							//if(updatePriorities(customerName,projectName,doi,data)){
								addData = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><priority>on</priority><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>stage-priority-on</status>'+commentsAdded+'</log></job-logs></stage>';
								updatePriorities(customerName,projectName,doi,data,addData)
							//}						
						},
						error: function(xhr, errorType, exception) {
						  return null;
						}
					});					
				}else{
					data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><priority>on</priority><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>stage-priority-on</status>'+commentsAdded+'</log></job-logs></stage>';
					updatePriorities(customerName,projectName,doi,data)
				}				
			}
			if(urgent.type == 'Fast Track All Stages'){
				document.getElementById("flagbtn"+id).style.backgroundColor="FF9000";
				document.getElementById("flagcol"+id).style.color="#fff";
				let urgentCard = '<span style="cursor:default" '+urgentIcontooltip+' class="onUrgentAll"><i class="material-icons">directions_walk</i></span>'

				$('#flagbtn'+id).parent().parent().parent().parent()
					.removeClass('fastTrackCurrent')
					.addClass('fastTrackAll')
					//.append(urgentCard)
					.find('.onUrgentCurrent').remove()
					
					// added for positioning Fasttrack icon
				$('#addFastrackIcon'+id)
					.append(urgentCard)
					

				$('#flagbtn'+id).attr('urgent-card', 'track-all');
				jQuery.ajax({
					type: "POST",
					url: "/api/updatedatausingxpath",
					data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : '<priority>on</priority>',"updateOn":"workflow"},			
					success: function (response) {
						if(currTrackChange=="stage"){						
							data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>article-priority-on</status>'+commentsAdded+'</log></job-logs></stage>';	
							//if(updatePriorities(customerName,projectName,doi,data)){
								addData = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><priority>off</priority><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>stage-priority-off</status>'+commentsAdded+'</log></job-logs></stage>';	
								updatePriorities(customerName,projectName,doi,data,addData)
							//}
						}else{
							//data = '<workflow><priority>on</priority><stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><job-logs><log><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>article-priority-on</status>'+commentsAdded+'</log></job-logs></stage></workflow>';
							data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>article-priority-on</status>'+commentsAdded+'</log></job-logs></stage>';	
							updatePriorities(customerName,projectName,doi,data)
						}					
					},
					error: function(xhr, errorType, exception) {
					  return null;
					}
				});					
			}
			if(urgent.type == 'No Fast Track'){
				document.getElementById("flagbtn"+id).style.backgroundColor="#fff";
				document.getElementById("flagcol"+id).style.color="#919191";
				$('#flagbtn'+id).parent().parent().parent().parent()
					.removeClass('fastTrackAll')
					.removeClass('fastTrackCurrent')
					.find('.onUrgentCurrent,.onUrgentAll').remove()

				$('#flagbtn'+id).attr('urgent-card', 'none');
				if(currTrackChange=="stage"){
					data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><priority>off</priority><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>stage-priority-off</status>'+commentsAdded+'</log></job-logs></stage>';
					updatePriorities(customerName,projectName,doi,data)
				}else if(currTrackChange=="article"){
					
					jQuery.ajax({
						type: "POST",
						url: "/api/updatedatausingxpath",
						data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : '<priority>off</priority>',"updateOn":"workflow"},			
						success: function (response) {
							data = '<stage><name>' + currStageName + '</name><currentstatus>in-progress</currentstatus><job-logs><log data-insert="true"><username>' + $('.username').html() + '</username><useremail>' + $('.useremail').html() + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status>article-priority-off</status>'+commentsAdded+'</log></job-logs></stage>';
							updatePriorities(customerName,projectName,doi,data);
						},
						error: function(xhr, errorType, exception) {
						  return null;
						}
					});				
				}
			}	
			
			if(urgent.type == 'No Fast Track'){
				document.getElementById("pausebtn"+id).style.backgroundColor="#fff";
				document.getElementById("pausecol"+id).style.color="#858585";
				$("#pausebtn"+id).removeAttr("disabled");
			}else{
				document.getElementById("pausebtn"+id).style.backgroundColor="#DFDFDF";
				document.getElementById("pausecol"+id).style.color="#858585";
				$("#pausebtn"+id).attr("disabled","disabled");
			}				
			$(modal).closeModal();
			document.getElementById("modalUrgent").style.display = "none";
			//$(modal).remove();
		}
    }
    if(type == 'CLOSE-AUTHORS'){
        $(modal).closeModal();
        $(modal).remove();
    }
	$('.lean-overlay').remove();
	$('[data-tooltip]').tooltip({delay: 30, effect: 'toggle'});
}

function updatePriorities(customerName,projectName,doi,data,addData){
	jQuery.ajax({
		type: "POST",
		url: "/api/updatedatausingxpath",
		data: {"apiKey" : '36ab61a9-47e1-4db6-96db-8b95a9923599', "customerName" : customerName, "projectName" : projectName, "doi" : doi, "data" : data},			
		success: function (response) {
			//console.log("success");
			if(addData!=null && addData!='' && addData!=undefined){
				updatePriorities(customerName,projectName,doi,addData,'')
			}else{
				return true;
			}			
		},
		error: function(xhr, errorType, exception) {
			//console.log("fail");
			return null;
		}
	});
}
// PAUSE BUTTON


// Dashboard upload model function #340
/*****
**	Modal function to upload digest, decision letter, striking image - Jai
*****/
function openFileUpload(e){
	$('#upload_model').remove();
	var doi = $(e).closest('*[data-doiid]').attr('data-doiid');
	var uploadType = $(e).attr('data-upload-type');
	if (typeof(doi) == "undefined") return false;
	$("body").append('<div id="upload_model" data-doi="' + doi + '" data-upload-type="' + uploadType + '" data-success="' + $(e).attr('data-success') + '" class="modal comments-modal-upload"><div class="modal-wrapper modal-hold"><div class="modal-container"><div class="modal-header"><h3 class="holdHeader">Upload ' + uploadType + ' for ' + doi + '<span style="margin-left: 20px;font-size: 12px;background: #fafafa;padding: 3px 10px;color: #313131;border-radius: 6px;display:none;" class="notify"></span></h3><button type="button" class="modal-action modal-close close" id="upload_close"><span>×</span></button></div><div class="modal-content"><div class="upload-area"><label class="upload_label"><strong>Select File:</strong></label><div class="file-field input-field" style="float:left;margin-right:20px;"><div class="btn">  <span><i class="fa fa-upload"></i>&nbsp;Select file</span><input class="file-holder" type="file"></div></div></div><div class="modal-footer"><span onclick="uploadFile()" class="btn btn-medium upload_button disabled">Upload</span></div></div></div></div>');
 
    $('#upload_model').openModal();
}
//upload file : gets upload path from server
function uploadFile(param, targetNode){
	var fileHolder = $('.upload-area .file-field input')[0];
	if (fileHolder.files.length == 0) {
		return;
	}
	$('.notify').show();
	$('.notify').html('Uploading to server');
	var param = {}
	var file = fileHolder.files[0]
	param.file = file;
	param.service = $('#upload_model').attr('data-upload-type').replace(/\s/g, '').toLocaleLowerCase();
	param.success = $('#upload_model').attr('data-success');
	//var block = param.block;
	//block.progress('Uploading to server');
	$.ajax({
		url: '/getS3UploadCredentials',
		type: 'GET',
		data: {'filename': file.name},
		success: function(response){
			if (response && response.upload_url){
				var formData = new FormData();
				$.each(response.params, function(k, v){
					formData.append(k, v);
				})
				formData.append('file', file, file.name);
				param.response = response;
				param.formData = formData;
				uploadToServer(param);
			}
		},
		error: function(err){
			if(param.error && typeof(param.error) == "function"){
				param.error(err);
			}
		}
	});
}
//sends file to default path in aws : on return sends another request to move it to client specific path
function uploadToServer(param, targetNode){
	var file = param.file;
	var block = param.block;
	var formData = param.formData;
	var response = param.response;
	$.ajax({
		url: response.upload_url,
		type: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			//Download progress
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
					$('.notify').html('Uploading to server ' + percentComplete);
				}
			}, false);
			xhr.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
					$('.notify').html('Uploading to server ' + percentComplete);
				}
			}, false);
			return xhr;
		},
		success: function(res){
			$('.notify').html('Converting for web view');
			kriya = {}; kriya.config = {}; kriya.config.content = {};
			kriya.config.content.customer = $('#customerselect').val();
			kriya.config.content.project = $('#projectselect').val();
			kriya.config.content.doi = $('#upload_model').attr('data-doi');
			if (res && res.hasChildNodes()){
				var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
				if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
					var ext = file.name.replace(/^.*\./, '')
					var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
					var params = {
							srcBucket: uploadResult.find('Bucket,bucket').text(),
							srcKey: uploadResult.find('Key,key').text(),
							dstBucket: "kriya-resources-bucket",
							dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
							convert: 'false'
						}
					/*if(param.convert){
						params.convert = param.convert;
					}*/
					$.ajax({
						async: true,
						crossDomain: true,
						url: response.apiURL,
						method: "POST",
						headers: {
							accept: "application/json"
						},
						data: JSON.stringify(params),
						success: function(status){
							if(param.success && typeof(window[param.success]) == "function"){
								window[param.success](status);
							}
							//uploadDigest(status)
						},
						error: function(err){
							if(param.error && typeof(param.error) == "function"){
								param.error(err);
							}
							console.log(err)
						}
					});
				}
			}
		}
	});
}
// send a request to server with uploaded file path, which will response with a job-id
function uploadDigest(status){
	if (typeof(status.fileDetailsObj.dstKey) == "undefined") return false;
	var param = {'customer': $('#customerselect').val(), 'project': $('#projectselect').val(), 'doi': $('#upload_model').attr('data-doi'), 'key': status.fileDetailsObj.dstKey, 'service': $('#upload_model').attr('data-upload-type').replace(/\s/g, '').toLocaleLowerCase()}
	$('.notify').html('Conversion in progress...');
	$.ajax({
		type: "POST",
		url: "/api/uploaddigest",
		data: param,
		success: function (msg) {
			getJobStatus($('#customerselect').val(), msg.message.jobid, '', 'exeter', 'typesetter', $('#upload_model').attr('data-doi'))
		},
		error: function(xhr, errorType, exception) {
			$('.la-container').fadeOut()
			  return null;
		}
	});
}
//to get status of a job from job mananger
function getJobStatus(customer, jobID, notificationID, userName, userRole, doi){
	$.ajax({
		type: 'POST',
		url: "/api/jobstatus",
		data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
		crossDomain: true,
		success: function(data){
			if(data && data.status && data.status.code && data.status.message && data.status.message.status){
				var status = data.status.message.status;
				var code = data.status.code;
				var currStep = 'Queue';
				if(data.status.message.stage.current){
				currStep = data.status.message.stage.current;
				}    
				var loglen = data.status.message.log.length;    
				var process = data.status.message.log[loglen-1];
				if (/completed/i.test(status)){
					var loglen = data.status.message.log.length;	
					var process = data.status.message.log[loglen-1];
					$('.notify').html('completed');
					$('#upload_model').closeModal();
				}else if (/failed/i.test(status) || code != '200'){
					$('notify').html(process);
				}else if (/no job found/i.test(status)){
					$('.notify').html(status);
				}else{
					$('.notify').html(data.status.message.displayStatus);
					var loglen = data.status.message.log.length;	
					if(loglen > 1){
						var process = data.status.message.log[loglen-1];
						if (process != '') $('.notify').html(process);
					}
					setTimeout(function() {
						getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
					}, 1000 );
				}
			}
			else{
				$('notify').html('Failed');
			}
		},
		error: function(error){
			if(error.status == 502){
				setTimeout(function() {
					getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
				}, 1000 );
			}
		}
	});
    }

function pause(currentNode,id){
	
	var currStageName = $(currentNode).attr('currStageName');
	var customerName = $(currentNode).attr('customerName');
	var projectName = $(currentNode).attr('projectName');
	var doi = $(currentNode).attr('doi');
	var prevStage = $(currentNode).attr('prevStage');
	var elementId = $(currentNode).attr('id');
	var holdCards = $(currentNode).attr('holded-card');
	
    if(holdCards != 'true')
    {
        document.getElementById(elementId).style.backgroundColor="4492e1";
        document.getElementById(elementId).style.color="#fff";

        var currBtn = document.getElementById(elementId);

        //$('#'+elementId).attr("disabled", "disabled");
        $("#modalHold .modal-footer").find("#holdClose").remove();
        $("#modalHold .modal-header").append('<button type="button" class="modal-close close" id="holdClose"><span aria-hidden="true">×</span></button>');
        $("#modalHold .modal-footer").append('<div class="clear"> </div>');
        $("#holdClose").attr('onclick', 'modalHandler(\'#modalHold\','+ id +',\'CLOSE-HOLD\')');  
        $("#holdSave").attr('onclick', 'modalHandler(\'#modalHold\','+ id +',\'SAVE-HOLD\')');  
        $("#holdOption").attr('currStageName',currStageName).attr('customerName',customerName).attr('projectName',projectName).attr('doi',doi).attr('nodeid',elementId); 
		// $('#holdSave').find('input:text, input:password, select, textarea').val('');
        // $('#holdSave').find('input:radio, input:checkbox').prop('checked', false);
		$("#modalHold").find('#holdError').html("");
        document.getElementById("modalHold").style.display = "block";
        $('select').material_select();

     /*   $('#modalHold').modal({
                dismissible: true,
                opacity: .5,
                inDuration: 300,
                outDuration: 200
            });*/

        $('#modalHold').openModal();
    }
    else{
        document.getElementById("pausebtn"+id).style.backgroundColor="#fff";
        document.getElementById("pausecol"+id).style.color="#919191";

       $("#modalUnHold .modal-footer").find("#unholdClose").remove();
	   $("#modalUnHold .modal-header").append('<button type="button" class="modal-close close" id="unholdClose"><span aria-hidden="true">×</span></button>');
	   $("#modalUnHold .modal-footer").append('<div class="clear"> </div>');
       $("#unholdClose").attr('onclick', 'modalHandler(\'#modalUnHold\','+ id +',\'CLOSE-UNHOLD\')');  
       $("#unholdSave").attr('onclick', 'modalHandler(\'#modalUnHold\','+ id +',\'SAVE-UNHOLD\')');
	 //  $("#unholdOption").attr('currStageName',currStageName).attr('customerName',customerName).attr('projectName',projectName).attr('doi',doi).attr('nodeid',elementId); 
		document.getElementById("modalUnhold").style.display = "block";
       // $('#main').append(modal)
        $('select').material_select();

        /*$('#modalunHold').modal({
            dismissible: true,
            opacity: .5,
            inDuration: 300,
            outDuration: 200
        });*/

        $('#modalunHold').openModal();
    }
}

// URGENT BUTTON

function flag(id){
   // if($('#flagbtn'+id).attr('urgent-card') == 'none' || !$('#flagbtn'+id).attr('urgent-card')) {       
        //$('#main').append(modal)
        $('select').material_select();
       /* $('#modalUrgent').modal({
            dismissible: true,
            opacity: .5,
            inDuration: 300,
            outDuration: 200
        });*/
		document.getElementById("modalUrgent").style.display = "block";
		$("#modalUrgent").find(".holdHeader").html("Fast Track");
		$("#modalUrgent .modal-footer").find("#closeUrgent").remove();
		$("#modalUrgent .modal-footer").append('<div class="clear"> </div>');
		$("#modalUrgent .modal-header").append('<button type="button" class="modal-close close" id="closeUrgentBtn"><span aria-hidden="true">×</span></button>');
		$("#closeUrgentBtn").attr('onclick', 'modalHandler(\'#modalUrgent\','+ id +',\'CLOSE-URGENT\')'); 
		$("#closeUrgent").attr('onclick', 'modalHandler(\'#modalUrgent\','+ id +',\'CLOSE-URGENT\')');
        $("#saveUrgent").attr('onclick', 'modalHandler(\'#modalUrgent\','+ id +',\'SAVE-URGENT\')'); 
        $('#modalUrgent').openModal();
   // }
}

// AUTHORS BUTTON

function authors(authorsNam,id,emailIds){	
		var modal =`<div class="modal-mask" id="modalAuthors" style="overflow:auto">
						<div class="modal-wrapper modal-hold">
							<div class="modal-container">
								<div class="modal-header">
									<slot name="header">
										<h3 class="holdHeader">Authors</h3>
									</slot>
									 <button type="button" class="modal-close close" onclick="modalHandler('#modalAuthors',`+ id +`,'CLOSE-AUTHORS')"><span aria-hidden="true">×</span></button> 
								</div>
								<div class="modal-body hold" id="customScroll">
								<div class="nano">
								<div class="content">
									<slot name="body">
										<table class="bordered">
											<thead>
												<tr>
													<th width="50%">Name</th>
													<th width="50%">email</th>
												</tr>
											</thead>
			
											<tbody>
												
											</tbody>
										</table>
									</slot>
									</div>
									</div>
								</div>
								<div class="modal-footer">
									<slot name="footer">
									</slot>
								</div>
							</div>
						</div>
					 </div>`;

		$('#main').append(modal);
		
		
		
		// updateScrollbar();
		
		if(authorsNam!="")
		{
			var authorsNamArr = authorsNam.split(",");
			var emailArr = emailIds.split(",");
			var nameLen = authorsNamArr.length;	
			for(var j = 0; j < nameLen; j++){
				if(authorsNamArr[j]!=undefined && authorsNamArr[j].trim().toLowerCase()!="undefined"){
					var emailRow = "";
					var emailArrNew = [];
					if(emailArr[j]!="" && emailArr[j]!=undefined && emailArr[j]!=null){
						emailArrNew = emailArr[j].split(";");
						for(var k = 0; k < emailArrNew.length; k++){	
							if(emailArrNew[k])
								emailRow += '<a class="emailBlock" href="'+emailArrNew[k]+'">'+emailArrNew[k]+'</a>';
						}
					}else{
						emailRow = '<a class="emailBlock" href="'+emailRow+'"></a>';
					}
					$('#modalAuthors').find('tbody').append('<tr><td>'+authorsNamArr[j]+'</td><td>'+emailRow+'</td></tr>');
				}
			}
		}else{
			$('#modalAuthors').find('thead').html("<tr><td><centre>Couldnt find author names</centre></td></tr>")
		}
		$('select').material_select();
		/*$('#modalAuthors').modal({
			dismissible: true,
			opacity: .5,
			inDuration: 300,
			outDuration: 200
		});*/
		
		// $("#customScroll .nano").nanoScroller();
		
		$('#customScroll .nano').nanoScroller({  alwaysVisible: true, scroll: 'top' });
		
		$('#modalAuthors').openModal();
	
}

// PICK DATE NEW

function pickDateNew(id, type){
	$('.datepicker-inline').parent().hide();
    // init Date Picker
	$('#date'+type+id).attr('data-old-value', $('#date'+type+id).html());
    var timeToSend = {
        time: '',
        date: ''
    }
    var timeToSet;

    var datePicker = $('#pickDate'+type+id).datepicker({
        timepicker: true,
        todayButton: true,
        language: {
            days: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            daysShort: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
            daysMin: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
            months: ['January','February','March','April','May','June','July','August','September','October','November','December'],
            monthsShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            today: 'Today',
            clear: 'Clear',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii',
            firstDay: 1
        },
        onShow: function(dp, animationCompleted){
            if (!animationCompleted) {

            } else {

            }
        },
        onHide: function(dp, animationCompleted){
            if (!animationCompleted) {

            } else {

            }
        },
		onSelect: function (formattedDate, date, inst) {
            timeToSend.date = formattedDate.slice(0, 10);
            timeToSend.time = formattedDate.slice(11, 16);
            timeToSet = date.toString().slice(4,15);
            // send time and date here
            $('#date'+type+id).text(timeToSet);
        }
    })
    datePicker.show();
	var inputDate = new Date($('#date'+type+id).html());
	if (inputDate == 'Invalid Date'){
		var currentDate = currentDate = new Date();
		datePicker.data('datepicker').selectDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()))
	}else{
		datePicker.data('datepicker').selectDate(inputDate)
	}
    $('.datepicker--button').text('CONFIRM');
    $('.datepicker--button').on('click',function () {
        datePicker.hide();
		if (datePicker.data('datepicker').selectedDates.length > 0){
			savePressDate(datePicker.attr('id'), 'yes');
		}else{
			savePressDate(datePicker.attr('id'), 'delete');
		}
    });
	event.preventDefault();
	event.stopPropagation();
}

function publishFile(btnNode){
    if (!btnNode) return true;
    var doiString = $(btnNode).closest('.jobCards').attr('data-doiid')
    var jrnlName = doiString.replace(/^[\s\t\r\n]*([a-z]+).*?$/gi, '$1');
	console.log('publishFile', doiString, jrnlName);
    var settings = {
	    "url": "/api/publisharticle?doi=" + doiString + '&jrnlName=' + jrnlName ,
	    "method": "GET"
    }
    $.ajax(settings).done(function (response) {
        alert(JSON.stringify(response));
    });
}

function reloadArticle(btnNode){
    if (!btnNode) return true;
    if ($(btnNode).attr('data-ftp-file-name') == undefined) return true;
    var doiString = $(btnNode).closest('.jobCards').attr('data-doiid')
	var params = {
					customer: $('#customerselect').val(),
					journalID: $('#projectselect').val(),
					articleID: doiString,
					ftpFileName: $(btnNode).attr('data-ftp-file-name'),
					articleTitle: 'articleTitle',
					articleType: 'articleType',
					subject: 'field',
					specialInstructions: 'specialInstructions',
					priority: 'priority',
					mandatoryFields: 'mandatoryFields',
					hasMandatoryData: 'hasMandatoryData',
					forceLoad: 'true',
					status: {
						code: 200
					}
				}
	$(btnNode).addClass('disabled');
    jQuery.ajax({
		type: "POST",
		url: "/workflow?currStage=parseEmail",
		data: params,
		success: function (data) {
			console.log(data)
		}
	});

}

//to confirm press date, option will be confirmed, un-confirmed or cancel - Jai
function confirmPressDate(datePicker){
	var pressDate = datePicker.closest('.pressDate');
	if (datePicker.data('datepicker').selectedDates.length > 0){
		var selectedDate = datePicker.data('datepicker').selectedDates[0].toString().slice(4,15);
		var confirmation = 'Is ' + selectedDate + ', the confirmed press date?';
		notification({title : 'Confirmed Date', type : 'warning', closeIcon : false, content : confirmation + '<br><span class="action"><span class="btn green lighten-2" onclick="savePressDate(\'' + datePicker.attr('id') + '\', \'yes\')">Yes</span><span class="btn orange lighten-2" onclick="savePressDate(\'' + datePicker.attr('id') + '\', \'no\')">No</span><span class="btn red lighten-2" onclick="savePressDate(\'' + datePicker.attr('id') + '\', \'cancel\')">Cancel</span></span>', icon: 'icon-warning2'});
	}else{
		if (pressDate.find('.Pickdate').attr('data-old-value') == "Press") return false;
		var confirmation = 'Do you want to delete the current press date?';
		notification({title : 'Confirmed Date', type : 'warning', closeIcon : false, content : confirmation + '<br><span class="action"><span class="btn green lighten-2" onclick="savePressDate(\'' + datePicker.attr('id') + '\', \'delete\')">Yes</span><span class="btn red lighten-2" onclick="savePressDate(\'' + datePicker.attr('id') + '\', \'cancel\')">Cancel</span></span>', icon: 'icon-warning2'});
	}
	$('.datepicker-inline:visible').parent().hide();
}

function savePressDate(datePicker, param){
	var pressDate = $('#' + datePicker).closest('.pressDate');
	if (param == 'yes'){
		var currPressDate = $('#' + datePicker).data('datepicker').selectedDates[0].toString().slice(4,15);
		var data = '<node data-xpath="//workflow" data-type="add-attribute" data-name="data-press-date" data-value="' + currPressDate + '"></node>';
	}else if (param == 'no'){
		var currPressDate = $('#' + datePicker).data('datepicker').selectedDates[0].toString().slice(4,15);
		var data = '<node data-xpath="//workflow" data-type="add-attribute" data-name="data-press-date" data-value="' + currPressDate + '"></node>';
	}else if (param == 'delete'){
		pressDate.find('.Pickdate').html('Press');
		var data = '<node data-xpath="//workflow" data-type="remove-attribute" data-name="data-press-date"></node>';
	}else{
		pressDate.find('.Pickdate').html(pressDate.find('.Pickdate').attr('data-old-value'));
		data = "";
	}
	if (data != ""){
		var param = {
				'customer': $('#customerselect').val(),
				'project' : $('#projectselect').val(), 
				'doi': pressDate.closest('.jobCards[data-doiid]').attr('data-doiid'),
			}
		param.data = '<root>' + data + '</root>';
		jQuery.ajax({
			type: "POST",
			url: "/api/updateattribute",
			data: param,
			success: function (data) {
				console.log(data)
			}
		});
	}
	$('.toast').fadeOut(function(){
		$(this).remove();
	});
}

function notification(param){
	var id = new Date().getTime();
	var notice = $('<div id="'+id+'" class="kriya-notice ' + param.type + '" />');
	notice.append('<div class="row kriya-notice-header" />');
	notice.append('<div class="row kriya-notice-body">'+ param.content +'</div>');

	if(param.icon){
		notice.find('.kriya-notice-header').append('<i class="icon '+param.icon+'"></i>');
	}
	if(param.title){
		notice.find('.kriya-notice-header').append(param.title);
	}
	if(param.closeIcon != false){
		notice.find('.kriya-notice-header').append('<i class="icon-close" onclick="kriya.removeNotify(this)"></i>');
	}
	Materialize.toast(notice[0].outerHTML, param.timeout);
	return id;
}