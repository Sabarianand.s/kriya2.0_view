/**
* eventHandler - this javascript holds all the functions required for Reference handling
*					so that the functionalities can be turned on and off by just calling the required functions
*					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['menu','query','components','welcome'];
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel', 
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);
			
			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			// event.target is the clicked element!

			kriya.evt = event;
			if (typeof(event.pageX) != "undefined"){
				kriya.pageXaxis = event.pageX;
				kriya.pageYaxis = event.pageY;
			}
			if ($(event.target).find('.picker__input').length > 0){
				event.stopPropagation();
			}
			elements = $(event.target);
			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel]) == "object" && typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}
								
								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.welcome = {
		signoff: {
			logOut: function(){
				if ($('#contentContainer').attr('data-stage-name') == undefined) return false;
				$('.la-container').fadeIn();
				var d = new Date();
				var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				var parameters = {
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project,
					'doi':kriya.config.content.doi,
					'type':'mergeData',
					'data': {
						'process': 'update',
						'xpath': '//workflow/stage[name[.="' + $('#contentContainer').attr('data-stage-name') + '"] and status[.="in-progress"]]',
						'content': '<stage><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status type="user">logged-off</status></log></job-logs></stage>' 
					}
				};
				kriya.general.sendAPIRequest('updatedata', parameters, function(res){
					kriya.config.pageStatus = "logged-off";
					setTimeout(function(){
						window.close();	
					},1000);
				},function(res){
					$('.la-container').fadeOut()
				});
			},
			reportIssueModal: function(param, targetNode){
				if (kriya.config.content.stage == 'support'){
					jQuery.ajax({
						type: "GET",
						url: "/api/getdata?customer=" + kriya.config.content.customer + "&project=" + kriya.config.content.project + "&doi=" + kriya.config.content.doi + "&xpath=//production-notes/note[@type='support'][.//div[@class='issue-data']][last()]",
						contentType: "application/xml; charset=utf-8",
						dataType: "xml",
						success: function (msg) {
							$('[data-component="resolveIssue_edit"]').removeAttr('data-parent-note-id');
							if (msg && !msg.error) {
								var commentObj = $(msg);
								if ($(commentObj).find('note').length > 0){
									var parentID = $(commentObj).find('note').attr('id');
									$('[data-component="resolveIssue_edit"]').attr('data-parent-note-id', parentID);
								}
								eventHandler.components.general.openPopUps($('[data-component="resolveIssue_edit"]'));
							}else{
								eventHandler.components.general.openPopUps($('[data-component="resolveIssue_edit"]'));
							}
						}, error: function(){
							eventHandler.components.general.openPopUps($('[data-component="resolveIssue_edit"]'));
						}
					})
				}else{
					$('[data-component="reportIssue_edit"]').find('[data-class="category"],[data-class="priority"]').val('');
					eventHandler.components.general.openPopUp('', $('[data-component="reportIssue_edit"]'));
				}
			},
			openUploadNotesFile: function(param, targetNode){
				eventHandler.components.general.getSlipPopUp($('[data-component="uploadNotesFile_edit"]')[0], $('[data-component="reportIssue_edit"]'));
			},
			uploadNotesFile: function(targetNode){
				if ($(targetNode).attr('src') != undefined || $(targetNode).attr('src') != ""){
					var fileContent = '<span class="comment-file"><img src="' + $(targetNode).attr('src') + '" data-original-name="' + $(targetNode).attr('data-original-name') + '" data-primary-extension="' + $(targetNode).attr('data-primary-extension') + '"/></span>';
					$('[data-component="reportIssue_edit"]').find('div[contenteditable].notesContainer').append(fileContent)
				}
				eventHandler.components.general.closePopUps('', $('[data-component="uploadNotesFile_edit"]'));
			},
			reportIssue: function(param, targetNode){
				var component = $(targetNode).closest('[data-component]');
				var notesContainer = $(component).find('div[contenteditable].notesContainer');
				var category = "";
				if (component.attr('data-component') == 'reportIssue_edit'){
					if ( !/[a-z]/i.test(notesContainer.html()) || $(component).find('[data-class="cause"]').val() == "" || $(component).find('[data-class="category"]').val() == "" || $(component).find('[data-class="priority"]').val() == ""){
						return false;
					}
					var cause = $(component).find('[data-class="cause"] option:selected').text();
					if ($(component).find('[data-class="cause"]').val() == 'others'){
						if ($(component).find('[data-class="other-reason"]').text() == ''){
							return false;
						}
						cause = $(component).find('[data-class="other-reason"]').text();
					}
					category = $(component).find('[data-class="category"]').val() + ',' + $(component).find('[data-class="priority"]').val() + ',' + kriya.config.content.stageFullName;
					category = category + ',level-2';
					categoryInfo = '<div class="category-info"><span class="cause">' + cause + '</span><span class="category">' + $(component).find('[data-class="category"] option:selected').text() + '</span><span class="priority">' + $(component).find('[data-class="priority"] option:selected').text() + '</span><span class="stage">' + kriya.config.content.stageFullName + '</span><span class="level">level-2</span></div>';
				}
				if ( !/[a-z]/i.test(notesContainer.html())){
					return false;
				}
				if (category == "" && component.attr('data-component') != 'reportIssue_edit'){
					if ($(component).find('[data-class="what-category"]').val() == "" || $(component).find('[data-class="who-category"]').val() == "" || $(component).find('[data-class="why-category"]').val() == "" || $(component).find('[data-class="where-category"]').val() == ""){
						return false;
					}
					category = $(component).find('[data-class="what-category"]').val() + ',' + kriya.config.content.stageFullName;
					categoryInfo = '<div class="category-info"><span class="what">' + $(component).find('[data-class="what-category"] option:selected').text() + '</span><span class="who">' + $(component).find('[data-class="who-category"] option:selected').text() + '</span><span class="why">' + $(component).find('[data-class="why-category"] option:selected').text() + '</span><span class="where">' + $(component).find('[data-class="where-category"] option:selected').text() + '</span><span class="stage">' + kriya.config.content.stageFullName + '</span></div>';
				}
				$('.la-container').fadeIn();
				var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
				var issueData = '<div class="issue-data">';
				issueData += categoryInfo;
				issueData += '<div class="issue-notes">' + notesContainer.html().replace(/(<img[^>]+)>/g, '$1/>') + '</div>';
				issueData += '</div>';
				var commentJSON = {
					"content": issueData,
					"fullname": kriyaEditor.user.name,
					"created": currTime
				}
				if (component.attr('data-component') == 'resolveIssue_edit' && $(component)[0].hasAttribute('data-parent-note-id')){
					commentJSON.parent = $(component).attr('data-parent-note-id');
				}
				var parameters = {
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project, 
					'doi':kriya.config.content.doi 
				};
				if (component.attr('data-component') == 'resolveIssue_edit'){
					parameters.releaseHold = 'true';
					parameters.holdComment = category;
				}else{
					parameters.stageName = 'Support';
					parameters.holdComment = category;
				}
				kriya.general.sendAPIRequest('addstage', parameters, function(res){
					$.ajax({
						type: 'POST',
						url: '/api/save_note',
						processData: false,
						contentType: 'application/json; charset=utf-8',
						data: JSON.stringify({
							"content": commentJSON,
							"doi": kriya.config.content.doi,
							"customer": kriya.config.content.customer,
							"project": kriya.config.content.project,
							"noteType": "support",
							"logging": 'true'
						}),
						success: function (comment) {
							if (comment && comment.id){
								$('.la-container').fadeOut();
								$('body .messageDiv .message').html('Your issue has been reported successfully.<br> Please close the browser to exit the system.');
								$('body .messageDiv').removeClass('hidden');
								$('body .messageDiv .feedBack').removeClass('hidden');
								kriya.config.pageStatus = "signed-off"; //This flag used in on window close in keeplive.js
							}else{
								$('.la-container').fadeOut();
								kriya.notification({
									title: 'Failed.',
									type: 'error',
									content: 'Issue in adding ticket to support. Please try again!',
									icon : 'icon-info'
								});
							}
						},
						error: function(err){
							$('.la-container').fadeOut();
							kriya.notification({
								title: 'Failed.',
								type: 'error',
								content: 'Issue in adding ticket to support. Please try again!',
								icon : 'icon-info'
							});
						}
					});
				},function(res){
					$('.la-container').fadeOut()
					kriya.notification({
						title: 'Failed.',
						type: 'error',
						content: 'Signoff article was failed.',
						icon : 'icon-info'
					});
				});
			}
		}
	},
	eventHandler.menu = {
		edit : {
			showHideMenu: function(param,targetNode){
				//get active nodes and remove class "active" 
				if (! $(targetNode).closest('.kriyaMenuControl').hasClass('active')){
					$('.templates [data-component]').addClass('hidden');
					$('.kriyaMenuControl.active').removeClass('active');
					$('[data-menu-selector]').addClass('hidden');
					$('[data-menu-selector]:first').removeClass('hidden');
					if (kriya.selection && $(targetNode).closest('.kriyaMenuControl').find('[data-menu-selector]').length > 0){
						var node = kriya.selection.commonAncestorContainer;
						if ($(node).closest('div.front,div.body,div.back, div.sub-body, div.sub-front, div.sub-back').length > 0){
							$('[data-menu-selector]').addClass('hidden');
							var blockClass = $(node).closest('div.front,div.body,div.back, div.sub-body, div.sub-front, div.sub-back').attr('class');
							$('*[data-menu-selector*=" ' + blockClass + ' "]').removeClass('hidden');
						}
					}
					$(targetNode).closest('.kriyaMenuControl').addClass('active');
				}else{
					$('.kriyaMenuControl.active').removeClass('active');
				}
			},
			formatting: function(param,targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;
				if(param && param.command){
					if(param.command.match(/color/i)){
						var argument = $(target).attr('data-color');
					}
					editor.composer.commands.exec(param.command, argument);
				}
			},
			content: function(param, targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;
				if(param && param.command){
					if(param.command.match(/color/i)){
						param.argument = $(target).attr('data-color');
					}
					kriyaEditor.init.execCommand(param.command, param.argument);	
					kriyaEditor.init.addUndoLevel('wysihtml-formatting');
					kriyaEditor.init.save();
				}
			}
		},
		upload: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				block.progress('Uploading to server');
				$.ajax({
					url: '/getS3UploadCredentials',
					type: 'GET',
					data: {'filename': file.name},
					success: function(response){
						if (response && response.upload_url){
							var formData = new FormData();
							$.each(response.params, function(k, v){
								formData.append(k, v);
							})
							formData.append('file', file, file.name);
							param.response = response;
							param.formData = formData;
							eventHandler.menu.upload.uploadToServer(param, targetNode);
						}
					},
					error: function(response){
					}
				});
			},
			uploadToServer: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var formData = param.formData;
				var response = param.response;
				$.ajax({
					url: response.upload_url,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								block.progress('Uploading to server ' + percentComplete);
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								block.progress('Uploading to server ' + percentComplete);
							}
						}, false);
						return xhr;
					},
					success: function(res){
						block.progress('Converting for web view');
						if (res && res.hasChildNodes()){
							var uploadResult = $('<res>' + res.firstChild.innerHTML + '</res>');
							if (uploadResult.find('Key,key').length > 0 && uploadResult.find('Bucket,bucket').length > 0){
								var ext = file.name.replace(/^.*\./, '')
								var fileName = uploadResult.find('Key,key').text().replace(/^(.*?)(\/.*)$/, '$1') + '.' + ext;
								var params = {
										srcBucket: uploadResult.find('Bucket,bucket').text(),
										srcKey: uploadResult.find('Key,key').text(),
										dstBucket: "kriya-resources-bucket",
										dstKey: kriya.config.content.customer + '/' + kriya.config.content.project + '/' + kriya.config.content.doi + '/resources/' + fileName,
										convert: 'true'
									}
									if (/docx?/i.test(ext)) params.convert = 'false'
								$.ajax({
									async: true,
									crossDomain: true,
									url: response.apiURL,
									method: "POST",
									headers: {
										accept: "application/json"
									},
									data: JSON.stringify(params),
									success: function(status){
										console.log(status);
										if (status.status.code == 500) {
											block.progress('Failed', true);
											setTimeout(function() {
												block.progress('Uploaded', true);
											}, 5000 );
											return false;
										}
										var fileDetailsObj = status.fileDetailsObj;
										if (/docx?/i.test(status.fileDetailsObj.extn)){
											var param = {'customer': kriya.config.content.customer, 'project': kriya.config.content.project, 'doi': kriya.config.content.doi, 'key': status.fileDetailsObj.dstKey, 'service': 'doctohtml', 'data': fileDetailsObj}
											$.ajax({
												type: "POST",
												url: "/api/convertwordtohtml",
												data: param,
												success: function (msg) {
													eventHandler.menu.upload.getJobStatus(kriya.config.content.customer, msg.message.jobid, block, 'exeter', 'typesetter', kriya.config.content.doi, {'onsuccess': 'eventHandler.components.general.replaceDoc'});
												},
												error: function(xhr, errorType, exception) {
													$('.la-container').fadeOut()
													return null;
												}
											});
										}else{
											$(targetNode).attr('src', '/resources/' + fileDetailsObj.dstKey);
											$(targetNode).attr('data-href', fileDetailsObj.dstKey);
											$(targetNode).attr('data-extension', status.fileDetailsObj.extn);
											$(targetNode).attr('data-name', status.fileDetailsObj.name);
										}
										$('.ama-uploader-dragging').removeClass('ama-uploader-dragging');
										setTimeout(function() {
											block.progress('Uploaded', true);
										}, 1000 );
									},
									error: function(err){
										console.log(err)
									}
								});
							}
						}
					}
				});
			},
			getJobStatus: function(customer, jobID, notificationID, userName, userRole, doi, callback){
				/***
				**	to get status of a job from job mananger
				**	optional : can send callback as an object {'process': 'somefunction', 'onfailure': 'anotherfunction', 'onsucess': 'successfunction'}, with which the status will be returned to that function according to the status
				***/
				if (callback == undefined) callback = false;
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.status){
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if(data.status.message.stage.current){
							currStep = data.status.message.stage.current;
							}    
							var loglen = data.status.message.log.length;    
							var process = data.status.message.log[loglen-1];
							if (/completed/i.test(status)){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onsuccess){
									var functionArray = callback.onsuccess.split('.');
									if (functionArray.length > 3 && typeof(window[functionArray[0]][functionArray[1]][functionArray[2]][functionArray[3]]) == "function")
									window[functionArray[0]][functionArray[1]][functionArray[2]][functionArray[3]](customer, jobID, notificationID, doi, data.status.message);
								}
							}else if (/failed/i.test(status) || code != '200'){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onfailure && typeof(window[callback.onfailure]) == "function"){
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							}else if (/no job found/i.test(status)){
								if (notificationID) notificationID.progress(process);
								if(callback && callback.onfailure && typeof(window[callback.onfailure]) == "function"){
									window[callback.onfailure](customer, jobID, notificationID, doi, data.status.message);
								}
							}else{
								if (process != '') $('.notify').html(process);
								if (notificationID) notificationID.progress(process);
								if(callback && callback.process && typeof(window[callback.process]) == "function"){
									window[callback.process](customer, jobID, notificationID, doi, data.status.message);
								}else{
									setTimeout(function() {
										eventHandler.menu.upload.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback);
									}, 1000 );
								}
							}
						}
						else{
							$('.notify').html('Failed');
						}
					},
					error: function(error){
						if(error.status == 502){
							setTimeout(function() {
								eventHandler.menu.upload.getJobStatus(customer, jobID, notificationID, userName, userRole, doi, callback);
							}, 1000 );
						}
					}
				});
			},
		},
		table:{
			// Functions needed to handle table structure edit
			// Delete row or column only if empty - MMH
			deleteRowAndCol: function(param, targetNode){
				if(param && param.deleteItem){
					var saveNode = $('.wysiwyg-tmp-selected-cell').closest('table').closest('[id]');
					var tableEle = $(editor.composer.tableSelection.table);
					if(param.deleteItem == "row" && $('.wysiwyg-tmp-selected-cell').closest('tr').text().trim() != ""){
						return false;	
					}
					else if(param.deleteItem == "column"){
						if($('.wysiwyg-tmp-selected-cell').index() >= -1 && $('.wysiwyg-tmp-selected-cell').closest('table').find('[colspan],[rowspan]').length == 0){
							if($("tr td:nth-child(" + ($('.wysiwyg-tmp-selected-cell').index() +1) + ")").text().trim() != "") return false;
						}
						if($('.wysiwyg-tmp-selected-cell').index() >= -1 && $('.wysiwyg-tmp-selected-cell').closest('table').find('[colspan]').length == 0){
							if($("tr td:nth-child(" + ($('.wysiwyg-tmp-selected-cell').index() +1) + ")").text().trim() != "") return false;
						}
						else if($('.wysiwyg-tmp-selected-cell').index() >= -1){
							var columnToRemove = $('.wysiwyg-tmp-selected-cell').index();
							var colSpans = 0;
							var colSpanRemaining = []
							var emptyCols = true;
							$('.wysiwyg-tmp-selected-cell').closest('table').find('tr').each(function(i,trElem){
								colSpans = 0;
								if(colSpanRemaining.length > 0) colSpans = colSpanRemaining.pop();
								$(trElem).find('td').each(function(j,tdElem){ 
									if( $(tdElem).attr('colspan') && $(tdElem).attr('colspan') > 1 && $(tdElem).attr('rowspan') && $(tdElem).attr('rowspan') > 1){
										for(var rspan = 0; rspan < parseInt($(tdElem).attr('rowspan'))-1;rspan++) colSpanRemaining.push((parseInt($(tdElem).attr('colspan'))-1));
										colSpans += (parseInt($(tdElem).attr('colspan'))-1);
									}
									else if( $(tdElem).attr('colspan') && $(tdElem).attr('colspan') > 1){
										colSpans += parseInt($(tdElem).attr('colspan')) - 1;
									}

									if((j+colSpans) == columnToRemove && ($(tdElem).text().trim()!="")) {
										return emptyCols = false;
									}
								})
								if(!emptyCols) return false;
							});
							
							if(!emptyCols) return false;

						}

					}
					editor.composer.commands.exec("deleteTableCells", param.deleteItem);
					kriyaEditor.settings.undoStack.push(saveNode);
					kriyaEditor.init.addUndoLevel('deleteRowAndCol');
				}
			},
			
			mergeAndSplit: function(param, targetNode){
				// while cell with col span is selected an extra cell will be introduced - aravind
				// to handle this reassiging start and end of table selection in editor.composer
				if(param && param.command && param.command == "merge"){
					if(editor.composer.tableSelection.end && editor.composer.tableSelection.end.hasAttribute('colspan')){
						var lastCellColSpan = parseInt($(editor.composer.tableSelection.end).attr('colspan'));
						if(lastCellColSpan && lastCellColSpan > 1){
							if($('.wysiwyg-tmp-selected-cell').length > 1){
								setCursorPosition($('.wysiwyg-tmp-selected-cell').last()[0],0);
								editor.composer.tableSelection.end = $('.wysiwyg-tmp-selected-cell').last()[0];
								editor.composer.tableSelection.start = $('.wysiwyg-tmp-selected-cell').first()[0];
							}
						}
					}
				}
				var tableEle = $(editor.composer.tableSelection.table);
				//#440 - All Customers | Table | Merge Cells - Prabakaran.A (prabakaran.a@focusite.com)
				editor.composer.commands.exec(param.command == 'split'?"splitTableCells":"mergeTableCells");
				//End of #440
				$('.wysiwyg-tmp-selected-cell').each(function(){
					//Remove the all empty para if cell has para with content
					//else skip the first empty para then remove all para
					if($(this).find('.jrnlTblBody:empty').length > 0 && $(this).find('.jrnlTblBody:not(:empty)').length > 0){
						$(this).find('.jrnlTblBody:empty').remove();
					}else if($(this).find('.jrnlTblBody:empty').length > 0 && $(this).find('.jrnlTblBody:not(:empty)').length < 1){
						$(this).find('.jrnlTblBody:empty:not(:first)').remove();
					}
				});
				assignUUIDs(tableEle.find(":not([id])"));
				kriyaEditor.settings.undoStack.push(tableEle);
				kriyaEditor.init.addUndoLevel('table-merge-split');
				editor.composer.tableSelection.unSelect();
			},
			showSpanHint: function(param, targetNode){
				// Toggle spans applied in table - MMH
				if(param && param.command && param.command == "toggle"){

					if($('.wysiwyg-tmp-selected-cell').length > 0){
						$('.wysiwyg-tmp-selected-cell').closest('table').find('th[rowspan="1"],td[rowspan="1"]').removeAttr('rowspan');
						$('.wysiwyg-tmp-selected-cell').closest('table').find('th[colspan="1"],td[colspan="1"]').removeAttr('colspan');
						$('.wysiwyg-tmp-selected-cell').closest('table').toggleClass('showSpanHint');
					}
				}
			},
		},
	},
	eventHandler.components = {
		general : {
			scrollElementOnContent : function(param, targetNode){
				var dataChild = targetNode[0];
				var topPos = parseFloat($(dataChild).css('top'));
				var contentHeight = 0;
				$('#contentDivNode > *').each(function(){contentHeight += $(this).height()});
				$('#contentDivNode').scrollTop((contentHeight/$('#contentDivNode').height()) * topPos);
			},
			getSlipPopUp: function(popUp, component){
				$(popUp).addClass('manualSave');
				eventHandler.components.general.openPopUps(popUp, component);
			},
			closePopUp: function(param, targetNode){
				$(targetNode).closest("[data-component]").animate({
					top: ('40%'),
					left:('40%'),
					width:('0'),
					height:('0'),
					opacity:('0')
				},500);
			},
			openPopUp: function(param, targetNode){
				var curentIndex = $('#compDivContent [data-type="popUp"]:visible:last').css('z-index');
				curentIndex = (curentIndex)?(parseInt(curentIndex)+1):'';
				$(targetNode).removeAttr('style').removeClass('hidden').css({
					'top':'30%',
					'left':'40%',
					'width':'0',
					'height':'0',
					'opacity':'1',
					'z-index' : curentIndex
				});
				$(targetNode).animate({
					top: '0',
					left:'0',
					width:'100%',//($(component).outerWidth()),
					height:'100%',//($(component).outerHeight()),
					opacity:('1')
				});
			},
			openPopUps: function(popUp, component){
				//if more than one popup is open then update the z-index
				var curentIndex = $('#compDivContent [data-type="popUp"]:visible:last').css('z-index');
				curentIndex = (curentIndex)?(parseInt(curentIndex)+1):'';
				kriya.popupSelection = false;
				$(popUp).find('.com-save').removeClass('disabled');

				$(popUp).find('[data-validate-for]').each(function(){
					var validateFor      = $(this).attr('data-validate-for');
					var validateSelector = validateFor.split('==');
					var validateNode     = kriya.xpath(validateSelector[0], $(component)[0]);
					if(validateNode.length > 0){
						$(this).attr('data-validate', validateSelector[1]);
					}else{
						$(this).removeAttr('data-validate');
					}
				});
				
				$(popUp).removeAttr('style').removeClass('hidden').css({
					'top':'30%',
					'left':'40%',
					'width':'0',
					'height':'0',
					'opacity':'1',
					'z-index' : curentIndex
				});
				$(popUp).animate({
					top: '0',
					left:'0',
					width:'100%',//($(component).outerWidth()),
					height:'100%',//($(component).outerHeight()),
					opacity:('1')
				},500, function(){
					if ($(popUp).find('[data-wrapper]').length > 0){
						popUpTracker = {};
						$(popUp).find('label').attr('contenteditable', 'false');
					}
				});
			},
			getReferenceStylus: function(){
				jQuery.ajax({
					type: "GET",
					url: "/api/getrefcomponent",
					data: {'customer':kriya.config.content.customer, 'project':kriya.config.content.project, 'type': 'stylus'},
					success: function (data) {
						data = data.replace(/&apos;/g, "\'");
						var hoverMenu = $('<div data-type="popUp" data-component="jrnlRefText" data-selection="true" class="autoWidth hidden z-depth-2 bottom">');
						$(hoverMenu).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
						$(hoverMenu).append($(data));
						$(hoverMenu).append('<span class="btn btn-hover" data-type=" jrnlRefText " onclick="stylePalette.actions.clearFormatting()" data-selection="true">Clear format</span>');
						$('.templates').append(hoverMenu);
						$('[data-component] .dropdown-button').dropdown();

					}
				});
			},
			updateMenuBar: function(param, targetNode){
				$('[data-menu-selector]').addClass('hidden');
				$('[data-menu-selector]:first').removeClass('hidden');
				$("[for*='menu']").removeClass('disabled');
				if (kriya.selection){
					var selection = rangy.getSelection();
					var range = selection.getRangeAt(0);
					$('.mnuButtons.insertQuery').addClass('disabled');
					if (range.toString() != ""){
						$('.mnuButtons.insertQuery').removeClass('disabled');
					}
					var node = kriya.selection.commonAncestorContainer;
					if ($(node).closest('div.front,div.body,div.back, div.sub-body, div.sub-front, div.sub-back').length > 0){
						$('[data-menu-selector]').addClass('hidden');
						var blockClass = $(node).closest('div.front,div.body,div.back, div.sub-body, div.sub-front, div.sub-back').attr('class');
						$('*[data-menu-selector*=" ' + blockClass + ' "]').removeClass('hidden');
					}
					if ($(node).closest('.jrnlTblBlock, .inline_table').length > 0){
						if ($("[for*='menu']:contains('Table')").hasClass('disabled')){
							$("[for*='menu']:contains('Table')").removeClass('disabled');
							$("[for*='menu']:contains('Table')").prev('input').prop("checked", true);
						}
					}else{
						$("[for*='menu']:contains('Table')").addClass('disabled');
						if ($("[for*='menu']:contains('Table')").prev('input').prop('checked')){
							$("[for*='menu']:contains('Home')").prev('input').prop('checked', true);
						}
					}
				}
			},
			contentEvt: function(param, targetNode){
				var target = kriya.evt.target || kriya.evt.srcElement;
				var range = rangy.getSelection();
				kriya.selection = range.getRangeAt(0);
				kriya.selectedNodes = kriya.selection.getNodes();
				
				eventHandler.components.general.updateMenuBar();
				$('.templates [data-component]').addClass('hidden');
				if (target.nodeType == 3){
					target = target.parentNode;
				}
				var componentName = target.className.split(" ")[0];
				if ($('[data-component="'+componentName+'"]').length == 0){
					do {
						target = target.parentNode;
						componentName = target.className.split(" ")[0];
					}
					while ($('[data-component="' + componentName + '"]').length == 0 &&
							(!/front|body|back/.test(componentName) && $(target).closest('.front,.body,.back, div.sub-body, div.sub-front, div.sub-back').length > 0)
					)
				}
				
				// for context menu
				if ($('[data-component="'+componentName+'"]').length == 0){
					target = kriya.evt.target || kriya.evt.srcElement;
					componentName = 'contextMenu';
				}
				
				if($(target).closest('.front,.body,.back,.jrnlRefText, div.sub-body, div.sub-front, div.sub-back').length > 0){
                    var parentClass = $(target).closest('.front,.body,.back,.jrnlRefText, div.sub-body, div.sub-front, div.sub-back').attr('class').split(' ')[0];
                }
                else{
                    if ($(target).closest('.AbsRefText').length > 0){
                        var parentClass = 'AbsRefText';
                    }else{
                        var parentClass = 'front';
                    }
				}
				
				if (parentClass == "jrnlRefText") {
					componentName = "jrnlRefText";
					if ($('[data-component="'+componentName+'"]').length > 0){
						var parentNode = $(target).closest('.jrnlRefText');
						if ($(parentNode).find('.RefJournalTitle').length > 0) {refType = 'Journal'}
						else if ($(parentNode).find('.RefDataTitle').length > 0) {refType = 'Data'}
						else if ($(parentNode).find('.RefBookTitle,.RefChapterTitle,.RefPublisherLoc').length > 0) {refType = 'Book'}
						else if ($(parentNode).find('.RefThesis').length > 0) {refType = 'Thesis'}
						else if ($(parentNode).find('.RefSoftName').length > 0) {refType = 'Software'}
						else if ($(parentNode).find('.RefPatentTitle').length > 0) {refType = 'Patent'}
						else if ($(parentNode).find('.RefConfName').length > 0) {refType = 'Conference'}
						else if ($(parentNode).find('.RefWebSite').length > 0) {refType = 'Website'}
						else {refType = 'Journal'}
						var component = $('[data-component="' + componentName + '"]');
						$(component).removeClass('hidden');
						$(component).find('[data-type]').addClass('hidden');
						$(component).find('[data-type*=" '+ parentClass + ' "]').removeClass('hidden');
						$(component).find('[data-ref-type]').addClass('hidden');
						$(component).find('[data-ref-type="'+ refType + '"]').removeClass('hidden');
						$(component).find('.referenceTypes').val(refType);
						var w = $(component).width();
						var l = kriya.evt.pageX - (w/2);
						if (l < 0) l = 0;
						var h = $(component).height();
						var t = kriya.evt.pageY - h - 25;
						if (component.find('.material-icons.arrow-denoter').length > 0){
							component.find('.material-icons.arrow-denoter').width(component.find('.material-icons.arrow-denoter').css('font-size'))
							var acp = kriya.evt.pageX - l - (component.find('.material-icons.arrow-denoter').width()/2);
							component.find('.material-icons.arrow-denoter').css('left', acp);
						}
						$(component).css({'top' : t + 'px', 'left': l + 'px', 'opacity' : '1', 'display' : 'block', 'position' : 'absolute'}).css({'z-index':66666}).fadeIn(200);
					}
				}
				else if ($('[data-component="'+componentName+'"]').length > 0){
					var component = $('[data-component="' + componentName + '"]');
					$(component).removeClass('hidden');
					$(component).find('[data-type]').addClass('hidden');
					$(component).find('[data-type*=" '+ parentClass + ' "]').removeClass('hidden');
					$(component).find('[data-pattern],[data-selection]').addClass('hidden');
					var patterns = kriya.selection.getNodes().filter(function(r){
						return $(r).hasClass('jrnlPatterns');
					})
					if (patterns.length > 0){
						$(component).find('[data-type*=" '+ parentClass + ' "][data-pattern]').removeClass('hidden');
					}else if (kriya.selection.toString() != ""){
						$(component).find('[data-type*=" '+ parentClass + ' "][data-selection]').removeClass('hidden');
					}
					var w = $(component).width();
					var l = kriya.evt.pageX - (w/2);
					if (l < 0) l = 0;
					var h = $(component).height();
					var t = kriya.evt.pageY - h - 25;
					if (component.find('.material-icons.arrow-denoter').length > 0){
						component.find('.material-icons.arrow-denoter').width(component.find('.material-icons.arrow-denoter').css('font-size'))
						var acp = kriya.evt.pageX - l - (component.find('.material-icons.arrow-denoter').width()/2);
						component.find('.material-icons.arrow-denoter').css('left', acp);
					}
					$(component).css({'top' : t + 'px', 'left': l + 'px', 'opacity' : '1', 'display' : 'block', 'position' : 'absolute'}).css({'z-index':66666}).fadeIn('slow');
				}
				$('.class-highlight').removeClass('hidden');
				$('.class-highlight').text($(kriya.selection.startContainer).closest('*[class]').attr('class').split(' ')[0].replace(/^(jrnl|Ref)/,''));
				// if clicked on table, open table structure edit popup
				if($(target).closest('table').length > 0 ){
					kriya.popUp.init($(target), {'name' : 'tableMenu'})
				}
			},
			changeRefType: function(param, targetNode){
				var refType = targetNode.val();
				$('[data-component="jrnlRefText"]').find('[data-ref-type]').addClass('hidden');
				$('[data-component="jrnlRefText"]').find('[data-ref-type="'+ refType + '"]').removeClass('hidden');
			},
			addAsCitation: function(param, targetNode){
				var popUp = $(targetNode).closest('[data-component]');
				var citationString = [];
				$(popUp).find('ul.collection:visible').find('li.active').each(function(){
					citationString.push($(this).attr('data-id'));
				});
				if (kriya.selection && kriya.selection.text() != "" && citationString.length > 0){
					var floatID = ' ' + citationString.join(' ') + ' ';
					var citeClass = $(popUp).find('ul.collection:visible').attr('data-class');
					var selectedText = kriya.selection.text().replace(/[\[\]\(\)\,\;\s\u2012\u2013\u2014\-]+|and/gi, '');
					selectedText = selectedText.replace(/[0-9]+/gi, '');
					if (selectedText == ""){
						var citeNode = $('<span class="' + citeClass + '" data-citation-string="' + floatID + '">' + kriya.selection.text() + '</span>');
					}else{
						var citeNode = $('<span class="' + citeClass + '" data-citation-string="' + floatID + '" data-mark-cited="' + citationString.join(' ') + '">' + kriya.selection.text() + '</span>');
					}
					kriya.selection.deleteContents();
					kriya.selection.insertNode(citeNode[0]);
					if(kriya.selection.commonAncestorContainer.parentElement.className == "jrnlUncitedRef" || kriya.selection.commonAncestorContainer.parentElement.className == "jrnlPossBibRef"){
						$(kriya.selection.commonAncestorContainer.parentElement).contents().unwrap()
					}
					kriyaEditor.init.addUndoLevel('navigation-key', citeNode);
					kriyaEditor.init.save();
				}
				$(targetNode).closest("[data-component]").animate({
					top: ('40%'),
					left:('40%'),
					width:('0'),
					height:('0'),
					opacity:('0')
				},500);
			},
			rejectCitation: function(param, targetNode){
				if (kriya.selection && param.type){
					var target = kriya.selection.commonAncestorContainer;
					if (target.nodeType == 3){
						//Getting closest node with class of param.type
						target = $(target).closest('.'+param.type);
					}
					if (target.length > 0 && target.attr('class') == param.type){
						var parentNode = $(target).parent();
						$(target).contents().unwrap();
						kriyaEditor.init.addUndoLevel('navigation-key', parentNode);
						kriyaEditor.init.save();
					}
				}
			},
			updateCitation: function(param, targetNode){
				if (kriya.selection && param.type){
					var target = kriya.selection.commonAncestorContainer;
					if (target.nodeType == 3){
						target = $(target).closest('.'+param.type);
					}
					if (target.length > 0 && target.attr('class') == param.type){
						var citationClass = {
							'jrnlFigRef':'Fig',
							'jrnlTblRef':'Tbl',
							'jrnlSupplRef':'Suppl',
							'jrnlMapRef':'Map',
						}
						var parentNode = $(target).parent();
						$(target).addClass('jrnlPossRef active');
						$('#possCiteValue').html($(target).text());
						className = citationClass[param.type];
						if($('.WordSection1 div.jrnl'+className+'Block p.jrnl'+className+'Caption[data-id]').length >0){
							$('#checkCiteDivNode').removeClass('hidden')
							if($('#refListDiv').html() == ""){
								$('.WordSection1 div.jrnl'+className+'Block p.jrnl'+className+'Caption[data-id]').each(function(){		
									$('#refListDiv').append($(this).clone())
								})
							}
						}			
					}
				}
			},
			createEqnImage: function(param, targetNode){
				if (kriya.selection && param.type){
					var target = kriya.selection.commonAncestorContainer;
					if (target.nodeType == 1){
						$(target).find('.kriyaFormula').each(function(){
							$(kriya.config.containerElm).find('.activeElement').removeClass('activeElement')
							$(this).addClass('activeElement')
							var mathtype = 'inline' 
							var clsname = $(target).attr('class');
							if(clsname.match('jrnlEqnPara')){
								var mathtype = 'display' 
							}
							$('.pre-load-container').fadeIn();
							$('.pre-load-container .pre-loader-head').html('Creating equation image');
							var tex = $(this).attr('data-tex');
							var mathtype = 'display';
							var parameters  = {
								'doi': kriya.config.content.doi,
								'customer':kriya.config.content.customer,
								'project':kriya.config.content.project,
								'tex':tex,
								'mathmode':mathtype
							};
							kriya.general.sendAPIRequest('texconversion', parameters, function(res){
								if(res.error){
									$('.pre-load-container').fadeOut();
									kriya.notification({
										title: 'Failed',
										type: 'error',
										content: 'Equation image conversion Failed',
										timeout: 8000,
										icon : 'icon-info'
									});
								}else if(res){
									$('.pre-load-container').fadeOut();
									var pngFile = (res.online)?res.online.png:'';
                                        res = res.print;
                                        var eqDepth = res.eq_sizes.Depth;
                                        var eqWidth  =  res.eq_sizes.Width;
                                        var eqHeight =  res.eq_sizes.TotalHeight;
                                        $(kriya.config.containerElm).find('.activeElement').removeAttr('data-eq-eps');
										$(kriya.config.containerElm).find('.activeElement').attr('src', pngFile).attr('data-tex', tex).attr('data-eq-depth', eqDepth).attr('data-eq-height', res.eq_sizes.Height).attr('data-eq-totalheight', res.eq_sizes.TotalHeight).attr('data-eq-width', res.eq_sizes.Width).attr('data-primary-extension', 'pdf');
										$(kriya.config.containerElm).find('.activeElement').removeClass('activeElement')
								}
							}, function(err){
								$('.pre-load-container').fadeOut();
									kriya.notification({
										title: 'Failed',
										type: 'error',
										content: 'Equation image conversion Failed.',
										timeout: 8000,
										icon : 'icon-info'
									});
								});
						});
					}
				}
			},

			unwrapPattern: function(param, targetNode){
				if (kriya.selection){
					var patterns = kriya.selection.getNodes().filter(function(r){
						return $(r).hasClass('jrnlPatterns');
					});
					$(patterns).contents().unwrap();
					$('.templates [data-component]').addClass('hidden');
				}
			},
			checkProofSelectors: function(param, targetNode){
				targetNode.closest('[data-wrapper]').find('[data-class="other-reason"]').addClass('hidden');
				if (targetNode.val() == "others"){
					targetNode.closest('[data-wrapper]').find('[data-class="other-reason"]').removeClass('hidden');
				}
			},
			//Set attribute data-table-continue to true in table by tamil selvan on 1.10.18
			setAttribDataTableContinue:function(param, targetNode){
				var parentNode = $(kriya.selection.startContainer).closest('table, p.jrnlTblCaption,p.jrnlTblFoot');
				if(parentNode.length > 0){
					$(parentNode).attr("data-table-continue","true");
				}
			},
			markAsCitation: function(param, targetNode){
				$('.templates [data-component]').addClass('hidden');
				var popUp = $('[data-component="CITATION_edit"]');
				$(popUp).find('ul.collection').each(function(){
					$(this).find('li:not([data-template])').remove();
					var selector = $(this).attr('data-source-selector');
					var template = $(this).find('li[data-template]');
					$('#contentDivNode .' + selector + '[id]').each(function(){
						var cloneNode = template.clone();
						$(cloneNode).removeAttr('data-template');
						$(cloneNode).attr('data-id', $(this).attr('id'));
						$(cloneNode).html($(this).html());
						$(template).parent().append(cloneNode);
						$(cloneNode).on('click', function(){
							$(this).toggleClass('active');
						})
					})
				});
				$(popUp).removeAttr('style').removeClass('hidden').css({
					'top':'30%',
					'left':'40%',
					'width':'0',
					'height':'0',
					'opacity':'1'
				});
				$(popUp).animate({
					top: '0',
					left:'0',
					width:'100%',
					height:'100%',
					opacity:('1')
				},500)
			},
			buildComponent: function(componentName){
				var component = kriya.componentList[componentName];
				if (! component.type) return false;
				var comp = document.createElement('div');
				comp.setAttribute('data-type', component.type);
				comp.setAttribute('data-component', componentName);
				comp.setAttribute('class', 'hidden z-depth-2');
				if (component.class){
					comp.setAttribute('class', component.class + ' ' + comp.getAttribute('class'))
				}
				$(comp).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
				for (var btn in component.buttons){
					var newButton = document.createElement('span');
					newButton.innerHTML = btn;
					newButton.setAttribute('class', 'btn btn-hover');
					if (component.buttons[btn].channel) newButton.setAttribute('data-channel', component.buttons[btn].channel);
					if (component.buttons[btn].topic) newButton.setAttribute('data-topic', component.buttons[btn].topic);
					if (component.buttons[btn].event) newButton.setAttribute('data-event', component.buttons[btn].event);
					if (component.buttons[btn].message) newButton.setAttribute('data-message', component.buttons[btn].message);
					comp.appendChild(newButton);
				}
				$('.templates').append(comp);
			},
			replaceDoc: function(customer, jobID, block, doi, status){
				/** Option to replace manuscript, which will send the ms-file to jon-manager for processing **/
				var loglen = status.log.length;    
				var result = status.log[loglen-1];
				var resultFile = $(result)
				var data = $('<file_list>');
				$('.file-list .file-table .file-case').each(function(){
					var clone = $(this).clone();
					clone.removeAttr('class');
					var file = $('<file>');
					for (var i = 0; i < clone[0].attributes.length; i++) {
						var attrib = clone[0].attributes[i];
						if (attrib.name != "data-href"){
							file.attr(attrib.name, attrib.value);
						}
					}
					if ($(this).hasClass('replace-in-progress')){
						$(this).removeClass('replace-in-progress');
						for (var i = 0; i < clone[0].attributes.length; i++) {
							var attrib = clone[0].attributes[i];
							if ($(resultFile).attr(attrib.name) != undefined && attrib.name != "data-href"){
								$(this).attr(attrib.name, $(resultFile).attr(attrib.name));
							}
						}
						if ($(resultFile).attr('file_href') != undefined){
							$(this).attr('data-href', '/resources' + $(resultFile).attr('file_href'));
						}
						$(this).find('.col.file-name').html('<span class="re-order" style="left: -10px;display: inline-block;position: relative;"><i class="material-icons moveDown" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'objectMoveDown\'}">arrow_downward</i><i class="material-icons moveUp" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'objectMoveUp\'}">arrow_upward</i></span>' + $(resultFile).attr('file_name'));
						$(file).attr('data-remove', 'true').attr('data-replaced-file', $(resultFile).attr('file_alt_name'));
						var d = new Date();
						var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
						var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
						$(resultFile).attr('data-track-person', kriyaEditor.user.name).attr('data-track-date', currDate + ' ' + currTime);
						data.append(resultFile);
					}
					data.append(file);
				});
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'data': data[0].outerHTML, 'file': 'file_list'};
				console.log(data[0].outerHTML);
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
					var comp = $('[data-component="FIGURE_edit"]');
					block.progress('Uploaded', true);
					eventHandler.components.general.closePopUp('', comp);
					$('.la-container').fadeOut();
				});
			},
			replaceFile: function(param, targetNode){
				/** Modal for replacing any type of type **/
				var comp= targetNode.closest('[data-component]')
				var imageNode = comp.find('img.jrnlFigure,img[data-class="jrnlFigure"]').first();
				var option = $('<option data-value="' + $('.replace-in-progress').attr('data-type') + '" file_name="' + imageNode.attr('data-name') + '" file_extension="' + imageNode.attr('data-extension') + '" file_path="' + imageNode.attr('data-href') + '" file_href="' + imageNode.attr('data-href') + '" file_designation="figure" data-href="' + imageNode.attr('src') + '" data-defined="' + $('.replace-in-progress').attr('data-float-id') + '">' + imageNode.attr('data-name') + '</option>');
				$('.replace-in-progress select').append(option);
				$('select[data-type="response"]').append(option.clone())
				$('.replace-in-progress select').find(option).attr('selected','selected');
				eventHandler.components.general.closePopUp('', comp);
				var data = $('<file_list>');
				$('select[data-type="response"] option').each(function(){
					var clone = $(this).clone();
					clone.removeAttr('class');
					var file = $('<file>');
					for (var i = 0; i < clone[0].attributes.length; i++) {
						var attrib = clone[0].attributes[i];
						file.attr(attrib.name, attrib.value);
					}
					file.removeAttr('data-defined');
					data.append(file);
				});
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'data': data[0].outerHTML};
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){});
				eventHandler.components.general.validateResources();
			},
			openReplaceFile: function(param, targetNode){
				$('.replace-in-progress').removeClass('replace-in-progress');
				$(targetNode).closest('.row').addClass('replace-in-progress');
				eventHandler.components.general.openPopUp('', $('[data-component="' + param.component + '"]'));
			},
			abstractDoi: function(num, size=4) {
				var s = num+"";
				while (s.length < size) s = "0" + s;
				return s;
			},
            applyHouseRulesAbstract: function(params, targetNode) {
                var sucCount = 0;
                $('#contentDivNode .jrnlAbsGroup').each(function(i, node) {
                    var content = node.outerHTML;
                    var doi = kriya.config.content.doi + '-' + eventHandler.components.general.abstractDoi($(node).attr('data-index').match(/\d+/));
                    params = {
                        'customerName': kriya.config.content.customer,
                        'projectName': kriya.config.content.project,
                        'articleID': doi,
						'content': content,
						'process':'ApplyHouseStyle',
					};
                    $.ajax({
                        type: "POST",
                        url: "/api/structurecontent_abstract",
                        data: params,
                        success: function(data) {
                            /* console.log('completed ' + doi); */
                            sucCount += 1;
							var response = (data.match(/<response.*/))[0];
							var index = $(response).find('content').find('.jrnlAbsGroup').attr('data-index');
							if ($('#contentDivNode .jrnlAbsGroup[data-index="' + index + '"]').length == 1) {
								$('#contentDivNode .jrnlAbsGroup[data-index="' + index + '"]').replaceWith($(response).find('content').html());
								$('#contentDivNode .jrnlAbsGroup[data-index="' + index + '"]').attr('data-edited',true);
							}
							if (sucCount == $('#contentDivNode .jrnlAbsGroup').length) {
								console.log('completed');
							} else {
								$('.pdfStatus').html('Styling Abstract ' + index + ' of ' + $('.seperated-content .jrnlAbsGroup').length);
							}
						},
						error: function(data) {
							console.log(data);
						},
                    });
                });
            },
            structureContentAbstract: function(param, targetNode) {
				param = {
					'customer':kriya.config.content.customer,
					'project':kriya.config.content.project,
					'doi':kriya.config.content.doi,
				}
				$('.la-container').fadeIn();
                $.ajax({
                    type: "POST",
                    url: "/api/structurecontent_abstract",
                    data: param,
                    success: function(data) {
                        console.log('completed');
                        data = data.replace(/&apos;/g, "\'");
                        var response = $('<r>' + data + '</r>');
                        if ($(response).find('response').length > 0 && $(response).find('response content').length > 0) {
                            $('#contentDivNode').html($(response).find('response content').html());
                            $('#contentDivNode').find('[data-untag]').each(function() {
                                var clone = $(this).clone(true);
                                $('#untaggedDivNode > div').append(clone);
                            })
                        }
                        var sucCount = 0;
                        $('#contentDivNode .jrnlAbsGroup').each(function(i, val) {
                            var content = $(this);
                            $(this).attr('data-index', 'abs' + eventHandler.components.general.abstractDoi(i));
                            $.ajax({
                                url: '/api/structurecontent_abstract',
                                data: {
                                    'process': 'style',
                                    'content': $(content).html(),
                                    'index': $(this).attr('data-index')
                                },
                                type: 'POST',
                                success: function(data) {
                                    sucCount += 1;
                                    var response = (data.match(/<response.*/))[0];
                                    var index = $(response).find('content').find('.jrnlAbsGroup').attr('data-index');
                                    /* console.log('SuccessIndex ' + currIndex); */
                                    prevIndex = parseInt(currIndex) - 1;
                                    if ($('#contentDivNode .jrnlAbsGroup[data-index="' + index + '"]').length == 1) {
                                        $('#contentDivNode .jrnlAbsGroup[data-index="' + index + '"]').replaceWith($(response).find('content').html());
                                    }
                                    if (sucCount == $('#contentDivNode .jrnlAbsGroup').length) {
										console.log('completed');
										$('.la-container').fadeOut();
										$("[data-component = fileDesignator_edit]").addClass('hidden');
                                    } else {
                                        $('.pdfStatus').html('Styling Abstract ' + index + ' of ' + $('.seperated-content .jrnlAbsGroup').length);
                                    }
                                },
                                error: function(data) {
                                    console.log(data);
                                },
                            });
                        });
                    },
                    error: function(err) {
                        console.log(err);
                        $('.pdfStatus').remove();
                        $('.la-container').fadeOut();
                    }
                });
            },
			structureContent: function(param, targetNode){
				var data = $('<files>');
				$('.file-list .file-table .file-case').each(function(){
					var clone = $(this).clone();
					clone.removeAttr('class');
					var file = $('<file>');
					for (var i = 0; i < clone[0].attributes.length; i++) {
						var attrib = clone[0].attributes[i];
						file.attr(attrib.name, attrib.value);
					}
					data.append(file);
				});
				/** save the current structure in the server **/
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'data': data[0].outerHTML};
				$('.pre-load-container .pre-loader-head').html('Structuring the content');
				$('.pre-load-container').fadeIn();
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
					var params = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'fileData': data[0].outerHTML};
					/** Sending the file list to structure content **/
					$('.pdfStatus').remove();
					//$('body').append('<span class="pdfStatus btn btn-primary">Structuring content...</span>');
					$.ajax({
						xhr: function(){
							var xhr = new window.XMLHttpRequest();
							//Download progress
							xhr.addEventListener("progress", function(evt){
								var lines = evt.currentTarget.response.split("\n");
								if(lines.length){
									var progress = lines[lines.length-1];
									if (progress) {
										//$('.pdfStatus').html(progress);
										if ($('<r>' + progress + '</r>').find('response').length == 0){
											$('.pre-load-container .pre-loader-message').html(progress);
										}
									}
								}else{
									console.log(evt.currentTarget.response);
								}
							}, false);
							return xhr;
						},
						type: "POST",
						url: "/api/structurecontent",
						data: params,
						success: function (data) {
							$('.pre-load-container').fadeOut();
							$('.pre-load-container .pre-loader-message').html('please wait ...');
							$('[data-type="popUp"]').addClass('hidden')
							data = data.replace(/&apos;/g, "\'");
							var response = $('<r>'+ data + '</r>');
							if ($(response).find('response').length > 0 && $(response).find('response content').length > 0){
								$('#contentDivNode').html($(response).find('response content').html());
								$('.pdfStatus').remove();
								//unwrap the givenname and surname in reference author after structure content
								$('#contentDivNode .RefGivenname, #contentDivNode .RefSurname, #contentDivNode .RefGivenName, #contentDivNode .RefSurName').contents().unwrap()
								//Hide "Approve" button after structure content by Tamil selvan on 10.10.18
								$('*[data-name="ApproveStyling"]').addClass('hidden');
								$('*[data-name="ApplyHouseRules"]').removeClass('hidden');
								//listout untagged element after structure content updated by vijayakumar on 7.11.2018
								$('#untaggedDivNode div').html('')
								$('#contentDivNode').find('[data-untag]').each(function(){
									var clone = $(this).clone(true);
									$('#infoDivContent > [id*=DivNode]').removeClass('active');
									$('#untaggedDivNode').addClass('active');
									$('#untaggedDivNode > div').append(clone);
								});
								//It will track No. of references untagged in structure content-kirankumar
								var parameters = {
							           	'customer': kriya.config.content.customer,
					                                'project' : kriya.config.content.project,
					                                'doi' : kriya.config.content.doi,
					                                'type'   : "tracker",
									'logMsg' : "<div><p>No. of references untagged : </p><p>"+$('.jrnlRefText.jrnluntagged:not([data-untag]), .jrnlRefText[data-untag]:not(.jrnluntagged), .jrnlRefText[data-untag].jrnluntagged').length+"</p></div>"
						 		};
					            
						            keeplive.sendAPIRequest('logerrors', parameters, function(res){
						            });
								//Create the structured content snapshot added by vijayakumar on 19-11-2018
								kriyaEditor.init.save({'funcName':kriyaEditor.init.saveBackup, 'funcParam':'structureContent'});
								var validatedMessage = '';
								$(kriya.config.containerElm).find('.jrnlFigCaption[id], .jrnlTblCaption[id]').each(function(){
									var FID = $(this).attr('id');
									var DID = $(this).attr('data-id');
									if (FID == "" || DID == ""){
										validatedMessage += '<p>Some captions are not styled properly, ID missing.</p>';
									}
								})
								if (validatedMessage != ''){
									$('#infoDivContent > [id*=DivNode]').removeClass('active');
									$('#infoDivContent').append($('<div id="validationDivNode" class="active"><p>Validation Errors</p><div>' + validatedMessage + '</div></div>'));
									kriya.notification({
										title : 'ERROR',
										type  : 'error',
										timeout : 5000,
										content : 'Structure content validation Failed',
										icon: 'icon-warning2'
									});
								}
							}
						},
						error: function(err){
							console.log(err);
							$('.pdfStatus').remove();
							$('.pre-load-container').fadeOut();
						}
					});
				},function(res){
					//error
				});
			},
			structureSubArticle: function(param, targetNode){
				var selection = rangy.getSelection();
				var node = kriya.selection.commonAncestorContainer;
				if($(node).closest('.sub-article').length > 0 ){
					$(node).closest('.sub-article').each(function(){
						var raw = $(this).clone();
						$(kriya.config.containerElm).find('.activeElement').removeClass('activeElement');
						$(this).addClass('activeElement');
						$(raw).find('span').contents().unwrap();
						$(raw).find('*[class-name]').removeAttr('class-name')
						$(raw).find('p, h1, h2, h3, h4').attr('class', 'MsoNormal');
						var rawcontent = $(raw).html()
						var params = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'fileData': '<body><div class="WordSection1">'+rawcontent+'</div></body>'};
						$('.pre-load-container').fadeIn();
						$.ajax({
							xhr: function(){
								var xhr = new window.XMLHttpRequest();
								//Download progress
								xhr.addEventListener("progress", function(evt){
									var lines = evt.currentTarget.response.split("\n");
									if(lines.length){
										var progress = lines[lines.length-1];
										if (progress) {
											//$('.pdfStatus').html(progress);
											if ($('<r>' + progress + '</r>').find('response').length == 0){
												$('.pre-load-container .pre-loader-message').html(progress);
											}
										}
									}else{
										console.log(evt.currentTarget.response);
									}
								}, false);
								return xhr;
							},
							type: "POST",
							url: "/api/structurecontent",
							data: params,
							success: function (data) {
								data = '<div>'+data+'</div>';
								if($(data).find('response content').length > 0 && $(data).find('response content').text() != ""){
									$(kriya.config.containerElm).find('.sub-article.activeElement').html($(data).find('response content').html())
									var updatecontent = $(kriya.config.containerElm).find('.sub-article.activeElement');
									var subid = $(updatecontent).attr('data-id');
									subid = subid.replace('sub-article', '');
									$(updatecontent).find('*[data-id], *[data-citation-string], *[data-id]').each(function(){
										if($(this).attr('data-citation-string')){
											var datacitation = $(this).attr('data-citation-string');
											datacitation = datacitation.replace(/(\s([a-zA-Z]))/g, ' SA'+subid+'-$2');
											$(this).attr('data-citation-string', datacitation);
										}else if($(this).attr('data-id')){
											var dataid = $(this).attr('data-id');
											$(this).attr('data-id', 'SA'+subid + '-'+dataid).attr('id', 'SA'+subid + '-'+dataid)
										}
									})
									$(updatecontent).find('.front, .body,.back').each(function(){
										$(this).attr('class', 'sub-'+$(this).attr('class'))
									})
									$('.pre-load-container').fadeOut();
									$('#contentDivNode .RefGivenname, #contentDivNode .RefSurname,.sub-article .WordSection1').contents().unwrap()
									$(kriya.config.containerElm).find('.activeElement').removeClass('activeElement');
								}else{
									$('.pre-load-container').fadeOut();
								}
							},
							error: function(err){
								console.log(err);
								$('.pdfStatus').remove();
								$('.pre-load-container').fadeOut();
							}
						});	
					});
				}
			},
			getResources: function(param, targetNode){
				/** display the list of resource to the user to map the right resource to its float **/
				$('.la-container').fadeIn();
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'process': 'resource'};
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
					$('.la-container').fadeOut();
					if (res.status == 500){
						kriyaEditor.init.postValidation();
						return;
					}
					var popUp = $('[data-component="fileDesignator_edit"]');
					var response = $(res);
					$('select[data-type="response"]').remove();
					popUp.find('[data-input-editable] .file-container .file-list').append($(res).html());
					popUp.find('[data-input-editable] .file-container .file-list .file-table').remove();
					var fileTable = $('<div class="file-table"><div class="row header"><div class="col file-name">Float</div><div class="col file-designation">Choose file/upload</div></div></div>');
					var options = $('<select data-channel="components" data-topic="general" data-event="change" data-message="{\'funcToCall\': \'validateResources\'}"></select>');
					var fileTypes = [];
					//try to identify ID for every file
					$('select[data-type="response"]').find('[data-value]').each(function(){
						var type = $(this).attr('data-value');
						if (! fileTypes[type]){
							fileTypes[type] = [];
							fileTypes[type]['na'] = [];
						}
						var fileName = $(this).attr('file_name');
						var rid = false;
						if (/fig|image[^0-9]*[0-9]/i.test(fileName) && /\.(jpe?g|tiff?|eps|png)$/i.test(fileName)){
							if (/^(fig(ures?)?|image)[\s\.\-]*[0-9]+\.([a-z]+)$/i.test(fileName)){
								rid = fileName.replace(/^(fig(ures?)?|image)[\s\.\-]*[0]*([1-9][0-9]*)\.([a-z]+)$/i, 'F$3');
							}else if (/^(fig(ures?)?|image)[\s\.\-]*[0-9]+[\s\.\-]*((fig(ure)?[\s\-]*)?supplement([\s\-]?fig(ure)?)?[\s\-]*)[0-9]+\.([a-z]+)$/i.test(fileName)){
								rid = fileName.replace(/^(fig(ures?)?|image)[\s\.\-]*([0-9]+)[\s\.\-]*((fig(ure)?[\s\-]*)?supplement([\s\-]?fig(ure)?)?[\s\-]*)([0-9]+)\.([a-z]+)$/, 'F$3-S$9');
							}
						}
						if (rid && $('#contentDivNode [id="' + rid + '"]').length > 0){
							fileTypes[type][rid] = $(this);
							//$(this).attr('data-defined', rid);
						}else{
							fileTypes[type]['na'].push($(this));
						}
					});
					eventHandler.components.general.mapResource({'className': '.jrnlFigCaption', 'options': options, 'fileTypes': fileTypes, 'type': 'figure'}, fileTable);
					eventHandler.components.general.mapResource({'className': '.jrnlSupplCaption', 'options': options, 'fileTypes': fileTypes, 'type': 'supplement'}, fileTable);
					popUp.find('[data-input-editable] .file-container .file-list').prepend(fileTable);
					if ($('.file-table .row.file-case').length  > 0){
						eventHandler.components.general.openPopUp('', popUp);
						$(popUp).find('.structure-content').addClass('hidden');
						$(popUp).find('.apply-house-style,.popup-menu').removeClass('hidden');
						eventHandler.components.general.validateResources();
					}else{
						//if no resource found, move to next step
						kriyaEditor.init.postValidation();
					}
				},function(res){
					//error
				});
			},
			mapResource: function(param, targetNode){
				//iterate through every float and match a file
				var fileTypes = param.fileTypes;
				$(param.className + '[data-id]').find('.label').each(function(){
					var curr = param.options.clone();
					curr.attr('data-file-type', param.type);
					var dataID = $(this).closest(param.className + '[data-id]').attr('data-id');
					if (param.className == '.jrnlSupplCaption' && /ST/.test(dataID)){
						return true;
					}
					if (Object.keys(fileTypes).length > 0 && fileTypes[param.type] && Object.keys(fileTypes[param.type]).length > 0 && fileTypes[param.type][dataID]){
						fileTypes[param.type][dataID].attr('data-defined', dataID);
						curr.append(fileTypes[param.type][dataID].clone());
						$('select[data-type="response"]').find('option[file_name="' + fileTypes[param.type][dataID].attr('file_name') + '"]').attr('data-defined', dataID);
						delete(fileTypes[param.type][dataID]);
					}
					curr.append('<option>---</option>');
					if (Object.keys(fileTypes).length > 0 && fileTypes[param.type] && Object.keys(fileTypes[param.type]).length > 0 && fileTypes[param.type]['na'] && fileTypes[param.type]['na'].length > 0){
						$.each(fileTypes[param.type]['na'], function(){
							curr.append($(this).clone());
						});
					}
					// add remove option if the resource should not be considered
					curr.append('<option value="remove">Remove</option>');
					targetNode.append('<div class="row file-case" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'viewFile\'}" data-float-id="' + dataID + '" data-type="' + param.type + '"><div class="col file-name">' + $(this).text() + '</div><div class="col file-designation">' + curr[0].outerHTML + '<span class="upload-file" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'openReplaceFile\', \'param\': {\'component\': \'FIGURE_edit\'}}"><i class="material-icons">file_upload</i></span></div></div>');
				});
				/* supplement */
				if($(param.className + '[data-id]').find('.label').length <= 0 && fileTypes[param.type] && fileTypes[param.type]['na'] && Object.keys(fileTypes[param.type]['na'].length>0)){
					var curr = param.options.clone();
					curr.append('<option>---</option>');
					curr.attr('data-file-type', param.type);
					$.each(fileTypes[param.type]['na'], function(){
						curr.append($(this).clone());
					});
					// add remove option if the resource should not be considered
					curr.append('<option value="remove">Remove</option>');
					$.each(fileTypes[param.type]['na'], function(i){
						targetNode.append('<div class="row file-case" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'viewFile\'}" data-float-id="SP'+(i+1)+'" data-type="' + param.type + '"><div class="col file-name"><select class="file_label"><option>Supplementary file</option><option>Supplementary figure</option><option>Supplementary table</option><option>Supplementary data</option><option>Supplementary appendix</option><option>Supplementary method</option><option>Supplementary material</option></select></div><div class="col file-designation">' + curr[0].outerHTML + '<span class="upload-file" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'openReplaceFile\', \'param\': {\'component\': \'FIGURE_edit\'}}"><i class="material-icons">file_upload</i></span></div></div>');
					});
				}
			},
			validateResources: function(param, targetNode){
				/** check if each float is mapped a resource and then enable Apply House Style **/
				if (typeof(targetNode) != "undefined"){
					var rid = targetNode.closest('[data-float-id]').attr('data-float-id');
					var fileName = targetNode.find('option:selected').attr('file_name');
					var type = targetNode.attr('data-file-type');
					targetNode.find('[data-defined]').removeAttr('data-defined');
					$('select[data-type="response"]').find('option[data-defined="' + rid + '"]').removeAttr('data-defined');
					if (targetNode.val() != '---'){
						targetNode.find('option:selected').attr('data-defined', rid);
						$('select[data-type="response"]').find('option[file_name="' + fileName + '"]').attr('data-defined', rid);
					}
					$('select[data-file-type="' + type + '"]').find('option:not([data-defined])').remove();
					$('select[data-file-type="' + type + '"]').each(function(){
						var selectOption = $(this)
						selectOption.prepend('<option>---</option>');
						$('select[data-type="response"]').find('option[data-value=' + type + ']:not([data-defined])').each(function(){
							selectOption.append($(this).clone());
						});
						// if remove option is in select(i.e: selected), then hide file upload button else append remove option and show file upload button
						if($(selectOption).find('option[value="remove"]').length == 0){
							selectOption.append('<option value="remove">Remove</option>');
							$(targetNode).closest('div.file-case').find('.upload-file.hidden').removeClass('hidden');
						}else{
							$(targetNode).closest('div.file-case').find('.upload-file').addClass('hidden');
						}
					});
				}
				
				var vr = [], valid = true;
				$('.file-list .file-table .file-case:visible .file-designation').each(function(){
					var val = $(this).find('select').val();
					if (val == '---'){
						valid = false;
					}else if (val=='remove'){
						// if selected value is remove. then dont validate
						//valid = true;
					}else if (vr.indexOf(val) >= 0){
						valid = false;
					}else{
						vr.push(val);
					}
				});
				$('.apply-house-style').addClass('disabled');
				
				/*** to display uncited resources, so it can either be removed or marked as uncited or a supplement ***/
				/*if ($('select[data-type="response"]').find('option[data-value]:not([data-defined])').length > 0){
					valid = false;
					$('.unidentified-files').remove();
					var unidentifiedFiles = $('<div class="row file-table unidentified-files"><p style="border-bottom:1px solid black;">Un-assigned files</p></div>');
					$('.apply-house-style').parent().before(unidentifiedFiles);
					var options = $('<select data-channel="components" data-topic="general" data-event="change" data-message="{\'funcToCall\': \'validateResources-test\'}"><option>---</option><option>Add as Figure block</option><option>Add as Table block</option><option>Add as Supplement</option><option>remove</option></select>');
					$('select[data-type="response"]').find('option[data-value]:not([data-defined])').each(function(){
						var clone = options.clone();
						//clone.append('<option>' + $(this).attr('data-value') + '</option>');
						var row = $('<div class="row"><div class="col s6" data-file-name="' + $(this).attr('file_name') + '">' + $(this).attr('file_name') + '</div><div class="col s6">' + clone[0].outerHTML + '</div></div>');
						unidentifiedFiles.append(row)
					})
				}*/
				if (valid){
					$('.apply-house-style').removeClass('disabled');
					var valid = $('<file_list>');
					$('.file-list .file-table .file-case:visible').each(function(){
						var clone = $(this).find('select:not(".file_label") option:selected');
						// if selected value is remove. then dont include this file(i.e:skip iteration)
						if($(clone).val() == 'remove') return;
						clone.removeAttr('data-value').removeAttr('value');
						var file = $('<file>');
						file.attr('data-float-id', $(this).attr('data-float-id'));
						for (var i = 0; i < clone[0].attributes.length; i++) {
							var attrib = clone[0].attributes[i];
							file.attr(attrib.name, attrib.value);
							if($(this).find('select.file_label').val() != undefined){
								file.html($(this).find('select.file_label').val());
							}
						}
						valid.append(file);
					});
				}
				return valid;
			},
			getFileDesignators: function(param, targetNode){
				/** display the list of file to the user to select its file type, with which structure content can be processed **/
				$('.la-container').fadeIn();
				var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.doi, 'process': 'get'};
				kriya.general.sendAPIRequest('getfiledesignators', parameters, function(res){
					$('.la-container').fadeOut();
					var popUp = $('[data-component="fileDesignator_edit"]');
					var response = $(res);
					popUp.find('[data-input-editable] .file-container .file-list .file-table').remove();
					popUp.find('[data-input-editable] .file-container .file-list').prepend('<div class="file-table">' + response.html() + '</div>');
					popUp.find('[data-input-editable] .file-container .file-table .file-case:not([file_extension=".zip"]) .file-designation').append('<span class="upload-file" data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'openReplaceFile\', \'param\': {\'component\': \'FIGURE_edit\'}}"><i class="material-icons">file_upload</i></span>');
					eventHandler.components.general.openPopUp('', popUp);
					$(popUp).find('.apply-house-style').addClass('hidden');
					$(popUp).find('.structure-content').removeClass('hidden');
					$('.file-list .file-case:visible').first().find('.moveUp').addClass('hidden')
					$('.file-list .file-case:visible').last().find('.moveDown').addClass('hidden')
					eventHandler.components.general.validateFileTypes();
				},function(res){
					//error
				});
			},
			validateFileTypes: function(param, targetNode){
				var msLength = 0;
				$('.file-list .file-table .file-case:visible').each(function(){
					$(this).attr('file_designation', $(this).find('select').val());
					if ($(this).find('select').val() == 'manuscript'){
						msLength++;
					}
				});
				$('.structure-content').addClass('disabled');
				if (msLength == 1){
					$('.structure-content').removeClass('disabled');
				}
			},
			viewFile: function(param, targetNode){
				var iframeHeight = $('[data-component="fileDesignator_edit"] .file-container').height() - $('[data-component="fileDesignator_edit"] .file-container .file-viewer').offset().top;
				var targetTitle = $(targetNode).clone();
				targetTitle.find('.re-order').remove();
				if ($(targetNode).closest('[data-href]').length > 0){
					$('[data-component="fileDesignator_edit"] .file-viewer').html('<h4 style="margin-top: 0px;text-align: center;">' + targetTitle.html() + '</h4><iframe style="width:100%;height:' + iframeHeight + 'px;" src="' + window.location.origin + $(targetNode).closest('[data-href]').attr('data-href') + '"></iframe>');
				}else if (targetNode.find('select option:selected').attr('data-href')){
					$('[data-component="fileDesignator_edit"] .file-viewer').html('<h4 style="margin-top: 0px;text-align: center;">' + targetTitle.html() + '</h4><iframe style="width:100%;height:' + iframeHeight + 'px;" src="' + window.location.origin + targetNode.find('select option:selected').attr('data-href') + '"></iframe>');
				}
			},
			objectMoveUp : function(param, targetNode){
				var component = targetNode.closest('.row');
				component.prev().before(component)
				$('.file-list .file-case').find('.moveUp,.moveDown').removeClass('hidden')
				$('.file-list .file-case:visible').first().find('.moveUp').addClass('hidden')
				$('.file-list .file-case:visible').last().find('.moveDown').addClass('hidden')
			},
			objectMoveDown : function(param, targetNode){
				var component = targetNode.closest('.row');
				component.next().after(component)
				$('.file-list .file-case').find('.moveUp,.moveDown').removeClass('hidden')
				$('.file-list .file-case:visible').first().find('.moveUp').addClass('hidden')
				$('.file-list .file-case:visible').last().find('.moveDown').addClass('hidden')
			},
		},
	};
	return eventHandler;
})(eventHandler || {});
