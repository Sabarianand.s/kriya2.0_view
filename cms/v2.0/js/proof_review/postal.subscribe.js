/* global postal, $ */

$( function() {
	// This gets you a handle to the default postal channel...
	// For grins, you can get a named channel instead like this:
	// var channel = postal.channel( 'DoctorWho' );
	/*var channel = postal.channel('content-menu');
	var subscription = channel.subscribe('menu.click', function(data, envelope) {
		//console.log(data);
		console.log(envelope);
	});
	
	var channel = postal.channel('menu');
	var subscription = channel.subscribe('*.clicked', function(data, envelope){
		
	});*/
	var subscription = postal.subscribe({
        channel: 'content-menu',
        topic: "*.click1",
        callback: function(data, envelope) {
			console.log(data, envelope);
            // `data` is the data published by the publisher.
            // `envelope` is a wrapper around the data & contains
            // metadata about the message like the channel, topic,
            // timestamp and any other data which might have been
            // added by the sender.
        }
    });
});
