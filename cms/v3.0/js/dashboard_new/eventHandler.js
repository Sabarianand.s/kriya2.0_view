var dataID;
var dataNum;
var dataCustomer,dataProject,dataStageName;
var articlesData;
var data = {};
data.info = [];
var pagefn;
var userData;

var tableToExcel = (function () {
	var uri = 'data:application/vnd.ms-excel;base64,',
		template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
	base64 = function (s) {
		return window.btoa(unescape(encodeURIComponent(s)))
	},
		format = function (s, c) {
			return s.replace(/{(\w+)}/g, function (m, p) {
				return c[p];
			})
		}
	return function (table, name) {
		if (!table.nodeType)
			table = document.getElementById(table)

		var ctx = {
			worksheet: name || 'Worksheet',
			table: table.innerHTML
		}
		// window.location.href.download = 'ad.xls'
		var blob = new Blob([format(template, ctx)]);
		var a = window.document.createElement('a');
		a.href = window.URL.createObjectURL(blob);
		a.download = name + '.xls';
		// Append anchor to body.
		document.body.appendChild(a);
		a.click();
		// Remove anchor from body
		document.body.removeChild(a);
		return false;
	}

})()

/**
 * eventHandler - this javascript holds all the functions required for Reference handling
 *					so that the functionalities can be turned on and off by just calling the required functions
 *					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
 */
var filterData = {};
var eventHandler = function () {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['mainMenu', 'components', 'dropdowns', 'bulkedit', 'common', 'flatplan'];
	//add or override the initial setting
	var initialize = function (params) {
		function extend(obj, ext) {
			if (obj === undefined) {
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function () {
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function () {
			/**
			 * attach a click event listner to the body tag
			 * if the target node has the special attribute 'data-channel',
			 * then we need to publish some data using the data in the attributes
			 */
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function () {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function (event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);
			//Hide more action dropdowns
			$('.moreActionsBtns:not(.hidden)').addClass('hidden');

			$(elements).each(function () {
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length) {
					targetNode = $(targetNodes[0]);
					if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
						return;
					}
					var message = eval('(' + targetNode.attr('data-message') + ')');
					var eveDetails = message[event.type];
					if (!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type) {
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic = targetNode.attr('data-topic');
					} else if (typeof (eveDetails) == "string" && typeof (message[eveDetails]) == "object") {
						eveDetails = message[eveDetails];
					} else if (!eveDetails || typeof (eveDetails) == "string") {
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic: eveDetails.topic + '.' + event.type,
						data: {
							message: eveDetails,
							event: event.type,
							target: targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function () {
			var subscribers = eventHandler.settings.subscribers;
			for (var i = 0; i < subscribers.length; i++) {
				initSubscribe(subscribers[i], subscribers[i] + '_subscription');
			}

			function initSubscribe(subscribeChannel, subscribeName) {
				if (!eventHandler.settings.subscriptions[subscribeName]) {
					eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
						channel: subscribeChannel,
						topic: "*.*",
						callback: function (data, envelope) {
							//console.log(data, envelope);
							if (data.message != '') {
								/*http://stackoverflow.com/a/3473699/3545167*/
								if (typeof (data.message) == "object") {
									var array = data.message;
								} else {
									var array = eval('(' + data.message + ')');
								}
								var functionName = array.funcToCall;
								var param = array.param;
								var topic = envelope.topic.split('.');
								//var fn = 'eventHandler.menu.edit.'+functionName;
								if (typeof (window[functionName]) == "function") {
									window[functionName](param, data.target);
								} else if (typeof (window['eventHandler'][envelope.channel][topic[0]]) == "object") {
									if (typeof (window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function') {
										window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
									}
								} else {
									console.log(functionName + ' is not defined in ' + topic[0]);
								}
							} else {
								console.log('Message is empty');
							}

							// `data` is the data published by the publisher.
							// `envelope` is a wrapper around the data & contains
							// metadata about the message like the channel, topic,
							// timestamp and any other data which might have been
							// added by the sender.
						}
					});
				}
			}
		},
		remove: function () {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};

	return eventHandler;
})(eventHandler || {});

/**
 *	Extend eventHandler function with event handling
 */
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;
	eventHandler.mainMenu = {
		toggle: {
			showManuscript: function (targetNode) {
				var param = {
					"customer": 'bmj',
					"project": 'project',
					"stageName": "stageName",
					"urlToPost": "getAssigned",
					"user": "userName",
					"version": "v2.0",
					"workflowStatus": "in-progress",
					"from": '0',
					"size": '500',
				};
				$('#articlesData,#pdfData').removeClass('d-none')
				$('#issueCarousel,#issueData,#issueList,#unAssinged').addClass('d-none')
				//$('#issueData,#unAssinged').addClass('d-none')
				$('.btn').removeClass('active')
				$('#articlesDataContent').html('')
				$('#articles').addClass('active')
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						data.info = respData.hits;
						articlesData = $('#articlesDataContent');
						pagefn = doT.template(document.getElementById('articlesTemplate').innerHTML, undefined, undefined);
						articlesData.append(pagefn(data));
					},
					error: function (respData) {
					}
				})
			},
			showUnassign: function (targetNode) {
				var param = {
					"customer": 'bmj',
					"project": 'project',
					"stageName": "stageName",
					"urlToPost": "getUnassigned",
					"user": "userName",
					"version": "v2.0",
					"workflowStatus": "in-progress",
					"from": '0',
					"size": '500',
					"userLevelStageAccess": "true",
					"optionforassigning": "true"
				};
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						$('.issuesort .articlecount span').append('All Articles (' +  respData.hits.length + ')')
						data.info = respData.hits;
						articlesData = $('#unAssingedContent');
						pagefn = doT.template(document.getElementById('unassignTemplate').innerHTML, undefined, undefined);
						articlesData.append(pagefn(data));
					},
					error: function (respData) {
					}
				})
			},
			showIssueManuscript: function (targetNode) {
				$('#articlesData,#pdfData,#unAssinged,#issueData,#issueList').addClass('d-none')
				$('#issueCarousel').removeClass('d-none')
				$('.btn').removeClass('active')
				$('#issues').addClass('active')
				var param={
					"customerName": 'bmj',
					"projectName": 'thoraxjnl'
				}
				$.ajax({
					type: "get",
					url: '/api/getissuemakeupdata',
					data: param,
					dataType: 'json',
					success: function (respData) {
						$('#issueCarouselContent').html('')
						data.info = respData.hits.hits.reverse();
						articlesData = $('#issueCarouselContent');
						pagefn = doT.template(document.getElementById('issueCarouselTemplate').innerHTML, undefined, undefined);
						articlesData.append(pagefn(data));
					},
					error: function (respData) {
					}
				})

			},
			showIssueList: function (targetNode) {
				$('#articlesData,#pdfData,#unAssinged,#issueData,#issueCarousel').addClass('d-none')
				$('#issueList').removeClass('d-none')
				$('.btn').removeClass('active')
				$(targetNode).addClass('active')
				var param={
					"customerName": 'bmj',
					"projectName": 'thoraxjnl'
				}
				$.ajax({
					type: "get",
					url: '/api/getissuemakeupdata',
					data: param,
					dataType: 'json',
					success: function (respData) {
						$('#issueListContent').html('')
						data.info = respData.hits.hits.reverse();
						articlesData = $('#issueListContent');
						pagefn = doT.template(document.getElementById('issuesListTemplate').innerHTML, undefined, undefined);
						articlesData.append(pagefn(data));
					},
					error: function (respData) {
					}
				})

			},
			displayPDF: function (targetNode,param) {
				var currentTab = '.articlesContainer';
				$('#issueData,#issueList,#unAssinged').addClass('d-none')
				$('#pdfData .noPDF').addClass('d-none')
				$('#pdfview').removeClass('d-none')
				$('#pdfData a').attr('href',param.param.link)
				$('#articlesDataContent div').removeClass('active')
				$(targetNode).parent().addClass('active')
				if (!param.param || !param.param.rightPanelCol) param.param.rightPanelCol = 4;
				if (param.param.rightPanelCol) {
					param.param.mainCol = 12 - param.param.rightPanelCol;
				}
				$(currentTab + '#articlesDataContent').addClass('col-' + param.param.mainCol).removeClass('col-12');
				$(currentTab + '#pdfData').addClass('col-' + param.param.rightPanelCol);
				$(currentTab + '#pdfData').find('[data-type]').addClass('hidden');
				if (param.param && param.param.target == 'pdfviewer') {
					$(currentTab + '#pdfData').find('[data-type*=" pdfviewer "]').removeClass('hidden');
					if (param.param && param.param.link) {
						$('#pdfData').find('#pdfview object').attr('data', param.param.link);
						$(currentTab + '#pdfins').hide();
						$(currentTab + '#pdfview').show();
					}
				}
				$(currentTab + '#pdfData').show();
				if (param.param && param.param.doi) {
					$(currentTab + '#pdfDoi').text(param.param.doi);
					console.log(param.param.doi)
				}
				$(currentTab + '#pdfData #pdfTab #download_pdf li,' + currentTab + '#pdfData #regionTab ul.dropdown-menu  li').unbind();
				$(currentTab + '#pdfData #pdfTab #download_pdf li,' + currentTab + '#pdfData #regionTab ul.dropdown-menu  li').on('click', function () {
					if ($(this).children('a').attr('data-href')) {
						window.open($(this).children('a').attr('data-href'), '_blank');
					}
				});
			},
			noPDf:function(target){
				$('#articlesDataContent div').removeClass('active')
				$(target).parent().addClass('active')
				$('#pdfData .noPDF').removeClass('d-none')
				$('#pdfData #pdfview').addClass('d-none')
			},
			moreActions: function (target) {
				$('#myModal').modal();
				dataID = $(target).attr('data-id');
				dataNum = $(target).attr('data-numb');
				dataCustomer = $(target).attr('data-customer');
				dataProject = $(target).attr('data-project');
				dataStageName = $(target).attr('data-stageName');
				$('.modal-header .modal-title').html('')
				$('.modal-header .modal-title').append('Kriya360 - ' + dataID)
				$('#email,#emailToggle,#resources,#workflow,#history,#notes').attr({ dataid: dataID, datanumb: dataNum, datacustomer: dataCustomer, dataproject: dataProject, dataStageName: dataStageName });
				$('#info').attr('data-id', dataID);
				$('#info').attr('data-numb', dataNum);
				$('#myModal tbody').html('');
				$('.Notifications #resources').addClass('d-none')
				$('.Notifications #emailToggle,#notes,#workflow,#email').removeClass('d-none')
				$('.toggle span').removeClass('active')
				$('.toggle #Notifications').addClass('active')
				var param = {
					"customer": 'bmj',
					"project": 'project',
					"stageName": "stageName",
					"urlToPost": "getAssigned",
					"user": "userName",
					"version": "v2.0",
					"workflowStatus": "in-progress",
					"from": '0',
					"size": '500',
				};
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						$('#popupArticle').html('')
						data.info = respData.hits;
						$('.article span').html('')
						$('.article span').append('Articles (' + respData.hits.length + ')')
						articlesData = $('#popupArticle');
						pagefn = doT.template(document.getElementById('popupTemplate').innerHTML, undefined, undefined);
						articlesData.append(pagefn(data));
					},
					error: function (respData) {
					}
				})
				eventHandler.mainMenu.toggle.email($('.Notifications #email')[0])

			},
			email:function(targetNode)
			{
				$('#myModal tbody').html('');
				$('#myModal #infotable,.filemanager,#articleHistory').addClass('d-none')
				$('#myModal .comments-area').removeClass("d-none")
				$('.Notifications span').removeClass('active')
				var activeNode = $(targetNode).attr('id')
				$('.Notifications').find('#'+activeNode).addClass('active')
    			$('.filemanager').find('.data').remove();
                $('#articleHistory table thead').html('');
                $('#articleHistory table tbody').html('');
                var dataType = $(targetNode).attr('data-rid');
				var parameters = {};
				parameters.customer = $(targetNode).attr('datacustomer');
				parameters.project = $(targetNode).attr('dataproject');
				parameters.doi = $(targetNode).attr('dataid');
				parameters.stageName = $(targetNode).attr('datastageName');
				var url = "/api/getdata?doi=#DOI#&project=#PROJECT#&customer=#CUSTOMER#&xpath=//article/production-notes";
				var notesURL = url.replace("#DOI#", parameters.doi).replace("#CUSTOMER#", parameters.customer).replace("#PROJECT#", parameters.project);
				notesURL = notesURL + '&bucketName=AIP';
                var modalDiv = $("#myModal");
				jQuery.ajax({
					type: "GET",
					url: notesURL,
					contentType: "application/xml; charset=utf-8",
					success: function (msg) {
                        $(modalDiv).find('.data-container').find('#comment-list').html('');
						if (msg && !msg.error) {
							var commentObj = $(msg);
							var notesDate = '', lastDate = '';
							var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
							var nl = $(commentObj).find('note').length - 1;
							$(commentObj).find('note').each(function (i, v) {
								var commentDiv = $('<li data-id="' + $(this).find('id').html() + '" class="comment"><div class="comment-wrapper"><div class="comment-info"><span class="copy-note-clipboard  pull-right" data-channel="mainMenu" title="Copy" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'copyNoteClipboard\', \'param\':this}">&nbsp;<i class="fa fa-files-o" aria-hidden="true">&nbsp;</i></span><span class="name">' + $(this).find('fullname').html() + '</span><span class="comment-time" data-original="' + moment($(this).find('created').html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '">' + moment($(this).find('created').html() + ' UTC').format('YYYY:MM:DD HH:mm:ss') + '</span><span class="comment-reply" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'replyComment\', \'param\':this}"><i class="fa fa-reply"/></span></div><div class="wrapper"><div class="content">' + $(this).find('content').html() + '</div></div></div></li>');
								commentDiv.find('*[data-class]').each(function () {
									$(this).attr('class', $(this).attr('data-class')).removeAttr('data-class');
								});
								commentDiv.find('.jsonresp').each(function () {
									$(this).addClass('toggle');
								});
								commentDiv.find('email-content').each(function () {
									$(this).find('> *').each(function () {
										var div = $('<div>');
										$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase());
										$(div).html($(this).html());
										$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
									});
									$(this).find(".mail-body p:contains('review_content/?key=')").each(function () {
										$(this).html($(this).html().replace(/(https?:\/\/[a-z\.]+\/review_content\/\?key=[a-z0-9]+)/, '<span class="email-link" data-href="$1">KRIYA-LINK</span>'))
									})
									var div = $('<div>');
									$(div).attr('class', $(this)[0].nodeName.toLocaleLowerCase()).addClass('toggle');
									$(div).html($(this).html());
									$(this)[0].parentNode.replaceChild(div[0], $(this)[0]);
								})
                            if ($(this).find('fullname').html() == 'Automation') {
									$(this).attr('type', 'automation');
								}
								if ($(this)[0].hasAttribute('type')) {
									var commentType = $(this).attr('type');
									if ($(modalDiv).find('#comment-tabs').find('[data-rid="' + commentType + '"]').length == 0) {
										$(modalDiv).find('#comment-tabs').append('<li data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'toggleProductionNotes\'}" data-rid="' + commentType + '"><i class="fa fa-comment"></i><br/>' + commentType + '</li>');
									}
									commentDiv.attr('data-type', commentType);
									commentDiv.find('.comment-reply').remove();
									$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="' + $(this).attr('type').toLocaleLowerCase() + '">' + $(this).attr('type').toLocaleUpperCase() + '</span>');
									if (commentType == 'support') {
										if (commentDiv.find('.issue-data .category-info .level').length > 0) {
											$(commentDiv).find('.comment-info').append('<span class="comment-label" data-type="support-level" data-level="' + commentDiv.find('.issue-data .category-info .level').text() + '" data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'changeSupportLevel\'}" data-rid="notes">' + commentDiv.find('.issue-data .category-info .level').text().toLocaleUpperCase() + '</span>');
										}
									}
								} else {
									if ($(modalDiv).find('#comment-tabs').find('[data-rid="notes"]').length == 0) {
										$(modalDiv).find('#comment-tabs').append('<li data-channel="components" data-topic="actionitems" data-event="click" data-message="{\'funcToCall\': \'toggleProductionNotes\'}" data-rid="notes"><i class="fa fa-comment"></i><br/>notes</li>');
									}
								}
                                if ($(this).find('parent').length > 0 && $('li.comment[data-id="' + $(this).find('parent').html() + '"]').length > 0) {
									var parentID = $(this).find('parent').html();
									commentDiv.addClass('reply').attr('data-parent-id', parentID).find('.comment-reply').remove();
									if ($('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment:not(.reply)').before(commentDiv)
									} else if ($('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').length > 0) {
										$('li.comment[data-id="' + parentID + '"]').next('.comment.reply[data-parent-id="' + parentID + '"]').after(commentDiv)
									} else {
										$('li.comment[data-id="' + parentID + '"]').after(commentDiv);
									}
								}
                                else {
									notesDate = $(this).find('created').html().replace(/\s.*$/, '');
									if (lastDate == '') {
										lastDate = notesDate
									}
									if (notesDate != lastDate) {
										var n = new Date(lastDate);
										var divider = '<p><span>&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span>&nbsp;</span></p>';
										$(modalDiv).find('#comment-list').prepend(divider);
									}
									lastDate = notesDate;
									$(modalDiv).find('#comment-list').prepend(commentDiv);
								}
                                if (i == nl) {
									var n = new Date(notesDate);
									var divider = '<p><span>&nbsp;</span>' + weekdays[n.getDay()] + ', ' + n.toDateString().replace(/^[a-z]+ /i, '') + '<span>&nbsp;</span></p>';
									$(modalDiv).find('#comment-list').prepend(divider);
								}

                            });
                            $('#comment-tabs').remove();
                            if(dataType == 'email' || dataType == 'workflow' || dataType =='notes'){
                                eventHandler.mainMenu.toggle.toggleWorkflow(targetNode)
                            }
                        }
                    },
                    error: function (respData) {
					}
                })
			},
			toggleWorkflow:function(targetNode){
				var dataType = $(targetNode).attr('data-rid');
                $('#myModal').find('#comment-tabs > li').removeClass('active');
				$(targetNode).addClass('active');
				$('#myModal').find('#comment-list > p.notes-description').remove();
				$('#myModal').find('#comment-list > li, #comment-list > p').addClass('d-none');
				if (dataType == 'all'){
					$('#myModal').find('#comment-list > li, #comment-list > p').removeClass('d-none');
				}else if (dataType == 'notes'){
					$('#myModal').find('#comment-list').prepend('<p class="notes-description"><span>&nbsp;</span>NOTES<span>&nbsp;</span></p>');
					$('#myModal').find('#comment-list > li:not([data-type])').removeClass('d-none');
				}else{
					$('#myModal').find('#comment-list').prepend('<p class="notes-description"><span>&nbsp;</span>' + dataType.toUpperCase() + '<span>&nbsp;</span></p>');
					$('#myModal').find('#comment-list > li[data-type="' + dataType + '"]').removeClass('d-none');
				}
			},
			workflow:function(targetNode){
				$('#myModal #infotable').removeClass('d-none')
				var activeNode = $(targetNode).attr('id')
				$('.Notifications span').removeClass('active')
				$('.Notifications').find('#'+activeNode).addClass('active')
				$('#myModal .filemanager,#articleHistory,.comments-area').addClass('d-none')
				$('.filemanager').find('.data').remove();
				$('#myModal').find('#comment-list > li, #comment-list > p').addClass('d-none');
                $('#articleHistory table thead').html('');
                $('#articleHistory table tbody').html('');
				var cardID = $(targetNode).attr('data-numb');
				var storeData = window["data"]["info"];
				var sourceData;
				if (storeData && storeData[cardID] && storeData[cardID]._source) {
					sourceData = storeData[cardID]._source;
				}
				if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var infoTable = '';
				$('#articleInfo .header-tag span').html(sourceData.doi);
				$('#articleInfo tbody').html('');
				for (var info of articleInfoDetails) {
					if (sourceData[info.attribute] == null) {
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>0</td></tr>'
					} else {
						var infoValue = '';
						if (info.name == 'Authors') {
							if (sourceData[info.attribute].name.length) {
								for (var author of sourceData[info.attribute].name) {
									infoValue += author['given-names'] + ' ' + author.surname + ', '
								}
								infoValue = infoValue.slice(0, -2);
							} else {
								infoValue = sourceData[info.attribute].name['given-names'] + ' ' + sourceData[info.attribute].name.surname
							}
						} else {
							infoValue = sourceData[info.attribute]
						}
						infoTable += '<tr><td>' + info.name + '</td><td>:</td><td>' + infoValue + '</td></tr>'
					}
				}
				$('#myModal #infotable tbody').html(infoTable);
			},
			resources:function(targetNode){
				$('.Notifications span').removeClass('active')
				var activeNode = $(targetNode).attr('id')
				$('.Notifications').find('#'+activeNode).addClass('active')
				$('#myModal .filemanager').removeClass('d-none')
				$('#myModal #infotable,#articleHistory,.comments-area').addClass('d-none')
				$('#myModal tbody').html('');
				$('#articleHistory table thead').html('');
				$('#articleHistory table tbody').html('');
				$('#myModal').find('#comment-list > li, #comment-list > p').addClass('d-none');
				var param={};
				param.customer = $(targetNode).attr('datacustomer');
				param.project = $(targetNode).attr('dataproject');
				param.doi = $(targetNode).attr('dataid');
				if (!param.doi || !param.customer || !param.project) {
							return;
						}
				var parameters = 'customer=' + param.customer + '&project=' + param.project + '&doi=' + param.doi + '&process=new';
						$.ajax({
							type: "GET",
							url: "/api/listbucket/?" + parameters,
							success: function (data) {
								$('.filemanager').find('.data').remove();
								$('.filemanager').append(data);
							},
							error: function (err) {
								console.log(err);
								$('.la-container').fadeOut();
							}
						});
			},
			history:function(targetNode)
			{
				$('#myModal #articleHistory').removeClass('d-none')
				$('#myModal #infotable,.filemanager,.comments-area').addClass('d-none')
				$('#myModal #infotable tbody').html('');
				$('.Notifications span').removeClass('active')
				var activeNode = $(targetNode).attr('id')
				$('.Notifications').find('#'+activeNode).addClass('active')
                $('#articleHistory table thead').html('');
                $('#articleHistory table tbody').html('');
                $('#myModal').find('#comment-list > li, #comment-list > p').addClass('d-none');
                $('.filemanager').find('.data').remove();
                var cardID = $(targetNode).attr('datanumb');
				var storeData = window["data"]["info"];
				var sourceData;
				if (storeData && storeData[cardID] && storeData[cardID]._source) {
					sourceData = storeData[cardID]._source;
				}
                if (sourceData == undefined || !sourceData || !sourceData.stage) {
					return;
				}
				var stages = sourceData.stage;
				var collapseRow = '', hideCompletedRow = '';
				var hideRowId = 0;
                for (var s = 0, sl = $(stages).length; s < sl; s++) {
					var stage = stages[s] ? stages[s] : stages;
					var doi = sourceData.id.replace('.xml', '').replace('\.', '');
					var stageRow = '';
					$('#historyDoi').text(sourceData.id.replace('.xml', ''));
					if (stage.status != 'in-progress' && stage.status != 'waiting') {
						var tempSlaStartDate = stage['sla-start-date'] ? moment(stage['sla-start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempSlaEndDate = stage['sla-end-date'] ? moment(stage['sla-end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaStartDate = stage['planned-start-date'] ? moment(stage['planned-start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var tempPlaEndDate = stage['planned-end-date'] ? moment(stage['planned-end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') : '';
						var stageDuration = ((stage['stageduration-days'] != undefined) ? stage['stageduration-days'] : '');
						var stageAssignedName = (stage.assigned && stage.assigned.to) ? stage.assigned.to.replace(/,.*/g, '') : 'Unassigned';
						if (hideRepeatedStage.default.includes(stage.name)) {
							collapseRow = '<tr style="display:none" class="hideRow' + hideRowId + ' stages"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td></tr>' + collapseRow;
							continue;
						} else {
							if (stages.length != s + 1 && stages[s + 1].status == 'completed' && hideRepeatedStage.default.includes(stages[s + 1].name)) {
								collapseRow = '<tr style="display:none" class="hideRow' + hideRowId + ' stages" ><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td></tr>' + collapseRow;
								continue;
							} else {
								if (collapseRow) {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td><i class="fa fa-plus-circle" data-toggle="collapse" onclick = "eventHandler.mainMenu.toggle.collapseHistoryRow(this,\'hideRow' + hideRowId + '\')" aria-expanded="true" aria-controls="collapseOne"></i></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td></tr>'
								} else {
									hideCompletedRow = '<tr class="stages" data-stage-name="' + stage.name + '"><td></td><td class="stage-name">' + stage.name + '</td><td>' + stageAssignedName + '</td><td>' + moment(stage['start-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td><td>' + moment(stage['end-date'] + ' UTC').format('MMM DD, YYYY HH:mm:ss') + '</td></tr>'
								}
							}
						}
					}
					if (collapseRow || hideCompletedRow) {
						var hideRow = '';

						if (hideCompletedRow) {
							hideRow += hideCompletedRow;
						}
						if (collapseRow) {
							hideRow += collapseRow;
						}
						stageRow = $(hideRow);
						collapseRow = hideCompletedRow = '';
						$('#articleHistory table tbody').prepend(stageRow);
					}
					hideRowId++;
					$('#articleHistory table tbody').prepend(stageRow);
				}
				$('#articleHistory table').prepend('<thead><tr class="stages"><th/><th>Stage Name</th><th>Assigned to</th><th>Start Date</th><th>End Date</th></tr></thead>')
			},
			modalBox:function(targetNode)
			{
				if($(targetNode).text() == 'Notifications')
				{
					$('.toggle span').removeClass('active')
					$('.Notifications #emailToggle,#notes,#workflow,#email').removeClass('d-none')
					$('.Notifications #resources').addClass('d-none')
					$('#articleHistory table thead,tbody').html('');
					$('#myModal #infotable tbody').html('');
					$('.filemanager').find('.data').remove();
					$(targetNode).addClass('active')
					eventHandler.mainMenu.toggle.email($('.Notifications #email')[0])
				}
				else if($(targetNode).text() == 'Assets'){
					$('.toggle span').removeClass('active')
					$('.Notifications #resources,#info,#history').removeClass('d-none')
					$('.Notifications #emailToggle,#notes,#workflow,#email').addClass('d-none')
					$(targetNode).addClass('active')
					eventHandler.mainMenu.toggle.resources($('.Notifications #resources')[0])
				}
				else if($(targetNode).text() == 'Info'){
					$('.toggle span').removeClass('active')
					$('.Notifications #resources,#emailToggle,#notes,#workflow,#email').addClass('d-none')
					$(targetNode).addClass('active')
					eventHandler.mainMenu.toggle.workflow($('#info')[0])
				}
				else if($(targetNode).text() == 'History'){
					$('.toggle span').removeClass('active')
					$('.Notifications #resources,#emailToggle,#notes,#workflow,#email').addClass('d-none')
					$(targetNode).addClass('active')
					eventHandler.mainMenu.toggle.history($('#history')[0])
				}
			},
			collapseHistoryRow: function (node, buttonClass) {
				$('.' + buttonClass).slideToggle(0);
				if ($(node).hasClass("fa-plus-circle")) {
					$(node).addClass("fa-minus-circle").removeClass('fa-plus-circle')
				} else {
					$(node).addClass("fa-plus-circle").removeClass('fa-minus-circle')
				}
			},
			issueData:function(targetNode)
			{
				 var param = {"issueId": targetNode.issueid,
				"projectName": targetNode.project,
				"customer": targetNode.customer,
				"type": "journal"}
				article={}
				article = param
				data.info = article;
				articlesData = $('#issuesCardData');
				$('#issuesCardData').html('')
				pagefn = doT.template(document.getElementById('issuesCardTemplate').innerHTML, undefined, undefined);
				articlesData.append(pagefn(data));
				eventHandler.flatplan.action.getMetaXML(param,targetNode)
				$('#issueData,#unAssinged').removeClass('d-none')
				$('#issueCarousel').addClass('d-none')
			},
			copyNoteClipboard: function (clipButton) {
				//Added by Anuraja for Notes-copy to clipboard function based on - http://jsfiddle.net/jdhenckel/km7prgv4/3
				var $temp = $("<div id='clipBoardHtml' style='display:none'>");
				$temp.html($(clipButton).closest('li.comment .comment-wrapper').find('.wrapper .content').html());
				$("body").append($temp);
				var str = document.getElementById('clipBoardHtml').innerHTML;
				function listener(e) {
					e.clipboardData.setData("text/html", str);
					e.clipboardData.setData("text/plain", str);
					e.preventDefault();
				}
				document.addEventListener("copy", listener);
				document.execCommand("copy");
				document.removeEventListener("copy", listener);
				$temp.remove();
				var noticationInfo = {};
				noticationInfo.notify = {};
				noticationInfo.notify.notifyTitle = 'Copy to clipboard';
				noticationInfo.notify.notifyText = 'Copied to clipboard';
				noticationInfo.notify.notifyType = 'info';
				PNotify.removeAll();
				eventHandler.common.functions.displayNotification(noticationInfo);
			},
		}
	}
	eventHandler.common = {
		functions:{
			displayNotification: function (param) {
				if (param.notify) {
					if (!param.hideNotify) PNotify.removeAll();
					if (param.notify.confirmNotify) {
						new PNotify({
							title: param.notify.notifyTitle,
							text: param.notify.notifyText,
							type: param.notify.notifyType,
							functionToCall: forceLogOutFunction[param.notify.functionToCall],
							hide: false,
							parameters: param.parameter,
							icon: 'fa fa-question-circle',
							confirm: {
								confirm: true,
								buttons: [{
									text: 'Yes',
									click: function (param) {
										eval(param.options.functionToCall)(param.options.parameters);
										PNotify.removeAll();
									}
								},
								{
									text: 'No',
									click: function () {
										PNotify.removeAll();
									}
								}]
							}
						});
					} else {
						new PNotify({
							title: param.notify.notifyTitle,
							text: param.notify.notifyText,
							type: param.notify.notifyType,
							hide: false,
						});
					}
				} else {
					new PNotify({
						title: 'Error',
						text: 'Unable to process',
						type: 'error',
						hide: false
					});
				}
			},
		conformationModal: function (type,targetNode) {
			if ($(targetNode).parentsUntil('li').find('.actionBtns .fa.fa-flag').length) {
				$('#blkupdate').find('.text').text('Article is in Fast-Track. Are you sure you want to put it on Hold?');
				$('#blkupdate').find('#conform').attr('onclick', 'eventHandler.components.actionitems.openmodal(\'' + type + '\',\'' + $(targetNode).attr('test') + '\')');
				$('#blkupdate').modal();
				return;
			} else {
				eventHandler.common.functions.openmodal(type, targetNode);
			}
		},
		openmodal: function (type, targetNode) {
			console.log(targetNode);
			if (typeof (targetNode) == 'string') {
				targetNode = $('#manuscriptsDataContent').find('[test="' + targetNode + '"][title="Hold"]');
				$('#blkupdate').modal('hide');
			} else {
				if ($(targetNode).attr('class') != undefined && $(targetNode).attr('class').match('disabled')) {
					return;
				}
			}
			var modal = '#commonmodal';
			$(modal + ' .commonModalHeader').text();
			$(modal + ' .save').removeAttr('data-message');
			$(modal + ' .commentbox').addClass('hidden');
			$(modal + ' .dropdown').addClass('hidden');
			var msCard = $(targetNode).closest('.msCard');
			var param = {};
			// param.cardID = $(targetNode).closest('.msCard').attr('data-id');
			// param.parentId = $(targetNode).closest('.msCard').attr('data-dashboardview');
			// // param.modal = modal;
			// if(targetNode.parent().hasClass('assignment'))
			// param.assignment = true;
			// console.log(param.cardID);
			var doi = $(targetNode).attr('data-id')
			var header = '';
			if (popups[type].title != undefined) {
				header = popups[type].title;
			}
			var descriptiontext = popups[type].descriptiontext;
			// if (popups[type].function != undefined) {
			// 	$(modal + ' .save').attr("data-message", "{'funcToCall': 'saveComment', 'param':{ 'funcToPost':'" + popups[type].function + "',  'currentNode':'" + JSON.stringify(param) + "'}}");
			// }
			if (header.match('{doi}')) {
				header = header.replace('{doi}', doi);
			}
			if (popups[type].commentbox) {
				$(modal + ' .commentbox').removeClass('hidden');
			}
			if (popups[type].dropdown) {
				$(modal + ' .dropdown').removeClass('hidden');
				options = "<option value='' disabled='disabled' selected='selected'>CHOOSE OPTION</option>";
				popups[type].options.forEach(function (element) {
					console.log(element);
					options += "<option value='" + element + "' >" + element.toUpperCase() + "</option>";
				});
				$('#commonmodal .dropdown').find('select').html(options);
			}
			if (popups[type].confirmation) { }
			$(modal + ' .commonModalHeader').text(header);
			$(modal).modal();
		},
		saveHold: function (param, comments) {
			var modal = $(param).closest('.modal');
			var hold = {};
			hold.type = $(modal).find('select').val();
			if ((hold.type == "" || hold.type == null || hold.type == undefined) && !$(modal).find('.dropdown').hasClass('hidden')) {
				$(modal).find('#modalError').html("Please select an option.");
				return;
			} else {
				var holdIcontooltip = "";
				if (hold.comment != null && hold.comment != "") {
					holdIcontooltip = 'data-tooltip="' + hold.comment + '"';
				}
				var parameters = {};
				var notify = {};
				// parameters.holdComment = $(modal).find('div.commentArea').text();
				if (hold.type != null && hold.type != undefined) {
					// parameters.holdComment = hold.type + ': ' + parameters.holdComment;
				}
				parameters.cardID = cardID;
				parameters.parentId = parentId;
				parameters.customer = $("" + parentId + " [data-id='" + cardID + "']").attr('data-customer');
				parameters.project = $("" + parentId + " [data-id='" + cardID + "']").attr('data-project');
				parameters.doi = $("" + parentId + " [data-id='" + cardID + "']").attr('data-doi');
				parameters.holdFrom = $(""+parentId+" [data-id='" + cardID + "']").attr('data-store-variable');
				var currStageName = $("" + parentId + " [data-id='" + cardID + "']").find('.msStageName').text();

				var d = new Date();
				var currDate = d.getUTCFullYear() + '-' + ("0" + (d.getUTCMonth() + 1)).slice(-2) + '-' + ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) + ':' + ("0" + d.getUTCMinutes()).slice(-2) + ':' + ("0" + d.getUTCSeconds()).slice(-2);
				if (currStageName == 'Hold') {
					parameters.releaseHold = 'true';
					notify.notifyTitle = "Remove Hold";
					notify.successText = parameters.doi + " has been removed from hold";
					notify.errorText = "Error while remove hold for " + parameters.doi;
				} else {
					var userName = '';
					if (userData && userData.kuser) {
						if (userData.kuser.kuname && userData.kuser.kuname.first) userName += userData.kuser.kuname.first
						if (userData.kuser.kuname && userData.kuser.kuname.last) userName += ' ' + userData.kuser.kuname.last
						if (userData.kuser.kuemail && userData.kuser.kuname.last) userName += ', ' + userData.kuser.kuemail
					}
					var userName = userData.kuser.kuname.first + ' ' + userData.kuser.kuname.last + ', ' + userData.kuser.kuemail;
					if (comments) {
						comments = comments.replace(/:\s(.+)/, ' ($1)').replace(/(\:\s)$/, '')
						parameters.holdComment = 'Hold for: ' + comments + '; Hold by: ' + userName;
					}
					parameters.stageName = 'Hold';
					notify.notifyTitle = "Add Hold";
					notify.successText = parameters.doi + " has been put on hold";
					notify.errorText = "Error while hold article " + parameters.doi;
				}
				parameters.notify = notify;
				console.log(parameters);
				$(modal).find('select').prop('selectedIndex', 0); //Sets the first option as selected
				$(modal).find('#modalError').html("");
				$(modal).find('div .commentArea').text('');
				eventHandler.common.functions.sendAPIRequest('addstage', parameters, "POST", eventHandler.dropdowns.article.getArticles, eventHandler.common.functions.displayNotification)
				$(modal).modal('hide');
			}
		}
	}
}
	return eventHandler;
})(eventHandler || {});



window.addEventListener("resize", function () {
	$('.windowHeight').css('height', (window.innerHeight - ($('.windowHeight').offset().top) - $('#footerContainer').height()) + 'px');
	/*if (!$('#rightPanelContainer').is(":hidden")) {
		$('#rightPanelContainer .tabContent:not(.hide)').css('max-height', (window.innerHeight - ($('#rightPanelContainer .tabContent:not(.hide)').offset().top) - $('#footerContainer').height()) + 'px');
		$('#manuscriptsData #tocContentDiv #sectionGroup').css('max-height', (window.innerHeight - ($('#manuscriptsData #tocContentDiv #sectionGroup').offset().top) - $('#footerContainer').height()) + 'px');
	}*/
});

$(document).ready(function() {
	userData = JSON.parse($('#userDetails').attr('data'))
	$('body').on('click', '.email-content .mail-subject', function () {
		$(this).closest('.email-content').toggleClass('toggle');
	});
	$('body').on('click', '.note-head', function () {
		$(this).closest('.jsonresp').toggleClass('toggle');
	});
	$('body').on('click', '.jsonresp:not(:has("div.note-head"))', function () {
		$(this).closest('.jsonresp').toggleClass('toggle');
	});

});


