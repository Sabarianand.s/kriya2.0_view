﻿var config = {
    "bookmarks":4,
    "pubIdentifier":"10.1136", 
    "defaultUnits":"pt",
    "baseLeading":11,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "landscapeFloatOnFirstPage":false,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY",
    "minStackSpace":18,
    "placeStyle":"Sandwich",
    "figCaptionMinLines": 0,
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.15,
    "applyTableBorderColor":'WHITE',
    "minNoLinesOnPag":3,
    "pageSize":{
        "width":595.276,
        "height":793.701
    },
    "exportPDFpreset":{
        "print":{
            "crop_marks":true, 
            "bleed_marks":false, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":8.504
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            },
         "cover":{
            "print":"BMJ_cover-Print",
            "online":"BMJ_cover-Online"
            },
        "toc":{
            "print":"BMJ_toc-Print",
            "online":"BMJ_toc-Online"
            },
        "advert":{
            "print":{
                "crop_marks":true, 
                "bleed_marks":false, 
                "registration_marks":true, 
                "colour_bars":false, 
                "page_information":false, 
                "offset":8.504
                },
            "online":{
                "crop_marks":false, 
                "bleed_marks":false, 
                "registration_marks":false, 
                "colour_bars":false, 
                "page_information":false,
                "offset":0
                }
            }
        },
    "watermark":{
        "Pre-editing":true, 
        "Typesetter QA":true, 
        "Author proof":true, 
        "Author revision":true, 
        "Revises":true
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":12.5,
      "right":12.5,
      "gutter":12.5
    },
    "geoBoundsVerso":[48,42,747.837678026575,553.275590551181
    ],
    "geoBoundsRecto":[48,637.275590551181,747.837890625,1148.55118110236
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1",
    "DEFAULT":"LAYOUT1",
    "BTEETCRS":"LAYOUT2",
    "ATAGE":"LAYOUT2",
    "IECE":"LAYOUT2",
    "COMMENTARY":"LAYOUT5",
    "EDITORIAL":"LAYOUT5",
    "OBITUARY":"LAYOUT5",
    "LETTER":"LAYOUT2",
    "PYSY":"LAYOUT2",
    "GLEYMEHS":"LAYOUT3",
    "IECE":"LAYOUT3",
    "BTEETCRS":"LAYOUT4",
    "THANK":"LAYOUT6",
    "MISCELLANEOUS":"LAYOUT7"
    },
    "pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
                  {"width":197.552, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":197.552, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                "columnDetails":[
                  {"width":246.587, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":246.587, "height":699.83,"floatsCited":false,"gutter":18.101}
                ],
                 "openerPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":145.672,
                "outside":42
                },
            "otherPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                }
            },
         "LAYOUT2":{
                "openerPageColumnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                "columnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                 "openerPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                },
            "otherPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                }
             },
         "LAYOUT3":{
            "openerPageColumnDetails":[
                  {"width":197.552, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":197.552, "height":699.83,"floatsCited":false,"gutter":18.101}
                ],
                "columnDetails":[
                  {"width":246.587, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":246.587, "height":699.83,"floatsCited":false,"gutter":18.101}
                ],
                 "openerPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                },
            "otherPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                }
             },
         "LAYOUT4":{
                "openerPageColumnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                "columnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                 "openerPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                },
            "otherPageMargin":{
                "top":48,
                "bottom":45.843,
                "inside":42,
                "outside":42
                }
             },
         "LAYOUT5":{
                "openerPageColumnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                "columnDetails":[
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5},
                  {"width":162.092, "height":699.83,"floatsCited":false,"gutter":12.5}
                ],
                 "openerPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":42,
                    "outside":42
                },
                "otherPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":42,
                    "outside":42
                }
             },
         "LAYOUT6":{
                "openerPageColumnDetails":[
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15}
                ],
                "columnDetails":[
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":0},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15},
                  {"width":116.569, "height":699.83,"floatsCited":false,"gutter":15}
                ],
                 "openerPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":42,
                    "outside":42
                },
                "otherPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":42,
                    "outside":42
                }
             },
         "LAYOUT7":{
                "openerPageColumnDetails":[
                  {"width":360, "height":699.83,"floatsCited":false,"gutter":0}
                ],
                "columnDetails":[
                  {"width":360, "height":699.83,"floatsCited":false,"gutter":0}
                ],
                 "openerPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":117.638,
                    "outside":117.638
                },
                "otherPageMargin":{
                    "top":48,
                    "bottom":45.843,
                    "inside":117.638,
                    "outside":117.638
                }
             }
         },
      "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":340.85, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsPara,jrnlAbsHead,jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,jrnlHead5",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],
    
    "docFontsList":[
       {
          "ClassGarmnd BT": {
            "Roman": {
                "Symbol (T1)": "Regular"
              },
            "Italic": {
                "Symbol (T1)": "Italic"
              },
            "Bold": {
                "Symbol (T1)": "Bold"
              },
            "Bold Italic": {
                "Symbol (T1)": "Bold"
              }
          }
      },
        {
          "Frutiger LT Pro": {
            "47 Light Condensed": {
                "Symbol (T1)": "Regular"
              },
                "48 Light Condensed Italic": {
                    "Symbol (T1)": "Italic"
              },
                "57 Condensed": {
                    "Symbol (T1)": "Regular"
            },
                "58 Condensed Italic": {
                    "Symbol (T1)": "Italic"
            },
                "67 Bold Condensed": {
                    "Symbol (T1)": "Bold"
            }
          }
        },
          {
          "Symbol (T1)": {
            "Regular": {
                "Arial Unicode MS": "Regular"
              },
                "Italic": {
                    "Arial Unicode MS": "Regular"
              },
                "Bold": {
                    "Arial Unicode MS": "Regular"
            }
          }
        }
    ],
    
   "replFonts":[
            {"fontFamily":"ClassGarmnd BT", "fontStyle":"Roman"},
            {"fontFamily":"ClassGarmnd BT", "fontStyle":"Italic"},
            {"fontFamily":"ClassGarmnd BT", "fontStyle":"Bold"},
            {"fontFamily":"ClassGarmnd BT", "fontStyle":"Bold Italic"},
            {"fontFamily":"Frutiger LT Pro", "fontStyle":"47 Light Condensed"},
            {"fontFamily":"Frutiger LT Pro", "fontStyle":"48 Light Condensed Italic"},
            {"fontFamily":"Frutiger LT Pro", "fontStyle":"57 Condensed"},
            {"fontFamily":"Frutiger LT Pro", "fontStyle":"58 Condensed Italic"},
            {"fontFamily":"Frutiger LT Pro", "fontStyle":"67 Bold Condensed"},
            {"fontFamily":"Symbol", "fontStyle":"Medium"},
            {"fontFamily":"Symbol", "fontStyle":"Italic"},
            {"fontFamily":"Symbol", "fontStyle":"Bold"}
    ],    
    
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCRRH']", "frame-name":"CRRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCLRH']", "frame-name":"CLRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRH']", "frame-name":"RRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRH']", "frame-name":"LRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlMetaInfo']", "frame-name":"METAINFO", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlRelatedInfo']", "frame-name":"RELATED_ARTICLE_INFO", "action":"move", "styleOverride":null}
       ]       
     },
     "colorDetails":{
       "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "EDITORIAL":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
        "OBITUARY":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
         "REVIEW":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
         "MISCELLANEOUS":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
         "INNOVATIONS":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
         "THANK":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,55"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,10"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"138,140,142"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"230,231,232"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            },
         "ATAGE":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"100,90,10,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"37,64,143"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            }
        },
     "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "7.5px",
                    "font-family" : "'frutigerltpro-condensed', frutigerltpro-condensed"
                },
                "fontPath"  : "frutigerltpro-condensed/frutigerltpro-condensed.otf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'frutigerltpro-boldcn_0',frutigerltpro-boldcn_0",
                    "font-size" : "7.5px",
                    "line-height" : "9.5px",
                    "padding" : "0.5px 0px 0.5px 0px",
                },
                "align" : "left",
                "valign": "top"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'frutigerltpro-condensed',frutigerltpro-condensed",
                    "font-size" : "7.5px",
                    "line-height" : "9.5px",
                    "padding" : "0px 0px 0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            }
        },
     "equation" : {
          "default" : {
              "page":{
                  "size" : "511.276pt"
              },
              "font" : {
                  "size"    : "9pt",
                  "ledding" : "11pt",
                  "path"    : "/ClassGarmnd_BT_Font/",
                     "ext"     : "ttf",
                  "bold"    : "CLSGARAB",
                  "italic"  : "CLSGARAI",
                  "bold_italic":"CLSGARBI",
                  "main"    : "CLSGARAN"
              }
           },
           "jrnlTblBlock" : {
              "page":{
                  "size" : "511.276pt"
              },
              "font" : {
                "size"    : "7.5pt",
                "ledding" : "9.5pt",
                "path"    : "/frutigerltpro-condensed/",
                "ext"     : "otf",
                  "bold"    : "frutigerltpro-boldcn_0",
                  "italic"  : "frutigerltpro-condensedita",
                  "bold-italic":"frutigerltpro-boldcnita",
                  "main":"frutigerltpro-condensed",
              }
           }
        },
    "stubColObj":{
        "bottom": {
            "METAINFO": false,
            "CROSSMARK": false,
            "RELATED_ARTICLE_INFO": false
        },
        "top": {
            "STUB_COLUMN": true,
            "STUB_STMT": false
        }
    }
}
this.config = config;