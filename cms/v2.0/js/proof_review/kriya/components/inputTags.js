/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    // 1. CONFIGURATION
    var config = {
        //container: '[data-component="InputBox"]',
		//container: '.jrnlHead1',
		container: '.InputBox',
		editable: true
    };

    // 2. COMPONENT OBJECT
    kriya.inputTags = {

        init: function (elm, component, child) {
			this.container = elm;
			this.component = component;
			this.child = child;
            addInputs(component);
			
			/**
			*# function which adds chips/input tags to a component 
			*# based on the param read from the attributes of the component
			*# selector: xpath selector which ahs to be converted as input-tags
			**/
			function addInputs (component) {
				if (kriya.inputTags.child && kriya.inputTags.child.getAttribute('data-type') == 'inputTags'){
					var inputTagger = $(kriya.inputTags.child);
				}else if ($('[data-type="inputTag"][data-component="'+component.name+'"]').length > 0){
					var inputTagger = $('[data-type="inputTag"][data-component="'+component.name+'"]');
				}
				if (inputTagger){	
					var template = false;
					if (inputTagger.find('*[data-input]').length > 0){
						var inputNode = inputTagger.find('*[data-input]')[0];
						if (inputTagger.find('*[data-template]').length > 0){
							template = inputTagger.find('*[data-template]')[0];
						}
					}else{
						var inputNode = inputTagger[0]
					}
						inputNode.innerHTML = '';
					// Add the actual source from the container to the input container
					if (typeof(inputTagger[0].getAttribute('data-selector')) != 'undefined'){
						inputTagger.removeClass('hidden');
						var sourceChilds = $(kriya.inputTags.container).find('.'+component.source);
						var selector = kriya.validateXpath(inputTagger, kriya.inputTags.container)
						var sourceChilds = kriya.xpath(selector, kriya.inputTags.container);

						var tags = [];
						if (typeof(component) == "object"){
							//get all childrens and match the class with the source and move it to the input tag
							for (var i = 0, dl = sourceChilds.length; i < dl; i++) {
								sourceChild = sourceChilds[i];
								if ($(sourceChild).closest('.del,*[data-track="del"]').length > 0){
									//do nohting
								}else if (inputTagger.find('.kriya-chips').length > 0){
									tags.push(sourceChild.outerHTML);
								}
								else if (template){
									var cloneNode = template.cloneNode(true);
									cloneNode.removeAttribute('data-template');
									cloneNode.innerHTML = sourceChild.innerHTML + cloneNode.innerHTML;
									inputNode.appendChild(cloneNode);
								}else{
									var cloneNode = document.createElement('span');
									cloneNode.setAttribute('class', 'input-tags chip');
									cloneNode.innerHTML = sourceChild.innerHTML + '<i class="close material-icons">close</i>';
									inputNode.appendChild(cloneNode);
								}
							}// End of for each childrens
						}
						inputTagger.removeClass('hidden');
						// trying to use materilize chip for tags
						if (inputTagger.find('.kriya-chips').length > 0){
							var suggestions = [], freeInput = true;
							if (inputTagger[0].getAttribute('data-suggestion') != undefined){
								sugSelectors = kriya.xpath(inputTagger[0].getAttribute('data-suggestion'), kriya.inputTags.container[0]);
								//suggestions = kriya.general.getHTML(sugSelectors);
								//Get the outer html of the data nodes and remove the ids - jagan
								$(sugSelectors).each(function(){
									var thisClone = $(this).clone(true);
									thisClone.removeAttr('id');
									thisClone.find('[id]').removeAttr('id');
									suggestions.push(thisClone[0].outerHTML);
								});

								if (! inputTagger[0].hasAttribute('data-free-input')){
									freeInput = false;
								}
							}
							kriya.inputTags.kriyaInputTags(tags, suggestions, $(inputNode), true, freeInput);
							if ($('#contentContainer').attr('data-state') == 'read-only'){
								$(inputNode).find('*[contenteditable]').removeAttr('contenteditable');
								$(inputNode).find('.removeKriyaTag').remove();
							}
						}
					}//End of if the current component has source to include
				}
			}
        },
		
		kriyaInputTags: function( constructedArray, suggestion, nodeToBeTagged, defaultInputValue, freeInputBool, maxTags){
			freeInputBool = (!freeInputBool) ? false : true;
			maxTags = (maxTags == undefined) ? '' : maxTags;
			tagsInput = constructedArray.join(',');
			if( defaultInputValue ){
				var input = $('<input type="text" value="' + tagsInput + '" />');
			}else {
				var input = $('<input type="text" />');
			}
			var inputTags = $('<div class="kriya-tagsinput" contenteditable="true">');
			$.each( constructedArray, function( key, value ) {
				if ( suggestion && typeof (suggestion == 'string' || suggestion == 'object')){
					$(inputTags).append('<span class="tags" contenteditable="false">'+value+'<span class="removeKriyaTag"></span></span>&nbsp;')
				}else{
					$(inputTags).append('<span class="tags">'+value+'<span class="removeKriyaTag"></span></span>&nbsp;')
				}
			})
			if($(nodeToBeTagged.find('[data-freetext]')).length==0){
				nodeToBeTagged.html(inputTags);
			}
			nodeToBeTagged.closest('*[data-type]').removeClass('hidden');
			$('ul.kriya-tags-suggestions').remove();
			if ( suggestion ){
				var items = [];
				if (typeof suggestion == 'string' ){
					$.getJSON( suggestion, function( data ) {
						$.each( data, function( key, val ) {
							items.push( "<li>" + val.trim() + "</li>" );
						});
					});
				}
				else if (typeof suggestion == 'object' ){
					$.each( suggestion, function( key, val ) {
						items.push( "<li>" + val.trim() + "</li>" );
					});
				}
				$( "<ul/>", {
					"class": "kriya-tags-suggestions hidden",
					html: items.join( "" )
				}).appendTo( "body" );
					
				nodeToBeTagged.find('.kriya-tagsinput').kriyaTags({
				  confirmKeys: [13, 44, 188, 186],
				  freeinput: freeInputBool,
				  max: maxTags,
				});
			}else{
				nodeToBeTagged.find('.kriya-tagsinput').kriyaTags({
				  confirmKeys: [13, 44, 188, 186],
				  freeinput: freeInputBool,
				  max: maxTags,
				});
			}
		},
		
		saveKriyaTagInput: function(inputNode){
			/* save tagged input's to their respective block */
			var type = $(inputNode).closest('*[data-type]').attr('data-type');
			if (type.match(/kwd|subject|otherType/)){
				if (type == 'otherType') type = $(inputNode).closest('*[data-sub-type]').attr('data-sub-type');
				var classVal = $(inputNode).closest('*[data-type]').attr('data-parent');
				var kwdGroup = $('.currentEditingNode').find('.' + classVal);
				kwdGroup.html('');
				$(inputNode).closest('[data-input-type="tags"]').find('.kriya-tagsinput .tags').each(function(){
					var kwd = $(this).clone();
					kwd.find('.removeKriyaTag').remove();
					kwdGroup.append('<span class="' + type + '">' + $(kwd).html() + '</span>');
				});
			}
		},
		
		changeAuthorFormat: function(elementToCheckAttr, elementToGetData){
			if(elementToCheckAttr == undefined && elementToGetData == undefined){
				return false;
			}
			if(elementToCheckAttr.length > 0 && elementToGetData.length > 0){
				var authorFormat = '',abbrvAuthorName=false;
				if($(elementToCheckAttr).attr('data-author-format')){
					authorFormat = $(elementToCheckAttr).attr('data-author-format');
				}
				if($(elementToCheckAttr).attr('data-abbrv-name')){
					abbrvAuthorName = $(elementToCheckAttr).attr('data-abbrv-name');
				}
				
				if(authorFormat && authorFormat!='' && ($(elementToGetData).find('.jrnlGivenName').length > 0 && $(elementToGetData).find('.jrnlSurName').length > 0)){
					var selectedText='';var fullText = '';
					var clonedNode = $(elementToGetData).clone();
					var givenName = $(elementToGetData).find('.jrnlGivenName').html();
					
					var childNodes = $(elementToGetData).children();
					childNodes.each(function(){
						fullText = fullText + ' ' + $(this).text();
					});
					if(fullText && fullText != ''){
						fullText = fullText.replace(/^\s|\s$/,'');
						if(fullText != ''){
							$(elementToGetData).attr('data-fulltext',fullText);
						}
					}
					if(abbrvAuthorName){
						givenName = givenName.replace(/([a-z\s\u00A0]|\&nbsp;)+/,'');
						$(clonedNode).find('.jrnlGivenName').html(givenName);
					}
					
					if(authorFormat == "SG"){
						selectedText = $(clonedNode).find('.jrnlSurName')[0].outerHTML + ' ' + $(clonedNode).find('.jrnlGivenName')[0].outerHTML;
						//if cloned nodes has author then get the selectedText as author
						if($(clonedNode).find('.jrnlAuthor').length > 0){
							$(clonedNode).find('.jrnlAuthor').html(selectedText);
							selectedText = $(clonedNode).find('.jrnlAuthor')[0].outerHTML;
						}
					}else if(authorFormat == "GS"){
						selectedText = $(clonedNode).find('.jrnlGivenName')[0].outerHTML + ' ' + $(clonedNode).find('.jrnlSurName')[0].outerHTML;
						//if cloned nodes has author then get the selectedText as author
						if($(clonedNode).find('.jrnlAuthor').length > 0){
							$(clonedNode).find('.jrnlAuthor').html(selectedText);
							selectedText = $(clonedNode).find('.jrnlAuthor')[0].outerHTML;
						}
					}else{
						selectedText = $(clonedNode).html();
					}
					
					return selectedText;
				}else{
					return $(elementToGetData).html();
				}
			}
			
		},
		
		detach: function () {
			
		},

    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));




$.fn.extend({
  kriyaTags: function(args) {
    return this.each(function() {
		var node = this;
		$('.kriya-tagsinput').off( "keydown" );
		$('.kriya-tagsinput').off( "keyup" );
		$('body').off( "click", "ul.kriya-tags-suggestions li" );
		$('.kriya-tagsinput').off( "click", '.removeKriyaTag' );
		if (args.confirmKeys){
			kriya.inputTags.confirmKeys = args.confirmKeys.join('|');
		}
		if (args.freeinput){
			kriya.inputTags.freeinput = args.freeinput;
		}else{
			kriya.inputTags.freeinput = false;
		}
		if (args.max){
			kriya.inputTags.max = args.max;
		}else{//#23-kriya.inputTags.max this value not reset anywere-kirankumar
			kriya.inputTags.max = undefined;
		}
		$('.kriya-tagsinput').keydown(function(event){
			//#295 - All | Components | Components popup | All Browsers - Prabakaran.A (prabakaran.a@focusite.com)
			//#154-it restricting all ctrl events in research organisum popup
			//if(event.ctrlKey && event.keyCode == 86){
				//return false;
			//}
			//End of #295
			
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '27'){
				$('ul.kriya-tags-suggestions, ul.kriya-tags-suggestions li').addClass('hidden');
			}
			var regexkeys = new RegExp('^' + kriya.inputTags.confirmKeys + '$', 'gi');
			if(regexkeys.test(keycode)){
				event.stopPropagation();
				event.stopImmediatePropagation();
				event.preventDefault();
				var target = event.target;
				var sel = rangy.getSelection()
				var range = sel.getRangeAt(0)
				prev = range.endContainer.previousSibling;//get previous sibling of the current range in order to check the range falls behind any citations
				startContainer = range.endContainer;
				if (startContainer.parentElement.nodeName != 'SPAN'){
					end = range.endOffset;
					var textContentLength = range.endContainer.textContent.length;
					var getPrev = true;
					while (getPrev) {
						if (!prev) {
							var parentNode = startContainer.parentNode;
							if ((parentNode) && (parentNode.nodeType == 1) && (parentNode.attributes.class) && (parentNode.attributes.class.textContent.match(/^kriya-tagsinput/gi))){//check if its a direct child
								getPrev = false;
							}else{
								prev = startContainer.parentElement.previousSibling;
							}
						}
							if ((prev) && (!((prev.nodeType == 1) && (prev.attributes.class) && (prev.attributes.class.textContent.match(/^tags$/gi))))){
								textContentLength += prev.textContent.length;
								startContainer = prev;
								prev = startContainer.previousSibling;
							}else if (!prev) {
								parentNode = startContainer.parentElement;
								 if ((parentNode) && (parentNode.nodeType == 1) && ((!parentNode.attributes.class) || (!(parentNode.attributes.class) || (parentNode.attributes.class.textContent.match(/^kriya-tagsinput/gi))))){
									startContainer = startContainer.parentElement;
								 }else{
									getPrev = false;
								 }
							}else{
								getPrev = false;
							}
					}

					range.setStart(startContainer, 0);
					range.setEnd(range.endContainer, end);
					//range = sel.getRangeAt(0);
					var selectedText = range.toHtml();
					if (!kriya.inputTags.freeinput){
						var notInList = true;
						$('ul.kriya-tags-suggestions li').each(function(){
							if (selectedText.replace(/^\&nbsp;/,'').toLocaleLowerCase() == $(this).text().toLocaleLowerCase()){
								notInList = false;
								selectedText = $(this).text();
							}
						});
					}
					if (notInList){
						return;
					}
					range.deleteContents();
					var el = document.createElement("span");
					if ($('ul.kriya-tags-suggestions').length > 0){
						el.innerHTML = '&nbsp;<span class="tags" contenteditable="false">' + selectedText.replace(/^\&nbsp;/,'') + '<span class="removeKriyaTag"></span></span>&nbsp;';
					}else{
						el.innerHTML = '&nbsp;<span class="tags">' + selectedText.replace(/^\&nbsp;/,'') + '<span class="removeKriyaTag"></span></span>&nbsp;';
					}
					var frag = document.createDocumentFragment(),
						node, lastNode;
					while ((node = el.firstChild)) {
						lastNode = frag.appendChild(node);
					}
					range.insertNode(frag);
					// Preserve the selection
					if (lastNode) {
						range = range.cloneRange();
						range.setStartAfter(lastNode);
						range.collapse(false);
						sel.removeAllRanges();
						sel.addRange(range);
					}
					if ($(lastNode).closest('.kriya-tagsinput').find('.tags:contains("'+selectedText.replace(/^\&nbsp;/,'')+'")').length > 1){
						lastNode.previousElementSibling.remove()
						lastNode.remove()
					}else{
						kriya.inputTags.saveKriyaTagInput(lastNode);
					}
					$('ul.kriya-tags-suggestions, ul.kriya-tags-suggestions li').addClass('hidden');
				}
			}
			if((keycode == '8')|| (keycode == '46')){
				var target = event.target;
				var sel = rangy.getSelection()
				var range = sel.getRangeAt(0)
				var parentNode = range.endContainer.parentElement;//get previous sibling of the current range in order to check the range falls behind any citations
				var prev = range.endContainer.previousElementSibling;
				if ((parentNode) && (parentNode.nodeType == 1) && (parentNode.attributes.class) && (parentNode.attributes.class.textContent.match(/^removeKriyaTag$/gi))){
					event.stopPropagation();
					event.stopImmediatePropagation();
					event.preventDefault();
					var parentParentNode = $(parentNode).parent()
					if (kriya.inputTags.freeinput){
						$(parentNode).remove()
						parentParentNode.before($(parentParentNode).contents())
						$(parentParentNode).remove()
					}
					$(parentParentNode).remove()
					kriya.inputTags.saveKriyaTagInput($('.kriya-tagsinput:visible'));
				}
				else if ((parentNode) && (parentNode.nodeType == 1) && (parentNode.attributes.class) && (parentNode.attributes.class.textContent.match(/^tags$/gi))){
					if (kriya.inputTags.freeinput){
						event.stopPropagation();
						event.stopImmediatePropagation();
						event.preventDefault();
						$(parentNode).find('.removeKriyaTag').remove();
						$(parentNode).before($(parentNode).contents())
					}
					$(parentNode).remove();
					kriya.inputTags.saveKriyaTagInput($('.kriya-tagsinput:visible'));
					//range.setStartAfter(range.endContainer)
				}// Block event if its content is only "non breakable space or space"
				else if($(target).html().match(/^([\s\u00A0]|\&nbsp;)+$/i)){
					event.stopPropagation();
					event.stopImmediatePropagation();
					event.preventDefault();
				}
			}
		});
		$('.kriya-tagsinput').keyup(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			var target = event.target;
				var target = event.target;
				var sel = rangy.getSelection()
				var range = sel.getRangeAt(0)
				//#154-when copy past range.endOffset value not set-kirankumar
				if(range.endOffset==0){
					range.endOffset=range.endContainer.textContent.length;
				}
				prev = range.endContainer.previousSibling;//get previous sibling of the current range in order to check the range falls behind any citations
				startContainer = range.endContainer;
				//$('ul.kriya-tags-suggestions, ul.kriya-tags-suggestions li').addClass('hidden')
				var validkey = 
					(keycode > 47 && keycode < 58)   || // number keys
					keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
					keycode == 8  || // backspace
					(keycode > 64 && keycode < 91)   || // letter keys
					(keycode > 95 && keycode < 112)  || // numpad keys
					(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
					(keycode > 218 && keycode < 223);
				if ((startContainer.parentElement.nodeName != 'SPAN') && (validkey)){
					end = range.endOffset;
					var textContentLength = range.endContainer.textContent.length;
					var getPrev = true;
					while (getPrev) {
						if ((startContainer) && (startContainer.nodeType == 1) && (startContainer.attributes.class) && (startContainer.attributes.class.textContent.match(/^kriya-tagsinput/gi))){
								getPrev = false;
						}
						if ((!prev) && (getPrev)){
							var parentNode = startContainer.parentNode;
							if ((parentNode) && (parentNode.nodeType == 1) && (parentNode.attributes.class) && (parentNode.attributes.class.textContent.match(/^kriya-tagsinput/gi))){//check if its a direct child
								getPrev = false;
							}else{
								prev = startContainer.parentElement.previousSibling;
							}
						}
							if ((getPrev) && (prev) && (!((prev.nodeType == 1) && (prev.attributes.class) && (prev.attributes.class.textContent.match(/^tags$/gi))))){
								textContentLength += prev.textContent.length;
								startContainer = prev;
								prev = startContainer.previousSibling;
							}else if ((!prev) && (getPrev)){
								parentNode = startContainer.parentElement;
								 if ((parentNode) && (parentNode.nodeType == 1) && ((!parentNode.attributes.class) || (!(parentNode.attributes.class) || (parentNode.attributes.class.textContent.match(/^kriya-tagsinput/gi))))){
									startContainer = startContainer.parentElement;
								 }else{
									getPrev = false;
								 }
							}else{
								getPrev = false;
							}
					}

					range.setStart(startContainer, 0);
					range.setEnd(range.endContainer, end);
					var rangeText = range.toString();
					var rngtop = range.getBoundingClientRect().bottom;
					var rngtop = $(this).offset().top + $(this).height();
					var rngleft = range.getBoundingClientRect().left;
					if(rngleft==0){
						rngleft=$(this).offset().left;
					}
					$('ul.kriya-tags-suggestions').css({'top': rngtop+'px', 'left': rngleft+'px'});
					rangeText = rangeText.replace('.', '\.');
					rangeText = rangeText.replace(/^[\s\u00A0]+|[\s\u00A0]+$/g, '');
					var regex = new RegExp(rangeText , 'gi');
					$('ul.kriya-tags-suggestions, ul.kriya-tags-suggestions li').addClass('hidden')
					if (typeof(kriya.inputTags.max) !="undefined" && $(range.startContainer).closest('.kriya-tagsinput').find('.tags').length >= kriya.inputTags.max) {
					}else{
						$('ul.kriya-tags-suggestions, ul.kriya-tags-suggestions li').removeClass('hidden')
						$('ul.kriya-tags-suggestions li').each(function(){
							var val = $(this).text();
						if ((regex.test(val)) || (val.match(regex))){
								$(this).removeClass('hidden');
							}else{
								$(this).addClass('hidden');
							}
						})
					}
					kriyarange = range;
					kriyasel = sel;
					range.collapse(false);
				}
				if (kriya.inputTags.freeinput){
					if((keycode == '8')|| (keycode == '46')){
						$(this).find('.tags').each(function(){
							if ($(this).find('.removeKriyaTag').length == 0){
								$(this).before($(this).contents())
								$(this).remove();
							}
						});
					}
				}else{
					if((keycode == '8')|| (keycode == '46')){
						if ($(this)[0].lastChild.tagName === 'SPAN'){
							$(this).find('br').remove();
							$(this).append('<br>');
						}else{
							kriya.inputTags.saveKriyaTagInput($('.kriya-tagsinput:visible'));
						}
					}
				}
		});
		$('body').on({
			click: function(event){
				var selectedText = $(this).html(); var dataFullText = '';
				if ($(kriyarange.startContainer).find('.tags').length >= kriya.inputTags.max) {
					$('ul.kriya-tags-suggestions').addClass('hidden');
					return false;
				}
				if($(kriyarange.startContainer) && $(kriyarange.startContainer).closest('*[data-author-format]').length > 0){
					selectedText = kriya.inputTags.changeAuthorFormat($(kriyarange.startContainer).closest('*[data-author-format]'),$(this)); 
				}
				if($(this).attr('data-fulltext')){
					dataFullText = 'data-fulltext="'+$(this).attr('data-fulltext')+'"';
				}
				var el = document.createElement("span");
//for giving editable permision to research organisum-kirankumar
				if($(kriyarange.startContainer.parentElement).closest('.kriya-chips').attr('data-inputtag-editable')=="true"){
					el.innerHTML = '&nbsp;<span class="tags" contenteditable="true" '+dataFullText+'>' + selectedText.replace(/^\&nbsp;/,'') + '<span class="removeKriyaTag"></span></span>&nbsp;';
				}else{
					el.innerHTML = '&nbsp;<span class="tags" contenteditable="false" '+dataFullText+'>' + selectedText.replace(/^\&nbsp;/,'') + '<span class="removeKriyaTag"></span></span>&nbsp;';
				}
				var frag = document.createDocumentFragment(),
					node, lastNode;
				while ((node = el.firstChild)) {
					lastNode = frag.appendChild(node);
				}
				if ($(kriyarange.startContainer) && $(kriyarange.startContainer).hasClass('kriya-tagsinput')){
					$(kriyarange.startContainer).find('br').remove();
					kriyarange.startContainer.appendChild(frag)
				}else{
					kriyarange.selectNode(kriyarange.startContainer);
					kriyarange.deleteContents();
					var existingData = [];
					$(kriyarange.startContainer).find(".tags").each(function(i,val){ 				    
						existingData.push($(val)[0].textContent.trim().replace(/ /g,''));
					});
					var currentData = $(frag)[0].textContent.trim().replace(/ /g,'');
					if(existingData.indexOf(currentData)>=0){
						$('ul.kriya-tags-suggestions').addClass('hidden');
						event.preventDefault();
					}else{
						kriyarange.insertNode(frag);
					}					
				}
				// Preserve the selection
				if (lastNode) {
					kriyarange = kriyarange.cloneRange();
					kriyarange.setStartAfter(lastNode);
					kriyarange.collapse(false);
					kriyasel.removeAllRanges();
					kriyasel.addRange(kriyarange);
				}
				if ($(lastNode).closest('.kriya-tagsinput').find('.tags:contains("'+$(this).text().replace(/^\&nbsp;/,'')+'")').length > 1){
					lastNode.previousElementSibling.remove()
					lastNode.remove()
				}else{
					kriya.inputTags.saveKriyaTagInput(lastNode);
				}
				$('ul.kriya-tags-suggestions').addClass('hidden');
			},
		},"ul.kriya-tags-suggestions li");
		$('.kriya-tagsinput').on({
			click: function(event){
				var lastNode = $(this).parent().parent();
				var prev = $(this).parent()[0].previousSibling;
				//remove previous textnode with space - jai
				if (prev && prev.nodeType == 3 && /^[\s\u00A0]+$/.test(prev.nodeValue)){
					$(prev).remove();
				}
				$(this).parent().remove();
				kriya.inputTags.saveKriyaTagInput(lastNode);
			},
		},".removeKriyaTag");
		return args;
    });
  }
});
