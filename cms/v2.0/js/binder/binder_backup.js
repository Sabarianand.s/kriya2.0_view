var binder = {
    meta: {
        "journal-name._text": {
            "component": "text",
            "label": "Journal Name: "
        },
        "sss":{
            "data" : "sabari anand"
        },
        "journal-abbrev._text": {
            "component": "text",
            "label": "Journal Abbreviation: "
        },
        "volume._text": {
            "component": "editable",
            "label": "Volume: "
        },
        "issue._text": {
            "component": "text",
            "label": "Issue: "
        },
        "day._text": {
            "component": "text",
            "label": "Publication Day: "
        },
        "month._text": {
            "component": "text",
            "label": "Publication Month: "
        },
        "year._text": {
            "component": "text",
            "label": "Publication Year: "
        },
        "page-count1": {
            "component": "text",
            "label": "Page Count: "
        }
    },
    "content":{

    }
}

function showMessage(obj){
    console.log(obj);
}

function resetData(){
    $('#listProject, #listIssue').remove();
    $('#detailsContainer').find(">div").addClass('hide');
    $('#search').val('');
    $('.header .icons').addClass('hide');
    $('#tocContainer .bodyDiv').html('')
}

/**
 * general get method to fetch JSON data using input url
 * @param {*} obj.url - url to fetch data from, obj.objectName - data to return from response
 */
function getDataFromURL(obj){
    return new Promise(function(resolve, reject){
        jQuery.ajax({
            type: "GET",
            url: obj.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg[obj.objectName]){
                    resolve({'code': 200, 'data': msg[obj.objectName]});
                }
                else{
                    reject({'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url});
                }
            },
            error: function(xhr, errorType, exception) {
                console.log(errorType, exception);
                reject({'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url});
            }
        });
    })
}

function getCustomerList(){
    return new Promise(function(resolve, reject){
        resetData();

        var obj = {'url': '/api/customers', 'objectName': 'customer'};
        getDataFromURL(obj)
        .then(function(customer){
            if ((typeof(customer) == 'undefined') || (typeof(customer.data) == 'undefined') || (customer.data.length == 0)){
                reject({'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url});
                return false;
            }
            var cData = customer.data
            var cLen = cData.length;
            var cHTML = '';
            for (var cIndex = 0; cIndex < cLen; cIndex++){
                cHTML += '<p class="customer folder" data-customer="' + cData[cIndex].name + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}" ><span class="cSpan"><span class="icon-folder-o"/>&#x00A0;' + cData[cIndex].name + '</span></p>';
            }
            $('#cList').html(cHTML);
            resolve(true);
        })
        .catch(function(err){
            reject(err)
        })
    })
}

function getProjectList(cName){
    return new Promise(function(resolve, reject){
        resetData();
        $('#explorerNav').append('<li class="customer" id="listProject" data-customer="' + cName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + cName +'</span>&#x00A0;</li>');
        var obj = {'url': '/api/projects?customerName=' + cName, 'objectName': 'project'};
        getDataFromURL(obj)
        .then(function(project){
            if ((typeof(project) == 'undefined') || (typeof(project.data) == 'undefined') || (project.data.length == 0)){
                reject({'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url});
                return false;
            }
            var pData = project.data
            var pLen = pData.length;
            var pHTML = '';
            for (var pIndex = 0; pIndex < pLen; pIndex++){
                pHTML += '<p class="project folder" data-customer="' + cName + '" data-project="' + pData[pIndex].name + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}" ><span class="cSpan"><span class="icon-folder-o"/>&#x00A0;' + pData[pIndex].name + '</span></p>';
            }
            $('#cList').html(pHTML);
            resolve(true);
        })
        .catch(function(err){
            reject(err)
        })
    })
}

function getIssueList(cName, pName){
    return new Promise(function(resolve, reject){
        resetData();
        $('#explorerNav')
        .append('<li class="customer" id="listProject" data-customer="' + cName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + cName +'</span>&#x00A0;</li>')
        .append('<li class="customer" id="listIssue" data-customer="' + cName + '" data-project="' + pName + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'toggle\'}">/&#x00A0;<span>' + pName +'</span>&#x00A0;</li>');
        var obj = {'url': '/api/getissuelist?client=' + cName + '&jrnlName=' + pName + '&returnType=list', 'objectName': 'list'};
        getDataFromURL(obj)
        .then(function(project){
            if ((typeof(project) == 'undefined') || (typeof(project.data) == 'undefined') || (project.data.length == 0)){
                reject({'code': 500, 'message': 'Something went wrong when fetching data from ' + obj.url});
                return false;
            }
            var pData = project.data
            var pLen = pData.length;
            var pHTML = '';
            for (var pIndex = 0; pIndex < pLen; pIndex++){
                pHTML += '<p class="list folder" data-customer="' + cName + '" data-project="' + pName + '" data-file="' + pData[pIndex] + '" data-channel="explorer" data-topic="customer" data-event="click" data-message="{\'funcToCall\': \'load\'}" ><span class="cSpan"><span class="icon-file-text2"/>&#x00A0;' + pData[pIndex] + '</span></p>';
            }
            $('#cList').html(pHTML);
            resolve(true);
        })
        .catch(function(err){
            reject(err)
        })
    })
}

function getIssueData(cName, pName, fileName){
    return new Promise(function(resolve, reject){
        // hide the right panel and enable it after populating data
        $('#detailsContainer').find(">div").addClass('hide');
        var obj = {'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&fileName=' + fileName, 'objectName': 'list'};
        getDataFromURL(obj)
        .then(function(issueData){
            issueData = JSON.parse(issueData.data);
            $('#tocContainer').data('binder', issueData);
            $('.icons').removeClass('hide');
            populateMetaContainer();
            populateTOC();
            resolve(true);
        })
        .catch(function(err){
            reject(err)
        })
    })
}

function populateMetaContainer(){
    $('#metaHistory').removeClass('hide');
    $('#manuscriptData').addClass('hide');
    var meta = $('#tocContainer').data('binder')['container-xml'].meta;
    if ((typeof(meta) == 'undefined') || (typeof(binder.meta) == 'undefined')){
        return false;
    }
    populateData($('#metadata'), 'meta', $('#tocContainer').data('binder')['container-xml'].meta);
}
function populateData(container, binderKey, data){
    var config = binder[binderKey];
    var containerHTML = '';
    Object.keys(config).forEach(function(key){
        var keyArr = key.split('.');
        var myData = data;
        keyArr.forEach(function(key){
            if (myData[key]){
                myData = myData[key];
            }
            else{
                myData = '';
            }
        })
        if (myData){
            var compObj = {
                type: config[key].component,
                data: myData
            }
            containerHTML += '<div class="row stripes"><div class="col s4 m4 l4 label">' + config[key].label + '</div><div class="col s8 m8 l8 data">' + getComponent(compObj) + '</div></div>';
        }
    });
    if (containerHTML){
        $(container).html(containerHTML);
    }
}

function getComponent(compObj){
    switch(compObj.type) {
        case "n":
            break;
        case "editable":
            return '<div contenteditable="true">' + compObj.data + '</div>';
            break;
        default:
            return '<div>' + compObj.data + '</div>';
    }
}

function populateTOC(){
    var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
    var prevSection = '';
    var container = $('#tocContainer .bodyDiv');
    container.html('<div class="row sectionHeader"><div class="col s6 m6 l6">Section Name</div><div class="col s6 m6 l6">Page Range</div></div>');
    var colStart = '<div class="col s6 m6 l6">';
    var closeDiv = '</div>';
    var containerHTML = '';
    var sectionIndex = 1;
    var tempID = -1;
    contentHash = {}
    content.forEach(function(entry){
        var id = entry._attributes.id;
        var type = entry._attributes.type;
        contentHash[++tempID] = content[tempID];
        entry['tid'] = tempID;
        if (/^(cover|editorial\-board|table\-of\-contents)$/gi.test(entry._attributes.type )){
            addSection(container, entry, 'noChild', sectionIndex++, tempID);
        }
        else {
            if (prevSection != entry._attributes.section){
                addSection(container, entry, 'hasChild', sectionIndex++);
            }
            prevSection = entry._attributes.section;
            containerHTML = '<div data-c-id="' + tempID + '" class="row sectionDataChild hide" data-sec="sec' + (sectionIndex - 1) + '" data-channel="toc" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'panel\'}">';
            containerHTML += '<div class="col s3 m3 l3">' + entry._attributes.id + '</div>';
            var titleText = 'Title Text';
            if (/^(blank|advert)$/gi.test(entry._attributes.type)){
                titleText = entry._attributes.type.toUpperCase();
            }
            containerHTML += '<div class="col s7 m7 l7 titleText">' + titleText + '</div>';
            containerHTML += '<div class="col s2 m2 l2">' + '<span class="rangeStart">' + entry._attributes.fpage + '</span>';
            if (entry._attributes.fpage != entry._attributes.lpage){
                containerHTML += '&#x2013;<span class="rangeEnd">' + entry._attributes.lpage + '</span>';
                $('#tocContainer .bodyDiv .sectionData:last .rangeEnd').text(entry._attributes.lpage);
            }
            containerHTML += closeDiv;
            containerHTML += closeDiv;
            container.append(containerHTML);
        }
    });
}
function addSection(container, entry, className, secIndex, cid){
    var colStart = '<div class="col s6 m6 l6">';
    var closeDiv = '</div>';
    var containerHTML = '';
    var cid = typeof(cid) != 'undefined' ? ' data-c-id="' + cid + '"' : '';
    // if it does not have a child, then clicking the section should bring up the details on the right else open the child nodes
    var funcName = className == 'noChild' ? 'panel' : 'section';
    containerHTML = '<div' + cid + ' id="sec' + secIndex + '" class="sectionData ' + className + ' ' + entry._attributes.type + '" data-section="' + entry._attributes.section + '" data-channel="toc" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'' + funcName + '\'}">';
    containerHTML += '<div class="row">';
    containerHTML += colStart + entry._attributes.section + closeDiv;
    containerHTML += '<div class="col s4 m4 l4">' + '<span class="rangeStart">' + entry._attributes.fpage + '</span>'
    if (entry._attributes.fpage != entry._attributes.lpage){
        containerHTML += '&#x2013;<span class="rangeEnd">' + entry._attributes.lpage + '</span>';
    }
    containerHTML += closeDiv;
    containerHTML += '<div class="col s2 m2 l2"><span class="right icons icon-bin"/><span class="right icons icon-add"/></div>';
    containerHTML += closeDiv;
    containerHTML += closeDiv;
    container.append(containerHTML);
}
$(document).ready(function(){
	//Initialize the poste and subscriber
	eventHandler.publishers.add();
	eventHandler.subscribers.add();
	window.addEventListener("resize", function(){
		$('#explorerContainer, #tocContainer, .vertDots, #metaHistory').css('height', (window.innerHeight - $('#explorerContainer').offset().top - $('#footerContainer').height() - 10 )+ 'px');
		$('.bodyDiv, .tabContent').css('height', (window.innerHeight - $('.bodyDiv').offset().top - $('#footerContainer').height() - 10 )+ 'px');
	});
    getCustomerList()
    .then(function(data){
        $('#explorerContainer, #tocContainer, .vertDots, #metaHistory').css('height', (window.innerHeight - $('#explorerContainer').offset().top - $('#footerContainer').height() )+ 'px');
		$('.bodyDiv, .tabContent').css('height', (window.innerHeight - $('.bodyDiv').offset().top - $('#footerContainer').height() - 10 )+ 'px');
    })
    .catch(function(err){
        console.log(err);
    })
})