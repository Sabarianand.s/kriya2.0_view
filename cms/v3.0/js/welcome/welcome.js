/*
main JS to load scripts related to app.

*/
var md5 = function(value) {
    return CryptoJS.MD5(value).toString();
}

var kriya = {};
function invalidData(param, error){
	if(param && param === 'reset'){
		jQuery('#reset-password #resetusername').removeClass('valid').val('');
		jQuery('#reset-password #resetnewpassword').removeClass('valid').val('');
		jQuery('#reset-password #resetoldpassword').removeClass('valid').val('');
		jQuery('#reset-password #resetconfirmpassword').removeClass('valid').val('');
		jQuery('#reset-password .error-text').removeClass('hide-element ');
		if(error) jQuery('.error-text').html(error);
		jQuery("#reset-password .login-form .reset a[href='#']").html('RESET').removeClass('disabled');
	}else{
	jQuery('.error-text').removeClass('hide-element ');
	jQuery('#username').val('');
	jQuery('#password').val('');
	jQuery('#username').removeClass('valid');
	jQuery('#password').removeClass('valid');
		jQuery(".login-form .login a[href='#']").html('login').removeClass('disabled');
	}
}

function inputIsEmpty(elmArray){
	var count, elmLength = elmArray.length;
	var isEmpty = false;
	for(count=0; count< elmLength; count++){
		var value = elmArray[count].val();
		if(value === ""){
			elmArray[count].addClass('invalid');
			elmArray[count].next().addClass('active');
			isEmpty =true;
		}else{
			elmArray[count].removeClass('invalid');
		}
	}
	return isEmpty;
}

function authenticate(isConfirm, onSuccess, onError, param){
	if(!param) param = {};
	if(!param.userName) param.userName = jQuery('#username');
	if(!param.password) param.password = jQuery('#password');
	var data =  [param.userName, param.password]
	if( !inputIsEmpty( data ) ){
		var userName    = param.userName.val();
		var passwordVal = param.password.val();
		//hashUser = md5(userName);
		//hashPass = md5(passwordVal);
		//hashUser = userName;
		//hashPass = passwordVal;
		var param = {
			'user': userName,
			'pass': passwordVal
		}

		if(isConfirm){
			param.ctoken = kriya.ctoken;
		}
		jQuery.ajax({
			type: "POST",
			url: "/api/authenticate",
			data: param,
			success: function (msg) {
				if(msg && msg.username){
					$('#username').html(msg.username);
					$('.notifymsg').removeClass('hide');
				}else{
					$('.notifymsg').addClass('hide');
				}
				if(msg && msg.changepassword){
					openChangePassword()
					return false;
				}
				if (msg && msg.redirectToPage){
					store.remove('kriyaLogoutFlag');
					if(onSuccess && typeof(onSuccess) == "function"){
						onSuccess(msg);
					}

					if(opener && opener.keeplive && opener.keeplive.handleVisibilityChange){
						$(opener.document).find('#lock-screen').hide();
						opener.keeplive.handleVisibilityChange();
						window.close();
					}else{
						window.location = msg.redirectToPage;
					}
				}else if(msg && msg.status == 2 && msg.token){
					$('.login-form .login-col-2').addClass('hide');
					$('.login-form .confirmationPanel').removeClass('hide');
					kriya.ctoken = msg.token;
				}
				else{
				   invalidData();
				}
			},
			error: function(xhr, errorType, exception) {
				if(onError && typeof(onError) == "function"){
					onError();
				}
			   invalidData();
			}
		});
	}
}
function openChangePassword(param){
	$('#password').val('');
	if(!param || !param.displayUser){
		$('#username').closest('.input-field').addClass('hide');
	}
	$('#login-page .resetpassword').addClass('hide');
	$('#login-page .forgetpassword').addClass('hide')
	$('label').removeClass('active');
	$('label[for="password"]').html('New Password');
	$('.changepassword').removeClass('hide');
	$('#confirmpassword').closest('.row').removeClass('hide');
	$('.login a[href="#"]').closest('.input-field').addClass('hide');
	$('.changepassword').closest('.input-field').removeClass('hide');
	$('.input-field .error-text').addClass('hide');
	$('.changepassword a[href="#"]').html('Change Password');
	$('.changepassword a[href="#"]').removeClass('disabled');
}
function changepassword(userName,newPasswordVal){
	var param = {
		'user': userName,
		'pass': newPasswordVal,
		'changepassword': true
	}
	$.ajax({
		type: "POST",
		url: "/api/authenticate",
		data: param,
			success: function (msg) {
				window.location = msg.redirectToPage;
			},
			error: function(xhr, errorType, exception) {
				if(onError && typeof(onError) == "function"){
					onError();
				}
			invalidData();
			}
	});
}
function sentForgetLink(parameters, onError){
	var param =  {};
	Object.keys(parameters).forEach(function(paramData, paramKey){
		param[paramData] = parameters[paramData].val();
	})
	param.sendForgetLink = true;
	$.ajax({
		type: "POST",
		url: "/api/authenticate",
		data: param,
			success: function (msg) {
				if(msg && msg.info){
					jQuery(".login-form .forgetConfirmationPanel").removeClass('hide');
					$('.login-form .login-col-2').addClass('hide');
				}else{
					if(onError && typeof(onError) == "function"){
						onError();
					}
					var error  = 'Unable to generate the link';
					if(msg && msg.error && typeof(msg.error)=='string') error = msg.error;
					$('#login-page .sendLinkError .error-text').removeClass('hide-element').html(error);
					$('#login-page .sendLinkError').removeClass('hide');
					jQuery(".login-form .sentForgetLink a[href='#']").html('Send Link').removeClass('disabled');
				}
			},
			error: function(xhr, errorType, exception) {
				if(onError && typeof(onError) == "function"){
					onError();
				}
				var error  = '';
				if(msg && msg.error && typeof(msg.error)=='string') error = msg.error;
				invalidData("reset", error);
			}
	});
}
function resetpassword(parameters, onError){
	var param =  {};
	Object.keys(parameters).forEach(function(paramData, paramKey){
		param[paramData] = parameters[paramData].val();
	})
	$.ajax({
		type: "POST",
		url: "/api/authenticate",
		data: param,
			success: function (msg) {
				if(msg && msg.redirectToPage){
					jQuery(".login-form .reset a[href='#']").html('Password Updated, please login with new password').addClass('disabled');
					jQuery(".login-form .resetConfirmationPanel").removeClass('hide');
					jQuery("#reset-password .resetlogin").addClass('hide');
				}else{
					if(onError && typeof(onError) == "function"){
						onError();
					}
					var error  = '';
					if(msg && msg.error && typeof(msg.error)=='string') error = msg.error;
					invalidData("reset", error);
				}
			},
			error: function(xhr, errorType, exception) {
				if(onError && typeof(onError) == "function"){
					onError();
				}
				var error  = '';
				if(msg && msg.error && typeof(msg.error)=='string') error = msg.error;
				invalidData("reset", error);
			}
	});
}
jQuery(function() {
	//jQuery(".login-form a[href='/toc']").click(function (evt) {
	jQuery(".login-form input").keypress(function (evt) {
		if (evt.which == 13) {
			if(jQuery('#confirmpassword:visible').length > 0){
				jQuery(".login-form .changepassword a[href='#']").trigger('click');
			}else if(jQuery("#reset-password:not(.hide) .reset").length > 0){
				jQuery("#reset-password:not(.hide) .reset a[href='#']").trigger('click');
			}else{
				jQuery(".login-form .login a[href='#']").trigger('click');
			}
			evt.preventDefault();
		}
	});
	jQuery(".login-form .login a[href='#']").click(function (evt) {
		jQuery(".login-form .login a[href='#']").html('logging in...').addClass('disabled');
		authenticate('', function(){
			jQuery(".login-form .login a[href='#']").html('login').removeClass('disabled');;
		});
		evt.preventDefault();
	});
	jQuery("#reset-password .login-form .resetCancel a[href='#']").click(function (evt) {
		jQuery("#login-page").removeClass('hide');
		jQuery("#reset-password").addClass('hide');
	})
	jQuery("#reset-password .login-form .reset a[href='#']").click(function (evt) {
		jQuery("#reset-password .login-form .reset a[href='#']").html('Changing in...').addClass('disabled');
		var param = {};
		console.log('onclick');
		param.user = jQuery("#reset-password .login-form #resetusername");
		param.pass = jQuery("#reset-password .login-form #resetoldpassword");
		param.resetpassword = jQuery("#reset-password .login-form #resetnewpassword");
		param.resetconfirmpassword = jQuery("#reset-password .login-form #resetconfirmpassword");
		if( !inputIsEmpty( [ param.user, param.pass, param.resetpassword, param.resetconfirmpassword] ) ){
			if(param.resetpassword.val() === param.resetconfirmpassword.val()){
				$(".login-form .reset a[href='#']").html('Reseting Password...').addClass('disabled');
				resetpassword(param);
			}else{
				$('#reset-password .input-field .error-text').removeClass('hide-element').html('Passwords not matched');
				jQuery("#reset-password .login-form .reset a[href='#']").html('RESET').removeClass('disabled');
			}
		}else{
			jQuery("#reset-password .login-form .reset a[href='#']").html('RESET').removeClass('disabled');
		}
	});
	jQuery(".login-form #relogin").click(function (evt) {
		location.reload();
	})
	jQuery(".login-form .resetpassword a[href='#']").click(function (evt) {
		jQuery("#login-page").addClass('hide');
		jQuery("#reset-password").removeClass('hide');
		evt.preventDefault();
	});
	jQuery(".login-form .forgetpassword a[href='#']").click(function (evt) {
		jQuery("#login-page .login-view").addClass('hide');
		jQuery("#login-page .forget-view").removeClass('hide');
		evt.preventDefault();
	});
	jQuery(".login-form .sentForgetLink a[href='#']").click(function (evt) {
		jQuery(".login-form .sentForgetLink a[href='#']").html('Sending ...').addClass('disabled');
		var param = {};
		console.log('onclick');
		param.user = jQuery("#login-page .login-form #username");
		if( !inputIsEmpty( [ param.user] ) ){
			if(param.user.val().match(/@exeter(premedia|pm)\.com/i)){
				$('#login-page .sendLinkError .error-text').removeClass('hide-element').html('This facility is not available for Exeter users');
				$('#login-page .sendLinkError').removeClass('hide');
				jQuery(".login-form .sentForgetLink a[href='#']").html('Send Link').removeClass('disabled');
			}else{
				sentForgetLink(param)
			}
		}else{
			jQuery(".login-form .sentForgetLink a[href='#']").html('Send Link').removeClass('disabled');
		}
		evt.preventDefault();
	});
	jQuery("#login-page .forgetpasswordCancel a[href='#']").click(function (evt) {
		jQuery("#login-page .login-form #username").val('');
		jQuery("#login-page .forget-view").addClass('hide');
		jQuery("#login-page .login-view").removeClass('hide');
	})
	$('body').on({
		click: function(evt){
			authenticate(true);
			evt.preventDefault();
		}
	}, '.confirmationPanel .confirm');

	$('body').on({
		click: function(evt){
			$('.login-form .login-col-2').removeClass('hide');
			$('.login-form .confirmationPanel').addClass('hide');
			$('#username').val('');
			$('#password').val('');
			kriya.ctoken = undefined;
		}
	}, '.confirmationPanel .confirmCancel');
	$(".login-form .changepassword a[href='#']").click(function (evt) {
		if( !inputIsEmpty( [ $('#username'), $('#password'), $('#confirmpassword') ] ) ){
			if($('#password').val() === $('#confirmpassword').val()){
				$(".login-form .changepassword a[href='#']").html('Modifying Password...').addClass('disabled');
				changepassword($('#username').val(),$('#password').val());
				$(".login-form .changepassword a[href='#']").html('Change Password').removeClass('disabled');
			}else{
				$('.input-field .error-text').removeClass('hide');
			}
		}
		evt.preventDefault();
	});
});

$(window).on('load', function(e) {
	setTimeout(function() {
		$("body").addClass("loaded")
	}, 200)
});
$(window).ready(function() {
	if($('#login-page[forget-link="true"]').length > 0){
		openChangePassword({displayUser: true})
	}
});
