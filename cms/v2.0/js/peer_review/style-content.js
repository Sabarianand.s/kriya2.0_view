var styles = {
		'front': {
			'Subject': 'p,jrnlArtType,block',
			'ChapNumber': 'h1,jrnlChapNumber,block',
			'ArtTitle': 'h1,jrnlArtTitle,block',
			'Authors': {
				'Authors': 'p,jrnlAuthors,block',
				'Author': 'span,jrnlAuthor,inline',
				'OnBehalfOf': 'span,jrnlOnBehalfOf,inline',
				'Degree': 'span,jrnlDegrees,inline',
				'Aff': 'p,jrnlAff,block',
				'Corresp': 'p,jrnlCorrAff,block',
				'AuthorFN': 'p,jrnlAuthorFN,block',
				'Biography': 'p,jrnlBiography,block',
				'Givenname': 'span,jrnlGivenName,inline',
				'Surname': 'span,jrnlSurName,inline',
			},
			'Corresp': {
				'Author': 'span,jrnlAuthor,inline',
				'Department': 'span,jrnlDepartment,inline',
				'Institution': 'span,jrnlInstitution,inline',
				'State': 'span,jrnlState,inline',
				'City': 'span,jrnlCity,inline',
				'Country': 'span,jrnlCountry,inline',
				'Phone': 'span,jrnlPhone,inline',
				'Fax': 'span,jrnlFax,inline',
				'Email': 'span,jrnlEmail,inline',
				'PinCode': 'span,jrnlPinCode,inline',
				'Givenname': 'span,jrnlGivenName,inline',
				'Surname': 'span,jrnlSurName,inline',
			},
			'Abstract': {
				'AbsHead': 'h1,jrnlAbsHead,block',
				'AbsTitle': 'h2,jrnlAbsTitle,block',
				'AbsPara': 'p,jrnlAbsPara,block',
				'KeywordHead': 'h1,jrnlKeywordHead,block',
				'KeywordPara': 'p,jrnlKeywordPara,block',
			},
			'RRH': 'p,jrnlRRH,block',
		},
		'body': {
			'Heads':{
				'Head1': 'h1,jrnlHead1,block',
				'Head2': 'h2,jrnlHead2,block',
				'Head3': 'h3,jrnlHead3,block',
				'Head4': 'h4,jrnlHead4,block',
				'Head-label' : 'span,label,inline',
				'BoxHead': 'p,jrnlBoxHead,block',
			},
			'Para': 'p,jrnlSecPara,block',
			'EqnPara': 'p,jrnlEqnPara,block',
			'CodePara': 'p,jrnlCodePara,block',
		},
		'back': {
			'Heads':{
				'AckHead': 'h1,jrnlAckHead,block',
				'RefHead': 'h1,jrnlRefHead,block',
				'FundHead': 'h1,jrnlFundingHead,block',
				'CompetingHead': 'h1,jrnlCompHead,block',
				'ContribHead': 'h1,jrnlContribHead,block',
				'EthicsHead': 'h1,jrnlEthicHead,block',
				'ConsentHead': 'h1,jrnlConsentHead,block',
				'DisclosureHead': 'h1,jrnlDisclosureHead,block',
				'DisclaimerHead': 'h1,jrnlDisclaimerHead,block',
				'DatasharingHead': 'h1,jrnlDatasharingHead,block',
				'DataAvailabilityHead': 'h1,jrnlDataAvailabilityHead,block',
				'AccessionNoHead': 'h1,jrnlAccessionNoHead,block',
			},
			'AckPara': 'p,jrnlAckPara,block',
			'RefText': 'p,jrnlRefText,block',
			'Paras':{
				'FundPara': 'p,jrnlFundingPara,block',
				'CompetingPara': 'p,jrnlCompPara,block',
				'ContribPara': 'p,jrnlContribPara,block',
				'EthicsPara': 'p,jrnlEthicPara,block',
				'ConsentPara': 'p,jrnlConsentPara,block',
				'DisclosurePara': 'p,jrnlDisclosurePara,block',
				'DisclaimerPara': 'p,jrnlDisclaimerPara,block',
				'DatasharingPara': 'p,jrnlDatasharingPara,block',
				'DataAvailabilityPara': 'p,jrnlDataAvailabilityPara,block',
				'AccessionNoPara': 'p,jrnlAccessionNoPara,block',
			},
			'EqnPara': 'p,jrnlEqnPara,block',
			'CodePara': 'p,jrnlCodePara,block',
			'App Heads':{
				'Head1': 'h1,jrnlAppHead1,block',
				'Head2': 'h2,jrnlAppHead2,block',
				'Head3': 'h3,jrnlAppHead3,block',
				'Head4': 'h4,jrnlAppHead4,block',
				'Head5': 'h5,jrnlAppHead5,block',
			},
			'AppPara': 'p,jrnlPara,block',
			'Abbreviation':{
				'AbbHead': 'h1,jrnlAbbrevHead,block',
				'AbbList': 'p,jrnlAbbrevList,block',
			},
			'Footnote': 'p,jrnlFootNotePara,block',
		},
		'body back': {
			'Captions':{
				'FigCaption': 'p,jrnlFigCaption,block',
				'FigFoot': 'p,jrnlFigFoot,block',
				'TblCaption': 'p,jrnlTblCaption,block',
				'TblFoot': 'p,jrnlTblFoot,block',
				'BoxCaption': 'p,jrnlBoxCaption,block',
				'BoxPara': 'p,jrnlBoxPara,block',
				'SupplCaption': 'p,jrnlSupplCaption,block',
				'VidCaption': 'p,jrnlVidCaption,block',
				'Disp-Quote': 'div,jrnlBlockQuote,block,,,jrnlQuotePara,true',
				'Quote-Attrib': 'p,jrnlQuoteAttrib,block',
				'Biography': 'p,jrnlBiography,block',
				'Eqn-Label' : 'span,jrnlEqnLabel,inline',
			},
			'FootNotes':{
				'General' : ',,block,fn-type,general',
				'Linked' : ',,block,fn-type,linked-fn',
				'Abbreviation' : ',,block,fn-type,abbrev',
			},
			'Table':{
				'Continue as table' : ',,block,data-table-continue,true',
				'Convert TableToBox' : ',,block,data-convert,TableToBox',
			},
		},
		'front body back': {
			'Delete': 'p,jrnlDeleted,block',
			'Inline-delete': 'span,del,inline',
		},
	};
	var refStyles = {
		'jrnlRefText': {
			'<u>S</u>l No.':['span,RefSlNo,inline'],
			'<u>A</u>U/ED':{
				'sk': 'alt+a',
				'<u>A</u>uthor': 'span,RefAuthor,inline',
				'<u>E</u>ditor':'span,RefEditor,inline',
				'<u>C</u>ollab':'span,RefCollaboration,inline',
				'E<u>t</u>al':'span,RefEtal,inline',
			},
			'<u>A</u>rt. Title':['span,RefArticleTitle,inline','Journal Website'],
			'<u>J</u>our. Title':['span,RefJournalTitle,inline','Journal'],
			'<u>B</u>ook Title':['span,RefBookTitle,inline','Book Website'],
			'<u>C</u>hap. Title':['span,RefChapterTitle,inline','Book'],
			'Pub. <u>N</u>ame':['span,RefPublisherName,inline','Book'],
			'Pub. <u>L</u>ocation':['span,RefPublisherLoc,inline','Book'],
			'Conf <u>N</u>ame':['span,RefConfName,inline','Conference'],
			'Conf <u>T</u>itle':['span,RefConfArticleTitle,inline','Conference'],
			'Conf <u>L</u>oc':['span,RefConfLoc,inline','Conference'],
			'Conf <u>D</u>ate':['span,RefConfDate,inline','Conference'],
			'<u>S</u>ource':['span,RefSource,inline','Conference Website'],
			'<u>S</u>oft Title':['span,RefSoftTitle,inline','Software'],
			'<u>S</u>oft Name':['span,RefSoftName,inline','Software'],
			'Soft <u>V</u>er':['span,RefSoftVer,inline','Software'],
			'Soft Man<u>L</u>oc':['span,RefSoftManLoc,inline','Software'],
			'Soft Man<u>N</u>ame':['span,RefSoftManName,inline','Software'],
			'<u>T</u>hesisTitle':['span,RefThesisTitle,inline','Thesis'],
			'Thesis':['span,RefThesis,inline','Thesis'],
			'Patent Title':['span,RefPatentTitle,inline','Thesis'],
			'Patent Num':['span,RefPatentNum,inline','Thesis'],
			'Data Title':['span,RefDataTitle,inline','Thesis'],
			'Data Source':['span,RefDataSource,inline','Thesis'],
			'Periodical Title':['span,RefPeriodicalTitle,inline','Thesis'],
			'Periodical Source':['span,RefPeriodicalSource,inline','Thesis'],
			'PrePrint Title':['span,RefPrePrintTitle,inline','Thesis'],
			'PrePrint Source':['span,RefPrePrintSource,inline','Thesis'],
			'Report Number':['span,RefReportNumber,inline','Report'],
			'Report Title':['span,RefReportTitle,inline','Report'],
			'News Title':['span,RefNewsPaperTitle,inline','Newspaper'],
			'News Source':['span,RefNewsPaperSource,inline','Newspaper'],
			'RefPrePrint Link':['span,RefPrePrintLink,inline','Thesis'],
			'<u>Y</u>ear':['span,RefYear,inline'],
			'<u>V</u>olume':['span,RefVolume,inline'],
			'<u>I</u>ssue':['span,RefIssue,inline','Journal'],
			'<u>F</u>page':['span,RefFPage,inline'],
			'<u>L</u>page':['span,RefLPage,inline'],
			'<u>I</u>npress':['span,RefInPress,inline'],
			'<u>E</u>Location':['span,RefELocation,inline','Journal'],
			'<u>W</u>ebSite Title':['span,RefWebSiteTitle,inline','Website'],
			'<u>W</u>ebSite':['span,RefWebSite,inline','Website Journal'],
			'L<u>A</u>D': 'span,RefLAD,inline,Journal',
			'DOI': 'span,RefDOI,inline,Journal',
			'<u>C</u>omments':{
				'sk': 'alt+c',
				'type': 'Book Journal',
				'<u>C</u>omment':'span,RefComments,inline',
				'<u>E</u>ditorEdComment': ['span,RefEditorEdComment,inline','Book'],
				'<u>E</u>dition':['span,RefEdition,inline','Book'],
				'<u>I</u>nComment':['span,RefInComment,inline','Book'],
				'<u>F</u>PageComment':['span,RefFPageComment,inline','Book'],
				'<u>D</u>ay':'span,RefDay,inline',
				'<u>M</u>onth':'span,RefMonth,inline',
			},
		}
	};
	var hoverMenu = $('<div data-type="popUp" data-component="contextMenu" data-selection="true" class="autoWidth hidden bottom z-depth-2">');
	$(hoverMenu).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
	var di = 1;
	$.each(styles, function(index, items) {
		$.each(items, function(key, method) {
			if (typeof(method) == "object"){
				var btnGroup = $('<span class="btn btn-hover" data-type=" ' + index + ' " title="Journal Group">');
				var sk = '';
				if (method['sk'] != undefined){
					sk = ' data-shortcut="' + method['sk'] + '"';
					delete method['sk'];
				}
				//$(btnGroup).append('<span class="btn btn-hover dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"' + sk + '>'+ key + '<span class="caret"></span></span>');
				$(btnGroup).append('<a class="btn dropdown-button" href="#" data-activates="drop-down-' + key.replace(/ /g, '') + di +'"' + sk + '>' + key + '<i class="material-icons">arrow_drop_down</i></a>');
				$(btnGroup).append('<ul class="dropdown-content" id="drop-down-' + key.replace(/ /g, '') + di++ + '">');
				$.each(method, function(k,m){
					if (k == "Author-old"){
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.parseAuthors();">' + k + '</a></li>');
					}else{
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m + '\');">' + k + '</a></li>');
					}
				});
				$(hoverMenu).append(btnGroup);
			}else{
				$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\''+method+'\')">'+key+'</span>');
			}
		});
	});

	$(hoverMenu).append('<span class="btn btn-hover" data-type=" body back " data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'markAsCitation\'}" data-selection="true">Mark as Citation</span>');
	$(hoverMenu).append('<span class="btn btn-hover" data-type=" body back " data-channel="components" data-topic="general" data-event="click" data-message="{\'funcToCall\': \'unwrapPattern\'}" data-pattern="true">Not a pattern</span>');
	$(hoverMenu).find('li:contains("Continue as table")').after('<li><a href="javascript:;" onclick="kriyaEditor.init.tableToEquation()">Convert TableToEquation</a></li><li><a href="javascript:;" onclick="kriyaEditor.init.markasheader()">Mark as Header</a></li><li><a href="javascript:;" onclick="kriyaEditor.init.mergetable()">Merge with previous table</a></li>');
	$('.templates').append(hoverMenu);

	var hoverMenu = $('<div data-type="popUp" data-component="jrnlRefText" data-selection="true" class="autoWidth hidden z-depth-2 bottom">');
	$(hoverMenu).append('<i class="material-icons arrow-denoter">arrow_drop_down</i>');
	var referenceTypes = ['Journal', 'Book', 'Conference', 'Thesis', 'Patent', 'Data', 'Software', 'Website', 'Report', 'Newspaper'];
	var referenceSelector = $('<select class="btn btn-hover referenceTypes" data-channel="components" data-topic="general" data-event="change" data-message="{\'funcToCall\': \'changeRefType\'}"></select>');
	$.each(referenceTypes, function(k, v) {
		$(referenceSelector).append('<option value="' + v + '">' + v + '</option>');
	});
	$(hoverMenu).append(referenceSelector)
	$.each(refStyles, function(index, items) {
		$.each(items, function(key, method) {
			if (typeof(method) == "object" && method.constructor !== Array){
				var btnGroup = $('<span class="btn btn-hover" data-type=" ' + index + ' " title="Journal Group">');
				if (method['type'] != undefined){
					btnGroup.attr('data-ref-type', ' ' + method['type'] + ' ');
					delete method['type'];
				}
				var sk = '';
				if (method['sk'] != undefined){
					sk = ' data-shortcut="' + method['sk'] + '"';
					delete method['sk'];
				}
				$(btnGroup).append('<a class="btn dropdown-button" href="#" data-activates="drop-down-' + di +'"' + sk + '>' + key + '<i class="material-icons">arrow_drop_down</i></a>');
				$(btnGroup).append('<ul class="dropdown-content" id="drop-down-' + di++ + '">');
				$.each(method, function(k,m) {
					var sk ='';
					var myRegexp = /<u>([a-z])<\/u>/i;
					if (myRegexp.test(k)){
						var match = myRegexp.exec(k);
						sk = ' data-shortcut="' + match[1].toLocaleLowerCase() + '"';
					}
					if (m.constructor == Array){
						btnGroup.find('ul').append('<li data-ref-type=" ' + m[1] + ' "><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m[0] + '\');"' + sk + '>' + k + '</a></li>');
					}else{
						btnGroup.find('ul').append('<li><a href="javascript:;" onclick="stylePalette.actions.applyFormat(\'' + m + '\');"' + sk + '>' + k + '</a></li>');
					}
				});
				$(hoverMenu).append(btnGroup);
			}else{
				if (method.constructor == Array){
					if (method[1] == undefined){
						$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method[0] + '\')">'+key+'</span>');
					}else{
						$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method[0] + '\')" data-ref-type=" ' + method[1] + ' ">'+key+'</span>');
					}
				}else{
					$(hoverMenu).append('<span data-type=" ' + index + ' " class="btn btn-hover" onclick="stylePalette.actions.applyFormat(\'' + method + '\')">'+key+'</span>');
				}
			}
		});
	});
	$('[data-component="contextMenu"]').append('<span class="btn btn-hover" data-type=" front " title="Journal Group"><a class="btn dropdown-button" href="#" data-activates="drop-down-Authorslist1">Link corresp to author <i class="material-icons">arrow_drop_down</i></a><ul class="dropdown-content" id="drop-down-Authorslist1"></ul></span>');
	// added a palette item to update chapter number
	$('[data-component="contextMenu"]').append('<span class="btn btn-hover" data-type=" front " title="Add/Edit Chapter" onclick="stylePalette.actions.openAddChapterPopup()">Update Chapter</span>');
	$('.front .jrnlAuthors .jrnlAuthor').each(function(){
		var authorname = $(this).text()
		$('ul#drop-down-Authorslist1').append('<li><a href="javascript:;" onclick="stylePalette.actions.correspMapping(\''+authorname+'\');">'+authorname+'</a></li>')
	})
	//$(hoverMenu).append('<span class="btn btn-hover" data-type=" jrnlRefText " onclick="stylePalette.actions.clearFormatting()" data-selection="true">Clear format</span>');
	$('[data-component="contextMenu"]').append('<span class="btn btn-hover" data-type=" front " onclick="stylePalette.actions.clearFormatting()" data-selection="true">Clear format</span>');
	//$('.templates').append(hoverMenu);
	$('body').append('<span class="class-highlight" style="position: absolute;bottom: 6px;display: inline-block;height: 25px;width: 200px;right: 10px;background: #eee;border: 1px solid #ddd;font-size: 12px;line-height: 110%;padding:5px;color:#424242;"></span>');
	$('.btn.dropdown-button:contains("Corresp")').parent().attr('data-type', ' hide ');
	if ($('.body[data-edited="true"]').length > 0){
		$('.btn.dropdown-button:contains("Corresp")').parent().attr('data-type', ' front ')
	}