 var kriyaEditor = function() {
	//default settings
	var settings = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (kriyaEditor) {
	settings = kriyaEditor.settings;
	var timer;
	kriyaEditor.user = {};
	kriyaEditor.user.name = $('#headerContainer .userProfile .username').text();
	if(kriya.config.content.role=='publisher'){
		kriyaEditor.user.name = kriyaEditor.user.name+' ('+kriya.config.content.customer.toUpperCase()+')';
	}else{
		kriyaEditor.user.name = kriyaEditor.user.name+' ('+kriya.config.content.role.toUpperCase()+')';
	}
	kriyaEditor.user.id   = $('#headerContainer .userProfile .username').attr('data-user-id');

	kriyaEditor.init = {
		editor: function(dataNode) {
			kriyaEditor.init.saveBackup();
			//kriyaEditor.init.trackArticle();
			return new wysihtml.Editor(dataNode, {
				//toolbar: "toolbar",
				parserRules: wysihtmlParserRules,
				useLineBreaks: false,
				autoLink: false
			});
		},
		saveBackup: function(){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return true;
			}

			//Create snap shot when page load - jagan
			var content = $('div.WordSection1')[0].outerHTML
			content += $('#navContainer #queryDivNode')[0].outerHTML
			var parameters = {
				'customer': kriya.config.content.customer,
				'project': kriya.config.content.project,
				'doi': kriya.config.content.doi,
				'content': encodeURIComponent(content),
				'fileName' : 'page_load'
			};
			kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
				console.log('Data Saved');
			});

			setInterval(function(){
				if(keeplive && keeplive.getUserStatus() != 'in-active'){
					var content = $('div.WordSection1')[0].outerHTML
					content += $('#navContainer #queryDivNode')[0].outerHTML
					var parameters = {
						'customer': kriya.config.content.customer,
						'project': kriya.config.content.project,
						'doi': kriya.config.content.doi,
						'content': encodeURIComponent(content)
					};
					kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
						console.log('Data Saved');
					});
				}
			},600000);
		},
		trackArticle: function(){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return true;
			}
			//setInterval(function(){
			if ($('#welcomeContainer:visible').length == 0){
				if ((typeof(timeTicker) != "undefined" && timeTicker.isRunning()) || typeof(timeTicker) == "undefined"){
					var parameters = {
						'customerName': kriya.config.content.customer,
						'projectName': kriya.config.content.project,
						'doi': kriya.config.content.doi,
						'skipElasticUpdate': 'true'
					};
					var d = new Date();
					var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
					var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
					parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time></log></job-logs></stage>';
					kriya.general.sendAPIRequest('updatedatausingxpath', parameters, function(res){});
					setTimeout(function(){
						kriyaEditor.init.trackArticle();
					}, 120000);
				}
			}
			//},300000);
		},
		tracker: function(dataNode){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return false;
			}
			kriyaEditor.init.trackeChanges();
			kriyaEditor.changeID = Math.floor(Date.now() / 1000);
			return new ice.InlineChangeEditor({
				element: dataNode,					// element to track - ice will make it contenteditable
				trackFormats: true,					// element to track - ice will make it contenteditable
				handleEvents: true,											// tell ice to setup/handle events on the `element`
				currentUser: {
					id: kriya.config.content.role + ' ' + kriyaEditor.user.id,
					name: kriyaEditor.user.name
				}, // set a user object to associate with each change
				plugins: [													// optional plugins
					'IceAddTitlePlugin',									// Add title attributes to changes for hover info
					//'powerpaste',
					{
						name: 'IceCopyPastePlugin',							// Track content that is cut and pasted
						settings: {
							preserve: 'p, span[id,class], em, strong,ul,ol,li, i, u, sup, sub',	// List of tags and attributes to preserve when cleaning a paste | Included  i, u, sup, sub, td by Raj
							preserveTrack: 'ul,ol,table', // List of tags and attributes to preserve adding track when paste
							afterPaste: function(targetElement){ //Callback function for paste was added by jagan to save the pasted data
								//If pasted element is multiple paragraph then using loop and save the content -Rajesh
								if($('#contentDivNode').find('p[data-paste=true]').length > 0){
									$('#contentDivNode').find('p[data-paste=true]').each(function(){
										$(this).removeAttr('data-paste');
										var spanNode=$(this)[0];
										kriyaEditor.settings.undoStack.push(spanNode);
										kriyaEditor.init.addUndoLevel('paste (ctrl+v)');
									});
								}else{
									//If pasted element is list then add new id and save as newly inserted
									//else save the target element
									if($(targetElement).closest('ul:not([id]),ol:not([id])').length > 0){
										var listNode = $(targetElement).closest('ul:not([id]),ol:not([id])');
										listNode.attr('id', uuid.v4()).attr('data-inserted', 'true');
										listNode.find('*').each(function(){
											$(this).attr('id', uuid.v4());
										});
										kriyaEditor.settings.undoStack.push(listNode);

										if(kriyaEditor.settings.summaryData){
											$(kriyaEditor.settings.summaryData.added).each(function(){
												var addedBlockNode = $(this).closest(tracker.blockEls.join());
												$(addedBlockNode).attr('id', uuid.v4());
												$(addedBlockNode).attr('data-inserted', 'true');
												kriyaEditor.settings.undoStack.push(addedBlockNode);
											});
											$(kriyaEditor.settings.summaryData.characterDataChanged).each(function(){
												var existBlockNode = $(this).closest(tracker.blockEls.join());
												kriyaEditor.settings.undoStack.push(existBlockNode);
											});
											$(kriyaEditor.settings.summaryData.removed).each(function(){
												$(this).attr('data-removed', 'true');
												kriyaEditor.settings.undoStack.push($(this));
											});
										}
									}else{
										kriyaEditor.settings.undoStack.push(targetElement);
									}

									kriyaEditor.init.addUndoLevel('paste (ctrl+v)');
								}
							}
						}
					}
				],
				menu: {
					edit: {
						title: 'Edit',
						items: 'undo redo | cut copy paste | selectall'
					}
				}
			});
		},
		trackeChanges: function (){
			//$('#changesDivNode .change-div').remove();
			//Initialize the tracked elements in the changes tab
			/*var trackNodes = document.querySelectorAll('.ins, .del, .sty, .styrm, [data-track="del"], [data-track="ins"]');
			for (var t = 0;t<trackNodes.length;t++) {
				var trackNode = trackNodes[t];
				eventHandler.menu.observer.addTrackChanges(trackNode, 'added');
			}*/
		},
		loadImage: function (){
			return true;
			// stopped loading pushing-pixels to avoid saving 'pushing-pixels' instead of original images
			setTimeout(function(){
				var imageNodes = document.querySelectorAll('img');
				for (var t = 0; t < imageNodes.length; t++) {
					var imageNode = imageNodes[t];
					var orginalImage = imageNode.getAttribute('data-alt-src');
					if(orginalImage){
						//imageNode.setAttribute('src', orginalImage);
						loadImage(imageNode, orginalImage)
					}
				}
				function loadImage(imageNode, orginalImage){
					imageNode.setAttribute('src', orginalImage);
					imageNode.onerror = function() {
						imageNode.setAttribute('src', '../images/pushing-pixels.gif');
					}
				}
				createCloneContent();
			},3000);
		},
		undoRedo: function(dataNode){
			var x = document.querySelector.bind(document);
			kriyaEditor.settings.undoStack = [];
			kriyaEditor.settings.contentNode = dataNode;
			kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
			
			var sel = rangy.getSelection();
			var blockNode = $(sel.anchorNode).closest('[id]:not(span)');
			kriyaEditor.settings.startCaretID = blockNode.attr('id');
			kriyaEditor.settings.startCaretPos = getCaretCharacterOffsetWithin(blockNode[0]);

			kriyaEditor.settings.changeNode = document.getElementById('navContainer');
			kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
			kriyaEditor.settings.forkedNode = false;

			var contentUndoObject = {
				container: kriyaEditor.settings.contentNode,
				oldValue : kriyaEditor.settings.contentNode.innerHTML,
				newValue : ''
			}

			kriyaEditor.settings.undo = x('[data-tooltip="Undo"]');
			kriyaEditor.settings.redo = x('[data-tooltip="Redo"]');
			kriyaEditor.settings.save = x('[data-tooltip="Save"]');

			stack = new Undo.Stack();
			stack.isDirty = false;
			var newValue = "";

			/**
			 * hook key-up events to track the changes made to content
			 */
			//var keysPressed = [];
			kriyaEditor.settings.contentNode.addEventListener('keyup', function(event){

				//#468 - Auto save content in editor on focus out using keystrokes - Prabakaran.A(prabakaran.a@focusite.com)
				if(stack.isDirty && kriyaEditor.lastSelection){
					var lastSelectionTargetElement = kriyaEditor.lastSelection.anchorNode;
				}
				//End of #468
				//keys[event.keyCode] = false;
				//var index = keysPressed.indexOf(event.key.toLocaleLowerCase());
				//keysPressed.splice(index, 1);

				var range = rangy.getSelection();
				if (range.anchorNode == null && range.rangeCount == 0){//clicked some where outside the content - JAI 19-02-2018
					return false;
				}
				//#469 - All Customers | Object Citations | Insert citations - Helen.J (helen.j@focusite.com)
				kriya.selection = rangy.getSelection().getRangeAt(0);
				kriya.selectedNodes = kriya.selection.getNodes();
				//End of #469

				//When space,break,backSpace,delete will type at that time showNonPrintChar()function will trigger for showing non-printable characters.
				if(range.anchorNode && $(range.anchorNode.parentNode).length > 0 && (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 32 || (event.shiftKey && event.keyCode == 13 ))){
					//The below line was commented because it consuming more time when typing - jagan
					//kriya.general.showNonPrintChar(range.anchorNode.parentNode);
				}

				var targetElement = range.anchorNode;
				kriyaEditor.lastSelection = range;

				if (targetElement && targetElement.nodeType == 3){
					targetElement = targetElement.parentNode;
				}

				//Clear the highlight when start typing not when cursor move
				if(!(event.keyCode >= 33 && event.keyCode <= 40) && $('#clonedContent .highlight.findOverlay').length > 0){
					eventHandler.menu.findReplace.searchText('clear');
				}

				//return false if the target is non editable
				if((targetElement && targetElement.hasAttribute('data-editable') && targetElement.getAttribute('data-editable')=='false') || $(targetElement.closest('*[data-editable="false"]')).length >0 || kriya.config.preventTyping && ($(targetElement).closest(kriya.config.preventTyping).length > 0)){
					//#468 - Auto save content in editor on focus out using keystrokes - Prabakaran.A(prabakaran.a@focusite.com)
					if (stack.isDirty && (event.keyCode >= 33 && event.keyCode <= 40)){
						kriyaEditor.init.addUndoLevel('navigation-key', lastSelectionTargetElement?lastSelectionTargetElement:targetElement);
					}
					//End of #468
					return false;
				}

				//update citation if page type text to citation - jagan
				var clonedTarget = $(targetElement).clone(true);
				clonedTarget.find('.jrnlBibRef').remove();
				if(range.anchorNode.nodeType == 3 && $(targetElement).find('.jrnlBibRef:not([data-track="del"])').length > 0 && $(targetElement).hasClass('ins') && !((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) && $(clonedTarget).text().match(/[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/)){
					$(targetElement).html($(targetElement).html()); //to join all text nodes
					if($(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling && $(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling.nodeType == 3){
						targetElement = $(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling;
					}
				}

				var prevNode = $(targetElement)[0].previousSibling;
				while(prevNode && prevNode.nodeType != 3 && $(prevNode).closest('.del, [data-track="del"]').length > 0){
					prevNode = $(prevNode)[0].previousSibling;
				}
				if(!((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) && prevNode && $(prevNode).is('.jrnlBibRef:not([data-track="del"])') && citeJS.settings && citeJS.settings.R && citeJS.settings.R.citationType == "1"){
					var trackText = $(targetElement).text();
					var pageNumMatches = trackText.match(/^[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/);
					if(pageNumMatches && pageNumMatches[1]){
						trackText = trackText.replace(/^[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/g, '');

						if($(targetElement)[0].nodeType == 3){
							$(targetElement)[0].nodeValue = trackText;
						}else{
							$(targetElement).text(trackText);
						}

						var pageNum = pageNumMatches[1];
						citeJS.general.updatePageNumInCitation(prevNode, pageNum);
						kriyaEditor.init.addUndoLevel('save-page-no', prevNode.closest('p'));
					}
				}

				switch (event.keyCode) {
					case 46: // delete
					case 8: // backspace

							//Remove the text field when it becomes empty - jagan
							if($(targetElement).closest('[data-text-field]:empty').length > 0){
								$(targetElement).closest('[data-text-field]:empty').attr('data-removed', 'true');
							}

							//Add data to save for the content remove when backspace and delete press
							if(kriyaEditor.settings.summaryData && kriyaEditor.settings.summaryData.removed.length > 0){
								$(kriyaEditor.settings.summaryData.removed).each(function(){
									if(this.nodeType != 3 && $(this).attr('id')){
										var removedId = $(this).attr('id');
										var removeNode = this;
										if($(kriya.config.containerElm+ ' #'+removedId).length < 1){
											var check = false;
											// Don't include if the node is child of any removed node
											$(kriyaEditor.settings.summaryData.removed).each(function(){
												if($(this).find('#'+removedId).length > 0){
													check = true;
												}
											});
											if(!check){
												$(removeNode).attr('data-removed', 'true');
												kriyaEditor.settings.undoStack.push(removeNode);
											}
										}
									}
								});

								if(kriyaEditor.settings.summaryData.reparented.length > 0){
									var reparentSaveNode = $(kriyaEditor.settings.summaryData.reparented).parents('[id]:first');
									if(reparentSaveNode.length > 0){
										if($(reparentSaveNode).closest('ul[id],ol[id]').length > 0){
											reparentSaveNode = $(reparentSaveNode).closest('ul[id],ol[id]');
										}
										kriyaEditor.settings.undoStack.push(reparentSaveNode);
									}
								}

								var mergedNode = $(targetElement).closest('p[id],div[id],table[id],h1[id],h2[id],h3[id],h4[id],h5[id],h6[id],li[id],ol[id]');
								kriyaEditor.settings.undoStack.push(mergedNode);
								kriyaEditor.init.addUndoLevel('merge-node');
								kriyaEditor.settings.summaryData = false;

							}else{
								kriyaEditor.init.addUndoLevel('delete-key', targetElement);
							}
						break;
					case 32: // space
						//console.log('add undo level called, space keys pressed');
						//The below line was commented because it consuming more time when typing - jagan
						//kriyaEditor.init.addUndoLevel('space-key', targetElement);
						kriyaEditor.init.addUndoLevel('character-key', targetElement);
						break;
					case 33: // page up
					case 34: // page down
					case 35: // end
					case 36: // home
					case 37: // arrow key: left
					case 38: // arrow key: up
					case 39: // arrow key: right
					case 40: // arrow key: down
						if (stack.isDirty){
							//console.log('add undo level called, navigation keys pressed');
							//#468 - Auto save content in editor on focus out using keystrokes - Prabakaran.A(prabakaran.a@focusite.com)
							kriyaEditor.init.addUndoLevel('navigation-key', lastSelectionTargetElement?lastSelectionTargetElement:targetElement);
							//End of #468
						}
						break;
					default:
						var keycode = event.keyCode;
						if (
							(keycode > 47 && keycode < 58)   || // number keys
							keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
							(keycode > 64 && keycode < 91)   || // letter keys
							(keycode > 95 && keycode < 112)  || // numpad keys
							(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
							(keycode > 218 && keycode < 223)   // [\]' (in order)
							){

								//Add new id if text field doesn't have id
								if($(targetElement).closest('[data-text-field]:not([id])').length > 0){
									$(targetElement).closest('[data-text-field]:not([id])').attr('data-inserted', 'true');
									$(targetElement).closest('[data-text-field]:not([id])').attr('id', uuid.v4());
								}

								if (! event.ctrlKey && !event.metaKey){
									//console.log('add undo level called, character keys pressed');

									//#355 - Id attribute missing for Paragraphs in Firefox - Lakshminarayanan.S (lakshminarayanan.s@focusite.com)
									// When a paragraph is split, the chrome takes the first paragraph as forked node but firefox takes the second node. Hence workaround for firefox
									//#533 - Save event is not triggered for on click - kriyaEditor.settings.forkedNode is undefined - prabakaran.a(prabakaran.a@focusite.com)
									if (navigator.userAgent.search("Firefox") > 0 && kriyaEditor.settings.forkedNode){
										//End of #533
										//forkedNode not conected with dom because of that next sibling and previous sibling comming as null-kirankumar
										if(!kriyaEditor.settings.forkedNode.previousElementSibling){
											var targetForForkNode = targetElement;
											while (targetForForkNode && kriya.config.blockElements.indexOf(targetForForkNode.nodeName.toLocaleLowerCase()) == -1){
												targetForForkNode = targetForForkNode.parentNode;
											}
											kriyaEditor.settings.forkedNode = targetForForkNode;
										}
										kriyaEditor.settings.forkedNode = kriyaEditor.settings.forkedNode.previousElementSibling;
										kriyaEditor.settings.summaryData = kriyaEditor.settings.forkedNode.nextElementSibling;
										kriyaEditor.settings.summaryData.added =  kriyaEditor.settings.forkedNode.nextElementSibling;
									   }
									//old contition - if (keycode == 13 && kriyaEditor.settings.forkedNode && kriyaEditor.settings.forkedNode.hasAttribute('id')){
									if (keycode == 13 && kriyaEditor.settings.forkedNode){

										if (targetElement.nodeType == 3){
											targetElement = targetElement.parentNode;
										}
										while (kriya.config.blockElements.indexOf(targetElement.nodeName.toLocaleLowerCase()) == -1){
											targetElement = targetElement.parentNode;
										}
										//when we click enter from h1-h6 then creating new element as div for that i replacing newly created tag name to forkednode tag name -#2618
										var forkeNode = kriyaEditor.settings.forkedNode;
										if(targetElement.nodeName != forkeNode.nodeName){
											targetElement = $(targetElement).renameElement(forkeNode.nodeName)[0];
											$(targetElement).attr('class',$(forkeNode).attr('class'));
											var range = document.createRange();
											range.setStart(targetElement, 0);
											selection.removeAllRanges();
											selection.addRange(range);
											//anchorNode not created in range for that i given this line.
											range = rangy.getSelection();
											if(kriyaEditor.settings.summaryData.added.length > 0){
												kriyaEditor.settings.summaryData.added[0] = targetElement;
											}
										}
										//old contition -if(kriyaEditor.settings.summaryData && (kriyaEditor.settings.summaryData.added.length > 0 || kriyaEditor.settings.summaryData)){
										if(kriyaEditor.settings.summaryData){
											$(kriyaEditor.settings.summaryData.added).each(function(){
												if(this.nodeType != 3){
													$(this).attr('id', uuid.v4());
													if($(kriyaEditor.settings.undoStack).filter($(this).parents()).length == 0){
														$(this).attr('data-inserted', 'true');
														//Rename the class to jrnlSecPara if new node is a jrnlEqnPara
														if($(this).hasClass('jrnlEqnPara')){
															$(this).removeClass('jrnlEqnPara');
															$(this).addClass('jrnlSecPara');
														}
														//data-id should not be duplicate after break the one element should remove data-id attr added by vijayakumar on 11-12-2018
														$(this).removeAttr('data-id');
														kriyaEditor.settings.undoStack.push(this);
													}
												}

											});
										}
										//End of #355

										if ($(kriyaEditor.settings.forkedNode).hasClass('activeElement')){
											$(kriyaEditor.settings.forkedNode).removeClass('activeElement');
										}
										kriyaEditor.settings.undoStack.push(kriyaEditor.settings.forkedNode);
										kriyaEditor.settings.undoStack.push(targetElement);
										//#535 -While entering two times,cursor is comes out of list but one extra list is added in last.
										kriyaEditor.settings.undoStack.push($(targetElement).prev());
										//End of #535
										kriyaEditor.init.addUndoLevel('navigation-key');
										kriyaEditor.settings.forkedNode = false;
									}else{
										kriyaEditor.init.addUndoLevel('character-key', targetElement);
									}
								}else if((event.ctrlKey || event.metaKey) && keycode == 86){
									//paste was handled in ice paste callback afterPaste function - jagan
								}else if((event.ctrlKey || event.metaKey) && keycode == 88){
									kriyaEditor.settings.undoStack.push(targetElement);
									kriyaEditor.init.addUndoLevel('cut (ctrl+x)');
								}
							}
				}

				//add/remove track changes to the side panel
				if (range.anchorNode.nodeType == 3){
					var parentNode = range.anchorNode.parentNode;
				}else if(kriya.selection && kriya.selection.startOffset != kriya.selection.endOffset){
				//Get the tracked elements when select the text and press delete or backspace key
					var parentNode = kriya.selection.startContainer.nextElementSibling;
					// if the current deleted node is the only child for its parent (inside table)
					if (! parentNode && $(kriya.selection.startContainer.parentNode).hasClass('del')){
						parentNode = kriya.selection.startContainer.parentNode;
					}
				}else{
					var parentNode = range.anchorNode;
				}
				if ($(parentNode).hasClass('del') || $(parentNode).hasClass('ins')){
					eventHandler.menu.observer.addTrackChanges(parentNode, 'added');
					//addUndoLevel('change', 'changesDivNode');
				}

				if ($(parentNode).siblings('.del:first,.ins:first').length > 0){
					//Add track changes for the inserted and deleted element when select the text and start typing
					eventHandler.menu.observer.addTrackChanges($(parentNode).siblings('.del:first,.ins:first')[0], 'added');
				}

				if ($(range.anchorNode).siblings('.del:first,.ins:first').length > 0){
					//Add track changes for the inserted and deleted element when select the text and start typing
					eventHandler.menu.observer.addTrackChanges($(range.anchorNode).siblings('.del:first,.ins:first')[0], 'added');
				}

				var underlayNode = $('#clonedContent #' + $(parentNode).closest('*[clone-id]').attr('clone-id'));
				if (underlayNode.length > 0 && $(underlayNode).find('.highlight').length > 0){
					eventHandler.menu.findReplace.reSearch(underlayNode);
				}

				if($(targetElement).closest('.jrnlArtTitle').length > 0){
					callUpdateArticleCitation($(targetElement).closest('.jrnlArtTitle'), '.jrnlCitation');
				}


			});
			//#509 - IE related - restricts resizing of border in IE - Rajasekar T (rajasekar.t@focusite.com)
			kriyaEditor.settings.contentNode.addEventListener( 'mscontrolselect', function( evt ) {
				evt.returnValue = false;
				return false;
			});
			//End of #509

			kriyaEditor.settings.contentNode.addEventListener('keydown', function(event){
				//console.log(rangy.getSelection().anchorNode.parentNode);
				//if menu item is active - jai
				/*if ($('.kriyaMenuControl.active').length > 0) {
					keycode = event.keyCode;
					if (keycode == 27){
						$('.kriyaMenuControl.active').removeClass('active');
					}
					event.preventDefault();
					event.stopPropagation();
				}else{*/
					var range = rangy.getSelection();
					var targetElement = range.anchorNode;
					if (targetElement && targetElement.nodeType == 3){
						targetElement = targetElement.parentNode;
					}

					//update citation if page type text to citation and type navigation key - jagan
					var clonedTarget = $(targetElement).clone(true);
					clonedTarget.find('.jrnlBibRef').remove();
					//if range.anchorNode is undefined code will break
					if(range.anchorNode && range.anchorNode.nodeType == 3 && $(targetElement).find('.jrnlBibRef:not([data-track="del"])').length > 0 && $(targetElement).hasClass('ins') && (event.keyCode >= 33 && event.keyCode <= 40) && $(clonedTarget).text().match(/[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/)){
						$(targetElement).html($(targetElement).html()); //to join all text nodes
						if($(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling && $(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling.nodeType == 3){
							targetElement = $(targetElement).find('.jrnlBibRef:not([data-track="del"])')[0].nextSibling;
						}
					}
					if(targetElement){
						var prevNode = $(targetElement)[0].previousSibling;
					}
					while(prevNode && prevNode.nodeType != 3 && $(prevNode).closest('.del, [data-track="del"]').length > 0){
						prevNode = $(prevNode)[0].previousSibling;
					}
					if((event.keyCode >= 33 && event.keyCode <= 40) && prevNode && $(prevNode).is('.jrnlBibRef:not([data-track="del"])') && citeJS.settings && citeJS.settings.R && citeJS.settings.R.citationType == "1"){
						var trackText = $(targetElement).text();
						var pageNumMatches = trackText.match(/^[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/);
						if(pageNumMatches && pageNumMatches[1]){
							trackText = trackText.replace(/^[\,\.\s]+pp?[\.\s]+(([\d]+)([\-\.][\d]+)?)/g, '');
							if($(targetElement)[0].nodeType == 3){
								$(targetElement)[0].nodeValue = trackText;
							}else{
								$(targetElement).text(trackText);
							}

							var pageNum = pageNumMatches[1];
							citeJS.general.updatePageNumInCitation(prevNode, pageNum);
							kriyaEditor.init.addUndoLevel('save-page-no', prevNode.closest('p'));
						}
					}

					if(targetElement && targetElement != null && (($('#contentContainer').attr('data-state') == 'read-only') || (targetElement.hasAttribute('data-editable') && targetElement.getAttribute('data-editable') == 'false') || (targetElement.hasAttribute('class') && /Block$/.test(targetElement.getAttribute('class'))) || $(targetElement.closest('*[data-editable="false"]')).length > 0 || $(targetElement.closest('*[data-track="del"]')).length > 0 || $(targetElement.closest('*[data-editable="false"]')).length > 0 || (kriya.config.preventTyping && $(targetElement).closest(kriya.config.preventTyping).length > 0))){
						keycode = event.keyCode || event.which;
						if (
						(keycode > 47 && keycode < 58) || // number keys
						keycode == 32 || keycode == 13 || // spacebar & return key(s) (if you want to allow carriage returns)
						keycode == 46 || keycode == 8 || // spacebar & return key(s) (if you want to allow carriage returns)
						(keycode > 64 && keycode < 91) || // letter keys
						(keycode > 95 && keycode < 112) || // numpad keys
						(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
						(keycode > 218 && keycode < 223) // [\]' (in order)
						){
						if ((event.ctrlKey && (!/^(X|V)$/i.test(event.key))) || event.altKey) {} else {
								if ($('.nonEditableMessage').length == 0){
									if((keycode == 8 || keycode == 46) && $(targetElement).closest('.jrnlDeleted').length > 0){
										//Do nothing - when delte key press and target is jrnlDeleted then don't show msg - jagan
									}else{
										var errMsg = "Editing is not allowed";
										if(kriya.config.preventTypingErMsg && typeof(kriya.config.preventTypingErMsg) == "object"){
											var classNames = Object.keys(kriya.config.preventTypingErMsg);
											for(var c=0;c<classNames.length;c++){
												var objKey = classNames[c];
												if($(targetElement).closest('.' + objKey).length > 0){
													errMsg = kriya.config.preventTypingErMsg[objKey];
													errMsg = errMsg.replace(/\{\$role\}/, kriya.config.content.role);
													break;
												}
											}
										}
										var notificationID = kriya.notification({
											title: 'Info',
											type: 'error',
											timeout: 5000,
											content: errMsg,
											icon: 'icon-warning2'
										});
										$('#'+notificationID).addClass('nonEditableMessage');
									}
								}
								event.preventDefault();
								event.stopPropagation();
							}
						}
					}
					switch (event.keyCode || event.which) {
						case 46: // delete
						case 13:
							if($(targetElement).closest('span[class$="Ref"], span[class$="Ref activeElement"],.jrnlTblFoot').length > 0){
								event.preventDefault();
								event.stopPropagation();
							}

							//handling enter
							while (targetElement && kriya.config.blockElements.indexOf(targetElement.nodeName.toLocaleLowerCase()) == -1){
								targetElement = targetElement.parentNode;
							}
							kriyaEditor.settings.forkedNode = targetElement;
							break;
						//case 9:
							//handling list when tab was clicked - JAGAN
							// this is now handled as wysihtml command, which already has a tab event in it - JAI 29-04-2017
						case 16:// shift+tab   //when we un list the items at that time it is not saving properlly and it is duplicating nodes-kirankumar
						case 9:// shift+tab
							//when we are click shift tab on paragraph then editor getting saved
							//if(event.shiftKey && event.keyCode == 9){
							//When evere shift+tab happend and if any node not deleted in editor at that time kriyaEditor.settings.summaryData.removed will come as undefined at that time here code code will break
							//if((event.shiftKey && event.keyCode == 9) && kriyaEditor.settings.summaryData.removed.length > 0){
							if((event.shiftKey && event.keyCode == 9) && (kriyaEditor.settings.summaryData.removed && kriyaEditor.settings.summaryData.removed.length > 0)){
								var startNode = $(rangy.getSelection().getRangeAt(0).startContainer.parentNode).closest('p');
								var endNode = $(rangy.getSelection().getRangeAt(0).endContainer.parentNode).closest('p');
								var endNodeId = $(endNode).attr('id');
								var startNodeId = $(startNode).attr('id');
								var selectedNodes = [];
								if(endNodeId != startNodeId){
									selectedNodes.push(startNode);
									var collectSelectedNodes = true;
									while(collectSelectedNodes){
										if($(startNode).next().attr('id') == endNodeId){
											selectedNodes.push(startNode.next());
											collectSelectedNodes = false;
										}else{
											selectedNodes.push(startNode.next());
											startNode = $(startNode).next();
										}
									}
								}else{
									selectedNodes.push(startNode);
								}
								for(var thisNode in selectedNodes){
									var currElement = selectedNodes[thisNode];
									if(($(currElement).closest('li').length == 0)&&($(currElement).hasClass('jrnlListPara'))){
										$(currElement).attr('id',uuid.v4())
										$(currElement).attr('class','jrnlSecPara').attr('data-inserted','true');

										if(($($(currElement).prev())[0].nodeName == 'UL')||($($(currElement).prev())[0].nodeName == 'OL')){
											var prevList = $(currElement).prev();
											//var prevListType = $(prevList).attr('type');
											//var prevListStyle = $(prevList).attr('style');
											kriyaEditor.settings.undoStack.push(prevList);
										}
										if(($($(currElement).next())[0].nodeName == 'UL')||($($(currElement).next())[0].nodeName == 'OL')){
											var nextList = $(currElement).next().attr('data-inserted','true');
											$(nextList).attr('type',$(nextList).css('list-style-type'));
										//$(nextList).attr('style',prevListStyle);
										kriyaEditor.settings.undoStack.push(nextList);
										}
										kriyaEditor.settings.undoStack.push(currElement);
									}
								}

								//When we unlist the list items at that time old list is not removing in xml it will remove old list-kirankumar
								if(kriyaEditor.settings.summaryData && kriyaEditor.settings.summaryData.removed.length > 0){
									$(kriyaEditor.settings.summaryData.removed).each(function(){
										if(this.nodeType != 3 && $(this).attr('id')){
											var removedId = $(this).attr('id');
											var removeNode = this;
											if($(kriya.config.containerElm+ ' #'+removedId).length < 1){
												var check = false;
												// Don't include if the node is child of any removed node
												$(kriyaEditor.settings.summaryData.removed).each(function(){
													if($(this).find('#'+removedId).length > 0){
														check = true;
													}
												});
												if(!check){
													$(removeNode).attr('data-removed', 'true');
													kriyaEditor.settings.undoStack.push(removeNode);
												}
											}
										}
									});

									if(kriyaEditor.settings.summaryData.reparented.length > 0){
										var reparentSaveNode = $(kriyaEditor.settings.summaryData.reparented).parents('[id]:first');
										if(reparentSaveNode.length > 0){
											if($(reparentSaveNode).closest('ul[id],ol[id]').length > 0){
												reparentSaveNode = $(reparentSaveNode).closest('ul[id],ol[id]');
											}
											kriyaEditor.settings.undoStack.push(reparentSaveNode);
										}
									}

									var mergedNode = $(targetElement).closest('p[id],div[id],table[id],h1[id],h2[id],h3[id],h4[id],h5[id],h6[id],li[id],ol[id]');
									kriyaEditor.settings.undoStack.push(mergedNode);
									//kriyaEditor.init.addUndoLevel('merge-node');
									kriyaEditor.settings.summaryData = false;

								}else{
									kriyaEditor.settings.undoStack.push(targetElement);
								}
								if(kriyaEditor.settings.undoStack.length > 0){
									kriyaEditor.init.addUndoLevel('shift+tab');
								}
							}
							break;
						case 8: // backspace
							//console.log('add undo level called, delete/backspace keys pressed');
							break;
					}
				//}

				// short cut key controls
				var sk = "";
				if (event.ctrlKey) sk += "ctrl+";
				if (event.metaKey) sk += "ctrl+";
				else if (event.altKey) sk += "alt+";
				else if (event.shiftKey) sk += "shift+";
				if (! /(control|alt|shift)/i.test(event.key)){
					sk += event.key;
				}
				if (sk.match(/\"/)) {
					sk = "";
				}
				//keysPressed[event.keyCode] = event.key.toLocaleLowerCase();
				//keysPressed.push(event.key.toLocaleLowerCase());
				//sk = keysPressed.join('+');
				//sk = sk.replace('control', 'ctrl');
				if ($('[data-shortcut="'+sk+'"]').length > 0){
					if ($('[data-shortcut="'+sk+'"]').attr('data-action') != undefined){
						var action = $('[data-shortcut="'+sk+'"]').attr('data-action');
						if (action == "focus"){
							$('[data-shortcut="'+sk+'"]').focus();
						}else{
							$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
						}
					}else{
						$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
					}
					event.preventDefault();
					event.stopPropagation();
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('mouseleave', function(event){
				var range = rangy.getSelection();
				if (range.rangeCount > 0 && range.text() != ""){
					var r = range.getRangeAt(0).startContainer.parentElement
					if ($(r).closest('#contentDivNode').length > 0){
						//kriya.selection = range.getRangeAt(0);
						//kriya.selectedNodes = kriya.selection.getNodes();
						//findReplace.general.highlight(range, 'textSelection');
					}
				}
			});
			kriyaEditor.settings.contentNode.addEventListener('click', function(event){
				if(stack.isDirty && kriyaEditor.lastSelection){
					var targetElement = kriyaEditor.lastSelection.anchorNode;
					kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
				}
				$('.highlight.textSelection,.highlight.queryText').remove();

				//Remove the foot note if newly insrted is empty
				$('.jrnlTblFoot:empty').not($(event.target).closest('.jrnlTblFoot')).remove();
				var emptyEthic = $('.jrnlEthicsFN[data-save-node] p:empty');
				if(emptyEthic.not($(event.target).closest(emptyEthic)).length > 0){
					emptyEthic.parent().remove();
				}
				$('.dropdown-button').dropdown('close');

				var hrefLink = '';
				if($(event.target).closest('a').length > 0 && $(event.target).closest('a').attr('href')){
					hrefLink = $(event.target).closest('a').attr('href');
				}
				if($(event.target).closest('.jrnlSupplSrc') && $(event.target).closest('.jrnlSupplSrc').attr('href')){
					hrefLink = $(event.target).closest('.jrnlSupplSrc').attr('href');
				}
				if(hrefLink != ''){
					window.open(hrefLink);
				}

			});

			kriyaEditor.settings.contentNode.addEventListener('focusout', function(event){
				if(stack.isDirty && kriyaEditor.lastSelection){
					var targetElement = kriyaEditor.lastSelection.anchorNode;
					kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
				}
			});

			//Initialize the scroll to table for handling search highlight
			$(kriya.config.containerElm + ' .jrnlTblContainer').each(function(){
				this.addEventListener('scroll', function(event){
					var tableID = $(this).find('table').attr('id');
					$('#clonedContent div[data-table-id="' + tableID + '"]').scrollTop($(this).scrollTop());
					//Initialize the horizontal scroll to table for handling search highlight - aravind
					$('#clonedContent div[data-table-id="' + tableID + '"]').scrollLeft($(this).scrollLeft());
				});
			});
			kriyaEditor.settings.contentNode.addEventListener('scroll', function(event){
				$('#clonedContent').scrollTop($('#contentDivNode').scrollTop())
				$('.suggestions,.resolve-query').remove();
				if (!$(".kriya-tags-suggestions").hasClass( "hidden" )){
					$('.kriya-tags-suggestions').addClass('hidden');
				}
				setTimeout(function(){
					if ($('.query-div.hover-inline').find('.cancelReply').length > 0){
						//if we open authore popup and scroll at that time two js events triggering (scroll,focusout),in this situation same function alling two time for restricting
						//that i added temp-data-removed attr.this attr life time before compleate this scroll event.
						$('.query-div.hover-inline').find('.cancelReply').attr('temp-data-removed','true');
						$('.query-div.hover-inline').find('.cancelReply').trigger('click');
					}else if ($('.query-div.hover-inline').find('.com-close').length > 0){
						$('.query-div.hover-inline').find('.com-close').trigger('click');
					}else{
						$('.query-div.disabled').remove();
						$('.query-div.hover-inline').removeAttr('style').removeClass('hover-inline');
					}
					$('.highlight.queryText_test,style.queryCss,.image-hover').remove();
				},100);
				if ($('.getBack').length > 0){
					var floatXpath = $('.getBack').attr('data-float-xpath');
					var floatnode = kriya.xpath($('.getBack').attr('data-float-xpath'));
					if(floatnode.length > 0){
						if ($(floatnode).position().top + $(floatnode).height() < 0 || $(floatnode).position().top > $('#contentDivNode').height()){
							$('.getBack').remove();
						}
					}
				}
				//addundo level already handled in focus out and it's slowing the scroll event
				/*if(stack.isDirty && kriyaEditor.lastSelection){
					var targetElement = kriyaEditor.lastSelection.anchorNode;
					kriyaEditor.init.addUndoLevel('navigation-key', targetElement);
				}*/
				var blockMenu = $('[data-component*="_menu"][data-component^="jrnl"]:visible');
				if(blockMenu.length > 0){
					$(blockMenu).css({
						'display' : '',
						'opacity' : '0',
						'left' : '',
						'top' : '',
					});
				}
			});

			/*document.getElementsByTagName('body')[0].addEventListener('keydown', function(event){
				if ($(event.target).closest('#contentDivNode').length > 0) return false;
				// short cut key controls
				var sk = "";
				if (event.ctrlKey) sk += "ctrl+";
				else if (event.altKey) sk += "alt+";
				else if (event.shiftKey) sk += "shift+";
				if (! /(control|alt|shift)/i.test(event.key)){
					sk += event.key;
				}
				if (sk.match(/\"/)){
					sk = "";
				}
				//commented by kirankumar issue:-ctrl+z not working in popup
				if ($('[data-shortcut="'+sk+'"][data-context="body"]').length > 0){
					if ($('[data-shortcut="'+sk+'"]').attr('data-action') != undefined){
						var action = $('[data-shortcut="'+sk+'"]').attr('data-action');
						if (action == "focus"){
							$('[data-shortcut="'+sk+'"]').focus();
						}else{
							$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
						}
					}else{
						$('[data-shortcut="'+sk+'"]:not(.disabled)').trigger('click');
					}
					event.preventDefault();
					event.stopPropagation();
				}
			});*/

			function stackUI() {
				if(stack.canRedo()){
					kriyaEditor.settings.redo.classList.remove("disabled")
				}else{
					kriyaEditor.settings.redo.classList.add("disabled")
				}

				if(stack.canUndo() && kriyaEditor.settings.save){
					kriyaEditor.settings.undo.classList.remove("disabled");
					kriyaEditor.settings.save.classList.remove("disabled");
					$('.pdfViewHeader .pdfViewMenu').removeClass('disabled');
					$('.pdfViewHeader #pdfNotice').removeClass('hidden');
				}else{
					//#348 All| Tables| Undo Redo cycle must not get affected after Accept/ Reject - Prabakatran.A(prabakaran.a@focusite.com)
					//#533 Undo is not working - ReferenceError: event is not defined - Prabakaran A(prabakaran.a@focusite.com)
					if(typeof event != 'undefined' && !$(event.currentTarget).is('[data-name="MenuBtnUndo"]')){
						kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;

						var sel = rangy.getSelection();
						var blockNode = $(sel.anchorNode).closest('[id]:not(span)');
						kriyaEditor.settings.startCaretID = blockNode.attr('id');
						kriyaEditor.settings.startCaretPos = getCaretCharacterOffsetWithin(blockNode[0]);

						kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
					}
					//End of #348
					if (kriyaEditor.settings.save){
						kriyaEditor.settings.undo.classList.add("disabled");
						kriyaEditor.settings.save.classList.add("disabled");
					}
				}
			}

			kriyaEditor.settings.undo.addEventListener("click", function () {
				var range = rangy.getSelection();
				var targetElement = range.anchorNode;
				if (stack.isDirty){
					kriyaEditor.init.addUndoLevel('undo-key', targetElement);
				}
				//#178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				if($('[data-type="popUp"]:not([data-component="contextMenu"]):not([data-component="trackChange"]):visible').length == 0 || $('[data-name="tableMenu"]:visible').length > 0)
				//End of the code #178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				stack.undo();
			});

			kriyaEditor.settings.redo.addEventListener("click", function () {
				//#178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				if($('[data-type="popUp"]:visible').length == 0 ||  $('[data-name="tableMenu"]:visible').length > 0)
				//End of the code #178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				stack.redo();
			});


			stack.changed = function () {
				stackUI();
			};
		},
		addUndoLevel: function(type, element){
			var stackObj = [];
			/*var newElement = null;
			var oldValue   = null;
			var newValue   = null;*/

			//Add content div node to undo object
			var newElement = kriyaEditor.settings.contentNode;
			var oldValue   = kriyaEditor.settings.startValue;

			var oldCaretPos = kriyaEditor.settings.startCaretPos;
			var oldCaretId  = kriyaEditor.settings.startCaretID;

			var newValue   = newElement.innerHTML;

			if ((type == 'delete-key') || (type == 'character-key')) {
				stack.isDirty = true;
				stack.oldValue = oldValue;
				stack.newValue = newValue;
				stack.oldCaretPos = oldCaretPos;
				stack.oldCaretId = oldCaretId;

				if (kriyaEditor.settings.save){
					(stack.isDirty)?(
						kriyaEditor.settings.undo.classList.remove("disabled"),
						kriyaEditor.settings.save.classList.remove("disabled")
					):(
						kriyaEditor.settings.undo.classList.add("disabled"),
						kriyaEditor.settings.save.classList.add("disabled")
					);
				}
				return;
			}

			if (kriyaEditor.settings.undoStack.length > 0){
				element = kriyaEditor.settings.undoStack;
			}
			kriyaEditor.settings.undoStack = [];
			if(element && $(element).length > 0){
				//Collect the all ids of the elements that add in the stack
				//If element doesn't have ids then get closest one
				//#395-All| List| Unable to revert List to para( #364 Requirement changed added para) -Prabakaran.A-prabakaran.a@focusite.com
				//In lists, when LI is converted to paragraph, <p> is removed and inserted as new paragraph. The edited list must be saved before the inserted tag.
				var saveNodeIdsTemp = {
					'removed': [],
					'edited': [],
					'inserted': [],
					'moved': []
				};
				var saveNodeIds = [];
				//End of #395-All| List| Unable to revert List to para( #364 Requirement changed added para) -Prabakaran.A-prabakaran.a@focusite.com
				var elementsToSave = element;
				$(element).each(function(i, obj){
					if($(this).closest('#compDivContent').length > 0){
						return; //If closest node is a component content then continue the loop
					}
					if($(this).closest('[id]:not(' + kriya.config.invalidSaveNodes + ')').length > 0){
						var saveNode = $(this).closest('[id]:not(' + kriya.config.invalidSaveNodes + ')');
						//to avoid saving newly inserted span, sup, sub, em, italic, underlines, bolds - JAI
						//Check the length of closest save node - jagan
						if (/^(S|E|I|U|B)/i.test($(saveNode)[0].nodeName) && $(saveNode).parent().closest('[id]:not(' + kriya.config.invalidSaveNodes + ')').length > 0){
							saveNode = $(saveNode).parent().closest('[id]:not(' + kriya.config.invalidSaveNodes + ')');
						}
						//when ids duplicate inside the save node duplicated ids replaced with new ids-kirankumar
						var ids=[];
						$($(saveNode).find('[id]')).each(function(){
							var pid=$(this).attr('id');
							if(ids.indexOf(pid) == -1){
								ids.push(pid);
							}else{
								$(saveNode).find('[id = '+pid+']').each(function(){
									$(this).attr('id',uuid.v4());
								});
							}
						});
						//Before saving the content add id to all elements which doesn't have id in save node.
						$(saveNode).find('*:not([id])').each(function(){
							// to restrict adding id to child nodes - aravind
							if(!$(this).is(kriya.config.preventAddId)){
								$(this).attr('id',uuid.v4());
							}
						});
						//Filter the invalid nodes to check pushToArray modify by jagan
						var pushToArray = true;
						$(elementsToSave).filter(function(){
							if(!$(this).is(kriya.config.invalidSaveNodes)){
								return this;
							}
						}).each(function(){
							if ($(this).attr('id') && $(this).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						//check if the current node is a child of a node which is already in the saving list - jai
						$.each(saveNodeIds.inserted, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						$.each(saveNodeIds.moved, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						$.each(saveNodeIds.edited, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})

						//var saveNode = $(this).siblings('[id]:not(' + kriya.config.invalidSaveNodes + ')').prevObject;
						// here we are collecting changes in an array as object with action as key and id as value
						// "saveNodeIdsTemp" variable is only used for validation purpose
						// validation: to avoid duplicate id getting collected in "saveNodeIds" collection
						if(saveNode[0].hasAttribute('data-removed')){
							if (saveNodeIdsTemp.removed.indexOf(saveNode.attr('id')) < 0){
								saveNodeIdsTemp.removed.push(saveNode.attr('id'));
								saveNodeIds.push({'removed':saveNode.attr('id')});
							}
						}else if(saveNode[0].hasAttribute('data-inserted')){
							if (saveNodeIdsTemp.inserted.indexOf(saveNode.attr('id')) < 0 && pushToArray){
								saveNodeIdsTemp.inserted.push(saveNode.attr('id'));
								saveNodeIds.push({'inserted':saveNode.attr('id')});
							}
						}else if(saveNode[0].hasAttribute('data-moved')){
							if (saveNodeIdsTemp.moved.indexOf(saveNode.attr('id')) < 0 && pushToArray){
								saveNodeIdsTemp.moved.push(saveNode.attr('id'));
								saveNodeIds.push({'moved':saveNode.attr('id')});
							}
						}else{
							//#45 (5) - Bug fix - Saving all nodes, when formatting is applied to multiple nodes - Prabakaran. A(prabakaran.a@focusite.com)
                            $(saveNode).each(function(){
                                if (saveNodeIdsTemp.edited.indexOf($(this).attr('id')) < 0 && pushToArray){
									saveNodeIdsTemp.edited.push($(this).attr('id'));
									saveNodeIds.push({'edited':$(this).attr('id')});
                                }
                            })
                            //End of #45 (5)
						}
					}else{
						console.log(this);
					}
				});
				stackObj.push({
					'saveNodes': saveNodeIds
				});
			}

			var sel = rangy.getSelection();
			var blockNode = $(sel.anchorNode).closest('[id]:not(span)');
			var blockID = blockNode.attr('id');
			var caretPosinBlock = getCaretCharacterOffsetWithin(blockNode[0]);
			//if we change any node values in this function before this line that will not saving in undo stack,when we undo after redo that changed values not comming for that changed nodes.
			newValue   = newElement.innerHTML;
			stackObj.push({
				container: newElement,
				oldValue: oldValue,
				newValue: newValue,
				caretPos: caretPosinBlock,
				caretBlock: blockID,
				oldCaretPos: oldCaretPos,
				oldCaretId: oldCaretId
			});

			//Adding right panel in the stack
			stackObj.push({
				container: kriyaEditor.settings.changeNode,
				oldValue : kriyaEditor.settings.contextStartValue,
				newValue : kriyaEditor.settings.changeNode.innerHTML
			});

			//add clone element in the stack
			stackObj.push({
				container: kriyaEditor.settings.cloneNode,
				oldValue : kriyaEditor.settings.cloneValue,
				newValue : kriyaEditor.settings.cloneNode.innerHTML
			});

			var EditCommand = Undo.Command.extend({
				constructor: function (undoObj) {
					this.undoObject = undoObj;
				},
				execute: function () {
					this.changed('execute');
				},
				undo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(!item.saveNodes){
							item.container.innerHTML = item.oldValue;
							if(item.oldCaretPos && item.oldCaretId){
								setCursorPosition($('#' + item.oldCaretId)[0], item.oldCaretPos);
							}
						}
					});
					this.changed('undo');
				},
				redo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(!item.saveNodes){
							item.container.innerHTML = item.newValue;
							if(item.caretPos && item.caretBlock){
								setCursorPosition($('#' + item.caretBlock)[0], item.caretPos);
							}
						}
					});
					this.changed('redo');
				},
				changed: function (type) {
					this.undoObject.forEach(function(item, index, array){
						if(item.saveNodes){
							var saveNodes = [];
							//Collect the all save elemets and send it to the save function
							// collecting method is modified as now saveNodes is an array, which have collection objects with action as key and id as value
							Object.keys(item.saveNodes).forEach(function(keys) {
								var nodes;
								Object.keys(item.saveNodes[keys]).forEach(function(key){
									//When undo the chnage inseted node should be removed and removed node should be insert
									var val = item.saveNodes[keys][key];
							    	if(type == "undo" && key == "inserted"){
							    		nodes = $('<span id="' + val + '" data-removed="true"></span>')[0];
							    	}else if(type == "undo" && key == "removed"){
							    		$('#' + val).attr('data-inserted', 'true');
							    		nodes = $('#' + val)[0];
							    	}else{
								    	if(key == 'removed'){
								    		nodes = $('<span id="' + val + '" data-removed="true"></span>')[0];
								    	}else if(key == 'moved'){
								    		nodes = $('#' + val).attr('data-moved', 'true');
								    	}else{
								    		nodes = $('#' + val)[0];
								    	}
							    	}
								});
								saveNodes = saveNodes.concat(nodes);
							});
							// added this below rule bcoz while undo is triggered
							// content disappear, to handle this we have reversed save node
							if(type == "undo"){
								saveNodes = $(saveNodes).reverse();
							}
							kriyaEditor.init.save(saveNodes);
						}
					});
					if(type == "undo" || type == "redo"){
						citeJS.general.updateStatus();
					}
					//exclude if role is author
					if((kriya.config.content.role != 'author') && (kriya.config.content.role != 'publisher')){
						kriya.config.content.changes = true;
					}
					 //Added By Anuraja to iniate PDF viewer iframe onload event after undo and redo
					 if (location.pathname.match(/(proof_review)/)){
						$('#pdfViewer').on("load", function() {
							callPDFViewer();
						});
					 }
					//to handle undo in list
					//previously after undo of first change, startValue is not updated
					kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;

					var sel = rangy.getSelection();
					var blockNode = $(sel.anchorNode).closest('[id]:not(span)');
					kriyaEditor.settings.startCaretID = blockNode.attr('id');
					kriyaEditor.settings.startCaretPos = getCaretCharacterOffsetWithin(blockNode[0]);

					kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;
					kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
				}
			});

			stack.execute(new EditCommand(stackObj));
			stack.isDirty = false;

			return;
		},
		save: function(saveNodes){
			if ($('#contentContainer').attr('data-state') == "read-only") return false;
			//if (arguments.callee.caller == null) return false;
			if(saveNodes && saveNodes.length < 1){
				return false;
			}
			$('.save-notice').text('Saving...');
			//The below line was commented because it consuming more time when typing - jagan
			//callUpdateArticleCitation(saveNodes,'.jrnlCitation');
			if(kriya.config.content.doi && kriya.config.content.customer && saveNodes){
				var content_element = document.createElement('content');
				$(saveNodes).each(function(){
					if($(this)[0].length == 0){
						return true;
					}
					var saveNode = $(this);

					//If the save node is inside a float block then save the block element - Jai 13-02-2018
					if($(this).closest('[data-id*="BLK"]').length > 0){
						saveNode = $(this).closest('[data-id*="BLK"]');
					}else if($(this).closest('.jrnlRefText').length > 0){
						saveNode = $(this).closest('.jrnlRefText');
					}

					if($(this).closest('[data-save-node][id]').length > 0){
						saveNode = $(this).closest('[data-save-node][id]');
						saveNode.removeAttr('data-save-node');
					}

					var prevElement = $(saveNode)[0].previousSibling;
					var nextElement = $(saveNode)[0].nextSibling;

					//If next and prev sibling or next sibling is a white space then get the sibling sibling
					//If text node value is more than a space then take previous sibiling - priya
					if(prevElement && prevElement.nodeType == 3 && prevElement.nodeValue.trim() == ""){
						prevElement = prevElement.previousSibling;
					}
					if(nextElement && nextElement.nodeType == 3 && nextElement.nodeValue.trim() == ""){
						nextElement = nextElement.nextSibling;
					}

					//If sibling element is jrnlTblContainer then get the sibling as table inside that container - jagan
					if($(prevElement).attr('class') == "jrnlTblContainer" && $(prevElement).find('table').length > 0){
						prevElement = $(prevElement).find('table')[0];
					}

					if($(nextElement).attr('class') == "jrnlTblContainer" && $(nextElement).find('table').length > 0){
						nextElement = $(nextElement).find('table')[0];
					}

					//if the savenode match selector given in kriya.config.sameClassSiblingId
					//then previous and next element class should match the savenode class
					//ex: corresponding affiliation
					if($(saveNode).closest(kriya.config.sameClassSiblingId).length > 0 && $(saveNode).attr('class')){
						var saveNodeClass = $(saveNode).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
						if(prevElement && $(prevElement).attr('class')){
							var prevNodeClass = $(prevElement).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
							if(prevNodeClass != saveNodeClass){
								prevElement = null;
							}
						}
						if(nextElement && $(nextElement).attr('class')){
							var nextNodeClass = $(nextElement).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
							if(nextNodeClass != saveNodeClass){
								nextElement = null;
							}
						}
					}

					saveNode = $(saveNode).clone(true);

					//remove the date picker class
					saveNode.removeClass('picker__input--target');
					saveNode.removeClass('activeElement');

					if(!$(saveNode).is(kriya.config.preventSiblingId)){
						var siblingIdStatus = false;
						//Add prev id if prev element is not text element and it has id
						if(saveNode && $(prevElement).length > 0 && prevElement.nodeType != 3 && prevElement.hasAttribute('id')){
							saveNode.attr('data-prevID', prevElement.getAttribute('id'));
							siblingIdStatus = true;
						}
						//Add next id if next element is not text element and it has id
						if(saveNode && $(nextElement).length > 0 && nextElement.nodeType != 3 && nextElement.hasAttribute('id')){
							saveNode.attr('data-nextID', nextElement.getAttribute('id'));
							siblingIdStatus = true;
						}
						//if the element is inserted and prev, next element is text node then save the closest node which has id
						if(!siblingIdStatus && saveNode[0].hasAttribute('data-inserted')){
							if($(this).parents('[id]:not(' + kriya.config.invalidSaveNodes + '):first').length > 0){
								saveNode = $(this).parents('[id]:not(' + kriya.config.invalidSaveNodes + '):first');
								saveNode = $(saveNode).clone(true);
							}
						}
					}

					//nextID OR prevID is mandatory for moved save nodes
					//Remove the data-moved attr if save node doesn't have sibling id
					if(saveNode && saveNode.attr('data-moved') == "true" && !saveNode.attr('data-nextID') && !saveNode.attr('data-prevID')){
						saveNode.removeAttr('data-moved');
					}

					if(saveNode.length > 0){
						content_element.appendChild(saveNode[0]);
					}
				});

				$(content_element).find('.activeElement').removeClass('activeElement'); //Remove the active element class
				$(content_element).find('.wysiwyg-tmp-selected-cell').removeClass('wysiwyg-tmp-selected-cell');
				$(content_element).find('.floatHeader').remove();

				$(content_element).find('.kriya-nbsp,.kriya-ensp,.kriya-emsp,.kriya-thin,.kriya-lnbreak').contents().unwrap();
				$(content_element).find('.kriya-nbsp:empty,.kriya-ensp:empty,.kriya-emsp:empty,.kriya-thin:empty,.kriya-lnbreak:empty').remove();

				var contentHTML = content_element.outerHTML;
				contentHTML = contentHTML.replace(/[ ]*(&nbsp;)+|(&nbsp;)+ +/g, ' '); //modify by jagan
				contentHTML = contentHTML.replace(/[ ]+/g, ' ');
				//return false;

				if(content_element.innerHTML != ""){
					kriya.config.saveQueue.push(contentHTML);
				}
				
				if(kriya.config.saveQueue.length == 1){
					kriyaEditor.init.saveQueueData();
				 }
				 if (location.pathname.match(/(proof_review)/)){
					updateContentViewNavigationPanel();
				}
				return false;
			}
		},
		saveQueueData: function(){
			if(!window.navigator.onLine){ // to prevent save if there is know internet connectivity
				return false;
			}
			if(kriya.config.saveQueue.length == 0){
				return false;
			}

			var formData = new FormData();
			formData.append('customer', kriya.config.content.customer);
			formData.append('doi', kriya.config.content.doi);
			formData.append('journal', kriya.config.content.project);
			if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
				formData.append('occurrence', '1');
			}else{
				formData.append('occurrence', '2');
			}
			var blob = new Blob([kriya.config.saveQueue[0]], {
				type: 'text/html'
			});
			//var fileToBeUp = new File([blob], 'save_data.html');
			//formData.append('file', fileToBeUp, fileToBeUp.name);
			formData.append('file', blob, 'save_data.html');
			$('.save-notice').text('Saving...');
			$.ajax({
				url: '/api/save_content',
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function(res){
					res = $('<response>'+res+'</response>');
					if(res.find('exception').length > 0){
						if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
							kriya.config.errorSaveQueue.push(kriya.config.saveQueue[0]);
						}else{
							kriya.config.saveQueue.splice(0,1);
							var settings = {
								'icon'  : '<i class="material-icons" style="margin-top: 3rem;margin-left: 3rem;color: red;">warning</i>',
								'title' : 'Content saving failed',
								'text'  : ' ',
								'size'  : 'pop-sm'
							};
							kriya.actions.confirmation(settings);
							$('.save-notice').text('Content saving failed');
						}
						kriyaEditor.init.saveQueueData();
					}else{
					//if (res && res.status && res.status == 'success'){
						kriyaEditor.settings.save.classList.add("disabled");
						$('[data-inserted]').removeAttr('data-inserted');
						$('[data-removed]').removeAttr('data-removed');
						$('[data-moved]').removeAttr('data-moved');
						var d = new Date();
						var hours = d.getHours();
						var minutes = d.getMinutes();
						var ampm = hours >= 12 ? 'pm' : 'am';
						hours = hours % 12;
						hours = hours ? hours : 12; // the hour '0' should be '12'
						minutes = minutes < 10 ? '0'+minutes : minutes;
						var strTime = hours + ':' + minutes + ' ' + ampm;
						$('.save-notice').text('All changes saved at ' + strTime);

						if(kriya.config.exportOnsave && typeof(eventHandler.menu.export[kriya.config.exportOnsave]) == "function"){
							eventHandler.menu.export[kriya.config.exportOnsave]('','',false);
							kriya.config.exportOnsave = null;
						}
						kriya.config.saveQueue.splice(0,1);
						kriyaEditor.init.saveQueueData();
					/*}else{
						kriya.config.saveQueue.splice(0,1);
						var settings = {
							'icon'  : '<i class="material-icons" style="margin-top: 3rem;margin-left: 3rem;color: red;">warning</i>',
							'title' : 'Content saving failed',
							'text'  : ' ',
							'size'  : 'pop-sm'
						};
						kriya.actions.confirmation(settings);
						$('.save-notice').text('Content saving failed');
					}*/
					}
				},
				error: function(err){
					// commented by JAI - to stop sending all the nodes to retry saving it again
					// Here only the unsave nodes has to be sent again, which is a YTD
					if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
						kriya.config.errorSaveQueue.push(kriya.config.saveQueue[0]);
					}else{
						kriya.config.saveQueue.splice(0,1);
						var settings = {
							'icon'  : '<i class="material-icons" style="margin-top: 3rem;margin-left: 3rem;color: red;">warning</i>',
							'title' : 'Content saving failed',
							'text'  : ' ',
							'size'  : 'pop-sm'
						};
						kriya.actions.confirmation(settings);
						$('.save-notice').text('Content saving failed');
					}
					kriyaEditor.init.saveQueueData();
				}
			});
		}
	}
	return kriyaEditor;

})(kriyaEditor || {});

function handleChanges(summary){

	if(kriyaEditor.settings){
		kriyaEditor.settings.summaryData = summary[0];
	}

	if (summary[0].removed.length > 0){
		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {
						'funcToCall': 'nodeChange',
						'param': {
							'node': summary[0].removed,
							'type': 'removed'
						}
					},
					event: 'click',
					target: ''
				}
			});
		},10)
	}

	//Added nodes used for saving when press enter
	//Used in keyup event
	if (summary[0].added.length > 0){

		//When enter key pressed after typing in content track change node was duplicate in new para
		//loop the added element if ele is a track node and text is null and more than one same cid is in content
		//then remove ele in added array and unwrap ele from  content
		for(i=0;i<summary[0].added.length;i++){
			var addedEle = summary[0].added[i];
			if(($(addedEle).hasClass('ins') || $(addedEle).hasClass('del')) && $(addedEle).text() == "" && addedEle.hasAttribute('data-cid')){
				var cid = $(addedEle).attr('data-cid');
				var eleClass = $(addedEle).attr('class').split(' ')[0];
				if($(kriya.config.containerElm + ' .'+eleClass+'[data-cid="' + cid + '"]').length > 1){
					$(addedEle).contents().unwrap();
					summary[0].added.splice(i, 1);
				}
			}
		}

		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {
						'funcToCall': 'nodeChange',
						'param': {
							'node': summary[0].added,
							'type': 'added'
						}
					},
					event: 'click',
					target: ''
				}
			});
		},10)
	}
}

function setupWatch(){
	var observer = new MutationSummary({
	  callback: handleChanges, // required
	  rootNode: document.getElementById('contentDivNode'), // optional, defaults to window.document
	  observeOwnChanges: false,// optional, defaults to false
	  oldPreviousSibling: false,// optional, defaults to false
		queries: [{
			all: true
		}]
	});
}
 /**
  * Added by ANuraja to display a split view like word on right panel
  */
 function updateContentViewNavigationPanel() {
 	var d = $('#contentContainer').clone()[0];
 	$('#contentviewDivNode #previewArea').html('');
 	$('#contentviewDivNode #previewArea').append(d);
	 $('#contentviewDivNode #previewArea .removeNode').remove();
	 $('#contentviewDivNode #previewArea .nano').removeClass('nano has-scrollbar');
	 $('#contentviewDivNode #previewArea .nano-content').removeClass('nano-content');
	 $('#contentviewDivNode #previewArea .content-scrollbar').remove();
 	$('#contentviewDivNode #previewArea .contentBtn').remove();
	 $('#contentviewDivNode #previewArea .btn').remove();
 	$('#contentviewDivNode #previewArea #contentContainer').find('*').each(function (index, element) {
 		var attrs = this.attributes;
 		var toRemove = [];
 		var element = $(this);
 		for (attr in attrs) {
 			if (typeof attrs[attr] === 'object' &&
 				typeof attrs[attr].name === 'string' &&
 				(/^(data-|removeNode|contenteditable|id|article-key|article-)/).test(attrs[attr].name)) {
 				toRemove.push(attrs[attr].name);
 			}
 		}
 		for (var i = 0; i < toRemove.length; i++) {
 			element.removeAttr(toRemove[i]);
		 }
	 });
	 $('#contentviewDivNode #previewArea #contentContainer').each(function (index, element) {
		var attrs = this.attributes;
		var toRemove = [];
		var element = $(this);
		for (attr in attrs) {
			if (typeof attrs[attr] === 'object' &&
				typeof attrs[attr].name === 'string' &&
				(/^(data-|removeNode|contenteditable|id|article-key|article-)(?!role)/).test(attrs[attr].name)) {
				toRemove.push(attrs[attr].name);
			}
		}
 		for (var i = 0; i < toRemove.length; i++) {
 			element.removeAttr(toRemove[i]);
 		}
 	});
	 var contentviewDivNode = $('#contentviewDivNode');
  if (($(contentviewDivNode).length > 0) && $(contentviewDivNode).is(':visible')) {
   		$(contentviewDivNode).css('height', (window.innerHeight - $(contentviewDivNode).offset().top - $('#footerContainer').height()) + 'px');
   		$(contentviewDivNode).parent('.nano').css('height', (window.innerHeight - $(contentviewDivNode).offset().top - $('#footerContainer').height()) + 'px');
   		$(contentviewDivNode).parent('.nano').nanoScroller({
   			alwaysVisible: true,
   			sliderMinHeight: 5,
   			preventPageScrolling: true
   		});
   		$(contentviewDivNode).parent().find('.nano-pane').addClass('content-scrollbar');
 	}
 }

 function callPDFViewer() {
 	if ($("iframe[id='pdfViewer'], iframe[id='pdfViewers']").length > 0) {
 		console.log("ready!");
 		$("iframe[id='pdfViewer'], iframe[id='pdfViewers']").contents().find("#viewerContainer").on("pagerendered", "#viewer", function (event) {
 			console.log('page rendered');
 			var allowInternalLinks = true;
 			var hyperlinks = $(this).find('a');
 			for (var i = 0; i < hyperlinks.length; i++) {
 				if (!allowInternalLinks || hyperlinks[i].className != 'internalLink') {
 					hyperlinks[i].onclick = function (e) {
 						$(e.target).attr('target', '_blank');
 					}
				} else {
 					var hrefid = $(hyperlinks[i]).attr('href').replace(/.*%3A(.*)%3A.*/, '$1');
 					$(hyperlinks[i]).attr('id', hrefid);
 					var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number')); //doing '-1' because index starts at '0'
 					$(hyperlinks[i]).closest('.linkAnnotation').wrap('<span id="' + hrefid + '" page="' + page + '" class="citation"></span>')
 					$(hyperlinks[i]).closest('.linkAnnotation').remove();
 					//$(hyperlinks[i]).closest('.linkAnnotation').addClass('citaion').removeClass('linkAnnotation');
 					//$(hyperlinks[i]).attr('href','');
				}
 			};
 		});
 		/**
 		 * hook onto the textlayerrendered event to add divs (for visual indication of editable area) using the coordinates obtained
 		 */
 		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("textlayerrendered", "#viewer", function (event) {
			var annoTation = $('<div class="markerLayer" style="' + $(event.target).attr('style') + '"></div>');
 			var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number') - 1); //doing '-1' because index starts at '0'
 			var ignoreProofControls = /author/;
 			// get the scale factor of the current view port
 			// this is important as the actual page width and the rendered page width differs and the scale factor helps restore the difference
 			var viewportScale = window.frames["pdfIFrame"].PDFViewerApplication.pdfViewer._pages[page].viewport.scale;
 			var currPageObj = coordinate[page];
 			if (currPageObj) {
 				Object.keys(currPageObj).forEach(function (id) {
 					var currParaObj = currPageObj[id];
					// ignore jrnlAuthGroup class tocreate (div)
					var ignoreClass = ".jrnlAuthorsGroup"; // add classes like .class1, class2
					var ignoreTag = false;
 					id = id.replace(/__[0-9]+$/, '');
					if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(ignoreClass)) {
						ignoreTag = true;
					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(ignoreClass)) {
						ignoreTag = true;
					}
					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)
					var proofControlAtt = '[data-word-spacing], [data-vj], [data-top-gap], .forceColBrk, .forceJustify';
 					proofControlIgnore = '.jrnlAuthor, .jrnlAuthorGroup, .jrnlArtTitle, .jrnlAff, .jrnlQueryRef'
 					var proofContTag = ' none';
 					if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(proofControlIgnore)) {
 						proofContTag = '';
 					} else if($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(proofControlIgnore)){
						proofContTag = '';
					 }else if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(proofControlAtt)) {
 						proofContTag = ' updated';
					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(proofControlAtt)) {
 						proofContTag = ' updated';
					}
 					if (kriya.config.content.role.match(ignoreProofControls)) {
 						proofContTag = '';
 					}
					var styleString = "";
					styleString += "top: " + (currParaObj[0] * viewportScale) + "px !important; ";
 					var proofStyleString = styleString + "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; width: " + (3 * viewportScale) + "px !important; left: " + ((currParaObj[1] * viewportScale) - 5) + "px !important; ";


 					var editBlocks = /(BLK_F|BLK_T)(\d+)/;
 					var proofcontrolsBlock = /^(BLK_F|BLK_T|R)(\d+)/;
 					if (id.match(editBlocks) && !kriya.config.content.role.match(ignoreProofControls)) {
 						proofContTag = '';
						var editStyleString = styleString + "height: 20px; width: 20px !important; left: " + ((currParaObj[1] * viewportScale) - (25 * viewportScale)) + "px !important; transform: scale(" + viewportScale + ")";
 						$('<div page="' + page + '" data-id=' + id + ' class="editMarker fa fa-gear" style="' + editStyleString + '">&nbsp;</div>').appendTo(annoTation);
					}
 					if (!id.match(proofcontrolsBlock) && !kriya.config.content.role.match(ignoreProofControls)) {
 						$('<div data-id=' + id + ' class="proofControls' + proofContTag + '" style="' + proofStyleString + '">&nbsp;</div>').appendTo(annoTation);
 					}
					if (!ignoreTag) {
 					styleString += "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; ";
 					styleString += "width: " + ((currParaObj[3] - currParaObj[1]) * viewportScale) + "px !important; ";
					styleString += "left: " + (currParaObj[1] * viewportScale) + "px !important; ";
					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)

						$('<div data-id=' + id + ' class="pdfMarker" style="' + styleString + '">&nbsp;</div>').appendTo(annoTation);
					}
 				});
				$(annoTation).appendTo($(event.target).closest('.page'))
 			}
 		});
		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .pdfMarker", function (event) {
			var divID = $(event.target).attr('data-id');
			if ($('#contentDivNode #' + divID).length > 0) {
				$('#contentDivNode #' + divID)[0].scrollIntoView();
				$('#contentDivNode #' + divID).trigger("click");
			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 			}
			 //Display edit option for Reference
 			if (divID.match(/R(\d+)/)) {
 				$('div[data-component="jrnlRefText"] > span:contains("Edit")').trigger('click')
			}
 		});
		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .proofControls", function (event) {
			var divID = $(event.target).attr('data-id');
 			var node;
			 var wordSpaceArray = [0.23, 0.28, 0, 0.38, 0.43];
			if ($('#contentDivNode #' + divID).length > 0) {
				$('#contentDivNode #' + divID)[0].scrollIntoView();
				$('#contentDivNode #' + divID).trigger("click");
 				node = $('#contentDivNode #' + divID)[0];
 				//$('#contentDivNode #' + divID).dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 				//$('#contentDivNode [data-id=' + divID + ']').dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
 				node = $('#contentDivNode [data-id=' + divID + ']');
			 }
				$(node).each(function () {
				$('#pdfProofControls #headerSpace').addClass('hidden');
				var proofHeaderSpace = '.jrnlHead1, .jrnlHead2, .jrnlHead3, .jrnlHead4, .jrnlHead5, .jrnlHead6, .jrnlRefHead';
				if($(node).is(proofHeaderSpace)){
					$('#pdfProofControls #headerSpace').removeClass('hidden');
				}
					$('#pdfProofControls').attr('data-id', divID);
 				$('#pdfProofControls input').prop('checked', false);
					$.each(this.attributes, function () {
						var proofControlAtt = '/data-word-spacing|data-vj|data-top-gap|forceColBrk|forceJustify/';
						if (this.specified) {
						 var value = this.value.replace(/(w)$/,'')
							if(this.name.match(/data-word-spacing|data-vj|data-top-gap|forceColBrk|forceJustify/)){
							 if($('#pdfProofControls input[value="' + value + '"][class=' + this.name + ']')>0){
								$('#pdfProofControls input[value="' + value + '"][class=' + this.name + ']').prop('checked', true)
							 }else{
								var count = closestNumber (wordSpaceArray, value)
								$('#pdfProofControls input[value="' + count + '"][class=' + this.name + ']').prop('checked', true)
							 }
								$('#pdfProofControls').attr(this.name, this.value);
							}
							console.log(this.name, this.value);
						}
					});
				});
				document.querySelector('[id="pdfProofControls"]').classList.remove("hidden");
				TomloprodModal.openModal('pdfProofControls');
 			if (divID.match(/BLK/)) {}
 		});
 		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .editMarker", function (event) {
 			var divID = $(event.target).attr('data-id');
 			$('#floatProofControls').attr('data-id', divID);
 			$('#floatProofControls input[type="radio"]').prop('checked', false);
 			$('#floatProofControls input').removeAttr('disabled');
 			$('#floatProofControls input[type="checkbox"]').prop('checked', false);
 			$('#floatProofControls input[type="number"]').val('')
 			$('[id="floatProofControls"] #imageSpace, [id="floatProofControls"] #imageSize ').addClass('hidden');
 			var node;
 			if ($('#contentDivNode #' + divID).length > 0) {
 				$('#contentDivNode #' + divID)[0].scrollIntoView();
 				//$('#contentDivNode #' + divID).trigger("click");
 				node = $('#contentDivNode #' + divID)[0];
 				//$('#contentDivNode #' + divID).dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
 			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
 				//$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 				//$('#contentDivNode [data-id=' + divID + ']').dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
 				node = $('#contentDivNode [data-id=' + divID + ']');
 	}
 			if (divID.match(/BLK_F(\d+)/)) {
 				$('[id="floatProofControls"] #floatName').text('Figure');
 				$('[id="floatProofControls"] #imageSpace').removeClass('hidden');
 				$(node).each(function () {
 					$.each(this.attributes, function () {
 						if (this.specified) {
 							console.log(this.name, this.value);
			}
	   });
 				});
 }
 			if (divID.match(/BLK_T(\d+)/)) {
 				$('[id="floatProofControls"] #floatName').text('Table');
 			}
 			//display if already proofcontrols applied
 			$(node).each(function () {
 				var cpage = parseInt($("iframe[id='pdfViewer']").contents().find('.page span.citation[id="' + divID + '"]').attr('page')) - 1;
 				console.log('cpage ', cpage);
 				if (cpage != '' && cpage != undefined && cpage != 'NaN') {
 					this.value = cpage + parseInt($(event.target).parents().closest('.page').attr('data-page-number'));
 					$('#floatProofControls #prevCitation').text(cpage);
 					$('#floatProofControls #cpage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number'))).val(cpage);
 					$('#floatProofControls #npage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number')) + 1).val(cpage + 1);
 					$('#floatProofControls #ppage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number')) - 1).val(cpage - 1);
 				}

 				$.each(this.attributes, function () {
 					var placeFloatsAtt = /data-page-num|data-top|data-top-gap|data-bot-gap|data-orientation|data-bot-gap|data-float-position|data-float-percent|data-column-start|data-float-placement/;
 					if (this.specified) {
 						if (this.name.match(placeFloatsAtt)) {
 							this.value = this.value.replace(/(pt|w)$/g, '')
 							$('#floatProofControls #prevCitation').text('');
 							$('#floatProofControls input[type="radio"][value="' + this.value + '"][class=' + this.name + ']').prop('checked', true)
 							$('#floatProofControls input[type="number"][class=' + this.name + ']').val(this.value)
 							$('#floatProofControls').attr(this.name, this.value);
 							$('#floatProofControls input.data-page-num[value="1"]').attr('disabled', '');
 							$('#floatProofControls input.data-page-num[value="0"]').attr('disabled', '');
 							$('#floatProofControls input.data-page-num[value="2"]').attr('disabled', '');
 						}
 						console.log(this.name, this.value);
 					}
 				});
 			});

 			document.querySelector('[id="floatProofControls"]').classList.remove("hidden");
 			TomloprodModal.openModal('floatProofControls');
 			/* document.querySelector('[id="floatProofControls"]').classList.remove("hidden");
 				TomloprodModal.openModal('floatProofControls');*/
 		});
 		$('#floatProofControls input[type="radio"]').on('click', function (e) {
 			console.log($(this).val());
 			var className = $(this).attr('class')
 			var selected = $(this).val();
 			$('input[class="' + className + '"]').prop('checked', false);
 			$('input[class="' + className + '"][value="' + selected + '"]').prop('checked', true);
 		})
 		$('#floatProofControls input[type="checkbox"]').on('click', function (e) {
 			console.log($(this).val());
 			var className = $(this).attr('class')
 			var selected = $(this).val();
 			var checked = $('input[class="' + className + '"][value="' + selected + '"]:checked').length
 			if ($(this).attr('display')) {
 				if (checked > 0) {
 					$('#floatProofControls [id="' + $(this).attr('display') + '"]').removeClass('hidden');
 				} else {
 					$('#floatProofControls [id="' + $(this).attr('display') + '"]').addClass('hidden');
 				}
 			}
 			$('input[class="' + className + '"]').prop('checked', false);
 			if (checked > 0) {
 				$('input[class="' + className + '"][value="' + selected + '"]').prop('checked', true);
 			}
 		})
 		$('input.data-word-spacing').on('click', function (e) {
 			console.log($(this).val());
 			var selected = $(this).val();
 			$('input.data-word-spacing').prop('checked', false);
 			$('input.data-word-spacing[value="' + selected + '"]').prop('checked', true);

 		});
 		$('input.data-top-gap').on('click', function (e) {
 			console.log($(this).val());
 			var selected = $(this).val();
 			$('input.data-top-gap').prop('checked', false);
 			$('input.data-top-gap[value="' + selected + '"]').prop('checked', true);

 		})
 	}
 }
 function callMainPDFViewer() {

 	/**
 	 * For main pdf
 	 */
 	/**
 	 * hook onto the textlayerrendered event to add divs (for visual indication of editable area) using the coordinates obtained
 	 */
 	if ($("iframe[id='pdfViewers']").length > 0) {
 		$("iframe[id='pdfViewers']").contents().find("#viewerContainer").on("textlayerrendered", "#viewer", function (event) {
 			var annoTation = $('<div class="markerLayer" style="' + $(event.target).attr('style') + '"></div>');
 			var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number') - 1); //doing '-1' because index starts at '0'
 			// get the scale factor of the current view port
 			// this is important as the actual page width and the rendered page width differs and the scale factor helps restore the difference
 			var viewportScale = window.frames["pdfIFrames"].PDFViewerApplication.pdfViewer._pages[page].viewport.scale;
 			var currPageObj = coordinate[page];
 			if (currPageObj) {
 				Object.keys(currPageObj).forEach(function (id) {
 					var currParaObj = currPageObj[id];
 					// apply jrnlQueryRef class tocreate (div)
 					var applyClass = ".jrnlQueryRef"; // add classes like .class1, class2
 					var applyTag = false;
 					var currentNodeClass = "";
 					id = id.replace(/__[0-9]+$/, '');
 					if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(applyClass)) {
 						applyTag = true;
 						currentNodeClass = $('#contentDivNode #' + id).attr('class');
 					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(applyClass)) {
 						applyTag = true;
 						currentNodeClass = $('#contentDivNode [data-id=' + id + ']').attr('class');
 					}

 					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)
 					var proofControlAtt = '[data-word-spacing], [data-vj], [data-top-gap], .forceColBrk, .forceJustify';
 					proofControlIgnore = '.jrnlAuthor, .jrnlAuthorGroup, .jrnlArtTitle, .jrnlAff'
 					var hoverString = '';
 					var styleString = "";
 					var extraClass = "";
 					var extraContent = "&nbsp;";
 					styleString += "top: " + (currParaObj[0] * viewportScale) + "px !important; ";
 					if (applyTag) {
 						if (currentNodeClass.match(/(jrnlQueryRef)/)) {
 							hoverString = $('#queryDivNode #' + $('#contentContainer #' + id).attr('data-rid')).find('.query-content').html();
 							extraClass = ' tooltip';
 							extraContent = '<span class="tooltiptext">' + hoverString + '</span>';
 						}
 						styleString += "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; ";
 						styleString += "width: " + ((currParaObj[3] - currParaObj[1]) * viewportScale) + "px !important; ";
 						styleString += "left: " + (currParaObj[1] * viewportScale) + "px !important; ";
 						// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)

 						$('<div data-id=' + id + ' class="pdfMarker' + extraClass + '" style="' + styleString + '">' + extraContent + '</div>').appendTo(annoTation);
 					}
 				});
 				$(annoTation).appendTo($(event.target).closest('.page'))
 			}
 		});
 		$("iframe[id='pdfViewers']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .pdfMarker", function (event) {
 			eventHandler.welcome.begin.startEditing()
 			var divID = $(event.target).attr('data-id');
 			if ($('#contentDivNode #' + divID).length > 0) {
 				$('#contentDivNode #' + divID)[0].scrollIntoView();
 				$('#contentDivNode #' + divID).trigger("click");
 			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
 				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 			}
 		});
 	}
 }
function callInitializeEvents(){
	eventHandler.publishers.add();
	eventHandler.subscribers.add();
	//#509 - added wicket good xpath initializer - Rajasekar T(rajasekar.t@focusite.com)
	wgxpath.install();
	if ($('#contentContainer').attr('data-state') != 'read-only' && $('.time-notice').length > 0){
		timeTicker = new easytimer.Timer();
		timeTicker.start();
		timeTicker.addEventListener('secondsUpdated', function (e) {
			$('.time-notice').html(timeTicker.getTimeValues().toString());
		});
	}
	setTimeout(function(){
		kriyaEditor.init.trackArticle();
	}, 120000);
	//#509
	$.each(kriya.config.preventTyping.split(","), function (i, val) {
		$(val).attr('data-editable', "false")
	})
	if (($(contentDivNode).length > 0) && $(contentDivNode).is(':visible')) {
		$(contentDivNode).css('height', (window.innerHeight - $(contentDivNode).offset().top - $('#footerContainer').height() )+ 'px');
		$(contentDivNode).parent('.nano').css('height', (window.innerHeight - $(contentDivNode).offset().top - $('#footerContainer').height())+ 'px');
		$(contentDivNode).parent('.nano').nanoScroller({
			alwaysVisible: true,
			sliderMinHeight: 5,
			preventPageScrolling: true
		});
		$(contentDivNode).parent().find('.nano-pane').addClass('content-scrollbar');
		//$(contentDivNode).width('90%');
	}
	$('#navContainer').css('height', ($(window).height() - $('#navContainer').offset().top - $('#footerContainer').height()) + 'px')
	$('#navContainer .navDivContent > div').css('height', $(window).height() - $('#navContainer #infoDivContent').offset().top - $('#footerContainer').height());
	if ($('#indexPanelContent .jstree-container-ul').length > 0){
		$('#indexPanelContent .jstree-container-ul li').removeAttr('class');
		tree = $("#indexPanelContent").jstree({
			core: {
				check_callback: true
			},
			plugins: ["dnd"]
		});
		tree.jstree("deselect_all").jstree('open_all');
	}

	//Initialize table row and index
	$(kriya.config.containerElm).find('.jrnlInlineTable, .jrnlTable').each(function(){
		setTableIndex($(this));
	});

	window.addEventListener("resize", function(){
		$(contentDivNode).css('height', (window.innerHeight - $(contentDivNode).offset().top - $('#footerContainer').height() )+ 'px');
		$(contentDivNode).parent('.nano').css('height', (window.innerHeight - $(contentDivNode).offset().top - $('#footerContainer').height())+ 'px');
		$('#navContainer').css('height', ($(window).height() - $('#navContainer').offset().top - $('#footerContainer').height()) + 'px');
		$('#navContainer .navDivContent > div').css('height', $(window).height() - $('#navContainer .navDivContent').offset().top - $('#footerContainer').height());
		//it will refresh contentContainer scrollbar size when we resize the window.#2539
		$(contentDivNode).parent(".nano").nanoScroller();
	});
	eventHandler.query.action.addBadge();
	$('.uncited-object-btn').each(function(){
		if ($(this).closest('div').find('[data-uncited="true"]').length > 0){
			$(this).append('<span class="notify-badge">'+ $(this).closest('div').find('[data-uncited="true"]').length + '</span>');
		}
	})
	$('body').append('<span class="class-highlight hidden" style="position: absolute;bottom: -12px;display: inline-block;height: 25px;width: 200px;right: 10px;background: #eee;border: 1px solid #ddd;font-size: 12px;line-height: 110%;padding:5px;color:#424242;"></span>');
	$('[data-type="popUp"]').find('input,textarea').attr("autocomplete","off").attr("autocorrect", "off").attr("autocapitalize", "off").attr("spellcheck", "false");
	$('[data-tooltip]').tooltip({
		delay: 50
	});

	//Initialize the content buttons
	if ($('#contentContainer').attr('data-state') != 'read-only'){
		var contentBtns = $('#compDivContent [data-component="contentButtons"] [data-xpath]');
		contentBtns.each(function(){
			var xpath = $(this).attr('data-xpath');
			var dataxpath = $(this).attr('data-condition');
			var maxVal = $(this).attr('data-maximum');
			var btnContainer = kriya.xpath(xpath);
			var clonedNode = $(this).clone(true);
			var btnContainerCond = "";
			var availdata = 0;
			if($(btnContainer).length > 0){
				if(dataxpath!=undefined){
					btnContainerCond = kriya.xpath(dataxpath);
					if(maxVal!=undefined){
						if($(btnContainerCond).length>=maxVal){
							$(clonedNode).addClass("hidden");
						}
					} else if ($(btnContainerCond).length > 0) {
						$(btnContainerCond).each(function(){
							if($(this).attr('data-track')==undefined || $(this).attr('data-track')!="del"){
								availdata=1;
							}
						})
						if(availdata==1){
							$(clonedNode).addClass("hidden");
						}
					}
				}
				$(btnContainer).append(clonedNode);
			}
		});

		//Initialize the filter options
		kriya.general.iniFilterOptions($('.filterOptions .dropdown-content'), '');

		var treeData = [
			{
			  'text' : 'All Floats', 
			  'children': []
			}
		];

		var figObj = {'text' : 'All Figures', 'id' : 'all-fig', 'children': []};
		var tblObj = {'text' : 'All Tables','id' : 'all-tbl', 'children': []};
		var vidObj = {'text' : 'All Videos','id' : 'all-vid', 'children': []};
		var boxObj = {'text' : 'All Boxes','id' : 'all-box', 'children': []};
		var supplObj = {'text' : 'All Supplements','id' : 'all-suppl', 'children': []};

		$(kriya.config.containerElm).find('.jrnlFigBlock, .jrnlTblBlock, .jrnlSupplBlock, .jrnlBoxBlock, .jrnlSupplBlock, .jrnlVidBlock, .jrnlMapBlock').each(function(){
			var className = $(this).attr('class');
			var dataID    = $(this).attr('data-id');
			dataID = "jstree-" + dataID;
			if($(this).find('.label').length > 0){
				var labelText = $(this).find('.label').clone(true).cleanTrackChanges().text();	
			}else if($(this).find('[class$="Caption"]:first').length > 0){
				var labelText = $(this).find('[class$="Caption"]:first').text();
				labelText = labelText.replace(/^\s+/g, '');
				labelText = labelText.substr(0, 10);
				labelText = labelText + '...';
			}
			(className == "jrnlFigBlock")?(figObj.children.push({'text' : labelText, 'id' : dataID})):(className == "jrnlMapBlock")?(figObj.children.push({'text' : labelText, 'id' : dataID})):(className == "jrnlTblBlock")?tblObj.children.push({'text' : labelText, 'id' : dataID}):(className == "jrnlSupplBlock")?supplObj.children.push({'text' : labelText, 'id' : dataID}):(className == "jrnlBoxBlock")?boxObj.children.push({'text' : labelText, 'id' : dataID}):vidObj.children.push({'text' : labelText, 'id' : dataID});
		});
		if($(kriya.config.containerElm).find('.jrnlFigBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(figObj);
		}
		if($(kriya.config.containerElm).find('.jrnlTblBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(tblObj);
		}
		if($(kriya.config.containerElm).find('.jrnlVidBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(vidObj);
		}
		if($(kriya.config.containerElm).find('.jrnlBoxBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(boxObj);
		}
		if($(kriya.config.containerElm).find('.jrnlSupplBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(supplObj);
		}
		if($(kriya.config.containerElm).find('.jrnlMapBlock:has([class$="Caption"])').length > 0){
			treeData[0].children.push(supplObj);
		}
		
		$('.searchInObj').jstree({
			'core' : {
				'themes' : {'icons' : false},
				'data' : treeData
			},
			'plugins': ['checkbox']
		 });

	}

	setTimeout(function(){
		initialiseDragDrop();
		eventHandler.components.reference.getReferenceModal();
		eventHandler.components.citation.initCitationConfig();


		var resizeTimer;
		$('#contentDivNode > br').remove();

		createCloneContent();
		/*$('#contentDivNode').scroll(function(){
			$('#clonedContent').scrollTop($('#contentDivNode').scrollTop())
			$('.suggestions,.resolve-query').remove();
			setTimeout(function(){
				if ($('.query-div.hover-inline').find('.cancelReply').length > 0){
					$('.query-div.hover-inline').find('.cancelReply').trigger('click');
				}else if ($('.query-div.hover-inline').find('.com-close').length > 0){
					$('.query-div.hover-inline').find('.com-close').trigger('click');
				}else{
					$('.query-div.disabled').remove();
					$('.query-div.hover-inline').removeAttr('style').removeClass('hover-inline');
				}
			},100);
		});*/

		//To remove the empty space in query node
		$('.jrnlQueryRef').html("");
		kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;

		var sel = rangy.getSelection();
		var blockNode = $(sel.anchorNode).closest('[id]:not(span)');
		if(blockNode.length > 0){
			kriyaEditor.settings.startCaretID = blockNode.attr('id');
			kriyaEditor.settings.startCaretPos = getCaretCharacterOffsetWithin(blockNode[0]);
		}else{
			kriyaEditor.settings.startCaretID  = "";
			kriyaEditor.settings.startCaretPos = "";
		}
		

		kriyaEditor.settings.cloneNode = document.getElementById('clonedContent');
		kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;

		eventHandler.query.action.regenerateActionQuery();

		//Order the query based on location
		eventHandler.query.action.orderQuery();

	},2000);

	setupWatch();
	//$.autoCite(); //commented by jai
	if (/version\?/.test(window.location.href)){
		$.ajax({
			type: "GET",
			url: "/api/articlenode?customerName="+kriya.config.content.customer+"&projectName="+kriya.config.content.project+"&bucketName=AIP&xpath=//workflow/stage[last()]&doi=" + kriya.config.content.doi,
			contentType: "application/xml; charset=utf-8",
			dataType: "xml",
			success: function (data) {
				if (data){
					dataNode = data.documentElement;
					if ($(dataNode).find('> name').text() == $('#contentContainer').attr('data-stage-name')){
						var versionBtn = $('<span data-name="Approve" class="btn btn-small action-btn" data-page="review_content" data-message="{\'click\':{\'funcToCall\': \'restoreVersion\',\'channel\':\'welcome\',\'topic\':\'signoff\'}}"><i class="material-icons" style="font-size:1.4rem">check_box</i>&nbsp;<span class="menu-text">Make this current version</span></span>')
						$('.kriyaSubMenuContainer.navDivContent').append(versionBtn);
					}
				}
			}
		})
	}
	window.onerror = function(msg, url, line, col, error) {
	   // Note that col & error are new to the HTML 5 spec and may not be
	   // supported in every browser.  It worked for me in Chrome.
	   var extra = !col ? '' : '\ncolumn: ' + col;
	   extra += !error ? '' : '\nerror: ' + error;
	   //it will give flow of function calling.
	   var flow = error.stack ? "\nFlow:"+error.stack : "";
	   var browser = '';
	   // Chrome 1+
	   //After updated chrome (!!window.chrome && !!window.chrome.webstore) condition failing in windows
		if((!!window.chrome && !!window.chrome.webstore) || (navigator.userAgent.search("Chrome") >= 0)){
			browser = "Chrome";
		}else if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){// Opera 8.0+
			browser = "Opera";
		}else if(typeof InstallTrigger !== 'undefined'){// Firefox 1.0+
			browser = "Firefox";
		}else if(/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){// Safari 3.0+ "[object HTMLElementConstructor]"
			browser = "Safari";
		}else if(/*@cc_on!@*/false || !!document.documentMode){// Internet Explorer 6-11
			browser = "Internet Explorer";
		}else if(!(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia){// Edge 20+
			browser = "Edge";
		//for blink we need to check like this.
		}else if(((!!window.chrome && !!window.chrome.webstore) || ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)) && !!window.CSS){// Blink engine detection
			browser = "Blink";
		}else{
			browser = "UnKnown";
		}
	   //it will give target ele id className and parent ele className
	   var target = (kriya.evt && kriya.evt.target)? "\nTarget id: "+kriya.evt.target.id+", class: "+kriya.evt.target.className+", parentClass: "+kriya.evt.target.parentNode.className : "";
	   //it will give what browser they are using.(if not chrome and mozilla it will print others)
	   //var browser = (!!window.chrome && !!window.chrome.webstore)?"chrome":(typeof InstallTrigger !== 'undefined')? "Mozilla":"other";
	   // You can view the information in an alert to see things working like this:
	   var errorMsg = "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra + "\nUser:" + kriyaEditor.user.name + "\n" + window.location.href + target + flow +"\nBrowser: "+browser ;
	   console.log(errorMsg);
	   // TODO: Report this error via ajax so you can keep track
	   //       of what pages have JS issues
		var parameters = {
			'log': errorMsg
		};
	   kriya.general.sendAPIRequest('logerrors',parameters,function(res){
			console.log('Data Saved');
		})
	   var suppressErrorAlert = true;
	   // If you return true, then error alerts (like in older versions of
	   // Internet Explorer) will be suppressed.
	   return suppressErrorAlert;
	};
}

function createCloneContent() {
	$('#clonedContent').remove();
	var clonedContent = $('<div id="clonedContent" class="nano-content">');

	/*var cid = Math.floor(Date.now() / 1000);
	$(kriya.config.containerElm).find('div.front,div.body,div.back').each(function(){
		if (!$(this)[0].hasAttribute('id')){
			$(this).attr('id', 'cid_'+cid);
		}
		$(this).children().each(function(){
			if (!$(this)[0].hasAttribute('id')){
				$(this).attr('id', 'cid_'+cid);
			}
			if (/^(DIV|UL|OL)$/.test($(this)[0].nodeName)){
				$(this).children().each(function(){
					if (!$(this)[0].hasAttribute('id')){
						$(this).attr('id', 'cid_'+cid);
					}
					var subChild = $(this);
					if (subChild[0].nodeName == "TABLE"){
						$(this).find('tr').each(function(i,e){
							if (!$(this)[0].hasAttribute('id')){
								$(this).attr('id', 'cid_'+cid);
							}
							$(this).find('th, td').each(function(i,e){
								if (!$(this)[0].hasAttribute('id')){
									$(this).attr('id', 'cid_'+cid);
								}
							});
						});
					}
				});
			}
		});
	});*/

	$(kriya.config.containerElm).after(clonedContent);
	var cloneHeight = 0
	$(kriya.config.containerElm).children().each(function(){
		cloneHeight += $(this).height();
	});

	var lastNode = $(kriya.config.containerElm + ' *:visible').last();
	var dummyNodeTop = lastNode[0].getBoundingClientRect().top + $('#contentDivNode').scrollTop();
	$('#clonedContent').append($('<span class="dummyHighlight" style="top:' + dummyNodeTop + 'px; display:inline-block;opacity:0;width: 18px;height: 18.5px;position: absolute;z-index: 0;"/>'));

	$('#clonedContent').append($('<div style="position: absolute;left: 0px;width: 1px;height: 1px;display: inline-block;opacity: 0;top:'+cloneHeight+'px"/>'));
	$('#clonedContent').height($('#contentDivNode').height());
}

function cloneChild(node, cid){
	var subChild = $(node).clone();
	$(subChild).html('');
	if (subChild[0].hasAttribute('id')){
		$(subChild).attr('clone-id', $(subChild).attr('id'));
		$(subChild).removeAttr('id');
	}else{
		$(node).attr('id', 'cid_'+cid);
		$(subChild).attr('clone-id', 'cid_' + cid++);
	}
	subChild.css({
		'position': 'relative'
	})
	if ($(node).css('display') == "none"){
		subChild.height('0');
		subChild.css({
			'display': 'none'
		});
	}else{
		subChild.height($(node).outerHeight());
		subChild.css({
			'display': 'block'
		});
	}
	$(subChild).css('margin', $(node).css('margin'));
	$(subChild).css('margin-top', $(node).css('marginTop'));
	$(subChild).css('margin-bottom', $(node).css('marginBottom'));
	$(subChild).css('padding-top', $(node).css('paddingTop'));
	$(subChild).css('padding-left', $(node).css('paddingLeft'));
	$(subChild).css('padding-bottom', $(node).css('paddingBottom'));
	$(subChild).css('padding-right', $(node).css('paddingRight'));
	return [subChild, cid];
}

function callUpdateArticleCitation(saveNodes,nodeToLookInto){
	// To update the article title in "cite this as" information while edit the article title.
	if(saveNodes && saveNodes.length < 1){
		return false;
	}
	var authorNameUpdated = false;
	$(saveNodes).each(function(i, obj){
		//If array value is undefined or empty then continue the each loop
		if(!obj){
			return;
		}
		var saveNodeClass = '';
		var currNode = $(this);
		if (/^(I|B|EM|STRONG|SUP|SUB|U)/.test(currNode.nodeName)){
			currNode = currNode.parent();
		}
		if($(currNode).hasClass('ins') || $(currNode).hasClass('del')){
			currNode = $(this).closest(':not(".ins,.del")');
		}
		if(currNode){
			var clonedNode = $(currNode).clone();
			if($(clonedNode).hasClass('activeElement')){
				$(clonedNode).removeClass('activeElement');
			}
			saveNodeClass = $(clonedNode).attr('class');
			if(saveNodeClass && saveNodeClass != ""){
				clonedNode.find('.del').each(function(){
					$(this).remove();
				});

				clonedNode.find('*').each(function(){
					$(this).cleanTrackChanges();
					$(this).cleanTrackAttributes();
				});


				var nodeValue = $(clonedNode).html();
				var updateNode = '';

				if(!authorNameUpdated && saveNodeClass.match(/^(jrnlAuthorGroup|jrnlAuthorsGroup|jrnlAuthors|jrnlAuthor)$/i)){
					var checkElement = '';
					var authorNodesInNodeToLookInto = $('#contentDivNode '+nodeToLookInto).find('.jrnlAuthorCite');
					if(authorNodesInNodeToLookInto.length > 0){
						checkElement = $(authorNodesInNodeToLookInto)[0];
					}
					if($('#contentDivNode '+nodeToLookInto).length > 0 && checkElement && checkElement!="" && checkElement.hasAttribute('data-maxauthors')){
						var maxAuthorCount = $(checkElement).attr('data-maxauthors');
					}else{
						// If data-maxauthors is not available, we have to find author's count - priya #105
						var maxAuthorCount = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"])').find('.jrnlAuthor').length+1;
					}
					var authorNameNodes = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"]):lt('+maxAuthorCount+')').find('.jrnlAuthor');
					var totalAuthors = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"])').find('.jrnlAuthor');
					var totalAuthorCount = $(totalAuthors).length;
					var currAuthorCount = $(authorNameNodes).length;
					var authorNameHTML = '';
					var andIndex = '';
					if($(authorNameNodes).length > 0){
						if($(totalAuthors).length > 1 && ($(totalAuthors).length <= maxAuthorCount)){
							andIndex = $(authorNameNodes).length;
						}

						if(checkElement && checkElement!="" && checkElement.hasAttribute('data-penultimatePunc') && $(checkElement).attr('data-penultimatePunc')!=''){
							penultimatePunc = $(checkElement).attr('data-penultimatePunc');
						}
						//removed data-maxauthors condition, bcoz its client specific #105 - priya
						if($('#contentDivNode '+nodeToLookInto).length > 0 && checkElement && checkElement!="" && $(checkElement).length > 0){
							for(var i=0; i<maxAuthorCount; i++){
								var etalAuthorCount = '';
								var authorNameNode = $(authorNameNodes)[i];
								if(authorNameNode){
									var surName = $(authorNameNode).find('.jrnlSurName').html();
									var givenName = $(authorNameNode).find('.jrnlGivenName').html();
									//updated by aravind on 26/06/18
									if(checkElement && checkElement!="" && checkElement.hasAttribute('data-regExp') && $(checkElement).attr('data-penultimatePunc')!=''){
										givenName = givenName.replace(new RegExp($(checkElement).attr('data-regExp')),'');
									}

									if(checkElement.hasAttribute('data-abbrv-name') && $(checkElement).attr('data-abbrv-name') == "true" && givenName && givenName != ""){
										//#10 - Adding special character in Given Name : To Cite. - Vimala.J(vimala.j@focusite.com)
                    					//#10 - To get the First letter of Givenname even after space with Uppercase - Vimala.J (vimala.j@focusite.com)
										var matches = givenName.match(/(\s|^)(\w|\S)/g);
										givenNameMatch = matches.join('');
										givenNameMatchUpper = givenNameMatch.toUpperCase();
										givenName = givenNameMatchUpper.replace(/\s+/g,'');
										givenName = givenName.replace(/([A-Z])/, ' $1').trim();
										// End of #10
									}

									var surNameHTML = '<span class="jrnlSurName">'+surName+'</span>';
									var givenNameHTML = '<span class="jrnlGivenName">'+givenName+'</span>';

									if(checkElement.hasAttribute('data-author-format')){
										var authorFormat = $(checkElement).attr('data-author-format');

										if(authorFormat == "SG"){
											authorNameHTML = authorNameHTML + '<span class="jrnlAuthor">' + surNameHTML + ' ' + givenNameHTML + '</span>';
										}else if(authorFormat == "GS"){
											authorNameHTML = authorNameHTML + '<span class="jrnlAuthor">' + givenNameHTML + ' ' + surNameHTML + '</span>';
										}else{
											authorNameHTML = authorNameHTML + '<span class="jrnlAuthor">' + surNameHTML + ' ' + givenNameHTML + '</span>';
										}
									} else {
										authorNameHTML = authorNameHTML + '<span class="jrnlAuthor">' + surNameHTML + ' ' + givenNameHTML + '</span>';
									}

									if(andIndex && andIndex!= '' && (andIndex - 2) == i){
										authorNameHTML = authorNameHTML + penultimatePunc;
									}else if(((i == (maxAuthorCount - 1) || (currAuthorCount && (i == (currAuthorCount - 1)))) && (andIndex != '')) || currAuthorCount == 1){
										if(checkElement.hasAttribute('data-endPunc') && $(checkElement).attr('data-endPunc') != ""){
											authorNameHTML = authorNameHTML + $(checkElement).attr('data-endPunc');
										}else{
											authorNameHTML = authorNameHTML + ' ';
										}
									}else{
										authorNameHTML = authorNameHTML + ', ';
									}

									if(checkElement.hasAttribute('data-etalauthors')){
										etalAuthorCount = $(checkElement).attr('data-etalauthors');
										if(totalAuthorCount && etalAuthorCount && etalAuthorCount!="" && totalAuthorCount > etalAuthorCount){
											etalAuthorCount = etalAuthorCount - 1;
											var etalText = '';
											if(checkElement.hasAttribute('data-etal-text')){
												etalText = $(checkElement).attr('data-etal-text');
											}else{
												etalText = 'et al ';
											}
											if(i == etalAuthorCount){
												authorNameHTML = authorNameHTML + etalText;
												break;
											}
										}
									}
								}
							}

							if(authorNodesInNodeToLookInto.length > 0 && authorNameHTML && authorNameHTML != ""){
								var firstNode = $(authorNodesInNodeToLookInto)[0];
								$(firstNode).html(authorNameHTML);
								authorNameUpdated = true;
							}
						}
					}
				} else if ($('#contentDivNode ' + nodeToLookInto).find('.' + saveNodeClass + 'Cite').length > 0) {
					updateNode = $('#contentDivNode '+nodeToLookInto).find('.'+saveNodeClass+'Cite')[0];
				}else if($('#contentDivNode '+nodeToLookInto).find('.'+saveNodeClass).length > 0){
					updateNode = $('#contentDivNode '+nodeToLookInto).find('.'+saveNodeClass)[0];
				}
				if(updateNode){
					$(updateNode).html(nodeValue);
				}
			}
		}
	});
	callUpdateArticleAuthorCopyRights(saveNodes, '.jrnlPermission');
}
 function closestNumber(array,num){
    var i=0;
    var minDiff=1000;
    var ans;
    for(i in array){
         var m=Math.abs(num-array[i]);
         if(m<minDiff){
                minDiff=m;
                ans=array[i];
            }
      }
    return ans;
}
function callUpdateArticleAuthorCopyRights(saveNodes,nodeToLookInto){

	if(saveNodes && saveNodes.length < 1){
		return false;
	}
	var authorNameUpdated = false;
	$(saveNodes).each(function(i, obj){

		if(!obj){
			return;
		}
		var saveNodeClass = '';
		var currNode = $(this);
		if (/^(I|B|EM|STRONG|SUP|SUB|U)/.test(currNode.nodeName)){
			currNode = currNode.parent();
		}
		if($(currNode).hasClass('ins') || $(currNode).hasClass('del')){
			currNode = $(this).closest(':not(".ins,.del")');
		}
		if(currNode){
			var clonedNode = $(currNode).clone();
			if($(clonedNode).hasClass('activeElement')){
				$(clonedNode).removeClass('activeElement');
			}
			saveNodeClass = $(clonedNode).attr('class');
			if(saveNodeClass && saveNodeClass != ""){
				clonedNode.find('.del').each(function(){
					$(this).remove();
				});

				clonedNode.find('*').each(function(){
					$(this).cleanTrackChanges();
					$(this).cleanTrackAttributes();
				});

				var nodeValue = $(clonedNode).html();
				var updateNode = '';

				if(!authorNameUpdated && saveNodeClass.match(/^(jrnlAuthorGroup|jrnlAuthors|jrnlAuthor)$/i)){
					var checkElement = '';
					var authorNodesInNodeToLookInto = $('#contentDivNode '+nodeToLookInto).find('.jrnlCopyrightHolder');
					if(authorNodesInNodeToLookInto.length > 0){
						checkElement = $(authorNodesInNodeToLookInto)[0];
					}
					if($('#contentDivNode '+nodeToLookInto).length > 0 && checkElement && checkElement!="" && checkElement.hasAttribute('data-maxauthors')){
						var maxAuthorCount = $(checkElement).attr('data-maxauthors');
					}else{
						// If data-maxauthors is not available, we have to find author's count - priya #105
						var maxAuthorCount = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"])').find('.jrnlAuthor').length+1;
					}
					var authorNameNodes = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"]):lt('+maxAuthorCount+')').find('.jrnlAuthor');
					var totalAuthors = $('#contentDivNode').find('.front').find('.jrnlAuthorGroup:not([data-track="del"])').find('.jrnlAuthor');
					var totalAuthorCount = $(totalAuthors).length;
					var currAuthorCount = $(authorNameNodes).length;
					var authorNameHTML = '';
					var andIndex = '';
					if($(authorNameNodes).length > 0){
						if($(totalAuthors).length > 1 && ($(totalAuthors).length <= maxAuthorCount)){
							andIndex = $(authorNameNodes).length;
						}

						if(checkElement && checkElement!="" && checkElement.hasAttribute('data-penultimatePunc') && $(checkElement).attr('data-penultimatePunc')!=''){
							penultimatePunc = $(checkElement).attr('data-penultimatePunc');
						}
						//removed data-maxauthors condition, bcoz its client specific #105 - priya
						if($('#contentDivNode '+nodeToLookInto).length > 0 && checkElement && checkElement!="" && $(checkElement).length > 0){
							for(var i=0; i<maxAuthorCount; i++){
								var etalAuthorCount = '';
								var authorNameNode = $(authorNameNodes)[i];
								if(authorNameNode){
									var surName = $(authorNameNode).find('.jrnlSurName').html();
									var surNameHTML = '<span class="jrnlSurName">'+surName+'</span>';
									authorNameHTML = authorNameHTML + '<span class="jrnlAuthor">' + surNameHTML + '</span>';
									if(andIndex && andIndex!= '' && (andIndex - 2) == i){
										authorNameHTML = authorNameHTML + penultimatePunc;
									}else if(((i == (maxAuthorCount - 1) || (currAuthorCount && (i == (currAuthorCount - 1)))) && (andIndex != '')) || currAuthorCount == 1){
										if(checkElement.hasAttribute('data-endPunc') && $(checkElement).attr('data-endPunc') != ""){
											authorNameHTML = authorNameHTML + $(checkElement).attr('data-endPunc');
										}else{
											authorNameHTML = authorNameHTML + ' ';
										}
									}else{
										authorNameHTML = authorNameHTML + ', ';
									}

									if(checkElement.hasAttribute('data-etalauthors')){
										etalAuthorCount = $(checkElement).attr('data-etalauthors');
										if(totalAuthorCount && etalAuthorCount && etalAuthorCount!="" && totalAuthorCount > etalAuthorCount){
											etalAuthorCount = etalAuthorCount - 1;
											var etalText = '';
											if(checkElement.hasAttribute('data-etal-text')){
												etalText = $(checkElement).attr('data-etal-text');
											}else{
												etalText = 'et al ';
											}
											if(i == etalAuthorCount){
												authorNameHTML = authorNameHTML + etalText;
												break;
											}
										}
									}
								}
							}

							if(authorNodesInNodeToLookInto.length > 0 && authorNameHTML && authorNameHTML != ""){
								var firstNode = $(authorNodesInNodeToLookInto)[0];
								$(firstNode).html(authorNameHTML);
								authorNameUpdated = true;
							}
						}
					}
				}

				if(updateNode){
					$(updateNode).html(nodeValue);
				}
			}
		}
	});
}
