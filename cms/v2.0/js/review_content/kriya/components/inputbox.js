/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
window.Kriya = (function ($, kriya) {

    // 1. CONFIGURATIONf
    var config = {
        //container: '[data-component="InputBox"]',
		//container: '.jrnlHead1',
		container: '.InputBox',
		editable: true
    };

    // 2. COMPONENT OBJECT
    kriya.InputBox = {

        version: 0.1,

        init: function (elm) {
            this.cacheItems(elm);
            if (this.container.length){
                this.bindEvents();
            }
        },
		
		detach: function () {
			
		},

        cacheItems: function (elm) { 	
		console.log(typeof(elm));
            this.container = typeof(elm)!='undefined'?elm: $(config.container);
			console.log('this.container='+this.container);
        },

        bindEvents: function () {
			
			if(config.editable){
				this.tinyEditor(config.container);
			}
			
			/*var _self = this;
            this.container.on('click', function (evt) {

				_self.tinyEditor(this.className);
			});	*/			
        },

		/* Rich text editor */
		tinyEditor: function(elm){
			$(function() {
				tinymce.init({
					selector: elm,
					inline: true,
					plugins: ['paste, charmap'],
					menubar: false,
					toolbar: 'undo redo | bold italic | superscript subscript underline | charmap'
				});
			   var tracker = new ice.InlineChangeEditor({
				   element: $(elm),
				   handleEvents: true,
				   currentUser: { id: 1, name: 'Miss T' },
				   // optional plugins
				   plugins: [
					 // Add title attributes to changes for hover info
					 'IceAddTitlePlugin',
					 // Two successively typed dashes get converted into an em-dash
					 'IceEmdashPlugin',
					 // Track content that is cut and pasted
					 {
					   name: 'IceCopyPastePlugin',
					   settings: {
						 // List of tags and attributes to preserve when cleaning a paste
						 pasteType : 'formattedClean',
						 preserve: 'strong'
					   }
					 }
				   ]
				}).startTracking();
			});
		}

    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.jQuery, window.Kriya || {}));