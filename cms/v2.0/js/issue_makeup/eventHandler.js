/**
* eventHandler - this javascript holds all the functions required for Reference handling
*					so that the functionalities can be turned on and off by just calling the required functions
*					Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var eventHandler = function() {
	//default settings
	var settings = {};
	settings.subscriptions = {};
	settings.subscribers = ['menu','components'];
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};
	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (eventHandler) {
	settings = eventHandler.settings;
	var timer;

	eventHandler.publishers = {
		add: function() {
			/**
			* attach a click event listner to the body tag
			* if the target node has the special attribute 'data-channel',
			* then we need to publish some data using the data in the attributes
			*/
			// Get the element, add a click listener...
			var bodyNode = document.getElementsByTagName('body').item(0);

			$(bodyNode).on('keyup', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('click', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('focusout', eventHandler.publishers.eventCallbackFunction);
			$(bodyNode).on('change', eventHandler.publishers.eventCallbackFunction);
		},
		remove: function() {
			var bodyNode = document.getElementsByTagName('body').item(0);
			bodyNode.removeEventListener("click", eventHandler.publishers.eventCallbackFunction);
		},
		eventCallbackFunction: function(event) {
			// event.target is the clicked element!
			//check if the cuurent click position has a underlay behind it
			elements = $(event.target);

			$(elements).each(function(){
				var targetNodes = $(this).closest('[data-message]');
				if (targetNodes.length){
					targetNode = $(targetNodes[0]);
					var message = eval('('+targetNode.attr('data-message')+')');
					var eveDetails = message[event.type];
					if(!eveDetails && targetNode.attr('data-channel') && targetNode.attr('data-event') == event.type){
						eveDetails = message;
						eveDetails.channel = targetNode.attr('data-channel');
						eveDetails.topic   = targetNode.attr('data-topic');
					}else if(typeof(eveDetails) == "string" && typeof(message[eveDetails]) == "object"){
						eveDetails = message[eveDetails];
					}else if(!eveDetails || typeof(eveDetails) == "string"){
						return;
					}
					postal.publish({
						channel: eveDetails.channel,
						topic  : eveDetails.topic + '.' + event.type,
						data: {
							message : eveDetails,
							event   : event.type,
							target  : targetNode
						}
					});
					event.stopPropagation();
				}
			});
		}
	};
	eventHandler.subscribers = {
		add: function() {
			var subscribers = eventHandler.settings.subscribers;
			for (var i=0;i<subscribers.length;i++) {
				initSubscribe(subscribers[i],subscribers[i]+'_subscription');
			}

			function initSubscribe(subscribeChannel,subscribeName){
				if (!eventHandler.settings.subscriptions[subscribeName]){
						eventHandler.settings.subscriptions[subscribeName] = postal.subscribe({
							channel: subscribeChannel,
							topic: "*.*",
							callback: function(data, envelope) {
								//console.log(data, envelope);
								if(data.message != ''){
									/*http://stackoverflow.com/a/3473699/3545167*/
									if (typeof(data.message) == "object"){
										var array = data.message;
									}else{
										var array = eval('('+data.message+')');
									}
									var functionName = array.funcToCall;
									var param = array.param;
									var topic = envelope.topic.split('.');
									//var fn = 'eventHandler.menu.edit.'+functionName;
									if(typeof(window[functionName]) == "function"){
										window[functionName](param,data.target);
									}else if(typeof(window['eventHandler'][envelope.channel][topic[0]]) == "object"){
										if (typeof(window['eventHandler'][envelope.channel][topic[0]][functionName]) == 'function'){
											window['eventHandler'][envelope.channel][topic[0]][functionName](param, data.target);
										}
									}else{
										console.log(functionName + ' is not defined in ' + topic[0]);
									}
								}else{
									console.log('Message is empty');
								}

								// `data` is the data published by the publisher.
								// `envelope` is a wrapper around the data & contains
								// metadata about the message like the channel, topic,
								// timestamp and any other data which might have been
								// added by the sender.
							}
					});
				}
			}
		},
		remove: function() {
			//settings.subscriptions.menuClickSubscription.unsubscribe();
			var subscriptions = settings.subscriptions;
			for (var subscription in subscriptions) {
				if (subscriptions.hasOwnProperty(subscription)) {
					subscriptions[subscription].unsubscribe()
				}
			}
		},
	};
	eventHandler.menu = {
		general: {
			addIssue: function(param, targetNode){
				var popUp = $('#compDivContent [data-component="addIssue_edit"]');
				openPopup(popUp);
				popUp.find('input[data-class]').val('');
				popUp.find('[data-class="customer"]').focus();
				popUp.find('[data-class="sPage"]').parent().removeClass('hide');
			},
			openIssue: function(param, targetNode){
				var popUp = $('#compDivContent [data-component="addIssue_edit"]');
				openPopup(popUp);
				popUp.find('input[data-class]').val('');
				popUp.find('[data-class="customer"]').focus();
				popUp.find('[data-class="sPage"]').parent().addClass('hide');
			},
			savePageDetails: function(param, targetNode){
				doi = $(targetNode).find('.doi').text();
				nodeValue = '<volume>' + issueMakeUp.config.volume + '</volume>';
				var param = {'doi': doi, 'xpath': '//front//volume', 'nodeValue': nodeValue}
				eventHandler.menu.general.updateArticle(param, targetNode);
				nodeValue = '<issue>' + issueMakeUp.config.issue + '</issue>';
				var param = {'doi': doi, 'xpath': '//front//issue', 'nodeValue': nodeValue}
				eventHandler.menu.general.updateArticle(param, targetNode);
				nodeValue = '<fpage>' + $(targetNode).find('.fpage').text() + '</fpage>';
				var param = {'doi': doi, 'xpath': '//front//fpage', 'nodeValue': nodeValue}
				eventHandler.menu.general.updateArticle(param, targetNode);
				nodeValue = '<lpage>' + $(targetNode).find('.lpage').text() + '</lpage>';
				var param = {'doi': doi, 'xpath': '//front//lpage', 'nodeValue': nodeValue}
				eventHandler.menu.general.updateArticle(param, targetNode);
			},
			updateArticle: function(param, targetNode){
				doi = param.doi;
				xpath = param.xpath;
				nodeValue = param.nodeValue;
				jQuery.ajax({
					type: "POST",
					url: "/api/updatearticle",
					data: {"customerName": issueMakeUp.config.customer, "projectName": issueMakeUp.config.project, "xpath": xpath, "doi": doi, "oldXMLFrag": "oldXMLFrag", "newXMLFrag": nodeValue, "xmlFrag": nodeValue},
					success: function (data) {
						if (data){
							 message = "The article with doi "+doi+" was successfully updated for field "+xpath;
							 return message;
						}
						else{
							message = "The article with doi "+doi+" could not be updated for field "+xpath;
							return message;
						}
					},
					error: function(xhr, errorType, exception) {
					  return null;
					}
				});
			},
			figureEdit: function(param, targetNode){
				var bankedCard = $(targetNode).closest('.bankedCard');
				var cardDoi = bankedCard.find('.doi').text();
				var popUp = $('#compDivContent [data-component="figure_edit"]');
				var figListHTML = $('<table></table>');
				figListHTML.append('<thead><tr><th>Figure(s)</th><th>Color in print</th></tr></thead>');
				var tableBodyHTML = '<tbody>';
				if(bankedCard.find('.articleFigure').length > 0){
					bankedCard.find('.articleFigure').each(function(){
						var label   = $(this).text();
						var currID  = Math.random().toString(36).slice(2);
						var color   = $(this).attr('data-color');
						var figurePath = $(this).attr('data-path');
						var figId = $(this).attr('id');
						var checked = (color == "1")?'checked="true"':'';
						tableBodyHTML += '<tr data-id="' + figId + '"><td>' + label + '</td><td><input type="checkbox" ' + checked + ' id="' + currID + '"/><label for="' + currID + '" /></td></tr>';
					});
				}else{
					tableBodyHTML += '<tr data-id="' + figId + '"><td colspan="3" style="text-align: center;">No Data Found.</td></tr>';
				}

				tableBodyHTML += '</tbody>';
				figListHTML.append(tableBodyHTML);
				popUp.find('.figuresDiv').html(figListHTML);
				popUp.attr('data-doi', cardDoi);
				openPopup(popUp);
			}
		},
		edit: {
			toggleNavPane: function(param, targetNode){
				//get active nodes and remove class "active"
				$('.toolRightTab').removeClass('active');
				$('#bankedContainer > div').removeClass('active');
				//add "active" class to current nodes
				$('#' + param).addClass('active');
				targetNode.addClass('active');
				console.log('Within showHideMenu function');
			},
			viewPdf: function(param, targetNode){
				var card = $(targetNode).closest('.bankedCard');
				var customer = $('[data-class="customer"]').val();
				var doi      = card.find('.doi').text();
				doi = doi.replace(/([^\/]*\/)/, '');

				//var pdfUrl = "http://resources.ama.uk.com/" + customer + "/__private/proofing/pdf/" + doi + "_print.pdf";
				var pdfUrl = param
				window.open(pdfUrl, '_blank');
			},
			mergePDF: function(param, targetNode){
				var pdfLinks = [];
				$('#sections .viewPDF').each(function(){
					if ($(this).attr('data-href') != ''){
						pdfLinks.push($(this).attr('data-href'))
					}
				})
				$('.la-container').fadeIn();
				$.ajax({
					type: "POST",
					url: "/api/mergepdf",
					data: {"customer": issueMakeUp.config.customer, "project": issueMakeUp.config.project, "volume": issueMakeUp.config.volume, "issue": issueMakeUp.config.issue, "pdfLinks": pdfLinks},
					success: function (data) {
						if (data && data.status && data.status.code == 200 && data.status.filePath){
							window.open(data.status.filePath, '_blank');
						}
						$('.la-container').fadeOut();
					},
					error: function(xhr, errorType, exception) {
						$('.la-container').fadeOut();
					  return null;
					}
				});
			},
			expandSections: function(){
				$(".collapsible-header").addClass("active");
				$(".collapsible").collapsible({accordion: false});
				$(".expandSection").addClass('hide');
				$(".collapseSection").removeClass('hide');
			},

			collapseSections: function(){
				$(".collapsible-header").removeClass(function(){
					return "active";
				});
				$(".collapsible").collapsible({accordion: true});
				$(".collapsible").collapsible({accordion: false});
				$(".expandSection").removeClass('hide');
				$(".collapseSection").addClass('hide');
			},

			addSection: function(){
				var newChild = '<li><div class="collapsible-header sectionHead"><i class="material-icons"></i><span class="sectionTitle">New Section</span><span class="right controls"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="editTitle(this)"></i><i class="fa fa-times fa-lg" aria-hidden="true" style="cursor:pointer" onclick="removeNode(this)"></i></span></div><div class="collapsible-body" id="section1"><span></span></div></li>'
				$(newChild).appendTo($('.section1'));
				return false;
			},

			newIssue: function(){
				$('#newIssue').openModal();
			}
		},
		export:{
			exportToPDF: function(param, targetNode, checkCitation){
				event.preventDefault();
				event.stopPropagation()
				$(targetNode).removeClass('red').removeClass('lighten-1').addClass('grey').html('Generate Pdf');
				if (param.doi){
					doi = param.doi;
				}else if ($(targetNode).closest('li').find('.doi').length > 0){
					var doi = $(targetNode).closest('li').find('.doi').text().replace(/^.*\//, '');
				}else{
					return false;
				}
				var customer = issueMakeUp.config.customer;
				var project = issueMakeUp.config.project;
				var volume = issueMakeUp.config.volume;
				var issue = issueMakeUp.config.issue;
				var fpage = $(targetNode).closest('li').find('.fpage').text();
				parameters = { "client": customer, "project": project, "idType": "doi", "id":doi, "processType": "InDesignSetter", "proof": "print", "volume": volume, "issue": issue, "fpage": fpage, "userName": userName, "userRole": userRole }
				if (param.peripherals){
					parameters.pheripherals = param.peripherals;
					parameters.id = project + '_' + volume + '_' + issue + '_' + doi;
				}
				
				var userName = 'kriyaUser';
				var userRole = 'typesetter';
				var notificationID = kriya.notification({
						title : 'PDF generation in progress... Please wait.',
						type  : 'success',
						content : ''
					});
				$.ajax({
					type: 'POST',
					url: "/api/pagination",
					data: parameters,
					crossDomain: true,
					success: function(data){
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
							eventHandler.menu.export.getJobStatus(customer, data.message.jobid, notificationID, userName, userRole, doi);
						}
						else if(data == ''){
							$('#'+notificationID+' .kriya-notice-body').html('Failed..');
						}else{
							$('#'+notificationID+' .kriya-notice-body').html(data.status.message);
						}
					},
					error: function(error){
						$('#'+notificationID+' .kriya-notice-body').html('Failed..');
					}
				});
			},
			getJobStatus: function(customer, jobID, notificationID, userName, userRole, doi){
				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.status){
							var status = data.status.message.status;
							var code = data.status.code;
							var currStep = 'Queue';
							if(data.status.message.stage.current){
								currStep = data.status.message.stage.current;
							}    
							var loglen = data.status.message.log.length;    
							var process = data.status.message.log[loglen-1];
							if (/completed/i.test(status)){
								var loglen = data.status.message.log.length;	
								var process = data.status.message.log[loglen-1];
								$('#'+notificationID+' .kriya-notice-body').html(process);
								$('#'+notificationID+' .kriya-notice-header').html('PDF generation completed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
							}else if (/failed/i.test(status) || code == '500'){
								$('#'+notificationID+' .kriya-notice-body').html(process);
								$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
							}else if (/no job found/i.test(status)){
								$('#'+notificationID+' .kriya-notice-body').html(status);
							}else{
								$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
								var loglen = data.status.message.log.length;	
								if(loglen > 1){
									var process = data.status.message.log[loglen-1];
									if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
								}
								setTimeout(function() {
									eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
								}, 1000 );
							}
						}
						else{
							$('#'+notificationID+' .kriya-notice-body').html('Failed');
						}
					},
					error: function(error){
						//alert('error; ' + eval(error));
					}
				});
			}
		},
		upload: {
			uploadFile: function(param, targetNode){
				var file = param.file;
				var block = param.block;
				var uploadPath = issueMakeUp.config.customer + '/' + issueMakeUp.config.project + '/' + issueMakeUp.config.volume + '_' + issueMakeUp.config.issue + '/';
				var formData = new FormData();
				formData.append('uploads[]', file, file.name);
				formData.append("path", uploadPath);

				$.ajax({
			      url: '/api/uploadfiles',
			      type: 'POST',
			      data: formData,
			      processData: false,
			      contentType: false,
			      xhr: function () {
						var xhr = new window.XMLHttpRequest();
						//Download progress
						xhr.upload.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								block.find('.loading-status').css('width', percentComplete).text(percentComplete);
							}
						}, false);
						xhr.addEventListener("progress", function (evt) {
							if (evt.lengthComputable) {
								var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
								block.find('.loading-status').css('width', percentComplete).text(percentComplete);
							}
						}, false);
						return xhr;
					},
			      error: function(e){
			      	console.log('Error' + e);
			      },
			      success: function(data){
					if(param && param.success && typeof(param.success) == "function"){
						var filePath = '../resources/' + uploadPath + data;
						param.success(filePath);
						console.log(filePath);
					}
			      }
			    });
			}
		}
	};

	eventHandler.components = {
		PopUp: {
			closeComponent: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				closePopUp(popper);
			},
			saveFigure: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var popDOI = popper.attr('data-doi');
				var bankedCard = $('#sections .bankedCard .doi:contains(' + popDOI + ')').closest('.bankedCard');
				var param = {
						'customer': issueMakeUp.config.customer,
						'project' : issueMakeUp.config.project, 
						'doi': popDOI,
					}
				var data = '';
				popper.find('tbody tr').each(function(){
					var figureLabel = $(this).find('td:eq(0)').text();
					var rowId       = $(this).attr('data-id');
					var isColored   = $(this).find('td:eq(1)').find('input[type="checkbox"]').is(":checked");
					isColored = (isColored)?1:0;
					if (isColored == 1){
						data += '<node data-xpath="//*[@id=\'' + rowId + '\']" data-type="add-attribute" data-name="data-color" data-value="cmyk"></node>'
					}else{
						data += '<node data-xpath="//*[@id=\'' + rowId + '\']" data-type="remove-attribute" data-name="data-color" data-value="cmyk"></node>'
					}
					bankedCard.find('.articleFigure#'+rowId).attr('data-color', isColored);
				});
				param.data = '<root>' + data + '</root>';
				kriya.sendAPIRequest('updateattribute', param);
				var content = bankedCard.closest('#sections')[0].outerHTML;
				var contentNode = $('<content>' + content + '</content>')
				contentNode.find('input').each(function(){
					$(this).replaceWith('<span class="'+$(this).attr('class')+'"/>');
				});
				content = contentNode.html();
				jQuery.ajax({
					type: "POST",
					url: "/api/save_issue",
					data: {"journal": issueMakeUp.config.project, "volume": issueMakeUp.config.volume, "issue": issueMakeUp.config.issue, "customer": issueMakeUp.config.customer, "update": "true", "content":encodeURIComponent('<issue>'+content+'</issue>')},
					success: function (respData) {
						console.log(respData);
					},
					error: function(xhr, errorType, exception) {
						console.log('unable to edit section title');
						return null;
					}
				});
				closePopUp(popper);	
			},
			startIssueProcess: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');	
				if(validateComponent(popper) == true){
					$('.collapsibleButtons').removeClass('hide');
					closePopUp(popper);
					// cleanup all temp states
					var customer = $(popper).find('[data-class="customer"]').val();
					var jname = $(popper).find('[data-class="journal"]').val();
					var vol = $(popper).find('[data-class="volume"]').val();
					var iss = $(popper).find('[data-class="issue"]').val();
					var fpage = $(popper).find('[data-class="sPage"]').val();
					
					//check is the issue exist
					issueMakeUp.config.isIssueExist = false;
					if(issueMakeUp.config.issueMeta){
						$(issueMakeUp.config.issueMeta).find('content-meta').each(function(){
							if($(this).find('volume').text() == vol && $(this).find('issue').text() == iss){
								issueMakeUp.config.isIssueExist = true;
								fpage = $(this).closest('container-xml').find('content[id-type="doi"]:eq(0) first-page').text();
							}
						});
					}
					
					fpage = parseInt(fpage);
					//Updat ethe issue config obj
					issueMakeUp.config.customer = customer;
					issueMakeUp.config.project  = jname;
					issueMakeUp.config.volume   = vol;
					issueMakeUp.config.issue    = iss;
					issueMakeUp.config.fpage    = fpage;

					if(!issueMakeUp.config.isIssueExist){
						jQuery.ajax({
							type: "POST",
							url: "/api/save_issue",
							data: {"journal": jname, "volume": vol, "issue": iss, "customer": customer},
							success: function (respData) {
								console.log(respData);
							},
							error: function(xhr, errorType, exception) {
								console.log('unable to create issue');
								return null;
							}
						});
					}
					$('.la-container').fadeIn();
					jQuery.ajax({
						type: "GET",
						url: "/api/getIssueData",
						data: {"jrnlName": jname, "volume": vol, "issue": iss, "client": customer},
						success: function (respData) {
							$('#sections').html($($(respData).children('span')[1]).html())
							//$('#bankedArticles').disableSelection()
							$('#bankedArticles')
								.html($($(respData).find('#ba')).html())
								.height($(window).height() - $('#bankedArticles').position().top);
							$('.collapsible').collapsible();
							//$('.collapsible').disableSelection();
							$('#sectionsContainer, #bankedContainer').height($('#footerContainer').position().top - $('#headerContainer').height());
							$('#bankedArticles').height($('#footerContainer').position().top - $('#bankedArticles').position().top)
							makeSortable();
							$('#sections .collapsible li .sortList p[data-id]').each(function(){
								var doi = $(this).attr('data-id');
								var bankedCard = $('#bankedArticles .bankedCard .doi:contains(' + doi + ')').closest('.bankedCard');
								if(bankedCard.length > 0){
									bankedCard.addClass('hide');
									bankedCard.find('.articleFigure').attr('data-color', '1');
									$(this).find('span[type="figure"]').each(function(){
										var label = $(this).attr('label');
										var colortype = $(this).attr('color');
										bankedCard.find('.articleFigure').each(function(){
											var cardLabel = $(this).text();
											cardLabel = cardLabel.replace(/[\.,\|\:\s]+$/g, '');
											if(cardLabel == label){
												$(this).attr('data-color', colortype);
											}
										});
									});	
									addToLi(bankedCard, $(this).closest('ul'));
								}
								var next = $(this).closest('li').next('li');
								if (next.length > 0 && next.find('.advertCard').length > 0){
									$(this).closest('ul').append(next)
								}
								$(this).closest('li').remove();
							});
							//$('.la-container').fadeOut();
							//return false;
							var coverPdf = issueMakeUp.config.project + '_' + issueMakeUp.config.volume + '_' + issueMakeUp.config.issue + '_' + 'cover.pdf';
							var coverHref = 'http://www.kriya.website/resources/' + issueMakeUp.config.customer + '/' + issueMakeUp.config.project + '/' + issueMakeUp.config.volume + '_' + issueMakeUp.config.issue + '/online/' + coverPdf;
							$('.coverNotes').closest('.bankedCard').find('.articleControls').after($('<span class="articleControls viewPDF right btn amber" data-message="{\'click\':{\'funcToCall\': \'viewPdf\',\'param\': \'' + coverHref + '\',\'channel\':\'menu\',\'topic\':\'edit\'}}" data-href="' + coverHref + '">View Pdf</span>'));
							
							var coverPdf = issueMakeUp.config.project + '_' + issueMakeUp.config.volume + '_' + issueMakeUp.config.issue + '_' + 'toc.pdf';
							var coverHref = 'http://www.kriya.website/resources/' + issueMakeUp.config.customer + '/' + issueMakeUp.config.project + '/' + issueMakeUp.config.volume + '_' + issueMakeUp.config.issue + '/online/' + coverPdf;
							$('.sectionTitle[plural*="table of content"]').parent().find('.articleControls').after($('<span class="articleControls viewPDF right btn amber" data-message="{\'click\':{\'funcToCall\': \'viewPdf\',\'param\': \'' + coverHref + '\',\'channel\':\'menu\',\'topic\':\'edit\'}}" data-href="' + coverHref + '">View Pdf</span>'));
							
							$('.editorialFile[data-path], .advertFile[data-path]').each(function(){
								$(this).append($('<span class="right"><span class="articleControls viewPDF right btn amber" data-message="{\'click\':{\'funcToCall\': \'viewPdf\',\'param\': \'' + $(this).attr('data-path').replace('..', 'http://www.kriya.website') + '\',\'channel\':\'menu\',\'topic\':\'edit\'}}" data-href="' + $(this).attr('data-path').replace('..', 'http://www.kriya.website') + '">View Pdf</span></span>'))
							})
							updatePageNumber();
							$('.la-container').fadeOut();
						},
						error: function(xhr, errorType, exception) {
							$('.la-container').fadeOut();
							console.log('unable to get issue data');
							return null;
						}
					});
					return true;
				}else{
					console.log('validation failed..');
					return false;	
				}
				
			},
			customerFocusOut: function(param, targetNode){
				var customerName = $(targetNode).val();
				if(customerName){
					jQuery.ajax({
						type: "GET",
						url: "/api/projects?customerName="+customerName,
						//data: '{"user": "' + hashUser + '", "pass": "' + hashPass + '"}',
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (res) {
							if (res && !res.error){
								var projectObj = {};
								$.each(res.project, function(i, obj){
									if(obj.name){
										projectObj[obj.name] = ''; 
									}	
								});
								$('[data-class="journal"]').parent().find('.autocomplete-content').remove();
								$('[data-class="journal"]').autocomplete({
									data: projectObj,
									limit: 20,
									appender: '#compDivContent'
								});
							}
						},
						error: function(xhr, errorType, exception) {
							  console.log(exception);
						}
					});	
				}
			},
			journalFocusOut: function(param, targetNode){
				var popper = $(targetNode).closest('[data-component]');
				var jounalName = $(targetNode).val();
				var customerName = popper.find('[data-class="customer"]').val(); 
				if(jounalName && customerName){
					jQuery.ajax({
						type: "GET",
						url: "/api/getissuelist",
						data: {"jrnlName": jounalName, "client": customerName},
						success: function (respData) {
							if(respData){
								respData = respData.replace(/<meta/g, '<content-meta');
								respData = respData.replace(/<\/meta/g, '</content-meta');
								issueMakeUp.config.issueMeta = respData;
								var volumeObj = {};
								var issueObj  = {};
								$(respData).find('content-meta volume').each(function(){
									var vol = $(this).text();
									volumeObj[vol] = '';
								});
								$(respData).find('content-meta issue').each(function(){
									var iss = $(this).text();
									issueObj[iss] = '';
								});
								$('[data-class="volume"]').parent().find('.autocomplete-content').remove();
								$('[data-class="volume"]').autocomplete({
									data: volumeObj,
									limit: 20
								});
								$('[data-class="issue"]').parent().find('.autocomplete-content').remove();
								$('[data-class="issue"]').autocomplete({
									data: issueObj,
									limit: 20
								});
							}
						},
						error: function(xhr, errorType, exception) {
							console.log(exception);
						}
					});
				}
			}
		}
	}

	return eventHandler;
})(eventHandler || {});
