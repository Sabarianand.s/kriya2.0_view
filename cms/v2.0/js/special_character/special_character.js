$( document ).ready(function() {
	$('body').on({
		mouseover: function (evt) {
			$('.dis_char').text($(this).text());
			$('.dis_char_name').text($(this).attr('title'));
			$('.dis_char_unicode').text("u+"+$(this).attr('data-unicode'));
		},
		click: function(){
			var charList = $(this).text();
			opener.kriya.general.insertSpecialChar(charList);
			window.close();
		}
	}, '.unicode_table .symb');
	$('body').on({
		click: function (evt) {
			var charBlock = $(this).text();
			$('.special_char_search').val('');
			$('.dropdown-button[data-activates="special_char_block"] a').text(charBlock);
			var charList = $('table:contains('+charBlock+')');
			if(charList.length > 0){
				$('.selected').removeClass('selected');
				charList.addClass('selected');
			}
		}
	}, '#special_char_block li');
	$('body').on({
		keyup: function (evt) {
			if (evt.which != 13){
				var searchText = $(this).val();
				searchText = searchText.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
				var searchList = $('.symb[title*="'+searchText+'"], .symb[data-html-entity*="'+ $(this).val() +'"]');
				var searchViewList = '';
				if(searchList.length > 0){
					searchList.each(function(i,ele){
						searchViewList += '<li><a href="#"><span data-name="char-name">'+$(this).attr('title')+'</span>&nbsp;&nbsp;&nbsp;&nbsp;<span data-name="char">'+$(this).text()+'</span></a></li>';
						if(i == 25){
							return false;
						}
					});
				}
				$('#special_char_search_list').html(searchViewList);
				$('#special_char_search_list').css({'display':'block','opacity':'1'});
			}
		}
	}, '.special_char_search');

	$('body').on({
		click: function (evt) {
			var charName = $(this).find('[data-name="char-name"]').text();
			$('.special_char_search').val(charName);
			var charList = $('.symb[title="'+charName+'"]').closest('table');
			if(charList.length > 0){
				$('.selected').removeClass('selected');
				charList.addClass('selected');
			}
			$('.unicode_table .symb.selected').removeClass('selected');
			$('.unicode_table .symb[title="'+charName+'"]').addClass('selected').trigger('mouseover');
			$('.dropdown-button[data-activates="special_char_block"] a').text(charList.find('.separator').text());
			$('#special_char_search_list').css({'display':'none','opacity':'0'});
		}
	}, '#special_char_search_list li');

	$('body').on({
		mouseleave: function(evt){
			$('#special_char_search_list').css({'display':'none','opacity':'0'});
		}
	}, '#special_char_search_list');    

	$('body').on({
		click: function(evt){
			var specialChar = $('[data-class="characterList"]').html();
			opener.kriya.general.insertSpecialChar(specialChar);
		}
	}, '.insertSymbol');

});