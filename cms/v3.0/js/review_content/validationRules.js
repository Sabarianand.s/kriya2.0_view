var validationRules = {
	"element": [
		/*{
			"selector": "#contentDivNode *[href]",
			"functions":[
				{
					"functionName":"validateURL","functionParam":"",
					"action": [{"false":[{"message":"Please enter valid url"}]}]
				}
			]
		},*/
		{
			"role":",preeditor,typesetter,",
			"description":"To validate the email id format",
			"selector":"#contentContainer .jrnlEmail:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkEmailFormat",
					"functionParam":{"mailDomain":"\.[a-z]{2,3}$"},
					"action":[{"false":"email validation failed : Please enter a valid email id"}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if the file attached in the supplementary block",
			"selector":"#contentContainer .jrnlSupplBlock:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkChildElement","functionParam":{"childSelector":".jrnlSupplSrc"},
					"action": [{"false":"Supplementary file missing. Supplementary block should have a file attached"}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate tbody rows has only td elements.",
			"selector":"#contentDivNode table:not([data-track='del']) tbody tr th",
			"action":[
				{
					"true":[
						{
							"functions":[
								{
									"functionName":"highlightElements",
									"functionParam":{"selector":"#contentDivNode table tbody tr th","message":"Table body cells are been tagged as 'th' instead of 'td.'"}
								}
							]
						}
					]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if image element exists in the figure block",
			"selector":"#contentContainer .jrnlFigBlock:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkChildElement","functionParam":{"childSelector":"img:not([data-track='del'])"},
					"action": [{"false":"Image missing. Figure block should have an image"}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if the table blocks has the table element",
			"selector":"#contentContainer .jrnlTblBlock:not([data-track='del']), #contentContainer div.jrnlInlineTable:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkChildElement","functionParam":{"childSelector":"table"},
					"action": [{"false":"Table missing. Table block should have table"}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if the video block has video element",
			"selector":"#contentContainer .jrnlVidBlock:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkChildElement","functionParam":{"childSelector":"video"},
					"action": [{"false":"video content missing. Video block should have video element"}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate the supplementary file holding element has the supplementary file path",
			"selector":"#contentContainer .jrnlSupplBlock:not([data-track='del']) .jrnlSupplSrc:not(.del):not([data-track='del'])",
			"attributes":[
				{
					"name":"href",
					"mandatory": "true",
					"action":[{"false":"Supplementary file path missing."}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate the supplementary file holding element has the supplementary file path",
			"selector":"#contentContainer .jrnlSupplBlock:not([data-track='del']) .jrnlSupplSrc[href='']:not(.del):not([data-track='del'])",
			"action":[
				{
					"true":[
						{
							"functions":[
								{
									"functionName":"highlightElements",
									"functionParam":{"selector":"#contentContainer .jrnlSupplBlock:not([data-track='del']) .jrnlSupplSrc[href='']:not(.del):not([data-track='del'])","message":"Supplementary file path missing."}
								}
							]
						}
					]
				}
			],
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the float block element has attribute 'data-id'",
			"selector":"#contentContainer .jrnlTblBlock:not([data-track='del']), #contentContainer .jrnlFigBlock:not([data-track='del']), #contentContainer .jrnlVidBlock:not([data-track='del']), #contentContainer .jrnlBoxBlock:not([data-block-type='digest']):not([data-track='del']), #contentContainer .jrnlSupplBlock:not([data-track='del'])",
			"attributes": [
				{
					"name": "data-id",
					"mandatory": "true",
					"action":[
						{
							"false":[
								{
									"message":"Float mapping attribute 'data-id' is missing in the block element.",
								}
							]
						}
					]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if the citation elements has the mapping attribute 'data-citation-string'",
			"selector":"#contentContainer .jrnlTblRef:not([data-track='del']), #contentContainer .jrnlFigRef:not([data-track='del']), #contentContainer .jrnlVidRef:not([data-track='del']), #contentContainer .jrnlBoxRef:not([data-track='del']), #contentContainer .jrnlSupplRef:not([data-track='del']), #contentContainer .jrnlBibRef:not([data-track='del'])",
			"attributes": [
				{
					"name": "data-citation-string",
					"mandatory": "true",
					"action":[
						{
							"false":[
								{
									"message":"Float mapping attribute 'data-citation-string' is missing in the citation element.",
								}
							]
						}
					]
				}
			]
		},
		/*{
			"role":",preeditor,typesetter,",
			"description":"To check if the citation elements has the mapping attribute 'data-citation-string'",
			"selector":"#contentContainer .jrnlTblRef, #contentContainer .jrnlFigRef, #contentContainer .jrnlVidRef, #contentContainer .jrnlBoxRef, #contentContainer .jrnlSupplRef, #contentContainer .jrnlBibRef",
			"attributes": [
				{
					"name": "data-citation-string",
					"value":"",
					"action":[
						{
							"true":[
								{
									"message":"Orphan citation exists. Please remove the element or map with corresponding block.",
								}
							]
						}
					]
				}
			]
		},*/
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the citation elements are been mapped to its corresponding block. If not mark it as orphan citation.",
			"selector":"#contentContainer .jrnlTblRef:not([data-track='del']), #contentContainer .jrnlFigRef:not([data-track='del']), #contentContainer .jrnlVidRef:not([data-track='del']), #contentContainer .jrnlBoxRef:not([data-track='del']), #contentContainer .jrnlSupplRef:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkCitationMapping","functionParam":{"checkAgainstBlkAttr":"data-id","blkAttrPrifix":"BLK_","checkAgainstCitAttr":"data-citation-string","rootElement":"#contentContainer"},
					"action": [{"false":[{"message":"Orphan citation exists. Please remove the element or map with corresponding block."}]}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the citation elements are been mapped to its corresponding block. If not mark it as orphan citation.",
			"selector":"#contentContainer .jrnlBibRef:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkCitationMapping","functionParam":{"checkAgainstBlkAttr":"data-id","checkAgainstCitAttr":"data-citation-string","rootElement":"#contentContainer"},
					"action": [{"false":[{"message":"Orphan reference citation exists. Please remove the element or map with corresponding block."}]}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the citation elements are been mapped to its corresponding block. If not mark it as orphan citation.",
			"selector":"#contentContainer .front .jrnlAffRef:not([data-track='del'])",
			"functions":[
				{
					"functionName":"checkCitationMapping","functionParam":{"checkAgainstBlkAttr":"data-id","checkAgainstCitAttr":"data-rid","rootElement":"#contentContainer .front"},
					"action": [{"false":[{"message":"Orphan affiliation citation exists. Please remove the element or map with corresponding block."}]}]
				}
			]
		},
		/*{
			"role":",preeditor,typesetter,",
			"description":"To validate if the float block elements has attribute 'data-stream-name'.",
			"selector":"#contentContainer .body .jrnlTblBlock, #contentContainer .body .jrnlFigBlock, #contentContainer .body .jrnlVidBlock, #contentContainer .body .jrnlBoxBlock, #contentContainer .body .jrnlSupplBlock",
			"attributes": [
				{
					"name": "data-stream-name",
					"mandatory": "true",
					"action":[
						{
							"false":[
								{
									"message":"Attribute 'data-stream-name' is missing in the block element.",
								}
							]
						}
					]
				}
			]
		},*/
		{
			"role":",preeditor,typesetter,",
			"description":"To check if duplication mapping id(data-id) value exists.",
			"selector":"#contentContainer *[data-id]",
			"functions":[
				{
					"functionName":"checkDuplication","functionParam":{"checkAgainst":"data-id","rootElement":"#contentContainer"},
					"action": [{"true":[{"message":"More than one element has same mapping id value."}]}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To check if more than one element has same id value.",
			"selector":"#contentContainer *[id]",
			"functions":[
				{
					"functionName":"checkDuplication","functionParam":{"checkAgainst":"id","rootElement":"#contentContainer"},
					"action": [{"true":[{"message":"More than one element has same id value."}]}]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the mapping id value of float blocks are in accurate format. e.g It should be data-id='BLK_T1' not 'BLK_'",
			"selector":"#contentContainer .jrnlTblBlock:not([data-track='del']), #contentContainer .jrnlFigBlock:not([data-track='del']), #contentContainer .jrnlVidBlock:not([data-track='del']), #contentContainer .jrnlBoxBlock:not([data-block-type='digest']):not([data-track='del']), #contentContainer .jrnlSupplBlock:not([data-track='del'])",
			"attributes": [
				{
					"name": "data-id",
					"value":"BLK_",
					"action":[
						{
							"true":[
								{
									"message":"The float block id is incomplete.",
								}
							]
						}
					]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if space exists in id attribute",
			"selector":"#contentContainer *:not([data-track='del'])[id]",
			"attributes": [
				{
					"name": "id",
					"value":":regex(\\s+)",
					"action":[
						{
							"true":[
								{
									"message":"Id value should not have space.",
								}
							]
						}
					]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if space exists in id attribute",
			"selector":"#contentContainer *[data-id]",
			"attributes": [
				{
					"name": "data-id",
					"value":":regex(\\s+)",
					"action":[
						{
							"true":[
								{
									"message":"Mapping id value should not have space.",
								}
							]
						}
					]
				}
			]
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate if the float block element has attribute 'data-id' and is empty or not.",
			"selector":"#contentContainer *:not([data-track='del'])[data-id='']",
			"action":[
				{
					"true":[
						{
							"functions":[
								{
									"functionName":"highlightElements",
									"functionParam":{"selector":"#contentContainer *:not([data-track='del'])[data-id='']","message":"Mapping id value should not be empty."}
								}
							]
						}
					]
				}
			],
		},
		{
			"role":",preeditor,typesetter,",
			"description":"To validate the table cells or cell count is same from top to bottom of the table. Including the rowspan and colspan",
			"selector":"#contentDivNode .jrnlTblBlock:not([data-track='del']) table, #contentDivNode .jrnlInlineTable:not([data-track='del']) table, #contentDivNode *:not([data-track='del']) table.jrnlInlineTable:not([data-track='del'])",
			"functions":[{"functionName":"validateTable","functionParam":""}]
		}
	]
}