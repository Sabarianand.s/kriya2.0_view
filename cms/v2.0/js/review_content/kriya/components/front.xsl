<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" exclude-result-prefixes="xsi xs xlink mml"><!-- process front node -->
    <xsl:template match="front">
        <div class="front">
            <xsl:apply-templates/>
        </div>
    </xsl:template><!-- Journal Meta Starts -->
    <xsl:template match="journal-meta">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="journal-id">
        <xsl:variable name="id" select="@journal-id-type"/>
        <p class="jrnlID" data-journal-id-type="{$id}">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="journal-title-group">
        <p class="jrnlTitleGroup">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="journal-title">
        <span class="jrnlTitle">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="journal-meta//abbrev-journal-title">
        <p class="jrnlAbbrTitle">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="journal-meta//issn">
        <xsl:variable name="format" select="@publication-format"/>
        <p class="jrnlISSN" data-publication-format="{$format}">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="journal-meta/publisher">
        <p class="jrnlPublisher">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="journal-meta/publisher/publisher-name">
        <span class="jrnlPublisherName">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- Journal Meta Ends --><!-- Article Meta Starts -->
    <xsl:template match="article-meta">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="article-id[@pub-id-type='publisher-id']">
        <p class="jrnlMsNumber">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-id[@pub-id-type='doi']">
        <p class="jrnlDOI">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-categories">
        <xsl:apply-templates select="./subj-group[@subj-group-type='display-channel']/subject" mode="displaychannel"/>
        <p class="jrnlArtType">
            <xsl:value-of select="//article/@article-type"/>
        </p>
        <p class="jrnlCategory">
            <xsl:apply-templates select="./subj-group[@subj-group-type='heading']/subject" mode="category"/>
        </p>
    </xsl:template>
    <xsl:template match="subj-group[@subj-group-type='display-channel']/subject" mode="displaychannel">
        <p class="jrnlArtType">
            <span class="jrnlSubject">
                <xsl:apply-templates select="@*|node()"/>
            </span>
        </p>
    </xsl:template>
    <xsl:template match="subj-group[@subj-group-type='heading']/subject" mode="category">
        <span class="jrnlSubject">
            <xsl:apply-templates select="@*|node()"/>
        </span>
    </xsl:template>
    <xsl:template match="subj-group|subject"/>
    <xsl:template match="title-group">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="title-group/article-title">
        <h1 class="jrnlArtTitle">
            <xsl:apply-templates/>
        </h1>
    </xsl:template>
    <xsl:template match="title-group/alt-title[@alt-title-type='right-running-head']">
        <p class="jrnlRRH">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="title-group/alt-title[@alt-title-type='left-running-head']">
        <p class="jrnlLRH">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="contrib//name/surname">
        <span class="jrnlSurName">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="contrib//name/given-names">
        <span class="jrnlGivenName">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="contrib-group/contrib/name">
        <span class="jrnlAuthor">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="contrib-group/contrib">
        <xsl:variable name="type" select="@contrib-type"/>
        <xsl:variable name="id" select="@id"/>
        <span class="jrnlAuthorGroup">
            <xsl:apply-templates select="@*|node()"/>
        </span>
    </xsl:template>
    <xsl:template match="contrib/collab">
        <span class="jrnlCollaboration">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="contrib-group">
        <p class="jrnlAuthors">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    <xsl:template match="role">
        <span class="jrnlRole">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- xref in contrib -->
    <xsl:template match="contrib/contrib-id">
        <span class="jrnlContribID">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="contrib/xref[@ref-type='aff']">
        <sup class="jrnlAffRef">
            <xsl:apply-templates select=" @*|node() "/>
        </sup>
    </xsl:template>
    <xsl:template match="xref[@ref-type='fn'][starts-with(@rid, 'conf')]"><!-- <xsl:variable name="rid" select="@rid "/> -->
        <span class="jrnlConfRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='fn'][not(starts-with(@rid, 'conf')) and (starts-with(@rid, 'con'))]"><!-- <xsl:variable name="rid" select="@rid "/> -->
        <span class="jrnlConRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='fn'][starts-with(@rid, 'equal-contrib')]|xref[@ref-type='author-notes']"><!-- <xsl:variable name="rid" select="@rid"/> -->
        <span class="jrnlFNRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='other'][starts-with(@rid, 'par-')]"><!-- <xsl:variable name="rid" select="@rid"/> -->
        <span class="jrnlFundRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='fn'][(not(starts-with(@rid, 'par-')) and (starts-with(@rid, 'pa'))) or (starts-with(@rid, 'fn'))]"><!-- <xsl:variable name="rid" select="@rid "/> -->
        <span class="jrnlFNRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='other'][starts-with(@rid, 'dataro')]"><!-- <xsl:variable name="rid" select="@rid"/> -->
        <span class="jrnlDataRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template>
    <xsl:template match="xref[@ref-type='corresp']">
        <span class="jrnlCorrRef">
            <xsl:apply-templates select=" @*|node() "/>
        </span>
    </xsl:template><!-- Affiliation -->
    <xsl:template match="aff">
        <p class="jrnlAff">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta/aff[1]|article-meta/contrib-group[./contrib[@contrib-type='author']]/aff[1]" priority="5">
        <div class="jrnlAffGroup">
            <xsl:apply-templates select=".|following-sibling::*[self::aff]" mode="affNode"/>
        </div>
    </xsl:template>
    <xsl:template match="article-meta/aff|article-meta/contrib-group[./contrib[@contrib-type='author']]/aff" mode="affNode">
        <p class="jrnlAff">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta/aff[position()&gt;1]|article-meta/contrib-group[./contrib[@contrib-type='author']]/aff[position()&gt;1]"/>
    <xsl:template match="aff//addr-line">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="aff/label">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    <xsl:template match="aff/institution[@content-type='dept']">
        <span class="jrnlDepartment">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="aff/institution">
        <span class="jrnlInstitution">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="aff/addr-line/named-content[@content-type='city']">
        <span class="jrnlCity">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="aff/addr-line/named-content[@content-type='state']">
        <span class="jrnlState">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="aff/country">
        <span class="jrnlCountry">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- Author Notes -->
    <xsl:template match="author-notes">
        <div class="author-notes">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="author-notes/fn[@fn-type!='corresp']">
        <p class="jrnlFN">
            <xsl:if test="./@fn-type">
                <xsl:attribute name="data-fn-type">
                    <xsl:value-of select="./@fn-type"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="./@id">
                <xsl:attribute name="id">
                    <xsl:value-of select="./@id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="./label">
                <span class="label">
                    <xsl:value-of select="./label"/>
                </span>
            </xsl:if>
            <span class="jrnlFNPara">
                <xsl:value-of select="./p"/>
            </span>
        </p>
    </xsl:template>
    <xsl:template match="author-notes/corresp|author-notes/fn[@fn-type='corresp']"><!-- <xsl:variable name="id" select="@id"/> -->
        <p class="jrnlCorrAff">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="author-notes/fn[@fn-type='corresp']/p">
        <xsl:apply-templates select=" @*|node() "/>
    </xsl:template>
    <xsl:template match="author-notes/corresp/email|author-notes/fn[@fn-type='corresp']//email">
        <span class="jrnlEmail">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- History Dates -->
    <xsl:template match="history/date[@date-type='accepted']">
        <p class="jrnlAcDate">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="history/date[@date-type='received']">
        <p class="jrnlReDate">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="pub-date">
        <p class="jrnlPubDate">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//day">
        <span class="jrnlDay">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="article-meta//month">
        <span class="jrnlMonth">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="article-meta//year">
        <span class="jrnlYear">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="history">
        <div class="jrnlHistory">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="history/fn/p">
        <p class="jrnlReference">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="fn//pub-id[@pub-id-type='doi']">
        <span class="RefDOI">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="history/fn">
        <xsl:apply-templates/>
    </xsl:template><!-- Permission -->
    <xsl:template match="article-meta//permissions">
        <div class="jrnlPermission">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="article-meta//permissions/copyright-statement">
        <p class="jrnlCopyrightStmt">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//permissions/copyright-year">
        <p class="jrnlCopyrightYear">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//permissions/copyright-holder">
        <p class="jrnlCopyrightHolder">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//permissions/license">
        <p class="jrnlLicense">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//permissions/license/license-p">
        <span class="jrnlLicensePara">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- common -->
    <xsl:template match="article-meta//volume">
        <p class="jrnlVolume">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//elocation-id">
        <p class="jrnlELocation">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//fpage">
        <p class="jrnlFPage">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//lpage">
        <p class="jrnlLPage">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta//self-uri">
        <xsl:variable name="href" select="@xlink:href"/>
        <xsl:variable name="type" select="@content-type"/>
        <p class="jrnlSelfUri" data-href="{$href}" data-content-type="{$type}">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta/related-article">
        <p class="jrnlRelatedArticle">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template><!-- Abstract -->
    <xsl:template match="abstract[not(@abstract-type)]">
        <div class="jrnlAbsGroup">
            <h1 class="jrnlAbsHead">Abstract</h1>
            <xsl:apply-templates select="./p[not(child::ext-link[@ext-link-type='doi'])]" mode="abstract"/>
        </div>
    </xsl:template>
    <xsl:template match="abstract[not(@abstract-type)]/p[not(child::ext-link[@ext-link-type='doi'])]" mode="abstract">
        <p class="jrnlAbsPara">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="abstract[@abstract-type='executive-summary']">
        <div class="jrnlBoxBlock">
            <h1 class="jrnlBoxCaption">
                <xsl:value-of select="./title"/>
            </h1>
            <xsl:apply-templates select="./p[not(child::ext-link[@ext-link-type='doi'])]" mode="digest"/>
        </div>
    </xsl:template>
    <xsl:template match="abstract[@abstract-type='executive-summary']/p[not(child::ext-link[@ext-link-type='doi'])]" mode="digest">
        <p class="jrnlBoxText">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="abstract[@abstract-type='executive-summary']/p"/>
    <xsl:template match="abstract[not(@abstract-type)]/p"/><!-- Keywords -->
    <xsl:template match="kwd-group">
        <div class="jrnlKwdGroup">
            <xsl:if test="./@kwd-group-type">
                <xsl:attribute name="data-kwd-group-type">
                    <xsl:value-of select="./@kwd-group-type"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="./title">
                <h1 class="jrnlKeywordHead">
                    <xsl:value-of select="./title"/>
                </h1>
            </xsl:if>
            <p class="jrnlKeywordPara">
                <xsl:apply-templates/>
            </p>
        </div>
    </xsl:template>
    <xsl:template match="kwd-group/kwd">
        <span class="jrnlKeyword">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- Contract number -->
    <xsl:template match="article-meta/contract-num">
        <p class="jrnlContractNum">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta/contract-sponsor">
        <p class="jrnlContractSponsor">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    <xsl:template match="article-meta/contract-sponsor/named-content">
        <span class="jrnlNamedContent">
            <xsl:apply-templates select="@*|node()"/>
        </span>
    </xsl:template><!-- Funding -->
    <xsl:template match="funding-group">
        <div class="jrnlFundingGroup">
            <table>
                <thead>
                    <th>Institution ID</th>
                    <th>Institution</th>
                    <th>Award ID</th>
                    <th>Award Recipients</th>
                </thead>
                <tbody>
                    <xsl:apply-templates/>
                </tbody>
            </table>
        </div>
    </xsl:template>
    <xsl:template match="funding-group/award-group">
        <xsl:variable name="id" select="@id"/>
        <tr class="award-group" id="{$id}">
            <td class="institution-id" institution-id-type="FundRef">
                <xsl:value-of select=".//institution-id"/>
            </td>
            <td class="institution">
                <xsl:value-of select=".//institution"/>
            </td>
            <td class="jrnlAwardID">
                <xsl:value-of select=".//award-id"/>
            </td>
            <td class="jrnlAwardRecipients">
                <xsl:for-each select="./principal-award-recipient/child::node()">
                    <xsl:choose>
                        <xsl:when test="name(.) = 'name'">
                            <span class="jrnlAuthor">
                                <span class="jrnlSurName">
                                    <xsl:value-of select=".//surname"/>
                                </span>
                                <span class="jrnlGivenName">
                                    <xsl:value-of select=".//given-names"/>
                                </span>
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="funding-statement">
        <tr class="jrnlFundingStatement">
            <td>
                <xsl:apply-templates/>
            </td>
        </tr>
    </xsl:template><!-- Custom Meta Group -->
    <xsl:template match="custom-meta-group">
        <div class="jrnlCustomMetaGroup">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="custom-meta-group/custom-meta">
        <p class="jrnlCustomMeta">
            <xsl:apply-templates select=" @*|node() "/>
        </p>
    </xsl:template>
    <xsl:template match="custom-meta-group/custom-meta/meta-name">
        <span class="jrnlMetaName">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="custom-meta-group/custom-meta/meta-value">
        <span class="jrnlMetaValue">
            <xsl:apply-templates/>
        </span>
    </xsl:template><!-- Article Meta Ends --><!--TODO: eLife specific - please remove later -->
    <xsl:template match="object-id"/>
    <xsl:template match="title"/>
    <xsl:template match="contrib/uri"/><!--TODO: END - eLife specific - please remove later -->
</xsl:stylesheet>