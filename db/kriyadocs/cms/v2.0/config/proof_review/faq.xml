<div id="helpDivNode">
	<p class="changesDivHeader">Help</p>
	<ul class="helpList">
		<li id="quickDemo">
            <p data-message="{'click':{'funcToCall': 'startIntro','channel':'welcome','topic':'begin'}}">Quick Demo</p>
        </li>
		<li>
            <p>Changes<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
			<li>Accept/reject tracked changes
				<ol>
					<li>In the Author Review stage, you will not be able to accept or reject changes made by previous users; you can only reject changes made during this stage.</li>
					<li>Accept or reject tracked changes from a location
					<ol>
				<li>Click on the edit you want to accept or reject.</li>
				<li>Click on <b>Accept</b> or <b>Reject</b> from the pop-up.</li>
				<li>The markup for the edit is removed and the edit becomes permanent (accepted) or is removed (rejected) from the content.</li>
				</ol>
					</li>
					<li>Accept or reject tracked changes from the right panel
						<ol>
							<li>Click on the <b>Changes</b> icon in the navigation toolbar.</li>
							<li>Scroll to the edit that needs to accepted or rejected.</li>
							<li>Click the green tick mark button to accept or the red cross mark button to reject the edit.</li>
							<li>The markup for the edit is removed and the edit becomes permanent (accepted) or is removed (rejected) from the content.</li>
						</ol>
					</li>
				</ol>
			</li>
			<li>Hide/show track changes
				<ol>
					<li>Click on the <b>Hide/Show Track Changes</b> icon to toggle between editor views with and without tracked changes.</li>
					<li>Note: Any edits made to the content in the <b>Hide Track Changes</b> view would be marked-up and would become visible in the <b>Show Track Changes</b> view.</li>
				</ol>
			</li>
			</ol>
		</li>
		<li>
            <p>Editing and formatting content<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
				<li>Inserting text
					<ol>
						<li>Place the cursor at the desired location in the text area and start typing.</li>
					</ol>
				</li>
				<li>Deleting text
					<ol>
						<li>Place the cursor at the start of the character to be deleted and hit the <b>Delete</b> key on the keyboard to delete the character.</li>
						<li>Place the cursor at the end of the character to be deleted and hit the <b>Backspace</b> key on the keyboard to delete the character.</li>
						<li>Select the text to be deleted and hit the <b>Delete</b> key on the keyboard to delete the selected text.</li>
					</ol>
				</li>
				<li>Cut, copy and paste text
					<ol>
						<li>Select the text you want to cut, click the <b>Cut</b> icon to remove the selected text and copy the selected content to the clipboard.</li>
						<li>Select the text you want to copy, click the <b>Copy</b> icon to copy the selected content to the clipboard.</li>
						<li>To paste text, do one of the following:
							<ol>
								<li>Place the cursor at the desired location and click the <b>Paste as text</b> icon to paste text at the location.</li>
								<li>Place the cursor at the desired location and click the <b>Paste as HTML</b> icon to paste content with HTML links at the location.</li>
							</ol>
						</li>
						<li>Note: Standard keyboard shortcuts (Ctrl+x, Ctrl+c, Ctrl+v) can be used to perform cut, copy and paste operations of text.</li>
					</ol>
				</li>
				<li>Apply/change/remove character styles
					<ol>
						<li>To apply/change a character style, select the text and click on one of the different character style icons.</li>
						<li>To remove a character style, select the text and click the style icon that appears selected in the toolbar.</li>
						<li>Styles available are: Bold, Italics, Underline, Superscript and Subscript.</li>
						<li>Note: Standard keyboard shortcuts (Ctrl+b, Ctrl+i, Ctrl+u) can be used to apply/change/remove these character styles.</li>
					</ol>
				</li>
				<li>Change text casing
					<ol>
						<li>To change text casing (capitalization), select the text whose casing you want to change, click the Change Case icon and chose one of the options to apply the desired casing.</li>
					</ol>
				</li>
				<!-- <li>Change text style
					<ol>
						<li>Select the text you want to format  > Home menu  > Click on one of the different styles to apply to the text.</li>
						<li>Styles available are: Bold, Italics, Underline, Superscript and Subscript.</li>
						<li>Note: Standard keyboard shortcuts are supported for formatting text.</li>
					</ol>
				</li> -->
				 <li>Change text and background color
					<ol>
						<li>To change text color, select the text whose color you want to change, click the <b>Text Color</b> icon and choose the desired color from the colors available in the palette.</li>
						<li>To change background color, select the text whose background color you want to change, click the <b>Background Color</b> (paintbrush) icon and choose the desired color from the colors available in the palette.</li>
					</ol>
				 </li>
				 <!-- <li>Remove text formatting
					<ol>
						<li>Select the formatted text  > click on the selected style icon on the toolbar to remove the formatting.</li>
						<li>Paragraph alignment and indentation</li>
					</ol>
				 </li> -->
				
				 <li>Paragraph alignment and indentation
					<ol>
						<li>Select the paragraph and click one of the following icons – <b>Align Left, Align Center, Align Right</b> and <b>Justify</b> – to apply the desired alignment to the selected paragraph.</li>
						<li>Click anywhere in the paragraph whose indentation you want to increase or decrease and click the <b>Increase Indent</b> icon to increase indentation and the <b>Decrease Indent</b> icon to decrease indentation.</li>
					</ol>
				 </li>
				 <li>Bullet and numbered lists
					<ol>
						<li>Bullet list
							<ol>
							<li>Make an existing paragraph a bullet list item
								<ol>
									<li>Place the cursor at the beginning of a paragraph, click the <b>Bullet List</b> icon and choose the desired style for the bullet list from the drop-down menu.</li> 
									<li>To convert multiple paragraphs to a bullet list, select the multiple paragraphs, click the <b>Bullet List</b> icon and choose the desired style for the bullet list from the drop-down menu.</li>
								</ol>
							</li>
							<li>Start a new bulleted paragraph
								<ol>
									<li>Place the cursor at the end of an existing paragraph below (or at the start of an existing paragraph before) which you want to create a bullet list and hit the <b>Enter/Return</b> key on the Keyboard.</li>
									<li>Place the cursor at the beginning of a new paragraph, click the <b>Bullet List</b> icon and choose the desired style of bullet list from the drop-down menu.</li>
									<li>Start typing text and at the end of the item, hit the <b>Enter/Return</b> key on the keyboard to generate the next item of the bullet list.</li>
								</ol>
							</li>
							<li>Remove a bullet list
								<ol>
									<li>Select the bullet list that needs to be removed, click the <b>Bullet List</b> icon and select None from the drop-down menu.</li>  
								</ol>
							</li>
							</ol>
						</li>
						<li>Numbered list
							<ol>
								<li>Make an existing paragraph a numbered list item
									<ol>
										<li>Place the cursor at the beginning of a paragraph, click the <b>Numbered List</b> icon and choose the desired style for the numbered list from the drop-down menu.</li>
										<li>To convert multiple paragraphs to a numbered list, select the multiple paragraphs, click the <b>Numbered List</b> icon and choose the desired style for the numbered list from the drop-down menu.</li>
									</ol>
								</li>
								<li>Start a new numbered paragraph
									<ol>
										<li>Place the cursor at the end of an existing paragraph below (or at the start of an existing paragraph before) which you want to create a numbered list and hit the <b>Enter/Return</b> key on the keyboard.</li>
										<li>Place the cursor at the beginning of a new paragraph, click the <b>Numbered List</b> icon and choose the desired style for the numbered list from the drop-down menu.</li>
										<li>Start typing text and at the end of the item, hit the <b>Enter/Return</b> key on the keyboard to generate the next item of the numbered list.</li>
									</ol>
								</li>
								<li>Remove a numbered list
									<ol>
										<li>Select the numbered list that needs to be removed, click the <b>Numbered List</b> icon and select None from the drop-down menu.</li>
									</ol>
								</li>
							</ol>
						</li>
						<li>Multilevel list
							<ol>
								<li>Select the main list item to be made a sublist item and click the <b>Increase Indent</b> icon or the <b>Tab</b> key on the keyboard once to convert the selected main list item to a sublist item with the default bullet style applied to it.</li>
								<li>Select the sublist item to be made a main list item and click the <b>Decrease Indent</b> icon or the <b>Shift+Tab</b> keys on the keyboard once to converted the selected sublist item to a main list item with the default bullet style applied to it.</li>
								<li>Note: The list style can be changed at any time by clicking the <b>Bullet List</b> or <b>Numbered List</b> icon and choosing the desired style from the drop-down menu.</li>
							</ol>
						</li>
					</ol>
				 </li>
			</ol>
		</li>
		<li>
            <p>Equations<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
				<li>Insert a new equation
					<ol>
					<li>Place the cursor where a new equation is to be inserted  &gt; Insert menu  &gt; click Insert Equation. An equation editor window opens.</li>
					<li>You can create your own equation(s) in the following ways:
						<ol>
								<li>Using Latex commands.</li>
								<li>Use the templates on either side of the main equation editor to add standard elements, operators, fonts, Greek symbols and Math &amp; Physics formula.</li>
						</ol>
					</li>
					<li>The equation can be previewed in the equation preview window at the bottom of the equation editor.</li> 
							<li>Click <b>Ok</b> to insert the equation or Cancel to close the window without saving the equation.</li>
                    </ol>
				</li>
				<li>Edit an equation
					 <ol>
						<li>Click on the equation to edit  &gt; click <b>Edit</b> on the pop-up menu. </li>
						<li>The equation can be edited in the equation editor window using Latex commands and/or the equation templates provided.</li> 
						<li>Click <b>Ok</b> to save the modified equation or <b>Cancel</b> to close the editor window, without saving the changes.</li>
					</ol>
				</li>
				<li>Delete an equation
					<ol>
						<li>Click on the equation  &gt; click <b>Delete</b> on the pop-up menu. A window appears requesting confirmation for deleting the equation.</li>
						<li>Click on <b>Yes</b> to confirm deletion or <b>No</b> to close the window retain the equation.</li>
					</ol>
				</li>
			</ol>
		</li>
		<li>
            <p>Figures<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
                <li>Insert a new figure
				<ol>
							<li>Place the cursor where you wish to insert a new display figure  &gt; Insert menu  &gt; Insert Figure icon. Insert new figure window appears.</li>
							<li>Add mandatory caption and optional footnote.</li> 
							<li>Drag and drop image or upload from local storage.</li>
						</ol>
				</li>
				<li>Replace figure
					<ol>
							<li>Drag and drop stored figures into the insert New Figure window from suitable storage source.</li>
						</ol>
				</li>
				<li>Navigate between figures
					<ol>
							<li>Click on the Figures icon in the right panel  &gt; Icon shows a list of figures in the document.</li>
							<li>Click on three vertical dots near figure titles for details.</li>
						</ol>
				</li>
				<li>Figure citations
						<ol>
							<li>Insert citation from the Insert menu
								<ol>
							<li>Click on the figure citation  &gt; <b>Insert</b> menu  &gt; <b>Insert citation</b> icon  &gt; Window opens  &gt; Select figure tab  &gt; Save.</li>
                            </ol>
							</li>
							<li>Insert by keying
								<ol>
							<li>Place cursor at the desired location  &gt; start typing the figure label  &gt; select from the available choices.</li>
                            </ol>
							</li>
							<li>Add a citation to an existing figure citation
								<ol>
							<li>Click on the figure citation  &gt; click + symbol from the pop-up  &gt; pop up window opens containing different citations  &gt; select the desired citation  &gt; click <b>Save</b> to add the citation.</li>
                            </ol>
							</li>
						</ol>
				</li>
				<li>Figure part label(s)
					<ol>
							<li>Click on the figure citation  &gt; pop up appears with figure title  &gt; click + in part label  &gt; insert desired part label(s) in the box provided.</li>
						</ol>
				</li>
			</ol>
		</li>
		<li>
            <p>Find and Replace<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
				<li>Click on Search at the right corner of the toolbar  &gt; enter text (word or phrase) to search.</li>
						<li>Select/deselect Match whole word or Match case based on your requirement.</li>
						<li>Navigate between the search results using the arrow buttons.</li>
						<li>To replace word, enter replacement text in the replace box  &gt; click Replace to replace text once and Replace All to replace all occurrences in the article.</li>
			</ol>
		</li>
		<li>
            <p>Queries<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
				<li>Insert query
					<ol>
							<li>Select text where you wish to add a query  &gt; click on the <b>Insert Query</b> pop-up.
							Start typing text.</li>
							<li>Choose the recipient of the query from the drop-down menu.</li>
							<li>Click on the checkbox <b>(Reply needed)</b> to request a reply from the recipient.</li> 
							<li>Click on <b>Upload</b> file if you wish to upload a file with the query.</li>
							<li>Click Add to add the query at the location or <b>Cancel</b> to close the dialog box without saving the query.</li>
							<li>The query is displayed on the right panel and the number of queries is displayed on the <b>Query icon</b>. The query citation is displayed next to the selected text.</li>
					</ol>
				</li>
				<li>Edit query
						<ol>
							<li>Queries can be accessed from within the content as well as the right panel.</li>
							<li>Click on the query to edit from the right panel or the content  &gt; Edit  &gt; make the required changes  &gt; click <b>Save</b>.</li>
							<li>If accessing query from the location, scroll up/down to move the query dialog box back to the panel.</li>
							<li>In the <b>edit</b> mode, recipient of the query can be changed; request for reply can be changed and uploaded files can be removed. </li>
						</ol>
				</li>
				<li>Upload a file with the query
						<ol>
							<li>Files can be uploaded during insert or edit query.</li>
							<li>To remove uploaded files, hover over the attached file  &gt; click on the delete icon. Uploaded files can be deleted in insert query and edit query.</li>
							<li>Note: Multiple file formats supported for upload.   </li>
						</ol>
				</li>
				<li>Delete query
							<ol>
							<li>Click on the required query within the text/right panel  &gt; click <b>Delete</b>. A message confirming deletion appears above the toolbar.</li>
						</ol>
				</li>
				<li>Undo deleted query
						<ol>
							<li>Select the required query from location within the text/right panel  &gt; <b>Delete</b>  &gt; click <b>Undo</b> from the confirmation message displayed above the toolbar.</li>
						</ol>
				</li>
			</ol>
		</li>
		<li>
            <p>Save<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
			<li>All the changes made to the article get auto saved with a concluding single click anywhere in the document after the required edits are made.</li>
            </ol>
		</li>
		<li>
            <p>Special Characters<i class="material-icons arrow-denoter">arrow_drop_down</i>
            </p>
			<ol>
				<li>Place cursor at the desired location   &gt; <b>Insert</b> menu  &gt; <b>Insert special character</b>. A separate window containing special characters opens.</li>
						<li>You can insert symbol(s) in the following ways:
							<ol>
							<li>Use the search box to search for specific characters. The special character is highlighted in the panel displayed. Click on the symbol to insert it in the text.</li>
							<li>Browse symbols from the drop-down menu. Hover over the symbols in the panel to see the explanation. Click on the symbol to insert it in the text. </li>
						</ol>
						</li>
			</ol>
		</li>
	</ul>
</div>
