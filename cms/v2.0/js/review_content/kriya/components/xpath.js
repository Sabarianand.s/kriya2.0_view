/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

	//https://developer.mozilla.org/en-US/docs/Web/API/Document/evaluate
	//https://developer.mozilla.org/en-US/docs/Introduction_to_using_XPath_in_JavaScript
    kriya.xpath = function (path, contextNode){
    	
    	//#39 - Figure - Blank image in versions Prabakaran.A(prabakaran.a@focusite.com) - Focus Team
    	if (typeof(XPathResult) == "undefined"){
			return;
		}
    	//End of #39 
    	
		if (typeof(path) == "undefined") return false;
		if (!contextNode) contextNode = $(kriya.config.containerElm)[0];
		//if (! /^\./.test(path)) contextNode = $(kriya.config.containerElm)[0];
		if (typeof(contextNode) != "undefined"){
			if (typeof(contextNode[0]) == "object") contextNode = contextNode[0];
		}
		var results = document.evaluate(path, contextNode, null, XPathResult.ANY_TYPE, null); 
		/* Search the document for all h2 elements.  
		 * The result will likely be an unordered node iterator. */
		var nodes = [];
		var currSelector = results.iterateNext(); 
		while (currSelector) {
			if (currSelector.nodeType == 1){
				//if ($(currSelector).closest('#content').length > 0){
					nodes.push(currSelector);
				//}
			}else{
				nodes.push(currSelector);
			}
			currSelector = results.iterateNext();
		}
		return nodes;
	},
	
	/**
	*# to validate xpath expression, where variables inside it will be 
	*# replaced using another xpath expression
	**/
	kriya.validateXpath = function(inputTagger, container, querySelector, xpathQuery){
		querySelector = (querySelector)?querySelector:'data-selector';

		if (inputTagger[0] == undefined) return;
		if (inputTagger[0].getAttribute(querySelector) == null && inputTagger[0].getAttribute('data-if-selector') == null) return;
		if (typeof(container) != "undefined"){
			if (typeof(container[0]) == "object") container = container[0];
		}
		var selector = inputTagger[0].getAttribute(querySelector);
		if (xpathQuery){
			selector = xpathQuery;
		}
		if (selector == null) selector = inputTagger[0].getAttribute('data-if-selector');
		if (/\$[A-Z]+/.test(selector) && (inputTagger[0].getAttribute('data-variables') != undefined)){
			var variables = inputTagger[0].getAttribute('data-variables').split(',');
			for (var v =0,vl =variables.length; v < vl; v++){
				var varArray = variables[v].split('==');
				var variableObj = kriya.xpath(varArray[1], container);
				if (variableObj.length > 0){
					var varValue = variableObj[0].value.trim()
					varValue = varValue.replace(/ .*?$/, '');
					var re = new RegExp("\\" + varArray[0], "g");
					selector = selector.replace(re, varValue)
				}
			}
		}
		return selector;
	},
	
	kriya.getElementXPath = function(element){
		if (typeof(element) != "undefined"){
			if (typeof(element[0]) == "object") element = element[0];
		}

		if(element.nodeType == Node.ATTRIBUTE_NODE){
			var attrOwnerNode = element.ownerElement;
			var attrName = element.nodeName;
			var ownerXpath = kriya.getElementTreeXPath(attrOwnerNode);
			return ownerXpath + '/@' + attrName;
		}

		if (element && element.id)
			return '//*[@id="' + element.id + '"]';
		else
			return kriya.getElementTreeXPath(element);
	};

	kriya.getElementTreeXPath = function(element){
		var paths = [];

		// Use nodeName (instead of localName) so namespace prefix is included (if any).
		for (; element && element.nodeType == Node.ELEMENT_NODE; element = element.parentNode)
		{
			var index = 0;
			var hasFollowingSiblings = false;
			for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling)
			{
				// Ignore document type declaration.
				if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
					continue;

				if (sibling.nodeName == element.nodeName)
					if (paths.length < 2 && element.hasAttribute('class')){
						if (sibling.hasAttribute('class')){
							if (sibling.getAttribute('class').split(' ')[0] == element.getAttribute('class').split(' ')[0]){
								++index;
							}
						}
					}else{
						++index;
					}
			}

			for (var sibling = element.nextSibling; sibling && !hasFollowingSiblings;
				sibling = sibling.nextSibling)
			{
				if (sibling.nodeName == element.nodeName)
					hasFollowingSiblings = true;
			}

			var tagName = (element.prefix ? element.prefix + ":" : "") + element.localName;
			if(element.getAttribute('data-rid') != undefined && element.getAttribute('data-rid') != ""){
				var pathIndex = '[@data-rid="'+element.getAttribute('data-rid')+'"]';
			}else{
				var pathIndex = (index || hasFollowingSiblings ? "[" + (index + 1) + "]" : "");
			}
			if (element.getAttribute('id') != undefined && element.getAttribute('id') != "") {
				pathIndex = '[@id="'+element.getAttribute('id')+'"]';
			}
			else if ((paths.length < 2) && (element.getAttribute('class') != undefined)) {
				pathIndex = '[contains(@class, "' + element.getAttribute('class').split(' ')[0] + '")]' + pathIndex;
			}

			//paths.splice(0, 0, tagName + tagID + tagClass + pathIndex);
			paths.splice(0, 0, tagName + pathIndex);
			//if (element.getAttribute('id') != undefined && element.getAttribute('id') == "content") element = false
			if (element.hasAttribute('id') && element.getAttribute('id') != undefined && element.getAttribute('id') != "") element = false
		}

		return paths.length ? "//" + paths.join("/") : null;
	};

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));