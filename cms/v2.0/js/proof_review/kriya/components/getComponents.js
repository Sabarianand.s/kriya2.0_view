/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {
    kriya.general = {

		/**
		*#
		*# Method to get HTML from provided selectors
		*# @params : selector, a jQuery selector		
		*# @params : format, html|text, where default will be html
		*# @returns : Array objects of the selectors html|text
		*#
		**/
        getHTML: function (selector, format) {
			if (typeof(selector) == 'undefined') return false;
			var htmlArray = [];
			for (var i = 0, sl = selector.length; i < sl; i++) {
				var child = selector[i];
				if (typeof(format) != 'undefined' && format == 'text'){
					htmlArray.push(child.textContent);
				}else if (child.nodeType == 2){
					htmlArray.push(child.nodeValue);
				}else if (child.nodeType == 3){
					htmlArray.push(child.textContent);
				}else{
					htmlArray.push(child.innerHTML);
				}
			}
			return htmlArray;
		},
        setHTML: function (htmlElement, parentElement) {
			if (parentElement == undefined){
				var container = kriya.config.activeElements;
			}else{
				var container = $(parentElement);
			}
			//if object type is display not inline and if it is not visible then return false
			if($(htmlElement).closest('[data-object-type="display"]').length > 0 && !$(htmlElement).is(':visible')){
				return false;
			}

			if ($(htmlElement).attr('data-sethtml') == "false") return false;

			//if (htmlElement.parentElement.hasAttribute('data-template')) return false;
			if($(htmlElement).closest('[data-template]').length > 0) return false;
			if (htmlElement.parentElement.hasAttribute('data-added')) return false;
			if (htmlElement.hasAttribute('data-template')) return false;
			if ((!$(htmlElement).is(':visible')) && ($(htmlElement).closest('[data-ref-type].hidden').length > 0 || $(htmlElement).closest('[data-ref-type]').parent().hasClass('hidden'))) return false;
			var selector = htmlElement.getAttribute('data-node-xpath');// ? htmlElement.getAttribute('data-node-xpath') : htmlElement.getAttribute('data-selector');
			var nodeValue = htmlElement.getAttribute('data-node-value');
			if (! selector){
				var selector = htmlElement.getAttribute('data-selector');
			}
			if ($(htmlElement)[0].hasAttribute('data-added')){
			// to add new elements - JAI
				var dataChilds = [];
			}else{
				var dataChilds = kriya.xpath(selector, container[0]);
				// to handle product info name - aravind
				if((dataChilds.length>1)&&($(dataChilds).parent().attr('class')=='jrnlProName')){ 
					dataChilds = $(dataChilds).eq(-1);
				}
			}
			if ($(htmlElement).find('[data-selector]').length > 0 && !($(htmlElement)[0].hasAttribute('data-added') || $(htmlElement)[0].hasAttribute('data-deleted'))){
				//Add to history before return false
				//if htmlelement has focus out data
				kriya.general.addToHistory(htmlElement, dataChilds[0], container);
				return false;
			} 
			//var linkEleSelector = htmlElement.getAttribute('data-link-selector');
			var linkEleSelector = kriya.validateXpath($(htmlElement), container, 'data-link-selector');
			var linkElement     = kriya.xpath(linkEleSelector, container[0]);
			//console.log(selector);
			//if (dataChilds.length == 0 && $(htmlElement).html() == "") return false;
			if (dataChilds.length == 0){
				//If he htmlElement value is null then return false 
				if (htmlElement.nodeName == "INPUT" || htmlElement.nodeName == "TEXTAREA" || htmlElement.nodeName == "SELECT"){
					
					if($(htmlElement).attr('type') == 'checkbox' && $(htmlElement).prop('checked') == false)return false;
					if($(htmlElement).val() == "") return false;					
				}else if ($(htmlElement).html() == "") {
					return false;	
				}
				
				//If the html has freez text linke orcid then check content editable element value if null then return false
				if($(htmlElement).attr('data-freez-text') && $(htmlElement).find('[contenteditable="true"]').html() == ""){
					return false;
				}

				var prevSelector = "";
				var nextSelector = "";

				$(htmlElement).closest('[data-wrapper="true"]').find('*[data-selector]:visible').each(function(){
					if ($(this).parent().attr('data-node-xpath') != undefined) return true;
					
					var curEleSelector = $(this)[0].getAttribute('data-node-xpath') ? $(this)[0].getAttribute('data-node-xpath') : $(this)[0].getAttribute('data-selector');
					var curEleContent  =  kriya.xpath(curEleSelector, container[0]);

					//If the curEleContent desn't have the parent which is specified in the data-parent then return or if curEleContent is not with in the content then return false
					if($(curEleContent).length > 0 && ((htmlElement.hasAttribute('data-parent') && $(curEleContent).parent('.' + htmlElement.getAttribute('data-parent')).length < 1))){
						return;
					}

					//If current content is a attribute then continue the loop
					if($(curEleContent).length > 0 && $(curEleContent)[0].nodeType == 2){
						return;
					}

					if ($(this).attr('data-class') == $(htmlElement).attr('data-class') && $(htmlElement).text() == $(this).text()) {
						if (prevSelector == "") {
							//if we have not got the prevSelector still, make it as null and search for nextSelector
							prevSelector = false;
							return true;
						}else{
							// if we have got a selector to insert the current after it, return it
							return false;
						}
					}
					if (typeof(prevSelector) == "boolean" && prevSelector == false && curEleContent.length > 0){
						//nextSelector = $(this)[0].getAttribute('data-node-xpath') ? $(this)[0].getAttribute('data-node-xpath') : $(this)[0].getAttribute('data-selector');
						nextSelector = curEleSelector;
						return false;
					}else if(curEleContent.length > 0){
						//prevSelector = $(this)[0].getAttribute('data-node-xpath') ? $(this)[0].getAttribute('data-node-xpath') : $(this)[0].getAttribute('data-selector');
						prevSelector = curEleSelector;
					}
				});

				//If selector is attribute then updat the attribute value
				//#533 - Insert Table is not working proper. - Prabakaran A(prabakaran.a@focusite.com)
				if(selector && (matches = selector.match(/(?:(\/+)\@)([a-z0-9\-]+)$/ig))){
					//End of #533
					var attrParentSelector = selector.replace(/(?:(\/+)\@)([a-z0-9\-]+)$/ig, '');
					var attrName = matches[0].replace(/(\/+)\@/ig, '');
					attrParentSelector = (attrParentSelector) ? attrParentSelector : ".";
					var dataChild  = document.createAttribute(attrName);
					var attrParent = kriya.xpath(attrParentSelector, container[0]);
					if(attrParent.length > 0){
						attrParent[0].setAttributeNode(dataChild);
					}
				}else{
					if($(htmlElement)[0].hasAttribute('data-create-from-temp')){
						var dataChild = kriya.general.convertTemplate(htmlElement.getAttribute('data-class'));
						if(!dataChild) return false;
						dataChild.attr('data-inserted', 'true');
						if($(htmlElement).attr('data-add-stack')){
							kriyaEditor.settings.undoStack.push(dataChild);
						}
					}else{

						var dataChild = document.createElement('span');
						dataChild.setAttribute('data-inserted', 'true');
						dataChild.setAttribute('id', uuid.v4());
						dataChild.setAttribute('data-class-new', htmlElement.getAttribute('data-class'));
						//to set an attribute from the available html - jai 02-02-2018
						if (htmlElement.hasAttribute('data-set-attribute')){
							var attr = htmlElement.getAttribute('data-set-attribute');
							if (htmlElement.hasAttribute(attr)){
								dataChild.setAttribute(attr, htmlElement.getAttribute(attr));
							}
						}
						//$(dataChild).kriyaTracker('ins');
						// to handle product info - aravind
						if($(htmlElement).closest('[data-component]').attr('data-component')=='jrnlProduct_edit'){														
							if (prevSelector != ""){
								var targetSelector = kriya.xpath(prevSelector, container[0]);
								if (($(targetSelector).attr('class')=="jrnlProSource")&&($(container).find('.jrnlProNameGroup').length)){									
									if($(htmlElement).closest('[data-component]').find('[data-class="jrnlProName"]').eq(-1).attr('data-node-xpath') != undefined){
										prevSelector = $(htmlElement).closest('[data-component]').find('[data-class="jrnlProName"]').eq(-1).attr('data-node-xpath');										
										prevSelector = kriya.xpath(prevSelector, container[0]);										
										prevSelector="//*[@id='"+($(prevSelector).parent().attr('id'))+"']";
									}
								}
								if($(htmlElement).attr('class')=="jrnlProSource"){
									prevSelector = $(container).find('img').attr('id');
								}
							}
						}						
						// to handle related article -aravind
						if($(htmlElement).closest('[data-component]').attr('data-component')=='jrnlRelArt_edit'){
							if (prevSelector != ""){
								var targetSelector = kriya.xpath(prevSelector, container[0]);
								var currEleClass = $(htmlElement).attr('data-class');
								var prevNodesFrmSkl = $("[tmp-class='jrnlRelArt']").find("span[tmp-class='"+currEleClass+"']");
								var newSelector;								
								$(prevNodesFrmSkl).prevAll().each(function(){
									if($(container).find('[class="'+$(this).attr('tmp-class')+'"]').length){
										newSelector = $(container).find('[class="'+$(this).attr('tmp-class')+'"]');										
										return false; 
									}
									else if($(container).find('[data-class-new="'+$(this).attr('tmp-class')+'"]').length){
										newSelector = $(container).find('[data-class-new="'+$(this).attr('tmp-class')+'"]');
										return false;
									}
								});
								if(newSelector != undefined){
									if(newSelector.length>1){
										newSelector = $(newSelector).parent()
									}
									prevSelector = "//*[@id='"+$(newSelector).attr('id')+"']";
								}
							}
						}
						if ($(htmlElement)[0].hasAttribute('data-append')){
							container.append(dataChild);
						}else if (prevSelector != ""){//check for previous sibling
							var prevSelector = kriya.xpath(prevSelector, container[0]);
							if (prevSelector.length > 0){
								prevSelector[0].parentElement.insertBefore(dataChild, prevSelector[0]);
								dataChild.parentElement.insertBefore(prevSelector[0], dataChild);
								var parentClass = $(prevSelector[0].parentElement).removeClass('activeElement').attr('class');
								parentClass = (parentClass)?parentClass.split(' ')[0]:parentClass;
								var currentClass = dataChild.getAttribute('data-class-new');
								var prevSiblingClass = prevSelector[0].getAttribute('class');
								if(prevSiblingClass==undefined){
									prevSiblingClass = prevSelector[0].getAttribute('data-class-new');
								}
								var punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+htmlElement.getAttribute('data-class')+'"]').attr('tmp-node-prefix');
								if (punc == undefined || punc == ""){
									punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+ prevSiblingClass +'"]').attr('tmp-node-suffix');
								}
								punc = (punc != undefined)?punc:'';
								if(punc!=''){
									$(prevSelector[0]).after(document.createTextNode(punc));
								}
							}else{
								container.append(dataChild);
							}
						}else if (nextSelector != ""){ //else check for next sibling
							var nextSelector = kriya.xpath(nextSelector, container[0]);
							if (nextSelector.length > 0){
								nextSelector[0].parentElement.insertBefore(dataChild, nextSelector[0]);
								if(nextSelector[0].previousSibling.nodeType != 3 || (nextSelector[0].previousSibling.nodeType == 3 && nextSelector[0].previousSibling.nodeValue == '')){
									var parentClass = $(nextSelector[0].parentElement).removeClass('activeElement').attr('class');
									parentClass = (parentClass)?parentClass.split(' ')[0]:parentClass;
									var nextSiblingClass = nextSelector[0].getAttribute('class');
									
									var punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+htmlElement.getAttribute('data-class')+'"]').attr('tmp-node-suffix');
									if (punc == undefined || punc == ""){
										punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+nextSiblingClass+'"]').attr('tmp-node-prefix');
									}
									punc = (punc != undefined)?punc:'';
									if(punc!=''){
										$(nextSelector[0]).before(document.createTextNode(punc));
									}	
									//nextSelector[0].parentElement.insertBefore(punc, nextSelector[0]);
								}
							}else{
								container.append(dataChild);
							}
						}else if($(htmlElement).parent().attr('data-node-xpath')){
							var parentNodeXpath = $(htmlElement).parent().attr('data-node-xpath');
							var dataChildParent = kriya.xpath(parentNodeXpath, container[0]);
							if($(dataChildParent).length > 0){
								$(dataChildParent).append(dataChild);
							}else{
								container.append(dataChild);
							}
						}else{
							 //else append to container
							container.append(dataChild);
						}

						if($(htmlElement).attr('data-add-stack')){
							kriyaEditor.settings.undoStack.push(dataChild);
						}

						dataChild.setAttribute('class', dataChild.getAttribute('data-class-new'));
						var nodeXpath = kriya.getElementXPath(dataChild);
						nodeXpath = nodeXpath.replace(/(.*)@class/, '$1@data-class-new');
						$(htmlElement).attr('data-node-xpath', nodeXpath);
						dataChild.removeAttribute('class');
					}
				}
			}else{
				dataChild = dataChilds[0];
				//to add default punc while editing - jai
				var parentClass = $(dataChild).parent().attr('class');
				parentClass = (parentClass) ? parentClass.split(' ')[0] : parentClass;
				if ($(dataChild.previousElementSibling).length > 0){
					var prevSelector = dataChild.previousElementSibling;
					var prevSiblingClass = $(prevSelector).attr('class');
					var punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="' + parentClass + '"]').find('*[tmp-class="' + htmlElement.getAttribute('data-class') + '"]').attr('tmp-node-prefix');
					if (punc == undefined || punc == ""){
						punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+ prevSiblingClass +'"]').attr('tmp-node-suffix');
					}
					punc = (punc != undefined)?punc:'';
					var prev = dataChild.previousSibling;
					/*if (punc != '' && prev && prev.nodeType == 3 && /^[\s\,\u00A0]+$/.test(prev.nodeValue)){
						prev.nodeValue = punc;
					}else */
					//ignore if previous is a text node, add punc only if previous is a element node
					if (punc != '' && prev && (prev.nodeType == 1 || (prev.nodeType == 3 && prev.nodeValue != punc))){
						$(prevSelector).after(document.createTextNode(punc));
					}
				}
			}

			/**
			 *  STEP 1 : If htmlElement nodeName is not INPUT or TEXTAREA then set the value as a html
			 *
			 *  STEP 2 : If the input type is a checkbox then set the value is true or false
			 *           else the nodeType is 2 (attribute node) then set the nodevalue
			 */
			if (htmlElement.nodeName == "INPUT" || htmlElement.nodeName == "TEXTAREA" || htmlElement.nodeName == "SELECT"){
				if(!nodeValue){
					if($(htmlElement).attr('type') == "checkbox" || $(htmlElement).attr('type') == "radio"){
						nodeValue = $(htmlElement).prop('checked');
						if(nodeValue == true){
							nodeValue = 'true';
							if ($(htmlElement)[0].hasAttribute('data-value')){
								nodeValue = $(htmlElement).attr('data-value');
							}
						}else{
							nodeValue = 'false';
						}
					}else{
						nodeValue = $(htmlElement).val();
					}	
				}else if(nodeValue && $(htmlElement).attr('type') == "checkbox" && !$(htmlElement).prop('checked')){
					//If checkbox is unchecked then nodeValue is empty - jagan					
					nodeValue = '';
				}

				if(dataChild.nodeType == 2){
					if (nodeValue != "false"){
						dataChild.nodeValue = nodeValue;
						$(dataChild.ownerElement).attr(dataChild.nodeName, dataChild.nodeValue);
					}
				}else{
					$(dataChild).html(nodeValue);
					if($(linkElement).length > 0){
						$(linkElement).html(nodeValue);
					}
				}
			}else{
				if ($(htmlElement)[0].hasAttribute('data-added') || $(htmlElement)[0].hasAttribute('data-deleted')){
					// to remove other attributes and add only class to its inner html - JAI
					// case : add new author in reference
					// Only the Author will be save along with its child at one shot and avoid its childs from saving
					$(htmlElement).find('[data-class]').each(function(){
						var dataClass = $(this).attr('data-class');
						while($(this)[0].attributes.length > 0)
							$(this)[0].removeAttribute($(this)[0].attributes[0].name);
						$(this).attr('class', dataClass);
					});
				}

				//to hanlde deleted nodes
				if ($(htmlElement)[0].hasAttribute('data-deleted')){
					if ($(htmlElement).parent().hasClass('del')){
						for (var i = 0; i < $(htmlElement).parent()[0].attributes.length; i++) {
							var attrib = $(htmlElement).parent()[0].attributes[i];
							$(dataChild)[0].setAttribute(attrib.name, attrib.value);
						}
					}
					$(dataChild)[0].setAttribute('data-old-class', $(htmlElement).attr('data-class'));
				}
				//to remove "delete link button" from htmlElement before saving - jai 02-02-2018
				if ($(htmlElement)[0].hasAttribute('data-append-button') && $(htmlElement).find('.' + $(htmlElement).attr('data-append-button')).length > 0){
					$(htmlElement).find('.' + $(htmlElement).attr('data-append-button')).remove();
				}
				
				var nodeHtml =  $(htmlElement).html();
				//If the node has the freez text then unwrap the element and update the text EX:ORCID
				if($(htmlElement).attr('data-freez-text')){
					if($(htmlElement).find('[contenteditable="true"]').html() != ""){
						$(htmlElement).find('.freezText, [contenteditable="true"]').contents().unwrap();
						nodeHtml =  $(htmlElement).html();	
					}else{
						nodeHtml =  "";
					}
				}

				//If dataChild is a text node or attribute node then update node value else update html
				if(dataChild.nodeType == 2 || dataChild.nodeType == 3){
					dataChild.nodeValue = nodeHtml;
				}else{
					//to retain query nodes while saving the html to its parent; case: adding query to footnote and saving it - JAI 27-02-2018
					if ($(dataChild).find('.jrnlQueryRef').length > 0 && $(dataChild).find('.jrnlQueryRef').length > $('<div>' + nodeHtml + '</div>').find('.jrnlQueryRef').length){
						$(dataChild).find('.jrnlQueryRef').each(function(){
							if ($('<div>' + nodeHtml + '</div>').find('.jrnlQueryRef[data-rid="' + $(this).attr('data-rid') + '"]').length == 0){
								nodeHtml = $(this)[0].outerHTML + nodeHtml;
							}
						})
					}
					$(dataChild).html(nodeHtml);
				}
				
				
				if($(linkElement).length > 0){
					$(linkElement).html(nodeHtml);
				}
			}

			if ($(dataChild).html() == ""){
				if($(dataChild).closest('[data-inserted]').length){
					if($(dataChild)[0].nextSibling && $(dataChild)[0].nextSibling.nodeType == 3){
						$(dataChild)[0].nextSibling.remove();
					}	
				}
				else{
					if($(dataChild)[0].nextSibling && $(dataChild)[0].nextSibling.nodeType == 3 && !($(dataChild)[0].nextSibling.nextSibling)){
						$(dataChild)[0].nextSibling.remove();
					}
				}				
				if($(dataChild)[0].previousSibling && $(dataChild)[0].previousSibling.nodeType == 3){
					$(dataChild)[0].previousSibling.remove();
				}
				$(dataChild).attr('data-removed', 'true');
				var parentNode = dataChild.parentNode;
				$(dataChild).remove();
				if ($(htmlElement).closest('.hidden').length > 0 && parentNode.innerHTML == ""){
					$(parentNode).remove();
				}
			}else if(dataChild.nodeType == 2 && nodeValue == "false" && $(htmlElement).attr('type') != "radio"){
				$(dataChild.ownerElement).removeAttr(dataChild.nodeName);
			}
			if($(htmlElement).attr('data-track') == "del"){
				$(dataChild).kriyaTracker('del');
			}

			kriya.general.addToHistory(htmlElement, dataChild, container);


			//Add stack if component has data-add-stack
			if($(htmlElement).attr('data-add-stack') && !$(dataChild).attr('data-inserted')){
				if(dataChild.nodeType!=2){
					kriyaEditor.settings.undoStack.push(dataChild);
				}
				else{
					kriyaEditor.settings.undoStack.push(dataChild.ownerElement);
				}				
			}
		},
		setProName: function (htmlElement, parentElement){
			if (parentElement == undefined){
				var container = kriya.config.activeElements;
			}else{
				var container = $(parentElement);
			}
			var selector = htmlElement.getAttribute('data-node-xpath');
			var nodeValue = htmlElement.getAttribute('data-node-value');
			if (! selector){
				var selector = htmlElement.getAttribute('data-selector');
			}
			if ($(htmlElement)[0].hasAttribute('data-added')){			
				var dataChilds = [];
			}else{
				var dataChilds = kriya.xpath(selector, container[0]);
			}
			if ($(htmlElement).find('[data-selector]').length > 0 && !($(htmlElement)[0].hasAttribute('data-added') || $(htmlElement)[0].hasAttribute('data-deleted'))){				
				kriya.general.addToHistory(htmlElement, dataChilds[0], container);
				return false;
			}
			// var nodeHtml =  $(htmlElement).html();
			if(dataChilds.length==0){
				var dataChild = kriya.general.convertTemplate(htmlElement.getAttribute('data-class'));
				if(!dataChild) return false;
				$(htmlElement).removeAttr('data-added');
				$(container).find('.jrnlProNameGroup').append(dataChild);
				dataChild.attr('data-inserted', 'true');				
			}
		},
		/**
		 * Function to set the float elements from the components
		 */
		setFloat : function(htmlElement, parentElement){
			var popper = $(htmlElement).closest('[data-component]');
			var replaceImgId = $(htmlElement).attr('data-replace-imgid');// Start Issue Id - #448 - Dhatshayani . D Jan 10, 2018 - To replace only the selected image.
			
			// handle empty image tag : case occured in author bio - JAI 18-06-2018
			//modify by jagan - check src only if htmlElement is image
			if ($(htmlElement).length > 0 && $(htmlElement)[0].nodeName == "IMG" && $(htmlElement).attr('src') == undefined) return;
			
			if (parentElement == undefined){
				var container = kriya.config.activeElements;
			}else{
				var container = $(parentElement);
			}
			//When insert new table at that time adding unwanted foottext in table block-kirankumar
			if( htmlElement.closest('[data-component="TABLE_edit"]') != null && !popper.find('[data-pop-type="new"]').hasClass('hidden')){
				container.find('.jrnlTblFootText').remove();
				container.find('.jrnlTblFoot').remove();
				}
			var nodeClass  = $(htmlElement).attr('data-class');
			if(container.find('table').length > 0){
				var dataNode = container.find('table');
				dataNode.html(htmlElement.innerHTML);
				if(dataNode[0].nodeName == "TABLE"){
					setTableIndex(dataNode);
				}
				kriya.general.addToHistory(htmlElement, dataNode, container);
			}else if(container[0].nodeName == "TABLE"){
				var dataNode = container;
				dataNode.html(htmlElement.innerHTML);
				kriya.general.addToHistory(htmlElement, dataNode, container);
			}else if(!$(htmlElement).hasClass('hidden')){
				if(container[0].nodeName == "IMG"){
					var dataNode = container;
				}else if(container.find('img').length > 0){
					// Start Issue Id - #448 - Dhatshayani . D Jan 10, 2018 - To replace only the selected image.
					if(replaceImgId && replaceImgId != ''){
						var dataNode = container.find('.'+nodeClass+'#'+replaceImgId+':not([data-track="del"])');
						$(htmlElement).removeAttr('data-replace-imgid');//by kirankumar issue:-after repalacing a fig  insert a new figure  it will display two times
					}else{
						var dataNode = container.find('.'+nodeClass+':not([data-track="del"])');
					}// End Issue ID - #448
				}else{
					var dataNode = container.find('.'+nodeClass);
				}

				if(dataNode.length == 0){
					var dataParentClass = $(htmlElement).attr('data-parent-class');
					if(dataParentClass){
						var dataNodeTemp = $('#compDivContent [data-element-template="true"] [tmp-class="' + dataParentClass + '"]').clone(true);
					}else{
						var dataNodeTemp = $('#compDivContent [data-element-template="true"] [tmp-class="' + nodeClass + '"]').clone(true);	
					}
					
					if(dataNodeTemp.length > 0){
						dataNodeTemp.removeAttr('tmp-insert-after').removeAttr('tmp-insert-before').attr('tmp-insert-at', 'false');
						dataNodeTemp = kriya.general.convertTemplate(nodeClass, '', '', dataNodeTemp);
						if(dataNodeTemp && dataNodeTemp.length > 0){
							$(container).append(dataNodeTemp);
							if(container.find('.'+nodeClass).length > 0){
								dataNode = container.find('.'+nodeClass);
							}
						}
					}
				}

				if(dataNode.length > 0){
					//add tracked images when replacing
					if(nodeClass == 'jrnlFigure' && $(htmlElement).closest('[data-name="biography"]').length > 0){
						dataNode.attr('src', $(htmlElement).attr('src')).removeAttr('width').removeAttr('height').attr('data-primary-extension', $(htmlElement).attr('data-primary-extension'));
						dataNode.attr('data-original-name', $(htmlElement).attr('data-original-name')).attr('data-fileSize', $(htmlElement).attr('data-fileSize')).attr('data-colorspace',$(htmlElement).attr('data-colorspace')).attr('data-geometry',$(htmlElement).attr('data-geometry'));						
						kriyaEditor.settings.undoStack.push(dataNode.parent());
						kriya.general.addToHistory(htmlElement, dataNode, container);
					}else if(nodeClass == 'jrnlFigure' && $(htmlElement).attr('src') != $(dataNode).attr('src')){
						var clonedImage = $('<img class="jrnlFigure"/>');
						clonedImage.attr('src', $(htmlElement).attr('src')).removeAttr('width').removeAttr('height').attr('data-primary-extension', $(htmlElement).attr('data-primary-extension'));
						clonedImage.attr('data-original-name', $(htmlElement).attr('data-original-name')).attr('data-fileSize', $(htmlElement).attr('data-fileSize')).attr('data-colorspace',$(htmlElement).attr('data-colorspace')).attr('data-geometry',$(htmlElement).attr('data-geometry'));						
						clonedImage.attr('id', uuid.v4());
						dataNode.after(clonedImage);
						dataNode.attr('data-old-src', dataNode.attr('src')).removeAttr('src').removeAttr('data-alt-src');
						//If the figure popup is new for insert then remove the data node
						//Else add track changes for figure
						if(popper.find('.popupHead:visible').attr('data-pop-type') != "new"){
							dataNode.attr('data-track', 'del');
							clonedImage.kriyaTracker('ins');
						}else{
							dataNode.remove();
						}
						clonedImage.closest('[class="jrnlFigBlock"]').find('.floatHeader').find('.image-versions').remove();
						if(popper.find('.popupHead:visible').attr('data-pop-type') != "new"){
							clonedImage.closest('[class="jrnlFigBlock"]').find('.floatHeader').prepend('<span class="btn btn-small image-versions" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'showImageVersions\'}"><i class="material-icons">collections</i> Versions</span>');
						}
						this.updateRightNavPanel(clonedImage.closest('[class="jrnlFigBlock"]').attr('data-id'), kriya.config.containerElm);
						kriyaEditor.settings.undoStack.push(clonedImage.parent());
						kriya.general.addToHistory(htmlElement, clonedImage, container);
					}else{
						$.each($(htmlElement)[0].attributes,function(i,attr){
							var attrVal = $(htmlElement).attr(attr.nodeName);
							if(attrVal && attr.nodeName != "data-type" && attr.nodeName != "data-selector" && attr.nodeName != "data-class"){
								if(attr.nodeName == "class"){
									$(dataNode).addClass(attrVal);
								}else{
									$(dataNode).attr(attr.nodeName, attrVal);		
								}
							}
						});
						dataNode.html(htmlElement.innerHTML);
						kriya.general.addToHistory(htmlElement, dataNode, container);
					}
				}
			}
		},
		setInputTag : function(htmlElement, parentElement){
			if (parentElement == undefined){
				var container = kriya.config.activeElements;
			}else{
				var container = $(parentElement);
			}
			var selector = htmlElement.getAttribute('data-source-selector');
			if (! selector){
				return;
			}
			if (htmlElement.hasAttribute('data-parent-container')){
				selector = htmlElement.getAttribute('data-parent-container') + selector;
			}
			var dataChilds = kriya.xpath(selector, container[0]);
			this.addLinksToObjects(htmlElement, parentElement);
			var separator  = $(htmlElement).attr('data-separator');
			//if ($(htmlElement)[0].hasAttribute('data-free-input')){
			var clone = $(htmlElement).find('.kriya-tagsinput').clone(true);
			clone.find('.removeKriyaTag').remove();
			
			var htmlContent = "";
			clone.find('.tags').each(function(){
				//htmlContent += '<span class="jrnlCollaboration">'+$(this).text()+'</span>';
				htmlContent += $(this).html();
			});
				
			//}else{
				//var htmlContent = kriya.general.getInputTagHtml(htmlElement).join(separator);	
			//}
			$(dataChilds).html(htmlContent);
		},
		getInputTagHtml : function(htmlElement){
			
			var returnVal = [];
			var selector  = htmlElement.getAttribute('data-suggestion');
			
			var dataLinks = kriya.xpath(selector);
			var templateNode = $(htmlElement).find('[data-set-template]');
			$(htmlElement).find('.kriya-tagsinput .tags').each(function(){
				if($(this).attr('data-fulltext')){
					var suggestion = $(this).attr('data-fulltext');
				}else{
					var suggestion = $(this).text().trim();
				}
				$(dataLinks).each(function(){
					if ($(this).text().trim() == suggestion){
						var clonedThis = $(this).clone(true);
						clonedThis.removeAttr('id');
						clonedThis.find('[id]').removeAttr('id');
						if(templateNode.length > 0){
							var clonedTemp = templateNode.clone(true);
							clonedTemp.removeAttr('data-set-template');
							clonedTemp.removeClass('hidden');
							clonedTemp.html($(clonedThis)[0].outerHTML);
							returnVal.push(clonedTemp[0].outerHTML);
						}else{
							returnVal.push($(clonedThis)[0].outerHTML);
						}
						return false;
					}
				});
			});
			return returnVal;
		},
		/**
		*#
		*# Method to get JSON text from the provided url
		*# @params : URL from where thr json to be read
		*# @returns : Array objects of the json
		*#
		**/
        getJSON: function (url) {
			if (typeof(url) == 'undefined') return false;
			var jsonData = '';
			//using aJax inStead of getJSON is for asyn
			/*$.getJSON( url, function( data ) {});*/
			$.ajax({
			  url: url,
			  dataType: 'json',
			  async: false,
			  success: function(data) {
				jsonData = data;
			  }
			});
			return jsonData;
		},
		
		/**
		*# function to get the linked reference object from the current active element and push it to display in a container
		*# param: htmlElement, xpath in the htmlElement helps to get the links
		*# and with the links, we get the reference object (e.g. get related affiliations)
		**/
		getLinkedObjects: function(container, component, htmlElement){
			//var selector = htmlElement.getAttribute('data-selector');
			var selector = kriya.validateXpath($(htmlElement), this.container);
			var dataLinks = kriya.xpath(selector, container[0]);
			if ($(htmlElement).find('*[data-template]').length > 0){
				var template = $(htmlElement).find('*[data-template]')[0];
			}
			$(htmlElement).children('*:not("[data-template],.collection-header")').remove();
			for (var i = 0, dl = dataLinks.length; i < dl; i++) {
				var dataLink = dataLinks[i];
				var oldAffRef = dataLink.getAttribute('data-rid');
				oldAffRef = (oldAffRef) ? oldAffRef : '';
				oldAffRef = oldAffRef.split(', ');
				oldAffRef.forEach(function(rid) {
					var linkedObj = $(kriya.config.containerElm + ' [data-id="' + rid +'"]:not([data-track="del"])');
					if (linkedObj.length > 0){
						linkedObj = linkedObj[0];
						if (template){
							var cloneNode = template.cloneNode(true);
							cloneNode.removeAttribute('data-template');
							cloneNode.setAttribute('class', cloneNode.getAttribute('class') + ' ' + linkedObj.getAttribute('class'))
							if ($(cloneNode).find('[data-input]').length > 0){
								$(cloneNode).find('[data-input]').html(linkedObj.innerHTML)
							}else{
								cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
							}
						}else{
							var cloneNode = document.createElement('li');
							cloneNode.setAttribute('class', linkedObj.getAttribute('class'));
							cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
						}
						cloneNode.setAttribute('data-rid', linkedObj.getAttribute('data-id'));
						htmlElement.appendChild(cloneNode);
					}
				});
			}
		},
		getLinkedCitations: function(container, component, htmlElement){
			var citeString = $(container).attr('data-citation-string');
			citeString = (citeString)?citeString:'';
			if(!citeString.trim()){
				kriya.notification({
                    title : 'Error',
                    type  : 'error',
                    timeout : 3000,
                    content : 'Invalid Citation',
                    icon : 'icon-info'
                });
				return false;
			}
			var cites = citeString.replace(/^[\s]+|[\s]+$/g, '');
			cites = cites.split(' ');
			$('.citeHolder').remove();
			if (cites.length > 1){
				tabs = $('<div class="citeHolder">Citations: </div>')
				$(htmlElement).before(tabs);
			}
			var partLabelObj = kriya.general.getCitationLabel(container);
			$.each(cites, function(i,v){
				if (cites.length > 1){
					$(tabs).append('<span class="cite" data-cite-id="'+ v + '" data-message="{\'click\':{\'funcToCall\': \'changeCiteTab\',\'channel\':\'components\',\'topic\':\'PopUp\'}}">'+ v.replace(/[A-Z]/gi, '') + '</span>');
				}
				var newNode = htmlElement;
				if ($(htmlElement).attr('data-template') != undefined){
					newNode = htmlElement.cloneNode(true);
					newNode.removeAttribute('data-template');
					newNode.setAttribute('data-clone', 'true');
					newNode.setAttribute('data-cite', v);
					// to remove primary citation button if cite are more than one
					if (cites.length > 1){
						$(newNode).find('.primaryCite').remove();
					}
					htmlElement.parentNode.insertBefore(newNode, htmlElement)
					var selector = $(newNode).attr('data-selector');
					selector = selector.replace('$ID', v);
					$(newNode).attr('data-selector', selector);

					//dropddowm button remove -template from activates attribute
					$(newNode).find('[data-activates]').each(function(){
						var activates = $(this).attr('data-activates');
						var dropDown = $(newNode).find('#' + activates);
						activates = activates.replace(/-template$/g, '');
						dropDown.attr('id', activates);
						$(this).attr('data-activates', activates);
						$(this).dropdown();
						if(citeJS.settings.R.citationType != '1' && $(this).hasClass('possessiveCitation')){
							$(this).addClass('hidden');
						}
					});

					$(newNode).find('[data-message*="$citeId"]').each(function(){
						var message = $(this).attr('data-message');
						message = message.replace('$citeId', v);
						$(this).attr('data-message', message);
					});
				}
				kriya.popUp.htmlComponent(newNode, container);
				//$(newNode).remove();

				var idNum = v.match(/(\d+)$/)[0];
				var partLabelTemplate = $(newNode).find('[data-template="true"][data-class="partLabel"]');
				//Add existing part label in the popup
				if(partLabelObj[idNum] && partLabelTemplate.length > 0){
					$.each(partLabelObj[idNum]['label'], function(i, val){
						if(val != ""){
							var clonedPartLabel = partLabelTemplate[0].cloneNode(true);
							$(clonedPartLabel).html(val);
							$(clonedPartLabel).removeAttr('data-template');
							$(clonedPartLabel).insertBefore(partLabelTemplate);
						}
					});
				}
				//$(htmlElement).find('')
			});
			$('.citeHolder .cite').first().addClass('active');
			$(htmlElement).parent().find('[data-type="getLinkedCitations"][data-clone]:not(":first")').addClass('hidden');
		},
		/**
		*# function which push an existing reference (on selecting from the drop-down option) 
		*# to the htmlContainer which holds the currently affiliated references
		**/
		linkObjects: function(htmlElement){
			container = kriya.config.activeElements;
			var parentDropDown = $(htmlElement).closest('.dropdown-link-content')
			var rid = htmlElement.getAttribute('data-rid');
			var Objclass = $(htmlElement).attr('data-class');
			$(htmlElement).remove();
			
			htmlElement = parentDropDown.closest('[data-component]').find('*[data-type="getLinkedObjects"][data-source="' + Objclass + '"]')[0];

			if ($(htmlElement).find('*[data-template]').length > 0){
				var template = $(htmlElement).find('*[data-template]')[0];
			}
			if ($(kriya.config.containerElm + ' [data-id="' + rid +'"]').length > 0){
				var linkedObj = $(kriya.config.containerElm + ' [data-id="' + rid +'"]')[0];
				if (template){
					var cloneNode = template.cloneNode(true);
					cloneNode.removeAttribute('data-template');
					cloneNode.setAttribute('class', cloneNode.getAttribute('class') + ' ' + linkedObj.getAttribute('class'));
					if ($(cloneNode).find('[data-input]').length > 0){
						$(cloneNode).find('[data-input]').html(linkedObj.innerHTML)
					}else{
						cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
					}
				}else{
					var cloneNode = document.createElement('li');
					cloneNode.setAttribute('class', 'collection-item dismissable ' + linkedObj.getAttribute('class'));
					cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
				}
				cloneNode.setAttribute('data-rid', linkedObj.getAttribute('data-id'))
				htmlElement.appendChild(cloneNode);
			}
			if ($(parentDropDown).children('*:not("[data-template],.collection-header")').length == 0){
				$(parentDropDown).parent('.btn').addClass('disabled');
			}
			
			var objects = $(htmlElement).find('> [data-rid]:not([data-template="true"])');
			var focusData = '';
			objects.each(function(){
				focusData += $(this).attr('data-rid') + ',';
			});
			focusData = focusData.replace(/([\,\s]+$)/g, '');
			$(htmlElement).attr('data-focusout-data', focusData);

			parentDropDown.addClass('hide');
		},
		/**
		*# remove link from the current active element
		**/
		removeLinks: function(htmlElement){
			container = kriya.config.activeElements;
			//added to just remove the list from its parent (specifically done for author roles) - jai 02-02-2018
			if ($(htmlElement).closest('[data-type="nonLinkedObject"]').length > 0){
				$(htmlElement).attr('data-focusout-data', '');
				$(htmlElement).html('');
				return true;
			}
			var parentDropDown = $(htmlElement).closest('[data-type="getLinkedObjects"]');
			parentDropDown.parent().find('ul[data-type="getNonLinkedObjects"]').parent('.btn').removeClass('disabled');
			var className = parentDropDown[0].getAttribute('data-class');
			var rid = htmlElement.getAttribute('data-rid');

			//container.find('.' + className + '[data-rid="' + rid + '"]').remove();
			$(htmlElement).remove();

			var objects = $(parentDropDown).find('> [data-rid]:not([data-template="true"])');
			var focusData = '';
			objects.each(function(){
				focusData += $(this).attr('data-rid') + ',';
			});
			focusData = focusData.replace(/([\,\s]+$)/g, '');
			$(parentDropDown).attr('data-focusout-data', focusData);
		},
		/**
		*# function to get all the non-linked references and put it to display in an htmlElement
		*# @param: htmlElement
		*# htmlelement has a xpath expression of the reference, which wil first get the related links
		*# and then on secordary get the ones which are not linked to the container
		**/
		getNonLinkedObjects: function(container, component, htmlElement){
			//var selector = htmlElement.getAttribute('data-selector');
			if ($(htmlElement).find('*[data-template]').length > 0){
				var template = $(htmlElement).find('*[data-template]')[0];
			}
			$(htmlElement).children('*:not("[data-template],.collection-header")').remove();
			var selector = kriya.validateXpath($(htmlElement), this.container);
			var dataLinks = $(htmlElement).closest('*[id*="_tab"]').find('*[data-type="getLinkedObjects"] li[data-rid]');
			if (dataLinks.length == 0){
				dataLinks = $(htmlElement).closest('.collection-header').parent().find('*[data-type="getLinkedObjects"] li[data-rid]');
			}
			var newSelector = htmlElement.getAttribute('data-source-selector');
			if (htmlElement.hasAttribute('data-parent-container')){
				newSelector = htmlElement.getAttribute('data-parent-container') + newSelector;
			}
			//when group-authors is added from edit authors popup
			if($(htmlElement).closest('[data-type="popUp"]').length > 0 && $(htmlElement).closest('[data-type="popUp"]')[0].hasAttribute('data-component-xpath')){
				var compXpath = $(htmlElement).closest('[data-type="popUp"]').attr('data-component-xpath');
				var componentType = $(htmlElement).closest('[data-type="popUp"]').attr('data-component');
				var parentComponent = $('[data-type="popUp"][data-component="' + compXpath + '"]');
				if (parentComponent.length > 0 && parentComponent.find('[data-temp-container][data-component-type="' + componentType + '"]').length > 0){
					tempContainer = parentComponent.find('[data-temp-container][data-component-type="' + componentType + '"]');
					if (tempContainer[0].hasAttribute('data-node-xpath')){
						tempContainer = kriya.xpath(tempContainer.attr('data-node-xpath'));
						if (tempContainer.length > 0){
							tempContainer = tempContainer[0].firstElementChild
						}
					}else{
						
					}
				}
			}else if ((!container) && $(htmlElement).closest('[data-type="popUp"]').length > 0 && $(htmlElement).closest('[data-type="popUp"]')[0].hasAttribute('data-node-xpath')){
				var compXpath = $(htmlElement).closest('[data-type="popUp"]').attr('data-node-xpath');
				container = kriya.xpath(compXpath);
				tempContainer = container;
			}else{
				tempContainer = container;
			}
			newSelector = kriya.validateXpath($(htmlElement), tempContainer, 'data-source-selector', newSelector);
			if (typeof(newSelector) == 'undefined') return false;
			if (dataLinks.length == 0 && $(kriya.evt.target).find('ul[data-type="getNonLinkedObjects"]').length == 0){
				dataLinks = kriya.xpath(selector, container[0]);
			}
			if (dataLinks.length > 0) {
				newSelector += "[@id]";
				newSelector += "[not(contains('|";
				for (var i = 0, dl = dataLinks.length; i < dl; i++) {
					var dataLink = dataLinks[i];
					var oldAffRef = dataLink.getAttribute('data-rid');
					if(oldAffRef){
					oldAffRef = oldAffRef.split(', ');
					oldAffRef.forEach(function(rid) {
						newSelector += rid + '|';
					});
					}
				}
				newSelector += "',concat('|', @data-id, '|')))]";
			}
			var dataLinks = kriya.xpath(newSelector, tempContainer);
			//when adding new author, to get previously added group affiliation of that group - jai 21-08-2017
			if (dataLinks.length == 0 && $(tempContainer).closest('.manualSave').length > 0 && /\$ID/.test(newSelector)){
				dataLinks = kriya.xpath('.' + htmlElement.getAttribute('data-source-selector'), tempContainer);
			}
			if (dataLinks.length == 0) {
				$(htmlElement).parent('.btn').addClass('disabled');
				return false;
			}
			
			//Remove the hidden links from list - jagan - #649
			dataLinks = $(dataLinks).not(':not(:visible)');

			//$(htmlElement).removeClass('hide');
			for (var i = 0, dl = dataLinks.length; i < dl; i++) {
				var linkedObj = dataLinks[i];
				if (linkedObj.getAttribute('data-track') == "del"){
					//continue;
				}
				var displayNode = (htmlElement.getAttribute('data-display-selector') != undefined)?$(dataLinks[i]).find(htmlElement.getAttribute('data-display-selector'))[0]:dataLinks[i];
				if(displayNode == undefined) displayNode = dataLinks[i];
				if (template){
					var cloneNode = template.cloneNode(true);
					cloneNode.removeAttribute('data-template');
					cloneNode.setAttribute('class', cloneNode.getAttribute('class') + ' ' + linkedObj.getAttribute('class'));
					if ($(cloneNode).find('[data-input]').length > 0){
						$(cloneNode).find('[data-input]').html(displayNode.innerHTML);
					}else{
						cloneNode.innerHTML = displayNode.innerHTML + cloneNode.innerHTML;
					}
				}else{
					var cloneNode = document.createElement('span');
					cloneNode.setAttribute('class', linkedObj.getAttribute('class'));
					cloneNode.innerHTML = displayNode.innerHTML + cloneNode.innerHTML;
				}
				cloneNode.setAttribute('id', linkedObj.getAttribute('id'));
				cloneNode.setAttribute('data-rid', linkedObj.getAttribute('data-id'));
				if (linkedObj.getAttribute('data-track') == "del"){
					cloneNode.setAttribute('data-track', "deleted");
				}
				htmlElement.appendChild(cloneNode);
			}

			$(htmlElement).parent('.btn').removeClass('disabled');
			if ($(htmlElement).children('*:not("[data-template],.collection-header")').length == 0){
				$(htmlElement).parent('.btn').addClass('disabled');
			}			
		},
		/**
		 *  Function to get and display linked float objects in citation edit popup
		 */
		getNonLinkedFloatObjects: function(container, component, htmlElement){
			var popper = $(htmlElement).closest('[data-component]');
			//Get the template from html element
			if ($(htmlElement).find('*[data-template]').length > 0){
				var template = $(htmlElement).find('*[data-template]')[0];
			}
			//remove the existing list
			$(htmlElement).children('*:not("[data-template],.collection-header")').remove();
			//Get the float objects
			var newSelector = htmlElement.getAttribute('data-source-selector');
			if (htmlElement.hasAttribute('data-parent-container')){
				newSelector = htmlElement.getAttribute('data-parent-container') + newSelector;
			}
			newSelector = kriya.validateXpath($(htmlElement), this.container, 'data-source-selector', newSelector);
			var dataLinks   = kriya.xpath(newSelector);
			if (dataLinks.length == 0) {
				return false;
			}
			for (var i = 0, dl = dataLinks.length; i < dl; i++) {
				var linkedObj = dataLinks[i];
				//If the html element has template then clone the template and set the value else create an empty span and update it
				if (template){
					//Clone the template from html component 
					var cloneNode = template.cloneNode(true);
					cloneNode.removeAttribute('data-template');
					cloneNode.setAttribute('class', cloneNode.getAttribute('class') + ' ' + linkedObj.getAttribute('class'));
					//Update the object html in the template
					if ($(cloneNode).find('[data-input]').length > 0){
						$(cloneNode).find('[data-input]').html(linkedObj.innerHTML);
					}else{
						cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
					}
				}else{
					var cloneNode = document.createElement('span');
					cloneNode.setAttribute('class', linkedObj.getAttribute('class'));
					cloneNode.innerHTML = linkedObj.innerHTML + cloneNode.innerHTML;
				}
				cloneNode.setAttribute('data-rid', linkedObj.getAttribute('data-id'));
				htmlElement.appendChild(cloneNode);
			}

			var displayBox = popper.find('.selectedCitation');
			//Reset the citation box
			displayBox.find('option.indirectCitation').html('Indirect');
			displayBox.find('option.directCitation').html('Direct');

			//if active element is a citation then remove the selected citation from list
			var ridString = $(kriya.config.activeElements).attr('data-citation-string');
			//$('.activeElement').attr('data-citation-string');
			if(ridString){
				var activeClass = $(kriya.config.activeElements).attr('class').split(/\s+/)[0];
				var rids = ridString.replace(/^\s+|\s+$/g, '').split(/\s+/);
				kriya.config.citeIds = [];
				for (var i = 0; i < rids.length; i++) {
					$('[data-rid="'+rids[i]+'"]').addClass('active selected');
					kriya.config.citeIds.push(rids[i]);
				}

				var clonedActiveNode = $(kriya.config.activeElements).clone(true);
				clonedActiveNode.cleanTrackChanges();

				// Get indirect citation value - Rajesh
				var citeText = kriya.general.getNewCitation(rids, '', '');
				var citeNode = $.parseHTML(citeText);
				displayBox.find('option.indirectCitation').html(citeNode);
				// Get direct citation value - Rajesh
				var citeText = kriya.general.getNewCitation(rids, '', true, false);
				var citeNode = $.parseHTML(citeText);
				displayBox.find('option.directCitation').html(citeNode);
			}
		},
		addLinksToObjects: function(htmlElement, container, component){
			if (typeof(container[0]) == "object") container = container[0];
			//Return if nothing is selected
			if($(htmlElement).find('.kriya-tagsinput .tags').length < 1){
				return;
			}
			//get all reference [xref] tags and add them a class
			var className = htmlElement.getAttribute('data-class');
			//var selector  = htmlElement.getAttribute('data-selector');
			var selector  = kriya.validateXpath($(htmlElement), container);
			var linkClass = htmlElement.getAttribute('data-link-component');
			var nodeValue = htmlElement.getAttribute('data-node-value');

			var idPrefix  = htmlElement.getAttribute('data-id-prefix');
			var dataLinks = kriya.xpath(selector, container);
			/*if(idPrefix){
				if(dataLinks.length > 0){
					var rid = $(dataLinks).siblings('.'+className+'[data-rid*="'+idPrefix+'"]').attr('data-rid');
				}else if($('[id*="'+idPrefix+'"]').length > 0){
					var l = $('[id*="'+idPrefix+'"]').length;
					var rid = idPrefix+(l+1);
				}else{
					var rid = idPrefix+"1";	
				}
			}else{
				var rid = container.getAttribute('id');	
			}*/
			
			var rid = container.getAttribute('data-id');
			
			var dataLinks = $('.'+ className + '[data-rid="'+ rid + '"],.'+ className + '[data-rid^="'+ rid + ',"],.'+ className + '[data-rid*=" '+ rid + ',"],.'+ className + '[data-rid$=" '+ rid + '"]');
			$('[data-validating]').removeAttr('data-validating');
			$(dataLinks).attr('data-validating', 'true');
			//get all authors
			var selector  = htmlElement.getAttribute('data-suggestion');
			var dataLinks = kriya.xpath(selector, container);

			/*var activeElementText = $(dataLinks).closest('.activeElement > *').text().trim();
			if(activeElementText && activeElementText != ""){
				$(htmlElement).find('.kriya-tagsinput').append('<span class="tags">'+activeElementText+'</span>');
			}*/

			$(htmlElement).find('.kriya-tagsinput .tags').each(function(tagIndex){
				var suggestion = $(this).text().trim();
				var fullText = $(this).attr('data-fulltext');
				$(dataLinks).each(function(){
					if ($(this).text().trim() == suggestion || (fullText && fullText.replace(/([\s\u00A0]|\&nbsp;)/i,'') == $(this).text().trim().replace(/([\s\u00A0]|\&nbsp;)/i,''))){
						if ($(this).parent().find('.'+className+'[data-rid*="'+ rid + '"]').length > 0){
							$(this).parent().find('.'+className+'[data-rid*="'+ rid + '"]').removeAttr('data-validating');
						}else{
							//insert new reference
							var newRef = document.createElement('span');
							newRef.setAttribute('class', className);
							newRef.setAttribute('data-rid', rid);
							//Add id to new ref
							newRef.setAttribute('id', uuid.v4());
							if(nodeValue){
								newRef.innerHTML = nodeValue;
							}else if ($('[data-id="'+rid+'"]').find('.label').length > 0){
								newRef.innerHTML = $('[data-id="'+rid+'"]').find('.label').html();
							}else{
								newRef.innerHTML = rid.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[^\d]*/, '');
							}
							$(this).parent().append(newRef);
							$(newRef).attr("data-inserted",true); // adding inserted attr to fix save issue in major-dataset - priya - 417
							kriyaEditor.settings.undoStack.push(newRef);
							
							//Add history to inserted ref
							kriya.general.addToHistory(htmlElement, newRef, container);
							//$(newRef).kriyaTracker('ins');
						}
						if(linkClass && linkClass!="" && $(component).find('[data-class="'+linkClass+'"]').length > 0){
							var linkComp = $(component).find('[data-class="'+linkClass+'"]');
							kriya.general.setHTML(linkComp[0], $(this).parent());
						}
						if($(this).closest('.jrnlAuthorGroup').length > 0){
							kriya.general.alignAuthorLinks($(this).closest('.jrnlAuthorGroup'));
						}
						if($(this).text().trim() == suggestion){
							return false;
						}
					}
				});
			});
			$('.'+className+'[data-validating][data-rid*="'+ rid + '"]').each(function(){
				var xref = $(this);
				var ridArray = xref.attr('data-rid').split(/,\s?/);
				if (ridArray.length > 1 && ridArray.indexOf(rid) >= 0){
					var i = ridArray.indexOf(rid)
					ridArray.splice(i, 1);
					xref.attr('data-rid', ridArray.join(', '));
					xref.removeAttr('data-validating');
				}
			});
			//$('.'+className+'[data-validating][data-rid="'+ rid + '"]').kriyaTracker('del');
			//remove all unlinked xref tags - jai 31-10-2017
			$('.'+className+'[data-validating][data-rid*="'+ rid + '"]').each(function(){
				kriyaEditor.settings.undoStack.push($(this).parent()[0]);
				//Add history to deleted ref
				kriya.general.addToHistory(htmlElement, this, container);
				$(this).remove();
			});
			$('.'+className+'[data-validating]').removeAttr('data-validating');
			if($(htmlElement).attr('data-reorder-link') == "true" && $(dataLinks).length > 0){
				kriya.general.reorderLinks(htmlElement.getAttribute('data-class'), component.getAttribute('data-class'), $(dataLinks)[0].parentNode);
			}
		},
		/**
		 * Function to add the equal contrib to the author and add footnote
		 */
		addEqualContrib: function(htmlElement, container, component){
			if (typeof(container[0]) == "object") container = container[0];
			
			var className = htmlElement.getAttribute('data-class');
			var fnType = htmlElement.getAttribute('data-fn-type');

			var selector  = htmlElement.getAttribute('data-suggestion');
			var dataLinks = kriya.xpath(selector, container);

			var idPrefix  = htmlElement.getAttribute('data-id-prefix');
			idPrefix = (idPrefix)?idPrefix:'';
			//get the existing contrib group based on rid
			var equalRefs = $(kriya.config.containerElm + ' .'+className + '[data-ref-type="' + fnType + '"]');
			var existRids = [];
			equalRefs.each(function(){
				var exRid = $(this).attr('data-rid');
				if(exRid && existRids.indexOf(exRid) < 0){
					existRids.push(exRid);
				}
			});
			
			//Check if the selected author has equal contrib then get that author rid
			var rid = idPrefix+(existRids.length + 1);
			$(htmlElement).find('.kriya-tagsinput .tags').each(function(tagIndex){
				var suggestion = $(this).text().trim();
				$(dataLinks).each(function(){
					if ($(this).text().trim() == suggestion){
						if ($(this).parent().find('.'+className+'[data-rid][data-ref-type="' + fnType + '"]').length > 0){
							rid = $(this).parent().find('.'+className+'[data-rid]').attr('data-rid');
						}
					}
				});
			});

			//Check if the current author has equal cotrib then get that rid
			if($(container).find('.'+className+'[data-rid][data-ref-type="' + fnType + '"]').length > 0){
				rid = $(container).find('.'+className+'[data-rid][data-ref-type="' + fnType + '"]').attr('data-rid');
			}
			
			var valueObj = kriya.config.content[fnType];
			valueObj = (valueObj && valueObj[rid])?valueObj[rid]:valueObj;			
			var nodeValue = (valueObj)?valueObj.symbol:'';

			nodeValue = (nodeValue)?nodeValue:rid.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[^\d]*/, '');

			var newRef = document.createElement('span');
			newRef.setAttribute('class', className);
			newRef.setAttribute('data-rid', rid);
			newRef.setAttribute('data-ref-type', fnType);
			newRef.innerHTML = nodeValue;
			
			var contributedAuthors = [];
			var availableLinks = $(kriya.config.containerElm+' .'+className+'[data-rid*="'+ rid + '"][data-ref-type="' + fnType + '"]');
			availableLinks.attr('data-validating', 'true');
			$(htmlElement).find('.kriya-tagsinput .tags').each(function(tagIndex){
				var suggestion = $(this).text().trim();
				$(dataLinks).each(function(){
					var thisEqRef = $(this).parent().find('.'+className+'[data-rid*="'+ rid + '"][data-ref-type="' + fnType + '"]');
					if ($(this).text().trim() == suggestion){
						contributedAuthors.push($(this))
						if (thisEqRef.length == 0){
							//insert new reference
							var equalRef = $(newRef).clone(true);
							$(this).parent().append(equalRef);
							equalRef.attr('data-inserted', 'true');
							kriyaEditor.settings.undoStack.push(equalRef);

							if($(this).closest('.jrnlAuthorGroup').length > 0){
								kriya.general.alignAuthorLinks($(this).closest('.jrnlAuthorGroup'));
							}
						}else{
							thisEqRef.removeAttr('data-validating');
						}
					}
				});
			});

			if(($(htmlElement).find('.kriya-tagsinput .tags').length > 0 ) || ($(htmlElement).attr('data-note') && $(htmlElement).html() != "")){
				contributedAuthors.push($(container).find('.jrnlAuthor'));
				var eqRef = $(container).find('.'+className+'[data-rid*="'+ rid + '"][data-ref-type="' + fnType + '"]');
				if (eqRef.length == 0){
					eqRef = $(newRef).clone(true);
					eqRef.attr('id', uuid.v4());

					$(container).append(eqRef);
				}else{
					eqRef.removeAttr('data-validating');
				}
				kriya.general.addToHistory(htmlElement, eqRef, container);

				if($(htmlElement).attr('data-note')){
					var noteHTML = $(htmlElement).html();
				}else{
					var noteHTML = (valueObj)?valueObj.note:'';
				}
				var noteSelector = kriya.config.containerElm + ' .jrnlEqContrib[data-fn-type="' + fnType + '"]';
				if ($(noteSelector + '[data-id="' + rid + '"]').length > 0){
					note = $(noteSelector + '[data-id="' + rid + '"]');
				}else{
					//assign a pre-defined id
					var selector = $(htmlElement).attr('data-alt-tmp-selector');
					if($('.front .jrnlEqContrib').length > 0 && selector){
						var eqlen = $('.front .jrnlEqContrib').length;
						var eqpathlen = $("[tmp-class='jrnlEqContrib'][tmp-data-fn-type='equal']").length;
						eqlen = eqlen + 1;
						if((eqlen) <= eqpathlen){
							$(htmlElement).attr('data-tmp-selector', selector + '['  + eqlen +']');
						}
						else{
							$(htmlElement).attr('data-tmp-selector', selector + '[last()]');
						}
					}
					else if(selector){
						$(htmlElement).attr('data-tmp-selector', selector);
					}
					var note = kriya.general.convertTemplate('jrnlEqContrib', $(htmlElement));
					
					//change the fn-type
					note.attr('data-fn-type', fnType);
					
					if(note){
						//note.find('sup').html(nodeValue);
						//note.append(noteHTML);
						note.attr('data-inserted', 'true');
						note.attr('data-id', rid);	
					}
				}
				//to order the elements according to its dom position
				function documentPositionComparator (a, b) {
					if (a[0] === b[0]) {
						return 0;
					}
					var position = a[0].compareDocumentPosition(b[0]);
					if (position & Node.DOCUMENT_POSITION_FOLLOWING || position & Node.DOCUMENT_POSITION_CONTAINED_BY) {
						return -1;
					}else if (position & Node.DOCUMENT_POSITION_PRECEDING || position & Node.DOCUMENT_POSITION_CONTAINS) {
						return 1;
					}else{
						return 0;
					}
				}
				contributedAuthors.sort(documentPositionComparator)
				//add authors to the footnote text
				if (note.find('.contributedAuthors').length > 0 && contributedAuthors.length > 0){
					var contribHTML = "";
					$.each(contributedAuthors, function(i,v){
						var surNameText = $(this).find('.jrnlSurName').text();
						var givenNameText = $(this).find('.jrnlGivenName').text();
						if ($('[tmp-class="jrnlEqContrib"]').find('.contributedAuthors').attr('data-abbrv-name') != undefined){
							var abbrvNames = $('[tmp-class="jrnlEqContrib"]').find('.contributedAuthors').attr('data-abbrv-name').split(',');
							for (var g = 0, gl = abbrvNames.length; g < gl; g++){
								if (abbrvNames[g] == 'gn'){
									givenNameText = givenNameText.replace(/(([aA-zZ])[\.\-\']?[aA-zZ]*)/g, '$2').replace(/\s/g, '');
								}else if (abbrvNames[g] == 'sn'){
									surNameText = surNameText.replace(/(([aA-zZ])[\.\-\']?[aA-zZ]*)/g, '$2').replace(/\s/g, '');
								}
							}
						}
						if (i != 0 && contributedAuthors.length == (i + 1)){
							contribHTML += ' and ';
						}else if (i != 0){
							contribHTML += ', ';
						}
						if ($('[tmp-class="jrnlEqContrib"]').find('.contributedAuthors').attr('data-format') == 'abbreviated'){
							contribHTML += givenNameText + surNameText;
						}else if ($('[tmp-class="jrnlEqContrib"]').find('.contributedAuthors').attr('data-format') == 'sngn'){
							contribHTML += surNameText + ' ' + givenNameText;
						}else{
							contribHTML += givenNameText + ' ' + surNameText;
						}
					})
					note.find('.contributedAuthors').html(contribHTML);
				}
				//Updat the cue value
				var cueValue = rid.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\-\s]+/gi, '');
				var tempNode = $('[tmp-class="jrnlEqContrib"][tmp-data-fn-type="' + fnType + '"]');

				if (tempNode.length > 0 && tempNode[0].hasAttribute('tmp-link-array')){
					var linkArray = tempNode.attr('tmp-link-array').split(',');
					if(linkArray.length == 1){
						cueValue = linkArray[0];
					}else if (linkArray.length > 0){
						cueValue = linkArray[cueValue - 1];
					}
				}
				$(note).find('sup,.label').text(cueValue);
				$(kriya.config.containerElm+' .'+className+'[data-rid*="'+ rid + '"][data-ref-type="' + fnType + '"]').html(cueValue);

				/*var note = $('<p class="jrnlEqContrib" fn-type="' + fnType + '" data-id="'+rid+'"><sup>'+nodeValue+'</sup> ' + noteHTML + ' </p>');
				if($(kriya.config.containerElm + ' .jrnlEqContrib').length == 0){
					note.insertAfter(kriya.config.containerElm + ' .jrnlCorrAff:last');
				}else if($(kriya.config.containerElm + ' .jrnlEqContrib[fn-type="' + fnType + '"]').length == 0){
					note.insertAfter(kriya.config.containerElm + ' .jrnlEqContrib:last');
				}
				note.attr('data-inserted', 'true');
				note.attr('id', uuid.v4());*/
				kriyaEditor.settings.undoStack.push(note);
			}else if($(kriya.config.containerElm + ' .jrnlEqContrib[data-fn-type="' + fnType + '"][data-id="' + rid + '"]').length > 0){
				//If every links are removed than remove the note
				var removeNote = $(kriya.config.containerElm + ' .jrnlEqContrib[data-fn-type="' + fnType + '"][data-id="' + rid + '"]');
				removeNote.each(function(){
					$(this).attr('data-removed', 'true');
					kriyaEditor.settings.undoStack.push(this);
				});
				removeNote.remove();
			}

			//remove the links if if author not asigned 
			availableLinks.filter('[data-validating]').each(function(){
				var author = $(this).closest('.jrnlAuthorGroup');
				kriyaEditor.settings.undoStack.push(author);
				kriya.general.addToHistory(htmlElement, this, container);
				$(this).remove();
			});
			
		},
		setCorrespondingAuthor: function(htmlElement, container, component){
			var corClass   = htmlElement.getAttribute('data-class');
			var noteClass  = htmlElement.getAttribute('data-note-class');

			//delete the corresponding author link if it doesn't have corresponding affiliation
			$(kriya.config.containerElm+ ' .'+corClass+':not([data-track="del"])').each(function(i){
				var corlink  = $(this).attr('data-rid');
				var corAff = $(kriya.config.containerElm+ ' .'+noteClass+'[data-id="' + corlink + '"][data-track!="del"]');
				if(corAff.length == 0){
					kriyaEditor.settings.undoStack.push($(this).parent());
					$(this).remove();
				}
			});
			//delete the corresponding author affiliation if it doesn't have corresponding link
			$(kriya.config.containerElm+ ' .'+noteClass+':not([data-track="del"])').each(function(i){
				var corID  = $(this).attr('data-id');
				var corlink = $(kriya.config.containerElm+ ' .'+corClass+'[data-rid="' + corID + '"][data-track!="del"]');
				if(corlink.length == 0){
					$(this).attr('data-removed', 'true');
					kriyaEditor.settings.undoStack.push($(this)[0]);
					$(this).remove();
				}
			});			

			if(corClass && $(container).find('.'+ corClass+':not([data-track="del"])').length > 0){
				var corRef = $(container).find('.'+ corClass+':not([data-track="del"])');
				var dataID    = corRef.attr('data-rid');
				var corAff = $(kriya.config.containerElm + ' [data-id="' +dataID+ '"]:not([data-track="del"])');

				if(!$(htmlElement).is(':checked')){
					kriyaEditor.settings.undoStack.push($(corRef).parent());
					$(corRef).remove();
					$(corAff).each(function(){
						$(this).attr('data-removed', 'true');
						kriyaEditor.settings.undoStack.push($(this));
						$(this).remove();
					});
					//$(corRef).kriyaTracker('del');
					//$(corAff).kriyaTracker('del', '', $(corRef).attr('data-cid'));
				}else{
					//Re construct the existing corresponding affiliation
					var corAffTemp = $('#compDivContent [data-element-template="true"] [tmp-class="' + noteClass + '"]').clone(true);
					if(corAffTemp){
						corAffTemp.removeAttr('tmp-insert-after').removeAttr('tmp-insert-before').attr('tmp-insert-at', 'false');
						corAffTemp = kriya.general.convertTemplate(noteClass, '', '', corAffTemp);
						if(corAffTemp){
							$(corAff).find('[class]').each(function(){
								var className = $(this).attr('class');
								if(this.id){
									corAffTemp.find('.' + className).attr('id', this.id);
								}
							});
							$(corAff).html(corAffTemp.html());
						}
					}
				}
			}else if($(htmlElement).is(':checked')){
				
				//Get the new corresponding affiliation id
				var idArr = [];
				var maxNum = 0
				var dataId = '';
				$(kriya.config.containerElm+ ' .'+noteClass+':not([data-track="del"])').each(function(i){
					var corID  = $(this).attr('data-id');
					if(corID.match(/\d+$/)){
						corID = parseInt(corID.match(/\d+$/)[0]);
						idArr.push(corID);
					}
				});

				if(idArr.length > 0){
					var maxNum = Math.max.apply(null, idArr);
				}
				maxNum = maxNum+1;
				dataId = 'cor'+maxNum;

				//create new corresponding foot note and ref
				var corSymbol = htmlElement.getAttribute('data-node-value');
				var corAff = kriya.general.convertTemplate(noteClass, $(component), $(container));
				if(corAff){
					corAff.attr('data-inserted', 'true');
					corAff.attr('data-id', dataId);

					var corRef = kriya.general.convertTemplate(corClass);
					//var dataID = $(corAff).attr('data-id');
					corRef.attr('data-rid', dataId);
					corRef.attr('data-inserted', 'true');

					$(container).find('.'+corClass).remove();
					$(container).append(corRef);
					//$(corRef).kriyaTracker('ins');
					//$(corAff).kriyaTracker('ins', '', $(corRef).attr('data-cid'));
				}
			}else if (container[0].hasAttribute('data-inserted') && !$(htmlElement).is(':checked')){
				$(container).find('.'+ corClass).remove();
			}

			//Move the deleted affiliations at the end
			$(kriya.config.containerElm+ ' .'+noteClass+'[data-track="del"]').each(function(){
				$(this).insertAfter(kriya.config.containerElm+ ' .'+noteClass+':last');
				$(this).attr('data-moved', 'true');
				kriyaEditor.settings.undoStack.push(this);
			});

			//Update the corr aff values
			$(corAff).find('[class]').each(function(){
				var corSubClass = $(this).attr('class');
				if ($(component).find('[data-class="' + corSubClass + '"]').length > 0){
					var nodeValue = $(component).find('[data-class="' + corSubClass + '"]').html();
					if (nodeValue != undefined){
						$(this).html(nodeValue);
						//remove query if present in author names
						$(this).find('.jrnlQueryRef').remove();
					}
				}
				$(this).find('[id]').each(function(){
					var id = $(this).attr('id');
					if($(kriya.config.containerElm + ' #' + id).length > 1){
						$(this).attr('id', uuid.v4());
					}
				});
			});
			// to remove empty in correspondence to info - aravind 12/08/18
			$(corAff).find('span').each(function(){
				if($(this).is(':empty')){
					$(this).remove();
				}
			});
			//If corresponding address is null then remove that node and punctuation - jagan
			if($(corAff).find('.jrnlCorAddress').length > 0 && $(corAff).find('.jrnlCorAddress').text() == ""){
				var prefixText = $('#compDivContent [data-element-template="true"] [tmp-class="jrnlCorrAff"] [tmp-class="jrnlCorAddress"]').attr('tmp-node-prefix');
				var suffixText = $('#compDivContent [data-element-template="true"] [tmp-class="jrnlCorrAff"] [tmp-class="jrnlCorAddress"]').attr('tmp-node-suffix');
				if(prefixText && $(corAff).find('.jrnlCorAddress')[0].previousSibling && $(corAff).find('.jrnlCorAddress')[0].previousSibling.nodeType == 3){
					var prevNodeVal = $(corAff).find('.jrnlCorAddress')[0].previousSibling.nodeValue;
					prevNodeVal = prevNodeVal.replace(prefixText, '');
					$(corAff).find('.jrnlCorAddress')[0].previousSibling.nodeValue = prevNodeVal;
				}
				
				if(suffixText && $(corAff).find('.jrnlCorAddress')[0].nextSibling && $(corAff).find('.jrnlCorAddress')[0].nextSibling.nodeType == 3){
					var nextNodeVal = $(corAff).find('.jrnlCorAddress')[0].nextSibling.nodeValue;
					nextNodeVal = nextNodeVal.replace(nextNodeVal, '');
					$(corAff).find('.jrnlCorAddress')[0].nextSibling.nodeValue = nextNodeVal;
				}
				$(corAff).find('.jrnlCorAddress').remove();
			}
			
			if(corRef && $(corRef).length > 0){
				kriya.general.addToHistory(htmlElement, corRef, container);
			}
			

			kriyaEditor.settings.undoStack.push(corAff);
			if(corRef && corRef.attr("id") && $('#'+corRef.attr("id")).length>0){
				kriyaEditor.settings.undoStack.push(corRef);
			}
			kriya.general.alignAuthorLinks($(container));		
		},
		/**
		 * Function to save the dropddown values like subject category - jagan
		 */
		saveDropdown:function(htmlElement, container, component){
			if($(htmlElement).attr('data-node-xpath')){
				var nodeXpath = $(htmlElement).attr('data-node-xpath');
				var dataNode = kriya.xpath(nodeXpath);
				if($(dataNode).length > 0){
					var nodevalue = $(htmlElement).html();
					$(dataNode).html(nodevalue);
					
					//Delete node
					if($(dataNode).find('.deletedNode').length > 0){
						$(dataNode).attr('data-template-button', 'true');
						$(dataNode).attr('data-removed', 'true');
						kriya.general.addToHistory(htmlElement, dataNode, container);
						$(dataNode).removeAttr('data-edited-node');
						kriyaEditor.settings.undoStack.push($(dataNode).clone(true));
						$(dataNode).removeAttr('id');
					}else if($(dataNode).attr('data-template-button') == "true"){
						//Insert node
						$(dataNode).removeAttr('data-template-button');
						$(dataNode).attr('id', uuid.v4());
						$(dataNode).attr('data-inserted', 'true');
						kriyaEditor.settings.undoStack.push(dataNode);
						kriya.general.addToHistory(htmlElement, dataNode, container);
					}else{
						//Edit node
						kriyaEditor.settings.undoStack.push(dataNode);
						kriya.general.addToHistory(htmlElement, dataNode, container);
					}
				}
			}
		},
		setFootnote: function(htmlElement, container, component){
			var conType = $(htmlElement).attr('id').replace('_1', '');
			var className = $(htmlElement).attr('data-class');
			var sourceName = $(htmlElement).attr('data-source');
			var conRef = $(container).find('.' + className + '[data-con-type="' + conType + '"]');
			var conId  = conRef.attr('data-rid');
			var conAff = $(kriya.config.containerElm + ' [data-id="' + conId + '"]');
			var conLabel = $(htmlElement).next('label').text();
			if ($(htmlElement)[0].hasAttribute('data-check-field')){
				var checkField = $(htmlElement).attr('data-check-field');
				var checkFieldEle = $(htmlElement).closest('[data-type="popUp"]').find('[data-class="'+checkField+'"]');
				if (checkFieldEle.length > 0) {
					conLabel = $(checkFieldEle).text();
				}
			}
			var addFootnote = false;
			if($(htmlElement).is(':checked')){
				addFootnote = true;
			}else if ($(htmlElement)[0].hasAttribute('data-type') && $(htmlElement).attr('data-type') == "htmlComponent"){
				if ($(htmlElement).is(":visible")){
					if ($(htmlElement).text().replace(/[\s\.\,\u00A0]/g, '') != ""){
						if ($(htmlElement).find('.jrnlFNPara').length > 0){
							conLabel = $(htmlElement).find('.jrnlFNPara').html();
						}else{
							conLabel = $(htmlElement).html();
						}
						addFootnote = true;
					}
				}else{
					addFootnote = false;
				}
			}
			if(addFootnote){
				if(conRef.length == 0){
					conRef = document.createElement('span');
					$(conRef).addClass(className);
					conRef.setAttribute('data-con-type', conType);
					conRef.setAttribute('id', uuid.v4());
					$(container).append(conRef);

				}
				if(conAff.length == 0 && conType != "other"){
					conAff = $(kriya.config.containerElm).find('*[class="'+sourceName+'"][data-con-type="'+ conType +'"]');
				}
				if(conAff.length == 0 || conAff.length > 1){
					var conAff = kriya.general.convertTemplate(sourceName, $(component), $(container));
					$(conAff).attr('data-inserted', 'true');
					if (sourceName == "jrnlConFN"){
						$(conAff).attr('data-con-type', conType);
					}
				}
				conAff.find('.jrnlFNPara').html(conLabel);
				kriyaEditor.settings.undoStack.push(conAff);
				if($(conAff).attr('data-id')){
					conId = $(conAff).attr('data-id');
				}
				if(conId){
					$(conRef).attr('data-rid', conId);	
				}
			}else{
				$(conRef).remove();
				if ($('.' + className + '[data-con-type="' + conType + '"]').length == 0){
					$(conAff).attr('data-removed', 'true');
					kriyaEditor.settings.undoStack.push(conAff);
					$(conAff).remove();
				}
			}
			kriya.general.addToHistory(htmlElement, $(conRef), container);
		},
		reorderCorrespondense: function(htmlElement, container, component){
			$(htmlElement).find('li:not("[data-template]")').each(function(i){
				var authorId = $(this).attr('id');
				var authorNode = $(kriya.config.containerElm + ' #'+authorId);
				if (authorNode.length > 0){
					var corLinkID = authorNode.closest('.jrnlAuthorGroup').find('.jrnlCorrRef[data-track!=del]').attr('data-rid');
					authorNode.closest('.jrnlAuthorGroup').attr('data-correspondence-order', (i+1));
					var corAff = $(kriya.config.containerElm + ' .jrnlCorrAff[data-id="' +corLinkID+ '"]');
					if(corAff.length > 0){
						corAff.insertAfter(kriya.config.containerElm + ' .jrnlCorrAff:not([data-track="del"]):last');
						corAff.attr('data-moved', 'true');
						kriyaEditor.settings.undoStack.push(corAff);
					}
				}
			});
		},
		deleteLinksfromObjects: function(htmlElement, container, component){
			if (typeof(container[0]) == "object") container = container[0];
			
			var objClass = container.getAttribute('class');
			if(objClass){
				objClass = objClass.split(' ')[0];
				var rid = container.getAttribute('data-id');
				var dataLinks = $('[data-rid*="'+ rid + '"]');
				var linkClass = $(dataLinks).attr('class');

				if(linkClass){
					linkClass = linkClass.split(' ')[0];
					$('[data-validating]').removeAttr('data-validating');
					$(dataLinks).attr('data-validating', 'true');
					
					var activeElementText = $(dataLinks).closest('.activeElement > *').text().trim();
					if(activeElementText && activeElementText != ""){
						$(htmlElement).find('.kriya-tagsinput').append('<span class="tags">'+activeElementText+'</span>');
					}

					$('.' + linkClass + '[data-validating][data-rid="'+ rid + '"]').kriyaTracker('del');
					//Remove deleted present address and save the content - issue #80
					$('.' + linkClass + '[data-validating][data-rid*="'+ rid + '"]').parent().each(function(){
						$('.' + linkClass + '[data-validating][data-rid="'+ rid + '"]').remove();
						kriyaEditor.settings.undoStack.push($(this)[0]);
					});
					//#267 - editor aff untagging while deleting aff - priya
					$('.' + linkClass + '[data-validating][data-rid="'+ rid + '"],'+'.' + linkClass + '[data-validating][data-rid^="'+ rid + '"],'+'.' + linkClass + '[data-validating][data-rid*=" '+ rid + '"],'+'.' + linkClass + '[data-validating][data-rid$=" '+ rid + '"]').each(function(){
						var oldRid = $(this).attr('data-rid').replace(rid, '').trim();
						oldRid = oldRid.replace(/^[\,\s]+|[\,\s]+$/, '');
						$(this).attr('data-rid', oldRid);
					});
					$('.' + linkClass + '[data-validating]').removeAttr('data-validating');
					kriya.general.reorderLinks(linkClass, objClass, $(container).closest('.jrnlAuthorsGroup,.jrnlGroupAuthorsGroup,.jrnlEditorsGroup,.jrnlReviewersGroup').find('.jrnlAuthorGroup,.jrnlGroupAuthor,.jrnlEditorGroup,.jrnlReviewerGroup')[0]);
					kriya.general.alignAuthorLinks($(container));
				}
			}
		},
		addComment: function(htmlElement, container, component){
			var popper = $(htmlElement).closest('[data-component]');
			var commentText = htmlElement.innerHTML;
			var nodeXpath = popper.find('[data-type="floatComponent"]').attr('data-node-xpath');
			if(commentText && kriya.config.content.role == "author" && nodeXpath){
				var dataNode = kriya.xpath(nodeXpath);
				if($(dataNode).closest('[data-id^="BLK_"]').length > 0){
					dataNode = $(dataNode).closest('[data-id^="BLK_"]');
					var label = $(dataNode).find('.label:first').text();
					label = label.replace(/([\.\,\:\s]+)$/g, '');
					commentText = label + ' was replaced. <br/> <b>Reason: </b>' + commentText;
				}
				var dataId = $(dataNode).attr('id');
				
				var qid = eventHandler.query.action.createQueryDiv('Publisher', 'info');
				$('#' + qid).find('.query-from').html(kriyaEditor.user.name);
				var queryContent = $('#' + qid).find('.query-content');
				queryContent.attr('data-assigned-by', kriyaEditor.user.name);
				$(queryContent).html(commentText);
				$('#' + qid).attr('data-type', 'float-comment');
				$('#' + qid).attr('data-float-id', dataId);
				kriyaEditor.settings.undoStack.push($('.query-div#' + qid)[0]);
			}
		},
		saveLinkedObjects: function(htmlElement, container, component){
			if (typeof(container[0]) == "object") container = container[0];
			if ($(container).hasClass('templateWrap')) container = $(container).find('.jrnlAuthorGroup')[0];
 			var selector = htmlElement.getAttribute('data-selector');
			var dataLinks = kriya.xpath(selector, container);
			
			var dataLinksRid = "";
			$(dataLinks).each(function(){
				var citeArray = $(this).attr('data-rid').split(',');
				for (var e= 0, cl = citeArray.length; e < cl; e++){
					dataLinksRid += citeArray[e].replace(/^ | $/g, '');
				}
			});
			var htmlElementID = "";
			$(htmlElement).find('li:not("[data-template]")').each(function(){
				//remove empty
				if ($(this).find('span[contenteditable][data-input]').length > 0 && $(this).find('span[contenteditable][data-input]').text().trim() == ""){
					$(this).remove();
				}else{
					htmlElementID += $(this).attr('data-rid');
				}
			});
			
			var idPrefix = htmlElement.getAttribute('data-id-prefix');
			idPrefix = (idPrefix) ? idPrefix : 'ele';
			if (htmlElement.hasAttribute('data-id-parent-prefix')){
				idPrefix = htmlElement.getAttribute('data-id-parent-prefix') + '-' + idPrefix;
			}

			var nodeValue = htmlElement.getAttribute('data-node-value');

			var sourceSelector = htmlElement.getAttribute('data-source-selector');
			if (htmlElement.hasAttribute('data-parent-container')){
				sourceSelector = htmlElement.getAttribute('data-parent-container') + sourceSelector;
			}
			var sourceElements = kriya.xpath(sourceSelector, container);
			var sourceClass = htmlElement.getAttribute('data-source');
			//var sourceClass = $(sourceElements).attr('class');
			
			$(dataLinks).each(function(){
				var citeArray = $(this).attr('data-rid').split(',');
				for (var e = 0, cl = citeArray.length; e < cl; e++){
					var rid = citeArray[e].replace(/^ | $/g, '');
					var nodeName = $(this).prop("tagName");
					var newRef = document.createElement(nodeName);
					newRef.setAttribute('class', htmlElement.getAttribute('data-class'));
					newRef.setAttribute('id', $(this).attr('id'));
					newRef.setAttribute('data-rid', rid);
					//newRef.innerHTML = rid.replace(/[a-z]+/, '');
					newRef.innerHTML = (nodeValue)?nodeValue:rid.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z]+/, '');
					if ($('[data-id='+rid+']').find('.label').length > 0){
						newRef.innerHTML = $('[data-id='+rid+']').find('.label').html();
					}
					$(this).before(newRef);
				}
				$(this).remove();
			});

			var dataLinks = kriya.xpath(selector, container);
			$(container).find('[data-validating]').removeAttr('data-validating');
			$(dataLinks).attr('data-validating', 'true');
			//if ($(dataLinks).length == 0 && $(htmlElement).find('li:not("[data-template]")').length == 0) return false;
			$(htmlElement).find('li:not("[data-template]")').each(function(i,v){
				var rid = $(this).attr('data-rid');
				var floatHtml = $(this).find('[data-input="true"]').html();
				// check if the object is already present in article
				if($(kriya.config.containerElm + ' [data-id="' + rid + '"]').length > 0){
					var currSelected = $(kriya.config.containerElm + ' [data-id="' + rid + '"]');
					currSelected.html(floatHtml);
					if (currSelected.attr('data-track') == 'del'){
						// Get parent data id for group author block and add with idPrefix - rajesh
						if($(currSelected).closest('.jrnlGroupAuthorsGroup').length > 0){
							var dataId = $(currSelected).closest('.jrnlGroupAuthorsGroup').attr('data-id');
							if(dataId){
								idPrefix = dataId + '_' + idPrefix;
							}
						}
						currSelected.removeAttr('data-track').removeAttr('data-cid').removeAttr('data-username').removeAttr('data-userid').removeAttr('data-time').removeAttr('data-callback');
						var randomID = new Date().getTime() + Math.floor(Math.random() * 7) + i;
						rid = idPrefix + randomID;
						do {
							rid = idPrefix + randomID++;
						} while ($('[data-id="' + rid + '"]').length > 0);
						currSelected.attr('data-id', rid);
					}
				}else{
				//else add as a new object and add it to saveObject
					if (!kriya.config.activeElements){
						kriya.config.activeElements = container;
					}
					var randomID = $(kriya.xpath(sourceSelector, container)).length + 1;
					rid = (rid) ? rid : idPrefix + randomID;
					do {
						rid = idPrefix + randomID++;
					} while ($('[data-id="' + rid + '"]').length > 0);
					
					if ($('[data-type="popUp"]:visible').length > 1){
						var newElement = kriya.general.convertTemplate(sourceClass, $(this), component);
					}else{
						var newElement = kriya.general.convertTemplate(sourceClass, $(this));
					}
					if(!newElement){
						return false;
					}
					if (htmlElement.hasAttribute('data-id-parent-prefix')){
						$(newElement).attr('data-id', rid);
					}else if ($(newElement)[0].hasAttribute('data-id') && $(newElement).attr('data-id') != rid){
						rid = $(newElement).attr('data-id');
					}
					var parentClass = newElement.parent().attr('class');
					$(newElement).find('*').each(function(){
						if($(this).html() == ''){
							var currentNode = $(this).attr('class');
							var nextNode = $(this).next().attr('class');
							//var parentClass = $(this).parent().attr('class');
							var className = $(this).find(' > *').attr('class');
							var punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+nextNode+'"]').attr('tmp-node-prefix');
							if (typeof(punc)){
								punc = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+parentClass+'"]').find('*[tmp-class="'+nextNode+'"]').attr('tmp-node-suffix');
							}
							if (typeof(punc)){
								punc = ", ";
							}
							if($(this)[0].nextSibling && $(this)[0].nextSibling.nodeValue == punc){
								$(this)[0].nextSibling.remove();
								$(this).remove();
							}else if((!$(this)[0].nextSibling || ($(this)[0].nextSibling && nextNode == "removeNode")) && $(this)[0].previousSibling && $(this)[0].previousSibling.nodeValue == punc){
								$(this)[0].previousSibling.remove();
								$(this).remove();
							}
						}
					});
					//newElement = newElement.contents().unwrap();
					//$(newElement).html(floatHtml);
					//newElement.attr('data-id', newElement.attr('id'));
					newElement.attr('data-inserted', 'true');
					kriyaEditor.settings.undoStack.push(newElement);
					//newElement.kriyaTracker('ins');
				}

				if ($(dataLinks).filter('[data-rid="'+rid+'"]').length == 1){
					$(dataLinks).filter('[data-rid="'+rid+'"]').removeAttr('data-validating');
					$(container).append($(dataLinks).filter('[data-rid="'+rid+'"]'));
				}else{
					var nodeName = ($(dataLinks).prop("tagName") != undefined)?$(dataLinks).prop("tagName"):'span';
					var newRef = document.createElement(nodeName);
					newRef.setAttribute('class', htmlElement.getAttribute('data-class'));
					newRef.setAttribute('id', uuid.v4());
					newRef.setAttribute('data-rid', rid);
					newRef.setAttribute('data-inserted', 'true');
					newRef.innerHTML = (nodeValue)?nodeValue:rid.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z]+/, '');
					if ($('[data-id='+rid+']').find('.label').length > 0){
						newRef.innerHTML = $('[data-id='+rid+']').find('.label').html();
					}
					//$(newRef).kriyaTracker('ins');
					if (dataLinks.length > 0){
						var dummyDataLinks = kriya.xpath(selector, container);
						$(dummyDataLinks).last().after(newRef);
					}else{
						$(container).append(newRef);
					}
				}
			});
			
			//remove the affiliated ref instead adding it as track-changes for following a consistency (as in one place we remove and some place we add as track changes) - Jai 23-10-2017
			$(container).find('[data-validating]').remove()
			//$(container).find('[data-validating]').kriyaTracker('del').removeAttr('id');
			if($(htmlElement).attr('data-reorder-link') == "true" && $('[data-type="popUp"]:visible').length <= 1 ){
				kriya.general.reorderLinks(htmlElement.getAttribute('data-class'), htmlElement.getAttribute('data-source'), container);
			}
			
			if($(dataLinks).length == 0){
				dataLinks = kriya.xpath(selector, container);
			}
			kriya.general.addToHistory(htmlElement, $(dataLinks), container);
			
			kriya.general.alignAuthorLinks($(container));
		},
		modifyOrder: function(htmlElement, container, component){
			var selector = htmlElement.getAttribute('data-source-selector');
			var dataLinks = kriya.xpath(selector, container);
			if ($(dataLinks).length == 0 && $(htmlElement).find('li:not("[data-template]")').length == 0) return false;
			$(htmlElement).find('li:not("[data-template]")').each(function(i){
				var rid = $(this).attr('id');
				var nextId = $(this).next('li:not("[data-template]")').attr('id');
				var prevId = $(this).prev('li:not("[data-template]")').attr('id');
				var dataNode = $(dataLinks).filter('[id="'+rid+'"]');
				if (dataNode.length == 1){
					if($(dataLinks).filter('[id="' + prevId + '"]').length > 0){
						dataNode.insertAfter($(dataLinks).filter('[id="' + prevId + '"]'));
					}else if($(dataLinks).filter('[id="' + nextId + '"]').length > 0){
						dataNode.insertBefore($(dataLinks).filter('[id="' + nextId + '"]'));
					}
				}
				dataNode.attr('data-moved', 'true');
				kriyaEditor.settings.undoStack.push(dataNode);
			});
		},
		reorderAuthors: function(htmlElement, container, component){
			var className;
			//re-order affiliations by iteration author-by-author
			var newAffRef = 1;
			var existingAffArray = [];
			var selector = htmlElement.getAttribute('data-selector');
			var dataLinks = kriya.xpath(selector, container);
			if ($(dataLinks).length == 0 && $(htmlElement).find('li:not("[data-template]")').length == 0) return false;
			$(dataLinks).removeAttr('data-order');
			$($(htmlElement).find('li:not("[data-template]")').get().reverse()).each(function(i){
				var rid = $(this).attr('id');
				var authorNode = $(dataLinks).filter('[id="'+rid+'"]');
				if (authorNode.length == 1){
					authorNode.removeAttr('data-validating');
					$(container).prepend(authorNode);
				}
				if(i == 0){
					authorNode.attr('data-order', 'last');
				}
				authorNode.attr('data-moved', 'true');
				kriyaEditor.settings.undoStack.push(authorNode);
			});

			eventHandler.components.general.reorderAff($(dataLinks[0]));

		},
		reorderLinks: function(refType, elementClass, container){
			/**
			* get all author's ref elements, remove duplicates, get it in a range and add it to that author
			**/
			var parentClass = $('#compDivContent *[data-element-template="true"]').find('*[tmp-class="'+elementClass+'"]').parent().attr('tmp-class');
			parentClass = (parentClass != undefined)?'.'+parentClass:'';
			if (typeof container == "undefined") container = kriya.config.activeElements[0];
			var containerClass = container.getAttribute('class').split(' ')[0]
			var containerGroup = $(container).closest('*[data-group]');
			if (containerGroup.length == 0){
				containerGroup = $(kriya.config.containerElm);
			}

			$.each($(containerGroup).find('.' + containerClass), function(){
				//if($(this).has('*[class="del"]').length > 0 && $(this).text() == $(this).find('> .del').text()){
				if($(this).closest('.del, [data-track="del"]').length > 0){
					kriyaEditor.settings.undoStack.push($(this)[0]);
					return true;
				}
				var thisAuthor = $(this);
				var arrayIds = [];
				
				$.each(thisAuthor.find('*[class="'+ refType + '"]'), function(){
					//if($(this).has('*[class="del"]').length > 0 && $(this).text() == $(this).find('> .del').text()){
					if($(this).closest('.del, [data-track="del"]').length > 0){
						//kriyaEditor.settings.undoStack.push($(this)[0]);
						return true;
					}
					oldAffRef = $(this).attr('data-rid');
					oldAffRef = oldAffRef.split(', ');
					oldAffRef.forEach(function(entry) {
						var affNode = $(containerGroup).find(parentClass+' .' + elementClass + '[data-id="' + entry + '"]:not([data-track="del"])');
						if (affNode.length == 0 && elementClass != 'jrnlAff'){
							affNode = $(parentClass+' .' + elementClass + '[data-id="' + entry + '"]');
						}
						if (affNode.length > 0){
							if (affNode[0].hasAttribute('deleted') || (affNode[0].hasAttribute('data-track') && affNode.attr('data-track') == "del")){
								//do nothing as it is deleted
							}else{
								arrayIds.push(entry);
							}
						}
					});
					//$(this).remove();
				});
				if (arrayIds.length > 0){
					var uniqueIDs = [];
					$.each(arrayIds, function(i, el){
						if($.inArray(el, uniqueIDs) === -1) uniqueIDs.push(el);
					});
					//uniqueIDs.sort();
					var ids = arrayIds.join(', ');
					ids = ids.replace(/^, |, $/, '');
					var linkNodes = thisAuthor.find('*[class="'+ refType + '"]:not([data-track="del"])');
					linkNodes.remove();

					if(linkNodes.length > 0){
						var modLinkNode = document.createElement('span');
						$.each(linkNodes[0].attributes, function(i, val){
							modLinkNode.setAttribute(val.name, val.nodeValue);
						});
						modLinkNode.setAttribute('data-rid', ids);
						modLinkNode.innerHTML = ids.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\-]/gi, '');
						thisAuthor.append(modLinkNode);
					}
					//thisAuthor.html(thisAuthor.html() + '<sup id="'+nodeID+'" class="'+refType+'" data-rid="' + ids + '">' + (ids.replace(/[a-z]/gi, '')) + '</sup>');
				}else{
					kriyaEditor.settings.undoStack.push(thisAuthor);
					$.each(thisAuthor.find('*[class="'+ refType + '"]'), function(){
						$(this).remove();
					});
				}
				kriya.general.alignAuthorLinks($(this));
			});
			var className;
			//re-order affiliations by iteration author-by-author
			var newAffRef = 1;
			var existingAffArray = [];
			$(containerGroup).find('.' + containerClass).find('*[class="' + refType + '"]').each(function(){
				if($(this).closest('.del, [data-track="del"]').length > 0) return true;
				var newAffID = "";
				oldAffRef = $(this).attr('data-rid');
				if ((oldAffRef != undefined) || (oldAffRef != "")){
					oldAffRef = oldAffRef.split(', ');
					oldAffRef.forEach(function(entry) {
						if ((!entry) || (entry == "")){
							return true;
						}
						className = entry.replace(/[0-9]+$/, '');
						var affNode = $(containerGroup).find(parentClass+' .' + elementClass + '[data-id="' + entry + '"]:not([data-track="del"])');
						if (affNode.length == 0 && elementClass != 'jrnlAff'){
							affNode = $(parentClass+' .' + elementClass + '[data-id="' + entry + '"]:not([data-track="del"])');
						}
						affNode.each(function(){
							//add sequence order if it is a new aff
							if (existingAffArray.indexOf(entry) == -1){
								if (! $(this)[0].hasAttribute('data-reordered')){
									existingAffArray.push(entry);
									$(this).attr("data-new-id", className + newAffRef);
									newAffID = newAffID + ', ' + className + newAffRef;
									newAffRef++;
									$(this).appendTo($(this).parents('.'+$(this).parent().attr('class')));
									$(this).attr('data-moved','true');
								}else{
									var aff = existingAffArray.indexOf(entry) + 1;
									newAffID = newAffID + ', ' + className + aff;
								}
							}else{
								var aff  = existingAffArray.indexOf(entry) + 1;
								newAffID = newAffID + ', ' + className + aff;
							}
							//if (!$(this).hasClass('reordered')) kriyaEditor.settings.undoStack.push($(this)[0]);
							$(this).attr('data-reordered', 'true');//add class, so tat it is not checked again
						});
						//push to an array
					});
				}
				newAffID = newAffID.replace(/^[,\s]+|[\s,]+$/ ,'');
				newAffID = newAffID.split(', ');
				var prefix = newAffID[0].replace(/[0-9\s\-]+$/gi, '');
				var newAffIDNew = [];
				$.each(newAffID, function( index, value ) {
					var affNumber = value.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\s\-]/gi, '');
					newAffIDNew.push(affNumber);
				});
				newAffID = newAffIDNew;
				newAffID.sort(function(a,b){return a-b});
				var newAffIDNew = [];
				$.each(newAffID, function( index, value ) {
					newAffIDNew.push(prefix + value);
				});
				//newAffID = newAffIDNew;
				$(this).attr('data-rid', newAffIDNew.join(', '));//add aff-ref to the author
				newAffID = newAffID.join(', ');
				var getRangeValue = newAffID.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\s\-]/gi, '');
				getRangeValue = getRangeValue.split(',');
				//getRangeValue = kriya.general.getRanges(getRangeValue);
				$(this).text(getRangeValue);//add aff-ref to the author
				kriyaEditor.settings.undoStack.push($(this).closest('.' + containerClass)[0]);
			});
			//re-order the affiliation which are not referred on authors
			if (elementClass != "jrnlAff"){
				containerGroup = kriya.config.containerElm;
			}
			$(containerGroup).find(parentClass+' .' + elementClass + ':not([data-reordered]):not([data-track="del"])').each(function(){
				if(className == undefined && $(this).attr("data-id")) className = $(this).attr("data-id").replace(/[0-9]+/, '');
				className = (className)?className:'';
				$(this).attr("data-new-id", className + newAffRef);
				newAffRef++;
				$(this).appendTo($(this).parents('.'+$(this).parent().attr('class')));
				//kriyaEditor.settings.undoStack.push($(this)[0]);
			});
			//to assign the deleted elements a sequencial rid, which will avoid same 'data-id' for multiple elements
			$(containerGroup).find(parentClass+' .' + elementClass + '[data-track="del"]:not([data-reordered])').each(function(){
				if(className == undefined && $(this).attr("data-id")) className = $(this).attr("data-id").replace(/[0-9]+/, '');
				className = (className) ? className : '';
				$(this).attr("data-new-id", className + newAffRef);
				newAffRef++;
				$(this).appendTo($(this).parents('.'+$(this).parent().attr('class')));
			});
			//re-order the affiliation which are not referred on authors
			$(containerGroup).find(parentClass + ' .' + elementClass).each(function(i, v){
				var idValue = $(this).attr('data-new-id');
				var oldIdVal = $(this).attr('data-id');
				if(idValue){
					$(this).attr("data-id", idValue);

					//Update the new aff id to role that related to affiliation - jagan
					$(containerGroup).find('.jrnlRole[data-rid="' + oldIdVal + '"]').attr('data-rid', idValue);

					var cueValue = idValue.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\-\s]+/gi, '');
					if ($('[tmp-class="' + elementClass + '"]').length > 0 && $('[tmp-class="' + elementClass + '"]')[0].hasAttribute('tmp-link-array')){
						var linkArray = $('[tmp-class="' + elementClass + '"]').attr('tmp-link-array').split(',');
						if ($('[tmp-class="' + elementClass + '"]')[0].hasAttribute('tmp-linked-object')){
							var linkedObjects = $('[tmp-class="' + elementClass + '"]').attr('tmp-linked-object');
							cueValue = parseInt(cueValue) + $(kriya.config.containerElm).find(linkedObjects).length;
						}
						if (linkArray.length > 0){
							cueValue = linkArray[cueValue - 1];
						}
					}
					//Check if affiliation order number is not available then add element - rajesh 
					if($(this).find('sup,.label').length == 0 && $('[tmp-class="' + elementClass + '"] sup').length > 0){
							$(this).prepend('<sup>' + cueValue + '</sup>');
					}
					$(this).find('sup,.label').text(cueValue);
					//check for uncited, generate a data-id with timestamp so it could be easily cited again and do not clash with newly added one's - jai 25-10-2017
					var randomID = new Date().getTime() + Math.floor(Math.random() * 7) + i;
					var newAffID = $(this).attr('data-id').replace(/[0-9]+$/, '') + randomID;
					do {
						newAffID = $(this).attr('data-id').replace(/[0-9]+$/, '') + randomID++;
					} while ($('[data-id="' + newAffID + '"]').length > 0);
					$(this).removeAttr('data-uncited');
					if ($(this).attr('data-track') == 'del'){
						$(this).attr('data-id', newAffID);
					}else if ($(kriya.config.containerElm).find('*[data-rid="'+idValue+'"],*[data-rid^="'+idValue+', "],*[data-rid*=" '+idValue+', "],*[data-rid$=" '+idValue+'"]').length == 1 && $(kriya.config.containerElm).find('*[data-rid="'+idValue+'"],*[data-rid^="'+idValue+', "],*[data-rid*=" '+idValue+', "],*[data-rid$=" '+idValue+'"]').closest('[data-track="del"]').length > 0){
						$(this).kriyaTracker('del');//retain data-id for deleted elements
						$(this).attr('data-id', newAffID);
						//$(this).attr('data-old-id', $(this).attr('data-id'));
						//$(this).removeAttr('data-id');
					}
					//some times affl not deleting-kirankumar
					else if ($(kriya.config.containerElm).find('*[class$="Group"]:not([data-track="del"]),.jrnlGroupAuthor:not([data-track="del"])').find('*[data-rid="'+idValue+'"],*[data-rid^="'+idValue+', "],*[data-rid*=" '+idValue+', "],*[data-rid$=" '+idValue+'"]').closest('.jrnlEditorGroup:not([data-track="del"]),.jrnlAuthorGroup:not([data-track="del"]),.jrnlReviewerGroup:not([data-track="del"])').length == 0){
						$(this).kriyaTracker('del');
						$(this).attr('data-id', newAffID);
						//$(this).attr('data-old-id', $(this).attr('data-id'));
						//$(this).removeAttr('data-id');
					}
				}
				$(this).removeAttr('data-new-id');
				$(this).attr("data-moved", 'true'); //Added by jagan
				$(this).appendTo($(this).parents('.'+$(this).parent().attr('class')));
				kriyaEditor.settings.undoStack.push($(this)[0]);
			});
			$(containerGroup).find(parentClass + ' .' + elementClass + '[data-track="del"]').each(function(){
				$(this).appendTo($(this).parents('.'+$(this).parent().attr('class')));
			})
			//to customize cues according to customer's style
			if ($('[tmp-class="' + elementClass + '"]').length > 0 && $('[tmp-class="' + elementClass + '"]')[0].hasAttribute('tmp-link-array')){
				var linkArray = $('[tmp-class="' + elementClass + '"]').attr('tmp-link-array').split(',');
				var linkedObject = 0;
				if ($('[tmp-class="' + elementClass + '"]')[0].hasAttribute('tmp-linked-object')){
					var linkedObjects = $('[tmp-class="' + elementClass + '"]').attr('tmp-linked-object');
					linkedObject = $(kriya.config.containerElm).find(linkedObjects).length;
				}
				$(containerGroup).find('.' + containerClass).find('*[class="' + refType + '"]').each(function(){
					if($(this).closest('.del, [data-track="del"]').length > 0) return true;
					var idValue = $(this).attr('data-rid').split(', ');
					if(idValue.length > 0){
						var newValue = [];
						$.each(idValue, function(i,val){
							var cueValue = parseInt(val.replace(/^[a-z-]+[0-9]+_/i, '').replace(/[a-z\-\s]+/gi, ''));
							cueValue = cueValue + linkedObject
							if (linkArray.length > 0){
								cueValue = linkArray[cueValue - 1];
							}
							newValue.push(cueValue);
						});
						$(this).text(newValue.join(', '));
					}
				});
			}
			$(containerGroup).find(parentClass).find('> .btn, .jrnlFundingStatement').each(function(){
				$(this).appendTo($(this).parent());
			})
			$('[data-reordered]').removeAttr('data-reordered');
		},
		/**
		 * Function to align the author links based on the array of selectors that's given in the function
		 * @param authorNode - Author element to reorder
		 * @param linkOrderArray - reorder elements selector
		 */
		alignAuthorLinks: function(authorNode, linkOrderArray){
			var linkOrderSelector = $('#compDivContent [data-component="jrnlAuthorGroup_edit"]').attr('data-link-order');
			linkOrderSelector = (linkOrderSelector)?linkOrderSelector:'.jrnlAffRef,.jrnlCorrRef,.jrnlEqContribRef[data-ref-type="first-authorship"],.jrnlEqContribRef[data-ref-type="equal"],.jrnlEqContribRef[data-ref-type="first-authorship"],.jrnlEqContribRef[data-ref-type="senior-authorship"],.jrnlPresAddRef,.jrnlFNRef,.jrnlFundRef,.jrnlConRef,.jrnlCompIntRef,.jrnlDataRef,.jrnlEmail,.jrnlOrcid,.jrnlGroupAuthorRef,.jrnlOnBehalfOf';
			var linkOrderArray = linkOrderSelector.split(',');
			if(authorNode){
				$.each(linkOrderArray, function(i, val){
					var linkNode = $(authorNode).find(val);
					if(linkNode.length > 0){
						$(authorNode).append(linkNode);
					}
				});
				authorNode.find('[data-class-new="jrnlOnBehalfOf"],[data-class-new="jrnlEmail"]').appendTo(authorNode);
				authorNode.find('.removeNode').appendTo(authorNode);
				var parentNode = $(authorNode).closest('.jrnlAuthors');
				parentNode.find('> .jrnlOnBehalfOf').appendTo(parentNode)
				parentNode.find('> .btn').appendTo(parentNode)
			}
		},
		saveDataSets: function(container){
			var parentContainer = container.parent();
			
			var id = container.attr('data-id');
				if (container.attr('data-track') && container.attr('data-track')=='del'){
					$(kriya.config.containerElm).find('[data-rid="'+id+'"]').each(function(){
						$(this).attr('data-removed','true');
						kriyaEditor.settings.undoStack.push($(this));
						var rid = $(this).attr('data-rid');
						$(this).remove();
					});
				}
			
			if (parentContainer.find('*[data-content-type="generated-dataset"]:not([data-track="del"])').length == 0){
				parentContainer.find('> p:contains("was generated")').remove();
			}
			if (parentContainer.find('> p:contains("was generated")').length == 0 && parentContainer.find('*[data-content-type="generated-dataset"]:not([data-track="del"])').length > 0){
				parentContainer.append('<p>The following dataset was generated:</p>');
				kriyaEditor.settings.undoStack.push(parentContainer.find('> p:contains("was generated")'));
			}
			parentContainer.append(parentContainer.find('> p:contains("was generated")'))
			parentContainer.find('*[data-content-type="generated-dataset"]').each(function(){
				parentContainer.append($(this));
			});
			if (parentContainer.find('*[data-content-type="existing-dataset"]:not([data-track="del"])').length == 0){
				parentContainer.find('> p:contains("previously published")').remove()
			}
			if (parentContainer.find('> p:contains("previously published")').length == 0 && parentContainer.find('*[data-content-type="existing-dataset"]:not([data-track="del"])').length > 0){
				parentContainer.append('<p>The following previously published datasets were used:</p>');
				kriyaEditor.settings.undoStack.push(parentContainer.find('> p:contains("previously generated")'));
			}
			parentContainer.append(parentContainer.find('> p:contains("previously published")'))
			parentContainer.find('*[data-content-type="existing-dataset"]').each(function(){
				parentContainer.append($(this));
			});
			kriyaEditor.settings.undoStack.push(parentContainer);
		},
		saveGroupAuthors: function(htmlElement, container, component){
			if (htmlElement.hasAttribute('data-node-xpath')){
				var selector = htmlElement.getAttribute('data-node-xpath');
				var dataChilds = kriya.xpath(selector, container[0]);
				if (dataChilds.length > 0){
					$(dataChilds[0]).html($(htmlElement).html());
					var rid = $(dataChilds[0]).attr('id');
					if ($(kriya.config.containerElm).find('[data-rid="' + rid + '"]').length > 0){
						$(dataChilds[0]).find('.jrnlGroupAuthorHead').remove();
						var collab = $(kriya.config.containerElm).find('[data-rid="' + rid + '"]').closest('.jrnlAuthorGroup').find('.jrnlCollaboration');
						$(dataChilds[0]).prepend('<h1 data-editable="false" class="jrnlGroupAuthorHead">' + collab.html() + '</h1>');
					}
					kriyaEditor.settings.undoStack.push($(dataChilds[0]));
				}
			}else if ($(htmlElement).find('.jrnlGroupAuthor').length > 0){
				var l = $('.jrnlGroupAuthorsGroup').length + 1
				var r = 'group-author' + l;
				var group = $('<div class="jrnlGroupAuthorsGroup" data-group="group-author"></div>');
				group.attr('data-id', r);
				group.attr('id', uuid.v4());
				$(group).html($(htmlElement).html());
				$(group).append('<p class="jrnlGroupAuthors"/>');
				$(group).find('.jrnlGroupAuthor').each(function(){
					var id = $(this).attr('data-id');
					if (! /_/.test(id)){
						$(this).attr('data-id', r + '_' + id);
					}
					$(group).find('.jrnlGroupAuthors').append($(this));
				})
				$(group).find('.jrnlGroupAuthors').append('<span class="btn btn-small lighten-1 contentBtn" data-channel="components" data-topic="PopUp" data-event="click" data-message="{\'funcToCall\': \'addPopUp\', \'param\' : {\'component\' : \'jrnlGroupAuthor\'}}" contenteditable="false">+ Add Author</span>');
				$(group).append('<div class="jrnlAffGroup"/>');
				$(group).find('.jrnlAff').each(function(){
					var id = $(this).attr('data-id');
					if (! /^group-author/.test(id)){
						$(this).attr('data-id', r + '_' + id);
					}
					$(group).find('.jrnlAffGroup').append($(this));
				});
				$(group).find('.jrnlAffRef').each(function(){
					var rid = $(this).attr('data-rid');
					var newAffID = rid.split(', ');
					var newAffIDNew = [];
					$.each(newAffID, function( index, value ) {
						newAffIDNew.push(r + '_' + value);
					});
					$(this).attr('data-rid', newAffIDNew.join(', '));
				});
				if ($('.jrnlGroupAuthorsGroup').length > 0){
					$('.jrnlGroupAuthorsGroup').last().after(group);
				}else{
					$('.jrnlAuthorsGroup,.jrnlPresAddGroup').last().after(group);
				}
				var xref = $('<span class="jrnlGroupAuthorRef" data-ref-type="other" data-rid="' + r + '">' + l + '</span>');
				$(container).append(xref);
				$(group).prepend('<h1 data-editable="false" class="jrnlGroupAuthorHead">' + container.find('.jrnlCollaboration').html() + '</h1>');
				group.attr('data-inserted', 'true');
				kriyaEditor.settings.undoStack.push(group);
			}
		},
		getRanges: function (array) { //used in reorderaff()
			var ranges = [], rstart, rend;
			array.sort(function(a,b){return a-b});
			for (var i = 0; i < array.length; i++) {
				rstart = array[i];
				rend   = rstart;
				while (array[i + 1] - array[i] == 1) {
					rend = array[i + 1]; // increment the index if the numbers sequential
					i++;
				}
				ranges.push(rstart == rend ? rstart+'' : rstart + '-' + rend);
			}
			function replacer(match){
				rangeVals = match.split('-');
				if ((rangeVals[1] - rangeVals[0]) == 1){
					return rangeVals[0] + "," + rangeVals[1];
				}else{
					return match;
				}
			};
			return ranges.join().replace(/((\d+)\-(\d+))/g, replacer);
		},
		saveDate: function(htmlElement, container, component){
			if(htmlElement.hasAttribute('data-new-date') || !htmlElement.hasAttribute('data-node-xpath')){
				var dateClass   = $(htmlElement).attr('data-class');
				var newDateNode = kriya.general.convertTemplate(dateClass);
				if(!newDateNode) return false;
				newDateNode.attr('data-inserted', 'true');
				if(htmlElement.hasAttribute('data-add-stack')){
					kriyaEditor.settings.undoStack.push(newDateNode);
				}
				$(htmlElement).find('[data-type]').each(function(){
					var selector = $(this).attr('data-selector');
					var dateChildNode = kriya.xpath(selector, newDateNode);
					var newXpath = kriya.getElementXPath(dateChildNode);
					$(this).attr('data-node-xpath', newXpath);
				});
			}else if(htmlElement.hasAttribute('data-node-xpath')){
				var selector = $(htmlElement).attr('data-node-xpath');
				var dateNode = kriya.xpath(selector, container);
				if($(dateNode).length > 0 && htmlElement.hasAttribute('data-add-stack')){
					kriyaEditor.settings.undoStack.push(dateNode);
				}
				kriya.general.addToHistory(htmlElement, $(dateNode), container);
			}
		},
		/**
		 * Function to save the refernce attributes doi and pubmed after validate the all reference
		 * @author - jagan
		 */
		saveRefAttribute: function(htmlElement, container, component){
			if($(htmlElement).closest('[data-template]').length > 0) return false;
			var refColList = $(htmlElement).closest('.collection-item');
			if(refColList.attr('data-node-xpath')){
				var selector = refColList.attr('data-node-xpath');
				var refNode = kriya.xpath(selector, container[0]);
				if($(refNode).length > 0){
					var selector = $(htmlElement).attr('data-selector');
					var dataVal = $(htmlElement).html();
					if(selector && (matches = selector.match(/(?:(\/+)\@)([a-z0-9\-]+)$/ig))){
						var attrName = matches[0].replace(/(\/+)\@/ig, '');
						if(dataVal){
							$(refNode).attr(attrName, dataVal);
							if(attrName == "data-doi"){
								$(refNode).append('<span class="RefDOI" pub-id-type="doi">' + dataVal + '</span>');
							}
						}else{
							$(refNode).removeAttr(attrName);
						}
					}
				}
			}
		},
		getDeceasedInfo: function(htmlElement, container, component){
			if((htmlElement)[0].hasAttribute('deceased')){
				$(htmlElement)
			}			
		},
		setDeceasedAuthor: function(htmlElement, container, component){						
			if($(htmlElement)[0].nodeName == "INPUT"){
				if($(htmlElement)[0].checked){
					$(container).attr('deceased','yes');
				}
				else{
					$(container).removeAttr('deceased');
				}
				kriyaEditor.settings.undoStack.push($(container));
			}		
			$(container).find('.jrnlDeceased').addClass('hidden');		
			if($(htmlElement)[0].nodeName == "SPAN"){
				if($(htmlElement).html() != ""){
					$(container).append('<span class="jrnlDeceasedRef hidden" ref-type="deceased" rid="deceased1">1</span>');											
				}
				else{
					$(container).find('.jrnlDeceasedRef').remove();
				}
				$('[data-fn-type="deceased"]').find('p').html($(htmlElement).html());
				kriyaEditor.settings.undoStack.push($('[data-fn-type="deceased"]'));				
			}			
		},		
		deceasedCheckBox: function(container, component, htmlElement){
			if($(container).attr('deceased') == 'yes'){
				$(htmlElement)[0].checked = true;
			}
			else{
				$(htmlElement)[0].checked = false;
			}
		},
		checkBoxComponent: function(container, component, htmlElement){	
				var selector  = kriya.validateXpath($(htmlElement), this.container);
				var dataLinks = kriya.xpath(selector, container[0]);
				if (dataLinks.length > 0) {
					if (htmlElement.hasAttribute('data-value') && htmlElement.getAttribute('data-value') != dataLinks[0].nodeValue){
						$(htmlElement)[0].checked = false;
					}else{
						$(htmlElement)[0].checked = true;
						//to handle other type contribution
						if ($(htmlElement)[0].hasAttribute('data-check-field') && $(dataLinks)[0].hasAttribute('data-con-type')){
							var rid = $(dataLinks).attr('data-rid');
							if ($('[data-id="'+rid+'"]').length > 0){
								var checkField = $(htmlElement).attr('data-check-field');
								var checkFieldEle = $(htmlElement).closest('[data-type="popUp"]').find('[data-class="'+checkField+'"]');
								if (checkFieldEle.length > 0) {
									$(checkFieldEle).text($('[data-id="'+rid+'"]').text());
								}
							}
						}
					}
				}else{
					$(htmlElement)[0].checked = false;
				}			
		},
		getDatePicker: function (container, component){
			if($(container).attr('class') == "picker__input"){
				return;
			}
			
			var inputDay   = 'jrnlDay';
			var inputMonth = 'jrnlMonth';
			var inputYear  = 'jrnlYear';
			//Default date format
			var dateFormat = 'dd mm yyyy';

			var compEle = $('#compDivContent [data-type="'+component.type+'"][data-component="'+component.name+'"]');
			if(compEle.length > 0){
				if(compEle.attr('data-date-format')){
					dateFormat = compEle.attr('data-date-format');
				}
				if(compEle.attr('data-date-class')){
					var dateClass = compEle.attr('data-date-class').split('|');
					inputDay   = dateClass[0];
					inputMonth = dateClass[1];
					inputYear  = dateClass[2];
				}
			}else if($(container).closest('[data-name]').length > 0){
				var dateContainer = $(container).closest('[data-name]');
				if(dateContainer.attr('data-date-format')){
					dateFormat = dateContainer.attr('data-date-format');
				}
				if(dateContainer.attr('data-date-class')){
					var dateClass = dateContainer.attr('data-date-class').split('|');
					inputDay   = dateClass[0];
					inputMonth = dateClass[1];
					inputYear  = dateClass[2];
				}
			}
			
			var dateComponent = $(container).pickadate({
				format: dateFormat,
				clear: 'Cancel',
				close: 'Save',
				container: '#compDivContent',
				klass: {
					input: false
				},
				onClose: function() {
					var date  = this.get('select', dateFormat.match(/d+/i)[0]);
					var month = this.get('select', dateFormat.match(/m+/i)[0]);
					var year  = this.get('select', dateFormat.match(/y+/i)[0]);
					if(date != "" && month != "" && year != ""){
						//Update date and remove the validation error
						container.find('.'+inputDay).text(date).removeAttr('data-error'); 
						container.find('.'+inputMonth).text(month).removeAttr('data-error');
						container.find('.'+inputYear).text(year).removeAttr('data-error');

						$(container).attr('data-focusout-data', $(container).text());

						//Add Undo level and save only if the container is in the content
						if($(container).closest(kriya.config.containerElm).length > 0){
							kriyaEditor.settings.undoStack.push(container);
							kriyaEditor.init.addUndoLevel('date-update');	
						}
					}
					picker.close();
					picker.stop();
				},
				onSet: function(context) {
					//console.log('Just set stuff:', context);
				}
			});

			var picker = dateComponent.pickadate('picker');
			picker.set('select', new Date(container.find('.'+inputYear).text()+"-"+container.find('.'+inputMonth).text()+"-"+container.find('.'+inputDay).text()));
			picker.open();

		},
		/**
		 * Function to convert the template and set the html from the component
		 * @param className       - class name of the template
		 * @param component       - component element
		 * @param containerEle    - float element
		 * @param elementTemplate - template element
		 * @info                  -  any one parameters are mantatory
		 */
		convertTemplate: function(className, component, containerEle, elementTemplate){
			if(!className){
				className = component.attr('data-class');
			}
			var returnAfterInsert = false;
			if (component && $(component).find('[data-input]:not(.kriya-chips)').length > 0){
				returnAfterInsert = component;
				component = false;
			}
			// previous if condition does some check and makesthe component to false
			// so to forcefully get component from param - Jai (is been called from savelinkedobjects, issue face when saving for groupauthors)
			if ($('[data-type="popUp"]:visible').length > 1 && containerEle && $(containerEle)[0].hasAttribute('data-component')){
				component = $(containerEle);
				containerEle = false;
			}
			if(!component){
				component = $('#compDivContent [data-component][data-class="'+className+'"]');
			}
			if(!className && component.length < 1){
				return false;
			}
			
			if ($(returnAfterInsert).length > 0 && $(component).length == 0){
				var templateSelector = $(returnAfterInsert).attr('data-tmp-selector')
			}else{
				var templateSelector = component.attr('data-tmp-selector');
			}
			if(!elementTemplate || elementTemplate.length < 1) {
				if(templateSelector){
					elementTemplate = kriya.xpath(templateSelector);
					elementTemplate = $(elementTemplate);
				}else{
					elementTemplate = $('[data-element-template="true"] [tmp-class="'+className+'"]');
				}	
			}
			
			if(elementTemplate.length < 1){
        		return;
        	}else{
        		//get the template
        		var eleParentElement = elementTemplate.parent('[tmp-class]');
        		var eleParentClass = elementTemplate.parent('[tmp-class]').attr('tmp-class');
				//if the eleParentClass is not available in the content and has an attribute where it has to be, creating it dynamically

				if ($(kriya.config.containerElm + ' .' + eleParentClass).length == 0 && elementTemplate.parents('[tmp-class]').length > 0 && elementTemplate.parents('[tmp-class]')[0].hasAttribute('tmp-insert-after')){
					var prevElementClass = elementTemplate.parents('[tmp-class]').attr('tmp-insert-after');
					var insertAt = elementTemplate.parents('[tmp-class]').attr('tmp-insert-at');
					var eleParent = $(elementTemplate.parents('[tmp-class]')[0].cloneNode(true));
					eleParent.attr('class', eleParent.attr('tmp-class'));
					eleParent.removeAttr('tmp-class');
					eleParent.removeAttr('tmp-insert-after');
					eleParent.attr('id', uuid.v4());
					eleParent.find(':header').attr('id',uuid.v4());
					eleParent.attr('data-inserted', 'true');
					//eleParent.html('');

					eleParent.find('[tmp-class="' + className + '"]').remove();
					eleParent.find('*').each(function(){
						convertAttr(this, {});
		        	});
					
					if($(kriya.config.containerElm).find(prevElementClass).length > 0){
						$(kriya.config.containerElm).find(prevElementClass).last().after(eleParent);
					}else if($(kriya.config.containerElm).find(insertAt).length > 0){
						$(kriya.config.containerElm).find(insertAt).last().append(eleParent);
					}
					kriyaEditor.settings.undoStack.push(eleParent);
				}

				if ($(kriya.config.containerElm + ' .' + eleParentClass).length == 0 && elementTemplate.parents('[tmp-class]').length > 0 && elementTemplate.parents('[tmp-class]')[0].hasAttribute('tmp-insert-before')){
					var nextElementClass = elementTemplate.parents('[tmp-class]').attr('tmp-insert-before');
					var insertAt = elementTemplate.parents('[tmp-class]').attr('tmp-insert-at');
					var eleParent = $(elementTemplate.parents('[tmp-class]')[0].cloneNode(true));
					eleParent.attr('class', eleParent.attr('tmp-class'));
					eleParent.removeAttr('tmp-class');
					eleParent.removeAttr('tmp-insert-before');
					eleParent.attr('id', uuid.v4());
					eleParent.attr('data-inserted', 'true');
					//eleParent.html('');
					eleParent.find('[tmp-class="' + className + '"]').remove();
					eleParent.find('*').each(function(){
						convertAttr(this, {});
					});
					if($(kriya.config.containerElm).find(nextElementClass).length > 0){
						$(kriya.config.containerElm).find(nextElementClass).last().before(eleParent);
					}else if($(kriya.config.containerElm).find(insertAt).length > 0){
						$(kriya.config.containerElm).find(insertAt).last().append(eleParent);
					}
					
					kriyaEditor.settings.undoStack.push(eleParent);
				}

        		elementTemplate = $(elementTemplate[0].cloneNode(true));
        	}

        	var tmpObj = {};
        	var insertCitation = elementTemplate.attr('tmp-insert-citation');

        	tmpObj.parentid = "";
			tmpObj.insertAt   = elementTemplate.attr('tmp-insert-at');
        	tmpObj.class      = elementTemplate.attr('tmp-class');
        	tmpObj.linkClass  = elementTemplate.attr('tmp-add-link');
        	tmpObj.linkArray  = elementTemplate.attr('tmp-link-array');
		tmpObj.addTrack   = elementTemplate.attr('tmp-add-track');
			
			var orgDOI = $(kriya.config.containerElm).attr('data-org-doi');
			var objectIDLength = $(kriya.config.containerElm + ' .jrnlObjectID').length;
			tmpObj.objectID =  orgDOI + '.' + lpad(objectIDLength, 3); 

			elementTemplate.removeAttr('tmp-insert-at');
			elementTemplate.removeAttr('tmp-add-link');
			elementTemplate.removeAttr('tmp-link-array');
			elementTemplate.removeAttr('tmp-add-track');

			var tempContainer = false;
			// to save the current element into another component
			if($(component).length > 0 && component[0].hasAttribute('data-component-xpath')){
				var compXpath = component.attr('data-component-xpath');
				var componentType = component.attr('data-component');
				var parentComponent = $('[data-type="popUp"][data-component="' + compXpath + '"]');
				if (parentComponent.length > 0 && parentComponent.find('[data-temp-container][data-component-type="' + componentType + '"]').length > 0){
					tempContainer = parentComponent.find('[data-temp-container][data-component-type="' + componentType + '"]');
				}
			}
			
        	var sameElements = $(kriya.config.containerElm + ' .' + eleParentClass + ' .' + elementTemplate.attr('tmp-class') + ':not([data-track="del"])');
			
			//for getting the length of float elements (add all deleted objects too)
			if (component.length > 0 && $(component).attr('data-insert-type') == 'float'){
				sameElements = $(kriya.config.containerElm + ' .' + eleParentClass + ' .' + elementTemplate.attr('tmp-class'));
			}
			//if elemnet is more than one or multiple pop-up is opened - jai 31-10-2017
			//then get the same elements
			if ($(kriya.config.containerElm + ' .' + eleParentClass).length > 1 || $('[data-type="popUp"]:visible').length > 1){
				if (tempContainer){
					sameElements = $(tempContainer).find('.' + elementTemplate.attr('tmp-class') + ':not([data-track="del"])');
				}else if (returnAfterInsert && $(returnAfterInsert).hasClass('collection-item')){
					var selector = $(returnAfterInsert).parent().attr('data-source-selector');
					if ($(returnAfterInsert).parent()[0].hasAttribute('data-parent-container')){
						selector = $(returnAfterInsert).parent()[0].getAttribute('data-parent-container') + selector;
					}
					if ($(returnAfterInsert).parent()[0].hasAttribute('data-variables')){
						selector = kriya.validateXpath($(returnAfterInsert).parent(), kriya.config.activeElements, 'data-source-selector', selector);
					}
					selector += "[not(@data-track='del')]";
					sameElements = kriya.xpath(selector);
					if ($(returnAfterInsert).parent()[0].hasAttribute('data-parent-container')){
						selector = $(returnAfterInsert).parent()[0].getAttribute('data-parent-container')
						if ($(returnAfterInsert).parent()[0].hasAttribute('data-variables')){
							selector = kriya.validateXpath($(returnAfterInsert).parent(), kriya.config.activeElements, 'data-source-selector', selector);
						}
						selectorXpath = selector;
						selector = kriya.xpath(selector);
						if (selector.length > 0){
							tempContainer = $(selector[0]);
						}else{
							//if the parent is not available (like adding affiliation for the first time for editor/reviewer), create the parent element first
							var parentSelector = kriya.xpath(selectorXpath.replace(/^(.*)\/\/.*?$/, '$1'));
							if (parentSelector.length > 0 && eleParentElement.length > 0){
								var eleParent = $(eleParentElement[0].cloneNode(true));
								eleParent.attr('class', eleParent.attr('tmp-class'));
								eleParent.removeAttr('tmp-class');
								eleParent.removeAttr('tmp-insert-before');
								eleParent.attr('id', uuid.v4());
								eleParent.attr('data-inserted', 'true');
								eleParent.html('');
								$(parentSelector).append(eleParent);
								kriyaEditor.settings.undoStack.push(eleParent);
								tempContainer = $(eleParent);
							}
						}
					}else if (sameElements.length > 0 && $(sameElements[0]).closest('.' + eleParentClass).length > 0){
						tempContainer = $(sameElements[0]).closest('.' + eleParentClass);
					}
					if (tempContainer && tempContainer.closest('[data-id]').length > 0){
						var rid = tempContainer.closest('[data-id]').attr('data-id');
						if ($('[data-rid="' + rid + '"]').length > 0){
							tmpObj.parentid = rid;
						}
					}
				}else if (returnAfterInsert && $(returnAfterInsert)[0].hasAttribute('data-node-xpath')){
					var selector = $(returnAfterInsert).attr('data-node-xpath');
					selector = kriya.xpath(selector);
					if (selector.length > 0){
						tempContainer = $(selector[0]);
						sameElements = $(tempContainer).find('.' + elementTemplate.attr('tmp-class') + ':not([data-track="del"])');
					}
				}else{
					sameElements = $(kriya.config.containerElm + ' .' + eleParentClass).first().find('.' + elementTemplate.attr('tmp-class') + ':not([data-track="del"])');
				}
			}
			
			tmpObj.position = sameElements.length+1;
			
			// if the label has some order and it has been defined in component template,
			// then get it from the list of array for the current position - Jai 31-07-2017
			tmpObj.linkcue = tmpObj.position;
			if (tmpObj.linkArray != null && tmpObj.position > 0){
				tmpObj.linkArray = tmpObj.linkArray.split(',');
				if (elementTemplate[0].hasAttribute('tmp-linked-object')){
					var linkedObjects = elementTemplate.attr('tmp-linked-object');
					elementTemplate.removeAttr('tmp-link-array');
					cueValue = parseInt(tmpObj.position) + $(kriya.config.containerElm).find(linkedObjects).length;
					tmpObj.linkcue = tmpObj.linkArray[cueValue - 1];
				}else{
					tmpObj.linkcue = tmpObj.linkArray[tmpObj.position - 1];
				}
			}
			if(containerEle){
				tmpObj.fid    = containerEle.attr('data-id');
				tmpObj.findex = (tmpObj.fid && tmpObj.fid.match(/\d+$/,'')) ? tmpObj.fid.match(/\d+$/,'')[0] : '';
			}

			elementTemplate = $("<div tmp-class='templateWrap'>"+elementTemplate[0].outerHTML+"</div>");
			
			setQueryAsNodeValue(elementTemplate,containerEle);

			//convert attributes from tmp to original attribute EX:(tmp-class -> class)
        	convertAttr(elementTemplate,tmpObj);
        	elementTemplate.find('*').each(function(){
				convertAttr(this, tmpObj);
        	});

			//set the value to elements
			setVariableAsNodeValue(elementTemplate,tmpObj);
			elementTemplate.find(':contains($)').each(function(){
				setVariableAsNodeValue($(this),tmpObj);
			});

			setValueForCondition(elementTemplate,tmpObj);
			
			var prevSelector = elementTemplate.find('.' + className).attr('insert-after');
			var nextSelector = elementTemplate.find('.' + className).attr('insert-before');

			if (tempContainer){
				if (tempContainer.find('.'+eleParentClass).length > 0){
					if (tempContainer.find('.'+eleParentClass).find('.' + tmpObj.class).length > 0){
						tempContainer.find('.'+eleParentClass).find('.' + tmpObj.class + ':last').after(elementTemplate);
					}else{
						tempContainer.find('.'+eleParentClass).append(elementTemplate);
					}
				}else{
					if (tempContainer.find('.' + tmpObj.class).length > 0){
						tempContainer.find('.' + tmpObj.class + ':last').after(elementTemplate);
					}else{
						$(tempContainer).append(elementTemplate);
					}
				}
			}else if(prevSelector && $(kriya.config.containerElm).find(prevSelector).length > 0){
				if(sameElements.length > 0){
					$(elementTemplate).insertAfter(kriya.config.containerElm+' .'+eleParentClass+' .'+tmpObj.class+':last');
				}else{
					$(kriya.config.containerElm).find(prevSelector).last().after(elementTemplate);
				}
				elementTemplate.find('.'+className).removeAttr('insert-after');
			}else if(nextSelector && $(kriya.config.containerElm).find(nextSelector).length > 0){
				if(sameElements.length > 0){
					$(elementTemplate).insertAfter(kriya.config.containerElm + ' .' + eleParentClass + ' .' + tmpObj.class + ':last');
				}else{
					$(kriya.config.containerElm).find(nextSelector).last().before(elementTemplate);
				}
				elementTemplate.find('.'+className).removeAttr('insert-before');
			}else if(!tmpObj.insertAt){
				if(sameElements.length > 0){
					$(kriya.config.containerElm + ' .' + eleParentClass).first().find('.' + tmpObj.class + ':last').after(elementTemplate)
				}else{
					$(kriya.config.containerElm + ' .' + eleParentClass).first().append(elementTemplate);
				}
			}else if(tmpObj.insertAt){
				if (tmpObj.insertAt == "float"){

					var blockId = component.find('[data-type="htmlComponent"][data-class$="Caption"] .label').attr('data-block-id');
					if(blockId && /\-/g.test(blockId)){
						var containerID = blockId.replace(/\-[a-z0-9]+$/i, '');
						var container = $(kriya.config.containerElm + ' [data-id="' + containerID + '"]');
						if(container.length > 0){
							container.append(elementTemplate);
						}

						$(elementTemplate).find('[class$="Caption"]').attr('data-id', blockId);
						$(elementTemplate).find('[class$="Block"]').attr('data-id', 'BLK_' + blockId);
						
					}else{
						$(kriya.config.containerElm + ' .body').append(elementTemplate);
					}
					
					//insert after active element if it is not in front matter or in reference part - JAI 06-09-2017
					/*if (kriya.config.activeElements.closest('.jrnlRefText,.front').length == 0){
						$(elementTemplate).insertAfter(kriya.config.activeElements);
					}else{//else end of document
						$(kriya.config.containerElm).find('> div:last').append(sameElements.last());
					}*/
				}else if(tmpObj.insertAt == "selection"){
					kriya.selection.insertNode(elementTemplate[0]);
				}else if(tmpObj.insertAt != "false"){
					$(kriya.config.containerElm).find(tmpObj.insertAt).append(elementTemplate);
				}
			}else{
				$(kriya.config.containerElm).find('.body').append(elementTemplate);
			}

			var floatHeader = $(elementTemplate).find('.floatHeader'); 
			if(floatHeader.length > 0){
				var labelText = $(component).find('.label').html();
				labelText = labelText.replace(/([\.\,\:]+)$/, '');
				floatHeader.find('.floatLabel').html(labelText);
				$(elementTemplate).find('[data-id^="BLK_"],.jrnlRefText').attr('data-uncited', 'true');
			}

			elementTemplate.find('[data-tag]').renameElement('', true);
			elementTemplate.renameElement('', true);

			if(component.length > 0 && !returnAfterInsert){
				if (elementTemplate.find('.'+className+'[set-html]').length > 0){
					elementTemplate.find('.'+className).html(component.find('.'+className).html());
					elementTemplate.find('.'+className).removeAttr('set-html');
				}else{
					//set the xpath to the component if the same template need for multiple input elements - Ex (Adding author in reference)
					elementTemplate.find('[data-clone="true"]').each(function(){
						$(this).removeAttr('data-clone');
						setXpath($(this),component);
						$(this).remove();
					});
					//var saved = kriya.saveComponent.triggerSaveFunc(component[0],elementTemplate);
				}
			}else if (returnAfterInsert){
				if ($(returnAfterInsert).find('[data-input="true"]:not(.kriya-chips)').find('[class]').length == 0){
					$(elementTemplate).children().first().html($(elementTemplate).children().first().html() + $(returnAfterInsert).find('[data-input="true"]:not(.kriya-chips)').html());
				}else{
					$(returnAfterInsert).find('[data-input="true"]:not(.kriya-chips)').find('[class]').each(function(){
						$(elementTemplate).find('.' + $(this).attr('class')).html($(this).html());
						$(elementTemplate).find('.' + $(this).attr('class')).attr('id',$(this).attr('id'));
					});
				}
			}

			//Set the id for the new elements
			elementTemplate.find('*:not([id])').each(function(){
				$(this).attr('id', uuid.v4());
			});

			elementTemplate = elementTemplate.contents().unwrap();
			if(tmpObj.addTrack == "true"){
				elementTemplate.kriyaTracker('ins');
			}
			if(tmpObj.linkClass){
				var linkElement = kriya.general.convertTemplate(tmpObj.linkClass, '', elementTemplate, $('[data-element-template="true"] [tmp-link-type="' + tmpObj.linkClass + '"]'));
				if(linkElement){
					//kriya.selection.insertNode(linkElement[0]);
					//linkElement = linkElement.contents().unwrap();
					$(linkElement).attr('data-inserted', 'true');
				}
			}
			if(insertCitation || ($(component).find('#needCitation').length > 0 && $(component).find('#needCitation').is(':checked') == true)){
				var captionNode = elementTemplate.find('[class*="Caption"]');
				var floatId  = (captionNode.length < 1)?$(elementTemplate).attr('data-id'):captionNode.attr('data-id');
				if(floatId){
					var citeNode = kriya.general.insertCitation([floatId]);
					if (citeNode){
						citeNode = $(citeNode).filter(function() {return this.nodeType === 1;});
						captionNode.closest('.jrnlSupplBlock').attr('data-new-suppl', 'true');
						//If Caption doesn't have label then prepend
						if($(component).find('[data-class*="Caption"] .label').length == 0){
							$(component).find('[data-class*="Caption"]').prepend('<span class="label">' + citeNode.text() + '</span>');
						}
						$(citeNode).kriyaTracker('ins');
						$(citeNode).closest('.ins, [data-track="ins"]').attr('data-inserted', 'true');
						elementTemplate.attr('data-inserted', 'true');
						$(citeNode).attr('data-cite-type', 'insert');
						citeJS.floats.addAnchorTag(elementTemplate, 'contentDivNode');
					}
				}
			}

			if ($(elementTemplate)[0].hasAttribute('parent-id') && $(elementTemplate)[0].hasAttribute('data-id') && $(elementTemplate).attr('parent-id') != ""){
				$(elementTemplate).attr('data-id', $(elementTemplate).attr('parent-id') + '_' + $(elementTemplate).attr('data-id'));
				$(elementTemplate).removeAttr('parent-id');
			}
			
			return elementTemplate;
			
			function setValueForCondition(htmlEle,rootEle){
				if(rootEle){
					rootEle = rootEle[0];
				}
				htmlEle.find('condition').each(function(){
					$(this).find('if,elseif,else').each(function(){
						var conditionName     = this.nodeName;
						var conditionQuery    = $(this).attr('condition-query');
						var lengthRegexp   = $(this).attr('length-regex');
						if(!conditionQuery || !lengthRegexp){
							return;
						}
						var qResults = kriya.xpath(conditionQuery, rootEle);
						var reg = new RegExp(lengthRegexp);
						if(reg.test(qResults.length)){
							$(this).replaceWith($(this).html());
							return false;  //break the loop
						}else{
							$(this).remove();
						}
					});
					$(this).find('if,elseif,else').remove();
					$(this).replaceWith($(this).html());
				});
			}
			/**
			 * Fucntion to set the value from the root element via procesing the query from template
			 * @param htmlEle - Element to set the value 
			 * @param rootEle - Elemet which has the query result
			 */
			function setQueryAsNodeValue(htmlEle,rootEle){
				if(rootEle){
					rootEle = rootEle[0];
				}

				htmlEle.find('[tmp-query]').each(function(){
					var tmplate = $(this);
					var queryText = tmplate.attr('tmp-query');
					if(!queryText){
						return;
					}
					var qResults = kriya.xpath(queryText, rootEle);
					$(qResults).each(function(){
						var cloneTmp = tmplate.clone(true);
						cloneTmp.insertBefore(tmplate);
						cloneTmp.removeAttr('tmp-query');
						if(cloneTmp.attr('tmp-template')){
							setQueryAsNodeValue(cloneTmp, $(this));
						}else{
							var nodeVal = $(this).html();
							if(cloneTmp.attr('tmp-replace-reg')){
								var re = new RegExp(cloneTmp.attr('tmp-replace-reg'), 'i');
								nodeVal = nodeVal.replace(re, '');
								cloneTmp.removeAttr('tmp-replace-reg');
							}
							cloneTmp.html(nodeVal);
							cloneTmp.find('[id]').removeAttr('id');
							//Convert th element text to initial
							if(cloneTmp.attr('tmp-intial-text')){
								var initialText = cloneTmp.text();
								initialText = initialText.split(/\s/i).map(function(a){
									if(a.match(/\w/)){
										return a.match(/\w/)[0];	
									}
								}).join('');
								cloneTmp.text(initialText);
								cloneTmp.removeAttr('tmp-intial-text');
							}
						}
					});
					tmplate.remove();
				});
			}
			/**
			 * Function to set the value to element from variable
			 * @param ele - Element to set the value 
			 * @param tmpObj - hash array which has some information about the template element and float element
			 */
			function setVariableAsNodeValue(ele,tmpObj){
				//set the value to elements
				var nodeVal = ele.html();
				var matchVal = nodeVal.match(/\$([a-z]+)/i);
				if(matchVal && tmpObj){
					nodeVal = nodeVal.replace(/\$([a-z]+)/i,tmpObj[matchVal[1]]);
					ele.html(nodeVal);	
				}
			}
			/**
			 * Function to set ethe xpath to the component
			 * @param htmlEle - Element from template node
			 * @param component - componenet to set the xpath of the htmlElement
			 */
			function setXpath(htmlEle,component){
				var className = htmlEle.attr('class');
				$(component).find('[data-class*="'+className+'"]').each(function(){
					if($(this).text() == ""){
						return;
					}
					var clonedNode = htmlEle[0].cloneNode(true);
					$(clonedNode).insertBefore(htmlEle);

					var nodeXpath = kriya.getElementXPath($(clonedNode)[0]);
					if (! (/^\/\//).test(nodeXpath)) nodeXpath = "/"+nodeXpath;
					$(this).attr('data-node-xpath',nodeXpath);

					$(this).find('[data-class]').each(function(){
						var childClassName = $(this).attr('data-class');
						var clonedChild = $(clonedNode).find('.'+childClassName);
						if(clonedChild.length > 0){
							var nodeXpath = kriya.getElementXPath(clonedChild[0]);
							if (! (/^\/\//).test(nodeXpath)) nodeXpath = "/"+nodeXpath;
							$(this).attr('data-node-xpath',nodeXpath);	
						}
		        	});

				});
			}
			/**
			 * Function to convert the template element attibute to the orginal format and set the variable as attribute value
			 * @param ele - Element from template node
			 * @param obj - hash array which has some information about the template element and float element
			 */
			function convertAttr(ele, obj){
				var attrList = [];
				$.each($(ele)[0].attributes,function(i,attr){
					attrList.push(attr.nodeName);
					var attrVal = $(ele).attr(attr.nodeName);
					if (matchVal = attrVal.match(/\$([a-z]+)/)){
						attrVal = attrVal.replace(/\$([a-z]+)/,obj[matchVal[1]]);
						$(ele).attr(attr.nodeName,attrVal);
					}
				});

				var nodesuffix = $(ele).attr("tmp-node-suffix");
				var nodeprefix = $(ele).attr("tmp-node-prefix");
				var penultimatepunc = $(ele).attr("tmp-penultimatePunc");
				if(penultimatepunc && $(ele).attr('tmp-class') == $(ele).prev().attr('class') && $(ele).attr('tmp-class') != $(ele).next().attr('tmp-class')){
					$(ele).before(document.createTextNode(penultimatepunc));
				}else {
					if(nodeprefix){
						$(ele).before(document.createTextNode(nodeprefix));
					}
					if(nodesuffix){
						$(ele).after(document.createTextNode(nodesuffix));
					}
				}

				$(ele).removeAttr("tmp-node-suffix");
				$(ele).removeAttr("tmp-node-prefix");
				$(ele).removeAttr("tmp-penultimatePunc");
				$(ele).removeAttr("data-name");
				$(ele).removeAttr("data-override");
				$.each(attrList,function(i,attr){
					if(attr.match(/tmp-/)){
						$(ele).renameAttr(attr, attr.replace(/tmp-/,''));
					}
					if(attr && attr.match(/^data-role/g)){
						$(ele).removeAttr(attr);
					}
				});
			}
		},
		addNewField: function(container, tmpClass){

			if ($(container).find('[data-template]').length > 0){
				
				if(tmpClass){
					var templateNode = $(container).find('[data-class="' + tmpClass + '"][data-template]')[0];
				}else{
					var templateNode = $(container).find('[data-template]')[0];
				}
				
				var template = templateNode.cloneNode(true);
				template.removeAttribute('data-template');
				$(templateNode).parent().append(template);
			}else{
				return false;
			}

			$(template).find('[data-class]:first-child').focus();
			$(template).attr('data-clone', 'true');

			//Added by jagan - don't add data-added for first author in relaredt article 
			//also in product info - aravind
			if(($(template).attr('data-class') != "jrnlProName" && $(template).attr('data-class') != "jrnlRelArtAuthor") || $(container).find('[data-class="' + $(template).attr('data-class') + '"]:not([data-template])').length > 1){
				$(template).attr('data-added', 'true');
			}
			
			
			if(template.hasAttribute('contenteditable')){
				$(template).focus();
			}else{
				$(template).find('[contenteditable]').first().focus();	
			}
			

			$(template).focusout(function(e){
				if (e.relatedTarget && (e.relatedTarget == template || e.relatedTarget.parentNode == template)){
					//do nothing
				}else if ($(template).text() == ""){
					$(template).remove();
				}
			});
		},
		addInlineElement: function(container, template, tmpClass){
			if (!template){
				if ($(container).parent().find('[data-template]').length > 0){
					if(tmpClass){
						var template = $(container).parent().find('[data-class="' + tmpClass + '"][data-template]')[0].cloneNode(true);
					}else{
						var template = $(container).parent().find('[data-template]')[0].cloneNode(true);
					}
					template.removeAttribute('data-template');
				}else{
					var template = container.cloneNode(true);
					$(template).text('');
				}

				var className = ($(template).attr('class'))?$(template).attr('class').split(' ')[0]:'';
				if(className && !kriya.general.validateInlineElement(className, 'insert')){
					return false;
				}

				$(container).after(template);
				if ($(container).hasClass('activeElement')){
					$(container).removeClass('activeElement');
					$(template).addClass('activeElement');
					//$(template).removeAttr('id');
					var id = uuid.v4();
					$(template).attr('id', id);
					$(template).attr('data-placeholder', 'key');

					$(template).focus();
				}else{
					$(template).focus();
					if (!$(template)[0].hasAttribute('contenteditable')){
						$(template).find('[contenteditable]').first().focus();
					}
					$(template).attr('data-clone', 'true');
					$(template).attr('data-added', 'true');
				}
			}else{
				$(template).addClass('activeElement');
				var id = uuid.v4();
				$(template).attr('id', id);
				$(template).attr('data-placeholder', 'key');
				$(template).focus();
			}

			$(template).attr('data-inserted', 'true'); //to save the element
			$(template).focusout(function(e){
				if (e.relatedTarget && (e.relatedTarget == template || e.relatedTarget.parentNode == template)){
					//do nothing
				}else if ($(template).text() == ""){
					$(template).remove();
				}
			});
		},
		deleteInlineElement: function(container){
			var prev = $(container)[0].previousSibling;
			if (prev && prev.nodeType == 3 && (/^[\,\;]\s?$/.test(prev.nodeValue))){
				//$(prev).remove();
			}else{
				var next = $(container)[0].nextSibling;
				if (next && next.nodeType == 3 && (/^[\,\;]\s?$/.test(next.nodeValue))){
					//$(next).remove();
				}
			}

			var className = ($(container).attr('class'))?$(container).attr('class').split(' ')[0]:'';
			if(className && !kriya.general.validateInlineElement(className, 'delete')){
				return false;
			}

			var trackNode = $(container);
			if($(container).text() == ''){
				$(container).remove();
			}else{
				$(container).attr('data-deleted', 'true');
				$(container).kriyaTracker('del').removeClass('activeElement');
				trackNode = $(container).closest('.del, [data-track="del"]');
			}
			if($(trackNode).closest(kriya.config.containerElm).length > 0){
				kriyaEditor.settings.undoStack.push($(trackNode)[0]);
				kriyaEditor.init.addUndoLevel('delete-inline-elements');	
			}
		},
		/**
		 * Function to validate the keywords
		 * Get the maximum and minimum values from element template
		 */
		validateInlineElement: function(compName , className, action, btnComponent){
			//Check the minimum inline
			if(className && compName){
				var kwdTemplate = $('[data-component="'+ compName + '"]');
				if(kwdTemplate.length > 0){
					var maximum = kwdTemplate.attr('data-maximum');
					var minimum = kwdTemplate.attr('data-minimum');

					var eleLength = $(kriya.config.containerElm + ' .'+className+':not([data-track="del"])').length;
					if(minimum && minimum >= eleLength && action == "delete"){
						var notificationID = kriya.notification({
							title : 'Warning',
							type  : 'warning',
							content : 'Minimum Keyword is '+ minimum,
							icon : 'icon-warning2',
							timeout: 5000
						});
						return false;
					}else if(maximum && maximum <= eleLength  && action == "insert"){
						var notificationID = kriya.notification({
							title : 'Warning',
							type  : 'warning',
							content : 'Maximum Keyword is '+ maximum,
							icon : 'icon-warning2',
							timeout: 5000
						});
						return false;
					}else if(maximum &&  maximum <= (eleLength+1) && action == "insert"){
						$(btnComponent).addClass('hidden');
					// While delete the keyword check with maximum count and hidden the add keyword - rajesh
					}else if(maximum &&  maximum <= (eleLength-1) && action == "delete"){
						$(btnComponent).addClass('hidden');
					}else{
						$(btnComponent).removeClass('hidden');
					}
				}
			}
			return true;
		},
		rearrangeObject: function(component, val){
			if (val == 'up'){
				$(component).prev().before($(component));
			}else{
				$(component).next().after($(component));
			}
		},
		/**
		 * Fucntion to trigger the citation reorder
		 * @param currentNode - inserted or deleted node
		 * @param type - insert or delete
		 * @param classArray - array of classes to reorder
		 */
		triggerCitationReorder: function(currentNode, type, classArray){
			/*$(currentNode).attr('data-cite-type', type);
			citeJS.init();
			citeSeqObject  = citeJS.floats.checkSequence();
			confirmContent = citeJS.floats.renumberCitationsConfirmation();
			citeJS.floats.reorder();*/
			postal.publish({
				channel: 'citejs',
				topic  : 'reorder',
				data: {
					element : currentNode,
					type : type,
					citeClassArray : classArray
				}
			});
		},
		updateEquation: function(mml,tex,data, fontSize, mathType){
			mml = mml.replace(/<!-- (.*?) -->/g, '');  //replace to single line
			mml = mml.replace(/ class="(.*?)"/g, '');  //replace to single line
			mml = mml.replace(/[\s\t\r\n]+</g, '<');  //replace to single line
			mml = mml.replace(/[\n\r\t]+/g, '');  //replace to single line
			mml = mml.replace(/</g, '«');
			mml = mml.replace(/>/g, '»');
			mml = mml.replace(/&/g, '§');
			mml = mml.replace(/"/g, '¨');
			mml = mml.replace(/'/g, '`');
			var saveNode = $(kriya.config.editEqNode);

			if(data.print && data.print.eq_sizes){
				var pngFile = (data.online)?data.online.png:'';
				data = data.print;
				
				//var eqDepth = (data.eq_sizes.Depth)?Math.floor(parseFloat(data.eq_sizes.Depth) * 10)/10:'';
				var eqDepth   = data.eq_sizes.Depth;
				
	            //Width to view in kriya
	            var eqWidth  =  data.eq_sizes.Width;
	            var eqHeight =  data.eq_sizes.TotalHeight;
	            
				if(kriya.config.editEqNode && $(kriya.config.editEqNode).attr('class').match(/kriyaFormula/g)){
					$(kriya.config.editEqNode).attr('src', pngFile).attr('data-mathml', mml).attr('data-tex', tex).attr('data-eq-depth', eqDepth).attr('data-eq-height', data.eq_sizes.Height).attr('data-eq-totalheight', data.eq_sizes.TotalHeight).attr('data-eq-width', data.eq_sizes.Width).attr('data-font-size', fontSize).attr('data-primary-extension', 'pdf');
					kriya.config.editEqNode = false;
				}else{
					if(mathType == "display"){
						var eqnPara = kriya.general.convertTemplate('jrnlEqnPara', '', '');
						var eqImage = eqnPara.find('.kriyaFormula');
						var currentPara = $(kriya.selection.startContainer).closest('p');
						if(currentPara.length > 0){
							$(currentPara).after(eqnPara);
						}else{
							$(kriya.config.containerElm + ' .body').append(eqnPara);
							$(eqnPara)[0].scrollIntoView();
						}
						
						saveNode = eqnPara;
						eqnPara.attr('data-inserted', 'true');
					}else{
						var eqImage = kriya.general.convertTemplate('kriyaFormula', '', '');
						kriya.selection.insertNode(eqImage[0]);
						eqImage.attr('data-inserted', 'true');
						saveNode = eqImage;
					}
					//eqImage.kriyaTracker('ins');
					eqImage.attr('src', pngFile).attr('data-mathml', mml).attr('data-tex', tex).attr('data-eq-depth', eqDepth).attr('data-eq-height', data.eq_sizes.Height).attr('data-eq-totalheight', data.eq_sizes.TotalHeight).attr('data-eq-width', data.eq_sizes.Width).attr('data-font-size', fontSize).attr('data-primary-extension', 'pdf');
				}
				if(mathType == "display"){
					kriya.general.updateRightNavPanel($(saveNode).closest('.jrnlEqnPara').attr('data-id'), kriya.config.containerElm)
				}
			}
			kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;
			kriyaEditor.settings.undoStack.push(saveNode);
			kriyaEditor.init.addUndoLevel('equation');
		},
		insertSpecialChar : function(specialChar){
			if ($('[data-type="popUp"]:visible').length > 0){
				if (typeof(kriya.popupSelection) != "undefined"){
					currRange = kriya.popupSelection;
				}else{
					return false;
				}
			}else{
				var range = rangy.getSelection();
				currRange = range.getRangeAt(0);
			}
			if(specialChar){
				specialChar = document.createTextNode(specialChar);
				currRange.insertNode(specialChar);
				//Add tracker and undo level if only inserted in editor content
				if($(specialChar).closest(kriya.config.containerElm).length > 0){
					$(specialChar).kriyaTracker('ins');
					$(specialChar).closest('.ins,[data-track="ins"]').attr('data-inserted', 'true');
					kriyaEditor.init.addUndoLevel('special-character', specialChar);
				}
			}
			kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;
		},
		updateTableData : function(tableEle){
			$('body').append('<div class="dummyData"/>');
			$('body .dummyData').append(tableEle);
			tableEle = $('body .dummyData table');
			//var tableObj = $(editor.composer.tableSelection.table);
			var tableObj = $(kriya.selection.startContainer).closest('table');
			if(!tableEle || tableObj.length < 1){
				return false;
			}
			tableObj.find('.headerRow, .leftHeader').remove();
            if(tableEle.attr('data-table-type')){
            	tableObj.attr('data-table-type', tableEle.attr('data-table-type'));
            }

            //Update font size
            if(tableEle.attr('data-font-size')){
            	tableObj.attr('data-font-size', tableEle.attr('data-font-size'));
            }
            if(tableEle.find('thead').attr('data-font-size')){
            	tableObj.find('thead').attr('data-font-size', tableEle.find('thead').attr('data-font-size'));
            }
            if(tableEle.find('tbody').attr('data-font-size')){
            	tableObj.find('tbody').attr('data-font-size', tableEle.find('tbody').attr('data-font-size'));
            }

            //update cell data
            var updateAttrArray = ['align','data-align','data-align-left-width','data-align-right-width','valign','colspan','data-col-index','data-rotate','data-border-bottom','data-border-top','data-border-right','data-border-left', 'data-left-space', 'data-left-indent', 'data-left-indent-space'];
            tableObj.find('td,th').each(function(){
            	var updateNode = $(this);
            	var nodeXpath = kriya.getElementXPath(updateNode[0]);
            	nodeXpath = nodeXpath.replace(/(.*?)table\[.*?\]\//,'');
            	var sourceNode = kriya.xpath('.' + nodeXpath, tableEle[0]);
            	sourceNode = $(sourceNode);
            	if(sourceNode.length < 1){
            		return;
            	}
            	updateNode = $(updateNode);
            	$.each(updateAttrArray, function(i,val){
            		if(sourceNode.attr(val)){
            			updateNode.attr(val,sourceNode.attr(val));
	            	}else{
	            		updateNode.removeAttr(val);
	            	}
            	});

            	//update the text alignment for para
                updateNode.find('p').each(function(){
                      if(sourceNode.attr('align')){
                         $(this).attr('align',sourceNode.attr('align'));
                      }else{
                          $(this).removeAttr('align');
                      }
                      if(sourceNode.attr('data-emspace')){
                        var paraHTML = $(this).html();
                        paraHTML = paraHTML.replace(/\u2003/g, "");
                        $(this).html(paraHTML);
                        $(this).prepend("&emsp;".repeat(sourceNode.attr('data-emspace')));
                      }
                });
                //if para is not found
                if(updateNode.find('p').length < 0){
                    if(sourceNode.attr('data-emspace')){
                        var paraHTML = updateNode.html();
                        paraHTML = paraHTML.replace(/\u2003/g, "");
                        updateNode.html(paraHTML);
                        updateNode.prepend("&emsp;".repeat(sourceNode.attr('data-emspace')));
					}
                }
            });

            var colWidth = '';
			tableEle.find('col').each(function(i) {
				var outerWidth    = parseFloat($(this).attr('width'));
				colWidth = colWidth + outerWidth + ',';	
			});
			colWidth = colWidth.replace(/\,$/,'');
			tableObj.attr('data-col-widths-px', colWidth);

			if(tableEle.attr('data-spread-index')){
				tableObj.attr('data-spread-index', tableEle.attr('data-spread-index'));
			}else{
				tableObj.removeAttr('data-spread-index');
			}

			if(tableEle.attr('data-stub-column')){
				tableObj.attr('data-stub-column', tableEle.attr('data-stub-column'));
			}else{
				tableObj.removeAttr('data-stub-column');
			}
			//If the table has multiple head then split as multiple table 
            if(tableObj.find('thead+tbody').length > 1){
              	var clonedtable = tableObj[0].cloneNode();
              	$(clonedtable).attr('data-new-table', 'true');
              	//insert the empty table before the head
              	$(clonedtable).insertBefore(tableObj.find('thead'));
              	tableObj.find('[data-new-table]').each(function(i){
              		//Move the head and body to the table untile next table come
              		var newTab = this;
              		$(this).nextAll().each(function(){
              			if(this.nodeName == "TABLE"){
              				return false;
              			}
              			$(newTab).append(this);
              		});
              		//Add id and inserted attribute if it not first table or if it doen't have id
              		if(i != 0 || !$(newTab).attr('id')){
              			$(newTab).attr('data-inserted', 'true');
              			$(newTab).attr('id', uuid.v4());
              		}
              		kriyaEditor.settings.undoStack.push(newTab);
              	});
              	//unwrap the original table
              	tableObj.contents().unwrap();
            }else{
            	kriyaEditor.settings.undoStack.push(tableObj);
            }
			$('body .dummyData').remove();
			setTableIndex(tableObj);
			kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;
			kriyaEditor.init.addUndoLevel('table-editor');
		},
		/**
		 * Function to send the request to the server
		 * @param funtionName   - Name of the api function or function path
		 * @param parameters    - parameters to the request
		 * @param onSuccessFunc - Callback function after the request was Success
		 * @param onError       - Callback function after the request was Error
		 * @param onProgress    - Callback function during the rquest progress
		 */
		sendAPIRequest: function (funtionName, parameters, onSuccessFunc, onError, onProgress) {
			$.ajax({
				url : '/api/'+funtionName,
				type: 'POST',
				data: parameters,
				xhrFields: {
                    onprogress: function(e) {
                        if(onProgress){
                        	onProgress(e);
                        }
                    }
                },
				error: function(res){
					if(onError){
						onError(res);
					}
				},
				success: function(res){
					if(onSuccessFunc){
						onSuccessFunc(res);
					}
				},
			});
		},
		/**
		 * Function to get the label from citation text
		 * @param  citeNode  - citation element not bib citation
		 * @return Array     - return the array with part labels
		 */
		getCitationLabel : function(citeNode){
			citeNode = $(citeNode).clone(true);
			citeNode.find('.del').remove();
			if(!citeNode || $(citeNode).length < 1){
				return false;
			}
			var returnVal = {};
			var citeText = $(citeNode).text();
			return this.extractLabelFromText(citeText);
		},
		/**
		 * Function to extract citation label from citation text
		 * @param  citeText  - citation text
		 * @return Array     - return the array with part labels
		 */
		extractLabelFromText : function(citeText){
			var returnVal = {};
			//var citeText=citeText.replace(/\sand\b/g,'');
			var matchedCitation = citeText.match(/([0-9])([^\d]*)/g);
			$.each(matchedCitation, function(i, val){
				var citeNum = val.match(/(^\d)/)[1];
				val = val.replace(/([\,\s\-\u2013\u2014]+)$/g,'');
				val = val.replace(/(\d)(\s+?and)/g,'$1'); //Remove the and followed by number

				returnVal[''+citeNum+''] = {};
				returnVal[''+citeNum+'']['match-val'] = val;
				val = val.replace(/(^\d)/,'');
				returnVal[''+citeNum+'']['label'] = val.split(/[\,]/);
			});
			return returnVal;
		},
		//Function to insert the citation at the cursor position
		insertCitation: function(floatIds) {
			if(floatIds.length < 1 && kriya.selection){
				return false;
			}
			//only if the current selection in uncited
			if ($(kriya.config.activeElements).length > 0 && $(kriya.config.activeElements)[0].hasAttribute('data-uncited')){
				var citeHTML = this.getNewCitation(floatIds, kriya.config.containerElm, '');
				citeHTML = $.parseHTML(citeHTML);
				//citeHTML = citeHTML.reverse();
				kriya.general.insertCitationText(citeHTML);
				return citeHTML;
			}else{
				return false;
			}
		},
		insertCitationText: function(citeNodes,preventUndo){
			if ($(kriya.config.activeElements).length > 0 && ($(kriya.config.activeElements)[0].hasAttribute('data-citation-string') || $(kriya.config.activeElements)[0].hasAttribute('data-uncited'))){
				//restore query and add aito reply - jai 07-11-2017
				if (kriya.config.activeElements.find('.jrnlQueryRef').length > 0){
					var citeElements = $(citeNodes).filter(function() {
						return this.nodeType === 1;
					});
					//add reply if is orphan citation - auto reply as citation is linked to existing reference
					var qid = $(kriya.config.activeElements).find('.jrnlQueryRef').attr('data-rid');
					var queryNode = $('.query-div#' + qid);
					if (!$(kriya.config.activeElements).find('.jrnlQueryRef')[0].hasAttribute('data-replied') && queryNode.length > 0 && queryNode.attr('data-query-action') == "uncited-citation"){
						var target = $(queryNode).find('.query-holder').first();
						$(queryNode).find('.query-action').remove();
						$(queryNode).attr('data-remove-edit','true');
						
						//Modify by jagan show reference number and auhtor year text
						var refText = '';
						var refIDArray = $(citeElements).first().attr('data-citation-string').replace(/^ | $/g, '').split(' ');
						if (refIDArray.length > 0){
							for(var c=0;c<refIDArray.length;c++){
								var refID = refIDArray[c];
								var citeText = citeJS.floats.getReferenceHTML(refID,'',true,'');
								var refNumber = refID.replace(/[a-z]+/gi, '');
								if(citeJS.settings.R.citationType == "0"){
									refText += refNumber + ' (' + citeText + '), ';
								}else{
									refText += citeText + ', ';
								}
								
							}
							refText = refText.replace(/(\,\s)+$/g, '');
							/*var reference = $('#contentDivNode [data-id="' + refID + '"]');
							if (reference.length > 0){
								refText = reference.text();
								refText = refText.substring(0, 100) + '...';
							}*/
						}
						
						//prevent undolevel function call for #346 - priya
						var addReplyParam = {'content':'<i>This citation has been linked to an existing reference: ' + refText + '</i>'};
						if(preventUndo!=undefined && preventUndo=='false'){
							addReplyParam['undoLevel'] = 'false' ;
						}
						eventHandler.query.action.addReply(addReplyParam, target);
						$(citeElements).first().prepend(kriya.config.activeElements.find('.jrnlQueryRef'));
					}
				}
				//Citation insert modify by jagan to handle the track change when replace the citation
				if($(citeNodes).length > 1 || ($(citeNodes[0]).html() != $(kriya.config.activeElements).html())){
					
					if($(kriya.config.activeElements).attr('data-citation-string') != $(citeNodes).attr('data-citation-string') || $(citeNodes[0]).html() != $(kriya.config.activeElements).html()){
						$(kriya.config.activeElements).kriyaTracker('del');
						var delNode = $(kriya.config.activeElements).closest('.del,[data-track="del"]');
						$(citeNodes[0]).insertAfter(delNode);	
					}else{
						$(kriya.config.activeElements).replaceWith(citeNodes[0]);
					}
					
					var insertedNode = citeNodes[0];
					if($(citeNodes[0]).html() != $(kriya.config.activeElements).html()){
						$(insertedNode).kriyaTracker('ins');
					}
					
				}
				citeNodes.splice(0, 1);
				var trackNode = false;
				$(citeNodes).each(function(i){
					if(this.nodeType == 3 || $(this).html() != $(kriya.config.activeElements).html()){
						$(this).insertAfter(insertedNode);
						insertedNode = this;
					}else if(this.nodeType != 3 && trackNode && $(trackNode).length > 0 && $(this).html() == $(kriya.config.activeElements).html()){
						$(this).insertAfter(trackNode);
						insertedNode = this;
					}
					
					if(i == 0){
						$(insertedNode).kriyaTracker('ins');
						trackNode = $(insertedNode).closest('.ins,[data-track="ins"]');
					}
				});
			}else{
				//insert  citation at end of the word - jai
				var selection = rangy.getSelection();
				selection.removeAllRanges();
				var currSelection = kriya.selection.cloneRange();
				selection.addRange(currSelection);
				var range = selection.getRangeAt(0);
				var curPosition = range.endOffset;
				var rangeTextContent = range.commonAncestorContainer.textContent;
				var offSetArray = [], charArray = [' ', ',', '.', ';'];
				if ((curPosition > 0 && charArray.indexOf(rangeTextContent[curPosition - 1]) >= 0) || curPosition == 0){
					// do nothing, insert at current position
				}else{
					for (var i =0, l = charArray.length; i < l; i++){
						var r = rangeTextContent.indexOf(charArray[i], curPosition);
						if (r > 0) {
							offSetArray.push(r);
						}
					}
					if (offSetArray.length > 0){
						spacePosition = Math.min.apply(Math, offSetArray);
					}else{
						var temp = rangeTextContent.replace(/[\.\:\;\'\"\?\,\!\]\[\}\{\}\-\_]*$/, '');
						spacePosition = temp.length;
					} 
					range.moveEnd('character', spacePosition - curPosition);
				}
				$(citeNodes).each(function(){
					//check for auto-query when citations are added, and if yes, add reply - jai
					if ($(this)[0].nodeType == 1 && $(this)[0].hasAttribute('data-citation-string')){
						var rids = $(this).attr('data-citation-string').replace(/^ | $/g, '').split(/\s/g);
						//Modify bu jagan split the citation string and loop all rids
						for(var i=0;i<rids.length;i++){
							var rid = rids[i];
							var queryRef = $(kriya.config.containerElm).find('[data-id="' + rid + '"]').find('.jrnlQueryRef');
							if (queryRef.length > 0){
								$(queryRef).each(function(){
									var qid = $(this).attr('data-rid');
									var queryNode = $('.query-div#' + qid);
									if (!$(this)[0].hasAttribute('data-replied') && queryNode.length > 0 && queryNode.attr('data-query-action') == "uncited-reference" && queryNode.attr('data-float-id') == rid){
										var target = $(queryNode).find('.query-holder').first();
										$(queryNode).find('.query-action').remove();
										$(queryNode).attr('data-remove-edit', 'true');
										//prevent undolevel function call for #346 - priya
										var addReplyParam = {'content':'<i>This reference has been cited</i>'};
										if(preventUndo!=undefined && preventUndo=='false'){
											addReplyParam['undoLevel'] = 'false' ;
										}
										eventHandler.query.action.addReply(addReplyParam, target);
									}
								})
							}
						}
						
					}
				})
				selection.collapseToEnd();
				kriya.selection = selection.getRangeAt(0);
				kriya.selection.insertNode(citeNodes[0]);
				$(citeNodes[0]).kriyaTracker('ins');
				var insertedNode = citeNodes[0];
				citeNodes.splice(0,1);
				$(citeNodes).each(function(){
					$(this).insertAfter(insertedNode);
					insertedNode = this;
				});
				//update track node
				var trackNode = $(insertedNode).closest('.ins,[data-track="ins"]');
				if(trackNode.length > 0){
					eventHandler.menu.observer.addTrackChanges(trackNode[0], 'added');
					//#464 - All| Supplementary block - Content saving failed when inserted using CITE NOW  button from the block - Prabakaran.A (prabakaran.a@focusite.com)
					if($(trackNode).find('[data-inserted="true"]').length>0){
						$(trackNode).attr('data-inserted', true);
						$(trackNode).attr('data-nextid', $(trackNode).attr('id'));
					}	
					//End of #464	
				}

			}
		},
		/**
		 * Function to get the new citation for the float
		 * @param floatIds  - Ids of the float element, All should be the same float
		 * @param rootEle   - Root element of the float 
		 * @param isDirect  - get the direct author year citation for reference default is indirect
		 * @param citePunc  - if false puctuation for the citation will not return default true
		 * @param isSentenceStart - If the citation needs insert at the starting of sentense
		 */
		getNewCitation : function(floatIds, rootEle, isDirect, citePunc, isSentenceStart){
			if(typeof(citePunc) == 'undefined'){
				citePunc = true;
			}
			if(floatIds.length < 1){
				return false;
			}
			var returnVal = '';
			var tmpNode = $('<span class="tmpNode"/>');
			var citeConfig     = citeJS.floats.getConfig(floatIds[0]);
			//Get the citation prefix sufix and intermediate punctuation from citation config
			//If citation  config doesn't has then get from common setting else put empty value
			var citationPrefix = (citeConfig && citeConfig.startPunc != undefined)?citeConfig.startPunc:(citeJS.settings.startPunc)?citeJS.settings.startPunc:'';
			var citationSuffix = (citeConfig && citeConfig.endPunc != undefined)?citeConfig.endPunc:(citeJS.settings.endPunc)?citeJS.settings.endPunc:'';
			var interPunc      = (citeConfig && citeConfig.interPunc != undefined)?citeConfig.interPunc:(citeJS.settings.interPunc)?citeJS.settings.interPunc:'';
			var separator      = (citeConfig && citeConfig.separator != undefined)?citeConfig.separator:(citeJS.settings.separator)?citeJS.settings.separator:'';

			var idArray = [];
			var lastID  = '';
			var insertType = 'append';
			var punc = '';
			$.each(floatIds, function(i, fId){
				var floatElement = $(rootEle + ' [data-id="' + fId + '"]');
				if(floatElement.closest('[class*="Block"]').length > 0){
					floatElement = floatElement.closest('[class*="Block"]');
				}
				var floatClass = floatElement.attr('class');
				floatClass = (floatClass)?floatClass.split(' ')[0]:'';
				var citeClass = citeJS.settings.citeClass[floatClass];

				citeConfig  = citeJS.floats.getConfig(fId);
				var subPart = fId.split('-');
				var prevID  = (i != 0)?floatIds[i-1]:'';
				var prevSubParts = prevID.split('-');
				    /** Step 1 : If the citation is refernce and reference type is author year then append as new node
				      * Step 2 : If the subPart length is one (EX:F1) and prev and current float is same add with last or
				      *          If the subPart length is more then one (EX:F1-S1) and prev and current float is same add with last
				      * Step 3 : Else append as new node
					  */
					if(fId.match(/^([R])/) && citeConfig && citeConfig.citationType == "1"){
						idArray = [fId];
						insertType = 'append';
					}else if(((subPart.length == 1 || prevSubParts.length == 1) && fId.replace(/(\d+)/, '') == prevID.replace(/(\d+)/, '')) || ((subPart.length > 1 && prevSubParts.length > 1) && fId.replace(subPart.pop(), '') == prevID.replace(prevSubParts.pop(), ''))){
						idArray = lastID.split(' ');
						idArray.push(fId);
						insertType = 'replaceLast';
					}else{
						idArray = [fId];
						insertType = 'append';
					}

					var citeString = idArray.join(' ');
					var citeNode = $("<span class='lastNode'/>");
					citeNode.attr('data-citation-string', ' ' +citeString+ ' ');

					//var citeClass = kriya.general.getCitationClass(citeNode);
					if(citeClass){
						citeNode.addClass(citeClass);
						citeNode.attr('id', uuid.v4());
					}

					if(citeClass == "jrnlBibRef"){
						punc = interPunc;
						var citeText   = citeJS.floats.getReferenceHTML(idArray.join(' '),$(citeNode)[0] ,'',isDirect, $(tmpNode).find('.' + citeClass + ':last'));
					}else{
						punc = separator;
						var citeText   = citeJS.floats.getCitationHTML(idArray, [], 'getNewCitation', isSentenceStart);
					}
					citeNode.html(citeText);

					if(insertType == 'replaceLast'){
						tmpNode.find('.lastNode').replaceWith(citeNode);
					}else{
						tmpNode.find('.lastNode').removeClass('lastNode');
						tmpNode.append(citeNode);
						tmpNode.append(document.createTextNode(punc));
					}

					lastID = citeString;
			});

			
			tmpNode.find('.lastNode').removeClass('lastNode');
			returnVal = tmpNode.html();

			//Remove the intermediate punctuation at the end
			var re = new RegExp(punc+'$');
			returnVal = returnVal.replace(re, '');
			
			//Add prefix to the citation
			if(citePunc && citationPrefix){
				returnVal = citationPrefix+returnVal;
			}
			//Add suffix to the citation
			if(citePunc && citationSuffix){
				returnVal = returnVal+citationSuffix;
			}
			
			return returnVal;

		},
		sortRefCitationIDs: function(fIdArray){
			if(!fIdArray || typeof(fIdArray) != "object"){
				return false;
			}
			var refCiteConfig = citeJS.floats.getConfig('R');
			if(refCiteConfig && refCiteConfig.sortby){
				var tempNode = $('<div/>');
				for(r=0;r<fIdArray.length;r++){
					var fId = fIdArray[r];
					var refEle = $(kriya.config.containerElm + ' .jrnlRefText[data-id="' + fId + '"]');
					var ele = $('<span rid="' +fId+ '" />');
					tempNode.append(ele);
					if(refEle.length > 0){
						var classOrder = refCiteConfig.sortby.split('|');
						var sortText = '';
						for(s=0;s<classOrder.length;s++){
							var eleText = refEle.find(classOrder[s]).clone(true).cleanTrackChanges().text();
							if(classOrder[s].match(/year/i)){
								eleText = eleText.replace(/([a-z])/i, '');
							}
							eleText = eleText.toLowerCase();
							sortText += eleText;
						}
						ele.attr('sort-by', sortText);
					}
				}
				
				tinysort(tempNode.find('[sort-by]'), {attr:'sort-by'});
				fIdArray = [];
				tempNode.find('[rid]').each(function(){
					fIdArray.push($(this).attr('rid'));
				});

				if(fIdArray.length > 2){
					for(r=0;r<fIdArray.length;r++){
						var fId = fIdArray[r];
						var refEle = $(kriya.config.containerElm + ' .jrnlRefText[data-id="' + fId + '"]');
						if(fId && fId.match(/^R/) && refEle.length > 0){
							var citeText   = citeJS.floats.getReferenceHTML(fId);
							var refYear = refEle.find('.RefYear').text();
							var authorText = citeText.replace(refYear, '');
							for(c=r+1;c<fIdArray.length;c++){
								var mId = fIdArray[c];
								var matchRefEle =  $(kriya.config.containerElm + ' .jrnlRefText[data-id="' + mId + '"]');
								if(mId && mId.match(/^R/) && matchRefEle.length > 0){
									var matchCiteText   = citeJS.floats.getReferenceHTML(mId);
									var refYear = matchRefEle.find('.RefYear').text();
									var matchAuthorText = matchCiteText.replace(refYear, '');
									if(matchAuthorText == authorText){
										fIdArray[c] = fIdArray[r+1];
										fIdArray[r+1] = mId;
									}
								}
							}
						}
					}
				}
			}
			return fIdArray;
		},
        /**
         * Function to sort the citation ids based on the config
         * If config not found then return the input ids
         * @param citeIds - Array of the ids
         * @return sorted citation ids 
         */
        sortMultiCiteIDs: function(citeIds){
			citeIds = citeIds.slice(); //copy the array
        	if(citeJS.settings && citeJS.settings.multiCitationOrder){
        		var returnArray = [];
        		var multiCiteConfig   = citeJS.settings.multiCitationOrder.split('|');
        		// Loop for the cite config
        		$.each(multiCiteConfig, function(i, val){
        			//Filter the array get the matched for config ids
        			//if matched then remove the value from array
				    var filterArray = citeIds.filter(function(a, index){
				        var re = new RegExp('^'+val+'\\d');
				        return a.match(re);
				    });
				    // Sorting ascending order from filterArray value - rajesh
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;
					function sortAlphaNum(a,b) {
		    			var aA = a.replace(reA, "");
		    			var bA = b.replace(reA, "");
		    			if(aA === bA) {
		        			var aN = parseInt(a.replace(reN, ""), 10);
		        			var bN = parseInt(b.replace(reN, ""), 10);
		        			return aN === bN ? 0 : aN > bN ? 1 : -1;
		    			} else {
		        		return aA > bA ? 1 : -1;
		    			}
					}
					filterArray.sort(sortAlphaNum);
				    $.each(filterArray, function(i, val){
				    	var index = citeIds.indexOf(val);
						citeIds.splice(index, 1);	
				    	returnArray.push(val);
				    });
				    //returnArray = $.merge(returnArray, filterArray);
				});
				//Move the unmatched ids to last
				returnArray = $.merge(returnArray, citeIds);
        	}else{
        		returnArray = citeIds;
        	}

        	return returnArray;
        },
        deleteCitationElement: function(citeNode, citeId){
        	if($(citeNode).length > 0){
        		if($(citeNode)[0].hasAttribute('data-citation-string')){
    				var citeString = $(citeNode).attr('data-citation-string');
    				citeString = citeString.replace(/^\s+|\s+$/g, '').split(/\s+/);
    				if(citeString.length > 1){
    					//Remove the selected one from array
						var index = citeString.indexOf(citeId);
						citeString.splice(index,1);

						var insertedNode = citeNode;
						var citeNodes    = ﻿kriya.general.getNewCitation(citeString, kriya.config.containerElm,'',false);
						$(citeNodes).each(function(i){
							//$(this).attr('data-cite-type', 'insert');
							$(this).insertAfter(insertedNode);
							insertedNode = this;
							if(i == 0){
								$(insertedNode).kriyaTracker('ins');
							}
						});
						//var cid = $(insertedNode).closest('.ins,[data-track="ins"]').attr('data-cid');
						//$(citeNode).kriyaTracker('del', '', cid);
						$(citeNode).kriyaTracker('del', '');
						$(citeNode).attr('data-cite-type', 'delete');
    				}else{
    					$(citeNode).kriyaTracker('del');
    					$(citeNode).attr('data-cite-type', 'delete');
    					//kriyaEditor.settings.undoStack.push(citeNode);
    				}
    				kriya.general.afterDeleteCitation($(citeNode));
			}
			eventHandler.menu.observer.addJrnlDeleted($(citeNode).closest('p'));
        	}
        },
        /**
		 * Function to delete puctuation for the deleted citation node
		 * @param - citeNode deleted element
		 */
		afterDeleteCitation : function(citeNode){
			var citePrevNode = "";
			var citeNextNode = "";
			var delPuncFlag = false;
			var delElement = citeNode.closest('.del, [data-track="del"]');
			var trackId    = delElement.attr('data-cid');
			var saveNode   = $(citeNode).parents('[id]:first');

			// If the citation node has deleted parent then remove the puctuation of the citation node in prev and next sibling
			if(delElement.length > 0){
				//first get the previous sibling
				citePrevNode = delElement[0].previousSibling;
				//get the next sibling
				citeNextNode = delElement[0].nextSibling;
			}else{
				//first get the previous sibling
				citePrevNode = citeNode[0].previousSibling;
				//get the next sibling
				citeNextNode = citeNode[0].nextSibling;
			}
				
			//if the previous node is a text node
			if(citePrevNode && citePrevNode.nodeType == 3){
				//get the leading punctuation if any
				var prevText = citePrevNode.nodeValue;
				var matchText = prevText.match(/([\s\,\;]+)$/);

				//if the punctuation exists, delete the punctuation from the previous node and include it in the deleted citation node
				if(matchText && matchText.length > 0){
					prevText = prevText.replace(/([\s\,\;]+)$/, '');
					citePrevNode.nodeValue = prevText;
					
					var textNode = $(document.createTextNode(matchText[1])).insertBefore(delElement);
					textNode.kriyaTracker('del', '');
					//kriyaEditor.settings.undoStack.push(textNode);
					//citeNode.parents('.del').prepend(document.createTextNode(matchText[1]));

					delPuncFlag = true;
				}else{
					//If the only citation within the paranthesis, remove paranthesis as well
					//Check for previous paranthesis
					var prevParanText = prevText.match(/([\s\(\[]+)$/);
					if(prevParanText && prevParanText.length > 0){
						if(citeNextNode.nodeType == 3){
							//Check for next paranthesis
							var nextText = citeNextNode.nodeValue;
							var nextParanText = nextText.match(/^([\s\)\]]+)/);
							if(nextParanText && nextParanText.length > 0){
								citePrevNode.nodeValue = prevText.replace(/([\s\(\[]+)$/, ''); //Remove paranthesis from prev node
								citeNextNode.nodeValue = nextText.replace(/^([\s\)\]]+)/, ''); //Remove paranthesis from next node
								
								var nextTextNode = $(document.createTextNode(nextParanText[1])).insertAfter(delElement);
								nextTextNode.kriyaTracker('del', '');
								var prevTextNode = $(document.createTextNode(prevParanText[1])).insertBefore(delElement);
								prevTextNode.kriyaTracker('del', '');

								//citeNode.parents('.del').append(document.createTextNode(nextParanText[1]));
								//citeNode.parents('.del').prepend(document.createTextNode(prevParanText[1]));

								delPuncFlag = true;
							}
						}
					}
				}
			}
				
			//Commented by vijayakumar on 06-09-18 - when the citation deleted after space also deleted. After space should not delete
			/**if the next node is a text node and that a punctuation has not already been deleted
			if(citeNextNode && citeNextNode.nodeType == 3 && !delPuncFlag){
				//get the leading punctuation if any
				var nextText = citeNextNode.nodeValue;
				var matchText = nextText.match(/^([\s\,\;]+)/);
				//if the punctuation exists, delete the punctuation from the previous node and include it in the deleted citation node
				if(matchText && matchText.length > 0){
					nextText = nextText.replace(/^([\s\,\;]+)/, '');
					citeNextNode.nodeValue = nextText;
					//citeNode.parents('.del').append(document.createTextNode(matchText[1]));
					var textNode = $(document.createTextNode(matchText[1])).insertAfter(delElement);
					textNode.kriyaTracker('del', '');
				}
			}*/

			//If the citation is remove from the content without tracker
			if($(kriya.config.containerElm).find(citeNode).length < 1 && kriya.lastRemoveNodeSibling && kriya.lastRemoveNodeSibling.nodeType == 3 ){
				saveNode = $(kriya.lastRemoveNodeSibling).closest('[id]');
				var nodeText = kriya.lastRemoveNodeSibling.nodeValue;
				var prevNode = kriya.lastRemoveNodeSibling.previousSibling;
				var prevText = prevNode.nodeValue;

				//If next has punctuation then remove from next else remove from previous
				if(nodeText.match(/^([\,\;]+)/)){
					nodeText = nodeText.replace(/^([\s\,\;]+)/, '');
					kriya.lastRemoveNodeSibling.nodeValue = nodeText;
				}else if(prevNode.nodeType == 3 && prevText.match(/^([\,\;]+)/)){
					prevText = prevText.replace(/^([\s\,\;]+)/, '');
					prevNode.nodeValue = prevText;
				}else if(nodeText.match(/^([\)\]]+)/) && prevNode.nodeType == 3 && prevText.match(/([\(\[]+)$/)){
					//Check if the all citation are removed with in the paranthesis
					nodeText = nodeText.replace(/^([\s\)\]]+)/, '');
					kriya.lastRemoveNodeSibling.nodeValue = nodeText;
					
					prevText = prevText.replace(/([\s\(\[]+)$/, '');
					prevNode.nodeValue = prevText;
				}
			}

			kriyaEditor.settings.undoStack.push(saveNode);
			//citeJS.general.updateStatus();
			//kriya.general.triggerCitationReorder($(citeNode), 'delete');
		},
		/**
		 * Function to recall the deleted float object
		 */
		reCallFloat: function(floatNode, action){
			if(action == "reject" && $(floatNode).length > 0){
				var floatClass = $(floatNode).attr('class');
				var floatId = $(floatNode).attr('data-id');
				floatId = floatId.replace(/(\d+)$/, '');
				//Get the last element and construct the newid
				var lastElements = $(kriya.config.containerElm + ' .'+floatClass+'[data-id^="' + floatId + '"]').not(floatNode).last();
				var newFloatID = floatId+1;
				if(lastElements.length > 0){
					var lastElementId = lastElements.attr('data-id');
					if(lastElementId && lastElementId.match(/(\d+)$/g)){
						var lastIdNum = parseInt(lastElementId.match(/(\d+)$/g)[0]);
						newFloatID = floatId+(lastIdNum+1);
					}
				}
				
				floatNode.attr('data-id', newFloatID);
				var cptId = newFloatID.replace(/^BLK_/, '');
				var message = '';
				//If the float node has caption then update the caption id label float header label
				//else if float is reftext then update the serial number if the reference has serial number node
				if(floatNode.find('[class^="jrnl"][class$="Caption"]').length > 0){
					floatNode.attr('data-stream-name', 'a_'+cptId);
					var captionNode = floatNode.find('[class^="jrnl"][class$="Caption"]');
					captionNode.attr('data-id', cptId);
					var newFloatLabelString = citeJS.floats.getCitationHTML([cptId], [], 'renumberFloats');
					if(captionNode.find('.label').length > 0){
						captionNode.find('.label').html(newFloatLabelString);
						floatNode.find('.floatHeader .floatLabel').html(newFloatLabelString);						
					}
					message = '<div class="row">' + newFloatLabelString + ' has been recalled. Please note that all citations associated with ' + newFloatLabelString + ' would have to be reinserted. Would you like to insert a citation for ' + newFloatLabelString + '?</div>';
				}else if(floatClass == "jrnlRefText"){
					var citeConfig = citeJS.floats.getConfig(newFloatID);
					if(floatNode.find('.RefSlNo').length > 0 && citeConfig){
						newLabelText = citeConfig['prefix'] + 
						newFloatID.replace(/[A-Z]+/gi, '') + 
						citeConfig['labelSuffix'];
						$(floatNode).find('.RefSlNo').html(newLabelText);
					}
					var newFloatLabelString = kriya.general.getNewCitation([newFloatID], '', '', false);
					message = '<div class="row">Reference ' + newFloatLabelString + ' has been recalled. Please note that all citations associated with ' + newFloatLabelString + ' would have to be reinserted. Would you like to insert a citation for ' + newFloatLabelString + '?</div>';
				}
				var savedXpath = kriya.getElementXPath($(floatNode)[0]);
				message += '<div class="row" style="margin-top: 1rem !important;"><div class="col s12"><span class="btn btn-success btn-medium pull-right" data-node-xpath=\'' + savedXpath + '\' data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}">Cite Now</span><span class="btn btn-danger btn-medium pull-right" onclick="kriya.removeNotify(this)">Later</span></div></div>';
				kriya.notification({
					title : 'Information',
					type  : 'success',
					content : message,
					icon : 'icon-info'
				});
			}
		},
		/**
		 * Function to update the right navigation panel
		 * @param floatId - Float block element data-id or reference data-id
		 * @param rootEle - Root element of float block or refence node
		 */
		updateRightNavPanel: function(floatId, rootEle){
			var floatElement = $(rootEle + ' [data-id="' + floatId + '"]');
			if(!floatId || $(rootEle + ' [data-id="' + floatId + '"]').length < 1 || $(rootEle + ' [data-id="' + floatId + '"][data-track="del"]').length > 0){
				$('.navDivContent [data-panel-id="' + floatId + '"]').remove();
				var emptyMsg = $('<div class="emptyMsg">No Data Found</div>');
				if(floatElement.find('.jrnlFigure').length > 0){
					if($('#navContainer #figureDivNode [data-class="jrnlFigBlock"]').length == 0){
						$('#navContainer #figureDivNode').append(emptyMsg);
					}
				}else if(floatElement.find('.jrnlTable').length > 0){
					if($('#navContainer #tableDivNode [data-class="jrnlTblBlock"]').length == 0){
						$('#navContainer #tableDivNode').append(emptyMsg);
					}
				}else if(floatElement.find('.jrnlBox,.jrnlVideo,.jrnlSupplSrc,.kriyaFormula').length > 0){
					if($('#navContainer #dataDivNode').find('[data-class="jrnlBoxBlock"],[data-class="jrnlSupplBlock"],[data-class="jrnlVidBlock"],[data-class="jrnlEqnPara"],.kriyaFormula').length == 0){
						$('#navContainer #dataDivNode').append(emptyMsg);
					}
				}
				return false;
			}
			
			if(floatElement.attr('class') && !floatElement.attr('class').match(/jrnlRefText/)){
				var navElement   = $('.navDivContent [data-panel-id="' + floatId + '"]');
				var newTitle = floatElement.find('[class*="Caption"] .label:first').text();
				newTitle = (newTitle)?newTitle.replace(/[\.\|\,\s]+$/,''):'';
				var newCaption = '';
				if(floatElement.find('[class*="Caption"]').length > 0){
					newCaption = floatElement.find('[class*="Caption"]')[0].outerHTML;
				}
				//added by kirankumar:-when insert new image in unsitedlist that figure not showing
				if(floatElement.find('[data-uncited="true"]')){
					navElement.attr('data-uncited','true');
				}
				if(navElement.length > 0){
					//Update the card title
					if(navElement.find('.card-content .card-title').length > 0 && newTitle){
						navElement.find('.card-content .card-title').html(newTitle);
						navElement.find('.card-content .card-title').append('<span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'},\'channel\':\'components\',\'topic\':\'general\'}}" data-tooltip="Delete"><i class="icon-bin"></i></span>');
						navElement.find('.card-content .card-title').append('<span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}"><i class="icon-link"></i>Cite Now</span>');
					}
					//Update card title in card reveal
					if(navElement.find('.card-reveal .card-title').length > 0 && newTitle){
						//navElement.find('.card-content .card-title').append('<i class="material-icons right">more_vert</i>');
						//navElement.find('.card-reveal .card-title').html(newTitle + '<i class="material-icons right">close</i>');
					}

					//update the image in the card
					if(floatElement.find('.jrnlFigure').length > 0){
						navElement.find('.jrnlFigure').remove();
						floatElement.find('.jrnlFigure').each(function(){
							var newImage = $(this)[0].outerHTML;
							if(newImage){
								navElement.find('.card-content .card-title').after(newImage);
							}
						})
					}
					if(navElement.find('[class*="Caption"]').length > 0 && newCaption != ""){
						navElement.find('[class*="Caption"]').each(function(){
							this.outerHTML = newCaption;
						});
					}
					if(navElement.find('.kriyaFormula').length > 0){
						if(floatElement.find('.kriyaFormula').not('[data-track="del"],.del').length > 0){
							var source = floatElement.find('.kriyaFormula').not('[data-track="del"],.del').attr('src');
							var eqlLabel = floatElement.find('.jrnlEqnLabel').text();
							navElement.find('.card-content').html('<img class="kriyaFormula" src="' + source + '"><span class="jrnlEqnLabel">' + eqlLabel + '</span>');
						}else{
							navElement.remove();
						}
					}
				}else{
					var card = $('<div class="card data small" data-message="{\'click\':{\'funcToCall\':\'scrollToObj\',\'channel\':\'menu\',\'topic\':\'navigation\'}}">');
					card.attr('data-panel-id', floatId);
					card.attr('data-class', floatElement.attr('class'));

					card.append('<div class="card-content"><span class="card-title activator grey-text text-darken-4">' + newTitle + '</span>' + newCaption + '</div>');
					card.find('.card-content .card-title').append('<span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'},\'channel\':\'components\',\'topic\':\'general\'}}" data-tooltip="Delete"><i class="icon-bin"></i></span>');
					card.find('.card-content .card-title').append('<span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}"><i class="icon-link"></i>Cite Now</span>');
					if(floatElement.find('.jrnlFigure').length > 0){
						$('#navContainer #figureDivNode .emptyMsg').remove();
						var image = $('<div class="card-image waves-effect waves-block waves-light">' + floatElement.find('.jrnlFigure')[0].outerHTML + '</div>');
						image.insertAfter(card.find('.card-content .card-title'));
						card.append('<div class="card-reveal"><span class="card-title grey-text text-darken-4">' + newTitle + '<i class="material-icons right">close</i></span>' + newCaption + '</div>');

						$('#navContainer #figureDivNode').append(card);
					}else if(floatElement.find('.jrnlTable').length > 0){
						$('#navContainer #tableDivNode .emptyMsg').remove();
						$('#navContainer #tableDivNode').append(card);
					}else if(floatElement.find('.jrnlBox').length > 0){
						$('#navContainer #dataDivNode .emptyMsg').remove();
						$('#navContainer #dataDivNode #boxBlock').append(card);
					}else if(floatElement.find('.jrnlVideo').length > 0){
						$('#navContainer #dataDivNode .emptyMsg').remove();
						$('#navContainer #dataDivNode #videoBlock').append(card);
					}else if(floatElement.find('.jrnlSupplSrc').length > 0){
						$('#navContainer #dataDivNode .emptyMsg').remove();
						$('#navContainer #dataDivNode #supplBlock').append(card);
					}else if(floatElement.find('.kriyaFormula').length > 0){
						$('#navContainer #dataDivNode .emptyMsg').remove();
						var source = floatElement.find('.kriyaFormula').attr('src');
						var eqlLabel = floatElement.find('.jrnlEqnLabel').text();
						card.find('.card-content').html('<img class="kriyaFormula" src="' + source + '"><span class="jrnlEqnLabel">' + eqlLabel + '</span>');
						$('#navContainer #dataDivNode #equBlock').append(card);
					}
				}
			}else{
				var navElement = $('.navDivContent [data-panel-id="' + floatId + '"]');
				if(navElement.length < 1){
					navElement = $('<div class="resource_reference" data-panel-id="' + floatId + '" data-type="Reference" data-message="{\'click\':{\'funcToCall\':\'scrollToObj\',\'channel\':\'menu\',\'topic\':\'navigation\'}}"><div class="card reference"><div class="card-content"></div></div></div>');
					// Start Issue Id - #346 - Dhatshayani .D Jan 09, 2018 - Insert the reference in navigation panel in the same order as reference inserted in content panel.
					// #533 - Reference is not inserted. - Prabakaran A(prabakaran.a@focusite.com)
					var prevNodeId = (floatElement[0].previousSibling && typeof floatElement[0].previousSibling  != "undefined") ? floatElement[0].previousSibling.getAttribute('id'):'';
					var nextNodeId = (floatElement[0].nextSibling && typeof floatElement[0].nextSibling != "undefined" && typeof floatElement[0].nextSibling.getAttribute == "function") ? floatElement[0].nextSibling.getAttribute('id'): '';
					// End of #533
					// If the element has previous element and if it exists in navigation panel then insert the new reference after that reference.
					if(prevNodeId && $('#navContainer #refDivNode').find('*[data-panel-id="'+prevNodeId+'"]').length > 0){
						var prevPanelNode = $('#navContainer #refDivNode').find('*[data-panel-id="'+prevNodeId+'"]');
						$(prevPanelNode).after(navElement);
					}else if(nextNodeId && $('#navContainer #refDivNode').find('*[data-panel-id="'+nextNodeId+'"]').length > 0){// If the element has next element and if it exists in navigation panel then insert the new reference before that reference.
						var nextPanelNode = $('#navContainer #refDivNode').find('*[data-panel-id="'+nextNodeId+'"]');
						$(prevPanelNode).before(navElement);
					}else{// Otherwise insert as last child in the reference navigation panel.
						//After Enter First Affiliation the NoData Found Message Will Remove
						$('#navContainer #refDivNode').find('.emptyMsg').remove();
						$('#navContainer #refDivNode').append(navElement);
					}// End Issue Id - #346
				}
				
				if(navElement.find('.card-content').length > 0){
					var clonedFloat = $(floatElement).clone(true);
					clonedFloat.removeAttr('id');
					clonedFloat.find('[id]').removeAttr('id');
					navElement.find('.card-content').html(clonedFloat[0].outerHTML);
				}
				navElement.find('.card-action').remove();
				var cardAction = $('<div class="card-action" />');
				if(floatElement.attr('data-doi')){
					cardAction.append('<b>DOI: </b><a href="http://dx.doi.org/' + floatElement.attr('data-doi') + '" target="_blank">' + floatElement.attr('data-doi') + '</a><br>');
					navElement.find('.reference').attr('doi', 'true');
				}else{
					navElement.find('.reference').removeAttr('doi');	
				}
				if(floatElement.attr('data-pmid')){
					cardAction.append('<b>PubMed: </b><a href="http://www.ncbi.nlm.nih.gov/pubmed/' + floatElement.attr('data-pmid') + '" target="_blank">' + floatElement.attr('data-pmid') + '</a>');
					navElement.find('.reference').attr('pmid', 'true');
				}else{
					navElement.find('.reference').removeAttr('pmid');	
				}
				var citeCount = $(kriya.config.containerElm + ' [data-citation-string=" ' + floatId + ' "]').length;
				var cardTool = '<div class="card-tools"><span class="citeCount">' + citeCount + '</span><span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'deleteBlockConfirmation\', \'param\': {\'delFunc\':\'deleteFloatBlock\'},\'channel\':\'components\',\'topic\':\'general\'}}" data-tooltip="Delete" data-tooltip-id="0622c88a-5646-6e10-9162-5af075434469"><i class="icon-bin"></i></span><span class="btn btn-small" data-message="{\'click\':{\'funcToCall\': \'citeNow\',\'channel\':\'components\',\'topic\':\'citation\'}}"><i class="icon-link"></i>Cite Now</span></div>';
				cardAction.append(cardTool);
				if(cardAction.html() != ""){
					cardAction.insertAfter(navElement.find('.card-content'));
				}
			}
		},
		iniFilterOptions: function(dropDown, filterCards){
			if($(dropDown).length == 0){
				return false;
			}
			//$('.filterOptions').find('.dropdown-content').each(function(){
			$(dropDown).each(function(){
				var getAttr  = $(this).attr('data-get-query');
				var dropdown = $(this);
				var cardSelector = $(this).attr('data-card-selector');
				var cards = ($(filterCards).length == 0)?$(cardSelector):filterCards;
				$(cards).each(function(){
					var dataNode = kriya.xpath(getAttr, this);
					if($(dataNode).length > 0 && dataNode[0].nodeType == 2){
						var dataValue = dataNode[0].nodeValue;
						if(dropdown.find('[data-value="' + dataValue + '"]').length == 0 && dataValue){
							var label = (dataValue == 'ins')?'Inserted':(dataValue == 'del')?'Deleted':(dataValue == 'sty')?'Formatted':(dataValue == 'styrm')?'Formatted Removed':dataValue;
							dropdown.append('<li data-value="' + dataValue + '"><input class="kriya-checkbox" type="checkbox" checked="true"/> ' + label + '</li>');
						}
					}
				});
			});
		},
		applyZebra: function(tableEle){
            var filter = "odd";
            if($(tableEle).find('thead').length > 0){
                filter = "even";
            }
            var column_count = $(tableEle).find('tbody tr').getRowColumn();
            $(tableEle).find("tbody tr").filter(function() {
                  //Adds row children and colspan values of those children
                  var child_count = 0;
                  $("td", this).each(function(index) {
                    if ($(this).attr('colspan') != null) {
                      child_count += parseInt($(this).attr('colspan'));
                    } else {
                      child_count += 1;
                    }
                  });

                  return child_count == column_count;
            }).filter(':'+filter).attr('data-fill', 'true');

            $(tableEle).find("tr[data-fill='true'] td[rowspan]").each(function() {
                  $(this).parent().nextAll().slice(0, this.rowSpan - 1).attr('data-fill', 'true');
            });
		 },
		 addToHistory: function(component, dataChild, contentNode){
			 //Component should have foucus out data 
			if($(component)[0].hasAttribute('data-focusout-data')){

				if(!dataChild || $(dataChild).length == 0){
					return false;
				}

				var hid = $(dataChild).attr('id');
				
				var popper = $(component).closest('[data-component]');

				//If dataChild is a attribute node then get owner id
				if($(dataChild)[0].nodeType == 2){
					hid = $($(dataChild)[0].ownerElement).attr('id');
				}
				if($(contentNode).closest('[data-component]').length > 0 && $(contentNode).attr('data-node-xpath')){
					var selector = $(contentNode).attr('data-node-xpath');
					var content = kriya.xpath(selector);
					var contentNodeId = $(content).attr('id');	
				}else{
					var contentNodeId = $(contentNode).attr('id');	
				}
				
				//If history id not found then get content id as hid
				if(!hid){
					hid = contentNodeId;
				}

				//If node doesn't have id then return false
				if(!hid || !contentNodeId){
					return false;
				}

				var objDate = new Date();
				var locale = "en-us",
				month = objDate.toLocaleString(locale, { month: "short" });
				time  = objDate.toLocaleString(locale, { hour: 'numeric',minute:'numeric', hour12: true });
				objDate = objDate.getDate() + ' ' + month + ' ' + (objDate.getYear() + 1900) + ' '+time; 

				var user = $('#headerContainer .userProfile .username').text();
				var hContent = '';

				var newVal = $(component).attr('data-focusout-data');
				var oldVal = $(component).attr('data-focusin-data');
				oldVal = (oldVal)?oldVal:'';

				//Remove the empty space at start and end
				newVal = newVal.replace(/^\s+|\s+$/g, '');
				oldVal = oldVal.replace(/^\s+|\s+$/g, '');

				//If the changes made and revert then oldVal is equal to newVal 
				//so return false
				if(oldVal == newVal){
					return false;
				}

				var type = $(component).attr('type');
				var compId = $(component).attr('id');
				var labelNode = $('label[for="' + compId + '"]');
				/**
				 * If labelNode length is zero and next elemnt is a label then get that as labelNode
				 * else labelNode length is zero and next elemnt is not label and prev elemnt is label then get that as labelNode
				 */
				if(labelNode.length == 0){
					labelNode = ( $(component).next('label').length > 0 )?$(component).next('label'):( $(component).prev('label').length > 0 )?$(component).prev('label'):'';
				}

				//Get the label for the field
				var label = '';
				if($(labelNode).length > 0){
					var clonedLabelNode = $(labelNode).clone(true);
					clonedLabelNode.find('.fieldChange').remove();
					label = clonedLabelNode.text(); 
					label = label.replace(/([\s|\.\;\:\|]+)$/, '');
				}
				
				switch (type){
					case 'text':
					case 'number':
						/**
						 * STEP 1: If old value is empty and new value is available then content was newly created
						 * STEP 2: If new value is empty and old value is available then content was deleted
						 * STEP 3: else content was edited. 
						 */

						var newTitle = (newVal.length >= 15)?' title="' + newVal.replace(/"/g,'&#8221;') + '" ':'';
						var oldTitle = (oldVal.length >= 15)?' title="' + oldVal.replace(/"/g,'&#8221;') + '" ':'';
						
						//newVal = (newVal.length >= 15)?newVal.substring(0, 15)+'...':newVal;
						//oldVal = (oldVal.length >= 15)?oldVal.substring(0, 15)+'...':oldVal;

						label = (label)?' in ' + label + ' field':'';

						if(!oldVal && newVal){
							hContent = '<span class="change-person">' + user + '</span> <span>inserted</span> <span class="newVal" ' + newTitle + '>' + newVal + '</span> <span>' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						}else if(!newVal && oldVal){
							hContent = '<span class="change-person">' + user + '</span> <span>deleted</span> <span class="oldVal" ' + oldTitle + '>' + oldVal + '</span> <span>' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						}else{
							hContent = '<span class="change-person">' + user + '</span> <span>changed</span> <span class="oldVal" ' + oldTitle + '>' + oldVal + '</span> <span>to</span> <span class="newVal" ' + newTitle + '>'+ newVal +'</span> <span>' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						}
						break;
					case 'checkbox':
						/**
						 * If newVal is true then check box is checked else unchecked
						 */
						if(newVal == "true"){
							hContent = '<span class="change-person">' + user + '</span> <span class="newVal">checked</span> <span>' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						}else{
							hContent = '<span class="change-person">' + user + '</span> <span class="newVal">unchecked</span> <span>' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						}
						break;
					case 'radio':
							var radioName = $(component).attr('name');
							/**
							 * Get the old label of the radio using foucsin data
							 */
							var oldRadio = popper.find('[name="' + radioName + '"][data-focusin-data="true"]');
							if(oldRadio.length > 0){
								var oldLabelNode = $('label[for="' + oldRadio.attr('id') + '"]');
								if(oldLabelNode.length == 0){
									oldLabelNode = ( $(oldRadio).next('label').length > 0 )?$(oldRadio).next('label'):( $(oldRadio).prev('label').length > 0 )?$(oldRadio).prev('label'):'';
								}
								if($(oldLabelNode).length > 0){
									oldLabelNode = oldLabelNode.clone(true);
									oldLabelNode.find('.fieldChange').remove();
									var oldLabel = oldLabelNode.text();
									if(newVal == "true"){
										hContent = '<span class="change-person">' + user + '</span> <span>changed</span> <span class="oldVal">' + oldLabel + '</span> <span>to</span> <span class="newVal">'+ label +'</span> <span>on</span> <span class="change-time">' + objDate + '</span>';
									}
								}
							}else{
								hContent = '<span class="change-person">' + user + '</span> <span>checked</span> <span class="newVal">'+ label +'</span> <span>on</span> <span class="change-time">' + objDate + '</span>';
							}
						break;	
					case 'inputTags':	
						/**
						 * STEP 1: Split the old and new value using comma
						 * STEP 2: Loop the old array and check if the old value is not in new array then old value is delted
						 * STEP 3: Loop the new array and check if the new value is not in old array then new value is inserted
						 * STEP 4: Construct the display content using inserted and deleted value 
						 */

						var oldArr = (oldVal)?oldVal.split(','):[];
						var newArr = (newVal)?newVal.split(','):[];
						var deletedVal = '';
						for(var o=0;o<oldArr.length;o++){
							if(newArr.indexOf(oldArr[o]) < 0){
								deletedVal += oldArr[o] + ',';
							}
						}
						deletedVal = deletedVal.replace(/\,+$/g, '');

						var insertedVal = '';
						for(var o=0;o<newArr.length;o++){
							if(oldArr.indexOf(newArr[o]) < 0){
								insertedVal += newArr[o] + ',';
							}
						}
						insertedVal = insertedVal.replace(/\,+$/g, '');
						
						hContent = '<span class="change-person">' + user + '</span>';
						if(deletedVal != ''){
							hContent += ' <span>deleted</span> <span class="newVal">' + deletedVal + '</span>';
						}
						if(deletedVal != '' && insertedVal != ''){
							hContent += ' <span>and</span> '
						}
						if(insertedVal != ''){
							hContent += ' <span>inserted</span> <span class="newVal">' + insertedVal + '</span>';
						}
						hContent += ' <span>in ' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						break;
					case 'linkedObjects':
						
						/**
						 * STEP 1: Split the old and new value using comma
						 * STEP 2: Loop the old array and check if the old value is not in new array then old value is delted
						 * STEP 3: Loop the new array and check if the new value is not in old array then new value is inserted
						 * STEP 4: Construct the display content using inserted and deleted value 
						 */

						var oldArr = (oldVal)?oldVal.split(','):[];
						var newArr = (newVal)?newVal.split(','):[];
						var deletedVal = '';
						for(var o=0;o<oldArr.length;o++){
							if(newArr.indexOf(oldArr[o]) < 0){
								var cloneNode = $(kriya.config.containerElm).find('[data-id="' + oldArr[o] + '"]:first').clone(true);
								cloneNode.find('sup,.removeNode,.label').remove();
								var nodeVal = cloneNode.text();
								nodeVal = nodeVal.replace(/^\s+|\s+$/g, '');
								
								//remove the more than one space
								nodeVal = nodeVal.replace(/(\s\s+)/g, ' ');

								var nodeTitle = (nodeVal.length >= 15)?'title="' + nodeVal + '"':'';
								//nodeVal = (nodeVal.length >= 15)?nodeVal.substring(0, 15)+'...':nodeVal;

								deletedVal += '<span class="newVal" ' + nodeTitle + '>' + nodeVal + '</span>,';
							}
						}
						deletedVal = deletedVal.replace(/\,+$/g, '');

						var insertedVal = '';
						for(var o=0;o<newArr.length;o++){
							if(oldArr.indexOf(newArr[o]) < 0){
								var cloneNode = $(kriya.config.containerElm).find('[data-id="' + newArr[o] + '"]:first').clone(true);
								cloneNode.find('sup,.removeNode').remove();
								var nodeVal = cloneNode.text();
								nodeVal = nodeVal.replace(/^\s+|\s+$/g, '');
								//remove the more than one space
								nodeVal = nodeVal.replace(/\s\s+/g, '');

								var nodeTitle = (nodeVal.length >= 15)?'title="' + nodeVal + '"':'';
								//nodeVal = (nodeVal.length >= 15)?nodeVal.substring(0, 15)+'...':nodeVal;

								insertedVal += '<span class="newVal" ' + nodeTitle + '>' + nodeVal + '</span>,';
							}
						}
						insertedVal = insertedVal.replace(/\,+$/g, '');
						
						hContent = '<span class="change-person">' + user + '</span>';
						if(deletedVal != ''){
							hContent += ' <span>deleted</span> ' + deletedVal;
						}
						if(deletedVal != '' && insertedVal != ''){
							hContent += ' <span>and</span> '
						}
						if(insertedVal != ''){
							hContent += ' <span>inserted</span> ' + insertedVal;
						}
						hContent += ' <span>in ' + label + ' on</span> <span class="change-time">' + objDate + '</span>';
						break;	
					case 'objFile':
							var comment = popper.find('[data-class="floatComment"]').html();
							if($(component)[0].nodeName == "IMG"){
								if(!oldVal && newVal){
									hContent = '<p><span class="change-person">' + user + '</span> <span>inserted image on</span> <span class="change-time">' + objDate + '</span></p> <p class="imageContainer"><a href="' + newVal + '" title="Click to view the image" target="_blank"><img src="' + newVal + '" /></a></p>';
								}else{
									hContent = '<p><span class="change-person">' + user + '</span> <span>changed image  on</span> <span class="change-time">' + objDate + '</span></p> <p class="imageContainer"><a href="' + oldVal + '" title="Click to view the image" target="_blank"><img  src="' + oldVal + '"/></a> <span class="material-icons">arrow_forward</span> <a href="' + newVal + '" title="Click to view the image" target="_blank"><img  src="' + newVal + '"/></a></p>';
								}
							}else{
								var newFilePath = newVal.split('|')[0];
								var newFileName = (/\|/g.test(newVal))?newVal.split('|')[1]:newVal;
								if(!oldVal && newVal){
									hContent = '<span class="change-person">' + user + '</span> <span>inserted file</span> <a href="' + newFilePath + '" title="Click to open the file" target="_blank"><span class="newVal">' + newFileName + '</span></a> <span>on</span> <span class="change-time">' + objDate + '</span>';
								}else{
									var oldFilePath = oldVal.split('|')[0];
									var oldFileName = (/\|/g.test(oldVal))?oldVal.split('|')[1]:oldVal;
									hContent = '<span class="change-person">' + user + '</span> <span>changed file</span> <a href="' + oldFilePath + '" title="Click to open the file" target="_blank"><span class="oldVal">' + oldFileName + '</span></a> <span>to</span> <a href="' + newFilePath + '" title="Click to open the file" target="_blank"><span class="newVal">' + newFileName + '</span></a> <span>on</span> <span class="change-time">' + objDate + '</span>';
								}
							}
							
							if(comment){
								hContent += '<p class="hisComment">' + comment + '</p>';
							}
						break;
					case 'table':
							if(newVal == "paste"){
								hContent = '<span class="change-person">' + user + '</span> <span>pasted the table on</span> <span class="change-time">' + objDate + '</span>';
							}
						break;	
					default:
						break;
				}
				
				//Create the card and append in right navigation panel
				var hisCard = $('<div class="historyCard card" id="' + uuid.v4() + '" data-h-id="' + hid + '" data-message="{\'click\':{\'funcToCall\': \'hightlightField\',\'channel\':\'components\',\'topic\':\'PopUp\'}}"/>');	
				hisCard.append('<div class="row"><span class="historyContent"/></div>');
				hisCard.attr('data-inserted', 'true');
				hisCard.find('.historyContent').html(hContent);

				//set the content id in history card
				hisCard.attr('data-content-id', contentNodeId);

				//Set the user details in card attribute
				hisCard.attr('data-userId', kriyaEditor.user.id);
				hisCard.attr('data-role', kriya.config.content.role);
				
				//remove the focus out data from component
				$(component).removeAttr('data-focusout-data');

				if(!$(popper).hasClass('manualSave') && $(dataChild)[0].nodeType != 2 && $(dataChild)[0].nodeType != 3 && ((!$(dataChild)[0].hasAttribute('data-inserted') && !$(dataChild)[0].hasAttribute('add-data-inserted')) || $(dataChild)[0].nodeType == 2)){
					$(contentNode).attr('data-edited-node', 'true');
				}
				
				if($(kriya.config.containerElm).find(contentNode).length > 0){
					$('#historyDivNode').prepend(hisCard);
					//add the card to stack for save
					kriyaEditor.settings.undoStack.push(hisCard);
				}else if($(contentNode).closest('[data-component]').length > 0 && kriya.config.subPopUpCards){
					$(kriya.config.subPopUpCards).prepend(hisCard);
				}
				
				
				return true;
				
			}
			return false;
		 },
		 /**
		  * Function to reorder table foot note symbols
		  */
		 reorderTblFN: function(tableBlockNode){
			 var linkArray = $('[data-element-template="true"] [tmp-class="jrnlTblFoot"]').attr('tmp-link-array');
			 var linkStyle = $('[data-element-template="true"] [tmp-class="jrnlTblFoot"]').attr('tmp-link-style');
			 var styleNotFor = $('[data-element-template="true"] [tmp-class="jrnlTblFoot"]').attr('tmp-link-style-not');
			 styleNotFor = (styleNotFor)?styleNotFor.split(','):[];
			if($(tableBlockNode).length > 0 && linkArray){
				linkArray = linkArray.split(',');
				var count = 0;
				$(tableBlockNode).find('.jrnlTblFootGroup .jrnlTblFoot[fn-type="linked-fn"]:not([data-track="del"])').each(function(i){
					var labelNode = $(this).find('.jrnlFNLabel:first');
					var r = (i+1)/linkArray.length;
					r = Math.ceil(r);
					var symbol = linkArray[count];
					if(symbol){
						var startTag = '';
						var endTag = '';
						if(linkStyle && styleNotFor.indexOf(symbol) < 0){
							startTag = '<' + linkStyle.replace(/\s/g, '><') + '>';
							endTag = '</' + linkStyle.replace(/\s/g, '></') + '>';
						}
						symbol = symbol.repeat(r);
						symbol = startTag + symbol + endTag;
						labelNode.html(symbol);
					}
					if(linkArray.length == count+1){
						count = 0;
					}else{
						count++;
					}
				});

			}
		 },
		 updateTblFNLinkCount: function(node){
			if($(node).length > 0){
				var citeString = $(node).attr('data-citation-string');
				if(citeString){
					citeString = citeString.replace(/^\s|\s$/g, '');
					var fnNode = $(kriya.config.containerElm + ' .jrnlTblBlock .jrnlTblFoot[data-id="' + citeString + '"]');
					var fnLinkLength = $(kriya.config.containerElm + ' .jrnlTblBlock [data-citation-string=" ' + citeString + ' "]:not([data-track="del"])').length;
					fnNode.find('.linkCount').html(fnLinkLength);
				}
			}
		 }

	};
    return kriya;

}(window.kriya || {}));
