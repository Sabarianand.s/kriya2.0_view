﻿var config = {
    "defaultUnits":"pt",
    "baseLeading":11,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY",
    "minStackSpace":18,
    "minNoLinesOnPag":3,
    "placeStyle":"Sandwich",
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.15,
    "applyTableBorderColor":'WHITE',
    "pageSize":{
        "width":595.276,
        "height":790.866
    },
    "exportPDFpreset":{
        "print":{
            "crop_marks":true, 
            "bleed_marks":false, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":8.504
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":18,
      "right":18,
      "gutter":18
    },
    "geoBoundsVerso":[80.5,47.5,723.366141732283,535.276
    ],
    "geoBoundsRecto":[80.5,655.275590551181,723.366141732283,1143.11773228346
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1"
    },
    "pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
                  {"width":234.888, "height":611.366,"floatsCited":false,"gutter":0},
                  {"width":234.888, "height":611.366,"floatsCited":false,"gutter":18}
                ],
                "columnDetails":[
                  {"width":234.888, "height":642.895,"floatsCited":false,"gutter":0},
                  {"width":234.888, "height":642.895,"floatsCited":false,"gutter":18}
                ],
                "openerPageMargin":{
                    "top":112,
                    "bottom":67.5,
                    "inside":60,
                    "outside":47.5
                },
            "otherPageMargin":{
                "top":80.5,
                "bottom":67.5,
                "inside":60,
                "outside":47.5
                }
            }
         },
    "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":340.85, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>(Continued)</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>(Continued)</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsPara",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],
    
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlCLRH']", "frame-name":"CLRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlCRRH']", "frame-name":"CRRH", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlSTLH']", "frame-name":"LRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlLRF']", "frame-name":"LRF", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlRRF']", "frame-name":"RRF", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlSTRH']", "frame-name":"RRH", "action":"move", "styleOverride":null}
       ]       
     },
 "colorDetails":{
         "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,80"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,60"
                    },
                  "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"95,172,223"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"210,226,241"
                    },
                 "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,255"
                    }
                }            
            }
        },
         "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "8px",
                    "font-family" : "'Times New Roman MT Std', Times New Roman"
                },
                "fontPath"  : "TimesNewRomanMTStd/TimesNewRomanMTStd_0.otf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'TimesNewRomanMTStd-Italic_0', Times New Roman",
                    "font-weight"    : "normal",
                    "line-height"    : "normal",
                    "padding" : "0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'TimesNewRomanMTStd_0', Times New Roman",
                    "font-weight"    : "normal",
                    "line-height"    : "normal",
                    "padding" : "0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            }
        },
        "equation" : {
          "default" : {
              "page":{
                  "size" : "504.676pt"
              },
              "font" : {
                  "size"    : "9.5pt",
                  "ledding" : "11.5pt",
                  "path"    : "/MinionPro/",
                  "ext"     : "otf",
                  "bold"    : "MinionProBold",
                  "italic"  : "MinionProItalic",
                  "bold_italic":"MinionProBoldItalic",
                  "main"    : "MinionProRegular"
              }
           },
           "jrnlTblBlock" : {
              "page":{
                  "size" : "487.776pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "9pt",
                "path"    : "/TimesNewRomanMTStd/",
                "ext"     : "otf",
                "bold"    : "TimesNewRomanMTStd-Bold",
                "italic"  : "TimesNewRomanMTStd-Italic",
                "bold_italic":"TimesNewRomanMTStd-BoldIt",
                "main"    : "TimesNewRomanMTStd"
              }
           }
        },
    "runOnSectionsOrArticles":[
        {"xpath":"//div[@class='jrnlRefGroup']",
            "continueType":"samePage",
            "spaceAbove":30,
            "firstbaselineOffset":1296852079,
            "rule":{"width":"0.5","offset":"15","showOnPageTop":"false"},
            "columnDetails":{        
                "openerPageColumnDetails":[
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":18}
                    ],
                    "columnDetails":[
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":18}
                    ],
                    "openerPageMargin":{
                        "top":112,
                        "bottom":67.5,
                        "inside":60,
                        "outside":47.5
                    },
                "otherPageMargin":{
                    "top":80.5,
                    "bottom":67.5,
                    "inside":60,
                    "outside":47.5
                    }
                }
            },
        {"xpath":"//div[@class='jrnlAppGroup']",
            "continueType":"samePage",
            "spaceAbove":30,
            "firstbaselineOffset":1296135023,
            "columnDetails":{        
                "openerPageColumnDetails":[
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":18}
                    ],
                    "columnDetails":[
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":18}
                    ],
                    "openerPageMargin":{
                        "top":112,
                        "bottom":67.5,
                        "inside":60,
                        "outside":47.5
                    },
                "otherPageMargin":{
                    "top":80.5,
                    "bottom":67.5,
                    "inside":60,
                    "outside":47.5
                    }
                }
            },
        {"xpath":"//div[@class='jrnlAppRefGroup']",
            "continueType":"samePage",
            "spaceAbove":30,
            "firstbaselineOffset":1296852079,
            "rule":{"width":"0.5","offset":"15","showOnPageTop":"false"},
            "columnDetails":{        
                "openerPageColumnDetails":[
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":611.366,"floatsCited":false,"gutter":18}
                    ],
                    "columnDetails":[
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":0},
                      {"width":234.888, "height":642.895,"floatsCited":false,"gutter":18}
                    ],
                    "openerPageMargin":{
                        "top":112,
                        "bottom":67.5,
                        "inside":60,
                        "outside":47.5
                    },
                "otherPageMargin":{
                    "top":80.5,
                    "bottom":67.5,
                    "inside":60,
                    "outside":47.5
                    }
                }
            },
        ]
    }
    this.config = config;