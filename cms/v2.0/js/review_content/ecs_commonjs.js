/**
* commonjs - this javascript holds all the functions required across ECS
*                            this is going to be huge but is constructed to element by element,
*                            so that the functionalities can be turned on and off by just calling the required functions
*                            Below is the core function declaration, which has the initialization, settings and a functions to retrieve the settings
*/
var commonjs = function() {
    //default settings
    var settings = {};
    settings.citationClass = {
        'jrnlFigCaption':'jrnlFigRef',
        'jrnlTblCaption':'jrnlTblRef',
        'jrnlBibCaption':'jrnlBibRef',
        'jrnlBibRef': 'Reference', 
        'jrnlFigRef': 'Figure',
        'jrnlTblRef': 'Table',
        'Figure': {
            'floatContainerHeadID': '#floatFigHead',
            'floatContainerID': '#floatsPanelFigContent',
            'floatCitationClass': '.jrnlFigRef',
            'floatNodeClass': '.jrnlFigBlock',
            'floatNodeType': 'Fig',
            },
        'Reference': {
            'floatContainerHeadID': '#floatRefHead',
            'floatContainerID': '#floatsPanelRefContent',
            'floatCitationClass': '.jrnlBibRef',
            'floatNodeClass': '.jrnlRefText',
            'floatNodeType': 'Bib',
            },
        'Table': {
            'floatContainerHeadID': '#floatTblHead',
            'floatContainerID': '#floatsPanelTblContent',
            'floatCitationClass': '.jrnlTblRef',
            'floatNodeClass': '.jrnlTblBlock',
            'floatNodeType': 'Tbl',
            },
    };
    settings.contextMenu = {
        'type': 'readonly'     //options => readonly, editor
    };
    settings.editor = {
        'isDirty': true,
        'init': false,
        'equationNode': false 
    };
    settings.nonEditableClass = {
        'RefernceCitation': 'jrnlBibRef',
        'FigureCitation': 'jrnlFigRef',
        'TableCitation': 'jrnlTblRef',
        'SchemaCitation': 'jrnlSchRef',
        'BoxCitation': 'jrnlBoxRef',
    };
    settings.equationEditor = 'tiny_mce_wiris';
    settings.currNotice = undefined;
    // use this number as identifier to identify the change panel during the undo action
    settings.currUndoLevel = 0;

    //add or override the initial setting
    var initialize = function(params) {
        function extend(obj, ext) {
            if (obj === undefined){
                return ext;
            }
            var i, l, name, args = arguments, value;
            for (i = 1, l = args.length; i < l; i++) {
                ext = args[i];
                for (name in ext) {
                    if (ext.hasOwnProperty(name)) {
                        value = ext[name];
                        if (value !== "undefined") {
                            obj[name] = value;
                        }
                    }
                }
            }
            return obj;
        }
        settings = settings = extend(settings, params);
        return this;
    };

    var getSettings = function(){
        var newSettings = {};
        for (var prop in settings) {
            newSettings[prop] = settings[prop];
        }
        return newSettings;
    };

    return {
        init: initialize,
        getSettings: getSettings,
        settings: settings
    };
}();


    /**
    *    Extend commonjs to add general function
    */
    (function (commonjs) {
        //settings = commonjs.getSettings();
        commonjs.general = {
            popCitationData: function(citationNode){
                if (!citationNode) return;
                var citRefNode = $(citationNode).clone();
                $(citRefNode).find('.del').remove();
                $(citRefNode).find('.ins').contents().unwrap();
                citRefNodeText = $(citRefNode).text();
                RIDType = citRefNode.attr('class').split(' ')[0];
                //RIDType = RIDType.replace(/^jrnl([A-Z]).*$/, '$1');
                var citationType = exetercs.settings.citationClass[RIDType];
                var objType = exetercs.settings.citationClass[citationType]['floatContainerHeadID'];
                $('.medial-strip .context-toggles a[data-field-type="'+objType+'"]').trigger('click');
                var resourcePanelClass = exetercs.settings.citationClass[citationType]['resourcePanel'];
                var parentDiv = $(resourcePanelClass).closest('div[class*="DivNode"]');
                $(parentDiv).attr('data-selected-rid', '')
                
                defaultStyle = '.resource_figures{max-height: 100px !important;} .resource_figures .jrnlFigBlock{max-height:68px !important;overflow:hidden;}.resource_figures .jrnlFigBlock .mediaDownloadDiv{display:none !important}.resource_figures .jrnlFigCaption{max-height:60px !important;overflow:hidden;}.resource_reference .type{display:none !important;} .resource_reference, .resource_figures{padding: 5px 10px !important;border-left:5px solid rgba(221, 221, 221, 0.35) !important;}.column-resources * p{font-size: 13px !important;line-height: 120% !important;margin: 0px;}.resource_figures img{float: right !important;max-width: 15% !important;margin-left: 15px !important;margin: 0 !important;}.resource_figures .label{float:left;display: inline-block !important;margin: 0px !important;padding: 0px !important;background-color: inherit !important;margin-right: 6px !important;font-weight: 600;}';
                styleHTML = defaultStyle;
                 $('style[data-field="resources"]').html(styleHTML);
                 $('.citationWidget').remove();
                 $('.column-resources .resources').before('<div class="citationWidget" style="padding:5px"><p class="modifyButtons"><button type="button" class="btn btn-info btn-xs citeCitation" onclick="commonjs.editor.citation.' + process + 'Citation(\'' + refType + '\')" style="min-width: 60px; margin-right: 5px;">' + process.charAt(0).toUpperCase() + process.substr(1) + '</button><button type="button" onclick="commonjs.editor.citation.cancel()" class="btn btn-warning btn-xs" style="min-width: 60px; margin-right: 5px;">Cancel</button></p></div>');
            },
            
            validateCitationData: function(citationNode){
                var citRefNode = $(citationNode).clone();
                $(citRefNode).find('.del').remove();
                $(citRefNode).find('.ins').contents().unwrap();
                citRefNodeText = $(citRefNode).text();
                citRefNodeText = citRefNodeText.replace(/\u00A0/g, ' ');
                var citeType = '';
                if (/Fig/gi.test(citRefNodeText)){
                    citeType = 'jrnlFigRef';
                }else if (/Tab|Tbl/gi.test(citRefNodeText)){
                    citeType = 'jrnlTblRef';
                }else if (/Suppl.*(movie|video|file)/gi.test(citRefNodeText)){
                    citeType = 'jrnlSupplRef';
                }else if (/movie|video/gi.test(citRefNodeText)){
                    citeType = 'jrnlVidRef';
                }
                citeData = citRefNodeText;
                var citationValid = false;
                if (/^(Figures|Figure|Figs?\.|Figs?|Tables|Table|Tab\.|Tab|Tbl|Schemas|Schema|Sch\.|Sch|Box|Pictures?|Author response image|Images?|Charts?|Appendix|Movies?|videos?|audios?|(supplement(s|ary|al)? (Files?|movies?|videos?)))\s{0,1}([0-9]{1,3})(\.?(\s?[a-z]{1}|[a-z]{1,})|\.[0-9]+)?([\,\-\u2012\u2013\u2014]\s?[a-z](?=\s|\)|\]|\.|\;|\:))?(\s*(and|\&|\,|\:|\;|\(|\[|\-|\u2013|\u2012|\u2014){1,}\s?([A-Z](?![a-z0-9])|[A-Z]?[0-9]{1,4})(?![\.])(\s*[a-zA-Z]{0,1}|[a-z]{0,})(?=[\s\)\]\,\.\;\:]*))*$/gi.test(citRefNodeText)){
                    citationValid = true;
                }
                if (/^(Appendix) ([0-9]+\s?[\-\u2012\u2013\u2014]\s?)?(Figures|Figure|Figs?\.|Figs?|Tables|Table|Tab\.|Tab|Tbl|Schemas|Schema|Sch\.|Sch|Box|Pictures?|Author response image|Images?|Charts?|Appendix|Movies?|videos?|audios?|(supplement(s|ary|al)? (Files?|movies?|videos?)))\s{0,1}([0-9]{1,3})(\.?\s?[a-z]{1}|\.[0-9]+)?([\,\-\u2012\u2013\u2014]\s?[a-z](?=\s|\)|\]|\.|\;|\:))?(\s*(and|\&|\,|\:|\;|\(|\[|\-|\u2013|\u2012|\u2014){1,}\s?([A-Z](?![a-z0-9])|[A-Z]?[0-9]{1,4})(?![\.])\s*[a-zA-Z]{0,1}(?=[\s\)\]\,\.\;\:]*))*$/gi.test(citRefNodeText)){
                    citationValid = true;
                }
                if (/^(Figures|Figure|Figs?\.|Figs?|Tables|Table|Tab\.|Tab|Tbl|Schemas|Schema|Sch\.|Sch|Box)\s{0,1}([0-9]{1,3})(\.?[a-zA-Z]{1,})?(\s?(and|\&|\,|\:|\;|\(|\[|\-|\u2014|\u2013|\u2012|\s){1,}([0-9]{1,3})?[a-zA-Z]{0,1})?[\.\-\s]*((figures?\s?)?(supplement(s|ary|al)?|source data)\s?(figures?\s?)?([0-9]{1,3}[a-zA-Z]{0,1})([\,\-\u2012\u2013\u2014]\s?[a-z](?=\s|\)|\]|\.))?(\s?(and|\&|\,|\-|\u2013|\u2014|\u2012)\s?)*( source data\s?[0-9\u2012\u2013\u2014a-z\,\-\s]))|((Figures|Figure|Figs?\.|Figs?|Tables|Table|Tab\.|Tab|Tbl|Schemas|Schema|Sch\.|Sch|Box)\s{0,1}([0-9]{1,3})(\.?[a-zA-Z]{1,})?(\s?(and|\&|\,|\:|\;|\(|\[|\-|\u2014|\u2013|\u2012|\s){1,}([0-9]{1,3})?[a-zA-Z]{0,1})?)[\.\-\s]*((figures?\s?)?(supplement(s|ary|al)?|source data)\s?(figures?\s?)?([0-9]{1,3}[a-zA-Z]{0,1})([\,\-\u2013\u2012|\u2014]\s?[a-z](?=\s|\)|\]|\.))?(\s?(and|\&|\;|\,|\-|\u2013|\u2014|\u2012)\s?(([a-z]|[0-9]{1,3}[a-zA-Z]{0,1})(?![a-z\s])))*)$/gi.test(citRefNodeText)){
                    citationValid = true;
                }
                if (/^((supplement(s|ary|al)? (Files?|movies?|videos?))\s{0,1}([0-9]{1,3}))(\.?[a-zA-Z0-9]{1,})?([\,\-\u2012\u2013\u2014]\s?[a-z](?=\s|\)|\]|\.))?((and|\&|\,|\:|\;|\(|\[|\-|\u2014|\u2013|\u2012|\s){1,}([A-Z]?[0-9\.]{1,4})[a-zA-Z]{0,1})*$/gi.test(citRefNodeText)){
                    citationValid = true;
                }
                var validCitationArray = [citationValid, citeType, citeData];
                return validCitationArray;
            },
            
            updateCitationData: function(citationNode, checkNode){
                if (!citationNode) return;
                if (checkNode == undefined){checkNode = true;}
                var citRefNode = $(citationNode).clone();
                $(citRefNode).find('.del').remove();
                $(citRefNode).find('.ins').contents().unwrap();
                citRefNodeText = $(citRefNode).text();
                citRefNodeText = citRefNodeText.replace(/\u00A0/g, ' ');
                RIDType = citRefNode.attr('class').split(' ')[0];
                RIDType = RIDType.replace(/^jrnl([A-Z]).*$/, '$1');
                if (citRefNode.hasClass('jrnlSupplRef')){
                    RIDType = 'SP';
                }
                if (/author response/i.test(citRefNodeText)){
                    RIDType = 'ARF';
                }
                if (/appendix ([0-9]+\s?[\-\u2012\u2013\u2014]\s?)?figure/i.test(citRefNodeText)){
                    RIDType = 'AF';
                    return false;
                }
                if (citationNode.text() == ''){
                    $(citationNode).remove();
                }
                /*else if (/^[\s\,\-\;]+$/.test(citationNode.text())){
                    $(citationNode).contents().unwrap();
                }*/
                else if (citRefNode.hasClass('jrnlBibRef')){
                    RIDType = 'B';
                    if (/^([0-9\u2013\,\-\s]{1,})/gi.test(citRefNodeText)){
                        var citRefArray = citRefNodeText.split(/,\s*/);
                        var citationString = "";
                        citRefArray.forEach(function(entry) {
                        entry = entry.replace(/[\sa-z\,\.]+/gi, '');
                            if (/[0-9]\s*[\-\u2013\u2014]\s*[0-9]/.test(entry)){
                                var rangeArray = entry.split(/[\-\u2014\u2013]/);
                                var startRange = parseInt(rangeArray[0],10);
                                var endRange = parseInt(rangeArray[1],10);
                                for (var i = startRange; i <= endRange; i++) {
                                    if ((/^[0-9]+$/.test(i)) && ($(kriya.config.containerElm+' #'+ RIDType +i).length > 0)){
                                        citationString = citationString + " " + RIDType + i;
                                    }
                                }
                            }else{
                                if ((/^[0-9]+$/.test(entry)) && ($(kriya.config.containerElm+' #'+ RIDType +entry).length > 0)){
                                    citationString = citationString + " " + RIDType + entry;
                                }
                            }
                        });
                        $(citationNode).removeAttr('data-citation-string');
                        if (citationString != ''){
                            citationString = ' ' + citationString.replace(/^ /, '') + ' ';
                            $(citationNode).attr('data-citation-string', citationString); 
                        }
                    }else{
                        var etal = false;
                        if (/et al/.test(citRefNodeText)){
                            etal = true;
                        }
                        citRefNodeText = citRefNodeText.replace(/et al\.?|[\.\)\]\[\(]+/gi, '');
                        year = citRefNodeText.replace(/^.*? ([0-9a-z]+)$/i, '$1');
                        citRefNodeText = citRefNodeText.replace(/^(.*?) ([0-9a-z]+)$/i, '$1');
                        var secondAuthor = citRefNodeText.replace(/^(.*?) (and|\u0026) (.*?)$/g, '$3');
                        if (secondAuthor == citRefNodeText){
                            secondAuthor = false;
                        }
                        citRefNodeText = citRefNodeText.replace(/^(.*?) and.*?$/g, '$1');
                        author = citRefNodeText.replace(/^(.*?) ([0-9a-z]+)$/i, '$1');
                        author = author.replace(/^(.*?)\s?, (.*)$/, '$1');
                        author = author.replace(/^(.*?)\s?,/, '$1');
                        //author = author.replace(/^(.*?)\s.*$/, '$1');
                        var ref = $( kriya.config.containerElm+' .jrnlRefText').find('.RefYear:contains("' + year + '")');
                        if ($(ref).length == 0){
                            year = year.replace(/[a-z]+/gi, '');
                            ref = $( kriya.config.containerElm+' .jrnlRefText').find('.RefYear:contains("' + year + '")');
                        }    
                        if ($(ref).length == 0){console.log('Nothing Found')}    
                        if (year.length > 3){
							ref = $(ref).closest('.jrnlRefText').each(function(){
								var re = new RegExp(author.trim(), "gi");
								if (($(this).find('.RefSurName:first').text().match(re)) || ($(this).find('.RefCollaboration:first').text().match(re)) || ($(this).find('.RefAuthor:first').text().match(re))){
                                refId = $(this).attr('id');
                                if ((! secondAuthor) && (! etal) && ($(this).find('.RefAuthor,.RefCollaboration').length == 1)){
                                    citationNode.attr('data-citation-string', ' ' + refId + ' ');
                                }else if ((secondAuthor) || (etal)){
                                    citationNode.attr('data-citation-string', ' ' + refId + ' ');
                                }else{
                                    citationNode.attr('data-citation-string', ' ' + refId + ' ');
								}
                                console.log(refId);
                            }
							});
						}
                    }
                }else{
                    //match => Figure 1-figure supplement 1 || Figure 1-source data 1 || Figure 1-figure supplement 1-source data 1
                    if (/([0-9][a-z]*[\u2012\u2013\u2014\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?)([0-9\u2013\u2014\u2012a-z\,\-\s]{1,})(source data\s?[0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})?|([A-Za-z]+[\.\s]{1,})([0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})source data\s?([0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})/gi.test(citRefNodeText)){
                        if (/([0-9][a-z]*[\u2012\u2013\u2014\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?)([0-9\u2013\u2014\u2012a-z\,\-\s]{1,})(source data\s?[0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})/i.test(citRefNodeText)){
                        //match => Figure 1-figure supplement 1-source data 1
                            citRefID = citRefNodeText.replace(/^.*\s([0-9]+)([a-z]*[\u2012\u2013\u2014\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?)\s?([0-9]+)([a-z]?[\u2012\u2013\u2014\,\;\-\s]{1,}source data\s?)([0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})/, '$1');
                            citSubRefID = citRefNodeText.replace(/^.*\s([0-9]+)([a-z]*[\u2012\u2013\u2014\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?)\s?([0-9]+)([a-z]?[\u2012\u2013\u2014\,\;\-\s]{1,}source data\s?)([0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})/, '$4');
                            citRefID = citRefID.replace(/([a-z\s]+)/gi, '');
                            citPrefix = '-S' + citSubRefID + '-SD';
                            citaionIDString = citRefNodeText.replace(/^.*\s([0-9]+)([a-z]*[\u2012\u2013\u2014\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?)\s?([0-9]+)([a-z]?[\u2012\u2013\u2014\,\;\-\s]{1,}source data\s?)([0-9\u2012\u2013\u2014a-z\,\;\-\s]{1,})/gi, '$6');
                        }else{
                            //match => Figure 1-figure supplement 1 || Figure 1-source data 1
                            citRefID = citRefNodeText.replace(/([\-\u2012\u2013\u2014]+)(.*)$/, '');
                            citRefID = citRefID.replace(/([a-z\s]+)/gi, '');
                            if (/source data/i.test(citRefNodeText)){
                                citPrefix = '-SD';
                                citaionIDString = citRefNodeText.replace(/^[a-z\s]+([0-9][\u2013\u2012\u2014a-z\,\-\s]{1,})(source data) ([0-9\u2013a-z\,\-\s]{1,})/gi, '$3');
                            }else{
                                citPrefix = '-S';
                                citaionIDString = citRefNodeText.replace(/^[a-z\s]+([0-9][\u2013\u2012\u2014a-z\,\-\s]{1,})(figures? supplement[a-z]*|supplement[a-z]* figures?) ([0-9\u2013a-z\,\-\s]{1,})/gi, '$3');
                            }
                        }
                        citaionIDString = citaionIDString.replace(/\s?and\s?/gi, ', ');
                        citaionIDString = citaionIDString.replace(/([0-9])[A-Z]([\-\u2013\u2014\s]+[A-Z])?/gi, '$1');
                        citaionIDString = citaionIDString.replace(/[a-z\s\(\)\[\]]+/gi, '');
                        var citRefArray = citaionIDString.split(/[,;]\s*/);
                        var citationString = "";
                        var uniqueIDs = [];
                        $.each(citRefArray, function(i, el){
                            if($.inArray(el, uniqueIDs) === -1) uniqueIDs.push(el);
                        });
                        uniqueIDs.sort();
                        uniqueIDs.forEach(function(entry) {
                            entry = entry.replace(/[\sa-z]+/gi, '');
                            entry = entry.replace(/[\-\u2013\u2014]$/, '');
                            if (entry != ""){
                                if (/[0-9]\s*[\-\u2013\u2014]\s*[0-9]/.test(entry)){
                                    var rangeArray = entry.split(/[\-\u2014\u2013]/);
                                    var startRange = parseInt(rangeArray[0],10);
                                    var endRange = parseInt(rangeArray[1],10);
                                    for (var i = startRange; i <= endRange; i++) {
                                        citationString = citationString + " " + RIDType + citRefID + citPrefix + i;
                                    }
                                }else{
                                    citationString = citationString + " " + RIDType + citRefID + citPrefix + entry;
                                }
                            }
                        });
                    }else if (/([0-9\u2013a-z\,\-\s\.]{1,})/gi.test(citRefNodeText)){
                    //match => Figure 1 || Figure 1-3 || Figure 1A || Figure 1A-B
                        //citaionIDString = citRefNodeText.replace(/^.*([0-9]+[\u2013a-z\,\-\s\.]{1,})/gi, '$1');
                        citaionIDString = citRefNodeText.replace(/^([A-Z]+)\.?/gi, '');
                        citaionIDString = citaionIDString.replace(/\s?and\s?/gi, ', ');
                        citaionIDString = citaionIDString.replace(/([0-9])[A-Z]([\-\u2013\u2014\s]+[A-Z])?/gi, '$1');
                        citaionIDString = citaionIDString.replace(/[a-z]\-[a-z]/gi, '');
                        citaionIDString = citaionIDString.replace(/[\sa-z\(\)\[\]]+/gi, '');
                        citaionIDString = citaionIDString.replace(/([0-9]+\.)([0-9])/, '$1');
                        if (/appendix/i.test(citRefNodeText)){
                            var appRIDType = citaionIDString.replace(/^([A-Z]+)/gi, '');
                            appRIDType = appRIDType.replace(/^([0-9]+)\s?[\-\u2012\u2013\u2014]\s?[0-9]+.*$/, '$1');
                            citaionIDString = citaionIDString.replace(/^[0-9]+\s?[\-\u2012\u2013\u2014]\s?(.*)$/, '$1');
                            RIDType = 'A' + appRIDType + '-' + RIDType;
                        }
                        var citRefArray = citaionIDString.split(/[,;]\s*/);
                        var citationString = "";
                        var uniqueIDs = [];
                        $.each(citRefArray, function(i, el){
                            if($.inArray(el, uniqueIDs) === -1) uniqueIDs.push(el);
                        });
                        uniqueIDs.sort();
                        uniqueIDs.forEach(function(entry) {
                            entry = entry.replace(/[\sa-z]+/gi, '');
                            entry = entry.replace(/[\-\u2013\u2014]$/, '');
                            if (entry != ""){
                                if (/[0-9]\s*[\-\u2013\u2014]\s*[0-9]/.test(entry)){
                                    var rangeArray = entry.split(/[\-\u2014\u2013]/);
                                    var startRange = parseInt(rangeArray[0],10);
                                    var endRange = parseInt(rangeArray[1],10);
                                    for (var i = startRange; i <= endRange; i++) {
                                        if ((checkNode) && ($(kriya.config.containerElm+' #'+ RIDType + i).length > 0)){
                                            citationString = citationString + " " + RIDType + i;
                                        }else if (! checkNode){
                                            citationString = citationString + " " + RIDType + i;
                                        }
                                    }
                                }else{
                                    if ((checkNode) && ($(kriya.config.containerElm+' #'+ RIDType + i).length > 0)){
                                        citationString = citationString + " " + RIDType + entry;
                                    }else if (! checkNode){
                                        citationString = citationString + " " + RIDType + entry;
                                    }
                                }
                            }
                        });
                    }
                    $(citationNode).removeAttr('data-citation-string');
                    if ((citationString != '') && (citationString != undefined)){
                        citationString = ' ' + citationString.replace(/^ /, '') + ' ';
                        $(citationNode).attr('data-citation-string', citationString); 
                    }
                }
            },
            getSomeText: function(node,direction){
                var returnText = '';
                var count = 6;    
                if( direction == 'before' ){
                    var prevNode = $(node)[0].previousSibling;
                    while( prevNode ){
                        returnText = prevNode.textContent + returnText;
                        prevNode = prevNode.previousSibling;
                        var checkCount = returnText.match(/\s+/gi);
                        if( checkCount != null && checkCount > count + 2 ) {
                            prevNode = false;
                        }
                    }
                }else {
                    var nextNode = $(node)[0].nextSibling;
                    while( nextNode ){
                        returnText += nextNode.textContent;
                        nextNode = nextNode.nextSibling;
                        var checkCount = returnText.match(/\s+/gi);
                        if( checkCount != null && checkCount > count + 2 ) {
                            nextNode = false;
                        }
                    }
                }
                //returnText = returnText.trim().split(' ');
                returnText = returnText.split(' ');
                var lastIndex = returnText.length;
                if( returnText.length > count && direction == 'before' ) {
                    returnText = returnText.splice( lastIndex - count, lastIndex );
                }else if( returnText.length > count && direction == 'after' ){
                    returnText = returnText.splice( 0, count );
                }
                returnText = returnText.join(' ');
                return returnText;
            },
            
        };
        return commonjs;

    })(commonjs || {});
				

//window.addEventListener("load", function(e){if (typeof(kriyaEditor.settings.contentNode) != "undefined"){setTimeout(function(){kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;var windowHasFocus = true;window.addEventListener("blur", function(e){if (windowHasFocus){kriyaEditor.htmlContent = kriyaEditor.settings.contentNode.innerHTML;}windowHasFocus = false;});window.addEventListener("focus", function(e){if (! windowHasFocus){if (/preeditor|typesetter/i.test(kriya.config.content.role)){if (kriyaEditor.htmlContent != kriyaEditor.settings.contentNode.innerHTML){var d = ["Y", "o", "u", " ", "h", "a", "v", "e", " ", "c", "h", "a", "n", "g", "e", "d", " ", "t", "h", "e", " ", "c", "o", "n", "t", "e", "n", "t", " ", "o", "u", "t", "s", "i", "d", "e", " ", "o", "f", " ", "t", "h", "e", " ", "e", "d", "i", "t", "o", "r", " ", "(", "d", "e", "v", "e", "l", "o", "p", "e", "r", " ", "t", "o", "o", "l", "?", ")", ",", " ", "s", "o", " ", "p", "a", "g", "e", " ", "w", "i", "l", "l", " ", "b", "e", " ", "r", "e", "l", "o", "a", "d", "e", "d", "!"];alert(d.join(""));kriyaEditor.settings.contentNode.remove();delete kriya;window.location.reload();e.stopPropagation();}}}windowHasFocus = true;})},1000)}});