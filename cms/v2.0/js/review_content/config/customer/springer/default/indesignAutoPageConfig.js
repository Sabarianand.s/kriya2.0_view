var config = {
    "defaultUnits":"pt",
    "baseLeading":10,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "landscapeFloatOnFirstPage":false,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"
    ],
    "floatTypeOnFirstPage":"KEY",
    "minStackSpace":18,
    "placeStyle":"Sandwich",
    "figCaptionMinLines": 0,
    "minNoLinesOnPag":3,
    "applyTableLeftRightBorder":true,
    "applyTableBorderWidth":0.15,
    "applyTableBorderColor":'WHITE',
    "pageSize":{
        "width":603,
        "height":783
    },
    "exportPDFpreset":{
        "print":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":15,
      "right":15,
      "gutter":15
    },
    "geoBoundsVerso":[36,36,702,567
    ],
    "geoBoundsRecto":[36,639,702,1170
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1",
    "DEFAULT":"LAYOUT1",
    "EDITORIAL":"LAYOUT2"
    },
    "pageColumnDetails":{
        "LAYOUT1":{
            "openerPageColumnDetails":[
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":0},
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":15}
                ],
                "columnDetails":[
                  {"width":258, "height":666,"floatsCited":false,"gutter":0},
                  {"width":258, "height":666,"floatsCited":false,"gutter":15}
                ],
                "openerPageMargin":{
                "top":36,
                "bottom":81,
                "inside":169.667,
                "outside":36
                },
            "otherPageMargin":{
                 "top":36,
                "bottom":81,
                "inside":36,
                "outside":36
                }
            },
         "LAYOUT2":{
                "openerPageColumnDetails":[
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":0},
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":15}
                ],
                "columnDetails":[
                  {"width":258, "height":666,"floatsCited":false,"gutter":0},
                  {"width":258, "height":666,"floatsCited":false,"gutter":15}
                ],
                 "openerPageMargin":{
                 "top":36,
                "bottom":81,
                "inside":169.667,
                "outside":36
                },
            "otherPageMargin":{
                 "top":36,
                "bottom":81,
                "inside":111.013,
                "outside":36
                }
             },
         "LAYOUT3":{
                "openerPageColumnDetails":[
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":0},
                  {"width":191.167, "height":666,"floatsCited":false,"gutter":15}
                ],
                "columnDetails":[
                  {"width":258, "height":666,"floatsCited":false,"gutter":0},
                  {"width":258, "height":666,"floatsCited":false,"gutter":15}
                ],
                 "openerPageMargin":{
                 "top":36,
                "bottom":81,
                "inside":169.667,
                "outside":36
                },
            "otherPageMargin":{
                 "top":36,
                "bottom":81,
                "inside":36,
                "outside":36
                }
             }              
        },
    "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":345, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T,EQN-O,EQN-T,EQN-B,EQN",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B,EQN-O,EQN-T,EQN-B,EQN"
         },
        "decrease":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,QUOTE-T,EQN-O,EQN-T,EQN-B,EQN",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,QUOTE-B,EQN-O,EQN-T,EQN-B,EQN"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsHead,jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":30,
            "maximum":60
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],

    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlCRBR']", "frame-name":"CRBR", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCRBL']", "frame-name":"CRBL", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlCVBL']", "frame-name":"CVBL", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlCVBR']", "frame-name":"CVBR", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlTRBL']", "frame-name":"TRBL", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlTVBR']", "frame-name":"TVBR", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlMetaInfo']", "frame-name":"METAINFO", "action":"move", "styleOverride":null}
       ]       
     },
    "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,0"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"255,255,255"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,0"
                    }
                }            
            },
            "EDITORIAL":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,0"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,100"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,0"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"255,255,255"
                    },
                "HYPERLINK":{
                    "colourType":"process",
                    "colourMode":"RGB",
                    "colourValue":"0,0,0"
                    }
                }            
            }
        },
         "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "8px",
                    "font-family" : "'ITC Stone Sans Std', ITC Stone Sans Std"
                },
                "fontPath"  : "ITCStoneSansStd/ITC-Stone-Sans-Std-Medium.ttf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'ITC-Stone-Sans-Std-Semibold', ITC-Stone-Sans-Std-Semibold",
                    "font-size" : "8px",
                    "line-height" : "10px",
                    "padding" : "2px 0px 1px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-family" : "'ITC-Stone-Sans-Std-Medium', ITC-Stone-Sans-Std-Medium",
                    "font-size" : "8px",
                    "line-height" : "9px",
                    "padding" : "0px 0px 0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            },
            "charAlignCenter" : true
        },
        "equation" : {
          "default" : {
              "page":{
                  "size" : "603pt"
              },
              "font" : {
                  "size"    : "10pt",
                  "ledding" : "12pt",
                  "path"    : "/Galliard/",
                  "ext"     : "otf",
                  "bold"    : "Galliard-Bold",
                  "italic"  : "Galliard-Italic",
                  "bold_italic":"Galliard-Bold",
                  "main"    : "Galliard-Roman"
              }
           },
           "jrnlTblBlock" : {
              "page":{
                  "size" : "603pt"
              },
              "font" : {
                "size"    : "8pt",
                "ledding" : "9pt",
                "path"    : "/ITC-Stone-Sans-Std-Medium/",
                "ext"     : "otf",
                "bold"    : "ITC-Stone-Sans-Std-Bold",
                "italic"  : "ITC-Stone-Sans-Std-Medium-Italic",
                "bold_italic":"ITC-Stone-Sans-Std-Semibold-Italic",
                "main"    : "ITC-Stone-Sans-Std-Medium"
              }
           }
        },
    "stubColObj":{
        "top": {
            "STUB_COLUMN": false,
            "STUB_STMT": false
        },
        "bottom": {
            "METAINFO": true,
            "CROSSMARK": false,
            "RELATED_ARTICLE_INFO": true
        }
    }
}
this.config = config;