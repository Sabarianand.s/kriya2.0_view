/**
 * custom functions for Book View
 */
var currentTab = '.dashboardTabs.active ';
var unassignedArticlesList;
var param;
(function (eventHandler) {
	eventHandler.flatplan = {
		action: {
			/**
			*	Add new book to a customer
			*	Get information from the provided form and construct a XML
			*	Send to API to add it to the Project List and create metaXML
			**/
			addBook: function (param, targetNode) {
				var projectExisting = $('.projectlist li').map(function () { return $(this).attr('data-project'); }).get().filter(v => v);
				projectExisting = projectExisting ? projectExisting : [];
				var params = {};
				var projectName = '';
				$('.save-notice').removeClass('hide');
				// To validate the Add new Book Modal
				var firstEmptyField = false;
				$('#addProj input[data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (!firstEmptyField) {
								$(this).focus();
								firstEmptyField = true;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (firstEmptyField) {
					$('.save-notice').text('Unable to create Book');
					return false;
				}
				// create project name with some random number
				var projectName = $('#manuscriptContent #customerVal').text() + '.' + (Math.floor(Math.random() * 90000) + 10000);
				while ($('.projectList .project[data-project="' + projectName + '"]').length > 0) {
					projectName = $('#manuscriptContent #customerVal').text() + '.' + (Math.floor(Math.random() * 90000) + 10000);
				}
				$('#journal-id').val(projectName);
				var empty = false;
				params.XML = '<nodes>';
				$('#addProj [data-form-details]:not(.hide)').each(function () {
					if ($(this).closest('.hide').length > 0) {
						return true;
					}
					var value = $(this).val();
					// if(!value && !($(this).parent().hasClass('hidden') || $(this).closest('div.row').hasClass('hide'))) {
					// 	// 	showMessage({ 'text': eventHandler.messageCode.errorCode[502][7], 'type': 'error' }); empty=true;
					// }
					var formDetail = $(this).attr('data-form-details').split('||');
					var attr = ($(this).attr('data-form-details-attr') && formDetail[3]) ? eventHandler.flatplan.components.attr($(this).attr('data-form-details-attr').split('||')) : '';
					if (attr != "") {
						attr = ' ' + attr;
					}
					if ($(this).closest('[data-parent-name]').length > 0) {
						value = "";
						$(this).closest('[data-parent-name]').find('[data-name]').each(function () {
							if ($(this).val() != "" && $(this).val() != null) {
								var tagName = $(this).attr('data-name');
								value += '<' + tagName + '>' + $(this).val() + '</' + tagName + '>';
							}
						})
						var tagName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						value += '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					} else {
						var tagName = $(this).attr('data-name');
						value = '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					}
					var currentElement = $(this)
					if ($(this).attr('data-siblings-name') && $(this).attr('data-siblings-name') != '') {
						var siblings = $(this).attr('data-siblings-name').split(',')
						//var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
						siblings.forEach(function (sibling) {
							var siblingTagName = sibling
							var siblingValue = $(currentElement).find('option[value="' + currentElement.val() + '"]').attr(sibling)
							if (siblingTagName && siblingValue) {
								value += '<' + siblingTagName + attr + '>' + siblingValue + '</' + siblingTagName + '>';
							}
						})
					}
					var innerTag = $(this).attr('data-inner-tag') ? $(this).attr('data-inner-tag') : '';
					if ((value) && (value != null) && (formDetail.length >= 3)) {
						params.XML += '<node><id>' + $(this).attr('id') + '</id><section>' + formDetail[0] + '</section><xpath>' + formDetail[1] + '</xpath><tagname>' + formDetail[2] + '</tagname><value>' + value + '</value></node>';
					}
				});
				var customer = $('#manuscriptContent #customerVal').text();
				if (empty) return false;
				if ($('#addProj .kriya-version .btn.active input').attr('id') == 'non-kriya'){
					params.XML += '<node><id>version</id><section>//container-xml</section><xpath>//meta</xpath><tagname>version</tagname><value><version>non-kriya</version></value></node>';
				}
				params.XML += '<node><id>project-name</id><section>//container-xml</section><xpath>//meta</xpath><tagname>project-name</tagname><value><project-name>' + projectName + '</project-name></value></node>'
				params.XML += '</nodes>';
				params.addProj = {
					id: customer,
					project: {
						projectName,
						fullName: $('#bookTitle').val(),
						name: projectName,
						workflowTemplate:'/default/workflowTemplate.xml',
						styleTemplate:'/default/styleTemplate.xml',
						componentTemplate:'/config/default/_config_review_content.xml',
						jobTemplate:'/default/jobTemplate.xml',
						customCSS:'/css/review_content/customers/'+customer+'/content-style.css',
						tableSetterConfig: '/js/review_content/config/customers/'+customer+'/default/indesignAutoPageConfig.js',
						tableSetterCSS: '/css/table_setter/customers/'+customer+'/default/table-config.css'
					},
					projectName : projectName
				}
				// if the project name already Exists
				if (projectExisting.indexOf(params.addProj.projectName.trim().toLowerCase().replace(' ', '_')) > -1) {
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][3], 'type': 'error' });
					//$('.save-notice').text('Unable to create Book');
					var parameters = {notify : {}};
					parameters.notify.notifyType = 'error';
					parameters.notify.notifyText = 'Unable to create Book';
					eventHandler.common.functions.displayNotification(parameters);
					return false;
				}
				if($('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').length >0 && $('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').val() !=""){
					params.addProj.project["rulesets-config"] = $('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').val();
				}
				if($('#addProj [data-form-details]:not(.hide)[data-name="action-query"]').length >0 && $('#addProj [data-form-details]:not(.hide)[data-name="action-query"]').val() !=""){
					params.addProj.project["action-query"] = $('#addProj [data-form-details]:not(.hide)[data-name="action-query"]').val();
				}
				if (params.addProj.projectName && params.addProj.id) {
					$('.la-container').fadeIn();
					$.ajax({
						type: "POST",
						url: "/api/customerproject",
						data: params.addProj,
						success: function (data) {
							$('.la-container').fadeOut();
							//console.log(data);
							//$('.save-notice').text('Book Created');
							$.ajax({
								type:"POST",
								url:"/api/issuedata?customer="+ params.addProj.id + '&project=' + params.addProj.projectName + '&type=generateContainerXML&projectType=book',
								data:params,
								success:function(data){
									if(data.list && data.list.status==200){
										$('[type="text"][data-form-details],[type="text"][data-name]').val('');
										$('select[data-form-details] option[value=""]').attr('selected', true);
										$('.dropdown-menu .custom-select').removeClass('custom-select');
										$('#addProj .dynamic').remove();
										$('#addProj').modal('hide');
										//$('.save-notice').text('Book has been added');
										var parameters = {notify : {}};
										parameters.notify.notifyType = 'success';
										parameters.notify.notifyTitle = 'success';
										parameters.notify.notifyText = 'Book has been added';
										eventHandler.common.functions.displayNotification(parameters);
										$('.la-container').fadeOut();
										$('#manuscriptContent #filterCustomer .filter-list.active').trigger('click');
									}
									else{
										$('.la-container').fadeOut();
										var parameters = {notify : {}};
										parameters.notify.notifyType = 'error';
										parameters.notify.notifyText = 'Unable to create Book';
										eventHandler.common.functions.displayNotification(parameters);
										$('.save-notice').text('Unable to create Book on' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss')) ;
										//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
									}
								},
								error:function(err){
									$('.la-container').fadeOut();
									var parameters = {notify : {}};
									parameters.notify.notifyType = 'error';
									parameters.notify.notifyText = 'Unable to create Book';
									eventHandler.common.functions.displayNotification(parameters);
									$('.save-notice').text('Unable to create Book on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
									//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
								}
							});
						},
						error: function (data) {
							console.log(data);
							$('.la-container').fadeOut();
							var parameters = {notify : {}};
							parameters.notify.notifyType = 'error';
							parameters.notify.notifyText = 'Unable to create Book';
							eventHandler.common.functions.displayNotification(parameters);
							$('.save-notice').text('Unable to create Book on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
							//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
						},
					});
				}
				else {
					$('.la-container').fadeOut();
					var parameters = {notify : {}};
					parameters.notify.notifyType = 'error';
					parameters.notify.notifyText = 'Unable to create Book';
					eventHandler.common.functions.displayNotification(parameters);
					//$('.save-notice').text('Unable to create Book');
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][4], 'type': 'error' });
				}
			},
			/**
			*	Save Book Info details back to metaXML
			*	Get information from the provided form and construct a JSON
			*	Send to API to modify metaXML
			**/
			saveProj: function (param, targetNode) {
				var metaUpdate = false;
				var modifiedArray = [];

				// For validating the Book info form
				var firstEmptyField = false;
				$('#addProj input[data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (!firstEmptyField) {
								$(this).focus();
								firstEmptyField = true;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (firstEmptyField) {
					$('.save-notice').text('Unable to Save Details');
					return false;
				}

				$('#addProj [data-form-details]:not(.hide)').each(function () {
					if ($(this).closest('.hide').length > 0) {
						return true;
					}
					var myMeta = $('#manuscriptsData').data('binder')['container-xml'].meta;
					var key = $(this).attr('data-name');
					if ($(this).closest('[data-parent-name]').length > 0) {
						var tagName = $(this).closest('[data-parent-name]').attr('data-tag-name');
						var parentName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						if (modifiedArray.indexOf(tagName) < 0) {
							modifiedArray.push(tagName);
							myMeta[tagName] = {};
							myMeta[tagName][parentName] = [];
						}
						var newObj = {}
						var attributes = $(this).closest('[data-parent-name]').attr('data-form-details-attr').split('||');
						if (attributes.length > 0) {
							newObj._attributes = {}
							$(attributes).each(function (i, e) {
								v = e.split('|');
								if (v.length == 2) {
									newObj._attributes[v[0]] = v[1];
								}
							});
						}
						$(this).closest('[data-parent-name]').find('[data-name]').each(function () {
							if ($(this).val() != "" && $(this).val() != null) {
								var value = $(this).attr('data-name');
								newObj[value] = {};
								newObj[value]._text = $(this).val();
							}
						})
						myMeta[tagName][parentName].push(newObj);
					}
					else if ($('#addProj [data-form-details][data-name="' + key + '"]').length > 1) {
						if (modifiedArray.indexOf(key) < 0) {
							modifiedArray.push(key);
							myMeta[key] = [];
						}
						if ($(this).val() != '' && $(this).val() != null) {
							var newObj = {}
							newObj._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr').split('||');
							if (attributes.length > 0) {
								newObj._attributes = {}
								$(attributes).each(function (i, e) {
									v = e.split('|');
									if (v.length == 2) {
										newObj._attributes[v[0]] = v[1];
									}
								});
							}
							myMeta[key].push(newObj);
						}
					}
					else {
						if ($(this).val() == '' || $(this).val() == null) {
							if (myMeta[key]) {
								delete (myMeta[key])
							}
						}
						else if (myMeta[key]) {
							myMeta[key]._text = $(this).val();
						}
						else {
							myMeta[key] = {};
							myMeta[key]._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr') ? $(this).attr('data-form-details-attr').split('||') : '';
							if (attributes && attributes.length > 0) {
								myMeta[key]._attributes = {}
								$(attributes).each(function (i, e) {
									v = e.split('|');
									if (v.length == 2) {
										myMeta[key]._attributes[v[0]] = v[1];
									}
								});
							}
						}
					}
					//
					var current = $(this)
					if ($(this).attr('data-siblings-name') && $(this).attr('data-siblings-name') != '') {
						var siblings = $(this).attr('data-siblings-name').split(',')
						var attributes = $(this).attr('data-form-details-attr') ? $(this).attr('data-form-details-attr').split('||') : '';
						siblings.forEach(function (sibling) {
							//console.log(sibling)
							if ($(current).find('option[value="' + current.val() + '"]').length > 0 && $(current).find('option[value="' + current.val() + '"]').attr(sibling) && $(current).find('option[value="' + current.val() + '"]').attr(sibling) != '') {
								myMeta[sibling] = {};
								myMeta[sibling]._text = $(current).find('option[value="' + current.val() + '"]').attr(sibling);
								if (attributes && attributes.length > 0) {
									myMeta[sibling]._attributes = {}
									$(attributes).each(function (i, e) {
										v = e.split('|');
										if (v.length == 2) {
											myMeta[sibling]._attributes[v[0]] = v[1];
										}
									});
								}
							}

						})
					}
				});
				eventHandler.flatplan.action.saveData();
				eventHandler.flatplan.action.updateprjElastic();
				$('#addProj').modal('hide');
			},

			updateprjElastic: function (params, targetNode) {
				if(!params){
					var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
					var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
					var params = {};
					params.addProj = {
						id: customer,
						project: {
							project,
							fullName: $('#bookTitle').val(),
							name: project,
							workflowTemplate:'/default/workflowTemplate.xml',
							styleTemplate:'/default/styleTemplate.xml',
							componentTemplate:'/config/default/_config_review_content.xml',
							jobTemplate:'/default/jobTemplate.xml',
							customCSS:'/css/review_content/customers/'+customer+'/content-style.css',
							tableSetterConfig: '/js/review_content/config/customers/'+customer+'/default/indesignAutoPageConfig.js',
							tableSetterCSS: '/css/table_setter/customers/'+customer+'/default/table-config.css'
						},
						projectName : project
					}
				}
				if($('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').length >0){
					if($('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').val() !=""){
						params.addProj.project["rulesets-config"] = $('#addProj [data-form-details]:not(.hide)[data-name="rulesets-config"]').val();
					}else{
						params.addProj.project["rulesets-config"] = "CustomerRules";
					}
				}
				$.ajax({
					type: "POST",
					url: "/api/customerproject",
					data: params.addProj,
					success: function (data) {
						console.log('Elastic updated')
					},
					error:function(err){
						console.log('Elastic update failed')
					}
				});
			},
			/**
			*	Add new chapter or any other part of a book
			*	Send uploaded Zip to AddJob API
			**/
			addJob: function (param, targetNode) {
				var parameters = new FormData();
				var client = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var doiSuffix = parseInt($('#chapterNumber').val());
				doiSuffix = ("000" + doiSuffix).slice(-3)
				var doi = project + '.' + doiSuffix;
				// check if already doi exists
				/*if ($('.tocBodyContainer .sectionDataChild[data-doi="' + doi + '"]').length > 0){
					// show notification
					return false;
				}*/
				// var typeOfJob = 'chapter';//$('.tab-pane.fade.active.in').attr('id');
				var typeOfJob = $('.tab-pane.active').attr('id');
				var validated = true;
				var uploadFileId = '#' + typeOfJob + ' #chapFile';
				// Form validation
				$('#addChapter #' + typeOfJob + ' [data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (validated) {
								$(this).focus();
								validated = false;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (!validated) {
					return false;
				}
				if (!($(uploadFileId)[0] && $(uploadFileId)[0].files && $(uploadFileId)[0].files.length > 0 && ($(uploadFileId).attr('accept') == '.zip' && /zip/.test($(uploadFileId)[0].files[0].type)) || ($(uploadFileId).attr('accept') == '.epub' && $(uploadFileId)[0].files[0].type == "application/epub+zip"))) {
					$(uploadFileId).addClass('is-invalid');
					return false;
				}
				if (typeOfJob == "bulkUpload"){
					var JZip = new JSZip();
					JZip.loadAsync($(uploadFileId)[0].files[0])
						.then(function(zip) {
							var zipFolders = {};
							var zipFiles = [];
							var client = $('#filterCustomer .filter-list.active').attr('value');
							var project = $('#filterProject .filter-list.active').attr('value');
							var folderNameReg = new RegExp('^' + project + '.[0-9]+$')
							zip.forEach(function (relativePath, zipEntry) {  // 2) print entries
								if (/^(.*?)\/.*$/, zipEntry.name){
									var folderName = zipEntry.name.replace(/^(.*?)\/.*$/, '$1');
									if (!zipFolders[folderName]){
										zipFolders[folderName] = 'valid'
										if (! folderNameReg.test(folderName)){
											zipFolders[folderName] = 'invalid';
										}
									}
								}else{
									zipFolders[zipEntry.name] = 'invalidfile';
									console.log('File:', zipEntry.name)
								}
							});
							$('#addMultipleJobs .modal-body table tbody').html('')
							var validZip = true;
							for (var f in zipFolders){
								var validClass = "valid";
								if (/invalid/.test(zipFolders[f])){
									validClass = "invalid"
								}
								var tr = '<tr data-doi="' + f + '" data-valid="'+ validClass + '"><td>' + f + '</td><td width="55%" class="statusBar">';
								if (zipFolders[f] == 'invalid'){
									tr += 'Folder name is invalid';
									validZip = false;
								}
								if (zipFolders[f] == 'invalidfile'){
									tr += 'Files are not allowed inside the zip';
									validZip = false;
								}
								tr += '</td></tr>';
								$('#addMultipleJobs .modal-body table tbody').append(tr);
							}
							$('#addMultipleJobs').modal();
							if (validZip){
								var parameters = new FormData();
								parameters.append('manuscript', $(uploadFileId)[0].files[0]);
								parameters.append('customer', client);
								parameters.append('project', project);
								eventHandler.flatplan.action.addMultipleJobs(parameters)
							}
						}, function (e) {
							console.log(e)
						});
						return false;
				}
				$('.la-container').fadeIn();
				var parameters = new FormData();
				parameters.append('manuscript', $(uploadFileId)[0].files[0]);
				if (typeOfJob == 'epubUpload') {
					// do not appened any parameter
					type = 'epub';
				} else if (typeOfJob == 'chapter') {
					chapterTitle = $('#chapterTitle').val();
					parameters.append('articleTitle', chapterTitle);
					chapterNumber = parseInt($('#chapterNumber').val()).toString();
					parameters.append('articleNumber', chapterNumber);
					type = 'chapter'
					parameters.append('articleType', type);
				} else {
					chapterTitle = 'Back Matter';
					chapterNumber = '';
					parameters.append('articleTitle', chapterTitle);
					type = $('#' + typeOfJob + ' #backType').val();
					parameters.append('articleType', type);
					doi = project + '.' + type;
				}

				parameters.append('customer', client);
				parameters.append('project', project);
				parameters.append('doi', doi);

				parameterToAddJobBook = {
					customer: client,
					project: project,
					doi: doi,
					chapterTitle: chapterTitle,
					chapterNumber: chapterNumber,
					type: type
				}
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						if (response) {
							if (type == 'epub') {
								/* var res = $(response);
								var chapterItems = res.find('manifest item[media-type="application/xhtml+xml"]');
								if (chapterItems.length == 0){

								}else{
									$(chapterItems).each(function(){
										var tr = $('<tr/>');
										tr.append('<td>' + $(this).attr('id') + '</td>');
										tr.append('<td>' + $(this).attr('href') + '</td>');
										tr.append('<td><span class="btn-primary btn-sm btn-info" data-channel="menu" data-topic="bookview" data-event="click" data-message="{\'funcToCall\': \'addEpubJob\'}">Add Job</span></td>');
										$('#addEpubJobs .modal-body table tbody').append(tr);
									});
									$('#addChapter').modal('hide');
									$('#addEpubJobs').modal('show');
								} */
								if (response.content && response.content.length) {
									$('#manuscriptsData').data('binder')['container-xml'].contents = response;
									eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#filterProject .active').trigger('click')" });
									$('#addChapter').modal('hide');
								}
							}
							else {
								var newCahpter = {};
								newCahpter.title = { '_text': chapterTitle };
								newCahpter._attributes = { "section": "Chapters", "type": "manuscript", "chapterNumber": chapterNumber, "id": doi, "id-type": "doi", "grouptype": "body", "siteName": "kriya2.0","pageExtent":"1" }
								var chapters = $('#manuscriptsData').data('binder')['container-xml']['contents'].content;
								// to add chapters to the end of body content and not add after any back content - JAI 09-Aug-2019
								var chapterAdded = false;
								for (var c = (chapters.length -1); c >= 0; c--){
									if (chapters[c]._attributes && chapters[c]._attributes['grouptype'] && chapters[c]._attributes['grouptype'] == 'body'){
										var currentIndex = c + 1;
										chapters.splice(currentIndex, 0, newCahpter);
										chapterAdded = true;
										break;
									}
								}
								if (!chapterAdded){
									$('#manuscriptsData').data('binder')['container-xml']['contents'].content.push(newCahpter);
								}
								eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#filterProject .active').trigger('click')" });
								$('#addChapter').modal('hide');
							}
							$('.la-container').css('display', 'none');
						} else {
							var parameters = { notify: { notifyType: 'error', notifyTitle: 'Unable to AddJob ' , notifyText: eventHandler.flatplan.errorCode[501][4] + ' ' + eventHandler.flatplan.errorCode[503][1]}};
							eventHandler.common.functions.displayNotification(parameters);
							//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
							$('.la-container').css('display', 'none');
							$('#addChapter').modal('hide');
						}
					},
					error: function (err) {
						$('#addChapter').modal('hide');
						if(err&& err.responseJSON && err.responseJSON.status && err.responseJSON.status.message){
							var parameters = { notify: { notifyType: 'error', notifyTitle: 'Unable to AddJob ' , notifyText: err.responseJSON.status.message } };
							eventHandler.common.functions.displayNotification(parameters);
						}
						console.log(err);
						$('.la-container').css('display', 'none');
					}
				})
			},
			addMultipleJobs: function(param, targetNode){
				console.log('Inside add multiple jobs');
				return;
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: param,
					contentType: false,
					processData: false,
					success: function (response) {
						if (response) {

						}else{

						}
					},
					error: function(err){

					}
				})
			},
			/**
			*	Get project list/issue list of a customer
			*	Construct html list based on customer type (book or journal)
			*	Either list of Books or list of issues available in a journal
			**/
			getBooks: function (param, targetNode) {
				var url = '/api/projects?customer=' + param.customer;
				if ($('.save-notice').text().match(/All changes saved/gi)) {
					$('.save-notice').addClass('hide');
				}
				$.ajax({
					url: url,
					type: 'GET',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (project) {
						$('.la-container').css('display', 'none');
						if (typeof (project) == 'undefined' || project == null) {
							return false;
						}
						if (project[0] == undefined && project[0]._source == undefined && project[0]._source.projects == undefined) {
							return false;
						}
						//	var pData = project['project'];
						var pData = project[0]._source.projects;
						if (pData.constructor.toString().indexOf("Array") == -1) {
							pData = [pData];
						}
						var pHTML = $('<ul class="projectList" style="margin-left:10%;margin-right:10%">');
						var pLen = pData.length;
						var cName = param.customer;
						var data = {};
						data.customer = param.customer;
						data.projects = pData;
						// var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
						// $('#manuscriptsDataContent').html(pagefn(data));
						// return;
						var count = 0;
						var extra = {};
						var projectHTML = '';
						for (var pIndex = 0; pIndex < pLen; pIndex++) {
							projectHTML += '<div class="filter-list" value="' + pData[pIndex].name + '" data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'getMetaXML\',\'param\':{\'type\': \'book\'}}" data-customer="' + param.customer + '" data-project="' + pData[pIndex].name + '" data-file="' + pData[pIndex].name + '.xml">' + pData[pIndex].fullName + '</div>'
							$('.add').removeAttr("data-target");
							$('.add').removeClass("hide");
							var urlToFetchData = '/api/issuedata?customer=' + param.customer + '&project=' + pData[pIndex].name + '&fileName=' + pData[pIndex].name + '.xml&type=fetchIssueXML';
							$.ajax({
								url: urlToFetchData,
								type: 'POST',
								contentType: "application/json; charset=utf-8",
								dataType: "json",
								success: function (issueResponse) {
									count++;
									if (typeof (issueResponse) == "undefined" || issueResponse == null) {
										$('.la-container').fadeOut();
									} else if (issueResponse.list.response.issue) {
										var containerXml = issueResponse.list.response.issue['container-xml'];
										if (containerXml && containerXml.contents && containerXml.contents.content) {
											var content = containerXml.contents.content;
										} else {
											var content = [];
										}
										if (content.constructor.toString().indexOf('Array') < 0) {
											content = [content];
										}
										var chapterCount = 0;
										for (eachContent of content) {
											if (eachContent._attributes.type == 'manuscript') {
												chapterCount++;
											}
										}
										var isbnHb = 'NA';
										var isbnPb = 'NA';
										if (!containerXml.meta) {

										} else if (!containerXml.meta['project-name'] || !containerXml.meta['project-name']._text) {

										} else {
											var journalID = containerXml.meta['project-name']._text;
											if (containerXml.meta && containerXml.meta['isbn']) {
												var isbnDetails = [];
												if (containerXml.meta['isbn'].constructor.toString().indexOf("Array") == -1) {
													isbnDetails = [containerXml.meta['isbn']]
												} else {
													isbnDetails = containerXml.meta['isbn']
												}
												for (var is = 0, isl = isbnDetails.length; is < isl; is++) {
													var currIsbn = isbnDetails[is]
													if (currIsbn._attributes && currIsbn._attributes['pub-type'] && currIsbn._attributes['pub-type'] == 'pb') {
														isbnPb = currIsbn._text;
													}
													if (currIsbn._attributes && currIsbn._attributes['pub-type'] && currIsbn._attributes['pub-type'] == 'eisbn') {
														isbnHb = currIsbn._text;
													}
												}
											}
											var authors = '';
											if (containerXml.meta && containerXml.meta['contrib-group'] && containerXml.meta['contrib-group'].contrib) {
												var authorArray = containerXml.meta['contrib-group'].contrib;
												if (containerXml.meta['contrib-group'].contrib.constructor.toString().indexOf("Array") == -1) {
													authorArray = [containerXml.meta['contrib-group'].contrib]
												}
												authorArray.forEach(function (author, authorIndex) {
													if (author.name && author.name._text) {
														if (authorIndex == authorArray.length - 1) {
															authors += author.name._text;
														} else {
															authors += author.name._text + ', ';
														}
													} else {
														authors += '';
													}
												})
											}
											extra[journalID] = {
												'chapterCount': chapterCount.toString(),
												'pageCount': (containerXml.meta['page-count'] && containerXml.meta['page-count']._text && containerXml.meta['page-count']._text != 'null') ? containerXml.meta['page-count']._text : '0',
												'isbnHb': isbnHb,
												'isbnPb': isbnPb,
												'authors': authors
											}
											if (containerXml.meta && containerXml.meta['version'] && containerXml.meta['version']._text == "non-kriya"){
												extra[journalID]['offline'] = 'true';
											}
										}
									}
									if (count == pLen) {
										data.additionalDetails = extra;
										var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
										$('#manuscriptsDataContent').html(pagefn(data));
									}
								},
								error: function (error) {
									console.log(error);
									count++;
									if (count == pLen) {
										data.additionalDetails = extra;
										var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
										$('#manuscriptsDataContent').html(pagefn(data));
									}
								}
							});
						}
						$('#manuscriptContent #filterProject').html(projectHTML);
					},
					error: function (res) {

					}
				});
			},
			/**
			*	Get meta/issue xml of a book or an issue
			*	Construct TOC based on the template available for that customer
			**/
			getMetaXML: function (param, targetNode) {
				param = param;
				//if data binder availbale and doi not equal to current target node attr file, log ogf\\
				if($('#manuscriptsData').data('binder') && $('#manuscriptsData').data('currentIssueUserLog') && $('#manuscriptsData').data('currentIssueUserLog')!=''){
					if($('#manuscriptsData').data('currentIssueUserLog') && $('#manuscriptsData').data('currentIssueUserLog')._attributes && $('#manuscriptsData').data('currentIssueUserLog')._attributes['data-doi'] != param.issueId){
							//log-off current issue
						eventHandler.flatplan.action.logOffUser({'issueData':$('#manuscriptsData').data('binder')});
					}
				}
				$('.showEmpty').hide();
				$('.save-notice').addClass('hide');
				$('.issue-Lock-screen').addClass('hidden');
				$('.issue-Lock-screen').css('display','none');
				$('.issue-Lock-screen .issueLocked,.issue-Lock-screen .issueInactivePage').addClass('hide');
				if (param.type == 'journal') {
					var url = '/api/issuedata?customer=' + param.customer + '&project=' + param.projectName + '&fileName=' + param.issueId + '&type=fetchIssueXML&projectType=' + param.type + '&xpath=//container-xml';
				} else {
					var url = '/api/issuedata?customer=' + targetNode.attr('data-customer') + '&project=' + targetNode.attr('data-project') + '&fileName=' + targetNode.attr('data-file') + '&type=fetchIssueXML&projectType=' + param.type + '';
					$('.showBreadcrumb[data-type="all"]').css('display', 'inline-block');
					var param = {};
					$('.save-notice').addClass('hide');
					if ($('.booksList [data-project]').length > 0) {
						//eventHandler.filters.populateNavigater(param);
						$('#filterProject .filter-list:not([value])').remove();
						$('#filterProject .filter-list').removeAttr('onclick');
						$('#filterProject .filter-list').attr('data-channel', "flatplan").attr('data-topic', "action").attr('data-event', "click").attr('data-message', "{'funcToCall': 'getMetaXML','param':{'type': 'book'}}");
						$('#filterProject .filter-list').each(function () {
							$(this).attr('data-customer', targetNode.attr('data-customer')).attr('data-project', $(this).attr('value')).attr('data-file', $('.booksList [data-project="' + $(this).attr('value') + '"]').attr('data-file'));
						})
					}
					$('.bookLogOff').removeClass('hidden').addClass('active');
					$('#filterProject .filter-list').removeClass('active');
					$('#filterProject .filter-list[value="' + targetNode.attr('data-project') + '"]').addClass('active');
					$('#manuscriptContent #projectVal').text((targetNode.find('.msTitle b').length) ? targetNode.find('.msTitle b').text() : targetNode.text());
				}
				// param.customer = $('#manuscriptContent #customerVal').text();
				//param.customer = $().find('#customer span').text();
				$('.la-container').css('display', '');
				$.ajax({
					url: url,
					type: 'POST',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (issueResponse) {
						if (typeof (issueResponse) == "undefined" || issueResponse == null) {
							$('.la-container').fadeOut();
							var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to find '+param.type, notifyText : eventHandler.flatplan.errorCode[500][1]}};
							eventHandler.common.functions.displayNotification(parameters);
							return false;
						}
						if (typeof (issueResponse.list) == "undefined") {
							$('.la-container').fadeOut();
							var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to find '+param.type, notifyText : eventHandler.flatplan.errorCode[500][1]}};
							eventHandler.common.functions.displayNotification(parameters);
							return false;
						}
						if (issueResponse.list && issueResponse.list.response && issueResponse.list.response.status && issueResponse.list.response.status._text && issueResponse.list.response.status._text == '500') {
							$('.la-container').fadeOut();
							var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to find '+param.type, notifyText : eventHandler.flatplan.errorCode[500][1]}};
							eventHandler.common.functions.displayNotification(parameters);
							return false;
						}
						var issueData = issueResponse.list.response;
						var issueWorkFlowData = issueData.issue['container-xml'].workflow;
						//check for workflow
						if(issueData.issue['container-xml'].workflow){
							if(issueData.issue['container-xml'].workflow.stage && issueData.issue['container-xml'].workflow.stage['job-logs']){
								//
								var currentJobLog = issueData.issue['container-xml'].workflow.stage['job-logs'];
								var issueLog = issueData.issue['container-xml'].workflow.stage['job-logs'].log;
								if (issueLog && issueLog.constructor.toString().indexOf("Array") < 0) {
									issueData.issue['container-xml'].workflow.stage['job-logs'].log = [issueData.issue['container-xml'].workflow.stage['job-logs'].log];
									issueLog = issueData.issue['container-xml'].workflow.stage['job-logs'].log;
								}
								else if(!issueLog){
									issueLog=[];
									issueData.issue['container-xml'].workflow.stage['job-logs'].log=issueLog;
								}
								var issueLoglen = issueLog.length;
								if(issueLoglen == 0){
									//add as new user
									var d = new Date();
									var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
									var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
									var curentIssueLog =  ({ "_attributes": { "data-doi": targetNode.attr('data-file'), "data-project": targetNode.attr('data-project'), "data-customer": targetNode.attr('data-customer')},"usename":{"_text":userData.kuser.kuname.first },"useemail":{"_text":userData.kuser.kuemail},"stat-date":{"_text":currDate},"end-date":{"_text":currDate},"stat-time":{"_text":currTime},"end-time":{"_text":currTime},"status":{"_text":"open"} })
									 issueLog.push(curentIssueLog)
									 $('#manuscriptsData').data('currentIssueUserLog',curentIssueLog);
								}
								//reject some other user has logged in with status open or active and time diff is less than 7 min
								else if(issueLog[issueLoglen-1].useemail._text != userData.kuser.kuemail && issueLog[issueLoglen-1].status &&(issueLog[issueLoglen-1].status._text == 'open' || issueLog[issueLoglen-1].status._text=='active')){
									var lastEndDate = issueLog[issueLoglen-1]['end-date']
									if(issueLog[issueLoglen-1]['end-date']._text){
										lastEndDate = issueLog[issueLoglen-1]['end-date']._text
									}
									var lastEndTime = issueLog[issueLoglen-1]['end-time']
									if(issueLog[issueLoglen-1]['end-time']._text){
										lastEndTime = issueLog[issueLoglen-1]['end-time']._text
									}
									var lastEndDateTime = new Date(lastEndDate + ' ' + lastEndTime)
									var d = new Date();
									var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
									var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
									var currDateTime = new Date(currDate + ' ' + currTime);
									var timeDiff = Math.abs(currDateTime - lastEndDateTime)
									var minDiff = Math.floor((timeDiff/1000)/60);

									if(minDiff > 7){
										//set the last logged in user as in-active
										issueLog[issueLoglen-1]['status']='in-active';
											//new user add new log
										var curentIssueLog =  ({ "_attributes": { "data-doi": targetNode.attr('data-file'), "data-project": targetNode.attr('data-project'), "data-customer": targetNode.attr('data-customer')},"usename":{"_text":userData.kuser.kuname.first },"useemail":{"_text":userData.kuser.kuemail},"stat-date":{"_text":currDate},"end-date":{"_text":currDate},"stat-time":{"_text":currTime},"end-time":{"_text":currTime},"status":{"_text":"open"} })
										issueLog.push(curentIssueLog)
										$('#manuscriptsData').data('currentIssueUserLog', curentIssueLog);

										$( + '.issue-Lock-screen').addClass('hidden');
										$( + '.issue-Lock-screen').css('display','none');
										$('.issue-Lock-screen .issueLocked,.issue-Lock-screen .issueInactivePage').addClass('hide');
									}
									else{
										$( + '.issue-Lock-screen').removeClass('hidden');
										$( + '.issue-Lock-screen').css('display','block');
										$( + '.issue-Lock-screen').find('.lockUsermail').html(issueLog[issueLoglen-1].usename._text);
										$( + '.issueLocked').removeClass('hide');
										$('.issueViewButton').addClass('hidden');
										$('.la-container').fadeOut();
										return false;
									}
								}
								//same user with status open or in-active (when refreshed)
								else if(issueLog[issueLoglen-1].useemail._text == userData.kuser.kuemail && issueLog[issueLoglen-1].status && (issueLog[issueLoglen-1].status._text == 'open' || issueLog[issueLoglen-1].status._text == 'active' || issueLog[issueLoglen-1].status._text == 'in-active')){
									//if time diff is > 15 add new log else change the endtime
										var lastEndDate = issueLog[issueLoglen-1]['end-date']
										if(issueLog[issueLoglen-1]['end-date']._text){
											lastEndDate = issueLog[issueLoglen-1]['end-date']._text
										}
										var lastEndTime = issueLog[issueLoglen-1]['end-time']
										if(issueLog[issueLoglen-1]['end-time']._text){
											lastEndTime = issueLog[issueLoglen-1]['end-time']._text
										}
										var lastEndDateTime = new Date(lastEndDate + ' ' + lastEndTime)
										var d = new Date();
										var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
										var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
										var currDateTime = new Date(currDate + ' ' + currTime);
										var timeDiff = Math.abs(currDateTime - lastEndDateTime)
										var minDiff = Math.floor((timeDiff/1000)/60);

										if(minDiff >15){
											issueLog[issueLoglen-1]['status']='in-active';
											var curentIssueLog =  ({ "_attributes": { "data-doi": param.issueId, "data-project": param.projectName, "data-customer": param.customer},"usename":{"_text":userData.kuser.kuname.first },"useemail":{"_text":userData.kuser.kuemail},"stat-date":{"_text":currDate},"end-date":{"_text":currDate},"stat-time":{"_text":currTime},"end-time":{"_text":currTime},"status":{"_text":"open"} })
											issueLog.push(curentIssueLog)
											$('#manuscriptsData').data('currentIssueUserLog', curentIssueLog);
										}
										else{
											issueLog[issueLoglen-1]['status'] = 'open';
											issueLog[issueLoglen-1]['end-time']=currTime;
											issueLog[issueLoglen-1]['end-date'] = currDate;
											$('#manuscriptsData').data('currentIssueUserLog', 	issueLog[issueLoglen-1]);
										}
										$( + '.issue-Lock-screen').addClass('hidden');
										$( + '.issue-Lock-screen').css('display','none');
										$('.issue-Lock-screen .issueLocked,.issue-Lock-screen .issueInactivePage').addClass('hide');

								}
								else{
										//add as new user
									var d = new Date();
									var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
									var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
									var curentIssueLog =  ({ "_attributes": { "data-doi": param.issueId, "data-project": param.projectName, "data-customer": param.customer},"usename":{"_text":userData.kuser.kuname.first },"useemail":{"_text":userData.kuser.kuemail},"stat-date":{"_text":currDate},"end-date":{"_text":currDate},"stat-time":{"_text":currTime},"end-time":{"_text":currTime},"status":{"_text":"open"} })
									 issueLog.push(curentIssueLog)
									$('#manuscriptsData').data('currentIssueUserLog', curentIssueLog);
									//new user add new log
									$( + '.issue-Lock-screen').addClass('hidden');
									$( + '.issue-Lock-screen').css('display','none');
									$('.issue-Lock-screen .issueLocked,.issue-Lock-screen .issueInactivePage').addClass('hide');
								}
							}
							else{
								//to handle
							}
						}
						else{
							//add workflow
						  //var workflow = ({'_attributes'})
							//new user
							var d = new Date();
							var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
							var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
							issueWorkFlowData = ( {"stage":{"name":{"_text":"issueMakeup"},"job-logs":{"log":{ "_attributes": { "data-doi": targetNode.attr('data-file'), "data-project": targetNode.attr('data-project'), "data-customer": targetNode.attr('data-customer')},"usename":{"_text":userData.kuser.kuname.first },"useemail":{"_text":userData.kuser.kuemail},"stat-date":{"_text":currDate},"end-date":{"_text":currDate},"stat-time":{"_text":currTime},"end-time":{"_text":currTime},"status":{"_text":"open"} }}}});
							issueData.issue['container-xml'].workflow = issueWorkFlowData
							$('#manuscriptsData').data('currentIssueUserLog',issueWorkFlowData.stage["job-logs"].log);
							$( + '.issue-Lock-screen').addClass('hidden');
							$( + '.issue-Lock-screen').css('display','none');
							$('.issue-Lock-screen .issueLocked,.issue-Lock-screen .issueInactivePage').addClass('hide');
						}

						$('#addJobBtn').removeClass('hidden');
						$('#addBookBtn').addClass('hidden');

						var configData = JSON.parse(JSON.stringify(issueData.issue.config));
						var proofConfigDetails;
						if (issueData.issue["review-config"] && issueData.issue["review-config"]["proof-details"]) {
							proofConfigDetails = issueData.issue["review-config"]["proof-details"];
						}
						if (issueData.issue["review-config"]) {
							delete (issueData.issue["review-config"]);
						}
						delete (issueData.issue.config);
						$('#manuscriptsData').data('binder', issueData.issue);
						$('#manuscriptsData').data('config', configData);
						$('#manuscriptsData').data('profConfigDetails', proofConfigDetails);
						issueData = issueData.issue;
						if (typeof (issueData['container-xml']['contents']['content']) == 'undefined') {
							issueData['container-xml']['contents']['content'] = [];
						}
						else if (issueData['container-xml']['contents']['content'].constructor.toString().indexOf("Array") == -1) {
							issueData['container-xml']['contents']['content'] = [issueData['container-xml']['contents']['content']];
						}
						if (typeof (issueData['container-xml']['change-history']) == 'undefined') {
							issueData['container-xml']['change-history'] = [];
						}
						else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
							issueData['container-xml']['change-history'] = [issueData['container-xml']['change-history']];
						}
						eventHandler.flatplan.components.populateBookMeta();
						var issueContent = issueData['container-xml'];
						if (!issueContent.contents.content) {
							$('#manuscriptsDataContent').html('');
							//eventHandler.components.actionitems.showRightPanel({ 'target': 'history' })
							return;
						}
						var customerType = 'book';
						$('#addBookBtn').addClass('hidden');
						if (param.type == 'journal') {
							$('#addJobBtn').addClass('hidden');
							customerType = 'issue';
						}
						eventHandler.flatplan.components.populateSectionCards(targetNode, customerType,param);
						//eventHandler.flatplan.components.loadUnassignedArticles(targetNode.attr('data-customer'), targetNode.attr('data-project') );

						eventHandler.flatplan.components.populateLayouts(param.customer, param.projectName);
						var articleList = issueContent.contents.content;
						var al = articleList.length;
						var doiString = [];
						var sectionItems = [];
						var sectionNames = [];
						var sn = -1;
						for (var a = 0; a < al; a++) {
							if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id) {
								doiString.push(articleList[a]._attributes.id);
							}
							if (articleList[a]._attributes['section'] && articleList[a]._attributes.id) {
								var sectionName = articleList[a]._attributes['section'];
								if (!sectionNames[sectionName]) {
									sectionNames[sectionName] = [];
									sn++;
									sectionItems[sn] = { 'name': sectionName, 'item': [] }
								}
								sectionItems[sn]['item'].push(articleList[a])
							}
						}
						var data = {}
						data.customer = param.customer;
						data.project = param.projectName;
						data.fileName = param.issueId;
						data.items = sectionItems;
						data.chaptersCount = doiString.length;
						//eventHandler.components.actionitems.showRightPanel({ 'target': 'history' })
						eventHandler.flatplan.components.populateHistory();
						//eventHandler.components.actionitems.showRightPanel({ 'target': 'article-unassigned' })
						//eventHandler.flatplan.components.loadUnassignedArticles(targetNode.attr('data-customer'), targetNode.attr('data-project'));
						if (param.type == 'journal') {
							var dotTemplate = 'journalIssueTemplate';
							eventHandler.flatplan.issue.populateMetaContainer();
							eventHandler.flatplan.components.loadUnassignedArticles(data);
						} else {
							var dotTemplate = 'issueTemplate';
						}
						$('#manuscriptsDataContent').html('');
						var pagefn = doT.template(document.getElementById(dotTemplate).innerHTML, undefined, undefined);
						$('#manuscriptsDataContent').html(pagefn(data));
						//$('#manuscriptsData').height(window.innerHeight - $( +'#manuscriptsData').position().top - $( +'#footerContainer').height());
						if (packageForBook.indexOf(param.customer) > -1) {
							$('#exportEpubBtn').removeClass('hidden');
						} else {
							$('#exportEpubBtn').addClass('hidden');
						}
						if (!sectionItems.length) {
							$('#combinePDF').addClass('hidden');
						}
						if (param.type == 'journal') {
							$( + '#tocContentDiv .sectionList').each(function(){
								var firstpage = "";
								var lastpage = "";
								var sec = $(this).find('.sectionDataChild').length
								if(sec > 0){
									var firstpage = $(this).find('.sectionDataChild .rangeStart').first().text()
									var lastpage = $(this).find('.sectionDataChild .rangeEnd').last().text()
									$(this).find('.sectionData .rangeStart').text(firstpage);
									$(this).find('.sectionData .rangeEnd').text(lastpage);
								}
							})
							$( + '#issueDetails .volume span').text(issueContent.meta.volume._text);
							$( + '#issueDetails .issue span').text(issueContent.meta.issue._text);
						}
						if (doiString.length == 0) {
							for (var a = 0; a < al; a++) {
								//if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
								if (articleList[a]._attributes.id) {
									var doi = articleList[a]._attributes.id;
									if ($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]').length > 0) {
										var articleCard = $($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]')[0]);
										articleCard.data('data', articleList[a]);
										if (articleList[a]._attributes.fpage) {
											articleCard.find('.pageRange').attr('data-fpage', articleList[a]._attributes.fpage)
											articleCard.find('.pageRange .rangeStart').text(articleList[a]._attributes.fpage)
										}
										if (articleList[a]._attributes.lpage) {
											articleCard.find('.pageRange').attr('data-lpage', articleList[a]._attributes.lpage)
											articleCard.find('.pageRange .rangeEnd').text(articleList[a]._attributes.lpage)
										}
										if (articleList[a]._attributes['data-uuid']) {
											var dataUuid = articleList[a]._attributes['data-uuid'];
										} else {
											var dataUuid = uuid.v4();
										}
										if (articleList[a]._attributes.chapterNumber && articleCard.find('.chapterNumber')) {
											articleCard.find('.msChNo').html(articleList[a]._attributes.chapterNumber)
										}
										articleCard.attr('data-uuid', dataUuid);
										articleCard.attr('updated', 'true');
										$(articleCard).parent().append(articleCard);
									}
								}
							}
							var c = 0;
							$( + '#tocContentDiv .sectionList > *:not(.sectionData)').each(function () {
								$(this).attr('data-c-id', c++)
							})
							eventHandler.flatplan.action.saveData({'hideNotification':'true'});
							setTimeout(function(){
								eventHandler.flatplan.action.trackIssue();
							},120000);
							$('.la-container').fadeOut();
							return false;
						}
						doiString = doiString.join(' OR ');
						/*$('.sectionList[data-section] .sectionDataChild').each(function (i, scIndex) {
							$(this).data('data', data.items[0].item[i]);
						})*/
						var params = {}
						params.url = '/api/getarticlelist';
						params.doistring = doiString;
						params.urlToPost = 'getDoiArticles';
						params.from = "0";
						params.size = "500";
						params.customer = param.customer;
						$.ajax({
							type: "POST",
							url: params.url,
							data: params,
							dataType: 'json',
							success: function (respData) {
								$('.la-container').fadeOut();
								if (!respData || respData.hits.length == 0) {
									for (var a = 0; a < al; a++) {
										//if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
										if (articleList[a]._attributes.id) {
											var doi = articleList[a]._attributes.id;
											if ($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]').length > 0) {
												var articleCard = $( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]');
												articleCard.data('data', articleList[a]);
												if (articleList[a]._attributes.fpage) {
													articleCard.find('.pageRange').attr('data-fpage', articleList[a]._attributes.fpage)
													articleCard.find('.pageRange .rangeStart').text(articleList[a]._attributes.fpage)
												}
												if (articleList[a]._attributes.lpage) {
													articleCard.find('.pageRange').attr('data-lpage', articleList[a]._attributes.lpage)
													articleCard.find('.pageRange .rangeEnd').text(articleList[a]._attributes.lpage)
												}
												if (articleList[a]._attributes['data-uuid']) {
													var dataUuid = articleList[a]._attributes['data-uuid'];
												} else {
													var dataUuid = uuid.v4();
												}
												if (articleList[a]._attributes.chapterNumber && articleCard.find('.chapterNumber')) {
													articleCard.find('.msChNo').html(articleList[a]._attributes.chapterNumber)
												}
												articleCard.attr('data-uuid', dataUuid)
												$(articleCard).parent().append(articleCard)
											}
										}
									}
									var c = 0;
									$( + '#tocContentDiv .sectionList > *:not(.sectionData)').each(function () {
										$(this).attr('data-c-id', c++)
									})
									$( + '#tocContentDiv #sectionGroup').css('max-height', (window.innerHeight - ($( + '#tocContentDiv #sectionGroup').offset().top )- $('#footerContainer').height()) + 'px');
									$('.la-container').css('display', 'none');
									//$('#manuscriptsDataContent').html('NO RESULTS FOUND');
									$('#msCount').text('0');
									return;
								}
								var data = {};
								data.dconfig = dashboardConfig;
								// data.info = [];
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['supportlevel'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['others'] = {};
								data.storeVariable = "bookData";
								data.dashboardview = +'#manuscriptsData';
								if(param.type=='journal') {
									issueValData = [];
									issueValData = respData.hits;
									data.storeVariable = 'issueValData';
									data.customerType = 'issue';
								}else{
									bookData = [];
									bookData = respData.hits;
									data.customerType = 'book';
									$(+ '#manuscriptsData').attr('style', 'overflow:hidden;')
								}
								data.disableFasttrack = disableFasttrack;
								data.userDet = JSON.parse($('#userDetails').attr('data'));
								// data.customerType = projectLists[$(targetNode).closest('.dashboardTabs.active').find('#customer span').text()].type;
								//$('#manuscriptsDataContent').html('');
								data.count = 0;
								data.info = respData.hits;
								manuscriptsData = $('#manuscriptsDataContent');
								var pagefn = doT.template(document.getElementById('articlesTemplate').innerHTML, undefined, undefined);
								var cardsData = $('<div>' + pagefn(data) + '</div>');
								$( + '#manuscriptsDataContent [data-section]').find('[updated]').removeAttr('updated');
								//$('#manuscriptsDataContent [data-section="chapters"]').append(pagefn(data));
								for (var a = 0; a < al; a++) {
									//if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
									if (articleList[a]._attributes.id) {
										var doi = articleList[a]._attributes.id;
										if ($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]').length > 0) {
											var articleCard = $($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]:not([updated])')[0]);
											if (cardsData.find('[data-doi="' + doi + '"]:not([updated])').length > 0) {
												selectedCard = $(cardsData.find('[data-doi="' + doi + '"]:not([updated])')[0]);
												selectedCard.addClass('sectionDataChild');
												selectedCard.attr('data-grouptype', articleList[a]._attributes.grouptype);
												selectedCard.attr('data-type', articleList[a]._attributes.type);
												selectedCard.attr('data-issue-section', articleList[a]._attributes.section);
												selectedCard.attr('data-section', articleList[a]._attributes.section);
												if(articleList[a]._attributes['data-runon-with'] && articleList[a]._attributes['data-runon-with'] !=''){
													selectedCard.attr('data-runon-with', articleList[a]._attributes['data-runon-with']);
												}
												var customer = $().find('[id="customer"] .filter-list.active').attr('value');
												if(cardsData.find('[data-doi="' + doi + '"]:not([updated]) .viewButtons.kriya1').length >0){
												var k1editpath = "http://"+customer+".kriyadocs.com/review_content/article/"+articleList[a]._attributes.cmsID+"/?reviewer=typesetter";
													$(cardsData.find('[data-doi="' + doi + '"]:not([updated]) .viewButtons.kriya1')[0]).attr('href', k1editpath);
												}
												articleCard.replaceWith(selectedCard.clone());
												articleCard = $($( + '#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]:not([updated])')[0]);
												articleCard.attr('updated', 'true');
											}
											if (articleList[a].flag) {
												if (articleList[a].flag.constructor !== Array) {
													articleList[a].flag = [articleList[a].flag];
												}
												var dataText = '';
												for (eachFlag of articleList[a].flag) {
													if ((!eachFlag._attributes.type.match(/run-on/g) && Number(eachFlag._attributes.value))) {
														dataText += eachFlag._attributes.type.charAt(0).toUpperCase() + eachFlag._attributes.type.slice(1) + ', ';
													} else if (eachFlag._attributes.type.match(/open-access/g) && !eachFlag._attributes.value.match(/None/gi)) {
														dataText += eachFlag._attributes.type.charAt(0).toUpperCase() + eachFlag._attributes.type.slice(1) + '( ' + eachFlag._attributes.value + ' ), ';
													}
												}
												articleCard.find('.selectedIssueData').text(dataText.replace(/,\s$/g, ''));
											}
											articleCard.data('data', articleList[a]);
											if (articleList[a]._attributes.fpage && articleList[a]._attributes.fpage!=undefined && articleList[a]._attributes.fpage!='NaN') {
												articleCard.find('.pageRange').attr('data-fpage', articleList[a]._attributes.fpage)
												articleCard.find('.pageRange .rangeStart').text(articleList[a]._attributes.fpage)
											} else {
												articleList[a]._attributes.fpage = 1;
												articleCard.find('.pageRange').attr('data-fpage', '1')
												articleCard.find('.pageRange .rangeStart').text('1')
												articleCard.find('.pageRange .rangeStart').text('1')
											}
											if (articleList[a]._attributes.lpage && articleList[a]._attributes.lpage!=undefined && articleList[a]._attributes.lpage!='NaN') {
												articleCard.find('.pageRange').attr('data-lpage', articleList[a]._attributes.lpage)
												articleCard.find('.pageRange .rangeEnd').text(articleList[a]._attributes.lpage)
											} else {
												articleList[a]._attributes.lpage = 1;
												articleCard.find('.pageRange').attr('data-lpage', '1')
												articleCard.find('.pageRange .rangeEnd').text('1')
											}
											if(articleList[a]._attributes['data-modified'] && articleList[a]._attributes['data-modified']=='true'){
												articleCard.find('.pageRange').addClass('pageModified');
												articleCard.find('.pageRange').attr('title', 'Page is Modified');
											}
											if(articleList[a]._attributes['data-incorrectPage'] && articleList[a]._attributes['data-modified']=='true'){
												articleCard.find('.pageRange').addClass('incorrectPage');
												articleCard.find('.pageRange').attr('title', 'Page is incorrect');
											}
											if (articleList[a]._attributes['data-uuid']) {
												var dataUuid = articleList[a]._attributes['data-uuid'];
											} else {
												var dataUuid = uuid.v4();
											}
											if (articleList[a]._attributes.chapterNumber && articleCard.find('.chapterNumber')) {
												articleCard.find('.msChNo').html(articleList[a]._attributes.chapterNumber)
											}
											articleCard.attr('data-uuid', dataUuid)
											if (articleList[a]._attributes['data-run-on'] == "true") {
												articleCard.attr('data-run-on', 'true');
												articleCard.find('.pageRange').hide();
												articleCard.find('.runOn').removeClass('hide');
											}
											$(articleCard).parent().append(articleCard)
										}
									}
								}
								$( + '#manuscriptsDataContent [data-section="chapters"] .msCard').attr('data-grouptype', 'body').attr('data-on-reorder', 'changeChapterNumber');
								var c = 0;
								$( + '#tocContentDiv .sectionList > [data-c-id],' +  + '#tocContentDiv .sectionList > li').each(function () {
									$(this).attr('data-c-id', c++)
								})
								if (respData.hits && respData.hits[0] && respData.hits[0]._source && respData.hits[0]._source.customer) {
									//eventHandler.components.actionitems.getUserDetails(respData.hits[0]._source.customer);
								}
								$('.msCount.loading.hidden').removeClass('hidden');
								$( + '#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								$('#msCount').text(respData.hits.length);
								//construct workflow progress bar
								var articleStages = {};
								for (var d = 0, dl = cardData.length; d < dl; d++) {
									var cardInfo = cardData[d];
									if (cardInfo && cardInfo._source) {
										sourceData = cardInfo._source;
									}
									if (sourceData == undefined || !sourceData || !sourceData.stage) {
										return false;
									}
									var sourceStage = [];
									if (sourceData.stage.constructor.toString().indexOf("Array") < 0) {
										sourceStage = [sourceData.stage]
									} else {
										sourceStage = sourceData.stage;
									}
									var currArticleStages = [];
									for (var s = 0, sl = sourceStage.length; s < sl; s++) {
										var currStage = sourceStage[s];
										var currStageName = currStage.name;
										if (/Pre-editing|Typesetter|^Copyediting$|Author|Publisher/i.test(currStageName)) {
											if (!articleStages[currStageName]) {
												articleStages[currStageName] = [];
											}
											if (currStage.status == "completed" && !currArticleStages[currStageName]) {
												articleStages[currStageName].push(cardInfo._id);
												currArticleStages[currStageName] = [];
											}
										}
									}
								}
								var chCount = $( + '#manuscriptsData .msCard').length;
								$('.progress-container').html('')
								for (var articleStage in articleStages) {
									var cnt = articleStages[articleStage].length;
									var completed = parseInt((cnt / chCount) * 100);
									var progress = '<div class="col" style="max-width: 125px !important;margin-right:10px;padding: 0px !important;min-width: 100px !important;"><div class="progress" style="width: 100%;height: 8px;display:inline-block;"><div class="progress-bar bg-success" role="progressbar" style="padding:0px 5px 2px;width: ' + completed + '%;height:100%;"></div></div>';
									progress += '<span  style="font-size: 11px;">' + articleStage + '</span><span class="pull-right" style="font-size: 11px;color: #4c4c4c;">' + completed + '%</span></div>';
									$('.progress-container').append(progress)
								}
								if (param.type == 'journal') {
									$( + '#issueDetails .volume span').text(issueContent.meta.volume._text);
									$( + '#issueDetails .issue span').text(issueContent.meta.issue._text);
								}
								eventHandler.flatplan.issue.populateSectionDetails();
								$( + '#tocContentDiv #sectionGroup').css('max-height', (window.innerHeight - ($( + '#tocContentDiv #sectionGroup').offset().top )- $('#footerContainer').height()) + 'px');
								$('.la-container').css('display', 'none');
							}
						});
						//trigger save without notification
						eventHandler.flatplan.action.saveData({'hideNotification':'true'});
						setTimeout(function(){
							eventHandler.flatplan.action.trackIssue();
						}, 120000);
					},
					error: function (res) {
						$('.la-container').fadeOut();
						var parameters = { notify: { notifyType: 'error', notifyTitle: 'Unable to find ' + param.type, notifyText: eventHandler.flatplan.errorCode[500][1] } };
						eventHandler.common.functions.displayNotification(parameters);
					}
				});
			},

			/**
			*	Save META XML JSON details back to metaXML
			*	Send to API to save metaXML
			**/
			saveData: function (param) {
				// var customer = $('#filterCustomer .filter-list.active').attr('value');
				// var project = $('#filterProject .filter-list.active').attr('value');
				var customer = 'bmj';
				var project = 'thoraxjnl';
				var fileName = $('#manuscriptsDataContent .newStatsRow').attr('data-file');
				fileName = fileName.replace('.xml', '');
				let xml = $('#manuscriptsData').data('binder');
				$('.save-notice').removeClass('hide');
				// $('.save-notice').text('Saving...');
				$.ajax({
					url: '/api/issuedata?customer=' + customer + '&project=' + project + '&fileName=' + fileName + '&type=saveXML',
					data: JSON.stringify(xml),
					contentType: 'application/json',
					type: 'POST',
					success: function (data) {
						if (param && param.onsuccess) {
							if (typeof (window[param.onsuccess]) == "function") {
								window[param.onsuccess]
							} else {
								eval(param.onsuccess);
							}
						}
						if ($('[data-section="frontcontents"]').children().length >= 3 || ($('[data-section="frontcontents"]').children().length == 2 && $('[data-section="chapters"]').children().length == 2) || $('[data-section="chapters"]').children().length >= 3) {
							$('#combinePDF').removeClass('hidden');
						}
						//showMessage({ 'text': eventHandler.messageCode.successCode[200][3], 'type': 'success' });
						// $('.save-notice').text(eventHandler.flatplan.successCode[200][3] + ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
						var parameters = {
							notify: {
								notifyType: 'success',
								notifyTitle: 'Success',
								notifyText: eventHandler.flatplan.successCode[200][3] + ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss')
							}
						};
						var saveNotifyObj = 	new PNotify({
							title: 'Success',
							text: eventHandler.flatplan.successCode[200][3] + ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'),
							type: 'success',
							hide: true
						});
							//eventHandler.common.functions.displayNotification(parameters);
						$('.la-container').fadeOut();
					},
					error: function (data) {
						//showMessage({ 'text': eventHandler.messageCode.errorCode[501][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
						$('.save-notice').text(eventHandler.flatplan.errorCode[501][1] + ' ' + eventHandler.flatplan.errorCode[503][1]);
						$('.la-container').fadeOut();
					}
				});
			},
			/**
			 *
			 */
			addNewIssue: function(param, targetNode){
				var file = $('#addNewIssue').find('input[id=upload_file]')
				var fileContents = file[0].files[0];
				var draftIssue = $('#addNewIssue').find('#draft0').is(':checked');
				if (typeof (FileReader) != "undefined" && fileContents != undefined) {
					$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
					var reader = new FileReader();
					reader.onload = function (e) {
						var param={
							"fileContents":e.target.result,
							"draftIssue": draftIssue,
							"roFileName":fileContents.name
						}
						eventHandler.flatplan.action.saveXML('issueXML',param)
						$('#addNewIssue').modal('hide');
					}
					reader.readAsText(file[0].files[0]);
				}
				else{
					$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
					var volume = $('#addNewIssue').find('input[id=volumeNo]').val()
					var issue = $('#addNewIssue').find('input[id=issueNo]').val()
					if(volume != '' && issue != '' ){
						var param={
							"volume": volume,
							"issue" : issue,
							"draftIssue" : draftIssue
						}
						eventHandler.flatplan.action.saveXML('issueXML',param)
						$('#addNewIssue').modal('hide');
					}
					else{
						$('#addNewIssue').find('label[id=invalidInput]').removeClass('hide')
					}
				}
				$(currentTab + '#issueProject .filter-list.active').trigger('click');
			},
			uploadManuscriptDocs:function(param,targetNode){
				$('#addManuscriptDocs').find('label[id=invalidInput]').removeClass('hide');
				let currentDoi=	$(currentTab + '#rightPanelContainer').attr('data-dc-doi');
				let onlinePDF_file = $('#addManuscriptDocs').find('input[id=upload_online_pdf]')[0];
				let printPDF_file = $('#addManuscriptDocs').find('input[id=upload_print_pdf]')[0];
				let xmlfile = $('#addManuscriptDocs').find('input[id=upload_xml]')[0];
				let printPDF;
				let onlinePDF;
				let articleXML;
				if ( onlinePDF_file &&  onlinePDF_file.files.length > 0) {
					onlinePDF = onlinePDF_file.files[0];
				}
				if ( printPDF_file &&  printPDF_file.files.length > 0) {
					printPDF = printPDF_file.files[0];
				}
				if ( xmlfile &&  xmlfile.files.length > 0) {
					articleXML = xmlfile.files[0];
				}
				let contentType =$(currentTab + '[data-doi="' + currentDoi + '"]').attr('data-type');
				let contentData = $(currentTab + '[data-doi="' + currentDoi + '"]').data('data');
				if(contentType == 'manuscript' && contentData && contentData._attributes && contentData._attributes.siteName && contentData._attributes.siteName=='Non-kriya'){
					if(!articleXML ||!onlinePDF || !printPDF){
						$('#addManuscriptDocs').find('label[id=invalidInput]').removeClass('hide');
						return false;
					}
				}
				else if(contentType == 'manuscript' && contentData && contentData._attributes && contentData._attributes['data-run-on'] && contentData._attributes['data-run-on']=='true'){
					if (!onlinePDF) {
						$('#addManuscriptDocs').find('label[id=invalidInput]').removeClass('hide');
						return false;
						//upload both online and print pdf
					}
				}
				else if(!onlinePDF || !printPDF){
					$('#addManuscriptDocs').find('label[id=invalidInput]').removeClass('hide');
					return false;
					//upload both online and print pdf
				}
				var flagsArray ;
				if(contentData){
					flagsArray = contentData.flag
				}
				var flagSet = false;
				if(flagsArray){
					if(flagsArray.constructor.toString().indexOf("Array") < 0){
							flagsArray = [flagsArray];
					}
					flagsArray.forEach(function(currFlagObj){
						if (currFlagObj._attributes.type == "offline"){
								currFlagObj._attributes.value = "1";
								flagSet = true;
						}
					});
				}
				else{
					var newFlag = ({"_attributes":{"type": "offline", "value": "1"}});
					contentData.flag = newFlag;
					flagSet = true;
				}
				if (!flagSet){
					flagsArray.push({"_attributes":{"type": "offline", "value": "1"}});
					contentData.flag = flagsArray;
				}
				eventHandler.flatplan.action.saveData();
				if ( articleXML) {
					if (typeof (FileReader) != "undefined") {
						var reader = new FileReader();
						reader.onload = function (e) {
							eventHandler.flatplan.action.saveXML('manuscript', e.target.result)
						}
						reader.readAsText(articleXML);
					}
				}
				if(printPDF){
					eventHandler.flatplan.issue.uploadFile($('#addManuscriptDocs').find('input[id=upload_print_pdf]')[0],'issueMakeup');
				}
				if(onlinePDF){
					eventHandler.flatplan.issue.uploadFile($('#addManuscriptDocs').find('input[id=upload_online_pdf]')[0],'issueMakeup');
				}

				$('#addManuscriptDocs').modal('hide');
				$('.la-container').fadeIn();
			},
			getDataFromURL: function(obj){
				return new Promise(function (resolve, reject) {
					jQuery.ajax({
						type: "POST",
						url: obj.url,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (msg) {
							if (msg && msg[obj.objectName] && msg[obj.objectName].response){
								var response = msg[obj.objectName].response;
								if (response.status.code._text == 200){
									var parameters = {
										notify: {
											notifyType: 'success',
											notifyTitle: 'Kriya 1 article Added',
											notifyText: 'Kriya1 article added into this issue'
										}
									};
									eventHandler.common.functions.displayNotification(parameters);
									resolve({
										'code': 200,
										'data': response.data
									});
								}else{
									//showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1] , 'type': 'error', 'hide':false });
									var parameters = {
										notify: {
											notifyType: 'error',
											notifyTitle: 'Kriya 1 article not Added',
											notifyText: 'Kriya1 article not added'
								}
									};
									eventHandler.common.functions.displayNotification(parameters);
									reject({
										'code': 500,
										'message': eventHandler.messageCode.errorCode[500][3] + obj.url
									});
							}
							} else if (msg && msg[obj.objectName]) {
								var parameters = {
									notify: {
										notifyType: 'success',
										notifyTitle: 'Kriya 1 article Added',
										notifyText: 'Kriya1 article added into this issue'
							}
								};
								eventHandler.common.functions.displayNotification(parameters);
								resolve({
									'code': 200,
									'data': msg[obj.objectName]
								});
							} else {
								//showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','hide':false });
								var parameters = {
									notify: {
										notifyType: 'error',
										notifyTitle: 'Kriya 1 article not Added',
										notifyText: 'Kriya1 article not added'
									}
								};
								eventHandler.common.functions.displayNotification(parameters);
								reject({
									'code': 500,
									'message': 'Error ' + obj.url
								});
							}
						},
						error: function (xhr, errorType, exception) {
							var parameters = {
								notify: {
									notifyType: 'error',
									notifyTitle: 'Kriya 1 article not Added',
									notifyText: 'Kriya1 article not added'
								}
							};
							eventHandler.common.functions.displayNotification(parameters);
							//showMessage({ 'text': eventHandler.messageCode.errorCode[500][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error', 'hide':false });
							reject({
								'code': 500,
								'message': 'error ' + obj.url
							});
						}
					});
				})
			},
			goToView: function(elementID){
				var element = $('#' + elementID)
				$('#flatPlanBodyDiv .wrapper').find('.focus').removeClass('focus')
				$('#' + elementID).parent().addClass('focus')
				$('#flatPlanBodyDiv .wrapper').find('img[data-id="' + elementID + '"]').parent().addClass('focus anuraja');
				if($("#"+elementID).length > 0 && $(currentTab+'#flatPlanBodyDiv #sectionGroup').length > 0){
					var myElement = $('#' + elementID)[0];
					var topPos = myElement.offsetTop;
					$(currentTab+'#flatPlanBodyDiv #sectionGroup')[0].scrollTop = topPos;
				}
			},
			saveXML: function(type, param) {
				var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
				var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
				if(!project || project.match(/project/gi)){
					var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add Job', notifyText : 'Please select project to add Job'}};
					eventHandler.common.functions.displayNotification(parameters);
					return;
				}
				if(type == 'manuscript'){
					var meta = $(currentTab+ ' #manuscriptsData').data('binder')['container-xml'].meta;
					var volume = meta["volume"]['_text'] ?  meta["volume"]['_text'] : parseInt(0);
					var issue = meta["issue"]['_text'] ?  meta["issue"]['_text'] : parseInt(0);
					var contentDOI = $(currentTab + '#rightPanelContainer').attr('data-dc-doi');
					$.ajax({
						type: 'POST',
						url: '/api/issuedata',
						data: {'customer': customer,'project': project,'volume':volume,'issue':issue,'currentDOI':contentDOI, 'articleXML': param, 'type':'saveArticleXML'},
						success: function (response) {
							$('.save-notice').text(eventHandler.flatplan.successCode[200][5] + 'on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
							var parameters = {notify : {notifyType : 'success', notifyTitle : 'Article XML added Successfully', notifyText : 'Job created successfully'}};
							eventHandler.common.functions.displayNotification(parameters);
							//$(currentTab + '#projectIssueList .active').trigger('click');
							//$('.la-container').fadeOut();
						},
						error: function (response) {
							var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add article xml', notifyText : 'Please select project to add Job'}};
							eventHandler.common.functions.displayNotification(parameters);
							$('.save-notice').text(eventHandler.flatplan.errorCode[501][2] + ' ' + eventHandler.flatplan.errorCode[503][1]);
						}
					 });
					}
				else if(type == 'issueXML'){
					$('.la-container').fadeIn();
					var fileContents =  param.fileContents
					// if RO XML is uploaded
					if (fileContents) {
						var volume = $(fileContents).attr('VOLUME');
						var issue = $(fileContents).attr('ISSUE');
						var issueName = project + '_' + volume + '_' + issue;
						if(param.draftIssue && param.draftIssue==true){
							issueName = issueName+'_draft';
						}
						$.ajax({
							type: 'POST',
							url: '/api/issuedata',
							data: { 'customer': customer, 'project': project, 'issueXML': param.fileContents, 'draftIssue': param.draftIssue, 'type': 'generateContainerXML','roFileName':param.roFileName,'username': userData.kuser.kuname.first },
							success: function (response) {
								$('.save-notice').text('Issue Created on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
								$('.save-notice').removeClass('hide');
								var parameters = {notify : {notifyType : 'success', notifyTitle : 'Issue added Successfully', notifyText : eventHandler.flatplan.successCode[200][2]+ ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss')}};
								eventHandler.common.functions.displayNotification(parameters);
								//	eventHandler.general.actions.getIssueList(customer,project)
								// $(currentTab + '#issueProject .filter-list.active').trigger('click');
								// setTimeout(function () {
								// 	$(currentTab + '#issueProject .filter-list.active').trigger('click');
								// }, 2)
								var projectIssue = '<div class="filter-list" data-customer="' + customer + '" data-project="' + project + '" data-file="' + issueName + '" data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'getMetaXML\',\'param\':{\'type\':  \'journal\',\'projectName\':\'' + project + '\',\'issueId\':\'' + issueName + '\'}}" value="' + issueName + '">' + issueName + '</div>';
								$(currentTab + '#projectIssueList').append(projectIssue);
								$('.la-container').fadeOut();
							},
							error: function (response) {
								if(response.responseJSON && response.responseJSON.status &&  response.responseJSON.status.message){
									$('.save-notice').text(response.responseJSON.status.message);
									var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add Job', notifyText : response.responseJSON.status.message}};
									eventHandler.common.functions.displayNotification(parameters);
								}
								else{
									if(typeof(response.responseJSON)=='object') response.responseJSON = JSON.stringify(response.responseJSON)
									$('.save-notice').text(eventHandler.flatplan.errorCode[501][6] + ' ' + eventHandler.flatplan.errorCode[503][1]);
									var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add Job', notifyText : response.responseJSON}};
									eventHandler.common.functions.displayNotification(parameters);
								}
								$('.la-container').fadeOut();
							}
						});
					}
					//if RO XML is not uploaded. generate issue xml using volume and issue
					else{
						var issueName = project+'_'+param.volume+'_'+param.issue;
						if(param.draftIssue && param.draftIssue==true){
							issueName = issueName+'_draft';
						}
						if($(currentTab+'#projectIssueList .filter-list[data-file="'+issueName+'"]').length>0 && (!param.draftIssue ||(param.draftIssue && param.draftIssue!=true))){
							var parameters = {notify : {notifyType : 'error', notifyTitle : 'Issue Already present', notifyText : issueName+ ' is already present'}};
							eventHandler.common.functions.displayNotification(parameters);
							$('.la-container').fadeOut();
							return;
						}
						$.ajax({
							type: 'POST',
							url: '/api/issuedata',
							data: {'customer': customer,'project': project, 'volume': param.volume,'issue': param.issue,'draftIssue': param.draftIssue,'type':'generateContainerXML'},
							success: function (response) {
								$('.save-notice').text(eventHandler.flatplan.successCode[200][2]+ ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss'));
							//	eventHandler.general.actions.getIssueList(customer,project)
								var parameters = {notify : {notifyType : 'success', notifyTitle : 'Issue added Successfully', notifyText : eventHandler.flatplan.successCode[200][2]+ ' on ' + dateFormat(new Date(), 'mm-dd-yyyy hh:mm:ss')}};
								eventHandler.common.functions.displayNotification(parameters);
								var projectIssue = '<div class="filter-list" data-customer="' + customer + '" data-project="' + project + '" data-file="' + issueName + '" data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'getMetaXML\',\'param\':{\'type\':  \'journal\',\'projectName\':\'' + project + '\',\'issueId\':\'' + issueName + '\'}}" value="' + issueName + '">' + issueName + '</div>';
								$(currentTab + '#projectIssueList').append(projectIssue);
								// $(currentTab + '#issueProject .filter-list.active').trigger('click');
								$('.la-container').fadeOut();
							},
							error: function (response) {
								if(response.responseJSON && response.responseJSON.status &&  response.responseJSON.status.message){
									$('.save-notice').text(response.responseJSON.status.message);
									var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add Job', notifyText : response.responseJSON.status.message}};
									eventHandler.common.functions.displayNotification(parameters);
								}
								else{
									var parameters = {notify : {notifyType : 'error', notifyTitle : 'Unable to add Job', notifyText : eventHandler.flatplan.errorCode[501][6] + ' ' + eventHandler.flatplan.errorCode[503][1]}};
									eventHandler.common.functions.displayNotification(parameters);
									$('.save-notice').text(eventHandler.flatplan.errorCode[501][6] + ' ' + eventHandler.flatplan.errorCode[503][1]);
								}
								$('.la-container').fadeOut();
							}
						});
					}
				}
				else if(type == 'fetchK1Article'){

				  var sectionName = param.sectionName
					var fileName = $('#issueVal').attr('data-file');
					var meta = $(currentTab+ ' #manuscriptsData').data('binder')['container-xml'].meta;
					var volume = meta["volume"]['_text'] ?  meta["volume"]['_text'] : parseInt(0);
					var issue = meta["issue"]['_text'] ?  meta["issue"]['_text'] : parseInt(0);

					return new Promise(function (resolve, reject) {
						$('.la-container').fadeIn();
						var customer = $(currentTab + '#issueCustomer .filter-list.active').attr('value');
						var project = $(currentTab + '#issueProject .filter-list.active').attr('value');
						var fileName = $('#projectIssueList .filter-list.active').attr('value');
						var obj = { 'url': '/api/issuedata?customer=' + customer + '&project=' + project + '&fileName=' + fileName +'&insertSection='+ param.sectionName+ '&volume='+ volume +'&issue='+ issue + '&msid='+ param.msid + '&processType=fetchK1Article&type=generateContainerXML', 'objectName': 'list' };
						eventHandler.flatplan.action.getDataFromURL(obj)
						.then(function (issueData) {
								var issueContents = $(currentTab +'#manuscriptsData').data('binder')['container-xml']
								newcontent = JSON.parse(JSON.stringify(issueData.data.content));
								console.log(newcontent);
								var firstSecNodeId = $(currentTab + '#tocContentDiv [data-grouptype="'+newcontent._attributes.grouptype+'"][data-c-id]:first').attr('data-c-id');
								console.log(issueContents.contents.content.length);
								var matchKey = "";
								issueContents.contents.content.forEach(function (val, key) {
									console.log(val);
									if (matchKey == '' && issueContents.contents.content[key] && issueContents.contents.content[key]._attributes && issueContents.contents.content[key]._attributes.section && issueContents.contents.content[key]._attributes.section == sectionName) {
										console.log(issueContents.contents.content[key], ' match ',issueContents.contents.content[key]._attributes.id);
										matchKey = key;
									}
								})
								var prevSecName='';
								var tempsectionName = sectionName.toLowerCase().replace(/\s/g,'');
								//if current section is empty add the content as the last article of previous section
								if(matchKey==''){
									var curSecIndex = 0;
									curSecIndex = parseInt(curSecIndex);
									var secValues = eventHandler.flatplan.components.getAvailableSections();
									secValues.forEach(function (secValue, secIndex) {
									if(secValue==sectionName){
											curSecIndex = secIndex;
										}
									});
									var tempIndex = curSecIndex;
									var contSecflag = false;
									while(tempIndex >= 0){
										if($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]').length>0){
											contSecflag = true;
											matchKey = parseInt($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]:last').attr('data-c-id'))+1;
											break;
										}
										tempIndex--;
									}
									if(contSecflag==false){
										matchKey = 1;
									}
								}
								issueContents.contents.content.splice(matchKey, NaN, newcontent)
								console.log(issueContents.contents.content.length);
								var metaFpage = '1';
								var metaLpage = '1';
								if(issueContents.meta && issueContents.meta['first-page'] && issueContents.meta['first-page']._text){
									metaFpage = issueContents.meta['first-page']._text;
								}
								var pageExt = "";
								if(newcontent._attributes.grouptype.match(/roman/g)){
									metaFpage = 'i';
									pageExt = (eventHandler.flatplan.components.fromRoman(newcontent._attributes.lpage) - eventHandler.flatplan.components.fromRoman(newcontent._attributes.fpage))+1
									metaLpage = eventHandler.flatplan.components.toRoman((eventHandler.flatplan.components.fromRoman(newcontent._attributes.fpage) + pageExt) -1);
								}else if(newcontent._attributes.grouptype.match(/epage/g)){
									metaFpage = 'e1';
									if(issueContents.meta['e-first-page']){
										metaFpage = issueContents.meta['e-first-page'];
									}
									pageExt = ((parseInt(newcontent._attributes.lpage) - parseInt(newcontent._attributes.fpage)) + 1)
									metaLpage = 'e'+ (parseInt(newcontent._attributes.fpage) + parseInt(pageExt)-1);
								}else{
									pageExt = ((parseInt(newcontent._attributes.lpage) - parseInt(newcontent._attributes.fpage)) + 1)
									metaLpage = (parseInt(metaFpage) + parseInt(pageExt)) -1;
								}
								if(newcontent && newcontent._attributes && newcontent._attributes.fpage){
									newcontent._attributes.fpage = metaFpage;
									newcontent._attributes.lpage = metaLpage;
									newcontent._attributes.pageExtent = pageExt;
								}
								var tempData = $('<li id="' + param.msid+ '"><span class="pageRange"><span class="rangeStartInfo">First Page:<span class="rangeStart">' +metaFpage+ '</span></span><span class="rangeEndInfo">Last Page:<span class="rangeEnd">' + newcontent._attributes.lpage+ '</span></span></span></li>');
								Object.keys(newcontent._attributes).forEach(function(name, attrVal){
									tempData.attr('data-' + name, newcontent._attributes[name]);
									//if
								})
								tempData.attr('data-issue-section', newcontent._attributes.section);
								$(tempData).data('data', newcontent);
								var firstSecNode = $(currentTab + '#tocContentDiv [data-grouptype="body"][data-c-id][data-section="' + sectionName + '"]:first');
								var lastNode = $(currentTab+'#tocContentDiv [data-c-id]:last');
								var lastNodeID = parseInt(0);
								if(lastNode && lastNode.length>0){
									parseInt(lastNode.attr('data-c-id'));
								}
								var tempID = lastNodeID + 1;
								tempData.attr('data-c-id', tempID);
								var firstContentFlag= false
								if($(currentTab + '#tocContentDiv [data-grouptype="'+newcontent._attributes.grouptype+'"][data-section="'+ sectionName +'"][data-c-id]:first').length>0){
									$(tempData).insertBefore($(currentTab + '#tocContentDiv [data-grouptype="'+newcontent._attributes.grouptype+'"][data-section="'+ sectionName +'"][data-c-id]:first'));
								}
								else if($(currentTab + '#tocContentDiv [class="sectionList"][data-section="'+ tempsectionName +'"]:first').length>0){
									$(currentTab + '#tocContentDiv [class="sectionList"][data-section="'+ tempsectionName +'"]:first').append($(tempData));
								}
								else if($(currentTab + '#tocContentDiv [class="sectionList"][data-section="'+ sectionName +'"]:first').length>0){
									$(currentTab + '#tocContentDiv [class="sectionList"][data-section="'+ sectionName +'"]:first').append($(tempData));
								}
								else{
									firstContentFlag = true;
								}
									if (/^(blank|advert|filler|filler-toc)$/gi.test(newcontent._attributes.type)) {
										newcontent._attributes['id'] += tempID;
									}
									var seqID = tempID;
									if(firstContentFlag == true){
										var histObj = {
											"change": "Inserted <b>" + param.msid + "</b> in section <u>" + param.sectionName + '</u>',
											"file": "",
											"fileType": "",
											"id-type": ""
										};
										eventHandler.flatplan.components.updateChangeHistory(histObj);
										eventHandler.flatplan.components.updatePageNumber(0, metaFpage, newcontent._attributes.grouptype, { 'onsuccess' : "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
									}
									else{
										while (lastNodeID && lastNodeID != tempID) {
											console.log(lastNodeID, ' - ' , tempID)
											lastNode.attr('data-c-id', seqID--);
											if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
												lastNode = $(lastNode).prevAll('[data-c-id]:first')
											} else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
												lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
											} else {
												lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
											}
											lastNodeID = parseInt(lastNode.attr('data-c-id'));
											console.log(lastNodeID);
										}
										console.log(seqID);
										lastNode.attr('data-c-id', seqID)//.data('data', newcontent);
										var histObj = {
											"change": "Inserted <b>" + param.msid + "</b> in section <u>" + param.sectionName + '</u>',
											"file": "",
											"fileType": "",
											"id-type": ""
										};
										eventHandler.flatplan.components.updateChangeHistory(histObj);
										eventHandler.flatplan.components.updatePageNumber(firstSecNodeId, metaFpage, newcontent._attributes.grouptype, { 'onsuccess' : "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
									}
								return;
							//eventHandler.general.actions.insertK1Article(newcontent,param.sectionName)
							var dataType = "manuscript";
							if (newcontent._attributes && newcontent._attributes.type) {
								dataType = newcontent._attributes.type
							}
							var dataGroupType = '';
							if (newcontent._attributes && newcontent._attributes.grouptype) {
								dataGroupType = newcontent._attributes.grouptype
							}
							newcontent._attributes.section = sectionName
							var content = $(currentTab+ ' #manuscriptsData').data('binder')['container-xml'].contents.content;

								var firstSecNode = $(currentTab + '#tocContentDiv li[data-grouptype="body"][data-c-id][data-section="' + sectionName + '"]:first');
							// get the last MS container
								var lastNode = $('#tocContentDiv li[data-c-id]:last');
							var lastNodeID = parseInt(lastNode.attr('data-c-id'));
							var tempID = lastNodeID + 1;
							if(firstSecNode){
								var firstSecNodeID = parseInt(firstSecNode.attr('data-c-id'))
								// for blank/filler/filler-toc/advert add tempID to entry id to generate unique ID
								if (/^(blank|advert|filler|filler-toc)$/gi.test(newcontent._attributes.type)) {
									newcontent._attributes['id'] +=  tempID;
								}
									/*var msConObj = {
									"dataType": dataType,
									"tempID": tempID,
									"sectionIndex": firstSecNode.attr('data-sec').replace('sec',''),
									"dataGroupType":dataGroupType
									}*/
									//var msContainer = eventHandler.general.components.getMSContainer(newcontent, msConObj);
									var msContainer = "";
									if ((msContainer == '') || (typeof (msContainer) === 'undefined')) {
										$('.save-notice').text(eventHandler.flatplan.errorCode[502][1] + ' ' + eventHandler.flatplan.errorCode[503][1]);
										return false;
									}
									$(msContainer).insertBefore(firstSecNode);
									var startPage = parseInt(firstSecNode.data('data')._attributes.fpage)
									$('[data-c-id="' + tempID + '"]').removeClass('hide');
									//push the 'entry' data into array as if its the last node, then push it through the array to it actual position
									var seqID = tempID;
									while (lastNodeID != tempID) {
										$(currentTab + ' #manuscriptsData').data('binder')['container-xml'].contents.content[lastNodeID + 1] = $('#tocContainer').data('binder')['container-xml'].contents.content[lastNodeID];
										lastNode.attr('data-c-id', seqID--);
										if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
											lastNode = $(lastNode).prevAll('[data-c-id]:first')
										} else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
											lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
										} else {
											lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
										}
										lastNodeID = parseInt(lastNode.attr('data-c-id'));
									}
									lastNode.attr('data-c-id', seqID).data('data', newcontent);
									$(currentTab + ' #manuscriptsData').data('binder')['container-xml'].contents.content[seqID] = newcontent;
									eventHandler.flatplan.components.updatePageNumber(seqID, startPage, newcontent._attributes.grouptype);
									//update change history*/
									var histObj = {
										"change": "Inserted <b>" + param.msid + "</b> in section <u>" + param.sectionName + '</u>',
										"file": "",
										"fileType": "",
										"id-type": ""
									};
									//eventHandler.flatplan.components.updateChangeHistory(histObj);
									//eventHandler.flatplan.action.saveData();
									$(currentTab + '#issueProject .filter-list.active').trigger('click');
								}
								$('.la-container').fadeOut();
								//$(currentTab + '#projectIssueList .filter-list.active').trigger('click');
								resolve(true);
							})
							.catch(function (err) {
								$('.la-container').fadeOut();
								$('.save-notice').text(eventHandler.flatplan.errorCode[500][9] + ' ' + eventHandler.flatplan.errorCode[503][1]);
								reject(err)
							})
					})

				}
			},
		},
		components: {
			attr: function (attr) {
				s = '';
				$(attr).each(function (i, e) {
					v = e.split('|');
					if (v.length == 2) {
						s += v[0] + '="' + v[1] + '" ';
					}
				});
				return s;
			},
			pad: function (number, length) {
				var str = '' + number;
				while (str.length < length) {
					str = '0' + str;
				}
				return str;
			},
			/** https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/
			*	Function to convert roman to integer
			*	input - roman numeral  e.g, fromRoman('iii')
			**/
			fromRoman: function (str) {
				var result = 0;
				// the result is now a number, not a string
				var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
				var roman = ["m", "cm", "d", "cd", "c", "xc", "l", "xl", "x", "ix", "v", "iv", "i"];
				for (var i = 0; i <= decimal.length; i++) {
					while (str.indexOf(roman[i]) === 0) {
						result += decimal[i];
						str = str.replace(roman[i], '');
					}
				}
				return result;
			},
			/** https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/
			*	Function to convert integer to roman
			*	input - intger val e.g, toRoman(4)
			**/
			toRoman: function (num) {
				var result = '';
				var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
				var roman = ["m", "cm", "d", "cd", "c", "xc", "l", "xl", "x", "ix", "v", "iv", "i"];
				for (var i = 0; i <= decimal.length; i++) {
					while (num % decimal[i] < num) {
						result += roman[i];
						num -= decimal[i];
					}
				}
				return result;
			},
			imgError: function (current) {
				current.src = "../images/NoImageFound.jpg"
				$(current).css('background', '');
			},
			appendTemplate: function (param, targetNode) {
				//e = eventHandler.menu.bookview.appendContent;
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-template="true"]');
				var clone = template.clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				clone.find('#corresAuthor').removeClass('is-invalid');
				$(targetNode).closest('[data-parent-name]').after(clone);
				//e+=1;
				//eventHandler.menu.bookview.appendContent = e;
			},
			removeTemplate: function (param, targetNode) {
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-cloned="true"]');
				if (template.length > 1) {
					$(targetNode).closest('[data-parent-name]').remove();
				}
			},
			bookType: function (param, targetNode) {
				$(targetNode).toggleClass('custom-select');
				var a = [];
				$('ul .custom-select').each(function () {
					a.push($(this).text());
				});
				a = a.toString();
				$(targetNode).parent().siblings('input[type="text"]').val(a).focus();
				return false;
			},
			addNewSection:function(param,targetNode){

				//find target node and its section name, add section after the target section
				//if target node is content node, create new section and add the current content and its following sibling(of same section) in the newly created section
				//console.log($(targetNode));
				var data = {};
				var articleCount;
				data.articleCount = 0;
				var followingNodesCount=0;
				var newSecCount = $(currentTab).find('div.sectionList[data-section^="newsection"]').length;
				let currentContent = $(targetNode).closest('.sectionDataChild');
				if(currentContent.length >0 ){
					data.articleCount = 1;
				}

				data.sectionName = 'newsection '+ (parseInt(newSecCount)+1);
				data.sectionHead = 'New Section'+(parseInt(newSecCount)+1);
				if(newSecCount == 0){
					data.sectionName = 'newsection';
					data.sectionHead = 'New Section'
				}
				var newSecTemplate = doT.template(document.getElementById('newSectionContent').innerHTML, undefined, undefined);
				//console.log(newSecTemplate);
				var newCardHtm = newSecTemplate(data);
				$(newCardHtm).insertAfter($(targetNode).closest('.sectionList'));
				if(currentContent.length >0 ){
					let currentSec = currentContent.attr('data-sec');
					let currentData = $(currentContent).data('data');
					if(currentData){
						if(typeof(currentData)!='undefined' && currentData._attributes){
							currentData._attributes.section = data.sectionName;
						}
					}
					let followingNodes = currentContent.nextAll('[data-sec="' + currentSec + '"]');
					followingNodesCount = followingNodes.length;
					if(followingNodesCount>0){
							data.articleCount = parseInt(followingNodesCount) +1;
					}
					if(followingNodesCount >0){
						//change the sec name of all the node to newSecname
						for(let fsecIndex=0;fsecIndex<followingNodesCount;fsecIndex++){
							if($(followingNodes[fsecIndex])){
								let currentData = $(followingNodes[fsecIndex]).data('data');
								if(typeof(currentData)!='undefined' && currentData._attributes){
									currentData._attributes.section = data.sectionName;
								}
							}
						}
					}
					var histObj = {
						"change": "moved article <b>"+currentData._attributes+ " </b> and its following articles to a new section named  <u>" + data.sectionHead + '</u>',
						"file": "",
						"fileType": "",
						"id-type": ""
					};
					eventHandler.flatplan.components.updateChangeHistory(histObj);
					//save and retrigger the current issue
					eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#projectIssueList .active').trigger('click')" });
				}
				else{
					var histObj = {
						"change": "created <b> a new section  </b> named <u>" + data.sectionHead + '</u>',
						"file": "",
						"fileType": "",
						"id-type": ""
					};
					eventHandler.flatplan.components.updateChangeHistory(histObj);
				}
			},
			changeBookType: function (param, targetNode) {
				$('.dynamic').remove();
				var bookType = $('#bookType').val();
				$('[data-cloned="true"]').remove();
				var clone = $('.' + bookType + '[data-template]').clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				$('.' + bookType + '[data-template]').after(clone)
			},
			addBookPopup: function (param, targetNode) {
				if ($('#filterCustomer > .filter-list.active').length == 0) {
					return false;
				}

				param = {
					customer: $('#manuscriptContent #customerVal').text()
				};
				$.ajax({
					type: "POST",
					url: "/api/issuedata?type=getReviewConfig&customer=" + $('#manuscriptContent #customerVal').text() + "",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (data) {
						if (data.list && data.list != '') {
							var properties = $(data.list);
							$(properties).find('[data-name]').each(function () {
								if (dataName = $(this).attr("data-name")) {
									$('#addProj [data-name=' + dataName + ']').parent().removeClass('hide');
								}
							});
							$('#addProj .addProj').removeClass('hide');
							$('#addProj .saveProj').addClass('hide');
							$('#addProj [data-name]').val('');
							$('#addProj').find('select').each(function () {
								if ($(this).find('option[selected]').length > 0) {
									$(this).val($(this).find('option[selected]').val())
								}
							});
							eventHandler.flatplan.components.changeBookType();
							var proofdetails = $(properties).find('proof-details')
							$(proofdetails).find('[data-name]').each(function () {
								dataName = $(this).attr("data-name")
								var currObj = $(this)
								if (dataName && $('#addProj select[data-name=' + dataName + ']').length > 0) {
									$('#addProj select[data-name=' + dataName + ']').find('option').remove()
									$('#addProj select[data-name=' + dataName + ']').append($('<option value=""></option>'))
									$(currObj).find('project').each(function () {
										var project = $(this)
										var element = $('<option value="' + $(this).attr("value") + '">' + $(this).attr("name") + '</option>')
										if ($('#addProj select[data-name=' + dataName + ']').attr('data-siblings-name') && $('#addProj select[data-name=' + dataName + ']').attr('data-siblings-name') != '') {
											var siblings = $('#addProj select[data-name=' + dataName + ']').attr('data-siblings-name').split(',')
											siblings.forEach(function (sibling) {
												if (project.attr(sibling) && project.attr(sibling) != '') {
													$(element).attr(sibling, project.attr(sibling))
												}
											})
										}
										$('#addProj select[data-name=' + dataName + ']').append($(element))
									})
								}

							})
							$('#addProj input[data-validate="true"]').removeClass('is-invalid');
							$('#addProj').modal();
							$('#addProj').on('hide.bs.modal', function (event) {
								$('#addProj .modal-body').scrollTop(0);
							});
						}
					},
					error: function (err) {
						//console.log(data);
					},
				});
			},
			/**
			 *
			 * @param {*} param
			 * @param {*} targetNode
			 */

			addNewIssuePopUp:function(param, targetNode){
				$('#addNewIssue').find('input[id=upload_file]').val('')
				$('#addNewIssue').find('label[id=invalidInput]').addClass('hide')
				$('#addNewIssue').find('#draft0').prop('checked',false)
				$('#addNewIssue').find('input[id=volumeNo]').val('')
				$('#addNewIssue').find('input[id=issueNo]').val('')
				$('#addNewIssue').modal('show');
			},
			addManuscriptDocsPopUp:function(param,targetNode){
				//console.log(targetNode);
				let currentDoi = targetNode.parents('#rightPanelContainer').attr('data-dc-doi');
				$('#addManuscriptDocs').find('input[id=upload_xml]').val('');
				$('#addManuscriptDocs').find('input[id=upload_print_pdf]').val('');
				$('#addManuscriptDocs').find('input[id=upload_online_pdf]').val('');
				$('#addManuscriptDocs').find('label[id=invalidInput]').addClass('hide');
				$('#addManuscriptDocs').find('#upload_manuscriptXML').removeClass('hide');
				if(currentDoi){
					let contentType =$(currentTab + '[data-doi="' + currentDoi + '"]').attr('data-type');
					let contentData = $(currentTab + '[data-doi="' + currentDoi + '"]').data('data');
					if(!contentData){
						//error
						$('#addManuscriptDocs').modal('show');
						return false
					}
					if(contentType == 'manuscript'){
						var siteName = contentData._attributes.siteName;
						var runonFlag = contentData._attributes['data-run-on'];
						if(siteName && siteName!='Non-kriya'){
							$('#addManuscriptDocs').find('#upload_manuscriptXML').addClass('hide');
						}
						if(runonFlag && runonFlag=='true'){
							$('#addManuscriptDocs').find('#upload_printPDF').addClass('hide');
						}
						$('#addManuscriptDocs').modal('show');
					}
					else{
						$('#addManuscriptDocs').find('#upload_manuscriptXML').addClass('hide');
						$('#addManuscriptDocs').modal('show');
					}
				}
				else{
					//unable to upload docs
				}
			},
			getCombinePDFPopup:function(param,targetNode){
				/**get unique region from issue-config, and display it in dropdown,
				 * when region is choosen and comnine pdf button is clicked call merge pdf function with region as param
				 *  */
				$('#generateCombinePDF').find('select[id=region_option]').html('')
				var configData = $(currentTab+'#manuscriptsData').data('config');
				if(!configData.regions){
					//call combine pdf
					eventHandler.flatplan.merge.mergePDF();
					return false;
				}
				var regionsConfig = JSON.parse(JSON.stringify(configData.regions)).region;
				if (regionsConfig.constructor.toString().indexOf("Array") < 0){
					regionsConfig = [regionsConfig];
				}
				var regionArray = [];
				regionsConfig.forEach(function(region){
					var currRegion = region._attributes.type;
					regionArray.push(currRegion.toLowerCase());
				});
				var regionOption=''
				var uniqueRegionArray = Array.from(new Set(regionArray));
				uniqueRegionArray.forEach(function(regionVal){
					var isSelected = '';
					regionVal = regionVal.toLowerCase();
					regionOption += '<option value="' + regionVal + '"' + isSelected + '>' + regionVal.toUpperCase() + '</option>'
				});
				$('#generateCombinePDF').find('select[id=region_option]').append(regionOption)
				$('#generateCombinePDF').modal('show');
			},
			populateBookMeta: function () {
				var meta = $('#manuscriptsData').data('binder')['container-xml'].meta;
				if ((typeof (meta) == 'undefined')) {
					return false;
				}
				$('#addProj [data-name]').val('');
				$('#addProj [data-cloned]').remove();
				for (dataName in meta) {
					if (meta[dataName].constructor.toString().indexOf("Array") != -1) {
						var dataValue = meta[dataName][0];
					} else {
						var dataValue = meta[dataName];
					}
					var component = $('#addProj [data-name=' + dataName + ']');
					if (component.length > 0) {
						if (component.length > 1 && dataValue._text && dataValue._attributes) {
							if (meta[dataName].constructor.toString().indexOf("Array") != -1) {
								dataValue = meta[dataName]
							} else {
								dataValue = [meta[dataName]];
							}
							dataValue.forEach(function (item) {
								var attrName = Object.keys(item._attributes);
								if (attrName.length == 1) {
									var attrValue = item._attributes[attrName[0]];
									component = $('#addProj [data-name=' + dataName + '][data-form-details-attr="' + attrName + '|' + attrValue + '"]')
								}
								if (component.length == 1 && item._text) {
									component.val(item._text)
								}
							});
						} else if (component.length == 1 && meta[dataName]._text) {
							component.val(meta[dataName]._text)
						}
					} else if (typeof (meta[dataName]) == 'object' && !meta[dataName]._text) {
						for (childName in meta[dataName]) {
							if (meta[dataName][childName].constructor.toString().indexOf("Array") != -1) {
								var dataValue = meta[dataName][childName];
							} else {
								var dataValue = [meta[dataName][childName]];
							}
							component = $('#addProj [data-parent-name=' + childName + ']');
							if (component.length > 1 && !dataValue[0]._text && dataValue[0]._attributes) {
								dataValue.forEach(function (childValue) {
									var attrName = Object.keys(childValue._attributes);
									if (attrName.length == 1) {
										var attrValue = childValue._attributes[attrName[0]];
										component = $('#addProj [data-parent-name=' + childName + '][data-form-details-attr="' + attrName + '|' + attrValue + '"][data-template]');
									}
									if (component.length == 1 && !childValue._text) {
										if (component.attr('data-template') == 'true') {
											clonedComponent = component.clone(true);
											component.parent().append(clonedComponent);
											component = clonedComponent.removeAttr('data-template').attr('data-cloned', 'true').removeClass('hide');
										}
										for (subChild in childValue) {
											if (subChild != '_attributes') {
												subComponent = component.find('[data-name="' + subChild + '"]');
												if (subComponent.length == 1 && childValue[subChild]._text) {
													subComponent.val(childValue[subChild]._text)
												}
											}
										}
									}
								});
							}
						}
					}
				}
				var proofConfigDetails = $('#manuscriptsData').data('profConfigDetails');
				if (proofConfigDetails) {
					var proofConfigProperty = proofConfigDetails["property"]
					if (proofConfigProperty) {
						if (proofConfigProperty.constructor.toString().indexOf("Array") < 0) {
							proofConfigProperty = [proofConfigProperty];
						}
						proofConfigProperty.forEach(function (property) {
							var current = property._attributes['data-name']
							var component = $('#addProj [data-name=' + current + '][data-proof-config-details]');
							if (component.length > 0) {
								$(component).html('')
								var defaultval = ''
								if (meta[current]) {
									defaultval = meta[current]._text
								}
								if (property['project']) {
									var projects = property['project']
									if (defaultval == '') {
										$(component).append(($('<option value=""></option>')))
									}
									if (projects.constructor.toString().indexOf("Array") < 0) {
										projects = [projects];
									}
									projects.forEach(function (project) {
										var name = project._attributes.name
										var val = project._attributes.value
										var isSelected = '';
										if (defaultval == val) {
											isSelected = ' selected="true"';
										}
										var element = $('<option value="' + val + '" ' + isSelected + '>' + name + '</option>')
										if ($(component).attr('data-siblings-name')) {
											var siblings = $(component).attr('data-siblings-name').split(',')
											siblings.forEach(function (sibling) {
												if (project._attributes[sibling] && project._attributes[sibling] != '') {
													$(element).attr(sibling, project._attributes[sibling])
												}
											})
										}
										//$(component).append(($('<option value="'+val +'" '+ isSelected+'>'+name +'</option>')))
										$(component).append($(element))
									})
								}
							}
						})
					}
				}
			},
			populateSectionCards: function (targetNode, customerType,param) {
				var sectionCards = $('#manuscriptsData').data('config')['section-cards'].card;
				if ((typeof (sectionCards) == 'undefined')) {
					return false;
				}
				if (sectionCards.constructor.toString().indexOf('Array') < 0) {
					sectionCards = [sectionCards];
				}
				var sectionCardParameters = [];
				var sectionConfig = sectionCards;
				if (sectionConfig.constructor.toString().indexOf("Array") < 0) {
					sectionConfig = [sectionConfig];
				}
				var scObj = {}, scIndex = 0;
				var sectionHTML = '';
				var tempID = '21000';
				var prevFlagSection = '';
				// scObj = sectionConfig[scIndex]
				sectionCards.forEach(function (scObj, scIndex) {
					var entryId = parseInt(tempID) + parseInt(scIndex)
					if (scObj) {
						var section = scObj._attributes['label'] ? scObj._attributes['label'] : '';
						var fpage = lpage = 1;
						var dataUuid = uuid.v4();
						var pageCount = lpage - fpage + 1
						var dataType = scObj._attributes['data-type']
						if (dataType && dataType != undefined && dataType.match('body')) {
							if (scObj._attributes.groupType == 'roman') {
								fpage = lpage = 'i'
								pageCount = eventHandler.flatplan.components.fromRoman(lpage) - eventHandler.flatplan.components.fromRoman(fpage) + 1
							}
							var cName = param.customer;
							var pName = param.projectName;
							var blankPdfPath = ''
							// var entry = ({"_attributes":{"id": entryId, "section": section, "grouptype": scObj._attributes.groupType,"type": scObj._attributes.type, "fpage": fpage,"lpage": lpage},"file":{"_attributes":{"proofType":"print","type":"pdf","path":blankPdfPath}},"title":{"_text":scObj._attributes['label']},"file":{"_attributes":{"proofType":"online","type":"pdf","path":blankPdfPath}}});
							var entry = ({ "_attributes": { "id": entryId, "data-uuid": dataUuid, "section": section, "grouptype": scObj._attributes.groupType, "type": scObj._attributes.type, "fpage": fpage, "lpage": lpage, "pageExtent": pageCount }, "title": { "_text": scObj._attributes['label'] } });
							var fileObj = []
							var file = ({ "_attributes": { "proofType": "online", "type": "pdf", "path": "" } })
							if (scObj._attributes.type == 'blank') {
								blankPdfPath = 'https://kriya2.kriyadocs.com/resources/' + cName + '/' + pName + '/resources/' + cName.toUpperCase() + '_' + pName.toUpperCase() + '_BLANK.pdf?method=server';
								file = ({ "_attributes": { "type": "pdf", "path": blankPdfPath } })
							}
							else if (scObj._attributes.type == 'advert' || scObj._attributes.type == 'advert_unpaginated') {
								file = '';
								if (scObj._attributes.type == 'advert_unpaginated') {
									entry._attributes['data-unpaginated'] = 'true';
								}
							}
							if (file != '') {
								entry['file'] = file
							}
							sectionCardParameters.push(entry);
						}
					}
					scIndex++;
				})

				var data = {};
				data.sectionCards = sectionCardParameters;
				//var pagefn = doT.template(document.getElementById('sectionDataTemplate').innerHTML, undefined, undefined);
				//$( '#sectionContent .innerCards').find('.uaa.card').remove();
				//$( '#sectionContent .innerCards').append(pagefn(data));
				// $( '#sectionContent .innerCards').css('height', window.innerHeight - $( '#sectionContent .innerCards').offset().top - $('#footerContainer').height() - 125 + 'px');
				//$( '#sectionContent .innerCards').css('height', (window.innerHeight - $( '#sectionContent .innerCards').offset().top - $('#footerContainer').height() - 15 ) + 'px');
				//$( '#sectionContent .innerCards').find('.uaa.card').each(function (i, scIndex) {
				//	$(this).data('data', sectionCardParameters[i]);
				//	$(this).attr('customer-type', customerType);
				//});
			},
			editBookMeta: function (param, targetNode) {
				$('#addProj .addProj').addClass('hide');
				$('#addProj .saveProj').removeClass('hide');
				$('#addProj input[data-validate="true"]').removeClass('is-invalid');
				$('#addProj').modal('show');
			},
			populateFlatPlan: function (param, targetNode) {
				//check if journal and display region option
				var regionVal;
				//if bookletRegionVal dropdown is available, get the choosen region and include the adverts that belongs to the choosen region
				if($(currentTab + '#bookletRegionVal').length >0){
					regionVal =$(currentTab + '#bookletRegionVal').find('select[id=booklet_region_option]').val().toLowerCase();
				}
				$(currentTab+'#manuscriptsDataContent #flatPlanBodyDiv').remove();
				$('[data-href="tocBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$('[data-href="flatPlanBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				var container = $('<div id="flatPlanBodyDiv" style="overflow:hidden;">');
				$(currentTab+'#manuscriptsDataContent').append(container);
				var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
				var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
				var fileName = $(currentTab + '#manuscriptsDataContent .newStatsRow').attr('data-file');
				var containerHTML = '<div id="sectionGroup" class="container"><div class="wrapper">';
				var content = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content;
				var imgArray = [];
				if (content) {
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					// push all pdfs in Imgarray,
					content.forEach(function (entry) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : '';
						var fpage = entry._attributes.fpage;
						var groupType = entry._attributes.grouptype
						var lpage = entry._attributes.lpage;
						var pdf = entry.file;
						var type = entry._attributes.type;
						var runOnflag = false;
						var srcId = id;
						var fileFlag = false
						var fileObj = entry['file'];
						if (fileObj) {
							if (fileObj.constructor.toString().indexOf("Array") == -1) {
								fileObj = [fileObj];
							}
							fileObj.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
								var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
								if (currProofType == 'print' && pdfType != 'preflightPdf' && file['_attributes']['path'] != '') {
									fileFlag = true
								}
							})
						}
						if(entry["region"]){
							var regObj = entry['region']
							if (regObj) {
								if (regObj.constructor.toString().indexOf("Array") == -1) {
									regObj = [regObj];
								}
								regObj.forEach(function (region) {
									if (region['file'] && regionVal!='' && region._attributes && region._attributes.type && regionVal == region._attributes.type.toLowerCase()) {
										var fileObj = region['file'];
										if (fileObj.constructor.toString().indexOf("Array") == -1) {
											fileObj = [fileObj];
										}
										srcId = region._attributes['data-proof-doi'] ? region._attributes['data-proof-doi'] : fileName+'_'+id+'_'+regionVal.toUpperCase();
										fileObj.forEach(function (file) {
											var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
											if (currProofType == 'print' && file['_attributes']['path']!='') {
												fileFlag = true;
											}
										});
									}
								})
							}
						}
						if (type == 'cover' || type == 'table-of-contents' || type == 'index') {
							srcId = entry._attributes['data-proof-doi'] ? entry._attributes['data-proof-doi'] : id
						}
						if (entry._attributes['data-run-on'] && entry._attributes['data-run-on'] == 'true') {
							runOnflag = true
						}
						//get the pageExtent of each content,  push each page in imgArray
						if (pageExtent && runOnflag == false) {
							var currentPage = 0;
							var ePageFlag = false;
							var romanFlag = false;
							if (/(c)$/gi.test(fpage) || pageExtent == 1) {
								var currentPage = fpage
								var pagneNum = eventHandler.flatplan.components.pad(1, 3)
								var src = '/resources/' + customer + '/' + project + '/' + srcId + '_print/resources/pdfThumbnails/page_' + pagneNum + '.png?method=server'
								if (!(/^(kriya|demo)/gi.test(window.location.host))) {
									src = '/resources/' + customer + '/' + project + '/' + srcId + '_print/resources/' + window.location.host + '/pdfThumbnails/page_' + pagneNum + '.png?method=server'
								}
								var altSrc = src
								if (fileFlag == false && !(entry["region"])) {
									src = '../images/NoImageFound.jpg'
								}
								var img = ({ "_attributes": { "src": src, "alt": altSrc, "data-type": type, "data-section": entry._attributes.section, "data-current-page": currentPage, "id": id } })
								imgArray.push(img);
							}
							else {
								//set epage flag
								if (/(e)/gi.test(fpage)) {
									fpage = fpage.replace('e', '')
									lpage = lpage.replace('e', '')
									ePageFlag = true
								}
								else if ((groupType != '' && groupType != undefined && groupType == 'roman' && typeof fpage != "number") || (type == 'table-of-contents' && typeof fpage != "number")) {
									fpage = eventHandler.flatplan.components.fromRoman(fpage)
									//roman set roman flag
									romanFlag = true
								}
								fpage = parseInt(fpage);
								//parseint fpage ,lpage

								for (var i = 0; i < pageExtent; i++) {
									var currentPage = fpage + i
									var pagneNum = eventHandler.flatplan.components.pad(i + 1, 3)
									if (romanFlag == true) {
										currentPage = eventHandler.flatplan.components.toRoman(currentPage)
									}
									else if (ePageFlag == true) {
										currentPage = 'e'.concat(currentPage)
									}
									var src = '/resources/' + customer + '/' + project + '/' + srcId + '_print/resources/pdfThumbnails/page_' + pagneNum + '.png?method=server'
									if (!(/^(kriya|demo)/gi.test(window.location.host))) {
										src = '/resources/' + customer + '/' + project + '/' + srcId + '_print/resources/' + window.location.host + '/pdfThumbnails/page_' + pagneNum + '.png?method=server'
									}
									var altSrc = src
									if (fileFlag == false && !(entry["region"])) {
										src = '../images/NoImageFound.jpg'
									}
									var img = ({ "_attributes": { "src": src, "alt": altSrc, "data-section": entry._attributes.section, "data-current-page": currentPage, "id": id } })
									imgArray.push(img)
									//   currIndex++;
								}
							}
						}
					});
				}
				var loadingGIF = $('<img class="col" src="../images/loading.gif">')
				var currIndex = 1;
				var openDiv = '<div style="height:100%">'
				var closeDiv = '</div>'
				var coverflag = false;
				$('#myModal1').remove();
				var bookletHTML = '<div class="modal" id="myModal1" role="dialog"><button type="button" style="float:right" class="btn btn-default" data-dismiss="modal">Close</button><div id="mybook">'
				if (imgArray) {
					imgArray.forEach(function (img, currIndex) {
						var refID = img._attributes.id.replace(/\./gi, '')
						if (currIndex == 0) {
							if (img._attributes['data-type'] && (/(cover)/gi.test(img._attributes['data-type']))) {
								containerHTML += '<div><p class="imgblock"  style="float:right;display:grid;margin-left:50%"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + currIndex + ')" style="height:130px;float:right;width:100%;border-right: 1px solid grey;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p></div>';
								coverflag = true
							}
							else {
								containerHTML += '<div style="height:100%"><p class="imgblock"  style="float:right;width:50%;display:grid;margin-left:50%" onClick="eventHandler.flatplan.components.populateBooklet(' + currIndex + ')"><span style="height: 130px;font-size: large;border: 1px solid black;padding: 40px 20px 20px 20px;"><span>COVER</span></span><span class="fpRange">C1</span></p></div>';
								var temp = parseInt(currIndex) + 1

								containerHTML += '<div><p class="imgblock"  style="float:left;width:50%;display:grid;"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + temp + ')" style="height:130px;float:right;padding:0px;width:100%;border-right: 1px solid grey;background: transparent url(../images/loading.gif) center no-repeat;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p>';
							}
						}
						if (currIndex == 0 && coverflag == false) {
							bookletHTML += openDiv + '<p style="margin: 35%;font-size: -webkit-xxx-large;">COVER</p>' + closeDiv
							bookletHTML += openDiv + '<img  class="img-responsive" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" style="padding:0px;height:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/>' + closeDiv
						}
						else {
							bookletHTML += openDiv + '<img  class="img-responsive" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" style="height:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/>' + closeDiv

						}
						if (coverflag == false) {
							if (currIndex != 0) {
								var temp = parseInt(currIndex) + 1
								if (currIndex % 2 != 0) {
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + temp + ')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p>'
									containerHTML += closeDiv
								}
								else {
									containerHTML += openDiv
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + temp + ')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p>'
								}
							}
						}
						else {
							if (currIndex != 0) {
								if (currIndex % 2 != 0) {
									containerHTML += openDiv
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + currIndex + ')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p>'
								}
								else {
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="' + refID + '" data-id="' + refID + '" data-current-page="' + img._attributes['data-current-page'] + '" onClick="eventHandler.flatplan.components.populateBooklet(' + currIndex + ')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="' + img._attributes.src + '" alt="' + img._attributes.alt + '" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">' + img._attributes['data-current-page'] + '</span></p>'
									containerHTML += closeDiv
								}
							}
						}
						currIndex++;
					})
				}
				containerHTML += '</div></div>'
				bookletHTML += '</div></div>'
				container.append(containerHTML);
				$('body').append(bookletHTML)
				//$(currentTab + '#flatPlanBodyDiv').css('max-height', (window.innerHeight - ($(currentTab + '#flatPlanBodyDiv').offset().top )- $('#footerContainer').height()) + 'px');
				$(currentTab + '#manuscriptsDataContent .sectionList').addClass('hidden');
				$(currentTab + '#flatPlanBodyDiv #sectionGroup').css('max-height', (window.innerHeight - ($(currentTab + '#flatPlanBodyDiv #sectionGroup').offset().top )- $('#footerContainer').height()) + 'px');
			},
			/* 	populate all contents from issueXML
				group contents by articles , Asverts/Fillers
				onClick of a content, scrolls to the corresponding pdf in flatplan
			*/
			populateContents: function () {
				var content = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content;
				var prevSection = '';
				var container = $(currentTab + '#contentsList .bodyDiv #artListTab');
				container.html('');
				var containerHTML = '';
				var sectionIndex = 1;
				var tempID = -1;
				var currIndex = 0;
				tableHTML = "<table id='flatplanTable' class='table table-striped'><thead><tr><th colspan='2'>";
				var type = $(currentTab + '#filterCustomer .filter-list.active').attr('data-type');
				if (type && type.match(/book/gi)) {
					containerHTML += '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Chapters</div>'
					tableHTML += 'Chapters';
				} else {
					containerHTML += '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Articles</div>'
					tableHTML += 'Articles';
				}
				tableHTML += '</th></tr></thead>';
				// list all articles, cover,toc
				if (content) {
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry, currIndex) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var type = entry._attributes.type;
						var child = false;
						var doi = entry._attributes.id
						doi = doi ? "'".concat(doi).concat("'") : ''
						doi = doi.replace(/\./gi, '')
						var colStart = '<div class="col-sm-12 col-md-12 col-lg-12" onClick="goToView(' + doi + ')">';
						var closeDiv = '</div>';
						tableHTML += '<tr data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'goToView\',\'param\': '+doi+'}">';
						if (entry._attributes && entry._attributes.type && (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)))) {
							dataType = ' data-type="' + entry._attributes.type + '"';
							containerHTML += colStart
							var title = entry.title ? entry.title._text : entry._attributes.type
							var currentId = entry._attributes.id;
							if (entry._attributes && entry._attributes.type && ((/^(cover|table\-of\-contents|editorial\-board)$/gi.test(entry._attributes.type)))) {
								currentId = entry._attributes.section
							}

							containerHTML += '<p  class="col-sm-8 col-md-8 col-lg-8"  rid="' + currentId + '" >' + currentId + '</p>'
							tableHTML += '<td rid="' + currentId + '" >' + currentId + '</td><td>' + entry._attributes.fpage;
							containerHTML += '<p  class="col-sm-4 col-md-4 col-lg-4" >' + entry._attributes.fpage
							if (entry._attributes.lpage && entry._attributes.fpage != entry._attributes.lpage) {
								containerHTML += '&#x2013;' + entry._attributes.lpage;
								tableHTML += '&#x2013;' + entry._attributes.lpage
							}
							tableHTML += '</td></tr>';
							containerHTML += '</p>'
							containerHTML += closeDiv
							currIndex++;
						}
					});
				}
				tableHTML += '</tbody></table>';
				containerHTML = '<div class="col-sm-12 col-md-12 col-lg-12 contentHead">Adverts/Fillers</div>'
				if (content) {
					tableHTML += "<table class='table table-striped'><thead><tr><th colspan='2'>Adverts/Fillers</th></tr></thead>";
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry, currIndex) {
						if (entry._attributes && entry._attributes.type && ((/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(entry._attributes.type)))) {
							var colStart = '<div class="col-sm-12 col-md-12 col-lg-12" >';
							var closeDiv = '</div>';
							containerHTML += colStart;
							var title = entry.title ? entry.title._text : entry._attributes.type
							var currentId = entry._attributes.id ? entry._attributes.id.replace(/\./gi, '') : '';
							tableHTML += '<tr data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'goToView\',\'param\': ' + currentId + '}">';
							containerHTML += '<p  class="col-sm-8 col-md-8 col-lg-8"  rid="' + currentId + '" onClick="goToView(' + currentId + ')">' + currentId + '</p>'
							containerHTML += '<p  class="col-sm-4 col-md-4 col-lg-4" >' + entry._attributes.fpage
							tableHTML += '<td>' + currentId + '</td>';
							tableHTML += '<td  class="col-sm-4 col-md-4 col-lg-4" >' + entry._attributes.fpage
							if (entry._attributes.lpage && entry._attributes.fpage != entry._attributes.lpage) {
								containerHTML += '&#x2013;' + entry._attributes.lpage;
								tableHTML += '&#x2013;' + entry._attributes.lpage
							}
							tableHTML += '</tr>';
							containerHTML += '</p>'
							containerHTML += closeDiv
						}
					})
					tableHTML += '</tbody></table>';
				}
				container.append(tableHTML);
				$(currentTab+"#flatplanSearchInput").unbind();
				$(currentTab+"#flatplanSearchInput").on("keyup", function() {
					var value = $(this).val().toLowerCase();
					$("#artListTab tr").filter(function() {
						$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
					});
				});
				$(currentTab + '#contentsList #artListTab').css('height', $(currentTab + '#contentsList .bodyDiv').height());
			},
			populateBooklet: function (startPage) {
				var w = window.innerWidth;
				var h = window.innerHeight;
				//console.log("Width: " + w + "<br>Height: " + h);
				w = parseInt(w) - 400;
				h = parseInt(h) - 10;
				$('#myModal1').modal('show')
				$('#mybook').booklet({
					pagePadding: 0,
					overlays: true,
					manual: false,
					pageNumbers: false,
					closed: true,
					startingPage: startPage + 1,
					height: h,
					width: w,
					speed: 1000,
					shadows: true
				});
				/*$('#myModal1').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"light-thick"
				});*/
			},
			closeBooklet: function () {
				$('#myModal1').css('display', 'none')
				$('#myModal1').css('padding-left', '0px')
				$('#myModal1').removeClass('in')
			},
			updatePageNumber: function (startNodeIndex, startPage, groupType, callBack) {
				if((!startPage || startPage==undefined || startPage==NaN) && (groupType=='body')){
					startPage = 1;
				}
				var configData = $(currentTab+'#manuscriptsData').data('config');
				var firstNode = $(currentTab+'#manuscriptsData *[data-grouptype="body"][data-c-id]:first');
				var firstNodeID = parseInt(firstNode.attr('data-c-id'));
				var lastNode = $(currentTab+'#manuscriptsData *[data-grouptype="body"][data-c-id]:last');
				var lastNodeID = parseInt(lastNode.attr('data-c-id'));
				startNodeIndex = parseInt(startNodeIndex);
				if (/^(e)/gi.test(startPage)) {
					startPage = startPage.replace('e', '');
				}
				else if (groupType != '' && groupType != undefined && groupType == 'roman' && typeof startPage != "number") {
					startPage = eventHandler.flatplan.components.fromRoman(startPage);
				}
				startPage = parseInt(startPage);
				//get pagecount of front contents
				var frontContents = $(currentTab+'#manuscriptsData *[data-c-id][data-grouptype="front"]');
				var frontContentsCount = parseInt(0);
				frontContents.each(function () {
					var currFrontNode = $(this);
					var currFrontNodeID = parseInt(currFrontNode.attr('data-c-id'));
					var currFrontNodeData = currFrontNode.data('data');
					var pageExtent = parseInt(currFrontNodeData._attributes.pageExtent);
					frontContentsCount += pageExtent;
				});
				var datagroupType = '';
				if (groupType != '' && groupType != undefined) {
					datagroupType = "[data-grouptype='" + groupType + "']";
				}
				/* if group type is roman, update only the articles/adverts with same sectionhead*/
				if (groupType != '' && groupType != undefined && groupType == 'roman') {
					var current = $('[data-c-id="' + (startNodeIndex) + '"]');
					var currentData = $(current).data('data')._attributes;
					var section = currentData["section"]
					if (section != '' && section != undefined) {
						datagroupType += "[data-section='" + section + "']";
					}
				}
				var nodesToUpdate = $(currentTab + "#tocContentDiv [data-c-id][data-type!='advert_unpaginated'][data-run-on!='true']" + datagroupType).filter(function () {
					return parseInt($(this).attr("data-c-id")) >= startNodeIndex;
				});
				nodesToUpdate.each(function () {
					var currNode = $(this);
					var currNodeID = parseInt(currNode.attr('data-c-id'));
					var currNodeData = currNode.data('data');
					try {
						var fpage = currNodeData._attributes.fpage;
						var lpage = currNodeData._attributes.lpage;
						if (/^(e)/gi.test(fpage)) {
							fpage = fpage.replace('e', '');
						}
						if (/^(e)/gi.test(lpage)) {
							lpage = lpage.replace('e', '');
						}
						if (groupType == 'roman' && typeof fpage != "number") {
							fpage = eventHandler.flatplan.components.fromRoman(fpage)
							lpage = lpage ? eventHandler.flatplan.components.fromRoman(lpage) : lpage
						}
						fpage = parseInt(fpage);
						lpage = parseInt(lpage);
						lpage = lpage ? lpage : fpage;
						var pageCount = lpage - fpage + 1;
						if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
							pageCount = currNodeData._attributes['pageExtent'];
						}
						pageCount = parseInt(pageCount);
						var runonFlag = currNodeData._attributes['data-run-on'];
						runonFlag = runonFlag ? runonFlag : 'false';
						currNodeData._attributes['pageExtent'] = pageCount;
						/*if (fpage == startPage){
							return true;
						}*/
						//if (currNodeData._attributes.type == 'manuscript'){
						if (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(currNodeData._attributes.type))) {
							// if fresh-recto and pagecount of front contents is odd, return true for first article
							if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (currNodeID == firstNodeID) && (frontContentsCount % 2 != 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
							// if the configuration says fresh-recto is true, then startPage should always start on odd page, if not show error
							else if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (startPage % 2 == 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								$(currNode).find('.pageRange').attr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
							else if ((configData['page']) && configData['page']._attributes['type'].toLowerCase() == 'continuous-publication') {
								if ((configData['page']._attributes['first-page'] == 'recto')) {
									// if first page is recto  and pagecount of front contents is odd, return true for first article
									if ((currNodeID == firstNodeID) && (frontContentsCount % 2 != 0)) {
										$(currNode).find('.pageRange').addClass('incorrectPage');
										$(currNode).find('.pageRange').attr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'true';
									}
									//if the configuration says continuous-publication and first page is recto , first page should always starts on odd page, if not show error
									else if ((currNodeID == firstNodeID) && (startPage % 2 == 0)) {
										$(currNode).find('.pageRange').addClass('incorrectPage');
										$(currNode).find('.pageRange').attr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'true';
									}
									else {
										$(currNode).find('.pageRange').removeClass('incorrectPage');
										$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'false';
									}
								}
								else {
									//if the configuration says continuous-publication and first page is not recto , page can start in odd or even
									$(currNode).find('.pageRange').removeClass('incorrectPage');
									$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
									currNodeData._attributes.incorrectPage = 'false';
								}
							}
							else {
								$(currNode).find('.pageRange').removeClass('incorrectPage');
								$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'false';
							}
						}

						if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
							currNodeData._attributes.fpage = 'e'.concat(startPage);
							var endPage = startPage + pageCount - 1;
							// if config data says e-lpage as false, set fpage and lpage as same
							if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')) {
								endPage = startPage;
							}
							currNodeData._attributes.lpage = 'e'.concat(endPage);
						}
						else {
							var endPage = startPage + pageCount - 1;
							currNodeData._attributes.fpage = startPage;
							currNodeData._attributes.lpage = endPage;
							if (groupType == 'roman' && typeof startPage == "number") {
								currNodeData._attributes.fpage = eventHandler.flatplan.components.toRoman(startPage);
								currNodeData._attributes.lpage = eventHandler.flatplan.components.toRoman(endPage);

							}
						}
						var unpaginated = currNodeData._attributes['data-unpaginated'];
						unpaginated = unpaginated ? unpaginated : 'false';
						// end page of last artricle should always be even
						if (unpaginated != 'true') {
							if ((currNodeID == lastNodeID) && (endPage % 2 != 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								$(currNode).find('.pageRange').attr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
						}
						//if page range is modified, add class pageModified
						//if(currNode.find('.rangeStart').text() != startPage || currNode.find('.rangeEnd').text() != endPage){
						if (currNode.find('.pageRange').attr('data-fpage') != startPage || currNode.find('.pageRange ').attr('data-lpage') != endPage) {
							if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
								currNode.find('.rangeStart').text('e'.concat(startPage));
								currNode.find('.rangeEnd').text('e'.concat(endPage));
								currNode.attr('data-modified', 'true');
								currNodeData._attributes['data-modified'] = 'true';
								$(currNode).find('.pageRange').attr('title', 'pageModified');
								currNode.find('.pageRange').attr('data-fpage', 'e'.concat(startPage));
								currNode.find('.pageRange').attr('data-lpage', 'e'.concat(endPage));

							}
							else {
								currNode.find('.rangeStart').text(startPage);
								currNode.find('.rangeEnd').text(endPage);
								currNode.find('.pageRange').attr('data-fpage', startPage);
								currNode.find('.pageRange').attr('data-lpage', endPage);
								if (groupType == 'roman' && typeof startPage == "number") {
									currNode.find('.rangeStart').text(eventHandler.flatplan.components.toRoman(startPage));
									currNode.find('.rangeEnd').text(eventHandler.flatplan.components.toRoman(endPage));
									currNode.find('.pageRange').attr('data-fpage', eventHandler.flatplan.components.toRoman(startPage));
									currNode.find('.pageRange').attr('data-lpage', eventHandler.flatplan.components.toRoman(endPage));

								}
								if (unpaginated != 'true' && currNodeData._attributes && currNodeData._attributes.type != 'blank') {
									currNode.attr('data-modified', 'true');
									$(currNode).find('.pageRange').addClass('pageModified');
									$(currNode).find('.pageRange').attr('title', 'Page is Modified');
									currNodeData._attributes['data-modified'] = 'true';
								}
							}
						}
						var nextNodeData = $(currentTab + '[data-c-id="' + (parseInt(currNode.attr('data-c-id')) + 1) + '"]').data('data');
						var nextRunOn = (nextNodeData && nextNodeData._attributes && nextNodeData._attributes['data-run-on']) ? nextNodeData._attributes['data-run-on'] : '';
						nextRunOn = nextRunOn ? nextRunOn : 'false';
						var nextUnpaginated = (nextNodeData && nextNodeData._attributes && nextNodeData._attributes['data-unpaginated']) ? nextNodeData._attributes['data-unpaginated'] : '';
						nextUnpaginated = nextUnpaginated ? nextUnpaginated : 'false';

						startPage = endPage + 1    // next manuscript should start from next page

						return true;
					}
					catch (e) {
						return false;
					}
				});
				var chapterNumberIndex = 1;
				var chapterNumberReordered = false;
				$(currentTab+'#manuscriptsData *.msCard[data-grouptype="body"][data-c-id][data-on-reorder="changeChapterNumber"]').each(function () {
					var msChNo = $(this).find('.msChNo');
					var cid = $(this).attr('data-c-id');
					var chDOI = $(this).attr('data-doi');
					if (chapterNumberIndex != msChNo.text()) {
						var chData = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[cid];
						if (chData && chData._attributes && chData._attributes.id && chData._attributes.id == chDOI && chData._attributes.chapterNumber && chData._attributes.chapterNumber == parseInt(msChNo.text())) {
							chapterNumberReordered = true;
						}
					}
					chapterNumberIndex++;
				});
				if (chapterNumberReordered) {
					(new PNotify({
						title: 'Confirmation Needed',
						text: 'Chapters are re-ordered. Do you want to re-order it?',
						hide: false,
						confirm: { confirm: true },
						buttons: { closer: false, sticker: false },
						history: { history: false },
						addclass: 'stack-modal',
						stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
					})).get()
						.on('pnotify.confirm', function () {
							eventHandler.flatplan.components.reorderChapterNumbers()
						})
						.on('pnotify.cancel', function () {

						});
				}
				eventHandler.flatplan.components.updateTotalPages();
				eventHandler.flatplan.action.saveData(callBack);
				var date = new Date()
				$('.save-notice').text('All changes saved on' + dateFormat(date, 'mm-dd-yyyy hh:mm:ss'));
				//calculate total pages
			},
			/*  calculate - total page number add the extent of articles,adverts,fillers*/
			updateTotalPages: function () {
				//get all contents - add pageExtent for each contents
				var totalPages = parseInt(0);
				var content = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content;
				if (content) {
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry) {
						if (entry) {
							var grouptype = entry._attributes.grouptype ? entry._attributes.grouptype : '';
							var section = entry._attributes.section ? entry._attributes.section : '';
							var runOn = entry._attributes['data-run-on'] ? entry._attributes['data-run-on'] : 'false';
							if (!(/^(cover|ifc|ibc|obc)/gi.test(section)) && runOn == 'false' && grouptype != 'epage') {
								var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : parseInt(0);
								pageExtent = parseInt(pageExtent);
								totalPages += pageExtent;
							}
						}
					});
				}
				//update the total page number value in MetaData
				var meta = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].meta;
				var pagecount = meta["page-count"] ? meta["page-count"]['_text'] : parseInt(0);
				if (totalPages != null && totalPages != 0 && meta["page-count"]) {
					meta["page-count"]['_text'] = totalPages
				}
				// update last page number in meta info
				var lastNode = $(currentTab + '#manuscriptsData [data-c-id][data-type="manuscript"][data-grouptype="body"][data-run-on!="true"]:last')
				var lastNodeData;
				if (lastNode && $(lastNode).data('data') && $(lastNode).data('data')._attributes) {
					lastNodeData = $(lastNode).data('data')._attributes
				}
				else {
					$(lastNode)._attributes
				}
				if (lastNodeData && meta["last-page"]) {
					var lastNodeLPage = lastNodeData.lpage
					meta["last-page"]['_text'] = lastNodeLPage
				}
			},
			reorderChapterNumbers: function () {
				var chapterNumberIndex = 1;
				$(currentTab + '#manuscriptsData *.msCard[data-grouptype="body"][data-c-id][data-on-reorder="changeChapterNumber"]').each(function () {
					var msChNo = $(this).find('.msChNo');
					var cid = $(this).attr('data-c-id');
					var chDOI = $(this).attr('data-doi');
					if (chapterNumberIndex != msChNo.text()) {
						var chData = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[cid];
						if (chData._attributes.id == chDOI && chData._attributes.chapterNumber == parseInt(msChNo.text())) {
							$(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[cid]._attributes.chapterNumber = chapterNumberIndex;
							$(this).find('.msChNo').text(chapterNumberIndex)
							var param = {
								'customer': $('#filterCustomer .filter-list.active').attr('value'),
								'project': $('#filterProject .filter-list.active').attr('value'),
								'doi': chData._attributes.id,
								'xpath': '//front//title-group/label',
								'data': '<label>' + chapterNumberIndex + '</label>'
							}
							eventHandler.flatplan.components.updateArticleData(param)
						}
					}
					chapterNumberIndex++;
				});
				eventHandler.flatplan.action.saveData();
			},
			updateArticleData: function (param, targetNode) {
				$.ajax({
					type: "POST",
					url: "/api/updatedata",
					data: {
						'customerName': param.customer,
						'projectName': param.project,
						'doi': param.doi,
						'data': {
							'process': "update",
							'update': param.xpath,
							'content': param.data
						}
					},
					success: function (res) {

					},
					error: function (err) {

					}
				});
			},
			openFileUploadModal: function (e) {
				$('#upload_model').remove();
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var doi = $('#rightPanelContainer').attr('data-dc-doi');
				var uploadType = 'File';
				var successFunction = 'eventHandler.flatplan.components.saveUploadedFile';
				if (typeof (doi) == "undefined") return false;
				$("body").append('<div id="upload_model" id="upload_modal" role="document" data-customer="' + customer + '" data-project="' + project + '" data-doi="' + doi + '" data-upload-type="' + uploadType + '" data-success="' + successFunction + '" class="modal comments-modal-upload"><div class="modal-dialog modal-dialog-centered modal-upload"><div class="modal-content"><div class="modal-header"><h3 class="uploadHeader">Upload ' + uploadType + ' for ' + doi + '<span style="margin-left: 20px;font-size: 12px;background: #fafafa;padding: 3px 10px;color: #313131;border-radius: 6px;display:none;" class="notify"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div id="uploader" class="fileHolder"><div class="fileList" style="margin: 10px;text-align:center;"></div><div class="i-note"><p style="opacity: 0.6;">Drag and drop Files here to Upload</p><p><span class="btn btn-small btn-info" style="padding: 0px !important;"><input type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser" style="padding:0.375rem 0.75rem;">Or Select Files to Upload</span></span></p></div></div><div class="modal-caption hidden" data-type="Striking Image" contenteditable="true">metacontent</div><div class="modal-footer"><span class="btn btn-small btn-success upload_button save disabled highlight-background" data-channel="components" data-topic="actionitems" data-event="click" data-success="eventHandler.components.actionitems.attachFileToNotes" data-upload-type="Files" data-message="{\'funcToCall\': \'uploadFile\', \'param\':this}"><i class="fa fa-upload"> upload</i></span></div></div></div></div>');
				var data = {
					proofType: $(e).attr('data-proofType'),
					node: e
				}
				$('#upload_model').data(data);
				$('#upload_model').modal();
				tempFileList = {};
			},
			saveUploadedFile: function (resp, targetNode) {
			//	$('.la-container').fadeOut();
				var htmlID = 'upload_model';
				if (resp && resp.htmlID) {
					htmlID = resp.htmlID;
				}
				$('#' + htmlID ).modal('hide');
				var fileObj = resp.fileDetailsObj;
				// var keyPath = "' + compObj.key + '";
				var keyPath = $(targetNode).attr('data-key-path');
				var advertLayoutSec = $(targetNode).attr('data-layout-sec');
				if (advertLayoutSec) {
					keyPath = '';
				}
				// var manualUpload = "file._attributes.data-manualUpload";
				var manualUpload = $(targetNode).attr('data-manualupload');
				var proofType = ($('#' + htmlID).data().proofType) ? $('#' + htmlID).data().proofType : '';
				if (proofType == '' || proofType == undefined) {
					proofType = $(targetNode).attr('data-prooftype');
				}
				var currRegionVal = $(targetNode).closest('div').parent().attr('data-region-id');
				var currLayoutVal = $(targetNode).closest('div').parent().attr('data-layout');
				if (currRegionVal == undefined) {
					currRegionVal = $(targetNode).parents('[data-region-config="true"]').find('.layoutBlock').attr('data-region-id')
				}
				if (currLayoutVal == undefined) {
					currLayoutVal = $(targetNode).parents('[data-region-config="true"]').find('.layoutBlock').attr('data-layout')
				}
				if (currRegionVal) {
					currRegionVal = currRegionVal.toLowerCase();
				}
				var fileEle = $('#' + htmlID).data().node;
				// var keyPath = $(targetNode).attr('data-key-path');
				if (resp.status.code == 200) {
					$(currentTab + '.save-notice').text(eventHandler.flatplan.successCode[200][1]);
					var filePath = '/resources/' + resp.fileDetailsObj.dstKey;
					var fileExtension = "pdf"
					if ($('.loading-status').parents('.tabContent').length > 0 && $('.loading-status').parents('.tabContent').find('object').length > 0 && proofType == 'print') {
						$('.loading-status').parents('.tabContent').find('object').attr('data', filePath)

					}
					$('.loading-status').remove();
					if (keyPath) {
						var dataCID = $(currentTab + '#rightPanelContainer').attr('data-c-rid');
						// get data from the corresponding article
						var myData = $(currentTab + '#manuscriptsData [data-c-id="' + dataCID + '"]').data('data');
						var uuIdVal = $(currentTab+'#rightPanelContainer').attr('data-r-uuid');
						// if the type is manuscript and the uploaded file is xml , then save the xml in AIP
						if ((myData._attributes) && (myData._attributes.type) && (myData._attributes.type == 'manuscript')) {
							var fileExtension = filePath.split('.');
							fileExtension = fileExtension.pop();
							if (fileExtension == 'xml') {
								$('.la-container').fadeOut();
								if (typeof (FileReader) != "undefined") {
									var reader = new FileReader();
									reader.onload = function (e) {
										//var xmlDoc = $.parseXML(e.target.result);
										eventHandler.flatplan.action.saveXML('manuscript', e.target.result)
									}
									reader.readAsText(fileObj);
								}

							}
							if (fileExtension == 'pdf') {
								var fileFlag = false;
								var files = myData['file']
								if(files){
									if (files.constructor.toString().indexOf("Array") < 0) {
										files = [files];
									}
									files.forEach(function (file) {
										var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
										if (currProofType == proofType) {
											file['_attributes']['path'] = filePath;
											fileFlag = true
										}
									})
								}
								else{
									files=[];
								}
								if (fileFlag == false) {
									var file = ({ "_attributes": { "type": "pdf", "path": filePath, "proofType": proofType } })
									files.push(file)
									myData['file'] = files;
								}
								if ($(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').length > 0) {
									$(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').find('ul li[data-proofType="' + proofType + '"] a').attr('target', '_blank');
									$(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').find('ul li[data-proofType="' + proofType + '"] a').attr('data-href', filePath);
								}
								if (proofType == 'print') {
									$('.la-container').fadeOut();
									if ($(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').length > 0) {
										$(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').find('object').attr('data', filePath);
										$(currentTab + '#rightPanelContainer[data-r-uuid="' + uuIdVal + '"]').find('object').css('display', 'block');
									}
									//on upload of manuscript pdf, get pageExtent from user as input, and update pageNumber
									var notify = new PNotify({
										text: '<div id="notifyForm" class="pageExtentText"><div class="pageExtent"><p>Type the page extent for the uploaded pdf</p><input class="pageExtent_update" type="text"/><input class=" pf-button btn btn-small" style="margin-right: 10px;" type="button" name="pageExtent_update" value="Save"/></div></div>',
										icon: false,
										width: 'auto',
										hide: false,
										buttons: {
											closer: false,
											sticker: false
										},
										addclass: 'stack-modal',
										stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
										insert_brs: false
									});
									notify.get().find('.pageExtentText').on('click', '[name=cancel]', function () {
										notify.remove();
									})
									notify.get().find('.pageExtentText').on('click', '[name=pageExtent_update]', function () {
										var pageExtent = $('.pageExtentText').find('input[class=pageExtent_update]').val()
										var startPage = parseInt(myData._attributes.fpage)
										var endPage = startPage + parseInt(pageExtent) - 1;
										myData._attributes.pageExtent = pageExtent;
										myData._attributes.lpage = parseInt(endPage);
										eventHandler.flatplan.action.saveData();
										var prevNode = $(currentTab + '[data-c-id="' + (dataCID - 1) + '"]');
										if (prevNode) {
											var prevNodeData = $(prevNode).data('data');
											//if prevNode groupType is same get fpage as startpage
											if (prevNodeData && prevNodeData._attributes && prevNodeData._attributes.type == 'manuscript') {
												startPage = parseInt(prevNodeData._attributes.lpage) + 1;
											}
										}
										if(!myData._attributes['data-run-on'] || (myData._attributes['data-run-on'] && myData._attributes['data-run-on']!='true')){
											eventHandler.flatplan.components.updatePageNumber(dataCID, startPage, myData._attributes.grouptype);
										}
										notify.remove();
										//  eventHandler.general.actions.saveData();
										$(fileEle).closest('div').find('object').attr('data', filePath);
										var preflightProfile='';
										var configData = $(currentTab+'#manuscriptsData').data('config');
										if (configData["binder"]["manuscript"] && configData["binder"]["manuscript"]['_attributes'] && configData["binder"]["manuscript"]['_attributes']['data-preflight-profile']) {
											preflightProfile = configData["binder"]["manuscript"]['_attributes']['data-preflight-profile']
										}
										if (preflightProfile && preflightProfile!='') {
											var param = { "processType": "PDFPreflight", "pdfLink": filePath }
											param.profile = preflightProfile;
											//on upload of print pdf, trigger preflight
											eventHandler.flatplan.action.saveData();
											eventHandler.flatplan.export.exportToPDF(param, targetNode);
										}
									})
								}
							}
						}
						else if (((myData._attributes) && (myData._attributes.type) && (myData._attributes.type == 'front-matter')) || (fileExtension == 'pdf' && $('#filterCustomer .active').attr('data-type') == 'book') || (fileExtension == 'pdf' && proofType && proofType != '')) {
							$('.la-container').fadeOut();
							var files = myData['file']
							var pdfFileFlag = false
							if (files.constructor.toString().indexOf("Array") < 0) {
								files = [files];
							}
							files.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
								if (currProofType == proofType) {
									file['_attributes']['path'] = filePath;
									pdfFileFlag = true
								}
							})
							if (pdfFileFlag == false) {
								var file = ({ "_attributes": { "type": "pdf", "path": filePath, "proofType": proofType } })
								files.push(file)
								myData['file'] = files;
							}
							$(currentTab + '#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data', myData);
							eventHandler.flatplan.action.saveData();
							var cardType = $(targetNode).attr('data-type');
							if (cardType == undefined || cardType == '') {
								cardType = 'sectionCard';
							}
							if (proofType && !proofType.match(/online/gi)) {
								$(currentTab + '[data-type*=" ' + cardType + ' "] object').attr('data', filePath);
								$(currentTab + '[data-type*=" ' + cardType + ' "] object').css('height', window.innerHeight - $('[data-type*=" ' + cardType + ' "] object').offset().top - $('#footerContainer').height() - 15 + 'px');
								$(currentTab + '[data-type*=" ' + cardType + ' "] object').show();
								$(currentTab + '[data-type*=" ' + cardType + ' "] .noPDF').hide();
							}
						}
						else {
							$('.la-container').fadeOut();
							var keyPathArr = keyPath.split('.');
							// the last key is required to update the data, if you include the key directly then we will get only the value
							var lastKey = keyPathArr.pop();
							keyPathArr.forEach(function (key) {
								if (myData[key]) {
									myData = myData[key];
								}
								else {
									myData[key] = {}
									myData = myData[key]
								}
							})
							myData[lastKey] = filePath;
							if (manualUpload) {
								var myData = $(currentTab + '#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data');
								if (myData == undefined) {
									myData = $(currentTab + '#manuscriptsData div[data-c-id="' + dataCID + '"]').data();
								}
								var manualUploadArr = manualUpload.split('.');
								var lastKey = manualUploadArr.pop();
								manualUploadArr.forEach(function (mkey) {
									myData = myData[mkey];
								})
								myData[lastKey] = "true";
							}
							$(fileEle).closest('div').find('object').attr('data', filePath);
							eventHandler.flatplan.action.saveData();
						}
					}
					else if (advertLayoutSec) { // if layout section is specifed, then loop through the region,layout and update the filepath
						// var dataCID = $(currentTab + '#msDataDiv').attr('data-c-rid');
						$('.la-container').fadeOut();
						var dataCID = $(currentTab + '#rightPanelContainer').attr('data-c-rid');
						// get data from the corresponding article
						var myData = $(currentTab + '#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data');
						var fileFlag = false;
						var regionArr = myData ? myData.region : ''
						if (regionArr) {
							if (regionArr.constructor.toString().indexOf("Array") < 0) {
								regionArr = [regionArr];
							}
						}
						else {
							regionArr = [];
						}
						var regionObj = {};
						var rcIndex = 0;
						var fileArray = [];
						var regionFlagVal = false;
						regionArr.forEach(function (regionObj, rcIndex) {
							if ((regionObj) && (regionObj._attributes['type'] == currRegionVal)) {
								regionFlagVal = true;
								fileArray = regionObj.file; // get files from data
								if (fileArray.constructor.toString().indexOf("Array") < 0) {
									fileArray = [fileArray];
								}
								//match the layout sec of file uploaded with the layout sec of file in issueXML
								fileArray.forEach(function (file) {
									if (file._attributes['data-layout'] == advertLayoutSec) {
										file._attributes['path'] = filePath;
										fileFlag = true;
									}
								})
								if (fileFlag == false) { // if file for the specified section is not available, then push the file into file Array
									var file = ({ "_attributes": { "type": "file", "data-region": currRegionVal, "data-layout": advertLayoutSec, "path": filePath } })
									fileArray.push(file)
								}
								regionObj["_attributes"]["layout"] = currLayoutVal; //change the layout value in xml
								regionObj["_attributes"]["value"] = "1";
								regionObj.file = fileArray;
								$(fileEle).closest('div').find('object').attr('data', filePath);
								eventHandler.flatplan.action.saveData();
							}
						})
						if (regionFlagVal == false) { // if region info is not available in data,add a new region
							var newRegion = ({ "_attributes": { "type": currRegionVal, "layout": currLayoutVal }, "file": { "_attributes": { "type": "file", "data-region": currRegionVal, "data-layout": advertLayoutSec, "path": filePath } } })
							regionArr.push(newRegion)
							if (myData) {
								myData.region = regionArr;
							}
							else {
								$(currentTab + '[data-c-id="' + dataCID + '"]').data('data', regionArr);
							}
							$(fileEle).closest('div').find('object').attr('data', filePath);
							eventHandler.flatplan.action.saveData();
						}
					}
				}
				else {
					$('.la-container').fadeOut();
					$('.save-notice').text(eventHandler.flatplan.errorCode[501][3] + ' ' + eventHandler.flatplan.errorCode[503][1] + ' on' + dateFormat(date, 'mm-dd-yyyy hh:mm:ss'));
				}
			},
			populateUAA: function (entry, tempID) {
				if (entry._attributes.type == 'blank') {
					containerHTML = '<div class="uaa card" draggable="true" id="' + tempID + '">';
					containerHTML += '<p class="row">';
					containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry._attributes.type + '">' + entry._attributes.type + '</span>';
					containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
					containerHTML += '</p>';
					containerHTML += '<p class="row">';
				}
				containerHTML = '<div class="uaa card" draggable="true" id="' + tempID + '">';
				containerHTML += '<p class="row">';
				// containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry.type._text + '">' + entry.type._text + '</span>';
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
				containerHTML += '</p>'
				containerHTML += '<p class="row">';
				var accDate = '';
				if (typeof (entry.accDate) != 'undefined') {
					accDate = new Date(entry.accDate);
					accDate = (accDate ? accDate : '').toDateString().replace(/^[a-z]+ /gi, '');
				}
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msAccDate" title="' + accDate + '">Acc. Date: <b>' + accDate + '</b></span>';
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msPCount">Page Count: <b>' + (entry._attributes.lpage - entry._attributes.fpage + 1) + '</b></span>';
				containerHTML += '</p>'
				containerHTML += '<p class="row">';
				var titleText = 'Title Text';
				if (entry.title && entry.title._text) {
					titleText = entry.title._text;
				}
				containerHTML += '<span class="col-sm-12 col-md-12 col-lg-12 msText">' + titleText + '</span>';
				containerHTML += '</p>'
				containerHTML += '</div>';
				return containerHTML;
			},
			loadUnassignedArticles: function (data) {
				var issueContent = $('#manuscriptsData').data('binder')['container-xml'];
				var doiString = [];
				if (issueContent && issueContent.contents && issueContent.contents.content) {
					var articleList = issueContent.contents.content;
					var al = articleList.length;
					for (var a = 0; a < al; a++) {
						if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id) {
							if(articleList[a]._attributes['ms-id']){
								doiString.push(articleList[a]._attributes['ms-id'].replace(/(.*)\//,''));
							}else{
								doiString.push(articleList[a]._attributes.id);
							}
						}
					}
				}
				if(doiString.length==0)doiString = ["test-doi"];
				doiString = doiString.join(' OR ');
				$(currentTab + '#unassignedTab #unassignedTabDiv').html('');
				//   var obj = { 'url': '/api/getissuedata?client=' + cName + '&jrnlName=' + pName + '&type=getArticlesXML', 'objectName': 'list' };
				var param = {
					'customer': data.customer,
					'project': data.project,
					'urlToPost': 'articlesunassignedtoissue',
					'excludeDoi': doiString,
					'from': '0',
					'size': '500'
				}
				$.ajax({
					type: "POST",
					url: '/api/getarticlelist',
					data: param,
					dataType: 'json',
					success: function (respData) {
						//console.log('unassinged articles');
						//console.log(respData);
						let unassignedArticles = respData.hits;
						if (unassignedArticles.constructor.toString().indexOf("Array") < 0) {
							unassignedArticles = [unassignedArticles];
						}
						unassignedArticlesList = unassignedArticles;
						var tempID = 10000;
						unassignedArticles.forEach(function (unassignedArticle) {
							let entryData = unassignedArticle._source;
							let pageCount = unassignedArticle._source.pageCount;
							if (typeof (pageCount) == 'undefined' || pageCount == null) {
								pageCount = 1;
							}
							let fpage = unassignedArticle._source.fpage ? unassignedArticle._source.fpage : 1;
							let lpage = unassignedArticle._source.lpage ? unassignedArticle._source.lpage : fpage;
							var entry = ({ "_attributes": { "id": unassignedArticle._source.id, "grouptype": "body", "type": "manuscript", "fpage": fpage, "lpage": lpage, "pageExtent": pageCount }, "title": { "_text": unassignedArticle._source.title }, "type": unassignedArticle._source.articleType });
							var file = ({ "_attributes": { "proofType": "online", "type": "pdf", "path": "" } })
							if (file != '') {
								entry['file'] = file
							}
							entry._attributes['ms-id'] = unassignedArticle._source.doi;
							entry._attributes['siteName'] = unassignedArticle._source['kriya-version'] ? unassignedArticle._source['kriya-version'].replace(/^v/, 'kriya') : '';
							entry['accDate'] = unassignedArticle._source.acceptedDate;
							$(currentTab + '#unassignedTab #unassignedTabDiv').append(eventHandler.flatplan.components.populateUAA(entry, tempID));
							$('#' + tempID++).data('data', entry);
							$(currentTab+"#unassignedTabSearch").unbind();
							$(currentTab+"#unassignedTabSearch").on("keyup", function() {
								var value = $(this).val().toLowerCase();
								$(currentTab+"#unassignedTab #unassignedTabDiv .card").filter(function() {
									$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
								});
							});
						})
					},
					error: function (error) {
						console.log(error);
					}
				})
			},
			getAvailableSections: function(){
				//get all section data, form a array with all available sectiondata
				var sections = $('#tocContentDiv div.sectionList');
				var sectionArray = [];
					sections.each(function(){
						var currNode = $(this);
						var currSection = currNode.find('.sectionHead').text();
							sectionArray.push(currSection);
					})

				var uniqueSectionArray = Array.from(new Set(sectionArray))
				return uniqueSectionArray
			},
			insertK1Article: function(){
				var msid = $('#insertK1ArticleModal').find('input[id=insert_K1Article]').val()
				var sectionName = $('#insertK1ArticleModal').find('select[id=sectionList]').val();
				var param = {
					"msid"     : msid,
					"sectionName"    : sectionName,
				}
				if(msid.match(/^(\s|undefined)*$/)){
					var parameters = {notify : {}};
					parameters.notify.notifyType = 'error';
					parameters.notify.notifyText = 'Please enter msid and select section name.';
					eventHandler.common.functions.displayNotification(parameters);
					return false;
				}
				eventHandler.flatplan.action.saveXML('fetchK1Article',param)
				$('#insertK1ArticleModal').modal('hide')
			},
			changeSectionHead :function(param, targetNode){
				var changedContent = _.escape($('#editSectionHead').find('input[id="sectionName"]').val());
				changedContent = changedContent.replace(/[\u00A0-\u00FF\u0100-\u017F\u0180-\u024F\u2022-\u2135\u4e00-\u9fff\u0600-\u06FF\u0D80-\u0DFF\u20A0-\u20CF\u2190-\u21FF\u2200-\u22FF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u2600-\u26FF\u25A0-\u25FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0700-\u074F\u0780-\u07BF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u13A0-\u13FF\u1400-\u167F\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u19E0-\u19FF\u1D00-\u1D7F\u1E00-\u1EFF\u1F00-\u1FFF\u20D0-\u20FF\u2150-\u218F\u2070-\u209F\u2100-\u214F\u2000-\u206F\u2300-\u23FF\u2400-\u243F\u3000-\u303F\u0300-\u036F\u0370-\u03FF\u2440-\u245F\u2460-\u24FF\u2500-\u257F\u2580-\u259F\u2700-\u27BF\u27C0-\u27EF\u27F0-\u27FF\u2800-\u28FF\u2900-\u297F\u2980-\u29FF\u2A00-\u2AFF\u2B00-\u2BFF\u2E80-\u2EFF\u2F00-\u2FDF\u2FF0-\u2FFF\u3040-\u309F\u30A0-\u30FF\u3100-\u312F\u3130-\u318F\u3190-\u319F\u31A0-\u31BF\u31F0-\u31FF\u3200-\u32FF\u3300-\u33FF\u3400-\u4DBF\u4DC0-\u4DFF\uA000-\uA48F\uA490-\uA4CF\uAC00-\uD7AF\uD800-\uDB7F\uDB80-\uDBFF\uDC00-\uDFFF\uE000-\uF8FF\uF900-\uFAFF\uFB00-\uFB4F\uFB50-\uFDFF\uFE00-\uFE0F\uFE20-\uFE2F\uFE30-\uFE4F\uFE50-\uFE6F\uFE70-\uFEFF\uFF00-\uFFEF\uFFF0-\uFFFF\u0250-\u02AF\u02B0-\u02FF]/g, function (c) {
					return '&#x' + c.charCodeAt(0).toString(16) + ';';
				});
				var currentSectionhead = $('#editSectionHead').find('input[id="sectionName"]').attr('data-org-data');
				var contents = $(currentTab +'#manuscriptsData').data('binder')['container-xml'].contents.content;
				//get all contents that matches cuurent sectionhead and replace with changed value
				contents.forEach(function (entry) {
					var sectionType = entry._attributes.section ? entry._attributes.section : '';
					if(sectionType != '' || sectionType !=undefined){
						sectionType = sectionType.trim();
					 }
					if(sectionType ==  currentSectionhead){
						entry._attributes.section  = changedContent;
					}
				});
				$('#editSectionHead').modal('hide');
				$('#editSectionHead').find('input[id="sectionName"]').val('');

                $('#editSectionHead').find('input[id="sectionName"]').removeAttr('data-org-data');
				var histObj = {
					"change": "changed <b> sectionhead </b> from <u>" + currentSectionhead + '</u> to <u>' + changedContent + '</u>',
					"file": "",
					"fileType": "",
					"id-type": ""
				};
				eventHandler.flatplan.components.updateChangeHistory(histObj);
				eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#projectIssueList .active').trigger('click')" });
			},
			cardControlOrder: function (e, target) {
				var targetNode = target;
				if (targetNode.hasClass('move_down')) {//icon-arrow_downward
					// swap the article info in the container array
					var currNode = targetNode.closest('[data-c-id]');
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var tempData = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex];
					$(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex] = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex + 1];
					$(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex + 1] = tempData;
					var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
					var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
					var nextNodeData = $(nextNode).data('data')._attributes;

					var startPage = histObj.fpage;
					if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') {
						if ((histObj.type == 'filler') && (/^(blank|advert|filler|filler-toc)$/gi.test($(nextNode).data('data')._attributes.type))) {
							// filler should run-on only after manuscript
							$('.save-notice').text(eventHandler.flatplan.errorCode[502][2] + 'on' + dateFormat(date, 'mm-dd-yyyy hh:mm:ss'));
							return false;
						}
						else {
							startPage = parseInt(histObj.fpage) + 1;// if run-on article, then add 1 to fpage
						}
					}

					histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' after ' + (nextNodeData['id-type'] ? nextNodeData['id-type'] : nextNodeData.type) + ' - ' + nextNodeData.id;
					currNode.attr('data-c-id', currNodeIndex + 1);
					nextNode.attr('data-c-id', currNodeIndex);
					currNode.insertAfter(nextNode);
					if (histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true') {
						eventHandler.flatplan.components.updatePageNumber(currNodeIndex, startPage, currNode.attr('data-grouptype'));
					}
					else if(histObj["data-unpaginated"] == 'true'){
						eventHandler.flatplan.components.updateTotalPages();
					}
					eventHandler.flatplan.components.updateChangeHistory(histObj);
					// push updated data to server
					eventHandler.flatplan.action.saveData();
				}
				else if (targetNode.hasClass('move_up')) {//icon-arrow_upward
					// swap the article info in the container array
					var currNode = targetNode.closest('[data-c-id]');
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var tempData = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex - 1];
					$(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex - 1] = $(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex];
					$(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex] = tempData;
					var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
					var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
					var prevNodeData = $(prevNode).data('data')._attributes;
					var startPage = prevNodeData.fpage;
					if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') { //check if run-on article
						var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
						var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
						// filler should run-on only after manuscript
						if ((histObj.type == 'filler') && (/^(blank|advert|filler)$/gi.test($(secondPrevNode).data('data')._attributes.type))) {
							$('.save-notice').text(eventHandler.flatplan.errorCode[502][2]);
							return false;
						}
						else { // if run-on article, then set 2nd previous node lpage as fpage
							startPage = parseInt(secondPrevNodeData.lpage);
						}
					}
					if (prevNodeData["data-unpaginated"] && prevNodeData["data-unpaginated"] == 'true') {
						var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
						if (secondPrevNode) {
							var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
							startPage = parseInt(secondPrevNodeData.lpage) + 1;
						}
						else {
							startPage = parseInt(histObj.fpage);
						}
					}
					histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' before ' + (prevNodeData['id-type'] ? prevNodeData['id-type'] : prevNodeData.type) + ' - ' + prevNodeData.id;
					currNode.attr('data-c-id', currNodeIndex - 1);
					prevNode.attr('data-c-id', currNodeIndex);
					currNode.insertBefore(prevNode);
					//if next node is filler, move filler along with article
					/*if(nextNodeData.type == 'filler'){
						prevNode.attr('data-c-id', currNodeIndex + 1);
						nextNode.attr('data-c-id', currNodeIndex);
						nextNode.insertAfter(currNode);
					}*/
					eventHandler.flatplan.components.updateChangeHistory(histObj);
					if ((histObj["data-unpaginated"] != 'true') && (histObj["data-run-on"] != 'true')) {
						//eventHandler.flatplan.action.saveData();
						eventHandler.flatplan.components.updatePageNumber(currNodeIndex - 1, startPage, currNode.attr('data-grouptype'));
					}
					else if(histObj["data-unpaginated"] == 'true'){
						eventHandler.flatplan.components.updateTotalPages();
						eventHandler.flatplan.action.saveData();
					}
					else{
						eventHandler.flatplan.action.saveData();
					}
					// push updated data to server
//					eventHandler.flatplan.action.saveData();
				}
				else if (targetNode.hasClass('editSectionHead')) {//icon-download2
					$('#editSectionHead').find('[id="sectionName"]').val('')
					var nodeValue = $(targetNode).closest('.sectionHeadInfo').find('.sectionHead').text()
					$('#editSectionHead').find('input[id="sectionName"]').attr('data-org-data', nodeValue);
					$('#editSectionHead').find('input[id="sectionName"]').val(nodeValue);
					$('#editSectionHead').modal('show');
				}
				else if (targetNode.hasClass('downloadK1Article')) {//icon-download2
					$('#insertK1ArticleModal').find('select[id=sectionList]').html('')
					$('#insertK1ArticleModal').find('input[id=insert_K1Article]').val('')
					var currentSectionhead = $(targetNode).parent().parent().text();
					var sectionHTML = '';
					var flagValues = eventHandler.flatplan.components.getAvailableSections()
					flagValues.forEach(function (selectValue, sIndex) {
						var isSelected = '';
						sectionHTML += '<option value="' + selectValue + '"' + isSelected + '>' + selectValue + '</option>'
					})
					$('#insertK1ArticleModal').find('select[id=sectionList]').append(sectionHTML)
					$('#insertK1ArticleModal').modal('show');
				}
				else if (targetNode.hasClass('remove_card')) {//icon-close
					(new PNotify({
						title: 'Confirmation Needed',
						text: 'Are you sure you want to remove the manuscript?',
						hide: false,
						confirm: { confirm: true },
						buttons: { closer: false, sticker: false },
						history: { history: false },
						addclass: 'stack-modal',
						stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
					})).get()
						.on('pnotify.confirm', function () {
							var currNode = targetNode.closest('[data-c-id]');
							var currNodeIndex = currNode.attr('data-c-id');
							// remove the deleted manuscript's from the container
							var removedData = JSON.parse(JSON.stringify($(currentTab+'#manuscriptsData').data('binder')['container-xml'].contents.content.splice(currNodeIndex, 1)));
							removedData[0]._attributes.lpage = removedData[0]._attributes.lpage - removedData[0]._attributes.fpage + 1;
							removedData[0]._attributes.fpage = 1;
							var removedDataID = $('#uaaDiv > div:last').attr('id');
							var removedDataID = removedDataID ? (parseInt(removedDataID) + 1) : 100000;
							if (removedData[0]._attributes.type == 'manuscript') {
								$('#uaaDiv').append(eventHandler.flatplan.components.populateUAA(removedData[0], removedDataID));
								$('#' + removedDataID).data('data', removedData[0]);
							}
							var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
							histObj.change = 'Removed ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id;
							eventHandler.flatplan.components.updateChangeHistory(histObj);
							var nextNodeIndex = parseInt(currNodeIndex) + 1
							var nextNode = $('[data-c-id="' + nextNodeIndex + '"]')
							// remove manuscript node from UI
							currNode.remove();
							// var lastNode = $('#manuscriptsData div[data-c-id]:last');
							var lastNode = $(currentTab+'#manuscriptsData [data-section="' + nextNode.parent().attr('data-section') + '"] [data-c-id]:last');
							var lastNodeID = parseInt(lastNode.attr('data-c-id'));
							var tempID = parseInt(currNodeIndex) + 1
							var seqID = parseInt(lastNode.attr('data-c-id')) - 1;
							var entry = nextNode.data('data')
							//change data-c-id for contents below the removed content
							if (lastNode.length > 0 && nextNode.length > 0) {
								lastNodeID = parseInt(lastNode.attr('data-c-id'));
								while (lastNodeID != tempID) {
									lastNode.attr('data-c-id', seqID--);
									if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
										lastNode = $(lastNode).prevAll('[data-c-id]:first')
									}
									else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
										lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
									}
									else {
										lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
									}
									lastNodeID = parseInt(lastNode.attr('data-c-id'));
								}
								lastNode.attr('data-c-id', seqID)
							}
							// update page number for the contents below removed Node
							if (nextNode) {
								var startPage = histObj.fpage;
								if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') { // if run-on article, add 1 to the fpage
									var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
									var prevNodeData = $(prevNode).data('data')._attributes;
									startPage = parseInt(prevNodeData.lpage) + 1;
								}
								if (histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true') {
									$('.save-notice').text('Saving...');
									eventHandler.flatplan.components.updatePageNumber(nextNode.attr('data-c-id'), startPage, histObj['grouptype']);
								}
								else if(histObj["data-unpaginated"] == 'true'){
									$('.save-notice').text('Saving...');
									eventHandler.flatplan.components.updateTotalPages();
								}
							}
							// push updated data to server
							$('.save-notice').text('Saving...');
							var chapterCount = Number($(currentTab+'#manuscriptsData .articleCount').contents()[1].nodeValue) - 1;
							$(currentTab+'#manuscriptsData .articleCount').contents()[1].nodeValue = chapterCount;
							eventHandler.flatplan.action.saveData();
							var data = {};
							data.customer = $(currentTab).find('#customer span').text();
							data.project = $(currentTab).find('#project span').text();
							eventHandler.flatplan.components.loadUnassignedArticles(data);
						})
						.on('pnotify.cancel', function () { });
				}
				//icon-add
				else if (targetNode.hasClass('addCard') || targetNode.hasClass('addContent')) {
					var currNode = targetNode.closest('[data-c-id]');
					if (currNode.length == 0) {
						var currSec = targetNode.closest('.sectionData')
						currNode = currSec.next()
					}
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var groupType = 'body'
					if (currNode.length > 0 && currNode.data('data')._attributes && currNode.data('data')._attributes.grouptype && currNode.data('data')._attributes.grouptype != '') {
						groupType = currNode.data('data')._attributes.grouptype
					}
					loadBlankOrAdvertOrFiller($('#filterProject .active').attr('data-project-type'), groupType)
					$(currentTab+'#metaHistory, ' + currentTab +'#manuscriptData, '+currentTab+'#advertsData').addClass('hide');
					$('#unassignedArticles').removeClass('hide');
				}
				// to edit section head
				else if (targetNode.hasClass('editSectionHead')) {//icon-edit
					var currentSectionhead = $(targetNode).parent().prev('.sectionHead').text()
					if (currentSectionhead != '' || currentSectionhead != undefined) {
						currentSectionhead = currentSectionhead.trim()
					}
					$('#changeSectionHead').find('input[class~=sectionHead_change]').val(currentSectionhead)
					$('#changeSectionHead').find('input[class~=sectionHead_change]').attr('data-org-data', currentSectionhead)
					$('#changeSectionHead').modal('show');
				}
			},
			updateChangeHistory: function (histObj) {
				var userName = userData.kuser.kuname.first;//$('.userinfo:first').attr('data-name');
				var currTime = new Date().toString();
				var changeText = histObj.change;
				var fileID = histObj.id;
				var fileType = histObj.type;
				var changeObj = { 'user': { "_text": userName }, 'time': { "_text": currTime }, 'change': { "_text": changeText }, 'file': { "_text": fileID }, 'fileType': { "_text": fileType }, 'id-type': { "_text": histObj['id-type'] } };
				$(currentTab + '#manuscriptsData').data('binder')['container-xml']['change-history'].push(changeObj);
				eventHandler.flatplan.components.populateHistory(changeObj);
			},
			populateHistory: function (chData) {
				//$('#changeHistory').html('')
				var changeHistoryData = [];
				if (typeof (chData) != 'undefined') {
					changeHistoryData = [{
						"user": chData.user ,
						"change":chData.change ,
						"time": chData.time ,
						"file": chData.file ,
						"fileType": chData.fileType
					}]
				}
				else {
					$('#changeHistory').html('')
					changeHistoryData = $('#manuscriptsData').data('binder')['container-xml']['change-history'];
				}
				var chHTML = '';
				changeHistoryData.forEach(function (chData) {
					var chTime = chData.time._text
					chTime = chTime.replace(/^[a-z]+ (.*:[0-9]{2}) .*$/gi, "$1");
					chHTML += '<p class="chData">';
					chHTML += '<span class="user">' + chData.user._text + '</span> <span class="change">' + chData.change._text + '</span> on <span class="time" title="' + chData.time._text + '">' + chTime + '</span>';
					if (chData.file && chData.file._text) {
						chHTML += ' for <span class="file">' + chData.file._text + '</span>&#x00A0;(<span class="type">' + chData.fileType._text + '</span>)';
					}
					chHTML += '</p>';
				});
				if ($('#changeHistory').children().hasClass('changeHistoryContainer')) {
					$('#changeHistory .changeHistoryContainer').append(chHTML);
				} else {
					$('#changeHistory').append('<div class="changeHistoryContainer" style="overflow:hidden;">' + chHTML + '</div>');
				}
				//$(currentTab + '#changeHistory .changeHistoryContainer').css('height', window.innerHeight - $(currentTab + '#changeHistory').offset().top - $('#footerContainer').height() - 20 + 'px');
				/*$('#changeHistory .changeHistoryContainer').mCustomScrollbar({
					axis: "y", // vertical scrollbar
					theme: "dark"
				});*/
			},
			exportEpub: function (targetNode) {
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var parameterString = "client=" + customer + "&id=" + project + "&jrnlName=" + project + "&processType=PackageCreator&type=" + customer + "-epub&issueFileName=" + project + ".xml&uploadTo=localftp&siteName=" + window.location.protocol + "//" + window.location.hostname;
				$('#notifyBox .notifyHeaderContent').text("Publishing file... Please wait.");
				$('#notifyBox #reloadinfobody').text('');
				$('#notifyBox').show();
				var param = {
					"client": customer,
					"id": project,
					"jrnlName": project,
					"processType": "PackageCreator",
					"type":  customer + "-epub",
					"issueFileName": project + ".xml",
					"uploadTo":"localftp",
					"siteName": window.location.protocol + "//" + window.location.hostname
				}
				$.ajax({
					type: 'POST',
					url: "/api/publisharticle",
					data: param,
					success: function (data) {
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))) {
							eventHandler.components.actionitems.getJobStatus(customer, data.message.jobid, '', '', '', project);
						}
						else if (data == '') {
							$('#notifyBox #reloadinfobody').text('Failed');
						} else {
							$('#notifyBox #reloadinfobody').text(data.status.message);
							if (data.status.message == "job already exist") {
								eventHandler.components.actionitems.getJobStatus(customer, data.message.jobid, '', '', '', project);
							}
						}
					},
					error: function (error) {
						$('#notifyBox #reloadinfobody').text('Failed');
					}
				});
			},
			//pull advert and filler layout from the config and append it at the end of #tocContainer
			//config of advert/filler layout is based on the customer
			populateLayouts: function (cName, pName) {
				$.ajax({
					url: '/api/issuedata?customer=' + cName + '&project=' + pName + '&type=getLayouts',
					type: 'POST',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (issueResponse) {
						if (typeof (issueResponse) == "undefined" || issueResponse == null) {
							$('.la-container').fadeOut();
							return false;
						}
						$(currentTab +'#manuscriptsData .layouts.hide').remove();
						$(currentTab +'#manuscriptsData').append(issueResponse.list)
					},
					error: function (error) {
						console.log(error);
					}
				});
			}
		},
		issue: {
			populateMetaContainer: function (metaData) {
				var containerHTML = '';
				var tabhtml = '';
				var data = $('#manuscriptsData').data('binder')['container-xml'].meta;
				var metaContainerData = $('#manuscriptsData').data('config');
				metaContainerData = JSONPath({ json: metaContainerData, path: '$..binder...meta.tab.path' })[0];
				if (metaContainerData == undefined) {
					return false;
				}
				if (metaContainerData.constructor.toString().indexOf("Array") < 0) {
					metaContainerData = [metaContainerData];
				}
				console.log(metaContainerData);
				var metaForm = '';
				for (element of metaContainerData) {
					var eachElement = element._attributes.string.replace(/\..*/g, '');
					var elementValue = '-';
					if (data[eachElement]) {
						elementValue = data[eachElement]._text
					}
					metaForm += '<div class="form-group row"><label class="col-5" for="name"><b>' + element.label._text.replace(/:/g, '') + '</b></label><span class="col-1">:</span>';
					if (element.component._text == 'editable') {
						metaForm += '<input type="text" onInput="$(\'' + currentTab + '#rightPanelContainer #metaSaveButton\').removeClass(\'disabled\');" class="form-control" class="col-6" value="' + ((elementValue) ? elementValue : '-') + '" id="' + eachElement + '" name="' + element.label._text.replace(/:/g, '') + '" data-required="true"></input>'
					} else {
						metaForm += '<span class="col-6">' + elementValue + '</span>'
					}
					metaForm += '</div>';
				}
				metaForm += '<div class="form-group row"><button type="button" id="metaSaveButton" class="btn btn-success disabled" data-channel="flatplan" data-topic="issue" data-event="click" data-message="{\'funcToCall\': \'saveMetaData\'}">Save</button></div>';
				$(currentTab + '#rightPanelContainer #metaData').html(metaForm);
			},
			saveMetaData: function (param, targetNode) {
				var metaValue = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].meta;
				var startNodeId = $(currentTab + '[data-grouptype="body"][data-run-on!="true"]:first').attr('data-c-id');
				var epageStartNodeId = $(currentTab + '[data-grouptype="epage"][data-run-on!="true"]:first').attr('data-c-id');
				$(currentTab + '#metaData input').each(function (a) {
					var empty = ($(this).val()) ? $(this).val() : '0';
					if (metaValue[$(this).attr('id')] && metaValue[$(this).attr('id')]._text != $(this).val()) {
						var oldValue = metaValue[$(this).attr('id')]._text;
						metaValue[$(this).attr('id')]._text = $(this).val();
						if ($(this).attr('id') == "first-page") {
							eventHandler.flatplan.components.updatePageNumber(startNodeId, $(this).val(), 'body', { 'onsuccess' : "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
						}
						var histObj = {
							"change": "changed Meta data of " + $(this).attr('name') + " from " + oldValue + " to " + empty,
							"file": "",
							"fileType": "",
							"id-type": ""
						};
						eventHandler.flatplan.components.updateChangeHistory(histObj);
					} else if (metaValue[$(this).attr('id')] == undefined && $(this).val() != '-') {
						metaValue[$(this).attr('id')] = {};
						metaValue[$(this).attr('id')]['_text'] = $(this).val();
						if ($(this).attr('id') == "e-first-page") {
							eventHandler.flatplan.components.updatePageNumber(epageStartNodeId, $(this).val(), 'epage', { 'onsuccess': "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
						}
						var histObj = {
							"change": "changed Meta data of " + $(this).attr('name') + " to " + empty,
							"file": "",
							"fileType": "",
							"id-type": ""
						};
						eventHandler.flatplan.components.updateChangeHistory(histObj);
					}
				})
				$(currentTab + '#manuscriptsData').data('binder')['container-xml'].meta = metaValue;
				$('.la-container').fadeIn();
				eventHandler.flatplan.action.saveData({ 'onsuccess' : "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
				$(currentTab + '#rightPanelContainer #metaSaveButton').addClass('disabled');
			},
			switchTabs: function (param, targetNode) {
				$(targetNode).parent().find('.tabHead').removeClass('active');
				if ($(targetNode).attr('data-href') == "tocBodyDiv") {
					$('[data-href="flatPlanBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
					$(targetNode).addClass('active').removeClass('btn-default').addClass('btn-primary');
				}
				var tabID = $(targetNode).addClass('active').attr('data-href');
				$(currentTab + '#' + tabID).parent().find('.tabContent').addClass('hide');
				$(currentTab + '#' + tabID).removeClass('hide');
			},
			imgError: function (current) {
				current.src = "../images/NoImageFound.jpg"
			},
			uploadFilePopup: function (param, targetNode) {
				$(targetNode).parent().find('input:file').val('');
				$(targetNode).parent().find('input:file').trigger('click');
				$('.loading-status').remove();
			},
			uploadFile: function (targetNode,processType) {
				$('.la-container').fadeIn();
				var htmlID = "upload_advert_file";
				$("body").find('#' + htmlID).remove();
				$("body").append('<div id="' + htmlID + '"></div>');
				var param = {};
				var currentArticle = $(currentTab + '#rightPanelContainer').data();
				param.file = targetNode.files[0];
				$('#' + htmlID).attr({
					'data-upload-type': 'Files',
					'data-success': 'eventHandler.flatplan.components.saveUploadedFile',
					'data-file-size': param.file.size,
					'data-customer': $(currentTab).find('#customer span').text(),
					'data-project': $(currentTab).find('#project span').text(),
					'data-doi': currentArticle._attributes.id
				});
				$('#' + htmlID).data().node = targetNode;
				param.htmlID = htmlID;
				if(processType && processType=='issueMakeup'){
					let volumeNo = 	$(currentTab + '#issueDetails .volume span').text();
					let issueNo	=	$(currentTab + '#issueDetails .issue span').text();
					let proofType = $(targetNode).attr('data-prooftype');
					param.keyPath = proofType;
					if(volumeNo && issueNo){
						param.keyPath = 'volume_'+volumeNo+'_issue_'+issueNo+'/'+proofType;
						param.processType='issueMakeup';
					}else{
						if(proofType=='online'){
							param.keyPath = currentArticle._attributes.id+'/'+proofType;
						}else if(proofType=='print'){
							param.keyPath = currentArticle._attributes.id+'_print/'+proofType;
						}
						param.processType='book';
					}
				}
				eventHandler.components.actionitems.uploadFile(param, targetNode);
			},
			saveTabsData: function (param, targetNode) {
				var saveData = true;
				var meta = $(currentTab + '#manuscriptsData').data('binder')['container-xml']['meta'];
				var content = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content;
				var changingData = targetNode.closest(currentTab + '#rightPanelContainer').data();
				var className = targetNode.attr('class');
				var history = {};
				var historyText = '';
				if (className.match('color')) {
					if (changingData['color'].constructor !== Array) {
						changingData['color'] = [changingData['color']];
					}
					changingData['color'].find(function (properties) {
						if (properties._attributes['ref-id'] == targetNode.attr('ref-id')) {
							history.figure = properties._attributes.label;
							if (Number(properties._attributes.color)) {
								properties._attributes.color = 0;
								history.text = 'greyscale';
							} else {
								properties._attributes.color = 1;
								history.text = 'color';
							}
						}
					})
					historyText = "changed " + history.figure + " to be " + history.text + " in print for " + changingData._attributes.id;
				} else if (className.match('flag')) {
					if (targetNode.attr('data-flag-type').match('section-head')) {
						var id = Number($(currentTab + '[data-doi="' + changingData._attributes.id + '"]').attr('data-c-id'));
						var indexToAdd = 0;
						var contentToChange = $(currentTab + '[data-grouptype="body"][data-c-id="' + id + '"]');
						var stopLoop = true;
						var secFlag = false;
						var orgSecName = changingData._attributes.section;
						content.find(function (cont, contIndex) {
							if (cont._attributes.section == targetNode.val() && stopLoop) {
								if (id > contIndex) {
									indexToAdd = contIndex;
									secFlag = true;
									// stopLoop = false;
								} else {
									indexToAdd = contIndex;
									stopLoop = false;
									secFlag = true;
								}
							}
						})
						//if section does not have any content, get the index by finding prev or next section
						if(secFlag == false){
							var newSecIndex = 0;
							var tempSecName ='';
							var curSecIndex = 0;
							curSecIndex = parseInt(curSecIndex);
							var secValues = eventHandler.flatplan.components.getAvailableSections();
							secValues.forEach(function (secValue, secIndex) {
								if(secValue==targetNode.val()){
									newSecIndex = secIndex;
								}
								else if(secValue==orgSecName){
									curSecIndex = secIndex;
								}
							});
							if(newSecIndex >= curSecIndex){
								var contSecflag = false;
								var tempIndex = newSecIndex-1;
								while(tempIndex > curSecIndex){
									if($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]').length>0){
										contSecflag = true;
										indexToAdd = parseInt($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]:last').attr('data-c-id'))+1;
										break;
									}
									tempIndex--;
								}
								if(contSecflag == false){
									indexToAdd = parseInt($(currentTab + '[data-section="' + secValues[curSecIndex] + '"][data-c-id]:last').attr('data-c-id'))+1;
								}
							}
							else{
								var contSecflag = false;
								var tempIndex = newSecIndex+1;
								while(tempIndex < curSecIndex){
									if($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]').length>0){
										contSecflag = true;
										indexToAdd = parseInt($(currentTab + '[data-section="' + secValues[tempIndex] + '"][data-c-id]:first').attr('data-c-id'))-1;
										break;
									}
									tempIndex++;
								}
								if(contSecflag == false){
									indexToAdd = parseInt($(currentTab + '[data-section="' + secValues[curSecIndex] + '"][data-c-id]:first').attr('data-c-id'))-1;
								}
							}
						}
						changingData._attributes.section = targetNode.val();
						if (changingData.type && changingData.type._text) {
							changingData.type._text = targetNode.val();
						}
						contentToChange.attr('data-c-id', (Number($(currentTab + '[data-c-id]:last').attr('data-c-id')) + 1));
						if (id > indexToAdd) {
							$(currentTab + '[data-c-id="' + indexToAdd + '"]').after(contentToChange);
							content.splice(indexToAdd + 1, 0, changingData);
							content.splice(id + 1, 1);
						} else {
							$(currentTab + '[data-c-id="' + (indexToAdd) + '"]').before(contentToChange);
							content.splice(indexToAdd, 0, changingData);
							content.splice(id, 1);
						}
						$(currentTab + '[data-grouptype="body"][data-c-id="' + id + '"]').remove();
						var startNodeId = $(currentTab + '[data-grouptype="body"][data-run-on!="true"]:first').attr('data-c-id');
						var startPage = (meta['first-page'] && meta['first-page']._text) ? meta['first-page']._text : 1;
						eventHandler.flatplan.components.updatePageNumber(startNodeId, startPage, 'body', { 'onsuccess': "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
						return false;
					} else {
						if (changingData['flag'] == undefined) {
							changingData['flag'] = [];
							var toPushObject = {
								'_attributes': {
									'type': targetNode.attr('data-flag-type'),
									'value': (targetNode.prop('checked')) ? 0 : 1
								}
							}
							changingData['flag'].push(toPushObject);
						}
						if (changingData['flag'].constructor !== Array) {
							changingData['flag'] = [changingData['flag']];
						}
						var setFlag = false;
						var propValue;
						changingData['flag'].find(function (properties) {
							if (properties._attributes.type == targetNode.attr('data-flag-type')) {
								propValue = '';
								if (targetNode.attr('type') && targetNode.attr('type').match('checkbox')) {
									if (Number(properties._attributes.value)) {
										propValue = 0;
									} else {
										propValue = 1;
									}
								} else {
									propValue = targetNode.val();
								}
								properties._attributes.value = propValue;
								setFlag = true;
							}
						})
						if (!setFlag) {
							if (targetNode.attr('data-flag-type').match(/open-access/g)) {
								var toPushObject = {
									'_attributes': {
										'type': targetNode.attr('data-flag-type'),
										'value': targetNode.val()
									}
								}
							} else {
								var toPushObject = {
									'_attributes': {
										'type': targetNode.attr('data-flag-type'),
										'value': (targetNode.prop('checked')) ? 1 : 0
									}
								}
							}
							changingData['flag'].push(toPushObject);
						}
						var checked = targetNode.prop('checked');
						var id = $(currentTab + '[data-doi="' + changingData._attributes.id + '"]').attr('data-c-id');
						if (!targetNode.attr('data-flag-type').match('run-on')) {
							var selectedIssueData = $(currentTab + '[data-doi="' + changingData._attributes.id + '"]').find('.selectedIssueData').text()
							if (targetNode.attr('data-flag-type').match('open-access')) {
								selectedIssueData = selectedIssueData.replace(new RegExp('(,\\s)?' + targetNode.attr('data-flag-type') + '\\(.*?\\)', 'gi'), '').replace(/^,\s/g, '');
								if (!targetNode.val().match(/none/gi)) {
									selectedIssueData += ', '+ targetNode.attr('data-flag-type').charAt(0).toUpperCase() + targetNode.attr('data-flag-type').slice(1) + '(' + targetNode.val() + ')';
								}
							} else {
								if (targetNode.prop('checked')) {
									selectedIssueData += ', '+ targetNode.attr('data-flag-type').charAt(0).toUpperCase() + targetNode.attr('data-flag-type').slice(1);
								} else {
									selectedIssueData = selectedIssueData.replace(new RegExp( '(,\\s)?' + targetNode.attr('data-flag-type'), 'gi'), '').replace(/^,\s/g, '');
								}
							}
							selectedIssueData = selectedIssueData.replace(/(^,\s)?/g, '').trim();
							$(currentTab + '[data-doi="' + changingData._attributes.id + '"]').find('.selectedIssueData').text(selectedIssueData);
						}
						if (targetNode.attr('data-flag-type').match('e-page')) {
							var oldPage = changingData._attributes.fpage;
							var checkNextEpage = false;
							if (checked) {
								changingData._attributes.grouptype = 'epage';
							} else {
								changingData._attributes.grouptype = 'body';
								checkNextEpage = true;
							}
							//To find start page
							var startPage = "";
							var epageSection = $(currentTab + '[data-grouptype="' + changingData._attributes.grouptype + '"]');
							if (epageSection && epageSection.length) {
								epageSection.each(function () {
									if (Number($(this).attr('data-c-id')) < Number(id)) {
										startPage = (Number($(this).find('.pageRange .rangeEnd').text().replace('e', '')) + 1);
									}
								})
							}
							if (changingData._attributes.grouptype == 'epage') {
								if (startPage == '' && meta && meta['e-first-page'] && meta['e-first-page']._text) {
									startPage = meta['e-first-page']._text;
								} else if (startPage == '') {
									startPage = '1';
								}
							} else {
								if (startPage == '') {
									startPage = '1';
								}
							}
							// To change the Next Epage numbers
							if (checkNextEpage) {
								var epageID = 0;
								var eStartPage = 0;
								epageSection = $(currentTab + '[data-grouptype="epage"]');
								epageSection.each(function () {
									if (Number($(this).attr('data-c-id')) == Number(id) && $(this).attr('data-grouptype') == 'epage') {
										eStartPage = (Number($(this).find('.pageRange .rangeEnd').text().replace('e', '')));
										// epageID = $(this).attr('data-c-id');
										epageID = $(currentTab + '[data-doi="' + changingData._attributes.id + '"]').next('[data-grouptype="epage"]').attr('data-c-id');
										eventHandler.flatplan.components.updatePageNumber(epageID, eStartPage, 'epage');
										return false;
									}
								})
							}
							$(currentTab + '[data-doi="' + changingData._attributes.id + '"]').attr('data-grouptype', changingData._attributes.grouptype);
							eventHandler.flatplan.components.updatePageNumber(id, startPage, changingData._attributes.grouptype, { 'onsuccess': "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
							// To update group type body after updating the e-page
							if (changingData._attributes.grouptype == 'epage') {
								id = $(currentTab + '[data-doi="' + changingData._attributes.id + '"]').next('[data-grouptype="body"]').attr('data-c-id');
								eventHandler.flatplan.components.updatePageNumber(id, oldPage, 'body', { 'onsuccess': "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
							}
						} else if (targetNode.attr('data-flag-type').match('retract')) {
							if (checked) {
								changingData._attributes['data-retracted'] = "true";
							} else {
								changingData._attributes['data-retracted'] = "false";
							}
						} else if (targetNode.attr('data-flag-type').match('run-on')) {
							// For Run-on the article
							var currentCard = $(currentTab + '[data-doi="' + changingData._attributes.id + '"]');
							var previousCard = $(currentTab + '[data-c-id="' + (Number(id)) + '"]').prevAll('[data-type="manuscript"][data-issue-section="' + changingData._attributes.section + '"]').find('.runOn.hide').parents('[data-c-id]');
							// var previousCard = $(currentTab + '[data-grouptype="body"][data-issue-section="' + changingData._attributes.section + '"][data-c-id="' + (Number(id)) + '"]').prevAll().find('.runOn.hide').closest('[data-c-id]').last();
							if (!previousCard.length || previousCard == undefined) {
								var parameters = { notify: {} };
								parameters.notify.notifyTitle = 'Access Denied'
								parameters.notify.notifyType = 'error';
								parameters.notify.notifyText = 'You can\'t able to run on the first article in section';
								eventHandler.common.functions.displayNotification(parameters);
								return false;
							}
							var startCardNumber = 0;
							var addDoi = true;
							if (checked) {
								currentCard.attr('data-run-on', 'true').attr('data-runon-with', previousCard.attr('data-doi'));
								changingData._attributes['data-run-on'] = 'true';
								changingData._attributes['data-runon-with'] = previousCard.attr('data-doi');
								startCardNumber = Number(id) + 1;
							} else {
								addDoi = false;
								currentCard.removeAttr('data-run-on').removeAttr('data-runon-with');
								delete changingData._attributes['data-run-on'];
								delete changingData._attributes['data-runon-with'];
								if (!currentCard.next('[data-issue-section="' + changingData._attributes.section + '"]').find('.runOn').hasClass('hide')) {
									var subarticle = '';
									content.find(function (properties, propertiesIndex) {
										if (Number(id) < propertiesIndex && properties._attributes['data-run-on'] == 'true' && properties._attributes.section == changingData._attributes.section) {
											properties._attributes['data-runon-with'] = changingData._attributes.id;
											subarticle += properties._attributes.id + ','
										}
									})
									changingData._attributes['data-subarticle'] = subarticle.replace(/,$/g,'');
								}
								if (!currentCard.prev('[data-issue-section="' + changingData._attributes.section + '"]').find('.runOn').hasClass('hide')) {
									var prevMainRunOn = currentCard.prevAll('[data-issue-section="' + changingData._attributes.section + '"]').find('.runOn.hide').parents('[data-c-id]').attr('data-c-id');
									var articleToChange = content[prevMainRunOn];
									articleToChange._attributes['data-subarticle'] = articleToChange._attributes['data-subarticle'] ?articleToChange._attributes['data-subarticle'].replace(new RegExp(changingData._attributes['id'] + '.*', 'g'), ''):'';
									content[prevMainRunOn] = articleToChange;
								}
								if (currentCard.prev('[data-issue-section="' + changingData._attributes.section + '"]').find('.runOn').hasClass('hide')) {
									var prevCard = content[currentCard.prev().attr('data-c-id')];
									delete prevCard._attributes['data-subarticle'];
									content[currentCard.prev().attr('data-c-id')] = prevCard;
								}
								startCardNumber = Number(id);
							}
							// Add or remove the run-on article from main run-on card in data-subarticle attribute
							content.filter(function (eachContent) {
								if (eachContent._attributes.id == previousCard.attr('data-doi')) {
									if (addDoi) {
										if (eachContent._attributes['data-subarticle']) {
											eachContent._attributes['data-subarticle'] = eachContent._attributes['data-subarticle'] + ',' + changingData._attributes['id'];
										} else {
											eachContent._attributes['data-subarticle'] = changingData._attributes['id'];
										}
									} else {
										eachContent._attributes['data-subarticle'] = eachContent._attributes['data-subarticle'] ? eachContent._attributes['data-subarticle'].replace(new RegExp(changingData._attributes['id'] + ',?', 'g'), ''):''};
								}
							})
							var checkedText = (targetNode.prop('checked')) ? "1" : "0";
							historyText = "changed flag " + targetNode.attr('data-flag-type') + " to " + checkedText + " in print for " + changingData._attributes.id;
							for (con in content) {
								if (content[con]._attributes.id == changingData._attributes.id) {
									content[con] = changingData;
								}
							}
							var histObj = {
								"change": historyText,
								"file": "",
								"fileType": "",
								"id-type": ""
							};
							eventHandler.flatplan.components.updateChangeHistory(histObj);
							eventHandler.flatplan.components.updatePageNumber(startCardNumber, Number(previousCard.find('.pageRange .rangeEnd').text()) + 1, 'body', { 'onsuccess' : "$(currentTab + '.issuesList .filter-list.active').trigger('click')" });
							return false;
						}
						var checkedText = (targetNode.prop('checked')) ? "1" : "0";
						if (targetNode.attr('data-flag-type').match('open-access')) {
							checkedText = targetNode.val();
						}
						historyText = "changed flag " + targetNode.attr('data-flag-type') + " to " + checkedText + " in print for " + changingData._attributes.id;
					}
				} else if (className.match('contenteditable')) {
					var oldText = changingData[targetNode.attr('data-flag-type')]._text;
					changingData[targetNode.attr('data-flag-type')]._text = targetNode.find('p').text();
					historyText = "changed title of " + changingData._attributes.id + " from <u>" + oldText + "</u> to <u>" + targetNode.find('p').text() + '</u>';
				} else if (className.match('af-data-editable')) {
					var oldName;
					if (changingData['region'].constructor !== Array) {
						changingData['region'] = [changingData['region']];
					}
					changingData['region'].find(function (component) {
						if (component._attributes.type.toUpperCase() == targetNode.closest('[data-region-config="true"]').attr('data-region')) {
							oldName = component._attributes['data-advert-name'];
							component._attributes['data-advert-name'] = targetNode.text();
						}
					})
					historyText = "changed advert name of " + changingData._attributes.id + " from <u>" + oldName + "</u> to <u>" + targetNode.text() + '</u>';
				} else if (className.match('advertLayoutDropdown')) {
					if (targetNode.attr('type').match(/float-placement/g)) {
						changingData._attributes['data-float-placement'] = targetNode.val();
						historyText = 'changed the ' + changingData._attributes.type + ' Float Placement to ' + targetNode.val() + ' for ' + changingData._attributes.id;
					} else {
						if (changingData['region'].constructor !== Array) {
							changingData['region'] = [changingData['region']];
						}
						var currentRegion;
						changingData['region'].find(function (component) {
							if (component._attributes.type.toUpperCase() == targetNode.closest('[data-region-config="true"]').attr('data-region')) {
								currentRegion = component;
							}
						})
						if (currentRegion['file'].constructor !== Array) {
							currentRegion['file'] = [currentRegion['file']];
						}
						var currentLayout = $(currentTab + '#manuscriptsData .layouts.hide #' + targetNode.val().replace(/\s/g, '')).clone();
						var files = [];
						targetNode.parents('[data-region-config="true"]').find('.layoutBlock').attr('data-region-id', targetNode.attr('data-flag-type').toUpperCase()).attr('data-layout', targetNode.val());
						currentLayout.find('.layoutsection').each(function (index, node) {
							var obj = {};
							var currentFile = currentRegion['file'].find(function (currentFile) {
								return currentFile._attributes['data-layout'] === $(node).attr('layout-sec')
							})
							obj._attributes = {
								'data-color': (currentFile && currentFile._attributes['data-color']) ? currentFile._attributes['data-color'] : "",
								'data-layout': $(this).attr('layout-sec'),
								'data-region': changingData._attributes.type,
								'path': (currentFile && currentFile._attributes.path) ? currentFile._attributes.path : "",
								'type': "file"
							}
							$(this).html('<p class="fileObjUpload"> <span class="btn-primary btn-sm"> <input type="file" onchange="eventHandler.flatplan.issue.uploadFile(this);" region="' + changingData._attributes.type + '" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" data-layout-sec="' + $(this).attr('layout-sec') + '"> <span data-channel="flatplan" data-topic="issue" data-event="click" data-message="{\'funcToCall\': \'uploadFilePopup\'}" class="fileChooser"> <span class="icon-cloud-upload"></span> Upload</span> </span>  <input type="checkbox" data-flag-type="color" class="filled-in color" id="fg{fcIndex}" data-region-id="' + changingData._attributes.type + '" data-layout-sec="' + $(this).attr('layout-sec') + '" data-key-path="data-color"> <label for="fg{fcIndex}"> <i>Color in Print</i> </label> </p>');
							$(this).append('<object class="uploadObject" data="' + obj._attributes.path + '"></object>');
							if (obj._attributes['data-color'] != "") {
								$(this).find('input[type="checkbox"]').attr('checked', 'checked')
							}
							files.push(obj);
						})
						currentRegion['file'] = files;
						$(currentTab + '#regionTab #' + currentRegion._attributes.type.toUpperCase() + ' .layoutBlock').html(currentLayout.children());
						historyText = 'changed the ' + changingData._attributes.type + ' Advert Layout to ' + targetNode.val() + ' for ' + changingData._attributes.id;
						saveData = false;
					}
				} else if (className.match('unpaginated')) {
					var checked = (targetNode.prop('checked')) ? 1 : 0;
					if (checked) {
						changingData._attributes.type = 'advert_unpaginated';
						changingData._attributes['data-unpaginated'] = true;
					} else {
						changingData._attributes.type = 'advert';
						delete changingData._attributes['data-unpaginated'];
					}
					if (changingData.region.constructor !== Array) {
						changingData.region = [changingData.region];
					}
					changingData.region.map(function (reg) {
						return reg._attributes['data-advert-name'] = changingData._attributes.type.charAt(0).toUpperCase() + changingData._attributes.type.slice(1);
					})
					$(currentTab + '#manuscriptsDataContent #sectionGroup').find('[data-doi=' + changingData._attributes.id + ']').attr('data-type', changingData._attributes.type);
					if (changingData['flag']) {
						changingData['flag']._attributes.value = checked;
					} else {
						var flag = {
							'_attributes': {
								'type': 'unpaginated',
								'value': checked
							}
						}
						changingData['flag'] = flag;
					}
					eventHandler.flatplan.action.saveData();
					var metaFpage = (meta && meta['first-page'] && meta['first-page']._text) ? meta['first-page']._text : 1;
					if(changingData._attributes.grouptype && changingData._attributes.grouptype=='roman'){
						metaFpage = 'i';
					}
					var firstSecNode = $(currentTab + '#tocContentDiv [data-grouptype="'+changingData._attributes.grouptype+'"][data-c-id]:first').attr('data-c-id');
					if(changingData._attributes.grouptype && changingData._attributes.grouptype=='roman'){
						firstSecNode = $(currentTab + '#tocContentDiv [data-grouptype="'+changingData._attributes.grouptype+'"][data-section="'+changingData._attributes.section+'"][data-c-id]:first');
					}
					var firstSecNodeId = firstSecNode.attr('data-c-id');
					var firstSecFpage = firstSecNode.data('data')?firstSecNode.data('data')._attributes.fpage:metaFpage;
					if(changingData._attributes.grouptype && changingData._attributes.grouptype=='roman'){
						firstSecFpage = eventHandler.flatplan.components.fromRoman(firstSecFpage);
					}
					eventHandler.flatplan.components.updatePageNumber(firstSecNodeId, firstSecFpage, changingData._attributes.grouptype, { 'onsuccess': "$('#projectIssueList .active').trigger('click')" });
					var checkedText = (checked) ? 'Unpaginated' : 'Paginated';
					historyText = 'changed the Advert  ' + changingData._attributes.id + ' as ' + checkedText;
					saveData = true;
				} else if (className.match('advertLayoutColor')) {
					if (changingData['region'].constructor !== Array) {
						changingData['region'] = [changingData['region']];
					}
					var currentRegion;
					changingData['region'].find(function (component) {
						if (component._attributes.type.toUpperCase() == targetNode.closest('[data-region-config="true"]').attr('data-region')) {
							currentRegion = component;
							if (currentRegion['file'].constructor !== Array) {
								currentRegion['file'] = [currentRegion['file']];
							}
							currentRegion['file'].find(function (file) {
								if (file._attributes['data-layout'] == targetNode.parents('.layoutsection').attr('layout-sec')) {
									if (targetNode.prop('checked')) {
										file._attributes['data-color'] = 'cmyk';
									} else {
										file._attributes['data-color'] = '';
									}
									var dataColor = (file._attributes['data-color']) ? '1' : '0';
									historyText = 'changed ' + component._attributes.type.toUpperCase() + '' + file._attributes['data-layout'] + ' to ' + dataColor + ' of ' + changingData._attributes.id;
								}
							})
						}
					})

				}
				for (con in content) {
					if (content[con]._attributes.id == changingData._attributes.id) {
						content[con] = changingData;
					}
				}
				var histObj = {
					"change": historyText,
					"file": "",
					"fileType": "",
					"id-type": ""
				};
				eventHandler.flatplan.components.updateChangeHistory(histObj);
				if (saveData) {
					eventHandler.flatplan.action.saveData();
				}

			},
			saveNoteEditor: function(targetNode){
				var dataCID = $(currentTab + '#rightPanelContainer').attr('data-c-rid');
				// get data from the corresponding article
				// var myData = $(currentTab + '#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
				var myData = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].contents.content[dataCID];
				var id = myData._attributes.id;
				var summerNoteData = $('#rightPanelContainer .coverText')
				$(summerNoteData).each(function (){
					var currentData = this
					var keyPath = currentData['attributes']?currentData['attributes']["data-key-path"].textContent:''
					// var myData = $(currentTab + '#tocBodyDiv div[data-c-id="' + dataCID + '"]').data('data');
					var id = myData._attributes.id;
					if (keyPath) {
						var keyPathArr = keyPath.split('.');
						// the last key is required to update the data, if you include the key directly then we will get only the value
						var lastKey = keyPathArr.pop();
						keyPathArr.forEach(function (key) {
							if( myData[key]){
								myData = myData[key];
								dataFlag = true;
							}
							else{
								myData[key] = {}
								myData = myData[key]
							}

						})
						if(myData){
							var dArr = '';
							var thisContent = $(currentData).find('.note-editable')
							$(thisContent).contents().each(function () {
								if (this.nodeType == 3) {
									var ceData = this.nodeValue;
									ceData = ceData.replace(/^[\s\t\r\n\u00A0]+|[\s\t\r\n\u00A0]+$/gi, '');
								}
								else {
									var ceData = this.outerHTML;
									ceData = ceData.replace(/[\s\t\r\n\u00A0]*<\/?(br)\/?>[\s\t\r\n\u00A0]*/gi, '');
									ceData = ceData.replace(/</g, '[[').replace(/>/g, ']]');
								}
								dArr += ceData
							});
							var orgData = '';
							orgData = myData[lastKey] ? myData[lastKey] : '';
							if (myData[lastKey] == undefined) {
								myData[lastKey] = {};
							}
							myData[lastKey]['_text'] = dArr;
							var histObj = {
								"change": "changed <b>" + lastKey + "</b> of <i>"+ id +"</i> from <u> </u> to <u>" + dArr + "</u>",
								"file": "",
								"fileType": "",
								"id-type": ""
							};
							eventHandler.flatplan.components.updateChangeHistory(histObj);
						}
					eventHandler.flatplan.action.saveData();
					$(this).attr('data-dirty', 'false');
					}
				});
			},
		},
		toggle: {
			listView: function (param, targetNode) {
				if ($('#issueView').hasClass('active')) {
					eventHandler.flatplan.components.populateHistory();
					eventHandler.flatplan.issue.populateMetaContainer();
				}
				$(currentTab + '#exportArticles').removeClass('hidden');
				$(currentTab + '#bookletRegionVal').addClass('hidden');
				$(currentTab + '#tocContentDiv').removeClass('hidden');
				$(currentTab + '[data-href="flatPlanBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$(currentTab + '[data-href="tocBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				$(currentTab + '#manuscriptsDataContent #flatPlanBodyDiv').remove();
				$(currentTab + '#manuscriptsDataContent .sectionList').removeClass('hidden');
				$(currentTab + '#rightPanelContainer').children(':not([data-type=" history "])').addClass('hidden')
				$(currentTab + '#rightPanelContainer').children('[data-type=" history "]').removeClass('hidden')
			},
			previewPDF: function (param, targetNode) {
				$(currentTab + '#tocContentDiv').addClass('hidden');
				$(currentTab + '#bookletRegionVal').removeClass('hidden');
				var configData = $(currentTab+'#manuscriptsData').data('config');
				if($(currentTab + '#bookletRegionVal').length >0){
					$(currentTab + '#exportArticles').addClass('hidden');
					$(currentTab + '#bookletRegionVal').find('select[id=booklet_region_option]').html('');
					var regionsConfig = JSON.parse(JSON.stringify(configData.regions)).region;
					if (regionsConfig.constructor.toString().indexOf("Array") < 0){
						regionsConfig = [regionsConfig];
					}
					var regionArray = [];
					regionsConfig.forEach(function(region){
						var currRegion = region._attributes.type;
						regionArray.push(currRegion.toLowerCase());
					});
					var regionOption=''
					var uniqueRegionArray = Array.from(new Set(regionArray));
					uniqueRegionArray.forEach(function(regionVal){
						var isSelected = '';
						regionVal = regionVal.toLowerCase();
						regionOption += '<option value="' + regionVal + '"' + isSelected + '">' + regionVal.toUpperCase() + '</option>'
					});
					$(currentTab + '#bookletRegionVal').find('select[id=booklet_region_option]').append(regionOption)
					$(currentTab + '#bookletRegionVal').find('select[id=booklet_region_option]').attr('onchange',"eventHandler.flatplan.components.populateFlatPlan()");
				}
				eventHandler.flatplan.components.populateContents();
				eventHandler.flatplan.components.populateFlatPlan();
				/*eventHandler.components.actionitems.showRightPanel({
					'target': 'bookpreviewinfo'
				});*/
			},
			tocSection: function (param, targetNode) {
				if ($(targetNode).hasClass('sectionData')) {
					if ($(targetNode).parent().hasClass('active')) {
						if ($(currentTab + '#manuscriptsData .tocIcons .fa-arrows').length) {
							$(targetNode).parent().siblings('.sectionList').removeClass('active');
							$(targetNode).parent().siblings('.sectionList').find('.openclose').addClass('fa-caret-right').removeClass('fa-caret-down');
							$(currentTab + '#manuscriptsData .tocIcons .fa-arrows').removeClass('fa-arrows').addClass('fa-arrows-alt');
						} else {
							$(targetNode).parent().removeClass('active');
							$(targetNode).parent().find('.openclose').addClass('fa-caret-right').removeClass('fa-caret-down');
						}
					} else {
						$(targetNode).parent().addClass('active');
						$(currentTab + '.sectionList.active').find('.openclose').removeClass('fa-caret-right').addClass('fa-caret-down');
					}
					$(targetNode).parent().siblings('.sectionList').find('.openclose').addClass('fa-caret-right').removeClass('fa-caret-down');
					$(targetNode).parent().siblings('.sectionList').removeClass('active');
				} else if ($(targetNode).hasClass('fa-arrows-alt')) {
					$(currentTab + '.sectionData.hasChild').addClass('active').find('.openclose').addClass('fa-caret-down').removeClass('fa-caret-right');
					$(currentTab + '.sectionList:has(div.hasChild)').addClass('active');
					$(targetNode).removeClass('fa-arrows-alt').addClass('fa-arrows');
				} else if ($(targetNode).hasClass('fa-arrows')) {
					$(currentTab + '.sectionData.hasChild').removeClass('active').find('.openclose').removeClass('fa-caret-down').addClass('fa-caret-right');
					$(currentTab + '.sectionList:has(div.hasChild)').removeClass('active');
					$(targetNode).removeClass('fa-arrows').addClass('fa-arrows-alt');
				}
			}
		},
		errorCode: {
			//error in get method
			500: {
				1: 'Something went wrong when fetching Data.',
				2: 'Something went wrong when fetching Customer List.',
				3: 'Something went wrong when fetching data from ',
				4: 'Something went wrong when fetching Project List',
				5: 'Something went wrong when retrieving Issue List.',
				6: 'Something went wrong when fetching Issue Data.',
				7: 'Something went wrong when fetching Project Stages.',
				8: 'Something went wrong when fetching Advert/Filler layouts.',
				9: 'Something went wrong when fetching Article.',
				10: 'Something went wrong when retrieving Article List.',
			},
			//error in post method
			501: {
				1: 'Saving failed.',
				2: 'Error while saving article XML.',
				3: 'Upload failed.',
				4: 'Saving Meta Infofailed.',
				5: 'Something went wrong when combining pdf.',
				6: 'Error in Creating Issue.'
			},
			//incorrect info
			502: {
				1: 'Unable to add the requested file.',
				2: 'Filler can oly be added below Manuscript.',
				3: 'Project already Exists.',
				4: 'Book || Customer is Missing.',
				5: 'Incorrect page sequence.',
				6: 'Issue XML already Exists.',
				7: 'Empty Value Found !',
				8: 'PDF is not uploaded for Non-Kriya Articles.'

			},
			//prompt message
			503: {
				1: 'Please try again or contact support.',
				2: 'Please correct RO XML and re-upload.',
				3: 'Please upload PDF and try again',
				4: 'already processed for this Issue. Pls export Individual PDF from List View'
			}
		},
		successCode: {
			200: {
				1: 'Upload Succeeded',
				2: 'Issue Created',
				3: 'All Changes Saved',
				4: 'Meta Info Saved',
				5: 'Article XML Saved'
			}
		},
		export: {
			exportToPDF: function (param, targetNode) {
				var configData = $(currentTab +'#manuscriptsData').data('config');
				var metaObj = $(currentTab + '#manuscriptsData').data('binder')['container-xml'].meta;
				var firstMsCid = $(currentTab + '#tocContentDiv [data-section="chapters"] .msCard.card:eq(0)').attr('data-uuid');
				var cid = $(currentTab + '#rightPanelContainer').attr('data-c-rid');

				if (param && param.cid) {
					cid = param.cid;
				}
				var uid = $(currentTab+'#rightPanelContainer').attr('data-r-uuid');
				let currContent;
				if ($(currentTab + '[data-uuid ="' + uid + '"]').length > 0) {
					currContent = $(currentTab + '[data-uuid ="' + uid + '"]');
				}
				if (param && param.uid) {
					uid = param.uid
				}
				var proofType = 'print';
				var processType = 'InDesignSetter';
				var exportAllarticleFlag = 'false';
				if (param && param.type) {
					proofType = param.type;
				}

				if(proofType=='print_online'){
					proofType = 'online';
				}
				if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
				if (param && param.processType) processType = param.processType;
				//Updated by ANuraja to get project exact value for book
				var customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
				var project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
				// need to store data before getting this valu - pending - jai
				var projectID = $('#projectVal').attr('data-project-id')
				let probeParam = {
					'contentType': currContent.attr('data-type'),
					'processType': processType,
					'doi': currContent.attr('data-doi'),
					'customer': customer,
					'project': project,
					'role': 'typesetter',
					'rules': 'proof',
					'stage': 'issueMakeup',
				}
				/*Probe Validation is removed for now
				TO re-test probe validation*/
						//probeNotifyText.innerHTML = 'No error in probe validation';
							//continue proofing
						var doinum = $(currentTab + '#rightPanelContainer').attr('data-dc-doi');
						var draftFlag = false
						if (metaObj.issue && metaObj.issue['_attributes'] && metaObj.issue['_attributes']["issue-type"] && metaObj.issue['_attributes']["issue-type"].toLowerCase() == 'draft') {
							draftFlag = true
						}
						if (param && param.draftFlag) {
							draftFlag = param.draftFlag
						}
						//if draft flag is true and index is 0, call exportModofied with param.type='draft' to export all manuscript
						var processAllArticles = false;
						if (firstMsCid == uid && draftFlag == true && processAllArticles == true) {
							param.type = 'draft'
							eventHandler.flatplan.export.exportModified(param, 0);
						}
						else {
							//var articleObj = $('[data-c-id="' + cid + '"]').data('data');
							var articleObj;
							if ($(currentTab + '[data-uuid ="' + uid + '"]').length > 0) {
								articleObj = $(currentTab + '[data-uuid ="' + uid + '"]').data('data');
							} else if ($(currentTab + '.msCard.highlightCard').length > 0) {
								articleObj = $(currentTab + '.msCard.highlightCard').data('data');
							} else {
								//unable to proof / preflight doi
								if (param && param.error && typeof (param.error) == "function") {
									param.error();
								}
								return false;
							}
							if (articleObj['_attributes']['type'] == "blank") {
								if (param && param.error && typeof (param.error) == "function") {
									param.error();
								}
								return false;
							}

							var doi = articleObj['_attributes']['id'];
							var ms_id = articleObj['_attributes']['ms-id'] ? articleObj['_attributes']['ms-id'] : articleObj['_attributes']['id']
							// var customer = $('#filterCustomer .filter-list.active').attr('value');

							var fpage = articleObj['_attributes']['fpage'];
							var lpage = articleObj['_attributes']['lpage'];
							//probeNotifyObj.remove();
							var userName = userData.kuser.kuname.first;//$('.userinfo:first').attr('data-name');
							var userRole = 'typesetter';

							if (param && param.doi) {
								doi = param.doi;
							}

							parameters = {
								"client": customer,
								"project": project,
								"idType": "doi",
								"id": doi,
								"msid": ms_id,
								//"processType": "InDesignSetter",
								"processType": processType,
								"proof": proofType,
								//"volume"     : metaObj.volume._text,
								//"issue"      : metaObj.issue._text,
								"fpage": fpage,
								"lpage": lpage,
								"userName": userName,
								"userRole": userRole,
								"uuid": uid,
								"exportAllArticles": exportAllarticleFlag
							}
							if (projectID && projectID != '') {
								parameters['project-id'] = projectID
							}
							if (metaObj.volume) {
								parameters.volume = metaObj.volume._text;
							}
							if (metaObj.issue) {
								parameters.issue = metaObj.issue._text;
							}
							var colorObj = articleObj['color']

							var dataColor = '';
							if (colorObj) {
								if (colorObj.constructor.toString().indexOf("Array") == -1) {
									colorObj = [colorObj];
								}
								colorObj.forEach(function (color) {
									if (color['_attributes']['color'] == '1') {
										dataColor += color['_attributes']['ref-id'] + ',';
									}
								})
							}

							dataColor = dataColor.replace(/\,$/, '');
							if (dataColor) {
								parameters['data-color'] = dataColor;
							}
							var artFlagObj = articleObj['flag'];
							if (artFlagObj) {
								if (artFlagObj.constructor.toString().indexOf("Array") == -1) {
									artFlagObj = [artFlagObj];
								}
								artFlagObj.forEach(function (flag) {
									if (flag['_attributes'] && flag['_attributes']['type'] && flag['_attributes']['type'] == 'offline' && flag['_attributes']['type'] =='1') {
											parameters['offline-proof'] = 'true';
									}
								});
							}
							var title = '';
							var volume = metaObj.volume ? metaObj.volume._text : ''
							var issue = metaObj.issue ? metaObj.issue._text : ''
							var fileName = $('#issueVal').attr('data-file');
							if (doi) {
								title = 'Proofing ' + doi +' - '+proofType;
								parameters.issueMakeup = 'true';
								if (fileName && (/(draft)/gi.test(fileName))) {
									parameters.draftIssue = 'true';
								}
							}
							if (articleObj['_attributes']['type'] == 'cover') {
								parameters.pheripherals = 'cover';
								if (volume && issue) {
									parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
									parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_cover';
								}
								else {
									parameters.id = project + '_' + doi;
									parameters.doi = project + '_cover';
								}
								title = 'Proofing Cover'+' - '+proofType;
							}
							else if (articleObj['_attributes']['type'] == 'table-of-contents' || articleObj['_attributes']['type'] == 'toc') {
								parameters.pheripherals = 'toc';
								if (volume && issue) {
									parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
									parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_toc';
								}
								else {
									parameters.id = project + '_' + doi;
									parameters.doi = project + '_toc';
								}
								title = 'Proofing Toc'+' - '+proofType;
							}
							else if (articleObj['_attributes']['type'] == 'index') {
								parameters.index = 'true';
								if (volume && issue) {
									parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
									parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_index';
								}
								else {
									parameters.id = project + '_' + doi;
									parameters.doi = project + '_index';
								}
								title = 'Proofing Index'+' - '+proofType;
							}
							else if (articleObj['_attributes']['type'] == 'advert' || articleObj['_attributes']['type'] == 'advert_unpaginated') {
								var currRegion = targetNode.closest('div').parent().attr('data-region')
								parameters.pheripherals = 'advert';
								parameters.advertID = doi;
								if (volume && issue) {
									parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
								}
								else {
									parameters.id = project + '_' + doi + '_' + currRegion;
								}
								title = 'Proofing Advert_' + doi + '_' + currRegion+' - '+proofType;
								parameters.region = currRegion;
								if (volume && issue) {
									parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
								}
								else {
									parameters.doi = project + '_' + doi + '_' + currRegion;
								}
							}
							if (processType == 'PDFPreflight') {
								parameters.doi = parameters.doi ? parameters.doi : doi
								title = 'Preflight for ' + parameters.doi;
								parameters.pdfLink = param.pdfLink
								parameters.profile = param.profile;
								var type = articleObj['_attributes']['type'];
								//if type is manuscript and exist dataColor change profile to cmyk
								if (type == 'manuscript' && dataColor && param.profile == 'bw') {
									parameters.profile = 'cmyk'
								}
								else if (type == 'manuscript' && dataColor && param.profile == 'bw+1') {
									parameters.profile = 'cmyk+1'
								}
							}

							var notifyObj = new PNotify({
								title: title,
								text: '<div class="progress"><div class="determinate" style="width: 0%"></div><span class="progress-text">Sending request..</span></div>',
								hide: false,
								type: 'success',
								buttons: { sticker: false },
								addclass: 'export-pdf',
								icon: false
							});
							var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
							var notifyContainer = notifyObj.container[0];
							$(notifyContainer).attr('data-c-nid', cid);
							$(notifyContainer).attr('data-c-nuid', uid);

							$.ajax({
								type: 'POST',
								url: "/api/pagination",
								data: parameters,
								crossDomain: true,
								success: function (data) {
									if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))) {
										eventHandler.flatplan.export.getJobStatus(customer, project, data.message.jobid, notifyObj, userName, userRole, doi, param);
									}
									else if (data == '') {
										notifyText.innerHTML = 'Failed..';
										if (param && param.error && typeof (param.error) == "function") {
											param.error();
										}
									} else {
										notifyText.innerHTML = data.status.message;
										if (param && param.error && typeof (param.error) == "function") {
											param.error();
										}
									}
								},
								error: function (error) {
									notifyText.innerHTML = 'Failed..';
									if (param && param.error && typeof (param.error) == "function") {
										param.error();
									}
								}
							});
						}
			},
			getJobStatus: function (customer, project, jobID, notifyObj, userName, userRole, doi, param) {
				var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
				var progressBar = $(notifyObj.text_container[0]).find('.determinate');
				var notifyTitle = notifyObj.title_container[0];
				var notifyContainer = notifyObj.container[0];

				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: { "id": jobID, "userName": userName, "userRole": userRole, "doi": doi, "customer": customer, "project":project ,"issueMakeup":"true"},
					crossDomain: true,
					success: function (data) {
						if (data && data.status && data.status.code && data.status.message && data.status.message.input && data.status.message.input.uuid) {
							var proofUUID = data.status.message.input.uuid
							if ($(currentTab +'*[data-uuid="' + proofUUID + '"]').length > 0) {
								if (data && data.status && data.status.code && data.status.message && data.status.message.status) {
									var status = data.status.message.status;
									var code = data.status.code;
									var currStep = 'Queue';
									if (data.status.message.stage.current) {
										currStep = data.status.message.stage.current;
									}
									if (data.status.message.progress) {
										var barWidth = data.status.message.progress / 3;
										if (data.status.message.stage.current == "collectfiles") {
											barWidth = barWidth + '%';
										} else if (data.status.message.stage.current == "proofing") {
											barWidth = (barWidth + 33.3) + '%';
										} else if (data.status.message.stage.current == "uploadPDF") {
											barWidth = (barWidth + 66.6) + '%';
										}

										progressBar.css('width', barWidth);
									}
									var loglen = data.status.message.log.length;
									var process = data.status.message.log[loglen - 1];
									if (/completed/i.test(status)) {
										var loglen = data.status.message.log.length;
										var process = data.status.message.log[loglen - 1];
										var processType = param.processType;
										var proofDoi, proofId;
										var configData = $(currentTab +'#manuscriptsData').data('config');
										if (data.status.message.input && data.status.message.input.doi) {
											proofDoi = data.status.message.input.doi
										}
										if (data.status.message.input && data.status.message.input.id) {
											proofId = data.status.message.input.id
										}
										//notifyText.innerHTML = process;
										if (processType == 'PDFPreflight') {
											var preflightLink = param.pdfLink;
											preflightLink = preflightLink.replace('.pdf', '_preflight.pdf')

											//display preflight link to user
											notifyTitle.innerHTML = preflightLink;
											notifyObj.remove();
											//var nid = $(notifyContainer).attr('data-c-nid');
											var nid = proofUUID
											if (nid && nid != '' && $(currentTab +'[data-uuid="' + nid + '"]').length > 0) {
												//set preflight flag as true
												var dataObj = $(currentTab +'[data-uuid="' + nid + '"]').data('data');
												dataObj['_attributes']['data-preflight'] = 'true';
												$(currentTab +'[data-uuid="' + nid + '"]').attr('data-preflight', 'true');
												var fileFlag = false;
												// save preflight pdf path in xml
												// for advert update file name in region
												//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
												if ((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated')) {
													var currRegion = targetNode.closest('div').parent().attr('data-region')
													currRegion = currRegion.toLowerCase();
													var fileFlag = false;
													var regObj = dataObj['region'];
													if (regObj) {
														if (regObj.constructor.toString().indexOf("Array") == -1) {
															regObj = [regObj];
														}
														regObj.forEach(function (region) {
															if (region['_attributes']['type'] == currRegion) {
																//region['_attributes']['path'] = pdfLink;
																var fileObj = region['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	if (file['_attributes'] && file['_attributes']['type'] && file['_attributes']['type'] == 'preflightPdf') {
																		file['_attributes']['path'] = preflightLink;
																		fileFlag = true;
																	}
																})
																if (fileFlag == false) {
																	var file = ({ "_attributes": { "type": "preflightPdf", "path": preflightLink } })
																	fileObj.push(file)
																	region['file'] = fileObj;
																}
															}
														})
														eventHandler.flatplan.action.saveData();
														// save the preflight link in li to download
														if (targetNode) {
															targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('target', '_blank');
															targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('data-href', preflightLink)
														}
													}
												}
												else { //else if(dataObj['file']){
													var fileFlag = false
													var fileObj = []
													if (dataObj['file']) {
														fileObj = dataObj['file'];
														if (fileObj.constructor.toString().indexOf("Array") == -1) {
															fileObj = [fileObj];
														}
														fileObj.forEach(function (file) {
															var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
															if (pdfType && pdfType == 'preflightPdf') {
																file['_attributes']['path'] = preflightLink;
																fileFlag = true;
															}
														})
													}
													if (fileFlag == false) {
														var file = ({ "_attributes": { "type": "preflightPdf", "path": preflightLink } })
														fileObj.push(file)
														dataObj['file'] = fileObj;
													}
												}
												// save the preflight link in li to download
												if ($(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').length > 0) {
													$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('ul li[data-preflight="true"] a').attr('target', '_blank');
													$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('ul li[data-preflight="true"] a').attr('data-href', preflightLink);
												}

												eventHandler.flatplan.action.saveData();
												//var exportAllarticleFlag = 'false';
												//if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
												/*if(exportAllarticleFlag == 'false'){
													//send request to online pdf
													if(nid){
														param.uid = nid
													}
													param.processType = 'InDesignSetter';
													param.type = 'online';
													eventHandler.pdf.export.exportToPDF(param,targetNode);
												}*/
											}
										}
										else {

											var proofType = 'print';
											if (param && param.type) {
												proofType = param.type;
											}
											if(proofType=='print_online'){
												proofType = 'online';
											}
											notifyTitle.innerHTML = proofType+' PDF generation completed.';
											var pdfLink = $(process).attr('href');
											//pdfLink =pdfLink.replace('http:','https:');
											if (window.location.protocol == 'https:') {
												pdfLink = pdfLink.replace('http:', 'https:');
											}
											// get endPage and PageCount from JobManager
											var endPage, pageCount;
											var datajsonObj = $(process).attr('data-json');
											if (pdfLink && processType == 'InDesignSetter') {
												notifyObj.remove();
												var nid = proofUUID;
												if (nid && nid != '' && $('*[data-uuid="' + nid + '"]').length > 0) {
													// if prooftype is print update the link in object , update download url
													if ($('#rightPanelContainer[data-r-uuid="' + nid + '"]').length > 0) {
														var proofingType = 'online';
														if (proofType == 'print') {
															proofingType = proofType;
															$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('object').attr('data', pdfLink);
															$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('object').css('display', 'block');
														}
														$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('ul li[data-proofType="' + proofingType + '"] a').attr('target', '_blank');
														$(currentTab + '#rightPanelContainer[data-r-uuid="' + nid + '"]').find('ul li[data-proofType="' + proofingType + '"] a').attr('data-href', pdfLink);
													}
													if (nid) {
														var dataObj = $(currentTab + '[data-uuid="' + nid + '"]').data('data');
														var currentDoi = dataObj._attributes.id;
														dataObj._attributes['data-proof-doi'] = proofDoi;
														//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
														if ((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated')) {
															var currRegion = targetNode.closest('div').parent().attr('data-region')
															currRegion = currRegion.toLowerCase();
															var fileFlag = false;
															var regObj = dataObj['region'];
															if (regObj) {
																if (regObj.constructor.toString().indexOf("Array") == -1) {
																	regObj = [regObj];
																}
																regObj.forEach(function (region) {
																	if (region['_attributes']['type'] == currRegion) {
																		//region['_attributes']['path'] = pdfLink;
																		region['_attributes']['data-proof-doi'] = proofDoi;
																		var fileObj = region['file'];
																		if (fileObj.constructor.toString().indexOf("Array") == -1) {
																			fileObj = [fileObj];
																		}
																		fileObj.forEach(function (file) {
																			if (file['_attributes'] && file['_attributes']['proofType'] && file['_attributes']['proofType'] == proofType) {
																				file['_attributes']['path'] = pdfLink;
																				fileFlag = true;
																			}
																		})
																		if (fileFlag == false) {
																			var file = ({ "_attributes": { "type": "pdf", "proofType": proofType, "path": pdfLink } })
																			fileObj.push(file)
																			region['file'] = fileObj;
																		}
																	}
																})
																eventHandler.flatplan.action.saveData();
																// save the pdf link in li to download
																if (proofType == 'print' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('data-href', pdfLink);
																}
																else if (proofType == 'online' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('data-href', pdfLink);
																}
															}
														}
														//loop through each file and update pdflink in path if proofType matches
														else {//dataObj['file']
															dataObj._attributes['data-proof-doi'] = proofDoi;
															var fileFlag = false
															var fileObj = []
															if (dataObj['file']) {
																fileObj = dataObj['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																	var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																	if (currProofType == proofType && pdfType != 'preflightPdf') {
																		file['_attributes']['path'] = pdfLink;
																		fileFlag = true
																	}
																})
															}
															if (fileFlag == false) {
																var file = ({ "_attributes": { "type": "pdf", "proofType": proofType, "path": pdfLink } })
																fileObj.push(file)
																dataObj['file'] = fileObj;
															}
														}
														if (datajsonObj) {
															datajsonObj = datajsonObj.replace(/\'/g, '"')
															var datajson = JSON.parse(datajsonObj)
															endPage = datajson['LPAGE']
															pageCount = datajson['PAGE_COUNT']
															//update the  page details of figures
															var colorObj = dataObj['color']
															if (colorObj) {
																if (colorObj.constructor.toString().indexOf("Array") == -1) {
																	colorObj = [colorObj];
																}
																for (var a in datajson) {
																	if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																		var pageNum = datajson[a];
																		a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/, '$1$2')
																		colorObj.forEach(function (color) {
																			if (color['_attributes'] && color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a) {
																				color['_attributes']['data-page-num'] = pageNum
																			}
																		})
																	}
																}
															}
															var runOnPagesDetails = datajson['RunOnPagesDetails']
															//if endpage, update endpage in issue xml,
															//update page range in UI and update the following article page number if there is change, call updatepage number function
															if (endPage) {
																if ((!dataObj['_attributes']['data-run-on']) || dataObj['_attributes']['data-run-on'] != 'true') {
																	//check the config and match the prooftype. if the prooftype is mentioned in config update pagenumber
																	//if attrib is not available in config, update pagenumber
																	//exclude pagenumber update when follow-on is 1
																	if ((configData['page']) && (configData['page']._attributes['pageNumberProofType'])) {
																		var values = configData['page']._attributes['pageNumberProofType'].toLowerCase()
																		values = values.split(',');
																		if (values.constructor.toString().indexOf("Array") < 0) {
																			values = [values];
																		}
																		values.forEach(function (value) {
																			if (value == proofType) {
																				dataObj['_attributes']['lpage'] = endPage;
																				dataObj['_attributes']['pageExtent'] = pageCount;
																				var seqId = $(currentTab + '[data-uuid="' + nid + '"]').attr('data-c-id');
																				eventHandler.flatplan.components.updatePageNumber(seqId, dataObj['_attributes']['fpage'], dataObj['_attributes']["grouptype"]);
																			}
																		})
																	}
																	//if data-proofType is print_online, update page number only for print type
																	else if(param && param['data-proofType'] && param['data-proofType']=='print_online'){
																		if(proofType=='print'){
																			dataObj['_attributes']['lpage'] = endPage;
																			dataObj['_attributes']['pageExtent'] = pageCount;
																			var seqId = $(currentTab + '[data-uuid="' + nid + '"]').attr('data-c-id');
																			eventHandler.flatplan.components.updatePageNumber(seqId, dataObj['_attributes']['fpage'], dataObj['_attributes']["grouptype"]);
																		}
																	}
																	else {
																		dataObj['_attributes']['lpage'] = endPage;
																		dataObj['_attributes']['pageExtent'] = pageCount;
																		var seqId = $(currentTab + '[data-uuid="' + nid + '"]').attr('data-c-id');
																		eventHandler.flatplan.components.updatePageNumber(seqId, dataObj['_attributes']['fpage'], dataObj['_attributes']["grouptype"]);
																	}
																}
															}
															//if current artcile has run-on articles - update pdflink in all run-on articles, and update the f-page, l-page for run-on articles
															if (runOnPagesDetails) {
																var currArtObj = $(currentTab + '[data-uuid="' + nid + '"]');
																var mainArtDoi = $(currArtObj).attr('data-doi')
																var subarticles = currArtObj.nextUntil('[data-c-id][data-type="manuscript"][data-run-on!="true"]')
																subarticles.each(function () {
																	var currNode = $(this).data('data');
																	if (currNode) {
																		var doi = currNode._attributes.id
																		//if(doi == currNode._attributes.id){
																		if (doi && $('[data-c-id][data-doi="' + doi + '"][data-runon-with="' + mainArtDoi + '"]').length > 0) {
																			var followOnAricle = $('[data-c-id][data-doi="' + doi + '"][data-runon-with="' + mainArtDoi + '"]').data('data')
																			if (followOnAricle['file']) {
																				var fileObj = followOnAricle['file'];
																				if (fileObj.constructor.toString().indexOf("Array") == -1) {
																					fileObj = [fileObj];
																				}
																				// update pdflink based on proofType
																				fileObj.forEach(function (file) {
																					var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																					var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																					if (currProofType == proofType && pdfType != 'preflightPdf') {
																						file['_attributes']['path'] = pdfLink;
																					}
																				})
																			}
																			// update the fpage,lpage, figure page details  for run-on articles
																			if (runOnPagesDetails[doi]) {
																				followOnAricle['_attributes']['fpage'] = runOnPagesDetails[doi]['f-page']
																				followOnAricle['_attributes']['lpage'] = runOnPagesDetails[doi]['l-page']
																				if (followOnAricle['color']) {
																					for (var a in runOnPagesDetails[doi]) {
																						if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																							var pageNum = runOnPagesDetails[doi][a];
																							a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/, '$1$2')
																							var runOnColorObj = followOnAricle['color']
																							if (runOnColorObj) {
																								if (runOnColorObj.constructor.toString().indexOf("Array") == -1) {
																									runOnColorObj = [runOnColorObj];
																								}
																								runOnColorObj.forEach(function (color) {
																									if (color['_attributes'] && color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a) {
																										color['_attributes']['data-page-num'] = pageNum
																									}
																								})
																							}
																						}
																					}
																				}
																			}
																		}

																		//}
																	}
																});
															}
														}
														//remove data-modified attrib from xml,UI and remove title - pageModified
														if (dataObj._attributes['data-modified'] && (dataObj._attributes['data-modified'].toLowerCase() == 'true')) {
															$(currentTab + '[data-uuid="' + nid + '"]').removeAttr('data-modified', 'true');
															$(currentTab + '[data-uuid="' + nid + '"]').find('.pageRange').removeAttr('title', 'Page is Modified');
															$(currentTab + '[data-uuid="' + nid + '"]').find('.pageRange').removeClass('pageModified');
															delete dataObj._attributes['data-modified'];
														}
														//set preflight flag as false
														dataObj['_attributes']['data-preflight'] = 'false';
														$(currentTab + '[data-uuid="' + nid + '"]').attr('data-preflight', 'false');
														eventHandler.flatplan.action.saveData();
													}
													//	when proof type is print and processType is  InDesignSetter, send request to Preflight
													if (proofType == 'print' && param.processType == 'InDesignSetter') {
														param.processType = 'PDFPreflight';
														param.pdfLink = pdfLink;
														if (nid) {
															param.uid = nid
														}
														//get preflight profile from config
														var type = dataObj['_attributes']['type'];
														var preflightProfile = '';
														if (configData["binder"][type] && configData["binder"][type]['_attributes'] && configData["binder"][type]['_attributes']['data-preflight-profile']) {
															preflightProfile = configData["binder"][type]['_attributes']['data-preflight-profile']
														}
														if (preflightProfile) {
															param.profile = preflightProfile;
															eventHandler.flatplan.export.exportToPDF(param, targetNode);
														}
													}
													else if(proofType == 'online' && param.processType == 'InDesignSetter' && param['data-proofType'] && param['data-proofType']=='print_online'){
														param.type ='print'
														eventHandler.flatplan.export.exportToPDF(param, targetNode);
													}
												}
											}
											if (param && param.success && typeof (param.success) == "function") {
												param.success();
											}
										}
									}
									else if (/failed/i.test(status) || code == '500') {
										notifyText.innerHTML = process;
										if (param.processType == 'InDesignSetter') {
											notifyTitle.innerHTML = 'PDF generation failed.';

										}
										else {
											notifyTitle.innerHTML = 'Preflight failed.';
										}
										if (param && param.error && typeof (param.error) == "function") {
											param.error();
										}
										//$('#'+notificationID+' .kriya-notice-body').html(process);
										//$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
									}
									else if (/no job found/i.test(status)) {
										notifyText.innerHTML = status;
									}
									else {
										if (data.status.message.displayStatus) {
											notifyText.innerHTML = data.status.message.displayStatus;
										}
										var loglen = data.status.message.log.length;
										if (loglen > 1) {
											var process = data.status.message.log[loglen - 1];
											if (process != '') notifyText.innerHTML = process;
										}
										setTimeout(function () {
											eventHandler.flatplan.export.getJobStatus(customer, project, jobID, notifyObj, userName, userRole, doi, param);
										}, 1000);
									}
								}
								else {
									notifyText.innerHTML = 'Failed';
								}
							}
							else {
								//uuid is not present in current xml, so close the notify object
								notifyObj.remove();
							}
						}
						else {
							//uuid is not returned from job manager
							notifyObj.remove();
						}
					},
					error: function (error) {
						if(error.status == 502){
							setTimeout(function() {
								eventHandler.flatplan.export.getJobStatus(customer, project, jobID, notifyObj, userName, userRole, doi, param);
							}, 1000 );
						}
						else if (param && param.error && typeof (param.error) == "function") {
							param.error();
						}
						else{
							notifyText.innerHTML = 'Unable to get JobStatus';
						}
					}
				});
			},
			exportExcel: function () {
				var contentData = $(currentTab +'#manuscriptsData').data('binder')['container-xml']['contents']['content']; // cloning to new object
				var configData = $(currentTab +'#manuscriptsData').data('config');
				var info = new Array();
				var totalPages = 0;
				var mergeCount=1;
				//seetha kumaraswamy{exeter} getting the value of color page and toc and edboard from issue config
				var coverColor=configData["binder"]["cover"]._attributes["data-preflight-profile"];
				var tocColor = '';
				if(configData && configData["binder"] && configData["binder"]["table-of-contents"] && configData["binder"]["table-of-contents"]._attributes && configData["binder"]["table-of-contents"]._attributes["data-preflight-profile"]){
					tocColor = configData["binder"]["table-of-contents"]._attributes["data-preflight-profile"];
				}else if(configData && configData["binder"] && configData["binder"]["toc"] && configData["binder"]["toc"]._attributes && configData["binder"]["toc"]._attributes["data-preflight-profile"]){
					tocColor = configData["binder"]["toc"]._attributes["data-preflight-profile"];
				}
				var edBoardColor=configData["binder"]["editorial-board"]._attributes["data-preflight-profile"];
				// Maniplating and collecting data from json
				for (var data in contentData) {
					var fPage = contentData[data]._attributes["fpage"];
					var lPage = contentData[data]._attributes["lpage"];
					var id = contentData[data]._attributes["id"];
					if ((typeof (id) != "string") && (typeof (id) != "number")) { // to handle is manuscript id not present
						contentData[data]._attributes["id"] = "";
					}
					if (contentData[data]._attributes["type"] == "cover") {
						contentData[data]._attributes["id"] = "Cover";
					}
					else if (contentData[data]._attributes["type"] == "editorial-board") {
						contentData[data]._attributes["id"] = "Editorial board";
					}
					else if (contentData[data]._attributes["type"] == "table-of-contents") {
						contentData[data]._attributes["id"] = "TOC";
					}
					else if ((contentData[data]._attributes["type"] == "advert_unpaginated") || (contentData[data]._attributes["type"] == "advert")) {
						contentData[data]._attributes["adverts"] = contentData[data]._attributes["id"];
						contentData[data]._attributes["id"] = "Advert";
						contentData[data]._attributes["advert_Pages"] = " ";
					}
					if (/[a-zA-Z]/gi.test(fPage)) {
						fPage = 0;
					}
					if (/[a-zA-Z]/gi.test(lPage)) {
						lPage = 0;
					}
					var figColorData = contentData[data].color;
					var figColorDetails = "";
					var monoPages = "";
					var colorPg = "";
					var page_1=[],count_1=0;
					var colorPageFlag = true;
					if ((typeof (figColorData) == "object") && (typeof (figColorData.length) == "undefined")) { // handling article with one figure
						figColorData = [figColorData];
					}
					for (var fig in figColorData) {
						if (typeof (figColorData[fig]._attributes) == "object") { // fetching color page details
							if (parseInt(figColorData[fig]._attributes["color"])) {
								if (fig == 0) {
									figColorDetails = figColorData[fig]._attributes["label"].replace(/[a-zA-Z]+\s/gi, "");
									page_1[count_1] =parseInt(figColorData[fig]._attributes["data-page-num"]);
								}
								else {
									figColorDetails = figColorDetails + ", " + figColorData[fig]._attributes["label"].replace(/[a-zA-Z]+\s/gi, "");
									page_1[count_1] =parseInt(figColorData[fig]._attributes["data-page-num"]);
								}
							}
							else if (figColorData[fig]._attributes["type"] == "page") {
								monoPages = " ";
								figColorDetails = "Colour";
							}
						}
						count_1=count_1+1;
					}
					//seetha kumaraswamy{exeter}page range for the color figure pages
					var counter=-1,index=0,colorfigpg = [];

					while (index<(page_1.length))
					{
						if(page_1[index]+1==page_1[index+1] && counter==-1)
						{
							colorfigpg[colorfigpg.length]=page_1[index]+"-";
    						counter=page_1[index];
						}
						while(page_1[index]==counter)
						{
							counter=counter+1;
							index=index+1;
							if(index==(page_1.length))
							{
								break;
							}
						}
						if(counter!=-1)
						{
							colorfigpg[(colorfigpg.length)-1]=colorfigpg[(colorfigpg.length)-1]+(page_1[index-1]);
						}
						else
						{
							colorfigpg[colorfigpg.length]=page_1[index];
							index=index+1;
						}
					counter=-1;

					}
					//seetha kumaraswamy{exeter}page range program ends here
					if (figColorDetails == "") {
						figColorDetails = " ";
						if (contentData[data]._attributes["type"] == "manuscript") { // collecting mono page details
							monoPages = fPage + "-" + lPage;
						}
						else {
							monoPages = "Mono";
						}
					}
					else
					{
						//seetha kumaraswamy{exeter}to find the pages without figures and store it into array
						var fPage1 = parseInt(fPage);
						var lPage1 = parseInt(lPage);
						var count_2,bwpg=[],counter_1=0,count,counter_1=0;
						for(count_1=fPage1;count_1<=lPage1;count_1++)
						{
							count=0;
							for(count_2=0;count_2<page_1.length;count_2++)
							{
								if(count_1!=page_1[count_2])
								{
									count=count+1

								}
								if(count==page_1.length)
								{
									bwpg[counter_1]=count_1;
									counter_1=counter_1+1;
								}
							}

						}
						//seetha kumaraswamy{exeter} to set the page range for the black&white pages
					counter=-1,index=0;
					var monoPages = [];

					while (index<(bwpg.length))
					{
						if(bwpg[index]+1==bwpg[index+1] && counter==-1)
						{
							monoPages[monoPages.length]=bwpg[index]+"-";
    						counter=bwpg[index];
						}
						while(bwpg[index]==counter)
						{
							counter=counter+1;
							index=index+1;
							if(index==(bwpg.length))
							{
								break;
							}
						}
						if(counter!=-1)
						{
							monoPages[(monoPages.length)-1]=monoPages[(monoPages.length)-1]+(bwpg[index-1]);
						}
						else
						{
							monoPages[monoPages.length]=bwpg[index];
							index=index+1;
						}
					counter=-1;

					}
					//seetha kumaraswamy{exeter} end of program page range
					}
					if ((monoPages == "") || (monoPages == "0-0")) {
						monoPages = " ";
					}
					fPage = parseInt(fPage);
					lPage = parseInt(lPage);
					contentData[data]._attributes["fpage_runon"] = fPage;
					contentData[data]._attributes["lpage_runon"] = lPage;
					//seetha kumaraswamy{exeter} to check for the advert coloured pages
					if ((typeof (contentData[data]["color"]) == "object") && ((contentData[data]._attributes["type"]) == "advert")) {
						if (typeof (contentData[data].color["_attributes"]) == "object") {
							if (contentData[data].color["_attributes"]["type"] == "page") {
								monoPages = " ";
								colorPg = "Colour";
							}
							else
							monoPages="mono";
						}
					}
					//seetha kumaraswamy{exeter}for filling up the color pages
					colorPg=colorfigpg;
					contentData[data]._attributes["Pages"] = (lPage - fPage) + 1;
					if (contentData[data]._attributes["type"] == "table-of-contents") { //toc page count are handled here
						contentData[data]._attributes["Pages"] = parseInt(contentData[data]._attributes["pageExtent"]);
					}
					if (typeof (contentData[data]._attributes["data-run-on"]) == "string") { // handling runon article page count
						contentData[data - mergeCount]._attributes["Pages"] = (lPage - contentData[data - mergeCount]._attributes["fpage_runon"]) + 1;
						delete contentData[data]._attributes["Pages"];
						if (typeof (contentData[data - mergeCount]._attributes["mergecell"]) == "number") { // collecting data for cell merge
							contentData[data - mergeCount]._attributes["mergecell"] = contentData[data - mergeCount]._attributes["mergecell"] + 1;
							mergeCount=mergeCount+1;
						}
						else {
							contentData[data - mergeCount]._attributes["mergecell"] = 2;
							mergeCount=mergeCount+1;
						}
					}
					else{
						mergeCount=1;
					}
					//seetha kumaraswamy{exeter}finding color pages for TOC,cover,Editorial Board
					if(contentData[data]._attributes["type"] != "editorial-board"&&contentData[data]._attributes["type"] != "cover"&&contentData[data]._attributes["type"] != "editorial-board"&&fPage1-lPage1==0 && typeof(contentData[data]["color"])=="object")
					{
						//if(contentData[data]["color"]._attributes["color"]=="1")
						{
						monoPages="-";
						colorPg=fPage1;
						colorfigpg=fPage1;
						}
					}
					if (contentData[data]._attributes["type"] == "cover")
					{
						if(coverColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}
					if (contentData[data]._attributes["type"] == "editorial-board")
					{
						if(edBoardColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}
					if (contentData[data]._attributes["type"] == "table-of-contents")
					{
						if(tocColor=="cmyk")
						{
							figColorDetails="";
							colorPg="Colour";
						}
						else
						{
							figColorDetails="";
							monoPages="Mono";
						}
					}

					contentData[data]._attributes["Mono Pages"] = monoPages;
					contentData[data]._attributes["Colour pages"] = colorPg;
					//seetha kumaraswamy{exeter}to makes the condition and not conside with others while filling up the remaining page of color page, color figure details and color figure pages
					if(colorPg=="Colour")
						{
							contentData[data]._attributes["Colour figure details"] = "";
						}
					else
						{
							contentData[data]._attributes["Colour figure details"] =figColorDetails;
						}
					if(colorfigpg=="Colour")
						{
							contentData[data]._attributes["Colour figure pages"] = "";
						}
					else
						{
							contentData[data]._attributes["Colour figure pages"] =colorfigpg;
						}

					if ((contentData[data]._attributes["Pages"] != " ") && (typeof (contentData[data]._attributes["Pages"]) != "undefined")) {
						totalPages = totalPages + contentData[data]._attributes["Pages"];
						//seetha kumarasway{exeter} to slter and fill for your requirement
					}
					info.push(contentData[data]._attributes);
				}
				var totalPageNo = new Object();
				totalPageNo["id"] = " ";
				totalPageNo["fpage"] = " ";
				totalPageNo["lpage"] = "Total";
				totalPageNo["Pages"] = totalPages;
				totalPageNo["Mono Pages"] = " ";
				totalPageNo["Colour pages"] = " ";
				totalPageNo["Colour figure details"] = " ";
				totalPageNo["Colour figure pages"] = " ";
				info.push(totalPageNo);
				$(currentTab +'#manuscriptsData').data('binder')['container-xml']['meta']["totalPageNo"] = totalPages;
				// json converted to html
				$("#dvjson").excelexportjs({
					containerid: "dvjson"
					, datatype: 'json'
					, dataset: info
					, columns: getColumns(info)
				});
			},
			exportModified: function (param, index) {
				return false;
				index = (index) ? index : 0;
				var thisObj = this;
				//var condition, if param.type is draft, condition is [data-type="manuscript"], else condition is [data-modofied="true"]
				var condition;
				if ((param && param.type && param.type == 'draft') || (param && param.exportType && param.exportType == 'proofAllArticles')) {
					condition = ' [data-type="manuscript"][data-siteName!="Non-Kriya"]'
				}
				else {
					condition = ' [data-modified="true"]'
				}
				var cid = $('#tocContainer .bodyDiv ' + condition + ':eq(' + index + ')').attr('data-c-id');
				var uid = $('#tocContainer .bodyDiv ' + condition + ':eq(' + index + ')').attr('data-uuid');

				var cid = $(currentTab + '#manuscriptsData #tocContentDiv ' + condition + ':eq(' + index + ')').attr('data-c-id');
				var uid = $(currentTab + '#manuscriptsData #tocContentDiv ' + condition + ':eq(' + index + ')').attr('data-uuid');

				var articleObj = $(currentTab + '[data-c-id="' + cid + '"]').data('data');

				var exportParam = {
					'draftFlag': 'false',
					'cid': cid,
					'uid': uid,
					'exportAllArticles': 'true',
					success: function () {
						$(currentTab + '#manuscriptsData #tocContentDiv ' + condition + ':eq(' + index + ')').removeAttr('data-modified', 'true');
						delete articleObj._attributes['data-modified'];
						eventHandler.flatplan.action.saveData();

						if ($(currentTab + '#manuscriptsData #tocContentDiv ' + condition + ':eq(' + (index + 1) + ')').length > 0) {
							thisObj.exportModified(param, index + 1);
						} else if ($(currentTab + '#manuscriptsData #tocContentDiv ' + condition + '').length == index + 1) {
							if (param && param.success && typeof (param.success) == "function") {
								param.success();
							}
						}
					},
					error: function () {
						if ($(currentTab + '#manuscriptsData #tocContentDiv ' + condition + ':eq(' + (index + 1) + ')').length > 0) {
							thisObj.exportModified(param, index + 1);
						} else if ($(currentTab + '#manuscriptsData #tocContentDiv ' + condition + '').length == index + 1) {
							if (param && param.success && typeof (param.success) == "function") {
								param.success();
							}
						}
					}
				}
				thisObj.exportToPDF(exportParam);
			},
			exportAllArticles:function(param){
				param.proofType = 'print_online';
				param.customer = $(currentTab).find('[id="customer"] .filter-list.active').attr('value');
				param.project = $(currentTab).find('[id="project"] .filter-list.active').attr('value');
				param.fileName = $(currentTab + '#manuscriptsDataContent .newStatsRow').attr('data-file');
				param.projectID = $('#projectVal').attr('data-project-id')?$('#projectVal').attr('data-project-id'):'';
				param.userName = userData.kuser.kuname.first;
				param.windowProtocol = window.location.protocol;
				var meta = $(currentTab + ' #manuscriptsData').data('binder')['container-xml'].meta;
				param.volume = meta["volume"]['_text'] ? meta["volume"]['_text'] : parseInt(0);
				param.issue = meta["issue"]['_text'] ? meta["issue"]['_text'] : parseInt(0);
				var notifyObj = new PNotify({
					title: 'Exporting All Manuscripts',
					text: 'Processing request..',
					hide: false,
					type: 'success',
					buttons: { sticker: false },
					icon: false
				});
				var notifyText = $(notifyObj.text_container[0]);
				$.ajax({
					type: 'POST',
					url: "/api/exportAllArticles",
					data: param,
					crossDomain: true,
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.addEventListener("progress", function (evt) {
							var resText = evt.target.responseText;
							if (resText) {
								resText = resText.split(/\n/).pop();
								if (!resText.match(/All manuscripts proofed/)) {
									notifyText.html(resText);
								} else {
									notifyText.html('PDF generated successfully for all Manuscripts.');
								}
							}
						}, false);
						return xhr;
					},
					success: function (data) {
						console.log('All articles proofed')
						$('#projectIssueList .active').trigger('click');
					},
					error: function (error) {
						notifyText.innerHTML = 'Failed..';
						if (param && param.error && typeof (param.error) == "function") {
							param.error();
						}
					}
				});
			},
			proofProbeValidation: function (param, targetNode, dIndex,doiArray,errArr,onSuccess, onError) {
				//skip probeValidation for peripherals and preflight, for non-kriya articles
				//for kriya1 article post to kriya1 peripherals api if available
				param.doi = doiArray[dIndex];
				if ((param && param.processType && param.processType != 'InDesignSetter') || (param && param.contentType && param.contentType != 'manuscript')) {
					if (onSuccess && typeof (onSuccess) == "function") {
						onSuccess(true);
						return true;
					}
				}
				let currentNode;
				if ($(currentTab + '#manuscriptsDataContent [data-section]').find('[data-doi="' + param.doi + '"]').length > 0) {
					currentNode = $(currentTab + '#manuscriptsDataContent [data-section]').find('[data-doi="' + param.doi + '"]');
					let siteName = 'v2.0';
					if(currentNode){
						siteName = currentNode.attr('data-kriya-version');
						let currentData = currentNode.data('data');
						if(currentData && currentData._attributes){
							let cmsID = currentData._attributes.cmsID? currentData._attributes.cmsID:'';
							if(cmsID!=''){
								param.cmsID = cmsID;
								param.siteName = 'v1.0';
							}
							//skip for non-kriya
							if (siteName=='Non-Kriya' && onSuccess && typeof (onSuccess) == "function") {
								onSuccess(true);
								return true;
							}
						}
					}
					param.siteName = siteName;
					if (param.doi && $('[data-c-id][type="manuscript"][data-runon-with="' + param.doi + '"]').length > 0) {
						let subArticles = $('[data-c-id][type="manuscript"][id!=""][data-runon-with="' + param.doi + '"]');
						subArticles.each(function () {
							var currNode = $(this).data('data');
							if (currNode) {
								var subArtdoi = currNode._attributes.id;
								//push only v1 and v2 articles, skip non-kriya articles
								if(currNode._attributes && currNode._attributes.siteName && currNode._attributes.siteName!='Non-Kriya'){
										doiArray.push(subArtdoi);
								}
							}
						});
					}
					let doiLen = doiArray.length;
						eventHandler.common.functions.sendAPIRequest('probevalidation', param, "POST", function (res) {
							if (res.error) {
								errArr.push(param.doi);
							}
							else if (res.response) {
								if (res.response && typeof (res.response) == 'string') {
									let resp = res.response.replace(/<column[\s\S]?\/>/g, '<column></columdn>');
									var resNode = $(resp);
									if (resNode.find('message:not(:empty)').length > 0) {
										errArr.push(param.doi);
									} else {
										//no probe error
									}
								}
							}
							else{
								//no response
								errArr.push(param.doi);
							}
							if ((doiLen-1) == dIndex) {
								if (errArr.length == 0) {
									onSuccess(true);
									return true;
								}
								else {
									if (onError && typeof (onError) == "function") {
										onError(errArr);
										return true;
									}
								}
							}
							//dIndex++;
							setTimeout(function () {
								dIndex++;
								eventHandler.flatplan.export.proofProbeValidation(param, targetNode,dIndex,doiArray,errArr,onSuccess, onError);
							}, 1000);
						});
				}
				else {
					return false;
				}
			}
		},
		merge: {
			mergePDF: function (param, targetNode, checkModify) {
				if (	$(currentTab + '#tocContentDiv .sectionDataChild .pageRange.incorrectPage').length > 0) {
					$('.save-notice').text(eventHandler.flatplan.errorCode[502][5]);
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][5], 'type': 'error','hide':false });
					return false;
				}
				var regionVal;
				//if combine pdf processType is region-based, get the choosen region and include the adverts that belongs to the choosen region
				if(param && param.processType && param.processType=='region-based'){
					regionVal =$('#generateCombinePDF').find('select[id=region_option]').val().toLowerCase();
					$('#generateCombinePDF').modal('hide');
				}
				/*if($('#tocContentDiv [data-modified="true"]').length > 0 && checkModify != 'false'){
					(new PNotify({
	                    title: 'Confirmation Needed',
	                    text: 'Some articles was modified do you want to proof?',
	                    hide: false,
	                    confirm: { confirm: true },
	                    buttons: { closer: false, sticker: false },
	                    history: { history: false },
	                    addclass: 'stack-modal',
	                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                	})).get()
                    .on('pnotify.confirm', function () {
                      eventHandler.flatplan.export.exportModified({
                      	success: function(){
                      		eventHandler.flatplan.merge.mergePDF('', targetNode, 'false');
                      	}
                      });
                    })
                    .on('pnotify.cancel', function () {
                    	eventHandler.flatplan.merge.mergePDF('', targetNode, 'false');
                    });
                    return false;
				}*/

				var pdfLinks = [];
				//$('#tocContainer [data-c-id]').each(function(){
				//epage articles to be excluded while binding
				$(currentTab + '#tocContentDiv [data-c-id][data-grouptype !="epage"]').each(function () {
					var data = $(this).data('data');
					//loop through each file and update pdflink in path if proofType matches
					if (data && data['file']) {
						var fileObj = data['file'];
						if (fileObj.constructor.toString().indexOf("Array") == -1) {
							fileObj = [fileObj];
						}
						fileObj.forEach(function (file) {
							var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
							if (currProofType == 'print') {
								var pdfPath = file['_attributes']['path'];
								if (pdfPath && pdfPath!='') {
									if (!pdfPath.match(/http(s)?:/)) {
										pdfPath = 'http://' + window.location.host + pdfPath;
									} else {
										pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
									}
									pdfLinks.push(pdfPath);
								}
							}
						})
					}
					else if (data && data['region']) {
						var regObj = data['region']
						if (regObj) {
							if (regObj.constructor.toString().indexOf("Array") == -1) {
								regObj = [regObj];
							}
							regObj.forEach(function (region) {
								if ( (region['file'] && (!regionVal || regionVal=='')) || (region._attributes && region._attributes.type && region._attributes.type.toLowerCase() == regionVal && region['file'])) {
									//region['_attributes']['path'] = pdfLink;
									var fileObj = region['file'];
									if (fileObj.constructor.toString().indexOf("Array") == -1) {
										fileObj = [fileObj];
									}
									fileObj.forEach(function (file) {
										var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
										if (currProofType == 'print') {
											var pdfPath = file['_attributes']['path'];
											if (pdfPath && pdfPath!='') {
												if (!pdfPath.match(/http(s)?:/)) {
													pdfPath = 'http://' + window.location.host + pdfPath;
												}
												else {
													pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
												}
												pdfLinks.push(pdfPath);
											}
										}
									});
								}
							});
						}
					}
				});

				var customer = $(currentTab).find('#customer span').text();
				var project = $(currentTab).find('#project span').text();
				var metaObj = $(currentTab+' #manuscriptsData').data('binder')['container-xml'].meta;

				var notifyObj = new PNotify({
					title: 'Combining PDF',
					text: 'Processing request..',
					hide: false,
					type: 'success',
					buttons: { sticker: false },
					icon: false
				});
				var notifyText = $(notifyObj.text_container[0]);

				$.ajax({
					type: "POST",
					url: "/api/mergepdf",
					data: {
						"customer": customer,
						"project": project,
						"volume": metaObj.volume ? metaObj.volume._text : '',
						"issue": metaObj.issue ? metaObj.issue._text : '',
						"pdfLinks": pdfLinks
					},
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.addEventListener("progress", function (evt) {
							var resText = evt.target.responseText;
							if (resText) {
								resText = resText.split(/\n/).pop();
								if (!resText.match(/File_path/)) {
									notifyText.html(resText);
								} else {
									notifyText.html('Completed.');
								}
							}
						}, false);
						return xhr;
					},
					success: function (data) {
						if (data) {
							var filePath = data.split(/\n/).pop();
							if (filePath.match(/File_path/)) {
								filePath = filePath.replace('File_path:', '');
								filePath += '?method=server';
								window.open(filePath, '_blank');
							}
						}
					},
					error: function (xhr, errorType, exception) {
						$('.save-notice').text(eventHandler.flatplan.errorCode[501][5]);
						//showMessage({ 'text': eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','hide':false });
						return null;
					}
				});
			}
		}
	}
})(eventHandler || {});