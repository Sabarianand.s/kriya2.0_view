﻿var dashboardConfig = {
    "editpage":{
        "default":"proof_review",
        "bmj": "review_content"
    },
    "editpagebystage":{
        "kriya":{
            "Associate Editor Check": "peer_review",
            "Editor Check": "peer_review"
        },
        "epos":{
            "Associate Editor Check": "peer_review",
            "Editor Check": "peer_review"
        }
    },
    "removeEdit": {
        "stage": {
            "addJob": "true",
            "Convert Files": "true",
            "File download": "true",
            "Hold": "true"
        },
        "version":{
            'article': {
                "v1.0": "true"
            },
            'book': {
                "v1.0": "true"
            },
            'issue': {
                "v1.0": "false"
            }
        }
    },
    "icons": [
        {
            "icon": " fa-info",
            "hovertext": "Reviewer Status",
            "callFunction": "openReviewerStatus",
            "rules": {
                "condition": {
                    "customers": "epos",
                    "stage": "all",
                    "role": "production|publisher",
                    "accessLevel": "manager|admin|developer|production|support"
                },
                "state": {
                    "true": {
                        "visible": true,
                        "clickable": true
                    },
                    "false": {
                        "visible": false,
                        "clickable": false
                    }
                }
            }
        },
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Final Deliverables",
                "role": "publisher",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Archive",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-refresh",
        "hovertext": "Resupply",
        "function": "resupply",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Banked",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Author Review",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Author Revision",
                "role": "publisher",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-minus-circle",
        "hovertext": "Revoke",
        "function": "revoke",
        "callFunction": "revokeArticle",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Author proof",
                "role": "publisher",
		        "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-play",
        "hovertext": "Unhold",
        "function": "unhold",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Hold",
                "role": "publisher|production|copyeditor",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-pause",
        "hovertext": "Hold",
        "function": "hold",
        "callFunction": "conformationModal",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Hold",
                "role": "publisher|production|copyeditor",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": false,
                    "clickable": false
                },
                "false": {
                    "visible": true,
                    "clickable": true
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "addJob",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "File download",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Convert Files",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Content-Loading",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-undo",
        "hovertext": "Re-try",
        "callFunction": "getArticleStatus",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Support",
                "role": "production",
                "accessLevel": "all"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
        "icon": " fa-times",
        "hovertext": "Withdraw",
        "function": "withdraw",
        "rules": {
            "condition": {
                "customers": "all",
                "stage": "Archive|Banked",
                "role": "publisher|production",
                "accessLevel": "manager|publisher|admin"
            },
            "state": {
                "true": {
                    "visible": true,
                    "clickable": true
                },
                "false": {
                    "visible": false,
                    "clickable": false
                }
            }
        }
    },
    {
            "icon": " fa-upload",
            "hovertext": "Publish this book to local",
            "callFunction": "packageCreation",
            "rules": {
                "condition": {
                    "customers": "atmaglobal",
                    "stage": "all",
                    "role": "publisher|production",
                    "accessLevel": "manager|publisher|admin|developer|production|support"
                },
                "state": {
                    "true": {
                        "visible": true,
                        "clickable": true
                    },
                    "false": {
                        "visible": false,
                        "clickable": false
                    }
                }
            }
        },
        {
            "icon": " fa-code",
            "hovertext": "Probe Validation",
            "callFunction": "probeValidation",
            "rules": {
                "condition": {
                    "customers": "all",
                    "stage": "all",
                    "role": "production",
                    "accessLevel": "manager|admin|developer|production|support"
                },
                "state": {
                    "true": {
                        "visible": true,
                        "clickable": true
                    },
                    "false": {
                        "visible": false,
                        "clickable": false
                    }
                }
            }
        }, {
            "icon": " fa-info-circle",
            "hovertext": "Info",
            "callFunction": "articleInfo",
            "rules": {
                "condition": {
                    "customers": "all",
                    "stage": "all",
                    "role": "production|publisher|copyeditor",
                    "accessLevel": "all"
                },
                "state": {
                    "true": {
                        "visible": true,
                        "clickable": true
                    },
                    "false": {
                        "visible": false,
                        "clickable": false
                    }
                }
            }
        },
    ]
}

var disableFasttrack = {
    "stage": ["Hold"]
}


var popups = {
    "hold": {
        "title": "Put on Hold - {doi}",
        "options": ["On Hold awaiting payment", "On Hold with Editor", "On Hold awaiting linked paper", "On Hold awaiting query response", "On Hold other"],
        "commentbox": true,
        "dropdown": true,
        "function": "eventHandler.components.actionitems.saveHold",

    },
    "unhold": {
        "title": "Release Hold - {doi}",
        "descriptiontext": "Hold",
        "options": ["Payment received", "Editor query acknowleged", "Linked paper processed", "Query responded", "Other"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.saveHold"
    },
    "revoke": {
        "title": "Revoke article - {doi}",
        "descriptiontext": "Hold",
        "options": ["Author declined"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.revokeArticle"
    },
    "resupply": {
        "title": "Resupply for  {doi}",
        "descriptiontext": "Hold",
        "options": ["test", "test2"],
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.resupplyArticle"
    },
    "withdraw":{
        "title": "Withdraw {doi}",
        "descriptiontext": "Withdraw",
        "commentbox": true,
        "dropdown": false,
        "function": "eventHandler.components.actionitems.withdrawArticle"
    }
}
var notifyConfig = {
    "sendRevokeArticle": {
        "notify": {
            "notifyTitle": "Revoke article",
            "successText": "{doi} has been revoked successfully",
            "errorText": "Error while revoke {doi}"
        }
    },
    "saveFastTrack": {
        "notify": {
            "notifyTitle": "Fast Track article",
            "successText": "Flag status for {doi} had been updated",
            "errorText": "Error while update flag status for {doi}"
        }
    },
    "resupplyArticle": {
        "notify": {
            "notifyTitle": "Resupply Article",
            "successText": "Resupply triggered for {doi}",
            "errorText": "Error while trigger resupply for {doi}"
        }
    },
    "saveWFDate": {
        "notify": {
            "notifyTitle": "Update due date",
            "successText": "Due date updated for {doi}",
            "errorText": "Error while update due date for {doi}"
        }
    },
    "addJob": {
        "notify": {
            "notifyTitle": "Add job",
            "successText": "Job added successfully",
            "errorText": "Error while add job"
        }
    },
    "submitJob": {
        "notify": {
            "notifyTitle": "Add job",
            "successText": "Job added successfully",
            "errorText": "Error while add job"
        }
    },
    "assignUser":{
        "notify": {
            "notifyTitle": "Assigning User",
            "successText": "User assigned successfully for {doi}",
            "errorText": "Error while assigning user for {doi}",
            "openedArticle": "{doi} is opened by {name}"
        },
        "bulkUpdate":{
            "notifyTitle": "Assigning User",
            "successText": "User assigned successfully for {doi}",
            "errorText": "Error while assigning user for {doi}",
        }
    },
    "blkUpdate":{
        "notify":{
            "openedArticle": "{doi} is opened by {name}",
            "unAccessible": "You cannot assign user to {doi}"
        }
    },
    "withdrawArticle": {
        "notify": {
            "notifyTitle": "Withdraw Article",
            "successText": "{doi} has been withdrawn",
            "errorText": "Error while withdrawn {doi}"
        }
    },
}
var filterMappingOther={
    "0-1d":{"attr":"data-overdue-type","name":"0-1d","val":"0-1d"},
    "1-2d":{"attr":"data-overdue-type","name":"1-2d","val":"1-2d"},
    "2-3d":{"attr":"data-overdue-type","name":"2-3d","val":"2-3d"},
    "3-5d":{"attr":"data-overdue-type","name":"3-5d","val":"3-5d"},
    "5-10d":{"attr":"data-overdue-type","name":"5-10d","val":"5-10d"},
    ">10d":{"attr":"data-overdue-type","name":">10d","val":">10d"},
    "FastTrack":{"attr":"data-fast-track","name":"FastTrack","val":"true"}
}
var filterMapping = {
    "project-sub": "data-project",
    "customer-sub": "data-customer",
    "articleType-sub": "data-article-type",
    "articleType": "all",
    "customer": "all",
    "project": "all",
    "typesetter": "data-stage-owner",
    "typesetter-sub": "data-stage-name",
    "publisher": "data-stage-owner",
    "publisher-sub": "data-stage-name",
    "copyeditor": "data-stage-owner",
    "copyeditor-sub": "data-stage-name",
    "author": "data-stage-owner",
    "author-sub": "data-stage-name",
    "editor": "data-stage-owner",
    "editor-sub": "data-stage-name",
    "support": "data-stage-owner",
    "support-sub": "data-support-category",
    "supportstage":"data-substage-owner",
    "contentloading-sub": "data-stage-name",
    "supportstage-sub":"data-supportstage-category",
    "supportlevel-sub":"data-supportstage-level",
    "others": "data-others",
    "default": "data-stage-name",
    "others-sub": filterMappingOther,
}
var sortmapping = {
    'MSID': {
        'sortAttr': 'sort-msid',
        'highLightAttr': '.msDOI'
    },
    'Doi': {
        'sortAttr': 'sort-doi',
        'highLightAttr': '.msPubID'
    },
    'AcceptDate' : {
        'sortAttr' : 'accept-date',
        'highLightAttr': '.msAccDate'
    },
    'Article' : {
        'sortAttr' : 'article-type',
        'highLightAttr': '.msType'
    },
    'Customer' : {
        'sortAttr' : 'customer',
        'highLightAttr': '.msCustomer'
    },
    'DaysInProd' : {
        'sortAttr' : 'days-in-prod',
        'highLightAttr': '.msDaysInProd'
    },
    'DueDate' : {
        'sortAttr' : 'duedate',
        'highLightAttr': '.msStageDue'
    },
    'Owner' : {
        'sortAttr' : 'owner',
        'highLightAttr': '.msStageOwner'
    },
    'Stage': {
        'sortAttr' : 'stage-name',
        'highLightAttr': '.msStageName'
    },
    'sortType': {
        'string': ['sort-msid', 'sort-doi', 'accept-date', 'article-type', 'customer', 'duedate', 'owner', 'stage-name'],
        'number': ['days-in-prod']
    }
}


var chartTableReport = {
    "default": {
        "columns": [{
            "columnName": "Article",
            "attribute": {
                "data-filter": "DOI",
                "col-size": "15",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Page Count",
            "attribute": {
                "data-filter": "ProofPageCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count",
            "attribute": {
                "data-filter": "WordCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count Without Ref",
            "attribute": {
                "data-filter": "WordCountWithoutRef",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Prod. Days",
            "attribute": {
                "data-filter": "DaysInProd",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Current Stage",
            "attribute": {
                "data-filter": "Stage Name",
                "col-size": "9",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Type",
            "attribute": {
                "data-filter": "Manuscript Type",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Start Date",
            "attribute": {
                "data-filter": "startDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "End Date",
            "attribute": {
                "data-filter": "endDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla Start Date",
            "attribute": {
                "data-filter": "slaStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla End Date",
            "attribute": {
                "data-filter": "slaEndDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned Start Date",
            "attribute": {
                "data-filter": "plannedStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned End Date",
            "attribute": {
                "data-filter": "Stage Due",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Accepted Date",
            "attribute": {
                "data-filter": "AcceptedDate",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Exported Date",
            "attribute": {
                "data-filter": "ExportedDate",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Invoice ID",
            "attribute": {
                "data-filter": "InvoiceDetails",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },{
            "columnName": "License Type",
            "attribute": {
                "data-filter": "License",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Corresponding Author",
            "attribute": {
                "data-filter": "CorrespondingAuthor",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },{
            "columnName": "Corresponding Author Email-ID",
            "attribute": {
                "data-filter": "CorrespondingAuthorEmail",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Assigned To",
            "attribute": {
                "data-filter": "assignUser",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage History",
            "attribute": {
                "data-filter": "reportHistory",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }]
    },
    "movementChart": {
        "columns": [{
            "columnName": "Article",
            "attribute": {
                "data-filter": "DOI",
                "col-size": "15",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Page Count",
            "attribute": {
                "data-filter": "ProofPageCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count",
            "attribute": {
                "data-filter": "WordCount",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Word Count Without Ref",
            "attribute": {
                "data-filter": "WordCountWithoutRef",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Prod. Days",
            "attribute": {
                "data-filter": "DaysInProd",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Chart Stage",
            "attribute": {
                "data-filter": "Stage Name",
                "col-size": "9",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Type",
            "attribute": {
                "data-filter": "Manuscript Type",
                "col-size": "7",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },
        {
            "columnName": "Start Date",
            "attribute": {
                "data-filter": "startDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "End Date",
            "attribute": {
                "data-filter": "endDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla Start Date",
            "attribute": {
                "data-filter": "slaStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Sla End Date",
            "attribute": {
                "data-filter": "slaEndDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned Start Date",
            "attribute": {
                "data-filter": "plannedStartDate",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Planned End Date",
            "attribute": {
                "data-filter": "Stage Due",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage Duration(In Days)",
            "attribute": {
                "data-filter": "stageDays",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Delay(In Days)",
            "attribute": {
                "data-filter": "delayDays",
                "col-size": "12",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Invoice ID",
            "attribute": {
                "data-filter": "InvoiceDetails",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },{
            "columnName": "License Type",
            "attribute": {
                "data-filter": "License",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Corresponding Author",
            "attribute": {
                "data-filter": "CorrespondingAuthor",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        },{
            "columnName": "Corresponding Author Email-ID",
            "attribute": {
                "data-filter": "CorrespondingAuthorEmail",
                "col-size": "5",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Assigned To",
            "attribute": {
                "data-filter": "assignUser",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }, {
            "columnName": "Stage History",
            "attribute": {
                "data-filter": "reportHistory",
                "col-size": "8",
                "onclick": "eventHandler.bulkedit.export.managefilter('sort')"
            }
        }]
    }
}

var ShowHideCol = {
    'default': {
        'ProofPageCount': { 'name': 'Page Count', 'index': '3', 'defaultShow': true },
        'WordCount': { 'name': 'Word Count', 'index': '4', 'defaultShow': true },
        'WordCountWithoutRef': { 'name': 'Word Count Without Ref', 'index': '5', 'defaultShow': true },
        'Prodn.Days': { 'name': 'Prodn. Days', 'index': '6', 'defaultShow': true },
        'CurrentStage': { 'name': 'Current Stage', 'index': '7', 'defaultShow': true },
        'Type': { 'name': 'Type', 'index': '8', 'defaultShow': true },
        'StartDate': { 'name': 'Start Date', 'index': '9', 'defaultShow': true },
        'EndDate': { 'name': 'End Date', 'index': '10', 'defaultShow': true },
        'SlaStartDate': { 'name': 'Sla-Start Date', 'index': '11', 'defaultShow': true },
        'SlaEndDate': { 'name': 'Sla-End Date', 'index': '12', 'defaultShow': true },
        'PlannedStartDate': { 'name': 'Planned-Start Date', 'index': '13', 'defaultShow': true },
        'PlannedEndDate': { 'name': 'Planned -End Date', 'index': '14', 'defaultShow': true },
        'AcceptedDate': { 'name': 'Accepted Date', 'index': '15', 'defaultShow': false },
        'ExportedDate': { 'name': 'Exported Date', 'index': '16', 'defaultShow': false },
        'InvoiceDetails': { 'name': 'Invoice ID', 'index': '17', 'defaultShow': false },
        'license': { 'name': 'License', 'index': '18', 'defaultShow': false },
        'CorrespondingAuthor': { 'name': 'Corresponding Author', 'index': '19', 'defaultShow': false },
        'CorrespondingAuthorEmail': { 'name': 'Corresponding Author Email', 'index': '20', 'defaultShow': false },
        'AssignedTo': { 'name': 'Assigned To', 'index': '21', 'defaultShow': true }
    },
    'movementChart':{
        'ProofPageCount': { 'name': 'Page Count', 'index': '3', 'defaultShow': true },
        'WordCount': { 'name': 'Word Count', 'index': '4', 'defaultShow': true },
        'WordCountWithoutRef': { 'name': 'Word Count Without Ref', 'index': '5', 'defaultShow': true },
        'Prodn.Days': { 'name': 'Prodn. Days', 'index': '6', 'defaultShow': true },
        'ChartStage': { 'name': 'Chart Stage', 'index': '7', 'defaultShow': true },
        'Type': { 'name': 'Type', 'index': '8', 'defaultShow': true },
        'StartDate': { 'name': 'Start Date', 'index': '9', 'defaultShow': true },
        'EndDate': { 'name': 'End Date', 'index': '10', 'defaultShow': true },
        'SlaStartDate': { 'name': 'Sla-Start Date', 'index': '11', 'defaultShow': true },
        'SlaEndDate': { 'name': 'Sla-End Date', 'index': '12', 'defaultShow': true },
        'PlannedStartDate': { 'name': 'Planned-Start Date', 'index': '13', 'defaultShow': true },
        'PlannedEndDate': { 'name': 'Planned -End Date', 'index': '14', 'defaultShow': true },
        'StageDiff': { 'name': 'Stage Duration(In Days)', 'index': '15', 'defaultShow': true },
        'DelayDiff': { 'name': 'Delay(In Days)', 'index': '16', 'defaultShow': true },
        'InvoiceDetails': { 'name': 'Invoice ID', 'index': '17', 'defaultShow': false },
        'License': { 'name': 'License Type', 'index': '18', 'defaultShow': false },
        'CorrespondingAuthor': { 'name': 'Corresponding Author', 'index': '19', 'defaultShow': false },
        'CorrespondingAuthorEmail': { 'name': 'Corresponding Author Email', 'index': '20', 'defaultShow': false },
        'AssignedTo': { 'name': 'Assigned To', 'index': '21', 'defaultShow': true }
    }
}

var tableReportFilterConfig = {
    "default":{
        "rowtype":['none', 'caseinsensitivestring', 'number', 'number','number',
        'number', 'caseinsensitivestring', 'caseinsensitivestring',
        {
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy}'
        },
        {
            type: 'date',
            locale: 'en',
            format: '{yyyy}-{MM}-{dd}'
        },'caseinsensitivestring',
        "none", "none"],
        "filter":['col_0', 'col_16', 'col_17']
    },
    "movementChart":{
        "rowtype":['none', 'caseinsensitivestring', 'number', 'number', 'number',
        'number', 'caseinsensitivestring', 'caseinsensitivestring',
        {
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },{
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },{
            type: 'date',
            locale: 'en',
            format: ['{MM} {dd},{yyyy} {hh}:{mm}:{ss}']
        },
        {
            type: 'date',
            locale: 'en',
            format: '{MM} {dd},{yyyy} {hh}:{mm}:{ss}'
        },'number','number', 'caseinsensitivestring','caseinsensitivestring'],
        "filter":['col_0', 'col_17']
    }
}


var refreshButtonConfig = {
    "WIP": {
        "workflowStatus": "in-progress OR completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        "excludeStage": "inCardView",
        'version': 'v2.0'
    },
    "Withdrawn": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Withdrawn',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "Archive": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Archive',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "Banked": {
        "workflowStatus": "completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        'stage': 'Banked',
        'urlToPost': 'getStage',
        'version': 'v2.0'
    },
    "All": {
        "workflowStatus": "in-progress OR completed",
        "customer": "selected",
        "project": "project",
        "custTag": "#filterCustomer",
        "prjTag": "#filterProject",
        "excludeStage": "inCardView",
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Assigned to me": {
        'workflowStatus':'in-progress',
        'urlToPost':'getAssigned',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'user':'user',
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Unassigned": {
        'workflowStatus': 'in-progress' ,
        'urlToPost':'getUnassigned',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'user':'user',
        'userLevelStageAccess':true,
        'optionforassigning':true,
        'version': 'v2.0'
    },
    "Other": {
        'workflowStatus': 'in-progress',
        'urlToPost':'getOtherarticles',
        'customer':'selected',
        'custTag': '#filterCustomer',
        'prjTag':'#filterProject',
        'userLevelStageAccess':true,
        'optionforassigning':true,
        'version': 'v2.0'
    }
}
var userAuthorization = {
    'publisher':{
        'production': {
            'toShow': [],
            'toHide': []
        },
        'manager': {
            'toShow': [],
            'toHide': []
        },
    },
    'production': {
        'production': {
            'toShow': [],
            'toHide': ['#manuscriptContent #allCards']
        },
        'manager': {
            'toShow': ['#manuscriptContent #allCards'],
            'toHide': []
        },
        'support': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'admin': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'developer': {
            'toShow': ['#manuscriptContent #allCards,.othersCard'],
            'toHide': []
        },
        'reviewer': {
            'toShow': [],
            'toHide': ['#manuscriptContent #allCards,.othersCard', '#manuscriptContent .assignAccess #filterStatus div:gt(0)']
        },
        'associateeditor': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard,#manuscriptContent #filterStatus .assignAccess:nth-child(2),.app-header #showmsChart,.app-header #reportView,.app-header #issueView,.app-header #search,#manuscriptContent .layoutIcons #sort']
        }
    },
    'copyeditor': {
        'production': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard,.app-header #issueView']
        },
        'manager': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard,.app-header #issueView']
        },
        'vendor': {
            'toShow': [],
            'toHide': ['#manuscriptContent .othersCard,.app-header #showmsChart,.app-header #reportView,.app-header #search,.app-header #issueView,#manuscriptContent .layoutIcons #sort']
        }
    }
}

var excludeStage = {
    'inCardView': ['Archive', 'Banked', 'Withdrawn']
}
var addJobConfig = {
    'customer': {
		'thebmj' : 'all',
		'jb' : 'all',
        'bmj' : 'production',
        'elife' : 'production',
        'kriya' : 'production'
    },
    "tabID": "cardView"
}
var addBookConfig = {
    'customer': {
	'cabi' : 'all',
	'scrivener' : 'all',
	'kriya_books' : 'all',
	'atmaglobal': 'all',
	'technica': 'all',
    'stylus': 'all',
    'sage_books':'all',
    'bep':'all',
    'brill':'all',
    'eup':'all'
    },
    "tabID": "cardView"
}
var addIssueConfig = {
    'customer': {
		'bmj' : 'all'
    },
    "tabID": "issueView"
}

/**
 * Customer, role-type and access-level mentioned in the below variable won't display the submitJob button
 */
var submitJobConfig = {
    'epos': {
        'production': ['associateeditor']
    },
    'kriya': {
        'publisher': ['associateeditor']
    }
}

/**
 * Based on role-type and access-level of user.
 */
var userAssignAccessRoleStages = {
    'author': {
        'author': ['Author Review', 'Author Revision']
    },
    'production':{
        'admin':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'developer':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'production':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'support':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'manager':['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'reviewer': ['Pre-editing', 'Typesetter Check', 'Typesetter Review', 'Validation Check', 'Final Deliverables', 'Copyediting Check', 'Waiting for CE', 'Support'],
        'associateeditor': ['Associate Editor Check']
    },
    'publisher':{
        'publisher':['Publisher Check', 'Publisher Review'],
        'manager':['Publisher Check', 'Publisher Review','Editor Check','Editor Review']
    },
    'copyeditor': {
        'production': ['Copyediting'],
        'vendor': ['Copyediting'],
        'manager': ['Waiting for CE', 'Copyediting', 'Copyediting QC', 'Copyediting Check']
    }
}

// To show only particular users for assigning the articles for particular stage based on user role-type and access-level
var showAssignUserName = {
    'copyeditor': {
        'manager': {
            'Waiting for CE':{
                'roleType': ['copyeditor'],
                'accessLevel': ['manager']
            },
            'Copyediting': {
                'roleType': ['copyeditor'],
                'accessLevel': ['vendor']
            },
            'Copyediting QC': {
                'roleType': ['copyeditor'],
                'accessLevel': ['manager']
            },
            'Copyediting Check':{
                'roleType': ['copyeditor'],
                'accessLevel': ['manager']
            }
        }
    }
}

var xpath = {
    'assignUser': 'data(//workflow/stage[last()]/job-logs/log[last()]/status[text()="open"]/../username)',
    'blkUpdate': 'data(//workflow/stage[last()]/job-logs/log[last()]/status[text()="open"]/../username)'
}

var articleLoopCountInTableReport = 500;

var hideRepeatedStage = {
    "default":['addJob', 'File download', 'Convert Files', 'Support', 'Hold']
}

var cardViewFilterSeperator = {
    "support": {
        "SecondaryFilterNames": ["supportstage", "supportlevel"]
    },
    "typesetter": {
        "SecondaryFilterNames": ["contentloading"]
    }
}

var checkPageCountWhileAssigning = {
    'copyeditor': {
        'access': ['manager', 'production', 'vendor'],
        'stage': ['Copyediting']
    }
}

var packageCreationParameter = {
    'atmaglobal': {
        "client": "atmaglobal",
        "id": "project",
        "doi": "id",
        "jrnlName": "project",
        "processType": "PackageCreator",
        "type": "atmaglobal-epub",
        "issueFileName": "project",
        "uploadTo": "localftp"
    }
}

var forceLogOutFunction = {
    "assignUser": "eventHandler.components.actionitems.assignUser",
    "updateXpath": "eventHandler.components.actionitems.assignUserForceLogOff"
}

// To show Support Tab in Dashboard
var showSupportTab = {
    'production': {
        'access': ['production', 'manager', 'support', 'developer', 'admin']
    }
}
var showAssignmentTab = {
    'production': {
        'access': ['manager', 'admin']
    }
}

var uploadXMLButton = {
    'production':{
        'access':['admin','developer','support']
    }
}

var articleInfoDetails = [
    {
        'name': 'MS ID',
        'attribute': 'doi'
    },
    {
        'name': 'DOI',
        'attribute': 'pubID'
    },
    {
        'name': 'Article Type',
        'attribute': 'articleType'
    },
    {
        'name': 'Article Title',
        'attribute': 'title'
    },
    {
        'name': 'Authors',
        'attribute': 'authors'
    },
    {
        'name': 'Customer',
        'attribute': 'customer'
    },
    {
        'name': 'Project',
        'attribute': 'project'
    },
    {
        'name': 'Kriya Version',
        'attribute': 'kriya-version'
    },
    {
        'name': 'CE Level',
        'attribute': 'CELevel'
    },
    {
        'name': 'Accepted Date',
        'attribute': 'acceptedDate'
    },
    {
        'name': 'Word Count',
        'attribute': 'word-count'
    },
    {
        'name': 'Word Count Without Ref',
        'attribute': 'word-count-without-ref'
    },
    {
        'name': 'Fig Count',
        'attribute': 'fig-count'
    },
    {
        'name': 'Table Count',
        'attribute': 'table-count'
    },
    {
        'name': 'Equation Count',
        'attribute': 'equation-count'
    },
    {
        'name': 'Ref Count',
        'attribute': 'ref-count'
    },
    {
        'name': 'Page Count',
        'attribute': 'page-count'
    },
    {
        'name': 'Proof Count',
        'attribute': 'proof-count'
    },
    {
        'name': 'Volume',
        'attribute': 'volume'
    },
    {
        'name': 'Issue',
        'attribute': 'issue'
    }
]

// Role and access level inside the variable sortAuthorization will only display sort option
var sortAuthorization = {
    'author': ['author'],
    'production': ['admin', 'developer', 'production', 'support', 'manager', 'reviewer'],
    'publisher': ['publisher', 'manager'],
    'copyeditor': ['production', 'manager']
}

//To disable cards except first card in unassigned bucket for CE -> Vendors
var cardDisable = {
    'copyeditor': {
        'vendor': {
            'addClassName': 'copyeditordisable'
        }
    }
}

var packageForBook = ['atmaglobal', 'kriya_books', 'brill']

var resourceTab = {
    'copyeditor': {
        'vendor' : ['inputs']
    }
}
