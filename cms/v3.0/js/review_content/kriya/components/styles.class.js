/**
 * @param        {Function} $ jQuery v1.9.1
 * @param        {Object} kriya
 */
(function (kriya) {

    // 1. CONFIGURATION
    var config = {
        //container: '[data-component="InputBox"]',
		//container: '.jrnlHead1',
		container: '.InputBox',
		editable: true
    };

    // 2. COMPONENT OBJECT
    kriya.styles = {

        init: function (elm, componentName, evt) {
			this.name = componentName;
			delete this.container;
            this.getComponents(elm);
            if (this.container != undefined && this.container.length > 0){
                this.bindEvents();
            }
        },
		
		detach: function () {
			
		},

        getComponents: function (elm) {
			if (typeof(elm) === 'undefined') return;
			var compList = kriya.componentList;
			if (typeof(compList[this.name]) === 'undefined') return;
            this.container = elm;
            this.component = compList[this.name];
            this.component.name = this.name;
        },

        bindEvents: function () {
			if (kriya.isUndefined(kriya[this.component.type])){
			   kriya[this.component.type].init(this.container, this.component);
		   }else if(kriya.isUndefined(kriya.general[this.component.type])){
		   		kriya.general[this.component.type](this.container, this.component);
		   }
			/*if(config.editable){
				this.tinyEditor(this.container);
			}*/
        },

		/* Rich text editor */
		tinyEditor: function(elm){
			$(function() {
			$(elm).attr('contenteditable', 'true');
				/*tinymce.init({
					selector: '.WordSection1',
					inline: true,
					plugins: ['paste, charmap'],
					menubar: false,
					toolbar: 'undo redo | bold italic | superscript subscript underline | charmap ice'
				});*/
				/*var tracker = new ice.InlineChangeEditor({
				   element: document.getElementById('contentDivNode'),
				   handleEvents: true,
				   currentUser: { id: 1, name: 'Miss T' }
				 });
				 tracker.startTracking();*/
			   /*var tracker = new ice.InlineChangeEditor({
				   element: elm,
				   handleEvents: true,
				   currentUser: { id: 1, name: 'Miss T' },
				   // optional plugins
				   plugins: [
					 // Add title attributes to changes for hover info
					 'IceAddTitlePlugin',
					 // Two successively typed dashes get converted into an em-dash
					 'IceEmdashPlugin',
					 // Track content that is cut and pasted
					 {
					   name: 'IceCopyPastePlugin',
					   settings: {
						 // List of tags and attributes to preserve when cleaning a paste
						 pasteType : 'formattedClean',
						 preserve: 'p,a[href],span[id,class]em,strong'
					   }
					 }
				   ]
				}).startTracking();*/
			});
		}
    };

    // 3. GLOBALIZE NAMESPACE
    return kriya;

}(window.kriya || {}));