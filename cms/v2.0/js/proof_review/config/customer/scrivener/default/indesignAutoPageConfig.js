var config = {
    "defaultUnits":"pt",
    "baseLeading":11.5,
    "preferredPlaceColumn":0,
    "floatLandscape":false,
    "floatOnFirstPageForDefaultLayout":true,
    "articleTypeNotFloatsOnFirstPage":["undefined","DEFAULT"],
    "floatTypeOnFirstPage":"KEY",    
    "minStackSpace":18,
    "placeStyle":"Sandwich",
    "minNoLinesOnPag":3,
    "applyTableLeftRightBorder":false,   
     "pageSize":{
             "width":595.276,
            "height":779.528
        },
    "breakingSectionHeads":{
        "jrnlRefHead":{
            "startAt":"frame",
            "styleoverrides":{
                "startParagraph":1313235563
                
                }
            }
        },
    "exportPDFpreset":{
        "print":{
            "crop_marks":true, 
            "bleed_marks":true, 
            "registration_marks":true, 
            "colour_bars":false, 
            "page_information":false,
            "offset":18.5
            },
        "online":{
            "crop_marks":false, 
            "bleed_marks":false, 
            "registration_marks":false, 
            "colour_bars":false, 
            "page_information":false,
            "offset":0
            }
        },
    "wrapAroundFloat":{
      "top":18,
      "bottom":18,
      "left":12,
      "right":12,
      "gutter":14.173
    },
"geoBoundsVerso":[62.3,45.3,713.21,549.975590551
    ],
    "geoBoundsRecto":[62.3,640.575590551,713.21,1145.251590551
    ],
    "articleTypeDetails":{
    "undefined":"LAYOUT1"
    },    
    
    "pageColumnDetails":{
        "LAYOUT1":{        
            "openerPageColumnDetails":[
                  {"width":374.172 , "height":587,"floatsCited":false,"gutter":0}
                 ],
                "columnDetails":[
                  {"width":245.251 , "height":650.91,"floatsCited":false,"gutter":0},
                  {"width":245.251 , "height":650.91,"floatsCited":false,"gutter":14.173}
                ],
                "openerPageMargin":{
                 "top":126.5,
                "bottom":66.318,
                "inside":175.75,
                "outside":45.3
                },
            "otherPageMargin":{
                 "top":62.3,
                "bottom":66.318,
                "inside":45.3,
                "outside":45.3
                }
            }
        },
    "jrnlBoxBlock":{//these objects are for overriding actual floats placement 
        "KEY":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":1,
            "preferredPlacement":'top'
        },
        "KEY_BACK":{
            "calcCitationHeight":false,
            "preferredOnCurrentPage":true,
            "preferredOnColumn":0,
            "preferredPlacement":null
        }
    },
    "landscape":{
      "singleColumnStyle":true,//if this is true then the landscape float could be placed in landscape single column
      "twoThirdColumnStyle":false,//if this is true then the landscape float could be placed in landscape two third column
      "twoThirdWidth":340.85, 
      "horizontalCenter":true//if this is true float could be center horizontally after rotated
    },
    "resizeImage":{
      "allow":false,
      "modifyLimit":0
    },
    "figCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
    "tblCaption":{
      "position":"bottom",
      "sideCaption":{
        "figMinWidth":0,
        "figMaxWidth":0,
        "preferredPlace":"top"
      }
    },
"continuedStyle":{
    "table":{
      "footer": {
        "continuedText": "(<i>Continued</i>)",
        "continuedTextStyle": "TBL_Cont",
        "tableBottomDefaultGap": 6
      },
      "header": {
//~         "continuedText":"Table [ID]. [CAPTION][ensp](<i>Continued</i>)",
        "continuedText":"Table [ID].[emsp](<i>Continued</i>)",
        "space-below": 0,
        "repeat-header": true,
        "repeat-sub-header": false,
        "tableLastRowStyle": "TBLR",
        "tableHeadRowStyle": "TCH",
        "tableContinuedStyle": "TBL_ContHead",
        "tableLabelStyle": "tblLabel"
        }
    }
      },
    "adjustParagraphSpaceStyleList":{
        "increase":{
            "before":"jrnlHead1,jrnlHead1_First,jrnlHead2,jrnlHead3,jrnlRefHead,jrnlFundingHead,jrnlEqualContribFNHead,BL-T,NL-T,BL-O,NL-O,EQN-O,EQN-T,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,EQN-O,EQN-B,QUOTE-B"
         },
        "decrease":{
            "before":"jrnlHead1_First,jrnlHead1,jrnlHead2,jrnlRefHead,BL-T,NL-T,BL-O,NL-O,EQN-O,EQN-T,QUOTE-T",
            "after":"BL-B,NL-B,BL-O,NL-O,QUOTE-O,EQN-O,EQN-B,QUOTE-B"
         },
        "limitationPercentage":{
            "minimum":40,
            "maximum":60
            }
    },
    "detailsForVJ":{
        "VJallowed":true,
        "stylesExcludedForVJ":"jrnlAuthors,jnrlArtTitle,jrnlArtType,jrnlAbsHead,jrnlHead1,jrnlHead2,jrnlHead3,jrnlHead4,jrnlHead5",
        },
    "adjustFloatsSpace":{
        "limitationPercentage":{
            "minimum":40,
            "maximum":80
            }        
    },
    "trackingLimit":{
        "minimum":15,
        "maximum":15
    },
    "nonDefaultLayers":["PRIM_SUR"],
    
    "assignUsingXpath": {
       "toFrames":[
         {"xpath" : "//div[@class='jrnlStubBlock']", "frame-name":"STUB_COLUMN", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlSTRH']", "frame-name":"jrnlSTRH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlARH']", "frame-name":"jrnlARH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlJVH']", "frame-name":"jrnlJVH", "action":"move", "styleOverride":null},
         {"xpath" : "//p[@pstyle='jrnlJT']", "frame-name":"jrnlJT", "action":"move", "styleOverride":null},
         {"xpath" : "//div[@class='jrnlDefBlock']", "frame-name":"ABBREVATION", "action":"move", "styleOverride":null}
         ]       
     },
    "colorDetails":{
        "undefined":{
            "print":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,70"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,70"
                    }
                },
            "online":{
                "COLOR1":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,70"
                    },
                "COLOR2":{
                    "colourType":"process",
                    "colourMode":"CMYK",
                    "colourValue":"0,0,0,70"
                    }
                }            
            }
        },
         "tableSetter" :{
            "table" : {
                "css" : {
                    "font-size" : "7px",
                    "font-family" : "'Helvetica Neue LT Std', serif"
                },
                "fontPath"  : "HelveticaNeueLTStd/HelveticaNeueLTStd.otf"
            },
            "thead": {
                "css" : {
                    "font-family" : "'HelveticaNeueLTStd-67BoldCondensed', serif",
                    "font-weight"    : "normal",
                    "line-height"    : "normal",
                    "padding" : "0px 3px 0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            },
            "tbody":{
                "css" : {
                    "font-weight"    : "normal",
                    "line-height"    : "normal",
                    "padding" : "0px 3px 0px 0px",
                },
                "align" : "left",
                "valign": "bottom"
            }
        },
        "equation" : {
          "default" : {
              "page":{
                  "size" : "504.676pt"
              },
              "font" : {
                  "size"    : "9.5pt",
                  "ledding" : "11.5pt",
                  "path"    : "/MinionPro/",
                  "ext"     : "otf",
                  "bold"    : "MinionProBold",
                  "italic"  : "MinionProItalic",
                  "bold_italic":"MinionProBoldItalic",
                  "main"    : "MinionProRegular"
              }
           }
    },
    "relinkFigures":['LOGO_1.eps'],
    "stubColObj":{
        "top": {
            "STUB_COLUMN": false,
            "STUB_STMT": false
        },
        "bottom": {
            "METAINFO": true,
            "CROSSMARK": false,
            "RELATED_ARTICLE_INFO": true
        }
    }
}
this.config = config;