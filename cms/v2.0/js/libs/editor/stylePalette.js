var stylePalette = function() {
	//default settings
	var settings = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (stylePalette) {
	settings = stylePalette.settings;
	
	stylePalette.actions = {
		applyFormat: function (param, targetNode){
			var params = param.split(',')
			param = [];
			param.name = params[0];
			param.class = params[1];
			param.type = params[2];
			param.attrName = params[3];
			param.attrValue = params[4];
			//these two values added for creating dispquote-kirankumar
			param.subClass = params[5];
			param.wrapNode = params[6];
			if (kriya.selection){
				range = rangy.getSelection();
				range.removeAllRanges();
				range.addRange(kriya.selection);
			}
			//http://stackoverflow.com/a/4220880/3545167
			//get block node with rangy

			//set attribute to table foot note by tamil selvan on 8.10.18
			//param.attrName need to check with empty string also-kirankumar
			if(param.attrName != "" && param.attrName!=undefined){
				var selection = rangy.getSelection();
				var range = selection.getRangeAt(0);
				if($(range.commonAncestorContainer).closest('p')){
					$(range.commonAncestorContainer).closest('p').attr(param.attrName,param.attrValue);
				}
			}else if (param.type == "inline"){
				var selection = rangy.getSelection();
				var range = selection.getRangeAt(0);
				var blockNode = false;
				if ($(range.commonAncestorContainer).closest('.jrnlRefText')){
					blockNode = $(range.commonAncestorContainer).closest('.jrnlRefText')
				}
				if (range.endContainer == range.commonAncestorContainer){
					if (range.commonAncestorContainer.parentNode.nodeName == 'SPAN' && (range.text()== "" || range.text() == range.commonAncestorContainer.parentNode.textContent)){
						range.commonAncestorContainer.parentNode.setAttribute('class', param.class);
					}else if (/I|EM|STRONG|B/.test(range.commonAncestorContainer.parentNode.nodeName) && range.commonAncestorContainer.parentNode.parentNode.nodeName == 'SPAN' && (range.text()== "" || range.text() == range.commonAncestorContainer.parentNode.parentNode.textContent)){
						range.commonAncestorContainer.parentNode.parentNode.setAttribute('class', param.class);
					}else{
						var newNode = document.createElement('span');
						var ancestor = range.commonAncestorContainer.parentNode;
						var textContent = range.commonAncestorContainer.textContent;
						newNode.setAttribute('class', param.class);
						newNode.innerHTML = range.toHtml();
						var r = new RegExp('^(.*?)' + range.text().replace(/(?=[\(\) \[\]])/g, '\\') + '(.*?)$');
						var firstGroup = textContent.replace(r, '$1');
						if (firstGroup == ""){
							if (ancestor.nodeName == 'SPAN'){
								//var ancestor = range.commonAncestorContainer;
								selection.getRangeAt(0).deleteContents();
								ancestor.parentNode.insertBefore(newNode, ancestor);
							}else{
								selection.getRangeAt(0).deleteContents()
								selection.getRangeAt(0).insertNode(newNode);
							}
						}else{
							selection.getRangeAt(0).deleteContents()
							selection.getRangeAt(0).insertNode(newNode);
							if(ancestor.nodeName == 'SPAN' && /^[\,\.\:\s\;\-]+$/.test(firstGroup)){
								var prev = newNode.previousSibling;
								ancestor.parentNode.insertBefore(prev, ancestor);
								ancestor.parentNode.insertBefore(newNode, ancestor);
							}
						}
					}
				}else{
					var newNode = document.createElement('span');
					newNode.setAttribute('class', param.class);
					newNode.innerHTML = range.toHtml();
					var formattingEls = newNode.getElementsByTagName("*");
					// Remove the formatting elements
					for (var i = 0, c = 0, l = formattingEls.length; i < l; i++ ) {
						el = formattingEls[c];
						if (! /^(I|B|EM|STRONG)/.test(el.nodeName)){
							stylePalette.actions.DOMRemove(el);
						}else{
							c++;
						}
					}
					selection.getRangeAt(0).deleteContents()
					selection.getRangeAt(0).insertNode(newNode);
					
					//to remove empty elements after tagging
					var prev = newNode.previousSibling
					if (prev && (prev.textContent == "" || prev.textContent == null)){
						prev.parentNode.removeChild(prev);
					}
					else if (prev && prev.nodeType == 1 && /^[\s\,\.\;\:]*$/.test(prev.textContent)){
						stylePalette.actions.DOMRemove(prev);
					}
					var next = newNode.nextSibling
					if (next && (next.textContent == "" || next.textContent == null)){
						next.parentNode.removeChild(next);
					}else if (next && next.nodeType == 1 && /^[\s\,\.\;\:]*$/.test(next.textContent)){
						stylePalette.actions.DOMRemove(next);
					}
				}
				if (blockNode){
					stylePalette.actions.checkUntagged(blockNode);
				}
			}else{
				var sel = rangy.getSelection();
				var range = sel.getRangeAt(0);
				if (sel.rangeCount) {
					// get All selected elements including text nodes
					var selectedNodes = stylePalette.actions.getBlockNodes();
				}
				//parameters( param.subClass, param.wrapNode) added for creating dispQuote-kirankumar
				selectedNodes = stylePalette.actions.renameNode(selectedNodes, param.name, param.class, param.subClass, param.wrapNode);
				stylePalette.actions.checkUntagged(selectedNodes);
				//to retain the selection
				if (selectedNodes.length > 0){
					sel = rangy.getSelection();
					range = sel.getRangeAt(0);
					range.setStart(selectedNodes[0], 1);
					var len = selectedNodes.length - 1;
					range.setEnd(selectedNodes[len], 1);
					sel.removeAllRanges();
					sel.addRange(range);
					kriya.selection = range;
				}
			}
			kriyaEditor.init.addUndoLevel('navigation-key', range.anchorNode);
			kriyaEditor.init.save();
		},
		renameNode: function(node, name, className,subClass,wrapNode){
			var newNodes = [];
			//this will helph full for creating display quote as div if have sibling disp-quotes contineusly it will merge in one div node.
			//this will helpfull for pullquote also
			if(wrapNode){
				var prvnode = $(node).prev('.'+className);
					var nextnode = $(node).next('.'+className);
					var CheckFrontQuoteAttrib = true;
					//this code will check "FrontQuoteAttrib" class in blockQuote and pullQuote
					//if there "FrontQuoteAttrib" class as last element or first element based on that blockQuote or pullQuote will create
					//start
					if(prvnode.length > 0 || nextnode.length > 0){
						if(prvnode.length > 0 && nextnode.length > 0){
							var prvtemp = $(prvnode).find('p').last();
							var nexttemp = $(nextnode).find('p').first();
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib') && nexttemp.length>0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else if(prvnode.length > 0){
							var temp = $(prvnode).find('p').last();
							if(temp.length > 0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}else{
							var temp = $(nextnode).find('p').first();
							if(temp.length>0 && temp.hasClass('jrnlQuoteAttrib')){
								CheckFrontQuoteAttrib = false;
							}
						}
					}
					if(CheckFrontQuoteAttrib && (prvnode.length > 0 || nextnode.length > 0) && $(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length == 0){
						var newParentNode =	$(node).prev('.'+className);
						if($(node).prev('.'+className).length > 0 && $(node).next('.'+className).length > 0){
							if(prvtemp.length > 0 && prvtemp.hasClass('jrnlQuoteAttrib')){
								var parent ="next";
								newParentNode =	$(node).next('.'+className);
							}else if(nexttemp.length > 0 && nexttemp.hasClass('jrnlQuoteAttrib')){
								var parent ="prev";
							}else{//end
								var parent = "both";
							}
						}else if($(node).prev('.'+className).length > 0){
							var parent ="prev";
						}else{
							var parent ="next";
							newParentNode =	$(node).next('.'+className);
						}
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
								$(currNode).closest('p').attr('class-name',subClass);
							}
							if(parent == "both"){
								var next = $(nextnode[0]).clone();
								$(newParentNode).append($(currNode)).append($(next).contents().unwrap());
								$(nextnode).attr('data-removed',true);
								kriyaEditor.settings.undoStack.push($(nextnode));
								$(nextnode).remove();
							}else if(parent == "next"){
								$(newParentNode).prepend($(currNode))
							}else{
								$(newParentNode).append($(currNode))
							}

						}
						//need to return newNodes if new nodes is empty save will not trigger
						newNodes.push($(newParentNode)[0]);
						kriyaEditor.settings.undoStack.push(newParentNode)
					}else if($(node).closest('.jrnlBlockQuote, .jrnlPullQuote').length > 0){
						var parentnode = $(node).closest('.jrnlBlockQuote , .jrnlPullQuote')
						$(parentnode).attr('class',className);
						var prvnode = $(parentnode).prev('.'+className);
						var nextnode = $(parentnode).next('.'+className);
						if(prvnode.length > 0 && nextnode.length > 0){
							var newParentNode = prvnode;
							var next = $(nextnode[0]).clone();
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap()).append($(next).contents().unwrap());
							$(nextnode).attr('data-removed',true);
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(nextnode));
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
							$(nextnode).remove();
						}else if(prvnode.length > 0){
							var newParentNode = prvnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).append($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}else if(nextnode.length > 0){
							var newParentNode = nextnode;
							var curNode = $(parentnode).clone();
							$(newParentNode).prepend($(curNode).contents().unwrap());
							$(parentnode).attr('data-removed',true);
							//need to return newNodes if new nodes is empty save will not trigger
							newNodes.push($(newParentNode)[0]);
							kriyaEditor.settings.undoStack.push($(newParentNode));
							$(parentnode).remove();
						}
						kriyaEditor.settings.undoStack.push($(parentnode));
					}else{
						var newParentNode = document.createElement(name);
						$(newParentNode).attr('class',className);
						$(newParentNode).attr('id',uuid.v4());
						$(newParentNode).attr('data-inserted','true');
						$(newParentNode).insertBefore(node[0]);
						for(var selNode = 0, sn = node.length; selNode < sn; selNode++){
							var currNode = $(node[selNode]).clone();
							$(node[selNode]).attr('data-removed','true');
							kriyaEditor.settings.undoStack.push(node[selNode]);
							$(node[selNode]).remove();
							if(subClass){
								$(currNode).closest('p').attr('class',subClass);
								$(currNode).closest('p').attr('class-name',subClass);
							}
							$(newParentNode).append($(currNode))
						}
						newNodes.push(newParentNode);
					}
			}else{
				for (var r = 0, rl = node.length; r < rl; r++){
					var currNode = node[r];
					if (currNode.nodeName == "DIV") return;
					if (typeof(className) != "undefined"){
						//to change label as ref citations
						if(window.location.pathname && window.location.pathname.match(/structure_content/g)){
							if (currNode.getAttribute('class').match(/Caption/i) && !className.match(/Caption/i)){
								$(currNode).find('.label').each(function(){
									$(this).attr('class', currNode.getAttribute('class').replace('Caption', 'Ref'));
									$(this).attr('data-citation-string', ' ' + currNode.getAttribute('id') + ' ');
								});
							}
							currNode.removeAttribute('id');
							currNode.removeAttribute('data-id');
						}
						if(!currNode.hasAttribute('data-old-class'))
						{
						currNode.setAttribute('data-old-class',currNode.className);
						}
						currNode.setAttribute('class', className);
						currNode.setAttribute('class-name', className.replace(/^(jrnl|Ref|Abs)/, ''));

						//create the id and data-id tp reftextand footnotePara
						if(className.match(/jrnlRefText|jrnlFootNotePara/i)){
							if(className=="jrnlRefText"){
								var idprefix = "R"
							}else if(className=="jrnlFootNotePara"){
								var idprefix = "BFN"
							}
							var refArray = [];
							$('#contentDivNode .'+className+'[data-id]').each(function(){
								refArray.push($(this).attr('data-id').replace(/[a-z]+/gi, ''));
							});
							if(refArray.length >0){
								var max = Math.max.apply( Math, refArray );
								refID = max + 1;
							}else{
								refID = 1;
							}
							if((!currNode.hasAttribute('data-id') && currNode.className == className) || currNode.className != className){
								currNode.setAttribute('data-id',idprefix+refID);
								currNode.setAttribute('id',idprefix+refID);
							}
						}
					}
					//caption style post to content structure added by vijayakumar on 5-10-2018
					if(className.match(/Caption/i)){
						$('.la-container').fadeIn();
						//unwrap the jrnlpattern and label before sent structure content updated by vijayakumar on 26-11-2018
						$(currNode).find('.label, .jrnlPatterns').contents().unwrap()
						var parameters = {'customer' : kriya.config.content.customer, 'project' : kriya.config.content.project, 'doi' : kriya.config.content.project+'.captions', 'fileData': '<div class="body">'+currNode.outerHTML+'</div>'};
						kriya.general.sendAPIRequest('structurecontent', parameters, function(res){
							if(res.error){
								$('.la-container').fadeOut();
							}else if(res){
								$('.la-container').fadeOut();
								$('[data-type="popUp"]').addClass('hidden')
								data = res.replace(/&apos;/g, "\'");
								var response = $('<r>'+ data + '</r>');
								if ($(response).find('response').length > 0 && $(response).find('response content').length > 0){
									var r = $(response).find('.body p')
									if(r.length > 0){
										$(currNode).after(r)
										$(currNode).remove()
									}
								}							
							}
						}, function(err){
							if(onError && typeof(onError) == "function"){
								onError(err);
							}
						});
					}else{
						if (currNode.nodeName != name){
							var newNode = document.createElement(name);
							Array.prototype.slice.call(currNode.attributes).forEach(function(a) {
								newNode.setAttribute(a.name, a.value);
							});
							while (currNode.firstChild) {
								newNode.appendChild(currNode.firstChild);
							}
							currNode.parentNode.replaceChild(newNode, currNode);
							newNodes.push(newNode);
						}else{
							
								newNodes.push(currNode);
							}
					}
				}
				//this code will unwrap nodes if that node is already in disp-Quote or pull-Quote div when we applay other options
				var unWarpFromNode = ['jrnlBlockQuote','jrnlPullQuote'];
				var allowedParaNode = ['jrnlQuoteAttrib','jrnlQuotePara'];
				var parentSelNode;
				var savedNodes = [];
				for(var uw = 0, nn = newNodes.length;  uw < nn; uw++){
					if((unWarpFromNode.indexOf($(newNodes[uw]).closest('div').attr('class'))>=0)&&(allowedParaNode.indexOf($(newNodes[uw]).attr('class')) < 0)){
						parentSelNode = $(newNodes[uw]).closest('div');
						var nextNode = $(newNodes[uw]).next();
						var prevNode = $(newNodes[uw]).prev();
						if(nextNode.length > 0 && prevNode.length > 0){
							var allNextNodes = $(newNodes[uw]).nextAll();
							$(newNodes[uw]).insertAfter($(parentSelNode));
							$(newNodes[uw]).attr('data-inserted','true');
							kriyaEditor.settings.undoStack.push($(parentSelNode));
							kriyaEditor.settings.undoStack.push($(newNodes[uw]));
							var className = $(parentSelNode).attr('class') ;
							var newParentNode = document.createElement('div');
							$(newParentNode).attr('class',className);
							$(newParentNode).attr('id',uuid.v4());
							$(newParentNode).attr('data-inserted','true');
							$(newParentNode).insertAfter(newNodes[uw]);
							$(newParentNode).append($(allNextNodes));
							kriyaEditor.settings.undoStack.push($(newParentNode));
						}
						else{
							if(nextNode.length == 0 && prevNode.length == 0){
								$(newNodes[uw]).attr('data-moved','true');
								$(newNodes[uw]).insertBefore($(parentSelNode));
								kriyaEditor.settings.undoStack.push($(newNodes[uw]));
								//After this function addUndoLevel() is calling.no need to call two times.if it call two times at that time undo button giving wrong result when we unwrap the node-kirankumar
								//kriyaEditor.init.addUndoLevel('style-change');
								newNodes.splice(uw);
							}
							else{
								if(nextNode.length > 0){
									$(newNodes[uw]).attr('data-moved','true');
									$(newNodes[uw]).insertBefore($(parentSelNode));
								}
								else if(prevNode.length > 0){
									$(newNodes[uw]).attr('data-moved','true');
									$(newNodes[uw]).insertAfter($(parentSelNode));
								}
								kriyaEditor.settings.undoStack.push($(newNodes[uw]));
							}
						}
					}
					if($(parentSelNode).find('p').length == 0){
						kriyaEditor.settings.undoStack.push($(parentSelNode).attr('data-removed','true'));
						//After this function addUndoLevel() is calling.no need to call two times.if it call two times at that time undo button giving wrong result when we unwrap the node-kirankumar
						//kriyaEditor.init.addUndoLevel('style-change');
						$(parentSelNode).remove();
					}
				}
			}
			return newNodes;
		},
		DOMRemove: function(el) {
			var parent = el.parentNode;
			while (el.hasChildNodes()) {
				parent.insertBefore(el.firstChild, el);
			}
			parent.removeChild(el);
		},
		getBlockNodes: function(){
			if (kriya.selection){
				range = rangy.getSelection();
				range.removeAllRanges();
				range.addRange(kriya.selection);
			}
			var sel = rangy.getSelection();
			var range = sel.getRangeAt(0);
			var selectedNodes = [];
			for (var i = 0; i < sel.rangeCount; ++i) {
				//http://stackoverflow.com/a/10075894/3545167
				//this gives all nodes including text and childnodes of the selected blocknodes
				//selectedNodes = selectedNodes.concat( sel.getRangeAt(i).getNodes() );
				
				//http://stackoverflow.com/a/4220880/3545167
				//this returns only the block level nodes, which we actually need here
				selectedNodes = range.getNodes([1], stylePalette.actions.isBlockElement);
			}
			//check if the first node is a blockNodeRegex
			//if not get the first possible block node
			if (selectedNodes.length == 0){
				ancestor = range.commonAncestorContainer;
				var blockElements = stylePalette.actions.isBlockElement(ancestor);
				if (! blockElements){
					do {
						ancestor = ancestor.parentNode;
						var blockElements = stylePalette.actions.isBlockElement(ancestor);
					}while (! blockElements);
				}
				selectedNodes = [ancestor];
			}
			return selectedNodes;
		},
		isBlockElement: function(el){
			// You may want to add a more complete list of block level element
			// names on the next line
			return /^(h[1-6]|p|hr|pre|blockquote|ol|ul|li|dl|dt|dd|div|table|caption|colgroup|col|tbody|thead|tfoot|tr|th|td)$/i.test(el.tagName);
		},
		moveUp: function(param, targetNode){
			var selectedBlocks = stylePalette.actions.getBlockNodes();
			if (selectedBlocks.length == 0) return;
			/*if(/^OL|UL|LI$/.test(selectedBlocks[0].nodeName)){
				var blocks = selectedBlocks;
			}else {
				var blocks = commonjs.editor.element.cleanSelectedBlocks();
			}*/
			var firstNodeInSel = selectedBlocks[0];
			var selBlocksLen = selectedBlocks.length;
			var lastNodeInSel = selectedBlocks[selBlocksLen - 1];
			var prevNode = $(firstNodeInSel).prev();
			if (prevNode.length == 0 && ( $(firstNodeInSel).parent().prop('tagName') == 'OL' || $(firstNodeInSel).parent().prop('tagName') == 'UL' ) ){ //if list then previous sibling is <ol>,<ul> previousSibling
				prevNode = $(firstNodeInSel).parent().prev();
			}
			if (prevNode.length == 0){
				//commonjs.general.showNotice(true, 'Cannot Move Content', '','warning');
				return;
			}
			if(!lastNodeInSel.hasAttribute('data-changed')){
				lastNodeInSel.setAttribute('data-changed','move-up');
			}
			prevNode.insertAfter( $(lastNodeInSel) );
			this.addToSave(lastNodeInSel, 'data-moved');
			this.trigerSave("move-up");
		},
		moveDown: function(param, targetNode){
			var selectedBlocks = stylePalette.actions.getBlockNodes();
			if (selectedBlocks.length == 0) return;

			var firstNodeInSel = selectedBlocks[0];
			var selBlocksLen = selectedBlocks.length;
			var lastNodeInSel = selectedBlocks[selBlocksLen - 1];
			var nextNode = $(lastNodeInSel).next();
			if (nextNode.length == 0 && ( $(lastNodeInSel).parent().prop('tagName') == 'OL' || $(lastNodeInSel).parent().prop('tagName') == 'UL' )){ //if list then previous sibling is <ol>,<ul> previousSibling
				nextNode = $(lastNodeInSel).parent().next();
			}
			if (nextNode.length == 0){
				//commonjs.general.showNotice(true, 'Cannot Move Content', '','warning');
				return;
			}
			if(!firstNodeInSel.hasAttribute('data-changed')){
				firstNodeInSel.setAttribute('data-changed','move-down');
			}
			nextNode.insertBefore( $(firstNodeInSel) );

			this.addToSave(firstNodeInSel, 'data-moved');
			this.trigerSave("move-down");
		},
		mergeWithPrev: function(){
			var thisObj = this;
			//get only the block elements and move them
			var selectedBlocks = stylePalette.actions.getBlockNodes();
			if (selectedBlocks.length == 0) return;
			
			blocksSelectedLen = selectedBlocks.length;
			var firstNodeInSel = selectedBlocks[0];
			var prevNode = $(firstNodeInSel).prev();
			if (prevNode.length == 0){
				//commonjs.general.showNotice(true, 'Cannot Merge Content', 'Cannot merge content with previous content.', 'warning');
				return;
			}
			if(!prevNode[0].hasAttribute('data-changed')){
				prevNode[0].setAttribute('data-changed','merge-with-prev');
			}
			$(selectedBlocks).each(function( index ) {
				if ($(this).attr('data-untag') != undefined){
					var position = $(this).attr('data-untag');
					$('#untaggedElementContent').find('*[data-untag="' + position + '"]').remove();
					$(this).removeAttr('class');
					$(this).removeAttr('data-untag');
				}
				prevNode.append( ' ' + $(this).html() );
				$(this).remove();
				thisObj.addToSave(this, 'data-removed');
			});
			if ($(prevNode).hasClass('jrnlRefText') || $(prevNode).hasClass('jrnlFootNotePara')){
				if($(prevNode).hasClass('jrnlRefText')){
					var className = "jrnlRefText";
					var refclass= "jrnlBibRef"
					var rid= "R";
				}else{
					var className = "jrnlFootNotePara";
					var refclass= "jrnlFootNoteRef"
					var rid= "BFN";
				}
				if(className == "jrnlFootNotePara"){
					var rcount = 1;  
					$(kriya.config.containerElm + ' .'+className).each(function(){
						var oldId = $(this).attr('data-id');
						var newID = rid + rcount
						if(newID != oldId){
							$(kriya.config.containerElm + ' .'+refclass+'[data-citation-string*=" '+ oldId +' "]').each(function(){
								var cid = $(this).attr('data-citation-string');
								cid = cid.replace(" "+ oldId +" ", " " + rid + rcount+ " ");
								$(this).attr('data-citation-string', cid);
							})
							
						}
						$(this).attr('id', newID);
						$(this).attr('data-id', newID);
						rcount++;
					});
				}
			}
			thisObj.addToSave(prevNode);
			thisObj.trigerSave("merge-with-prev");

		},
		mergeWithNext: function(){
			var thisObj = this;
			//get only the block elements and move them
			var selectedBlocks = stylePalette.actions.getBlockNodes();
			if (selectedBlocks.length == 0) return;
			
			selBlocksLen = selectedBlocks.length;
			var lastNodeInSel = selectedBlocks[selBlocksLen - 1];
			var nextNode = $(lastNodeInSel).next();
			if (nextNode.length == 0){
				//commonjs.general.showNotice(true, 'Cannot Merge Content', 'Cannot merge content with next content.', 'warning');
				return;
			}
			if(!nextNode[0].hasAttribute('data-changed')){
				nextNode[0].setAttribute('data-changed','merge-with-next');
			}
			$(selectedBlocks.reverse() ).each(function( index ) {
				if ($(this).attr('data-untag') != undefined){
					var position = $(this).attr('data-untag');
					$('#untaggedElementContent').find('*[data-untag="' + position + '"]').remove();
					$(this).removeAttr('class');
					$(this).removeAttr('data-untag');
				}
				nextNode.prepend( $(this).html() + ' ');
				$(this).remove();
				thisObj.addToSave(this, 'data-removed');
			});
			if ($(nextNode).hasClass('jrnlRefText') || $(nextNode).hasClass('jrnlFootNotePara')){
				if($(nextNode).hasClass('jrnlRefText')){
					var className = "jrnlRefText";
					var refclass= "jrnlBibRef"
					var rid= "R";
				}else{
					var className = "jrnlFootNotePara";
					var refclass= "jrnlFootNoteRef"
					var rid= "BFN";
				}
				if(className == "jrnlFootNotePara"){
					var rcount = 1;  
					$(kriya.config.containerElm + ' .'+className).each(function(){
						var oldId = $(this).attr('data-id');
						var newID = rid + rcount
						if(newID != oldId){
							$(kriya.config.containerElm + ' .'+refclass+'[data-citation-string*=" '+ oldId +' "]').each(function(){
								var cid = $(this).attr('data-citation-string');
								cid = cid.replace(" "+ oldId +" ", " " + rid + rcount+ " ");
								$(this).attr('data-citation-string', cid);
							})
							
						}
						$(this).attr('id', newID);
						$(this).attr('data-id', newID);
						rcount++;
					});
				}
			}
			thisObj.addToSave(nextNode);
			thisObj.trigerSave("merge-with-next");
		},
		moveToPrevSection: function(position){
			if(position == undefined || position == '') position = 'top';
			var newSection = false;
			var blocks = stylePalette.actions.getBlockNodes();
			if (blocks.length == 0) return;
			
			blocksSelectedLen = blocks.length;
			
			var nodes = '.front, .body, .back, .sub-front, .sub-body, .sub-back, .sub-article';
			if( $(blocks[0]).closest(nodes).length > 0 ){
				if($(blocks[0]).closest(nodes).prev().length > 0){
					if($(blocks[0]).closest(nodes).prev().attr('class') == "sub-article"){
						newsec = $(blocks[0]).closest('.sub-article').prev()
						if($(newsec).find('.sub-front, .sub-body, .sub-back').length > 0){
							if(position == 'top'){
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').first()
							}else{
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').last()
							}
						}else{
							newSection = newsec;
						}
					}else{
						newSection = $(blocks[0]).closest('.front, .body, .back, .sub-front, .sub-body, .sub-back').prev()
					}
				}else if($(blocks[0]).closest('.sub-article').length){
					if($(blocks[0]).closest('.sub-article').prev().length > 0){
						var newsec = $(blocks[0]).closest('.sub-article').prev();
						if($(newsec).find('.sub-front, .sub-body, .sub-back').length > 0){
							if(position == 'top'){
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').first()
							}else{
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').last()
							}
						}else{
							newSection = newsec;
						}
					}
				}
			}else {
				alert('Cannot Move Content\nThere is no previous section to move.');
				return;
			}
			if(!newSection)return;
			if(position == 'top'){
				for(var i= (blocks.length -1); i >= 0; i--){
					if(newSection.children().first().length == 1){
						newSection.children().first().before($(blocks[i]));
					}else {
						newSection.append($(blocks[i]));
					}
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-prv-top');
					}
					$(blocks[i]).removeAttr('class-name').removeAttr('id').removeAttr('data-id').attr('class', 'jrnlunTagged');
				}
			}else{
				for(var i= 0; i <  blocks.length; i++){
					newSection.append($(blocks[i]));
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-prv-bottom');
					}
					$(blocks[i]).removeAttr('class-name').removeAttr('id').removeAttr('data-id').attr('class', 'jrnlunTagged');
				}
			}
		},

		//Selected node move to prev float added by vijayakumar on 20-12-2018
		MoveToFloatBlock: function(){
			var blocks = stylePalette.actions.getBlockNodes();
			if(($(stylePalette.actions.getBlockNodes()).prev().length > 0 && $(stylePalette.actions.getBlockNodes()).prev().attr('class'))|| ($(kriya.selection.commonAncestorContainer).closest('table.ecsTable').length > 0 && $(kriya.selection.commonAncestorContainer).closest('table.ecsTable').prev().length > 0 && $(kriya.selection.commonAncestorContainer).closest('table.ecsTable').prev().attr('class'))){
				if($(kriya.selection.commonAncestorContainer).closest('table.ecsTable').length>0){
					var newSection = $(kriya.selection.commonAncestorContainer).closest('table.ecsTable').prev()
					var classname = $(newSection).attr('class')
				}else{
					var newSection = $(stylePalette.actions.getBlockNodes()).prev()
					var classname = $(newSection).attr('class')
				}
				if(classname.match(/^jrnl(Fig|Tbl|Box|Suppl|Vid)Block$/i)){
					if($(kriya.selection.commonAncestorContainer).closest('table.ecsTable').length > 0){
						var blocks = $(kriya.selection.commonAncestorContainer).closest('table.ecsTable');
					}
					for(var i= (blocks.length -1); i >= 0; i--){
						newSection.append($(blocks[i]));
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-next-top');
					}
					}
				}
			}			
		},
		splitSubArticle: function(){
			var blocks = stylePalette.actions.getBlockNodes();
			if(blocks && $(blocks).length > 0){
				if($(blocks).parent().attr('class').match(/[front|body|sub\-article]/)){
					var subcount = $(kriya.config.containerElm).find('.sub-article').length
					$(blocks[0]).before("<div class='sub-article' id='sub-article"+ (subcount+1) +"' data-id='sub-article"+ (subcount+1) +"'/>")
					var prevnote = $(blocks[0]).prev();
					$(prevnote).append($(prevnote).nextAll());
					$(blocks).parent().parent().after(prevnote);
				}

			}		
		},
		splitSection: function(tagname, classname){
			if(!tagname && !classname)return;
			var blocks = stylePalette.actions.getBlockNodes();
			if(blocks && $(blocks).length > 0 && $(blocks).closest('.sub-article').length > 0){
					$(blocks[0]).before("<"+ tagname +" class='"+classname+"'/>")
					var prevnote = $(blocks[0]).prev();
					for(var i= 0; i <  blocks.length; i++){
					$(prevnote).append($(blocks[i]));
				}
			}		
		},
		//Selected node move to after float added by vijayakumar on 20-12-2018
		MoveToAfterBlock: function(){
			var blocks = stylePalette.actions.getBlockNodes();
			if($(kriya.selection.commonAncestorContainer).closest('*[class^="jrnl"][class*="Block"]').length > 0){
				var newSection = $(kriya.selection.commonAncestorContainer).closest('*[class^="jrnl"][class*="Block"]')
				if(($(kriya.selection.commonAncestorContainer).closest('table.ecsTable').length>0) || ($(stylePalette.actions.getBlockNodes()).length > 0)){
					if($(kriya.selection.commonAncestorContainer).closest('table.ecsTable').length>0){
						var blocks = $(kriya.selection.commonAncestorContainer).closest('table.ecsTable')
					}
					for(var i= (blocks.length -1); i >= 0; i--){
						$(newSection).after($(blocks[i]));
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-next-top');
					}
				}
			}
			}
		},
		replaceTable : function(){			
			var componentType = "TABLE_REPLACE_edit";
			var con = $(kriya.selection.commonAncestorContainer).closest('table')
			if(con.length>0){
				$(con).attr('data-selected', 'true');	
				var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
				$(component).removeAttr('style').removeClass('hidden').css({'top':'30%','left':'40%','width':'0','height':'0','opacity':'1'
				});
				$(component).find('*[data-type="floatComponent"]').html('');
				$(component).find('*[data-type="htmlComponent"]').removeClass('hidden')
				$(component).animate({top: '0',	left:'0', width:'100%',	height:'100%', opacity:('1')},500)
			}
		},
		saveTable : function(){
			var componentType = "TABLE_REPLACE_edit";
			var component = $("*[data-type='popUp'][data-component='" + componentType + "']");
			var tablehtml = $(component).find('[data-type="floatComponent"]').html()
			if(tablehtml !=""){
				var tablenode = $(component).find('*[data-type="floatComponent"]');
				var table = $(tablenode)[0].outerHTML;
				var oldtable = $(kriya.config.containerElm).find('*[data-selected="true"]')
				if(oldtable.length == 1){
					$.each($(table)[0].attributes,function(i,attr){
						var attrVal = $(table).attr(attr.nodeName);
						if(attrVal && attr.nodeName != "data-type" && attr.nodeName != "data-selector" && attr.nodeName != "data-class" && attr.nodeName != "class"){
							if(attr.nodeName == "class"){
								$(oldtable).addClass(attrVal);
							}else{
								$(oldtable).attr(attr.nodeName, attrVal);		
							}
						}
					});
					$(oldtable).html(tablehtml);
					$(kriya.config.containerElm).find('*[data-selected="true"]').removeAttr('data-selected')
					eventHandler.components.general.closePopUp('', $('[data-component="TABLE_REPLACE_edit"]'));
				}
			}else{
				kriya.notification({title : 'WARNING',type  : 'warning',timeout : 5000,content : 'Table not replaced. Please contect Support' ,icon: 'icon-warning2'});
			}
		},
		moveToNextSection: function(position){
			if(position == undefined || position == '') position = 'top';
			var newSection = false;
			var blocks = stylePalette.actions.getBlockNodes();
			//check if selected content is empty then there is only one block to move.
			var nodes = '.front, .body, .back, .sub-front, .sub-body, .sub-back, .sub-article';
			if( $(blocks[0]).closest(nodes).length > 0 ){
				if($(blocks[0]).closest(nodes).next().length > 0){
					if($(blocks[0]).closest(nodes).next().attr('class') == "sub-article"){
						newsec = $(blocks[0]).closest('.sub-article').next()
						if($(newsec).find('.sub-front, .sub-body, .sub-back').length > 0){
							if(position == 'top'){
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').first()
							}else{
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').last()
							}
						}else{
							newSection = newsec;
						}
					}else{
						newSection = $(blocks[0]).closest('.front, .body, .back, .sub-front, .sub-body, .sub-back').next()
					}
				}else if($(blocks[0]).closest('.sub-article').length){
					if($(blocks[0]).closest('.sub-article').next().length > 0){
						newsec = $(blocks[0]).closest('.sub-article').next()
						if($(newsec).find('.sub-front, .sub-body, .sub-back').length > 0){
							if(position == 'top'){
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').first()
							}else{
								newSection = $(newsec).find('.sub-front, .sub-body, .sub-back').last()
							}
						}else{
							newSection = newsec;
						}
					}
				}
			}else {
				alert('Cannot Move Content\nThere is no next section to move.');
				return;
			}
			if(!newSection)return;
			if(position == 'top'){
				for(var i= (blocks.length -1); i >= 0; i--){
					if(newSection.children().first().length == 1){
						newSection.children().first().before($(blocks[i]));
					}else {
						newSection.append($(blocks[i]));
					}
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-next-top');
					}
					$(blocks[i]).removeAttr('class-name').removeAttr('id').removeAttr('data-id').attr('class', 'jrnlunTagged');
				}
			}else{
				for(var i= 0; i <  blocks.length; i++){
					newSection.append($(blocks[i]));
					if(!blocks[i].hasAttribute('data-changed')){
						blocks[i].setAttribute('data-changed','move-next-bottom');
					}
					$(blocks[i]).removeAttr('class-name').removeAttr('id').removeAttr('data-id').attr('class', 'jrnlunTagged');
				}
			}
		},
		correspMapping: function(authornode){
			var con = $(kriya.selection.commonAncestorContainer).closest('p')
			var conclass = con.attr('class');
			if(conclass == 'jrnlCorrAff'){
				var conid = con.attr('data-id');
				//If data-id not found then create new data-id
				if(!conid){
					//Get the max id and increase one from that
					var arr = [];
					conid = "cor1";
					$(kriya.config.containerElm).find('.jrnlCorrAff[data-id]').each(function(){
						var rid = $(this).attr('data-id');
						if(rid.match(/cor(\d+)/)){
							arr.push(parseInt(rid.match(/cor(\d+)/)[1]));
						}
					});
					if(arr.length > 0){
						var max = arr.sort(function(a, b) { return b - a })[0];
						conid = "cor" + (max+1);
					}
					con.attr('data-id', conid);
				}

				//Add Corr author symbol updated by vijayakumar on 14-11-2018
				var symbole = "*";
				var consymbol = con.attr('data-symbol');
				if(consymbol){
					symbole = consymbol;
				}else{
					con.attr('data-symbol', symbole);
				}
				$('.front .jrnlAuthors .jrnlAuthor:contains(' + authornode + ')').parent().find('.jrnlCorrRef').remove();
				$('.front .jrnlAuthors .jrnlAuthor:contains(' + authornode + ')').after('<span class="jrnlCorrRef" data-ref-type="corresp" rid="'+conid+'" data-rid="'+conid+'">'+symbole+'</span>');
			}
			kriyaEditor.init.save();
		},
		clearFormatting: function(){
			if (kriya.selection){
				range = rangy.getSelection();
				range.removeAllRanges();
				range.addRange(kriya.selection);
			}
			var sel = rangy.getSelection();
			var range = sel.getRangeAt(0);
			selectedNodes = [];
			for (var i = 0; i < sel.rangeCount; ++i) {
				selectedNodes = range.getNodes([1,3], function(el) {
					return ! stylePalette.actions.isBlockElement(el)
				});
			}
			var commonAncestor = true;
			if (selectedNodes.length > 1){
				for (var s =0, sl = selectedNodes.length; s < sl; s++){
					if (!(selectedNodes[s].parentNode == range.commonAncestorContainer || selectedNodes[s].parentNode.parentNode == range.commonAncestorContainer)){
						commonAncestor = false;
					}
				}
			}
			//to unwrap common ancestor node instead of its child's
			if (selectedNodes.length > 1 && commonAncestor && sel.text() == range.commonAncestorContainer.textContent){
				//skip creal format if blockelement updated by vijayakumar on 12-11-2018
				var parentRange = range.commonAncestorContainer.parentNode;
				if (! /^(I|EM|B|STRONG|SUP|SUB)$/.test(range.commonAncestorContainer.nodeName) && !stylePalette.actions.isBlockElement(parentRange)){
					stylePalette.actions.DOMRemove(range.commonAncestorContainer);
				}
			}
			else if (selectedNodes.length == 1){
				var newNode = document.createTextNode(range.text());
				if (range.endContainer == range.commonAncestorContainer){
					var parentRange = range.commonAncestorContainer.parentNode;
					if (range.text() == parentRange.textContent){
						//remove only if its not a formatting node
						//skip creal format if blockelement updated by vijayakumar on 12-11-2018
						if (! /^(I|EM|B|STRONG|SUP|SUB)$/.test(parentRange.nodeName) && !stylePalette.actions.isBlockElement(parentRange)){
							stylePalette.actions.DOMRemove(parentRange);
						}
					}else{
						var newNode = document.createTextNode(range.text());
						var textContent = parentRange.textContent;
						var r = new RegExp('^(.*?)' + range.text() + '(.*?)$');
						var firstGroup = textContent.replace(r, '$1');
						if (firstGroup != ""){
							if(range.commonAncestorContainer.parentNode.tagName== "P"){
								return false;
							}
							var so = range.startOffset;
							sel.getRangeAt(0).deleteContents();
							range.setStart(parentRange, 0);
							range.setEnd(parentRange.firstChild, so);
							var parentNode = parentRange.cloneNode();
							parentNode.innerHTML = range.toHtml();
							parentRange.parentNode.insertBefore(parentNode, parentRange);
							parentRange.parentNode.insertBefore(newNode, parentRange);
							sel.getRangeAt(0).deleteContents();
						}else{
							sel.getRangeAt(0).deleteContents()
							parentRange.parentNode.insertBefore(newNode, parentRange);
						}
					}
					//sel.getRangeAt(0).deleteContents()
					//range.commonAncestorContainer.parentNode.insertNode(newNode);
				}else{
					selection.getRangeAt(0).deleteContents()
					selection.getRangeAt(0).insertNode(newNode);
				}	
			}else{
				var rangeText = range.text();
				rangeText = rangeText.replace(/\u0013/g, '');
				rangeText = rangeText.replace(/\u000A/g, ' ');
				var parentRange = false;
				for (var i = 0, sl = selectedNodes.length; i < sl; i++) {
					var el = selectedNodes[i];
					var reg = new RegExp('^\\s?'+ el.textContent.replace(/(?=[\(\) \[\]])/g, '\\'));
					if (reg.test(rangeText)){
						rangeText = rangeText.replace(reg, '');
						//replace the element with its child nodes
						if (el.nodeType == 1 && ! /^(I|EM|B|STRONG|SUP|SUB)$/.test(el.nodeName) && el.parentElement != null){
							var l = el.childNodes.length;
							for (var i = 0; i < l; i++){
								var cn = el.childNodes[0];
								el.parentNode.insertBefore(cn, el);
							}
							el.parentNode.removeChild(el);
						}
						parentRange = el;
					}else{
						var r = new RegExp('^\\s?' + rangeText.replace(/(?=[\(\) \[\]])/g, '\\'))
						if (r.test(el.textContent)){
							var c = rangeText;
							if (el.nodeType == 1){
								var ra = rangy.getSelection();
								var sel = ra.getRangeAt(0);
								sel.setStart(el.firstChild, 0);
								if (el.childNodes.length == 1 || el.firstChild.textContent.length > start){
									sel.setEnd(el.lastChild, c.length);
								}else{
									var l = el.childNodes.length - 1;
									var sl = 0;
									for (var i = 0; i < l; i++){
										var sl = sl + el.childNodes[i].textContent.length;
										if (sl >= end){
											i = l;
											sel.setEnd(el.childNodes[i], c.length);
										}
									}
								}
								//sel.setEnd(el, c.length);
								var newTextContent = sel.text();
								sel.deleteContents()
								var newNode = document.createTextNode(newTextContent);
								el.parentNode.insertBefore(newNode, el);
							}
						}else{
							var c = el.textContent.slice(range.startOffset);
							var r = new RegExp('\\s?' + rangeText.replace(/(?=[\(\) \[\]])/g, '\\'))
							reg = new RegExp('^\\s?' + c.replace(/(?=[\(\) \[\]])/g, '\\'));
							if (c !== "" && reg.test(rangeText) && r.test(el.textContent) && el.nodeType == 1 && el.parentNode.parentNode !== null){
								var ra = rangy.getSelection();
								var sel = ra.getRangeAt(0);
								var start = el.textContent.length - c.length;
								if (el.childNodes.length == 1 || el.firstChild.textContent.length > start){
									sel.setStart(el.firstChild, start);
								}else{
									var l = el.childNodes.length - 1;
									var sl = 0;
									for (var i = 0; i < l; i++){
										var sl = sl + el.childNodes[i].textContent.length;
										if (sl >= start){
											i = l;
											sel.setStart(el.childNodes[i], start);
										}
									}
								}
								sel.setEnd(el.lastChild, el.textContent.length);
								var newTextContent = sel.text();
								sel.deleteContents()
								var newNode = document.createTextNode(newTextContent);
								el.parentNode.insertBefore(newNode, el);
								newNode.parentNode.insertBefore(el, newNode);
							}
						}
						if (c !== ""){
							reg = new RegExp('^\\s?' + c.replace(/(?=[\(\) \[\]])/g, '\\'));
							rangeText = rangeText.replace(reg, '');
						}
					}
				}
			}
		},
		checkUntagged: function(blockNode){
			$(blockNode).each(function(){
				var untagged = false;
				var clone = $(this).clone(true);
				if ($(this).attr('class') == 'jrnlRefText'){
					clone.find('span[class^="Ref"]').remove();
					var textNodes = textNodesUnder(clone[0]);
					for (var tl = 0, tnLength = textNodes.length; tl < tnLength; tl++){
						var curNode = textNodes[tl];
						curNode.textContent = curNode.textContent.replace(/([\.\,\;\:\?\"\'\(\[\u0020\u2013\u201C\u201D\u2018\u2019\u0026\-\)\]]+)/ig, '');
						curNode.textContent = curNode.textContent.replace(/(and|doi|in|pp?|eds?|available (to|from))/ig, '');
						curNode.textContent = curNode.textContent.replace(/[\s\u00A0\u0009]+/ig, '');
						if (curNode.textContent != "") {
							untagged = true;
						}
					}
				}else if ($(this).attr('class') == 'jrnlAuthors'){
					clone.find('.jrnlAuthor,.jrnlDegrees,sup,span[class]').remove();
					clone.text(clone.text().replace(/ and[\,\s]+/, ''))
					if (/[a-z]/i.test(clone.text())){
						untagged = true;
					}
				}else if ($(this).find('.jrnlAuthor').length > 0){
					//to remove author names, if the current one was author and changed
					$(this).find('.jrnlAuthor,.jrnlDegrees,.jrnlDegree').contents().unwrap();
				}
				if (!untagged){
					var rid = $(this).attr('data-untag');
					$('#untaggedDivNode [data-untag="' + rid + '"]').remove();
					$(this).removeAttr('data-untag');
				}
			});
		},
		parseAuthors: function(){
			if (kriya.selection){
				range = rangy.getSelection();
				range.removeAllRanges();
				range.addRange(kriya.selection);
				var selection = rangy.getSelection();
				var range = selection.getRangeAt(0);
				if (range.text() == "") return false;
				var params = {"authorNodeString" : '<p>'+ range.text() +'</p>', "classPrefix" : "jrnl"};
				$('.la-container').fadeIn();
				kriya.general.sendAPIRequest('parseauthors', params, function(res){
					$('.la-container').fadeOut();
					if (res.body == ''){
						alert('Unable to parse');
					}else if ($(res.body).find('.jrnlAuthor').length > 0){
						var newNode = $(res.body).find('.jrnlAuthor')[0];
						selection.getRangeAt(0).deleteContents()
						selection.getRangeAt(0).insertNode(newNode);
					}
				},function(res){
					$('.la-container').fadeOut();
				});
			}
		},
		parseReference: function(){
		//structure reference or style the current one's as refernce which will also do the structuring of reference
			if (kriya.selection){
				var sel = rangy.getSelection();
				var range = sel.getRangeAt(0);
				if (sel.rangeCount) {
					// get All selected elements including text nodes
					var selectedNodes = stylePalette.actions.getBlockNodes();
				}
				if (selectedNodes.length > 0){
					var refArray = [];
					$('#contentDivNode .jrnlRefText[data-id]').each(function(){
						refArray.push($(this).attr('data-id').replace(/[a-z]+/gi, ''));
					});
					if(refArray.length >0){
						var max = Math.max.apply( Math, refArray );
						refID = max + 1;
					}else{
						refID = 1;
					}
					var postData = '';
					//add class nad id, if it is not a reference
					$(selectedNodes).each(function(){
						if ((!$(this).attr('data-id') && (/jrnlRefText/.test($(this).attr('class')))) || !/jrnlRefText/.test($(this).attr('class'))){
							$(this).attr('class', 'jrnlRefText').attr('id', 'R'+refID).attr('data-id', 'R'+refID++);
						}
						postData += $(this)[0].outerHTML;
					});
					//postData += '</div>';
					var params = {"referenceString" : postData};
					$('.la-container').fadeIn();
					kriya.general.sendAPIRequest('parsereference', params, function(res){
						$('.la-container').fadeOut();
						var htmlBlock = $('<div>').html(res.body);
						//replace the response on the related reference
						$(htmlBlock).find('p.jrnlRefText').each(function(){
							seqID = $(this).attr('id');
							var currRef = $('#contentDivNode .jrnlRefText[id = "' + seqID + '"]');
							var currParsedRefString = $(this).html();
							if (currParsedRefString != ""){
								$(currRef).replaceWith($(this));
							}
						});
					},function(res){
						$('.la-container').fadeOut();
					});
				}
			}
		},
		delete: function(){
			var blocks = stylePalette.actions.getBlockNodes();
		},
		
		/**
		 * Function to add the elements in undoStack to save the elements
		 * @param element - element to save
		 * @param attr - action attribute Ex(data-removed, data-moved)
		 */
		addToSave: function(element, attr){
			if(window.location.pathname && window.location.pathname.match(/review_content/g)){
				$(element).each(function(){
					if(attr){
						$(this).attr(attr, 'true');	
					}
					kriyaEditor.settings.undoStack.push(this);	
				});
			}
		},
		/**
		 * Function to trigger save in review content page
		 * @param action - action that modify the content
		 */
		trigerSave: function(action){
			if(window.location.pathname && window.location.pathname.match(/review_content/g)){
				kriyaEditor.init.addUndoLevel(action);
			}else if(window.location.pathname && window.location.pathname.match(/structure_content/g)){
				kriyaEditor.init.save('true');
			}
		},
		trackLog:function(){
			var changeNodes=$('#dataContainer').find('[data-changed],[data-old-class]');
			var changes=[];
			$(changeNodes).each(function(){
				changes.push(this.outerHTML);
			});
			$(changeNodes).removeAttr('data-changed');
			$(changeNodes).removeAttr('data-old-class');
			var parameters = {'log':changes,'logName':"errorlogs",'doi':kriya.config.content.doi,'customer':kriya.config.content.customer,'project':kriya.config.content.project};
			kriya.general.sendAPIRequest('logerrors',parameters,function(res){
				 console.log('Data Saved');
			})
		},
		openAddChapterPopup:function(){
			// open popup to add chapter. If chapter number already provided, then edit the chapter.
			var popUp = $('[data-component="chapter_edit"]');
			if ($('#contentDivNode .jrnlChapNumber').length == 0){
				$('[data-component="chapter_edit"] .chapterNumberContainer').text('');
				$('[data-component="chapter_edit"] .btn.edit-chapter').addClass('hidden');
				$('[data-component="chapter_edit"] .btn.add-chapter').removeClass('hidden');
			}else{
				$('[data-component="chapter_edit"] .chapterNumberContainer').text($('#contentDivNode .jrnlChapNumber').text());
				$('[data-component="chapter_edit"] .btn.add-chapter').addClass('hidden');
				$('[data-component="chapter_edit"] .btn.edit-chapter').removeClass('hidden');
			}
			eventHandler.components.general.openPopUp('', popUp);
		},
		addNode: function (nodeToAdd,nodeTextContainer,beforeRefNode,nodeAttributes) {
			// fx to add new node to the content
			if(beforeRefNode && $(beforeRefNode).length){
				var text = $(nodeTextContainer).text();
				$(beforeRefNode).before("<"+nodeToAdd+" "+ nodeAttributes.replace(/\,/," ") +">"+text+"</"+nodeToAdd+">");
				kriyaEditor.init.save();
				eventHandler.components.general.closePopUp('', $('[data-component="chapter_edit"]'));
				$(nodeTextContainer).text('');
			}
		},
		editChapter: function () {
			// update the chapter in content and save
			$('#contentDivNode .jrnlChapNumber').text($('[data-component="chapter_edit"] .chapterNumberContainer').text());
			kriyaEditor.init.save();
			eventHandler.components.general.closePopUp('', $('[data-component="chapter_edit"]'));
			$(nodeTextContainer).text('');
		},
	}
	return stylePalette;

})(stylePalette || {});
