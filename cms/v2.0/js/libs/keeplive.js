var keeplive = {
    callKeeplive: function(param,onSuccess,onError){
        $.ajax({
            type: 'POST',
            url: "/api/keeplive",
            data: param,
            success: function(data){
                if(onSuccess && typeof(onSuccess) == "function"){
                    onSuccess(data);
                }
            },
            error: function(error){
                if(onError && typeof(onError) == "function"){
                    onError(error);
                }
            }
        });
    },
    sendAPIRequest: function (funtionName, parameters, onSuccessFunc, onError, onProgress) {
        $.ajax({
            url : '/api/'+funtionName,
            type: 'POST',
            data: parameters,
            xhrFields: {
                onprogress: function(e) {
                    if(onProgress){
                        onProgress(e);
                    }
                }
            },
            error: function(res){
                if(onError){
                    onError(res);
                }
            },
            success: function(res){
                if(onSuccessFunc){
                    onSuccessFunc(res);
                }
            },
        });
    },
    handleVisibilityChange: function(){
        if (document.hidden) {
            $('#lock-screen').show();
			if (typeof(timeTicker) != "undefined"){
				timeTicker.pause();
			}
            //If the user switch the tab and the user didn't access the page 5 min then page is in active
			var idleTimeInterval = 300000;
			var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
			if (! /dashboard/.test(curPageName)){
				if (typeof(kriya) != 'undefined' && kriya.config && kriya.config.content && kriya.config.content.role && ! /publisher|author|^editor|copyeditor/i.test(kriya.config.content.role)){
					idleTimeInterval = 60000;
				}
			}
            setTimeout(function(){
                if($('#lock-screen:visible').length > 0){
                    keeplive.pageInactive();
                }
            }, idleTimeInterval);
        }else{
            if($('#lock-screen .errorPage:visible').length > 0 || $('#lock-screen .logoutTimer:visible').length > 0){
                return false;
            }
            $('#lock-screen').hide();
			if (typeof(timeTicker) != "undefined"){
				timeTicker.start();
			}
            //Check the log out status when focus the page
            keeplive.checkLogoutStatus();
            //If the page is in active when focus then activate page
            if(keeplive.getCurrPageStatus() == "in-active"){
                keeplive.pageActive();
            }else if(!keeplive.getCurrPageStatus() || !keeplive.getUserStatus()){
                keeplive.pageActive();
                keeplive.updateArticleStatus('available');
            }
        }
    },
    /**
     * Function to track the user
     */
    initTrack: function(){
        var that = this;
        
        //If the user is logged out then return false
        if(store.get('kriyaLogoutFlag') == true || $('#lock-screen .loginPage:visible').length > 0 || $('#lock-screen .errorPage:visible').length > 0){
            setTimeout(function(){
                that.initTrack();
            }, 300000);

            return false;
        }
        
        var parameters = null;
        var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
        if((curPageName == "review_content" || curPageName == "structure_content" || curPageName == "proof_review") && kriya && kriya.config){
            parameters = {
                'customer':kriya.config.content.customer,
                'project':kriya.config.content.project, 
                'doi':kriya.config.content.doi
            };
        }
        
        that.callKeeplive(parameters, function(res){
            if(res && res.redirectToPage){
                alert('You are logged in another browser.');
                store.set('kriyaLogoutFlag', true);
                store.remove('kriyaPageDetails');
                window.location = res.redirectToPage;
                return false;
            }else if(res && res.hold == "true"){
                if($('#lock-screen .errorPage').length == 0){
                    $('#lock-screen').show();
                    $('#lock-screen').append('<div class="row errorPage" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">This article is put on hold. You will not be able to work on this article any further.</div>');
                }
                return false;
            }else if(res && res.locked == "true"){
                if($('#lock-screen .errorPage').length == 0 && res.user && res.user.name && $('#contentDivNode').attr('data-readonly') != "true"){
                    $('#lock-screen').show();
                    $('#lock-screen').append('<div class="row errorPage" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">The article is LOCKED because <span style="text-decoration:underline;">' + res.user.name + '</span> is viewing it. To access your article please contact ' + res.user.name + ' and ask them to Log out. <br>You will need to sign in again once this is done.</div>');
                }
                return false;
            }else if(res && res.assigneTo == "false" && kriya &&  kriya.config && kriya.config.content && kriya.config.content.role == "typesetter" && kriya.config.content.role == "preeditor"){
                if($('#lock-screen .errorPage').length == 0 && $('#contentDivNode').attr('data-readonly') != "true"){
                    $('#lock-screen').show();
                    $('#lock-screen').append('<div class="row errorPage" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">Access denied as article is not assigned to you.</div>');
                }
                return false;
            } 
            
            setTimeout(function(){
                that.initTrack();
            }, 300000);
        },function(error){
            //Error function
            console.log(error);
        });
    },
    trackSession: function(){
        if($('#lock-screen .logoutTimer:visible').length > 0 || $('#lock-screen .loginPage:visible').length > 0 || keeplive.getCurrPageStatus() == "in-active"){
            return false;
        }
        console.log('track session called');
        var sessionExpTime = keeplive.getCookie('kAccExp'); //get the session expire time
        if(sessionExpTime){
            var currentTime = Date.now();
            var diffToExp   = sessionExpTime - currentTime;
            if(diffToExp < 0){
                //If the session time is expired
                alert('Your Session has expired.');
                store.set('kriyaLogoutFlag', true);
                store.remove('kriyaPageDetails');
                
                keeplive.checkLogoutStatus();
                
                return false;
            }else if(diffToExp <= 300000){
                //if the session expired time is less than 5 min
                //show the timer to reset

                if(keeplive.getUserStatus() == 'active'){
                    keeplive.callKeeplive({'reset': 'true'}, function(res){
                        if(res && res['exp-time']){
                            console.log('Reset session success.');
                        }else{
                            console.log('Reset session failed.');
                        }
                    },function(){
                        console.log('Reset session failed.');
                    });
                }else{
                    $('#lock-screen').show();
                    $('#lock-screen .inactivePage').addClass('hide');
                    $('#lock-screen .logoutTimer').removeClass('hide');
                    //Show the session expire timer to reset
                    diffToExp = diffToExp/1000;
                    if(diffToExp >= 60){
                        var timeoutSec = diffToExp;
                        timeoutSec = Math.ceil(timeoutSec);
                        while (timeoutSec >= 60) {
                            timeoutSec = timeoutSec-60;
                        }
                    }else{
                        var timeoutSec = diffToExp;
                    }

                    $('#lock-screen .logoutTimer .sessionOutTimer').text(timeoutSec);
                    keeplive.sessionTimer = setInterval(function(){
                        timeoutSec = parseInt(timeoutSec-1);
                        if(timeoutSec <= 0){
                            //if session reset button not clicked and session is expired then call checkLogoutStatus
                            clearInterval(keeplive.sessionTimer);
                            alert('Your Session has expired.');
                            $('#lock-screen .logoutTimer').addClass('hide');
                            store.set('kriyaLogoutFlag', true);
                            store.remove('kriyaPageDetails');
                            
                            keeplive.checkLogoutStatus();
                        }else{
                            $('#lock-screen .logoutTimer .sessionOutTimer').text(timeoutSec); //update the session expire second in UI
                        }
                    }, 1000);
                    return false;
                }
            }
        }
    },
    initScreenLock: function(){
        // Set the name of the hidden property and the change event for visibility
        var visibilityChange;
        if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
            visibilityChange = "visibilitychange";
        } else if (typeof document.msHidden !== "undefined") {
            visibilityChange = "msvisibilitychange";
        } else if (typeof document.webkitHidden !== "undefined") {
            visibilityChange = "webkitvisibilitychange";
        }

        // Warn if the browser doesn't support addEventListener or the Page Visibility API
        if(typeof document.addEventListener === "undefined" || typeof document.hidden === "undefined") {
            console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
        }else{
            var screenNode = $('<div id="lock-screen"/>');
			var idleTimeInterval = '15 minutes';
			var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
			if (! /dashboard/.test(curPageName)){
				if (typeof(kriya) != 'undefined' && kriya.config && kriya.config.content && kriya.config.content.role && ! /publisher|author|^editor|copyeditor/i.test(kriya.config.content.role)){
					idleTimeInterval = '1 minute';
				}
			}
            screenNode.append('<div class="row inactivePage hide" style="background-color: #f5b113;padding: 25px !important;text-align: center;font-size: 2rem;">You have been inactive for the last ' + idleTimeInterval + '. Move the mouse or press ESC key to activate the page.</div>');
            screenNode.append('<div class="row logoutTimer hide" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 2rem;color:white;"><span class="message">Your session will expire in <span class="sessionOutTimer">60</span> seconds. Please click Reset button to reactivate session.</span><br><span class="btn resetSession" style="font-size: 1rem;padding: 2px 10px 2px 10px;margin-right: 10px;">Reset</span><span class="btn cancelResetSession" style="font-size: 1rem;padding: 2px 10px 2px 10px;margin-right: 10px;">Logout</span></div>');
            screenNode.css({
                'background': '#f5f5f5',
                'height': '100%',
                'left': '0px',
                'overflow': 'auto',
                'position': 'fixed',
                'pointer-events': 'all',
                'padding': '10px',
                'top': '0px',
                'width': '100%',
                'z-index': '10000',
                'display': 'none'
            });
            $('body').append(screenNode);
            // Handle page visibility change   
            document.addEventListener(visibilityChange, this.handleVisibilityChange, false);
        }
    },
    /**
     * Function to redirect to the login screen
     */
    checkLogoutStatus: function(){
        // if the logout flag kriyaLogoutFlag and the page is has welcomeContainer and login via welcome login EX: ORCID, LOOP then show the welcomeContainer 
        // if the logout flag kriyaLogoutFlag and the welcomeContainer is not visibe then show the login screen
        if(store.get('kriyaLogoutFlag') == true){
            if((window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && window.location.search.match(/key=(.*)/gi) && $('#welcomeContainer').length > 0){
                $('#lock-screen').hide();
                $('#welcomeContainer').show();
            }else if($('#welcomeContainer:visible').length == 0){
                $('#lock-screen').show();
                $('#lock-screen .inactivePage').addClass('hide');
                if($('#lock-screen .loginPage').length == 0){
                    $('#lock-screen').append('<div class="row loginPage" style="background-color: #f5b113;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">Your session has expired. Click <span class="loginWin" style="color:#039be5;cursor:pointer;">here</span> to login.</div>');
                }else{
                    $('#lock-screen .loginPage').show();
                }
            }
        }
    },
    getCookie: function(cname){
        var name = cname + "=";
        //when we are decoding cookie values,if cookie values have any wrong data at that time code breaking.
        try{
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
        }catch(err){
            var parameters = {
                'log': "Cookie value for Uri malformed error : "+document.cookie
            };
            kriya.general.sendAPIRequest('logerrors',parameters,function(res){
                console.log('Data Saved');
            })
        }
        return "";
    },
    createStoreObj: function(){
        var pageDetails = store.get('kriyaPageDetails');
        var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
        //If the browser local storage has page details then parse the string
        /*if(pageDetailsString){
            pageDetails = JSON.parse(pageDetailsString);
        }*/
        if(!pageDetails){
            var pageDetails = {};
        }
        //If the page details doesn't have page name then create new page object
        if(!pageDetails[curPageName]){
            pageDetails[curPageName] = {};
        }
        
        var pageReqId = performance.timing.requestStart;
        //if the page is review content then get the doi as page id
        if((curPageName == "review_content" || curPageName == "structure_content" || curPageName == "proof_review") && kriya && kriya.config){
            pageReqId = kriya.config.content.doi;
        }

        //If the page details doesn't have tab details then create new tab object
        if(!pageDetails[curPageName][pageReqId]){
            pageDetails[curPageName][pageReqId] = {};
        }

        //store the page request time in obj
        pageDetails[curPageName][pageReqId].requestStart = performance.timing.requestStart;

        //pageDetailsString = JSON.stringify(pageDetails);
        store.set('kriyaPageDetails', pageDetails);
    },
    /**
     * Function to update the page status in in local storage
     * @param status - page status mandatory parameter
     */
    updatePageDetails: function(status){
        if(!status){
            return false;
        }

        this.createStoreObj();
        var pageDetails = store.get('kriyaPageDetails');
        var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
        
        
        var pageReqId = performance.timing.requestStart;
        //if the page is review content then get the doi as page id
        if((curPageName == "review_content" || curPageName == "structure_content" || curPageName == "proof_review") && kriya && kriya.config){
            pageReqId = kriya.config.content.doi;
        }

        //store the page request time in obj
        pageDetails[curPageName][pageReqId].requestStart = performance.timing.requestStart;

        //set the page status
        pageDetails[curPageName][pageReqId].status = status;

        //pageDetailsString = JSON.stringify(pageDetails);
        store.set('kriyaPageDetails', pageDetails);
    },
    /**
     * Function to get the user status
     * Read the page details from browser local storage
     * If the all tab is in-active then user is in-active else user is active
     * If the page details not found in local storage then return false
     */
    getUserStatus: function(){
        var returnVal = 'in-active';
        var pageDetails = store.get('kriyaPageDetails');
        if(pageDetails){
            //pageDetails = JSON.parse(pageDetails);
            $.each(pageDetails, function(pageName){
                if(returnVal != "active"){
                    $.each(this, function(pageId){
                        if(this.status == "active"){
                            returnVal = 'active';
                            return false;
                        }
                    });
                }else{
                    return false;
                }
            });
        }else{
            return false;
        }
        return returnVal;
    },
    /**
     * Function to update the status of the article is opened in browser or not
     */
    updateArticleStatus: function(status){
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "page-exist"){
            var doi = kriya.config.content.doi;
            this.createStoreObj();
            var pageDetails = store.get('kriyaPageDetails');
            var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
            if(pageDetails){
                //pageDetails = JSON.parse(pageDetails);
                if(pageDetails[curPageName] && pageDetails[curPageName][doi]){
                    pageDetails[curPageName][doi]['article_status'] = status;
                    
                    //var pageDetailsString = JSON.stringify(pageDetails);
                    store.set('kriyaPageDetails', pageDetails);
                }
            }
        }
    },
    /**
     * Function to get the current page status (active or in-active)
     */
    getCurrPageStatus: function(){
        var returnVal = false;
        var pageDetails = store.get('kriyaPageDetails');
        var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
        if(pageDetails){
            //pageDetails = JSON.parse(pageDetails);
            if(pageDetails[curPageName]){
                var pageReqId = performance.timing.requestStart;
                if((curPageName == "review_content" || curPageName == "structure_content" || curPageName == "proof_review") && kriya && kriya.config){
                    pageReqId = kriya.config.content.doi;   
                }
                if(pageDetails[curPageName][pageReqId]){
                    returnVal = pageDetails[curPageName][pageReqId].status;
                }
            }
        }
        return returnVal;
    },
    /**
     * Function to activate the page
     * @param pageLoad - Don't check and update the article job-log if page load is true
     */
    pageActive: function(pageLoad){
        //Don't in-activivate page if welcome screen is view in review content page
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/") && $('#welcomeContainer:visible').length > 0){
            return false;
        }
        
        //Don't update page active and inactive if logout flag is true
        //If error page is visible then don't update the status
        if(store.get('kriyaLogoutFlag') == true || $('#lock-screen .errorPage:visible').length > 0 || $('#lock-screen .loginPage:visible').length > 0){
            return false;
        }

        console.log('Activating page...');

        if((window.location.pathname != "/review_content/" || window.location.pathname != "/proof_review/") && window.location.pathname != "/structure_content/"){
            keeplive.updatePageDetails('active');    
        }else if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "page-exist"){
            keeplive.updatePageDetails('active');
        }
        
        //remove the lock screen
        $('#lock-screen .inactivePage').addClass('hide');
        $('#lock-screen').hide();

        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "logged-off" && kriya.config.pageStatus != "signed-off" && kriya.config.pageStatus != "page-exist" && !pageLoad && $('#contentContainer').attr('data-state') != "read-only"){
            keeplive.updatePageDetails('active');
            //Construct the parameters
            var parameters = {
                'customer': kriya.config.content.customer,
                'project' : kriya.config.content.project, 
                'doi'     : kriya.config.content.doi,
                'xpath'   : '//workflow/stage[status[.="in-progress"]]'
            };
            //Send the request to get the current stage xml
            keeplive.sendAPIRequest('getdatausingxpath', parameters, function(res){
                if(res){
                    var stageNode = $(res);
                    //get the last log node
                    var currLog = stageNode.find('job-logs log:last');
                    if(currLog){
                        var currStageStatus = currLog.find('status').text();
                        var useremail  =  currLog.find('useremail').text();
                        var userName   = currLog.find('username').text();
                        //if the log status is inactive then update the status open and hide the lock screen
                        //Else if current stage is open and email is not equal to current login email then reload the page
                        if(currStageStatus == "in-active"){
                            keeplive.updateArticleSatus('open', function(res){
								// to send end-time to log time
								setTimeout(function(){
									kriyaEditor.init.trackArticle();
								}, 120000);
                                console.log('page activate success..'); 
                            },function(res){
                                console.log('page activate failed..');
                            });
                        }else if(currStageStatus == "open" && useremail != $('#headerContainer .userProfile .username').attr('data-user-email')){
                            //location.reload();
                            $('#lock-screen').show();
                            if($('#lock-screen .errorPage').length == 0){
                                $('#lock-screen').append('<div class="row errorPage" style="background-color: #e74c3c;padding: 25px !important;text-align: center;font-size: 18px;line-height: 150%;color:white;">The article is LOCKED because <span style="text-decoration:underline;">' + userName + '</span> is viewing it. To access your article please contact ' + userName + ' and ask them to Log out. <br>You will need to sign in again once this is done.</div>');
                            }
                        }
                    }
                }
            },function(res){
                console.log('Error getting workflow');
            });
        }

        //If the anyone kriya page is active then update the user xml is active
        if(keeplive.getUserStatus() == 'active'){
            var currentDate = new Date();
            var parameters = {
                'xpath': '//last-login/@status',
                'data' : 'active'
            };
            keeplive.sendAPIRequest('updateuser', parameters, function(res){
                //console.log(res);
            }, function(err){
                console.log(err);
            });
        }

    },
    /**
     * Function to inactivate the page
     * @param preventLockScreen - if true lock screen will not show
     */
    pageInactive: function(preventLockScreen){
        //Don't update page active and inactive if logout flag is true
        //If error page is visible then don't updat ethe status
        if(store.get('kriyaLogoutFlag') == true || $('#lock-screen .errorPage:visible').length > 0 || $('#lock-screen .loginPage:visible').length > 0 || keeplive.getCurrPageStatus() == "in-active"){
            return false;
        }

        console.log('In activating page..');

        //Don't in-activivate page if welcome screen is view in review content page
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/") && $('#welcomeContainer:visible').length > 0){
            return false;
        }

        if(window.location.pathname != "/review_content/" && window.location.pathname != "/structure_content/" && window.location.pathname != "/proof_review/"){
            keeplive.updatePageDetails('in-active');
        }else if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "page-exist"){
            keeplive.updatePageDetails('in-active');
        }
        
        //show the lock screen
        if(preventLockScreen != true){
            $('#lock-screen .inactivePage').removeClass('hide');
            $('#lock-screen .loginPage').hide();
            $('#lock-screen').show();
            
        }

        /**
         * If the page is review content then update the status in-active in article xml and show the lock screen after successfully updated
         * Don't update article status if article is logged of or signed off 
         */
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "logged-off" && kriya.config.pageStatus != "signed-off" && kriya.config.pageStatus != "page-exist" && $('#contentContainer').attr('data-state') != "read-only"){
            this.updateArticleSatus('in-active', function(res){
                console.log('Page in active success.');
            },function(res){
                console.log('Page in active failed.');
            });
        }

        //If the all kriya page in active then update the user xml is in active
        if(keeplive.getUserStatus() == 'in-active'){
            var currentDate = new Date();
            var parameters  = {
                'xpath': '//last-login/@status',
                'data' : 'in-active'
            };
            keeplive.sendAPIRequest('updateuser', parameters, function(res){
                //console.log(res);
            }, function(err){
                console.log(err);
            });
        }
    },
    updateArticleSatus: function(status, onSuccess, onError){
        if(!status){
            return false;
        }
        //Update the article status only if the page is review content
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config){
            //Construct the parameters
            var parameters = {
                'customerName':kriya.config.content.customer,
                'projectName':kriya.config.content.project, 
                'doi':kriya.config.content.doi
            };
            var d = new Date();
            var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
            var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
            //Construct the request data
            parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status type="system">' + status + '</status></log></job-logs></stage>';
            if(status == "open"){
                parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log data-insert="true" data-doi="' + kriya.config.content.doi + '" data-project="' + kriya.config.content.project + '" data-customer="' + kriya.config.content.customer + '"><username>' + $('#headerContainer .userProfile .username').text() + '</username><useremail>' + $('#headerContainer .userProfile .username').attr('data-user-email') + '</useremail><start-date>' + currDate + '</start-date><start-time>' + currTime + '</start-time><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time><status type="system">' + status + '</status></log></job-logs></stage>';
            }
            keeplive.sendAPIRequest('updatedatausingxpath', parameters, onSuccess, onError);
        }
    },
    logoutKriya: function(){
        store.set('kriyaLogoutFlag', true);
        store.remove('kriyaPageDetails');
        window.location.href='/logout';
    }
}


$(document).ready(function(){
    keeplive.initScreenLock();
    var currentTime = Date.now();
    keeplive.initTrack();
    
    //Check the session time every 1 min 
    //and when the page load
    keeplive.trackSession();
    setInterval(function(){
        keeplive.trackSession();
    }, 60000);

    //Initialize the idle timer 15 min
	var idleTimeInterval = 900000;
	var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
	if (!/dashboard/.test(curPageName) && !/peer_review/.test(curPageName)){
		if (typeof(kriya) != 'undefined' && kriya.config && kriya.config.content && kriya.config.content.role && ! /publisher|author|^editor|copyeditor/i.test(kriya.config.content.role)){
			idleTimeInterval = 60000;
		}
	}
    $(document).idleTimer({ timeout: idleTimeInterval });

    //Idle timer page in active event
    $(document).on("idle.idleTimer", function (event, elem, obj) {
        //In-activate the page if the logoutTimer(session expire timer) not shown
        if($('#lock-screen .logoutTimer:visible').length == 0){
            keeplive.pageInactive();
        }
		if (typeof(timeTicker) != "undefined"){
			if (timeTicker.isRunning()){
				timeTicker.pause();
			}
		}
    });
    //Idle timer page active event
    $(document).on("active.idleTimer", function (event, elem, obj, e) {
        //Activate the page if the logoutTimer(session expire timer) not shown 
        if($('#lock-screen .logoutTimer:visible').length == 0){
            keeplive.pageActive();
        }
		if (typeof(timeTicker) != "undefined"){
			if (timeTicker.isPaused()){
				timeTicker.start();
			}
		}
    });


    /**
     * Local storage chnage event
     * If the article is opened multiple tab readirect the other tab to dashboard 
     * if article acces by the key then show the welcome screen
     */
    window.addEventListener("storage", function(){
        var pageDetails = store.get('kriyaPageDetails');
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && kriya && kriya.config && kriya.config.pageStatus != "logged-off" && kriya.config.pageStatus != "signed-off" && $('#contentContainer').attr('data-state') != "read-only"){
            if(store.get('kriyaLogoutFlag') != true && $('#lock-screen .errorPage:visible').length == 0 && $('#lock-screen .loginPage:visible').length == 0 && kriya.config.pageStatus != "page-exist"){
                var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
                if(pageDetails){
                    if(pageDetails[curPageName] && pageDetails[curPageName][kriya.config.content.doi]){
                        var pageReqId = performance.timing.requestStart;
                        var articleActiveID = pageDetails[curPageName][kriya.config.content.doi].requestStart;
                        if(articleActiveID && pageReqId && pageReqId != articleActiveID){
                            kriya.config.pageStatus = "page-exist";
                            if((window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/") && window.location.search.match(/key=(.*)/gi) && $('#welcomeContainer').length > 0){
                                $('#lock-screen').hide();
                                $('#welcomeContainer').show();
                            }else if($('#welcomeContainer:visible').length == 0){
                                window.location = "/dashboard";
                            }
                        }
                    }
                }
            }
        }

    }, false);
    
    /** 
     * onbeforeunload event also trigger when page reload So
     * If the page is referesh and page is review content and update the article status open
     * After the page referesh window.performance.navigation.type will be 1
     **/
    if(window.performance.navigation.type == 1){
        keeplive.updateArticleStatus('available');
        setTimeout(function () {
            keeplive.pageActive();
        }, 30000);
    }else if(window.performance.navigation.type == 0 || window.performance.navigation.type == 2){ //when page newly opened or duplicating tab
        //Check the article is already opened in another tab
        //if true then don't update the page status
        /*if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/") && kriya && kriya.config){
            var doi = kriya.config.content.doi;
            var pageDetails = store.get('kriyaPageDetails');
            var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
            if(pageDetails){
                //pageDetails = JSON.parse(pageDetails);
                if(pageDetails[curPageName] && pageDetails[curPageName][doi] && pageDetails[curPageName][doi]['article_status'] == "available"){
                    kriya.config.pageStatus = "page-exist";
                    $('body').append('<div class="messageDiv"><div class="message">You have already opened this article in another window.<br> Please close this window.</div></div>');
                    console.log('Article was already opened..');
                }
            }
        }*/
        
        //when review content page load using key login then remove kriyaLogoutFlag
        if((window.location.pathname == "/review_content/" || window.location.pathname == "/structure_content/" || window.location.pathname == "/proof_review/") && window.location.search.match(/key=(.*)/gi)){
            store.remove('kriyaLogoutFlag');
        }

        keeplive.updateArticleStatus('available');
        setTimeout(function () {
            keeplive.pageActive(true);
        }, 30000);
    }

    if(window.performance.navigation.type == 2 && (window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/")){
        location.reload();
    }


    window.onbeforeunload = function(){
        //If the page is review content
        /*if(window.location.pathname == "/review_content/" && kriya && kriya.config && kriya.config.pageStatus != "logged-off" && kriya.config.pageStatus != "signed-off"){
            //update article status in active when page unload 
            keeplive.updateArticleSatus('in-active', function(res){
                console.log('page activate success..');
            },function(res){
                console.log('page activate failed..');
            });
        }*/
		// to prevent closing article while content to be saved is in save queue - aravind
        if(typeof(kriya) != 'undefined' && kriya.config && kriya.config.saveQueue && kriya.config.saveQueue.length > 0 && kriya.config.pageStatus != "signed-off"){
            return false;
        }
        keeplive.pageInactive(true);
        keeplive.updateArticleStatus('unavailable');
    }

    $('body').on({
        click: function(evt){
            keeplive.callKeeplive({'reset': 'true'}, function(res){
                if(res && res['exp-time']){
                    $('#lock-screen .logoutTimer').addClass('hide');
                    $('#lock-screen').hide();
                    clearInterval(keeplive.sessionTimer);
                }else{
                    console.log('Session reset failed');
                }
            },function(){
                console.log('Session reset failed');
            });
        }
    }, '#lock-screen .resetSession');

    $('body').on({
        click: function(evt){
            store.set('kriyaLogoutFlag', true);
            store.remove('kriyaPageDetails');
            
            if((window.location.pathname == "/review_content/" || window.location.pathname == "/proof_review/") && window.location.search.match(/key=(.*)/gi) && $('#welcomeContainer').length > 0){
                $('#lock-screen').hide();
                $('#welcomeContainer').show();
            }else if($('#welcomeContainer:visible').length == 0){
                window.location.href='/logout';
            }
        }
    }, '#lock-screen .cancelResetSession');

    $('body').on({
        click: function(evt){
            if (!keeplive.loginWin || keeplive.loginWin.closed){
                keeplive.loginWin = window.open('/logout', 'Login window', 'height=500, width=1000');
            }else{
                keeplive.loginWin.focus();
            }
            
        }
    }, '#lock-screen .loginWin');
    
});

window.addEventListener("offline", function(e) {
    if($('#offline-screen').length == 0){
        $('body').append('<div id="offline-screen" style="background: rgba(245, 245, 245, 0.61);height: 100%;left: 0px;overflow: auto;position: fixed;pointer-events: null;padding: 10px;top: 0px;width: 100%;z-index: 10000;display:none;"><div class="row" style="width: 60%;background-color: #f5b113;padding: 20px !important;text-align: center;font-size: 2rem;margin-left: 20% !important;margin-top: 20% !important;">Your device lost its internet connection.</div></div>');
    }
    $('#offline-screen').fadeIn();
  }, false);
  
window.addEventListener("online", function(e) {
    //Trigger save if saveQueue has list when system comes online
    var curPageName = window.location.pathname.replace(/^\/|\/$/g, '');
    if((curPageName == "review_content" || curPageName == "proof_review")){
        if(kriya.config.saveQueue.length > 0){
            kriyaEditor.init.saveQueueData();
        }
    }
    $('#offline-screen').fadeOut();
  }, false);