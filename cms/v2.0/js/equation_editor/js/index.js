(function () {
            'use strict';

            var runLocal = (window.location.search.indexOf("runLocal", 0) > 0),
                runNotCodeMirror = (window.location.search.indexOf("runNotCodeMirror", 0) > 0),
                runNotMathJax = (window.location.search.indexOf("runNotMathJax", 0) > 0),
                runNotColorPicker = (window.location.search.indexOf("runNotColorPicker", 0) > 0),
                runNotCanvg = (window.location.search.indexOf("runNotCanvg", 0) > 0),
                vmeURL,
                vmeRunParam;

            /*jQuery Javascript*/
            document.write("<script type=\"text\/javascript\" src=\"\/js\/libs\/jquery1.8.0\/jquery.min.js\"><\/script>");
            document.write("<script type=\"text\/javascript\" src=\"\/js\/libs\/jquery.url.js\"><\/script>");

            /*jQuery Easyui Javascript*/
            document.write("<script type=\"text\/javascript\" src=\"\/js\/equation_editor\/js\/jquery-easyui\/jquery.easyui.min.js\"><\/script>");
            
            /*jQuery Colorpicker Javascript*/
            if (!runNotColorPicker) {
                document.write("<link rel=\"stylesheet\" id=\"colorpickerCSSblack\" type=\"text\/css\" href=\"\/js\/equation_editor\/js\/jquery-colorpicker\/css\/colorpicker.css\" disabled=\"true\" >");
                document.write("<link rel=\"stylesheet\" id=\"colorpickerCSSgray\" type=\"text\/css\" href=\"\/js\/equation_editor\/js\/jquery-colorpicker\/css\/colorpicker_gray.css\" disabled=\"true\" >");
                document.write("<script type=\"text\/javascript\" src=\"\/js\/equation_editor\/js\/jquery-colorpicker\/js\/colorpicker.js\"><\/script>");
            }

            /*MathJax    Javascript */
            if (!runNotMathJax) {
                //vmeURL = window.location.protocol + "//" + window.location.host + window.location.pathname.substring(0, window.location.pathname.lastIndexOf("/") + 1);
                vmeURL = window.location.protocol + "//" + window.location.host;
                document.write("<script type=\"text\/javascript\" id=\"MathJaxScript\" src=\"\/js\/libs\/MathJax-2.7.0\/MathJax.js?config=" + vmeURL + "\/js\/libs\/mathjax-MathEditorExtend\/x-mathjax-config.js\"><\/script>");
            }
            /*Codemirror Javascript and css*/
            if (!runNotCodeMirror) {
                document.write("<link rel=\"stylesheet\" type=\"text\/css\" href=\"\/js\/equation_editor\/js\/codemirror\/lib\/codemirror.css\">");
                document.write("<link rel=\"stylesheet\" type=\"text\/css\" href=\"\/js\/equation_editor\/js\/codemirror\/theme\/twilight.css\">");
                document.write("<script src=\"\/js\/equation_editor\/js\/codemirror\/lib\/codemirror.js\"><\/script>");
            }
            /*Canvg Javascript*/
            if (!runNotCanvg) {
                document.write("<script type=\"text\/javascript\" src=\"\/js\/equation_editor\/js\/canvg\/rgbcolor.js\"><\/script>");
                document.write("<script type=\"text\/javascript\" src=\"\/js\/equation_editor\/js\/canvg\/StackBlur.js\"><\/script>");
                document.write("<script type=\"text\/javascript\" src=\"\/js\/equation_editor\/js\/canvg\/canvg.js\"><\/script>");
            }

            /*VisualMathEditor Javascript*/
            vmeRunParam = "";
            if (runLocal) {
                vmeRunParam += "runLocal=true";
            }
            if (runNotCodeMirror) {
                if (vmeRunParam.length > 0) { vmeRunParam += "&"; }
                vmeRunParam += "runNotCodeMirror=true";
            }
            if (runNotMathJax) {
                if (vmeRunParam.length > 0) { vmeRunParam += "&"; }
                vmeRunParam += "runNotMathJax=true";
            }
            if (runNotColorPicker) {
                if (vmeRunParam.length > 0) { vmeRunParam += "&"; }
                vmeRunParam += "runNotColorPicker=true";
            }
            if (runNotCanvg) {
                if (vmeRunParam.length > 0) { vmeRunParam += "&"; }
                vmeRunParam += "runNotCanvg=true";
            }

            if (vmeRunParam.length > 0) { vmeRunParam = "?" + vmeRunParam; }
            document.write("<script type=\"text\/javascript\" id=\"vmeScript\" src=\"\/js\/equation_editor\/js\/VisualMathEditor.js" + vmeRunParam + "\"><\/script>");
        }());