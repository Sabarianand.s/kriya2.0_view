/**
 * custom functions for Book View
 */
(function (eventHandler) {
    eventHandler.flatplan = {
		action: {
			/**
			*	Add new book to a customer
			*	Get information from the provided form and construct a XML
			*	Send to API to add it to the Project List and create metaXML
			**/
			addBook: function(param, targetNode){
				var projectExisting = $('.projectlist li').map(function() {return $(this).attr('data-project');}).get().filter(v => v);
				projectExisting = projectExisting ? projectExisting : [];
				var params = {};
				var projectName = '';
				$('.save-notice').removeClass('hide');

				// To validate the Add new Book Modal
				var firstEmptyField = false;
				$('#addProj input[data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (!firstEmptyField) {
								$(this).focus();
								firstEmptyField = true;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (firstEmptyField) {
					$('.save-notice').text('Unable to create Book');
					return false;
				}
				// create project name with some random number
				var projectName = $('#manuscriptContent #customerVal').text() + '.' + (Math.floor(Math.random()*90000) + 10000);
				while ($('.projectList .project[data-project="' + projectName + '"]').length > 0){
					projectName = $('#manuscriptContent #customerVal').text() + '.' + (Math.floor(Math.random()*90000) + 10000);
				}
				$('#journal-id').val(projectName);
				var empty = false;
				params.XML='<nodes>';
				$('#addProj [data-form-details]:not(.hide)').each(function(){
					if ($(this).closest('.hide').length > 0){
						return true;
					}
					var value = $(this).val();
					// if(!value && !($(this).parent().hasClass('hidden') || $(this).closest('div.row').hasClass('hide'))) {
					// 	// 	showMessage({ 'text': eventHandler.messageCode.errorCode[502][7], 'type': 'error' }); empty=true;
					// }
					var formDetail = $(this).attr('data-form-details').split('||');
					var attr = ($(this).attr('data-form-details-attr') && formDetail[3]) ? eventHandler.flatplan.components.attr($(this).attr('data-form-details-attr').split('||')) : '';
					if (attr != ""){
						attr = ' ' + attr;
					}
					if ($(this).closest('[data-parent-name]').length > 0){
						value = "";
						$(this).closest('[data-parent-name]').find('[data-name]').each(function(){
							if ($(this).val() != "" && $(this).val() != null){
								var tagName = $(this).attr('data-name');
								value += '<' + tagName + '>' + $(this).val() + '</' + tagName + '>';
							}
						})
						var tagName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						value += '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					}else{
						var tagName = $(this).attr('data-name');
						value = '<' + tagName + attr + '>' + value + '</' + tagName + '>';
					}
					var currentElement = $(this)
					if ($(this).attr('data-siblings-name')&& $(this).attr('data-siblings-name')!=''){
						var siblings = $(this).attr('data-siblings-name').split(',')
						//var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
						siblings.forEach(function(sibling){
							var siblingTagName = sibling
							var siblingValue = $(currentElement).find('option[value="'+currentElement.val()+'"]').attr(sibling)
							if(siblingTagName && siblingValue){
								value += '<' + siblingTagName + attr + '>' + siblingValue + '</' + siblingTagName + '>';
							}	
						})
					}
					var innerTag = $(this).attr('data-inner-tag') ? $(this).attr('data-inner-tag') : '';
					if((value) && (value!=null) && (formDetail.length >= 3)){
						params.XML += '<node><id>' + $(this).attr('id') + '</id><section>' + formDetail[0] + '</section><xpath>' + formDetail[1] + '</xpath><tagname>'+formDetail[2]+'</tagname><value>'+value+'</value></node>';
					}
				});
				if (empty) return false;
				params.XML += '<node><id>project-name</id><section>//container-xml</section><xpath>//meta</xpath><tagname>project-name</tagname><value><project-name>'+ projectName +'</project-name></value></node>'
				params.XML += '</nodes>';
				params.addProj = {
					customer : $('#manuscriptContent #customerVal').text(),
					project : projectName,
					projectFullName : $('#bookTitle').val(),
					projectType : 'book'
				}
				// if the project name already Exists
				if(projectExisting.indexOf(params.addProj.project.trim().toLowerCase().replace(' ','_')) > -1){
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][3], 'type': 'error' });
					$('.save-notice').text('Unable to create Book');
					return false;
				}
				if(params.addProj.project && params.addProj.customer){
					$('.la-container').fadeIn();
					$.ajax({
						type:"POST",
						url:"/api/addproject",
						data:params.addProj,
						success:function(data){
							$.ajax({
								type:"POST",
								url:"/api/bookmeta",
								data:params,
								success:function(data){
									if(data.statusCode==200){
										$('[type="text"][data-form-details],[type="text"][data-name]').val('');
										$('select[data-form-details] option[value=""]').attr('selected', true);
										$('.dropdown-menu .custom-select').removeClass('custom-select');
										$('#addProj .dynamic').remove();
										//eventHandler.menu.flatplan.appendContent = 0;
										$('#addProj').modal('hide');
										$('.save-notice').text('Book has been added');
										$('.la-container').fadeOut();
										$('#manuscriptContent #filterCustomer .filter-list.active').trigger('click');
										/*eventHandler.general.actions.getProjectList($('#projectVal').attr('data-customer'),$('div.filter-list.customer.active'))
										.then(function(){
											$('.la-container').fadeOut();
										})
										.catch(function(err){
											$('.la-container').fadeOut();
											console.log('error getProjectList')
										})*/
									}
									else{
										$('.la-container').fadeOut();
										$('.save-notice').text('Unable to create Book');
										//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
									}
								},
								error:function(err){
									$('.la-container').fadeOut();
									$('.save-notice').text('Unable to create Book');
									//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
								}
							});
						},
						error: function (data) {
							$('.la-container').fadeOut();
							$('.save-notice').text('Unable to create Book');
							//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
						},
					});
				}
				else{
					$('.la-container').fadeOut();
					$('.save-notice').text('Unable to create Book');
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][4], 'type': 'error' });
				}
			},
			/**
			*	Save Book Info details back to metaXML
			*	Get information from the provided form and construct a JSON
			*	Send to API to modify metaXML
			**/
			saveProj: function(param, targetNode){
				var metaUpdate = false;
				var modifiedArray = [];
				
				// For validating the Book info form
				var firstEmptyField = false;
				$('#addProj input[data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (!firstEmptyField) {
								$(this).focus();
								firstEmptyField = true;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (firstEmptyField) {
					$('.save-notice').text('Unable to Save Details');
					return false;
				}

				$('#addProj [data-form-details]:not(.hide)').each(function(){
					if ($(this).closest('.hide').length > 0){
						return true;
					}
					var myMeta = $('#manuscriptsData').data('binder')['container-xml'].meta;
					var key = $(this).attr('data-name');
					if ($(this).closest('[data-parent-name]').length > 0){
						var tagName = $(this).closest('[data-parent-name]').attr('data-tag-name');
						var parentName = $(this).closest('[data-parent-name]').attr('data-parent-name');
						if (modifiedArray.indexOf(tagName) < 0){
							modifiedArray.push(tagName);
							myMeta[tagName] = {};
							myMeta[tagName][parentName] = [];
						}
						var newObj = {}
						var attributes = $(this).closest('[data-parent-name]').attr('data-form-details-attr').split('||');
						if (attributes.length > 0){
							newObj._attributes = {}
							$(attributes).each(function(i,e){
								v = e.split('|');
								if (v.length == 2){
									newObj._attributes[v[0]] = v[1];
								}
							});
						}
						$(this).closest('[data-parent-name]').find('[data-name]').each(function(){
							if ($(this).val() != "" && $(this).val() != null){
								var value = $(this).attr('data-name');
								newObj[value] = {};
								newObj[value]._text = $(this).val();
							}
						})
						myMeta[tagName][parentName].push(newObj);
					}
					else if($('#addProj [data-form-details][data-name="' + key + '"]').length > 1){
						if (modifiedArray.indexOf(key) < 0){
							modifiedArray.push(key);
							myMeta[key] = [];
						}
						if ($(this).val() != '' && $(this).val() != null){
							var newObj = {}
							newObj._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr').split('||');
							if (attributes.length > 0){
								newObj._attributes = {}
								$(attributes).each(function(i,e){
									v = e.split('|');
									if (v.length == 2){
										newObj._attributes[v[0]] = v[1];
									}
								});
							}
							myMeta[key].push(newObj);
						}
					}
					else{
						if ($(this).val() == '' || $(this).val() == null){
							if (myMeta[key]){
								delete(myMeta[key])
							}
						}
						else if (myMeta[key]){
							myMeta[key]._text = $(this).val();
						}
						else{
							myMeta[key] = {};
							myMeta[key]._text = $(this).val();
							var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
							if (attributes && attributes.length > 0){
								myMeta[key]._attributes = {}
								$(attributes).each(function(i,e){
									v = e.split('|');
									if (v.length == 2){
										myMeta[key]._attributes[v[0]] = v[1];
									}
								});
							}
						}
					}
					//
					var current = $(this)
					if ($(this).attr('data-siblings-name')&& $(this).attr('data-siblings-name')!=''){
						var siblings = $(this).attr('data-siblings-name').split(',')
						var attributes = $(this).attr('data-form-details-attr')?$(this).attr('data-form-details-attr').split('||'):'';
						siblings.forEach(function(sibling){
							//console.log(sibling)
							if($(current).find('option[value="'+current.val()+'"]').length >0 && $(current).find('option[value="'+current.val()+'"]').attr(sibling) && $(current).find('option[value="'+current.val()+'"]').attr(sibling)!=''){
								myMeta[sibling] = {};
								myMeta[sibling]._text = $(current).find('option[value="'+current.val()+'"]').attr(sibling);
								if (attributes && attributes.length > 0){
									myMeta[sibling]._attributes = {}
									$(attributes).each(function(i,e){
										v = e.split('|');
										if (v.length == 2){
											myMeta[sibling]._attributes[v[0]] = v[1];
										}
									});
								}
							}
							
						})
					}
				});
				eventHandler.flatplan.action.saveData();
				$('#addProj').modal('hide');
			},
			/**
			*	Add new chapter or any other part of a book
			*	Send uploaded Zip to AddJob API
			**/
			addJob: function(param, targetNode){
				var parameters = new FormData();
				var client = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var doiSuffix = parseInt($('#chapterNumber').val());
				doiSuffix = ("000"+doiSuffix).slice(-3)
				var doi = project + '.' + doiSuffix;
				// check if already doi exists
				/*if ($('.tocBodyContainer .sectionDataChild[data-doi="' + doi + '"]').length > 0){
					// show notification
					return false;
				}*/
				// var typeOfJob = 'chapter';//$('.tab-pane.fade.active.in').attr('id');
				var typeOfJob = $('.tab-pane.active').attr('id');
				var validated = true;
				var uploadFileId = '#' + typeOfJob + ' #chapFile';
				// Form validation
				$('#addChapter #' + typeOfJob + ' [data-validate="true"]').each(function () {
					if (!$(this).closest('.authored.hide,.contributed.hide').length) {
						if ($(this).val().trim() == '') {
							$(this).addClass('is-invalid');
							if (validated) {
								$(this).focus();
								validated = false;
							}
						} else {
							$(this).removeClass('is-invalid');
						}
					}
				})
				if (!validated) {
					return false;
				}
				if (!($(uploadFileId)[0] && $(uploadFileId)[0].files && $(uploadFileId)[0].files.length > 0 && ($(uploadFileId).attr('accept') == '.zip' && /zip/.test($(uploadFileId)[0].files[0].type)) || ($(uploadFileId).attr('accept') == '.epub' && $(uploadFileId)[0].files[0].type == "application/epub+zip"))) {
						$(uploadFileId).addClass('is-invalid');
						return false;
				}
				$('.la-container').fadeIn();
				var parameters = new FormData();
				parameters.append('manuscript', $(uploadFileId)[0].files[0]);
				if (typeOfJob == 'epubUpload') {
					// do not appened any parameter
					type = 'epub';
				}else if(typeOfJob == 'chapter'){
					chapterTitle = $('#chapterTitle').val();
					parameters.append('articleTitle', chapterTitle);
					chapterNumber = parseInt($('#chapterNumber').val()).toString();
					parameters.append('articleNumber', chapterNumber);
					type = 'chapter'
					parameters.append('articleType', type);
				}else{
					chapterTitle = 'Back Matter';
					chapterNumber ='';
					parameters.append('articleTitle', chapterTitle);
					type = $('#' + typeOfJob + ' #backType').val();
					parameters.append('articleType', type);
					doi = project + '.' + type;
				}
				
				parameters.append('customer', client);
				parameters.append('project', project);
				parameters.append('doi', doi);
				
				parameterToAddJobBook = {
					customer : client,
					project : project,
					doi : doi,
					chapterTitle : chapterTitle,
					chapterNumber : chapterNumber,
					type : type
				}
				$.ajax({
					type: 'POST',
					url: '/api/addjob',
					data: parameters,
					contentType: false,
					processData: false,
					success: function (response) {
						if(response){
							if (type == 'epub'){
								/* var res = $(response);
								var chapterItems = res.find('manifest item[media-type="application/xhtml+xml"]');
								if (chapterItems.length == 0){
									
								}else{
									$(chapterItems).each(function(){
										var tr = $('<tr/>');
										tr.append('<td>' + $(this).attr('id') + '</td>');
										tr.append('<td>' + $(this).attr('href') + '</td>');
										tr.append('<td><span class="btn-primary btn-sm btn-info" data-channel="menu" data-topic="bookview" data-event="click" data-message="{\'funcToCall\': \'addEpubJob\'}">Add Job</span></td>');
										$('#addEpubJobs .modal-body table tbody').append(tr);
									});
									$('#addChapter').modal('hide');
									$('#addEpubJobs').modal('show');
								} */
								if (response.content && response.content.length) {
									$('#manuscriptsData').data('binder')['container-xml'].contents = response;
									eventHandler.flatplan.action.saveData({ 'onsuccess': "$('#filterProject .active').trigger('click')" });
									$('#addChapter').modal('hide');
								}
							}
							else{
								var newCahpter = {};
								newCahpter.title = {'_text': chapterTitle};
								newCahpter._attributes = {"section" : "Chapters", "type" : "manuscript", "chapterNumber" : chapterNumber, "id" : doi, "id-type": "doi", "grouptype" : "body", "siteName": "kriya2.0"}
								$('#manuscriptsData').data('binder')['container-xml']['contents'].content.push(newCahpter);
								eventHandler.flatplan.action.saveData({'onsuccess': "$('#filterProject .active').trigger('click')"});
								$('#addChapter').modal('hide');
							}
							$('.la-container').css('display', 'none');
						}else{
							//showMessage({ 'text':  eventHandler.messageCode.errorCode[501][4] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' });
							$('.la-container').css('display', 'none');
							$('#addChapter').modal('hide');
						}
					},
					error: function (err) {
						$('#addChapter').modal('hide');
						console.log(err);
						$('.la-container').css('display', 'none');
					}
				})
			},
			/**
			*	Get project list/issue list of a customer
			*	Construct html list based on customer type (book or journal)
			*	Either list of Books or list of issues available in a journal
			**/
			getBooks: function(param, targetNode){
				var url = '/api/projects?customerName=' + param.customer;
				if ($('.save-notice').text().match(/All changes saved/gi)) {
					$('.save-notice').addClass('hide');
				}
				$.ajax({
					url: url,
					type: 'GET',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (project) {
						$('.la-container').css('display', 'none');
						if (typeof (project) == 'undefined' || project == null) {
							return false;
						}
						if (project['project'] == undefined) {
							return false;
						}
						var pData = project['project'];
						if (pData.constructor.toString().indexOf("Array") == -1) {
							pData = [pData];
						}
						var pHTML = $('<ul class="projectList" style="margin-left:10%;margin-right:10%">');
						var pLen = pData.length;
						var cName = param.customer;
						var data = {};
						data.customer = param.customer;
						data.projects = pData;
						// var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
						// $('#manuscriptsDataContent').html(pagefn(data));
						// return;
						var count = 0;
						var extra = {};
						var projectHTML = '';
						for (var pIndex = 0; pIndex < pLen; pIndex++) {
							projectHTML += '<div class="filter-list" value="' + pData[pIndex].name + '" data-channel="flatplan" data-topic="action" data-event="click" data-message="{\'funcToCall\': \'getMetaXML\',\'param\':{\'type\': \'book\'}}" data-customer="' + param.customer + '" data-project="' + pData[pIndex].name + '" data-file="' + pData[pIndex].name + '.xml">' + pData[pIndex].fullName + '</div>'
							$('.add').removeAttr("data-target");
							$('.add').removeClass("hide");
							var urlToFetchData = '/api/getissuedata?client=' + param.customer + '&jrnlName=' + pData[pIndex].name + '&fileName=' + pData[pIndex].name + '.xml&process=getIssueXML';
							$.ajax({
								url: urlToFetchData,
								type: 'GET',
								contentType: "application/json; charset=utf-8",
								dataType: "json",
								success: function (issueResponse) {
									count++;
									if (typeof (issueResponse) == "undefined" || issueResponse == null) {
										$('.la-container').fadeOut();
									}else if (issueResponse.list.response.data) {
										var containerXml = issueResponse.list.response.data.issue['container-xml'];
										if (containerXml.contents.content){
											var content = containerXml.contents.content;
										}else{
											var content = [];
										}
										if (content.constructor.toString().indexOf('Array') < 0){
											content = [content];
										}
										var chapterCount = 0;
										for (eachContent of content) {
											if (eachContent._attributes.type == 'manuscript') {
												chapterCount++;
											}
										}
										var isbnHb = 'NA';
										var isbnPb = 'NA';
										if( !containerXml.meta) {
											
										}else if(!containerXml.meta['project-name'] || !containerXml.meta['project-name']._text) {
											
										}else{
											var journalID = containerXml.meta['project-name']._text;
											if (containerXml.meta && containerXml.meta['isbn']){
												var isbnDetails = [];
												if (containerXml.meta['isbn'].constructor.toString().indexOf("Array") == -1) {
													isbnDetails = [containerXml.meta['isbn']]
												}else{
													isbnDetails = containerXml.meta['isbn']
												}
												for (var is = 0, isl = isbnDetails.length; is < isl; is++){
													var currIsbn = isbnDetails[is]
													if (currIsbn._attributes && currIsbn._attributes['pub-type'] && currIsbn._attributes['pub-type'] == 'pb') {
														isbnPb = currIsbn._text;
													}
													if (currIsbn._attributes && currIsbn._attributes['pub-type'] && currIsbn._attributes['pub-type'] == 'eisbn') {
														isbnHb = currIsbn._text;
													}
												}
											}
											var authors = '';
											if (containerXml.meta && containerXml.meta['contrib-group'] && containerXml.meta['contrib-group'].contrib) {
												var authorArray = containerXml.meta['contrib-group'].contrib;
												if (containerXml.meta['contrib-group'].contrib.constructor.toString().indexOf("Array") == -1) {
													authorArray = [containerXml.meta['contrib-group'].contrib]
												}
												authorArray.forEach(function (author, authorIndex) {
													if (author.name && author.name._text) {
														if (authorIndex == authorArray.length - 1) {
															authors += author.name._text;
														} else {
															authors += author.name._text + ', ';
														}
													} else {
														authors += '';
													}
												})
											}
											extra[journalID] = {
												'chapterCount': chapterCount.toString(),
												'pageCount': (containerXml.meta['page-count'] && containerXml.meta['page-count']._text && containerXml.meta['page-count']._text != 'null') ? containerXml.meta['page-count']._text : '0',
												'isbnHb': isbnHb,
												'isbnPb': isbnPb,
												'authors': authors
											}
										}
									}
									if (count == pLen) {
										data.additionalDetails = extra;
										var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
										$('#manuscriptsDataContent').html(pagefn(data));
									}
								},
								error: function (error) {
									console.log(error);
									if (count == pLen) {
										data.additionalDetails = extra;
										var pagefn = doT.template(document.getElementById('bookTemplate').innerHTML, undefined, undefined);
										$('#manuscriptsDataContent').html(pagefn(data));
									}
								}
							});
						}
						$('#manuscriptContent #filterProject').html(projectHTML);
					},
					error: function (res) {

					}
				});
			},
			/**
			*	Get meta/issue xml of a book or an issue
			*	Construct TOC based on the template available for that customer
			**/
			getMetaXML: function(param, targetNode){
				var url = '/api/getissuedata?client=' + targetNode.attr('data-customer') + '&jrnlName=' + targetNode.attr('data-project') + '&fileName=' + targetNode.attr('data-file') + '&process=getIssueXML';
				$('.showBreadcrumb[data-type="all"]').css('display', 'inline-block');
				var param = {};
				$('.save-notice').addClass('hide');
				param.customer = $('#manuscriptContent #customerVal').text();
				$('#filterProject .filter-list').removeClass('active');
				if ($('.booksList [data-project]').length > 0){
					//eventHandler.filters.populateNavigater(param);
					$('#filterProject .filter-list:not([value])').remove();
					$('#filterProject .filter-list').removeAttr('onclick');
					$('#filterProject .filter-list').attr('data-channel', "flatplan").attr('data-topic', "action").attr('data-event', "click").attr('data-message', "{'funcToCall': 'getMetaXML','param':{'type': 'book'}}");
					$('#filterProject .filter-list').each(function(){
						$(this).attr('data-customer', targetNode.attr('data-customer')).attr('data-project', $(this).attr('value')).attr('data-file', $('.booksList [data-project="' + $(this).attr('value') + '"]').attr('data-file'));
					})
				}
				$('#filterProject .filter-list[value="' + targetNode.attr('data-project') + '"]').addClass('active');
				$('#manuscriptContent #projectVal').text((targetNode.find('.msTitle b').length) ? targetNode.find('.msTitle b').text() : targetNode.text());
				$('.la-container').css('display', '');
				$.ajax({
					url: url,
					type: 'GET',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (issueResponse) {
						if (typeof(issueResponse) == "undefined" || issueResponse == null){
							$('.la-container').fadeOut();
							return false;
						}
						if (typeof(issueResponse.list) == "undefined"){
							$('.la-container').fadeOut();
							return false;
						}
						if (issueResponse.list && issueResponse.list.response && issueResponse.list.response.status && issueResponse.list.response.status.code && issueResponse.list.response.status.code._text && issueResponse.list.response.status.code._text == '500'){
							$('.la-container').fadeOut();
							return false;
						}
						$('#addBookBtn').addClass('hidden');
						$('#addJobBtn').removeClass('hidden');
						var issueData = issueResponse.list.response;
						var configData = JSON.parse(JSON.stringify(issueData.data.issue.config));
						var proofConfigDetails;
						if(issueData.data.issue["proof-details"]){
							proofConfigDetails = issueData.data.issue["proof-details"];
							delete(issueData.data.issue["proof-details"]);
						}
						delete(issueData.data.issue.config);
						$('#manuscriptsData').data('binder', issueData.data.issue);
						$('#manuscriptsData').data('config', configData);
						$('#manuscriptsData').data('profConfigDetails', proofConfigDetails);
						issueData = issueData.data.issue;
						if (typeof(issueData['container-xml']['contents']['content']) == 'undefined'){
								issueData['container-xml']['contents']['content'] = [];
						}
						else if (issueData['container-xml']['contents']['content'].constructor.toString().indexOf("Array") == -1) {
							issueData['container-xml']['contents']['content'] = [issueData['container-xml']['contents']['content']];
						}
						if (typeof(issueData['container-xml']['change-history']) == 'undefined'){
								issueData['container-xml']['change-history'] = [];
						}
						else if (issueData['container-xml']['change-history'].constructor.toString().indexOf("Array") == -1) {
							issueData['container-xml']['change-history'] = [issueData['container-xml']['change-history']];
						}
						eventHandler.flatplan.components.populateBookMeta();
						var issueContent = issueData['container-xml'];
						if (!issueContent.contents.content) {
							$('#manuscriptsDataContent').html('')
							eventHandler.components.actionitems.showRightPanel({'target': 'history'})
							return;
						}
						eventHandler.flatplan.components.populateSectionCards(targetNode);
						var articleList = issueContent.contents.content;
						var al = articleList.length;
						var doiString = [];
						var sectionItems = [];
						var sectionNames = [];
						var sn = -1;
						for (var a = 0; a < al; a++){
							if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
								doiString.push(articleList[a]._attributes.id);
							}
							if (articleList[a]._attributes['section'] && articleList[a]._attributes.id){
								var sectionName = articleList[a]._attributes['section'];
								if (! sectionNames[sectionName]){
									sectionNames[sectionName] = [];
									sn++;
									sectionItems[sn] = {'name' : sectionName, 'item' : []}
								}
								sectionItems[sn]['item'].push(articleList[a])
							}
						}
						
						var data = {}
						data.customer = $('#manuscriptContent #customerVal').text();
						data.fileName = $(targetNode).attr('data-file');
						data.items = sectionItems;
						data.chaptersCount = doiString.length;
						eventHandler.components.actionitems.showRightPanel({'target': 'history'})
						eventHandler.flatplan.components.populateHistory();
						var pagefn = doT.template(document.getElementById('issueTemplate').innerHTML, undefined, undefined);
						$('#manuscriptsDataContent').html(pagefn(data));
						$('#manuscriptsData').height(window.innerHeight - $('#manuscriptsData').position().top - $('#footerContainer').height());
						if (packageForBook.indexOf(param.customer) > -1) {
							$('#exportEpubBtn').removeClass('hidden');
						} else {
							$('#exportEpubBtn').addClass('hidden');
						}
						if (!sectionItems.length) {
							$('#combinePDF').addClass('hidden');
						}
						if (doiString.length == 0){
							for (var a = 0; a < al; a++){
								//if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
								if (articleList[a]._attributes.id){
									var doi = articleList[a]._attributes.id;
									if ($('#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]').length > 0){
										var articleCard = $('#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]');
										articleCard.data('data', articleList[a]);
										if (articleList[a]._attributes.fpage){
											articleCard.find('.pageRange').attr('data-fpage', articleList[a]._attributes.fpage)
											articleCard.find('.pageRange .rangeStart').text(articleList[a]._attributes.fpage)
										}
										if (articleList[a]._attributes.lpage){
											articleCard.find('.pageRange').attr('data-lpage', articleList[a]._attributes.lpage)
											articleCard.find('.pageRange .rangeEnd').text(articleList[a]._attributes.lpage)
										}
										if (articleList[a]._attributes['data-uuid']){
											var dataUuid = articleList[a]._attributes['data-uuid'];
										}else{
											var dataUuid = uuid.v4();
										}
										if (articleList[a]._attributes.chapterNumber && articleCard.find('.chapterNumber')){
											articleCard.find('.msChNo').html(articleList[a]._attributes.chapterNumber)
										}
										articleCard.attr('data-uuid', dataUuid)
										$(articleCard).parent().append(articleCard)
									}
								}
							}
							var c = 0;
							$('#tocContentDiv .sectionList > *:not(.sectionData)').each(function () {
								$(this).attr('data-c-id', c++)
							})
							$('.la-container').fadeOut();
							return false;
						}
						doiString = doiString.join(' OR ');
						/*$('.sectionList[data-section] .sectionDataChild').each(function (i, scIndex) {
							$(this).data('data', data.items[0].item[i]);
						})*/
						var params = {}
						params.url = '/api/getarticlelist';
						params.doistring = doiString;
						params.urlToPost = 'getDoiArticles';
						params.from = "0";
						params.size = "500";
						params.customer = data.customer;
						$.ajax({
						type: "POST",
						url: params.url,
						data: params,
						dataType: 'json',
							success: function (respData) {
								$('.la-container').fadeOut();
								if (!respData || respData.hits.length == 0) {
									$('.la-container').css('display', 'none');
									//$('#manuscriptsDataContent').html('NO RESULTS FOUND');
									$('#msCount').text('0');
									return;
								}
								$('.showEmpty').addClass('hidden');
								var data = {};
								data.dconfig = dashboardConfig;
								// data.info = [];
								data.filterObj = {};
								data.filterObj['customer'] = {};
								data.filterObj['project'] = {};
								data.filterObj['articleType'] = {};
								data.filterObj['typesetter'] = {};
								data.filterObj['publisher'] = {};
								data.filterObj['author'] = {};
								data.filterObj['editor'] = {};
								data.filterObj['copyeditor'] = {};
								data.filterObj['support'] = {};
								data.filterObj['supportstage'] = {};
								data.filterObj['contentloading'] = {};
								data.filterObj['others'] = {};
								data.disableFasttrack = disableFasttrack;
								data.userDet = JSON.parse($('#userDetails').attr('data'));
								data.customerType = projectLists[$('#manuscriptContent #customerVal').text()].type;
								//$('#manuscriptsDataContent').html('');
								data.count = 0;
								cardData = [];
								cardData = respData.hits;
								data.info = cardData;
								manuscriptsData = $('#manuscriptsDataContent');
								var pagefn = doT.template(document.getElementById('manuscriptTemplate').innerHTML, undefined, undefined);
								var cardsData = $('<div>' + pagefn(data) + '</div>');
								//$('#manuscriptsDataContent [data-section="chapters"]').append(pagefn(data));
								for (var a = 0; a < al; a++){
									//if (articleList[a]._attributes['id-type'] && articleList[a]._attributes['id-type'] == 'doi' && articleList[a]._attributes.id){
									if (articleList[a]._attributes.id){
										var doi = articleList[a]._attributes.id;
										if ($('#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]').length > 0){
											var articleCard = $('#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]');
											if (cardsData.find('[data-doi="' + doi + '"]').length > 0){
												articleCard.replaceWith(cardsData.find('[data-doi="' + doi + '"]'));
												articleCard = $('#manuscriptsDataContent [data-section]').find('[data-doi="' + doi + '"]');
											}
											articleCard.data('data', articleList[a]);
											if (articleList[a]._attributes.fpage){
												articleCard.find('.pageRange').attr('data-fpage', articleList[a]._attributes.fpage)
												articleCard.find('.pageRange .rangeStart').text(articleList[a]._attributes.fpage)
											} else {
												articleCard.find('.pageRange').attr('data-fpage', '1')
												articleCard.find('.pageRange .rangeStart').text('1')
											}
											if (articleList[a]._attributes.lpage){
												articleCard.find('.pageRange').attr('data-lpage', articleList[a]._attributes.lpage)
												articleCard.find('.pageRange .rangeEnd').text(articleList[a]._attributes.lpage)
											} else {
												articleCard.find('.pageRange').attr('data-lpage','1')
												articleCard.find('.pageRange .rangeEnd').text('1')
											}
											if (articleList[a]._attributes['data-uuid']){
												var dataUuid = articleList[a]._attributes['data-uuid'];
											}else{
												var dataUuid = uuid.v4();
											}
											if (articleList[a]._attributes.chapterNumber && articleCard.find('.chapterNumber')){
												articleCard.find('.msChNo').html(articleList[a]._attributes.chapterNumber)
											}
											articleCard.attr('data-uuid', dataUuid)
											$(articleCard).parent().append(articleCard)
										}
									}
								}
								$('#manuscriptsDataContent [data-section="chapters"] .msCard').attr('data-grouptype', 'body').attr('data-on-reorder', 'changeChapterNumber');
								var c = 0;
								$('#tocContentDiv .sectionList > *:not(.sectionData)').each(function () {
									$(this).attr('data-c-id', c++)
								})
								if(respData.hits && respData.hits[0] && respData.hits[0]._source && respData.hits[0]._source.customer){
									eventHandler.components.actionitems.getUserDetails(respData.hits[0]._source.customer);
								}
								$('.msCount.loading.hidden').removeClass('hidden');
								$('#manuscriptsDataContent li:visible').find('.highlightSort').removeClass('highlightSort')
								$('.msCount.loading').removeClass('loading').addClass('loaded');
								$('#msCount').text(respData.hits.length);
								//construct workflow progress bar
								var articleStages = {};
								for (var d = 0, dl = cardData.length; d < dl; d++){
									var cardInfo = cardData[d];
									if (cardInfo && cardInfo._source){
										sourceData = cardInfo._source;
									}
									if (sourceData == undefined || !sourceData || !sourceData.stage) {
										return false;
									}
									var sourceStage = [];
									if (sourceData.stage.constructor.toString().indexOf("Array") < 0) {
										sourceStage = [sourceData.stage]
									}else{
										sourceStage = sourceData.stage;
									}
									var currArticleStages = [];
									for (var s = 0, sl = sourceStage.length; s < sl; s++){
										var currStage = sourceStage[s];
										var currStageName = currStage.name;
										if (/Pre-editing|Typesetter|^Copyediting$|Author|Publisher/i.test(currStageName)){
											if (! articleStages[currStageName]){
												articleStages[currStageName] = [];
											}
											if (currStage.status == "completed" && ! currArticleStages[currStageName]){
												articleStages[currStageName].push(cardInfo._id);
												currArticleStages[currStageName] = [];
											}
										}
									}
								}
								var chCount = $('#manuscriptsData .msCard').length;
								$('.progress-container').html('')
								for (var articleStage in articleStages){
									var cnt = articleStages[articleStage].length;
									var completed = parseInt((cnt/chCount)*100);
									var progress = '<div class="col" style="max-width: 125px !important;margin-right:10px;padding: 0px !important;min-width: 100px !important;"><div class="progress" style="width: 100%;height: 8px;display:inline-block;"><div class="progress-bar bg-success" role="progressbar" style="padding:0px 5px 2px;width: ' + completed + '%;height:100%;"></div></div>';
									progress += '<span  style="font-size: 11px;">' + articleStage + '</span><span class="pull-right" style="font-size: 11px;color: #4c4c4c;">' + completed + '%</span></div>';
									$('.progress-container').append(progress)
								}
								$('.la-container').css('display', 'none');
							}
						});
					},
					error: function (res) {
					}
				});
			},
			/**
			*	Save META XML JSON details back to metaXML
			*	Send to API to save metaXML
			**/
			saveData: function(param) {
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var fileName = $('#manuscriptsDataContent .newStatsRow').attr('data-file');
				$('.save-notice').removeClass('hide');
				$('.save-notice').text('Saving...');
				$.ajax({
					type: 'POST',
					url: '/api/postissuedata?client=' + customer + '&jrnlName=' + project + '&fileName=' + fileName,
					data: JSON.stringify($('#manuscriptsData').data('binder')),
					success: function (data) {
						if (param && param.onsuccess) {
							if (typeof (window[param.onsuccess]) == "function") {
								window[param.onsuccess]
							}else{
								eval(param.onsuccess)
							}
						}
						if ($('[data-section="frontcontents"]').children().length >= 3 || ($('[data-section="frontcontents"]').children().length == 2 && $('[data-section="chapters"]').children().length == 2) || $('[data-section="chapters"]').children().length >= 3) {
							$('#combinePDF').removeClass('hidden');
						}
						//showMessage({ 'text': eventHandler.messageCode.successCode[200][3], 'type': 'success' });
						$('.save-notice').text(eventHandler.flatplan.successCode[200][3]);
					},
					error: function (data) {
						//showMessage({ 'text': eventHandler.messageCode.errorCode[501][1] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error' ,'hide':false});
						$('.save-notice').text(eventHandler.flatplan.errorCode[501][1] + ' ' + eventHandler.flatplan.errorCode[503][1]);
					},
					contentType: "application/json",
					dataType: 'json'
				});
			},
		},
		components: {
			attr:function(attr){
				s = '';
				$(attr).each(function(i,e){
					v = e.split('|');
					if(v.length==2){
						s += v[0]+'="'+v[1]+'" ';
					}
				});
				return s;
			},
			pad: function(number, length) {
				var str = '' + number;
				while (str.length < length) {
					str = '0' + str;
				}
				return str;
			},
			/** https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
			*	Function to convert roman to integer 
			*	input - roman numeral  e.g, fromRoman('iii')
			**/
			fromRoman: function (str) {  
				var result = 0;
				// the result is now a number, not a string
				var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
				var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
				for (var i = 0;i<=decimal.length;i++) {
				  while (str.indexOf(roman[i]) === 0){
					result += decimal[i];
					str = str.replace(roman[i],'');
				  }
				}
				return result;
			},
			/** https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/ 
			*	Function to convert integer to roman 
			*	input - intger val e.g, toRoman(4)
			**/
			toRoman: function(num) {  
				var result = '';
				var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
				var roman = ["m", "cm","d","cd","c", "xc", "l", "xl", "x","ix","v","iv","i"];
				for (var i = 0;i<=decimal.length;i++) {
				  while (num%decimal[i] < num) {     
					result += roman[i];
					num -= decimal[i];
				  }
				}
				return result;
			},
			imgError: function(current){
				current.src = "../images/NoImageFound.jpg"
				$(current).css('background', '');
			},
			appendTemplate: function(param,targetNode){
				//e = eventHandler.menu.bookview.appendContent;
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-template="true"]');
				var clone = template.clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				clone.find('#corresAuthor').removeClass('is-invalid');
				$(targetNode).closest('[data-parent-name]').after(clone);
				//e+=1;
				//eventHandler.menu.bookview.appendContent = e;
			},
			removeTemplate: function(param, targetNode){
				var templateType = $(targetNode).closest('[data-parent-name]').attr('data-type');
				var template = $('[data-type="' + templateType + '"][data-cloned="true"]');
				if (template.length > 1){
					$(targetNode).closest('[data-parent-name]').remove();
				}
			},
			bookType:function(param,targetNode){
				$(targetNode).toggleClass('custom-select');
				var a=[];
				$('ul .custom-select').each(function(){
					a.push($(this).text());
				});
				a = a.toString();
				$(targetNode).parent().siblings('input[type="text"]').val(a).focus();
				return false;
			},
			changeBookType: function(param,targetNode){
				$('.dynamic').remove();
				var bookType = $('#bookType').val();
				$('[data-cloned="true"]').remove();
				var clone = $('.'+bookType+'[data-template]').clone(true);
				clone.removeAttr('data-template').removeClass('hide').attr('data-cloned', 'true');
				$('.'+bookType+'[data-template]').after(clone)
			},
			addBookPopup:function(param,targetNode){
				if ($('#filterCustomer > .filter-list.active').length == 0){
					return false;
				}
				param ={
					customer : $('#manuscriptContent #customerVal').text()
				};
				$.ajax({
					type: "GET",
					url: "/api/getadbookmodal",
					data: param,
					success:function(data){
						if((data.statusCode == 200) && data.body){
							var properties = $(data.body);
							$(properties).find('[data-name]').each(function(){
								if(dataName = $(this).attr("data-name")){
									$('#addProj [data-name='+dataName+']').parent().removeClass('hide');
								}
							});
							$('#addProj .addProj').removeClass('hide');
							$('#addProj .saveProj').addClass('hide');
							$('#addProj [data-name]').val('');
							$('#addProj').find('select').each(function(){
								if ($(this).find('option[selected]').length > 0){
									$(this).val($(this).find('option[selected]').val())
								}
							});
							eventHandler.flatplan.components.changeBookType();
							var proofdetails = $(properties).find('proof-details')
							$(proofdetails).find('[data-name]').each(function(){
								dataName = $(this).attr("data-name")
								var currObj = $(this)
								if(dataName && $('#addProj select[data-name='+dataName+']').length >0){
									$('#addProj select[data-name='+dataName+']').find('option').remove()
									$('#addProj select[data-name='+dataName+']').append($('<option value=""></option>'))	
									$(currObj).find('project').each(function(){
										var project = $(this)
										var element = $('<option value="'+$(this).attr("value")+'">'+ $(this).attr("name")+'</option>')
										if($('#addProj select[data-name='+dataName+']').attr('data-siblings-name') && $('#addProj select[data-name='+dataName+']').attr('data-siblings-name')!=''){
											var siblings = $('#addProj select[data-name='+dataName+']').attr('data-siblings-name').split(',')
											siblings.forEach(function(sibling){
												if(project.attr(sibling) && project.attr(sibling)!=''){
													$(element).attr(sibling,project.attr(sibling))
												}
											})
										}
										$('#addProj select[data-name='+dataName+']').append($(element))	
									})
								}
								
							})
							$('#addProj input[data-validate="true"]').removeClass('is-invalid');
							$('#addProj').modal();
							$('#addProj').on('hide.bs.modal', function (event) {
								$('#addProj .modal-body').scrollTop(0);
							});
						}
					},
					error:function(err){
						console.log(data);
					},
				});
			},
			populateBookMeta: function(){
				var meta = $('#manuscriptsData').data('binder')['container-xml'].meta;
				if ((typeof (meta) == 'undefined')) {
					return false;
				}
				$('#addProj [data-name]').val('');
				$('#addProj [data-cloned]').remove();
				for (dataName in meta){
					if (meta[dataName].constructor.toString().indexOf("Array") != -1){
						var dataValue = meta[dataName][0];
					}else{
						var dataValue = meta[dataName];
					}
					var component = $('#addProj [data-name='+dataName+']');
					if (component.length > 0){
						if (component.length > 1 && dataValue._text && dataValue._attributes){
							if (meta[dataName].constructor.toString().indexOf("Array") != -1){
								dataValue = meta[dataName]
							}else{
								dataValue = [meta[dataName]];
							}
							dataValue.forEach(function(item){
								var attrName = Object.keys(item._attributes);
								if (attrName.length == 1){
									var attrValue = item._attributes[attrName[0]];
									component = $('#addProj [data-name='+dataName+'][data-form-details-attr="' + attrName +'|' + attrValue + '"]')
								}
								if (component.length == 1 && item._text){
									component.val(item._text)
								}
							});
						}else if (component.length == 1 && meta[dataName]._text){
							component.val(meta[dataName]._text)
						}
					}else if (typeof(meta[dataName]) == 'object' && !meta[dataName]._text){
						for (childName in meta[dataName]){
							if (meta[dataName][childName].constructor.toString().indexOf("Array") != -1){
								var dataValue = meta[dataName][childName];
							}else{
								var dataValue = [meta[dataName][childName]];
							}
							component = $('#addProj [data-parent-name='+childName+']');
							if (component.length > 1 && !dataValue[0]._text && dataValue[0]._attributes){
								dataValue.forEach(function(childValue){
									var attrName = Object.keys(childValue._attributes);
									if (attrName.length == 1){
										var attrValue = childValue._attributes[attrName[0]];
										component = $('#addProj [data-parent-name='+childName+'][data-form-details-attr="' + attrName +'|' + attrValue + '"][data-template]');
									}
									if (component.length == 1 && !childValue._text){
										if (component.attr('data-template') == 'true'){
											clonedComponent = component.clone(true);
											component.parent().append(clonedComponent);
											component = clonedComponent.removeAttr('data-template').attr('data-cloned', 'true').removeClass('hide');
										}
										for (subChild in childValue){
											if (subChild != '_attributes'){
												subComponent = component.find('[data-name="' + subChild + '"]');
												if (subComponent.length == 1 && childValue[subChild]._text){
													subComponent.val(childValue[subChild]._text)
												}
											}
										}
									}
								});
							}
						}
					}
				}
				var proofConfigDetails = $('#manuscriptsData').data('profConfigDetails');
				if(proofConfigDetails){
					var proofConfigProperty = proofConfigDetails["property"]
					if(proofConfigProperty){
						if (proofConfigProperty.constructor.toString().indexOf("Array") < 0) {
							proofConfigProperty = [proofConfigProperty];
						}		
						proofConfigProperty.forEach(function (property) {
							var current = property._attributes['data-name']
							var component = $('#addProj [data-name='+current+'][data-proof-config-details]');
							if(component.length > 0 ){
								$(component).html('')
								var defaultval = ''
								if(meta[current]){
									defaultval = meta[current]._text
								}
								if (property['project']){
									var projects = property['project']
									if(defaultval == ''){
										$(component).append(($('<option value=""></option>')))
									}
									if (projects.constructor.toString().indexOf("Array") < 0) {
										projects = [projects];
									}
									projects.forEach(function (project) {
										var name = project._attributes.name
										var val = project._attributes.value
										var isSelected = '';
										if (defaultval == val){
											isSelected = ' selected="true"';
										}
										var element = $('<option value="'+val +'" '+ isSelected+'>'+name +'</option>')
										if($(component).attr('data-siblings-name')){
											var siblings = $(component).attr('data-siblings-name').split(',')
												siblings.forEach(function(sibling){
													if(project._attributes[sibling] && project._attributes[sibling]!=''){
														$(element).attr(sibling,project._attributes[sibling])
													}
												})
										}
										//$(component).append(($('<option value="'+val +'" '+ isSelected+'>'+name +'</option>')))
										$(component).append($(element))
									})
								}
							}
						})
					}
				}
			},
			populateSectionCards: function (targetNode) {
				var sectionCards = $('#manuscriptsData').data('config')['section-cards'].card;
				if ((typeof (sectionCards) == 'undefined')) {
					return false;
				}
				if (sectionCards.constructor.toString().indexOf('Array') < 0){
					sectionCards = [sectionCards];
				}
				var sectionCardParameters = [];
				var sectionConfig = sectionCards;
				if (sectionConfig.constructor.toString().indexOf("Array") < 0) {
					sectionConfig = [sectionConfig];
				}
				var scObj = {}, scIndex = 0;
				var sectionHTML = '';
				var tempID = '21000';
				var prevFlagSection = '';
				// scObj = sectionConfig[scIndex]
				sectionCards.forEach(function (scObj, scIndex) {
					var entryId = parseInt(tempID) + parseInt(scIndex)
					if (scObj) {
						var section = scObj._attributes['label'] ? scObj._attributes['label'] : '';
						var fpage = lpage = 1;
						var dataUuid = uuid.v4();
						var pageCount = lpage - fpage + 1
						var dataType = scObj._attributes['data-type']
						if (dataType && dataType != undefined && dataType.match('body')) {
							if (scObj._attributes.groupType == 'roman') {
								fpage = lpage = 'i'
								pageCount = eventHandler.flatplan.components.fromRoman(lpage) - eventHandler.flatplan.components.fromRoman(fpage) + 1
							}
							var cName = $(targetNode).attr('data-customer');
							var pName = $(targetNode).attr('data-project');
							var blankPdfPath = ''
							// var entry = ({"_attributes":{"id": entryId, "section": section, "grouptype": scObj._attributes.groupType,"type": scObj._attributes.type, "fpage": fpage,"lpage": lpage},"file":{"_attributes":{"proofType":"print","type":"pdf","path":blankPdfPath}},"title":{"_text":scObj._attributes['label']},"file":{"_attributes":{"proofType":"online","type":"pdf","path":blankPdfPath}}});
							var entry = ({ "_attributes": { "id": entryId, "data-uuid": dataUuid, "section": section, "grouptype": scObj._attributes.groupType, "type": scObj._attributes.type, "fpage": fpage, "lpage": lpage, "pageExtent": pageCount }, "title": { "_text": scObj._attributes['label'] } });
							var fileObj = []
							var file = ({ "_attributes": { "proofType": "online", "type": "pdf", "path": "" } })
							if (scObj._attributes.type == 'blank') {
								blankPdfPath = 'https://kriya2.kriyadocs.com/resources/' + cName + '/' + pName + '/resources/' + cName.toUpperCase() + '_' + pName.toUpperCase() + '_BLANK.pdf?method=server';
								file = ({ "_attributes": { "type": "pdf", "path": blankPdfPath } })
							}
							else if (scObj._attributes.type == 'advert' || scObj._attributes.type == 'advert_unpaginated') {
								file = ''
							}
							if (file != '') {
								entry['file'] = file
							}
							sectionCardParameters.push(entry);
						}
					}
					scIndex++;
				})

				var data = {};
				data.sectionCards = sectionCardParameters;
				var pagefn = doT.template(document.getElementById('sectionDataTemplate').innerHTML, undefined, undefined);
				$('#sectionContent .innerCards').find('.uaa.card').remove();
				$('#sectionContent .innerCards').append(pagefn(data));
				// $('#sectionContent .innerCards').css('height', window.innerHeight - $('#sectionContent .innerCards').offset().top - $('#footerContainer').height() - 125 + 'px');
				$('#sectionContent .innerCards').find('.uaa.card').each(function (i, scIndex) {
					$(this).data('data', sectionCardParameters[i]);
				});
			},
			editBookMeta: function (param, targetNode) {
				$('#addProj .addProj').addClass('hide');
				$('#addProj .saveProj').removeClass('hide');
				$('#addProj input[data-validate="true"]').removeClass('is-invalid');
				$('#addProj').modal('show');
			},
			populateFlatPlan: function(param, targetNode){
				$('#manuscriptsDataContent #flatPlanBodyDiv').remove();
				$('[data-href="tocBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$('[data-href="flatPlanBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				var container = $('<div id="flatPlanBodyDiv">');
				$('#manuscriptsDataContent').append(container);
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var containerHTML = '<div class="container"><div class="wrapper">';
				var content = $('#manuscriptsData').data('binder')['container-xml'].contents.content;
				var imgArray = [];
				if(content){
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					// push all pdfs in Imgarray, 
					content.forEach(function (entry) {
						var id = entry._attributes.id ? entry._attributes.id : '';
						var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : '';
						var fpage =  entry._attributes.fpage;
						var groupType = entry._attributes.grouptype
						var lpage =  entry._attributes.lpage;
						var pdf = entry.file;
						var type=entry._attributes.type;
						var runOnflag = false;
						var srcId = id;
						var fileFlag = false
						var fileObj = entry['file'];
						if(fileObj){
							if (fileObj.constructor.toString().indexOf("Array") == -1) {
								fileObj = [fileObj];
							}
							fileObj.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
								var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
								if(currProofType == 'print' && pdfType != 'preflightPdf' && file['_attributes']['path']!=''){
									fileFlag = true
								}
							})
						}	
                		if(type == 'cover' || (/(advert)/gi.test(type)) || type == 'table-of-contents' || type == 'index'){
                    		srcId = entry._attributes['data-proof-doi'] ? entry._attributes['data-proof-doi'] : id
                   		}
            			if(entry._attributes['data-run-on'] && entry._attributes['data-run-on'] == 'true'){
							runOnflag = true
						}
						//get the pageExtent of each content,  push each page in imgArray
						if(pageExtent && runOnflag == false){
							var currentPage = 0;
							var ePageFlag = false;
							var romanFlag = false;
							if(/(c)$/gi.test(fpage) || pageExtent == 1){
								var  currentPage = fpage
								var pagneNum = eventHandler.flatplan.components.pad(1, 3)
								var src= '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/pdfThumbnails/page_'+pagneNum+'.png?method=server'
								if (!(/^(kriya|demo)/gi.test(window.location.host))) {
									src = '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/'+ window.location.host +'/pdfThumbnails/page_'+pagneNum+'.png?method=server'
								}
								var altSrc= src
								if(fileFlag == false && !(entry["region"])){
									src = '../images/NoImageFound.jpg'
								}
								var img= ({"_attributes":{"src": src,"alt":altSrc,"data-type":type,"data-section":entry._attributes.section,"data-current-page":currentPage,"id":id}})
								imgArray.push(img);
							}
							else{
								//set epage flag
								if(/(e)/gi.test(fpage)){
									fpage = fpage.replace('e','')
									lpage = lpage.replace('e','')
									ePageFlag = true
								}
								else if((groupType!='' && groupType!=undefined && groupType== 'roman' && typeof fpage != "number")||(type == 'table-of-contents' && typeof fpage != "number")){
									fpage = eventHandler.flatplan.components.fromRoman(fpage)
									//roman set roman flag
									romanFlag = true
								}
								fpage = parseInt(fpage);  
								//parseint fpage ,lpage
						
								for(var i=0; i<pageExtent; i++){
									var currentPage = fpage+i
									var pagneNum = eventHandler.flatplan.components.pad(i+1, 3)
									if(romanFlag == true){
										currentPage = eventHandler.flatplan.components.toRoman(currentPage)
									 }
									 else if( ePageFlag == true){
										 currentPage = 'e'.concat(currentPage)
									 }
									var src= '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/pdfThumbnails/page_'+pagneNum+'.png?method=server'
									if (!(/^(kriya|demo)/gi.test(window.location.host))) {
										src = '/resources/'+customer+'/'+project+'/'+ srcId+'_print/resources/'+window.location.host +'/pdfThumbnails/page_'+pagneNum+'.png?method=server'
									} 	
									var altSrc = src
									if(fileFlag == false && !(entry["region"])){
										src = '../images/NoImageFound.jpg'
									}
									var img= ({"_attributes":{"src": src,"alt":altSrc,"data-section":entry._attributes.section,"data-current-page":currentPage,"id":id}})
									imgArray.push(img)
								//   currIndex++;
								}
							}
						}
					});
				}
				var loadingGIF = $('<img class="col" src="../images/loading.gif">')
				var currIndex = 1;   
				var openDiv ='<div style="height:100%">'
				var closeDiv = '</div>'
				var coverflag = false
				var bookletHTML = '<div class="modal" id="myModal1" role="dialog"><button type="button" style="float:right" class="btn btn-default" data-dismiss="modal">Close</button><div id="mybook">'
				if(imgArray){
					imgArray.forEach(function (img,currIndex) {
						var refID = img._attributes.id.replace(/\./gi, '')
						if(currIndex == 0){
							if(img._attributes['data-type'] && (/(cover)/gi.test(img._attributes['data-type']))){
								containerHTML += '<div><p class="imgblock"  style="float:right;display:grid;margin-left:50%"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+ currIndex +')" style="height:130px;float:right;width:100%;border-right: 1px solid grey;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p></div>';
								coverflag = true      
							}
							else{
								containerHTML += '<div style="height:100%"><p class="imgblock"  style="float:right;width:50%;display:grid;margin-left:50%" onClick="eventHandler.flatplan.components.populateBooklet('+ currIndex +')"><span style="height: 130px;font-size: large;border: 1px solid black;padding: 40px 20px 20px 20px;"><span>COVER</span></span><span class="fpRange">C1</span></p></div>';
								var temp = parseInt(currIndex)+1
						
								containerHTML += '<div><p class="imgblock"  style="float:left;width:50%;display:grid;"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+ temp +')" style="height:130px;float:right;padding:0px;width:100%;border-right: 1px solid grey;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>';               
							}
						}
						if(currIndex == 0 && coverflag ==false){
							bookletHTML+= openDiv + '<p style="margin: 35%;font-size: -webkit-xxx-large;">COVER</p>' +closeDiv
							bookletHTML+= openDiv + '<img  class="img-responsive" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" style="padding:0px;height:100%;background: transparent url(../images/loading.gif) center no-repeat;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/>' +closeDiv
						} 
						else{
							bookletHTML+= openDiv + '<img  class="img-responsive" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" style="height:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/>' +closeDiv

						}
						if(coverflag ==false){
							if(currIndex !=0){
								var temp = parseInt(currIndex)+1
								if(currIndex %2 != 0 ){
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+ temp +')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>'
									containerHTML += closeDiv
								}
								else{
									containerHTML += openDiv   
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+temp +')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+ img._attributes['data-current-page']+'</span></p>'
								}
							}
						}
						else{
							if(currIndex !=0){
								if(currIndex %2 != 0 ){
									containerHTML += openDiv   
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+currIndex +')"  style="height:130px;float:left;border-right: 1px solid grey;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+img._attributes['data-current-page']+'</span></p>'
								}
								else{
									containerHTML += '<p class="imgblock" style="float:left;width:50%;display:grid"><img class="col" id="'+ refID +'" data-id="'+ refID +'" data-current-page="'+img._attributes['data-current-page'] +'" onClick="eventHandler.flatplan.components.populateBooklet('+currIndex +')"  style="height:130px;float:left;width:100%;background: transparent url(../images/loading.gif) center no-repeat;padding:0px;" src="'+ img._attributes.src +'" alt="'+ img._attributes.alt +'" onerror="eventHandler.flatplan.components.imgError(this)"/><span class="fpRange">'+ img._attributes['data-current-page']+'</span></p>'
									containerHTML += closeDiv
								}
							}
						}
						currIndex++;
					})
				}
				containerHTML +='</div></div>'
				bookletHTML +='</div></div>'
				container.append(containerHTML);
				$('body').append(bookletHTML)
				$('#flatPlanBodyDiv').css('height', (window.innerHeight - $('#flatPlanBodyDiv').offset().top  - $('#footerContainer').height() - 10) + 'px');
				$('#manuscriptsDataContent .sectionList').addClass('hidden')
			},
			populateBooklet: function(startPage) {
				var w = window.innerWidth;
				var h = window.innerHeight;
				//console.log("Width: " + w + "<br>Height: " + h);
				w = parseInt(w) - 450
				h = parseInt(h)
				$('#myModal1').modal('show')
				$('#mybook').booklet({
					pagePadding: 0,
					overlays: true,
					manual: false,
					pageNumbers: false,
					closed: true,
					startingPage :startPage+1,
					height: h,
					width:w,
					speed: 1000,
					shadows:true
				});
				/*$('#myModal1').mCustomScrollbar({
					axis:"y", // vertical scrollbar
					theme:"light-thick"
				});*/
			},
			closeBooklet: function() {
			   $('#myModal1').css('display','none')
			   $('#myModal1').css('padding-left','0px')
			   $('#myModal1').removeClass('in')
			},
			updatePageNumber: function (startNodeIndex, startPage, groupType) {
				var configData = $('#manuscriptsData').data('config');
				var firstNode = $('#manuscriptsData *[data-grouptype="body"][data-c-id]:first');
				var firstNodeID = parseInt(firstNode.attr('data-c-id'));
				var lastNode = $('#manuscriptsData *[data-grouptype="body"][data-c-id]:last');
				var lastNodeID = parseInt(lastNode.attr('data-c-id'));
				startNodeIndex = parseInt(startNodeIndex);
				if (/^(e)/gi.test(startPage)) {
					startPage = startPage.replace('e', '');
				}
				else if (groupType != '' && groupType != undefined && groupType == 'roman' && typeof startPage != "number") {
					startPage = eventHandler.flatplan.components.fromRoman(startPage);
				}
				startPage = parseInt(startPage);
				//get pagecount of front contents
				var frontContents = $('#manuscriptsData *[data-c-id][data-grouptype="front"]');
				var frontContentsCount = parseInt(0);
				frontContents.each(function () {
					var currFrontNode = $(this);
					var currFrontNodeID = parseInt(currFrontNode.attr('data-c-id'));
					var currFrontNodeData = currFrontNode.data('data');
					var pageExtent = parseInt(currFrontNodeData._attributes.pageExtent);
					frontContentsCount += pageExtent;
				});
				var datagroupType = '';
				if (groupType != '' && groupType != undefined) {
					datagroupType = "[data-grouptype='" + groupType + "']";
				}
				/* if group type is roman, update only the articles/adverts with same sectionhead*/
				if (groupType != '' && groupType != undefined && groupType == 'roman') {
					var current = $('[data-c-id="' + (startNodeIndex) + '"]');
					var currentData = $(current).data('data')._attributes;
					var section = currentData["section"]
					if (section != '' && section != undefined) {
						datagroupType += "[data-section='" + section + "']";
					}
				}
				var nodesToUpdate = $("#tocContentDiv [data-c-id][data-type!='advert_unpaginated'][data-run-on!='true']" + datagroupType).filter(function () {
					return parseInt($(this).attr("data-c-id")) >= startNodeIndex;
				});
				nodesToUpdate.each(function () {
					var currNode = $(this);
					var currNodeID = parseInt(currNode.attr('data-c-id'));
					var currNodeData = currNode.data('data');
					try {
						var fpage = currNodeData._attributes.fpage;
						var lpage = currNodeData._attributes.lpage;
						if (/^(e)/gi.test(fpage)) {
							fpage = fpage.replace('e', '');
						}
						if (/^(e)/gi.test(lpage)) {
							lpage = lpage.replace('e', '');
						}
						if (groupType == 'roman' && typeof fpage != "number") {
							fpage = eventHandler.flatplan.components.fromRoman(fpage)
							lpage = lpage ? eventHandler.flatplan.components.fromRoman(lpage) : lpage
						}
						fpage = parseInt(fpage);
						lpage = parseInt(lpage);
						lpage = lpage ? lpage : fpage;
						var pageCount = lpage - fpage + 1;
						if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
							pageCount = currNodeData._attributes['pageExtent'];
						}
						pageCount = parseInt(pageCount);
						var runonFlag = currNodeData._attributes['data-run-on'];
						runonFlag = runonFlag ? runonFlag : 'false';
						currNodeData._attributes['pageExtent'] = pageCount;
						/*if (fpage == startPage){
							return true;
						}*/
						//if (currNodeData._attributes.type == 'manuscript'){
						if (!(/^(blank|advert|filler|filler-toc|advert_unpaginated)$/gi.test(currNodeData._attributes.type))) {
							// if fresh-recto and pagecount of front contents is odd, return true for first article
							if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (currNodeID == firstNodeID) && (frontContentsCount % 2 != 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
							// if the configuration says fresh-recto is true, then startPage should always start on odd page, if not show error
							else if ((configData['page']) && (configData['page']._attributes['type'].toLowerCase() == 'fresh-recto') && (startPage % 2 == 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								$(currNode).find('.pageRange').attr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
							else if ((configData['page']) && configData['page']._attributes['type'].toLowerCase() == 'continuous-publication') {
								if ((configData['page']._attributes['first-page'] == 'recto')) {
									// if first page is recto  and pagecount of front contents is odd, return true for first article
									if ((currNodeID == firstNodeID) && (frontContentsCount % 2 != 0)) {
										$(currNode).find('.pageRange').addClass('incorrectPage');
										$(currNode).find('.pageRange').attr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'true';
									}
									//if the configuration says continuous-publication and first page is recto , first page should always starts on odd page, if not show error
									else if ((currNodeID == firstNodeID) && (startPage % 2 == 0)) {
										$(currNode).find('.pageRange').addClass('incorrectPage');
										$(currNode).find('.pageRange').attr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'true';
									}
									else {
										$(currNode).find('.pageRange').removeClass('incorrectPage');
										$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
										currNodeData._attributes.incorrectPage = 'false';
									}
								}
								else {
									//if the configuration says continuous-publication and first page is not recto , page can start in odd or even
									$(currNode).find('.pageRange').removeClass('incorrectPage');
									$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
									currNodeData._attributes.incorrectPage = 'false';
								}
							}
							else {
								$(currNode).find('.pageRange').removeClass('incorrectPage');
								$(currNode).find('.pageRange').removeAttr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'false';
							}
						}

						if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
							currNodeData._attributes.fpage = 'e'.concat(startPage);
							var endPage = startPage + pageCount - 1;
							// if config data says e-lpage as false, set fpage and lpage as same
							if ((configData['page']) && (configData['page']._attributes['e-lpage']) && (configData['page']._attributes['e-lpage'].toLowerCase() == 'false')) {
								endPage = startPage;
							}
							currNodeData._attributes.lpage = 'e'.concat(endPage);
						}
						else {
							var endPage = startPage + pageCount - 1;
							currNodeData._attributes.fpage = startPage;
							currNodeData._attributes.lpage = endPage;
							if (groupType == 'roman' && typeof startPage == "number") {
								currNodeData._attributes.fpage = eventHandler.flatplan.components.toRoman(startPage);
								currNodeData._attributes.lpage = eventHandler.flatplan.components.toRoman(endPage);

							}
						}
						var unpaginated = currNodeData._attributes['data-unpaginated'];
						unpaginated = unpaginated ? unpaginated : 'false';
						// end page of last artricle should always be even
						if (unpaginated != 'true') {
							if ((currNodeID == lastNodeID) && (endPage % 2 != 0)) {
								$(currNode).find('.pageRange').addClass('incorrectPage');
								$(currNode).find('.pageRange').attr('title', 'incorrectPage');
								currNodeData._attributes.incorrectPage = 'true';
							}
						}
						//if page range is modified, add class pageModified
						//if(currNode.find('.rangeStart').text() != startPage || currNode.find('.rangeEnd').text() != endPage){
						if (currNode.find('.pageRange').attr('data-fpage') != startPage || currNode.find('.pageRange ').attr('data-lpage') != endPage) {
							if (currNodeData._attributes && currNodeData._attributes.grouptype == 'epage') {
								currNode.find('.rangeStart').text('e'.concat(startPage));
								currNode.find('.rangeEnd').text('e'.concat(endPage));
								currNode.attr('data-modified', 'true');
								currNodeData._attributes['data-modified'] = 'true';
								$(currNode).find('.pageRange').attr('title', 'pageModified');
								currNode.find('.pageRange').attr('data-fpage', 'e'.concat(startPage));
								currNode.find('.pageRange').attr('data-lpage', 'e'.concat(endPage));

							}
							else {
								currNode.find('.rangeStart').text(startPage);
								currNode.find('.rangeEnd').text(endPage);
								currNode.find('.pageRange').attr('data-fpage', startPage);
								currNode.find('.pageRange').attr('data-lpage', endPage);
								if (groupType == 'roman' && typeof startPage == "number") {
									currNode.find('.rangeStart').text(eventHandler.flatplan.components.toRoman(startPage));
									currNode.find('.rangeEnd').text(eventHandler.flatplan.components.toRoman(endPage));
									currNode.find('.pageRange').attr('data-fpage', eventHandler.flatplan.components.toRoman(startPage));
									currNode.find('.pageRange').attr('data-lpage', eventHandler.flatplan.components.toRoman(endPage));

								}
								if (unpaginated != 'true' && currNodeData._attributes && currNodeData._attributes.type != 'blank') {
									currNode.attr('data-modified', 'true');
									$(currNode).find('.pageRange').addClass('pageModified');
									$(currNode).find('.pageRange').attr('title', 'Page is Modified');
									currNodeData._attributes['data-modified'] = 'true';
								}
							}
						}
						var nextNodeData = $('[data-c-id="' + (parseInt(currNode.attr('data-c-id')) + 1) + '"]').data('data');
						var nextRunOn = nextNodeData._attributes['data-run-on'];
						nextRunOn = nextRunOn ? nextRunOn : 'false';
						var nextUnpaginated = nextNodeData._attributes['data-unpaginated'];
						nextUnpaginated = nextUnpaginated ? nextUnpaginated : 'false';

						startPage = endPage + 1    // next manuscript should start from next page

						return true;
					}
					catch (e) {
						return false;
					}
				});
				var chapterNumberIndex = 1;
				var chapterNumberReordered = false;
				$('#manuscriptsData *.msCard[data-grouptype="body"][data-c-id][data-on-reorder="changeChapterNumber"]').each(function(){
					var msChNo = $(this).find('.msChNo');
					var cid = $(this).attr('data-c-id');
					var chDOI = $(this).attr('data-doi');
					if (chapterNumberIndex != msChNo.text()){
						var chData = $('#manuscriptsData').data('binder')['container-xml'].contents.content[cid];
						if (chData._attributes.id == chDOI && chData._attributes.chapterNumber == parseInt(msChNo.text())){
							chapterNumberReordered = true;
						}
					}
					chapterNumberIndex++;
				});
				if (chapterNumberReordered){
					(new PNotify({
						title: 'Confirmation Needed',
						text: 'Chapters are re-ordered. Do you want to re-order it?',
						hide: false,
						confirm: { confirm: true },
						buttons: { closer: false, sticker: false },
						history: { history: false },
						addclass: 'stack-modal',
						stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
					})).get()
					.on('pnotify.confirm', function () {
						eventHandler.flatplan.components.reorderChapterNumbers()
					})
					.on('pnotify.cancel', function () {
						
					});
				}
				eventHandler.flatplan.action.saveData();
				$('.save-notice').text('All changes saved');
				//calculate total pages
				//eventHandler.flatplan.components.updateTotalPages();
			},
			/*  calculate - total page number add the extent of articles,adverts,fillers*/
			updateTotalPages: function () {
				//get all contents - add pageExtent for each contents
				var totalPages = parseInt(0);
				var content = $('#tocContainer').data('binder')['container-xml'].contents.content;
				if (content) {
					if (content.constructor.toString().indexOf("Array") == -1) {
						content = [content];
					}
					content.forEach(function (entry) {
						if (entry) {
							var grouptype = entry._attributes.grouptype ? entry._attributes.grouptype : '';
							var section = entry._attributes.section ? entry._attributes.section : '';
							var runOn = entry._attributes['data-run-on'] ? entry._attributes['data-run-on'] : 'false';
							if (!(/^(cover|ifc|ibc|obc)/gi.test(section)) && runOn == 'false' && grouptype != 'epage') {
								var pageExtent = entry._attributes.pageExtent ? entry._attributes.pageExtent : parseInt(0);
								pageExtent = parseInt(pageExtent);
								totalPages += pageExtent;
							}
						}
					});
				}
				//update the total page number value in MetaData
				var meta = $('#tocContainer').data('binder')['container-xml'].meta;
				var pagecount = meta["page-count"] ? meta["page-count"]['_text'] : parseInt(0);
				if (totalPages != null && totalPages != 0 && meta["page-count"]) {
					meta["page-count"]['_text'] = totalPages
				}
				// update last page number in meta info
				var lastNode = $('#tocBodyDiv div[data-c-id][data-type="manuscript"][data-grouptype="body"][data-run-on!="true"]:last')
				var lastNodeData;
				if (lastNode && $(lastNode).data('data') && $(lastNode).data('data')._attributes) {
					lastNodeData = $(lastNode).data('data')._attributes
				}
				else {
					$(lastNode)._attributes
				}
				if (lastNodeData && meta["last-page"]) {
					var lastNodeLPage = lastNodeData.lpage
					meta["last-page"]['_text'] = lastNodeLPage
				}
			},
			reorderChapterNumbers: function(){
				var chapterNumberIndex = 1;
				$('#manuscriptsData *.msCard[data-grouptype="body"][data-c-id][data-on-reorder="changeChapterNumber"]').each(function(){
					var msChNo = $(this).find('.msChNo');
					var cid = $(this).attr('data-c-id');
					var chDOI = $(this).attr('data-doi');
					if (chapterNumberIndex != msChNo.text()){
						var chData = $('#manuscriptsData').data('binder')['container-xml'].contents.content[cid];
						if (chData._attributes.id == chDOI && chData._attributes.chapterNumber == parseInt(msChNo.text())){
							$('#manuscriptsData').data('binder')['container-xml'].contents.content[cid]._attributes.chapterNumber = chapterNumberIndex;
							$(this).find('.msChNo').text(chapterNumberIndex)
							var param = {
								'customer': $('#filterCustomer .filter-list.active').attr('value'),
								'project': $('#filterProject .filter-list.active').attr('value'),
								'doi': chData._attributes.id,
								'xpath': '//front//title-group/label',
								'data': '<label>' + chapterNumberIndex + '</label>'
							}
							eventHandler.flatplan.components.updateArticleData(param)
						}
					}
					chapterNumberIndex++;
				});
				eventHandler.flatplan.action.saveData();
			},
			updateArticleData: function(param, targetNode){
				$.ajax({
					type: "POST",
					url: "/api/updatearticle",
					data: {
						'customerName': param.customer,
						'projectName': param.project,
						'doi': param.doi,
						'xpath': param.xpath,
						'xmlFrag': param.data
					},
					success: function(res){
						
					},
					error: function(err){
						
					}
				});
			},
			openFileUploadModal: function (e) {
				$('#upload_model').remove();
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var doi = $('#rightPanelContainer').attr('data-dc-doi');
				var uploadType = 'File';
				var successFunction = 'eventHandler.flatplan.components.saveUploadedFile';
				if (typeof (doi) == "undefined") return false;
				$("body").append('<div id="upload_model" id="upload_modal" role="document" data-customer="' + customer + '" data-project="' + project + '" data-doi="' + doi + '" data-upload-type="' + uploadType + '" data-success="' + successFunction + '" class="modal comments-modal-upload"><div class="modal-dialog modal-dialog-centered modal-upload"><div class="modal-content"><div class="modal-header"><h3 class="uploadHeader">Upload ' + uploadType + ' for ' + doi + '<span style="margin-left: 20px;font-size: 12px;background: #fafafa;padding: 3px 10px;color: #313131;border-radius: 6px;display:none;" class="notify"></span></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div id="uploader" class="fileHolder"><div class="fileList" style="margin: 10px;text-align:center;"></div><div class="i-note"><p style="opacity: 0.6;">Drag and drop Files here to Upload</p><p><span class="btn btn-small btn-info" style="padding: 0px !important;"><input type="file" name="files[]" multiple="" style="display:none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><span class="fileChooser" style="padding:0.375rem 0.75rem;">Or Select Files to Upload</span></span></p></div></div><div class="modal-caption hidden" data-type="Striking Image" contenteditable="true">metacontent</div><div class="modal-footer"><span class="btn btn-small btn-success upload_button save disabled highlight-background" data-channel="components" data-topic="actionitems" data-event="click" data-success="eventHandler.components.actionitems.attachFileToNotes" data-upload-type="Files" data-message="{\'funcToCall\': \'uploadFile\', \'param\':this}"><i class="fa fa-upload"> upload</i></span></div></div></div></div>');
				var data = {
					proofType: $(e).attr('data-proofType'),
					node: e
				}
				$('#upload_model').data(data);
				$('#upload_model').modal();
				tempFileList = {};
			},
			saveUploadedFile: function (resp, targetNode) {
				$('#upload_model').modal('hide');
				var fileObj = resp.fileDetailsObj;
				var keyPath = "' + compObj.key + '";
				// var advertLayoutSec = $(targetNode).attr('data-layout-sec');
				var manualUpload = "file._attributes.data-manualUpload";
				var proofType = $('#upload_model').data().proofType;
				var currRegionVal = $(targetNode).closest('div').parent().attr('data-region-id')
				var currLayoutVal = $(targetNode).closest('div').parent().attr('data-layout')
				if (currRegionVal) {
					currRegionVal = currRegionVal.toLowerCase();
				}
				var fileEle = $('#upload_model').data().node;
				// var keyPath = $(targetNode).attr('data-key-path');
				if (resp.status.code == 200) {
					$('.save-notice').text(eventHandler.flatplan.successCode[200][1]);
					var filePath = '/resources/' + resp.fileDetailsObj.dstKey;
					var fileExtension = "pdf"
					if ($('.loading-status').parents('.tabContent').length > 0 && $('.loading-status').parents('.tabContent').find('object').length > 0 && proofType == 'print') {
						$('.loading-status').parents('.tabContent').find('object').attr('data', filePath)

					}
					$('.loading-status').remove();
					if (keyPath) {
						var dataCID = $('#rightPanelContainer').attr('data-c-rid');
						// get data from the corresponding article
						var myData = $('#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data');
						// if the type is manuscript and the uploaded file is xml , then save the xml in AIP
						if ((myData._attributes) && (myData._attributes.type) && (myData._attributes.type == 'manuscript')) {
							var fileExtension = filePath.split('.');
							fileExtension = fileExtension.pop();
							if (fileExtension == 'xml') {
								if (typeof (FileReader) != "undefined") {
									var reader = new FileReader();
									reader.onload = function (e) {
										//var xmlDoc = $.parseXML(e.target.result);
										eventHandler.general.actions.saveXML('manuscript', e.target.result)
									}
									reader.readAsText(fileObj);
								}

							}
							if (fileExtension == 'pdf') {
								var fileFlag = false;
								var files = myData['file']
								if (files.constructor.toString().indexOf("Array") < 0) {
									files = [files];
								}
								files.forEach(function (file) {
									var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
									if (currProofType == proofType) {
										file['_attributes']['path'] = filePath;
										fileFlag = true
									}
								})
								if (fileFlag == false) {
									var file = ({ "_attributes": { "type": "pdf", "path": filePath, "proofType": proofType } })
									files.push(file)
									myData['file'] = files;
								}
								//on upload of manuscript pdf, get pageExtent from user as input, and update pageNumber
								var notify = new PNotify({
									text: '<div id="notifyForm" class="pageExtentText"><div class="pageExtent"><p>Type the page extent for the uploaded pdf</p><input class="pageExtent_update" type="text"/><input class=" pf-button btn btn-small" style="margin-right: 10px;" type="button" name="pageExtent_update" value="Save"/></div></div>',
									icon: false,
									width: 'auto',
									hide: false,
									buttons: {
										closer: false,
										sticker: false
									},
									addclass: 'stack-modal',
									stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true },
									insert_brs: false
								});
								notify.get().find('.pageExtentText').on('click', '[name=cancel]', function () {
									notify.remove();
								})
								notify.get().find('.pageExtentText').on('click', '[name=pageExtent_update]', function () {
									var pageExtent = $('.pageExtentText').find('input[class=pageExtent_update]').val()
									var startPage = parseInt(myData._attributes.fpage)
									var endPage = startPage + parseInt(pageExtent) - 1;
									myData._attributes.pageExtent = pageExtent;
									myData._attributes.lpage = parseInt(endPage);
									eventHandler.flatplan.action.saveData();
									var prevNode = $('[data-c-id="' + (dataCID - 1) + '"]');
									if (prevNode) {
										var prevNodeData = $(prevNode).data('data')._attributes;
										//if prevNode groupType is same get fpage as startpage
										if (prevNodeData.type && prevNodeData.type == 'manuscript') {
											startPage = parseInt(prevNodeData.lpage) + 1;
										}
									}
									eventHandler.flatplan.components.updatePageNumber(dataCID, startPage, myData._attributes.grouptype);
									//  eventHandler.general.actions.saveData();
									notify.remove();
								})

								if (proofType == 'print') {
									$(fileEle).closest('div').find('object').attr('data', filePath);
									var param = { "processType": "PDFPreflight", "pdfLink": filePath }
									//on upload of print pdf, trigger preflight
									eventHandler.flatplan.export.exportToPDF(param, targetNode);
									eventHandler.flatplan.action.saveData();
								}

							}
						}
						else if (((myData._attributes) && (myData._attributes.type) && (myData._attributes.type == 'front-matter')) || (fileExtension == 'pdf' && $('#filterCustomer .active').attr('data-type') == 'book') || (fileExtension == 'pdf' && proofType != '')) {
							var files = myData['file']
							var pdfFileFlag = false
							if (files.constructor.toString().indexOf("Array") < 0) {
								files = [files];
							}
							files.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
								if (currProofType == proofType) {
									file['_attributes']['path'] = filePath;
									pdfFileFlag = true
								}
							})
							if (pdfFileFlag == false) {
								var file = ({ "_attributes": { "type": "pdf", "path": filePath, "proofType": proofType } })
								files.push(file)
								myData['file'] = files;
							}
							$('#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data', myData);
							eventHandler.flatplan.action.saveData();
							$('[data-type=" sectionCard "] object').attr('data', filePath);
							$('[data-type=" sectionCard "] object').css('height', window.innerHeight - $('[data-type=" sectionCard "] object').offset().top - $('#footerContainer').height() - 15 + 'px');
							$('[data-type=" sectionCard "] object').show();
							$('[data-type=" sectionCard "] .noPDF').hide();
						}
						else {
							var keyPathArr = keyPath.split('.');
							// the last key is required to update the data, if you include the key directly then we will get only the value
							var lastKey = keyPathArr.pop();
							keyPathArr.forEach(function (key) {
								if (myData[key]) {
									myData = myData[key];
								}
								else {
									myData[key] = {}
									myData = myData[key]
								}
							})
							myData[lastKey] = filePath;
							if (manualUpload) {
								var myData = $('#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data');
								var manualUploadArr = manualUpload.split('.');
								var lastKey = manualUploadArr.pop();
								manualUploadArr.forEach(function (mkey) {
									myData = myData[mkey];
								})
								myData[lastKey] = "true";
							}
							$(fileEle).closest('div').find('object').attr('data', filePath);
							eventHandler.flatplan.action.saveData();
						}
					}
					else if (advertLayoutSec) { // if layout section is specifed, then loop through the region,layout and update the filepath
						var dataCID = $('#msDataDiv').attr('data-c-rid');
						// get data from the corresponding article
						var myData = $('#manuscriptsData div[data-c-id="' + dataCID + '"]').data('data');
						var fileFlag = false;
						var regionArr = myData ? myData.region : ''
						if (regionArr) {
							if (regionArr.constructor.toString().indexOf("Array") < 0) {
								regionArr = [regionArr];
							}
						}
						else {
							regionArr = [];
						}
						var regionObj = {};
						var rcIndex = 0;
						var fileArray = [];
						var regionFlagVal = false;
						regionArr.forEach(function (regionObj, rcIndex) {
							if ((regionObj) && (regionObj._attributes['type'] == currRegionVal)) {
								regionFlagVal = true;
								fileArray = regionObj.file; // get files from data
								if (fileArray.constructor.toString().indexOf("Array") < 0) {
									fileArray = [fileArray];
								}
								//match the layout sec of file uploaded with the layout sec of file in issueXML
								fileArray.forEach(function (file) {
									if (file._attributes['data-layout'] == advertLayoutSec) {
										file._attributes['path'] = filePath;
										fileFlag = true;
									}
								})
								if (fileFlag == false) { // if file for the specified section is not available, then push the file into file Array
									var file = ({ "_attributes": { "type": "file", "data-region": currRegionVal, "data-layout": advertLayoutSec, "path": filePath } })
									fileArray.push(file)
								}
								regionObj["_attributes"]["layout"] = currLayoutVal; //change the layout value in xml
								regionObj["_attributes"]["value"] = "1";
								regionObj.file = fileArray;
								$(fileEle).closest('div').find('object').attr('data', filePath);
								eventHandler.flatplan.action.saveData();
							}
						})
						if (regionFlagVal == false) { // if region info is not available in data,add a new region
							var newRegion = ({ "_attributes": { "type": currRegionVal, "layout": currLayoutVal }, "file": { "_attributes": { "type": "file", "data-region": currRegionVal, "data-layout": advertLayoutSec, "path": filePath } } })
							regionArr.push(newRegion)
							if (myData) {
								myData.region = regionArr;
							}
							else {
								$('[data-c-id="' + dataCID + '"]').data('data', regionArr);
							}
							$(fileEle).closest('div').find('object').attr('data', filePath);
							eventHandler.flatplan.action.saveData();
						}
					}
				}
				else {
					$('.save-notice').text(eventHandler.messageCode.errorCode[501][3] + ' ' + eventHandler.messageCode.errorCode[503][1]);
				}
			},
			populateUAA: function (entry, tempID) {
				if (entry._attributes.type == 'blank') {
					containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
					containerHTML += '<p class="row">';
					containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry._attributes.type + '">' + entry._attributes.type + '</span>';
					containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
					containerHTML += '</p>';
					containerHTML += '<p class="row">';
				}
				containerHTML = '<div class="uaa" draggable="true" id="' + tempID + '">';
				containerHTML += '<p class="row">';
				// containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msType" title="' + entry.type._text + '">' + entry.type._text + '</span>';
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msID">' + entry._attributes.id + '</span>';
				containerHTML += '</p>'
				containerHTML += '<p class="row">';
				var accDate = '';
				if (typeof (entry.accDate) != 'undefined') {
					accDate = new Date(entry.accDate._text);
					accDate = (accDate ? accDate : '').toDateString().replace(/^[a-z]+ /gi, '');
				}
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msAccDate" title="' + accDate + '">Acc. Date: <b>' + accDate + '</b></span>';
				containerHTML += '<span class="col-sm-6 col-md-6 col-lg-6 msPCount">Page Count: <b>' + (entry._attributes.lpage - entry._attributes.fpage + 1) + '</b></span>';
				containerHTML += '</p>'
				containerHTML += '<p class="row">';
				var titleText = 'Title Text';
				if (entry.title && entry.title._text) {
					titleText = entry.title._text;
				}
				containerHTML += '<span class="col-sm-12 col-md-12 col-lg-12 msText">' + titleText + '</span>';
				containerHTML += '</p>'
				containerHTML += '</div>';
				return containerHTML;
			},
			cardControlOrder: function (e, target) {
				var targetNode = target;
				if (targetNode.hasClass('move_down')) {//icon-arrow_downward
					// swap the article info in the container array
					var currNode = targetNode.closest('[data-c-id]');
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var tempData = $('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex];
					$('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex] = $('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex + 1];
					$('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex + 1] = tempData;
					var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
					var nextNode = $('[data-c-id="' + (currNodeIndex + 1) + '"]');
					var nextNodeData = $(nextNode).data('data')._attributes;

					var startPage = histObj.fpage;
					if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') {
						if ((histObj.type == 'filler') && (/^(blank|advert|filler|filler-toc)$/gi.test($(nextNode).data('data')._attributes.type))) {
							// filler should run-on only after manuscript
							$('.save-notice').text(eventHandler.messageCode.errorCode[502][2]);
							return false;
						}
						else {
							startPage = parseInt(histObj.fpage) + 1;// if run-on article, then add 1 to fpage
						}
					}

					histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' after ' + (nextNodeData['id-type'] ? nextNodeData['id-type'] : nextNodeData.type) + ' - ' + nextNodeData.id;
					currNode.attr('data-c-id', currNodeIndex + 1);
					nextNode.attr('data-c-id', currNodeIndex);
					currNode.insertAfter(nextNode);
					if (histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true') {
						eventHandler.flatplan.components.updatePageNumber(currNodeIndex, startPage, currNode.attr('data-grouptype'));
					}
					eventHandler.flatplan.components.updateChangeHistory(histObj);
					// push updated data to server
					eventHandler.flatplan.action.saveData();
				}
				else if (targetNode.hasClass('move_up')) {//icon-arrow_upward
					// swap the article info in the container array
					var currNode = targetNode.closest('[data-c-id]');
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var tempData = $('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex - 1];
					$('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex - 1] = $('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex];
					$('#manuscriptsData').data('binder')['container-xml'].contents.content[currNodeIndex] = tempData;
					var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
					var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
					var prevNodeData = $(prevNode).data('data')._attributes;
					var startPage = prevNodeData.fpage;
					if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') { //check if run-on article
						var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
						var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
						// filler should run-on only after manuscript
						if ((histObj.type == 'filler') && (/^(blank|advert|filler)$/gi.test($(secondPrevNode).data('data')._attributes.type))) {
							showMessage({ 'text': eventHandler.messageCode.errorCode[502][2], 'type': 'error', 'hide': false });
							return false;
						}
						else { // if run-on article, then set 2nd previous node lpage as fpage
							startPage = parseInt(secondPrevNodeData.lpage);
						}
					}
					if (prevNodeData["data-unpaginated"] && prevNodeData["data-unpaginated"] == 'true') {
						var secondPrevNode = $('[data-c-id="' + (currNodeIndex - 2) + '"]');
						if (secondPrevNode) {
							var secondPrevNodeData = $(secondPrevNode).data('data')._attributes;
							startPage = parseInt(secondPrevNodeData.lpage) + 1;
						}
						else {
							startPage = parseInt(histObj.fpage);
						}
					}
					histObj.change = 'Moved ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id + ' before ' + (prevNodeData['id-type'] ? prevNodeData['id-type'] : prevNodeData.type) + ' - ' + prevNodeData.id;
					currNode.attr('data-c-id', currNodeIndex - 1);
					prevNode.attr('data-c-id', currNodeIndex);
					currNode.insertBefore(prevNode);
					//if next node is filler, move filler along with article
					/*if(nextNodeData.type == 'filler'){
						prevNode.attr('data-c-id', currNodeIndex + 1);
						nextNode.attr('data-c-id', currNodeIndex);
						nextNode.insertAfter(currNode);
					}*/
					eventHandler.flatplan.components.updateChangeHistory(histObj);
					if ((histObj["data-unpaginated"] != 'true') && (histObj["data-run-on"] != 'true')) {
						eventHandler.flatplan.action.saveData();
						eventHandler.flatplan.components.updatePageNumber(currNodeIndex - 1, startPage, currNode.attr('data-grouptype'));
					}
					// push updated data to server
					eventHandler.flatplan.action.saveData();
				}
				else if (targetNode.hasClass('downloadK1Article')) {//icon-download2
					$('#insertK1ArticleModal').find('select[id=sectionList]').html('')
					$('#insertK1ArticleModal').find('input[id=insert_K1Article]').val('')
					var currentSectionhead = $(targetNode).parent().parent().text();
					var sectionHTML = '';
					var flagValues = eventHandler.general.components.getAvailableSections()
					flagValues.forEach(function (selectValue, sIndex) {
						var isSelected = '';
						sectionHTML += '<option value="' + selectValue + '"' + isSelected + '>' + selectValue + '</option>'
					})
					$('#insertK1ArticleModal').find('select[id=sectionList]').append(sectionHTML)
					$('#insertK1ArticleModal').modal('show');
				}
				else if (targetNode.hasClass('remove_card')) {//icon-close
					(new PNotify({
						title: 'Confirmation Needed',
						text: 'Are you sure you want to remove the manuscript?',
						hide: false,
						confirm: { confirm: true },
						buttons: { closer: false, sticker: false },
						history: { history: false },
						addclass: 'stack-modal',
						stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
					})).get()
						.on('pnotify.confirm', function () {
							var currNode = targetNode.closest('[data-c-id]');
							var currNodeIndex = currNode.attr('data-c-id');
							// remove the deleted manuscript's from the container
							var removedData = JSON.parse(JSON.stringify($('#manuscriptsData').data('binder')['container-xml'].contents.content.splice(currNodeIndex, 1)));
							removedData[0]._attributes.lpage = removedData[0]._attributes.lpage - removedData[0]._attributes.fpage + 1;
							removedData[0]._attributes.fpage = 1;
							var removedDataID = $('#uaaDiv > div:last').attr('id');
							var removedDataID = removedDataID ? (parseInt(removedDataID) + 1) : 100000;
							if (removedData[0]._attributes.type == 'manuscript') {
								$('#uaaDiv').append(eventHandler.flatplan.components.populateUAA(removedData[0], removedDataID));
								$('#' + removedDataID).data('data', removedData[0]);
							}
							var histObj = JSON.parse(JSON.stringify(currNode.data('data')._attributes));
							histObj.change = 'Removed ' + (histObj['id-type'] ? histObj['id-type'] : histObj.type) + ' - ' + histObj.id;
							eventHandler.flatplan.components.updateChangeHistory(histObj);
							var nextNodeIndex = parseInt(currNodeIndex) + 1
							var nextNode = $('[data-c-id="' + nextNodeIndex + '"]')
							// remove manuscript node from UI
							currNode.remove();
							// var lastNode = $('#manuscriptsData div[data-c-id]:last');
							var lastNode = $('#manuscriptsData [data-section="'+ nextNode.parent().attr('data-section')	+'"] [data-c-id]:last');
							var lastNodeID = parseInt(lastNode.attr('data-c-id'));
							var tempID = parseInt(currNodeIndex) + 1
							var seqID = parseInt(lastNode.attr('data-c-id')) - 1;
							var entry = nextNode.data('data')
							//change data-c-id for contents below the removed content
							if (lastNode.length > 0 && nextNode.length > 0) {
								lastNodeID = parseInt(lastNode.attr('data-c-id'));
								while (lastNodeID != tempID) {
									lastNode.attr('data-c-id', seqID--);
									if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
										lastNode = $(lastNode).prevAll('[data-c-id]:first')
									}
									else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
										lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
									}
									else {
										lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
									}
									lastNodeID = parseInt(lastNode.attr('data-c-id'));
								}
								lastNode.attr('data-c-id', seqID)
							}
							// update page number for the contents below removed Node
							if (nextNode) {
								var startPage = histObj.fpage;
								if (histObj["data-run-on"] && histObj["data-run-on"] == 'true') { // if run-on article, add 1 to the fpage
									var prevNode = $('[data-c-id="' + (currNodeIndex - 1) + '"]');
									var prevNodeData = $(prevNode).data('data')._attributes;
									startPage = parseInt(prevNodeData.lpage) + 1;
								}
								if (histObj["data-unpaginated"] != 'true' && histObj["data-run-on"] != 'true') {
									$('.save-notice').text('Saving...');
									eventHandler.flatplan.components.updatePageNumber(nextNode.attr('data-c-id'), startPage, histObj['grouptype']);
								}
							}
							// push updated data to server
							$('.save-notice').text('Saving...');
							var chapterCount = Number($('#manuscriptsData .articleCount').contents()[1].nodeValue) - 1;
							$('#manuscriptsData .articleCount').contents()[1].nodeValue = chapterCount;
							eventHandler.flatplan.action.saveData();
						})
						.on('pnotify.cancel', function () { });
				}
				//icon-add
				else if (targetNode.hasClass('addCard') || targetNode.hasClass('addContent')) {
					var currNode = targetNode.closest('[data-c-id]');
					if (currNode.length == 0) {
						var currSec = targetNode.closest('.sectionData')
						currNode = currSec.next()
					}
					var currNodeIndex = parseInt(currNode.attr('data-c-id'));
					var groupType = 'body'
					if (currNode.length > 0 && currNode.data('data')._attributes && currNode.data('data')._attributes.grouptype && currNode.data('data')._attributes.grouptype != '') {
						groupType = currNode.data('data')._attributes.grouptype
					}
					loadBlankOrAdvertOrFiller($('#filterProject .active').attr('data-project-type'), groupType)
					$('#metaHistory, #manuscriptData, #advertsData').addClass('hide');
					$('#unassignedArticles').removeClass('hide');
				}
				// to edit section head
				else if (targetNode.hasClass('editSectionHead')) {//icon-edit
					var currentSectionhead = $(targetNode).parent().prev('.sectionHead').text()
					if (currentSectionhead != '' || currentSectionhead != undefined) {
						currentSectionhead = currentSectionhead.trim()
					}
					$('#changeSectionHead').find('input[class~=sectionHead_change]').val(currentSectionhead)
					$('#changeSectionHead').find('input[class~=sectionHead_change]').attr('data-org-data', currentSectionhead)
					$('#changeSectionHead').modal('show');
				}
			},
			updateChangeHistory: function (histObj) {
				var userName = userData.kuser.kuname.first;//$('.userinfo:first').attr('data-name');
				var currTime = new Date().toString();
				var changeText = histObj.change;
				var fileID = histObj.id;
				var fileType = histObj.type;
				var changeObj = { 'user': userName, 'time': currTime, 'change': changeText, 'file': fileID, 'fileType': fileType, 'id-type': histObj['id-type'] };
				$('#manuscriptsData').data('binder')['container-xml']['change-history'].push(changeObj);
				eventHandler.flatplan.components.populateHistory(changeObj);
			},
			populateHistory: function (chData) {
				//$('#changeHistory').html('')
				var changeHistoryData = [];
				if (typeof (chData) != 'undefined') {
					changeHistoryData = [{
						"user": { "_text": chData.user },
						"change": { "_text": chData.change },
						"time": { "_text": chData.time },
						"file": { "_text": chData.file },
						"fileType": { "_text": chData.fileType }
					}]
				}
				else {
					$('#changeHistory').html('')
					changeHistoryData = $('#manuscriptsData').data('binder')['container-xml']['change-history'];
				}
				var chHTML = '';
				changeHistoryData.forEach(function (chData) {
					var chTime = chData.time._text
					chTime = chTime.replace(/^[a-z]+ (.*:[0-9]{2}) .*$/gi, "$1");
					chHTML += '<p class="chData">';
					chHTML += '<span class="user">' + chData.user._text + '</span> <span class="change">' + chData.change._text + '</span> on <span class="time" title="' + chData.time._text + '">' + chTime + '</span>';
					if (chData.file && chData.file._text) {
						chHTML += 'for <span class="file">' + chData.file._text + '</span>&#x00A0;(<span class="type">' + chData.fileType._text + '</span>)';
					}
					chHTML += '</p>';
				});
				$('#changeHistory').append('<div class="changeHistoryContainer" style="overflow-y:auto;">' + chHTML + '</div>');
				$('#changeHistory .changeHistoryContainer').css('height', window.innerHeight - $('#changeHistory').offset().top - $('#footerContainer').height() - 15 + 'px');
				/*$('#changeHistory .changeHistoryContainer').mCustomScrollbar({
					axis: "y", // vertical scrollbar
					theme: "dark"
				});*/
			},
			exportEpub: function (targetNode) {
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var parameterString = "client=" + customer + "&id=" + project + "&jrnlName=" + project + "&processType=PackageCreator&type=" + customer + "-epub&issueFileName=" + project + ".xml&uploadTo=localftp&siteName=" + window.location.protocol + "//" + window.location.hostname;
				$('#notifyBox .notifyHeaderContent').text("Publishing file... Please wait.");
				$('#notifyBox #reloadinfobody').text('');
				$('#notifyBox').show();
				$.ajax({
					type: 'GET',
					url: "/api/publisharticle?" + parameterString,
					success: function (data) {
						if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))) {
							eventHandler.components.actionitems.getJobStatus(customer, data.message.jobid, '', '', '', project);
						}
						else if (data == '') {
							$('#notifyBox #reloadinfobody').text('Failed');
						} else {
							$('#notifyBox #reloadinfobody').text(data.status.message);
							if (data.status.message == "job already exist") {
								eventHandler.components.actionitems.getJobStatus(customer, data.message.jobid, '', '', '', project);
							}
						}
					},
					error: function (error) {
						$('#notifyBox #reloadinfobody').text('Failed');
					}
				});
			}
		},
		toggle: {
			listView: function(param, targetNode){
				$('[data-href="flatPlanBodyDiv"]').removeClass('active').removeClass('btn-primary').addClass('btn-default');
				$('[data-href="tocBodyDiv"]').addClass('active').removeClass('btn-default').addClass('btn-primary');
				$('#manuscriptsDataContent #flatPlanBodyDiv').remove()
				$('#manuscriptsDataContent .sectionList').removeClass('hidden')
			},
			previewPDF: function(param, targetNode){
				eventHandler.flatplan.components.populateFlatPlan()
			},
			tocSection: function (param, targetNode) {
				if ($(targetNode).hasClass('sectionData')) {
					$('.sectionData.active').find('.openclose').toggleClass('fa-caret-right').toggleClass('fa-caret-down');
					$(targetNode).find('.openclose').toggleClass('fa-caret-right').toggleClass('fa-caret-down');
					if ($(targetNode).parent().hasClass('active')) {
						$('.sectionList.active').removeClass('active');
					}else {
						$('.sectionList.active').removeClass('active');
						$(targetNode).parent().addClass('active')
					}
				}
				else if ($(targetNode).hasClass('icon-fullscreen')) {
					$('.sectionData').addClass('active').find('.openclose').addClass('icon-arrow_drop_down').removeClass('icon-arrow_drop_up');
					$('[data-sec]').removeClass('hide');
					$(targetNode).removeClass('icon-fullscreen').addClass('icon-fullscreen_exit');
				}
				else if ($(targetNode).hasClass('icon-fullscreen_exit')) {
					$('.sectionData').removeClass('active').find('.openclose').addClass('icon-arrow_drop_up').removeClass('icon-arrow_drop_down');
					$('[data-sec]').addClass('hide');
					$(targetNode).removeClass('icon-fullscreen_exit').addClass('icon-fullscreen');
				}
			}
		},
		errorCode: {
			//error in get method
			500: {
				1: 'Something went wrong when fetching Data.',
				2: 'Something went wrong when fetching Customer List.',
				3: 'Something went wrong when fetching data from ',
				4: 'Something went wrong when fetching Project List',
				5: 'Something went wrong when retrieving Issue List.',
				6: 'Something went wrong when fetching Issue Data.',
				7: 'Something went wrong when fetching Project Stages.',
				8: 'Something went wrong when fetching Advert/Filler layouts.',
				9: 'Something went wrong when fetching Article.',
				10: 'Something went wrong when retrieving Article List.',
			},
			//error in post method
			501: {
				1: 'Saving failed.',
				2: 'Error while saving article XML.',
				3: 'Upload failed.',
				4: 'Saving Meta Infofailed.',
				5: 'Something went wrong when combining pdf.',
				6: 'Error in Creating Issue.'
			},
			//incorrect info
			502: {
				1: 'Unable to add the requested file.',
				2: 'Filler can oly be added below Manuscript.',
				3: 'Project already Exists.',
				4: 'Book || Customer is Missing.',
				5: 'Incorrect page sequence.',
				6: 'Issue XML already Exists.',
				7: 'Empty Value Found !',
				8: 'PDF is not uploaded for Non-Kriya Articles.'

			},
			//prompt message
			503: {
				1: 'Please try again or contact support.',
				2: 'Please correct RO XML and re-upload.',
				3: 'Please upload PDF and try again',
				4: 'already processed for this Issue. Pls export Individual PDF from List View'
			}
		},
		successCode: {
			200: {
				1: 'Upload Succeeded',
				2: 'Issue Created',
				3: 'All Changes Saved',
				4: 'Meta Info Saved',
				5: 'Article XML Saved'
			}
		},
		export:{
			exportToPDF: function(param, targetNode){
				var metaObj = $('#manuscriptsData').data('binder')['container-xml'].meta;
				var firstMsCid = $('#tocContentDiv [data-section="chapters"] .msCard.card:eq(0)').attr('data-uuid');
				var cid = $('#rightPanelContainer').attr('data-c-rid');

				if(param && param.cid){
					cid = param.cid;
				}
				var uid = $('#rightPanelContainer').attr('data-r-uuid');
				if(param && param.uid){
					uid = param.uid
				}
				var doinum =  $('#rightPanelContainer').attr('data-dc-doi');
				var draftFlag = false
				if(metaObj.issue && metaObj.issue['_attributes'] && metaObj.issue['_attributes']["issue-type"] && metaObj.issue['_attributes']["issue-type"].toLowerCase() == 'draft'){
					draftFlag = true
				}	
				if (param && param.draftFlag){
					draftFlag = param.draftFlag
				}	
				//if draft flag is true and index is 0, call exportModofied with param.type='draft' to export all manuscript
				var processAllArticles = false;
				if(firstMsCid == uid && draftFlag == true && processAllArticles == true){
					param.type = 'draft'
					eventHandler.flatplan.export.exportModified(param, 0);
				}
				else{
					//var articleObj = $('[data-c-id="' + cid + '"]').data('data');
					var articleObj;
					if($('[data-uuid ="' + uid + '"]').length >0){
						articleObj = $('[data-uuid ="' + uid + '"]').data('data');
					}else if ($('.msCard.highlightCard').length > 0){
						articleObj = $('.msCard.highlightCard').data('data');
					}else {
						//unable to proof / preflight doi
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
						return false;
					}
					if(articleObj['_attributes']['type'] == "blank"){
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
						return false;
					}

					var doi = articleObj['_attributes']['id'];
					var ms_id = articleObj['_attributes']['ms-id'] ? articleObj['_attributes']['ms-id'] : articleObj['_attributes']['id']
					var customer = $('#filterCustomer .filter-list.active').attr('value');
					var project = $('#filterProject .filter-list.active').attr('value');
					// need to store data before getting this valu - pending - jai
					var projectID = $('#projectVal').attr('data-project-id')
					var fpage = articleObj['_attributes']['fpage'];
					var lpage = articleObj['_attributes']['lpage'];

					var userName = userData.kuser.kuname.first;//$('.userinfo:first').attr('data-name');
					var userRole = 'typesetter';

					if (param && param.doi){
						doi = param.doi;
					}
					var proofType = 'print';
					var processType = 'InDesignSetter';
					var exportAllarticleFlag = 'false';
					if (param && param.type) proofType = param.type;
					if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
					if (param && param.processType) processType = param.processType;
					parameters = {
						"client"     : customer,
						"project"    : project, 
						"idType"     : "doi", 
						"id"         : doi, 
						"msid"      : ms_id,
						//"processType": "InDesignSetter", 
						"processType": processType, 
						"proof"      : proofType, 
						//"volume"     : metaObj.volume._text, 
						//"issue"      : metaObj.issue._text, 
						"fpage"      : fpage,
						"lpage"      : lpage,					
						"userName"   : userName, 
						"userRole"   : userRole,
						"uuid"       : uid,
						"exportAllArticles": exportAllarticleFlag
					}
					if(projectID && projectID!=''){
						parameters['project-id'] = projectID
					}
					if(metaObj.volume){
						parameters.volume = metaObj.volume._text;
					}
					if(metaObj.issue){
						parameters.issue = metaObj.issue._text;
					}
					var colorObj  = articleObj['color']
					
					var dataColor = '';
					if(colorObj){
						if (colorObj.constructor.toString().indexOf("Array") == -1) {
							colorObj = [colorObj];
						}
						colorObj.forEach(function (color) {
							if(color['_attributes']['color'] == '1'){
								dataColor += color['_attributes']['ref-id']+',';
							}
						})
					}

					dataColor = dataColor.replace(/\,$/, '');
					if(dataColor){
						parameters['data-color'] = dataColor;
					}
					
					var title = '';
					var volume = metaObj.volume ? metaObj.volume._text :''
					var issue = metaObj.issue ? metaObj.issue._text :''
					var fileName = $('#issueVal').attr('data-file');
					if(doi){
						title = 'Proofing ' + doi;
						parameters.issueMakeup = 'true';
						if (fileName && (/(draft)/gi.test(fileName))) {
						parameters.draftIssue = 'true';
						}
					}
					if(articleObj['_attributes']['type'] == 'cover'){
						parameters.pheripherals = 'cover';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_cover';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_cover';
						}
						title = 'Proofing Cover';	
					}
					else if(articleObj['_attributes']['type'] == 'table-of-contents' || articleObj['_attributes']['type'] == 'toc'){
						parameters.pheripherals = 'toc';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_toc';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_toc';
						}
						title = 'Proofing Toc';
					}
					else if(articleObj['_attributes']['type'] == 'index'){
						parameters.index = 'true';
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi;
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_index';
						}
						else{
							parameters.id = project + '_'  + doi;
							parameters.doi = project + '_index';
						}
						title = 'Proofing Index';
					}
					else if(articleObj['_attributes']['type'] == 'advert'  || articleObj['_attributes']['type'] == 'advert_unpaginated'){
						var currRegion = targetNode.closest('div').parent().attr('data-region')
						parameters.pheripherals = 'advert';
						parameters.advertID = doi;
						if(volume && issue){
							parameters.id = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
						}
						else{
							parameters.id = project + '_'  + doi + '_' + currRegion;
						}
						title = 'Proofing Advert_' + doi + '_' + currRegion;
						parameters.region = currRegion;
						if(volume && issue){
							parameters.doi = project + '_' + metaObj.volume._text + '_' + metaObj.issue._text + '_' + doi + '_' + currRegion;
						}
						else{
							parameters.doi = project + '_' + doi + '_' + currRegion;
						}
					}
					if(processType == 'PDFPreflight'){
						parameters.doi = parameters.doi ? parameters.doi : doi
						title = 'Preflight for '+ parameters.doi;
						parameters.pdfLink = param.pdfLink
						parameters.profile = param.profile;
						var type = articleObj['_attributes']['type']; 
					//if type is manuscript and exist dataColor change profile to cmyk
						if (type == 'manuscript' && dataColor && param.profile == 'bw'){
							parameters.profile = 'cmyk'
						}
						else if (type == 'manuscript' && dataColor && param.profile == 'bw+1'){
							parameters.profile = 'cmyk+1'
						}
					}
					
					var notifyObj = new PNotify({
						title : title,
						text: '<div class="progress"><div class="determinate" style="width: 0%"></div><span class="progress-text">Sending request..</span></div>',
						hide: false,
						type: 'success',
						buttons: { sticker: false },
						addclass: 'export-pdf',
						icon: false
					});
					var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
					var notifyContainer = notifyObj.container[0];
					$(notifyContainer).attr('data-c-nid', cid);
					$(notifyContainer).attr('data-c-nuid', uid);
					
					$.ajax({
						type: 'POST',
						url: "/api/pagination",
						data: parameters,
						crossDomain: true,
						success: function(data){
							if (data != '' && data.status.code == '200' && (/success/i.test(data.status.message))){
								eventHandler.flatplan.export.getJobStatus(customer, data.message.jobid, notifyObj, userName, userRole, doi, param);
							}
							else if(data == ''){
								notifyText.innerHTML = 'Failed..';
								if(param && param.error && typeof(param.error) == "function"){
									param.error();
								}
							}else{
								notifyText.innerHTML = data.status.message;
								if(param && param.error && typeof(param.error) == "function"){
									param.error();
								}
							}
						},
						error: function(error){
							notifyText.innerHTML = 'Failed..';
							if(param && param.error && typeof(param.error) == "function"){
								param.error();
							}
						}
					});
				}	
			},
			getJobStatus: function(customer, jobID, notifyObj, userName, userRole, doi, param){
				var notifyText = $(notifyObj.text_container[0]).find('.progress-text')[0];
				var progressBar = $(notifyObj.text_container[0]).find('.determinate');
				var notifyTitle = notifyObj.title_container[0];
				var notifyContainer = notifyObj.container[0];

				$.ajax({
					type: 'POST',
					url: "/api/jobstatus",
					data: {"id": jobID, "userName":userName, "userRole":userRole, "doi":doi, "customer":customer},
					crossDomain: true,
					success: function(data){
						if(data && data.status && data.status.code && data.status.message && data.status.message.input && data.status.message.input.uuid){
							var proofUUID = data.status.message.input.uuid
							if($('*[data-uuid="'+proofUUID+'"]').length > 0){
								if(data && data.status && data.status.code && data.status.message && data.status.message.status){
									var status = data.status.message.status;
									var code = data.status.code;
									var currStep = 'Queue';
									if(data.status.message.stage.current){
										currStep = data.status.message.stage.current;
									}
									if(data.status.message.progress){
										var barWidth = data.status.message.progress/3;
										if(data.status.message.stage.current == "collectfiles"){
											barWidth = barWidth+'%';
										}else if(data.status.message.stage.current == "proofing"){
											barWidth = (barWidth+33.3)+'%';
										}else if(data.status.message.stage.current == "uploadPDF"){
											barWidth = (barWidth+66.6)+'%';
										}

										progressBar.css('width', barWidth);
									}
									var loglen = data.status.message.log.length;    
									var process = data.status.message.log[loglen-1];
									if (/completed/i.test(status)){
										var loglen = data.status.message.log.length;	
										var process = data.status.message.log[loglen-1];
										var processType = param.processType;
										var proofDoi, proofId;
										var configData = $('#manuscriptsData').data('config');
										if(data.status.message.input && data.status.message.input.doi){
											proofDoi = data.status.message.input.doi
										}
										if(data.status.message.input && data.status.message.input.id){
											proofId = data.status.message.input.id
										}
										//notifyText.innerHTML = process;
										if(processType == 'PDFPreflight'){
											var preflightLink = param.pdfLink;
											preflightLink = preflightLink.replace('.pdf','_preflight.pdf')
										
											//display preflight link to user
											notifyTitle.innerHTML = preflightLink;
											notifyObj.remove();
													//var nid = $(notifyContainer).attr('data-c-nid');
											var nid = proofUUID
											if(nid && nid !='' && $('[data-uuid="' + nid + '"]').length >0){
												//set preflight flag as true
												var dataObj = $('[data-uuid="' + nid + '"]').data('data');
													dataObj['_attributes']['data-preflight'] = 'true';
												$('[data-uuid="' + nid + '"]').attr('data-preflight','true');
												var fileFlag = false;
												// save preflight pdf path in xml
												// for advert update file name in region
												//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
												if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated' ) ){
													var currRegion = targetNode.closest('div').parent().attr('data-region')
													currRegion = currRegion.toLowerCase();
													var fileFlag = false;
													var regObj = dataObj['region'];
													if(regObj){
														if (regObj.constructor.toString().indexOf("Array") == -1) {
															regObj = [regObj];
														}
														regObj.forEach(function (region) {
															if(region['_attributes']['type'] == currRegion){
																//region['_attributes']['path'] = pdfLink;
																var fileObj = region['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	if(file['_attributes'] &&  file['_attributes']['type'] && file['_attributes']['type'] == 'preflightPdf'){
																		file['_attributes']['path'] = preflightLink;
																		fileFlag = true;
																	}
																})
																if(fileFlag == false){
																	var file = ({"_attributes":{"type": "preflightPdf","path": preflightLink}})
																	fileObj.push(file)
																	region['file'] = fileObj;
																}
															}
														})
														eventHandler.flatplan.action.saveData();
														// save the preflight link in li to download
														if(targetNode){
														targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('target', '_blank');
														targetNode.closest('div').find('ul li[data-preflight="true"] a').attr('href', preflightLink)
														}	
													}
												}
												else { //else if(dataObj['file']){
													var fileFlag = false
													var fileObj = []
													if(dataObj['file']){
														fileObj = dataObj['file'];
														if (fileObj.constructor.toString().indexOf("Array") == -1) {
															fileObj = [fileObj];
														}
														fileObj.forEach(function (file) {
															var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
															if(pdfType && pdfType == 'preflightPdf'){
																file['_attributes']['path'] = preflightLink;
																fileFlag = true;
															}
														})
													}
													if(fileFlag == false)	{
														var file = ({"_attributes":{"type": "preflightPdf", "path": preflightLink}})
														fileObj.push(file)
														dataObj['file'] = fileObj;
													}
												}
												// save the preflight link in li to download
												if($('#msDataDiv[data-r-uuid="'+nid+'"]').length >0){
													$('#msDataDiv[data-r-uuid="'+nid+'"').find('ul li[data-preflight="true"] a').attr('target', '_blank');
													$('#msDataDiv[data-r-uuid="'+nid+'"').find('ul li[data-preflight="true"] a').attr('href', preflightLink);	
												}
										
												eventHandler.flatplan.action.saveData();
												//var exportAllarticleFlag = 'false';
												//if (param && param.exportAllArticles) exportAllarticleFlag = param.exportAllArticles;
												/*if(exportAllarticleFlag == 'false'){
													//send request to online pdf
													if(nid){
														param.uid = nid
													}
													param.processType = 'InDesignSetter';
													param.type = 'online';
													eventHandler.pdf.export.exportToPDF(param,targetNode);
												}*/
											}		
										}
										else{
											notifyTitle.innerHTML = 'PDF generation completed.';
											var proofType = 'print';
											if (param && param.type) proofType = param.type;
									
											var pdfLink = $(process).attr('href');
											//pdfLink =pdfLink.replace('http:','https:');
											if (window.location.protocol == 'https:') {
												pdfLink = pdfLink.replace('http:','https:');
											}
											// get endPage and PageCount from JobManager
											var endPage,pageCount;
											var datajsonObj = $(process).attr('data-json');
											if (pdfLink && processType == 'InDesignSetter'){
												notifyObj.remove();
												var nid = proofUUID;
												if(nid && nid !='' &&  $('*[data-uuid="'+ nid+'"]').length > 0){
													// if prooftype is print update the link in object , update download url 
													if($('#rightPanelContainer[data-r-uuid="'+nid+'"]').length > 0){
														var proofingType = 'online';
														if (proofType == 'print') {
															proofingType = proofType;
															$('#rightPanelContainer[data-r-uuid="'+nid+'"]').find('object').attr('data', pdfLink);
														}
														$('#rightPanelContainer[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="' + proofingType + '"] a').attr('target', '_blank');
														$('#rightPanelContainer[data-r-uuid="'+nid+'"]').find('ul li[data-proofType="' + proofingType + '"] a').attr('href', pdfLink);
													}
													if(nid){
														var dataObj = $('[data-uuid="' + nid + '"]').data('data');
														var currentDoi = dataObj._attributes.id;
														dataObj._attributes['data-proof-doi'] = proofDoi;
														//for advert/filler, get the crrent region and match the current region with xml and update the path accordingly
														if((dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert') || (dataObj['_attributes'] && dataObj['_attributes']['type'] == 'advert_unpaginated')){
															var currRegion = targetNode.closest('div').parent().attr('data-region')
															currRegion = currRegion.toLowerCase();
															var fileFlag = false;
															var regObj = dataObj['region'];
															if(regObj){
																if (regObj.constructor.toString().indexOf("Array") == -1) {
																	regObj = [regObj];
																}
																regObj.forEach(function (region) {
																	if(region['_attributes']['type'] == currRegion){
																		//region['_attributes']['path'] = pdfLink;
																		region['_attributes']['data-proof-doi'] = proofDoi;
																		var fileObj = region['file'];
																		if (fileObj.constructor.toString().indexOf("Array") == -1) {
																			fileObj = [fileObj];
																		}
																		fileObj.forEach(function (file) {
																			if(file['_attributes'] &&  file['_attributes']['proofType'] && file['_attributes']['proofType'] == proofType){
																				file['_attributes']['path'] = pdfLink;
																				fileFlag = true;
																			}
																		})
																		if(fileFlag == false)	{
																			var file = ({"_attributes":{"type": "pdf","proofType":proofType, "path": pdfLink}})
																			fileObj.push(file)
																			region['file'] = fileObj;
																		}
																	}
																})
																eventHandler.flatplan.action.saveData();
																// save the pdf link in li to download
																if(proofType == 'print' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="print"] a').attr('href', pdfLink);
																}
																else if(proofType == 'online' && targetNode) {
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('target', '_blank');
																	targetNode.closest('div').find('ul li[data-prooftype="online"] a').attr('href', pdfLink);
																}
															}
														}
														//loop through each file and update pdflink in path if proofType matches 
														else{//dataObj['file'] 
															dataObj._attributes['data-proof-doi'] = proofDoi;
															var fileFlag = false
															var fileObj = []
															if(dataObj['file']){
																fileObj = dataObj['file'];
																if (fileObj.constructor.toString().indexOf("Array") == -1) {
																	fileObj = [fileObj];
																}
																fileObj.forEach(function (file) {
																	var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																	var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																	if(currProofType == proofType && pdfType != 'preflightPdf'){
																		file['_attributes']['path'] = pdfLink;
																		fileFlag = true
																	}
																})
															}
															if(fileFlag == false)	{
																var file = ({"_attributes":{"type": "pdf","proofType":proofType, "path": pdfLink}})
																fileObj.push(file)
																dataObj['file'] = fileObj;
															}
														}	
														if(datajsonObj){
															datajsonObj = datajsonObj.replace(/\'/g,'"')
															var datajson = JSON.parse(datajsonObj)
															endPage = datajson['LPAGE']
															pageCount = datajson['PAGE_COUNT']
															//update the  page details of figures 
															var colorObj  = dataObj['color']
															if(colorObj){
																if (colorObj.constructor.toString().indexOf("Array") == -1) {
																	colorObj = [colorObj];
																}
																for(var a in datajson){
																	if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																		var pageNum = datajson[a];
																		a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/,'$1$2')	
																		colorObj.forEach(function (color) {
																			if(color['_attributes']&& color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a ){
																				color['_attributes']['data-page-num'] = pageNum
																			}
																		})
																	}
																}
															}
															var runOnPagesDetails = datajson['RunOnPagesDetails']
															//if endpage, update endpage in issue xml,  
															//update page range in UI and update the following article page number if there is change, call updatepage number function
															if(endPage){
																if ((!dataObj['_attributes']['data-run-on']) || dataObj['_attributes']['data-run-on'] != 'true'){
																//check the config and match the prooftype. if the prooftype is mentioned in config update pagenumber
																//if attrib is not available in config, update pagenumber
																//exclude pagenumber update when follow-on is 1
																	if ((configData['page']) && (configData['page']._attributes['pageNumberProofType'])){
																		var values =  configData['page']._attributes['pageNumberProofType'].toLowerCase()
																		values = values.split(',');
																		if (values.constructor.toString().indexOf("Array") < 0) {
																			values = [values];
																		}
																		values.forEach(function (value) {
																			if(value == proofType){
																				dataObj['_attributes']['lpage'] = endPage;
																				dataObj['_attributes']['pageExtent'] = pageCount;
																				var seqId = $('[data-uuid="' + nid + '"]').attr('data-c-id')
																				eventHandler.flatplan.components.updatePageNumber(seqId,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
																			}
																		})
																	}
																	else{
																		dataObj['_attributes']['lpage'] = endPage;
																		dataObj['_attributes']['pageExtent'] = pageCount;
																		var seqId = $('[data-uuid="' + nid + '"]').attr('data-c-id')
																		eventHandler.flatplan.components.updatePageNumber(seqId,dataObj['_attributes']['fpage'],dataObj['_attributes']["grouptype"]);
																	}
																}
															}
															//if current artcile has run-on articles - update pdflink in all run-on articles, and update the f-page, l-page for run-on articles
															if(runOnPagesDetails){
																var currArtObj = $('[data-uuid="' + nid + '"]');
																var mainArtDoi =$(currArtObj).attr('data-doi')
																var subarticles = currArtObj.nextUntil('[data-c-id][data-type="manuscript"][data-run-on!="true"]')
																subarticles.each(function(){
																	var currNode = $(this).data('data');
																	if(currNode){
																		var doi = currNode._attributes.id
																		//if(doi == currNode._attributes.id){
																		if (doi && $('[data-c-id][data-doi="'+ doi +'"][data-runon-with="'+ mainArtDoi +'"]').length > 0) {
																			var followOnAricle = $('[data-c-id][data-doi="'+ doi +'"][data-runon-with="'+ mainArtDoi +'"]').data('data')
																			if(followOnAricle['file']){
																				var fileObj = followOnAricle['file'];
																				if (fileObj.constructor.toString().indexOf("Array") == -1) {
																					fileObj = [fileObj];
																				}
																				// update pdflink based on proofType
																				fileObj.forEach(function (file) {
																					var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : 'print'
																					var pdfType = file['_attributes']['type'] ? file['_attributes']['type'] : ''
																					if(currProofType == proofType && pdfType != 'preflightPdf'){
																						file['_attributes']['path'] = pdfLink;
																					}
																				})
																			}
																			// update the fpage,lpage, figure page details  for run-on articles
																			if( runOnPagesDetails[doi]){
																				followOnAricle['_attributes']['fpage'] = runOnPagesDetails[doi]['f-page']
																				followOnAricle['_attributes']['lpage'] = runOnPagesDetails[doi]['l-page']
																				if(followOnAricle['color']){
																					for (var a in runOnPagesDetails[doi]){
																						if (/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/gi.test(a)) {
																							var pageNum = runOnPagesDetails[doi][a];
																							a = a.replace(/(BLK_)([(A-Z|a-z)]+[0-9]+)(_.*)/,'$1$2')	
																							var runOnColorObj = followOnAricle['color']
																							if(runOnColorObj){
																								if (runOnColorObj.constructor.toString().indexOf("Array") == -1) {
																									runOnColorObj = [runOnColorObj];
																								}
																								runOnColorObj.forEach(function (color) {
																									if(color['_attributes']&& color['_attributes']['ref-id'] && color['_attributes']['ref-id'] == a ){
																										color['_attributes']['data-page-num'] = pageNum
																									}
																								})
																							}
																						}
																					}
																				}
																			}	
																		}
																		
																		//}
																	}
																});
															}
														}
														//remove data-modified attrib from xml,UI and remove title - pageModified
														if (dataObj._attributes['data-modified'] && (dataObj._attributes['data-modified'].toLowerCase() == 'true')){
															$('[data-uuid="' + nid + '"]').removeAttr('data-modified','true');
															$('[data-uuid="' + nid + '"]').find('.pageRange').removeAttr('title', 'Page is Modified');
															$('[data-uuid="' + nid + '"]').find('.pageRange').removeClass('pageModified');
															delete dataObj._attributes['data-modified'];
														}
														//set preflight flag as false
														dataObj['_attributes']['data-preflight'] = 'false';
														$('[data-uuid="' + nid + '"]').attr('data-preflight','false');
														eventHandler.flatplan.action.saveData();
													}
													//	when proof type is print and processType is  InDesignSetter, send request to Preflight
													if( proofType == 'print' && param.processType == 'InDesignSetter'){
														param.processType = 'PDFPreflight';
														param.pdfLink = pdfLink;
														if(nid){
															param.uid = nid
														}
														//get preflight profile from config
														var type = dataObj['_attributes']['type']; 
														var preflightProfile = '';
														if(configData["binder"][type] && configData["binder"][type]['_attributes'] && configData["binder"][type]['_attributes']['data-preflight-profile']){
															preflightProfile = configData["binder"][type]['_attributes']['data-preflight-profile']
														}
														if(preflightProfile){
															param.profile = preflightProfile;
															eventHandler.flatplan.export.exportToPDF(param,targetNode);
														}
													}	
												}
											}
											if(param && param.success && typeof(param.success) == "function"){
												param.success();
											}
										}
									}	
									else if (/failed/i.test(status) || code == '500'){
										notifyText.innerHTML = process;
										if(param.processType == 'InDesignSetter'){
											notifyTitle.innerHTML = 'PDF generation failed.';
											
										}
										else{
											notifyTitle.innerHTML = 'Preflight failed.';
										}
										if(param && param.error && typeof(param.error) == "function"){
											param.error();
										}
										//$('#'+notificationID+' .kriya-notice-body').html(process);
										//$('#'+notificationID+' .kriya-notice-header').html('PDF generation failed. <i class="icon-close" onclick="kriya.removeNotify(this)">&nbsp;</i>');
									}
									else if (/no job found/i.test(status)){
										notifyText.innerHTML = status;
										//$('#'+notificationID+' .kriya-notice-body').html(status);
									}
									else{
										if(data.status.message.displayStatus){
										notifyText.innerHTML = data.status.message.displayStatus;
										}
										//$('#'+notificationID+' .kriya-notice-body').html(data.status.message.displayStatus);
										var loglen = data.status.message.log.length;	
										if(loglen > 1){
											var process = data.status.message.log[loglen-1];
											if (process != '') notifyText.innerHTML = process;
											///if (process != '') $('#'+notificationID+' .kriya-notice-body').html(process);
										}
										setTimeout(function() {
											eventHandler.flatplan.export.getJobStatus(customer, jobID, notifyObj, userName, userRole, doi, param);
											//eventHandler.menu.export.getJobStatus(customer, jobID, notificationID, userName, userRole, doi);
										}, 1000 );
									}
								}	
								else{
									notifyText.innerHTML = 'Failed';
									//$('#'+notificationID+' .kriya-notice-body').html('Failed');
								}
							}
							else{
								//uuid is not present in current xml, so close the notify object
								notifyObj.remove();
							}
						}
						else{
							//uuid is not returned from job manager 
							notifyObj.remove();
						}
					},
					error: function(error){
						if(param && param.error && typeof(param.error) == "function"){
							param.error();
						}
					}
				});
			},
			exportModified: function(param, index){
				return false;
				index = (index)?index:0;
				var thisObj = this;
				//var condition, if param.type is draft, condition is [data-type="manuscript"], else condition is [data-modofied="true"]
				var condition;
				if((param && param.type && param.type=='draft') || (param && param.exportType && param.exportType=='proofAllArticles')) {
					condition = ' [data-type="manuscript"][data-siteName!="Non-Kriya"]'	
				}
				else
				{
					condition = ' [data-modified="true"]'
				}
				var cid = $('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').attr('data-c-id');
				var uid = $('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').attr('data-uuid');
				
				var articleObj = $('[data-c-id="' + cid + '"]').data('data');

				var exportParam = {
					'draftFlag' :'false',
					'cid' : cid,
					'uid' : uid,
					'exportAllArticles': 'true',
					success: function(){
						$('#tocContainer .bodyDiv ' + condition +':eq(' + index + ')').removeAttr('data-modified', 'true');
						delete articleObj._attributes['data-modified'];
						eventHandler.flatplan.action.saveData();

						if($('#tocContainer .bodyDiv ' + condition +':eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv ' + condition + '').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					},
					error: function(){
						if($('#tocContainer .bodyDiv ' + condition +':eq(' + (index+1) + ')').length > 0){
							thisObj.exportModified(param, index+1);
						}else if($('#tocContainer .bodyDiv ' + condition +'').length == index+1){
							if(param && param.success && typeof(param.success) == "function"){
								param.success();
							}
						}
					}
				}
				thisObj.exportToPDF(exportParam);
			},
			exportAllArticles: function(param){
				// export all manuscript based on proof Type and only when exportAllArticles flag is true
				var proofType = param.type
				param.exportAllArticles = 'true';
				var exportFlag = 'true'
				if(proofType == 'print'){
					exportFlag = $('#manuscriptsData').data('binder')['container-xml'].contents._attributes ? $('#manuscriptsData').data('binder')['container-xml'].contents._attributes.exportAllArticlesPrint : 'false'
				}
				else{
					exportFlag = $('#manuscriptsData').data('binder')['container-xml'].contents._attributes ? $('#manuscriptsData').data('binder')['container-xml'].contents._attributes.exportAllArticlesOnline : 'false'
				}
				if(exportFlag == 'false'){//true
					param.exportType = 'proofAllArticles'
					console.log(param)

					// check if any non-kriya article is avilable. If so, check whether PDF is uploaded 
					var pdfFlag = true
					var nonKriyaArticles = $('#tocBodyDiv div[data-c-id][data-type="manuscript"][data-siteName="Non-Kriya"]');
						nonKriyaArticles.each(function(){
							var currArticleNode = $(this);
							var currArticleNodeID = parseInt(currArticleNode.attr('data-c-id'));
							var  currArticleNodeData = currArticleNode.data('data');
							var currArticleNodeDOI = currArticleNodeData._attributes.id
							var fileObj = currArticleNodeData.file;
							fileObj.forEach(function (file) {
								var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
									if(currProofType == param.type){
										var pdfPath = file['_attributes']['path'];
										if(pdfPath == ''){
											pdfFlag = false
										}
									}
								})
						});
					// if pdf is not uploaded for non-kriya article, prompt user to upload pdf
					if(pdfFlag == false){
						//showMessage({ 'text': param.type + '  ' + eventHandler.messageCode.errorCode[502][8] + ' ' + eventHandler.messageCode.errorCode[503][3], 'type': 'error' ,'hide':false});
					}
					//export all articles when pdf for techset articles are uploaded
					else{
						//set exportAllarticle flag as false
						if(proofType == 'print'){
							$('#manuscriptsData').data('binder')['container-xml'].contents._attributes.exportAllArticlesPrint = 'true'
						}
						else{
							$('#manuscriptsData').data('binder')['container-xml'].contents._attributes.exportAllArticlesOnline = 'true'
						}
						eventHandler.flatplan.action.saveData();
						eventHandler.flatplan.export.exportModified(param, 0);
						
					}
				}
				else{
					//if exportFlag is true
					//showMessage({ 'text':'Export All PDFs for ' + proofType + ' is '+ eventHandler.messageCode.errorCode[503][4], 'type': 'error','hide':false });
					return false;
				}	
			}
		},
		merge: {
			mergePDF: function (param, targetNode, checkModify) {
				if ($('#tocContentDiv .sectionDataChild .pageRange.incorrectPage').length > 0) {
					$('.save-notice').text(eventHandler.messageCode.errorCode[502][5]);
					//showMessage({ 'text': eventHandler.messageCode.errorCode[502][5], 'type': 'error','hide':false });
					return false;
				}

				/*if($('#tocContentDiv [data-modified="true"]').length > 0 && checkModify != 'false'){
					(new PNotify({
	                    title: 'Confirmation Needed',
	                    text: 'Some articles was modified do you want to proof?',
	                    hide: false,
	                    confirm: { confirm: true },
	                    buttons: { closer: false, sticker: false },
	                    history: { history: false },
	                    addclass: 'stack-modal',
	                    stack: { 'dir1': 'down', 'dir2': 'right', 'modal': true }
                	})).get()
                    .on('pnotify.confirm', function () {
                      eventHandler.flatplan.export.exportModified({
                      	success: function(){
                      		eventHandler.flatplan.merge.mergePDF('', targetNode, 'false');	
                      	}
                      }); 
                    })
                    .on('pnotify.cancel', function () {
                    	eventHandler.flatplan.merge.mergePDF('', targetNode, 'false');
                    });
                    return false;
				}*/

				var pdfLinks = [];
				//$('#tocContainer [data-c-id]').each(function(){
				//epage articles to be excluded while binding 
				$('#tocContentDiv [data-c-id][data-grouptype !="epage"]').each(function(){	
					var data = $(this).data('data');
					//loop through each file and update pdflink in path if proofType matches 
					if(data && data['file']){
						var fileObj = data['file'];
						if (fileObj.constructor.toString().indexOf("Array") == -1) {
							fileObj = [fileObj];
						}
						fileObj.forEach(function (file) {
							var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
							if(currProofType == 'print'){
								var pdfPath = file['_attributes']['path'];
								if(pdfPath){
									if(!pdfPath.match(/http(s)?:/)){
										pdfPath = 'http://'+ window.location.host + pdfPath;
									}else{
										pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
									}
									pdfLinks.push(pdfPath);
								}
							}
						})
					}
					else if(data && data['region']){
						var regObj = data['region']
						if(regObj){
							if (regObj.constructor.toString().indexOf("Array") == -1) {
								regObj = [regObj];
							}
							regObj.forEach(function (region) {
								if(region['file']){
									//region['_attributes']['path'] = pdfLink;
									var fileObj = region['file'];
									if (fileObj.constructor.toString().indexOf("Array") == -1) {
										fileObj = [fileObj];
									}
									fileObj.forEach(function (file) {
										var currProofType = file['_attributes']['proofType'] ? file['_attributes']['proofType'] : ''
										if(currProofType== 'print'){
											var pdfPath = file['_attributes']['path'];
											if(pdfPath){
												if(!pdfPath.match(/http(s)?:/)){
													pdfPath = 'http://'+ window.location.host + pdfPath;
												}
												else{
													pdfPath = pdfPath.replace(/http(s)?:/, 'http:');
												}
												pdfLinks.push(pdfPath);
											}
										}
									})
								}
							})
						}
					}
				});
				
				var customer = $('#filterCustomer .filter-list.active').attr('value');
				var project = $('#filterProject .filter-list.active').attr('value');
				var metaObj = $('#manuscriptsData').data('binder')['container-xml'].meta;
				
				var notifyObj = new PNotify({
					title : 'Combining PDF',
					text: 'Processing request..',
					hide: false,
					type: 'success',
					buttons: { sticker: false },
					icon: false
				});
				var notifyText = $(notifyObj.text_container[0]);
				
				$.ajax({
					type: "POST",
					url: "/api/mergepdf",
					data: {
						"customer": customer,
						"project" : project,
						"volume"  : metaObj.volume ? metaObj.volume._text : '', 
						"issue"   : metaObj.issue ? metaObj.issue._text : '',
						"pdfLinks": pdfLinks
					},
					xhr: function () {
						var xhr = new window.XMLHttpRequest();
						xhr.addEventListener("progress", function (evt) {
							var resText = evt.target.responseText;
							if(resText){
								resText = resText.split(/\n/).pop();
								if(!resText.match(/File_path/)){
									notifyText.html(resText);
								}else{
									notifyText.html('Completed.');
								}
							}
						}, false);
						return xhr;
					},
					success: function (data) {
						if(data){
							var filePath = data.split(/\n/).pop();
							if(filePath.match(/File_path/)){
								filePath = filePath.replace('File_path:', '');
								filePath += '?method=server';
								window.open(filePath, '_blank');
							}
						}
					},
					error: function (xhr, errorType, exception) {
						$('.save-notice').text(eventHandler.messageCode.errorCode[501][5]);
						//showMessage({ 'text': eventHandler.messageCode.errorCode[501][5] + ' ' + eventHandler.messageCode.errorCode[503][1], 'type': 'error','hide':false });
						return null;
					}
				});
			}
		}
    }
})(eventHandler || {});
