 var kriyaEditor = function() {
	//default settings
	var settings = {};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments,
				value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

/**
*	Extend eventHandler function with event handling
*/
(function (kriyaEditor) {
	settings = kriyaEditor.settings;
	var timer;
	kriyaEditor.user = {};
	kriyaEditor.user.name = $('#headerContainer .userProfile .username').text();
	if(kriya.config.content.role=='publisher'){
		kriyaEditor.user.name = kriyaEditor.user.name+' ('+kriya.config.content.customer.toUpperCase()+')';
	}else{
		kriyaEditor.user.name = kriyaEditor.user.name+' ('+kriya.config.content.role.toUpperCase()+')';
	}
	kriyaEditor.user.id   = $('#headerContainer .userProfile .username').attr('data-user-id');

	kriyaEditor.init = {
		editor: function(dataNode) {
			//kriyaEditor.init.saveBackup();
			return true;
		},
		saveBackup: function(){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return true;
			}

			//Create snap shot when page load - jagan
			var content = $('div.WordSection1')[0].outerHTML
			if($('#navContainer #queryDivNode').length){
				content += $('#navContainer #queryDivNode')[0].outerHTML
			}
			var parameters = {
				'customer': kriya.config.content.customer,
				'project': kriya.config.content.project,
				'doi': kriya.config.content.doi,
				'content': encodeURIComponent(content),
				'fileName' : 'page_load'
			};
			kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
				console.log('Data Saved');
			});

			setInterval(function(){
				if(keeplive && keeplive.getUserStatus() != 'in-active'){
					var content = $('div.WordSection1')[0].outerHTML
					if($('#navContainer #queryDivNode').length){
						content += $('#navContainer #queryDivNode')[0].outerHTML
					}
					var parameters = {
						'customer': kriya.config.content.customer,
						'project': kriya.config.content.project,
						'doi': kriya.config.content.doi,
						'content': encodeURIComponent(content)
					};
					kriya.general.sendAPIRequest('createsnapshots',parameters,function(res){
						console.log('Data Saved');
					});
				}
			},600000);
		},
		trackArticle: function(){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return true;
			}
			setInterval(function(){
				if ($('#welcomeContainer:visible').length == 0){
					var parameters = {
						'customerName': kriya.config.content.customer,
						'projectName': kriya.config.content.project,
						'doi': kriya.config.content.doi,
						'skipElasticUpdate': 'true'
					};
				var d = new Date();
				var currDate = d.getUTCFullYear() +'-'+  ("0" + (d.getUTCMonth()+1)).slice(-2) +'-'+ ("0" + d.getUTCDate()).slice(-2);
				var currTime = ("0" + d.getUTCHours()).slice(-2) +':'+ ("0" + d.getUTCMinutes()).slice(-2) +':'+ ("0" + d.getUTCSeconds()).slice(-2);
				parameters.data = '<stage><name>' + $('#contentContainer').attr('data-stage-name') + '</name><currentstatus>in-progress</currentstatus><job-logs><log><end-date>' + currDate + '</end-date><end-time>' + currTime + '</end-time></log></job-logs></stage>';
				kriya.general.sendAPIRequest('updatedatausingxpath', parameters, function(res){});
				}
			},300000);
		},
		tracker: function(dataNode){
			if ($('#contentContainer').attr('data-state') == 'read-only'){
				return false;
			}
			kriyaEditor.init.trackeChanges();
			kriyaEditor.changeID = Math.floor(Date.now() / 1000);
			return new ice.InlineChangeEditor({
				element: dataNode,					// element to track - ice will make it contenteditable
				trackFormats: true,					// element to track - ice will make it contenteditable
				handleEvents: true,											// tell ice to setup/handle events on the `element`
				currentUser: {
					id: kriya.config.content.role + ' ' + kriyaEditor.user.id,
					name: kriyaEditor.user.name
				}, // set a user object to associate with each change
				plugins: [													// optional plugins
					'IceAddTitlePlugin',									// Add title attributes to changes for hover info
					//'powerpaste',
					{
						name: 'IceCopyPastePlugin',							// Track content that is cut and pasted
						settings: {

							preserve: 'p, span[id,class], em, strong,ul,ol,li, i, u, sup, sub',	// List of tags and attributes to preserve when cleaning a paste | Included  i, u, sup, sub, td by Raj

							preserveTrack: 'ul,ol,table' // List of tags and attributes to preserve adding track when paste
						}
					}
				],
				menu: {
					edit: {
						title: 'Edit',
						items: 'undo redo | cut copy paste | selectall'
					}
				}
			});
		},
		trackeChanges: function (){
			//$('#changesDivNode .change-div').remove();
			//Initialize the tracked elements in the changes tab
			/*var trackNodes = document.querySelectorAll('.ins, .del, .sty, .styrm, [data-track="del"], [data-track="ins"]');
			for (var t = 0;t<trackNodes.length;t++) {
				var trackNode = trackNodes[t];
				eventHandler.menu.observer.addTrackChanges(trackNode, 'added');
			}*/
		},
		loadImage: function (){
			return true;
			// stopped loading pushing-pixels to avoid saving 'pushing-pixels' instead of original images
			setTimeout(function(){
				var imageNodes = document.querySelectorAll('img');
				for (var t = 0; t < imageNodes.length; t++) {
					var imageNode = imageNodes[t];
					var orginalImage = imageNode.getAttribute('data-alt-src');
					if(orginalImage){
						//imageNode.setAttribute('src', orginalImage);
						loadImage(imageNode, orginalImage)
					}
				}
				function loadImage(imageNode, orginalImage){
					imageNode.setAttribute('src', orginalImage);
					imageNode.onerror = function() {
						imageNode.setAttribute('src', '../images/pushing-pixels.gif');
					}
				}
				createCloneContent();
			},3000);
		},
		undoRedo: function(dataNode){
			var x = document.querySelector.bind(document);
			kriyaEditor.settings.undoStack = [];
			kriyaEditor.settings.contentNode = dataNode;
			kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
			kriyaEditor.settings.changeNode = document.getElementById('navContainer');
			kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
			kriyaEditor.settings.forkedNode = false;

			var contentUndoObject = {
				container: kriyaEditor.settings.contentNode,
				oldValue : kriyaEditor.settings.contentNode.innerHTML,
				newValue : ''
			}

			//kriyaEditor.settings.undo = x('[data-tooltip="Undo"]');
			//kriyaEditor.settings.redo = x('[data-tooltip="Redo"]');
			kriyaEditor.settings.save = x('[data-tooltip="Save"]');

			stack = new Undo.Stack();
			stack.isDirty = false;
			var newValue = "";


			function stackUI() {
				/*if(stack.canRedo()){
					kriyaEditor.settings.redo.classList.remove("disabled")
				}else{
					kriyaEditor.settings.redo.classList.add("disabled")
				}*/

				if(stack.canUndo() && kriyaEditor.settings.save){
					//kriyaEditor.settings.undo.classList.remove("disabled");
					//kriyaEditor.settings.save.classList.remove("disabled");
					$('.pdfViewHeader .pdfViewMenu').removeClass('disabled');
					$('.pdfViewHeader #pdfNotice').removeClass('hidden');
				}else{
					//#348 All| Tables| Undo Redo cycle must not get affected after Accept/ Reject - Prabakatran.A(prabakaran.a@focusite.com)
					//#533 Undo is not working - ReferenceError: event is not defined - Prabakaran A(prabakaran.a@focusite.com)
					if(typeof event != 'undefined' && !$(event.currentTarget).is('[data-name="MenuBtnUndo"]')){
						kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
						kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
					}
					//End of #348
					if (kriyaEditor.settings.save){
						//kriyaEditor.settings.undo.classList.add("disabled");
						//kriyaEditor.settings.save.classList.add("disabled");
					}
				}
			}

			/*kriyaEditor.settings.undo.addEventListener("click", function () {
				var range = rangy.getSelection();
				var targetElement = range.anchorNode;
				if (stack.isDirty){
					kriyaEditor.init.addUndoLevel('undo-key', targetElement);
				}
				//#178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				if($('[data-type="popUp"]:not([data-component="contextMenu"]):not([data-component="trackChange"]):visible').length == 0 || $('[data-name="tableMenu"]:visible').length > 0)
				//End of the code #178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				stack.undo();
			});*/

			/*kriyaEditor.settings.redo.addEventListener("click", function () {
				//#178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				if($('[data-type="popUp"]:visible').length == 0 ||  $('[data-name="tableMenu"]:visible').length > 0)
				//End of the code #178 -Tables: Copy & Paste (Undo&Redo changes does not take place in tables)- Prabakaran.A-Focusteam
				stack.redo();
			});*/


			stack.changed = function () {
				stackUI();
			};
		},
		addUndoLevel: function(type, element){
			var stackObj = [];
			/*var newElement = null;
			var oldValue   = null;
			var newValue   = null;*/

			//Add content div node to undo object
			var newElement = kriyaEditor.settings.contentNode;
			var oldValue   = kriyaEditor.settings.startValue;
			var newValue   = newElement.innerHTML;

			if ((type == 'delete-key') || (type == 'character-key')) {
				stack.isDirty = true;
				stack.oldValue = oldValue;
				stack.newValue = newValue;
				if (kriyaEditor.settings.save){
					/*(stack.isDirty)?(
						kriyaEditor.settings.undo.classList.remove("disabled")
						//kriyaEditor.settings.save.classList.remove("disabled")
					):(
						kriyaEditor.settings.undo.classList.add("disabled")
						//kriyaEditor.settings.save.classList.add("disabled")
					);*/
				}
				return;
			}

			if (kriyaEditor.settings.undoStack.length > 0){
				element = kriyaEditor.settings.undoStack;
			}
			kriyaEditor.settings.undoStack = [];
			if(element && $(element).length > 0){
				//Collect the all ids of the elements that add in the stack
				//If element doesn't have ids then get closest one
				//#395-All| List| Unable to revert List to para( #364 Requirement changed added para) -Prabakaran.A-prabakaran.a@focusite.com
				//In lists, when LI is converted to paragraph, <p> is removed and inserted as new paragraph. The edited list must be saved before the inserted tag.
				var saveNodeIds = {
					'removed': [],
					'edited': [],
					'inserted': [],
					'moved': []
				};
				//End of #395-All| List| Unable to revert List to para( #364 Requirement changed added para) -Prabakaran.A-prabakaran.a@focusite.com
				var elementsToSave = element;
				$(element).each(function(i, obj){
					if($(this).closest('[id]:not(' + kriya.config.invalidSaveNodes + ')').length > 0){
						var saveNode = $(this).closest('[id]:not(' + kriya.config.invalidSaveNodes + ')');
						//to avoid saving newly inserted span, sup, sub, em, italic, underlines, bolds - JAI
						//Check the length of closest save node - jagan
						if (/^(S|E|I|U|B)/i.test($(saveNode)[0].nodeName) && $(saveNode).parent().closest('[id]:not(' + kriya.config.invalidSaveNodes + ')').length > 0){
							saveNode = $(saveNode).parent().closest('[id]:not(' + kriya.config.invalidSaveNodes + ')');
						}
						//when ids duplicate inside the save node duplicated ids replaced with new ids-kirankumar
						var ids=[];
						$($(saveNode).find('[id]')).each(function(){
							var pid=$(this).attr('id');
							if(ids.indexOf(pid) == -1){
								ids.push(pid);
							}else{
								$(saveNode).find('[id = '+pid+']').each(function(){
									$(this).attr('id',uuid.v4());
								});
							}
						});
						//Before saving the content add id to all elements which doesn't have id in save node.
						$(saveNode).find('*:not([id])').each(function(){
							$(this).attr('id',uuid.v4());
						});
						//Filter the invalid nodes to check pushToArray modify by jagan
						var pushToArray = true;
						$(elementsToSave).filter(function(){
							if(!$(this).is(kriya.config.invalidSaveNodes)){
								return this;
							}
						}).each(function(){
							if ($(this).attr('id') && $(this).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						//check if the current node is a child of a node which is already in the saving list - jai
						$.each(saveNodeIds.inserted, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						$.each(saveNodeIds.moved, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})
						$.each(saveNodeIds.edited, function(i, v){
							if ($('#' + v).find('#'+ saveNode.attr('id')).length > 0){
								pushToArray = false;
							}
						})

						//var saveNode = $(this).siblings('[id]:not(' + kriya.config.invalidSaveNodes + ')').prevObject;
						if(saveNode[0].hasAttribute('data-removed')){
							if (saveNodeIds.removed.indexOf(saveNode.attr('id')) < 0){
								saveNodeIds.removed.push(saveNode.attr('id'));
							}
						}else if(saveNode[0].hasAttribute('data-inserted')){
							if (saveNodeIds.inserted.indexOf(saveNode.attr('id')) < 0 && pushToArray){
								saveNodeIds.inserted.push(saveNode.attr('id'));
							}
						}else if(saveNode[0].hasAttribute('data-moved')){
							if (saveNodeIds.moved.indexOf(saveNode.attr('id')) < 0 && pushToArray){
								saveNodeIds.moved.push(saveNode.attr('id'));
							}
						}else{
							//#45 (5) - Bug fix - Saving all nodes, when formatting is applied to multiple nodes - Prabakaran. A(prabakaran.a@focusite.com)
                            $(saveNode).each(function(){
                                if (saveNodeIds.edited.indexOf($(this).attr('id')) < 0 && pushToArray){
                                    saveNodeIds.edited.push($(this).attr('id'));
                                }
                            })
                            //End of #45 (5)
						}
					}else{
						console.log(this);
					}
				});
				stackObj.push({
					'saveNodes': saveNodeIds
				});
			}
			//if we change any node values in this function before this line that will not saving in undo stack,when we undo after redo that changed values not comming for that changed nodes.
			newValue   = newElement.innerHTML;
			stackObj.push({
				container: newElement,
				oldValue: oldValue,
				newValue: newValue
			});

			//Adding right panel in the stack
			stackObj.push({
				container: kriyaEditor.settings.changeNode,
				oldValue : kriyaEditor.settings.contextStartValue,
				newValue : kriyaEditor.settings.changeNode.innerHTML
			});

			//add clone element in the stack
			stackObj.push({
				container: kriyaEditor.settings.cloneNode,
				oldValue : kriyaEditor.settings.cloneValue,
				newValue : kriyaEditor.settings.cloneNode.innerHTML
			});

			var EditCommand = Undo.Command.extend({
				constructor: function (undoObj) {
					this.undoObject = undoObj;
				},
				execute: function () {
					this.changed('execute');
				},
				undo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(!item.saveNodes){
							item.container.innerHTML = item.oldValue;
						}
					});
					this.changed('undo');
				},
				redo: function () {
					this.undoObject.forEach(function(item, index, array){
						if(!item.saveNodes){
							item.container.innerHTML = item.newValue;
						}
					});
					this.changed('redo');
				},
				changed: function (type) {
					this.undoObject.forEach(function(item, index, array){
						if(item.saveNodes){
							var saveNodes = [];
							//Collect the all save elemets and send it to the save function
							Object.keys(item.saveNodes).forEach(function(key) {
							    var nodes = item.saveNodes[key].map(function(val){
							    	//When undo the chnage inseted node should be removed and removed node should be insert
							    	if(type == "undo" && key == "inserted"){
							    		return $('<span id="' + val + '" data-removed="true"></span>')[0];
							    	}else if(type == "undo" && key == "removed"){
							    		$('#' + val).attr('data-inserted', 'true');
							    		return $('#' + val)[0];
							    	}else{
								    	if(key == 'removed'){
								    		return $('<span id="' + val + '" data-removed="true"></span>')[0];
								    	}else if(key == 'moved'){
								    		return $('#' + val).attr('data-moved', 'true');
								    	}else{
								    		return $('#' + val)[0];
								    	}
							    	}
								});
								saveNodes = saveNodes.concat(nodes);
							});
							kriyaEditor.init.saveHTML(saveNodes);
						}
					});
					if(type == "undo" || type == "redo"){
						citeJS.general.updateStatus();
					}
					//exclude if role is author
					if((kriya.config.content.role != 'author') && (kriya.config.content.role != 'publisher')){
						kriya.config.content.changes = true;
					}
					//to handle undo in list
					//previously after undo of first change, startValue is not updated
					kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
					kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;
					kriyaEditor.settings.contextStartValue = kriyaEditor.settings.changeNode.innerHTML;
				}
			});

			stack.execute(new EditCommand(stackObj));
			stack.isDirty = false;

			return;
		},
		saveHTML: function(saveNodes){
			if ($('#contentContainer').attr('data-state') == "read-only") return false;
			//if (arguments.callee.caller == null) return false;
			if(saveNodes && saveNodes.length < 1){
				return false;
			}
			$('.save-notice').text('Saving...');
			//The below line was commented because it consuming more time when typing - jagan
			//callUpdateArticleCitation(saveNodes,'.jrnlCitation');
			if(kriya.config.content.doi && kriya.config.content.customer && saveNodes){
				var content_element = document.createElement('content');
				$(saveNodes).each(function(){
					if($(this)[0].length == 0){
						return true;
					}
					var saveNode = $(this);

					//If the save node is inside a float block then save the block element - Jai 13-02-2018
					if($(this).closest('[data-id*="BLK"]').length > 0){
						saveNode = $(this).closest('[data-id*="BLK"]');
					}else if($(this).closest('.jrnlRefText').length > 0){
						saveNode = $(this).closest('.jrnlRefText');
					}

					if($(this).closest('[data-save-node][id]').length > 0){
						saveNode = $(this).closest('[data-save-node][id]');
						saveNode.removeAttr('data-save-node');
					}

					var prevElement = $(saveNode)[0].previousSibling;
					var nextElement = $(saveNode)[0].nextSibling;

					//If next and prev sibling or next sibling is a white space then get the sibling sibling
					//If text node value is more than a space then take previous sibiling - priya
					if(prevElement && prevElement.nodeType == 3 && prevElement.nodeValue.trim() == ""){
						prevElement = prevElement.previousSibling;
					}
					if(nextElement && nextElement.nodeType == 3 && nextElement.nodeValue.trim() == ""){
						nextElement = nextElement.nextSibling;
					}

					//If sibling element is jrnlTblContainer then get the sibling as table inside that container - jagan
					if($(prevElement).attr('class') == "jrnlTblContainer" && $(prevElement).find('table').length > 0){
						prevElement = $(prevElement).find('table')[0];
					}

					if($(nextElement).attr('class') == "jrnlTblContainer" && $(nextElement).find('table').length > 0){
						nextElement = $(nextElement).find('table')[0];
					}

					//if the savenode match selector given in kriya.config.sameClassSiblingId
					//then previous and next element class should match the savenode class
					//ex: corresponding affiliation
					if($(saveNode).closest(kriya.config.sameClassSiblingId).length > 0 && $(saveNode).attr('class')){
						var saveNodeClass = $(saveNode).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
						if(prevElement && $(prevElement).attr('class')){
							var prevNodeClass = $(prevElement).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
							if(prevNodeClass != saveNodeClass){
								prevElement = null;
							}
						}
						if(nextElement && $(nextElement).attr('class')){
							var nextNodeClass = $(nextElement).attr('class').replace(/^\s+|\s+$/g, '').split(/\s/)[0];
							if(nextNodeClass != saveNodeClass){
								nextElement = null;
							}
						}
					}

					saveNode = $(saveNode).clone(true);

					//remove the date picker class
					saveNode.removeClass('picker__input--target');
					saveNode.removeClass('activeElement');

					if(!$(saveNode).is(kriya.config.preventSiblingId)){
						var siblingIdStatus = false;
						//Add prev id if prev element is not text element and it has id
						if(saveNode && $(prevElement).length > 0 && prevElement.nodeType != 3 && prevElement.hasAttribute('id')){
							saveNode.attr('data-prevID', prevElement.getAttribute('id'));
							siblingIdStatus = true;
						}
						//Add next id if next element is not text element and it has id
						if(saveNode && $(nextElement).length > 0 && nextElement.nodeType != 3 && nextElement.hasAttribute('id')){
							saveNode.attr('data-nextID', nextElement.getAttribute('id'));
							siblingIdStatus = true;
						}
						//if the element is inserted and prev, next element is text node then save the closest node which has id
						if(!siblingIdStatus && saveNode[0].hasAttribute('data-inserted')){
							if($(this).parents('[id]:not(' + kriya.config.invalidSaveNodes + '):first').length > 0){
								saveNode = $(this).parents('[id]:not(' + kriya.config.invalidSaveNodes + '):first');
								saveNode = $(saveNode).clone(true);
							}
						}
					}

					//nextID OR prevID is mandatory for moved save nodes
					//Remove the data-moved attr if save node doesn't have sibling id
					if(saveNode && saveNode.attr('data-moved') == "true" && !saveNode.attr('data-nextID') && !saveNode.attr('data-prevID')){
						saveNode.removeAttr('data-moved');
					}

					if(saveNode.length > 0){
						content_element.appendChild(saveNode[0]);
					}
				});

				$(content_element).find('.activeElement').removeClass('activeElement'); //Remove the active element class
				$(content_element).find('.wysiwyg-tmp-selected-cell').removeClass('wysiwyg-tmp-selected-cell');
				$(content_element).find('.floatHeader, .confidential-comments, .query-action').remove();

				$(content_element).find('.kriya-nbsp,.kriya-ensp,.kriya-emsp,.kriya-thin,.kriya-lnbreak').contents().unwrap();
				$(content_element).find('.kriya-nbsp:empty,.kriya-ensp:empty,.kriya-emsp:empty,.kriya-thin:empty,.kriya-lnbreak:empty').remove();

				var contentHTML = content_element.outerHTML;
				contentHTML = contentHTML.replace(/[ ]*(&nbsp;)+|(&nbsp;)+ +/g, ' '); //modify by jagan
				contentHTML = contentHTML.replace(/[ ]+/g, ' ');
				//return false;

				kriya.config.saveQueue.push(contentHTML);
				if(kriya.config.saveQueue.length == 1){
					kriyaEditor.init.saveQueueData();
				}
				return false;
			}
		},
		saveQueueData: function(){
			if(!window.navigator.onLine){ // to prevent save if there is know internet connectivity
				return false;
			}
			if(kriya.config.saveQueue.length == 0){
				return false;
			}

			var formData = new FormData();
			formData.append('customer', kriya.config.content.customer);
			formData.append('doi', kriya.config.content.doi);
			formData.append('project', kriya.config.content.project);
			if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
				formData.append('occurrence', '1');
			}else{
				formData.append('occurrence', '2');
			}
			var blob = new Blob([kriya.config.saveQueue[0]], {
				type: 'text/html'
			});
			//var fileToBeUp = new File([blob], 'save_data.html');
			//formData.append('file', fileToBeUp, fileToBeUp.name);
			formData.append('file', blob, 'save_data.html');

			$.ajax({
				url: '/api/save_content',
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function(res){
					res = $('<response>'+res+'</response>');
					if(res.find('exception').length > 0){
						if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
							kriya.config.errorSaveQueue.push(kriya.config.saveQueue[0]);
						}else{
							kriya.config.saveQueue.splice(0,1);
							var settings = {
								'icon'  : '<i class="material-icons" style="margin-top: 3rem;margin-left: 3rem;color: red;">warning</i>',
								'title' : 'Content saving failed',
								'text'  : ' ',
								'size'  : 'pop-sm'
							};
							kriya.actions.confirmation(settings);
							$('.save-notice').text('Content saving failed');
						}
						kriyaEditor.init.saveQueueData();
					}else{
						if(kriyaEditor.settings.save){
							kriyaEditor.settings.save.classList.add("disabled");
						}
						
						$('[data-inserted]').removeAttr('data-inserted');
						$('[data-removed]').removeAttr('data-removed');
						$('[data-moved]').removeAttr('data-moved');
						
						var d = new Date();
						var hours = d.getHours();
						var minutes = d.getMinutes();
						var ampm = hours >= 12 ? 'pm' : 'am';
						hours = hours % 12;
						hours = hours ? hours : 12; // the hour '0' should be '12'
						minutes = minutes < 10 ? '0'+minutes : minutes;
						var strTime = hours + ':' + minutes + ' ' + ampm;
						$('.save-notice').text('All changes saved at ' + strTime);

						if(kriya.config.exportOnsave && typeof(eventHandler.menu.export[kriya.config.exportOnsave]) == "function"){
							eventHandler.menu.export[kriya.config.exportOnsave]('','',false);
							kriya.config.exportOnsave = null;
						}
						kriya.config.saveQueue.splice(0,1);
						kriyaEditor.init.saveQueueData();
					}
				},
				error: function(err){
					if(kriya.config.errorSaveQueue.indexOf(kriya.config.saveQueue[0]) < 0){
						kriya.config.errorSaveQueue.push(kriya.config.saveQueue[0]);
					}else{
						kriya.config.saveQueue.splice(0,1);
						var settings = {
							'icon'  : '<i class="material-icons" style="margin-top: 3rem;margin-left: 3rem;color: red;">warning</i>',
							'title' : 'Content saving failed',
							'text'  : ' ',
							'size'  : 'pop-sm'
						};
						kriya.actions.confirmation(settings);
						$('.save-notice').text('Content saving failed');
					}
					kriyaEditor.init.saveQueueData();
				}
			});
		},
		save: function(modal){
			if(kriya.config.content.doi){
				if (modal) $('.la-container').fadeIn();
				var content_element = document.createElement('body');
				content_element.innerHTML = kriyaEditor.settings.contentNode.innerHTML;
				$(content_element).find('.jrnlReviewersGroup, .jrnlEditorsGroup').remove();
				var parameters = {
					'customer':kriya.config.content.customer, 
					'project' : kriya.config.content.project,
					'doi':kriya.config.content.doi,
					'type': 'saveHTML',
					'data': {
						'process': 'update',
						'update': '//body',
						'content': encodeURIComponent(content_element.outerHTML)
					}
				};
				$('.save-notice').text('Saving...');
				kriya.general.sendAPIRequest('updatedata',parameters,function(res){
					$('.la-container').fadeOut();
					if(kriyaEditor.settings.save){
						kriyaEditor.settings.save.classList.add("disabled");
					}

					var d = new Date();
					var hours = d.getHours();
					var minutes = d.getMinutes();
					var ampm = hours >= 12 ? 'pm' : 'am';
					hours = hours % 12;
					hours = hours ? hours : 12; // the hour '0' should be '12'
					minutes = minutes < 10 ? '0'+minutes : minutes;
					var strTime = hours + ':' + minutes + ' ' + ampm;
					$('.save-notice').text('All changes saved at ' + strTime);

					if(modal && typeof(modal) == "object" && typeof(modal.funcName)=="function"){
						eval(modal.funcName)(modal.funcParam);
					}
				},function(res){
					$('.la-container').fadeOut();
					console.log('ERROR:saveing content.'+res);
				});
			}
		},
		save_old: function(modal){
			if(kriya.config.content.doi){
				if (modal) $('.la-container').fadeIn();
				var content_element = document.createElement('body');
				content_element.innerHTML = kriyaEditor.settings.contentNode.innerHTML;
				$(content_element).find('.jrnlReviewersGroup, .jrnlEditorsGroup').remove();
				var parameters = {
					'customerName':kriya.config.content.customer, 
					'project' : kriya.config.content.project,
					'doi':kriya.config.content.doi, 
					'content':encodeURIComponent(content_element.outerHTML)
				};
				$('.save-notice').text('Saving...');
				kriya.general.sendAPIRequest('save_html_content',parameters,function(res){
					$('.la-container').fadeOut();
					if(kriyaEditor.settings.save){
						kriyaEditor.settings.save.classList.add("disabled");
					}
					
					var d = new Date();
					var hours = d.getHours();
					var minutes = d.getMinutes();
					var ampm = hours >= 12 ? 'pm' : 'am';
					hours = hours % 12;
					hours = hours ? hours : 12; // the hour '0' should be '12'
					minutes = minutes < 10 ? '0'+minutes : minutes;
					var strTime = hours + ':' + minutes + ' ' + ampm;
					$('.save-notice').text('All changes saved at ' + strTime);

					if(modal && typeof(modal) == "object" && typeof(modal.funcName)=="function"){
						eval(modal.funcName)(modal.funcParam);
					}
				},function(res){
					$('.la-container').fadeOut();
					$('.save-notice').text('Content saving failed');
				});
			}
		}
	}
	return kriyaEditor;

})(kriyaEditor || {});

function handleChanges(summary){

	if(kriyaEditor.settings){
		kriyaEditor.settings.summaryData = summary[0];
	}

	if (summary[0].removed.length > 0){
		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {
						'funcToCall': 'nodeChange',
						'param': {
							'node': summary[0].removed,
							'type': 'removed'
						}
					},
					event: 'click',
					target: ''
				}
			});
		},10)
	}

	//Added nodes used for saving when press enter
	//Used in keyup event
	if (summary[0].added.length > 0){

		//When enter key pressed after typing in content track change node was duplicate in new para
		//loop the added element if ele is a track node and text is null and more than one same cid is in content
		//then remove ele in added array and unwrap ele from  content
		for(i=0;i<summary[0].added.length;i++){
			var addedEle = summary[0].added[i];
			if(($(addedEle).hasClass('ins') || $(addedEle).hasClass('del')) && $(addedEle).text() == "" && addedEle.hasAttribute('data-cid')){
				var cid = $(addedEle).attr('data-cid');
				var eleClass = $(addedEle).attr('class').split(' ')[0];
				if($(kriya.config.containerElm + ' .'+eleClass+'[data-cid="' + cid + '"]').length > 1){
					$(addedEle).contents().unwrap();
					summary[0].added.splice(i, 1);
				}
			}
		}

		setTimeout(function(){
			postal.publish({
				channel: 'menu',
				topic: 'observer' + '.' + 'click',
				data: {
					message: {
						'funcToCall': 'nodeChange',
						'param': {
							'node': summary[0].added,
							'type': 'added'
						}
					},
					event: 'click',
					target: ''
				}
			});
		},10)
	}
}

function setupWatch(){
	var observer = new MutationSummary({
	  callback: handleChanges, // required
	  rootNode: document.getElementById('contentDivNode'), // optional, defaults to window.document
	  observeOwnChanges: false,// optional, defaults to false
	  oldPreviousSibling: false,// optional, defaults to false
		queries: [{
			all: true
		}]
	});
}

function callPDFViewer() {
 	if ($("iframe[id='pdfViewer'], iframe[id='pdfViewers']").length > 0) {
 		console.log("ready!");
 		$("iframe[id='pdfViewer'], iframe[id='pdfViewers']").contents().find("#viewerContainer").on("pagerendered", "#viewer", function (event) {
 			console.log('page rendered');
 			var allowInternalLinks = true;
 			var hyperlinks = $(this).find('a');
 			for (var i = 0; i < hyperlinks.length; i++) {
 				if (!allowInternalLinks || hyperlinks[i].className != 'internalLink') {
 					hyperlinks[i].onclick = function (e) {
 						$(e.target).attr('target', '_blank');
 					}
				} else {
 					var hrefid = $(hyperlinks[i]).attr('href').replace(/.*%3A(.*)%3A.*/, '$1');
 					$(hyperlinks[i]).attr('id', hrefid);
 					var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number')); //doing '-1' because index starts at '0'
 					$(hyperlinks[i]).closest('.linkAnnotation').wrap('<span id="' + hrefid + '" page="' + page + '" class="citation"></span>')
 					$(hyperlinks[i]).closest('.linkAnnotation').remove();
 					//$(hyperlinks[i]).closest('.linkAnnotation').addClass('citaion').removeClass('linkAnnotation');
 					//$(hyperlinks[i]).attr('href','');
				}
 			};
 		});
 		/**
 		 * hook onto the textlayerrendered event to add divs (for visual indication of editable area) using the coordinates obtained
 		 */
 		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("textlayerrendered", "#viewer", function (event) {
			var annoTation = $('<div class="markerLayer" style="' + $(event.target).attr('style') + '"></div>');
 			var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number') - 1); //doing '-1' because index starts at '0'
 			var ignoreProofControls = /author/;
 			// get the scale factor of the current view port
 			// this is important as the actual page width and the rendered page width differs and the scale factor helps restore the difference
 			var viewportScale = window.frames["pdfIFrame"].PDFViewerApplication.pdfViewer._pages[page].viewport.scale;
 			var currPageObj = coordinate[page];
 			if (currPageObj) {
 				Object.keys(currPageObj).forEach(function (id) {
 					var currParaObj = currPageObj[id];
					// ignore jrnlAuthGroup class tocreate (div)
					var ignoreClass = ".jrnlAuthorsGroup"; // add classes like .class1, class2
					var ignoreTag = false;
 					id = id.replace(/__[0-9]+$/, '');
					if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(ignoreClass)) {
						ignoreTag = true;
					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(ignoreClass)) {
						ignoreTag = true;
					}
					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)
					var proofControlAtt = '[data-word-spacing], [data-vj], [data-top-gap], .forceColBrk, .forceJustify';
 					proofControlIgnore = '.jrnlAuthor, .jrnlAuthorGroup, .jrnlArtTitle, .jrnlAff, .jrnlQueryRef'
 					var proofContTag = ' none';
 					if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(proofControlIgnore)) {
 						proofContTag = '';
 					} else if($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(proofControlIgnore)){
						proofContTag = '';
					 }else if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(proofControlAtt)) {
 						proofContTag = ' updated';
					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(proofControlAtt)) {
 						proofContTag = ' updated';
					}
 					if (kriya.config.content.role.match(ignoreProofControls)) {
 						proofContTag = '';
 					}
					var styleString = "";
					styleString += "top: " + (currParaObj[0] * viewportScale) + "px !important; ";
 					var proofStyleString = styleString + "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; width: " + (3 * viewportScale) + "px !important; left: " + ((currParaObj[1] * viewportScale) - 5) + "px !important; ";


 					var editBlocks = /(BLK_F|BLK_T)(\d+)/;
 					var proofcontrolsBlock = /^(BLK_F|BLK_T|R)(\d+)/;
 					if (id.match(editBlocks) && !kriya.config.content.role.match(ignoreProofControls)) {
 						proofContTag = '';
						var editStyleString = styleString + "height: 20px; width: 20px !important; left: " + ((currParaObj[1] * viewportScale) - (25 * viewportScale)) + "px !important; transform: scale(" + viewportScale + ")";
 						$('<div page="' + page + '" data-id=' + id + ' class="editMarker fa fa-gear" style="' + editStyleString + '">&nbsp;</div>').appendTo(annoTation);
					}
 					if (!id.match(proofcontrolsBlock) && !kriya.config.content.role.match(ignoreProofControls)) {
 						$('<div data-id=' + id + ' class="proofControls' + proofContTag + '" style="' + proofStyleString + '">&nbsp;</div>').appendTo(annoTation);
 					}
					if (!ignoreTag) {
 					styleString += "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; ";
 					styleString += "width: " + ((currParaObj[3] - currParaObj[1]) * viewportScale) + "px !important; ";
					styleString += "left: " + (currParaObj[1] * viewportScale) + "px !important; ";
					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)

						$('<div data-id=' + id + ' class="pdfMarker" style="' + styleString + '">&nbsp;</div>').appendTo(annoTation);
					}
 				});
				$(annoTation).appendTo($(event.target).closest('.page'))
 			}
 		});
		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .pdfMarker", function (event) {
			var divID = $(event.target).attr('data-id');
			if ($('#contentDivNode #' + divID).length > 0) {
				$('#contentDivNode #' + divID)[0].scrollIntoView();
				$('#contentDivNode #' + divID).trigger("click");
			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
			}
 		});
		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .proofControls", function (event) {
			var divID = $(event.target).attr('data-id');
 			var node;
			if ($('#contentDivNode #' + divID).length > 0) {
				$('#contentDivNode #' + divID)[0].scrollIntoView();
				$('#contentDivNode #' + divID).trigger("click");
 				node = $('#contentDivNode #' + divID)[0];
 				//$('#contentDivNode #' + divID).dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 				//$('#contentDivNode [data-id=' + divID + ']').dblclick()
 				//$('[data-name="proofControlLink"]').trigger("click");
 				node = $('#contentDivNode [data-id=' + divID + ']');
			 }
				$(node).each(function () {
				$('#pdfProofControls #headerSpace').addClass('hidden');
				var proofHeaderSpace = '.jrnlHead1, .jrnlHead2, .jrnlHead3, .jrnlHead4, .jrnlHead5, .jrnlHead6, .jrnlRefHead';
				if($(node).is(proofHeaderSpace)){
					$('#pdfProofControls #headerSpace').removeClass('hidden');
				}
					$('#pdfProofControls').attr('data-id', divID);
 				$('#pdfProofControls input').prop('checked', false);
					$.each(this.attributes, function () {
						var proofControlAtt = '/data-word-spacing|data-vj|data-top-gap|forceColBrk|forceJustify/';
						if (this.specified) {
							if(this.name.match(/data-word-spacing|data-vj|data-top-gap|forceColBrk|forceJustify/)){
 							$('#pdfProofControls input[value="' + this.value + '"][class=' + this.name + ']').prop('checked', true)
								$('#pdfProofControls').attr(this.name, this.value);
							}
							console.log(this.name, this.value);
						}
					});
				});
				document.querySelector('[id="pdfProofControls"]').classList.remove("hidden");
				TomloprodModal.openModal('pdfProofControls');
 			if (divID.match(/BLK/)) {}
 		});
 		$("iframe[id='pdfViewer']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .editMarker", function (event) {
 			var divID = $(event.target).attr('data-id');
 			$('#floatProofControls').attr('data-id', divID);
 			$('#floatProofControls input[type="radio"]').prop('checked', false);
 			$('#floatProofControls input').removeAttr('disabled');
 			$('#floatProofControls input[type="checkbox"]').prop('checked', false);
 			$('#floatProofControls input[type="number"]').val('')
 			$('[id="floatProofControls"] #imageSpace, [id="floatProofControls"] #imageSize ').addClass('hidden');
 			var node;
 			if ($('#contentDivNode #' + divID).length > 0) {
 				$('#contentDivNode #' + divID)[0].scrollIntoView();
 				$('#contentDivNode #' + divID).trigger("click");
 				node = $('#contentDivNode #' + divID)[0];
				$('#contentDivNode #' + divID).dblclick()
				$('[data-name="proofControlLink"]').trigger("click");
 			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
 				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 				$('#contentDivNode [data-id=' + divID + ']').dblclick()
 				$('[data-name="proofControlLink"]').trigger("click");
 				node = $('#contentDivNode [data-id=' + divID + ']');
			}
 			if (divID.match(/BLK_F(\d+)/)) {
 				$('[id="floatProofControls"] #floatName').text('Figure');
 				$('[id="floatProofControls"] #imageSpace').removeClass('hidden');
 				$(node).each(function () {
 					$.each(this.attributes, function () {
 						if (this.specified) {
 							console.log(this.name, this.value);
			}
	   });
	});
 }
 			if (divID.match(/BLK_T(\d+)/)) {
 				$('[id="floatProofControls"] #floatName').text('Table');
 			}
 			//display if already proofcontrols applied
 			$(node).each(function () {
 				var cpage = parseInt($("iframe[id='pdfViewer']").contents().find('.page span.citation[id="' + divID + '"]').attr('page')) - 1;
 				console.log('cpage ', cpage);
 				if (cpage != '' && cpage != undefined && cpage != 'NaN') {
 					this.value = cpage + parseInt($(event.target).parents().closest('.page').attr('data-page-number'));
 					$('#floatProofControls #prevCitation').text(cpage);
 					$('#floatProofControls #cpage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number'))).val(cpage);
 					$('#floatProofControls #npage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number')) + 1).val(cpage + 1);
 					$('#floatProofControls #ppage').attr('value', parseInt($(event.target).parents().closest('.page').attr('data-page-number')) - 1).val(cpage - 1);
 				}

 				$.each(this.attributes, function () {
 					var placeFloatsAtt = /data-page-num|data-top|data-top-gap|data-bot-gap|data-orientation|data-bot-gap|data-float-position|data-float-percent|data-column-start|data-float-placement/;
 					if (this.specified) {
 						if (this.name.match(placeFloatsAtt)) {
 							this.value = this.value.replace(/(pt|w)$/g, '')
 							$('#floatProofControls #prevCitation').text('');
 							$('#floatProofControls input[type="radio"][value="' + this.value + '"][class=' + this.name + ']').prop('checked', true)
 							$('#floatProofControls input[type="number"][class=' + this.name + ']').val(this.value)
 							$('#floatProofControls').attr(this.name, this.value);
 							$('#floatProofControls input.data-page-num[value="1"]').attr('disabled', '');
 							$('#floatProofControls input.data-page-num[value="0"]').attr('disabled', '');
 							$('#floatProofControls input.data-page-num[value="2"]').attr('disabled', '');
 						}
 						console.log(this.name, this.value);
 					}
 				});
 			});

 			document.querySelector('[id="floatProofControls"]').classList.remove("hidden");
 			TomloprodModal.openModal('floatProofControls');
 			/* document.querySelector('[id="floatProofControls"]').classList.remove("hidden");
 				TomloprodModal.openModal('floatProofControls');*/
 		});
 		$('#floatProofControls input[type="radio"]').on('click', function (e) {
 			console.log($(this).val());
 			var className = $(this).attr('class')
 			var selected = $(this).val();
 			$('input[class="' + className + '"]').prop('checked', false);
 			$('input[class="' + className + '"][value="' + selected + '"]').prop('checked', true);
 		})
 		$('#floatProofControls input[type="checkbox"]').on('click', function (e) {
 			console.log($(this).val());
 			var className = $(this).attr('class')
 			var selected = $(this).val();
 			var checked = $('input[class="' + className + '"][value="' + selected + '"]:checked').length
 			if ($(this).attr('display')) {
 				if (checked > 0) {
 					$('#floatProofControls [id="' + $(this).attr('display') + '"]').removeClass('hidden');
 				} else {
 					$('#floatProofControls [id="' + $(this).attr('display') + '"]').addClass('hidden');
 				}
 			}
 			$('input[class="' + className + '"]').prop('checked', false);
 			if (checked > 0) {
 				$('input[class="' + className + '"][value="' + selected + '"]').prop('checked', true);
 			}
 		})
 		$('input.data-word-spacing').on('click', function (e) {
 			console.log($(this).val());
 			var selected = $(this).val();
 			$('input.data-word-spacing').prop('checked', false);
 			$('input.data-word-spacing[value="' + selected + '"]').prop('checked', true);

 		});
 		$('input.data-top-gap').on('click', function (e) {
 			console.log($(this).val());
 			var selected = $(this).val();
 			$('input.data-top-gap').prop('checked', false);
 			$('input.data-top-gap[value="' + selected + '"]').prop('checked', true);

 		})
 	}

 	/**
 	 * For main pdf
 	 */
 	/**
 	 * hook onto the textlayerrendered event to add divs (for visual indication of editable area) using the coordinates obtained
 	 */
 	if ($("iframe[id='pdfViewers']").length > 0) {
 		$("iframe[id='pdfViewers']").contents().find("#viewerContainer").on("textlayerrendered", "#viewer", function (event) {
 			var annoTation = $('<div class="markerLayer" style="' + $(event.target).attr('style') + '"></div>');
 			var page = parseInt($(event.target).closest('div[data-page-number]').attr('data-page-number') - 1); //doing '-1' because index starts at '0'
 			// get the scale factor of the current view port
 			// this is important as the actual page width and the rendered page width differs and the scale factor helps restore the difference
 			var viewportScale = window.frames["pdfIFrames"].PDFViewerApplication.pdfViewer._pages[page].viewport.scale;
 			var currPageObj = coordinate[page];
 			if (currPageObj) {
 				Object.keys(currPageObj).forEach(function (id) {
 					var currParaObj = currPageObj[id];
 					// apply jrnlQueryRef class tocreate (div)
 					var applyClass = ".jrnlQueryRef"; // add classes like .class1, class2
 					var applyTag = false;
 					var currentNodeClass = "";
 					id = id.replace(/__[0-9]+$/, '');
 					if ($('#contentDivNode #' + id).length > 0 && $('#contentDivNode #' + id).is(applyClass)) {
 						applyTag = true;
 						currentNodeClass = $('#contentDivNode #' + id).attr('class');
 					} else if ($('#contentDivNode [data-id=' + id + ']').length > 0 && $('#contentDivNode [data-id=' + id + ']').is(applyClass)) {
 						applyTag = true;
 						currentNodeClass = $('#contentDivNode [data-id=' + id + ']').attr('class');
 					}

 					// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)
 					var proofControlAtt = '[data-word-spacing], [data-vj], [data-top-gap], .forceColBrk, .forceJustify';
 					proofControlIgnore = '.jrnlAuthor, .jrnlAuthorGroup, .jrnlArtTitle, .jrnlAff'
 					var hoverString = '';
 					var styleString = "";
 					var extraClass = "";
 					var extraContent = "&nbsp;";
 					styleString += "top: " + (currParaObj[0] * viewportScale) + "px !important; ";
 					if (applyTag) {
 						if (currentNodeClass.match(/(jrnlQueryRef)/)) {
 							hoverString = $('#queryDivNode #' + $('#contentContainer #' + id).attr('data-rid')).find('.query-content').html();
 							extraClass = ' tooltip';
 							extraContent = '<span class="tooltiptext">' + hoverString + '</span>';
 						}
 						styleString += "height: " + ((currParaObj[2] - currParaObj[0]) * viewportScale) + "px !important; ";
 						styleString += "width: " + ((currParaObj[3] - currParaObj[1]) * viewportScale) + "px !important; ";
 						styleString += "left: " + (currParaObj[1] * viewportScale) + "px !important; ";
 						// check if we have any proof controls for the paragraph and add class='proofControls' to show a border on the visual indicator (div)

 						$('<div data-id=' + id + ' class="pdfMarker' + extraClass + '" style="' + styleString + '">' + extraContent + '</div>').appendTo(annoTation);
 					}
 				});
 				$(annoTation).appendTo($(event.target).closest('.page'))
 			}
 		});
 		$("iframe[id='pdfViewers']").contents().find("#viewerContainer").on("click", "#viewer > .page > .markerLayer > .pdfMarker", function (event) {
 			eventHandler.welcome.begin.startEditing()
 			var divID = $(event.target).attr('data-id');
 			if ($('#contentDivNode #' + divID).length > 0) {
 				$('#contentDivNode #' + divID)[0].scrollIntoView();
 				$('#contentDivNode #' + divID).trigger("click");
 			} else if ($('#contentDivNode [data-id=' + divID + ']').length > 0) {
 				$('#contentDivNode [data-id=' + divID + ']')[0].scrollIntoView();
 				$('#contentDivNode [data-id=' + divID + ']').trigger("click");
 			}
 		});
 	}
 }
function callInitializeEvents(){
	eventHandler.publishers.add();
	eventHandler.subscribers.add();
	//#509 - added wicket good xpath initializer - Rajasekar T(rajasekar.t@focusite.com)
	wgxpath.install();
	//#509
	$.each(kriya.config.preventTyping.split(","), function (i, val) {
		$(val).attr('data-editable', "false")
	})
	if (($(contentDivNode).length > 0) && $(contentDivNode).is(':visible')) {
		//$(contentDivNode).css('height', (window.innerHeight - $(contentDivNode).offset().top - $('#footerContainer').height() )+ 'px');
		$('#contentContainer').parent().css('height', (window.innerHeight - $('#contentContainer').parent().offset().top - $('#footerContainer').height())+ 'px');
		/*$(contentDivNode).parent('.nano').nanoScroller({
	 		alwaysVisible: true,
	 		sliderMinHeight: 5,
	 		preventPageScrolling: true
	 	});*/
		//$(contentDivNode).parent().find('.nano-pane').addClass('content-scrollbar');
		//$(contentDivNode).width('90%');
	}
	$('#navContainer').css('height', ($(window).height() - $('#navContainer').offset().top - $('#footerContainer').height()) + 'px')
	$('#navContainer .navDivContent > div').css('height', $(window).height() - $('#navContainer #infoDivContent').offset().top - $('#footerContainer').height());
	if ($('#indexPanelContent .jstree-container-ul').length > 0){
		$('#indexPanelContent .jstree-container-ul li').removeAttr('class');
		tree = $("#indexPanelContent").jstree({
			core: {
				check_callback: true
			},
			plugins: ["dnd"]
		});
		tree.jstree("deselect_all").jstree('open_all');
	}

	//Initialize table row and index
	$(kriya.config.containerElm).find('.jrnlInlineTable, .jrnlTable').each(function(){
		setTableIndex($(this));
	});

	window.addEventListener("resize", function(){
		$('#contentContainer').parent().css('height', (window.innerHeight - $('#contentContainer').parent().offset().top - $('#footerContainer').height())+ 'px');
		$('#navContainer').css('height', ($(window).height() - $('#navContainer').offset().top - $('#footerContainer').height()) + 'px')
		$('#navContainer .navDivContent > div').css('height', $(window).height() - $('#navContainer #infoDivContent').offset().top - $('#footerContainer').height());
	});
	eventHandler.query.action.addBadge();
	$('.uncited-object-btn').each(function(){
		if ($(this).closest('div').find('[data-uncited="true"]').length > 0){
			$(this).append('<span class="notify-badge">'+ $(this).closest('div').find('[data-uncited="true"]').length + '</span>');
		}
	})
	$('body').append('<span class="class-highlight" style="position: absolute;bottom: -12px;display: inline-block;height: 25px;width: 200px;right: 10px;background: #eee;border: 1px solid #ddd;font-size: 12px;line-height: 110%;padding:5px;color:#424242;"></span>');
	$('[data-type="popUp"]').find('input,textarea').attr("autocomplete","off").attr("autocorrect", "off").attr("autocapitalize", "off").attr("spellcheck", "false");
	$('[data-tooltip]').tooltip({
		delay: 50
	});

	if(kriya.config.content.role == "author"){
		eventHandler.menu.edit.quickDemo();
	}
	
	//Initialize the content buttons
	if ($('#contentContainer').attr('data-state') != 'read-only'){
		var contentBtns = $('#compDivContent [data-component="contentButtons"] [data-xpath]');
		contentBtns.each(function(){
			var xpath = $(this).attr('data-xpath');
			var dataxpath = $(this).attr('data-condition');
			var maxVal = $(this).attr('data-maximum');
			var btnContainer = kriya.xpath(xpath);
			var clonedNode = $(this).clone(true);
			var btnContainerCond = "";
			var availdata = 0;
			if($(btnContainer).length > 0){
				if(dataxpath!=undefined){
					btnContainerCond = kriya.xpath(dataxpath);
					if(maxVal!=undefined){
						if($(btnContainerCond).length>=maxVal){
							$(clonedNode).addClass("hidden");
						}
					} else if ($(btnContainerCond).length > 0) {
						$(btnContainerCond).each(function(){
							if($(this).attr('data-track')==undefined || $(this).attr('data-track')!="del"){
								availdata=1;
							}
						})
						if(availdata==1){
							$(clonedNode).addClass("hidden");
						}
					}
				}
				$(btnContainer).append(clonedNode);
			}
		});

		//Initialize the filter options
		kriya.general.iniFilterOptions($('.filterOptions .dropdown-content'), '');

	}

	setTimeout(function(){
		// initialiseDragDrop();
		// eventHandler.components.reference.getReferenceModal();
		// eventHandler.components.citation.initCitationConfig();
		var resizeTimer;
		$('#contentDivNode > br').remove();

		createCloneContent();
		$('#contentDivNode').scroll(function(){
			$('#clonedContent').scrollTop($('#contentDivNode').scrollTop())
			$('.suggestions,.resolve-query').remove();
			setTimeout(function(){
				if ($('.query-div.hover-inline').find('.cancelReply').length > 0){
					$('.query-div.hover-inline').find('.cancelReply').trigger('click');
				}else if ($('.query-div.hover-inline').find('.com-close').length > 0){
					$('.query-div.hover-inline').find('.com-close').trigger('click');
				}else{
					$('.query-div.disabled').remove();
					$('.query-div.hover-inline').removeAttr('style').removeClass('hover-inline');
				}
			},100);
		});

		//To remove the empty space in query node
		$('.jrnlQueryRef').html("");
		kriyaEditor.settings.startValue = kriyaEditor.settings.contentNode.innerHTML;
		kriyaEditor.settings.cloneNode = document.getElementById('clonedContent');
		kriyaEditor.settings.cloneValue = kriyaEditor.settings.cloneNode.innerHTML;

		if($('#welcomeContainer').attr('data-accept-article') == "true"){
			eventHandler.components.general.acceptReviewer();
		}else if($('#welcomeContainer').attr('data-reject-article') == "true"){
			eventHandler.components.general.rejectReviewer();
		}
		

	},2000);

	setupWatch();
	
	window.onerror = function(msg, url, line, col, error) {
	   // Note that col & error are new to the HTML 5 spec and may not be
	   // supported in every browser.  It worked for me in Chrome.
	   var extra = !col ? '' : '\ncolumn: ' + col;
	   extra += !error ? '' : '\nerror: ' + error;
	   //it will give flow of function calling.
	   var flow = error.stack ? "\nFlow:"+error.stack : "";
	   var browser = '';
	   // Chrome 1+
		if(!!window.chrome && !!window.chrome.webstore){
			browser = "Chrome";
		}else if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){// Opera 8.0+
			browser = "Opera";
		}else if(typeof InstallTrigger !== 'undefined'){// Firefox 1.0+
			browser = "Firefox";
		}else if(/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){// Safari 3.0+ "[object HTMLElementConstructor]"
			browser = "Safari";
		}else if(/*@cc_on!@*/false || !!document.documentMode){// Internet Explorer 6-11
			browser = "Internet Explorer";
		}else if(!(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia){// Edge 20+
			browser = "Edge";
		//for blink we need to check like this.
		}else if(((!!window.chrome && !!window.chrome.webstore) || ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)) && !!window.CSS){// Blink engine detection
			browser = "Blink";
		}else{
			browser = "UnKnown";
		}
	   //it will give target ele id className and parent ele className
	   var target = (kriya.evt && kriya.evt.target)? "\nTarget id: "+kriya.evt.target.id+", class: "+kriya.evt.target.className+", parentClass: "+kriya.evt.target.parentNode.className : "";
	   //it will give what browser they are using.(if not chrome and mozilla it will print others)
	   //var browser = (!!window.chrome && !!window.chrome.webstore)?"chrome":(typeof InstallTrigger !== 'undefined')? "Mozilla":"other";
	   // You can view the information in an alert to see things working like this:
	   var errorMsg = "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra + "\nUser:" + kriyaEditor.user.name + "\n" + window.location.href + target + flow +"\nBrowser: "+browser ;
	   console.log(errorMsg);
	   // TODO: Report this error via ajax so you can keep track
	   //       of what pages have JS issues
		var parameters = {
			'log': errorMsg
		};
	   kriya.general.sendAPIRequest('logerrors',parameters,function(res){
			console.log('Data Saved');
		})
	   var suppressErrorAlert = true;
	   // If you return true, then error alerts (like in older versions of
	   // Internet Explorer) will be suppressed.
	   return suppressErrorAlert;
	};
}

function createCloneContent() {
	$('#clonedContent').remove();
	var clonedContent = $('<div id="clonedContent" class="nano-content">');
	$(kriya.config.containerElm).after(clonedContent);
	var cloneHeight = 0
	$(kriya.config.containerElm).children().each(function(){
		cloneHeight += $(this).height();
	});

	var lastNode = $(kriya.config.containerElm + ' *:visible').last();
	if(lastNode.length > 0){
		var dummyNodeTop = lastNode[0].getBoundingClientRect().top + $('#contentDivNode').scrollTop();
		$('#clonedContent').append($('<span class="dummyHighlight" style="top:' + dummyNodeTop + 'px; display:inline-block;opacity:0;width: 18px;height: 18.5px;position: absolute;z-index: 0;"/>'));
	}
	
	$('#clonedContent').append($('<div style="position: absolute;left: 0px;width: 1px;height: 1px;display: inline-block;opacity: 0;top:'+cloneHeight+'px"/>'));
	$('#clonedContent').height($('#contentDivNode').height());
	//$('#queryDivNode').height($('#contentDivNode').height());

	//Create version container
	var versionContainer = $('<div id="versionContainer" data-message="{\'click\':{\'funcToCall\': \'contentEvt\',\'channel\':\'components\',\'topic\':\'general\'}}" class="hidden"/>');
	$(kriya.config.containerElm).after(versionContainer);
	$('#versionContainer').height($('#contentDivNode').height());
	

}

function cloneChild(node, cid){
	var subChild = $(node).clone();
	$(subChild).html('');
	if (subChild[0].hasAttribute('id')){
		$(subChild).attr('clone-id', $(subChild).attr('id'));
		$(subChild).removeAttr('id');
	}else{
		$(node).attr('id', 'cid_'+cid);
		$(subChild).attr('clone-id', 'cid_' + cid++);
	}
	subChild.css({
		'position': 'relative'
	})
	if ($(node).css('display') == "none"){
		subChild.height('0');
		subChild.css({
			'display': 'none'
		});
	}else{
		subChild.height($(node).outerHeight());
		subChild.css({
			'display': 'block'
		});
	}
	$(subChild).css('margin', $(node).css('margin'));
	$(subChild).css('margin-top', $(node).css('marginTop'));
	$(subChild).css('margin-bottom', $(node).css('marginBottom'));
	$(subChild).css('padding-top', $(node).css('paddingTop'));
	$(subChild).css('padding-left', $(node).css('paddingLeft'));
	$(subChild).css('padding-bottom', $(node).css('paddingBottom'));
	$(subChild).css('padding-right', $(node).css('paddingRight'));
	return [subChild, cid];
}
