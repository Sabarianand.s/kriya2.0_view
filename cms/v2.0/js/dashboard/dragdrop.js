/*https://codepen.io/sagarpatil/pen/LEZLav*/
// function that handles the dragstart event
function dragStart(e) {
    e.stopPropagation();
    var boxObj = $(this);
    boxObj.addClass('is-dragged');

    // when an element is dragged the preview image is needed to show that the element is being dragged
    //  for some reason the default image is not clear, so did some tweaks to get the working
    var width = boxObj.width() / 1.5;
    var height = boxObj.height() / 1.5;

    var dragImage = boxObj.clone();
    dragImage.removeAttr('id').removeClass('uaa').addClass('uaaDrag');
    $('#dragImage').removeClass('hide')
        .width(width + 20).height(height + 20)
        .html(dragImage)
        .find('.uaaDrag').width(width - 10).height(height + 10);

    e.originalEvent.dataTransfer.setDragImage($('#dragImage .uaaDrag').get(0), 0, 0);
    e.originalEvent.dataTransfer.effectAllowed = 'copy';
}

function dragEnd(e) {
    e.stopPropagation();
    $('#dragImage').html('').addClass('hide');
    $(this).removeClass('is-dragged');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
    $('.dropTop').removeClass('dropTop');
    $('.dropBottom').removeClass('dropBottom');
}

function dragEnter(e) {
    if (e.stopPropagation) e.stopPropagation();
    $('.dropArrow.icon-arrow_forward').removeClass('hide');
}

function dragLeave(e) {
    if (e.stopPropagation) e.stopPropagation();
    $(this).removeClass('dropTop').removeClass('dropBottom');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
}

/**
 * based on were you are dragging over the droppable element, show a clue as to whether
 *  the dropped element is to be placed above or below the droppable element
 * @param {*} e
 */
function dragOver(e) {
    if (e.stopPropagation) e.stopPropagation();
    e.preventDefault();
    e.originalEvent.dataTransfer.dropEffect = 'copy';
    var clientX = e.originalEvent.clientX;
    var clientY = e.originalEvent.clientY;
    var thisPosition = $(this).position();
    var halfHeight = thisPosition.top + ($(this).height() / 2)
    var leftValue = thisPosition.left + parseInt($(this).css('margin-left')) - parseInt($('.dropArrow').css('font-size'));
    var topValue = thisPosition.top;
    if (halfHeight > clientY) {
        $(this).addClass('dropTop').removeClass('dropBottom');
    }
    else {
        topValue += $(this).height();
        $(this).addClass('dropBottom').removeClass('dropTop');
    }
    $('.dropArrow.icon-arrow_forward').removeClass('hide').css({ top: topValue, left: leftValue })
    return false;
}

function drop(e) {
    $('.la-container').fadeIn();
    if (e.stopPropagation) e.stopPropagation();
    var droppedOnNode = $(e.currentTarget);
    var dropTop = false;
    if (droppedOnNode.hasClass('dropTop')) {
        dropTop = true;
    }

    var draggedNode = $('.is-dragged');
    $('.dropArrow.icon-arrow_forward').addClass('hide');
    droppedOnNode.removeClass('dropTop').removeClass('dropBottom');
    var entry = '';
    if (draggedNode.attr('data-obj')) {
        var entryData = draggedNode.attr('data-obj');
        entryData = entryData.replace(/\'/gi, '"');
        entry = JSON.parse(entryData);
    }
    else {
        entry = draggedNode.data('data');
    }
    var dataType = "manuscript";
    if (entry._attributes && entry._attributes.type) {
        dataType = entry._attributes.type;
    }
    var dataGroupType = '';
    if (entry._attributes && entry._attributes.grouptype) {
        dataGroupType = entry._attributes.grouptype;
    }
    var runOnFlag = 'false'; // check if run-on article
    if (entry._attributes && entry._attributes['data-run-on']) {
        runOnFlag = entry._attributes['data-run-on'];
    }
    //check if unpaginated
    var unpaginated = 'false';
    if (entry._attributes && entry._attributes['data-unpaginated']) {
        unpaginated = entry._attributes['data-unpaginated']
    }
    // get the last MS container
    var lastNode = $('#tocContentDiv *[data-c-id]:last');
    var lastNodeID, tempID;
    if (lastNode.length > 0) {
        lastNodeID = parseInt(lastNode.attr('data-c-id'));
        tempID = lastNodeID + 1;
    }
    else {
        tempID = parseInt(0)
    }
    if (!(/^(manuscript)/gi.test(entry._attributes.type))) {
        entry._attributes['id'] += tempID;
    }
    if (!(entry._attributes.fpage) || entry._attributes.fpage == '') {
        entry._attributes.fpage = 1;
    }
    if (!(entry._attributes.fpage) || entry._attributes.lpage == '') {
        entry._attributes.lpage = entry._attributes.fpage;
    }
    var secIndex = ''
    if (droppedOnNode.attr('data-sec')) {
        secIndex = droppedOnNode.attr('data-sec').replace('sec', '')
    }
    else if (droppedOnNode.attr('id')) {
        secIndex = droppedOnNode.attr('id').replace('sec', '')
    }
    else {
        secIndex = parseInt(0)
    }
    var msConObj = {
        "dataType": dataType,
        "tempID": tempID,
        "sectionIndex": secIndex,
        "dataGroupType": dataGroupType
    }
    // var msContainer = eventHandler.general.components.getMSContainer(entry, msConObj);

    if ($(droppedOnNode).data('data') == undefined && entry._attributes.section == '' && (/^(manuscript|chapter)/gi.test(entry._attributes.type))) {
        entry._attributes.section = entry.type ? entry.type._text : ''
    }
    else if ($(droppedOnNode).data('data')) {
        entry._attributes.section = $(droppedOnNode).data('data')._attributes.section;
    } else if ($(droppedOnNode)[0].hasAttribute('data-section')) {
        entry._attributes.section = $(droppedOnNode).attr('data-section');
    }
    var data = {};
    data.entry = entry;
    data.msConObj = msConObj;
    var pagefn = doT.template(document.getElementById('makeCardWhileDroppingSectionCard').innerHTML, undefined, undefined);
    var msContainer = pagefn(data);
    // var msContainer = '<div data-type="half-title" data-grouptype="roman" data-c-id="' + msConObj.tempID + '" class="row sectionDataChild" data-sec="sec1" data-doi="21028" data-channel="menu" data-topic="toggle" data-event="click" data-message="{\'funcToCall\': \'manuscript\'}" data-section="Front Contents">'
    // msContainer += '<div class="row" style="margin-right:0px;width: 100%;">'
    // msContainer += '<div class="col-3 msID" style="padding-right:0px"><span class="doi">21028</span> <span class="artType"></span> <span class="currentStage"></span> <span class="flagList"></span> </div> <div class="col-4 titleText" title="Half Title" style="padding-left:0px"> <span class="msTitle">Title</span> </div> <div class="col-2" style="padding-left:0px"> <div class="row" style="width: 100%;"> <div> <span class="editArticle viewButtons disabled hidden" data-filter="Title" data-filter-type="string"> <a data-toggle="tooltip" title="Edit Article" href=""> <i class="fa fa-pencil-square-o editArticleIcon disabled"></i> </a> </span> <span class="editArticle btn btn-xs" style="color: #ffffff;background-color: #cacaca;margin-right: 5px;">OFFLINE</span> </div> <div class=""> <span class="pdfview viewButtons" title="View PDF"> <i class="fa fa-eye"></i> </span> </div> </div> </div> <div class="col-3" style="padding:0px"> <span class="col-8 pageRange" data-fpage="i" data-lpage="i" style="padding-left:0px"> <span class="rangeStartInfo">First Page: <span class="rangeStart ">i</span> </span> <span class="site"></span> </span> <!--span class="msControls col-4" style="padding-right:0px"><span class="controlIcons fa fa-arrow-down move_down" title="Move down"></span><span class="controlIcons fa fa-arrow-up move_up" title="Move up"></span> <span class="controlIcons fa fa-times remove_card" title="Remove"></span></span--> </div> </div> </div>'
    var populateTOCFlag = false;
    if ((msContainer == '') || (typeof (msContainer) === 'undefined')) {
        // showMessage({ 'text': 'Unable to add the requested file. Please try again or contact support', 'type': 'error' });
        return false;
    }
    var romanFlag = false
    if ($(droppedOnNode).data('data') && ($(droppedOnNode).data('data')._attributes.grouptype) && (/^(roman)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))) {
        romanFlag = true
    } else if ($(droppedOnNode).next().data('data') && ($(droppedOnNode).next().data('data')._attributes.grouptype) && (/^(roman)/gi.test($(droppedOnNode).next().data('data')._attributes.grouptype))) {
        romanFlag = true
    }
    //for book drag drop based on grouptype
    var configData = $('#manuscriptsData').data('config');
    if (configData._attributes && configData._attributes['type'] && configData._attributes['type'] == 'book') {
        if ($(droppedOnNode).data('data') && dataType != 'blank' && entry._attributes.grouptype != $(droppedOnNode).data('data')._attributes.grouptype) {
            // showMessage({ 'text': 'Card can oly be added to the same grouptype ', 'type': 'error' });
            $('.la-container').fadeOut();
            return false;
        }
    }
    if ((dataType != ' data-type="filler"') && (dataType != ' data-type="filler-toc"')) {
        if (droppedOnNode.hasClass('noChild')) {
            $(msContainer).insertAfter(droppedOnNode);
            droppedOnNode.removeClass('noChild')
            droppedOnNode.addClass('hasChild')
        }

        else if (droppedOnNode.hasClass('dropTop')) {
            $(msContainer).insertBefore(droppedOnNode);
        }
        else if (droppedOnNode.hasClass('sectionHeader')) {
            $(msContainer).insertAfter(droppedOnNode);
        }
        else {
            $(msContainer).insertAfter(droppedOnNode);
        }
    }
    else {  //if the drop node is filler/filler-toc, then add it only after manuscript
        if (droppedOnNode.attr("data-type") == 'manuscript') {
            $(msContainer).insertAfter(droppedOnNode);
        }
        else { //if filler/filler-toc is added after blank/advert display error msg
            // $(msContainer).insertBefore(droppedOnNode);
            // showMessage({ 'text': 'Filler/Filler TOC can oly be added below Manuscript ', 'type': 'error' });
            $('.la-container').fadeOut();
            return false;
        }
    }
    if ($(droppedOnNode).data('data') && ($(droppedOnNode).data('data')._attributes.grouptype) && (/^(roman|front|back)/gi.test($(droppedOnNode).data('data')._attributes.grouptype))) {
        entry._attributes.grouptype = $(droppedOnNode).data('data')._attributes.grouptype;
        eventHandler.flatplan.action.saveData();
    }
    $('[data-c-id="' + tempID + '"]').removeClass('hide');
    var lastNode = $('#tocContentDiv *[data-c-id]:last');
    var lastNodeID;
    var seqID = tempID;
    if (lastNode.length > 0) {
        lastNodeID = parseInt(lastNode.attr('data-c-id'));
        //push the 'entry' data into array as if its the last node, then push it through the array to it actual position
        while (lastNodeID != tempID) {
            $('#manuscriptsData').data('binder')['container-xml'].contents.content[lastNodeID + 1] = $('#manuscriptsData').data('binder')['container-xml'].contents.content[lastNodeID];
            lastNode.attr('data-c-id', seqID--);
            if ($(lastNode).prevAll('[data-c-id]:first').length > 0) {
                lastNode = $(lastNode).prevAll('[data-c-id]:first')
            }
            else if ($(lastNode).parent().prevAll().find('[data-c-id]:last').length > 0) {
                lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
            }
            else {
                //lastNode = $(lastNode).parent().prevAll().find('[data-c-id]:last').last()
                lastNode = $(lastNode).parent().prevAll('[data-c-id]:first')
            }
            lastNodeID = parseInt(lastNode.attr('data-c-id'));
        }
        lastNode.attr('data-c-id', seqID).data('data', entry);
    }
    else {
        $('[data-c-id="' + tempID + '"]').data('data', entry);
    }
    $('#manuscriptsData').data('binder')['container-xml'].contents.content[seqID] = entry;
    // to determine the start page number of the currently added MS look for page number in the previous node
    // previous node will be something like manuscript or blank or ..., else it would be section, if its a section get the first page, else get the last page
    var prevNode = lastNode.prev();
    var startPage = 1;
    if (prevNode) {
        if (prevNode.attr('data-type') == 'section') {
            if ($(prevNode).find('.rangeStart').length > 0) {
                if (unpaginated == 'true') {  // if run-on article, set the prev node lpage as fpage
                    startPage = parseInt($(prevNode).find('.rangeStart')[0].textContent) - 1;
                }
                else if (romanFlag == true) {
                    startPage = eventHandler.flatplan.components.fromRoman($(prevNode).find('.rangeStart')[0].textContent);
                    startPage = parseInt(startPage)
                }
                else {
                    startPage = parseInt($(prevNode).find('.rangeStart')[0].textContent);
                }
            }
        }
        else if (/(sectionHeader)/gi.test(prevNode.attr('class'))) {
            startPage = parseInt(1)
            populateTOCFlag = true;
        }
        else {
            if (runOnFlag == 'true') {
                startPage = parseInt(prevNode.data('data')._attributes.lpage);
            }
            else if (romanFlag == true) {
                startPage = eventHandler.flatplan.components.fromRoman(prevNode.data('data')._attributes.lpage);
                startPage = parseInt(startPage) + 1;
            }
            else {
                startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
            }
            // startPage = parseInt(prevNode.data('data')._attributes.lpage) + 1;
        }
    }
    else {
        startPage = parseInt(1)
    }
    if (romanFlag == true) {
        lastNode.find('.rangeStart').text(eventHandler.flatplan.components.toRoman(startPage));
        lastNode.data('data')._attributes.fpage = lastNode.data('data')._attributes.lpage = eventHandler.flatplan.components.toRoman(startPage);
    } else {
        lastNode.find('.rangeStart').text(startPage);
        lastNode.data('data')._attributes.fpage = lastNode.data('data')._attributes.lpage = startPage;
    }
    if (unpaginated != 'true') {
        eventHandler.flatplan.components.updatePageNumber(seqID, startPage, entry._attributes.grouptype);
    }
    if (!draggedNode.attr('data-obj')) {
        //dont remove the card for blank/advert/filler
        if ((entry._attributes.type != 'blank') && (entry._attributes.type != 'advert') && (entry._attributes.type != 'advert_unpaginated') && (entry._attributes.type != 'filler') && (entry._attributes.type != 'filler-toc')) {
            draggedNode.remove();
        }
    }
    eventHandler.flatplan.action.saveData();
    if (populateTOCFlag == true) {
        // eventHandler.general.components.populateTOC();
    }
    $('.la-container').fadeOut();
    return false;
}