
var findReplace = function() {
	//default settings
	var settings = {};
	settings.options = {
		content: '#contentDivNode',
		className: 'highlight'
	};
	//add or override the initial setting
	var initialize = function(params) {
		function extend(obj, ext) {
			if (obj === undefined){
				return ext;
			}
			var i, l, name, args = arguments, value;
			for (i = 1, l = args.length; i < l; i++) {
				ext = args[i];
				for (name in ext) {
					if (ext.hasOwnProperty(name)) {
						value = ext[name];
						if (value !== undefined) {
							obj[name] = value;
						}
					}
				}
			}
			return obj;
		}
		settings = settings = extend(settings, params);
		return this;
	};

	var getSettings = function(){
		var newSettings = {};
		for (var prop in settings) {
			newSettings[prop] = settings[prop];  
		}
		return newSettings;
	};

	return {
		init: initialize,
		getSettings: getSettings,
		settings: settings
	};
}();

(function (findReplace) {
	var settings = findReplace.settings;
	var captureGroup;
	//settings = commonjs.getSettings();
	findReplace.general = {
		find: function(options){
			var searchText = options.searchText;
			if (searchText=="" && !options.matchFormat) { //Removed minimum character condition - priya
				return false;
			}
			searchText = searchText.replace("&amp;", "&"); // replace amp; while searching & #383 - priya
			searchText = searchText.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|\&]/g, "\\$&");
			searchText = searchText.replace(/\s/g, "[\u00A0 ]");
			var matchCase = 'gi';
			if (options.matchCase) matchCase = 'g';//match case
			if (options.wholeWord) searchText = '\\b' + searchText + '\\b';//match whole word only
			//122 - For wholw word  modified search text match added for spaecial character by Vimala
			if (options.wholeWord) {
				searchText = searchText.match(/[a-zA-Z0-9]+$/i) ? '\\b' + searchText + '\\b' : '(^|\\s)' + searchText + '(\\s|$)'; //match whole word only
				matchCase = searchText.match(/[a-zA-Z0-9]+$/i) ? 'gi' : 'g'; //#122 matchacse for special character by Vimala
			}
			var regex = typeof searchText === 'string' ? new RegExp(searchText, matchCase) : searchText;
			var count = 0;
			var className = 'findOverlay';
			if (typeof(options.className) != "undefined"){
				className = options.className;
			}
			if (typeof(options.contextNode) == "undefined"){
				contextNode = $(settings.options.content)[0];

				//var childNodes = contextNode.childNodes;				
				if (searchText=="" && options.matchFormat!=""){
					var queryText = './/'+options.matchFormat;
				}else{
					var queryText = './/div/*[not(self::table)][not(self::div)][not(self::ol)][not(self::ul)]|.//td[not(p)]|.//td/p|.//li[not(p)][not(ul)][not(ol)]|.//li/p';
				}		
				
				//var childNodes = contextNode.childNodes;
				var results = document.evaluate(queryText, contextNode, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
				var childNodes = [];
				var currSelector = results.iterateNext(); 
				while (currSelector) {
					if (currSelector.nodeType == 1){
						childNodes.push(currSelector);
					}else{
						childNodes.push(currSelector);
					}
					currSelector = results.iterateNext();
				}
				//console.log(childNodes);
				//return false;
			}else{
				var childNodes = [$(options.contextNode)[0]];
				contextNode = options.contextNode;
			}
			var xy = $(settings.options.content).scrollTop();
			//$(contextNode).focus();
			$(settings.options.content).scrollTop(xy);
			var cnLength = childNodes.length;
			//console.log(new Date());
			var matchNodes = [];
			if(searchText=="" && options.matchFormat!=""){
				while (cnLength--) {
					var currentNode = childNodes[cnLength];
					if($(currentNode).hasClass(".btn") || $(currentNode).closest(".btn").length>0){
						continue;//No need to highlight the button and current replaced node - priya #119-#123 
					}
					var range = rangy.getSelection();
					var sel = range.getRangeAt(0);
					sel.setStart(currentNode, 0);
					sel.setEndAfter(currentNode);
					if (range){
						var node = findReplace.general.highlight(range, className);
						matchNodes.unshift(node[0]);
					}
				}
				return matchNodes;
			}
		
			while (cnLength--) {
				var currentNode = childNodes[cnLength];
				//var textNodeValue = currentNode.textContent;
				$(currentNode).find('*').each(function(){
					if ($(this).css('display') == "none"){
						$(this).attr('data-temp-attr', 'find')
					}
				});
				var cloneNode = currentNode.cloneNode(true)
				$(cloneNode).find('*[data-temp-attr]').remove()
				$(currentNode).find('*[data-temp-attr]').removeAttr('data-temp-attr');
				
				if(options.spellInlineEle!='undefined'){
					$(cloneNode).find(options.spellInlineEle).remove();
				}
				var textNodeValue = cloneNode.textContent;
				if (textNodeValue.match(regex)) {
					var matches = [];
					while (m = regex.exec(textNodeValue)) {
						//#122 whole word match for special character by Vimala
						if(searchText.match(/[a-zA-Z0-9]+$/i) == null  && (options.wholeWord)){
							m[0] = m[0].replace(/^\s|\s$/gm,'');
							m['index'] = m['index'] + 1; 
						}
						//if we not catch error in this line it will break the code because some times getMatchIndexes() will throw error
						//matches.push(getMatchIndexes(m, captureGroup))
						try{
							matches.push(getMatchIndexes(m, captureGroup))
						}catch(err){
							console.log(err);
						}
					}
					if (matches.length) {
						//var textNodes = document.evaluate('.//text()', currentNode, null, XPathResult.ANY_TYPE, null);
						var textNodes = textNodesUnder(currentNode);
						var tnLength = textNodes.length;
						var startNode = false, endNode = false, startNodeIndex, endNodeIndex, innerNodes = [], atIndex = 0, matchLocation = matches.shift(), matchIndex = 0, rangeMatch = true;
						var rangeArray = [];
						var matchedNodes = [];
						//iterate each text nodes untill we get the offset
						for (var tl = 0; tl < tnLength; tl++){
							var curNode = textNodes[tl];
							if ($(curNode.parentNode).css('display') == 'none' || !$(curNode.parentNode).is(':visible') || (options.spellInlineEle!='undefined' && $(curNode).closest(options.spellInlineEle).length>0)){ //button text should not get highlight, a word is replaced by same word its shouls not get highlight - priya #119 - #123		
								continue;
							}
							rangeMatch = true;
							while (rangeMatch){
								if (!endNode && startNode && curNode.length + atIndex >= matchLocation[1]) {
									// We've found the ending
									endNode = curNode;
									endNodeIndex = matchLocation[1] - atIndex - 1;
									range = rangy.getSelection();
									sel = range.getRangeAt(0);
									//sel = sel.cloneRange();
									sel.setStart(startNode, startNodeIndex + 1);
									sel.setEnd(endNode, endNodeIndex + 1 );
									if($(startNode,endNode).hasClass(".btn") || $(startNode,endNode).closest(".btn").length>0 || $(startNode,endNode).attr("[data-replaced]")!=undefined || $(startNode,endNode).closest("[data-replaced]").length>0){
										//No need to highlight the button and current replaced node - priya #119-#123 
									}else{
										var matchHighLight = 1;
										if(options.matchFormat!=undefined && options.matchFormat!="" && $(startNode).closest(options.matchFormat).length==0){
											matchHighLight = 0; //check the closest node is formatting node - priya #119
										}
										if(matchHighLight==1){
											//when finding text taking text parent as one node and when replacing text at that time taking text parent as another node. because of that replacing text not happening in fig,table... at the time of search in object option selected.-#2668
											var node = findReplace.general.highlight(range, className,$(currentNode));
											//range.removeAllRanges();
											var addDisableattr = 0;
											if ($(startNode,endNode).closest('.del').length > 0 || $(startNode,endNode).closest('.floatLabel').length > 0 || ($(startNode,endNode).closest('.front').length > 0 && $(startNode,endNode).closest('.jrnlArtTitle').length == 0 && $(startNode,endNode).closest('.jrnlAbsGroup').length == 0) || ($(startNode,endNode).closest('.back').length > 0 && $(startNode,endNode).closest('.jrnlAppGroup').length == 0) || $(startNode,endNode).closest('[class$="Ref"]').length>0){ // Replace should not happen in component/citation/reference - priya #119-#123
												addDisableattr = 1;
												if($(startNode,endNode).closest('.back').length > 0 && $(startNode,endNode).closest('[data-id^="BLK_"]').length > 0 &&  $(startNode,endNode).closest('.floatLabel').length == 0){
													addDisableattr = 0;
												}
											}
											if (node){
												//add it to an array to return matched nodes
												for (var m = 0, n = node.length; m < n; m++){
													//matchNodes.unshift(matchedNodes[m]);
													node[m].setAttribute('start-offset', matchLocation[0]);
													node[m].setAttribute('end-offset', matchLocation[1]);
													if(addDisableattr==1){
														node[m].setAttribute('data-replace',"false");
													}
													matchedNodes.unshift(node[m]);
												}
											}
										}
									}	
									//sel.collapse(true);
									//range.addRange(sel);
									count++;
									if (matches.length){
										var startNode = false, endNode = false;
										matchLocation = matches.shift();
									}else{
										rangeMatch = false;
										tl = tnLength;
										//break;
									}
								}else if (startNode) {
									// Intersecting node
									innerNodes.push(curNode);
									rangeMatch = false;
								}
								if (!startNode && curNode.length + atIndex > matchLocation[0]) {
									// We've found the match start
									startNode = curNode;
									startNodeIndex = matchLocation[0] - atIndex - 1;
								}else{
									rangeMatch = false;
								}
							}
							atIndex += curNode.length;
						}
						
						for (var m = 0, n = matchedNodes.length; m < n; m++){
							matchNodes.unshift(matchedNodes[m]);
						}
					}// end of match length
				}//end of if regex matches text content
			}
			//console.log(new Date());
			return matchNodes;
		},
		
		replace: function(options){
			if (options.type == "all"){
				if (options.replaceNodes){
					var highLighters = $(options.replaceNodes);
				}else{	
					var highLighters = $('.highlight.findOverlay');
				}
			}else{
				if (options.replaceNodes){
					var highLighters = $(options.replaceNodes);
				}else{	
					var highLighters = $('.highlight.findOverlay.active')
				}
			}
			var replaceText = options.replaceText;
			var replacedNodes = [];
			var uid = new Date().valueOf() + new Date().getUTCMilliseconds();
			for (var h = 0, hl = highLighters.length; h < hl; h++){
				//inserted by kiran for activate the next node from del node when trying to replace del text				
				if($(highLighters[h]).attr("data-replace")=="false"){
				var nextRid=parseInt($('.highlight.findOverlay.active').attr("data-rid"))+1;
				var arr=[];
				$('.highlight.findOverlay').each(function(index){arr[index]=parseInt($(this).attr('data-rid'))});
				var lastRid=Math.max.apply(null,arr);
				for(var i=nextRid;i<=lastRid;i++){
					if ($('.highlight.findOverlay[data-rid="' + i + '"]').length != 0){
						nextRid=i;
						break;
					}
				}
				$('.highlight.findOverlay.active').removeClass("active");
				$('.highlight.findOverlay[data-rid="' + nextRid + '"]').addClass('active');
					continue;
				}
				var highLighter = highLighters[h];
				var currentNode = $('#contentDivNode #' + highLighter.getAttribute('data-parent-id'))[0];
				//var currentNode = $('#contentDivNode #' + highLighter.parentNode.getAttribute('clone-id'))[0];
				matchLocation = [highLighter.getAttribute('start-offset'), highLighter.getAttribute('end-offset')]
				var textNodes = textNodesUnder(currentNode);
				var tnLength = textNodes.length;
				var startNode = false, endNode = false, startNodeIndex, endNodeIndex, innerNodes = [], atIndex = 0, matchIndex = 0, rangeMatch = true;
				var rangeArray = [];
				for (var tl = 0; tl < tnLength; tl++) {
					var curNode = textNodes[tl];
					if ($(curNode.parentNode).css('display') == 'none' || (options.spellInlineEle!='undefined' && $(textNodes[tl]).closest(options.spellInlineEle).length>0)){
						//console.log("textnode exists"+textNodes[tl].data); //closest node skip deletion
						continue;
					}
					rangeMatch = true;
					while (rangeMatch){
						if (!endNode && startNode && curNode.length + atIndex >= matchLocation[1]) {
							//inserted by kiran:-if try to replace any deleted text it disable the replace buttons
							if($( curNode.parentElement ).hasClass( "del" ))
							{
								var nextRid=parseInt($('.highlight.findOverlay.active').attr("data-rid"))+1;
								var arr=[];
								$('.highlight.findOverlay').each(function(index){arr[index]=parseInt($(this).attr('data-rid'))});
								var lastRid=Math.max.apply(null,arr);
								for(var i=nextRid;i<=lastRid;i++){
									if ($('.highlight.findOverlay[data-rid="' + i + '"]').length != 0){
										nextRid=i;
										break;
									}
								}
								$('.highlight.findOverlay.active').removeClass("active");
								$('.highlight.findOverlay[data-rid="' + nextRid + '"]').addClass('active');
								return false;
							}
							// We've found the ending
							endNode = curNode;
							endNodeIndex = matchLocation[1] - atIndex - 1;
							range = rangy.getSelection();
							sel = range.getRangeAt(0);
							var addOn = 1;  
							sel.setStart(startNode, startNodeIndex + addOn);
							sel.setEnd(endNode, endNodeIndex + addOn);
							while (range.text() != highLighter.getAttribute('data-content')){
								addOn++;
								sel.setStart(startNode, startNodeIndex + addOn);
								sel.setEnd(endNode, endNodeIndex + addOn);
							}
							var currDate = new Date();
							var time = currDate.getTime();
							var timeString  = currDate.toLocaleDateString("en-au", {year: "numeric", month: "numeric",day: "numeric",hour: "2-digit",minute: "2-digit"});
							var delNode = document.createElement('span');
							delNode.setAttribute('class', 'del cts-1');
							delNode.setAttribute('data-cid', uid);
							delNode.setAttribute('data-time', time);
							delNode.setAttribute('title', 'Deleted by ' + kriyaEditor.user.name + ' - '+timeString);
							delNode.setAttribute('data-username', kriyaEditor.user.name);
							delNode.setAttribute('data-userid',kriya.config.content.role+' '+kriyaEditor.user.id);
							delNode.innerHTML = sel.toHtml();
							sel.deleteContents();
							sel.insertNode(delNode);
							var insNode = document.createElement('span');
							insNode.setAttribute('class', 'ins cts-1');
							insNode.setAttribute('data-cid', uid++);
							insNode.setAttribute('data-time', time);
							insNode.setAttribute('title', 'Inserted by ' + kriyaEditor.user.name + ' - '+timeString);
							insNode.setAttribute('data-username', kriyaEditor.user.name);
							insNode.setAttribute('data-userid',kriya.config.content.role+' '+kriyaEditor.user.id);
							insNode.innerHTML = replaceText;
							delNode.parentNode.insertBefore(insNode, delNode);
							insNode.parentNode.insertBefore(delNode, insNode);
							
							replacedNodes.push(insNode);
							//console.log(count);
							/*if (matches.length){
								var startNode = false, endNode = false;
								matchLocation = matches.shift();
							}else{
								rangeMatch = false;
								tl = tnLength;
								//break;
							}*/
							rangeMatch = false;
							break;
						}else if (startNode) {
							// Intersecting node
							innerNodes.push(curNode);
							rangeMatch = false;
						}
						if (!startNode && curNode.length + atIndex > matchLocation[0]) {
							// We've found the match start
							startNode = curNode;
							startNodeIndex = matchLocation[0] - atIndex - 1;
						}else{
							rangeMatch = false;
						}
					}
					atIndex += curNode.length;
				}
			}
			return replacedNodes;
		},

		highlight: function(range, className, contexNode){
			if (typeof(className) == "undefined"){
				className = settings.options.className;
			}else{
				className = settings.options.className + ' ' + className;
			}
			var ancestorNode = $('#contentDivNode');
			var sel = range.getRangeAt(0);
			var rect = sel.getBoundingClientRect();
			var topOffset = sel.getBoundingClientRect().top + $('#contentDivNode').scrollTop()
			//when finding text taking text parent as one node and when replacing text at that time taking text parent as another node. because of that replacing text not happening in fig,table... at the time of search in object option selected.-#2668
			if(contexNode){
				var parentNode = contexNode;
			}else{
				var parentNode = $(sel.startContainer).closest('*[id]');
			}
			while (parentNode[0].nodeName.match(/^(A|SPAN|EM|I|B|STRONG|SUB|SUP|U)$/)){
				parentNode = parentNode.parent()
			}
			var topPos = topOffset - ancestorNode.offset().top;
			var parentLeft = ancestorNode.offset().left;
			if (sel.getBoundingClientRect().left >= sel.getStartClientPos().x){
				var leftOffset =  sel.getBoundingClientRect().left;
				var rightOffset = sel.getEndClientPos().x;
			}else{
				var leftOffset =  sel.getStartClientPos().x;
				var rightOffset = sel.getEndClientPos().x - sel.getBoundingClientRect().left;
			}
			var parentFontSize = parseInt($(parentNode).css('font-size'));
			var parentLineHeight = parseInt($(parentNode).css('line-height'));
			var height = parentLineHeight;
			var topLeft = rect.top;
			var topRight = rect.bottom - parentLineHeight;
			var rangeHeight = sel.getBoundingClientRect().bottom - sel.getBoundingClientRect().top;
			var parentWidth = parentNode.width();
			if (rangeHeight < (parentLineHeight + parentLineHeight/2)){
				topRight = topLeft;
			}
			if (isNaN(height)) return;
			var i = 0, highLight = true;
			var highLightNodes = [];
			while (highLight){
				if (topLeft >= topRight){
					if (rightOffset < leftOffset) leftOffset = parentLeft;
					var currWidth = rightOffset - leftOffset;
					var height = parentLineHeight;
					if (i == 0){
						if (topOffset < topLeft) topPos = topLeft - ancestorNode.offset().top
						//leftOffset = leftOffset + parentLeft;
					}
				}else{
					var currWidth = Math.abs(parentWidth - leftOffset);
				}
				leftOffset = leftOffset - parentLeft;
				if (leftOffset == 0) leftOffset = parentNode.offset().left;
				var rHeight = height;
				if (parentLineHeight == rHeight){
					rHeight = rHeight - (parentLineHeight - parentFontSize)/2;
				}
				
				var divNode = $('<span class="' + className + '" data-content="' + sel.text() + '" style="top:' + topPos + 'px;left:' + leftOffset + 'px; width:' + currWidth + 'px; height:' + rHeight + 'px; display:inline-block;">');
				divNode.attr('data-parent-id', parentNode.attr('id'));
				if (i != 0) divNode.attr('data-cont', 'true');
				if($(sel.startContainer).closest('.jrnlTblBlock').length>0){ // for horizontal scroll hightlight position #383 - priya
					divNode.attr('data-tableBlock', $(sel.startContainer).closest('.jrnlTblBlock').attr('id')).attr('data-left',leftOffset);
				}
				
				highLightNodes.push(divNode[0]);
				
				if($(sel.startContainer).closest('.jrnlTblContainer').length>0){
					var tblContainer = $(sel.startContainer).closest('.jrnlTblContainer');
					var tblID = tblContainer.find('table').attr('id');
					var tblTopOffset = tblContainer[0].getBoundingClientRect().top + $('#contentDivNode').scrollTop();
					var containerTopPos = tblTopOffset - ancestorNode.offset().top;
					var tblLeftOffset = leftOffset - tblContainer.offset().left;

					divNode.css('top', (topPos-containerTopPos) + 'px').css('left', tblLeftOffset + 'px');
					if($('#clonedContent [data-table-id="' + tblID + '"]').length == 0){
						var tblContainerHeight = tblContainer.find('table')[0].getBoundingClientRect().height;
						var tblMarginTop    = tblContainer.find('table').css('margin-top');
						tblMarginTop = parseInt(tblMarginTop);
						tblContainerHeight = tblContainerHeight + tblMarginTop;
						var tblMarginBottom = tblContainer.find('table').css('margin-bottom');
						tblMarginBottom = parseInt(tblMarginBottom);
						tblContainerHeight = tblContainerHeight + tblMarginBottom;
						
						var margins = "";
						if(tblContainer.closest('.jrnlTblBlock').length > 0){
							margins = "margin:0px " + tblContainer.closest('.jrnlTblBlock').css('padding-right') + " 0px " + tblContainer.closest('.jrnlTblBlock').css('padding-left') + ";";
						}

						var containerDiv  = $('<div data-table-id="' + tblID + '" style="top:' + containerTopPos + 'px; width:' + tblContainer.width() + 'px; height:' + tblContainerHeight + 'px;position: inherit;max-height: 350px;overflow-y: scroll;' + margins + '">');
						containerDiv.prepend(divNode);
						$('#clonedContent').prepend(containerDiv);
					}else{
						$('#clonedContent [data-table-id="' + tblID + '"]').prepend(divNode);
					}
					if($('#clonedContent [data-table-id="' + tblID + '"] .dummyHighlight').length == 0){
						var lastNode = tblContainer.find('table th, table td').last();
						var dummyNodeTop = lastNode[0].getBoundingClientRect().top + $('#contentDivNode').scrollTop();
						var dummyNodeLeft = lastNode[0].getBoundingClientRect().left + $('#contentDivNode').scrollLeft();
						dummyNodeTop = dummyNodeTop-containerTopPos; // to handle horizontal scroll
						$('#clonedContent [data-table-id="' + tblID + '"]').prepend($('<span class="dummyHighlight" style="top:' + dummyNodeTop + 'px; left:' + dummyNodeLeft + 'px; display:inline-block;opacity:0;width: 18px;height: 18.5px;position: absolute;z-index: 0;"/>'));
					}
				}else{
					//$('#clonedContent [clone-id="'+ parentNode.attr('id') + '"]').append(divNode);
					$('#clonedContent').prepend(divNode);
				}

				
				if(topLeft >= topRight){
					highLight = false;
				}else if (topLeft != topRight){
					leftOffset = parentLeft;
					topPos += height;
					topLeft += height;
					if (i == 0){
						height = topRight - topLeft;
					}
					i++;
				}
			}
			if(className.indexOf('spelling')>=0){
				sel.collapse(true);
				range.addRange(sel);
			}
			return highLightNodes;
		},
	};
	return findReplace;
	
	function textNodesUnder(el){
		var n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
		while(n=walk.nextNode()) a.push(n);
		return a;
	}
	
	
	function getMatchIndexes(m, captureGroup) {
		captureGroup = captureGroup || 0;

		if (!m[0]) {
			throw 'findAndReplaceDOMText cannot handle zero-length matches';
		}

		var index = m.index;

		if (captureGroup > 0) {
			var cg = m[captureGroup];

			if (!cg) {
				throw 'Invalid capture group';
			}

			index += m[0].indexOf(cg);
			m[0] = cg;
		}

		return [index, index + m[0].length, [m[0]]];
	}
	
})(findReplace || {});